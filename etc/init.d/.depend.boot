TARGETS = console-setup alsa-utils mountkernfs.sh ufw pppd-dns apparmor hostname.sh plymouth-log x11-common dns-clean udev keyboard-setup mountdevsubfs.sh resolvconf brltty procps cryptdisks cryptdisks-early networking hwclock.sh checkroot.sh lvm2 checkfs.sh urandom mountnfs.sh mountall.sh mountall-bootclean.sh bootmisc.sh checkroot-bootclean.sh mountnfs-bootclean.sh kmod
INTERACTIVE = console-setup udev keyboard-setup cryptdisks cryptdisks-early checkroot.sh checkfs.sh
udev: mountkernfs.sh
keyboard-setup: mountkernfs.sh udev
mountdevsubfs.sh: mountkernfs.sh udev
resolvconf: dns-clean
brltty: mountkernfs.sh udev
procps: mountkernfs.sh udev
cryptdisks: checkroot.sh cryptdisks-early udev lvm2
cryptdisks-early: checkroot.sh udev
networking: resolvconf mountkernfs.sh urandom procps dns-clean
hwclock.sh: mountdevsubfs.sh
checkroot.sh: hwclock.sh keyboard-setup mountdevsubfs.sh hostname.sh
lvm2: cryptdisks-early mountdevsubfs.sh udev
checkfs.sh: cryptdisks lvm2 checkroot.sh
urandom: hwclock.sh
mountnfs.sh: networking
mountall.sh: lvm2 checkfs.sh checkroot-bootclean.sh
mountall-bootclean.sh: mountall.sh
bootmisc.sh: mountall-bootclean.sh checkroot-bootclean.sh mountnfs-bootclean.sh udev
checkroot-bootclean.sh: checkroot.sh
mountnfs-bootclean.sh: mountnfs.sh
kmod: checkroot.sh
