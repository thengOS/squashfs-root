<?php
### This file is part of the dictionaries-common package.
### It has been automatically generated.
### DO NOT EDIT!
$SQSPELL_APP = array (
  'American English (aspell)' => 'aspell -a -d en_US   ',
  'Australian English (hunspell)' => 'hunspell -a -d en_AU   ',
  'British English (aspell)' => 'aspell -a -d en_GB   ',
  'Canadian English (aspell)' => 'aspell -a -d en_CA   ',
  'English (aspell)' => 'aspell -a -d en   ',
  'Portuguese/Brazil (hunspell)' => 'hunspell -a -d pt_BR -w ����������������������������������  ',
  'Portuguese/European (hunspell)' => 'hunspell -a -d pt_PT -w ��������������������������������  ',
  'Spanish 8 bit (hunspell)' => 'hunspell -a -d es -w ������������������������  -B',
  'russian (hunspell)' => 'hunspell -a -d ru_RU   '
);
