  8�    �            @   @A operação executada em $(ARG1) foi anulada.          0   0O acesso a $(ARG1) foi negado.          $   $$(ARG1) já existe.    1     &   &O destino já existe.    :    �  �Você está prestes a salvar/exportar uma biblioteca básica protegida por senha contendo os módulo(s) 
$(ARG1)
que são grandes demais para serem armazenados em formato binário. Se desejar que os usuários sem acesso à senha da biblioteca possam executar macros neste(s) módulo(s), será necessário dividir estes módulos em módulos menores. Deseja continuar a salvar/exportar esta biblioteca?          N   NA soma de verificação dos dados do $(ARG1) está incorreta.         P   PNão é possível criar o objeto $(ARG1) no diretório $(ARG2).         <   <Não foi possível ler os dados de $(ARG1).         P   PNão foi possível executar a operação de procura em $(ARG1).         N   NNão foi possível executar a operação de tell em $(ARG1).          B   BNão foi possível gravar os dados para $(ARG1).     	     D   DAção impossível: $(ARG1) é o diretório atual.     
     ,   ,$(ARG1) não está pronto.          `   `Ação impossível: $(ARG1) e $(ARG2) são dispositivos diferentes (unidades).          B   BErro geral de entrada/saída ao acessar $(ARG1).          R   RFoi feita uma tentativa de acessar $(ARG1) de maneira inválida.          8   8$(ARG1) contém caracteres inválidos.          >   >O dispositivo (unidade) $(ARG1) é inválido.         <   <Os dados de $(ARG1) têm tamanho inválido.         R   RA operação em $(ARG1) foi iniciada com um parâmetro inválido.         d   dNão é possível executar a operação porque $(ARG1) contém caracteres coringas.         @   @Erro durante o acesso compartilhado a $(ARG1).          @   @$(ARG1) contém caracteres na posição errada.         :   :O nome $(ARG1) contém caracteres demais.         &   &$(ARG1) não existe.          0   0O caminho $(ARG1) não existe.          X   XA operação em $(ARG1) não pode ser feita neste sistema operacional.          0   0$(ARG1) não é um diretório.          ,   ,$(ARG1) não é um arquivo.         :   :Não há espaço no dispositivo $(ARG1).          n   nNão é possível executar a operação em $(ARG1) porque muitos arquivos já estão abertos.         n   nNão é possível executar a operação em $(ARG1) porque não há mais memória disponível.         ^   ^A operação em $(ARG1) não pode continuar porque há mais dados pendentes.          >   >Não é possível copiar $(ARG1) em si mesmo.          H   HErro desconhecido de entrada/saída ao acessar $(ARG1).    !     <   <$(ARG1) está protegido contra gravação.     "     8   8$(ARG1) não está no formato correto.     #     :   :A versão de $(ARG1) não está correta.     $     0   0A unidade $(ARG1) não existe.     %     .   .A pasta $(ARG1) não existe.     &     <   <A versão Java instalada não é suportada.    '     H   HA versão Java instalada de $(ARG1) não é suportada.     (     j   jA versão Java instalada não é suportada, é necessária a versão $(ARG1) ou superior.    )     t   tA versão Java instalada do $(ARG1) não é suportada, pelo menos a versão $(ARG2) é necessária.    *     D   DOs dados associados à parceria estão corrompidos.    +     L   LOs dados associados à parceria $(ARG1) estão corrompidos.    ,     4   4O volume $(ARG1) não está pronto.    -     P   P$(ARG1) não está pronto; coloque uma mídia de armazenamento.    .     \   \O volume de $(ARG1) não está pronto; coloque uma mídia de armazenamento.    /     *   *Coloque o disco $(ARG1).     0     H   HNão é possível criar o objeto no diretório $(ARG1).    2     �   �O %PRODUCTNAME não pode evitar que os arquivos sejam sobrescritos quando este protocolo de transmissão for utilizado. Deseja continuar mesmo assim?    3    �  �O arquivo '$(ARG1)' foi corrompido e não pode ser aberto normalmente. O %PRODUCTNAME tentará abri-lo e recuperá-lo.

O defeito pode ser decorrente da manipulação inadequada do documento ou de um dano estrutural no documento em consequência de erros de transmissão de dados.

Recomendamos não confiar no conteúdo do documento recuperado.
A execução de macros estará desabilitada para este documento.

Deseja que o %PRODUCTNAME recupere o arquivo?
     4     b   bNão foi possível reparar o arquivo '$(ARG1)', e assim ele não pode ser aberto.    5     �   �Os dados de configuração no '$(ARG1)' estão corrompidos. Sem eles, algumas funções podem não operar corretamente.
Deseja inicializar o %PRODUCTNAME sem os dados de configuração corrompidos?    6      O arquivo de configuração pessoal '$(ARG1)' está corrompido e deve ser excluído para que seja possível prosseguir. Algumas de suas configurações pessoais podem ser perdidas.
Deseja inicializar o %PRODUCTNAME sem os dados de configuração corrompidos?     7     �   �A fonte de dados de configuração '$(ARG1)' não está disponível. Sem ela, algumas funções podem não operar corretamente.    8     �   �A fonte de dados de configuração '$(ARG1)' não está disponível. Sem ela, algumas funções podem não operar corretamente.
Deseja continuar a inicialização do %PRODUCTNAME sem os dados de configuração faltantes?     9     L   LO formulário contém dados inválidos. Deseja prosseguir?     ;     �   �O arquivo $(ARG1) está travado por outro usuário. No momento, não é possível outorgar outro acesso de escrita nesse arquivo.    <     �   �O arquivo $(ARG1) está travado por você mesmo. No momento, não é possível outorgar outro acesso de escrita a esse arquivo.    =     F   FO arquivo $(ARG1) não está travado por você mesmo.    >    $  $A trava obtida anteriormente para o arquivo $(ARG1) expirou.
Isso pode ocorrer por problemas no servidor gerenciador da trava do arquivo. Não é possível garantir que as operações de escrita neste arquivo não sobrescreverão as alterações feitas por outros usuários!    a�     �   �Não foi possível verificar a identidade do site $(ARG1).

Antes de aceitar esse certificado, examine com cuidado o certificado do site. Pretende confiar nesse certificado com a finalidade de identificar o site internet $(ARG1)?   a�     �   �$(ARG1) é um site que usa um certificado de segurança para criptografar dados durante a transmissão, mas a data do certificado venceu em $(ARG2).

Verifique se o relógio de seu computador está com a data e hora certa.    a�    �  �Você tentou estabelecer uma conexão com $(ARG1). Entretanto, o certificado de segurança apresentado pertence a $(ARG2). É possível, ainda que pouco provável, que alguém esteja tentando interceptar sua comunicação com este site internet.

Ao suspeitar que o certificado apresentado não pertence a $(ARG1), cancele a conexão e notifique o administrador do site.

Deseja continuar mesmo assim?   a�     �   �Não foi possível validar o certificado. Examine com cuidado o certificado deste site.

Ao suspeitar do certificado apresentado, cancele a conexão e notifique o administrador do site.   a�     F   FAviso de segurança: O nome do domínio não confere    a�     J   JAviso de segurança: O certificado do site está vencido    a�     H   HAviso de segurança: O certificado do site é inválido    ?     �   �O componente não pode ser carregado, as causas possíveis são uma instalação defeituosa ou incompleta.
Mensagem de erro completa:

 $(ARG1).    8�     8   8Lembra~r a senha até o fim da sessão    8�          ~Lembrar senha    8�     8   8A confirmação da senha não confere.    8�     .   .A senha mestre está errada.    8�     &   &A senha está errada.   8�     B   BSenha incorreta. O arquivo não pode ser aberto.    8�     F   FSenha incorreta. O arquivo não pode ser modificado.    8�     &   &Usuário desconhecido   8�     *   *Documento sendo utilizado   8�     �   �O arquivo do documento '$(ARG1)' esta travado para edição por:

$(ARG2)

Abra o documento em somente leitura ou abra uma cópia do documento para editar.

   8�     (   (Ab~rir somente-leitura    8�        Abrir ~cópia   8�     4   4O documento foi alterado por outros   8�     �   �O arquivo foi alterado depois que foi aberto para edição no %PRODUCTNAME. Salvar esta sua versão do documento irá sobrescrever as alterações feitas por outros usuários.

Deseja salvar mesmo assim?

   8�     $   $~Salvar mesmo assim   8�     "   "Documento em uso    8�     �   �O arquivo do documento '$(ARG1)' está travado para edição por você mesmo em um sistema diferente desde $(ARG2)

Abra o documento em somente leitura, ou ignore a trava de arquivo e abra o documento para edição.

   8�     (   (Ab~rir somente-leitura    8�        ~Abrir    8�     8   8Não foi possível bloquear o documento   8�     �   �Não foi possível travar o arquivo para dar acesso exclusivo ao %PRODUCTNAME, por que você não tem permissão para criar um arquivo de trava no mesmo local.   8�     2   2Não mostrar mais e~sta mensagem    8�     *   *Documento sendo utilizado   8�     �   �O arquivo do documento '$(ARG1)' está travado para edição por:

$(ARG2)

Tente salvar seu documento mais tarde ou então salve uma cópia deste documento.

   8�     *   *Tenta~r salvar novamente    8�          ~Salvar como...   8�      O arquivo do documento '$(ARG1)' está travado para edição por você mesmo em um sistema diferente desde $(ARG2)

Feche o documento no outro sistema e tente salvar novamente ou então ignore a trava de arquivo e salve o documento atual.

    8�     *   *Tenta~r salvar novamente    8�        ~Salvar   8�     ,   ,Fluxos não criptografados    8�     2   2Assinatura de documento inválida   8�     8   8Digite a senha para abrir o arquivo: 
    8�     <   <Digite a senha para modificar o arquivo: 
    8�     �   �Já existe um arquivo com o nome "%NAME" na localização "%FOLDER".
Escolha Substituir para sobrescrever o arquivo existente ou forneça um novo nome.   8�     j   jJá existe um arquivo com o nome "%NAME" na localização "%FOLDER".
Digite um novo nome.   8�     8   8Forneça um nome de arquivo diferente!    8�     "   "Digite a senha:     8�     $   $Confirme a senha:     8�        Definir senha   8�          Digite a senha    8�     �   �A senha de confirmação não confere com a senha. Defina a senha novamente digitando a mesma senha em ambas as caixas.    �           +     �  )�    8�        8�  �    8�  (    8�  H    8�  �    8�  �    8�  �    8�      8�  \    8�  �    8�  �    8�   Z    8�   �    8�   �    8�   �    8�  !�    8�  !�    8�  !�    8�  "�    8�  #
    8�  #"    8�  #Z    8�  $
    8�  $<    8�  $f    8�  %    8�  %@    8�  %`    8�  &b    8�  &�    8�  &�    8�  &�    8�  '    8�  ':    8�  'v    8�  (    8�  (�    8�  (�    8�  (�    8�  )    8�  )$    8�  )D  