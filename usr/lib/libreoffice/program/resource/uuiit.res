  8�    �            F   FL'operazione eseguita su $(ARG1) è stata interrotta.         *   *Accesso a $(ARG1) negato.         &   &$(ARG1) esiste già.     1     .   .La destinazione esiste già.     :    �  �Stai per salvare/esportare una libreria di base protetta con password contenente i moduli 
$(ARG1)
le cui dimensioni sono troppo grandi per la memorizzazione in formato binario. Se desideri che gli utenti che non dispongono dell'accesso alla password della libreria possano eseguire macro in questi moduli, devi dividere i moduli in diversi moduli più piccoli. Vuoi continuare a salvare/esportare questa libreria?          L   LLa somma di controllo dei dati di $(ARG1) non è corretta.          N   NImpossibile creare l'oggetto $(ARG1) nella cartella $(ARG2).          8   8Impossibile leggere i dati di $(ARG1).          N   NImpossibile portare a termine l'operazione 'seek' su $(ARG1).         N   NImpossibile portare a termine l'operazione 'tell' su $(ARG1).         :   :Impossibile scrivere i dati per $(ARG1).     	     N   NL'operazione non è ammessa: $(ARG1) è la cartella corrente.    
     (   ($(ARG1) non è pronto.          b   bL'operazione non è ammessa: $(ARG1) e $(ARG2) sono dispositivi diversi (unità).         D   DErrore generale di I/O durante l'accesso a $(ARG1).         Z   ZÈ stato rilevato un tentativo di accedere a $(ARG1) in modo non valido.          8   8$(ARG1) contiene caratteri non validi.          @   @Il dispositivo (unità) $(ARG1) non è valido.          B   BLa lunghezza dei dati di $(ARG1) non è ammessa.          R   RIl parametro per avviare l'operazione su $(ARG1) non è ammesso.          \   \Impossibile eseguire l'operazione perché $(ARG1) contiene caratteri jolly.         >   >Errore durante l'accesso condiviso a $(ARG1)          @   @$(ARG1) contiene caratteri in posizione errata.         2   2Il nome $(ARG1) è troppo lungo.          $   $$(ARG1) non esiste.         0   0Il percorso $(ARG1) non esiste.         L   LIl sistema operativo non supporta l'operazione su $(ARG1).          .   .$(ARG1) non è una cartella.          (   ($(ARG1) non è un file.         B   BL'unità $(ARG1) non ha più spazio disponibile.          ^   ^Impossibile eseguire l'operazione su $(ARG1) perché sono aperti troppi file.         p   pImpossibile eseguire l'operazione su $(ARG1) perché la memoria disponibile non è sufficiente.         \   \Impossibile proseguire l'operazione su $(ARG1) perché mancano alcuni dati.         :   :Impossibile copiare $(ARG1) su se stesso.          H   HErrore sconosciuto di I/O durante l'accesso a $(ARG1).     !     2   2$(ARG1) è protetto da scrittura.    "     .   .$(ARG1) ha un formato errato.    #     8   8La versione di $(ARG1) non è corretta.    $     .   .L'unità $(ARG1) non esiste.     %     0   0La cartella $(ARG1) non esiste.    &     B   BLa versione di Java installata non è supportata.    '     J   JLa versione di Java $(ARG1) installata non è supportata.    (     �   �La versione di Java installata non è supportata. È necessario installare la versione $(ARG1) o una versione successiva.    )     �   �La versione di Java $(ARG1) installata non è supportata. È necessario installare la versione $(ARG2) o una versione successiva.    *     D   DI dati relativi alla partnership sono danneggiati.     +     L   LI dati relativi alla partnership $(ARG1) sono danneggiati.     ,     2   2Il volume $(ARG1) non è pronto.     -     D   D$(ARG1) non è pronto; inserisci un supporto dati.     .     N   NIl volume $(ARG1) non è pronto; inserisci un supporto dati.     /     ,   ,Inserisci il disco $(ARG1).    0     F   FImpossibile creare l'oggetto nella cartella $(ARG1).     2     �   �L'uso di questo protocollo di trasmissione può causare un'involontaria sovrascrittura di file in %PRODUCTNAME. Vuoi procedere comunque?     3    �  �Impossibile aprire il file '$(ARG1)' in quanto è danneggiato. %PRODUCTNAME può tentare di riparare il file.

Il danno potrebbe essere frutto di una manipolazione o di un problema durante il trasferimento del file.

È consigliato non fidarsi del documento riparato.
L'esecuzione delle macro è disabilitata per questo documento.

Vuoi che %PRODUCTNAME ripari il file?
     4     X   XImpossibile riparare il file '$(ARG1)'. Il file non può essere aperto.    5     �   �I dati di configurazione in '$(ARG1)' sono danneggiati. Senza questi dati è possibile che alcune funzioni non operino correttamente.
Vuoi continuare l'esecuzione di %PRODUCTNAME senza i dati di configurazione danneggiati?     6    *  *Il file di configurazione personale '$(ARG1)' è danneggiato e deve essere eliminato perché il processo possa continuare. È possibile che alcune impostazioni personali siano andate perdute.
Vuoi continuare l'esecuzione di %PRODUCTNAME senza i dati di configurazione danneggiati?     7     �   �La sorgente dei dati di configurazione '$(ARG1)' non è disponibile. Senza questi dati è possibile che alcune funzioni non operino correttamente.     8     �   �La sorgente dei dati di configurazione '$(ARG1)' non è disponibile. Senza questi dati è possibile che alcune funzioni non operino correttamente.
Vuoi continuare l'esecuzione di %PRODUCTNAME senza i dati di configurazione mancanti?     9     F   FIl formulario contiene dati errati. Vuoi continuare?     ;     �   �Il file $(ARG1) è bloccato da un altro utente. Al momento non può essere garantito un altro accesso in scrittura al file.    <     �   �Il file $(ARG1) è bloccato da voi. Al momento non può essere garantito un altro accesso in scrittura al file.    =     B   BIl file $(ARG1) non è al momento bloccato da te.    >      Il blocco precedentemente ottenuto per il file $(ARG1) è scaduto.
Ciò può essere dovuto a problemi del server nella gestione del blocco. Non si garantisce che le operazioni di scrittura sul file non sovrascrivano le modifiche eseguite da altri utenti!    a�     �   �Impossibile verificare l'identità del sito $(ARG1) 

Prima di accettare questo certificato, devi esaminare attentamente questo certificato del sito. Hai intenzione di accettare questo certificato identificativo del sito web $(ARG1)?   a�     �   �$(ARG1) è un sito che usa un certificato di sicurezza per codificare i dati durante la trasmissione del file, ma il certificato è scaduto in $(ARG2).

Dovresti verificare che l'orario del tuo computer sia corretto.    a�    �  �Hai tentato di stabilire una connessione con $(ARG1). Tuttavia, il certificato di sicurezza presentato appartiene a $(ARG2). È possibile, benché improbabile, che qualcuno possa cercare di intercettare la tua comunicazione con questo sito web.

Se sospetti che il certificato mostrato non appartenga a $(ARG1), annulla la connessione e notifica l'amministratore del sito.

Continuare lo stesso?   a�     �   �Il certificato potrebbe non essere convalidato. Esamina attentamente il certificato del sito.

Se hai dubbi circa la validità del certificato mostrato, annulla la connessione e notifica l'amministratore del sito.   a�     J   JAvviso di sicurezza: nome del dominio non corrispondente    a�     D   DAvviso di sicurezza: certificato del server scaduto   a�     H   HAvviso di sicurezza: certificato del server non valido     ?     �   �Impossibile caricare il componente, l'installazione è probabilmente incompleta o danneggiata.
Messaggio di errore completo:

 $(ARG1).   8�     F   FMemorizza la password fino al termine della sessione    8�     $   $Memorizza pa~ssword   8�     :   :La password di conferma non corrisponde.    8�     2   2La password principale è errata.   8�     (   (La password è errata.    8�     N   NLa password non è corretta. Impossibile aprire il documento.   8�     R   RLa password non è corretta. Impossibile modificare il documento.   8�     $   $Utente sconosciuto    8�     &   &Documento già in uso   8�     �   �Il file '$(ARG1)' non è modificabile perché bloccato da:

$(ARG2)

Apri il documento in sola lettura oppure modifica una copia.

   8�     &   &Apri in sola lettu~ra   8�        Apri ~copia   8�     8   8Il documento è stato cambiato da altri   8�     �   �Il file è stato modificato da quando è stato aperto per la modifica con %PRODUCTNAME. Salvare questa versione del documento sovrascriverà le modifiche fatte da altri.

Vuoi salvare comunque?

   8�          ~Salva comunque   8�     "   "Documento in uso    8�     �   �Il documento '$(ARG1)' è bloccato per la modifica su un altro sistema dal $(ARG2)

Apri il documento in sola lettura, o apri il file per la modifica ignorando il blocco.

    8�     &   &Apri in sola lettu~ra   8�        ~Apri   8�     2   2Impossibile bloccare il documento   8�     �   �%PRODUCTNAME non è riuscito a ottenere un accesso esclusivo al file. Non hai i permessi per la creazione di un file di blocco nel percorso del file.   8�     4   4~Non mostrare più questo messaggio   8�     &   &Documento già in uso   8�     �   �Il documento '$(ARG1)' è bloccato per la modifica da:

$(ARG2)

Riprova a salvare più tardi oppure salva una copia del documento.

   8�     $   $~Riprova a salvare    8�          ~Salva come...    8�     �   �Il documento '$(ARG1)' è bloccato per la modifica su un altro sistema dal $(ARG2)

Chiudi il documento sull'altro sistema e riprova a salvare, o salva il documento corrente ignorando il blocco.

    8�     $   $~Riprova a salvare    8�        ~Salva    8�     $   $Flussi non cifrati    8�     0   0Firma del documento non valida    8�     B   BInserisci la password per l'apertura del file: 
    8�     @   @Inserisci la password per modificare il file: 
   8�     �   �Esiste già un file di nome "%NAME" nella posizione "%FOLDER".
Scegli Sostituisci per sovrascrivere il file esistente o digita un nuovo nome.   8�     f   fEsiste già un file di nome "%NAME" nella posizione "%FOLDER".
Digita un nuovo nome.    8�     4   4Digita un nome diverso per il file.   8�     &   &Digita la password:     8�     $   $Conferma password:    8�     $   $Imposta la password   8�     &   &Inserisci la password   8�     r   rLa password di conferma non corrisponde. Impostala di nuovo affinché le due password coincidano.    �           +     �  (�    8�        8�  �    8�  �    8�      8�  L    8�  ~    8�  �    8�  �    8�  F    8�  j    8�  �    8�   $    8�   J    8�   f    8�   �    8�  !r    8�  !�    8�  !�    8�  "r    8�  "�    8�  "�    8�  "�    8�  #�    8�  #�    8�  #�    8�  $v    8�  $�    8�  $�    8�  %�    8�  %�    8�  %�    8�  %�    8�  &     8�  &b    8�  &�    8�  '@    8�  '�    8�  '�    8�  (     8�  ($    8�  (H    8�  (n  