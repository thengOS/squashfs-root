  �    $,     *�        Basis Latein    *�        Latein-1    *�     $   $Latein Erweitert-A    *�     $   $Latein Erweitert-B    *�     *   *Lautschrift Erweiterungen   *�     *   *Phonetische Hilfszeichen    *�     4   4Kombinierende diakritische Zeichen    *�     "   "Basis Griechisch    *�     2   2Griechische Symbole und Koptisch    *�        Kyrillisch    *�        Armenisch   *�     "   "Basis Hebräisch    *�     (   (Erweitertes Hebräisch    *�          Basis Arabisch    *�     &   &Erweitertes Arabisch    *�        Devanagari    *�        Bengalisch    *�        Gurmukhi    *�        Gujarati    *�        Odia    *�        Tamil   *�        Telugu    *�        Kannada   *�        Malayisch   *�        Thailändisch   *�        Laotisch    *�          Basis Georgisch   *�     &   &Erweitertes Georgisch   *�        Hangul Jamo   *�     .   .Latein Erweiterte Zufügungen   *�     (   (Erweitertes Griechisch    *�     *   *Allgemeine Interpunktion    *�     *   *Hoch- und Tiefstellungen    *�     "   "Währungssymbole    *�     4   4Kombinierende diakritische Symbole    *�     "   "Buchstabensymbole   *�        Zahlensymbole   *�        Pfeilsymbole    *�     *   *Mathematische Operatoren    *�     0   0Verschiedene Technische Zeichen   *�     &   &Steuerelementsymbole    *�     *   *Maschinenlesbare Zeichen    *�     &   &Eingekreiste Zeichen    *�          Rahmungssymbole   *�        Blocksymbole    *�     &   &Geometrische Symbole    *�     &   &Verschiedene Symbole    *�        Symbolzeichen   *�     .   .CJK Symbole und Punktierungen   *�        Hiragana    *�        Katakana    *�        Bopomofo    *�     (   (Hangul kompatibles Jamo   *�     *   *Verschiedene CJK Zeichen    *�     4   4Eingekreiste CJK Zeichen und Monate   *�     $   $CJK Kompatibilität   *�        Hangul    *�     0   0Vereinheitlichte CJK-Ideogramme   +N     @   @Vereinheitlichte CJK-Ideogramme, Erweiterung A    *�     0   0Zeichensatzspezifische Symbole    *�     *   *CJK Kompatible Ideogramme   *�     4   4Alphabetische Präsentations Formen   *�     2   2Arabische Präsentations Formen-A   *�     *   *Kombinierende Halbmarken    *�     ,   ,CJK Kompatibilitätsformen    *�     &   &Kleine Form Varianten   *�     2   2Arabische Präsentationsformen-B    *�     .   .Halbbreite und breite Formen    *�     "   "Spezielle Symbole   *�        Yi-Silben   *�        Yi-Radikale   *�        Altitalisch   *�        Gotisch   *�        Mormonisch    *�     2   2Byzantinische Notenschriftzeichen   *�     $   $Notenschriftzeichen   *�     6   6Mathematische alphanumerische Zeichen   *�     @   @Vereinheitlichte CJK-Ideogramme, Erweiterung B    *�     @   @Vereinheitlichte CJK-Ideogramme, Erweiterung C    *�     @   @Vereinheitlichte CJK-Ideogramme, Erweiterung D    *�     @   @Erweiterung für CJK-Kompatibilitätsideogramme   *�        Sprachtags    *�     (   (Kyrillisch, Ergänzung    *�     "   "Variantenauswahl    *�     >   >Ergänzender Bereich zum privaten Gebrauch-A    *�     >   >Ergänzender Bereich zum privaten Gebrauch-B    *�        Limbu   *�        Tai Lue   *�        Khmer   *�     *   *Phonetische Erweiterungen   *�     0   0Verschiedene Symbole und Pfeile   *�     "   "I-Ging-Hexagramme   *�     (   (Linear-B-Silbenzeichen    *�     $   $Linear-B-Ideogramme   *�     (   (Ägäische Zahlzeichen    *�        Ugaritisch    *�        Shaw-Alphabet   *�        Osmaniya    +Z        Singhalesisch   +[        Tibetisch   +\        Birmanisch    +]        Khmer   +^        Ogham   +f        Runen   +g        Syrisch   +_        Thaana    +h        Äthiopisch   +i        Cherokee    +`     ,   ,Kanadische Aborigin Silben    +j        Mongolisch    +k     6   6Verschiedene Mathematische Symbole-A    +l     &   &Ergänzende Pfeile-A    +a        Braille   +m     &   &Ergänzende Pfeile-B    +n     6   6Verschiedene Mathematische Symbole-B    +b     (   (Ergänzte CJK Radikale    +o          Kangxi-Radikale   +p     4   4Ideographische Beschreibungszeichen   +q        Tagalog   +r        Hanunoo   +c        Tagbanwa    +t        Buhid   +s        Kanbun    +d     &   &Erweitertes Bopomofo    +e     &   &Phonetisches Katakana   *�        CJK-Striche   *�          Kyprisch Silben   *�     &   &Tai-Xuan-Jing-Zeichen   *�     .   .Variantenauswahl, Ergänzung    *�     4   4Altgriechische Notenschriftzeichen    *�     &   &Altgriechische Zahlen   *�     &   &Arabisch, Ergänzung    *�        Buginesisch   +      @   @Kombinierende diakritische Zeichen, Ergänzung    +        Koptisch    +     *   *Äthiopisch (Erweiterung)   +     $   $Äthiopisch, Zusatz   +     &   &Georgisch, Ergänzung   +        Glagolitisch    +        Kharoshthi    +     *   *Modifizierende Tonzeichen   +        Tai Lue (neu)   +	        Altpersisch   +
     6   6Phonetische Erweiterungen, Ergänzung   +     ,   ,Zusätzliche Interpunktion    +        Syloti Nagri    +        Tifinagh    +     "   "Vertikale Formen    +        N'Ko    +        Balinesisch   +     (   (Lateinisch, erweitert-C   +     (   (Lateinisch, erweitert-D   +        Phagspa   +        Phönizisch   +        Keilschrift   +     <   <Keilschrift-Zahlzeichen und -Interpunktion    +     "   "Zählstabziffern    +        Sudanesisch   +        Lepcha    +        Ol Chiki    +     (   (Kyrillisch, erweitert-A   +        Vai   +     (   (Kyrillisch, erweitert-B   +        Saurashtra    +        Kayah Li    +         Rejang    +!        Cham    +"        Alte Symbole    +#     $   $Diskos von Phaistos   +$        Lykisch   +%        Karisch   +&        Lydisch   +'          Mahjonggsteine    +(        Dominosteine    +)        Samaritanisch   +*     R   RVereinheitlichte Silbenzeichen kanadischer Ureinwohner, erweitert   ++        Lanna   +,     (   (Vedische Erweiterungen    +-        Lisu    +.        Bamum   +/     ,   ,Allgemeine indische Ziffern   +0     &   &Devanagari, erweitert   +1     *   *Hangeul-Jamo, erweitert-A   +2        Javanisch   +3     (   (Birmanisch, erweitert-A   +4        Tai Viet    +5        Meitei-Mayek    +6     *   *Hangeul-Jamo, erweitert-B   +7        Aramäisch    +8          Altsüdarabisch   +9        Avestisch   +:     &   &Inschriften-Parthisch   +;     $   $Inschriften-Pahlavi   +<        Alt-Türkisch   +=        Rumi-Ziffern    +>        Kaithi    +?     *   *Ägyptische Hieroglyphen    +@     B   BZusätzliche umschlossene alphanumerische Zeichen   +A     6   6Zusätzliche umschlossene CJK-Zeichen   +C        Mandäisch    +D        Batak   +E     ,   ,Äthiopisch (Erweiterung A)   +F        Brahmi    +G     "   "Bamum, Ergänzung   +H     "   "Kana, Ergänzung    +I        Spielkarten   +J     4   4Verschiedene piktografische Symbole   +K        Smileys   +L     ,   ,Verkehrs- und Kartensymbole   +M     (   (Alchemistische Symbole    +O     &   &Arabisch, erweitert-A   +P     8   8Arabic Mathematical Alphabetic Symbols    +Q        Chakma    +R     ,   ,Meitei-Mayek, Erweiterungen   +S     &   &Meroitisch-demotisch    +T     *   *Meroitische Hieroglyphen    +U          Pollard-Schrift   +V        Sharada   +W        Sora-Sompeng    +X     *   *Sundanesisch, Ergänzung    +Y        Takri   +u        Bassa vah   +v     &   &Kaukasisch-albanisch    +w     (   (Koptische Jahresepakte    +x     @   @Kombinierende diakritische Zeichen, Ergänzung    +y     "   "Duployé-Schrift    +z        Elbasan   +{     0   0Erweiterte geometrische Formen    +|        Grantha   +}        Khojki    +~        Khudabadi   +     $   $Latein Erweitert-E    +�        Linear-A    +�        Mahajani    +�        Manichäisch    +�        Mende   +�        Modi    +�        Mro   +�     (   (Birmanisch, erweitert-B   +�        Nabatäisch   +�          Altnordarabisch   +�        Altpermisch   +�        Ziersymbole   +�        Pahawh Hmong    +�        Palmyrenisch    +�        Pau Cin Hau   +�          Psalter-Pahlavi   +�     *   *Kurzschrift-Steuerzeichen   +�        Siddham   +�     ,   ,Singhalesische Zahlzeichen    +�     &   &Ergänzende Pfeile-C    +�        Tirhuta   +�        Varang Kshiti   +�        Ahom    +�     *   *Anatolische Hieroglyphen    +�     &   &Cherokee, Ergänzung    +�     @   @Vereinheitlichte CJK-Ideogramme, Erweiterung E    +�     .   .Frühdynastische Keilschrift    +�        Hatran    +�        Multanisch    +�        Altungarisch    +�     @   @Verschiedene piktografische Symbole, Ergänzung   +�     &   &Sutton-Zeichenschrift   (1    �              Links            Innen            Rechts             Außen     !        Mitte            Von Links            Von Innen            Absatzbereich         "   "Absatztextbereich    	     "   "Seitenrand links          "   "Seitenrand innen     
     "   "Seitenrand rechts         "   "Seitenrand außen         "   "Absatzrand links          "   "Absatzrand innen          "   "Absatzrand rechts         "   "Absatzrand außen            Gesamte Seite         "   "Seitentextbereich            Oben             Unten             Mitte    "        Von Oben     #        Von Unten    $        Unterhalb    %        Von rechts     &          Seitenrand oben    '     "   "Seitenrand unten     (          Absatzrand oben    )     "   "Absatzrand unten             Rand          "   "Absatztextbereich         "   "Rahmenrand links          "   "Rahmenrand innen          "   "Rahmenrand rechts         "   "Rahmenrand außen              Gesamter Rahmen         "   "Rahmentextbereich            Grundlinie             Zeichen            Zeile    *        Textzeile   (n    	z          �   �Ein Thesaurus für die eingestellte Sprache ist nicht verfügbar.
Überprüfen Sie bitte Ihre Installation und installieren Sie gegebenenfalls die gewünschte Sprache.            $(ARG1) wird von der Prüfung nicht unterstützt bzw. ist gegenwärtig nicht aktiviert.
Überprüfen Sie Ihre Installation und installieren Sie gegebenenfalls das benötigte Sprachmodul
bzw. aktivieren Sie es unter  'Extras - Optionen - Spracheinstellungen - Linguistik'.        @   @Die Rechtschreibprüfung ist nicht verfügbar.         6   6Die Trennhilfe ist nicht verfügbar.         P   PDas persönliche Wörterbuch $(ARG1) kann nicht gelesen werden.        R   RDas persönliche Wörterbuch $(ARG1) kann nicht angelegt werden.         B   BDie Grafik $(ARG1) konnte nicht gefunden werden.         H   HEine unverknüpfte Grafik konnte nicht geladen werden.    	     H   HAm ausgewählten Begriff ist keine Sprache eingestellt.   
     �   �Die Formular-Schicht wurden nicht geladen, da notwendige IO-Services (com.sun.star.io.*) nicht instanziert werden konnten.    
     �   �Die Formular-Schicht wurden nicht geschrieben, da notwendige IO-Services (com.sun.star.io.*) nicht instanziert werden konnten.         v   vBeim Lesen der Formular-Steuerelemente trat ein Fehler auf. Die Formular-Schicht wurde nicht geladen.        ~   ~Beim Schreiben der Formular-Steuerelemente trat ein Fehler auf. Die Formular-Schicht wurde nicht gespeichert.        �   �Beim Lesen eines Aufzählungszeichens trat ein Fehler auf. Es konnten nicht alle Aufzählungszeichen geladen werden.         �   �Alle Veränderungen am Dokument Basic Code gehen verloren. Stattdessen wird der originale VBA Makrocode gespeichert.         \   \Der im Dokument enthaltene originale VBA Basic Code wird nicht gespeichert.         V   VDas Kennwort ist ungültig. Das Dokument kann nicht geöffnet werden.        �   �Die Verschlüsselungsmethode des aktuellen Dokumentes wird nicht unterstützt. Nur die Microsoft Office 97/2000 kompatible Kennwortverschlüsselung wird unterstützt.         r   rDas Laden von kennwortgeschützten Microsoft PowerPoint Präsentationen wird nicht unterstützt.         �   �Der Kennwortschutz wird beim Speichern von Dokumementen im Microsoft Office Format nicht unterstützt.
Möchten Sie das Dokument ohne Kennwortschutz speichern?   (o    v           6   6$(ERR) beim Ausführen des Thesaurus.         F   F$(ERR) bei der Ausführung der Rechtschreibprüfung.          <   <$(ERR) bei der Ausführung der Trennhilfe.          :   :$(ERR) beim Anlegen eines Wörterbuches.          D   D$(ERR) beim Festlegen eines Hintergrundattributes.          0   0$(ERR) beim Laden einer Grafik.   *d    �        y  X  X   	Umrandungseinstellung     Linke Umrandungslinie    Rechte Umrandungslinie     Obere Umrandungslinie    Untere Umrandungslinie     Horizontale Umrandungslinie    Vertikale Umrandungslinie    Diagonale Umrandungslinie von oben links nach unten rechts     Diagonale Umrandungslinie von unten links nach oben rechts          y  X  X   	Umrandungseinstellung     Linke Umrandungslinie    Rechte Umrandungslinie     Obere Umrandungslinie    Untere Umrandungslinie     Horizontale Umrandungslinie    Vertikale Umrandungslinie    Diagonale Umrandungslinie von oben links nach unten rechts     Diagonale Umrandungslinie von unten links nach oben rechts             ,   ,       svx/res/frmsel.bmp    FP     T              Tabelle            Abfrage            SQL   FQ    T              WIE            NICHT            LEER             WAHR             FALSCH             IST            ZWISCHEN             ODER     	        UND    
        Mittelwert             Anzahl             Maximum            Minimum            Summe            Alle             Irgendein            Einige             STDDEV_POP             STDDEV_SAMP            VAR_SAMP             VAR_POP            Sammeln            Vereinigung            Durchschnitt          L   L$(WIDTH) x $(HEIGHT) ($(WIDTH_IN_PX) x $(HEIGHT_IN_PX) px)         4   4$(WIDTH) x $(HEIGHT) bei $(DPI) DPI             $(CAPACITY) kB            GIF-Bild            JPEG-Bild           PNG-Bild            TIFF-Bild           WMF-Bild            MET-Bild    	        PCT-Bild    
        SVG-Bild            BMP-Bild            Unbekannt   
         Zeichenobjekt   
          Zeichenobjekte    
        Gruppenobjekt   
          Gruppenobjekte    
     &   &leeres Gruppenobjekt    
     &   &leere Gruppenobjekte    
        Tabelle   
        Tabellen    
        Linie   
	     "   "horizontale Linie   

          vertikale Linie   
          diagonale Linie   
        Linien    
        Rechteck    
        Rechtecke   
        Quadrat   
        Quadrate    
          Parallelogramm    
          Parallelogramme   
        Raute   
        Rauten    
     &   &abgerundetes Rechteck   
     &   &abgerundete Rechtecke   
     &   &abgerundetes Quadrat    
     &   &abgerundete Quadrate    
     ,   ,abgerundetes Parallelogramm   
     ,   ,abgerundete Parallelogramme   
     "   "abgerundete Raute   
     $   $abgerundete Rauten    
        Kreis   
        Kreise    
          Kreisausschnitt   
      "   "Kreisausschnitte    
!        Kreisbogen    
"        Kreisbögen   
#          Kreisabschnitt    
$          Kreisabschnitte   
%        Ellipse   
&        Ellipsen    
'     $   $Ellipsenausschnitt    
(     $   $Ellipsenausschnitte   
)        Ellipsenbogen   
*          Ellipsenbögen    
+     "   "Ellipsenabschnitt   
,     $   $Ellipsenabschnitte    
-        Polygon   
.     (   (Polygon mit %2 Punkten    
/        Polygone    
0        Polylinie   
1     *   *Polylinie mit %2 Punkten    
2        Polylinien    
3        Bézierkurve    
4        Bézierkurven   
5        Bézierkurve    
6        Bézierkurven   
7        Freihandlinie   
8          Freihandlinien    
9        Freihandlinie   
:          Freihandlinien    
;        Kurvenobjekt    
<        Kurvenobjekte   
=        Kurvenobjekt    
>        Kurvenobjekte   
?     $   $Natürlicher Spline   
@     $   $Natürliche Splines   
A     $   $Periodischer Spline   
B     $   $Periodische Splines   
C        Textrahmen    
D        Textrahmen    
E     (   (verknüpfter Textrahmen   
F     (   (verknüpfte Textrahmen    
G     (   (angepasstes Textobjekt    
H     (   (angepasste Textobjekte    
I     (   (angepasstes Textobjekt    
J     (   (angepasste Textobjekte    
K        Titeltext   
L        Titeltexte    
M          Gliederungstext   
N     "   "Gliederungstexte    
O        Grafik    
P        Grafiken    
Q     $   $verknüpfte Grafik    
R     &   &verknüpfte Grafiken    
S     $   $leeres Grafikobjekt   
T     $   $leere Grafikobjekte   
U     2   2leeres verknüpftes Grafikobjekt    
V     0   0leere verknüpfte Grafikobjekte   
W        MetaFile    
X        MetaFiles   
Y     &   &verknüpftes MetaFile   
Z     &   &verknüpfte MetaFiles   
[        Bitmap    
\        Bitmaps   
]     $   $verknüpfte Bitmap    
^     $   $verknüpfte Bitmaps   
_        Mac-Grafik    
`        Mac-Grafiken    
a     *   *verknüpfte Mac-Grafiken    
b     (   (verknüpfte Mac-Grafik    
c     ,   ,eingebettetes Objekt (OLE)    
d     ,   ,eingebettete Objekte (OLE)    
e     8   8verknüpftes eingebettetes Objekt (OLE)   
f     8   8verknüpfte eingebettete Objekte (OLE)    
g        Objekt    
h        Frame   
i        Frames    
j        Frame   
k          Objektverbinder   
l          Objektverbinder   
m        Legende   
n        Legenden    
o     *   *Seitendarstellungsobjekt    
p     *   *Seitendarstellungsobjekte   
q     "   "Bemaßungsobjekt    
r     "   "Bemaßungsobjekte   
s          Zeichenobjekte    
t     $   $kein Zeichenobjekt    
u        und   
v     "   "Zeichenobjekt(e)    
w        Steuerelement   
x          Steuerelemente    
y        3D Würfel    
z        3D Würfel    
{     "   "Extrusionsobjekt    
|     "   "Extrusionsobjekte   
}        3D Text   
~        3D Texte    
          Rotationsobjekt   
�     "   "Rotationsobjekte    
�        3D Objekt   
�        3D Objekte    
�        3D Polygone   
�        3D Szene    
�        3D Szenen   
�        Kugel   
�        Kugeln    
�     (   (Bitmap mit Transparenz    
�     4   4verknüpfte Bitmap mit Transparenz    
�     (   (Bitmaps mit Transparenz   
�     4   4verknüpfte Bitmaps mit Transparenz   
�        Form    
�        Formen    
�        Medienobjekt    
�        Medienobjekte   
�        Fontwork    
�        Fontwork    
�        SVG   
�        SVGs    
�        mit Kopie   
�     4   4Position und Größe für %1 setzen   
�        %1 löschen   
�     ,   ,%1 weiter nach vorn stellen   
�     .   .%1 weiter nach hinten stellen   
�     &   &%1 nach vorn stellen    
�     (   (%1 nach hinten stellen    
�     ,   ,Reihenfolge von %1 umkehren   
�          %1 verschieben    
�     *   *Größe von %1 verändern   
�        %1 drehen   
�     (   (%1 horizontal spiegeln    
�     &   &%1 vertikal spiegeln    
�     &   &%1 diagonal spiegeln    
�     "   "%1 frei spiegeln    
�     0   0%1 verzerren (schräg stellen)    
�     &   &%1 im Kreis anordnen    
�     *   *%1 kreisförmig verbiegen   
�     "   "%1 frei verzerren   
�        %1 auftrennen   
�     4   4Béziereigenschaften von %1 ändern   
�     4   4Béziereigenschaften von %1 ändern   
�        %1 schließen   
�     2   2Austrittsrichtung für %1 setzen    
�     0   0Relativattribut für %1 setzen    
�     ,   ,Bezugspunkt für %1 setzen    
�        %1 gruppieren   
�        %1 auflösen    
�     *   *Attribute auf %1 anwenden   
�     (   (Vorlage auf %1 anwenden   
�     *   *Vorlage von %1 entfernen    
�     (   (%1 in Polygon umwandeln   
�     *   *%1 in Polygone umwandeln    
�     &   &%1 in Kurve umwandeln   
�     (   (%1 in Kurven umwandeln    
�        %1 ausrichten   
�     *   *%1 kopfbündig ausrichten   
�     *   *%1 fußbündig ausrichten   
�     *   *%1 horizontal zentrieren    
�     ,   ,%1 linksbündig ausrichten    
�     ,   ,%1 rechtsbündig ausrichten   
�     (   (%1 vertikal zentrieren    
�        %1 zentrieren   
�     "   "%1 transformieren   
�        %1 verbinden    
�        %1 verbinden    
�        %1 aufbrechen   
�        %1 aufbrechen   
�        %1 aufbrechen   
�     4   4StarDraw Dos Zeichnung importieren    
�     "   "HPGL importieren    
�          DXF importieren   
�     (   (%1 in Kontur umwandeln    
�     *   *%1 in Konturen umwandeln    
�          %1 verschmelzen   
�          %1 subtrahieren   
�        %1 schneiden    
�     .   .Objekte der Auswahl verteilen   
�     &   &Breite %1 angleichen    
�     $   $Höhe %1 angleichen   
�     $   $Objekt(e) einfügen   
�          %1 ausschneiden   
�     *   *Zwischenablage einfügen    
�     "   "%1 Ziehen&Ablegen   
�     *   *Ziehen&Ablegen einfügen    
�     &   &Punkt in %1 einfügen   
�     ,   ,Klebepunkt in %1 einfügen    
�     *   *Referenzpunkt verschieben   
�     *   *%1 geometrisch verändern   
�          %1 verschieben    
�     *   *Größe von %1 verändern   
�        %1 drehen   
�     (   (%1 horizontal spiegeln    
�     &   &%1 vertikal spiegeln    
�     &   &%1 diagonal spiegeln    
�     "   "%1 frei spiegeln    
�     0   0%1 verzerren (schräg stellen)    
�     &   &%1 im Kreis anordnen    
�     *   *%1 kreisförmig verbiegen   
�     "   "%1 frei verzerren   
�     .   .Eckenradius von %1 verändern   
�        %1 verändern   
�     *   *Größe von %1 verändern   
�          %1 verschieben    
�     ,   ,Endpunkt von %1 verschieben   
�     &   &Winkel von %1 ändern   
�        %1 verändern   
�     2   2interaktiver Farbverlauf für %1    
�     0   0Interaktive Transparenz für %1   
�          %1 zuschneiden    
�     :   :TextEdit: Absatz %1, Zeile %2, Spalte %3    
�          %1 ausgewählt    
�        Punkt von %1    
�     "   "%2 Punkte von %1    
�     "   "Klebepunkt von %1   
�     &   &%2 Klebepunkte von %1   
�     "   "Markiere Objekte    
�     *   *Markiere weitere Objekte    
�          Markiere Punkte   
�     (   (Markiere weitere Punkte   
�     &   &Markiere Klebepunkte    
�     .   .Markiere weitere Klebepunkte    
�        Erzeuge %1    
�        %1 einfügen    
�        %1 kopieren   
�     2   2Objektreihenfolge für %1 ändern   
�     &   &Text von %1 editieren              Seite einfügen             Seite löschen              Seite kopieren         2   2Reihenfolge der Seiten verändern        *   *Hintergrundseite zuordnen        6   6Hintergrundseitenzuordnung entfernen         8   8Hintergrundseitenzuordnung verschieben         4   4Hintergrundseitenzuordnung ändern         $   $Dokument einfügen    	          Ebene einfügen   
          Ebene löschen         2   2Reihenfolge der Ebenen verändern        ,   ,Objektname für %1 ändern         ,   ,Objekttitel für %1 ändern        4   4Objektbeschreibung für %1 ändern            Standard            ein           aus           ja            nein            Typ 1           Typ 2           Typ 3           Typ 4           Horizontal            Vertikal            Automatisch           Aus           Proportional         $   $Alle Zeilen separat        0   0Durch hartes ueberattributieren           Oben             Mitte   !        Unten   "     .   .Ausnutzung der gesamten Höhe   #        Gedehnt   $        Links   %        Mitte   &        Rechts    '     0   0Ausnutzung der gesamten Breite    (        Gedehnt   )        aus   *        blinken   +        durchlaufen   ,        alternierend    -     "   "nur reinschieben    .        nach links    /        nach rechts   0        nach oben   1        nach unten    2     $   $Standard-Verbinder    3     "   "Linien-Verbinder    4     "   "Direkt-Verbinder    5     "   "Kurven-Verbinder    6        Standard    7        Radius    8        automatisch   9        links außen    :        innen (Mitte)   ;        rechts außen   <        automatisch   =        auf der Linie   >     $   $unterbrochene Linie   ?          unter der Linie   @     $   $zur Linie zentriert   A        Vollkreis   B        Kreissektor   C          Kreisabschnitt    D        Kreisbogen    E        Schatten    F        Schattenfarbe   G     .   .Horizontaler Schattenversatz    H     ,   ,Vertikaler Schattenversatz    I     $   $Schattentransparenz   J        3D-Schatten   K     *   *Perspektivischer Schatten   R        Legendentyp   S        Winkelvorgabe   T        Winkel    U        Freiraum    V     "   "Austrittsrichtung   W     ,   ,Relative Austrittsposition    X     "   "Austrittsposition   Y     "   "Austrittsposition   Z        Linienlänge    [     *   *Automatische Linienlänge   c        Eckenradius   d     &   &Linker Rahmenabstand    e     &   &Rechter Rahmenabstand   f     &   &Oberer Rahmenabstand    g     &   &Unterer Rahmenabstand   h     8   8Automatische Anpassung der Rahmenhöhe    i     &   &Minimale Rahmenhöhe    j     &   &Maximale Rahmenhöhe    k     8   8Automatische Anpassung der Rahmenbreite   l     &   &Minimale Rahmenbreite   m     &   &Maximale Rahmenbreite   n     *   *Vertikale Textverankerung   o     ,   ,Horizontale Textverankerung   p          Text-an-Rahmen    q        Rotanteil   r        Grünanteil   s        Blauanteil    t        Helligkeit    u        Kontrast    v        Gamma   w        Transparenz   x        Invertieren   y        Grafikmodus   z            {            |            }            ~                        �     "   "Diverse Attribute   �          Positionsschutz   �          Größenschutz    �        Nicht drucken   �        Ebenenkennung   �        ~Ebene    �        Objektname    �        Startwinkel   �        Endwinkel   �        X-Position    �        Y-Position    �        Breite    �        Höhe   �        Drehwinkel    �        Scherwinkel   �     &   &Unbekanntes Attribut    �        Linienstil    �        Linienmuster    �        Strichstärke   �        Linienfarbe   �        Linienanfang    �        Linienende    �     $   $Linienanfangsbreite   �     "   "Linienendebreite    �     (   (Linienanfang zentriert    �     &   &Linienende zentriert    �     "   "Linientransparenz   �          Linienübergang   �          Linie Reserve 2   �          Linie Reserve 3   �          Linie Reserve 4   �          Linie Reserve 5   �          Linie Reserve 6   �          Linienattribute   �        Füllstil   �        Füllfarbe    �        Farbverlauf   �        Schraffur   �        Füllbitmap   �        Transparenz   �     *   *Farbverlaufschrittanzahl    �          Kachelfüllung    �     $   $Füllbitmapposition   �     "   "Füllbitmapbreite   �     "   "Füllbitmaphöhe    �     &   &Transparenter Verlauf   �     $   $Füllung Reserve 2    �     *   *Kachelgröße nicht in %    �     &   &Kachelversatz X in %    �     &   &Kachelversatz Y in %    �          Bitmapstreckung   �        Bmp Reserve 3   �        Bmp Reserve 4   �        Bmp Reserve 5   �        Bmp Reserve 6   �        Bmp Reserve 7   �        Bmp Reserve 8   �     &   &Kachelposition X in %   �     &   &Kachelposition Y in %   �     $   $Hintergrundfüllung   �     $   $Füllung Reserve 10   �     $   $Füllung Reserve 11   �     $   $Füllung Reserve 12   �     "   "Flächenattribute   �        Fontworkstil    �     $   $Fontworkausrichtung   �          Fontworkabstand   �     (   (Fontwork Schriftbeginn    �     (   (Fontwork Spiegelschrift   �     (   (Fontwork Konturschrift    �     "   "Fontworkschatten    �     &   &Fontworkschattenfarbe   �     *   *Fontworkschattenversatz X   �     *   *Fontworkschattenversatz Y   �     *   *Fontworkkontur verstecken   �     ,   ,Fontworkschattentransparenz   �     $   $Fontwork Reserve 2    �     $   $Fontwork Reserve 3    �     $   $Fontwork Reserve 4    �     $   $Fontwork Reserve 5    �     $   $Fontwork Reserve 6    �        Schatten    �        Schattenfarbe   �     "   "Schattenabstand X   �     "   "Schattenabstand Y   �     $   $Schattentransparenz   �        3D-Schatten   �     *   *Perspektivischer Schatten   �        Legendentyp   �     (   (Legendenwinkelfixierung   �          Legendenwinkel    �     &   &Legendenlinienabstand   �     *   *Legendenaustrittsrichtung   �     ,   ,Legende relativer Austritt    �     ,   ,Legende relativer Austritt    �     ,   ,Legende absoluter Austritt    �     &   &Legendenlinienlänge    �     0   0Legendenlinienlängenautomatik    �        Eckenradius   �     &   &Minimale Rahmenhöhe    �     .   .Automatische Höhenanpassung    �          Text-an-Rahmen    �     *   *Linker Textrahmenabstand    �     *   *Rechter Textrahmenabstand   �     *   *Oberer Textrahmenabstand    �     *   *Unterer Textrahmenabstand   �     *   *Vertikale Textverankerung   �     &   &Maximale Rahmenhöhe    �     &   &Minimale Rahmenbreite   �     &   &Maximale Rahmenbreite   �     .   .Automatische Breitenanpassung   �     ,   ,Horizontale Textverankerung            Laufschrift        $   $Laufschriftrichtung        ,   ,Laufschriftstart innerhalb         *   *Laufschriftstop innerhalb        ,   ,Laufschriftdurchlaufanzahl         $   $Laufschriftzeittakt        (   (Laufschriftschrittweite        "   "Konturentextfluss        "   "Formen Justierung   	     .   .Benutzerdefinierte Attribute    
     @   @Schriftartunabhängigen Zeilenabstand verwenden        (   (Text in Form umbrechen              Auto Grow Size         "   "SvDraw Reserve 18        "   "SvDraw Reserve 19           Verbindertyp         (   (Horz. Abstand Objekt 1         (   (Vert. Abstand Objekt 1         (   (Horz. Abstand Objekt 2         (   (Vert. Abstand Objekt 2         &   &Klebeabstand Objekt 1        &   &Klebeabstand Objekt 2        .   .Anzahl verschiebbarer Linien              Versatz Linie 1             Versatz Linie 2             Versatz Linie 3   $        Bemaßungstyp   %     ,   ,Maßtext Horizontalposition   &     *   *Maßtext Vertikalposition   '     "   "Maßlinienabstand   (     *   *Maßhilfslinienüberhang    )     (   (Maßhilfslinienabstand    *     .   .Maßhilfslinienüberlänge 1    +     .   .Maßhilfslinienüberlänge 2    ,     &   &Unterkantenbemaßung    -     ,   ,Maßtext quer zu Maßlinie    .     ,   ,Maßtext um 180 Grad drehen   /     &   &Maßlinienüberstand    0        Maßeinheit   1     &   &Maßstabszusatzfaktor   2     &   &Maßeinheitenanzeige    3          Maßtextformat    4     (   (Maßtextwinkelautomatik   5     4   4Winkel für Maßtextwinkelautomatik   6     *   *Maßtextwinkelfestsetzung   7          Maßzahlwinkel    8     "   "Nachkommastellen    9     $   $Bemaßung Reserve 5   :     $   $Bemaßung Reserve 6   ;     $   $Bemaßung Reserve 7   =        Kreistyp    >        Startwinkel   ?        Endwinkel   @          Kreis Reserve 0   A          Kreis Reserve 1   B          Kreis Reserve 2   C          Kreis Reserve 3   D          Objekt sichtbar   E     *   *Objektposition geschützt   F     *   *Objektgröße geschützt    G          Objekt druckbar   H        Ebenenkennung   I        Ebene   J        Objektname    K     "   "X-Position gesamt   L     "   "Y-Position gesamt   M        Breite gesamt   N        Höhe gesamt    O     $   $X-Position einzeln    P     $   $Y-Position einzeln    Q          Breite einzeln    R        Höhe einzeln   S          Breite logisch    T        Höhe logisch   U     $   $Drehwinkel einzeln    V     $   $Scherwinkel einzeln   W     "   "Horz. verschieben   X     "   "Vert. verschieben   Y     "   "Resize X einzeln    Z     "   "Resize Y einzeln    [          Drehen einzeln    \     &   &Scheren horz. einzeln   ]     &   &Scheren vert. einzeln   ^          Resize X gesamt   _          Resize Y gesamt   `        Drehen gesamt   a     &   &Scheren horz. gesamt    b     &   &Scheren vert. gesamt    c     "   "Referenzpunkt 1 X   d     "   "Referenzpunkt 1 Y   e     "   "Referenzpunkt 2 X   f     "   "Referenzpunkt 2 Y   g          Silbentrennung    h     .   .Aufzählungszeichen anzeigen    i     *   *Einzüge bei Aufzählung    j     "   "Aufzählungsebene   k     6   6Aufzählungszeichen und Nummerierung    l        Einzüge    m          Absatzabstände   n        Zeilenabstand   o     "   "Absatzausrichtung   p        Tabulatoren   q        Schriftfarbe    r        Zeichensatz   s        Schriftgrad   t        Schriftbreite   u        Schriftdicke    v          Unterstreichung   w          Überstreichen    x          Durchstreichung   y        Kursiv    z          Konturenschrift   {     &   &Schatten für Schrift   |     $   $Hoch-/tiefstellung    }        Kerning   ~     *   *Manuelle Unterschneidung         0   0Leerräume nicht unterstreichen   �        Tabulator   �     &   &Weicher Zeilenumbruch   �     .   .Nichtkonvertierbares Zeichen    �        Feldbefehl    �        Rotanteil   �        Grünanteil   �        Blauanteil    �        Helligkeit    �        Kontrast    �        Gamma   �        Transparenz   �        Invertieren   �        Grafikmodus   �        Zuschneiden   �            �            �            �            �     ,   ,Tabellenattribute anwenden    �     $   $AutoFormat Tabelle    �     "   "Spalte einfügen    �          Zeile einfügen   �          Spalte löschen   �          Zeile löschen    �        Zellen teilen   �     "   "Zellen verbinden    �     "   "Zelle formatieren   �     0   0Zeilen gleichmäßig verteilen    �     0   0Spalten gleichmäßig verteilen   �          Tabellenformat    �     2   2Einstellungen für Tabellenformat   �     (   (Zelleninhalte löschen    �     $   $Nachfolger im Text    '        [Alle]    'W     &   &Tabellenende erreicht   'Y        Favorit   'Z        X   '[        Y   '\        Z   ']        R:    '^        G:    '_     &   &Dokumentende erreicht   '`     *   *einschließlich Vorlagen    'a        (Suchen)    'b        (Ersetzen)    'c     ,   ,Suche nach Absatz~vorlagen    'd        B:    'e     *   *Suche nach ~Zellvorlagen    'h     ,   ,Suchbegriff nicht gefunden    'i     h   hSind Sie sicher, dass Sie die %PRODUCTNAME Dokumentwiederherstellung verwerfen wollen?    'j     (   (Dokumentanfang erreicht   '�        Durchgängig    '�        Farbverlauf   '�        Bitmap    '�        Linienstil    '�        Keine   '�        Farbe   '�        Schraffur   '�        - kein -    '�          Keine Füllung    '�        Muster    '�        Umrandung   '�          Umrandungsfarbe   '�          Umrandungsstil    '�     *   *Farbe für Hervorhebungen   '�     &   &Formatierung löschen   '�     $   $Weitere Optionen...   '�     f   fName der Schriftart. Die gewählte Schriftart ist nicht verfügbar und wird ersetzt.    '�        Schriftart    '�        Linienfarbe   '�     $   $Weitere Vorlagen...   '�        Füllfarbe    '�     *   *Weitere Nummerierungen...   '�     *   *Weitere Aufzählungen...    '�          Standardpalette   '�          Dokumentfarben    '�        Dokumentfarbe   '�     R   REinfügemodus. Klicken Sie, um zum Überschreibmodus zu wechseln.   '�     R   RÜberschreibmodus. Klicken Sie, um zum Einfügemodus zu wechseln.   '�          Überschreiben    '�     @   @Digitale Signatur: Die Dokumentsignatur ist OK.   '�     x   xDigitale Signatur: Die Dokumentsignatur ist OK, aber die Zertifikate konnten nicht verifiziert werden.    '�     �   �Digitale Signatur: Die Dokumentsignatur stimmt nicht mit dem Dokumentinhalt überein. Wir empfehlen Ihnen dringend, diesem Dokument nicht zu trauen.    '�     D   DDigitale Signatur: Das Dokument ist nicht signiert.   '�     �   �Digitale Signatur: Die Dokumentsignatur und das Zertifikat sind korrekt, aber nicht alle Bestandteile des Dokuments sind signiert.    '�        Linienspitzen   (        Links   (        Rechts    (        Mitte   (        Dezimal   (
        Benutzer    (        Gallery-Thema   (          Themen-Elemente   (        Vorschau    (        Schließen    (#        Schwarz   ($        Blau    (%        Grün   (&        Cyan    ('        Rot   ((        Magenta   (*        Grau    (1        Gelb    (2        Weiß   (3        Blaugrau    (4        Orange    (5        Türkis   (6        Turquoise   (7     "   "Klassisches blau    (8        Blue classic    (<        Pfeil   (=        Quadrat   (>        Kreis   (A        Transparenz   (B        Zentriert   (C          Nicht Zentriert   (D        Liste   (E        Filter    (�        Transparent   (�        Quellfarbe    (�        Farbpalette   (�        Toleranz    (�          Ersetzen durch    (�     $   $Objekt(e) einfügen   (�     ,   ,Erstelle 3D-Rotationsobjekt   (�          Anzahl Segmente   (�        Objekttiefe   (�     "   "Kamerabrennweite    (�          Kameraposition    (�     $   $3D-Objekt rotieren    )      ,   ,Extrusionsobjekt erstellen    )     ,   ,Rotationskörper erstellen    )     &   &3D-Objekt aufbrechen    )        3D-Attribute    )        Standard    )        Graustufen    )        Schwarz/Weiß   )        Wasserzeichen   )     *   *Intel Indeo Video (*.ivf)   )     ,   ,Video für Windows (*.avi)    )     (   (QuickTime Movie (*.mov)   )     J   JMPEG - Motion Pictures Experts Group (*.mpe;*.mpeg;*.mpg)   )         <Alle>    )!          Audio einfügen   )"          Video einfügen   )#        Hintergrund   ),        Lila    )-        Bordeaux    ).        Blassgelb   )/        Blassgrün    )0        Dunkellila    )1        Lachs   )2        Seeblau   )4     2   2Grün 1 (%PRODUCTNAME Grundfarbe)   )5          Grüner Akzent    )6        Blauer Akzent   )7          Oranger Akzent    )8        Purpur    )9        Purpur Akzent   ):        Gelb Akzent   )@        3D    )A        Schwarz 1   )B        Schwarz 2   )C        Blau    )D        Braun   )E        Währung    )F        Währung 3D   )G        Währung Grau   )H     "   "Währung Flieder    )I     "   "Währung Türkis    )J        Grau    )K        Grün   )L        Flieder   )M        Rot   )N        Türkis   )O        Gelb    )Z     $   $Flaches Linienende    )[     "   "Rundes Linienende   )\     (   (Rechteckiges Linienende   )]     *   *Linienübergang gemittelt   )^     *   *Linienübergang verbunden   )_     ,   ,Linienübergang geschnitten   )`     &   &Linienübergang rund    )c        Black   )d        Blue    )e        Green   )f        Cyan    )g        Red   )h        Magenta   )j        Gray    )q        Yellow    )r        White   )s        Blue gray   )t        Orange    )u        Violet    )v        Bordeaux    )w        Pale yellow   )x        Pale green    )y        Dark violet   )z        Salmon    ){        Sea blue    )|        Sun   )}        Diagramm    )~        Chart   )        Purpur    )�        Purple    )�        Himmelblau    )�        Sky blue    )�        Gelb-grün    )�        Yellow green    )�        Rosa    )�        Pink    )�     2   2Green 1 (%PRODUCTNAME Main Color)   )�        Green Accent    )�        Blue Accent   )�        Orange Accent   )�        Purple    )�        Purple Accent   )�        Yellow Accent   )�        Tango: Butter   )�        Tango: Orange   )�     "   "Tango: Schokolade   )�     "   "Tango: Chameleon    )�     "   "Tango: Himmelblau   )�          Tango: Pflaume    )�     $   $Tango: Scharlachrot   )�     "   "Tango: Aluminium    )�        Tango: Butter   )�        Tango: Orange   )�     "   "Tango: Chocolate    )�     "   "Tango: Chameleon    )�          Tango: Sky Blue   )�        Tango: Plum   )�     $   $Tango: Scarlet Red    )�     "   "Tango: Aluminium    )�     &   &Black 45 Degrees Wide   )�     "   "Black 45 Degrees    )�     "   "Black -45 Degrees   )�     "   "Black 90 Degrees    )�     (   (Red Crossed 45 Degrees    )�     &   &Red Crossed 0 Degrees   )�     (   (Blue Crossed 45 Degrees   )�     (   (Blue Crossed 0 Degrees    )�     (   (Blue Triple 90 Degrees    )�          Black 0 Degrees   )�        Hatching    )�     &   &Schwarz 45 Grad weit    )�          Schwarz 45 Grad   )�     "   "Schwarz -45 Grad    )�          Schwarz 90 Grad   )�     "   "Rot Netz 45 Grad    )�          Rot Netz 0 Grad   )�     "   "Blau Netz 45 Grad   )�     "   "Blau Netz 0 Grad    )�     &   &Blau 3er Netz 90 Grad   )�          Schwarz 0 Grad    )�        Schraffur   )�        Empty   )�        Sky   )�        Aqua    )�        Coarse    )�        Space Metal   )�        Space   )�        Metal   )�        Wet   )�        Marble    )�        Linen   )�        Stone   )�        Pebbles   )�        Wall    )�        Red Wall    )�        Pattern   )�        Leaves    )�          Lawn Artificial   )�        Daisy   )�        Orange    )�        Fiery   )�        Roses   )�        Bitmap    )�        Leer    )�        Himmel    )�        Wasser    )�        Grobkörnig   )�        Quecksilber   )�        Weltraum    )�        Metall    )�        Tropfen   )�        Marmor    )�        Leinen    )�        Stein   )�        Schotter    )�        Mauer   )�        Ziegelsteine    )�        Geflecht    )�        Laub    )�        Kunstrasen    )�          Gänseblümchen   )�        Orange    )�        Feurig    )�        Rosen   )�        Bitmap    )�     "   "Ultrafine Dashed    )�        Fine Dashed   )�     *   *Ultrafine 2 Dots 3 Dashes   )�        Fine Dotted   )�     $   $Line with Fine Dots   )�     "   "Fine Dashed (var)   )�     &   &3 Dashes 3 Dots (var)   )�     (   (Ultrafine Dotted (var)    )�        Line Style 9    )�        2 Dots 1 Dash   )�        Dashed (var)    )�        Dash    *         Line Style    *     &   &Ultrafein gestrichelt   *     "   "Fein gestrichelt    *     $   $2 Punkte 3 Striche    *          Fein gepunktet    *     *   *Strich mit feinen Punkten   *     "   "Fein gestrichelt    *     $   $3 Striche 3 Punkte    *     $   $Ultrafein gepunktet   *	        Linienstil 9    *
     "   "2 Punkte 1 Strich   *        Gestrichelt   *        Gestrichelt   *        Linienstil    *2          Auswahl drucken   *3     J   JWollen Sie die Auswahl oder das gesamte Dokument drucken?   *4        ~Alles    *5        Au~swahl    *D        Zuschneiden   *E        Bildmodus   *F        Rotanteil   *G        Grünanteil   *H        Blauanteil    *I        Helligkeit    *J        Kontrast    *K        Gamma   *L        Transparenz   *Y        Automatisch   *^     6   6Aktionen rückgängig machen: $(ARG1)   *_     4   4Aktionen wiederherstellen: $(ARG1)    *`     6   6Aktionen rückgängig machen: $(ARG1)   *a     4   4Aktionen wiederherstellen: $(ARG1)    *b        Transparency    *c        Transparenz   *m     "   "3D-Materialfarbe    *n        Schriftfarbe    *o     "   "Hintergrundfarbe    *p        Keiner    *q        Ausgefüllt   *r        Schraffiert   *s        Verlauf   *t        Bitmap    *u        mit   *v        Stil    *w        und   *�     $   $Ecken-Steuerelement   *�     *   *Auswahl eines Eckpunktes.   *�     &   &Winkel-Steuerelement    *�     0   0Auswahl eines (Haupt-) Winkels.   *�        Links oben    *�        Mitte oben    *�        Rechts oben   *�        Links mittig    *�        Mitte   *�        Rechts mittig   *�        Links unten   *�        Mitte unten   *�        Rechts unten    *�        0 Grad    *�        45 Grad   *�        90 Grad   *�        135 Grad    *�        180 Grad    *�        225 Grad    *�        270 Grad    *�        315 Grad    *�     *   *Steuerelement der Kontur    *�     8   8Hier können Sie die Kontur bearbeiten.   *�     &   &Sonderzeichenauswahl    *�        Zeichen-Code    *�     B   BWählen Sie Sonderzeichen in diesem Bereich aus.    *�        Bildexport    *�        Extrusion   *�        Fontwork    *�          Extrusion Color   *�        ~0 cm   *�        ~1 cm   *�        ~2.5 cm   *�        ~5 cm   *�        10 ~cm    *�        0 inch    *�        0.~5 inch   *�        ~1 inch   *�        ~2 inch   *�        ~4 inch   *�        Seiten    +n     (   (Fontwork-Form anwenden    +o     @   @Fontwork-Buchstaben mit gleicher Höhe anwenden   +p     .   .Fontwork-Ausrichtung anwenden   +r     2   2Fontwork-Zeichenabstand anwenden    +s     ,   ,Extrusion ein/aus anwenden    +t     "   "Nach unten kippen   +u     "   "Nach oben kippen    +v     "   "Nach links kippen   +w     $   $Nach rechts kippen    +x     *   *Extrusions Tiefe ändern    +y     "   "Richtung ändern    +z     *   *Projektionsmodus wechseln   +{     &   &Beläuchtung ändern    +|     $   $Helligkeit ändern    +}     $   $Oberfläche ändern   +~     *   *Extrusionsfarbe wechseln    +�     F   FKleine ausgefüllte kreisförmige Aufzählungszeichen   +�     F   FGroße ausgefüllte kreisförmige Aufzählungszeichen   +�     B   BAusgefüllte diamantförmige Aufzählungszeichen    +�     F   FGroße ausgefüllte quadratische Aufzählungszeichen    +�     R   RAusgefüllte nach rechts zeigende Pfeile als Aufzählungszeichen    +�     D   DNach rechts zeigende Pfeile als Aufzählungszeichen   +�     4   4Kreuzmarken als Aufzählungszeichen   +�     .   .Haken als Aufzählungszeichen   +�     $   $Nummer (1) (2) (3)    +�          Nummer 1. 2. 3.   +�     $   $Nummer (1) (2) (3)    +�     4   4Große römische Zahlen I. II. III.   +�     *   *Großbuchstaben A) B) C)    +�     *   *Kleinbuchstaben a) b) c)    +�     ,   ,Kleinbuchstaben (a) (b) (c)   +�     4   4Kleine römische Zahlen i. ii. iii.   +�     p   pNumerisch, numerisch, kleine Buchstaben, kleiner ausgefüllter kreisförmiger Aufzählungspunkt   +�     f   fNumerisch, kleine Buchstaben, kleiner ausgefüllter kreisförmiger Aufzählungspunkt    +�     �   �Numerisch, kleine Buchstaben, kleine römische Zahlen, große Buchstaben, kleiner ausgefüllter kreisförmiger Aufzählungspunkt    +�        Numerisch   +�     �   �Große römische Zahlen, große Buchstaben, kleine römische Zahlen, kleine Buchstaben, kleiner ausgefüllter kreisförmiger Aufzählungspunkt    +�     �   �Große Buchstaben, große römische Zahlen, kleine Buchstaben, kleine römische Zahlen, kleiner ausgefüllter kreisförmiger Aufzählungspunkt    +�     (   (Numerisch, vollständig   +�        Nach rechts zeigender Pfeil als Aufzählungspunkt, ausgefüllte schwarzer nach rechts zeigender Pfeil als Aufzählungspunkt, ausgefüllter schwarzer diamantförmiger Aufzählungspunkt, kleiner ausgefüllter kreisförmiger Aufzählungspunkt   +�     (   (Tabellendesignvorlagen    +�        Schriftfarbe    +�     8   8Nach formatierten Zeichenfolgen suchen    +�     (   (Groß-/Kleinschreibung    +�        Suchen    +�     V   VDas Dokument wurde verändert. Doppelklicken Sie, um es zu speichern.   +�     P   PDas Dokument wurde seit dem letzten Speichern nicht verändert.   +�     "   "Dokument laden...   +�     &   &Ungültiges Kennwort    +�     4   4Kennwörter stimmen nicht überein    +�     4   4Folie auf Fenstergröße anpassen.    ,     .   .Erfolgreich wiederhergestellt   ,     4   4Original Dokument wiederhergestellt   ,     2   2Wiederherstellung fehlgeschlagen    ,     6   6Wiederherstellung wird durchgeführt    ,     .   .Noch nicht wiederhergestellt    ,     �   �%PRODUCTNAME %PRODUCTVERSION beginnt jetzt mit der Wiederherstellung Ihrer Dokumente. Dieses kann abhängig von der Größe der Dokumente einige Zeit in Anspruch nehmen.   ,     �   �Die Wiederherstellung Ihrer Dokumente ist abgeschlossen.
Betätigen Sie die Schaltfläche 'Fertig', um zu den wiederhergestellten Dokumenten zu gelangen.   ,        ~Fertig   ,     ~   ~Maßstab. Rechtsklicken Sie, um den Maßstab zu ändern, oder klicken Sie, um den Dialog Maßstab zu öffnen.   ,        Vergrößern    ,        Verkleinern   ,     &   &~Benutzerdefiniert...   ,         ~Unendlich    ,!        ~Drahtrahmen    ,"        ~Matte    ,#        ~Plastik    ,$        Me~tall   ,%        ~Sehr eng   ,&        ~Eng    ,'        ~Normal   ,(        ~Weit   ,)        Se~hr weit    ,*     &   &~Benutzerdefiniert...   ,+     &   &~Zeichenpaarekerning    ,8     *   *Extrusion nach Nordwesten   ,9     &   &Extrusion nach Norden   ,:     *   *Extrusion nach Nordosten    ,;     &   &Extrusion nach Westen   ,<     &   &Extrusion nach hinten   ,=     &   &Extrusion nach Osten    ,>     *   *Extrusion nach Südwesten   ,?     &   &Extrusion nach Süden   ,@     *   *Extrusion nach Südosten    ,B        ~Perspektive    ,C        P~arallel   ,D        ~Hell   ,E        ~Normal   ,F        ~Abgeblendet    ,h        ~Linksbündig   ,i        ~Zentriert    ,j          ~Rechtsbündig    ,k     "   "~Wortausrichtung    ,l     &   &~Streckung ausrichten   ,v        25%   ,w        50%   ,x        75%   ,y        100%    ,z        150%    ,{        200%    ,|        Ganze Seite   ,}        Seitenbreite    ,~     "   "Optimale Ansicht    .�        Gradient    .�     "   "Linear blue/white   .�     &   &Linear magenta/green    .�     $   $Linear yellow/brown   .�     $   $Radial green/black    .�     "   "Radial red/yellow   .�     &   &Rectangular red/white   .�     $   $Square yellow/white   .�     0   0Ellipsoid blue grey/light blue    .�     &   &Axial light red/white   .�        Diagonal 1l   .�        Diagonal 1r   .�        Diagonal 2l   .�        Diagonal 2r   .�        Diagonal 3l   .�        Diagonal 3r   .�        Diagonal 4l   .�        Diagonal 4r   .�        Diagonal Blue   .�          Diagonal Green    .�          Diagonal Orange   .�        Diagonal Red    .�     $   $Diagonal Turquoise    .�          Diagonal Violet   .�        From a Corner   .�     $   $From a Corner, Blue   .�     &   &From a Corner, Green    .�     &   &From a Corner, Orange   .�     $   $From a Corner, Red    .�     *   *From a Corner, Turquoise    .�     &   &From a Corner, Violet   .�          From the Middle   /      &   &From the Middle, Blue   /     (   (From the Middle, Green    /     (   (From the Middle, Orange   /     &   &From the Middle, Red    /     ,   ,From the Middle, Turquoise    /     (   (From the Middle, Violet   /        Horizontal    /          Horizontal Blue   /     "   "Horizontal Green    /	     "   "Horizontal Orange   /
          Horizontal Red    /     &   &Horizontal Turquoise    /     "   "Horizontal Violet   /        Radial    /        Radial Blue   /        Radial Green    /        Radial Orange   /        Radial Red    /     "   "Radial Turquoise    /        Radial Violet   /        Vertical    /        Vertical Blue   /          Vertical Green    /          Vertical Orange   /        Vertical Red    /     $   $Vertical Turquoise    /          Vertical Violet   /        Gray Gradient   /          Yellow Gradient   /          Orange Gradient   /        Red Gradient    /        Pink Gradient   /         Sky   /!        Cyan Gradient   /"        Blue Gradient   /#        Purple Pipe   /$        Night   /%          Green Gradient    /&        Tango Green   /'     $   $Subtle Tango Green    /(        Tango Purple    /)        Tango Red   /*        Tango Blue    /+        Tango Yellow    /,        Tango Orange    /-        Tango Gray    /.        Argile    //        Olive Green   /0        Grison    /1        Sunburst    /2        Brownie   /3        Sun-Set   /4        Deep Green    /5        Deep Orange   /6        Deep Blue   /7        Purple Haze   /D        Farbverlauf   /E     "   "Linear Blau/weiß   /F     &   &Linear violett/grün    /G     "   "Linear gelb/braun   /H     &   &Radial grün/schwarz    /I          Radial rot/gelb   /J     $   $Rechteck rot/weiß    /K     (   (Quadratisch gelb/weiß    /L     ,   ,Ellipsoid blaugrau/hellblau   /M     $   $Axial hellrot/weiß   /N        Diagonal 1l   /O        Diagonal 1r   /P        Diagonal 2l   /Q        Diagonal 2r   /R        Diagonal 3l   /S        Diagonal 3r   /T        Diagonal 4l   /U        Diagonal 4r   /V        Diagonal blau   /W          Diagonal grün    /X          Diagonal orange   /Y        Diagonal rot    /Z     "   "Diagonale türkis   /[     "   "Diagonal violett    /\          Von einer Ecke    /]     &   &Von einer Ecke, blau    /^     &   &Von einer Ecke, grün   /_     (   (Von einer Ecke, orange    /`     $   $Von einer Ecke, rot   /a     (   (Von einer Ecke, türkis   /b     (   (Von einer Ecke, violett   /c        Aus der Mitte   /d     $   $Aus der Mitte, blau   /e     &   &Aus der Mitte, grün    /f     &   &Aus der Mitte, orange   /g     $   $Aus der Mitte, rot    /h     (   (Aus der Mitte, türkis    /i     (   (Aus der Mitte, violett    /j        Horizontal    /k          Horizontal blau   /l     "   "Horizontal grün    /m     "   "Horizontal orange   /n          Horizontal rot    /o     $   $Horizontal türkis    /p     $   $Horizontal violett    /q        Radial    /r        Radial blau   /s        Radial grün    /t        Radial orange   /u        Radial rot    /v          Radial türkis    /w          Radial violett    /x        Vertikal    /y        Vertikal blau   /z          Vertikal grün    /{          Vertikal orange   /|        Vertikal rot    /}     "   "Vertikal türkis    /~     "   "Vertikal violett    /     $   $Grauer Farbverlauf    /�     $   $Gelber Farbverlauf    /�     $   $Oranger Farbverlauf   /�     "   "Roter Farbverlauf   /�     $   $Pinker Farbverlauf    /�        Himmel    /�     (   (Blaugrüner Farbverlauf   /�     $   $Blauer Farbverlauf    /�        Lilanes Rohr    /�        Nacht   /�     $   $Grüner Farbverlauf   /�        Tango: Grün    /�     "   "Tango: Hellgrün    /�        Tango: Lila   /�        Tango: Rot    /�        Tango: Blau   /�        Tango: Gelb   /�        Tango: Orange   /�        Tango: Grau   /�        Sandfarben    /�        Oliv-grün    /�        Silber    /�        Sonnenaufgang   /�        Brownie   /�          Sonnenuntergang   /�        Dunkelgrün   /�        Dunkelorange    /�        Dunkelblau    /�        Mattlila    1�        Arrow concave   1�        Square 45   1�        Small Arrow   1�          Dimension Lines   1�        Double Arrow    1�     $   $Rounded short Arrow   1�          Symmetric Arrow   1�        Line Arrow    1�     $   $Rounded large Arrow   1�        Circle    1�        Square    1�        Arrow   1�     "   "Short line Arrow    1�     "   "Triangle unfilled   1�     "   "Diamond unfilled    1�        Diamond   1�          Circle unfilled   1�     $   $Square 45 unfilled    1�          Square unfilled   1�     &   &Half Circle unfilled    1�        Arrowhead   1�        Pfeil konkav    1�        Quadrat 45    1�          Schmaler Pfeil    1�          Bemaßungsenden   1�        Doppelpfeil   1�     *   *Abgerundeter kurzer Pfeil   1�     $   $Symmetrischer Pfeil   1�        Linien-Pfeil    1�     *   *Abgerundeter langer Pfeil   2         Kreis   2        Quadrat   2        Pfeil   2     $   $Kurzer Linienpfeil    2     &   &Ungefülltes Dreieck    2     "   "Ungefüllte Raute   2        Raute   2     $   $Ungefüllter Kreis    2     (   (Ungefülltes Quadrat 45   2	     &   &Ungefülltes Quadrat    2
     (   (Ungefüllter Halbkreis    2        Linienspitze    :h        Schriftfarbe    :p        Suchen    :q        Alle suchen   :r        Ersetzen    :s        Alle ersetzen   :t          Zeichenvorlage    :u        Absatzvorlage   :v        Rahmenvorlage   :w        Seitenvorlage   :z        Formel    :{        Wert    :|        Notiz   :        StarWriter    :�        StarCalc    :�        StarDraw    :�        StarBase    :�        Keine   :�        Voll    :�        Horizontal    :�        Vertikal    :�        Karo    :�        Karo diagonal   :�        Diagonal hoch   :�          Diagonal runter   :�        25%   :�        50%   :�        75%   :�        Bild    <      *   *Schreibrichtung Standard    <     $   $Von oben nach unten   <     $   $Von unten nach oben   <        Übereinander   <!        Tabelle   <"        Nicht Tabelle   <#     (   (Abstand freigeschaltet    <$     .   .Abstand nicht freigeschaltet    <%     .   .Abstand nicht unterschreiten    <&     0   0Abstand unterschreiten erlaubt    <F        Linker Rand:    <G        Oberer Rand:    <H          Rechter Rand:     <I          Unterer Rand:     <X     &   &Seitenbeschreibung:     <Y          Großbuchstaben   <Z          Kleinbuchstaben   <[          Römisch groß    <\          Römisch klein    <]        Arabisch    <^        Keine   <_        Quer    <`        Hoch    <a        Links   <b        Rechts    <c        Alle    <d        Gespiegelt    <o        Autor:    <p        Datum:    <q        Text:     <r     $   $Hintergrundfarbe:     <s        Musterfarbe:    <u     $   $Zeichenhintergrund    C        Farbpalette   C        Wechseln    FQ     j   jDer angegebene Name '%1' ist nicht XML konform. Bitte geben sie einen anderen Namen ein.    FR     n   nDer angegebene Präfix '%1' ist nicht XML konform. Bitte geben Sie einen anderen Präfix ein.   FS     d   dDer angegebene Name '%1' existiert bereits. Bitte geben Sie einen neuen Namen ein.    FT     8   8Die Submission muss einen Namen haben.    FU     �   �Das Löschen der Bindung '$BINDINGNAME' wirkt sich auf alle Steuerelemente aus, die damit verbunden sind.

Möchten Sie die Bindung wirklich löschen?    FV     �   �Das Löschen der Vorlage '$SUBMISSIONNAME' wirkt sich auf alle Steuerelemente aus, die damit verbunden sind.

Möchten Sie die Vorlage wirklich löschen?   FW     N   NMöchten Sie das Attribut '$ATTRIBUTENAME' wirklich löschen?   FX     �   �Das Löschen des Elements '$ELEMENTNAME' wirkt sich auf alle Steuerelemente aus, die mit diesem verbunden sind.
Möchten Sie das Element wirklich löschen?   FY     �   �Das Löschen der Instanz '$INSTANCENAME' wirkt sich auf alle Steuerelemente aus, die mit dieser Instanz verbunden sind.
Möchten Sie die Instanz wirklich löschen?   F[        Formular    F\        Datensatz   F]        von   F^     ,   ,Setzen der Eigenschaft '#'    F_     (   (Einfügen in Container    F`        # löschen    Fk     $   $# Objekte löschen    Fl     2   2Ersetzen eines Containerelements    Fn     "   "Struktur löschen   Fo     (   (Steuerelement ersetzen    Fp     (   (Symbolleiste Navigation   Ft        Formular    Fu     "   "Feld hinzufügen:   Fv     0   0Kein Steuerelement ausgewählt    Fw          Eigenschaften:    Fx     (   (Formular-Eigenschaften    Fy     $   $Formular Navigator    Fz        Formulare   F{     B   BFehler beim Schreiben von Daten in die Datenbank    F|     <   <Sie beabsichtigen, 1 Datensatz zu löschen.   F}     �   �Wenn Ja ausgewählt wird, kann die Operation nicht mehr rückgängig gemacht werden!
Wollen Sie trotzdem fortfahren?    F~        Rahmenelement   F        Navigation    F�        Spalte    F�        Datum   F�        Zeit    F�     (   (Symbolleiste Navigation   F�        Schaltfläche   F�        Optionsfeld   F�        Markierfeld   F�     "   "Beschriftungsfeld   F�     $   $Gruppierungsrahmen    F�        Textfeld    F�        Listenfeld    F�     "   "Kombinationsfeld    F�     (   (Grafische Schaltfläche   F�     *   *Grafisches Steuerelement    F�        Dateiauswahl    F�        Datumsfeld    F�        Zeitfeld    F�     "   "Numerisches Feld    F�        Währungsfeld   F�          Maskiertes Feld   F�     (   (Tabellen-Steuerelement    F�          Mehrfachauswahl   F�     >   >Sie beabsichtigen # Datensätze zu löschen.    F�        Steuerelement   F�         (Datum)    F�         (Zeit)   F�     t   tEs existieren keine an ein gültiges Tabellenfeld gebundenen Steuerelemente im aktuellen Formular!    F�     "   "Filter Navigator    F�        Filtern nach    F�        Oder    F�     "   "Formatiertes Feld   F�     <   <Fehler bei der Analyse des Anfrageausdrucks   F�     �   �Das Löschen des Modells '$MODELNAME' wirkt sich auf alle Steuerelemente aus, die mit diesem verbunden sind.
Möchten Sie das Modell wirklich löschen?   F�     �   �Im aktuellen Formular existieren keine gültig gebundenen Steuerelemente, die für die Tabellenansicht verwendet werden könnten.   F�        <AutoFeld>    F�     .   .Syntaxfehler im SQL-Ausdruck    F�     B   BDer Wert #1 kann nicht mit WIE verwendet werden.    F�     @   @WIE kann bei diesem Feld nicht benutzt werden.    F�     X   XDas angegebene Kriterium kann nicht mit diesem Feld verglichen werden.    F�     J   JDas Feld kann nicht mit einem Integer verglichen werden.    F�     �   �Der eingegebene Wert ist kein gültiges Datum. Bitte geben Sie ein Datum in einem gültigen Format ein, z.B. TT.MM.JJ.    F�     R   RDas Feld kann nicht mit einer Fließkommazahl verglichen werden.    F�     B   BDie Datenbank enthält keine Tabelle namens "#".    F�     D   DDie Spalte "#1" ist in der Tabelle "#2" unbekannt.    F�          Bildlaufleiste    F�        Drehfeld    F�     *   *Verstecktes Steuerelement   F�        Element   F�        Attribut    F�        Bindung   F�     "   "Bindungsausdruck    F�        Senden    F�        Put   F�        Get   F�        Kein    F�        Instanz   F�        Dokument    F�          Daten Navigator   F�        Submission:     F�        ID:     F�        Aktion:     F�        Methode:    F�        Referenz:     F�        Bindung:    F�        Ersetzen:     F�     $   $Element hinzufügen   F�     $   $Element bearbeiten    F�     "   "Element löschen    F�     &   &Attribute hinzufügen   F�     $   $Attribut bearbeiten   F�     "   "Attribut löschen   F�     $   $Bindung hinzufügen   F�     $   $Bindung bearbeiten    F�     "   "Bindung löschen    F�     (   (Submission hinzufügen    F�     &   &Submission bearbeiten   F�     $   $Submission löschen   F�     X   XDie Datenbank enthält weder eine Tabelle noch eine Abfrage namens "#".   F�     V   VDie Datenbank enthält bereits eine Tabelle oder Ansicht namens "#".    F�     H   HDie Datenbank enthält bereits eine Abfrage namens "#".   F�     &   & (schreibgeschützt)    F�     N   NDie Datei existiert bereits. Soll sie überschrieben werden?    F�     &   &#object#-Beschriftung   H�     6   6Fehler beim Erzeugen eines Formulars    H�     R   REintrag existiert bereits.
Bitte wählen Sie einen anderen Namen.   H�     `   `Für das Feld '#' ist eine Eingabe erforderlich. Geben Sie dort einen Wert ein.   H�     �   �Wählen Sie ein Element der Liste aus oder geben Sie Text ein, der mit einem der aufgelisteten Elemente übereinstimmt.   (G  y   �   �   Millimeter     Zentimeter     Meter    Kilometer    Zoll     Fuß     	Meilen     
Pica     Point    Zeichen    Zeile      (H  y  
  
   NWesteuropa (Windows-1252/WinLatin 1)     Westeuropa (Apple Macintosh)     Westeuropa (DOS/OS2-850/International)     Westeuropa (DOS/OS2-437/US)    Westeuropa (DOS/OS2-860/Portugiesisch)     Westeuropa (DOS/OS2-861/Isländisch)     Westeuropa (DOS/OS2-863/Französisch (Kand.))    Westeuropa (DOS/OS2-865/Nordisch)    Westeuropa (ASCII/US)    Westeuropa (ISO-8859-1)    Osteuropa (ISO-8859-2)     Latin 3 (ISO-8859-3)     Baltisch (ISO-8859-4)    Kyrillisch (ISO-8859-5)    Arabisch (ISO-8859-6)    Griechisch (ISO-8859-7)    Hebräisch (ISO-8859-8)    Türkisch (ISO-8859-9)     Westeuropa (ISO-8859-14)     Westeuropa (ISO-8859-15/EURO)    Griechisch (DOS/OS2-737)     Baltisch (DOS/OS2-775)     Osteuropa (DOS/OS2-852)    Kyrillisch (DOS/OS2-855)     Türkisch (DOS/OS2-857)    Hebräisch (DOS/OS2-862)     Arabisch (DOS/OS2-864)     Kyrillisch (DOS/OS2-866/Russisch)    Griechisch (DOS/OS2-869/Modern)    Osteuropa (Windows-1250/WinLatin 2)    !Kyrillisch (Windows-1251)    "Griechisch (Windows-1253)    #Türkisch (Windows-1254)     $Hebräisch (Windows-1255)    %Arabisch (Windows-1256)    &Baltisch (Windows-1257)    'Vietnamesisch (Windows-1258)     (Osteuropa (Apple Macintosh)    *Osteuropa (Apple Macintosh/Kroatisch)    +Kyrillisch (Apple Macintosh)     ,Griechisch (Apple Macintosh)     /Westeuropa (Apple Macintosh/Isländisch)     3Osteuropa (Apple Macintosh/Rumänisch)     4Türkisch (Apple Macintosh)    6Kyrillisch (Apple Macintosh/Ukrainisch)    7Chinesisch vereinfacht (Apple Macintosh)     8Chinesisch traditionell (Apple Macintosh)    9Japanisch (Apple Macintosh)    :Koreanisch (Apple Macintosh)     ;Japanisch (Windows-932)    <Chinesisch vereinfacht (Windows-936)     =Koreanisch (Windows-949)     >Chinesisch traditionell (Windows-950)    ?Japanisch (Shift-JIS)    @Chinesisch vereinfacht (GB-2312)     AChinesisch vereinfacht (GB-18030)    UChinesisch traditionell (GBT-12345)    BChinesisch vereinfacht (GBK/GB-2312-80)    CChinesisch traditionell (Big5)     DChinesisch traditionell (Big5-HKSCS)     VJapanisch (EUC-JP)     EChinesisch vereinfacht (EUC-CN)    FChinesisch traditionell (EUC-TW)     GJapanisch (ISO-2022-JP)    HChinesisch vereinfacht (ISO-2022-CN)     IKyrillisch (KOI8-R)    JUnicode (UTF-7)    KUnicode (UTF-8)    LOsteuropa (ISO-8859-10)    MOsteuropa (ISO-8859-13)    NKoreanisch (EUC-KR)    OKoreanisch (ISO-2022-KR)     PKoreanisch (Windows-Johab-1361)    TUnicode (UTF-16)    ��Thai (ISO-8859-11/TIS-620)     WThai (Windows-874)      Kyrillisch (KOI8-U)    XKyrillisch (PT154)     ]  <v  y  �  �   ;Maßstab    'Pinsel    'Tabulatoren   'Zeichen   'Schrift   'Schriftstellung   'Schriftstärke    'Schattiert    'Wortweise   'Kontur    'Durchgestrichen   'Unterstrichen   'Schriftgrad   'Rel. Schriftgrad    ' Schriftfarbe    '!Kerning   '"Effekte   '#Sprache   '$Position    '%Zeichen blinkend    'SZeichensatzfarbe    *}Überstreichung   -0Absatz    '*Ausrichtung   '+Zeilenabstand   '1Seitenumbruch   '5Silbentrennung    '6Absätze nicht trennen    '7Schusterjungen    '8Hurenkinder   '9Absatzabstände   ':Absatzeinzug    ';Einzug    '@Abstand   'ASeite   'BSeitenvorlage   'QAbsätze zusammenhalten   'RBlinkend    (�Registerhaltigkeit    (�Zeichenhintergrund    )_Asiatische Schrift    *�Schriftgrad asiatische Schrift    *�Sprache asiatische Schrift    *�Schriftstellung asiatische Schrift    *�Schriftstärke asiatische Schrift   *�CTL   *�Schriftgrad komplexe Skripte    *�Sprache komplexe Skripte    *�Schriftstellung komplexe Skripte    *�Schriftstärke komplexe Skripte   *�Doppelzeilig    *�Betonungszeichen    *�Text Abstand    *�Hängende Interpunktion   *�Verbotene Zeichen   *�Drehung   *�Zeichenskalierung   *�Relief    *�Vertikale Textausrichtung   *�  �     *   *      res/grafikei.png    �     *   *      res/grafikde.png    �     ,   ,      svx/res/markers.png   �     4   4      svx/res/pageshadow35x35.png   �     (   (      res/oleobj.png    �     0   0      svx/res/cropmarkers.png   '�     .   .      svx/res/rectbtns.png    '�     ,   ,      svx/res/objects.png   '�     (   (      	svx/res/ole.png   '�     ,   ,      
svx/res/graphic.png   'S  #   f   f            8   8      svx/res/slidezoombutton_10.png              ��  ��      'T  #   b   b            4   4      svx/res/slidezoomout_10.png             ��  ��      'U  #   b   b            4   4      svx/res/slidezoomin_10.png              ��  ��      'e  #   V   V            (   (      res/sc10223.png             ��  ��      'f  #   V   V            (   (      res/sc10224.png             ��  ��      'g  #   `   `            2   2      svx/res/signet_11x16.png              ��  ��      'i  #   `   `            2   2      svx/res/caution_11x16.png             ��  ��      'k  #   d   d            6   6      svx/res/notcertificate_16.png             ��  ��      '�  #   Z   Z            ,   ,      svx/res/lighton.png             ��  ��      '�  #   X   X            *   *      svx/res/light.png             ��  ��      '�  #   \   \            .   .      svx/res/colordlg.png              ��  ��      '�  #   H   H            4   4      svx/res/selection_10x22.png   (  #   \   \            .   .      svx/res/notcheck.png              ��  ��      (  #   \   \            .   .      svx/res/lngcheck.png              ��  ��      (�  #   V   V            (   (      res/sc10865.png             ��  ��      (�  #   V   V            (   (      res/sc10866.png             ��  ��      (�  #   V   V            (   (      res/sc10867.png             ��  ��      (�  #   V   V            (   (      res/sc10863.png             ��  ��      (�  #   V   V            (   (      res/sc10864.png             ��  ��      (�  #   V   V            (   (      res/sc10868.png             ��  ��      (�  #   V   V            (   (      res/sc10869.png             ��  ��      (�  #   V   V            (   (       res/reload.png              ��  ��      (�  #   Z   Z            ,   ,      !svx/res/reloads.png             ��  ��      (�  #   h   h            :   :      "svx/res/extrusioninfinity_16.png              ��  ��      (�  #   d   d            6   6      #svx/res/extrusion0inch_16.png             ��  ��      (�  #   f   f            8   8      $svx/res/extrusion05inch_16.png              ��  ��      (�  #   d   d            6   6      %svx/res/extrusion1inch_16.png             ��  ��      (�  #   d   d            6   6      &svx/res/extrusion2inch_16.png             ��  ��      (�  #   d   d            6   6      'svx/res/extrusion4inch_16.png             ��  ��      )$  #   `   `            2   2      (svx/res/wireframe_16.png              ��  ��      )%  #   \   \            .   .      )svx/res/matte_16.png              ��  ��      )&  #   ^   ^            0   0      *svx/res/plastic_16.png              ��  ��      )'  #   \   \            .   .      +svx/res/metal_16.png              ��  ��      +�  #   f   f            8   8      ,svx/res/doc_modified_yes_14.png             ��  ��      +�  #   f   f            8   8      -svx/res/doc_modified_no_14.png              ��  ��      +�  #   h   h            :   :      .svx/res/doc_modified_feedback.png             ��  ��      +�  #   f   f            8   8      /svx/res/zoom_page_statusbar.png             ��  ��      +�  #   J   J            6   6      0svx/res/symphony/spacing3.png   +�  #   P   P            <   <      1svx/res/symphony/Indent_Hanging.png   +�  #   H   H            4   4      2svx/res/symphony/blank.png    +�  #   H   H            4   4      3svx/res/symphony/width1.png   ,   #   H   H            4   4      4svx/res/symphony/width2.png   ,  #   H   H            4   4      5svx/res/symphony/width3.png   ,  #   H   H            4   4      6svx/res/symphony/width4.png   ,  #   H   H            4   4      7svx/res/symphony/width5.png   ,  #   H   H            4   4      8svx/res/symphony/width6.png   ,  #   H   H            4   4      9svx/res/symphony/width7.png   ,  #   H   H            4   4      :svx/res/symphony/width8.png   ,  #   H   H            4   4      ;svx/res/symphony/axial.png    ,  #   L   L            8   8      <svx/res/symphony/ellipsoid.png    ,	  #   L   L            8   8      =svx/res/symphony/Quadratic.png    ,
  #   H   H            4   4      >svx/res/symphony/radial.png   ,  #   H   H            4   4      ?svx/res/symphony/Square.png   ,  #   H   H            4   4      @svx/res/symphony/linear.png   ,  #   N   N            :   :      Asvx/res/symphony/rotate_left.png    ,  #   N   N            :   :      Bsvx/res/symphony/rotate_right.png   ,  #   >   >            *   *      Csvx/res/nu01.png    ,  #   >   >            *   *      Dsvx/res/nu04.png    ,  #   >   >            *   *      Esvx/res/nu02.png    ,,  #   h   h            :   :      Fsvx/res/directionnorthwest_22.png             ��  ��      ,-  #   d   d            6   6      Gsvx/res/directionnorth_22.png             ��  ��      ,.  #   h   h            :   :      Hsvx/res/directionnortheast_22.png             ��  ��      ,/  #   d   d            6   6      Isvx/res/directionwest_22.png              ��  ��      ,0  #   h   h            :   :      Jsvx/res/directionstraight_22.png              ��  ��      ,1  #   d   d            6   6      Ksvx/res/directioneast_22.png              ��  ��      ,2  #   h   h            :   :      Lsvx/res/directionsouthwest_22.png             ��  ��      ,3  #   d   d            6   6      Msvx/res/directionsouth_22.png             ��  ��      ,4  #   h   h            :   :      Nsvx/res/directionsoutheast_22.png             ��  ��      ,6  #   b   b            4   4      Osvx/res/perspective_16.png              ��  ��      ,7  #   ^   ^            0   0      Psvx/res/parallel_16.png             ��  ��      ,G  #   j   j            <   <      Qsvx/res/lightofffromtopleft_22.png              ��  ��      ,H  #   f   f            8   8      Rsvx/res/lightofffromtop_22.png              ��  ��      ,I  #   j   j            <   <      Ssvx/res/lightofffromtopright_22.png             ��  ��      ,J  #   f   f            8   8      Tsvx/res/lightofffromleft_22.png             ��  ��      ,L  #   h   h            :   :      Usvx/res/lightofffromright_22.png              ��  ��      ,M  #   l   l            >   >      Vsvx/res/lightofffrombottomleft_22.png             ��  ��      ,N  #   h   h            :   :      Wsvx/res/lightofffrombottom_22.png             ��  ��      ,O  #   n   n            @   @      Xsvx/res/lightofffrombottomright_22.png              ��  ��      ,Q  #   h   h            :   :      Ysvx/res/lightonfromtopleft_22.png             ��  ��      ,R  #   d   d            6   6      Zsvx/res/lightonfromtop_22.png             ��  ��      ,S  #   j   j            <   <      [svx/res/lightonfromtopright_22.png              ��  ��      ,T  #   f   f            8   8      \svx/res/lightonfromleft_22.png              ��  ��      ,V  #   f   f            8   8      ]svx/res/lightonfromright_22.png             ��  ��      ,W  #   l   l            >   >      ^svx/res/lightonfrombottomleft_22.png              ��  ��      ,X  #   h   h            :   :      _svx/res/lightonfrombottom_22.png              ��  ��      ,Y  #   l   l            >   >      `svx/res/lightonfrombottomright_22.png             ��  ��      ,[  #   f   f            8   8      asvx/res/lightfromtopleft_22.png             ��  ��      ,\  #   b   b            4   4      bsvx/res/lightfromtop_22.png             ��  ��      ,]  #   h   h            :   :      csvx/res/lightfromtopright_22.png              ��  ��      ,^  #   d   d            6   6      dsvx/res/lightfromleft_22.png              ��  ��      ,_  #   d   d            6   6      esvx/res/lightfromfront_22.png             ��  ��      ,`  #   d   d            6   6      fsvx/res/lightfromright_22.png             ��  ��      ,a  #   j   j            <   <      gsvx/res/lightfrombottomleft_22.png              ��  ��      ,b  #   f   f            8   8      hsvx/res/lightfrombottom_22.png              ��  ��      ,c  #   j   j            <   <      isvx/res/lightfrombottomright_22.png             ��  ��      ,e  #   `   `            2   2      jsvx/res/brightlit_16.png              ��  ��      ,f  #   `   `            2   2      ksvx/res/normallit_16.png              ��  ��      ,g  #   \   \            .   .      lsvx/res/dimlit_16.png             ��  ��      ,m  #   h   h            :   :      msvx/res/fontworkalignleft_16.png              ��  ��      ,n  #   l   l            >   >      nsvx/res/fontworkaligncentered_16.png              ��  ��      ,o  #   h   h            :   :      osvx/res/fontworkalignright_16.png             ��  ��      ,p  #   l   l            >   >      psvx/res/fontworkalignjustified_16.png             ��  ��      ,q  #   j   j            <   <      qsvx/res/fontworkalignstretch_16.png             ��  ��      ,r  #   >   >            *   *      rsvx/res/fw018.png   ,s  #   >   >            *   *      ssvx/res/fw019.png   ,t  #   >   >            *   *      tsvx/res/fw016.png   ,u  #   >   >            *   *      usvx/res/fw017.png      $  �  �   id              ��  ��       svx/res/id01.png     svx/res/id02.png     svx/res/id03.png     svx/res/id04.png     svx/res/id05.png     svx/res/id06.png     svx/res/id07.png     svx/res/id08.png     svx/res/id030.png    svx/res/id031.png    svx/res/id032.png     svx/res/id033.png    !svx/res/id040.png    (svx/res/id041.png    )svx/res/id016.png    svx/res/id018.png    svx/res/id019.png       'Q  $  @  @   fr              ��  ��       svx/res/fr01.png     svx/res/fr02.png     svx/res/fr03.png     svx/res/fr04.png     svx/res/fr05.png     svx/res/fr06.png     svx/res/fr07.png     svx/res/fr08.png     svx/res/fr09.png     	svx/res/fr010.png    
svx/res/fr011.png    svx/res/fr012.png       'R  $   �   �   da              ��  ��       res/da01.png     res/da02.png     res/da03.png     res/da04.png     res/da05.png     res/da06.png        FP  $  �  �   sx              �   �        res/sx10594.png   )bres/sx10595.png   )cres/sx10596.png   )dres/sx10597.png   )eres/sx10598.png   )fres/sx10599.png   )gres/sx10600.png   )hres/sx10601.png   )ires/sx10607.png   )ores/sx10144.png   '�res/sx10593.png   )ares/sx18013.png   F]res/sx18002.png   FRres/sx18003.png   FSres/sx10604.png   )lres/sx10605.png   )mres/sx10704.png   )�res/sx10705.png   )�res/sx10706.png   )�res/sx10707.png   )�res/sx10708.png   )�res/sx18022.png   Ffres/sx10710.png   )�res/sx10603.png   )kres/sx10715.png   )�res/sx10728.png   )�res/sx10757.png   *res/sx18027.png   Fkres/sx10768.png   *res/sx10769.png   *   FQ  $   �   �   tb              ��  ��       res/tb01.png     res/tb02.png     res/tb03.png     res/tb04.png     res/tb05.png        (K  D     (   8            ?   �     D   h   h  @�    �  SVX_HID_PPROPERTYPANEL_TEXT_UNDERLINE_VS                 9   xUnderline      F   `   `  @�       SVX_HID_UNDERLINE_BTN          }      9   ~Weitere Optionen...     
  #   H   H            4   4      vsvx/res/symphony/line1.png       #   H   H            4   4      wsvx/res/symphony/line2.png       #   H   H            4   4      xsvx/res/symphony/line3.png       #   H   H            4   4      ysvx/res/symphony/line4.png       #   H   H            4   4      zsvx/res/symphony/line5.png       #   H   H            4   4      {svx/res/symphony/line6.png       #   H   H            4   4      |svx/res/symphony/line7.png       #   H   H            4   4      }svx/res/symphony/line8.png       #   H   H            4   4      ~svx/res/symphony/line9.png       #   H   H            4   4      svx/res/symphony/line10.png    (  #   P   P            <   <      �svx/res/symphony/selected-line1.png    )  #   P   P            <   <      �svx/res/symphony/selected-line2.png    *  #   P   P            <   <      �svx/res/symphony/selected-line3.png    +  #   P   P            <   <      �svx/res/symphony/selected-line4.png    ,  #   P   P            <   <      �svx/res/symphony/selected-line5.png    -  #   P   P            <   <      �svx/res/symphony/selected-line6.png    .  #   P   P            <   <      �svx/res/symphony/selected-line7.png    /  #   P   P            <   <      �svx/res/symphony/selected-line8.png    0  #   P   P            <   <      �svx/res/symphony/selected-line9.png    1  #   R   R            >   >      �svx/res/symphony/selected-line10.png     2        (Ohne)             Einfach            Doppelt            Fett             Gepunktet         "   "Gepunktet (Fett)             Gestrichelt         "   "Lang gestrichelt             Punkt Strich          "   "2 Punkt 1 Strich             Gewellt   (M  D     (   8            V   �     D   n   n  @�    �  SVX_HID_PPROPERTYPANEL_TEXT_SPACING_VS                 P   ZCharacter Spacing      W   H   H   �      @          _      H   Benutzerdefiniert:       W   F   F   �      @          n      >   ~Zeichenabstand:       U   z   z  @?     SVX_HID_SPACING_CB_KERN          x      >   P��   Standard      Gesperrt      Schmal           W   F   F   �      @          �      >   Ersetzen ~durch:     	  d   h   h  @?     `SVX_HID_SPACING_MB_KERN          �      >          '             
     #   P   P            <   <      �svx/res/symphony/spacing_normal.png       #   T   T            @   @      �svx/res/symphony/spacing_very tight.png    !  #   P   P            <   <      �svx/res/symphony/spacing_tight.png     "  #   P   P            <   <      �svx/res/symphony/spacing_loose.png     #  #   T   T            @   @      �svx/res/symphony/spacing_very loose.png    3  #   R   R            >   >      �svx/res/symphony/spacing_normal_s.png    4  #   V   V            B   B      �svx/res/symphony/spacing_very tight_s.png    5  #   R   R            >   >      �svx/res/symphony/spacing_tight_s.png     6  #   R   R            >   >      �svx/res/symphony/spacing_loose_s.png     7  #   V   V            B   B      �svx/res/symphony/spacing_very loose_s.png    $  #   T   T            @   @      �svx/res/symphony/last_custom_common.png    %  #   Z   Z            F   F      �svx/res/symphony/last_custom_common_grey.png     =        Sehr schmal    >        Schmal     ?        Normal     @        Weit     A        Sehr weit    B     &   &Letzter eigener Wert     C     ,   , Abstand verkürzt um: 3 pt    D     .   . Abstand verkürzt um: 1.5 pt    E     "   " Abstand: Normal     F     0   0 Abstand vergrößert um: 3 pt     G     0   0 Abstand vergrößert um: 6 pt     H     (   ( Abstand verkürzt um      I     *   * Abstand vergrößert um     J        pt    (P  D  �                	  W   4   4   �              -   	Zentrum ~X:    
  d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_CENTER_X       -   Wählen Sie den relativen Abstand des Farbverlauf-Schattens waagerecht zum Zentrum. 50% entspricht der Mitte.         d             d        W   4   4   �              4   	Zentrum ~Y:      d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_CENTER_Y       -   Wählen Sie den relativen Abstand des Farbverlauf-Schattens senkrecht zum Zentrum. 50% entspricht der Mitte.          d             d        W   2   2   �              d   	~Winkel:       d   �   �  B8     ` SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_ANGLE        -   Drehwinkel für den Farbverlaufs-Schatten wählen.       ����  '       Grad            W   4   4   �              -   	~Startwert:      d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_SVALUE       -   Transparenzwert für den Startpunkt des Farbverlaufs eingeben (0% entspricht keiner Transparenz und 100% entspricht voller Transparenz)         d             d        W   2   2   �              4   	~Endwert:      d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_EVALUE       -   Transparenzwert für den Endpunkt des Farbverlaufs eingeben (0% entspricht keiner Transparenz und 100% entspricht voller Transparenz)         d             d        W   0   0   �              d   	~Rand:       d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_BORDER       -   Wählen Sie die Transparenz des Farbverlaufs.         d             d        q   �   �  @�      @SVX_HID_PPROPERTYPANEL_AREA_BTN_LEFT_SECOND Rotate Left        �         p           Rotate Left      q   �   �  @�      @SVX_HID_PPROPERTYPANEL_AREA_BTN_RIGHT_FIRST Rotate Right         �         p   "   "   Rotate Right          <   <Gegen den Uhrzeigersinn um 45 Grad drehen.          4   4Im Uhrzeigersinn um 45 Grad drehen.   (T  D  �   (   8            V   �     D   `   `  @�    �  SVX_HID_PPROPERTYPANEL_LINE_VS_WIDTH                 P   lWidth      W   H   H   �                 q      P   Benutzerdefiniert:       W   D   D   �                 �      @   Linien~breite:       d   �   �  B?     aSVX_HID_PPROPERTYPANEL_LINE_MTR_WIDTH          �      (   Linienbreite angeben.      6  �                �   
     #   T   T            @   @      �svx/res/symphony/last_custom_common.png      #   Z   Z            F   F      �svx/res/symphony/last_custom_common_grey.png          $   $Letzer eigener Wert    	        pt    (W  D   �                    W   �   �   �                       d   Für die aktuelle Auswahl sind keine Einstellungen der auszuführenden Aufgabe verfügbar   '�  T   D   D  @8    �  SVX_HID_STYLE_LISTBOX       <   V        'b    �  �               @   @   J   Ganze Seite SVX_HID_MNU_ZOOM_WHOLE_PAGE          B   B   J   Seitenbreite  SVX_HID_MNU_ZOOM_PAGE_WIDTH          D   D   J   Optimale Ansicht  SVX_HID_MNU_ZOOM_OPTIMAL           0   0   J   50% SVX_HID_MNU_ZOOM_50          0   0   J   75% SVX_HID_MNU_ZOOM_75          4   4   J   100%  SVX_HID_MNU_ZOOM_100           4   4   J   150%  SVX_HID_MNU_ZOOM_150           0   0   H200%  SVX_HID_MNU_ZOOM_200    'c    �  �               6   6   HMittelwert  SVX_HID_MNU_FUNC_AVG           8   8   J   Anzahl2 SVX_HID_MNU_FUNC_COUNT2          8   8   J   Anzahl  SVX_HID_MNU_FUNC_COUNT           6   6   J   Maximum SVX_HID_MNU_FUNC_MAX           6   6   J   Minimum SVX_HID_MNU_FUNC_MIN           4   4   J   	Summe SVX_HID_MNU_FUNC_SUM           B   B   J   Zellen  SVX_HID_MNU_FUNC_SELECTION_COUNT           4   4   J   Keine SVX_HID_MNU_FUNC_NONE   'd     X   X               @   @   HDigitale Signaturen...  SVX_HID_XMLSEC_CALL   'l    �  �               $   $      Millimeter           (   (         Zentimeter           "   "         Meter          &   &         Kilometer          "   "         Zoll           "   "      	   Fuß           $   $      
   Meilen           "   "         Point          "   "         Pica           $   $         Zeichen          "   "         Zeile   '�     t   t               .   .   
  '�Vorlage aktualisieren          .   .   
  '�Vorlage bearbeiten...   '�     �   �               (   (      Standardauswahl          .   .         Auswahl erweitern          .   .         Auswahl ergänzen          *   *         Blockauswahl    (�    �  �               $   $   Beschreibung...          "   "   
   
Ma~kro...                
   	Aktiv                          �   �  
   Anordnung          �   �               (   (   
   Ganz nach vorn           *   *   
   W~eiter nach vorn          ,   ,   
   Weiter ~nach hinten          *   *   
   Ganz nach ~hinten                          *   *   
   ~Alles auswählen          "   "   
   ~Löschen   FP    4  4               �   �  J  )�~Neu  SVX_HID_FM_NEW           �   �               6   6   J  )�Formular  SVX_HID_FM_NEW_FORM          H   H   J  )pVerstecktes Steuerelement SVX_HID_FM_NEW_HIDDEN          @   @   J  )�Ersetzen durch  .uno:ChangeControlType           0   0  
  N~Ausschneiden .uno:Cut           ,   ,  
  O~Kopieren .uno:Copy          0   0  
  P~Einfügen  .uno:Paste           4   4   J  )�~Löschen SVX_HID_FM_DELETE          D   D   J  )wAktivierungsreihenfolge ... .uno:TabDialog           >   >   J  )q~Umbenennen SVX_HID_FM_RENAME_OBJECT           B   B   J  )�~Eigenschaften  .uno:ShowPropertyBrowser           D   D   J  )�Im Entwurfsmodus öffnen  .uno:OpenReadOnly          P   P   J  *Automatischer Steuerelement-Fokus .uno:AutoControlFocus   FQ     �   �               >   >   J  )rZeilen löschen SVX_HID_FM_DELETEROWS          :   :   J  )�Datensatz speichern .uno:RecSave           B   B   J  )�Rückgängig: Dateneingabe  .uno:RecUndo    FR    �  �              �  �  J  )s~Spalte einfügen SVX_HID_FM_INSERTCOL          �  �               ,   ,   J  )gTextfeld  .uno:Edit          2   2   J  )dMarkierfeld .uno:CheckBox          8   8   J  )iKombinationsfeld  .uno:ComboBox          2   2   J  )hListenfeld  .uno:ListBox           4   4   J  )�Datumsfeld  .uno:DateField           2   2   J  )�Zeitfeld  .uno:TimeField           <   <   J  )�Numerisches Feld  .uno:NumericField          :   :   J  )�Währungsfeld .uno:CurrencyField           :   :   J  )�Maskiertes Feld .uno:PatternField          >   >   J  )�Formatiertes Feld .uno:FormattedField          N   N  J  *Datums- und Zeit-Feld SVX_HID_CONTROLS_DATE_N_TIME            >   >   J  )n~Ersetzen durch SVX_HID_FM_CHANGECOL           >   >   J  )tSpalte löschen SVX_HID_FM_DELETECOL           @   @   J  *Spalte ~ausblenden  SVX_HID_FM_HIDECOL           �   �  J  *Spalten ~einblenden SVX_HID_FM_SHOWCOLS          �   �               <   <   J  *~Mehr...  SVX_HID_FM_SHOWCOLS_MORE                           6   6   J  *~Alle SVX_HID_FM_SHOWALLCOLS           <   <   J  )�Spalte... .uno:ShowPropertyBrowser    FS     D   D               ,   ,  
  O~Kopieren .uno:Copy   FT    �  �               J   J  J  )�~Textfeld .uno:ConvertToEdit  .uno:ConvertToEdit           T   T  J  )�~Schältfläche .uno:ConvertToButton  .uno:ConvertToButton           T   T  J  )�~Beschriftungsfeld  .uno:ConvertToFixed .uno:ConvertToFixed          T   T  J  )�~Gruppierungsrahmen .uno:ConvertToGroup .uno:ConvertToGroup          L   L  J  )�~Listenfeld .uno:ConvertToList  .uno:ConvertToList           V   V  J  )�~Markierfeld  .uno:ConvertToCheckBox  .uno:ConvertToCheckBox           N   N  J  )�~Optionsfeld  .uno:ConvertToRadio .uno:ConvertToRadio          R   R  J  )�~Kombinationsfeld .uno:ConvertToCombo .uno:ConvertToCombo          b   b  J  )�~Grafische Schaltfläche  .uno:ConvertToImageBtn  .uno:ConvertToImageBtn           Z   Z  J  )�~Dateiauswahl .uno:ConvertToFileControl .uno:ConvertToFileControl          L   L  J  )�D~atumsfeld .uno:ConvertToDate  .uno:ConvertToDate           J   J  J  )�~Zeitfeld .uno:ConvertToTime  .uno:ConvertToTime           V   V  J  )�~Numerisches Feld .uno:ConvertToNumeric .uno:ConvertToNumeric          X   X  J  )�~Währungsfeld  .uno:ConvertToCurrency  .uno:ConvertToCurrency           V   V  J  )�Mask~iertes Feld  .uno:ConvertToPattern .uno:ConvertToPattern          j   j  J  )�G~rafisches Steuerelement .uno:ConvertToImageControl  .uno:ConvertToImageControl           \   \  J  )�~Formatiertes Feld  .uno:ConvertToFormatted .uno:ConvertToFormatted          X   X  J  *Bildlaufleiste  .uno:ConvertToScrollBar .uno:ConvertToScrollBar          V   V  J  *Drehfeld  .uno:ConvertToSpinButton  .uno:ConvertToSpinButton           h   h  J  *Symbolleiste Navigation .uno:ConvertToNavigationBar .uno:ConvertToNavigationBar   FU     �   �               4   4   J  )�~Löschen SVX_HID_FM_DELETE          4   4   J  *~Bearbeiten SVX_HID_FM_EDIT          <   <   J  *~Ist Leer SVX_HID_FM_FILTER_IS_NULL          @   @   J  *~Ist nicht leer SVX_HID_FM_IS_NOT_NULL    FV                     2   2  
  'Schrift .uno:CharFontName          0   0  
  'Größe .uno:FontHeight         <  <  J  FWSti~l SVX_HID_MENU_FM_TEXTATTRIBUTES_STYLE          �  �      
         (   (  
  'Fett  .uno:Bold          ,   ,  
  'Kursiv  .uno:Italic          6   6  
  -0Überstreichen  .uno:Overline          8   8  
  'Unterstreichen  .uno:Underline           8   8  
  'Durchstreichen  .uno:Strikeout           0   0  
  'Schatten  .uno:Shadowed          2   2  
  'Kontur  .uno:OutlineFont                           8   8  
  (6Ho~chstellen  .uno:SuperScript           6   6  
  (7~Tiefstellen  .uno:SubScript          B  B  N  FX   Aus~richtung  SVX_HID_MENU_FM_TEXTATTRIBUTES_ALIGNMENT           �   �               2   2    ',   ~Links  .uno:LeftPara          4   4    '-   ~Rechts .uno:RightPara           8   8    '.   ~Zentriert  .uno:CenterPara          8   8    '/   Blocksatz .uno:JustifyPara              J  FYZeilenabs~tand  SVX_HID_MENU_FM_TEXTATTRIBUTES_SPACING           �   �               6   6    '2   Einzeilig .uno:SpacePara1          :   :    '3   1,5-zeilig  .uno:SpacePara15           8   8    '4   ~Zweizeilig .uno:SpacePara2   FZ    �  �               L   L   J   
Objekt hinzufügen  SVX_HID_XFORMS_TOOLBOX_ITEM_ADD          T   T   J   Element hinzufügen SVX_HID_XFORMS_TOOLBOX_ITEM_ADD_ELEMENT          X   X   J   Attribute hinzufügen SVX_HID_XFORMS_TOOLBOX_ITEM_ADD_ATTRIBUTE          F   F   J   Bearbeiten  SVX_HID_XFORMS_TOOLBOX_ITEM_EDIT                           F   F   J   Löschen  SVX_HID_XFORMS_TOOLBOX_ITEM_REMOVE     �          �     � �J    �        (1  $,    (n  )    (o  2�    *d  3�    FP  6�    FQ  78       9�      9�      :      :,      :F      :`      :z      :�      :�    	  :�    
  :�      :�      ;    
   ;0    
  ;N    
  ;n    
  ;�    
  ;�    
  ;�    
  ;�    
  <    
  <*    
	  <@    

  <b    
  <�    
  <�    
  <�    
  <�    
  <�    
  =    
  =     
  =@    
  =`    
  =v    
  =�    
  =�    
  =�    
  >     
  >&    
  >R    
  >~    
  >�    
  >�    
  >�    
  >�    
   ?    
!  ?4    
"  ?P    
#  ?l    
$  ?�    
%  ?�    
&  ?�    
'  ?�    
(  @    
)  @&    
*  @D    
+  @d    
,  @�    
-  @�    
.  @�    
/  @�    
0  A    
1  A    
2  AH    
3  Ad    
4  A�    
5  A�    
6  A�    
7  A�    
8  A�    
9  B    
:  B8    
;  BX    
<  Bv    
=  B�    
>  B�    
?  B�    
@  B�    
A  C    
B  C<    
C  C`    
D  C|    
E  C�    
F  C�    
G  C�    
H  D    
I  D8    
J  D`    
K  D�    
L  D�    
M  D�    
N  D�    
O  E     
P  E    
Q  E2    
R  EV    
S  E|    
T  E�    
U  E�    
V  E�    
W  F&    
X  F@    
Y  FZ    
Z  F�    
[  F�    
\  F�    
]  F�    
^  F�    
_  G    
`  G:    
a  GX    
b  G�    
c  G�    
d  G�    
e  H    
f  H:    
g  Hr    
h  H�    
i  H�    
j  H�    
k  H�    
l  H�    
m  I    
n  I&    
o  I@    
p  Ij    
q  I�    
r  I�    
s  I�    
t  I�    
u  J    
v  J0    
w  JR    
x  Jp    
y  J�    
z  J�    
{  J�    
|  J�    
}  K    
~  K$    
  K>    
�  K^    
�  K�    
�  K�    
�  K�    
�  K�    
�  K�    
�  L    
�  L    
�  L4    
�  L\    
�  L�    
�  L�    
�  L�    
�  M    
�  M    
�  M8    
�  MV    
�  Mp    
�  M�    
�  M�    
�  M�    
�  M�    
�  N    
�  N    
�  NJ    
�  Nx    
�  N�    
�  N�    
�  N�    
�  O    
�  O<    
�  OV    
�  O~    
�  O�    
�  O�    
�  O�    
�  P    
�  PB    
�  Pl    
�  P�    
�  P�    
�  P�    
�  Q    
�  Q2    
�  Qd    
�  Q�    
�  Q�    
�  Q�    
�  Q�    
�  R&    
�  RN    
�  Rx    
�  R�    
�  R�    
�  R�    
�  S    
�  S6    
�  S`    
�  S�    
�  S�    
�  S�    
�  T    
�  T4    
�  TR    
�  Tt    
�  T�    
�  T�    
�  T�    
�  T�    
�  U
    
�  U>    
�  U`    
�  U�    
�  U�    
�  U�    
�  U�    
�  V    
�  V0    
�  V^    
�  V�    
�  V�    
�  V�    
�  V�    
�  W    
�  W8    
�  Wb    
�  W�    
�  W�    
�  W�    
�  X    
�  X(    
�  XR    
�  Xl    
�  X�    
�  X�    
�  X�    
�  Y    
�  Y2    
�  YX    
�  Y�    
�  Y�    
�  Y�    
�  Y�    
�  Z    
�  Z:    
�  Zf    
�  Z�    
�  Z�    
�  Z�    
�  [    
�  [,    
�  [f    
�  [�    
�  [�    
�  [�    
�  [�    
�  \    
�  \0    
�  \Z    
�  \z    
�  \�    
�  \�    
�  \�    
�  ]    
�  ]0    
�  ]L    
�  ]~       ]�      ]�      ]�      ^      ^6      ^`      ^�      ^�      _    	  _&    
  _F      _f      _�      _�      _�      `$      `>      `R      `f      `z      `�      `�      `�      `�      `�      a      a      a:      aN      al      a�      a�       a�    !  a�    "  b    #  b0    $  bH    %  b^    &  bt    '  b�    (  b�    )  b�    *  b�    +  c     ,  c    -  c:    .  c\    /  cx    0  c�    1  c�    2  c�    3  c�    4  d    5  d2    6  dT    7  dn    8  d�    9  d�    :  d�    ;  d�    <  d�    =  e    >  e6    ?  eZ    @  ez    A  e�    B  e�    C  e�    D  e�    E  f    F  f*    G  fH    H  fv    I  f�    J  f�    K  f�    R  g    S  g(    T  gF    U  g^    V  gx    W  g�    X  g�    Y  g�    Z  h
    [  h(    c  hR    d  hn    e  h�    f  h�    g  h�    h  i    i  i>    j  id    k  i�    l  i�    m  i�    n  j    o  j8    p  jd    q  j�    r  j�    s  j�    t  j�    u  j�    v  k    w  k"    x  k>    y  kZ    z  kv    {  k�    |  k�    }  k�    ~  k�      k�    �  k�    �  l    �  l$    �  lD    �  lb    �  l�    �  l�    �  l�    �  l�    �  l�    �  m    �  m"    �  m:    �  mP    �  ml    �  m�    �  m�    �  m�    �  m�    �  n    �  n"    �  n@    �  n\    �  n�    �  n�    �  n�    �  n�    �  o    �  o2    �  oR    �  or    �  o�    �  o�    �  o�    �  o�    �  p    �  p(    �  pD    �  p^    �  pz    �  p�    �  p�    �  p�    �  q    �  q&    �  qH    �  qn    �  q�    �  q�    �  q�    �  r    �  r(    �  rF    �  rd    �  r�    �  r�    �  r�    �  r�    �  s    �  s(    �  sL    �  sp    �  s�    �  s�    �  s�    �  s�    �  t    �  t<    �  td    �  t�    �  t�    �  t�    �  t�    �  u&    �  uP    �  uz    �  u�    �  u�    �  u�    �  v    �  v6    �  vZ    �  vt    �  v�    �  v�    �  v�    �  v�    �  w    �  w@    �  w\    �  w�    �  w�    �  w�    �  w�    �  x     �  xL    �  xx    �  x�    �  x�    �  x�    �  y    �  y>    �  y^    �  y�    �  y�    �  y�    �  z    �  z0    �  zV    �  z|    �  z�    �  z�       z�      {      {<      {h      {�      {�      {�      |
      |,    	  |N    
  ||      |�      |�      }      }&      }H      }f      }�      }�      }�      ~      ~,      ~R      ~�      ~�      ~�    $  ~�    %  ~�    &  *    '  T    (  v    )  �    *  �    +  �    ,  �$    -  �J    .  �v    /  ��    0  ��    1  ��    2  �
    3  �0    4  �P    5  �x    6  ��    7  ��    8  ��    9  �    :  �<    ;  �`    =  ��    >  ��    ?  ��    @  ��    A  ��    B  �    C  �4    D  �T    E  �t    F  ��    G  ��    H  ��    I  �    J  �    K  �8    L  �Z    M  �|    N  ��    O  ��    P  ��    Q  �     R  �     S  �>    T  �^    U  �|    V  ��    W  ��    X  ��    Y  �    Z  �*    [  �L    \  �l    ]  ��    ^  ��    _  ��    `  ��    a  �    b  �<    c  �b    d  ��    e  ��    f  ��    g  ��    h  �
    i  �8    j  �b    k  ��    l  ��    m  ��    n  ��    o  �    p  �4    q  �P    r  �n    s  ��    t  ��    u  ��    v  ��    w  �    x  �"    y  �B    z  �Z    {  �z    |  ��    }  ��    ~  ��      �    �  �6    �  �P    �  �v    �  ��    �  ��    �  ��    �  ��    �  �    �  �.    �  �H    �  �^    �  �z    �  ��    �  ��    �  ��    �  ��    �  ��    �  �    �  �    �  �B    �  �f    �  ��    �  ��    �  ��    �  ��    �  �    �  �(    �  �J    �  �z    �  ��    �  ��    �  ��    �  �$    '  �H    'W  �`    'Y  ��    'Z  ��    '[  ��    '\  ��    ']  ��    '^  ��    '_  ��    '`  �"    'a  �L    'b  �f    'c  ��    'd  ��    'e  ��    'h  ��    'i  �    'j  ��    '�  ��    '�  ��    '�  ��    '�  ��    '�  �    '�  �,    '�  �B    '�  �\    '�  �v    '�  ��    '�  ��    '�  ��    '�  ��    '�  �    '�  �2    '�  �X    '�  �|    '�  ��    '�  ��    '�  �    '�  �>    '�  �Z    '�  ��    '�  ��    '�  ��    '�  ��    '�  �    '�  �^    '�  ��    '�  ��    '�  �    '�  ��    '�  �.    '�  �r    '�  �    (  �$    (  �:    (  �R    (  �h    (
  ��    (  ��    (  ��    (  ��    (  ��    (#  �    ($  �&    (%  �<    (&  �R    ('  �h    ((  �|    (*  ��    (1  ��    (2  ��    (3  ��    (4  ��    (5  �    (6  �     (7  �:    (8  �\    (<  �z    (=  ��    (>  ��    (A  ��    (B  ��    (C  ��    (D  �    (E  �*    (�  �B    (�  �^    (�  �z    (�  ��    (�  ��    (�  ��    (�  ��    (�  �     (�  �@    (�  �\    (�  �~    (�  ��    )   ��    )  ��    )  �    )  �@    )  �^    )  �x    )  ��    )  ��    )  ��    )  ��    )  �&    )  �N    )   ��    )!  ��    )"  ��    )#  ��    ),  �    )-  �"    ).  �<    )/  �V    )0  �r    )1  ��    )2  ��    )4  ��    )5  ��    )6  �    )7  �,    )8  �L    )9  �d    ):  ��    )@  ��    )A  ��    )B  ��    )C  ��    )D  ��    )E  �    )F  �,    )G  �H    )H  �f    )I  ��    )J  ��    )K  ��    )L  ��    )M  ��    )N  �    )O  �    )Z  �0    )[  �T    )\  �v    )]  ��    )^  ��    )_  ��    )`  �    )c  �D    )d  �Z    )e  �p    )f  ��    )g  ��    )h  ��    )j  ��    )q  ��    )r  ��    )s  �    )t  �&    )u  �>    )v  �V    )w  �p    )x  ��    )y  ��    )z  ��    ){  ��    )|  ��    )}  �
    )~  �$    )  �:    )�  �R    )�  �j    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �8    )�  �V    )�  �r    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �     )�  �B    )�  �d    )�  ��    )�  ��    )�  ��    )�  ��    )�  �
    )�  �(    )�  �J    )�  �l    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �6    )�  �X    )�  �z    )�  ��    )�  ��    )�  ��    )�  �    )�  �@    )�  �`    )�  �z    )�  ��    )�  ��    )�  ��    )�  �    )�  �$    )�  �D    )�  �f    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �(    )�  �@    )�  �\    )�  �r    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �(    )�  �@    )�  �X    )�  �x    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �     )�  �    )�  �0    )�  �L    )�  �h    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �(    )�  �F    )�  �`    )�  �v    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �2    )�  �N    )�  �x    )�  ��    )�  ��    )�  ��    )�  �     )�  �(    )�  �F    )�  �d    )�  ��    *   ��    *  ��    *  ��    *  ��    *  �     *  �@    *  �j    *  ��    *  ��    *	  ��    *
  ��    *  �    *  �0    *  �L    *2  �h    *3  ��    *4  ��    *5  ��    *D  �    *E  �     *F  �:    *G  �T    *H  �p    *I  ��    *J  ��    *K  ��    *L  ��    *Y  ��    *^  �    *_  �F    *`  �z    *a  ��    *b  ��    *c  �    *m  �    *n  �@    *o  �^    *p  ��    *q  ��    *r  ��    *s  ��    *t  ��    *u  �     *v  �    *w  �*    *�  �>    *�  �b    *�  ��    *�  ��    *�  ��    *�  ��    *�  �    *�  �6    *�  �T    *�  �j    *�  ��    *�  ��    *�  ��    *�  ��    *�  ��    *�  �    *�  �&    *�  �@    *�  �Z    *�  �t    *�  ��    *�  ��    *�  ��    *�  �
    *�  �0    *�  �N    *�  ��    *�  ��    *�  ��    *�  ��    *�  �     *�  �    *�  �,    *�  �D    *�  �Z    *�  �r    *�  ��    *�  ��    *�  ��    *�  ��    *�  ��    +n  �    +o  �,    +p  �l    +r  ��    +s  ��    +t  ��    +u  �    +v  �<    +w  �^    +x  ��    +y  ��    +z  ��    +{  ��    +|  �    +}  �B    +~  �f    +�  ��    +�  ��    +�  �    +�  �^    +�  ��    +�  ��    +�  �:    +�  �n    +�  ��    +�  ��    +�  ��    +�  �    +�  �8    +�  �b    +�  ��    +�  ��    +�  ��    +�  �\    +�  ��    +�  �T    +�  �n    +�  �    +�  Į    +�  ��    +�  ��    +�  ��    +�  �    +�  �T    +�  �|    +�  Ɣ    +�  ��    +�  �:    +�  �\    +�  ǂ    +�  Ƕ    ,  ��    ,  �    ,  �L    ,  �~    ,  ȴ    ,  ��    ,  ɜ    ,  �F    ,  �^    ,  ��    ,  ��    ,  �    ,   �<    ,!  �X    ,"  �v    ,#  ˎ    ,$  ˨    ,%  ��    ,&  ��    ,'  ��    ,(  �    ,)  �    ,*  �:    ,+  �`    ,8  ̆    ,9  ̰    ,:  ��    ,;  �     ,<  �&    ,=  �L    ,>  �r    ,?  ͜    ,@  ��    ,B  ��    ,C  �
    ,D  �$    ,E  �:    ,F  �R    ,h  �p    ,i  Ύ    ,j  Ϊ    ,k  ��    ,l  ��    ,v  �    ,w  �&    ,x  �:    ,y  �N    ,z  �d    ,{  �z    ,|  ϐ    ,}  Ϭ    ,~  ��    .�  ��    .�  �    .�  �(    .�  �N    .�  �r    .�  Ж    .�  и    .�  ��    .�  �    .�  �2    .�  �X    .�  �t    .�  ѐ    .�  Ѭ    .�  ��    .�  ��    .�  �     .�  �    .�  �8    .�  �V    .�  �v    .�  Җ    .�  Ҵ    .�  ��    .�  ��    .�  �    .�  �:    .�  �`    .�  ӆ    .�  Ӫ    .�  ��    .�  ��    /   �    /  �@    /  �h    /  Ԑ    /  Զ    /  ��    /  �
    /  �&    /  �F    /	  �h    /
  Պ    /  ժ    /  ��    /  ��    /  �
    /  �&    /  �D    /  �b    /  �~    /  ֠    /  ־    /  ��    /  ��    /  �    /  �6    /  �T    /  �x    /  ט    /  ׶    /  ��    /  ��    /  �    /   �2    /!  �F    /"  �d    /#  ؂    /$  ؞    /%  ش    /&  ��    /'  ��    /(  �    /)  �2    /*  �L    /+  �h    /,  ن    /-  ٤    /.  ��    //  ��    /0  ��    /1  �    /2  �&    /3  �>    /4  �V    /5  �r    /6  ڎ    /7  ڨ    /D  ��    /E  ��    /F  �    /G  �(    /H  �J    /I  �p    /J  ې    /K  ۴    /L  ��    /M  �    /N  �,    /O  �H    /P  �d    /Q  ܀    /R  ܜ    /S  ܸ    /T  ��    /U  ��    /V  �    /W  �*    /X  �J    /Y  �j    /Z  ݈    /[  ݪ    /\  ��    /]  ��    /^  �    /_  �8    /`  �`    /a  ބ    /b  ެ    /c  ��    /d  ��    /e  �    /f  �<    /g  �b    /h  ߆    /i  ߮    /j  ��    /k  ��    /l  �    /m  �4    /n  �V    /o  �v    /p  ��    /q  �    /r  ��    /s  ��    /t  �    /u  �.    /v  �J    /w  �j    /x  �    /y  �    /z  ��    /{  ��    /|  �    /}  �     /~  �B    /  �d    /�  �    /�  �    /�  ��    /�  ��    /�  �    /�  �.    /�  �V    /�  �z    /�  �    /�  �    /�  ��    /�  ��    /�  �    /�  �.    /�  �J    /�  �f    /�  �    /�  �    /�  �    /�  ��    /�  ��    /�  �    /�  �*    /�  �B    /�  �b    /�  �~    /�  �    /�  �    1�  ��    1�  ��    1�  �
    1�  �&    1�  �F    1�  �d    1�  �    1�  �    1�  ��    1�  ��    1�  �     1�  �    1�  �.    1�  �P    1�  �r    1�  �    1�  �    1�  ��    1�  ��    1�  �    1�  �6    1�  �P    1�  �n    1�  �    1�  �    1�  ��    1�  ��    1�  �    1�  �4    1�  �R    2   �|    2  �    2  �    2  ��    2  ��    2  �
    2  �,    2  �B    2  �f    2	  �    2
  �    2  ��    :h  ��    :p  �    :q  �0    :r  �L    :s  �f    :t  �    :u  �    :v  ��    :w  ��    :z  ��    :{  �    :|  �,    :  �B    :�  �^    :�  �x    :�  �    :�  �    :�  ��    :�  ��    :�  ��    :�  �    :�  �$    :�  �B    :�  �`    :�  �    :�  �    :�  ��    :�  ��    <   ��    <  ��    <  �     <  �D    <!  �b    <"  �z    <#  �    <$  ��    <%  ��    <&  �    <F  �L    <G  �j    <H  �    <I  �    <X  ��    <Y  ��    <Z  �    <[  �.    <\  �N    <]  �n    <^  ��    <_  �    <`  �    <a  ��    <b  ��    <c  ��    <d  �    <o  �*    <p  �B    <q  �Z    <r  �r    <s  �    <u  �    C  ��    C  ��    FQ  �    FR  �x    FS  ��    FT  �J    FU  �    FV  �*    FW  ��    FX  �"    FY  ��    F[  ��    F\  ��    F]  ��    F^  ��    F_  ��    F`  �    Fk  �:    Fl  �^    Fn  ��    Fo  ��    Fp  ��    Ft  �    Fu  �    Fv  �>    Fw  �n    Fx  ��    Fy  ��    Fz  ��    F{  ��    F|  �6    F}  �r    F~  ��    F  �    F�  �2    F�  �J    F�  �`    F�  �v    F�  ��    F�  ��    F�  ��    F�  ��    F�  �    F�  �:    F�  �T    F�  �p    F�  ��    F�  ��    F�  ��    F�  �    F�  �    F�  �8    F�  �Z    F�  �x    F�  ��    F�  ��    F�  ��    F�  �    F�  �<    F�  �V    F�  �n    F�  ��    F�  �    F�  �"    F�  �8    F�  �Z    F�  ��    F�  �>    F�  ��    F�  ��    F�      F�  \    F�  �    F�  �    F� >    F� �    F�     F� Z    F� �    F� �    F� �    F�     F�     F� 4    F� L    F� n    F� �    F� �    F� �    F� �    F� �    F� �    F�     F� 4    F� J    F� d    F� ~    F� �    F� �    F� �    F� �    F�     F� :    F� `    F� �    F� �    F� �    F� �    F�     F� 8    F� ^    F� �    F� �    F� 0    F� x    F� �    F� �    H�     H� H    H� �    H� �    � �    �     � 8    � d    � �    � �    '� �    '�     '� J    '� r    'b c    'c d�    'd f�    'l g    '� h�    '� i*    (� i�    FP k�    FQ n�    FR o�    FS t~    FT t�    FU {�    FV |�    FZ ��  #  'S �  #  'T   #  'U f  #  'e �  #  'f   #  'g t  #  'i �  #  'k 4  #  '� �  #  '� �  #  '� J  #  '� �  #  ( �  #  ( J  #  (� �  #  (� �  #  (�  R  #  (�  �  #  (�  �  #  (� !T  #  (� !�  #  (� "   #  (� "V  #  (� "�  #  (� #  #  (� #|  #  (� #�  #  (� $F  #  (� $�  #  )$ %  #  )% %n  #  )& %�  #  )' &(  #  +� &�  #  +� &�  #  +� 'P  #  +� '�  #  +� (  #  +� (h  #  +� (�  #  +� )   #  ,  )H  #  , )�  #  , )�  #  , *   #  , *h  #  , *�  #  , *�  #  , +@  #  , +�  #  ,	 +�  #  ,
 ,   #  , ,h  #  , ,�  #  , ,�  #  , -F  #  , -�  #  , -�  #  , .  #  ,, .N  #  ,- .�  #  ,. /  #  ,/ /�  #  ,0 /�  #  ,1 0N  #  ,2 0�  #  ,3 1  #  ,4 1~  #  ,6 1�  #  ,7 2H  #  ,G 2�  #  ,H 3  #  ,I 3v  #  ,J 3�  #  ,L 4F  #  ,M 4�  #  ,N 5  #  ,O 5�  #  ,Q 5�  #  ,R 6X  #  ,S 6�  #  ,T 7&  #  ,V 7�  #  ,W 7�  #  ,X 8^  #  ,Y 8�  #  ,[ 92  #  ,\ 9�  #  ,] 9�  #  ,^ :b  #  ,_ :�  #  ,` ;*  #  ,a ;�  #  ,b ;�  #  ,c <^  #  ,e <�  #  ,f =(  #  ,g =�  #  ,m =�  #  ,n >L  #  ,o >�  #  ,p ?   #  ,q ?�  #  ,r ?�  #  ,s @4  #  ,t @r  #  ,u @�  $    @�  $  'Q B�  $  'R C�  $  FP D�  $  FQ G  D  (K G�  D  (M O�  D  (P W�  D  (T _�  D  (W b(  T  '� b�  y  (G 	�  y  (H 
  y  <v 0  N4