  �    "J     *�          Latino di base    *�        Latino 1    *�          Latino esteso A   *�          Latino esteso B   *�          Estensioni IPA    *�     0   0Lettere modificatrici di spazio   *�     2   2Segni diacritici di combinazione    *�        Greco di base   *�     &   &Simboli greci e copti   *�        Cirillico   *�        Armeno    *�        Ebraico base    *�          Ebraico esteso    *�        Arabo di base   *�        Arabo esteso    *�        Devanagari    *�        Bengalese   *�        Gurmukhi    *�        Gujarati    *�        Odia    *�        Tamil   *�        Telugu    *�        Kannada   *�        Malayalam   *�        Thai    *�        Lao   *�     "   "Georgiano di base   *�     "   "Georgiano esteso    *�        Hangul Jamo   *�     *   *Latino esteso addizionale   *�        Greco esteso    *�     (   (Punteggiatura generale    *�          Apici e pedici    *�     "   "Simboli di valute   *�     4   4Simboli diacritici di combinazione    *�     &   &Simboli delle lettere   *�     "   "Simboli numerici    *�        Frecce    *�     &   &Operatori matematici    *�        Tecnici misti   *�     "   "Simboli tastiera    *�     4   4Riconoscimento ottico dei caratteri   *�     &   &Alfanumerici inclusi    *�          Disegno caselle   *�        Blocchi   *�     $   $Simboli geometrici    *�        Simboli vari    *�     &   &Caratteri decorativi    *�     ,   ,Simboli e punteggiatura CJK   *�        Hiragana    *�        Katakana    *�        Bopomofo    *�     ,   ,Jamo compatibile con Hangul   *�        Caratteri CJK   *�     .   .Caratteri e mesi CJK inclusi    *�     $   $Compatibilità CJK    *�        Hangul    *�     *   *Ideogramma CJK unificato    +N     6   6Ideogramma CJK unificato estensione A   *�     "   "Area uso privato    *�     ,   ,Ideogrammi compatibili CJK    *�     4   4Forme di presentazione alfabetiche    *�     0   0Forme di presentazione arabe A    *�     .   .Mezze marche di combinazione    *�     &   &Forme compatibili CJK   *�     *   *Piccole varianti di forma   *�     0   0Forme di presentazione arabe B    *�     0   0Forme di spessore pieno/ridotto   *�     "   "Simboli speciali    *�        Sillabe Yi    *�        Radicali Yi   *�          Vecchio corsivo   *�        Gotico    *�        Deseret   *�     ,   ,Simboli musicali bizantini    *�     "   "Simboli musicali    *�     0   0Simboli matematici alfanumerici   *�     6   6Ideogramma CJK unificato estensione B   *�     6   6Ideogramma CJK unificato estensione C   *�     6   6Ideogramma CJK unificato estensione D   *�     :   :Ideogrammi compatibili CJK - Supplemento    *�        Tag   *�     &   &Supplemento cirillico   *�     &   &Selettori di varianti   *�     2   2Area uso privato A supplementare    *�     2   2Area uso privato B supplementare    *�        Limbu   *�        Tai Le    *�        Simboli khmer   *�     &   &Estensioni fonetiche    *�     &   &Simboli e frecce vari   *�     *   *Simboli esagrammi Yijing    *�     &   &Sillabario lineare B    *�     &   &Ideogrammi lineari B    *�        Numeri egei   *�        Ugaritico   *�        Shaviano    *�        Osmanya   +Z        Singalese   +[        Tibetano    +\        Birmano   +]        Khmer   +^        Ogham   +f        Runico    +g        Siriano   +_        Thaana    +h        Etiopico    +i        Cherokee    +`     8   8Sillabe delle lingue autoctone canadesi   +j        Mongolo   +k     .   .Diversi simboli matematici A    +l     &   &Supplementi frecce A    +a        Braille   +m     &   &Supplementi frecce B    +n     .   .Diversi simboli matematici B    +b     ,   ,Radicali supplementari CJK    +o          Radicali kangxi   +p     6   6Caratteri di descrizione ideografica    +q        Tagalog   +r        Hanunoo   +c        Tagbanwa    +t        Buhid   +s        Kanbun    +d          Bopomofo esteso   +e     "   "Fonetica Katakana   *�        Tratti CJK    *�     $   $Sillabario cipriota   *�     &   &Simboli Tai Xuan Jing   *�     4   4Selettori di varianti - supplemento   *�     0   0Notazione musicale Greco antico   *�     &   &Numeri greci antichi    *�     "   "Supplemento arabo   *�        Buginese    +      B   BSupplemento di combinazione dei segni diacritici    +        Copto   +          Etiopico esteso   +     &   &Supplemento etiopico    +     &   &Supplemento georgiano   +        Glagolitico   +        Kharoshthi    +     .   .Lettere modificatrici di toni   +        Nuovo Tai Lue   +	          Persiano antico   +
     4   4Estensioni fonetiche - Supplemento    +     ,   ,Punteggiatura supplementare   +        Syloti Nagri    +        Tifinagh    +          Forme verticali   +        Nko   +        Balinese    +          Latino esteso C   +          Latino esteso D   +        Phags-Pa    +        Fenicio   +        Cuneiforme    +     2   2Numeri e punteggiatura cuneiformi   +          Barre numerali    +        Sundanese   +        Lepcha    +        Ol Chiki    +     $   $Cirillico esteso-A    +        Vai   +     $   $Cirillico esteso-B    +        Saurashtra    +        Kayah Li    +         Rejang    +!        Cham    +"          Simboli arcaici   +#          Disco di Festo    +$        Licio   +%        Cario   +&        Lidio   +'     $   $Tessere del Mahjong   +(     $   $Tessere del domino    +)        Samaritano    +*     4   4Sillabico aborigeno canadese esteso   ++        Tai Tham    +,     $   $Estensioni vediche    +-        Lisu    +.        Bamum   +/     0   0Forme numeriche indiane comuni    +0     "   "Devanagari esteso   +1     &   &Hangul Jamo esteso-A    +2        Giavanese   +3     "   "Myanmar esteso-A    +4        Tai Viet    +5        Meetei Mayek    +6     &   &Hangul Jamo esteso-B    +7     $   $Aramaico imperiale    +8        Sudarabico    +9        Avestico    +:     &   &Partico iscrizionale    +;     &   &Pahlavi iscrizionale    +<        Turco arcaico   +=     &   &Simboli numerici rumi   +>        Kaithi    +?     &   &Geroglifici egiziani    +@     4   4Supplemento alfanumerico racchiuso    +A     2   2Supplemento ideografico racchiuso   +C        Mandaico    +D        Batak   +E     "   "Etiopico esteso-A   +F        Brahmi    +G     "   "Supplemento Bamum   +H     "   "Supplemento Kana    +I          Carte da gioco    +J     ,   ,Simboli e pittogrammi vari    +K        Emoticons   +L     .   .Simboli di mappe e trasporto    +M     "   "Simboli alchemici   +O          Arabo esteso-A    +P     4   4Simboli alfabetici matematici arabi   +Q        Chakma    +R     (   (Estensioni Meetei Mayek   +S     "   "Corsivo meroitico   +T     &   &Geroglifici meroitici   +U        Miao    +V        Sharada   +W        Sora Sompeng    +X     &   &Supplemento sundanese   +Y        Takri   +u        Bassa Vah   +v     $   $Albanese caucasico    +w     $   $Numeri epatta copta   +x     8   8Segni diacritici di combinazione estesi   +y        Duployan    +z        Elbasan   +{     *   *Simboli geometrici estesi   +|        Grantha   +}        Khojki    +~        Khudawadi   +          Latino esteso E   +�        Lineare A   +�        Mahajani    +�        Manicheo    +�        Mende Kikakui   +�        Modi    +�        Mro   +�     "   "Myanmar esteso B    +�        Nabateo   +�        Nordarabico   +�        Abur    +�     2   2Caratteri decorativi ornamentali    +�        Pahawh Hmong    +�        Palmireno   +�        Pau Cin Hau   +�     "   "Salterio Pahlavi    +�     0   0Controlli formati stenografici    +�        Siddham   +�     (   (Numeri arcaici Sinhala    +�     &   &Supplementi frecce C    +�        Tirhuta   +�        Warang Citi   +�        Ahom    +�     &   &Geroglifici anatolici   +�     &   &Supplemento cherokee    +�     6   6Ideogramma CJK unificato estensione E   +�     ,   ,Cuneiforme proto-dinastico    +�     "   "Aramaico di Hatra   +�        Multani   +�     "   "Ungherese arcaico   +�     4   4Simboli e pittogrammi supplementari   +�     (   (Sign Writing di Sutton    (1    �              A sinistra             Interno            A destra             Esterno    !        Al centro            Da sinistra            Dall'interno               Area paragrafo          .   .Area del testo del paragrafo     	     ,   ,Bordo sinistro della pagina         ,   ,Bordo interno della pagina     
     *   *Bordo destro della pagina         ,   ,Bordo esterno della pagina          .   .Bordo sinistro del paragrafo          ,   ,Bordo interno del paragrafo         ,   ,Bordo destro del paragrafo          ,   ,Bordo esterno del paragrafo            Pagina intera         ,   ,Area di testo della pagina             In alto            In basso              Al centro    "        Dall'alto    #        Dal basso    $        Sotto    %        Da destra    &     .   .Bordo superiore della pagina     '     .   .Bordo inferiore della pagina     (     .   .Bordo superiore del paragrafo    )     .   .Bordo inferiore del paragrafo            Margine         .   .Area del testo del paragrafo          .   .Bordo sinistro della cornice          ,   ,Bordo interno della cornice         ,   ,Bordo destro della cornice          ,   ,Bordo esterno della cornice              Cornice intera          ,   ,Area di testo della cornice            Linea di base            Carattere            Riga     *        Riga di testo   (n    	T          �   �Per la lingua scelta non esiste un dizionario dei sinonimi.
Controlla l'installazione e installa la lingua desiderata.         .  .$(ARG1) non è supportato per il controllo ortografico, oppure non è attivato al momento.
Controlla l'installazione e installa, se necessario, la lingua desiderata
oppure imposta il modulo linguistico corrispondente in 'Strumenti - Opzioni - Impostazioni della lingua - Linguistica'.         >   >Il controllo ortografico non è disponibile.         4   4La sillabazione non è disponibile.        L   LIl dizionario personalizzato $(ARG1) non può essere letto.        N   NIl dizionario personalizzato $(ARG1) non può essere creato.         8   8Impossibile trovare l'immagine $(ARG1).        @   @Impossibile caricare un'immagine non collegata.   	     N   NNon è stata impostata la lingua per il termine selezionato.    
     �   �Il livello del modulo non è stato caricato poiché non è stato possibile istanziare i servizi IO necessari (com.sun.star.io.*).   
     �   �Il livello del modulo non è stato scritto poiché non è stato possibile istanziare i servizi IO necessari (com.sun.star.io.*).         �   �Si è verificato un errore nella lettura dei campi di controllo del modulo. Il livello del modulo non è stato caricato.         �   �Si è verificato un errore durante la scrittura dei campi di controllo del modulo. Il livello del modulo non è stato salvato.         z   zSi è verificato un errore durante la lettura di un punto. Non è stato possibile caricare tutti i punti.        �   �Tutte le modifiche apportate al codice Basic Code andranno perdute. Verrà invece salvato il codice Macro VBA originale VBA.         Z   ZIl codice VBA Basic originale contenuto nel documento non verrà salvato.         N   NLa password non è corretta. Impossibile aprire il documento.        �   �Il metodo di cifratura usato in questo documento non è supportato. È supportata solo la cifratura con password compatibile con Microsoft Office 97/2000.         p   pIl caricamento delle presentazioni Microsoft PowerPoint cifrate con password non è supportato.        �   �La protezione con password non è supportata se i documenti sono salvati in formato Microsoft Office.
Vuoi salvare il documento senza la protezione con password?   (o    l           B   B$(ERR) nell'eseguire il dizionario dei sinonimi.          @   @$(ERR) nell'eseguire il controllo ortografico.          6   6$(ERR) nell'eseguire la sillabazione.         2   2$(ERR) nel creare un dizionario.          @   @$(ERR) nel definire un attributo dello sfondo.          2   2$(ERR) nel caricare un'immagine.    *d    p        y       	Impostazione bordo      Bordo sinistro     Bordo destro     Bordo superiore    Bordo inferiore    Bordo orizzontale    Bordo verticale    Bordo diagonale dall'alto a sinistra in basso a destra     Bordo diagonale dal basso a sinistra in alto a destra         y       	Impostazione bordo      Bordo sinistro     Bordo destro     Bordo superiore    Bordo inferiore    Bordo orizzontale    Bordo verticale    Bordo diagonale dall'alto a sinistra in basso a destra     Bordo diagonale dal basso a sinistra in alto a destra            ,   ,       svx/res/frmsel.bmp    FP     T              Tabella            Ricerca            SQL   FQ    @              COME             NON            VUOTO            VERO             FALSO            È             TRA            O    	        E    
        Media            Conteggio            Massimo            Minimo             Somma            Tutti            Uno o più             Alcuni             STDDEV_POP             STDDEV_SAMP            VAR_SAMP             VAR_POP            Unione             Fusione            Intersezione          L   L$(WIDTH) x $(HEIGHT) ($(WIDTH_IN_PX) x $(HEIGHT_IN_PX) px)         2   2$(WIDTH) x $(HEIGHT) a $(DPI) DPI             $(CAPACITY) kiB           Immagine GIF            Immagine JPEG           Immagine PNG            Immagine TIFF           Immagine WMF            Immagine MET    	        Immagine PCT    
        Immagine SVG            Immagine BMP            Sconosciuto   
      $   $oggetto di disegno    
     $   $oggetti di disegno    
     "   "oggetto di gruppo   
     "   "oggetti di gruppo   
     (   (oggetto di gruppo vuoto   
     (   (oggetti di gruppo vuoti   
        Tabella   
        Tabelle   
        Linea   
	     "   "linea orizzontale   

          Linea verticale   
          linea diagonale   
        Linee   
        Rettangolo    
        Rettangoli    
        Quadrato    
        Quadrate    
          Parallelogramma   
          Parallelogrammi   
        Rombo   
        Rombi   
     (   (Rettangolo arrotondato    
     (   (Rettangoli arrotondati    
     &   &quadrato arrotondato    
     &   &Quadrati arrotondati    
     ,   ,Parallelogramma arrotondato   
     ,   ,Parallelogrammi arrotondati   
     "   "rombo arrotondato   
     "   "Rombi arrotondati   
        Cerchio   
        Cerchi    
     $   $Settore di cerchio    
      $   $Settori di cerchio    
!        Arco    
"     "   "Archi di cerchio    
#     $   $Segmento di cerchio   
$     $   $Segmenti di cerchio   
%        Ellisse   
&        Ellissi   
'     $   $Settore di ellisse    
(     $   $Settori di ellisse    
)          Arco ellittico    
*          Archi ellittici   
+     $   $Segmento di ellisse   
,     $   $Segmenti di ellisse   
-        Poligono    
.     &   &Poligono con %2 punti   
/        Poligoni    
0        Polilinea   
1     (   (Polilinea con %2 punti    
2        Polilinee   
3     "   "Curva di Bézier    
4     "   "Curve di Bézier    
5     "   "Curva di Bézier    
6     "   "Curve di Bézier    
7     $   $Linea a mano libera   
8     $   $Linee a mano libera   
9     $   $Linea a mano libera   
:     $   $Linee a mano libera   
;        Curva   
<        Oggetti curva   
=        Curva   
>        Oggetti curva   
?     &   &Curva Spline naturale   
@     &   &Curve Spline naturali   
A     (   (Curva Spline periodica    
B     (   (Curve Spline periodiche   
C     "   "Cornice di testo    
D     "   "Cornice di testo    
E     ,   ,Cornice di testo collegata    
F     ,   ,Cornici di testo collegate    
G     *   *Oggetto di testo adattato   
H     *   *Oggetti di testo adattati   
I     *   *Oggetto di testo adattato   
J     *   *Oggetti di testo adattati   
K     "   "Testo del titolo    
L     "   "Testi del titolo    
M          Testo struttura   
N          Testi struttura   
O        Immagine    
P        Immagini    
Q     $   $Immagine collegata    
R     $   $Immagini collegate    
S     &   &Oggetto grafico vuoto   
T     &   &Oggetti grafici vuoti   
U     0   0Oggetto grafico vuoto collegato   
V     0   0Oggetti grafici vuoti collegati   
W        Metafile    
X        Metafile    
Y     $   $Metafile collegato    
Z     $   $Metafile collegati    
[        Bitmap    
\        Bitmap    
]     "   "Bitmap collegata    
^     "   "Bitmap collegate    
_        Immagine Mac    
`        Immagini Mac    
a     (   (Immagine Mac collegata    
b     (   (Immagini Mac collegate    
c     *   *Oggetto incorporato (OLE)   
d     *   *Oggetti incorporati (OLE)   
e     6   6oggetto collegato e incorporato (OLE)   
f     6   6Oggetti collegati e incorporati (OLE)   
g        Oggetto   
h        Frame   
i        Frame   
j        Frame   
k     &   &Connettori di oggetti   
l     &   &Connettori di oggetti   
m        Legenda   
n        Legende   
o     ,   ,Oggetto anteprima di stampa   
p     ,   ,Oggetti anteprima di stampa   
q     &   &Oggetto di quotatura    
r     &   &Oggetti di quotatura    
s     $   $oggetti di disegno    
t     *   *Nessun oggetto disegnato    
u        e   
v     &   &oggetto(i) di disegno   
w     $   $Campo di controllo    
x     $   $Campi di controllo    
y        Cubo 3D   
z        Cubi 3D   
{     &   &Oggetto di estrusione   
|     &   &Oggetti di estrusione   
}        Testo 3D    
~        Testi 3D    
     &   &oggetto di rotazione    
�     &   &oggetti di rotazione    
�        Oggetto 3D    
�        Oggetti 3D    
�        Poligoni 3D   
�        Scena 3D    
�        Scene 3D    
�        sfera   
�        sfere   
�     (   (Bitmap con trasparenza    
�     2   2Bitmap con trasparenza collegata    
�     (   (Bitmap con trasparenza    
�     2   2Bitmap con trasparenza collegata    
�        Forma   
�        Forme   
�     &   &Oggetto multimediale    
�     &   &Oggetti multimediali    
�        fontwork    
�        fontwork    
�        SVG   
�        SVG   
�        con la copia    
�     6   6Imposta posizione e dimensione per %1   
�        Elimina %1    
�     &   &Sposta %1 più avanti   
�     (   (Sposta %1 più indietro   
�     $   $Sposta %1 in avanti   
�     $   $Sposta %1 indietro    
�     *   *Inverti la sequenza di %1   
�        Sposta %1   
�          Ridimensiona %1   
�        Ruota %1    
�     .   .Rispecchia %1 in orizzontale    
�     ,   ,Rispecchia %1 in verticale    
�     ,   ,Rispecchia %1 in diagonale    
�     *   *Rispecchia %1 liberamente   
�     &   &Distorci %1 (inclina)   
�     &   &Disponi %1 in cerchio   
�     $   $Piega %1 a cerchio    
�        Distorci %1   
�        Annulla %1    
�     6   6Modifica le proprietà Bézier di %1    
�     6   6Modifica le proprietà Bézier di %1    
�        Chiudi %1   
�     ,   ,Imposta allineamento per %1   
�     4   4Imposta l'attributo relativo per %1   
�     4   4Imposta punto di riferimento per %1   
�        Raggruppa %1    
�        Separa %1   
�     (   (Applica attributi a %1    
�     $   $Applica stili a %1    
�     .   .Rimuovi fogli di stile da %1    
�     (   (Converti %1 in poligono   
�     (   (Converti %1 in poligoni   
�     &   &Converti %1 in curva    
�     &   &Converti %1 in curve    
�        Allinea %1    
�     $   $Allinea %1 in alto    
�     $   $Allinea %1 in basso   
�     *   *Centra %1 in orizzontale    
�     &   &Allinea %1 a sinistra   
�     $   $Allinea %1 a destra   
�     (   (Centra %1 in verticale    
�        Centra %1   
�        Trasforma %1    
�        Combina %1    
�        Combina %1    
�        Separa %1   
�        Separa %1   
�        Separa %1   
�     4   4StarDraw Dos Zeichnung importieren    
�     "   "HPGL importieren    
�          DXF importieren   
�     (   (Converti %1 in contorno   
�     (   (Converti %1 in contorni   
�        Unisci %1   
�        Sottrai %1    
�        Interseca %1    
�     :   :Distribuisci gli oggetti della selezione    
�     &   &Uniforma larghezza %1   
�     $   $Uniforma altezza %1   
�          Incolla oggetti   
�        Taglia %1   
�          Incolla Appunti   
�        Drag&Drop %1    
�     "   "Incolla Drag&Drop   
�     &   &Inserisci punto a %1    
�     6   6Inserisci punto di incollaggio in %1    
�     ,   ,Sposta punto di riferimento   
�     *   *Cambia %1 geometricamente   
�        Sposta %1   
�          Ridimensiona %1   
�        Ruota %1    
�     .   .Rispecchia %1 in orizzontale    
�     ,   ,Rispecchia %1 in verticale    
�     ,   ,Rispecchia %1 in diagonale    
�     *   *Rispecchia %1 liberamente   
�     &   &Distorci %1 (inclina)   
�     &   &Disponi %1 in cerchio   
�     $   $Piega %1 a cerchio    
�        Distorci %1   
�     0   0Cambia il raggio d'angolo di %1   
�        Cambia %1   
�          Ridimensiona %1   
�        Sposta %1   
�     "   "Sposta coda di %1   
�     $   $Cambia angolo di %1   
�        Cambia %1   
�     .   .Sfumatura interattiva per %1    
�     0   0Trasparenza interattiva per %1    
�        Ritaglia %1   
�     <   <TextEdit: Paragrafo %1, Riga %2, Colonna %3   
�          %1 selezionato    
�        Punto di %1   
�          %2 punti di %1    
�     ,   ,Punto di incollaggio di %1    
�     .   .%2 punti di incollaggio di %1   
�        Marca oggetti   
�     *   *Marca oggetti aggiuntivi    
�        Marca punti   
�     &   &Marca ulteriori punti   
�     ,   ,Marca punti di incollaggio    
�     6   6Marca ulteriori punti di incollaggio    
�        Crea %1   
�        Inserisci %1    
�        Copia %1    
�     8   8Cambia la sequenza degli oggetti di %1    
�     &   &Modifica testo di %1          "   "Inserisci pagina              Elimina pagina            Copia pagina         0   0Cambia la sequenza delle pagine        *   *Assegna pagina di sfondo         6   6Elimina assegnazione pagina di sfondo        6   6Sposta assegnazione pagina di sfondo         8   8Modifica assegnazione pagina di sfondo         $   $Inserisci documento   	     "   "Inserisci livello   
          Elimina livello        0   0Cambia la sequenza dei livelli         6   6Cambia il nome dell'oggetto di %1 da         4   4Cambia il titolo dell'oggetto di %1        :   :Cambia la descrizione dell'oggetto di %1            Standard            on            off           sì           No            Tipo 1            Tipo 2            Tipo 3            Tipo 4            Orizzontale           Verticale           Automatico            Off           Proporzionale        6   6Adatta (tutte le righe separatamente)        $   $Usa attributi fissi           Sopra            Centro    !        Sotto   "     .   .Utilizzo dell'intera altezza    #        Esteso    $        Sinistra    %        Centro    &        Destra    '     0   0Utilizzo dell'intera larghezza    (        Esteso    )        off   *        lampeggia   +        Scorrimento   ,     "   "alternativamente    -     ,   ,solo scorrimento inserente    .          verso sinistra    /        verso destra    0        verso l'alto    1          verso il basso    2     $   $Connettore standard   3     $   $Connettore lineare    4     $   $Connettore diretto    5     "   "Connettore curvo    6        Standard    7        Raggio    8        automatico    9     (   (a sinistra all'esterno    :     "   "interno (centro)    ;     &   &a destra all'esterno    <        automatico    =        sulla linea   >     "   "linea interrotta    ?          sotto la linea    @     $   $centrato alla linea   A        cerchio pieno   B     $   $Settore di cerchio    C     $   $Segmento di cerchio   D        Arco    E        Ombra   F        Colore ombra    G     .   .Spostamento orizzontale ombra   H     ,   ,Spostamento verticale ombra   I     "   "Trasparenza ombra   J        Ombra 3D    K     &   &Ombra in prospettiva    R          Tipo di legenda   S        Angolo dato   T        Angolo    U        Spazio libero   V     $   $Direzione di uscita   W     .   .Posizione di uscita relativa    X     $   $Posizione di uscita   Y     $   $Posizione di uscita   Z          Lunghezza linea   [     ,   ,Lunghezza automatica linea    c          Raggio d'angolo   d     *   *Distanza sinistra cornice   e     (   (Distanza destra cornice   f     ,   ,Distanza superiore cornice    g     ,   ,Distanza inferiore cornice    h     <   <Adattamento automatico dell'altezza cornice   i     (   (Altezza minima cornice    j     (   (Altezza massima cornice   k     @   @Adattamento automatico della larghezza cornice    l     *   *Larghezza minima cornice    m     *   *Larghezza massima cornice   n     ,   ,Ancoraggio verticale testo    o     .   .Ancoraggio orizzontale testo    p     .   .Adatta il testo alla cornice    q        Rosso   r        Verde   s        Blu   t        Luminosità   u        Contrasto   v        Gamma   w        Trasparenza   x        Inverti   y        Modo grafico    z            {            |            }            ~                        �     "   "Diversi attributi   �     &   &Protezione posizione    �     &   &Protezione dimensione   �        Non stampare    �     &   &Contrassegno livello    �        Livello   �        Nome oggetto    �          Angolo iniziale   �        Angolo finale   �        Posizione X   �        Posizione Y   �        Larghezza   �        Altezza   �     $   $Angolo di rotazione   �     $   $Angolo di apertura    �     &   &Attributo sconosciuto   �        Stile linea   �        Modello linea   �     $   $Spessore del tratto   �        Colore linea    �        Inizio linea    �        Fine linea    �     &   &Spessore inizio linea   �     $   $Spessore fine linea   �     $   $Centra inizio linea   �     "   "Centra fine linea   �     "   "Trasparenza linee   �          Passaggio linea   �     &   &Riservato per linea 2   �     &   &Riservato per linea 3   �     &   &Riservato per linea 4   �     &   &Riservato per linea 5   �     &   &Riservato per linea 6   �          Attributi linea   �     "   "Stile riempimento   �     &   &Colore di riempimento   �        Sfumatura   �        Tratteggio    �     &   &Bitmap di riempimento   �        Trasparenza   �     *   *Numero gradi di sfumatura   �     (   (Riempimento piastrella    �     0   0Posizione bitmap di riempimento   �     0   0Larghezza bitmap di riempimento   �     .   .Altezza bitmap di riempimento   �     &   &Sfumatura trasparente   �     ,   ,Riempimento riservato per 2   �     0   0Dimensione piastrella non in %    �     *   *Spost. piastrella X in %    �     *   *Spost. piastrella Y in %    �     "   "Estensione bitmap   �     (   (Riservato per bitmap 3    �     (   (Riservato per bitmap 4    �     (   (Riservato per bitmap 5    �     (   (Riservato per bitmap 6    �     (   (Riservato per bitmap 7    �     (   (Riservato per bitmap 8    �     ,   ,Posizione piastrella X in %   �     ,   ,Posizione piastrella Y in %   �     $   $Riempimento sfondo    �     .   .Riservato per riempimento 10    �     .   .Riservato per riempimento 11    �     .   .Riservato per riempimento 12    �     *   *Attributi di riempimento    �          Stile Fontwork    �     &   &Allineamento Fontwork   �     "   "Distanza fontwork   �     *   *Inizio carattere Fontwork   �     0   0Carattere rispecchiato Fontwork   �     ,   ,Carattere contorno Fontwork   �          Ombra Fontwork    �     &   &Colore ombra Fontwork   �     (   (Spost. ombra Fontwork X   �     (   (Spost. ombra Fontwork Y   �     ,   ,Nascondi contorno Fontwork    �     ,   ,Trasparenza ombra fontwork    �     *   *Riservato per Fontwork 2    �     *   *Riservato per Fontwork 3    �     *   *Riservato per Fontwork 4    �     *   *Riservato per Fontwork 5    �     *   *Riservato per Fontwork 6    �        Ombra   �        Colore ombra    �     "   "Distanza ombra X    �     "   "Distanza ombra Y    �     "   "Trasparenza ombra   �        Ombra 3D    �     &   &Ombra in prospettiva    �          Tipo di legenda   �     *   *Fissaggio angolo legenda    �          Angolo legenda    �     (   (Distanza linee legenda    �     *   *Direzione uscita legenda    �     (   (Uscita relativa legenda   �     (   (Uscita relativa legenda   �     (   (Uscita assoluta legenda   �     (   (Lunghezza linee legenda   �     :   :Lunghezza automatica delle linee legenda    �          Raggio d'angolo   �     (   (Altezza minima cornice    �     0   0Adattamento altezza automatico    �     .   .Adatta il testo alla cornice    �     0   0Distanza sinistra cornice testo   �     .   .Distanza destra cornice testo   �     2   2Distanza superiore cornice testo    �     2   2Distanza inferiore cornice testo    �     ,   ,Ancoraggio verticale testo    �     (   (Altezza massima cornice   �     *   *Larghezza minima cornice    �     *   *Larghezza massima cornice   �     2   2Adattamento automatico larghezza    �     .   .Ancoraggio orizzontale testo          $   $Scritta scorrevole         .   .Direzione scritta scorrevole         *   *Inizio scritta scorrevole        (   (Fine scritta scorrevole        0   0Numero passi scritta scorrevole        .   .Velocità scritta scorrevole         *   *Passi scritta scorrevole         *   *Flusso testo di contorno         "   "Adattamento forma   	     *   *Attributi personalizzati    
     6   6Interlinea indipendente dal carattere        0   0Scorrimento parole nella forma         D   DAdattamento automatico della forma in base al testo        (   (Riservato per SvDraw 18        (   (Riservato per SvDraw 19             Tipo connettore        *   *Distanza orizz. oggetto 1        *   *Distanza vert. oggetto 1         *   *Distanza orizz. oggetto 2        *   *Distanza vert. oggetto 2         0   0Distanza incollaggio oggetto 1         0   0Distanza incollaggio oggetto 2         $   $Numero linee mobili             Linea spost. 1              Linea spost. 2              Linea spost. 3    $     "   "Tipo di quotatura   %     <   <Testo di quotatura in posizione orizzontale   &     :   :Testo di quotatura in posizione verticale   '     ,   ,Distanza linea di quotatura   (     0   0Sbalzo linee guida di quotatura   )     2   2Distanza linee guida di quotatura   *     2   2Sbalzo linee guida di quotatura 1   +     2   2Sbalzo linee guida di quotatura 2   ,     *   *Quotatura bordi inferiori   -     >   >Testo di quotatura perpendicolare alla linea    .     2   2Ruota testo di quotatura di 180°   /     *   *Sbalzo linee di quotatura   0     "   "Unità di misura    1     .   .Fattore distanza di quotatura   2     2   2Visualizzazione unità di misura    3     ,   ,Formato testo di quotatura    4     B   BPosizionamento automatico del testo di quotatura    5     P   PAngolo per il posizionamento automatico del testo di quotatura    6     B   BDeterminazione dell'angolo del testo di quotatura   7     .   .Angolo del testo di quotatura   8     $   $Posizioni decimali    9     *   *Riservato per quotatura 5   :     *   *Riservato per quotatura 6   ;     *   *Riservato per quotatura 7   =          Tipo di cerchio   >          Angolo iniziale   ?        Angolo fine   @     (   (Riservato per cerchio 0   A     (   (Riservato per cerchio 1   B     (   (Riservato per cerchio 2   C     (   (Riservato per cerchio 3   D     "   "Oggetto visibile    E     0   0Posizione dell'oggetto protetta   F     ,   ,Dimensione oggetto protetta   G     $   $Oggetto stampabile    H        ID livello    I        Livello   J        Nome oggetto    K     &   &Posizione X completa    L     &   &Posizione Y completa    M     $   $Larghezza completa    N     "   "Altezza completa    O     $   $Posizione X singola   P     $   $Posizione Y singola   Q     "   "Larghezza singola   R          Altezza singola   S     "   "Larghezza logica    T          Altezza logica    U     ,   ,Angolo di rotazione singolo   V     .   .Angolo di troncatura singolo    W     &   &Sposta in orizzontale   X     $   $Sposta in verticale   Y     .   .Ridimensiona X singolarmente    Z     .   .Ridimensiona Y singolarmente    [     "   "Rotazione singola   \     *   *Troncatura orizz. singola   ]     *   *Troncatura vert. singola    ^     .   .Ridimensiona X completamente    _     .   .Ridimensiona Y completamente    `        Ruota tutto   a     $   $Tronca tutto orizz.   b     $   $Tronca tutto vert.    c     *   *Punto di riferimento 1 X    d     *   *Punto di riferimento 1 Y    e     *   *Punto di riferimento 2 X    f     *   *Punto di riferimento 2 Y    g        Sillabazione    h     4   4Mostra caratteri per la numerazione   i     *   *Rientri nella numerazione   j     (   (Livello di numerazione    k     ,   ,Elenchi puntati e numerati    l        Rientri   m     $   $Distanza paragrafi    n        Interlinea    o     (   (Allineamento paragrafo    p        Tabulazioni   q     "   "Colore carattere    r     &   &Insieme di caratteri    s        Dimensione    t     $   $Larghezza carattere   u     0   0Grassetto (spessore carattere)    v        Sottolineato    w          Sopralineatura    x        Barrato   y        Corsivo   z     $   $Contorno carattere    {          Ombra carattere   |        Apice/pedice    }        Crenatura   ~     "   "Crenatura manuale        (   (Non sottolineare spazi    �        Tabulazione   �          A capo morbido    �     ,   ,Carattere non convertibile    �     "   "Comandi di campo    �        Rosso   �        Verde   �        Blu   �        Luminosità   �        Contrasto   �        Gamma   �        Trasparenza   �        Inverti   �        Modo grafico    �        Ritaglia    �            �            �            �            �     6   6Utilizza gli attributi della tabella    �     2   2Formattazione automatica tabella    �     "   "Inserisci colonna   �          Inserisci riga    �          Elimina colonna   �        Elimina riga    �        Dividi celle    �        Unisci celle    �          Formatta cella    �     $   $Distribuisci righe    �     &   &Distribuisci colonne    �     "   "Stile di tabella    �     2   2Impostazioni stile della tabella    �     .   .Elimina contenuto della cella   �     >   >Collegamento successivo nella catena di testo   '        [Tutte]   'W     6   6È stata raggiunta la fine del foglio   'Y        Preferito   'Z        X   '[        Y   '\        Z   ']        R:    '^        G:    '_     :   :È stata raggiunta la fine del documento    '`        Includi stili   'a        (Cerca)   'b        (Sostituisci)   'c     *   *Cerca st~ili di paragrafo   'd        B:    'e     &   &Cerca ~stili di cella   'h     $   $Termine non trovato   'i     R   RVuoi annullare il ripristino dati del documento di %PRODUCTNAME?    'j     :   :È stato raggiunto l'inizio del documento   '�        Piena   '�        Sfumatura   '�        Bitmap    '�        Stile linea   '�        Nessuno   '�        Colore    '�        Tratteggio    '�        - nessuno -   '�        Trasparente   '�        Modello   '�        Bordi   '�        Colore bordo    '�        Stile bordo   '�     *   *Colore di evidenziazione    '�     (   (Cancella formattazione    '�     "   "Altre opzioni...    '�     b   bNome del carattere. Il carattere corrente non è disponibile e sarà sostituito.    '�     $   $Nome del carattere    '�        Colore linea    '�          Altri stili...    '�     &   &Colore di riempimento   '�     *   *Altri elenchi numerati...   '�     *   *Altri elenchi puntati...    '�     &   &Tavolozza predefinita   '�     $   $Colori predefiniti    '�     &   &Colore del documento    '�     \   \Modalità Inserimento. Fai clic per passare alla modalità Sovrascrittura.    '�     \   \Modalità Sovrascrittura. Fai clic per passare alla modalità Inserimento.    '�          Sovrascrittura    '�     D   DFirma digitale: la firma del documento è corretta.   '�     x   xFirma digitale: la firma del documento è corretta ma non è stato possibile convalidare i certificati.   '�     �   �Firma digitale: la firma del documento non corrisponde al suo contenuto. Questo documento deve essere considerato inattendibile.    '�     >   >Firma digitale: il documento non è firmato.    '�     �   �Firma digitale: la firma del documento e il certificato sono corretti, ma non tutte le parti del documento sono firmate.    '�     "   "Estremità linea    (        A sinistra    (        A destra    (        Al centro   (        Decimale    (
        Utente    (     *   *Categoria della Galleria    (     *   *Elementi della categoria    (        Anteprima   (        Chiudi    (#        Nero    ($        Blu   (%        Verde   (&        Ciano   ('        Rosso   ((        Magenta   (*        Grigio    (1        Giallo    (2        Bianco    (3        Grigioblu   (4        Arancione   (5        Turchese    (6        Turquoise   (7        Blu classico    (8        Blue classic    (<        Freccia   (=        Quadrato    (>        Cerchio   (A        Trasparenza   (B        Centrato    (C        Non centrato    (D        Lista   (E        Filtro    (�        Trasparente   (�     "   "Colore d'origine    (�     "   "Tavola dei colori   (�        Tolleranza    (�          Sostituisci con   (�          Incolla oggetti   (�     .   .Crea oggetto di rotazione 3D    (�     $   $Numero dei segmenti   (�     $   $Profondità oggetto   (�          Ampiezza focale   (�     "   "Posizione focale    (�     "   "Ruota oggetto 3D    )      0   0Creare un oggetto di estrusione   )     0   0Creare un oggetto di rotazione    )     "   "Separa oggetto 3D   )        Attributi 3D    )        Predefinita   )          Toni di grigio    )        Bianco/Nero   )        Filigrana   )     *   *Intel Indeo Video (*.ivf)   )     *   *Video per Windows (*.avi)   )     *   *Filmato QuickTime (*.mov)   )     J   JMPEG - Motion Pictures Experts Group (*.mpe;*.mpeg;*.mpg)   )         <Tutti>   )!          Inserisci audio   )"          Inserisci video   )#        Sfondo    ),        Lilla   )-        Bordeaux    ).        Giallo chiaro   )/        Verde chiaro    )0        Lilla scuro   )1        Salmone   )2        Blu mare    )4     <   <Verde 1 (Colore principale di %PRODUCTNAME)   )5        Accento verde   )6        Accento blu   )7     "   "Accento arancione   )8        Viola   )9        Accento viola   ):          Accento giallo    )@        3D    )A        Nero 1    )B        Nero 2    )C        Blu   )D        Marrone   )E        Valuta    )F        Valuta 3D   )G        Valuta grigio   )H          Valuta lavanda    )I          Valuta turchese   )J        Grigio    )K        Verde   )L        Lavanda   )M        Rosso   )N        Turchese    )O        Giallo    )Z     $   $Estremità, piatta    )[     (   (Estremità, arrotondata   )\     &   &Estremità, quadrata    )]          Giuntura, media   )^          Giuntura, unita   )_     "   "Giuntura, secante   )`     "   "Giuntura, rotonda   )c        Black   )d        Blue    )e        Green   )f        Cyan    )g        Red   )h        Magenta   )j        Gray    )q        Yellow    )r        White   )s        Blue gray   )t        Orange    )u        Violet    )v        Bordeaux    )w        Pale yellow   )x        Pale green    )y        Dark violet   )z        Salmon    ){        Sea blue    )|        Sun   )}        Grafico   )~        Chart   )        Viola   )�        Purple    )�        Cielo azzurro   )�        Sky blue    )�        Gialloverde   )�        Yellow green    )�        Rosa    )�        Pink    )�     2   2Green 1 (%PRODUCTNAME Main Color)   )�        Green Accent    )�        Blue Accent   )�        Orange Accent   )�        Purple    )�        Purple Accent   )�        Yellow Accent   )�        Tango: Burro    )�          Tango: Arancio    )�     "   "Tango: Cioccolato   )�     "   "Tango: Camaleonte   )�     &   &Tango: Cielo azzurro    )�        Tango: Prugna   )�     (   (Tango: Rosso scarlatto    )�     "   "Tango: Alluminio    )�        Tango: Butter   )�        Tango: Orange   )�     "   "Tango: Chocolate    )�     "   "Tango: Chameleon    )�          Tango: Sky Blue   )�        Tango: Plum   )�     $   $Tango: Scarlet Red    )�     "   "Tango: Aluminium    )�     &   &Black 45 Degrees Wide   )�     "   "Black 45 Degrees    )�     "   "Black -45 Degrees   )�     "   "Black 90 Degrees    )�     (   (Red Crossed 45 Degrees    )�     &   &Red Crossed 0 Degrees   )�     (   (Blue Crossed 45 Degrees   )�     (   (Blue Crossed 0 Degrees    )�     (   (Blue Triple 90 Degrees    )�          Black 0 Degrees   )�        Hatching    )�     &   &Nero 45 gradi, ampio    )�        Nero 45 gradi   )�          Nero -45 gradi    )�        Nero 90 gradi   )�     &   &Rosso a rete 45 gradi   )�     &   &Rosso a rete 0 gradi    )�     $   $Blu a rete 45 gradi   )�     $   $Blu a rete 0 gradi    )�     *   *Blu tripla rete 90 gradi    )�        Nero 0 gradi    )�        Tratteggio    )�        Empty   )�        Sky   )�        Aqua    )�        Coarse    )�        Space Metal   )�        Space   )�        Metal   )�        Wet   )�        Marble    )�        Linen   )�        Stone   )�        Pebbles   )�        Wall    )�        Red Wall    )�        Pattern   )�        Leaves    )�          Lawn Artificial   )�        Daisy   )�        Orange    )�        Fiery   )�        Roses   )�        Bitmap    )�        Vuoto   )�        Cielo   )�        Acqua   )�        Grana grossa    )�        Mercurio    )�        Spazio    )�        Metallo   )�        Gocce   )�        Marmo   )�        Lino    )�        Pietra    )�        Ghiaia    )�        Muro    )�        Mattoni   )�        Rete    )�        Foglie    )�     "   "Prato artificiale   )�        Margherite    )�        Arancione   )�        Fiamma    )�        Rose    )�        Bitmap    )�     "   "Ultrafine Dashed    )�        Fine Dashed   )�     *   *Ultrafine 2 Dots 3 Dashes   )�        Fine Dotted   )�     $   $Line with Fine Dots   )�     "   "Fine Dashed (var)   )�     &   &3 Dashes 3 Dots (var)   )�     (   (Ultrafine Dotted (var)    )�        Line Style 9    )�        2 Dots 1 Dash   )�        Dashed (var)    )�        Dash    *         Line Style    *     $   $A tratti finissimi    *        A tratti fini   *     "   "A 2 punti 3 linee   *        A punti fini    *     &   &Linea con punti fini    *        A tratti fini   *     "   "A 3 linee 3 punti   *     "   "A punti finissimi   *	        Stile linea 9   *
     "   "A 2 punti 1 linea   *        Tratteggiato    *        A tratti    *        Stile linea   *2     $   $Stampa la selezione   *3     B   BVuoi stampare la selezione o l'intero documento?    *4        ~Tutto    *5        ~Selezione    *D        Ritaglia    *E        Modo immagine   *F     &   &Percentuale di rosso    *G     &   &Percentuale di verde    *H     $   $Percentuale di blu    *I        Luminosità   *J        Contrasto   *K        Gamma   *L        Trasparenza   *Y        Automatico    *^     .   .Azioni da annullare: $(ARG1)    *_     0   0Azioni da ripristinare: $(ARG1)   *`     .   .Azioni da annullare: $(ARG1)    *a     0   0Azioni da ripristinare: $(ARG1)   *b        Transparency    *c        Trasparenza   *m     $   $Colore materiale 3D   *n     "   "Colore carattere    *o     "   "Colore di sfondo    *p        Nessuno   *q        Pieno   *r        Tratteggiato    *s        Sfumatura   *t        Bitmap    *u        con   *v        Stile   *w        e   *�     "   "Controllo spigolo   *�     4   4Selezione di un punto dello spigolo   *�     "   "Controllo angolo    *�     6   6Selezione di un angolo (principale).    *�     $   $A sinistra in alto    *�     "   "Al centro in alto   *�     "   "A destra in alto    *�     &   &In centro a sinistra    *�        Centro    *�     $   $In centro a destra    *�     $   $A sinistra in basso   *�     $   $In centro in basso    *�     "   "A destra in basso   *�        0 gradi   *�        45 gradi    *�        90 gradi    *�        135 gradi   *�        180 gradi   *�        225 gradi   *�        270 gradi   *�        315 gradi   *�     $   $Controllo contorno    *�     "   "Modifica contorno   *�     .   .Selezione carattere speciale    *�     "   "Codice carattere    *�     D   DSeleziona il carattere speciale in questa sezione.    *�     &   &Esportazione immagine   *�        Estrusione    *�        Fontwork    *�     "   "Colore estrusione   *�        ~0 cm   *�        ~1 cm   *�        ~2,5 cm   *�        ~5 cm   *�        10 ~cm    *�        0 pollici   *�        0,~5 pollici    *�        ~1 pollice    *�        ~2 pollici    *�        ~4 pollici    *�        Pagine    +n     (   (Applica forma fontwork    +o     <   <Applica lettere di uguale altezza fontwork    +p     .   .Applica allineamento fontwork   +r     6   6Applica spaziatura caratteri fontwork   +s     *   *Applica estrusione sì/no   +t     "   "Inclina in basso    +u          Inclina in alto   +v     $   $Inclina a sinistra    +w     "   "Inclina a destra    +x     0   0Modifica profondità estrusione   +y     $   $Cambia orientamento   +z     *   *Cambia tipo di proiezione   +{     &   &Cambia illuminazione    +|     $   $Cambia luminosità    +}     0   0Cambia superficie di estrusione   +~     ,   ,Cambia colore di estrusione   +�     .   .Punti circolari piccoli pieni   +�     .   .Punti circolari grandi pieni    +�     (   (Punti pieni a diamante    +�     ,   ,Punti quadrati grandi pieni   +�     4   4Punti a freccia pieni verso destra    +�     .   .Punti a freccia verso destra    +�     (   (Punti a segno di spunta   +�     .   .Punti a segno di graduazione    +�          Numero 1) 2) 3)   +�          Numero 1. 2. 3.   +�     $   $Numero (1) (2) (3)    +�     4   4Numero romano maiuscolo I. II. III.   +�     ,   ,Lettera maiuscola A) B) C)    +�     ,   ,Lettera minuscola a) b) c)    +�     .   .Lettera minuscola (a) (b) (c)   +�     4   4Numero romano minuscolo i. ii. iii.   +�     N   NPunti circolari piccoli pieni con lettere minuscole e numeri    +�     N   NPunti circolari piccoli pieni con lettere minuscole e numeri    +�     z   zPunti circolari piccoli pieni con lettere maiuscole, numeri romani minuscoli, lettere minuscole e numeri    +�        Numerico    +�     �   �Punti circolari piccoli pieni con lettere minuscole, numeri romani minuscoli, lettere maiuscole e numeri romani maiuscoli   +�     �   �Punti circolari piccoli pieni con numeri romani minuscoli, lettere minuscole, numeri romani maiuscoli e lettere maiuscole   +�     2   2Numerico con tutti i sottolivelli   +�     �   �Punti rivolti verso destra, punti con freccia verso destra, punti pieni a diamante, punti circolari piccoli pieni   +�     6   6Stili per la struttura delle tabelle    +�     &   &Colore del carattere    +�     <   <Cerca stringa di visualizzazione formattata   +�     $   $Maiuscole/minuscole   +�        Trova   +�     \   \Il documento è stato modificato. Fai doppio clic per salvare il documento.   +�     N   NIl documento non è stato modificato dall'ultimo salvataggio.   +�     0   0Il documento viene caricato...    +�     $   $Password non valida   +�     .   .Le password non corrispondono   +�     <   <Adatta la diapositiva alla finestra attiva.   ,     ,   ,Ripristinato correttamente    ,     2   2Documento originale ripristinato    ,     (   (Ripristino non riuscito   ,     $   $Ripristino in corso   ,     (   (Non ancora ripristinato   ,     �   �%PRODUCTNAME %PRODUCTVERSION inizierà il ripristino dei tuoi documenti. A seconda della loro dimensione, questa procedura può richiedere alcuni minuti.   ,     l   lIl ripristino dei documenti è terminato.
Fai clic su 'Fine' per visualizzare i documenti.    ,        ~Fine   ,     �   �Livello di zoom. Fai clic col pulsante destro del mouse per modificare il livello dell'ingrandimento, o fai clic per aprire la finestra di dialogo Zoom.    ,        Ingrandisci   ,        Riduci    ,     "   "~Personalizza...    ,         ~Infinito   ,!        Fil di ferro    ,"        ~Opaco    ,#        ~Plastica   ,$        Me~tallico    ,%          ~Molto stretto    ,&        ~Stretto    ,'        ~Normale    ,(        ~Largo    ,)        Molto ~largo    ,*     "   "~Personalizza...    ,+     0   0~Crenatura coppia di caratteri    ,8     &   &Estrusione nord-ovest   ,9          Estrusione nord   ,:     $   $Estrusione nord-est   ,;     "   "Estrusione ovest    ,<     (   (Estrusione all'indietro   ,=          Estrusione est    ,>     &   &Estrusione sud-ovest    ,?          Estrusione sud    ,@     $   $Estrusione sud-est    ,B        ~Prospettiva    ,C        P~arallelo    ,D        ~Luminoso   ,E        ~Normale    ,F        ~Oscurato   ,h     $   $~Allinea a sinistra   ,i        ~Centrale   ,j     "   "~Allinea a destra   ,k     $   $Giustifica ~parole    ,l     &   &Giustifica ~caratteri   ,v        25%   ,w        50%   ,x        75%   ,y        100%    ,z        150%    ,{        200%    ,|        Pagina intera   ,}     "   "Larghezza pagina    ,~          Vista ottimale    .�        Gradient    .�     "   "Linear blue/white   .�     &   &Linear magenta/green    .�     $   $Linear yellow/brown   .�     $   $Radial green/black    .�     "   "Radial red/yellow   .�     &   &Rectangular red/white   .�     $   $Square yellow/white   .�     0   0Ellipsoid blue grey/light blue    .�     &   &Axial light red/white   .�        Diagonal 1l   .�        Diagonal 1r   .�        Diagonal 2l   .�        Diagonal 2r   .�        Diagonal 3l   .�        Diagonal 3r   .�        Diagonal 4l   .�        Diagonal 4r   .�        Diagonal Blue   .�          Diagonal Green    .�          Diagonal Orange   .�        Diagonal Red    .�     $   $Diagonal Turquoise    .�          Diagonal Violet   .�        From a Corner   .�     $   $From a Corner, Blue   .�     &   &From a Corner, Green    .�     &   &From a Corner, Orange   .�     $   $From a Corner, Red    .�     *   *From a Corner, Turquoise    .�     &   &From a Corner, Violet   .�          From the Middle   /      &   &From the Middle, Blue   /     (   (From the Middle, Green    /     (   (From the Middle, Orange   /     &   &From the Middle, Red    /     ,   ,From the Middle, Turquoise    /     (   (From the Middle, Violet   /        Horizontal    /          Horizontal Blue   /     "   "Horizontal Green    /	     "   "Horizontal Orange   /
          Horizontal Red    /     &   &Horizontal Turquoise    /     "   "Horizontal Violet   /        Radial    /        Radial Blue   /        Radial Green    /        Radial Orange   /        Radial Red    /     "   "Radial Turquoise    /        Radial Violet   /        Vertical    /        Vertical Blue   /          Vertical Green    /          Vertical Orange   /        Vertical Red    /     $   $Vertical Turquoise    /          Vertical Violet   /        Gray Gradient   /          Yellow Gradient   /          Orange Gradient   /        Red Gradient    /        Pink Gradient   /         Sky   /!        Cyan Gradient   /"        Blue Gradient   /#        Purple Pipe   /$        Night   /%          Green Gradient    /&        Tango Green   /'     $   $Subtle Tango Green    /(        Tango Purple    /)        Tango Red   /*        Tango Blue    /+        Tango Yellow    /,        Tango Orange    /-        Tango Gray    /.        Argile    //        Olive Green   /0        Grison    /1        Sunburst    /2        Brownie   /3        Sun-Set   /4        Deep Green    /5        Deep Orange   /6        Deep Blue   /7        Purple Haze   /D        Sfumatura   /E     $   $Lineare blu/bianco    /F     &   &Lineare magenta/verde   /G     (   (Lineare giallo/marrone    /H     $   $Radiale verde/nero    /I     &   &Radiale rosso/giallo    /J     *   *Rettangolare rosso/bianco   /K     (   (Quadrato giallo/bianco    /L     0   0Ellissoidale blu-grigio/celeste   /M     ,   ,Assiale rosso chiaro/bianco   /N        Diagonale 1s    /O        Diagonale 1d    /P        Diagonale 2s    /Q        Diagonale 2d    /R        Diagonale 3s    /S        Diagonale 3d    /T        Diagonale 4s    /U        Diagonale 4d    /V        Diagonale blu   /W          Diagonale verde   /X     $   $Diagonale arancione   /Y          Diagonale rosso   /Z     $   $Diagonale turchese    /[          Diagonale viola   /\        Da un angolo    /]     "   "Da un angolo, blu   /^     $   $Da un angolo, verde   /_     (   (Da un angolo, arancione   /`     $   $Da un angolo, rosso   /a     (   (Da un angolo, turchese    /b     $   $Da un angolo, lilla   /c        Dal centro    /d          Dal centro, blu   /e     "   "Dal centro, verde   /f     &   &Dal centro, arancione   /g     "   "Dal centro, rosso   /h     &   &Dal centro, turchese    /i     "   "Dal centro, lilla   /j        Orizzontale   /k          Orizzontale blu   /l     "   "Orizzontale verde   /m     &   &Orizzontale arancione   /n     "   "Orizzontale rossa   /o     &   &Orizzontale turchese    /p     "   "Orizzontale lilla   /q        Radiale   /r        Radiale blu   /s        Radiale verde   /t     "   "Radiale arancione   /u        Radiale rosso   /v     "   "Radiale turchese    /w        Radiale lilla   /x        Verticale   /y        Verticale blu   /z          Verticale verde   /{     $   $Verticale arancione   /|          Verticale rosso   /}     $   $Verticale turchese    /~          Verticale lilla   /     "   "Sfumatura grigia    /�     "   "Sfumatura gialla    /�     $   $Sfumatura arancione   /�          Sfumatura rossa   /�          Sfumatura rosa    /�        Cielo   /�          Sfumatura ciano   /�        Sfumatura blu   /�        Tubo violetto   /�        Notte   /�          Sfumatura verde   /�        Tango Verde   /�     $   $Tango Verde sfumato   /�        Tango Viola   /�        Tango Rosso   /�        Tango Blu   /�        Tango Giallo    /�          Tango Arancione   /�        Tango Grigio    /�        Argilla   /�        Verde oliva   /�        Argento   /�          Raggio di sole    /�        Brownie   /�        Tramonto    /�        Verde scuro   /�          Arancione scuro   /�        Blu scuro   /�        Foschia viola   1�        Arrow concave   1�        Square 45   1�        Small Arrow   1�          Dimension Lines   1�        Double Arrow    1�     $   $Rounded short Arrow   1�          Symmetric Arrow   1�        Line Arrow    1�     $   $Rounded large Arrow   1�        Circle    1�        Square    1�        Arrow   1�     "   "Short line Arrow    1�     "   "Triangle unfilled   1�     "   "Diamond unfilled    1�        Diamond   1�          Circle unfilled   1�     $   $Square 45 unfilled    1�          Square unfilled   1�     &   &Half Circle unfilled    1�        Arrowhead   1�          Freccia concava   1�        Quadrato 45   1�          Freccia piccola   1�     &   &Estremità quotatura    1�          Freccia doppia    1�     *   *Freccia corta arrotondata   1�     $   $Freccia simmetrica    1�        Linea freccia   1�     *   *Freccia lunga arrotondata   2         Cerchio   2        Quadrato    2        Freccia   2     $   $Linea freccia corta   2          Triangolo vuoto   2        Losanga vuota   2        Losanga   2        Cerchio vuoto   2     "   "Quadrato 45 vuoto   2	          Quadrato vuoto    2
     "   "Semicerchio vuoto   2     "   "Estremità linea    :h     "   "Colore carattere    :p        Cerca   :q        Trova tutto   :r        Sostituisci   :s     "   "Sostituisci tutto   :t     $   $Stile di carattere    :u     $   $Stile di paragrafo    :v     "   "Stile di cornice    :w          Stile di pagina   :z        Formula   :{        Valore    :|        Nota    :        StarWriter    :�        StarCalc    :�        StarDraw    :�        StarBase    :�        Nessuno   :�        Pieno   :�        Orizzontale   :�        Verticale   :�        Griglia   :�        Losanga   :�        Diagonale su    :�          Diagonale giù    :�        25%   :�        50%   :�        75%   :�        Immagine    <      *   *Orientamento predefinito    <     $   $Dall'alto in basso    <     "   "Dal basso in alto   <        Impilato    <!        Tabella   <"        Non tabella   <#     $   $Distanza abilitata    <$     &   &Distanza disabilitata   <%     .   .Brevi distanze non consentite   <&     *   *Brevi distanze consentite   <F     $   $Margine sinistro:     <G     $   $Margine superiore:    <H     "   "Margine destro:     <I     $   $Margine inferiore:    <X     ,   ,Descrizione della pagina:     <Y        Maiuscole   <Z        Minuscole   <[     "   "Romano maiuscolo    <\     "   "Romano minuscolo    <]        Arabo   <^        Nessuno   <_        Orizzontale   <`        Verticale   <a        Sinistra    <b        Destra    <c        Tutto   <d        Rispecchiato    <o        Autore:     <p        Data:     <q        Testo:    <r     $   $Colore di sfondo:     <s          Colore motivo:    <u     "   "Sfondo carattere    C     "   "Tavola dei colori   C        Cambia    FQ     L   LIl nome '%1' non è valido in XML. Indica un nome diverso.    FR     V   VIl prefisso '%1' non è valido in XML. Inserisci un prefisso diverso.   FS     D   DIl nome '%1' esiste già. Inserisci un nuovo nome.    FT     ,   ,L'invio deve avere un nome.   FU     �   �L'eliminazione dell'associazione '$BINDINGNAME' ha effetto su tutti i controlli a essa associati.

Vuoi davvero eliminarla?   FV     �   �L'eliminazione dell'invio '$SUBMISSIONNAME' ha effetto su tutti i controlli a esso associati.

Vuoi davvero eliminarlo?   FW     F   FVuoi davvero eliminare l'attributo '$ATTRIBUTENAME'?    FX     �   �L'eliminazione dell'elemento '$ELEMENTNAME' ha effetto su tutti i controlli a esso associati.
Vuoi davvero eliminarlo?    FY     �   �L'eliminazione dell'istanza '$INSTANCENAME' ha effetto su tutti i controlli a essa associati.
Vuoi davvero eliminarla?    F[        Formulario    F\          Record di dati    F]        da    F^     2   2Impostazione della proprietà '#'   F_     *   *Inserisci nel contenitore   F`        Elimina #   Fk     "   "Elimina oggetti #   Fl     4   4Sostituisci un elemento contenitore   Fn     "   "Elimina struttura   Fo     0   0Sostituisci campo di controllo    Fp     &   &Barra di navigazione    Ft        Formulario    Fu          Aggiungi campo:   Fv     L   LNessun elemento di controllo selezionato o selezione mista    Fw        Proprietà:     Fx     &   &Proprietà formulario   Fy     &   &Navigatore formulario   Fz        Formulari   F{     >   >Errore nella scrittura dei dati nel database    F|     4   4Intendi eliminare 1 record di dati.   F}     l   lSe scegli Sì, non sarà più possibile annullare l'operazione.
Vuoi continuare lo stesso?    F~     $   $Elemento di cornice   F        Navigazione   F�        Colonna   F�        Data    F�        Ora   F�     &   &Barra di navigazione    F�        Pulsante    F�     $   $Pulsante di scelta    F�     &   &Casella di controllo    F�        Testo fisso   F�     "   "Casella di gruppo   F�          Campo di testo    F�     &   &Casella di riepilogo    F�     "   "Casella combinata   F�     "   "Pulsante immagine   F�     ,   ,Campo di controllo immagine   F�        Scelta file   F�        Campo data    F�        Campo ora   F�          Campo numerico    F�          Campo di valuta   F�     "   "Campo a maschera    F�     ,   ,Campo di controllo tabella    F�     $   $Selezione multipla    F�     4   4Intendi eliminare # record di dati.   F�     &   &Elemento di controllo   F�         (Data)   F�         (Ora)    F�     L   LNon esistono controlli connessi a un campo tabella valido!    F�     "   "Navigatore filtro   F�        Filtra per    F�        Oppure    F�     "   "Campo formattato    F�     @   @Errore nell'analizzare l'espressione di ricerca   F�     �   �L'eliminazione del modello '$MODELNAME' ha effetto su tutti i controlli a esso associati.
Vuoi davvero eliminarlo?    F�     z   zNel formulario attuale non esistono controlli legati in modo valido da poter usare per la vista tabella.    F�     $   $<Campo automatico>    F�     8   8Errore di sintassi nell'espressione SQL   F�     >   >Il valore #1 non può essere usato con COME.    F�     B   BCOME non può essere utilizzato in questo campo!    F�     T   TIl criterio specificato non può essere paragonato a questo campo.    F�     B   BIl campo non può essere paragonato a un intero.    F�     v   vIl valore indicato non è una data valida. Indica la data in un formato valido, ad esempio: GG/MM/AA.   F�     T   TIl campo non può essere paragonato a un numero in virgola mobile.    F�     F   FIl database non contiene una tabella denominata "#".    F�     H   HLa colonna "#1" risulta sconosciuta alla tabella "#2".    F�     &   &Barra di scorrimento    F�     &   &Pulsante di selezione   F�     $   $Controllo nascosto    F�        Elemento    F�        Attributo   F�        Associazione    F�     (   (Espressione vincolante    F�        Post    F�        Put   F�        Get   F�        Nessuno   F�        Istanza   F�        Documento   F�          Navigatore dati   F�        Invio:    F�        ID:     F�        Operazione:     F�        Metodo:     F�        Riferimento:    F�          Associazione:     F�        Sostituisci:    F�     "   "Aggiungi elemento   F�     "   "Modifica elemento   F�     "   "Elimina elemento    F�     $   $Aggiungi attributo    F�     $   $Modifica attributo    F�     "   "Elimina attributo   F�     &   &Aggiungi associazione   F�     &   &Modifica associazione   F�     &   &Elimina associazione    F�          Aggiungi invio    F�          Modifica invio    F�        Elimina invio   F�     T   TIl database non contiene una tabella o una ricerca denominate "#".    F�     R   RIl database contiene già una tabella o una vista denominate "#".   F�     F   FIl database contiene già una ricerca denominata "#".   F�           (sola lettura)   F�     6   6Il file esiste già. Sovrascriverlo?    F�     $   $Etichetta #object#    H�     <   <Errore durante la creazione del formulario    H�     <   <La voce esiste già.
Scegli un altro nome.    H�     R   RNel campo '#' è necessario immettere dei dati. Digita un valore.   H�     z   zSeleziona un elemento dell'elenco oppure inserisci un testo corrispondente a uno degli elementi elencati.   (G  y   �   �   Millimetro     Centimetro     Metro    Chilometro     Pollice    Piede    	Miglia     
Pica     Punto    Caratt.    Riga       (H  y  
<  
<   NEuropa occidentale (Windows-1252/WinLatin 1)     Europa occidentale (Apple Macintosh)     Europa occidentale (DOS/OS2-850/Internazionale)    Europa occidentale (DOS/OS2-437/US)    Europa occidentale (DOS/OS2-860/Portoghese)    Europa occidentale (DOS/OS2-861/Islandese)     Europa occidentale (DOS/OS2-863/Francese(Can.))    Europa occidentale (DOS/OS2-865/Nordico)     Europa occidentale (ASCII/US)    Europa occidentale (ISO-8859-1)    Europa occidentale (ISO-8859-2)    Latino 3 (ISO-8859-3)    Paesi baltici (ISO-8859-4)     Cirillico (ISO-8859-5)     Arabo (ISO-8859-6)     Greco (ISO-8859-7)     Ebraico (ISO-8859-8)     Turco (ISO-8859-9)     Europa occidentale (ISO-8859-14)     Europa occidentale (ISO-8859-15/EURO)    Greco (DOS/OS2-737)    Paesi baltici (DOS/OS2-775)    Europa orientale (DOS/OS2-852)     Cirillico (DOS/OS2-855)    Turco (DOS/OS2-857)    Ebraico (DOS/OS2-862)    Arabo (DOS/OS2-864)    Cirillico (DOS/OS2-866/Russo)    Greco (DOS/OS2-869/Moderno)    Europa orientale (Windows-1250/WinLatin 2)     !Cirillico (Windows-1251)     "Greco (Windows-1253)     #Turco (Windows-1254)     $Ebraico (Windows-1255)     %Arabo (Windows-1256)     &Paesi baltici (Windows-1257)     'Vietnamita (Windows-1258)    (Europa orientale (Apple Macintosh)     *Europa orientale (Apple Macintosh/Croato)    +Cirillico (Apple Macintosh)    ,Greco (Apple Macintosh)    /Europa occidentale (Apple Macintosh/Islandese)     3Europa orientale (Apple Macintosh/Rumeno)    4Turco (Apple Macintosh)    6Cirillico (Apple Macintosh/Ucraino)    7Cinese semplificato (Apple Macintosh)    8Cinese tradizionale (Apple Macintosh)    9Giapponese (Apple Macintosh)     :Coreano (Apple Macintosh)    ;Giapponese (Windows-932)     <Cinese semplificato (Windows-936)    =Coreano (Windows-949)    >Cinese tradizionale (Windows-950)    ?Giapponese (Shift-JIS)     @Cinese semplificato (GB-2312)    ACinese semplificato (GB-18030)     UCinese tradizionale (GBT-12345)    BCinese semplificato (GBK/GB-2312-80)     CCinese tradizionale (Big5)     DCinese tradizionale (BIG5-HKSCS)     VGiapponese (EUC-JP)    ECinese semplificato (EUC-CN)     FCinese tradizionale (EUC-TW)     GGiapponese (ISO-2022-JP)     HCinese semplificato (ISO-2022-CN)    ICirillico (KOI8-R)     JUnicode (UTF-7)    KUnicode (UTF-8)    LEuropa occidentale (ISO-8859-10)     MEuropa occidentale (ISO-8859-13)     NCoreano (EUC-KR)     OCoreano (ISO-2022-KR)    PCoreano (Windows-Johab-1361)     TUnicode (UTF-16)    ��Thai (ISO-8859-11/TIS-620)     WThai (Windows-874)      Cirillico (KOI8-U)     XCirillico (PT154)    ]  <v  y  f  f   ;Zoom    'Pennello    'Tabulazioni   'Carattere   'Tipo di carattere   'Orientamento dei caratteri    'Spessore del carattere    'Ombreggiato   'Solo parole   'Contorno    'Barrato   'Sottolineato    'Dimensioni carattere    'Dimens. carattere relat.    ' Colore carattere    '!Crenatura   '"Effetti   '#Lingua    '$Posizione   '%Carattere lampeggiante    'SColore insieme di caratteri   *}Sopralineatura    -0Paragrafo   '*Allineamento    '+Interlinea    '1Interruzione di pagina    '5Sillabazione    '6Non dividere il paragrafo   '7Orfane    '8Vedove    '9Spaziatura del paragrafo    ':Rientro del paragrafo   ';Rientro   '@Distanza    'APagina    'BStile di pagina   'QMantieni assieme i paragrafi    'RLampeggiante    (�Conformità registro    (�Sfondo carattere    )_Caratteri asiatici    *�Dimensione del carattere asiatico   *�Lingua del carattere asiatico   *�Orientamento del carattere asiatico   *�Spessore del carattere asiatico   *�Disposiz. testo complesso   *�Dimensione degli script complessi   *�Lingua degli script complessi   *�Orientamento degli script complessi   *�Spessore degli script complessi   *�A riga doppia   *�Punti esclamativi   *�Spaziatura testo    *�Punteggiatura sospesa   *�Caratteri non ammessi   *�Rotazione   *�Ridimensionamento proporzionale del carattere   *�Rilievo   *�Allineamento verticale del testo    *�  �     *   *      res/grafikei.png    �     *   *      res/grafikde.png    �     ,   ,      svx/res/markers.png   �     4   4      svx/res/pageshadow35x35.png   �     (   (      res/oleobj.png    �     0   0      svx/res/cropmarkers.png   '�     .   .      svx/res/rectbtns.png    '�     ,   ,      svx/res/objects.png   '�     (   (      	svx/res/ole.png   '�     ,   ,      
svx/res/graphic.png   'S  #   f   f            8   8      svx/res/slidezoombutton_10.png              ��  ��      'T  #   b   b            4   4      svx/res/slidezoomout_10.png             ��  ��      'U  #   b   b            4   4      svx/res/slidezoomin_10.png              ��  ��      'e  #   V   V            (   (      res/sc10223.png             ��  ��      'f  #   V   V            (   (      res/sc10224.png             ��  ��      'g  #   `   `            2   2      svx/res/signet_11x16.png              ��  ��      'i  #   `   `            2   2      svx/res/caution_11x16.png             ��  ��      'k  #   d   d            6   6      svx/res/notcertificate_16.png             ��  ��      '�  #   Z   Z            ,   ,      svx/res/lighton.png             ��  ��      '�  #   X   X            *   *      svx/res/light.png             ��  ��      '�  #   \   \            .   .      svx/res/colordlg.png              ��  ��      '�  #   H   H            4   4      svx/res/selection_10x22.png   (  #   \   \            .   .      svx/res/notcheck.png              ��  ��      (  #   \   \            .   .      svx/res/lngcheck.png              ��  ��      (�  #   V   V            (   (      res/sc10865.png             ��  ��      (�  #   V   V            (   (      res/sc10866.png             ��  ��      (�  #   V   V            (   (      res/sc10867.png             ��  ��      (�  #   V   V            (   (      res/sc10863.png             ��  ��      (�  #   V   V            (   (      res/sc10864.png             ��  ��      (�  #   V   V            (   (      res/sc10868.png             ��  ��      (�  #   V   V            (   (      res/sc10869.png             ��  ��      (�  #   V   V            (   (       res/reload.png              ��  ��      (�  #   Z   Z            ,   ,      !svx/res/reloads.png             ��  ��      (�  #   h   h            :   :      "svx/res/extrusioninfinity_16.png              ��  ��      (�  #   d   d            6   6      #svx/res/extrusion0inch_16.png             ��  ��      (�  #   f   f            8   8      $svx/res/extrusion05inch_16.png              ��  ��      (�  #   d   d            6   6      %svx/res/extrusion1inch_16.png             ��  ��      (�  #   d   d            6   6      &svx/res/extrusion2inch_16.png             ��  ��      (�  #   d   d            6   6      'svx/res/extrusion4inch_16.png             ��  ��      )$  #   `   `            2   2      (svx/res/wireframe_16.png              ��  ��      )%  #   \   \            .   .      )svx/res/matte_16.png              ��  ��      )&  #   ^   ^            0   0      *svx/res/plastic_16.png              ��  ��      )'  #   \   \            .   .      +svx/res/metal_16.png              ��  ��      +�  #   f   f            8   8      ,svx/res/doc_modified_yes_14.png             ��  ��      +�  #   f   f            8   8      -svx/res/doc_modified_no_14.png              ��  ��      +�  #   h   h            :   :      .svx/res/doc_modified_feedback.png             ��  ��      +�  #   f   f            8   8      /svx/res/zoom_page_statusbar.png             ��  ��      +�  #   J   J            6   6      0svx/res/symphony/spacing3.png   +�  #   P   P            <   <      1svx/res/symphony/Indent_Hanging.png   +�  #   H   H            4   4      2svx/res/symphony/blank.png    +�  #   H   H            4   4      3svx/res/symphony/width1.png   ,   #   H   H            4   4      4svx/res/symphony/width2.png   ,  #   H   H            4   4      5svx/res/symphony/width3.png   ,  #   H   H            4   4      6svx/res/symphony/width4.png   ,  #   H   H            4   4      7svx/res/symphony/width5.png   ,  #   H   H            4   4      8svx/res/symphony/width6.png   ,  #   H   H            4   4      9svx/res/symphony/width7.png   ,  #   H   H            4   4      :svx/res/symphony/width8.png   ,  #   H   H            4   4      ;svx/res/symphony/axial.png    ,  #   L   L            8   8      <svx/res/symphony/ellipsoid.png    ,	  #   L   L            8   8      =svx/res/symphony/Quadratic.png    ,
  #   H   H            4   4      >svx/res/symphony/radial.png   ,  #   H   H            4   4      ?svx/res/symphony/Square.png   ,  #   H   H            4   4      @svx/res/symphony/linear.png   ,  #   N   N            :   :      Asvx/res/symphony/rotate_left.png    ,  #   N   N            :   :      Bsvx/res/symphony/rotate_right.png   ,  #   >   >            *   *      Csvx/res/nu01.png    ,  #   >   >            *   *      Dsvx/res/nu04.png    ,  #   >   >            *   *      Esvx/res/nu02.png    ,,  #   h   h            :   :      Fsvx/res/directionnorthwest_22.png             ��  ��      ,-  #   d   d            6   6      Gsvx/res/directionnorth_22.png             ��  ��      ,.  #   h   h            :   :      Hsvx/res/directionnortheast_22.png             ��  ��      ,/  #   d   d            6   6      Isvx/res/directionwest_22.png              ��  ��      ,0  #   h   h            :   :      Jsvx/res/directionstraight_22.png              ��  ��      ,1  #   d   d            6   6      Ksvx/res/directioneast_22.png              ��  ��      ,2  #   h   h            :   :      Lsvx/res/directionsouthwest_22.png             ��  ��      ,3  #   d   d            6   6      Msvx/res/directionsouth_22.png             ��  ��      ,4  #   h   h            :   :      Nsvx/res/directionsoutheast_22.png             ��  ��      ,6  #   b   b            4   4      Osvx/res/perspective_16.png              ��  ��      ,7  #   ^   ^            0   0      Psvx/res/parallel_16.png             ��  ��      ,G  #   j   j            <   <      Qsvx/res/lightofffromtopleft_22.png              ��  ��      ,H  #   f   f            8   8      Rsvx/res/lightofffromtop_22.png              ��  ��      ,I  #   j   j            <   <      Ssvx/res/lightofffromtopright_22.png             ��  ��      ,J  #   f   f            8   8      Tsvx/res/lightofffromleft_22.png             ��  ��      ,L  #   h   h            :   :      Usvx/res/lightofffromright_22.png              ��  ��      ,M  #   l   l            >   >      Vsvx/res/lightofffrombottomleft_22.png             ��  ��      ,N  #   h   h            :   :      Wsvx/res/lightofffrombottom_22.png             ��  ��      ,O  #   n   n            @   @      Xsvx/res/lightofffrombottomright_22.png              ��  ��      ,Q  #   h   h            :   :      Ysvx/res/lightonfromtopleft_22.png             ��  ��      ,R  #   d   d            6   6      Zsvx/res/lightonfromtop_22.png             ��  ��      ,S  #   j   j            <   <      [svx/res/lightonfromtopright_22.png              ��  ��      ,T  #   f   f            8   8      \svx/res/lightonfromleft_22.png              ��  ��      ,V  #   f   f            8   8      ]svx/res/lightonfromright_22.png             ��  ��      ,W  #   l   l            >   >      ^svx/res/lightonfrombottomleft_22.png              ��  ��      ,X  #   h   h            :   :      _svx/res/lightonfrombottom_22.png              ��  ��      ,Y  #   l   l            >   >      `svx/res/lightonfrombottomright_22.png             ��  ��      ,[  #   f   f            8   8      asvx/res/lightfromtopleft_22.png             ��  ��      ,\  #   b   b            4   4      bsvx/res/lightfromtop_22.png             ��  ��      ,]  #   h   h            :   :      csvx/res/lightfromtopright_22.png              ��  ��      ,^  #   d   d            6   6      dsvx/res/lightfromleft_22.png              ��  ��      ,_  #   d   d            6   6      esvx/res/lightfromfront_22.png             ��  ��      ,`  #   d   d            6   6      fsvx/res/lightfromright_22.png             ��  ��      ,a  #   j   j            <   <      gsvx/res/lightfrombottomleft_22.png              ��  ��      ,b  #   f   f            8   8      hsvx/res/lightfrombottom_22.png              ��  ��      ,c  #   j   j            <   <      isvx/res/lightfrombottomright_22.png             ��  ��      ,e  #   `   `            2   2      jsvx/res/brightlit_16.png              ��  ��      ,f  #   `   `            2   2      ksvx/res/normallit_16.png              ��  ��      ,g  #   \   \            .   .      lsvx/res/dimlit_16.png             ��  ��      ,m  #   h   h            :   :      msvx/res/fontworkalignleft_16.png              ��  ��      ,n  #   l   l            >   >      nsvx/res/fontworkaligncentered_16.png              ��  ��      ,o  #   h   h            :   :      osvx/res/fontworkalignright_16.png             ��  ��      ,p  #   l   l            >   >      psvx/res/fontworkalignjustified_16.png             ��  ��      ,q  #   j   j            <   <      qsvx/res/fontworkalignstretch_16.png             ��  ��      ,r  #   >   >            *   *      rsvx/res/fw018.png   ,s  #   >   >            *   *      ssvx/res/fw019.png   ,t  #   >   >            *   *      tsvx/res/fw016.png   ,u  #   >   >            *   *      usvx/res/fw017.png      $  �  �   id              ��  ��       svx/res/id01.png     svx/res/id02.png     svx/res/id03.png     svx/res/id04.png     svx/res/id05.png     svx/res/id06.png     svx/res/id07.png     svx/res/id08.png     svx/res/id030.png    svx/res/id031.png    svx/res/id032.png     svx/res/id033.png    !svx/res/id040.png    (svx/res/id041.png    )svx/res/id016.png    svx/res/id018.png    svx/res/id019.png       'Q  $  @  @   fr              ��  ��       svx/res/fr01.png     svx/res/fr02.png     svx/res/fr03.png     svx/res/fr04.png     svx/res/fr05.png     svx/res/fr06.png     svx/res/fr07.png     svx/res/fr08.png     svx/res/fr09.png     	svx/res/fr010.png    
svx/res/fr011.png    svx/res/fr012.png       'R  $   �   �   da              ��  ��       res/da01.png     res/da02.png     res/da03.png     res/da04.png     res/da05.png     res/da06.png        FP  $  �  �   sx              �   �        res/sx10594.png   )bres/sx10595.png   )cres/sx10596.png   )dres/sx10597.png   )eres/sx10598.png   )fres/sx10599.png   )gres/sx10600.png   )hres/sx10601.png   )ires/sx10607.png   )ores/sx10144.png   '�res/sx10593.png   )ares/sx18013.png   F]res/sx18002.png   FRres/sx18003.png   FSres/sx10604.png   )lres/sx10605.png   )mres/sx10704.png   )�res/sx10705.png   )�res/sx10706.png   )�res/sx10707.png   )�res/sx10708.png   )�res/sx18022.png   Ffres/sx10710.png   )�res/sx10603.png   )kres/sx10715.png   )�res/sx10728.png   )�res/sx10757.png   *res/sx18027.png   Fkres/sx10768.png   *res/sx10769.png   *   FQ  $   �   �   tb              ��  ��       res/tb01.png     res/tb02.png     res/tb03.png     res/tb04.png     res/tb05.png        (K  D     (   8            ?   �     D   h   h  @�    �  SVX_HID_PPROPERTYPANEL_TEXT_UNDERLINE_VS                 9   xUnderline      F   \   \  @�       SVX_HID_UNDERLINE_BTN          }      9   ~Altre opzioni...    
  #   H   H            4   4      vsvx/res/symphony/line1.png       #   H   H            4   4      wsvx/res/symphony/line2.png       #   H   H            4   4      xsvx/res/symphony/line3.png       #   H   H            4   4      ysvx/res/symphony/line4.png       #   H   H            4   4      zsvx/res/symphony/line5.png       #   H   H            4   4      {svx/res/symphony/line6.png       #   H   H            4   4      |svx/res/symphony/line7.png       #   H   H            4   4      }svx/res/symphony/line8.png       #   H   H            4   4      ~svx/res/symphony/line9.png       #   H   H            4   4      svx/res/symphony/line10.png    (  #   P   P            <   <      �svx/res/symphony/selected-line1.png    )  #   P   P            <   <      �svx/res/symphony/selected-line2.png    *  #   P   P            <   <      �svx/res/symphony/selected-line3.png    +  #   P   P            <   <      �svx/res/symphony/selected-line4.png    ,  #   P   P            <   <      �svx/res/symphony/selected-line5.png    -  #   P   P            <   <      �svx/res/symphony/selected-line6.png    .  #   P   P            <   <      �svx/res/symphony/selected-line7.png    /  #   P   P            <   <      �svx/res/symphony/selected-line8.png    0  #   P   P            <   <      �svx/res/symphony/selected-line9.png    1  #   R   R            >   >      �svx/res/symphony/selected-line10.png     2        (Senza)            Singola            Doppia             Grassetto            Punto         "   "Punto (grassetto)            Tratto             Tratto lungo             Punto tratto          $   $Punto punto tratto             Onda    (M  D  <   (   8            V   �     D   n   n  @�    �  SVX_HID_PPROPERTYPANEL_TEXT_SPACING_VS                 P   ZCharacter Spacing      W   D   D   �      @          _      H   Personalizzato:      W   L   L   �      @          n      >   ~Spaziatura caratteri:       U   ~   ~  @?     SVX_HID_SPACING_CB_KERN          x      >   P��   Predefinito     Espanso     Condensato           W   F   F   �      @          �      >   Modificato ~di:      	  d   h   h  @?     `SVX_HID_SPACING_MB_KERN          �      >          '             
     #   P   P            <   <      �svx/res/symphony/spacing_normal.png       #   T   T            @   @      �svx/res/symphony/spacing_very tight.png    !  #   P   P            <   <      �svx/res/symphony/spacing_tight.png     "  #   P   P            <   <      �svx/res/symphony/spacing_loose.png     #  #   T   T            @   @      �svx/res/symphony/spacing_very loose.png    3  #   R   R            >   >      �svx/res/symphony/spacing_normal_s.png    4  #   V   V            B   B      �svx/res/symphony/spacing_very tight_s.png    5  #   R   R            >   >      �svx/res/symphony/spacing_tight_s.png     6  #   R   R            >   >      �svx/res/symphony/spacing_loose_s.png     7  #   V   V            B   B      �svx/res/symphony/spacing_very loose_s.png    $  #   T   T            @   @      �svx/res/symphony/last_custom_common.png    %  #   Z   Z            F   F      �svx/res/symphony/last_custom_common_grey.png     =        Molto stretto    >        Stretto    ?        Normale    @        Largo    A        Molto largo    B     .   .Ultimo valore personalizzato     C     2   2 Spaziatura: Condensata di: 3 pt     D     4   4 Spaziatura: Condensata di: 1.5 pt     E     &   & Spaziatura: Normale     F     .   . Spaziatura: Espansa di: 3 pt    G     .   . Spaziatura: Espansa di: 6 pt    H     .   . Spaziatura: Condensata di:      I     *   * Spaziatura: Espansa di:     J        pt    (P  D  L                	  W   4   4   �              -   	Centra ~X:     
  d      B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_CENTER_X       -   Specifica la percentuale di spostamento orizzontale dal centro per lo stile di sfumatura dell'ombreggiatura. 50% è il centro orizzontale.          d             d        W   4   4   �              4   	Centra ~Y:       d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_CENTER_Y       -   Specifica la percentuale di spostamento verticale dal centro per lo stile di sfumatura dell'ombreggiatura. 50% è il centro verticale.          d             d        W   2   2   �              d   	~Angolo:       d   �   �  B8     ` SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_ANGLE        -   Specifica l'angolo di rotazione per lo stile di sfumatura dell'ombreggiatura.      ����  '       gradi             W   :   :   �              -   	~Valore iniziale:      d      B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_SVALUE       -   Inserisci un valore di trasparenza per il punto iniziale della sfumatura, compreso tra 0% (totalmente opaco) e 100% (totalmente trasparente).         d             d        W   :   :   �              4   	~Valore ~finale:       d      B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_EVALUE       -   Inserisci un valore di trasparenza per il punto finale della sfumatura, compreso tra 0 % (totalmente opaco) e 100% (totalmente trasparente).          d             d        W   0   0   �              d   	~Bordo:      d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_BORDER       -   Specifica il valore del bordo della trasparenza della sfumatura.          d             d        q   �   �  @�      @SVX_HID_PPROPERTYPANEL_AREA_BTN_LEFT_SECOND Rotate Left        �         p           Rotate Left      q   �   �  @�      @SVX_HID_PPROPERTYPANEL_AREA_BTN_RIGHT_FIRST Rotate Right         �         p   "   "   Rotate Right          8   8Ruota in senso antiorario di 45 gradi.          4   4Ruota in senso orario di 45 gradi.    (T  D  �   (   8            V   �     D   `   `  @�    �  SVX_HID_PPROPERTYPANEL_LINE_VS_WIDTH                 P   lWidth      W   D   D   �                 q      P   Personalizzato:      W   F   F   �                 �      @   ~Spessore linea:       d   �   �  B?     aSVX_HID_PPROPERTYPANEL_LINE_MTR_WIDTH          �      (   Specifica lo spessore della linea.       6  �                �   
     #   T   T            @   @      �svx/res/symphony/last_custom_common.png      #   Z   Z            F   F      �svx/res/symphony/last_custom_common_grey.png          .   .Ultimo valore personalizzato     	        pt    (W  D   �                    W   �   �   �                       d   Le proprietà per l'attività che stai eseguendo non sono disponibili per la selezione corrente   '�  T   D   D  @8    �  SVX_HID_STYLE_LISTBOX       <   V        'b    �  �               B   B   J   Pagina intera SVX_HID_MNU_ZOOM_WHOLE_PAGE          F   F   J   Larghezza pagina  SVX_HID_MNU_ZOOM_PAGE_WIDTH          B   B   J   Vista ottimale  SVX_HID_MNU_ZOOM_OPTIMAL           0   0   J   50% SVX_HID_MNU_ZOOM_50          0   0   J   75% SVX_HID_MNU_ZOOM_75          4   4   J   100%  SVX_HID_MNU_ZOOM_100           4   4   J   150%  SVX_HID_MNU_ZOOM_150           0   0   H200%  SVX_HID_MNU_ZOOM_200    'c    �  �               8   8   HValore medio  SVX_HID_MNU_FUNC_AVG           >   >   J   Conta valori  SVX_HID_MNU_FUNC_COUNT2          >   >   J   Conta numeri  SVX_HID_MNU_FUNC_COUNT           6   6   J   Massimo SVX_HID_MNU_FUNC_MAX           6   6   J   Minimo  SVX_HID_MNU_FUNC_MIN           4   4   J   	Somma SVX_HID_MNU_FUNC_SUM           N   N   J   Conteggio selezione SVX_HID_MNU_FUNC_SELECTION_COUNT           6   6   J   Nessuna SVX_HID_MNU_FUNC_NONE   'd     R   R               :   :   HFirme digitali... SVX_HID_XMLSEC_CALL   'l    �  �               $   $      Millimetro           (   (         Centimetro           "   "         Metro          (   (         Chilometro           $   $         Pollice          "   "      	   Piede          $   $      
   Miglia           "   "         Punto          "   "         Pica           "   "         Car.           "   "         Riga    '�     ~   ~               <   <   
  '�Aggiorna per abbinare la selezione           *   *   
  '�Modifica stile...   '�     �   �               ,   ,      Selezione standard           .   .         Selezione estesa           4   4         Selezione con aggiunta           0   0         Selezione a blocchi   (�    �  �               $   $   Descrizione...           "   "   
   
~Macro...                  
   	Attivo                           �   �  
   Disponi          �   �               .   .   
   Porta in primo piano           ,   ,   
   Porta più ~avanti           .   .   
   Porta più i~ndietro           (   (   
   Porta in ~fondo                          *   *   
   Seleziona ~tutto           "   "   
   ~Elimina    FP                       �   �  J  )�~Nuovo  SVX_HID_FM_NEW           �   �               8   8   J  )�Formulario  SVX_HID_FM_NEW_FORM          B   B   J  )pControllo nascosto  SVX_HID_FM_NEW_HIDDEN          @   @   J  )�Sostituisci con .uno:ChangeControlType           *   *  
  N~Taglia .uno:Cut           *   *  
  OC~opia  .uno:Copy          .   .  
  P~Incolla  .uno:Paste           4   4   J  )�~Elimina  SVX_HID_FM_DELETE          D   D   J  )wSequenza di attivazione...  .uno:TabDialog           <   <   J  )q~Rinomina SVX_HID_FM_RENAME_OBJECT           >   >   J  )�Propr~ietà .uno:ShowPropertyBrowser           >   >   J  )�Apri nel modo bozza .uno:OpenReadOnly          T   T   J  *Punto focale automatico di controllo  .uno:AutoControlFocus   FQ     �   �               <   <   J  )rElimina righe SVX_HID_FM_DELETEROWS          4   4   J  )�Salva record  .uno:RecSave           @   @   J  )�Annulla: Immissione dati  .uno:RecUndo    FR    �  �              �  �  J  )s~Inserisci colonna  SVX_HID_FM_INSERTCOL          �  �               2   2   J  )gCampo di testo  .uno:Edit          <   <   J  )dCasella di controllo  .uno:CheckBox          8   8   J  )iCasella combinata .uno:ComboBox          <   <   J  )hCasella di riepilogo  .uno:ListBox           4   4   J  )�Campo data  .uno:DateField           6   6   J  )�Campo orario  .uno:TimeField           :   :   J  )�Campo numerico  .uno:NumericField          <   <   J  )�Campo di valuta .uno:CurrencyField           <   <   J  )�Campo a maschera  .uno:PatternField          >   >   J  )�Campo formattato  .uno:FormattedField          L   L  J  *Campo data e orario SVX_HID_CONTROLS_DATE_N_TIME            @   @   J  )n~Sostituisci con  SVX_HID_FM_CHANGECOL           >   >   J  )tElimina colonna SVX_HID_FM_DELETECOL           >   >   J  *Nascondi colonna  SVX_HID_FM_HIDECOL           �   �  J  *~Mostra colonne SVX_HID_FM_SHOWCOLS          �   �               <   <   J  *~Altro... SVX_HID_FM_SHOWCOLS_MORE                           8   8   J  *~Tutte  SVX_HID_FM_SHOWALLCOLS           >   >   J  )�~Colonna... .uno:ShowPropertyBrowser    FS     B   B               *   *  
  OC~opia  .uno:Copy   FT    �  �               P   P  J  )�Campo di ~testo .uno:ConvertToEdit  .uno:ConvertToEdit           N   N  J  )�~Pulsante .uno:ConvertToButton  .uno:ConvertToButton           N   N  J  )�Testo ~fisso  .uno:ConvertToFixed .uno:ConvertToFixed          T   T  J  )�Casella di ~gruppo  .uno:ConvertToGroup .uno:ConvertToGroup          V   V  J  )�~Casella di riepilogo .uno:ConvertToList  .uno:ConvertToList           ^   ^  J  )�Casella di controllo  .uno:ConvertToCheckBox  .uno:ConvertToCheckBox           T   T  J  )�Pulsante di scelta  .uno:ConvertToRadio .uno:ConvertToRadio          T   T  J  )�Casella com~binata  .uno:ConvertToCombo .uno:ConvertToCombo          \   \  J  )�P~ulsante immagine  .uno:ConvertToImageBtn  .uno:ConvertToImageBtn           Z   Z  J  )�Scelta ~file  .uno:ConvertToFileControl .uno:ConvertToFileControl          L   L  J  )�Campo ~data .uno:ConvertToDate  .uno:ConvertToDate           N   N  J  )�Campo ~orario .uno:ConvertToTime  .uno:ConvertToTime           T   T  J  )�Campo ~numerico .uno:ConvertToNumeric .uno:ConvertToNumeric          Z   Z  J  )�Campo di ~valuta  .uno:ConvertToCurrency  .uno:ConvertToCurrency           V   V  J  )�Campo a ~maschera .uno:ConvertToPattern .uno:ConvertToPattern          n   n  J  )�Campo di controllo ~immagine  .uno:ConvertToImageControl  .uno:ConvertToImageControl           Z   Z  J  )�Campo fo~rmattato .uno:ConvertToFormatted .uno:ConvertToFormatted          ^   ^  J  *Barra di scorrimento  .uno:ConvertToScrollBar .uno:ConvertToScrollBar          b   b  J  *Pulsante di selezione .uno:ConvertToSpinButton  .uno:ConvertToSpinButton           f   f  J  *Barra di navigazione  .uno:ConvertToNavigationBar .uno:ConvertToNavigationBar   FU     �   �               4   4   J  )�~Elimina  SVX_HID_FM_DELETE          2   2   J  *~Modifica SVX_HID_FM_EDIT          <   <   J  *È ~vuoto SVX_HID_FM_FILTER_IS_NULL          >   >   J  *~Non è vuoto SVX_HID_FM_IS_NOT_NULL    FV                     <   <  
  'Tipo di carattere .uno:CharFontName          4   4  
  'Dimensione  .uno:FontHeight         0  0  J  FWSt~ile  SVX_HID_MENU_FM_TEXTATTRIBUTES_STYLE          �  �      
         ,   ,  
  'Grassetto .uno:Bold          ,   ,  
  'Corsivo .uno:Italic          6   6  
  -0Sopralineatura  .uno:Overline          6   6  
  'Sottolineato  .uno:Underline           0   0  
  'Barrato .uno:Strikeout           2   2  
  'Ombreggiato .uno:Shadowed          4   4  
  'Contorno  .uno:OutlineFont                           2   2  
  (6A~pice  .uno:SuperScript           0   0  
  (7~Pedice .uno:SubScript          J  J  N  FX   ~Allineamento SVX_HID_MENU_FM_TEXTATTRIBUTES_ALIGNMENT           �   �               6   6    ',   A ~sinistra .uno:LeftPara          6   6    '-   A ~destra .uno:RightPara           6   6    '.   ~Centrato .uno:CenterPara          <   <    '/   Giustificato  .uno:JustifyPara              J  FY~Interlinea SVX_HID_MENU_FM_TEXTATTRIBUTES_SPACING           �   �               4   4    '2   Singola .uno:SpacePara1          8   8    '3   1,5 righe .uno:SpacePara15           4   4    '4   ~Doppia .uno:SpacePara2   FZ    �  �               F   F   J   
Aggiungi voce SVX_HID_XFORMS_TOOLBOX_ITEM_ADD          R   R   J   Aggiungi elemento SVX_HID_XFORMS_TOOLBOX_ITEM_ADD_ELEMENT          V   V   J   Aggiungi attributo  SVX_HID_XFORMS_TOOLBOX_ITEM_ADD_ATTRIBUTE          D   D   J   Modifica  SVX_HID_XFORMS_TOOLBOX_ITEM_EDIT                           D   D   J   Elimina SVX_HID_XFORMS_TOOLBOX_ITEM_REMOVE     �          �     � ��    �        (1  "J    (n  ($    (o  1x    *d  2�    FP  5T    FQ  5�       7�      84      8f      8�      8�      8�      8�      8�      9    	  9:    
  9X      9v      9�    
   9�    
  9�    
  9�    
  :    
  :<    
  :d    
  :�    
  :�    
  :�    
	  :�    

  :�    
  ;    
  ;4    
  ;J    
  ;f    
  ;�    
  ;�    
  ;�    
  ;�    
  ;�    
  <    
  <"    
  <J    
  <r    
  <�    
  <�    
  <�    
  =    
  =8    
  =Z    
  =r    
  =�    
   =�    
!  =�    
"  =�    
#  >
    
$  >.    
%  >R    
&  >j    
'  >�    
(  >�    
)  >�    
*  >�    
+  ?
    
,  ?.    
-  ?R    
.  ?l    
/  ?�    
0  ?�    
1  ?�    
2  ?�    
3  @    
4  @*    
5  @L    
6  @n    
7  @�    
8  @�    
9  @�    
:  @�    
;  A     
<  A6    
=  AT    
>  Aj    
?  A�    
@  A�    
A  A�    
B  A�    
C  B$    
D  BF    
E  Bh    
F  B�    
G  B�    
H  B�    
I  C    
J  C>    
K  Ch    
L  C�    
M  C�    
N  C�    
O  C�    
P  D    
Q  D     
R  DD    
S  Dh    
T  D�    
U  D�    
V  D�    
W  E    
X  E.    
Y  EH    
Z  El    
[  E�    
\  E�    
]  E�    
^  E�    
_  F    
`  F"    
a  F@    
b  Fh    
c  F�    
d  F�    
e  F�    
f  G    
g  GP    
h  Gh    
i  G~    
j  G�    
k  G�    
l  G�    
m  G�    
n  H    
o  H&    
p  HR    
q  H~    
r  H�    
s  H�    
t  H�    
u  I    
v  I*    
w  IP    
x  It    
y  I�    
z  I�    
{  I�    
|  I�    
}  J    
~  J.    
  JH    
�  Jn    
�  J�    
�  J�    
�  J�    
�  J�    
�  K    
�  K    
�  K2    
�  KH    
�  Kp    
�  K�    
�  K�    
�  K�    
�  L    
�  L(    
�  LN    
�  Lt    
�  L�    
�  L�    
�  L�    
�  L�    
�  L�    
�  M$    
�  M@    
�  Mf    
�  M�    
�  M�    
�  M�    
�  N     
�  N    
�  N:    
�  NT    
�  N�    
�  N�    
�  N�    
�  O    
�  O*    
�  OP    
�  Ot    
�  O�    
�  O�    
�  O�    
�  P    
�  P2    
�  P^    
�  P�    
�  P�    
�  P�    
�  P�    
�  Q&    
�  QJ    
�  Qx    
�  Q�    
�  Q�    
�  Q�    
�  R    
�  R0    
�  RT    
�  Rx    
�  R�    
�  R�    
�  R�    
�  S    
�  S.    
�  SL    
�  Sh    
�  S�    
�  S�    
�  S�    
�  S�    
�  T    
�  T(    
�  TH    
�  Tp    
�  T�    
�  T�    
�  T�    
�  T�    
�  U&    
�  UL    
�  Up    
�  U�    
�  U�    
�  U�    
�  U�    
�  V
    
�  V0    
�  Vf    
�  V�    
�  V�    
�  V�    
�  V�    
�  W    
�  W>    
�  Wj    
�  W�    
�  W�    
�  W�    
�  X    
�  X0    
�  XL    
�  X|    
�  X�    
�  X�    
�  X�    
�  X�    
�  Y    
�  Y0    
�  Y^    
�  Y�    
�  Y�    
�  Y�    
�  Z    
�  Z"    
�  ZB    
�  Zn    
�  Z�    
�  Z�    
�  Z�    
�  [     
�  [&    
�  [R    
�  [�    
�  [�    
�  [�    
�  [�    
�  \       \6      \X      \x      \�      \�      \�      ]&      ]\      ]�    	  ]�    
  ]�      ]�      ^*      ^`      ^�      ^�      ^�      ^�      _      _$      _8      _P      _h      _�      _�      _�      _�      _�      _�      `      `R      `v       `�    !  `�    "  `�    #  `�    $  a     %  a    &  a2    '  aJ    (  az    )  a�    *  a�    +  a�    ,  a�    -  a�    .  b*    /  bJ    0  bh    1  b�    2  b�    3  b�    4  b�    5  c    6  c4    7  cN    8  cf    9  c�    :  c�    ;  c�    <  c�    =  d    >  d*    ?  dL    @  dl    A  d�    B  d�    C  d�    D  d�    E  e    F  e"    G  e@    H  en    I  e�    J  e�    K  e�    R  e�    S  f    T  f8    U  fP    V  fn    W  f�    X  f�    Y  f�    Z  g    [  g(    c  gT    d  gt    e  g�    f  g�    g  g�    h  h    i  hZ    j  h�    k  h�    l  h�    m  i    n  i>    o  ij    p  i�    q  i�    r  i�    s  i�    t  j    u  j"    v  j<    w  jR    x  jn    y  j�    z  j�    {  j�    |  j�    }  j�    ~  j�      j�    �  k    �  k2    �  kX    �  k~    �  k�    �  k�    �  k�    �  k�    �  l    �  l6    �  lR    �  ln    �  l�    �  l�    �  l�    �  l�    �  m    �  m*    �  mH    �  ml    �  m�    �  m�    �  m�    �  m�    �  n    �  n2    �  nT    �  nv    �  n�    �  n�    �  n�    �  o    �  o.    �  oT    �  ot    �  o�    �  o�    �  o�    �  o�    �  p    �  p4    �  p^    �  p�    �  p�    �  p�    �  q    �  q:    �  qf    �  q�    �  q�    �  q�    �  r    �  r4    �  r\    �  r�    �  r�    �  r�    �  r�    �  s(    �  sT    �  sx    �  s�    �  s�    �  t    �  t,    �  tL    �  tr    �  t�    �  t�    �  t�    �  u    �  u:    �  u`    �  u�    �  u�    �  u�    �  v    �  v2    �  v\    �  v�    �  v�    �  v�    �  v�    �  w    �  w0    �  wR    �  wt    �  w�    �  w�    �  w�    �  w�    �  x    �  xF    �  xp    �  x�    �  x�    �  x�    �  y    �  yJ    �  yj    �  y�    �  y�    �  y�    �  z     �  zN    �  z�    �  z�    �  z�    �  {    �  {0    �  {Z    �  {�       {�      {�      |      |6      |^      |�      |�      |�      }    	  }2    
  }\      }�      }�      ~      ~.      ~V      ~v      ~�      ~�      ~�            N      ~      �      �      �    $  �    %  �$    &  �`    '  ��    (  ��    )  ��    *  �(    +  �Z    ,  ��    -  ��    .  ��    /  �&    0  �P    1  �r    2  ��    3  ��    4  ��    5  �@    6  ��    7  ��    8  �     9  �$    :  �N    ;  �x    =  ��    >  ��    ?  ��    @  ��    A  �&    B  �N    C  �v    D  ��    E  ��    F  ��    G  �    H  �@    I  �\    J  �t    K  ��    L  ��    M  ��    N  �    O  �$    P  �H    Q  �l    R  ��    S  ��    T  ��    U  ��    V  �    W  �J    X  �p    Y  ��    Z  ��    [  ��    \  �    ]  �<    ^  �f    _  ��    `  ��    a  ��    b  �    c  �&    d  �P    e  �z    f  ��    g  ��    h  ��    i  �     j  �J    k  �r    l  ��    m  ��    n  ��    o  ��    p  �    q  �:    r  �\    s  ��    t  ��    u  ��    v  ��    w  �    x  �0    y  �H    z  �`    {  ��    |  ��    }  ��    ~  ��      ��    �  �&    �  �B    �  �b    �  ��    �  ��    �  ��    �  ��    �  ��    �  �    �  �&    �  �<    �  �X    �  �p    �  ��    �  ��    �  ��    �  ��    �  ��    �  ��    �  �&    �  �X    �  �z    �  ��    �  ��    �  ��    �  ��    �  �    �  �4    �  �X    �  �~    �  ��    �  ��    �  �     '  �>    'W  �V    'Y  ��    'Z  ��    '[  ��    '\  ��    ']  ��    '^  ��    '_  �    '`  �>    'a  �\    'b  �t    'c  ��    'd  ��    'e  ��    'h  ��    'i  �    'j  �l    '�  ��    '�  ��    '�  ��    '�  ��    '�  �
    '�  �"    '�  �:    '�  �V    '�  �r    '�  ��    '�  ��    '�  ��    '�  ��    '�  ��    '�  �     '�  �H    '�  �j    '�  ��    '�  ��    '�  �    '�  �.    '�  �T    '�  �~    '�  ��    '�  ��    '�  ��    '�  �    '�  �t    '�  ��    '�  ��    '�  �4    '�  ��    '�  �>    '�  �|    '�  �    (  �(    (  �D    (  �^    (  �x    (
  ��    (  ��    (  ��    (  ��    (  �    (#  �0    ($  �F    (%  �Z    (&  �p    ('  ��    ((  ��    (*  ��    (1  ��    (2  ��    (3  ��    (4  �    (5  �0    (6  �J    (7  �d    (8  ��    (<  ��    (=  ��    (>  ��    (A  ��    (B  �    (C  �     (D  �>    (E  �T    (�  �l    (�  ��    (�  ��    (�  ��    (�  ��    (�  �    (�  �(    (�  �V    (�  �z    (�  ��    (�  ��    (�  ��    )   �    )  �2    )  �b    )  ��    )  ��    )  ��    )  ��    )  ��    )  �    )  �>    )  �h    )  ��    )   ��    )!  ��    )"  �    )#  �4    ),  �L    )-  �b    ).  �|    )/  ��    )0  ��    )1  ��    )2  ��    )4  �    )5  �B    )6  �`    )7  �|    )8  ��    )9  ��    ):  ��    )@  ��    )A  �    )B  �    )C  �6    )D  �J    )E  �b    )F  �z    )G  ��    )H  ��    )I  ��    )J  ��    )K  �
    )L  �     )M  �8    )N  �N    )O  �h    )Z  ��    )[  ��    )\  ��    )]  ��    )^  �    )_  �2    )`  �T    )c  �v    )d  ��    )e  ��    )f  ��    )g  ��    )h  ��    )j  ��    )q  �    )r  �(    )s  �>    )t  �X    )u  �p    )v  ��    )w  ��    )x  ��    )y  ��    )z  ��    ){  �    )|  �(    )}  �<    )~  �T    )  �j    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �
    )�  �     )�  �6    )�  �h    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �2    )�  �R    )�  �t    )�  ��    )�  ��    )�  ��    )�  �    )�  �$    )�  �B    )�  �`    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �&    )�  �L    )�  �n    )�  ��    )�  ��    )�  ��    )�  �     )�  �(    )�  �P    )�  �x    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �4    )�  �Z    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �,    )�  �B    )�  �V    )�  �l    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �$    )�  �<    )�  �R    )�  �l    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �     )�  �    )�  �.    )�  �D    )�  �Z    )�  �p    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �    )�  �2    )�  �J    )�  �`    )�  �x    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �,    )�  �D    )�  �f    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �4    )�  �\    )�  �z    )�  ��    )�  ��    *   ��    *  ��    *  �    *  �*    *  �L    *  �j    *  ��    *  ��    *  ��    *	  ��    *
  �    *  �2    *  �P    *  �j    *2  ��    *3  ��    *4  ��    *5  �    *D  �     *E  �:    *F  �X    *G  �~    *H  ��    *I  ��    *J  ��    *K  ��    *L  �    *Y  �0    *^  �L    *_  �z    *`  ��    *a  ��    *b  �    *c  �&    *m  �B    *n  �f    *o  ��    *p  ��    *q  ��    *r  ��    *s  ��    *t  �    *u  �(    *v  �<    *w  �R    *�  �d    *�  ��    *�  ��    *�  ��    *�  �    *�  �6    *�  �X    *�  �z    *�  ��    *�  ��    *�  ��    *�  �     *�  �$    *�  �F    *�  �^    *�  �x    *�  ��    *�  ��    *�  ��    *�  ��    *�  ��    *�  �    *�  �8    *�  �Z    *�  ��    *�  ��    *�  ��    *�  �    *�  �0    *�  �J    *�  �l    *�  ��    *�  ��    *�  ��    *�  ��    *�  ��    *�  ��    *�  �    *�  �2    *�  �N    *�  �j    +n  ��    +o  ��    +p  ��    +r  �    +s  �J    +t  �t    +u  ��    +v  ��    +w  ��    +x  ��    +y  �,    +z  �P    +{  �z    +|  ��    +}  ��    +~  ��    +�  �     +�  �N    +�  �|    +�  ¤    +�  ��    +�  �    +�  �2    +�  �Z    +�  È    +�  è    +�  ��    +�  ��    +�  �     +�  �L    +�  �x    +�  Ħ    +�  ��    +�  �(    +�  �v    +�  ��    +�  �
    +�  Ɣ    +�  �    +�  �P    +�  ��    +�  �    +�  �.    +�  �j    +�  Ȏ    +�  Ȥ    +�  �     +�  �N    +�  �~    +�  ɢ    +�  ��    ,  �    ,  �8    ,  �j    ,  ʒ    ,  ʶ    ,  ��    ,  ˈ    ,  ��    ,  �
    ,  ̴    ,  ��    ,  ��    ,   �
    ,!  �$    ,"  �B    ,#  �Z    ,$  �t    ,%  ͐    ,&  Ͱ    ,'  ��    ,(  ��    ,)  ��    ,*  �    ,+  �<    ,8  �l    ,9  Β    ,:  β    ,;  ��    ,<  ��    ,=  �     ,>  �@    ,?  �f    ,@  φ    ,B  Ϫ    ,C  ��    ,D  ��    ,E  ��    ,F  �    ,h  �2    ,i  �V    ,j  �p    ,k  В    ,l  ж    ,v  ��    ,w  ��    ,x  �    ,y  �    ,z  �.    ,{  �D    ,|  �Z    ,}  �x    ,~  њ    .�  Ѻ    .�  ��    .�  ��    .�  �    .�  �@    .�  �d    .�  ҆    .�  Ҭ    .�  ��    .�  �     .�  �&    .�  �B    .�  �^    .�  �z    .�  Ӗ    .�  Ӳ    .�  ��    .�  ��    .�  �    .�  �$    .�  �D    .�  �d    .�  Ԃ    .�  Ԧ    .�  ��    .�  ��    .�  �    .�  �.    .�  �T    .�  �x    .�  բ    .�  ��    /   ��    /  �    /  �6    /  �^    /  ք    /  ְ    /  ��    /  ��    /  �    /	  �6    /
  �X    /  �x    /  מ    /  ��    /  ��    /  ��    /  �    /  �0    /  �L    /  �n    /  ،    /  ئ    /  ��    /  ��    /  �    /  �"    /  �F    /  �f    /  ل    /  ٤    /  ��    /  ��    /   �     /!  �    /"  �2    /#  �P    /$  �l    /%  ڂ    /&  ڢ    /'  ھ    /(  ��    /)  �     /*  �    /+  �6    /,  �T    /-  �r    /.  ێ    //  ۦ    /0  ��    /1  ��    /2  ��    /3  �    /4  �$    /5  �@    /6  �\    /7  �v    /D  ܒ    /E  ܬ    /F  ��    /G  ��    /H  �    /I  �B    /J  �h    /K  ݒ    /L  ݺ    /M  ��    /N  �    /O  �4    /P  �R    /Q  �p    /R  ގ    /S  ެ    /T  ��    /U  ��    /V  �    /W  �$    /X  �D    /Y  �h    /Z  ߈    /[  ߬    /\  ��    /]  ��    /^  �    /_  �0    /`  �X    /a  �|    /b  �    /c  ��    /d  ��    /e  �    /f  �&    /g  �L    /h  �n    /i  �    /j  �    /k  ��    /l  ��    /m  �    /n  �:    /o  �\    /p  �    /q  �    /r  �    /s  ��    /t  ��    /u  �    /v  �6    /w  �X    /x  �v    /y  �    /z  �    /{  ��    /|  ��    /}  �    /~  �6    /  �V    /�  �x    /�  �    /�  �    /�  ��    /�  ��    /�  �    /�  �4    /�  �R    /�  �p    /�  �    /�  �    /�  ��    /�  ��    /�  �    /�  �    /�  �8    /�  �V    /�  �v    /�  �    /�  �    /�  ��    /�  ��    /�  �     /�  �    /�  �2    /�  �N    /�  �n    /�  �    1�  �    1�  ��    1�  ��    1�  ��    1�  �    1�  �8    1�  �\    1�  �|    1�  �    1�  �    1�  ��    1�  ��    1�  �    1�  �$    1�  �F    1�  �h    1�  �    1�  �    1�  ��    1�  ��    1�  �
    1�  �$    1�  �D    1�  �`    1�  �    1�  �    1�  ��    1�  ��    1�  �    1�  �2    2   �\    2  �t    2  �    2  �    2  ��    2  ��    2  �    2  �     2  �>    2	  �`    2
  �    2  �    :h  ��    :p  ��    :q  ��    :r  �    :s  �4    :t  �V    :u  �z    :v  �    :w  ��    :z  ��    :{  ��    :|  �    :  �&    :�  �B    :�  �\    :�  �v    :�  �    :�  �    :�  �    :�  ��    :�  ��    :�  �    :�  �$    :�  �B    :�  �b    :�  �v    :�  �    :�  �    <   �    <  ��    <  �    <  �(    <!  �B    <"  �Z    <#  �v    <$  �    <%  ��    <&  ��    <F  �    <G  �<    <H  �`    <I  �    <X  �    <Y  ��    <Z  ��    <[  �    <\  �(    <]  �J    <^  �`    <_  �x    <`  �    <a  �    <b  ��    <c  ��    <d  ��    <o  �    <p  �.    <q  �F    <r  �^    <s  �    <u  �    C  ��    C  ��    FQ  ��    FR  �J    FS  ��    FT  ��    FU  �    FV  ��    FW  �$    FX  �j    FY  ��    F[  �z    F\  ��    F]  ��    F^  ��    F_  ��    F`  �&    Fk  �@    Fl  �b    Fn  ��    Fo  ��    Fp  ��    Ft  �    Fu  �*    Fv  �J    Fw  ��    Fx  ��    Fy  ��    Fz  �     F{  �    F|  �X    F}  ��    F~  ��    F  �    F�  �8    F�  �P    F�  �f    F�  �z    F�  ��    F�  ��    F�  ��    F�  �    F�  �     F�  �B    F�  �b    F�  ��    F�  ��    F�  ��    F�  ��    F�  �    F�  �0    F�  �J    F�  �j    F�  ��    F�  ��    F�  ��    F�  ��    F�  �0    F�  �V    F�  �n    F�  ��    F�  ��    F�  ��    F�  �    F�  �(    F�  �J    F�  ��    F�      F�  �    F�  �    F�  �    F� "    F� d    F� �    F� �    F� p    F� �    F� 
    F� R    F� x    F� �    F� �    F� �    F� �    F�     F� <    F� R    F� f    F� z    F� �    F� �    F� �    F� �    F� �    F�     F� 0    F� J    F� h    F� �    F� �    F� �    F� �    F�     F� 0    F� T    F� v    F� �    F� �    F� �    F�     F� (    F� F    F� �    F� �    F� 2    F� R    F� �    H� �    H� �    H� 	$    H� 	v    � ,    � V    � �    � �    � �    �     '� 8    '� f    '� �    '� �    'b e    'c f�    'd h�    'l i.    '� j�    '� kL    (� l"    FP n     FQ q     FR q�    FS v�    FT v�    FU }�    FV ~�    FZ ��  #  'S �  #  'T L  #  'U �  #  'e   #  'f f  #  'g �  #  'i   #  'k |  #  '� �  #  '� :  #  '� �  #  '� �  #  (  6  #  (  �  #  (�  �  #  (� !D  #  (� !�  #  (� !�  #  (� "F  #  (� "�  #  (� "�  #  (� #H  #  (� #�  #  (� #�  #  (� $`  #  (� $�  #  (� %*  #  (� %�  #  (� %�  #  )$ &V  #  )% &�  #  )& '  #  )' 'p  #  +� '�  #  +� (2  #  +� (�  #  +� )   #  +� )f  #  +� )�  #  +� *   #  +� *H  #  ,  *�  #  , *�  #  , +   #  , +h  #  , +�  #  , +�  #  , ,@  #  , ,�  #  , ,�  #  ,	 -  #  ,
 -h  #  , -�  #  , -�  #  , .@  #  , .�  #  , .�  #  , /  #  , /X  #  ,, /�  #  ,- /�  #  ,. 0b  #  ,/ 0�  #  ,0 1.  #  ,1 1�  #  ,2 1�  #  ,3 2b  #  ,4 2�  #  ,6 3.  #  ,7 3�  #  ,G 3�  #  ,H 4X  #  ,I 4�  #  ,J 5(  #  ,L 5�  #  ,M 5�  #  ,N 6b  #  ,O 6�  #  ,Q 78  #  ,R 7�  #  ,S 8  #  ,T 8n  #  ,V 8�  #  ,W 9:  #  ,X 9�  #  ,Y :  #  ,[ :z  #  ,\ :�  #  ,] ;B  #  ,^ ;�  #  ,_ <  #  ,` <r  #  ,a <�  #  ,b =@  #  ,c =�  #  ,e >  #  ,f >p  #  ,g >�  #  ,m ?,  #  ,n ?�  #  ,o @   #  ,p @h  #  ,q @�  #  ,r A>  #  ,s A|  #  ,t A�  #  ,u A�  $    B6  $  'Q C�  $  'R E$  $  FP E�  $  FQ HX  D  (K H�  D  (M P�  D  (P Y,  D  (T ax  D  (W d  T  '� d�  y  (G 	�  y  (H 
�  y  <v �  N4