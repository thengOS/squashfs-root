  �      Möchten Sie die Version $NEW der Extension '$NAME' installieren?
Die neuere Version $DEPLOYED ist bereits installiert.
Klicken Sie auf 'OK', um die bereits installierte Version zu ersetzen.
Klicken Sie auf 'Abbrechen', um die Installation abzubrechen.    �      Möchten Sie die Version $NEW der Extension '$NAME' installieren?
Die neuere Version $DEPLOYED, '$OLDNAME', ist bereits installiert.
Klicken Sie auf 'OK', um die bereits installierte Version zu ersetzen.
Klicken Sie auf 'Abbrechen', um die Installation abzubrechen.   �     �   �Möchten Sie die Version $NEW der Extension '$NAME' installieren?
Diese Version ist bereits installiert.
Klicken Sie auf 'OK', um die bereits installierte Version zu ersetzen.
Klicken Sie auf 'Abbrechen', um die Installation abzubrechen.   �      Möchten Sie die Version $NEW der Extension '$NAME' installieren?
Die Version '$OLDNAME' ist bereits installiert.
Klicken Sie auf 'OK', um die bereits installierte Version zu ersetzen.
Klicken Sie auf 'Abbrechen', um die Installation abzubrechen.    �      Möchten Sie die Version $NEW der Extension '$NAME' installieren?
Die ältere Version $DEPLOYED ist bereits installiert.
Klicken Sie auf 'OK', um die bereits installierte Version zu ersetzen.
Klicken Sie auf 'Abbrechen', um die Installation abzubrechen.   �      Möchten Sie die Version $NEW der Extension '$NAME' installieren?
Die ältere Version $DEPLOYED, '$OLDNAME', ist bereits installiert.
Klicken Sie auf 'OK', um die bereits installierte Version zu ersetzen.
Klicken Sie auf 'Abbrechen', um die Installation abzubrechen.    �     �   �Möchten Sie die Extension '%NAME' installieren?
Klicken Sie auf 'OK', um mit der Installation fortzufahren.
Klicken Sie auf 'Abbrechen', um die Installation abzubrechen.    �     8   8Es sind keine neuen Updates verfügbar.   �     �   �Es sind keine installierbaren Updates verfügbar. Um ignorierte oder deaktivierte Updates angezeigt zu bekommen, wählen Sie 'Zeige alle Updates'.    �     0   0Es ist ein Fehler aufgetreten:    �     $   $Unbekannter Fehler.   �     L   LEs ist keine Beschreibung für diese Extension verfügbar.    �     R   RDie Extension kann aus folgendem Grund nicht aktualisiert werden:   �     L   LDie benötigte %PRODUCTNAME-Version stimmt nicht überein:    �     <   <Sie haben %PRODUCTNAME %VERSION installiert   �     *   *Browser-basiertes Update    �        Version   �     ,   ,Dieses Update überspringen   �     &   &Updates ermöglichen    �     ,   ,Alle Updates überspringen    �     2   2Dieses Update wird ausgelassen.
    �     0   0Installieren der Extensions...    �     &   &Installation beendet    �     2   2Es sind keine Fehler aufgetreten.   �     (   (Die Fehlermeldung ist:    �     ^   ^Ein Fehler ist während des Herunterladens der Extension %NAME aufgetreten.     �     \   \Ein Fehler ist während der Installation der Extension %NAME aufgetreten.     �     N   NDie Lizenzvereinbarung für Extension %NAME wurde abgelehnt.    �     6   6Die Extension wird nicht installiert.        *   *Extension(s) hinzufügen             ~Entfernen    !        ~Aktivieren   "        ~Deaktivieren   #        ~Update...    $        ~Optionen...    %     ,   ,Füge %EXTENSION_NAME hinzu   &     *   *Entferne %EXTENSION_NAME    '     *   *Aktiviere %EXTENSION_NAME   (     ,   ,Deaktiviere %EXTENSION_NAME   )     8   8Lizenz für %EXTENSION_NAME akzeptieren   ,     D   DFehler: Der Status der Extension ist nicht bekannt    -        Schließen    .        Beenden   /    �  �%PRODUCTNAME wurde auf eine neue Version aktualisiert. Einige für alle Anwender installierte %PRODUCTNAME-Extensions sind mit dieser Version nicht kompatibel und müssen aktualisiert werden, bevor %PRODUCTNAME gestartet werden kann.

Um für alle Anwender installierte Extensions zu aktualisieren, werden Administratorrechte benötigt. Kontaktieren Sie ihren Administrator, um folgende Extensions zu aktualisieren:   0     z   zDie Erweiterung kann nicht aktiviert werden, da die folgenden Systemvoraussetzungen nicht erfüllt sind:    1     \   \Die Extension ist deaktiviert, da die Lizenz noch nicht akzeptiert wurde.
    2          Lizenz anzeigen   5     P   PDie Extension '%Name' ist auf diesem Computer nicht lauffähig.   7     �   �Möchten Sie die Extension '%NAME' entfernen?
Klicken Sie auf 'OK', um die Extension zu entfernen.
Klicken Sie auf 'Abbrechen', um das Entfernen abzubrechen.   8    ,  ,Stellen Sie sicher, dass keine anderen Anwender mit der gleichen %PRODUCTNAME Installation arbeiten, wenn Sie gemeinsam genutzte Extensions in einer Mehrbenutzerumgebung ändern.
Klicken Sie 'OK', um die Extension zu entfernen.
Klicken Sie 'Abbrechen', um das Entfernen abzubrechen.    9    .  .Stellen Sie sicher, dass keine anderen Anwender mit der gleichen %PRODUCTNAME Installation arbeiten, wenn Sie gemeinsam genutzte Extensions in einer Mehrbenutzerumgebung ändern.
Klicken Sie 'OK', um die Extension zu aktivieren.
Klicken Sie 'Abbrechen', um die Aktivierung abzubrechen.   :    2  2Stellen Sie sicher, dass keine anderen Anwender mit der gleichen %PRODUCTNAME Installation arbeiten, wenn Sie gemeinsam genutzte Extensions in einer Mehrbenutzerumgebung ändern.
Klicken Sie 'OK', um die Extension zu deaktivieren.
Klicken Sie 'Abbrechen', um die Deaktivierung abzubrechen.   �  #   H   H            4   4       desktop/res/caution_12.png      #   H   H            4   4      desktop/res/caution_16.png    
  #   D   D            0   0      desktop/res/lock_16.png     #   J   J            6   6      desktop/res/extension_32.png      #   F   F            2   2      desktop/res/shared_16.png   �  #   V   V            (   (      res/sx03256.png             ��  ��      �  #   V   V            (   (      res/im30820.png             ��  ��      �  #   ^   ^            0   0      res/dialogfolder_16.png             ��  ��      �  #   V   V            (   (      res/xml_16.png              ��  ��      |  #   \   \            .   .      	res/component_16.png              ��  ��      ~  #   `   `            2   2      
res/javacomponent_16.png              ��  ��      �  #   Z   Z            ,   ,      res/library_16.png              ��  ��      �  #   ^   ^            0   0      res/javalibrary_16.png              ��  ��      X  #   ^   ^            0   0      res/sc_helperdialog.png             ��  ��       �           C     �  &    �        �      �  (    �  &    �  .    �  <    �  X    �      �  L    �  �    �       �  D    �  �    �  �    �  	.    �  	j    �  	�    �  	�    �  	�    �  	�    �  
*    �  
\    �  
�    �  
�    �  
�    �      �  j    �  �    �        J       t    !  �    "  �    #  �    $  �    %      &  0    '  Z    (  �    )  �    ,  �    -  ,    .  H    /  `    0      1  �    2  �    5      7  V    8      9  0    :  ^  #  �  �  #    �  #  
     #    d  #    �  #  �  �  #  �  J  #  �  �  #  �  �  #  |  T  #  ~  �  #  �    #  �  j  #  X  �  (