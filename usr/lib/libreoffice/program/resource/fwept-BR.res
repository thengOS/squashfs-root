  �        Suplementos   �     &   &Ajuda d~o suplemento    �        Todos   �        At~ualizar    �     *   *Fe~char e retornar para     �     (   (S~alvar cópia como...    �          Sem documentos    �     "   "Suplemento %num%    �        Repetir   �    r  rNão foi possível salvar informações internas importantes do %PRODUCTNAME devido à falta de espaço em disco no seguinte local:
%PATH

Você não poderá continuar trabalhando com o %PRODUCTNAME sem alocar mais espaço em disco nesse local.

Pressione o botão 'Repetir' depois de obter mais espaço em disco para tentar salvar os dados novamente.

   �        ~Redefinir    �     �   �Ocorreu um erro ao carregar os dados de configuração da interface do usuário. A aplicação será encerrada agora.
Tente reinstalar a aplicação.   �     �   �Ocorreu um erro ao carregar os dados de configuração da interface do usuário. A aplicação será encerrada agora.
Tente remover seu perfil de usuário para a aplicação.    �     �   �Ocorreu um erro ao carregar os dados da interface do usuário. A aplicação será encerrada agora.
Tente primeiro remover seu perfil de usuário para a aplicação ou tente reinstalar a aplicação.   �     $   $Múltiplos idiomas    �     4   4Nenhum (Não verificar ortografia)    �        Mais...   �     2   2Definir o idioma para a seleção   �     0   0Definir o idioma do parágrafo    �     2   2Definir o idioma de todo o texto    �        Sem título   �     0   0Redefinir para o idioma padrão   �        Limpar lista    �     \   \Limpa a lista de arquivos recentes. Esta operação não pode ser desfeita.   �     x   xIdioma do texto. Clique com o botão direito do mouse para definir o idioma do caractere ou parágrafo.   �     &   &Abrir arquivo remoto    �         (Remoto)   :�  #   H   H            4   4       res/savemodified_small.png    :�  #   H   H            4   4      res/savemodified_large.png    '    �  �               <   <  Botões ~visíveis                           ^   ^  
   ~Personalizar barras de ferramentas...  .uno:ConfigureToolboxVisible                           8   8   
   ~Encaixar barras de ferramentas          B   B   
   Encaixar todas as barras de ~ferramentas                           D   D   
   ~Travar a posição da barra de ferramentas          8   8   
   Fechar a ~barra de ferramentas     �                �  	�    �        �       �   B    �   X    �   t    �   �    �   �    �   �    �      �       �  �    �  �    �  T    �      �  �    �      �  D    �  \    �  �    �  �    �  �    �      �  <    �  Z    �  �    �  .    �  T    '  �  #  :�  n  #  :�  �  x