  �    #�     *�        Latim básico   *�        Latim-1   *�     "   "Latim estendido-A   *�     "   "Latim estendido-B   *�          Extensões IPA    *�     6   6Letras modificadoras de espaçamento    *�     4   4Marcadores diacríticos associados    *�        Grego básico   *�     *   *Símbolos gregos e coptas   *�        Cirílico   *�        Armênio    *�     "   "Hebraico básico    *�     $   $Hebraico estendido    *�          Árabe básico    *�     "   "Árabe estendido    *�        Devanágari   *�        Bengali   *�        Gurmukhi    *�        Guzerate    *�        Odia    *�        Tâmil    *�        Telugo    *�        Canarês    *�        Malaiala    *�        Tailandês    *�        Laosiano    *�     "   "Georgiano básico   *�     $   $Georgiano estendido   *�        Hangul Jamo   *�     .   .Adicionais de latim estendido   *�          Grego estendido   *�     "   "Pontuação geral   *�     *   *Sobrescritos e subscritos   *�     &   &Símbolos monetários   *�     2   2Símbolos diacríticos associados   *�     (   (Símbolos alfabéticos    *�     "   "Formas numéricas   *�        Setas   *�     (   (Operadores matemáticos   *�     $   $Técnicas diversas    *�     $   $Imagens de controle   *�     6   6Reconhecimento óptico de caracteres    *�     2   2Código Alfanumérico delimitado    *�     "   "Desenho de caixa    *�     $   $Elementos de bloco    *�     $   $Formas geométricas   *�     $   $Símbolos diversos    *�     &   &Caracteres gráficos    *�     ,   ,Pontuação e símbolos CJK   *�        Hiragana    *�        Katakana    *�        Bopomofo    *�     ,   ,Hangul compatível com Jamo   *�        CJK Diversos    *�     0   0Meses e Letras CJK delimitados    *�     $   $Compatibilidade CJK   *�        Hangul    *�     *   *Ideogramas CJK unificados   +N     :   :Extensão unificada de ideogramas CJK - A   *�     &   &Área de uso privado    *�     2   2Ideogramas de compatibilidade CJK   *�     6   6Formas de apresentação alfabética    *�     4   4Formas de apresentação arábica-A   *�     .   .Meias marcações associadas    *�     .   .Formas de compatibilidade CJK   *�     *   *Subtipos de forma pequena   *�     4   4Formas de apresentação arábica-B   *�     :   :Formas de meia largura e largura inteira    *�        Especiais   *�        Sílabas Yi   *�        Radicais Yi   *�          Itálico antigo   *�        Gótico   *�        Deseret   *�     .   .Símbolos musicais bizantinos   *�     $   $Símbolos musicais    *�     6   6Símbolos matemáticos alfanuméricos   *�     6   6Ideogramas unificados CJK extensão B   *�     6   6Ideogramas unificados CJK extensão C   *�     6   6Ideogramas unificados CJK extensão D   *�     @   @Ideogramas de compatibilidade CJK suplementares   *�        Marcas    *�     &   &Suplemento cirílico    *�     (   (Seletores de variação   *�     4   4Área de uso privado suplementar-A    *�     4   4Área de uso privado suplementar-B    *�        Limbu   *�        Tai Le    *�          Símbolos Khmer   *�     &   &Extensões fonéticas   *�     ,   ,Símbolos e setas diversos    *�     ,   ,Símbolos hexagramas Yijing   *�     $   $Sílabas lineares B   *�     &   &Ideogramas lineares B   *�          Números egeus    *�        Ugarítico    *�        Shaviano    *�        Osmania   +Z        Sinhala   +[        Tibetano    +\        Myanmar   +]        Khmer   +^        Ogam    +f        Rúnico   +g        Siríaco    +_        Tana    +h        Etíope   +i        Cherokee    +`     0   0Sílabas aborígines canadenses   +j        Mongol    +k     2   2Símbolos matemáticos diversos-A   +l     &   &Setas suplementares-A   +a     "   "Padrões Braille    +m     &   &Setas suplementares-B   +n     2   2Símbolos matemáticos diversos-B   +b     6   6Formas suplementares de radicais CJK    +o          Radicais Kangxi   +p     8   8Caracteres de descrição ideográficos   +q        Tagalo    +r        Hanunoo   +c        Tagbanúa   +t        Buhid   +s        Kanbun    +d     $   $Bopomofo estendido    +e     $   $Fonética Katakana    *�        Traços CJK   *�     $   $Sílabas cipriotas    *�     (   (Símbolos Tai Xuan Jing   *�     6   6Suplemento de seletores de variação   *�     0   0Notação musical grega antiga    *�     (   (Números gregos antigos   *�     "   "Suplemento Árabe   *�        Buginês    +      >   >Suplemento de marcas diacríticas combinadas    +        Copta   +     "   "Etíope estendido   +     $   $Suplemento Etíope    +     &   &Suplemento Georgiano    +        Glagolítico    +        Kharoshthi    +     ,   ,Letras modificadoras de tom   +        Tai Lue Novo    +	        Persa antigo    +
     4   4Suplemento de extensões fonéticas   +     (   (Pontuação suplementar   +        Syloti Nagri    +        Tifinagh    +     "   "Formas verticais    +        Nko   +        Balinês    +     "   "Latim estendido-C   +     "   "Latim estendido-D   +        Phags-pa    +        Fenício    +        Cuneiforme    +     4   4Pontuação e números cuneiformes    +     "   "Barras de contar    +        Sundanês   +        Lepcha    +        Ol Chiki    +     &   &Cirílico estendido-A   +        Vai   +     &   &Cirílico estendido-B   +        Saurashtra    +        Kayah Li    +         Rejang    +!        Cham    +"     "   "Símbolos antigos   +#          Disco de Festos   +$        Lício    +%        Carian    +&        Lídio    +'     "   "Peças do Mahjong   +(     "   "Peças de dominó   +)        Samaritano    +*     J   JSílabas estendidas unificadas dos aborígenes canadenses   ++        Tai Tham    +,     $   $Extensões védicas   +-        Lisu    +.        Bamum   +/     2   2Formas numéricas índicas comuns   +0     &   &Devanágari estendido   +1     (   (Hangul Jamo estendido-A   +2        Javanês    +3     &   &Birmanês estendido-A   +4     &   &Tailandês vietnamita   +5        Meetei Mayek    +6     (   (Hangul Jamo estendido-B   +7     $   $Aramáico imperial    +8     &   &Árabe do sul antigo    +9        Avéstico   +:     "   "Pártico inscrito   +;     "   "Pálavi inscrito    +<        Turco antigo    +=     *   *Símbolos numéricos Rumi   +>        Kaithi    +?     (   (Hieróglifos egípcios    +@     2   2Suplemento alfanumérico contidos   +A     0   0Suplemento ideográfico contido   +C        Mandaico    +D        Batak   +E     $   $Etíope estendido-A   +F        Brahmi    +G     "   "Suplemento Bamum    +H          Suplemento Kana   +I          Jogo de cartas    +J     2   2Diversos símbolos e pictogramas    +K        Emoticons   +L     2   2Símbolos de transportes e mapas    +M     &   &Símbolos alquímicos   +O     $   $Árabe estendido-A    +P     <   <Símbolos alfabéticos matemáticos árabes   +Q        Chakma    +R     (   (Extensões Meetei Mayek   +S     $   $Meroítica cursiva    +T     *   *Hieróglifos meroíticos    +U        Miao    +V        Sharada   +W        Sora Sompeng    +X     &   &Suplemento sundanês    +Y        Takri   +u        Bassa Vah   +v     $   $Albanês caucasiano   +w     ,   ,Números epactos cópticos    +x     :   :Marcas diacríticas combinadas expandidas   +y     *   *Estenografia de Duployé    +z     (   (Estenografia de Elbasan   +{     0   0Formas geométricas expandidas    +|        Grantha   +}        Khojki    +~        Khudawadi   +     "   "Latim estendido-E   +�        Linear A    +�        Mahajani    +�        Maniqueio   +�          Alfabeto Mende    +�        Modi    +�        Mro   +�     &   &Birmanês estendido-B   +�     $   $Alfabeto nabateano    +�     ,   ,Árabe setentrional antigo    +�          Pérmico antigo   +�     (   (Pictogramas ornamentais   +�        Pahawh Hmong    +�        Palmireno   +�        Pau Cin Hau   +�          Psalter Pahlavi   +�     4   4Controles de formato taquigráfico    +�        Siddham   +�     .   .Números arcaicos cingaleses    +�     (   (Setas suplementares - C   +�        Tirhuta   +�        Warang Citi   +�        Ahom    +�     *   *Hieróglifos anatolianos    +�     $   $Suplemento cherokee   +�     <   <Extensão de ideogramas unificados CJK - E    +�     0   0Cuneiformes dinástico arcaico    +�        Hatran    +�        Multani   +�          Húngaro antigo   +�     6   6Símbolos e pictogramas suplementares   +�     ,   ,Escrita de sinais de Sutton   (1    �              Esquerda             Dentro             Direita            Fora     !        Centro             Da esquerda            Do interior         $   $Área de parágrafo         .   .Área de texto do parágrafo     	     *   *Borda esquerda da página         *   *Borda interna da página     
     *   *Borda direita da página          *   *Borda externa da página          .   .Borda esquerda do parágrafo          ,   ,Borda interna do parágrafo         ,   ,Borda direita do parágrafo         ,   ,Borda externa do parágrafo              Página inteira         *   *Área de texto da página            Superior             Inferior              Centro     "        De cima    #        De baixo     $        Abaixo     %        Da direita     &     *   *Borda superior da página    '     *   *Borda inferior da página    (     .   .Borda superior do parágrafo     )     .   .Borda inferior do parágrafo             Margem          .   .Área de texto do parágrafo          *   *Borda esquerda do quadro          (   (Borda interna do quadro         (   (Borda direita do quadro         (   (Borda externa do quadro              Quadro inteiro          *   *Área de texto do quadro             Linha de base            Caractere            Linha    *          Linha de texto    (n    	          �   �Nenhum dicionário de sinônimos está disponível para o idioma atual.
Verifique a instalação e instale o idioma desejado.        4  4A verificação ortográfica não oferece suporte a $(ARG1) ou ela não está ativa no momento.
Verifique a instalação e, caso necessário, instale o módulo
do idioma desejado ou ative o idioma em 'Ferramentas - Opções - Definições de Idioma - Recursos de verificação ortográfica'.        F   FA verificação ortográfica não está disponível.         0   0Hifenização não disponível.        L   LNão foi possível ler o dicionário personalizado $(ARG1).        N   NNão foi possível criar o dicionário personalizado $(ARG1).        @   @Não foi possível encontrar a figura $(ARG1).         F   FNão foi possível carregar um gráfico desvinculado.   	     F   FNão foi definido um idioma para o termo selecionado.   
     �   �A camada de formulário não foi carregada por que requer serviços de E/S (com.sun.star.io.*) que não puderam ser instanciados.   
     �   �A camada de formulário não foi gravada por que requer serviços de E/S (com.sun.star.io.*) que não puderam ser instanciados.        |   |Ocorreu um erro durante a leitura de controles de formulário. A camada de formulário não foi carregada.         z   zOcorreu um erro durante a gravação de controles de formulário. A camada de formulário não foi salva.        Z   ZErro ao ler um dos marcadores. Nem todos os marcadores foram carregados.         �   �Todas as alterações no código Basic foram perdidas. O código de macro do VBA original foi salvo em seu lugar.        X   XO código Basic do VBA original contido no documento não será salvo.          N   NA senha está incorreta. Não é possível abrir o documento.        �   �Não há suporte para o método de criptografia utilizado neste documento. Apenas a criptografia de senha compatível com Microsoft Office 97/2000 é aceita.        r   rNão há suporte para carregar apresentações do Microsoft PowerPoint criptografadas por senha.         �   �Não há suporte para proteção de senha quando os documentos são salvos num formato do Microsoft Office.
Deseja salvar o documento sem esse tipo de proteção?    (o    h           @   @$(ERR) ao executar o dicionário de sinônimos.         B   B$(ERR) ao executar a verificação ortográfica.          4   4$(ERR) ao executar a hifenização.         0   0$(ERR) ao criar um dicionário.         B   B$(ERR) ao definir os atributos de plano de fundo.         0   0$(ERR) ao carregar as figuras.    *d             y  r  r   	Configuração da borda     Linha de borda esquerda    Linha de borda direita     Linha de borda superior    Linha de borda inferior    Linha de borda horizontal    Linha de borda vertical    Linha de borda em diagonal da esquerda superior até a direita inferior    Linha de borda em diagonal da esquerda inferior até a direita superior         y  r  r   	Configuração da borda     Linha de borda esquerda    Linha de borda direita     Linha de borda superior    Linha de borda inferior    Linha de borda horizontal    Linha de borda vertical    Linha de borda em diagonal da esquerda superior até a direita inferior    Linha de borda em diagonal da esquerda inferior até a direita superior            ,   ,       svx/res/frmsel.bmp    FP     V              Tabela             Consulta             SQL   FQ    D              LIKE             NOT            EMPTY            TRUE             FALSE            IS             BETWEEN            OR     	        AND    
        Média             Contar             Máximo            Mínimo            Soma             Cada             Qualquer             Algum            STDDEV_POP             STDDEV_SAMP            VAR_SAMP             VAR_POP            Coletar            Fusão             Interseção          L   L$(WIDTH) x $(HEIGHT) ($(WIDTH_IN_PX) x $(HEIGHT_IN_PX) px)         4   4$(WIDTH) x $(HEIGHT) com $(DPI) PPP             $(CAPACITY) kiB           Imagem GIF            Imagem JPEG           Imagem PNG            Imagem TIFF           Imagem WMF            Imagem MET    	        Imagem PCT    
        Imagem SVG            Imagem BMP            Desconhecido    
      "   "objeto de desenho   
     $   $objetos de desenho    
          objeto de grupo   
     "   "objetos de grupo    
     &   &objeto de grupo vazio   
     (   (Objetos de grupo vazios   
        Tabela    
        Tabelas   
        Linha   
	     "   "linha horizontal    

          linha vertical    
          linha diagonal    
        Linhas    
        Retângulo    
        Retângulos   
        Quadrado    
        Quadrados   
        Paralelogramo   
          Paralelogramos    
        Losango   
        Losangos    
     (   (Retângulo arredondado    
     *   *Retângulos arredondados    
     &   &quadrado arredondado    
     (   (Quadrados arredondados    
     *   *Paralelogramo arredondado   
     ,   ,Paralelogramos arredondados   
     $   $losango arredondado   
     &   &Losangos arredondados   
        Círculo    
        Círculos   
     "   "Setor de círculo   
      $   $Setores de círculo   
!        Arco    
"        Arcos   
#     &   &Segmento de círculo    
$     &   &Segmentos de círculo   
%        Elipse    
&        Elipses   
'          Setor de elipse   
(     "   "Setores de elipse   
)          Arco elíptico    
*     "   "Arcos elípticos    
+     $   $Segmento de elipse    
,     $   $Segmentos de elipse   
-        Polígono   
.     *   *Polígono de %2 vértices   
/        Polígonos    
0        Polilinha   
1     ,   ,Polilinha com %2 vértices    
2        Polilinhas    
3     "   "Curva de Bézier    
4     "   "Curvas de Bézier   
5     "   "Curva de Bézier    
6     "   "Curvas de Bézier   
7     $   $Linha à mão livre   
8     &   &Linhas à mão livre    
9     $   $Linha à mão livre   
:     &   &Linhas à mão livre    
;        Curva   
<          Objetos curvos    
=        Curva   
>          Objetos curvos    
?          Spline natural    
@     "   "Splines naturais    
A     "   "Spline periódica   
B     $   $Splines periódicas   
C          Quadro de texto   
D          Quadro de texto   
E     *   *Quadro de texto vinculado   
F     ,   ,Quadros de texto vinculados   
G     8   8Objeto de texto com tamanho ajustável    
H     8   8Objetos de texto com tamanho ajustável   
I     8   8Objeto de texto com tamanho ajustável    
J     8   8Objetos de texto com tamanho ajustável   
K     "   "Texto do título    
L     "   "Textos de título   
M     "   "Contorno do texto   
N     $   $Contorno de textos    
O        figura    
P        figuras   
Q     "   "Figura vinculada    
R     $   $Figuras vinculadas    
S     &   &Objeto gráfico vazio   
T     *   *Objetos gráficos vazios    
U     (   (Figura vinculada vazia    
V     *   *Figuras vinculadas vazias   
W        Meta-arquivo    
X        Meta-arquivos   
Y     (   (Meta-arquivo vinculado    
Z     *   *Meta-arquivos vinculados    
[        Bitmap    
\        Bitmaps   
]     "   "Bitmap vinculado    
^     $   $Bitmaps vinculados    
_        Figura do Mac   
`          Figuras do Mac    
a     (   (Figura do Mac vinculada   
b     *   *Figuras do Mac vinculadas   
c     *   *objeto incorporado (OLE)    
d     ,   ,Objetos incorporados (OLE)    
e     4   4objeto incorporado vinculado (OLE)    
f     6   6Objetos incorporados vinculados (OLE)   
g        Objeto    
h        Quadro    
i        Quadros   
j        Quadro    
k     &   &Conectores de objetos   
l     &   &Conectores de objetos   
m     "   "Texto explicativo   
n     $   $Textos explicativos   
o     "   "Visualizar objeto   
p     $   $Visualizar objetos    
q        Linha de cota   
r     &   &Objetos de cotação    
s     $   $objetos de desenho    
t     *   *Nenhum objeto de desenho    
u        e   
v     &   &objeto(s) de desenho    
w        Controle    
x        Controles   
y        Cubo 3D   
z        Cubos 3D    
{     $   $Objeto de extrusão   
|     &   &Objetos de extrusão    
}        Texto 3D    
~        Textos 3D   
     $   $objeto de rotação   
�     &   &objetos de rotação    
�        Objeto 3D   
�        Objetos 3D    
�        Polígonos 3D   
�        Cena 3D   
�        Cenas 3D    
�        esfera    
�        esferas   
�     *   *Bitmap com transparência   
�     4   4Bitmap vinculado com transparência   
�     ,   ,Bitmaps com transparência    
�     6   6Bitmaps vinculados com transparência   
�        Forma   
�        Formas    
�     "   "Objeto de mídia    
�     "   "Objetos de mídia   
�        font work   
�        font works    
�        SVG   
�        SVGs    
�        com cópia    
�     4   4Definir posição e tamanho para %1   
�        Excluir %1    
�     "   "Mover %1 adiante    
�     *   *Mover %1 mais para trás    
�     (   (Mover %1 para a frente    
�     $   $Mover %1 para trás   
�     &   &Inverter ordem de %1    
�        Mover %1    
�     "   "Redimensionar %1    
�        Girar %1    
�     *   *Inverter %1 na horizontal   
�     (   (Inverter %1 na vertical   
�     (   (Inverter %1 na diagonal   
�     *   *Inverter %1 à mão livre   
�     ,   ,Distorcer %1 (inclinação)   
�     &   &Dispor %1 em Círculo   
�     &   &Curvar %1 em Círculo   
�        Distorcer %1    
�        Desfazer %1   
�     8   8Modificar propriedades de Bézier de %1   
�     8   8Modificar propriedades de Bézier de %1   
�        Fechar %1   
�     4   4Definir direção da saída para %1   
�     0   0Definir atributo relativo em %1   
�     6   6Definir ponto de referência para %1    
�        Agrupar %1    
�        Desagrupar %1   
�     (   (Aplicar atributos a %1    
�     &   &Aplicar estilos a %1    
�     &   &Remover estilo de %1    
�     *   *Converter %1 em polígono   
�     ,   ,Converter %1 em polígonos    
�     &   &Converter %1 em curva   
�     (   (Converter %1 em curvas    
�        Alinhar %1    
�     &   &Alinhar %1 para cima    
�     &   &Alinhar %1 para baixo   
�     0   0Centralizar horizontalmente %1    
�     (   (Alinhar %1 à esquerda    
�     &   &Alinhar %1 à direita   
�     .   .Centralizar verticalmente %1    
�          Centralizar %1    
�          Transformar %1    
�        Combinar %1   
�        Combinar %1   
�        Dividir %1    
�        Dividir %1    
�        Dividir %1    
�     4   4StarDraw Dos Zeichnung importieren    
�     "   "HPGL importieren    
�          DXF importieren   
�     *   *Converter %1 em contorno    
�     *   *Converter %1 em contornos   
�        Mesclar %1    
�        Subtrair %1   
�          Intersectar %1    
�     0   0Distribuir objetos selecionados   
�     &   &Igualar largura de %1   
�     &   &Igualar altura de %1    
�     "   "Inserir objeto(s)   
�        Cortar %1   
�     8   8Colar dados da área de transferência    
�     &   &Arrastar e soltar %1    
�     *   *Inserir arrastar e soltar   
�     $   $Inserir ponto em %1   
�     0   0Inserir ponto de colagem em %1    
�     ,   ,Mover ponto de referência    
�     ,   ,Alterar geometricamente %1    
�        Mover %1    
�     "   "Redimensionar %1    
�        Girar %1    
�     *   *Inverter %1 na horizontal   
�     (   (Inverter %1 na vertical   
�     (   (Inverter %1 na diagonal   
�     *   *Inverter %1 à mão livre   
�     ,   ,Distorcer %1 (inclinação)   
�     &   &Dispor %1 em Círculo   
�     &   &Curvar %1 em Círculo   
�        Distorcer %1    
�     $   $Alterar raio em %1    
�        Alterar %1    
�     "   "Redimensionar %1    
�        Mover %1    
�     (   (Mover extremidade de %1   
�     &   &Ajustar ângulo em %1   
�        Alterar %1    
�     .   .Gradiente interativo para %1    
�     2   2Transparência interativa para %1   
�        Cortar %1   
�     B   BEditar texto: Parágrafo %1, Linha %2, Coluna %3    
�          %1 selecionado    
�        Ponto de %1   
�          %2 pontos de %1   
�     (   (Ponto de colagem de %1    
�     ,   ,%2 pontos de colagem de %1    
�          Marcar objetos    
�     *   *Marcar objetos adicionais   
�        Marcar pontos   
�     *   *Marcar pontos adicionais    
�     *   *Marcar pontos de colagem    
�     4   4Marcar pontos de colagem adicionais   
�        Criar %1    
�        Inserir %1    
�        Copiar %1   
�     .   .Alterar ordem do objeto de %1   
�     $   $Editar texto de %1               Inserir página             Excluir página             Copiar página         .   .Alterar a ordem das páginas         *   *Atribuir página de fundo        8   8Limpar atribuição da página de fundo        8   8Mover atribuição da página de fundo         :   :Alterar atribuição da página de fundo         "   "Inserir documento   	          Inserir camada    
          Excluir camada         ,   ,Alterar a ordem das camadas        4   4Alterar o nome de objeto de %1 para        2   2Alterar o título de objeto de %1        6   6Alterar a descrição de objeto de %1           Padrão           ativado           desativado            sim           Não            Tipo 1            Tipo 2            Tipo 3            Tipo 4            Horizontal            Vertical            Automático           Desativado            Proporcional         D   DAjustar ao tamanho (todas as linhas separadamente)         ,   ,Utilizar atributos rígidos           Em cima            No meio   !        Embaixo   "     (   (Utilizar toda a altura    #        Esticado    $        Esquerda    %        No meio   &        Direita   '     (   (Utilizar toda a largura   (        Esticado    )        desativado    *        piscar    +        Percorrer   ,        alternado   -     "   "Rolar para dentro   .          para a esquerda   /          para a direita    0        para cima   1        para baixo    2     "   "Conector padrão    3     $   $Conector de linhas    4        Conector reto   5          Conector curvo    6        Padrão   7        Raio    8        automático   9     "   "exterior esquerdo   :     (   (interior (centralizado)   ;     "   "exterior direito    <        automático   =        na linha    >          linha quebrada    ?          abaixo da linha   @        centralizado    A     "   "círculo completo   B        Disco   C     &   &Segmento de círculo    D        Arco    E        Sombra    F        Cor da sombra   G     .   .Contorno horizontal da sombra   H     ,   ,Contorno vertical da sombra   I     *   *Transparência da sombra    J        Sombra 3D   K     &   &Sombra em perspectiva   R     *   *Tipo de texto explicativo   S     "   "Ângulo fornecido   T        Ângulo   U        Folga   V     $   $Direção de saída   W     .   .Posição relativa de saída    X     $   $Posição de saída   Y     $   $Posição de saída   Z     &   &Comprimento da linha    [     2   2Comprimento automático da linha    c        Raio do canto   d     0   0Espaçamento da borda esquerda    e     .   .Espaçamento da borda direita   f     0   0Espaçamento da borda superior    g     0   0Espaçamento da borda inferior    h     .   .Autoajustar altura do quadro    i     *   *Altura mínima do quadro    j     *   *Altura máxima do quadro    k     .   .Autoajustar largura do quadro   l     *   *Largura mínima do quadro   m     *   *Largura máxima do quadro   n     *   *Âncora vertical do texto   o     ,   ,Âncora horizontal de texto   p     (   (Ajustar texto ao quadro   q        Vermelho    r        Verde   s        Azul    t        Brilho    u        Contraste   v        Gama    w          Transparência    x        Inverter    y     *   *Modo de objetos gráfico    z            {            |            }            ~                        �     "   "Vários atributos   �     $   $Posição protegida   �     &   &Proteção de tamanho   �        Não imprimir   �     $   $Indicador de camada   �        Ní~vel   �          Nome do objeto    �          Ângulo inicial   �        Ângulo final   �        Posição X   �        Posição Y   �        Largura   �        Altura    �     &   &Ângulo de rotação    �     (   (Ângulo de cisalhamento   �     &   &Atributo desconhecido   �          Estilo de linha   �     "   "Padrão da linha    �     "   "Largura da linha    �        Cor da linha    �     "   "Início da linha    �        Fim de linha    �     ,   ,Largura do início de linha   �     (   (Largura do fim de linha   �     .   .Início de linha centralizado   �     *   *Fim de linha centralizado   �     (   (Transparência da linha   �     "   "Junção da linha   �     (   (Linha reservada para 2    �     (   (Linha reservada para 3    �     (   (Linha reservada para 4    �     (   (Linha reservada para 5    �     (   (Linha reservada para 6    �     $   $Atributos da linha    �     (   (Estilo de preenchimento   �     &   &Cor do preenchimento    �        Gradiente   �        Hachuras    �     (   (Bitmap de preenchimento   �          Transparência    �     (   (Número de gradações    �     *   *Preenchimento lado a lado   �     6   6Posição do bitmap de preenchimento    �     4   4Largura do bitmap de preenchimento    �     2   2Altura do bitmap de preenchimento   �     (   (Gradiente transparente    �     0   0Preenchimento reservado para 2    �     :   :Tamanho do mosaico não se encontra em %    �     0   0Deslocamento X lado a lado em %   �     0   0Deslocamento Y lado a lado em %   �     *   *Dimensionamento do bitmap   �     (   (Bitmap reservado para 3   �     (   (Bitmap reservado para 4   �     (   (Bitmap reservado para 5   �     (   (Bitmap reservado para 6   �     (   (Bitmap reservado para 7   �     (   (Bitmap reservado para 8   �     .   .Posição X lado a lado em %    �     .   .Posição lado a lado Y em %    �     0   0Preenchimento de plano de fundo   �     0   0Preenchimento reservado para 10   �     0   0Preenchimento reservado para 11   �     0   0Preenchimento reservado para 12   �     $   $Atributos da área    �     $   $Estilo do Fontwork    �     (   (Alinhamento do Fontwork   �     *   *Espaçamento do Fontwork    �     *   *Início da fonte Fontwork   �     $   $Espelho do Fontwork   �     &   &Contorno do Fontwork    �     $   $Sombra do Fontwork    �     *   *Cor da sombra do Fontwork   �     6   6Deslocamento X da sombra do Fontwork    �     6   6Deslocamento Y da sombra do Fontwork    �     .   .Ocultar contorno do Fontwork    �     6   6Transparência da sombra do Fontwork    �     *   *Fontwork reservado para 2   �     *   *Fontwork reservado para 3   �     *   *Fontwork reservado para 4   �     *   *Fontwork reservado para 5   �     *   *Fontwork reservado para 6   �        Sombra    �        Cor da sombra   �     *   *Espaçamento X da sombra    �     *   *Espaçamento Y da sombra    �     *   *Transparência da sombra    �        Sombra 3D   �     &   &Sombra em perspectiva   �          Tipo de legenda   �     (   (Ângulo fixo da legenda   �     $   $Ângulo da legenda    �     2   2Espaçamento de linhas da legenda   �     2   2Alinhamento da saída da legenda    �     ,   ,Saída relativa da legenda    �     ,   ,Saída relativa da legenda    �     ,   ,Saída absoluta da legenda    �     0   0Comprimento da linha da legenda   �     <   <Comprimento automático da linha da legenda   �        Raio do canto   �     *   *Altura mínima do quadro    �     $   $Autoajustar altura    �     (   (Ajustar texto ao quadro   �     :   :Distância à esquerda do quadro de texto   �     :   :Distância à direita do quadro de texto    �     8   8Distância superior do quadro de texto    �     8   8Distância inferior do quadro de texto    �     *   *Âncora vertical do texto   �     *   *Altura máxima do quadro    �     *   *Largura mínima do quadro   �     *   *Largura máxima do quadro   �     $   $Autoajustar largura   �     ,   ,Âncora horizontal de texto            Animar         (   (Direção da animação        .   .Animação inicia do interior        0   0Animação termina no interior         6   6Número de execuções de animação         *   *Velocidade da animação         2   2Tamanho das etapas de animação         ,   ,Contorno do fluxo de texto         "   "Ajuste de formas    	     2   2Atributos definidos pelo usuário   
     F   FUtilizar espaçamento de linhas independente da fonte        0   0Disposição do texto à forma         D   DCrescer forma automaticamente para encaixar o texto        *   *SvDraw reservado para 18         *   *SvDraw reservado para 19         "   "Tipo de conector         4   4Espaçamento horizontal do objeto 1        2   2Espaçamento vertical do objeto 1        4   4Espaçamento horizontal do objeto 2        2   2Espaçamento vertical do objeto 2        4   4Espaçamento de colagem do objeto 1        4   4Espaçamento de colagem do objeto 2        *   *Número de linhas móveis        (   (Deslocamento da linha 1        (   (Deslocamento da linha 2        (   (Deslocamento da linha 3   $     "   "Tipo de cotação   %     6   6Valor da cota - posição horizontal    &     4   4Valor da cota - posição vertical    '     *   *Espaço da linha de cota    (     6   6Projeção da linha auxiliar da cota    )     8   8Espaçamento da linha auxiliar da cota    *     4   4Reserva da linha de cota auxiliar 1   +     4   4Reserva da linha de cota auxiliar 2   ,     .   .Cotação da aresta inferior    -     >   >Valor da cota perpendicular à linha de cota    .     2   2Girar valor da cota em 180 graus    /     ,   ,Projeção da linha de cota   0     "   "Unidade de medida   1     *   *Fator de escala adicional   2     0   0Exibição da unidade de medida   3     *   *Formato do valor da cota    4     <   <Posicionamento automático do valor da cota   5     L   LÂngulo para o posicionamento automático do valor da cota    6     <   <Determinação do ângulo do valor da cota    7     *   *Ângulo do valor da cota    8          Casas decimais    9     .   .Reservado para a cotação 5    :     .   .Reservado para a cotação 6    ;     .   .Reservado para a cotação 7    =     "   "Tipo de círculo    >          Ângulo inicial   ?        Ângulo final   @     *   *Círculo reservado para 0   A     *   *Círculo reservado para 1   B     *   *Círculo reservado para 2   C     *   *Círculo reservado para 3   D     "   "Objeto, visível    E     .   .Posição do objeto protegido   F     ,   ,Tamanho do objeto protegido   G     $   $Objeto, imprimível   H        ID do nível    I        Camada    J          Nome do objeto    K     &   &Posição X, completa   L     &   &Posição Y, completa   M        Largura total   N     "   "Altura, completa    O     (   (Posição X individual    P     (   (Posição Y individual    Q     $   $Largura individual    R     "   "Altura individual   S          Largura lógica   T          Altura lógica    U     0   0Ângulo de rotação individual   V     4   4Ângulo de cisalhamento individual    W     &   &Mover horizontalmente   X     $   $Mover verticalmente   Y     ,   ,Redimensionar X, individual   Z     ,   ,Redimensionar Y, individual   [     &   &Rotação individual    \     0   0Cisalhamento horizontal simples   ]     2   2Cisalhamento vertical individual    ^     *   *Redimensionar X, Completo   _     *   *Redimensionar Y, completo   `        Girar todos   a     2   2Cisalhamento horizontal, completo   b     0   0Cisalhamento vertical, completo   c     *   *Ponto de referência 1 X    d     *   *Ponto de referência 1 Y    e     *   *Ponto de referência 2 X    f     *   *Ponto de referência 2 Y    g        Hifenização   h     "   "Exibir marcadores   i     &   &Recuos de numeração   j     &   &Nível de numeração   k     *   *Marcadores e numerações   l        Recuos    m     ,   ,Espaçamento de parágrafo    n     (   (Espaçamento de linhas    o     *   *Alinhamento do parágrafo   p        Tabuladores   q        Cor da fonte    r     (   (Conjunto de caracteres    s     "   "Tamanho da fonte    t     "   "Largura da fonte    u     $   $Negrito (espessura)   v        Sublinhado    w        Sobrelinha    x        Tachado   y        Itálico    z        Contorno    {          Sombra da fonte   |     &   &Sobrescrito/subscrito   }        Kerning   ~          Kerning manual         .   .Sem sublinhado para espaços    �        Tabulador   �     *   *Quebra de linha opcional    �     ,   ,Caractere não conversível   �        Campos    �        Vermelho    �        Verde   �        Azul    �        Brilho    �        Contraste   �        Gama    �          Transparência    �        Inverter    �     *   *Modo de objetos gráfico    �        Aparar    �            �            �            �            �     ,   ,Aplicar atributos de tabela   �     $   $Autoformatar tabela   �          Inserir coluna    �        Inserir linha   �          Excluir coluna    �        Excluir linha   �          Dividir célula   �     "   "Mesclar células    �     "   "Formatar célula    �     "   "Distribuir linhas   �     $   $Distribuir colunas    �     "   "Estilo da tabela    �     4   4Configurações do estilo da tabela   �     .   .Excluir conteúdo da célula    �     6   6Próximo vínculo na cadeia de texto    '        [Todos]   'W     0   0O fim da planilha foi atingido    'Y        Favorito    'Z        X   '[        Y   '\        Z   ']        R:    '^        G:    '_     0   0O fim do documento foi atingido   '`          Incluir estilos   'a        (Localizar)   'b        (Substituir)    'c     2   2Pesquisar est~ilos de parágrafos   'd        B:    'e     0   0Pesquisar est~ilos de células    'h     2   2Chave de pesquisa não encontrada   'i     l   lTem certeza de que deseja descartar os dados de recuperação de documento do %PRODUCTNAME?   'j     2   2Alcançado o início do documento   '�        Contínuo   '�        Gradiente   '�        Bitmap    '�          Estilo de linha   '�        Nenhum    '�        Cor   '�        Hachuras    '�        - nenhum -    '�     "   "Sem preenchimento   '�        Padrão   '�        Bordas    '�        Cor da borda    '�          Estilo da borda   '�        Cor de realce   '�     $   $Limpar formatação   '�     $   $Outras opções...    '�     Z   ZNome da fonte. A fonte atual não está disponível e será substituída.   '�        Nome da fonte   '�        Cor da linha    '�     "   "Outros estilos...   '�     &   &Cor do preenchimento    '�     (   (Outras numerações...    '�     &   &Outros marcadores...    '�          Paleta padrão    '�     $   $Cores do documento    '�     "   "Cor do documento    '�     R   RModo de inserção. Clique para mudar para o modo de sobrescrita.   '�     R   RModo de sobrescrita. Clique para mudar para o modo de inserção.   '�        Sobrescrever    '�     H   HAssinatura digital: A assinatura do documento está OK.   '�     x   xAssinatura digital: A assinatura do documento está OK, mas não foi possível validar os certificados.   '�     �   �Assinatura digital: A assinatura do documento não corresponde ao conteúdo do documento. Recomendamos enfaticamente que não confie neste documento.   '�     F   FAssinatura digital: O documento não está assinado.    '�     �   �Assinatura digital: A assinatura digital do documento e o certificado estão corretos, mas nem todas as partes do documento estão assinadas.   '�     "   "Pontas das setas    (        Esquerda    (        Direita   (        Centro    (        Decimal   (
        Usuário    (          Tema da galeria   (        Itens do tema   (        Visualizar    (        Fechar    (#        Preto   ($        Azul    (%        Verde   (&        Ciano   ('        Vermelho    ((        Magenta   (*        Cinza   (1        Amarelo   (2        Branco    (3        Cinza azulado   (4        Laranja   (5        Turquesa    (6        Turquoise   (7          Azul clássico    (8        Blue classic    (<        Seta    (=        Quadrado    (>        Círculo    (A          Transparência    (B        Centralizado    (C     "   "Não centralizado   (D        Lista   (E        Filtro    (�        Transparente    (�        Cor de origem   (�          Paleta de cores   (�        Tolerância   (�          Substituir por    (�     "   "Inserir objeto(s)   (�     .   .Criar objeto de rotação 3D    (�     &   &Número de segmentos    (�     (   (Profundidade do objeto    (�     "   "Distância focal    (�     &   &Posição da câmera    (�          Girar objeto 3D   )      *   *Criar objeto em extrusão   )     *   *Criar objeto de rotação   )     "   "Dividir objeto 3D   )        Atributos 3D    )        Padrão   )          Escala de cinza   )        Preto/Branco    )        Marca d'água   )     ,   ,Vídeo Intel Indeo (*.ivf)    )     ,   ,Vídeo para Windows (*.avi)   )     *   *Vídeo QuickTime (*.mov)    )     J   JMPEG - Motion Pictures Experts Group (*.mpe;*.mpeg;*.mpg)   )         <Todos>   )!          Inserir áudio    )"          Inserir vídeo    )#          Plano de fundo    ),        Violeta   )-        Bordô    ).        Amarelo claro   )/        Verde claro   )0          Violeta escuro    )1        Salmão   )2        Azul-marinho    )4     8   8Verde 1 (cor principal do %PRODUCTNAME)   )5          Verde acentuado   )6          Azul acentuado    )7     "   "Laranja acentuado   )8        Púrpura    )9     $   $Púrpura acentuado    ):     "   "Amarelo acentuado   )@        3D    )A        Preto 1   )B        Preto 2   )C        Azul    )D        Marrom    )E        Moeda   )F        Moeda 3D    )G        Moeda Cinza   )H        Moeda Lavanda   )I          Moeda Turquesa    )J        Cinza   )K        Verde   )L        Lavanda   )M        Vermelho    )N        Turquesa    )O        Amarelo   )Z     "   "Extremidade plana   )[     (   (Extremidade arredondada   )\     &   &Extremidade quadrada    )]     ,   ,Média da junção de linha   )^     .   .Moldura da junção de linha    )_     .   .Ângulo da junção de linha    )`     .   .Junção de linha arredondada   )c        Black   )d        Blue    )e        Green   )f        Cyan    )g        Red   )h        Magenta   )j        Gray    )q        Yellow    )r        White   )s        Blue gray   )t        Orange    )u        Violet    )v        Bordeaux    )w        Pale yellow   )x        Pale green    )y        Dark violet   )z        Salmon    ){        Sea blue    )|        Sun   )}        Gráfico    )~        Chart   )        Púrpura    )�        Purple    )�        Azul celeste    )�        Sky blue    )�        Amarelo verde   )�        Yellow green    )�        Cor de rosa   )�        Pink    )�     2   2Green 1 (%PRODUCTNAME Main Color)   )�        Green Accent    )�        Blue Accent   )�        Orange Accent   )�        Purple    )�        Purple Accent   )�        Yellow Accent   )�          Tango: Manteiga   )�          Tango: Laranja    )�     "   "Tango: Chocolate    )�     "   "Tango: Camaleão    )�     $   $Tango: Azul celeste   )�        Tango: Ameixa   )�     *   *Tango: Vermelho escarlate   )�     "   "Tango: Alumínio    )�        Tango: Butter   )�        Tango: Orange   )�     "   "Tango: Chocolate    )�     "   "Tango: Chameleon    )�          Tango: Sky Blue   )�        Tango: Plum   )�     $   $Tango: Scarlet Red    )�     "   "Tango: Aluminium    )�     &   &Black 45 Degrees Wide   )�     "   "Black 45 Degrees    )�     "   "Black -45 Degrees   )�     "   "Black 90 Degrees    )�     (   (Red Crossed 45 Degrees    )�     &   &Red Crossed 0 Degrees   )�     (   (Blue Crossed 45 Degrees   )�     (   (Blue Crossed 0 Degrees    )�     (   (Blue Triple 90 Degrees    )�          Black 0 Degrees   )�        Hatching    )�     *   *Preto 45 graus de largura   )�          Preto 45 graus    )�          Preto -45 graus   )�          Preto 90 graus    )�     *   *Vermelho cruzado 45 graus   )�     (   (Vermelho cruzado 0 grau   )�     &   &Azul cruzado 45 graus   )�     $   $Azul cruzado 0 grau   )�     &   &Azul triplo 90 graus    )�        Preto 0 grau    )�        Hachuras    )�        Empty   )�        Sky   )�        Aqua    )�        Coarse    )�        Space Metal   )�        Space   )�        Metal   )�        Wet   )�        Marble    )�        Linen   )�        Stone   )�        Pebbles   )�        Wall    )�        Red Wall    )�        Pattern   )�        Leaves    )�          Lawn Artificial   )�        Daisy   )�        Orange    )�        Fiery   )�        Roses   )�        Bitmap    )�        Em branco   )�        Céu    )�        Água   )�     "   "Granulado grosso    )�        Mercúrio   )�        Espaço   )�        Metal   )�        Gotas   )�        Mármore    )�        Linho   )�        Pedra   )�        Cascalho    )�        Parede    )�        Tijolo    )�        Rede    )�        Folhas    )�     $   $Gramado artificial    )�        Margarida   )�        Laranja   )�        Chamas    )�        Rosas   )�        Bitmap    )�     "   "Ultrafine Dashed    )�        Fine Dashed   )�     *   *Ultrafine 2 Dots 3 Dashes   )�        Fine Dotted   )�     $   $Line with Fine Dots   )�     "   "Fine Dashed (var)   )�     &   &3 Dashes 3 Dots (var)   )�     (   (Ultrafine Dotted (var)    )�        Line Style 9    )�        2 Dots 1 Dash   )�        Dashed (var)    )�        Dash    *         Line Style    *     &   &Tracejado ultra fino    *          Tracejado fino    *     $   $2 pontos 3 traços    *          Pontilhado fino   *     (   (Linha com pontos finos    *          Tracejado fino    *     $   $3 traços 3 pontos    *     &   &Pontilhado ultra fino   *	     "   "Estilo de linha 9   *
     "   "2 pontos 1 traço   *        Tracejado   *        Tracejado   *          Estilo de linha   *2     (   (Seleção de impressão   *3     D   DDeseja imprimir a seleção ou o documento inteiro?   *4        ~Todo   *5        ~Seleção    *D        Aparar    *E          Modo de imagem    *F        Vermelho    *G        Verde   *H        Azul    *I        Brilho    *J        Contraste   *K        Gama    *L          Transparência    *Y        Automática   *^     ,   ,Ações a desfazer: $(ARG1)   *_     ,   ,Ações a refazer: $(ARG1)    *`     ,   ,Ações a desfazer: $(ARG1)   *a     ,   ,Ações a refazer: $(ARG1)    *b        Transparency    *c          Transparência    *m     $   $Cor de material 3D    *n        Cor da fonte    *o     &   &Cor do plano de fundo   *p        Nenhum    *q        Sólido   *r        Com hachuras    *s        Gradiente   *t        Bitmap    *u        com   *v        Estilo    *w        e   *�     "   "Controle de canto   *�     0   0Seleção de um ponto do canto.   *�     $   $Controle de ângulo   *�     2   2Seleção de um ângulo superior.   *�     "   "Superior esquerda   *�          Centro superior   *�     "   "Superior direita    *�     $   $Centro à esquerda    *�        Centro    *�     "   "Centro à direita   *�     "   "Inferior esquerda   *�          Centro inferior   *�     "   "Direita inferior    *�        0 grau    *�        45 graus    *�        90 graus    *�        135 graus   *�        180 graus   *�        225 graus   *�        270 graus   *�        315 graus   *�     &   &Controle de contorno    *�     4   4Aqui você pode editar o contorno.    *�     2   2Seleção de caracteres especiais   *�     &   &Código do caractere    *�     @   @Selecione os caracteres especiais nesta área.    *�          Exportar imagem   *�        Extrusão   *�        Fontwork    *�     "   "Cor da extrusão    *�        ~0 cm   *�        ~1 cm   *�        ~2,5 cm   *�        ~5 cm   *�        10 ~cm    *�        0 pol   *�        0,~5 pol    *�        ~1 pol    *�        ~2 pol    *�        ~4 pol    *�        Páginas    +n     *   *Aplicar forma do Fontwork   +o     @   @Aplicar as mesmas alturas de letras do Fontwork   +p     0   0Aplicar alinhamento do Fontwork   +r     @   @Aplicar espaçamento de caracteres do Fontwork    +s     :   :Ativar/Desativar aplicação de extrusão   +t     $   $Inclinar para baixo   +u     $   $Inclinar para cima    +v     *   *Inclinar para a esquerda    +w     (   (Inclinar para a direita   +x     2   2Alterar profundidade da extrusão   +y     &   &Alterar orientação    +z     ,   ,Alterar tipo de projeção    +{     &   &Alterar iluminação    +|          Alterar brilho    +}     2   2Alterar superfície da extrusão    +~     *   *Alterar cor da extrusão    +�     8   8Marcadores circulares pequenos sólidos   +�     8   8Marcadores circulares grandes sólidos    +�     0   0Marcadores em losango sólidas    +�     6   6Marcadores quadrados grandes sólidos   +�     6   6Marcadores de setas à direita vazias   +�     8   8Marcadores em forma de seta à direita    +�     :   :Marcadores em forma de caixa de seleção   +�     .   .Marcadores em forma de tique    +�     "   "Número 1) 2) 3)    +�     "   "Número 1. 2. 3.    +�     $   $Número (1) (2) (3)   +�     :   :Números romanos maiúsculos I. II. III.    +�     *   *Letra maiúscula A) B) C)   +�     *   *Letra minúscula a) b) c)   +�     .   .Letra minúscula (a) (b) (c)    +�     :   :Números romanos minúsculos i. ii. iii.    +�     Z   ZNumérico, numérico, letras minúsculas, marca circular pequena sólida    +�     N   NNumérico, letras minúsculas, marca circular pequena sólida   +�     x   xNumérico, letras minúsculas, romanos minúsculos, letras maiúsculas, marca circular pequena sólida    +�        Numérico   +�     �   �Romanos maiúsculos, letras maiúsculas, romanos minúsculos, letras minúsculas, marca circular pequena sólida    +�     �   �Letras maiúsculas, romanos maiúsculos, letras minúsculas, romanos minúsculos, marca circular pequena sólida    +�     2   2Numérico com todos os subníveis   +�     ~   ~Marca à direita, marca em forma de seta à direita, marca em losango sólida, marca circular pequena sólida   +�     .   .Estilos de modelos de tabela    +�        Cor da fonte    +�     *   *Pesquisar valor formatado   +�     8   8Diferenciar maiúsculas de minúsculas    +�        Localizar   +�     \   \O documento foi modificado. Faça um clique duplo para salvar o documento.    +�     T   TO documento não foi modificado desde a última vez que foi salvo.    +�     (   (Carregando documento...   +�          Senha inválida   +�     ,   ,Senhas não correspondentes   +�     0   0Ajustar slide à janela atual.    ,     (   (Recuperado com sucesso    ,     .   .Documento original recuperado   ,     (   (Falha na recuperação    ,     ,   ,Recuperação em andamento    ,     &   &Não recuperado ainda   ,     �   �O %PRODUCTNAME %PRODUCTVERSION iniciará a recuperação de seus documentos. Dependendo do tamanho dos documentos, esse processo pode levar algum tempo.    ,     j   jA recuperação dos documentos foi concluída.
Clique 'Concluir' para ver os documentos.    ,        ~Concluir   ,     |   |Escala do zoom. Clique no botão direito para mudar o zoom ou clique para abrir a caixa de diálogo do Zoom   ,        Mais zoom   ,        Menos zoom    ,     "   "~Personalizado...   ,         ~Infinito   ,!        A~rame    ,"        ~Fosco    ,#        ~Plástico    ,$        Me~tal    ,%          ~Muito apertado   ,&        ~Apertado   ,'        ~Normal   ,(        ~Folgado    ,)          Muito ~folgado    ,*     "   "~Personalizar...    ,+     0   0~Kerning em pares de caracteres   ,8     (   (Extrusão para noroeste   ,9     (   (Extrusão para o norte    ,:     (   (Extrusão para nordeste   ,;     &   &Extrusão para oeste    ,<     &   &Extrusão para trás    ,=     &   &Extrusão para leste    ,>     (   (Extrusão para sudoeste   ,?     &   &Extrusão para o sul    ,@     (   (Extrusão para sudeste    ,B        ~Perspectiva    ,C        P~aralelo   ,D        ~Brilhante    ,E        ~Normal   ,F        ~Esmaecido    ,h     &   &~Alinhar à esquerda    ,i        ~Centralizar    ,j     $   $~Alinhar à direita   ,k        ~Justificar   ,l     (   (Esticar jus~tificação   ,v        25%   ,w        50%   ,x        75%   ,y        100%    ,z        150%    ,{        200%    ,|          Página inteira   ,}     $   $Largura da página    ,~     "   "Exibição ideal    .�        Gradient    .�     "   "Linear blue/white   .�     &   &Linear magenta/green    .�     $   $Linear yellow/brown   .�     $   $Radial green/black    .�     "   "Radial red/yellow   .�     &   &Rectangular red/white   .�     $   $Square yellow/white   .�     0   0Ellipsoid blue grey/light blue    .�     &   &Axial light red/white   .�        Diagonal 1l   .�        Diagonal 1r   .�        Diagonal 2l   .�        Diagonal 2r   .�        Diagonal 3l   .�        Diagonal 3r   .�        Diagonal 4l   .�        Diagonal 4r   .�        Diagonal Blue   .�          Diagonal Green    .�          Diagonal Orange   .�        Diagonal Red    .�     $   $Diagonal Turquoise    .�          Diagonal Violet   .�        From a Corner   .�     $   $From a Corner, Blue   .�     &   &From a Corner, Green    .�     &   &From a Corner, Orange   .�     $   $From a Corner, Red    .�     *   *From a Corner, Turquoise    .�     &   &From a Corner, Violet   .�          From the Middle   /      &   &From the Middle, Blue   /     (   (From the Middle, Green    /     (   (From the Middle, Orange   /     &   &From the Middle, Red    /     ,   ,From the Middle, Turquoise    /     (   (From the Middle, Violet   /        Horizontal    /          Horizontal Blue   /     "   "Horizontal Green    /	     "   "Horizontal Orange   /
          Horizontal Red    /     &   &Horizontal Turquoise    /     "   "Horizontal Violet   /        Radial    /        Radial Blue   /        Radial Green    /        Radial Orange   /        Radial Red    /     "   "Radial Turquoise    /        Radial Violet   /        Vertical    /        Vertical Blue   /          Vertical Green    /          Vertical Orange   /        Vertical Red    /     $   $Vertical Turquoise    /          Vertical Violet   /        Gray Gradient   /          Yellow Gradient   /          Orange Gradient   /        Red Gradient    /        Pink Gradient   /         Sky   /!        Cyan Gradient   /"        Blue Gradient   /#        Purple Pipe   /$        Night   /%          Green Gradient    /&        Tango Green   /'     $   $Subtle Tango Green    /(        Tango Purple    /)        Tango Red   /*        Tango Blue    /+        Tango Yellow    /,        Tango Orange    /-        Tango Gray    /.        Argile    //        Olive Green   /0        Grison    /1        Sunburst    /2        Brownie   /3        Sun-Set   /4        Deep Green    /5        Deep Orange   /6        Deep Blue   /7        Purple Haze   /D        Gradiente   /E     $   $Azul/branco linear    /F     &   &Magenta/verde linear    /G     &   &Amarelo/marrom linear   /H     $   $Verde/preto radial    /I     (   (Vermelho/amarelo radial   /J     ,   ,Vermelho/branco retangular    /K     (   (Quadrado amarelo/branco   /L     4   4Cinza azulado/azul claro elipsoidal   /M     ,   ,Vermelho claro/branco axial   /N        Diagonal 1e   /O        Diagonal 1d   /P        Diagonal 2e   /Q        Diagonal 2d   /R        Diagonal 3e   /S        Diagonal 3d   /T        Diagonal 4e   /U        Diagonal 4d   /V        Diagonal azul   /W          Diagonal verde    /X     "   "Diagonal laranja    /Y     "   "Diagonal vermelha   /Z     "   "Diagonal turquesa   /[     "   "Diagonal violeta    /\        De um canto   /]     "   "De um canto, azul   /^     $   $De um canto, verde    /_     &   &De um canto, laranja    /`     &   &De um canto, vermelho   /a     &   &De um canto, turquesa   /b     &   &De um canto, violeta    /c        Do meio   /d        Do meio, azul   /e          Do meio, verde    /f     "   "Do meio, laranja    /g     "   "Do meio, vermelho   /h     "   "Do meio, turquesa   /i     "   "Do meio, violeta    /j        Horizontal    /k          Horizontal azul   /l     "   "Horizontal verde    /m     $   $Horizontal laranja    /n     $   $Horizontal vermelho   /o     $   $Horizontal turquesa   /p     $   $Horizontal violeta    /q        Radial    /r        Radial azul   /s        Radial verde    /t          Radial laranja    /u          Radial vermelho   /v          Radial turquesa   /w          Radial violeta    /x        Vertical    /y        Vertical azul   /z          Vertical verde    /{     "   "Vertical laranja    /|     "   "Vertical vermelho   /}     "   "Vertical turquesa   /~     "   "Vertical violeta    /     $   $Gradiente de cinza    /�     &   &Gradiente de amarelo    /�     &   &Gradiente de laranja    /�     &   &Gradiente de vermelho   /�     "   "Gradiente de rosa   /�        Céu    /�     $   $Gradiente de ciano    /�     "   "Gradiente de azul   /�        Tubo púrpura   /�        Noite   /�     $   $Gradiente de verde    /�        Tango verde   /�     "   "Tango verde sutil   /�          Tango púrpura    /�          Tango vermelho    /�        Tango azul    /�        Tango amarelo   /�        Tango laranja   /�        Tango cinza   /�        Argila    /�        Verde oliva   /�        Prata   /�        Raios solares   /�        Brownie   /�        Crepúsculo   /�          Verde profundo    /�     "   "Laranja profundo    /�        Azul profundo   /�          Névoa púrpura   1�        Arrow concave   1�        Square 45   1�        Small Arrow   1�          Dimension Lines   1�        Double Arrow    1�     $   $Rounded short Arrow   1�          Symmetric Arrow   1�        Line Arrow    1�     $   $Rounded large Arrow   1�        Circle    1�        Square    1�        Arrow   1�     "   "Short line Arrow    1�     "   "Triangle unfilled   1�     "   "Diamond unfilled    1�        Diamond   1�          Circle unfilled   1�     $   $Square 45 unfilled    1�          Square unfilled   1�     &   &Half Circle unfilled    1�        Arrowhead   1�        Seta côncava   1�        Quadrado 45   1�        Seta pequena    1�          Linhas de cota    1�        Seta dupla    1�     (   (Seta curta arredondada    1�          Seta simétrica   1�        Seta de linha   1�     (   (Seta grande arredondada   2         Círculo    2        Quadrado    2        Seta    2        Seta curta    2     ,   ,Triângulo não preenchido    2     (   (Losango não preenchido   2        Losango   2     *   *Círculo não preenchido    2     ,   ,Quadrado 45 não preenchido   2	     *   *Quadrado não preenchido    2
     .   .Semicírculo não preenchido    2     "   "Pontas das setas    :h        Cor da fonte    :p        Pesquisar   :q          Localizar todos   :r        Substituir    :s     "   "Substituir todos    :t     $   $Estilo do caractere   :u     &   &Estilo do parágrafo    :v     "   "Estilo do quadro    :w     "   "Estilo de página   :z        Fórmula    :{        Valor   :|        Nota    :        StarWriter    :�        StarCalc    :�        StarDraw    :�        StarBase    :�        Nenhum    :�        Sólido   :�        Horizontal    :�        Vertical    :�        Grade   :�        Losango   :�     $   $Diagonal para cima    :�     $   $Diagonal para baixo   :�        25%   :�        50%   :�        75%   :�        Imagem    <      &   &Orientação padrão    <     $   $De cima para baixo    <     $   $De baixo para cima    <        Empilhado   <!        Tabela    <"        Sem tabela    <#     &   &Espaçamento ativado    <$     (   (Espaçamento desativado   <%     2   2Manter intervalo de espaçamento    <&     @   @Permitida a falta de intervalos de espaçamento   <F     "   "Margem esquerda:    <G     "   "Margem superior:    <H     "   "Margem direita:     <I     "   "Margem inferior:    <X     *   *Descrição da página:     <Y        Maiúsculas   <Z        Minúsculas   <[     "   "Romano maiúsculo   <\     "   "Romano minúsculo   <]        Arábico    <^        Nenhum    <_        Paisagem    <`        Retrato   <a        Esquerda    <b        Direita   <c        Todos   <d        Espelhado   <o        Autor:    <p        Data:     <q        Texto:    <r     (   (Cor do plano de fundo:    <s     "   "Cor do padrão:     <u     ,   ,Plano de fundo do caractere   C          Paleta de cores   C        Alternar    FQ     H   HO nome '%1' não é válido em XML. Digite outro nome.    FR     N   NO prefixo '%1' não é válido em XML. Digite outro prefixo.    FS     >   >O nome '%1' já existe. Digite um novo nome.    FT     *   *O envio deve ter um nome.   FU     �   �Ao excluir a associação '$BINDINGNAME', todos os controles associados a ela no momento serão afetados.

Deseja realmente excluir essa associação?    FV     �   �Ao excluir o envio '$SUBMISSIONNAME', todos os controles vinculados no momento a esse envio serão afetados.

Deseja realmente excluir esse envio?    FW     F   FDeseja realmente excluir o atributo '$ATTRIBUTENAME'?   FX     �   �Ao excluir o elemento '$ELEMENTNAME', todos os controles vinculados no momento ao elemento serão afetados.
Deseja realmente excluir esse elemento?   FY     �   �Ao excluir a instância '$INSTANCENAME', todos os controles vinculados no momento a essa instância serão afetados.
Deseja realmente excluir essa instância?    F[        Formulário   F\        Registro    F]        de    F^     (   (Definir propriedade '#'   F_     *   *Inserir em um recipiente    F`        Excluir #   Fk     "   "Excluir # objetos   Fl     6   6Substituir um elemento do recipiente    Fn     "   "Excluir estrutura   Fo     $   $Substituir controle   Fp     &   &Barra de navegação    Ft        Formulário   Fu     "   "Adicionar campo:    Fv     ,   ,Nenhum controle selecionado   Fw          Propriedades:     Fx     ,   ,Propriedades do formulário   Fy     *   *Navegador de formulários   Fz        Formulários    F{     :   :Erro ao gravar os dados no banco de dados   F|     4   4Você pretende excluir 1 registro.    F}     n   nAo clicar em Sim, você não poderá desfazer esta operação.
Deseja continuar assim mesmo?    F~     $   $Elemento de quadro    F        Navegação   F�        Col   F�        Data    F�        Hora    F�     &   &Barra de navegação    F�     $   $Botão de pressão    F�     "   "Botão de opção   F�     $   $Caixa de seleção    F�     "   "Campo de rótulo    F�          Caixa de grupo    F�          Caixa de texto    F�     "   "Caixa de listagem   F�     &   &Caixa de combinação   F�     "   "Botão de imagem    F�     $   $Controle de imagem    F�     &   &Seleção de arquivo    F�        Campo de data   F�        Campo de hora   F�          Campo numérico   F�     "   "Campo monetário    F�     "   "Campo de padrão    F�     $   $Controle de tabela    F�     $   $Seleção múltipla   F�     0   0# registros serão excluídos.    F�        Controle    F�         (Data)   F�         (Hora)   F�     F   FSem controles relativos a dados no formulário atual!   F�     &   &Navegador de filtros    F�        Filtrar para    F�        Ou    F�          Campo formatado   F�     :   :Erro de sintaxe na expressão da consulta   F�     �   �Ao excluir o modelo '$MODELNAME', todos os controles vinculados no momento ao modelo serão afetados.
Deseja realmente excluir esse modelo?   F�     �   �Não existem no formulário atual, controles vinculados válidos que possam ser utilizados na exibição da tabela.   F�     $   $<Campo automático>   F�     4   4Erro de sintaxe na instrução SQL    F�     :   :O valor #1 não pode ser usado com LIKE.    F�     >   >LIKE não pode ser utilizado com este campo.    F�     R   RNão foi possível comparar o critério inserido com este campo.    F�     L   LNão é possível comparar o campo com um número inteiro.    F�     z   zO valor inserido não é uma data válida. Insira uma data em um formato válido, por exemplo, DD/MM/AA.    F�     V   VNão é possível comparar o campo com um número de ponto flutuante.   F�     N   NO banco de dados não contém nenhuma tabela com o nome "#".    F�     <   <A tabela "#2" não reconhece a coluna "#1".   F�     "   "Barra de rolagem    F�     "   "Botão giratório   F�          Controle oculto   F�        Elemento    F�        Atributo    F�        Associação    F�     ,   ,Expressão de associação    F�        Post    F�        Put   F�        Get   F�        Nenhum    F�        Instância    F�        Documento   F�     $   $Navegador de dados    F�        Envio:    F�        ID:     F�        Ação:     F�        Método:    F�        Referência:    F�          Associação:     F�        Substituir:     F�     $   $Adicionar elemento    F�          Editar elemento   F�     "   "Excluir elemento    F�     $   $Adicionar atributo    F�          Editar atributo   F�     "   "Excluir atributo    F�     (   (Adicionar associação    F�     $   $Editar associação   F�     &   &Excluir associação    F�          Adicionar envio   F�        Editar envio    F�        Excluir envio   F�     Z   ZO banco de dados não contém nenhuma tabela ou consulta com o nome "#".    F�     V   VO banco de dados já contém um tabela ou exibição com o nome "#".    F�     J   JO banco de dados já contém uma consulta com o nome "#".   F�     $   $ (somente-leitura)    F�     4   4O arquivo já existe. Sobrescrever?   F�     $   $Rótulo de #object#   H�     ,   ,Erro ao criar o formulário   H�     :   :A entrada já existe.
Escolha outro nome.   H�     D   DEntrada obrigatória no campo '#'. Insira um valor.   H�     h   hEscolha uma entrada da lista ou insira um texto correspondente a um dos itens da lista.   (G  y   �   �   Milímetro     Centímetro    Metro    Quilômetro    Polegada     Pé    	Milhas     
Pica     Ponto    Caractere    Linha      (H  y  
@  
@   NEuropa ocidental (Windows-1252/WinLatin 1)     Europa ocidental (Apple Macintosh)     Europa ocidental (DOS/OS2-850/Internacional)     Europa ocidental (DOS/OS2-437/Inglês norte-americano)     Europa ocidental (DOS/OS2-860/Português)    Europa ocidental (DOS/OS2-861/Islandês)     Europa ocidental (DOS/OS2-863/Francês (Can.))     Europa ocidental (DOS/OS2-865/Nórdico)    Europa ocidental (ASCII/Inglês norte-americano)     Europa ocidental (ISO-8859-1)    Europa oriental (ISO-8859-2)     Latim 3 (ISO-8859-3)     Báltico (ISO-8859-4)    Cirílico (ISO-8859-5)     Árabe (ISO-8859-6)    Grego (ISO-8859-7)     Hebraico (ISO-8859-8)    Turco (ISO-8859-9)     Europa ocidental (ISO-8859-14)     Europa ocidental (ISO-8859-15/EURO)    Grego (DOS/OS2-737)    Báltico (DOS/OS2-775)     Europa oriental (DOS/OS2-852)    Cirílico (DOS/OS2-855)    Turco (DOS/OS2-857)    Hebraico (DOS/OS2-862)     Árabe (DOS/OS2-864)     Cirílico (DOS/OS2-866/Russo)    Grego (DOS/OS2-869/Moderno)    Europa oriental (Windows-1250/WinLatin 2)    !Cirílico (Windows-1251)     "Grego (Windows-1253)     #Turco (Windows-1254)     $Hebraico (Windows-1255)    %Árabe (Windows-1256)    &Báltico (Windows-1257)    'Vietnamita (Windows-1258)    (Europa oriental (Apple Macintosh)    *Europa oriental (Apple Macintosh/Croata)     +Cirílico (Apple Macintosh)    ,Grego (Apple Macintosh)    /Europa ocidental (Apple Macintosh/Islandês)     3Europa Ocidental (Apple Macintosh/Romeno)    4Turco (Apple Macintosh)    6Cirílico (Apple Macintosh/Ucraniano)    7Chinês simplificado (Apple Macintosh)     8Chinês tradicional (Apple Macintosh)    9Japonês (Apple Macintosh)     :Coreano (Apple Macintosh)    ;Japonês (Windows-932)     <Chinês simplificado (Windows-936)     =Coreano (Windows-949)    >Chinês tradicional (Windows-950)    ?Japonês (Shift-JIS)     @Chinês simplificado (GB-2312)     AChinês simplificado (GB-18030)    UChinês tradicional (GBT-12345)    BChinês simplificado (GBK/GB-2312-80)    CChinês tradicional (Big5)     DChinês tradicional (BIG5-HKSCS)     VJaponês (EUC-JP)    EChinês simplificado (EUC-CN)    FChinês tradicional (EUC-TW)     GJaponês (ISO-2022-JP)     HChinês simplificado (ISO-2022-CN)     ICirílico (KOI8-R)     JUnicode (UTF-7)    KUnicode (UTF-8)    LEuropa oriental (ISO-8859-10)    MEuropa oriental (ISO-8859-13)    NCoreano (EUC-KR)     OCoreano (ISO-2022-KR)    PCoreano (Windows-Johab-1361)     TUnicode (UTF-16)    ��Tailandês (ISO-8859-11/TIS-620)     WTailandês (Windows-874)      Cirílico (KOI8-U)     XCirílico (PT154)    ]  <v  y  (  (   ;Escala    'Pincel    'Paradas de tabulação    'Caractere   'Fonte   'Postura da fonte    'Espessura da fonte    'Sombreado   'Palavras individuais    'Contorno    'Tachado   'Sublinhado    'Tamanho da fonte    'Tamanho relativo da fonte   ' Cor da fonte    '!Kerning   '"Efeitos   '#Idioma    '$Posição   '%Caractere piscante    'SCor do conjunto de caracteres   *}Sobrelinha    -0Parágrafo    '*Alinhamento   '+Espaçamento de linhas    '1Quebra de página   '5Hifenização   '6Não dividir parágrafo   '7Viúvas   '8Órfãs   '9Espaçamento entre parágrafos    ':Recuo de parágrafo   ';Recuo   '@Espaçamento    'APágina   'BEstilo de página   'QManter com o próximo parágrafo    'RIntermitente    (�Registro de conformidade    (�Plano de fundo do caractere   )_Fonte asiática   *�Tamanho da fonte asiática    *�Idioma das fontes asiáticas    *�Postura das fontes asiáticas   *�Espessura de fontes asiáticas    *�CTL   *�Tamanho dos scripts complexos   *�Idioma dos scripts complexos    *�Postura dos scripts complexos   *�Espessura dos scripts complexos   *�Linha dupla   *�Marca de ênfase    *�Espaçamento do texto   *�Pontuação suspensa    *�Caracteres proibidos    *�Rotação   *�Escalonamento de caracteres   *�Relevo    *�Alinhamento de texto vertical   *�  �     *   *      res/grafikei.png    �     *   *      res/grafikde.png    �     ,   ,      svx/res/markers.png   �     4   4      svx/res/pageshadow35x35.png   �     (   (      res/oleobj.png    �     0   0      svx/res/cropmarkers.png   '�     .   .      svx/res/rectbtns.png    '�     ,   ,      svx/res/objects.png   '�     (   (      	svx/res/ole.png   '�     ,   ,      
svx/res/graphic.png   'S  #   f   f            8   8      svx/res/slidezoombutton_10.png              ��  ��      'T  #   b   b            4   4      svx/res/slidezoomout_10.png             ��  ��      'U  #   b   b            4   4      svx/res/slidezoomin_10.png              ��  ��      'e  #   V   V            (   (      res/sc10223.png             ��  ��      'f  #   V   V            (   (      res/sc10224.png             ��  ��      'g  #   `   `            2   2      svx/res/signet_11x16.png              ��  ��      'i  #   `   `            2   2      svx/res/caution_11x16.png             ��  ��      'k  #   d   d            6   6      svx/res/notcertificate_16.png             ��  ��      '�  #   Z   Z            ,   ,      svx/res/lighton.png             ��  ��      '�  #   X   X            *   *      svx/res/light.png             ��  ��      '�  #   \   \            .   .      svx/res/colordlg.png              ��  ��      '�  #   H   H            4   4      svx/res/selection_10x22.png   (  #   \   \            .   .      svx/res/notcheck.png              ��  ��      (  #   \   \            .   .      svx/res/lngcheck.png              ��  ��      (�  #   V   V            (   (      res/sc10865.png             ��  ��      (�  #   V   V            (   (      res/sc10866.png             ��  ��      (�  #   V   V            (   (      res/sc10867.png             ��  ��      (�  #   V   V            (   (      res/sc10863.png             ��  ��      (�  #   V   V            (   (      res/sc10864.png             ��  ��      (�  #   V   V            (   (      res/sc10868.png             ��  ��      (�  #   V   V            (   (      res/sc10869.png             ��  ��      (�  #   V   V            (   (       res/reload.png              ��  ��      (�  #   Z   Z            ,   ,      !svx/res/reloads.png             ��  ��      (�  #   h   h            :   :      "svx/res/extrusioninfinity_16.png              ��  ��      (�  #   d   d            6   6      #svx/res/extrusion0inch_16.png             ��  ��      (�  #   f   f            8   8      $svx/res/extrusion05inch_16.png              ��  ��      (�  #   d   d            6   6      %svx/res/extrusion1inch_16.png             ��  ��      (�  #   d   d            6   6      &svx/res/extrusion2inch_16.png             ��  ��      (�  #   d   d            6   6      'svx/res/extrusion4inch_16.png             ��  ��      )$  #   `   `            2   2      (svx/res/wireframe_16.png              ��  ��      )%  #   \   \            .   .      )svx/res/matte_16.png              ��  ��      )&  #   ^   ^            0   0      *svx/res/plastic_16.png              ��  ��      )'  #   \   \            .   .      +svx/res/metal_16.png              ��  ��      +�  #   f   f            8   8      ,svx/res/doc_modified_yes_14.png             ��  ��      +�  #   f   f            8   8      -svx/res/doc_modified_no_14.png              ��  ��      +�  #   h   h            :   :      .svx/res/doc_modified_feedback.png             ��  ��      +�  #   f   f            8   8      /svx/res/zoom_page_statusbar.png             ��  ��      +�  #   J   J            6   6      0svx/res/symphony/spacing3.png   +�  #   P   P            <   <      1svx/res/symphony/Indent_Hanging.png   +�  #   H   H            4   4      2svx/res/symphony/blank.png    +�  #   H   H            4   4      3svx/res/symphony/width1.png   ,   #   H   H            4   4      4svx/res/symphony/width2.png   ,  #   H   H            4   4      5svx/res/symphony/width3.png   ,  #   H   H            4   4      6svx/res/symphony/width4.png   ,  #   H   H            4   4      7svx/res/symphony/width5.png   ,  #   H   H            4   4      8svx/res/symphony/width6.png   ,  #   H   H            4   4      9svx/res/symphony/width7.png   ,  #   H   H            4   4      :svx/res/symphony/width8.png   ,  #   H   H            4   4      ;svx/res/symphony/axial.png    ,  #   L   L            8   8      <svx/res/symphony/ellipsoid.png    ,	  #   L   L            8   8      =svx/res/symphony/Quadratic.png    ,
  #   H   H            4   4      >svx/res/symphony/radial.png   ,  #   H   H            4   4      ?svx/res/symphony/Square.png   ,  #   H   H            4   4      @svx/res/symphony/linear.png   ,  #   N   N            :   :      Asvx/res/symphony/rotate_left.png    ,  #   N   N            :   :      Bsvx/res/symphony/rotate_right.png   ,  #   >   >            *   *      Csvx/res/nu01.png    ,  #   >   >            *   *      Dsvx/res/nu04.png    ,  #   >   >            *   *      Esvx/res/nu02.png    ,,  #   h   h            :   :      Fsvx/res/directionnorthwest_22.png             ��  ��      ,-  #   d   d            6   6      Gsvx/res/directionnorth_22.png             ��  ��      ,.  #   h   h            :   :      Hsvx/res/directionnortheast_22.png             ��  ��      ,/  #   d   d            6   6      Isvx/res/directionwest_22.png              ��  ��      ,0  #   h   h            :   :      Jsvx/res/directionstraight_22.png              ��  ��      ,1  #   d   d            6   6      Ksvx/res/directioneast_22.png              ��  ��      ,2  #   h   h            :   :      Lsvx/res/directionsouthwest_22.png             ��  ��      ,3  #   d   d            6   6      Msvx/res/directionsouth_22.png             ��  ��      ,4  #   h   h            :   :      Nsvx/res/directionsoutheast_22.png             ��  ��      ,6  #   b   b            4   4      Osvx/res/perspective_16.png              ��  ��      ,7  #   ^   ^            0   0      Psvx/res/parallel_16.png             ��  ��      ,G  #   j   j            <   <      Qsvx/res/lightofffromtopleft_22.png              ��  ��      ,H  #   f   f            8   8      Rsvx/res/lightofffromtop_22.png              ��  ��      ,I  #   j   j            <   <      Ssvx/res/lightofffromtopright_22.png             ��  ��      ,J  #   f   f            8   8      Tsvx/res/lightofffromleft_22.png             ��  ��      ,L  #   h   h            :   :      Usvx/res/lightofffromright_22.png              ��  ��      ,M  #   l   l            >   >      Vsvx/res/lightofffrombottomleft_22.png             ��  ��      ,N  #   h   h            :   :      Wsvx/res/lightofffrombottom_22.png             ��  ��      ,O  #   n   n            @   @      Xsvx/res/lightofffrombottomright_22.png              ��  ��      ,Q  #   h   h            :   :      Ysvx/res/lightonfromtopleft_22.png             ��  ��      ,R  #   d   d            6   6      Zsvx/res/lightonfromtop_22.png             ��  ��      ,S  #   j   j            <   <      [svx/res/lightonfromtopright_22.png              ��  ��      ,T  #   f   f            8   8      \svx/res/lightonfromleft_22.png              ��  ��      ,V  #   f   f            8   8      ]svx/res/lightonfromright_22.png             ��  ��      ,W  #   l   l            >   >      ^svx/res/lightonfrombottomleft_22.png              ��  ��      ,X  #   h   h            :   :      _svx/res/lightonfrombottom_22.png              ��  ��      ,Y  #   l   l            >   >      `svx/res/lightonfrombottomright_22.png             ��  ��      ,[  #   f   f            8   8      asvx/res/lightfromtopleft_22.png             ��  ��      ,\  #   b   b            4   4      bsvx/res/lightfromtop_22.png             ��  ��      ,]  #   h   h            :   :      csvx/res/lightfromtopright_22.png              ��  ��      ,^  #   d   d            6   6      dsvx/res/lightfromleft_22.png              ��  ��      ,_  #   d   d            6   6      esvx/res/lightfromfront_22.png             ��  ��      ,`  #   d   d            6   6      fsvx/res/lightfromright_22.png             ��  ��      ,a  #   j   j            <   <      gsvx/res/lightfrombottomleft_22.png              ��  ��      ,b  #   f   f            8   8      hsvx/res/lightfrombottom_22.png              ��  ��      ,c  #   j   j            <   <      isvx/res/lightfrombottomright_22.png             ��  ��      ,e  #   `   `            2   2      jsvx/res/brightlit_16.png              ��  ��      ,f  #   `   `            2   2      ksvx/res/normallit_16.png              ��  ��      ,g  #   \   \            .   .      lsvx/res/dimlit_16.png             ��  ��      ,m  #   h   h            :   :      msvx/res/fontworkalignleft_16.png              ��  ��      ,n  #   l   l            >   >      nsvx/res/fontworkaligncentered_16.png              ��  ��      ,o  #   h   h            :   :      osvx/res/fontworkalignright_16.png             ��  ��      ,p  #   l   l            >   >      psvx/res/fontworkalignjustified_16.png             ��  ��      ,q  #   j   j            <   <      qsvx/res/fontworkalignstretch_16.png             ��  ��      ,r  #   >   >            *   *      rsvx/res/fw018.png   ,s  #   >   >            *   *      ssvx/res/fw019.png   ,t  #   >   >            *   *      tsvx/res/fw016.png   ,u  #   >   >            *   *      usvx/res/fw017.png      $  �  �   id              ��  ��       svx/res/id01.png     svx/res/id02.png     svx/res/id03.png     svx/res/id04.png     svx/res/id05.png     svx/res/id06.png     svx/res/id07.png     svx/res/id08.png     svx/res/id030.png    svx/res/id031.png    svx/res/id032.png     svx/res/id033.png    !svx/res/id040.png    (svx/res/id041.png    )svx/res/id016.png    svx/res/id018.png    svx/res/id019.png       'Q  $  @  @   fr              ��  ��       svx/res/fr01.png     svx/res/fr02.png     svx/res/fr03.png     svx/res/fr04.png     svx/res/fr05.png     svx/res/fr06.png     svx/res/fr07.png     svx/res/fr08.png     svx/res/fr09.png     	svx/res/fr010.png    
svx/res/fr011.png    svx/res/fr012.png       'R  $   �   �   da              ��  ��       res/da01.png     res/da02.png     res/da03.png     res/da04.png     res/da05.png     res/da06.png        FP  $  �  �   sx              �   �        res/sx10594.png   )bres/sx10595.png   )cres/sx10596.png   )dres/sx10597.png   )eres/sx10598.png   )fres/sx10599.png   )gres/sx10600.png   )hres/sx10601.png   )ires/sx10607.png   )ores/sx10144.png   '�res/sx10593.png   )ares/sx18013.png   F]res/sx18002.png   FRres/sx18003.png   FSres/sx10604.png   )lres/sx10605.png   )mres/sx10704.png   )�res/sx10705.png   )�res/sx10706.png   )�res/sx10707.png   )�res/sx10708.png   )�res/sx18022.png   Ffres/sx10710.png   )�res/sx10603.png   )kres/sx10715.png   )�res/sx10728.png   )�res/sx10757.png   *res/sx18027.png   Fkres/sx10768.png   *res/sx10769.png   *   FQ  $   �   �   tb              ��  ��       res/tb01.png     res/tb02.png     res/tb03.png     res/tb04.png     res/tb05.png        (K  D     (   8            ?   �     D   h   h  @�    �  SVX_HID_PPROPERTYPANEL_TEXT_UNDERLINE_VS                 9   xUnderline      F   \   \  @�       SVX_HID_UNDERLINE_BTN          }      9   ~Mais opções...    
  #   H   H            4   4      vsvx/res/symphony/line1.png       #   H   H            4   4      wsvx/res/symphony/line2.png       #   H   H            4   4      xsvx/res/symphony/line3.png       #   H   H            4   4      ysvx/res/symphony/line4.png       #   H   H            4   4      zsvx/res/symphony/line5.png       #   H   H            4   4      {svx/res/symphony/line6.png       #   H   H            4   4      |svx/res/symphony/line7.png       #   H   H            4   4      }svx/res/symphony/line8.png       #   H   H            4   4      ~svx/res/symphony/line9.png       #   H   H            4   4      svx/res/symphony/line10.png    (  #   P   P            <   <      �svx/res/symphony/selected-line1.png    )  #   P   P            <   <      �svx/res/symphony/selected-line2.png    *  #   P   P            <   <      �svx/res/symphony/selected-line3.png    +  #   P   P            <   <      �svx/res/symphony/selected-line4.png    ,  #   P   P            <   <      �svx/res/symphony/selected-line5.png    -  #   P   P            <   <      �svx/res/symphony/selected-line6.png    .  #   P   P            <   <      �svx/res/symphony/selected-line7.png    /  #   P   P            <   <      �svx/res/symphony/selected-line8.png    0  #   P   P            <   <      �svx/res/symphony/selected-line9.png    1  #   R   R            >   >      �svx/res/symphony/selected-line10.png     2        (Sem)            Simples            Duplo            Negrito            Pontilhado          &   &Pontilhado (Negrito)             Traço             Traço longo             Ponto traço          $   $Ponto ponto traço             Ondulado    (M  D  X   (   8            V   �     D   n   n  @�    �  SVX_HID_PPROPERTYPANEL_TEXT_SPACING_VS                 P   ZCharacter Spacing      W   D   D   �      @          _      H   Personalizado:       W   P   P   �      @          n      >   Espaço entre ~caracteres:       U   |   |  @?     SVX_HID_SPACING_CB_KERN          x      >   P��   Padrão     Estendido     Condensado           W   F   F   �      @          �      >   Modificar ~por:      	  d   h   h  @?     `SVX_HID_SPACING_MB_KERN          �      >          '             
     #   P   P            <   <      �svx/res/symphony/spacing_normal.png       #   T   T            @   @      �svx/res/symphony/spacing_very tight.png    !  #   P   P            <   <      �svx/res/symphony/spacing_tight.png     "  #   P   P            <   <      �svx/res/symphony/spacing_loose.png     #  #   T   T            @   @      �svx/res/symphony/spacing_very loose.png    3  #   R   R            >   >      �svx/res/symphony/spacing_normal_s.png    4  #   V   V            B   B      �svx/res/symphony/spacing_very tight_s.png    5  #   R   R            >   >      �svx/res/symphony/spacing_tight_s.png     6  #   R   R            >   >      �svx/res/symphony/spacing_loose_s.png     7  #   V   V            B   B      �svx/res/symphony/spacing_very loose_s.png    $  #   T   T            @   @      �svx/res/symphony/last_custom_common.png    %  #   Z   Z            F   F      �svx/res/symphony/last_custom_common_grey.png     =          Muito apertado     >        Apertado     ?        Normal     @        Folgado    A        Muito folgado    B     ,   ,Último valor personalizado    C     2   2 Espaçamento: Condensar de: 3 pt    D     4   4 Espaçamento: Condensar de: 1,5 pt    E     (   ( Espaçamento - normal     F     2   2 Espaçamento: Expandido de: 3 pt    G     4   4 Espaçamento: Expandido de:  6 pt     H     0   0 Espaçamento - condensado de:     I     0   0 Espaçamento - estendido de:      J        pt    (P  D                  	  W   4   4   �              -   	Centro ~X:     
  d      B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_CENTER_X       -   Especifique a percentagem de deslocamento horizontal a partir do centro para o estilo de sombreamento do gradiente. 50% é o centro horizontal.         d             d        W   4   4   �              4   	Centro ~Y:       d      B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_CENTER_Y       -   Especifique a percentagem de deslocamento vertical a partir do centro para o estilo de sombreamento do gradiente. 50% é o centro vertical.         d             d        W   2   2   �              d   	Â~ngulo:      d   �   �  B8     ` SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_ANGLE        -   Especifique o ângulo de rotação do estilo de sombreamento do gradiente.       ����  '       graus             W   8   8   �              -   	Valor ~inicial:      d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_SVALUE       -   Insira o valor de transparência para o ponto inicial do gradiente, sendo 0% total opacidade e 100% total transparência.         d             d        W   6   6   �              4   	Valor ~final:      d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_EVALUE       -   Insira o valor de transparência para o ponto final do gradiente, sendo 0% total opacidade e 100% total transparência.         d             d        W   0   0   �              d   	~Borda:      d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_BORDER       -   Especifica o valor da borda da transparência do gradiente.         d             d        q   �   �  @�      @SVX_HID_PPROPERTYPANEL_AREA_BTN_LEFT_SECOND Rotate Left        �         p           Rotate Left      q   �   �  @�      @SVX_HID_PPROPERTYPANEL_AREA_BTN_RIGHT_FIRST Rotate Right         �         p   "   "   Rotate Right          8   8Gira 45 graus no sentido anti-horário.         4   4Gira 45 graus no sentido horário.    (T  D  �   (   8            V   �     D   `   `  @�    �  SVX_HID_PPROPERTYPANEL_LINE_VS_WIDTH                 P   lWidth      W   D   D   �                 q      P   Personalizado:       W   H   H   �                 �      @   L~argura da linha:       d   �   �  B?     aSVX_HID_PPROPERTYPANEL_LINE_MTR_WIDTH          �      (   Especifique a largura da linha.      6  �                �   
     #   T   T            @   @      �svx/res/symphony/last_custom_common.png      #   Z   Z            F   F      �svx/res/symphony/last_custom_common_grey.png          ,   ,Último valor personalizado    	        pt    (W  D   �                    W   �   �   �                       d   As propriedades da tarefa em execução não estão disponíveis para a seleção atual   '�  T   D   D  @8    �  SVX_HID_STYLE_LISTBOX       <   V        'b    �  �               D   D   J   Página inteira SVX_HID_MNU_ZOOM_WHOLE_PAGE          H   H   J   Largura da página  SVX_HID_MNU_ZOOM_PAGE_WIDTH          8   8   J   Ideal SVX_HID_MNU_ZOOM_OPTIMAL           0   0   J   50% SVX_HID_MNU_ZOOM_50          0   0   J   75% SVX_HID_MNU_ZOOM_75          4   4   J   100%  SVX_HID_MNU_ZOOM_100           4   4   J   150%  SVX_HID_MNU_ZOOM_150           0   0   H200%  SVX_HID_MNU_ZOOM_200    'c    �  �               2   2   HMédia  SVX_HID_MNU_FUNC_AVG           >   >   J   Cont. Valores SVX_HID_MNU_FUNC_COUNT2          :   :   J   Contagem  SVX_HID_MNU_FUNC_COUNT           6   6   J   Máximo SVX_HID_MNU_FUNC_MAX           6   6   J   Mínimo SVX_HID_MNU_FUNC_MIN           4   4   J   	Soma  SVX_HID_MNU_FUNC_SUM           P   P   J   Contagem da seleção SVX_HID_MNU_FUNC_SELECTION_COUNT           6   6   J   Nenhum  SVX_HID_MNU_FUNC_NONE   'd     X   X               @   @   HAssinaturas digitais... SVX_HID_XMLSEC_CALL   'l    �  �               $   $      Milímetro           (   (         Centímetro          "   "         Metro          (   (         Quilômetro          &   &         Polegada                      	   Pé          $   $      
   Milhas           "   "         Ponto          "   "         Pica           &   &         Caractere          "   "         Linha   '�     z   z               8   8   
  '�Atualizar estilo pela seleção          *   *   
  '�Editar estilo...    '�     �   �               *   *      Seleção padrão          0   0         Estender seleção           0   0         Adicionar seleção          0   0         Seleção em bloco    (�    �  �               $   $   Descrição...           "   "   
   
~Macro...                
   	Ativo                          �   �  
   Organizar          �   �               .   .   
   Trazer para a frente           "   "   
   ~Avançar                  
   ~Recuar          ,   ,   
   Enviar ~para trás                           *   *   
   Selecion~ar tudo           "   "   
   ~Excluir    FP                     �   �  J  )�~Novo SVX_HID_FM_NEW           �   �               8   8   J  )�Formulário SVX_HID_FM_NEW_FORM          >   >   J  )pControle oculto SVX_HID_FM_NEW_HIDDEN          @   @   J  )�Substituir por  .uno:ChangeControlType           *   *  
  NCor~tar .uno:Cut           *   *  
  O~Copiar .uno:Copy          ,   ,  
  P~Colar  .uno:Paste           4   4   J  )�~Excluir  SVX_HID_FM_DELETE          @   @   J  )wOrdem de tabulação... .uno:TabDialog           <   <   J  )q~Renomear SVX_HID_FM_RENAME_OBJECT           @   @   J  )�Propr~iedades .uno:ShowPropertyBrowser           D   D   J  )�Abrir no modo de edição .uno:OpenReadOnly          L   L   J  *Foco automático no controle  .uno:AutoControlFocus   FQ     �   �               >   >   J  )rExcluir linhas  SVX_HID_FM_DELETEROWS          6   6   J  )�Salvar registro .uno:RecSave           B   B   J  )�Desfazer: entrada de dados  .uno:RecUndo    FR    �  �              �  �  J  )sInserir ~coluna SVX_HID_FM_INSERTCOL          �  �               2   2   J  )gCaixa de texto  .uno:Edit          :   :   J  )dCaixa de seleção  .uno:CheckBox          <   <   J  )iCaixa de combinação .uno:ComboBox          8   8   J  )hCaixa de listagem .uno:ListBox           6   6   J  )�Campo de data .uno:DateField           6   6   J  )�Campo de hora .uno:TimeField           :   :   J  )�Campo numérico .uno:NumericField          >   >   J  )�Campo monetário  .uno:CurrencyField           <   <   J  )�Campo de padrão  .uno:PatternField          <   <   J  )�Campo formatado .uno:FormattedField          N   N  J  *Campo de data e hora  SVX_HID_CONTROLS_DATE_N_TIME            >   >   J  )nSubstitui~r por SVX_HID_FM_CHANGECOL           >   >   J  )tExcluir coluna  SVX_HID_FM_DELETECOL           <   <   J  *~Ocultar coluna SVX_HID_FM_HIDECOL           �   �  J  *Mo~strar colunas  SVX_HID_FM_SHOWCOLS          �   �               <   <   J  *~Mais...  SVX_HID_FM_SHOWCOLS_MORE                           8   8   J  *~Todas  SVX_HID_FM_SHOWALLCOLS           <   <   J  )�Coluna... .uno:ShowPropertyBrowser    FS     B   B               *   *  
  O~Copiar .uno:Copy   FT    �  �               P   P  J  )�Caixa de ~texto .uno:ConvertToEdit  .uno:ConvertToEdit           L   L  J  )�~Botão .uno:ConvertToButton  .uno:ConvertToButton           R   R  J  )�~Campo de rótulo .uno:ConvertToFixed .uno:ConvertToFixed          P   P  J  )�Caixa de g~rupo .uno:ConvertToGroup .uno:ConvertToGroup          T   T  J  )�Caixa de l~istagem  .uno:ConvertToList  .uno:ConvertToList           \   \  J  )�~Caixa de seleção .uno:ConvertToCheckBox  .uno:ConvertToCheckBox           T   T  J  )�~Botão de opção  .uno:ConvertToRadio .uno:ConvertToRadio          X   X  J  )�Cai~xa de combinação  .uno:ConvertToCombo .uno:ConvertToCombo          Z   Z  J  )�Botão de i~magem .uno:ConvertToImageBtn  .uno:ConvertToImageBtn           b   b  J  )�~Seleção de arquivo .uno:ConvertToFileControl .uno:ConvertToFileControl          P   P  J  )�Campo de ~data  .uno:ConvertToDate  .uno:ConvertToDate           P   P  J  )�Campo d~e hora  .uno:ConvertToTime  .uno:ConvertToTime           V   V  J  )�Campo ~numérico  .uno:ConvertToNumeric .uno:ConvertToNumeric          Z   Z  J  )�Campo ~monetário .uno:ConvertToCurrency  .uno:ConvertToCurrency           V   V  J  )�Campo de ~padrão .uno:ConvertToPattern .uno:ConvertToPattern          d   d  J  )�Controle de ima~gem .uno:ConvertToImageControl  .uno:ConvertToImageControl           Z   Z  J  )�Campo fo~rmatado  .uno:ConvertToFormatted .uno:ConvertToFormatted          Z   Z  J  *Barra de rolagem  .uno:ConvertToScrollBar .uno:ConvertToScrollBar          ^   ^  J  *Botão giratório .uno:ConvertToSpinButton  .uno:ConvertToSpinButton           f   f  J  *Barra de navegação  .uno:ConvertToNavigationBar .uno:ConvertToNavigationBar   FU     �   �               4   4   J  )�~Excluir  SVX_HID_FM_DELETE          0   0   J  *~Editar SVX_HID_FM_EDIT          <   <   J  *É ~nulo  SVX_HID_FM_FILTER_IS_NULL          >   >   J  *Nã~o é nulo SVX_HID_FM_IS_NOT_NULL    FV                     0   0  
  'Fonte .uno:CharFontName          0   0  
  'Tamanho .uno:FontHeight         0  0  J  FW~Estilo SVX_HID_MENU_FM_TEXTATTRIBUTES_STYLE          �  �      
         *   *  
  'Negrito .uno:Bold          .   .  
  'Itálico  .uno:Italic          2   2  
  -0Sobrelinha  .uno:Overline          4   4  
  'Sublinhado  .uno:Underline           0   0  
  'Tachado .uno:Strikeout           .   .  
  'Sombra  .uno:Shadowed          4   4  
  'Contorno  .uno:OutlineFont                           8   8  
  (6~Sobrescrito  .uno:SuperScript           4   4  
  (7Su~bscrito  .uno:SubScript          J  J  N  FX   ~Alinhamento  SVX_HID_MENU_FM_TEXTATTRIBUTES_ALIGNMENT           �   �               4   4    ',   ~Esquerda .uno:LeftPara          6   6    '-   ~Direita  .uno:RightPara           :   :    '.   ~Centralizado .uno:CenterPara          :   :    '/   Justificado .uno:JustifyPara              J  FYEspaçamento de ~linhas SVX_HID_MENU_FM_TEXTATTRIBUTES_SPACING           �   �               4   4    '2   Simples .uno:SpacePara1          8   8    '3   1,5 linha .uno:SpacePara15           4   4    '4   ~Duplo  .uno:SpacePara2   FZ    �  �               H   H   J   
Adicionar item  SVX_HID_XFORMS_TOOLBOX_ITEM_ADD          T   T   J   Adicionar elemento  SVX_HID_XFORMS_TOOLBOX_ITEM_ADD_ELEMENT          V   V   J   Adicionar atributo  SVX_HID_XFORMS_TOOLBOX_ITEM_ADD_ATTRIBUTE          B   B   J   Editar  SVX_HID_XFORMS_TOOLBOX_ITEM_EDIT                           D   D   J   Excluir SVX_HID_XFORMS_TOOLBOX_ITEM_REMOVE     �          �     � �"    �        (1  #�    (n  )�    (o  2�    *d  4    FP  76    FQ  7�       9�      :      :P      :p      :�      :�      :�      :�      :�    	  ;    
  ;4      ;P      ;l    
   ;�    
  ;�    
  ;�    
  ;�    
  <    
  <8    
  <`    
  <x    
  <�    
	  <�    

  <�    
  <�    
  =    
  =     
  =<    
  =X    
  =r    
  =�    
  =�    
  =�    
  =�    
  =�    
  >$    
  >N    
  >t    
  >�    
  >�    
  >�    
  ?    
  ?<    
  ?V    
  ?p    
   ?�    
!  ?�    
"  ?�    
#  ?�    
$  @    
%  @.    
&  @F    
'  @^    
(  @~    
)  @�    
*  @�    
+  @�    
,  A    
-  A*    
.  AD    
/  An    
0  A�    
1  A�    
2  A�    
3  A�    
4  B    
5  B0    
6  BR    
7  Bt    
8  B�    
9  B�    
:  B�    
;  C    
<  C    
=  C>    
>  CT    
?  Ct    
@  C�    
A  C�    
B  C�    
C  C�    
D  D    
E  D<    
F  Df    
G  D�    
H  D�    
I  E    
J  E:    
K  Er    
L  E�    
M  E�    
N  E�    
O  E�    
P  F    
Q  F,    
R  FN    
S  Fr    
T  F�    
U  F�    
V  F�    
W  G    
X  G2    
Y  GP    
Z  Gx    
[  G�    
\  G�    
]  G�    
^  G�    
_  H    
`  H6    
a  HV    
b  H~    
c  H�    
d  H�    
e  H�    
f  I2    
g  Ih    
h  I�    
i  I�    
j  I�    
k  I�    
l  I�    
m  J    
n  J6    
o  JZ    
p  J|    
q  J�    
r  J�    
s  J�    
t  K    
u  K2    
v  KD    
w  Kj    
x  K�    
y  K�    
z  K�    
{  K�    
|  K�    
}  L    
~  L4    
  LN    
�  Lr    
�  L�    
�  L�    
�  L�    
�  L�    
�  M    
�  M    
�  M6    
�  MN    
�  Mx    
�  M�    
�  M�    
�  N    
�  N$    
�  N<    
�  N^    
�  N�    
�  N�    
�  N�    
�  N�    
�  N�    
�  N�    
�  O0    
�  OL    
�  On    
�  O�    
�  O�    
�  O�    
�  P
    
�  P$    
�  PF    
�  P`    
�  P�    
�  P�    
�  P�    
�  Q    
�  Q0    
�  QV    
�  Q|    
�  Q�    
�  Q�    
�  Q�    
�  R&    
�  R@    
�  Rt    
�  R�    
�  R�    
�  R�    
�  S    
�  S<    
�  Sb    
�  S�    
�  S�    
�  S�    
�  T    
�  T,    
�  TH    
�  Tn    
�  T�    
�  T�    
�  T�    
�  U    
�  U@    
�  U`    
�  U�    
�  U�    
�  U�    
�  U�    
�  U�    
�  V    
�  V@    
�  Vb    
�  V�    
�  V�    
�  V�    
�  V�    
�  W    
�  W.    
�  W^    
�  W�    
�  W�    
�  W�    
�  W�    
�  X    
�  XD    
�  Xn    
�  X�    
�  X�    
�  X�    
�  Y    
�  Y4    
�  YV    
�  Yp    
�  Y�    
�  Y�    
�  Y�    
�  Z    
�  Z@    
�  Zf    
�  Z�    
�  Z�    
�  Z�    
�  Z�    
�  [    
�  [&    
�  [N    
�  [t    
�  [�    
�  [�    
�  [�    
�  \
    
�  \L    
�  \l    
�  \�    
�  \�    
�  \�    
�  \�    
�  ]    
�  ]F    
�  ]d    
�  ]�    
�  ]�    
�  ]�    
�  ^    
�  ^"    
�  ^<    
�  ^j       ^�      ^�      ^�      ^�      _      _F      _~      _�      _�    	  `    
  `2      `R      `~      `�      `�      a      a2      aJ      af      az      a�      a�      a�      a�      a�      b      b&      bB      b^      b|      b�      b�       c    !  c    "  c4    #  c\    $  cv    %  c�    &  c�    '  c�    (  c�    )  d    *  d    +  d6    ,  dP    -  dj    .  d�    /  d�    0  d�    1  d�    2  e    3  e$    4  eH    5  ef    6  e�    7  e�    8  e�    9  e�    :  e�    ;  f    <  f<    =  fX    >  fr    ?  f�    @  f�    A  f�    B  f�    C  g    D  g.    E  gD    F  g\    G  gz    H  g�    I  g�    J  g�    K  h    R  h>    S  hh    T  h�    U  h�    V  h�    W  h�    X  i
    Y  i.    Z  iR    [  ix    c  i�    d  i�    e  i�    f  j&    g  jV    h  j�    i  j�    j  j�    k  k    l  k6    m  k`    n  k�    o  k�    p  k�    q  l    r  l"    s  l8    t  lN    u  lf    v  l�    w  l�    x  l�    y  l�    z  l�    {  m    |  m    }  m0    ~  mB      mT    �  mf    �  m�    �  m�    �  m�    �  m�    �  n    �  n,    �  nL    �  nl    �  n�    �  n�    �  n�    �  n�    �  n�    �  o    �  o@    �  of    �  o�    �  o�    �  o�    �  o�    �  p
    �  p(    �  pT    �  p|    �  p�    �  p�    �  p�    �  q    �  qF    �  qn    �  q�    �  q�    �  q�    �  r
    �  r2    �  rX    �  rr    �  r�    �  r�    �  r�    �  r�    �  s&    �  s\    �  s�    �  s�    �  s�    �  t    �  tT    �  t�    �  t�    �  t�    �  u    �  u.    �  uV    �  u~    �  u�    �  u�    �  u�    �  v*    �  vZ    �  v�    �  v�    �  v�    �  w    �  w2    �  wZ    �  w�    �  w�    �  w�    �  w�    �  x    �  xF    �  x|    �  x�    �  x�    �  y    �  y@    �  yj    �  y�    �  y�    �  y�    �  z     �  z    �  zH    �  zr    �  z�    �  z�    �  z�    �  z�    �  {$    �  {H    �  {z    �  {�    �  {�    �  |    �  |0    �  |`    �  |�    �  |�    �  |�    �  }    �  }0    �  }j    �  }�    �  }�    �  ~    �  ~>    �  ~h    �  ~�    �  ~�    �  ~�             $      L      z      �      �      �
      �<      �h    	  ��    
  ��      �      �2      �v      ��      ��      ��      �       �R      ��      ��      ��      �       �J      �r      ��    $  ��    %  ��    &  �    '  �N    (  �x    )  ��    *  ��    +  �    ,  �N    -  �|    .  ��    /  ��    0  �    1  �:    2  �d    3  ��    4  ��    5  ��    6  �F    7  ��    8  ��    9  ��    :  ��    ;  �(    =  �V    >  �x    ?  ��    @  ��    A  ��    B  �
    C  �4    D  �^    E  ��    F  ��    G  ��    H  ��    I  �    J  �4    K  �T    L  �z    M  ��    N  ��    O  ��    P  �    Q  �0    R  �T    S  �v    T  ��    U  ��    V  ��    W  �    X  �@    Y  �d    Z  ��    [  ��    \  ��    ]  �    ^  �D    _  �n    `  ��    a  ��    b  ��    c  �    d  �@    e  �j    f  ��    g  ��    h  ��    i  ��    j  �$    k  �J    l  �t    m  ��    n  ��    o  ��    p  �
    q  �&    r  �D    s  �l    t  ��    u  ��    v  ��    w  ��    x  �    y  �$    z  �>    {  �X    |  �x    }  ��    ~  ��      ��    �  �    �  �    �  �H    �  �t    �  ��    �  ��    �  ��    �  ��    �  ��    �  �    �  �    �  �:    �  �T    �  �~    �  ��    �  ��    �  ��    �  ��    �  ��    �  �
    �  �.    �  �N    �  �l    �  ��    �  ��    �  ��    �  ��    �  �    �  �0    �  �T    �  �v    �  ��    �  ��    '  �    'W  �&    'Y  �V    'Z  �p    '[  ��    '\  ��    ']  ��    '^  ��    '_  ��    '`  ��    'a  �    'b  �:    'c  �X    'd  ��    'e  ��    'h  ��    'i  �     'j  �l    '�  ��    '�  ��    '�  ��    '�  ��    '�  �
    '�  �"    '�  �6    '�  �P    '�  �l    '�  ��    '�  ��    '�  ��    '�  ��    '�  ��    '�  �    '�  �>    '�  �b    '�  ��    '�  ��    '�  ��    '�  �    '�  �@    '�  �h    '�  ��    '�  ��    '�  ��    '�  ��    '�  �F    '�  ��    '�  ��    '�  ��    '�  �v    '�  �    '�  �b    '�  �     (  �"    (  �<    (  �T    (  �l    (
  ��    (  ��    (  ��    (  ��    (  ��    (#  �    ($  �&    (%  �<    (&  �R    ('  �h    ((  ��    (*  ��    (1  ��    (2  ��    (3  ��    (4  ��    (5  �    (6  �0    (7  �J    (8  �j    (<  ��    (=  ��    (>  ��    (A  ��    (B  ��    (C  �    (D  �2    (E  �H    (�  �`    (�  �~    (�  ��    (�  ��    (�  ��    (�  ��    (�  �    (�  �H    (�  �n    (�  ��    (�  ��    (�  ��    )   ��    )  �(    )  �R    )  �t    )  ��    )  ��    )  ��    )  ��    )  �    )  �2    )  �^    )  ��    )   ��    )!  ��    )"  �
    )#  �*    ),  �J    )-  �b    ).  �z    )/  ��    )0  ��    )1  ��    )2  ��    )4  �
    )5  �B    )6  �b    )7  ��    )8  ��    )9  ��    ):  ��    )@  �    )A  �    )B  �0    )C  �H    )D  �^    )E  �v    )F  ��    )G  ��    )H  ��    )I  ��    )J  �     )K  �    )L  �,    )M  �D    )N  �^    )O  �x    )Z  ��    )[  ��    )\  ��    )]  �     )^  �,    )_  �Z    )`  ��    )c  ��    )d  ��    )e  ��    )f  ��    )g  �    )h  �"    )j  �:    )q  �P    )r  �h    )s  �~    )t  ��    )u  ��    )v  ��    )w  ��    )x  ��    )y  �    )z  �6    ){  �N    )|  �h    )}  �|    )~  ��    )  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �4    )�  �R    )�  �n    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �&    )�  �D    )�  �b    )�  ��    )�  ��    )�  ��    )�  ��    )�  �
    )�  �(    )�  �R    )�  �t    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �0    )�  �T    )�  �v    )�  ��    )�  ��    )�  ��    )�  �    )�  �*    )�  �P    )�  �x    )�  ��    )�  ��    )�  ��    )�  �    )�  �,    )�  �L    )�  �l    )�  ��    )�  ��    )�  ��    )�  �    )�  �(    )�  �N    )�  �l    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �&    )�  �:    )�  �R    )�  �h    )�  �~    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �,    )�  �D    )�  �Z    )�  �p    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �
    )�  �"    )�  �8    )�  �N    )�  �h    )�  �~    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �0    )�  �J    )�  �b    )�  �z    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �,    )�  �P    )�  �r    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    *   �0    *  �L    *  �r    *  ��    *  ��    *  ��    *  ��    *  �    *  �B    *	  �h    *
  ��    *  ��    *  ��    *  ��    *2  �     *3  �(    *4  �l    *5  ��    *D  ��    *E  ��    *F  ��    *G  ��    *H  �    *I  �    *J  �4    *K  �N    *L  �d    *Y  ��    *^  ��    *_  ��    *`  ��    *a  �$    *b  �P    *c  �n    *m  ��    *n  ��    *o  ��    *p  ��    *q  �    *r  �&    *s  �D    *t  �^    *u  �v    *v  ��    *w  ��    *�  ��    *�  ��    *�  �    *�  �*    *�  �\    *�  �~    *�  ��    *�  ��    *�  ��    *�  ��    *�  �    *�  �@    *�  �`    *�  ��    *�  ��    *�  ��    *�  ��    *�  ��    *�  �    *�  �    *�  �6    *�  �P    *�  �v    *�  ��    *�  ��    *�  �    *�  �B    *�  �b    *�  �|    *�      *�  ¸    *�  ��    *�  ��    *�  ��    *�  �    *�  �*    *�  �@    *�  �Z    *�  �r    *�  Ê    *�  â    +n  ü    +o  ��    +p  �&    +r  �V    +s  Ė    +t  ��    +u  ��    +v  �    +w  �B    +x  �j    +y  Ŝ    +z  ��    +{  ��    +|  �    +}  �4    +~  �f    +�  Ɛ    +�  ��    +�  �     +�  �0    +�  �f    +�  ǜ    +�  ��    +�  �    +�  �<    +�  �^    +�  Ȁ    +�  Ȥ    +�  ��    +�  �    +�  �2    +�  �`    +�  ɚ    +�  ��    +�  �B    +�  ʺ    +�  ��    +�  �V    +�  ��    +�  �
    +�  ̈    +�  ̶    +�  ��    +�  ��    +�  �6    +�  �P    +�  ͬ    +�  �     +�  �(    +�  �H    +�  �t    ,  Τ    ,  ��    ,  ��    ,  �"    ,  �N    ,  �t    ,  �    ,  Ј    ,  Т    ,  �    ,  �8    ,  �T    ,   �v    ,!  ѐ    ,"  Ѩ    ,#  ��    ,$  ��    ,%  ��    ,&  �    ,'  �.    ,(  �F    ,)  �`    ,*  Ҁ    ,+  Ң    ,8  ��    ,9  ��    ,:  �"    ,;  �J    ,<  �p    ,=  Ӗ    ,>  Ӽ    ,?  ��    ,@  �
    ,B  �2    ,C  �P    ,D  �j    ,E  Ԇ    ,F  Ԟ    ,h  Ժ    ,i  ��    ,j  ��    ,k  �"    ,l  �>    ,v  �f    ,w  �z    ,x  Վ    ,y  բ    ,z  ո    ,{  ��    ,|  ��    ,}  �    ,~  �(    .�  �J    .�  �d    .�  ֆ    .�  ֬    .�  ��    .�  ��    .�  �    .�  �<    .�  �`    .�  א    .�  ׶    .�  ��    .�  ��    .�  �
    .�  �&    .�  �B    .�  �^    .�  �z    .�  ؖ    .�  ش    .�  ��    .�  ��    .�  �    .�  �6    .�  �V    .�  �t    .�  ٘    .�  پ    .�  ��    .�  �    .�  �2    .�  �X    /   �x    /  ڞ    /  ��    /  ��    /  �    /  �@    /  �h    /  ۄ    /  ۤ    /	  ��    /
  ��    /  �    /  �.    /  �P    /  �h    /  ܄    /  ܢ    /  ��    /  ��    /  ��    /  �    /  �6    /  �T    /  �t    /  ݔ    /  ݲ    /  ��    /  ��    /  �    /  �4    /  �T    /  �r    /   ސ    /!  ޤ    /"  ��    /#  ��    /$  ��    /%  �    /&  �2    /'  �N    /(  �r    /)  ߐ    /*  ߪ    /+  ��    /,  ��    /-  �    /.  �    //  �6    /0  �R    /1  �j    /2  ��    /3  ��    /4  �    /5  ��    /6  ��    /7  �    /D  �"    /E  �<    /F  �`    /G  �    /H  �    /I  ��    /J  ��    /K  �$    /L  �L    /M  �    /N  �    /O  ��    /P  ��    /Q  �     /R  �    /S  �8    /T  �T    /U  �p    /V  �    /W  �    /X  ��    /Y  ��    /Z  �    /[  �0    /\  �R    /]  �n    /^  �    /_  �    /`  ��    /a  �     /b  �&    /c  �L    /d  �d    /e  �    /f  �    /g  ��    /h  ��    /i  �    /j  �*    /k  �F    /l  �f    /m  �    /n  �    /o  ��    /p  ��    /q  �    /r  �0    /s  �L    /t  �j    /u  �    /v  �    /w  ��    /x  ��    /y  �    /z  �"    /{  �B    /|  �d    /}  �    /~  �    /  ��    /�  ��    /�  �    /�  �:    /�  �`    /�  �    /�  �    /�  �    /�  ��    /�  ��    /�  �    /�  �6    /�  �R    /�  �t    /�  �    /�  �    /�  ��    /�  ��    /�  �    /�  �(    /�  �@    /�  �\    /�  �r    /�  �    /�  �    /�  ��    /�  ��    /�  �    /�  �$    1�  �D    1�  �b    1�  �|    1�  �    1�  �    1�  ��    1�  ��    1�  �    1�  �6    1�  �Z    1�  �r    1�  �    1�  ��    1�  ��    1�  ��    1�  �    1�  �    1�  �>    1�  �b    1�  �    1�  �    1�  ��    1�  ��    1�  ��    1�  �    1�  �:    1�  �V    1�  �~    1�  �    1�  �    2   ��    2  ��    2  �    2  �.    2  �J    2  �v    2  �    2  �    2  ��    2	  �    2
  �6    2  �d    :h  �    :p  �    :q  �    :r  ��    :s  ��    :t  �    :u  �@    :v  �f    :w  �    :z  �    :{  ��    :|  ��    :  ��    :�  �    :�  �&    :�  �@    :�  �Z    :�  �r    :�  �    :�  �    :�  ��    :�  ��    :�  ��    :�  �    :�  �6    :�  �J    :�  �^    :�  �r    <   �    <  ��    <  ��    <  ��    <!  �    <"  �*    <#  �F    <$  �l    <%  ��    <&  ��    <F  �    <G  �(    <H  �J    <I  �l    <X  ��    <Y  ��    <Z  ��    <[  ��    <\  �    <]  �4    <^  �N    <_  �f    <`  ��    <a  ��    <b  ��    <c  ��    <d  ��    <o  ��    <p  �    <q  �*    <r  �B    <s  �j    <u  ��    C  ��    C  ��    FQ  ��    FR  �:    FS  ��    FT  ��    FU  ��    FV  ��    FW  �<    FX  ��    FY  �&    F[  ��    F\  ��    F]  �    F^  �     F_  �H    F`  �r    Fk  ��    Fl  ��    Fn  ��    Fo  �    Fp  �*    Ft  �P    Fu  �l    Fv  ��    Fw  ��    Fx  ��    Fy  �    Fz  �0    F{  �N    F|  ��    F}  ��    F~  *    F  N    F�  j    F�  ~    F�  �    F�  �    F�  �    F�  �    F�     F� :    F� \    F� |    F� �    F� �    F� �    F�     F� *    F� P    F� n    F� �    F� �    F� �    F� �    F�     F� 8    F� h    F� �    F� �    F� �    F� �    F�     F� <    F� P    F� p    F� �    F� F    F� �    F� �    F� "    F� \    F� �    F� �    F� 8    F� �    F�     F� V    F� �    F� �    F� �    F� �    F� 	    F� 	*    F� 	H    F� 	t    F� 	�    F� 	�    F� 	�    F� 	�    F� 	�    F� 
     F� 
$    F� 
<    F� 
R    F� 
l    F� 
�    F� 
�    F� 
�    F� 
�    F�     F� &    F� H    F� l    F� �    F� �    F� �    F� �    F�      F� @    F� ^    F� |    F� �    F� ,    F� v    F� �    F� �    H� �    H�     H� X    H� �    �     � 2    � \    � �    � �    � �    '�      '�  B    '�  n    '�  �    'b i�    'c k�    'd m�    'l m�    '� o�    '� p    (� p�    FP r�    FQ u�    FR v�    FS {R    FT {�    FU ��    FV �z    FZ �|  #  'S  �  #  'T !(  #  'U !�  #  'e !�  #  'f "B  #  'g "�  #  'i "�  #  'k #X  #  '� #�  #  '� $  #  '� $n  #  '� $�  #  ( %  #  ( %n  #  (� %�  #  (� &   #  (� &v  #  (� &�  #  (� '"  #  (� 'x  #  (� '�  #  (� ($  #  (� (z  #  (� (�  #  (� )<  #  (� )�  #  (� *  #  (� *j  #  (� *�  #  )$ +2  #  )% +�  #  )& +�  #  )' ,L  #  +� ,�  #  +� -  #  +� -t  #  +� -�  #  +� .B  #  +� .�  #  +� .�  #  +� /$  #  ,  /l  #  , /�  #  , /�  #  , 0D  #  , 0�  #  , 0�  #  , 1  #  , 1d  #  , 1�  #  ,	 1�  #  ,
 2D  #  , 2�  #  , 2�  #  , 3  #  , 3j  #  , 3�  #  , 3�  #  , 44  #  ,, 4r  #  ,- 4�  #  ,. 5>  #  ,/ 5�  #  ,0 6
  #  ,1 6r  #  ,2 6�  #  ,3 7>  #  ,4 7�  #  ,6 8
  #  ,7 8l  #  ,G 8�  #  ,H 94  #  ,I 9�  #  ,J :  #  ,L :j  #  ,M :�  #  ,N ;>  #  ,O ;�  #  ,Q <  #  ,R <|  #  ,S <�  #  ,T =J  #  ,V =�  #  ,W >  #  ,X >�  #  ,Y >�  #  ,[ ?V  #  ,\ ?�  #  ,] @  #  ,^ @�  #  ,_ @�  #  ,` AN  #  ,a A�  #  ,b B  #  ,c B�  #  ,e B�  #  ,f CL  #  ,g C�  #  ,m D  #  ,n Dp  #  ,o D�  #  ,p ED  #  ,q E�  #  ,r F  #  ,s FX  #  ,t F�  #  ,u F�  $    G  $  'Q H�  $  'R J   $  FP J�  $  FQ M4  D  (K M�  D  (M U�  D  (P ^,  D  (T fH  D  (W h�  T  '� i�  y  (G   y  (H �  y  <v �  N4