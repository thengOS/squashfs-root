  �    #�     *�        Latim básico   *�        Latim 1   *�     "   "Latim expandido A   *�     "   "Latim expandido B   *�          Extensões IPA    *�     6   6Letras modificadoras de espaçamento    *�     0   0Marcas diacríticas combinadas    *�        Grego básico   *�     *   *Símbolos e coptas gregos   *�        Cirílico   *�        Arménio    *�     "   "Hebraico básico    *�     $   $Hebraico expandido    *�          Árabe básico    *�     "   "Árabe expandido    *�        Devanagari    *�        Bengali   *�        Gurmukhi    *�        Gujarati    *�        Odia    *�        Tamil   *�        Telugu    *�        Kannada   *�        Malayalam   *�        Tailandês    *�        Lao   *�     "   "Georgiano básico   *�     $   $Georgiano expandido   *�        Hangul Jamo   *�     *   *Latim expandido adicional   *�          Grego expandido   *�     "   "Pontuação geral   *�     (   (Sobrescrito e subscrito   *�     &   &Símbolos monetários   *�     2   2Símbolos diacríticos combinados   *�     (   (Símbolos alfabéticos    *�     "   "Formas numéricas   *�        Setas   *�     (   (Operadores matemáticos   *�     &   &Caracteres técnicos    *�     &   &Símbolos do teclado    *�     4   4Reconhecimento ótico de caracteres   *�     4   4Caracteres alfanuméricos inclusos    *�     "   "Desenho de caixa    *�     $   $Elementos de bloco    *�     $   $Formas geométricas   *�     $   $Símbolos diversos    *�        Ornamentos    *�     ,   ,Símbolos e pontuação CJK   *�        Hiragana    *�        Katakana    *�        Bopomofo    *�     ,   ,Jamo compatível com Hangul   *�     (   (Diversos caracteres CJK   *�     0   0Letras e meses CJK delimitadas    *�     $   $Compatibilidade CJK   *�        Hangul    *�     *   *Ideogramas unificados CJK   +N     6   6Ideogramas unificados CJK extensão A   *�     .   .Área de utilização privada   *�     2   2Ideogramas de compatibilidade CJK   *�     6   6Formas de apresentação alfabética    *�     2   2Formas de apresentação árabe A   *�     .   .Meias marcas de combinação    *�     .   .Formas de compatibilidade CJK   *�     .   .Pequenas variantes de formas    *�     2   2Formas de apresentação árabe B   *�     8   8Formas de meia largura e largura normal   *�        Especiais   *�        Sílabas Yi   *�        Radicais Yi   *�          Itálico antigo   *�        Gótico   *�        Deseret   *�     .   .Símbolos musicais bizantinos   *�     $   $Símbolos musicais    *�     6   6Símbolos matemáticos alfanuméricos   *�     6   6Ideogramas unificados CJK extensão B   *�     6   6Ideogramas unificados CJK extensão C   *�     6   6Ideogramas unificados CJK extensão D   *�     @   @Ideogramas de compatibilidade CJK suplementares   *�        Etiquetas   *�     $   $Cirílico adicional   *�     (   (Seletores de variação   *�     4   4Área de uso privado suplementar A    *�     4   4Área de uso privado suplementar B    *�        Limbu   *�        Tai Le    *�          Símbolos khmer   *�     &   &Extensões fonéticas   *�     ,   ,Símbolos e setas diversos    *�     0   0Símbolos de hexagramas Yijing    *�     $   $Sílabas lineares B   *�     &   &Ideogramas lineares B   *�          Números egeus    *�        Ugarítico    *�        Shavian   *�        Osmania   +Z        Cingalês   +[        Tibetano    +\        Mianmar   +]        Khmer   +^        Ogham   +f        Rúnico   +g        Sírio    +_        Thaana    +h        Etíope   +i        Cherokee    +`     0   0Sílabas aborígenes canadianas   +j        Mongol    +k     2   2Diversos símbolos matemáticos A   +l     &   &Setas suplementares A   +a     $   $Padrões em braille   +m     &   &Setas suplementares B   +n     2   2Diversos símbolos matemáticos B   +b     (   (Suplemento radical CJK    +o          Radicais Kangxi   +p     8   8Caracteres de descrição ideográfica    +q        Tagalog   +r        Hanunoo   +c        Tagbanwa    +t        Buhid   +s        Kanbun    +d     $   $Bopomofo expandido    +e     $   $Fonética Katakana    *�        Traços CJK   *�     $   $Sílabas cipriotas    *�     (   (Símbolos Tai Xuan Jing   *�     6   6Suplemento de seletores de variação   *�     0   0Notação musical grega antiga    *�     (   (Números gregos antigos   *�     &   &Arábico suplementar    *�        Buginês    +      >   >Suplemento de marcas diacríticas combinadas    +        Copta   +     "   "Etíope extendido   +     $   $Etíope suplementar   +     &   &Geórgio suplementar    +        Glagolítico    +        Kharoshthi    +     ,   ,Letras modificadoras de tom   +        Novo Tai Lue    +	        Persa antigo    +
     4   4Suplemento de extensões fonéticas   +     (   (Pontuação suplementar   +        Syloti Nagri    +        Tifinagh    +     "   "Formas verticais    +        Nko   +        Balinês    +     "   "Latim expandido C   +     "   "Latim expandido D   +        Phags-pa    +        Fenício    +        Cuneiforme    +     2   2Números e pontuação cuneiforme   +     $   $Barras de contagem    +        Sudanês    +        Lepcha    +        Ol Chiki    +     &   &Cirílico expandido A   +        Vai   +     &   &Cirílico expandido B   +        Saurashtra    +        Kayah Li    +         Rejang    +!        Cham    +"     &   &Símbolos ancestrais    +#     $   $Discos de Phaistos    +$        Lício    +%        Carian    +&        Lídio    +'     "   "Peças do Mahjong   +(     "   "Peças de dominó   +)        Samaritano    +*     0   0Sílabas aborígenes canadenses   ++        Tai Tham    +,     $   $Extensões védicas   +-        Lisu    +.        Bamum   +/     2   2Formas numéricas índicas comuns   +0     &   &Devanagari expandido    +1     (   (Hangul Jamo expandido A   +2        Javanês    +3     &   &Birmanês expandido A   +4     &   &Tailandês vietnamita   +5        Meetei Mayek    +6     (   (Hangul Jamo expandido B   +7     "   "Aramaico imperial   +8     &   &Árabe do sul antigo    +9        Avestan   +:     *   *Parthian de inscrições    +;     (   (Pahlavi de inscrições   +<        Turco antigo    +=     *   *Símbolos numéricos rumi   +>        Kaithi    +?     (   (Hieróglifos egípcios    +@     2   2Suplemento alfanumérico incluso    +A     0   0Suplemento ideográfico incluso   +C        Mandaico    +D        Batak   +E     $   $Etíope extendido A   +F        Brahmi    +G     "   "Suplemento bamum    +H          Suplemento kana   +I          Cartas de jogo    +J     2   2Diversos símbolos e pictogramas    +K     "   "Ícone expressivo   +L     2   2Símbolos de transportes e mapas    +M     &   &Símbolos de alquimia   +O     $   $Árabe expandido A    +P     <   <Símbolos alfabéticos matemáticos árabes   +Q        Chakma    +R     (   (Extensões Meetei Mayek   +S     $   $Meroítica cursiva    +T     *   *Hieróglifos meroíticos    +U        Miao    +V        Sharada   +W        Sora Sompeng    +X     &   &Sudanês suplementar    +Y        Takri   +u        Bassa Vah   +v     $   $Albanês caucasiano   +w     $   $Números cópticos    +x     :   :Marcas diacríticas combinadas expandidas   +y     *   *Estenografia de Duployé    +z     (   (Estenografia de Elbasan   +{     0   0Formas geométricas expandidas    +|        Grantha   +}        Khojki    +~        Khudawadi   +     "   "Latim expandido E   +�        Lineal A    +�        Mahajani    +�        Maniqueio   +�          Alfabeto Mende    +�        Modi    +�        Mro   +�     &   &Birmanês expandido B   +�     $   $Alfabeto nabateano    +�     ,   ,Árabe setentrional antigo    +�          Pérmico antigo   +�     (   (Pictogramas ornamentais   +�        Pahawh hmong    +�        Palmireno   +�        Pau Cin Hau   +�          Psalter Pahlavi   +�     4   4Controlos de formato taquigráfico    +�        Siddham   +�     .   .Números arcaicos cingaleses    +�     &   &Setas suplementares C   +�        Tirhuta   +�        Warang Citi   +�        Ahom    +�     *   *Hieróglifos de Anatólia   +�     $   $Suplemento cherokee   +�     6   6Ideogramas unificados CJK extensão E   +�     .   .Cuneiforme dinástico arcaico   +�        Hatran    +�        Multani   +�          Húngaro antigo   +�     6   6Símbolos e pictogramas suplementares   +�     "   "Escrita de Sutton   (1    �              Esquerda             Interior             Direita            Exterior     !        Centro             Da esquerda            Do interior         $   $Área de parágrafo         .   .Área de texto do parágrafo     	     ,   ,Margem esquerda da página          ,   ,Margem interior da página     
     *   *Margem direita da página         ,   ,Margem exterior da página          .   .Margem esquerda do parágrafo         .   .Margem interior do parágrafo         .   .Margem direita do parágrafo          .   .Margem exterior do parágrafo         "   "Página completa          *   *Área de texto da página            Superior             Inferior              Centro     "        De cima    #        De baixo     $        Abaixo     %        Da direita     &     ,   ,Margem superior da página     '     ,   ,Margem inferior da página     (     .   .Margem superior do parágrafo    )     .   .Margem inferior do parágrafo            Margem          .   .Área de texto do parágrafo          ,   ,Limite esquerdo da moldura          ,   ,Limite interior da moldura          *   *Limite direito da moldura         ,   ,Limite exterior da moldura               Moldura inteira         ,   ,Área da moldura para texto            Linha base             Carácter            Linha    *          Linha de texto    (n    	$          �   �O dicionário de sinónimos não está disponível para o idioma atual.
Verifique a instalação e, se necessário, instale o idioma pretendido.           $(ARG1) não tem suporte a verificação ortográfica ou então não está ativa.
Verifique a sua instalação e, se necessário, instale o módulo linguístico desejado ou
então ative-o em "Ferramentas - Opções - Definições de idioma - Auxiliares de escrita".        F   FA verificação ortográfica não está disponível.         8   8A hifenização não está disponível.        L   LNão é possível ler o dicionário personalizado $(ARG1).         N   NNão é possível criar o dicionário personalizado $(ARG1).         @   @Não foi possível encontrar a imagem $(ARG1).         D   DNão foi possível carregar uma imagem dissociada.    	     F   FNão foi definido o idioma para o termo selecionado.    
     �   �A camada do formulário não foi carregada porque os serviços necessários (com.sun.star.io.*) não puderam ser iniciados.   
     �   �A camada do formulário não foi gravada porque os serviços necessários (com.sun.star.io.*) não puderam ser iniciados.        z   zOcorreu um erro ao ler os controlos do formulário. Não foi possível carregar a camada do formulário.         ~   ~Ocorreu um erro ao escrever os controlos do formulário. Não foi possível guardar a camada do formulário.         f   fOcorreu um erro durante a leitura de uma marca. Nem todas as marcas foram carregadas.        j   jAs alterações ao código Basic serão perdidas. Será guardado o código original VBA.         T   TO código original VBA existente no documento não será guardado.          V   VA palavra-passe está incorreta. Não é possível abrir o documento.        �   �O método de encriptação utilizado neste documento não é suportado. Apenas é suportada a encriptação de palavras-passe compatível com o Microsoft Office 97/2000.        |   |O carregamento de apresentações do Microsoft PowerPoint encriptadas por palavra-passe não é suportado.         �   �A proteção por palavra-passe não é suportada para documentos guardados em formatos Microsoft Office.
Pretende guardar o documento sem proteção por palavra-passe?   (o    \           @   @$(ERR) ao executar o dicionário de sinónimos.         B   B$(ERR) ao executar a verificação ortográfica.          4   4$(ERR) ao executar a hifenização.         0   0$(ERR) ao criar o dicionário.          8   8$(ERR) ao definir o atributo de fundo.          .   .$(ERR) ao carregar a imagem.    *d    |        y  �  �   	Definição do contorno     Linha de contorno esquerda     Linha de contorno direita    Linha de contorno superior     Linha de contorno inferior     Linha de contorno horizontal     Linha de contorno vertical     Linha de contorno diagonal desde a parte superior esquerda até à inferior direita    Linha de contorno diagonal desde a parte inferior esquerda até à superior direita         y  �  �   	Definição do contorno     Linha de contorno esquerda     Linha de contorno direita    Linha de contorno superior     Linha de contorno inferior     Linha de contorno horizontal     Linha de contorno vertical     Linha de contorno diagonal desde a parte superior esquerda até à inferior direita    Linha de contorno diagonal desde a parte inferior esquerda até à superior direita            ,   ,       svx/res/frmsel.bmp    FP     V              Tabela             Consulta             SQL   FQ    F              LIKE             NOT            EMPTY            TRUE             FALSE            IS             BETWEEN            OR     	        AND    
        Média             Contar             Máximo            Mínimo            Soma             Todos            Qualquer             Algum            STDDEV_POP             STDDEV_SAMP            VAR_SAMP             VAR_POP            Recolher             Fusão             Interseção          L   L$(WIDTH) x $(HEIGHT) ($(WIDTH_IN_PX) x $(HEIGHT_IN_PX) px)         4   4$(WIDTH) x $(HEIGHT) com $(DPI) PPP             $(CAPACITY) kiB           Imagem GIF            Imagem JPEG           Imagem PNG            Imagem TIFF           Imagem WMF            Imagem MET    	        Imagem PCT    
        Imagem SVG            Imagem BMP            Desconhecido    
      "   "objeto de desenho   
     $   $objetos de desenho    
          objeto de grupo   
     "   "objetos de grupo    
     &   &objeto de grupo vazio   
     (   (objetos de grupo vazios   
        Tabela    
        Tabelas   
        Linha   
	     "   "Linha horizontal    

          Linha vertical    
          Linha diagonal    
        Linhas    
        Retângulo    
        Retângulos   
        Quadrado    
        Quadrados   
        Paralelograma   
          Paralelogramas    
        Losango   
        Losangos    
     (   (Retângulo arredondado    
     *   *Retângulos arredondados    
     &   &Quadrado arredondado    
     (   (Quadrados arredondados    
     *   *Paralelograma arredondado   
     ,   ,Paralelogramas arredondados   
     $   $Losango arredondado   
     &   &Losangos arredondados   
        Círculo    
        Círculos   
     "   "Setor de círculo   
      $   $Setores de círculo   
!        Arco    
"        Arcos   
#     &   &Segmento de círculo    
$     &   &Segmentos de círculo   
%        Elipse    
&        Elipses   
'          Setor de elipse   
(     "   "Setores de elipse   
)          Arco elíptico    
*     "   "Arcos elípticos    
+     $   $Segmento elíptico    
,     &   &Segmentos elípticos    
-        Polígono   
.     $   $Polígono %2 cantos   
/        Polígonos    
0        Polilinha   
1     (   (Polilinha com %2 cantos   
2        Polilinhas    
3        Curva Bézier   
4          Curvas Bézier    
5        Curva Bézier   
6          Curvas Bézier    
7        Linha livre   
8        Linhas livres   
9        Linha livre   
:        Linhas livres   
;        Curva   
<          Objetos curvos    
=        Curva   
>          Objetos curvos    
?     *   *Curva polinomial natural    
@     ,   ,Curvas polinomiais naturais   
A     ,   ,Curva polinomial periódica   
B     0   0Curvas polinominais periódicas   
C     "   "Moldura de texto    
D     "   "Moldura de texto    
E     ,   ,Moldura de texto associada    
F     .   .Molduras de texto associadas    
G     *   *Objeto de texto ajustado    
H     *   *Objetos de texto ajustado   
I     *   *Objeto de texto ajustado    
J     *   *Objetos de texto ajustado   
K     "   "Texto do título    
L     "   "Textos do título   
M     "   "Contorno do texto   
N     $   $Contorno dos textos   
O        imagem    
P        imagens   
Q     "   "Imagem associada    
R     $   $Imagens associadas    
S     &   &Objeto gráfico vazio   
T     *   *Objetos gráficos vazios    
U     (   (Imagem associada vazia    
V     *   *Imagens associadas vazias   
W        Metaficheiro    
X        Metaficheiros   
Y     (   (Meta-ficheiro associado   
Z     *   *Meta-ficheiros associados   
[        Mapa de bits    
\        Mapas de bits   
]     (   (Mapa de bits associado    
^     *   *Mapas de bits associados    
_        Imagem Mac    
`        Imagens Mac   
a     &   &Imagem Mac associada    
b     (   (Imagens Mac associadas    
c     *   *Objeto incorporado (OLE)    
d     ,   ,Objetos incorporados (OLE)    
e     6   6Objeto incorporado e associado (OLE)    
f     8   8Objetos incorporados e associados (OLE)   
g        Objeto    
h        Moldura   
i        Molduras    
j        Moldura   
k     &   &Conectores de objetos   
l     &   &Conectores de objetos   
m        Chamada   
n        Chamadas    
o     (   (Pré-visualizar objeto    
p     (   (Pré-visualizar objetos   
q     $   $Linha de dimensão    
r     ,   ,Objetos de dimensionamento    
s     $   $Objetos de desenho    
t     &   &Sem objeto de desenho   
u        e   
v     &   &Objeto(s) de desenho    
w        Controlo    
x        Controlos   
y        Cubo 3D   
z        Cubos 3D    
{     $   $Objeto de extrusão   
|     &   &Objetos de extrusão    
}        Texto 3D    
~        Textos 3D   
     $   $Objeto de rotação   
�     &   &Objetos de rotação    
�        Objeto 3D   
�        Objetos 3D    
�        Polígonos 3D   
�        Cena 3D   
�        Cenas 3D    
�        Esfera    
�        Esferas   
�     *   *Mapa de bits transparente   
�     :   :Mapa de bits associado com transparência   
�     ,   ,Mapas de bits transparentes   
�     <   <Mapas de bits associados com transparência   
�        Forma   
�        Formas    
�     $   $Objeto multimédia    
�     $   $Objetos multimédia   
�        font work   
�        font works    
�        SVG   
�        SVGs    
�        com cópia    
�     4   4Definir posição e tamanho para %1   
�        Eliminar %1   
�     (   (Mover %1 para a frente    
�     *   *Mover %1 mais para trás    
�     (   (Mover %1 para a frente    
�     $   $Mover %1 para trás   
�     &   &Inverter ordem de %1    
�        Mover %1    
�     "   "Redimensionar %1    
�        Rodar %1    
�     *   *Inverter %1 na horizontal   
�     (   (Inverter %1 na vertical   
�     (   (Inverter %1 na diagonal   
�     (   (Inverter %1 manualmente   
�     ,   ,Distorcer %1 (inclinação)   
�     *   *Organizar %1 em círculo    
�     &   &Curvar %1 em círculo   
�        Distorcer %1    
�        Anular %1   
�     6   6Modificar propriedades bézier de %1    
�     6   6Modificar propriedades bézier de %1    
�        Fechar %1   
�     4   4Definir direção de saída para %1   
�     2   2Definir atributo relativo para %1   
�     6   6Definir ponto de referência para %1    
�        Agrupar %1    
�        Desagrupar %1   
�     (   (Aplicar atributos a %1    
�     &   &Aplicar estilos a %1    
�     &   &Remover estilo de %1    
�     *   *Converter %1 em polígono   
�     ,   ,Converter %1 em polígonos    
�     &   &Converter %1 em curva   
�     (   (Converter %1 em curvas    
�        Alinhar %1    
�     $   $Alinhar %1 ao topo    
�     $   $Alinhar %1 à base    
�     *   *Centrar %1 na horizontal    
�     (   (Alinhar %1 à esquerda    
�     &   &Alinhar %1 à direita   
�     (   (Centrar %1 na vertical    
�        Centrar %1    
�          Transformar %1    
�        Combinar %1   
�        Combinar %1   
�        Dividir %1    
�        Dividir %1    
�        Dividir %1    
�     4   4StarDraw Dos Zeichnung importieren    
�     "   "HPGL importieren    
�          DXF importieren   
�     *   *Converter %1 em contorno    
�     *   *Converter %1 em contornos   
�        Unir %1   
�        Subtrair %1   
�        Intersetar %1   
�     0   0Distribuir objetos selecionados   
�     &   &Igualar largura de %1   
�     &   &Igualar altura de %1    
�     "   "Inserir objeto(s)   
�        Cortar %1   
�     <   <Colar conteúdo da área de transferência    
�     &   &Arrastar e largar %1    
�     *   *Inserir arrastar e largar   
�     &   &Inserir ponto para %1   
�     2   2Inserir ponto de colagem para %1    
�     ,   ,Mover ponto de referência    
�     ,   ,Alterar %1 geometricamente    
�        Mover %1    
�     "   "Redimensionar %1    
�        Rodar %1    
�     *   *Inverter %1 na horizontal   
�     (   (Inverter %1 na vertical   
�     (   (Inverter %1 na diagonal   
�     (   (Inverter %1 manualmente   
�     ,   ,Distorcer %1 (inclinação)   
�     *   *Organizar %1 em círculo    
�     &   &Curvar %1 em círculo   
�        Distorcer %1    
�     $   $Alterar raio em %1    
�        Alterar %1    
�     "   "Redimensionar %1    
�        Mover %1    
�     (   (Mover ponto final de %1   
�     &   &Ajustar ângulo em %1   
�        Alterar %1    
�     .   .Gradiente interativo para %1    
�     2   2Transparência interativa para %1   
�        Recortar %1   
�     B   BEditar texto: parágrafo %1, linha %2, coluna %3    
�          %1 selecionado    
�        Ponto de %1   
�          %2 pontos de %1   
�     (   (Ponto de colagem de %1    
�     ,   ,%2 pontos de colagem de %1    
�          Marcar objetos    
�     *   *Marcar objetos adicionais   
�        Marcar pontos   
�     *   *Marcar pontos adicionais    
�     *   *Marcar pontos de colagem    
�     4   4Marcar pontos de colagem adicionais   
�        Criar %1    
�        Inserir %1    
�        Copiar %1   
�     0   0Alterar ordem dos objetos de %1   
�     $   $Editar texto de %1               Inserir página        "   "Eliminar página              Copiar página         ,   ,Alterar ordem das páginas         *   *Atribuir página de fundo        6   6Limpar atributos da página de fundo         4   4Mover atributos da página de fundo        6   6Alterar atributos da página de fundo        "   "Inserir documento   	          Inserir camada    
          Eliminar camada        *   *Alterar ordem das camadas        2   2Alterar nome do objeto de %1 para        0   0Alterar título do objeto de %1        4   4Alterar descrição do objeto de %1           Padrão           Ligado            Desligado           Sim           Não            Tipo 1            Tipo 2            Tipo 3            Tipo 4            Horizontal            Vertical            Automático           Desligar            Proporcional         <   <Ajustar ao tamanho (linhas separadamente)          ,   ,Utilizar atributos rígidos           Acima            No centro   !        Em baixo    "     (   (Aproveitar altura total   #        Esticado    $        Esquerda    %        No centro   &        Direita   '     *   *Aproveitar largura total    (        Esticado    )        Desligado   *        intermitente    +        Percorrer   ,        alternado   -     &   &Deslocar para dentro    .          para a esquerda   /          para a direita    0        para cima   1        para baixo    2     "   "Conector padrão    3          Conector linear   4        Conector reto   5     &   &Conector curvilíneo    6        Padrão   7        Raio    8        automático   9     "   "extremo esquerdo    :     $   $interior (centrado)   ;          extremo direito   <        automático   =        na linha    >     $   $linha interrompida    ?          abaixo da linha   @        centrado    A     "   "círculo inteiro    B     "   "Setor de círculo   C     &   &Segmento de círculo    D        Arco    E        Sombra    F        Cor da sombra   G     .   .Contorno horizontal da sombra   H     ,   ,Contorno vertical da sombra   I     *   *Transparência da sombra    J        Sombra 3D   K     &   &Sombra em perspetiva    R          Tipo de chamada   S        Ângulo dado    T        Ângulo   U        Espaço livre   V     $   $Direção de saída   W     .   .Posição de saída relativa    X     $   $Posição de saída   Y     $   $Posição de saída   Z     $   $Extensão da linha    [     0   0Extensão automática de linha    c        Raio do canto   d     2   2Espaçamento do contorno esquerdo   e     2   2Espaçamento do contorno direito    f     2   2Espaçamento do contorno superior   g     2   2Espaçamento do contorno inferior   h     *   *Ajustar altura da moldura   i     *   *Altura mínima da moldura   j     *   *Altura máxima da moldura   k     ,   ,Ajustar largura da moldura    l     ,   ,Largura mínima da moldura    m     ,   ,Largura máxima da moldura    n     *   *Âncora vertical do texto   o     ,   ,Âncora horizontal do texto   p     *   *Ajustar texto à moldura    q        Vermelho    r        Verde   s        Azul    t        Brilho    u        Contraste   v        Gama    w          Transparência    x        Inverter    y        Modo gráfico   z            {            |            }            ~                        �     $   $Atributos diversos    �     (   (Proteção de posição   �     &   &Proteção de tamanho   �        Não imprimir   �     &   &Indicador de camadas    �        Ní~vel   �          Nome do objeto    �          Ângulo inicial   �        Ângulo final   �        Posição X   �        Posição Y   �        Largura   �        Altura    �     &   &Ângulo de rotação    �     (   (Ângulo de cisalhamento   �     &   &Atributo desconhecido   �          Estilo de linha   �     "   "Padrão da linha    �     "   "Largura da linha    �        Cor da linha    �     "   "Início da linha    �        Fim da linha    �     ,   ,Largura do início de linha   �     (   (Largura do fim de linha   �     *   *Início de linha centrado   �     &   &Fim de linha centrado   �     (   (Transparência da linha   �          União de linha   �     (   (Linha reservada para 2    �     (   (Linha reservada para 3    �     (   (Linha reservada para 4    �     (   (Linha reservada para 5    �     (   (Linha reservada para 6    �     $   $Atributos da linha    �     (   (Estilo de preenchimento   �     &   &Cor de preenchimento    �        Gradiente   �        Tracejado   �     .   .Mapa de bits de preenchimento   �          Transparência    �     &   &Número de gradientes   �     *   *Preenchimento lado a lado   �     <   <Posição do mapa de bits de preenchimento    �     :   :Largura do mapa de bits de preenchimento    �     8   8Altura do mapa de bits de preenchimento   �     (   (Gradiente transparente    �     0   0Preenchimento reservado para 2    �     (   (Tamanho não está em %   �     0   0Deslocamento X lado a lado em %   �     0   0Deslocamento Y lado a lado em %   �     (   (Escala do mapa de bits    �     .   .Mapa de bits reservado para 3   �     .   .Mapa de bits reservado para 4   �     .   .Mapa de bits reservado para 5   �     .   .Mapa de bits reservado para 6   �     .   .Mapa de bits reservado para 7   �     .   .Mapa de bits reservado para 8   �     .   .Posição X lado a lado em %    �     .   .Posição Y lado a lado em %    �     (   (Preenchimento de fundo    �     0   0Preenchimento reservado para 10   �     0   0Preenchimento reservado para 11   �     0   0Preenchimento reservado para 12   �     $   $Atributos da área    �          Estilo Fontwork   �     &   &Alinhamento Fontwork    �     &   &Espaçamento Fontwork   �     ,   ,Início das letras Fontwork   �     "   "Refletir Fontwork   �     &   &Contorno do Fontwork    �          Sombra Fontwork   �     (   (Cor da sombra Fontwork    �     2   2Deslocamento X da sombra Fontwork   �     2   2Deslocamento Y da sombra Fontwork   �     .   .Ocultar contorno do Fontwork    �     2   2Transparência da sombra Fontwork   �     *   *Fontwork reservado para 2   �     *   *Fontwork reservado para 3   �     *   *Fontwork reservado para 4   �     *   *Fontwork reservado para 5   �     *   *Fontwork reservado para 6   �        Sombra    �        Cor da sombra   �     *   *Espaçamento X da sombra    �     *   *Espaçamento Y da sombra    �     *   *Transparência da sombra    �        Sombra 3D   �     &   &Sombra em perspetiva    �          Tipo de legenda   �     (   (Ângulo fixo da legenda   �     $   $Ângulo da legenda    �     8   8Espaçamento entre as linhas de legenda   �     2   2Alinhamento de saída da legenda    �     ,   ,Saída relativa da legenda    �     ,   ,Saída relativa da legenda    �     ,   ,Saída absoluta da legenda    �     .   .Extensão da linha de legenda   �     <   <Extensão automática das linhas de legenda   �        Raio do canto   �     *   *Altura mínima da moldura   �     .   .Ajuste automático da altura    �     *   *Ajustar texto à moldura    �     >   >Espaçamento à esquerda da moldura de texto    �     <   <Espaçamento à direita da moldura de texto   �     :   :Espaçamento superior à moldura de texto   �     :   :Espaçamento inferior à moldura de texto   �     *   *Âncora vertical do texto   �     *   *Altura máxima da moldura   �     ,   ,Largura mínima da moldura    �     ,   ,Largura máxima da moldura    �     .   .Ajuste automático da largura   �     ,   ,Âncora horizontal do texto            Animação         (   (Direção da animação        .   .Animação inicia do interior        0   0Animação termina no interior         (   (Número de animações         (   (Cadência da animação        $   $Ritmo da animação        ,   ,Contorno do fluxo de texto         "   "Ajuste de formas    	     *   *Atributos personalizados    
     F   FUtilizar espaçamento de linhas independente da letra        &   &Moldar texto à forma        B   BAumento automático da forma para ajustar o texto        *   *SvDraw reservado para 18         *   *SvDraw reservado para 19         "   "Tipo de conector         4   4Espaçamento horizontal do objeto 1        2   2Espaçamento vertical do objeto 1        4   4Espaçamento horizontal do objeto 2        2   2Espaçamento vertical do objeto 2        4   4Espaçamento de adesão do objeto 1        4   4Espaçamento de adesão do objeto 2        *   *Número de linhas móveis        (   (Deslocamento da linha 1        (   (Deslocamento da linha 2        (   (Deslocamento da linha 3   $     $   $Tipo de dimensões    %     :   :Valor de dimensão - posição horizontal   &     8   8Valor de dimensão - posição vertical   '     2   2Espaço entre linhas de dimensão   (     :   :Projeção da linha auxiliar de dimensão   )     <   <Espaçamento da linha auxiliar de dimensão   *     :   :Reserva da linha 1 da ajuda da dimensão    +     :   :Reserva da linha 2 da ajuda da dimensão    ,     .   .Dimensões da margem inferior   -     H   HValor de dimensionamento oblíquo à linha de dimensão   .     6   6Rodar valor de dimensão em 180 graus   /     2   2Projeção da linha da dimensão    0     "   "Unidade de medida   1     *   *Fator adicional de escala   2     *   *Mostrar unidade de medida   3     .   .Formato do valor de dimensão   4     B   BPosicionamento automático do valor de dimensão    5     L   LÂngulo do posicionamento automático do valor de dimensão   6     :   :Determinar ângulo do valor de dimensão    7     .   .Ângulo do valor de dimensão   8          Casas decimais    9     2   2Dimensionamento reservado para 5    :     2   2Dimensionamento reservado para 6    ;     2   2Dimensionamento reservado para 7    =     "   "Tipo de círculo    >          Ângulo inicial   ?        Ângulo final   @     *   *Círculo reservado para 0   A     *   *Círculo reservado para 1   B     *   *Círculo reservado para 2   C     *   *Círculo reservado para 3   D          Objeto visível   E     .   .Posição do objeto protegido   F     ,   ,Tamanho do objeto protegido   G     $   $Objeto imprimível    H        ID de nível    I        Camada    J          Nome do objeto    K     &   &Posição X, completa   L     &   &Posição Y, completa   M        Largura total   N          Altura completa   O     (   (Posição X individual    P     (   (Posição Y individual    Q     $   $Largura individual    R     "   "Altura individual   S          Largura lógica   T          Altura lógica    U     0   0Ângulo de rotação individual   V     .   .Ângulo de desvio individual    W     &   &Mover horizontalmente   X     $   $Mover verticalmente   Y     *   *Redimensionar X, simples    Z     *   *Redimensionar Y, simples    [     &   &Rotação individual    \     .   .Desvio horizontal individual    ]     ,   ,Desvio vertical individual    ^     *   *Redimensionar X, completo   _     *   *Redimensionar Y, completo   `        Rodar todos   a     ,   ,Desvio horizontal, completo   b     *   *Desvio vertical, completo   c     *   *Ponto de referência 1 X    d     *   *Ponto de referência 1 Y    e     *   *Ponto de referência 2 X    f     *   *Ponto de referência 2 Y    g        Hifenização   h          Mostrar marcas    i     (   (Avanços de numeração   j     &   &Nível de numeração   k     &   &Marcas e numeração    l        Avanços    m     0   0Espaçamento entre parágrafos    n     *   *Espaçamento entre linhas   o     *   *Alinhamento de parágrafo   p        Tabuladores   q        Cor da letra    r     (   (Conjunto de caracteres    s     "   "Tamanho da letra    t     "   "Largura da letra    u     $   $Negrito (espessura)   v        Sublinhado    w        Sobrelinha    x        Rasurado    y        Itálico    z        Contorno    {        Sombra    |     &   &Sobrescrito/subscrito   }        Kerning   ~          Kerning manual         (   (Não sublinhar espaços   �        Tabulação   �     *   *Quebra opcional de linha    �     ,   ,Carácter não convertível   �        Campos    �        Vermelho    �        Verde   �        Azul    �        Brilho    �        Contraste   �        Gama    �          Transparência    �        Inverter    �        Modo gráfico   �        Recortar    �            �            �            �            �     ,   ,Aplicar atributos de tabela   �     .   .Tabela de formato automático   �          Inserir coluna    �        Inserir linha   �          Eliminar coluna   �          Eliminar linha    �     "   "Dividir células    �        Unir células   �     "   "Formatar célula    �     "   "Distribuir linhas   �     $   $Distribuir colunas    �     "   "Estilo de tabela    �     2   2Definições do estilo de tabela    �     .   .Eliminar conteúdo da célula   �     6   6Próxima ligação na cadeia de texto   '        [Tudo]    'W     (   (Chegou ao fim da folha    'Y        Favorito    'Z        X   '[        Y   '\        Z   ']        R:    '^        G:    '_     ,   ,Chegou ao fim do documento    '`          Incluir estilos   'a        (Procurar)    'b        (Substituir)    'c     2   2Procurar est~ilos de parágrafos    'd        B:    'e     .   .Procurar est~ilos de células   'h     2   2Termo de procura não encontrado    'i     p   pTem a certeza de que pretende rejeitar os dados de recuperação de documentos do %PRODUCTNAME?   'j     .   .Chegou ao inicio do documento   '�        Contínuo   '�        Gradiente   '�        Mapa de bits    '�          Estilo de linha   '�        Nenhum    '�        Cor   '�        Tracejado   '�        - nenhum -    '�     "   "Sem preenchimento   '�        Padrão   '�        Contornos   '�          Cor do contorno   '�     $   $Estilo do contorno    '�        Cor de realce   '�     $   $Limpar formatação   '�     "   "Mais opções...    '�     l   lO nome do tipo de letra. O tipo de letra atual não está disponível e será substituido.    '�     &   &Nome do tipo de letra   '�        Cor de linha    '�          Mais estilos...   '�     &   &Cor de preenchimento    '�     &   &Mais numerações...    '�          Mais marcas...    '�          Paleta padrão    '�     $   $Cores do documento    '�     "   "Cor do documento    '�     V   VModo de inserção. Clique para mudar para o modo de substituição.    '�     V   VModo de substituição. Clique para mudar para o modo de inserção.    '�        Substituir    '�     N   NAssinatura digital: a assinatura do documento está correta.    '�     ~   ~Assinatura digital: a assinatura do documento está correta, mas não foi possível validar os certificados.    '�     �   �Assinatura digital: a assinatura do documento não coincide com o conteúdo do documento. Não deve confiar neste documento.    '�     F   FAssinatura digital: o documento não está assinado.    '�     �   �Assinatura digital: a assinatura do documento e o certificado estão corretos, mas nem todas as partes do documento estão assinadas.   '�        Ponta de seta   (        Esquerda    (        Direita   (        Centro    (        Decimal   (
        Utilizador    (          Tema da galeria   (        Itens do tema   (          Pré-visualizar   (        Fechar    (#        Preto   ($        Azul    (%        Verde   (&        Ciano   ('        Vermelho    ((        Magenta   (*        Cinzento    (1        Amarelo   (2        Branco    (3     "   "Azul acinzentado    (4        Laranja   (5        Turquesa    (6        Turquoise   (7          Azul clássico    (8        Blue classic    (<        Seta    (=        Quadrado    (>        Círculo    (A          Transparência    (B        Centrado    (C        Não centrado   (D        Lista   (E        Filtro    (�        Transparente    (�        Cor original    (�          Paleta de cores   (�        Tolerância   (�          Substituir por:   (�     "   "Inserir objeto(s)   (�     .   .Criar objetos de rotação 3D   (�     &   &Número de segmentos    (�     (   (Profundidade do objeto    (�     "   "Distância focal    (�     &   &Posição da câmara    (�          Rodar objeto 3D   )      *   *Criar objeto de extrusão   )     *   *Criar objeto de rotação   )     "   "Dividir objeto 3D   )        Atributos 3D    )        Padrão   )     $   $Escala de cinzentos   )        Preto/Branco    )        Marca d'água   )     *   *Intel Indeo Video (*.ivf)   )     ,   ,Vídeo para Windows (*.avi)   )     *   *Vídeo QuickTime (*.mov)    )     J   JMPEG - Motion Pictures Experts Group (*.mpe;*.mpeg;*.mpg)   )         <Todos>   )!          Inserir áudio    )"          Inserir vídeo    )#        Fundo   ),        Violeta   )-        Bordeaux    ).        Amarelo claro   )/        Verde claro   )0          Violeta escuro    )1        Salmão   )2        Azul marinho    )4     8   8Verde 1 (cor principal do %PRODUCTNAME)   )5          Verde acentuado   )6          Azul acentuado    )7     "   "Laranja acentuado   )8        Roxo    )9          Roxo acentuado    ):     "   "Amarelo acentuado   )@        3D    )A        Preto 1   )B        Preto 2   )C        Azul    )D        Castanho    )E        Moeda   )F        Moeda 3D    )G     "   "Moeda em cinzento   )H        Moeda lavanda   )I     "   "Moeda em turquesa   )J        Cinzento    )K        Verde   )L        Lavanda   )M        Vermelho    )N        Turquesa    )O        Amarelo   )Z     *   *Acabamento de linha plano   )[     ,   ,Acabamento de linha redondo   )\     .   .Acabamento de linha quadrado    )]     (   (União de linha, média   )^     (   (União de linha, bisel    )_     ,   ,União de linha, esquadria    )`     *   *União de linha, redonda    )c        Black   )d        Blue    )e        Green   )f        Cyan    )g        Red   )h        Magenta   )j        Gray    )q        Yellow    )r        White   )s        Blue gray   )t        Orange    )u        Violet    )v        Bordeaux    )w        Pale yellow   )x        Pale green    )y        Dark violet   )z        Salmon    ){        Sea blue    )|        Sun   )}        Gráfico    )~        Chart   )        Roxo    )�        Purple    )�        Azul celeste    )�        Sky blue    )�        Amarelo verde   )�        Yellow green    )�        Cor de rosa   )�        Pink    )�     2   2Green 1 (%PRODUCTNAME Main Color)   )�        Green Accent    )�        Blue Accent   )�        Orange Accent   )�        Purple    )�        Purple Accent   )�        Yellow Accent   )�          Tango: manteiga   )�          Tango: laranja    )�     "   "Tango: chocolate    )�     "   "Tango: camaleão    )�     $   $Tango: azul celeste   )�        Tango: ameixa   )�     *   *Tango: vermelho escarlate   )�     "   "Tango: alumínio    )�        Tango: Butter   )�        Tango: Orange   )�     "   "Tango: Chocolate    )�     "   "Tango: Chameleon    )�          Tango: Sky Blue   )�        Tango: Plum   )�     $   $Tango: Scarlet Red    )�     "   "Tango: Aluminium    )�     &   &Black 45 Degrees Wide   )�     "   "Black 45 Degrees    )�     "   "Black -45 Degrees   )�     "   "Black 90 Degrees    )�     (   (Red Crossed 45 Degrees    )�     &   &Red Crossed 0 Degrees   )�     (   (Blue Crossed 45 Degrees   )�     (   (Blue Crossed 0 Degrees    )�     (   (Blue Triple 90 Degrees    )�          Black 0 Degrees   )�        Hatching    )�     *   *Preto 45 graus de largura   )�          Preto 45 graus    )�          Preto -45 graus   )�          Preto 90 graus    )�     *   *Vermelho cruzado 45 graus   )�     *   *Vermelho cruzado 0 graus    )�     &   &Azul cruzado 45 graus   )�     &   &Azul cruzado 0 graus    )�     &   &Azul triplo 90 graus    )�        Preto 0 graus   )�        Tracejado   )�        Empty   )�        Sky   )�        Aqua    )�        Coarse    )�        Space Metal   )�        Space   )�        Metal   )�        Wet   )�        Marble    )�        Linen   )�        Stone   )�        Pebbles   )�        Wall    )�        Red Wall    )�        Pattern   )�        Leaves    )�          Lawn Artificial   )�        Daisy   )�        Orange    )�        Fiery   )�        Roses   )�        Bitmap    )�        Vazio   )�        Céu    )�        Água   )�          Granular grosso   )�        Mercúrio   )�        Espacial    )�        Metal   )�        Gotas   )�        Mármore    )�        Linho   )�        Pedra   )�        Cascalho    )�        Parede    )�        Tijolo    )�        Rede    )�        Folhas    )�     "   "Relva artificial    )�        Margarida   )�        Laranja   )�        Chamas    )�        Rosas   )�        Mapa de bits    )�     "   "Ultrafine Dashed    )�        Fine Dashed   )�     *   *Ultrafine 2 Dots 3 Dashes   )�        Fine Dotted   )�     $   $Line with Fine Dots   )�     "   "Fine Dashed (var)   )�     &   &3 Dashes 3 Dots (var)   )�     (   (Ultrafine Dotted (var)    )�        Line Style 9    )�        2 Dots 1 Dash   )�        Dashed (var)    )�        Dash    *         Line Style    *     &   &Tracejado ultra fino    *          Tracejado fino    *     $   $2 pontos 3 traços    *          Pontilhado fino   *     (   (Linha com pontos finos    *          Tracejado fino    *     $   $3 traços 3 pontos    *     &   &Pontilhado ultra fino   *	     "   "Estilo de linha 9   *
     "   "2 pontos 1 traço   *        Tracejado   *        Tracejado   *          Estilo de linha   *2     (   (Seleção de impressão   *3     D   DPretende imprimir a seleção ou todo o documento?    *4        ~Todo   *5        ~Seleção    *D        Recortar    *E          Modo de imagem    *F        Vermelho    *G        Verde   *H        Azul    *I        Brilho    *J        Contraste   *K        Gama    *L          Transparência    *Y        Automático   *^     *   *Ações a anular: $(ARG1)   *_     ,   ,Ações a refazer: $(ARG1)    *`     *   *Ações a anular: $(ARG1)   *a     ,   ,Ações a refazer: $(ARG1)    *b        Transparency    *c          Transparência    *m     &   &Material 3D colorido    *n        Cor do texto    *o        Cor de fundo    *p        Nenhum    *q        Sólida   *r        Tracejado   *s        Gradiente   *t        Mapa de bits    *u        com   *v        Estilo    *w        e   *�     $   $Controle de cantos    *�     .   .Seleção de ponto de canto.    *�     &   &Controle de ângulos    *�     ,   ,Seleção do maior ângulo.   *�     "   "Esquerda superior   *�          Centro superior   *�     "   "Direita superior    *�          Centro esquerda   *�        Centro    *�          Centro direita    *�     "   "Esquerda inferior   *�          Centro inferior   *�     "   "Direita inferior    *�        0 graus   *�        45 graus    *�        90 graus    *�        135 graus   *�        180 graus   *�        225 graus   *�        270 graus   *�        315 graus   *�     &   &Controlo de contornos   *�     .   .Aqui pode editar o contorno.    *�     2   2Seleção de caracteres especiais   *�     &   &Código de carácter    *�     @   @Selecione os caracteres especiais nesta área.    *�     (   (Exportação de imagens   *�        Extrusão   *�        Fontwork    *�     "   "Cor de extrusão    *�        ~0 cm   *�        ~1 cm   *�        ~2,5 cm   *�        ~5 cm   *�        10 ~cm    *�        0 polegadas   *�          0,~5 polegadas    *�        ~1 polegada   *�        ~2 polegadas    *�        ~4 polegadas    *�        Páginas    +n     (   (Aplicar forma Fontwork    +o     >   >Aplicar letras com a mesma altura no Fontwork   +p     .   .Aplicar alinhamento Fontwork    +r     @   @Aplicar espaçamento entre caracteres Fontwork    +s     ,   ,Ativar/desativar extrusão    +t     $   $Inclinar para baixo   +u     $   $Inclinar para cima    +v     *   *Inclinar para a esquerda    +w     (   (Inclinar para a direita   +x     2   2Alterar profundidade de extrusão   +y     &   &Alterar orientação    +z     ,   ,Alterar tipo de projeção    +{     &   &Alterar iluminação    +|          Alterar brilho    +}     2   2Alterar superfície de extrusão    +~     *   *Alterar cor de extrusão    +�     4   4Marcas circulares sólidas pequenas   +�     4   4Marcas circulares sólidas grandes    +�     ,   ,Marcas em losango sólidas    +�     2   2Marcas quadradas sólidas grandes   +�     D   DMarcas preenchidas em forma de seta para a direita    +�     8   8Marcas em forma de seta para a direita    +�     (   (Marcas de verificação   +�     &   &Marcas de validação   +�     "   "Número 1) 2) 3)    +�     "   "Número 1. 2. 3.    +�     $   $Número (1) (2) (3)   +�     6   6Número romano maiúsculo I. II. III.   +�     *   *Letra maiúscula A) B) C)   +�     *   *Letra minúscula a) b) c)   +�     .   .Letra minúscula (a) (b) (c)    +�     6   6Número romano minúsculo i. ii. iii.   +�     Z   ZNumérico, numérico, letras minúsculas, marca circular sólida pequena    +�     N   NNumérico, letras minúsculas, marca circular sólida pequena   +�     x   xNumérico, letras minúsculas, romanas minúsculas, letras maiúsculas, marca circular sólida pequena    +�        Numéricas    +�     �   �Romanas maiúsculas, letras maiúsculas, romanas minúsculas, letras minúsculas, marca circular sólida pequena    +�     �   �Letras maiúsculas, romanas maiúsculas, letras minúsculas, romanas minúsculas, marca circular sólida pequena    +�     4   4Numéricas com todos os subníveis    +�     �   �Marca a apontar para a direita, marca de seta para a direita, marca em losango sólido, marca circular sólida pequena    +�     ,   ,Estilos de design de tabela   +�        Cor da letra    +�     .   .Procurar valor como mostrado    +�     (   (Maiúsculas/minúsculas   +�        Localizar   +�     D   DO documento foi modificado. Clique para o guardar.    +�     L   LO documento não foi modificado desde a última gravação.   +�     (   (A carregar documento...   +�     (   (Palavra-passe inválida   +�     2   2As palavras-passe não coincidem    +�     6   6Ajustar diapositivo à janela atual.    ,     ,   ,Recuperação bem sucedida    ,     .   .Documento original recuperado   ,     (   (Falha na recuperação    ,     (   (Recuperação em curso    ,     &   &Ainda não recuperado   ,     �   �O %PRODUCTNAME %PRODUCTVERSION vai iniciar a recuperação dos documentos. Este processo poderá ser demorado, dependendo do tamanho dos documentos.    ,     f   fA recuperação dos documentos terminou.
Clique em "Terminar" para ver os documentos.   ,        Ter~minar   ,     |   |Nível de ampliação. Clique no botão direito para mudar o nível clique para abrir a caixa de diálogo.    ,        Ampliar   ,        Reduzir   ,     "   "Personali~zar...    ,         ~Infinito   ,!        Es~boço    ,"        ~Mate   ,#        ~Plástico    ,$        Me~tal    ,%        ~Muito justo    ,&        ~Justo    ,'        ~Normal   ,(        ~Largo    ,)        Muito ~largo    ,*     "   "Personali~zar...    ,+     0   0~Kerning em pares de caracteres   ,8     (   (Extrusão para noroeste   ,9     &   &Extrusão para norte    ,:     (   (Extrusão para nordeste   ,;     &   &Extrusão para oeste    ,<     &   &Extrusão para trás    ,=     $   $Extrusão para este   ,>     (   (Extrusão para sudoeste   ,?     $   $Extrusão para Sul    ,@     (   (Extrusão para sudeste    ,B        ~Perspetiva   ,C        P~aralelo   ,D        ~Acentuar   ,E        ~Normal   ,F        A~tenuar    ,h     &   &Alinhar à es~querda    ,i     $   $Alinhar ao ~centro    ,j     $   $Alinhar à ~direita   ,k     &   &~Palavra justificada    ,l     &   &Es~ticar justificado    ,v        25%   ,w        50%   ,x        75%   ,y        100%    ,z        150%    ,{        200%    ,|          Página inteira   ,}     $   $Largura da página    ,~        Vista ideal   .�        Gradient    .�     "   "Linear blue/white   .�     &   &Linear magenta/green    .�     $   $Linear yellow/brown   .�     $   $Radial green/black    .�     "   "Radial red/yellow   .�     &   &Rectangular red/white   .�     $   $Square yellow/white   .�     0   0Ellipsoid blue grey/light blue    .�     &   &Axial light red/white   .�        Diagonal 1l   .�        Diagonal 1r   .�        Diagonal 2l   .�        Diagonal 2r   .�        Diagonal 3l   .�        Diagonal 3r   .�        Diagonal 4l   .�        Diagonal 4r   .�        Diagonal Blue   .�          Diagonal Green    .�          Diagonal Orange   .�        Diagonal Red    .�     $   $Diagonal Turquoise    .�          Diagonal Violet   .�        From a Corner   .�     $   $From a Corner, Blue   .�     &   &From a Corner, Green    .�     &   &From a Corner, Orange   .�     $   $From a Corner, Red    .�     *   *From a Corner, Turquoise    .�     &   &From a Corner, Violet   .�          From the Middle   /      &   &From the Middle, Blue   /     (   (From the Middle, Green    /     (   (From the Middle, Orange   /     &   &From the Middle, Red    /     ,   ,From the Middle, Turquoise    /     (   (From the Middle, Violet   /        Horizontal    /          Horizontal Blue   /     "   "Horizontal Green    /	     "   "Horizontal Orange   /
          Horizontal Red    /     &   &Horizontal Turquoise    /     "   "Horizontal Violet   /        Radial    /        Radial Blue   /        Radial Green    /        Radial Orange   /        Radial Red    /     "   "Radial Turquoise    /        Radial Violet   /        Vertical    /        Vertical Blue   /          Vertical Green    /          Vertical Orange   /        Vertical Red    /     $   $Vertical Turquoise    /          Vertical Violet   /        Gray Gradient   /          Yellow Gradient   /          Orange Gradient   /        Red Gradient    /        Pink Gradient   /         Sky   /!        Cyan Gradient   /"        Blue Gradient   /#        Purple Pipe   /$        Night   /%          Green Gradient    /&        Tango Green   /'     $   $Subtle Tango Green    /(        Tango Purple    /)        Tango Red   /*        Tango Blue    /+        Tango Yellow    /,        Tango Orange    /-        Tango Gray    /.        Argile    //        Olive Green   /0        Grison    /1        Sunburst    /2        Brownie   /3        Sun-Set   /4        Deep Green    /5        Deep Orange   /6        Deep Blue   /7        Purple Haze   /D        Gradiente   /E     $   $Azul/branco linear    /F     &   &Magenta/verde linear    /G     (   (Amarelo/castanho linear   /H     $   $Verde/preto radial    /I     (   (Vermelho/amarelo radial   /J     ,   ,Vermelho/branco retangular    /K     (   (Amarelo/branco quadrado   /L     8   8Azul acinzentado/azul claro elipsoidal    /M     ,   ,Vermelho claro/branco axial   /N        Diagonal 1e   /O        Diagonal 1d   /P        Diagonal 2e   /Q        Diagonal 2d   /R        Diagonal 3e   /S        Diagonal 3d   /T        Diagonal 4e   /U        Diagonal 4d   /V        Azul diagonal   /W          Verde diagonal    /X     "   "Laranja diagonal    /Y     "   "Vermelho diagonal   /Z     "   "Turquesa diagonal   /[     "   "Violeta diagonal    /\          Desde um canto    /]     &   &Desde um canto, azul    /^     &   &Desde um canto, verde   /_     (   (Desde um canto, laranja   /`     *   *Desde um canto, vermelho    /a     *   *Desde um canto, turquesa    /b     (   (Desde um canto, violeta   /c          Desde o centro    /d     &   &Desde o centro, azul    /e     &   &Desde o centro, verde   /f     (   (Desde o centro, laranja   /g     *   *Desde o centro, vermelho    /h     *   *Desde o centro, turquesa    /i     (   (Desde o centro, violeta   /j        Horizontal    /k          Azul horizontal   /l     "   "Verde horizontal    /m     $   $Laranja horizontal    /n     $   $Vermelho horizontal   /o     $   $Turquesa horizontal   /p     $   $Violeta horizontal    /q        Radial    /r        Azul radial   /s        Verde radial    /t          Laranja radial    /u          Vermelho radial   /v          Turquesa radial   /w          Violeta radial    /x        Vertical    /y        Azul vertical   /z          Verde vertical    /{     "   "Laranja vertical    /|     "   "Vermelho vertical   /}     "   "Turquesa vertical   /~     "   "Violeta vertical    /     $   $Gradiente cinzento    /�     "   "Gradiente amarelo   /�     "   "Gradiente laranja   /�     $   $Gradiente vermelho    /�          Gradiente rosa    /�        Céu    /�          Gradiente ciano   /�          Gradiente azul    /�     $   $Gradiente púrpura    /�        Noite   /�          Gradiente verde   /�        Tango verde   /�     "   "Tango verde suave   /�          Tango púrpura    /�          Tango vermelho    /�        Tango azul    /�        Tango amarelo   /�        Tango laranja   /�          Tango cinzento    /�        Barro   /�        Verde oliva   /�        Prata   /�        Raios solares   /�        Brownie   /�        Ocaso   /�        Verde escuro    /�          Laranja escuro    /�        Azul escuro   /�          Névoa púrpura   1�        Arrow concave   1�        Square 45   1�        Small Arrow   1�          Dimension Lines   1�        Double Arrow    1�     $   $Rounded short Arrow   1�          Symmetric Arrow   1�        Line Arrow    1�     $   $Rounded large Arrow   1�        Circle    1�        Square    1�        Arrow   1�     "   "Short line Arrow    1�     "   "Triangle unfilled   1�     "   "Diamond unfilled    1�        Diamond   1�          Circle unfilled   1�     $   $Square 45 unfilled    1�          Square unfilled   1�     &   &Half Circle unfilled    1�        Arrowhead   1�        Seta côncava   1�        Quadrado 45   1�        Seta pequena    1�     $   $Linhas de dimensão   1�        Seta dupla    1�     (   (Seta curta arredondada    1�          Seta simétrica   1�        Seta de linha   1�     (   (Seta longa arredondada    2         Círculo    2        Quadrado    2        Seta    2        Seta curta    2     ,   ,Triângulo não preenchido    2     (   (Losango não preenchido   2        Losango   2     *   *Círculo não preenchido    2     ,   ,Quadrado 45 não preenchido   2	     *   *Quadrado não preenchido    2
     .   .Meio círculo não preenchido   2        Ponta de seta   :h        Cor da letra    :p        Procurar    :q          Localizar tudo    :r        Substituir    :s          Substituir tudo   :t     &   &Estilo de caracteres    :u     &   &Estilo de parágrafo    :v     $   $Estilos de moldura    :w     "   "Estilo de página   :z        Fórmula    :{        Valor   :|        Nota    :        StarWriter    :�        StarCalc    :�        StarDraw    :�        StarBase    :�        Nenhum    :�        Sólido   :�        Horizontal    :�        Vertical    :�        Grelha    :�        Losango   :�     $   $Diagonal para cima    :�     $   $Diagonal para baixo   :�        25%   :�        50%   :�        75%   :�        Imagem    <      &   &Orientação padrão    <     $   $De cima para baixo    <     $   $De baixo para cima    <        Empilhado   <!        Tabela    <"        Sem tabela    <#     $   $Espaçamento ativo    <$     &   &Espaçamento inativo    <%     2   2Manter intervalo de espaçamento    <&     D   DPermitido espaçamento menor do que o especificado    <F     "   "Margem esquerda:    <G     "   "Margem superior:    <H     "   "Margem direita:     <I     "   "Margem inferior:    <X     *   *Descrição da página:     <Y        Maiúsculas   <Z        Minúsculas   <[     $   $Maiúsculas romanas   <\     $   $Minúsculas romanas   <]        Árabe    <^        Nenhum    <_        Horizontal    <`        Vertical    <a        Esquerda    <b        Direita   <c        Todos   <d        Invertido   <o        Autor:    <p        Data:     <q        Texto:    <r          Cor de fundo:     <s        Cor padrão:    <u     &   &Fundo dos caracteres    C          Paleta de cores   C        Trocar    FQ     R   RO nome "%1" não é válido em XML. Introduza um nome diferente.    FR     X   XO prefixo "%1" não é válido em XML. Introduza um prefixo diferente.    FS     @   @O nome "%1" já existe. Introduza um novo nome.   FT     ,   ,O envio tem de ter um nome.   FU     �   �Se eliminar a associação "$BINDINGNAME", todos os controlos associados serão afetados.

Tem a certeza de que pretende eliminar esta associação?    FV     �   �Se eliminar o envio "$SUBMISSIONNAME", todos os controlos associados serão afetados.

Tem a certeza de que pretende eliminar este envio?   FW     T   TTem a certeza de que pretende eliminar o atributo "$ATTRIBUTENAME"?   FX     �   �Se eliminar o elemento "$ELEMENTNAME", todos os controlos associados serão afetados.
Tem a certeza de que pretende eliminar este elemento?   FY     �   �Se eliminar a instância "$INSTANCENAME", todos os controlos associados serão afetados.
Tem a certeza de que pretende eliminar esta instância?    F[        Formulário   F\        Registo   F]        de    F^     (   (Definir propriedade "#"   F_     &   &Inserir no contentor    F`        Eliminar #    Fk     $   $Eliminar # objetos    Fl     4   4Substituir um elemento do contentor   Fn     $   $Eliminar estrutura    Fo     $   $Substituir controlo   Fp     &   &Barra de navegação    Ft        Formulário   Fu     "   "Adicionar campo:    Fv     ,   ,Nenhum controlo selecionado   Fw          Propriedades:     Fx     .   .Propriedades de formulários    Fy     *   *Navegador de formulários   Fz        Formulários    F{     6   6Erro ao gravar dados na base de dados   F|     4   4Você pretende eliminar 1 registo.    F}     Z   ZSe clicar em Sim, não será possível anular esta operação.
Continuar?   F~     $   $Elemento de moldura   F        Navegação   F�        Col   F�        Data    F�        Hora    F�     &   &Barra de navegação    F�     "   "Botão de ação    F�     "   "Botão de opção   F�     (   (Caixa de verificação    F�     "   "Campo de etiqueta   F�          Caixa de grupo    F�          Caixa de texto    F�          Caixa de lista    F�     &   &Caixa de combinação   F�     "   "Botão de imagem    F�     $   $Controle de imagem    F�     &   &Seleção de ficheiro   F�        Campo de data   F�        Campo de hora   F�          Campo numérico   F�     "   "Campo monetário    F�        Campo padrão   F�     $   $Controlo de tabela    F�     $   $Seleção múltipla   F�     .   .Serão eliminados # registos.   F�        Controlo    F�        (Data)    F�         (Hora)   F�     T   TNão existem controlos relacionados com dados no formulário atual!   F�     &   &Navegador de filtros    F�        Filtrar por   F�        Ou    F�          Campo formatado   F�     .   .Erro de sintaxe na expressão   F�     �   �Se eliminar o modelo "$MODELNAME", todos os controlos associados serão afetados.
Tem a certeza de que pretende eliminar este modelo?   F�     �   �No formulário atual, não existem controlos válidos que possam ser utilizados para a visualização da tabela.    F�     $   $<Campo automático>   F�     2   2Erro de sintaxe na expressão SQL   F�     >   >O valor #1 não pode ser utilizado com LIKE.    F�     6   6Não pode utilizar LIKE neste campo.    F�     H   HNão pode comparar este campo com o critério indicado.   F�     D   DEste campo não pode ser comparado com um inteiro.    F�     z   zO valor indicado não é uma data válida. Introduza uma data num formato válido, por exemplo DD-MM-AA.    F�     Z   ZEste campo não pode ser comparado com um número de vírgula flutuante.    F�     H   HA base de dados não contém uma tabela com o nome "#".   F�     >   >A coluna "#1" é desconhecida na tabela "#2".   F�     &   &Barra de deslocação   F�          Botão rotativo   F�          Controlo oculto   F�        Elemento    F�        Atributo    F�        Associação    F�     ,   ,Expressão de associação    F�        Post    F�        Put   F�        Get   F�        Nenhum    F�        Instância    F�        Documento   F�     $   $Navegador de dados    F�        Envio:    F�        ID:     F�        Ação:     F�        Método:    F�        Referência:    F�          Associação:     F�        Substituir:     F�     $   $Adicionar elemento    F�          Editar elemento   F�     "   "Eliminar elemento   F�     $   $Adicionar atributo    F�          Editar atributo   F�     "   "Eliminar atributo   F�     (   (Adicionar associação    F�     $   $Editar associação   F�     &   &Eliminar associação   F�          Adicionar envio   F�        Editar envio    F�          Eliminar envio    F�     Z   ZA base de dados não contém uma tabela nem uma consulta com o nome "#".    F�     P   PA base de dados já contém uma tabela ou vista com o nome "#".   F�     J   JA base de dados já contém uma consulta com o nome "#".    F�     "   " (apenas leitura)   F�     4   4O ficheiro já existe. Substituir?    F�     &   &Etiqueta de #object#    H�     ,   ,Erro ao criar o formulário   H�     >   >Esta entrada já existe.
Escolha outro nome.    H�     D   DRequer uma entrada no campo "#". Indique um valor.    H�     p   pEscolha uma entrada na lista ou introduza um texto correspondente a um dos elementos na lista.    (G  y   �   �   Milímetro     Centímetro    Metro    Quilómetro    Polegada     Pé    	Milhas     
Pica     Ponto    Carácter    Linha      (H  y  
   
    NEuropa Ocidental (Windows-1252/WinLatin 1)     Europa Ocidental (Apple Macintosh)     Europa Ocidental (DOS/OS2-850/Internacional)     Europa de Leste (DOS/OS2-437/US)     Europa Ocidental (DOS/OS2-860/Português)    Europa Ocidental (DOS/OS2-861/Islandês)     Europa Ocidental (DOS/OS2-863/Francês (Can.))     Europa Ocidental (DOS/OS2-865/Nórdico)    Europa Ocidental (ASCII/US)    Europa Ocidental (ISO-8859-1)    Europa de Leste (ISO-8859-2)     Latim 3 (ISO-8859-3)     Báltico (ISO-8859-4)    Cirílico (ISO-8859-5)     Árabe (ISO-8859-6)    Grego (ISO-8859-7)     Hebraico (ISO-8859-8)    Turco (ISO-8859-9)     Europa Ocidental (ISO-8859-14)     Europa Ocidental (ISO-8859-15/EURO)    Grego (DOS/OS2-737)    Báltico (DOS/OS2-775)     Europa de Leste (DOS/OS2-852)    Cirílico (DOS/OS2-855)    Turco (DOS/OS2-857)    Hebraico (DOS/OS2-862)     Árabe (DOS/OS2-864)     Cirílico (DOS/OS2-866/Russo)    Grego (DOS/OS2-869/Moderno)    Europa de Leste (Windows-1250/WinLatin 2)    !Cirílico (Windows-1251)     "Grego (Windows-1253)     #Turco (Windows-1254)     $Hebraico (Windows-1255)    %Árabe (Windows-1256)    &Báltico (Windows-1257)    'Vietnamita (Windows-1258)    (Europa de Leste (Apple Macintosh)    *Europa de Leste (Apple Macintosh/Croata)     +Cirílico (Apple Macintosh)    ,Grego (Apple Macintosh)    /Europa Ocidental (Apple Macintosh/Islandês)     3Europa Ocidental (Apple Macintosh/Romeno)    4Turco (Apple Macintosh)    6Cirílico (Apple Macintosh/Ucraniano)    7Mandarim simplificado (Apple Macintosh)    8Mandarim tradicional (Apple Macintosh)     9Japonês (Apple Macintosh)     :Coreano (Apple Macintosh)    ;Japonês (Windows-932)     <Mandarim simplificado (Windows-936)    =Coreano (Windows-949)    >Mandarim tradicional (Windows-950)     ?Japonês (Shift-JIS)     @Mandarim simplificado (GB-2312)    AMandarim simplificado (GB-18030)     UMandarim tradicional (GBT-12345)     BMandarim simplificado (GBK/GB-2312-80)     CMandarim tradicional (Big5)    DMandarim tradicional (BIG5-HKSCS)    VJaponês (EUC-JP)    EMandarim simplificado (EUC-CN)     FMandarim tradicional (EUC-TW)    GJaponês (ISO-2022-JP)     HMandarim simplificado (ISO-2022-CN)    ICirílico (KOI8-R)     JUnicode (UTF-7)    KUnicode (UTF-8)    LEuropa de Leste (ISO-8859-10)    MEuropa de Leste (ISO-8859-13)    NCoreano (EUC-KR)     OCoreano (ISO-2022-KR)    PCoreano (Windows-Johab-1361)     TUnicode (UTF-16)    ��Tailandês (ISO-8859-11/TIS-620)     WTailandês (Windows-874)      Cirílico (KOI8-U)     XCirílico (PT154)    ]  <v  y  H  H   ;Escala    'Pintar    'Marcas de tabulação   'Caracteres    'Tipo de letra   'Posição do tipo de letra    'Espessura da letra    'Sombreado   'Palavras individuais    'Contorno    'Rasurado    'Sublinhado    'Altura da letra   'Tamanho relativo das letras   ' Cor da letra    '!Kerning   '"Efeitos   '#Idioma    '$Posição   '%Intermitência de caracteres    'SCor dos caracteres    *}Sobrelinha    -0Parágrafo    '*Alinhamento   '+Espaçamento entre linhas   '1Quebra de página   '5Hifenização   '6Não dividir parágrafo   '7Linhas órfãs    '8Linhas isoladas   '9Espaçamento entre parágrafos    ':Avanço de parágrafo   ';Avanço   '@Espaçamento    'APágina   'BEstilo de página   'QManter com parágrafo seguinte    'RIntermitente    (�Registo de conformidade   (�Fundo dos caracteres    )_Letras asiáticas   *�Tamanho das letras asiáticas   *�Idioma das letras asiáticas    *�Posição das letras asiáticas   *�Espessura das letras asiáticas   *�CTL   *�Tamanho dos scripts complexos   *�Idioma dos scripts complexos    *�Posição dos scripts complexos   *�Espessura dos scripts complexos   *�Linha dupla   *�Marca de ênfase    *�Espaçamento do texto   *�Pontuação isolada   *�Caracteres proibidos    *�Rotação   *�Escala de caracteres    *�Relevo    *�Alinhamento vertical do texto   *�  �     *   *      res/grafikei.png    �     *   *      res/grafikde.png    �     ,   ,      svx/res/markers.png   �     4   4      svx/res/pageshadow35x35.png   �     (   (      res/oleobj.png    �     0   0      svx/res/cropmarkers.png   '�     .   .      svx/res/rectbtns.png    '�     ,   ,      svx/res/objects.png   '�     (   (      	svx/res/ole.png   '�     ,   ,      
svx/res/graphic.png   'S  #   f   f            8   8      svx/res/slidezoombutton_10.png              ��  ��      'T  #   b   b            4   4      svx/res/slidezoomout_10.png             ��  ��      'U  #   b   b            4   4      svx/res/slidezoomin_10.png              ��  ��      'e  #   V   V            (   (      res/sc10223.png             ��  ��      'f  #   V   V            (   (      res/sc10224.png             ��  ��      'g  #   `   `            2   2      svx/res/signet_11x16.png              ��  ��      'i  #   `   `            2   2      svx/res/caution_11x16.png             ��  ��      'k  #   d   d            6   6      svx/res/notcertificate_16.png             ��  ��      '�  #   Z   Z            ,   ,      svx/res/lighton.png             ��  ��      '�  #   X   X            *   *      svx/res/light.png             ��  ��      '�  #   \   \            .   .      svx/res/colordlg.png              ��  ��      '�  #   H   H            4   4      svx/res/selection_10x22.png   (  #   \   \            .   .      svx/res/notcheck.png              ��  ��      (  #   \   \            .   .      svx/res/lngcheck.png              ��  ��      (�  #   V   V            (   (      res/sc10865.png             ��  ��      (�  #   V   V            (   (      res/sc10866.png             ��  ��      (�  #   V   V            (   (      res/sc10867.png             ��  ��      (�  #   V   V            (   (      res/sc10863.png             ��  ��      (�  #   V   V            (   (      res/sc10864.png             ��  ��      (�  #   V   V            (   (      res/sc10868.png             ��  ��      (�  #   V   V            (   (      res/sc10869.png             ��  ��      (�  #   V   V            (   (       res/reload.png              ��  ��      (�  #   Z   Z            ,   ,      !svx/res/reloads.png             ��  ��      (�  #   h   h            :   :      "svx/res/extrusioninfinity_16.png              ��  ��      (�  #   d   d            6   6      #svx/res/extrusion0inch_16.png             ��  ��      (�  #   f   f            8   8      $svx/res/extrusion05inch_16.png              ��  ��      (�  #   d   d            6   6      %svx/res/extrusion1inch_16.png             ��  ��      (�  #   d   d            6   6      &svx/res/extrusion2inch_16.png             ��  ��      (�  #   d   d            6   6      'svx/res/extrusion4inch_16.png             ��  ��      )$  #   `   `            2   2      (svx/res/wireframe_16.png              ��  ��      )%  #   \   \            .   .      )svx/res/matte_16.png              ��  ��      )&  #   ^   ^            0   0      *svx/res/plastic_16.png              ��  ��      )'  #   \   \            .   .      +svx/res/metal_16.png              ��  ��      +�  #   f   f            8   8      ,svx/res/doc_modified_yes_14.png             ��  ��      +�  #   f   f            8   8      -svx/res/doc_modified_no_14.png              ��  ��      +�  #   h   h            :   :      .svx/res/doc_modified_feedback.png             ��  ��      +�  #   f   f            8   8      /svx/res/zoom_page_statusbar.png             ��  ��      +�  #   J   J            6   6      0svx/res/symphony/spacing3.png   +�  #   P   P            <   <      1svx/res/symphony/Indent_Hanging.png   +�  #   H   H            4   4      2svx/res/symphony/blank.png    +�  #   H   H            4   4      3svx/res/symphony/width1.png   ,   #   H   H            4   4      4svx/res/symphony/width2.png   ,  #   H   H            4   4      5svx/res/symphony/width3.png   ,  #   H   H            4   4      6svx/res/symphony/width4.png   ,  #   H   H            4   4      7svx/res/symphony/width5.png   ,  #   H   H            4   4      8svx/res/symphony/width6.png   ,  #   H   H            4   4      9svx/res/symphony/width7.png   ,  #   H   H            4   4      :svx/res/symphony/width8.png   ,  #   H   H            4   4      ;svx/res/symphony/axial.png    ,  #   L   L            8   8      <svx/res/symphony/ellipsoid.png    ,	  #   L   L            8   8      =svx/res/symphony/Quadratic.png    ,
  #   H   H            4   4      >svx/res/symphony/radial.png   ,  #   H   H            4   4      ?svx/res/symphony/Square.png   ,  #   H   H            4   4      @svx/res/symphony/linear.png   ,  #   N   N            :   :      Asvx/res/symphony/rotate_left.png    ,  #   N   N            :   :      Bsvx/res/symphony/rotate_right.png   ,  #   >   >            *   *      Csvx/res/nu01.png    ,  #   >   >            *   *      Dsvx/res/nu04.png    ,  #   >   >            *   *      Esvx/res/nu02.png    ,,  #   h   h            :   :      Fsvx/res/directionnorthwest_22.png             ��  ��      ,-  #   d   d            6   6      Gsvx/res/directionnorth_22.png             ��  ��      ,.  #   h   h            :   :      Hsvx/res/directionnortheast_22.png             ��  ��      ,/  #   d   d            6   6      Isvx/res/directionwest_22.png              ��  ��      ,0  #   h   h            :   :      Jsvx/res/directionstraight_22.png              ��  ��      ,1  #   d   d            6   6      Ksvx/res/directioneast_22.png              ��  ��      ,2  #   h   h            :   :      Lsvx/res/directionsouthwest_22.png             ��  ��      ,3  #   d   d            6   6      Msvx/res/directionsouth_22.png             ��  ��      ,4  #   h   h            :   :      Nsvx/res/directionsoutheast_22.png             ��  ��      ,6  #   b   b            4   4      Osvx/res/perspective_16.png              ��  ��      ,7  #   ^   ^            0   0      Psvx/res/parallel_16.png             ��  ��      ,G  #   j   j            <   <      Qsvx/res/lightofffromtopleft_22.png              ��  ��      ,H  #   f   f            8   8      Rsvx/res/lightofffromtop_22.png              ��  ��      ,I  #   j   j            <   <      Ssvx/res/lightofffromtopright_22.png             ��  ��      ,J  #   f   f            8   8      Tsvx/res/lightofffromleft_22.png             ��  ��      ,L  #   h   h            :   :      Usvx/res/lightofffromright_22.png              ��  ��      ,M  #   l   l            >   >      Vsvx/res/lightofffrombottomleft_22.png             ��  ��      ,N  #   h   h            :   :      Wsvx/res/lightofffrombottom_22.png             ��  ��      ,O  #   n   n            @   @      Xsvx/res/lightofffrombottomright_22.png              ��  ��      ,Q  #   h   h            :   :      Ysvx/res/lightonfromtopleft_22.png             ��  ��      ,R  #   d   d            6   6      Zsvx/res/lightonfromtop_22.png             ��  ��      ,S  #   j   j            <   <      [svx/res/lightonfromtopright_22.png              ��  ��      ,T  #   f   f            8   8      \svx/res/lightonfromleft_22.png              ��  ��      ,V  #   f   f            8   8      ]svx/res/lightonfromright_22.png             ��  ��      ,W  #   l   l            >   >      ^svx/res/lightonfrombottomleft_22.png              ��  ��      ,X  #   h   h            :   :      _svx/res/lightonfrombottom_22.png              ��  ��      ,Y  #   l   l            >   >      `svx/res/lightonfrombottomright_22.png             ��  ��      ,[  #   f   f            8   8      asvx/res/lightfromtopleft_22.png             ��  ��      ,\  #   b   b            4   4      bsvx/res/lightfromtop_22.png             ��  ��      ,]  #   h   h            :   :      csvx/res/lightfromtopright_22.png              ��  ��      ,^  #   d   d            6   6      dsvx/res/lightfromleft_22.png              ��  ��      ,_  #   d   d            6   6      esvx/res/lightfromfront_22.png             ��  ��      ,`  #   d   d            6   6      fsvx/res/lightfromright_22.png             ��  ��      ,a  #   j   j            <   <      gsvx/res/lightfrombottomleft_22.png              ��  ��      ,b  #   f   f            8   8      hsvx/res/lightfrombottom_22.png              ��  ��      ,c  #   j   j            <   <      isvx/res/lightfrombottomright_22.png             ��  ��      ,e  #   `   `            2   2      jsvx/res/brightlit_16.png              ��  ��      ,f  #   `   `            2   2      ksvx/res/normallit_16.png              ��  ��      ,g  #   \   \            .   .      lsvx/res/dimlit_16.png             ��  ��      ,m  #   h   h            :   :      msvx/res/fontworkalignleft_16.png              ��  ��      ,n  #   l   l            >   >      nsvx/res/fontworkaligncentered_16.png              ��  ��      ,o  #   h   h            :   :      osvx/res/fontworkalignright_16.png             ��  ��      ,p  #   l   l            >   >      psvx/res/fontworkalignjustified_16.png             ��  ��      ,q  #   j   j            <   <      qsvx/res/fontworkalignstretch_16.png             ��  ��      ,r  #   >   >            *   *      rsvx/res/fw018.png   ,s  #   >   >            *   *      ssvx/res/fw019.png   ,t  #   >   >            *   *      tsvx/res/fw016.png   ,u  #   >   >            *   *      usvx/res/fw017.png      $  �  �   id              ��  ��       svx/res/id01.png     svx/res/id02.png     svx/res/id03.png     svx/res/id04.png     svx/res/id05.png     svx/res/id06.png     svx/res/id07.png     svx/res/id08.png     svx/res/id030.png    svx/res/id031.png    svx/res/id032.png     svx/res/id033.png    !svx/res/id040.png    (svx/res/id041.png    )svx/res/id016.png    svx/res/id018.png    svx/res/id019.png       'Q  $  @  @   fr              ��  ��       svx/res/fr01.png     svx/res/fr02.png     svx/res/fr03.png     svx/res/fr04.png     svx/res/fr05.png     svx/res/fr06.png     svx/res/fr07.png     svx/res/fr08.png     svx/res/fr09.png     	svx/res/fr010.png    
svx/res/fr011.png    svx/res/fr012.png       'R  $   �   �   da              ��  ��       res/da01.png     res/da02.png     res/da03.png     res/da04.png     res/da05.png     res/da06.png        FP  $  �  �   sx              �   �        res/sx10594.png   )bres/sx10595.png   )cres/sx10596.png   )dres/sx10597.png   )eres/sx10598.png   )fres/sx10599.png   )gres/sx10600.png   )hres/sx10601.png   )ires/sx10607.png   )ores/sx10144.png   '�res/sx10593.png   )ares/sx18013.png   F]res/sx18002.png   FRres/sx18003.png   FSres/sx10604.png   )lres/sx10605.png   )mres/sx10704.png   )�res/sx10705.png   )�res/sx10706.png   )�res/sx10707.png   )�res/sx10708.png   )�res/sx18022.png   Ffres/sx10710.png   )�res/sx10603.png   )kres/sx10715.png   )�res/sx10728.png   )�res/sx10757.png   *res/sx18027.png   Fkres/sx10768.png   *res/sx10769.png   *   FQ  $   �   �   tb              ��  ��       res/tb01.png     res/tb02.png     res/tb03.png     res/tb04.png     res/tb05.png        (K  D     (   8            ?   �     D   h   h  @�    �  SVX_HID_PPROPERTYPANEL_TEXT_UNDERLINE_VS                 9   xUnderline      F   \   \  @�       SVX_HID_UNDERLINE_BTN          }      9   ~Mais opções...    
  #   H   H            4   4      vsvx/res/symphony/line1.png       #   H   H            4   4      wsvx/res/symphony/line2.png       #   H   H            4   4      xsvx/res/symphony/line3.png       #   H   H            4   4      ysvx/res/symphony/line4.png       #   H   H            4   4      zsvx/res/symphony/line5.png       #   H   H            4   4      {svx/res/symphony/line6.png       #   H   H            4   4      |svx/res/symphony/line7.png       #   H   H            4   4      }svx/res/symphony/line8.png       #   H   H            4   4      ~svx/res/symphony/line9.png       #   H   H            4   4      svx/res/symphony/line10.png    (  #   P   P            <   <      �svx/res/symphony/selected-line1.png    )  #   P   P            <   <      �svx/res/symphony/selected-line2.png    *  #   P   P            <   <      �svx/res/symphony/selected-line3.png    +  #   P   P            <   <      �svx/res/symphony/selected-line4.png    ,  #   P   P            <   <      �svx/res/symphony/selected-line5.png    -  #   P   P            <   <      �svx/res/symphony/selected-line6.png    .  #   P   P            <   <      �svx/res/symphony/selected-line7.png    /  #   P   P            <   <      �svx/res/symphony/selected-line8.png    0  #   P   P            <   <      �svx/res/symphony/selected-line9.png    1  #   R   R            >   >      �svx/res/symphony/selected-line10.png     2        (Sem)            Simples            Duplo            Negrito            Pontilhado          &   &Pontilhado (Negrito)             Traço             Travessão             Ponto traço          $   $Ponto ponto traço             Onda    (M  D  :   (   8            V   �     D   n   n  @�    �  SVX_HID_PPROPERTYPANEL_TEXT_SPACING_VS                 P   ZCharacter Spacing      W   D   D   �      @          _      H   Personalizado:       W   V   V   �      @          n      >   Espaçamento entre ~caracteres:        U   |   |  @?     SVX_HID_SPACING_CB_KERN          x      >   P��   Padrão     Expandido     Condensado           W   B   B   �      @          �      >   Mudar p~or:      	  d   h   h  @?     `SVX_HID_SPACING_MB_KERN          �      >          '             
     #   P   P            <   <      �svx/res/symphony/spacing_normal.png       #   T   T            @   @      �svx/res/symphony/spacing_very tight.png    !  #   P   P            <   <      �svx/res/symphony/spacing_tight.png     "  #   P   P            <   <      �svx/res/symphony/spacing_loose.png     #  #   T   T            @   @      �svx/res/symphony/spacing_very loose.png    3  #   R   R            >   >      �svx/res/symphony/spacing_normal_s.png    4  #   V   V            B   B      �svx/res/symphony/spacing_very tight_s.png    5  #   R   R            >   >      �svx/res/symphony/spacing_tight_s.png     6  #   R   R            >   >      �svx/res/symphony/spacing_loose_s.png     7  #   V   V            B   B      �svx/res/symphony/spacing_very loose_s.png    $  #   T   T            @   @      �svx/res/symphony/last_custom_common.png    %  #   Z   Z            F   F      �svx/res/symphony/last_custom_common_grey.png     =          Muito apertado     >        Apertado     ?        Normal     @        Largo    A        Muito largo    B     ,   ,Último valor personalizado    C     .   .Espaçamento - condensar 3 pt    D     0   0Espaçamento - condensar 1,5 pt    E     &   &Espaçamento - normal    F     .   . Espaçamento - expandir 3 pt    G     .   . Espaçamento - expandir 6 pt    H     ,   , Espaçamento - condensar:     I     ,   , Espaçamento - expandir:      J        pt    (P  D                   	  W   4   4   �              -   	Centro ~X:     
  d        B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_CENTER_X       -   Especifique a percentagem de deslocamento horizontal a partir do centro para o estilo de sombra do gradiente. 50% é o centro horizontal.         d             d        W   4   4   �              4   	Centro ~Y:       d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_CENTER_Y       -   Especifique a percentagem de deslocamento vertical a partir do centro para o estilo de sombra do gradiente. 50% é o centro vertical.         d             d        W   2   2   �              d   	Â~ngulo:      d   �   �  B8     ` SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_ANGLE        -   Especifique o ângulo de rotação do estilo de sombreamento do gradiente.       ����  '      graus            W   8   8   �              -   	Valor ~inicial:      d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_SVALUE       -   Indique o valor de transparência para o inicio do gradiente, sendo 0% total opacidade e 100% total transparência.         d             d        W   6   6   �              4   	Valor ~final:      d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_EVALUE       -   Indique o valor de transparência para o final do gradiente, sendo 0% total opacidade e 100% total transparência.          d             d        W   4   4   �              d   	Con~torno:       d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_BORDER       -   Especifique o valor de contorno da transparência do gradiente.         d             d        q   �   �  @�      @SVX_HID_PPROPERTYPANEL_AREA_BTN_LEFT_SECOND Rotate Left        �         p           Rotate Left      q   �   �  @�      @SVX_HID_PPROPERTYPANEL_AREA_BTN_RIGHT_FIRST Rotate Right         �         p   "   "   Rotate Right          0   0Rodar 45 graus para a esquerda.         0   0Rodar 45 graus para a direita.    (T  D  �   (   8            V   �     D   `   `  @�    �  SVX_HID_PPROPERTYPANEL_LINE_VS_WIDTH                 P   lWidth      W   D   D   �                 q      P   Personalizado:       W   H   H   �                 �      @   L~argura da linha:       d   �   �  B?     aSVX_HID_PPROPERTYPANEL_LINE_MTR_WIDTH          �      (   Especifique a largura da linha.      6  �                �   
     #   T   T            @   @      �svx/res/symphony/last_custom_common.png      #   Z   Z            F   F      �svx/res/symphony/last_custom_common_grey.png          ,   ,Último valor personalizado    	        pt    (W  D   �                    W   �   �   �                       d   As propriedades da tarefa em execução não estão disponíveis para a seleção atual   '�  T   D   D  @8    �  SVX_HID_STYLE_LISTBOX       <   V        'b    �  �               D   D   J   Página inteira SVX_HID_MNU_ZOOM_WHOLE_PAGE          H   H   J   Largura de página  SVX_HID_MNU_ZOOM_PAGE_WIDTH          >   >   J   Vista ideal SVX_HID_MNU_ZOOM_OPTIMAL           0   0   J   50% SVX_HID_MNU_ZOOM_50          0   0   J   75% SVX_HID_MNU_ZOOM_75          4   4   J   100%  SVX_HID_MNU_ZOOM_100           4   4   J   150%  SVX_HID_MNU_ZOOM_150           0   0   H200%  SVX_HID_MNU_ZOOM_200    'c    �  �               2   2   HMédia  SVX_HID_MNU_FUNC_AVG           <   <   J   Contar.Val  SVX_HID_MNU_FUNC_COUNT2          8   8   J   Contar  SVX_HID_MNU_FUNC_COUNT           6   6   J   Máximo SVX_HID_MNU_FUNC_MAX           6   6   J   Mínimo SVX_HID_MNU_FUNC_MIN           4   4   J   	Soma  SVX_HID_MNU_FUNC_SUM           L   L   J   Contar seleção  SVX_HID_MNU_FUNC_SELECTION_COUNT           6   6   J   Nenhum  SVX_HID_MNU_FUNC_NONE   'd     X   X               @   @   HAssinaturas digitais... SVX_HID_XMLSEC_CALL   'l    �  �               $   $      Milímetro           (   (         Centímetro          "   "         Metro          (   (         Quilómetro          &   &         Polegada                      	   Pé          $   $      
   Milhas           "   "         Ponto          "   "         Pica           &   &         Carácter          "   "         Linha   '�     �   �               B   B   
  '�Atualizar para corresponder à seleção           *   *   
  '�Editar estilo...    '�     �   �               *   *      Seleção padrão          4   4         Seleção por extensão          4   4         Seleção por adição           0   0         Seleção em bloco    (�    �  �               $   $   Descrição...           "   "   
   
~Macro...                
   	Ativo                          �   �  
   Organizar          �   �               .   .   
   Trazer para a frente           4   4   
   Tra~zer para primeiro plano          4   4   
   En~viar para segundo plano           ,   ,   
   Enviar para trá~s                           *   *   
   Selecion~ar tudo           "   "   
   E~liminar   FP                     �   �  J  )�~Novo SVX_HID_FM_NEW           �   �               8   8   J  )�Formulário SVX_HID_FM_NEW_FORM          >   >   J  )pControlo oculto SVX_HID_FM_NEW_HIDDEN          @   @   J  )�Substituir por: .uno:ChangeControlType           *   *  
  NCor~tar .uno:Cut           *   *  
  O~Copiar .uno:Copy          ,   ,  
  PCo~lar  .uno:Paste           4   4   J  )�E~liminar SVX_HID_FM_DELETE          @   @   J  )wOrdem de tabulação... .uno:TabDialog           >   >   J  )qMuda~r nome SVX_HID_FM_RENAME_OBJECT           @   @   J  )�Propr~iedades .uno:ShowPropertyBrowser           B   B   J  )�Abrir no modo de design .uno:OpenReadOnly          L   L   J  *Foco automático de controlo  .uno:AutoControlFocus   FQ     �   �               >   >   J  )rEliminar linhas SVX_HID_FM_DELETEROWS          6   6   J  )�Guardar registo .uno:RecSave           @   @   J  )�Anular: entrada de dados  .uno:RecUndo    FR    �  �              �  �  J  )sInserir ~coluna SVX_HID_FM_INSERTCOL          �  �               2   2   J  )gCaixa de texto  .uno:Edit          >   >   J  )dCaixa de verificação  .uno:CheckBox          <   <   J  )iCaixa de combinação .uno:ComboBox          6   6   J  )hCaixa de lista  .uno:ListBox           6   6   J  )�Campo de data .uno:DateField           6   6   J  )�Campo de hora .uno:TimeField           :   :   J  )�Campo numérico .uno:NumericField          >   >   J  )�Campo monetário  .uno:CurrencyField           8   8   J  )�Campo padrão .uno:PatternField          <   <   J  )�Campo formatado .uno:FormattedField          N   N  J  *Campo de data e hora  SVX_HID_CONTROLS_DATE_N_TIME            >   >   J  )nSubstitui~r por SVX_HID_FM_CHANGECOL           >   >   J  )tEliminar coluna SVX_HID_FM_DELETECOL           <   <   J  *~Ocultar coluna SVX_HID_FM_HIDECOL           �   �  J  *Mostrar ~colunas  SVX_HID_FM_SHOWCOLS          �   �               <   <   J  *~Mais...  SVX_HID_FM_SHOWCOLS_MORE                           8   8   J  *~Todas  SVX_HID_FM_SHOWALLCOLS           <   <   J  )�Coluna... .uno:ShowPropertyBrowser    FS     B   B               *   *  
  O~Copiar .uno:Copy   FT    �  �               P   P  J  )�Caixa de ~texto .uno:ConvertToEdit  .uno:ConvertToEdit           L   L  J  )�~Botão .uno:ConvertToButton  .uno:ConvertToButton           R   R  J  )�Ca~mpo de rótulo .uno:ConvertToFixed .uno:ConvertToFixed          P   P  J  )�Caixa de g~rupo .uno:ConvertToGroup .uno:ConvertToGroup          P   P  J  )�Caixa de ~lista .uno:ConvertToList  .uno:ConvertToList           `   `  J  )�~Caixa de verificação .uno:ConvertToCheckBox  .uno:ConvertToCheckBox           T   T  J  )�B~otão de opção  .uno:ConvertToRadio .uno:ConvertToRadio          X   X  J  )�Cai~xa de combinação  .uno:ConvertToCombo .uno:ConvertToCombo          Z   Z  J  )�Botão de i~magem .uno:ConvertToImageBtn  .uno:ConvertToImageBtn           d   d  J  )�Seleção de ~ficheiro  .uno:ConvertToFileControl .uno:ConvertToFileControl          P   P  J  )�Campo de ~data  .uno:ConvertToDate  .uno:ConvertToDate           P   P  J  )�Campo d~e hora  .uno:ConvertToTime  .uno:ConvertToTime           V   V  J  )�Campo ~numérico  .uno:ConvertToNumeric .uno:ConvertToNumeric          Z   Z  J  )�Campo m~onetário .uno:ConvertToCurrency  .uno:ConvertToCurrency           T   T  J  )�Campo ~padrão  .uno:ConvertToPattern .uno:ConvertToPattern          d   d  J  )�Controle de ima~gem .uno:ConvertToImageControl  .uno:ConvertToImageControl           Z   Z  J  )�Campo fo~rmatado  .uno:ConvertToFormatted .uno:ConvertToFormatted          ^   ^  J  *Barra de deslocação .uno:ConvertToScrollBar .uno:ConvertToScrollBar          \   \  J  *Botão rotativo .uno:ConvertToSpinButton  .uno:ConvertToSpinButton           f   f  J  *Barra de navegação  .uno:ConvertToNavigationBar .uno:ConvertToNavigationBar   FU     �   �               4   4   J  )�E~liminar SVX_HID_FM_DELETE          0   0   J  *~Editar SVX_HID_FM_EDIT          <   <   J  *É ~Null  SVX_HID_FM_FILTER_IS_NULL          >   >   J  *Não é N~ull SVX_HID_FM_IS_NOT_NULL    FV                     8   8  
  'Tipo de letra .uno:CharFontName          0   0  
  'Tamanho .uno:FontHeight         4  4  J  FWEst~ilo SVX_HID_MENU_FM_TEXTATTRIBUTES_STYLE          �  �      
         *   *  
  'Negrito .uno:Bold          .   .  
  'Itálico  .uno:Italic          2   2  
  -0Sobrelinha  .uno:Overline          4   4  
  'Sublinhado  .uno:Underline           2   2  
  'Rasurado  .uno:Strikeout           0   0  
  'Sombreado .uno:Shadowed          4   4  
  'Contorno  .uno:OutlineFont                           8   8  
  (6S~obrescrito  .uno:SuperScript           4   4  
  (7Su~bscrito  .uno:SubScript          F  F  N  FX   ~Alinhamento  SVX_HID_MENU_FM_TEXTATTRIBUTES_ALIGNMENT           �   �               4   4    ',   Es~querda .uno:LeftPara          6   6    '-   Di~reita  .uno:RightPara           6   6    '.   ~Centrado .uno:CenterPara          :   :    '/   Justificado .uno:JustifyPara              J  FYEspaçamento entre ~linhas  SVX_HID_MENU_FM_TEXTATTRIBUTES_SPACING           �   �               4   4    '2   Simples .uno:SpacePara1          :   :    '3   1,5 linhas  .uno:SpacePara15           4   4    '4   ~Duplo  .uno:SpacePara2   FZ    �  �               H   H   J   
Adicionar item  SVX_HID_XFORMS_TOOLBOX_ITEM_ADD          T   T   J   Adicionar elemento  SVX_HID_XFORMS_TOOLBOX_ITEM_ADD_ELEMENT          V   V   J   Adicionar atributo  SVX_HID_XFORMS_TOOLBOX_ITEM_ADD_ATTRIBUTE          B   B   J   Editar  SVX_HID_XFORMS_TOOLBOX_ITEM_EDIT                           F   F   J   Eliminar  SVX_HID_XFORMS_TOOLBOX_ITEM_REMOVE     �          �     � �    �        (1  #�    (n  )�    (o  2�    *d  4    FP  7�    FQ  7�       :       :l      :�      :�      :�      :�      ;      ;0      ;L    	  ;h    
  ;�      ;�      ;�    
   ;�    
  ;�    
  <     
  <@    
  <b    
  <�    
  <�    
  <�    
  <�    
	  <�    

  =    
  =8    
  =X    
  =p    
  =�    
  =�    
  =�    
  =�    
  =�    
  >    
  >2    
  >L    
  >t    
  >�    
  >�    
  >�    
  ?    
  ?B    
  ?f    
  ?�    
  ?�    
  ?�    
   ?�    
!  @    
"  @    
#  @2    
$  @X    
%  @~    
&  @�    
'  @�    
(  @�    
)  @�    
*  A    
+  A2    
,  AV    
-  A|    
.  A�    
/  A�    
0  A�    
1  A�    
2  B    
3  B4    
4  BR    
5  Br    
6  B�    
7  B�    
8  B�    
9  B�    
:  C    
;  C$    
<  C:    
=  CZ    
>  Cp    
?  C�    
@  C�    
A  C�    
B  D    
C  DB    
D  Dd    
E  D�    
F  D�    
G  D�    
H  E
    
I  E4    
J  E^    
K  E�    
L  E�    
M  E�    
N  E�    
O  F    
P  F*    
Q  FB    
R  Fd    
S  F�    
T  F�    
U  F�    
V  G     
W  G*    
X  GH    
Y  Gf    
Z  G�    
[  G�    
\  G�    
]  G�    
^  H    
_  HF    
`  Hb    
a  H~    
b  H�    
c  H�    
d  H�    
e  I"    
f  IX    
g  I�    
h  I�    
i  I�    
j  I�    
k  I�    
l  J    
m  J>    
n  JV    
o  Jp    
p  J�    
q  J�    
r  J�    
s  K    
t  K4    
u  KZ    
v  Kl    
w  K�    
x  K�    
y  K�    
z  K�    
{  K�    
|  L    
}  LB    
~  L\    
  Lv    
�  L�    
�  L�    
�  L�    
�  L�    
�  M    
�  M,    
�  MF    
�  M^    
�  Mv    
�  M�    
�  M�    
�  N    
�  NB    
�  NX    
�  Np    
�  N�    
�  N�    
�  N�    
�  N�    
�  O    
�  O    
�  O4    
�  Oh    
�  O�    
�  O�    
�  O�    
�  O�    
�  P"    
�  PH    
�  Pb    
�  P�    
�  P�    
�  P�    
�  P�    
�  Q    
�  Q@    
�  Ql    
�  Q�    
�  Q�    
�  Q�    
�  Q�    
�  R*    
�  R`    
�  Rz    
�  R�    
�  R�    
�  S    
�  S2    
�  SP    
�  Sx    
�  S�    
�  S�    
�  S�    
�  T    
�  T@    
�  Th    
�  T�    
�  T�    
�  T�    
�  T�    
�  U    
�  UD    
�  Ul    
�  U�    
�  U�    
�  U�    
�  U�    
�  U�    
�  V    
�  V4    
�  Vh    
�  V�    
�  V�    
�  V�    
�  V�    
�  W    
�  W2    
�  WP    
�  W�    
�  W�    
�  W�    
�  W�    
�  X    
�  XD    
�  Xj    
�  X�    
�  X�    
�  X�    
�  Y    
�  YD    
�  Y^    
�  Y�    
�  Y�    
�  Y�    
�  Y�    
�  Z    
�  Z<    
�  Zh    
�  Z�    
�  Z�    
�  Z�    
�  Z�    
�  [    
�  [8    
�  [R    
�  [z    
�  [�    
�  [�    
�  [�    
�  \    
�  \8    
�  \z    
�  \�    
�  \�    
�  \�    
�  \�    
�  ]*    
�  ]J    
�  ]t    
�  ]�    
�  ]�    
�  ]�    
�  ^    
�  ^4    
�  ^P    
�  ^j    
�  ^�       ^�      ^�      _       _       _L      _v      _�      _�      `    	  `8    
  `X      `x      `�      `�      a      a8      aP      ah      a�      a�      a�      a�      a�      a�      b      b(      bB      b^      bx      b�      b�      b�       c    !  c.    "  cH    #  cp    $  c�    %  c�    &  c�    '  c�    (  d     )  d    *  d4    +  dR    ,  dl    -  d�    .  d�    /  d�    0  d�    1  e    2  e"    3  eD    4  ed    5  e�    6  e�    7  e�    8  e�    9  e�    :  f    ;  f8    <  fX    =  ft    >  f�    ?  f�    @  f�    A  f�    B  g    C  g0    D  gV    E  gl    F  g�    G  g�    H  g�    I  g�    J  h&    K  h@    R  hf    S  h�    T  h�    U  h�    V  h�    W  h�    X  i,    Y  iP    Z  it    [  i�    c  i�    d  i�    e  j    f  jJ    g  j|    h  j�    i  j�    j  k    k  k,    l  kX    m  k�    n  k�    o  k�    p  l    q  l0    r  lJ    s  l`    t  lv    u  l�    v  l�    w  l�    x  l�    y  l�    z  m    {  m(    |  m:    }  mL    ~  m^      mp    �  m�    �  m�    �  m�    �  m�    �  n    �  n8    �  nP    �  np    �  n�    �  n�    �  n�    �  n�    �  n�    �  o    �  o<    �  od    �  o�    �  o�    �  o�    �  o�    �  p    �  p.    �  pL    �  px    �  p�    �  p�    �  p�    �  q    �  q8    �  q`    �  q�    �  q�    �  q�    �  r     �  r$    �  rL    �  rr    �  r�    �  r�    �  r�    �  r�    �  s    �  sD    �  s�    �  s�    �  s�    �  t    �  tJ    �  tr    �  t�    �  t�    �  t�    �  u(    �  uV    �  u�    �  u�    �  u�    �  v    �  v<    �  vj    �  v�    �  v�    �  v�    �  w"    �  wF    �  wf    �  w�    �  w�    �  w�    �  x     �  x&    �  xF    �  xn    �  x�    �  x�    �  y     �  y2    �  y\    �  y�    �  y�    �  y�    �  z    �  z    �  z:    �  zd    �  z�    �  z�    �  z�    �  z�    �  {    �  {@    �  {d    �  {�    �  {�    �  {�    �  |&    �  |R    �  |�    �  |�    �  |�    �  }    �  }2    �  }\    �  }�    �  }�    �  ~    �  ~J    �  ~t    �  ~�    �  ~�    �  ~�    �  $       P      l      �      �      �      �      �B      �f      ��    	  ��    
  ��      �$      �J      ��      ��      ��      �      �6      �h      ��      ��      �      �6      �`      ��      ��    $  ��    %  ��    &  �6    '  �n    (  ��    )  ��    *  �    +  �P    ,  ��    -  ��    .  �     /  �6    0  �h    1  ��    2  ��    3  ��    4  �    5  �N    6  ��    7  ��    8  �    9  �"    :  �T    ;  ��    =  ��    >  ��    ?  ��    @  �    A  �B    B  �l    C  ��    D  ��    E  ��    F  �    G  �:    H  �^    I  �|    J  ��    K  ��    L  ��    M  �     N  �    O  �>    P  �f    Q  ��    R  ��    S  ��    T  ��    U  �    V  �D    W  �r    X  ��    Y  ��    Z  ��    [  �    \  �6    ]  �d    ^  ��    _  ��    `  ��    a  �     b  �,    c  �V    d  ��    e  ��    f  ��    g  ��    h  �    i  �<    j  �d    k  ��    l  ��    m  ��    n  ��    o  �$    p  �N    q  �j    r  ��    s  ��    t  ��    u  ��    v  �    w  �4    x  �P    y  �j    z  ��    {  ��    |  ��    }  ��    ~  ��      �    �  �<    �  �X    �  ��    �  ��    �  ��    �  ��    �  ��    �  �    �  �$    �  �>    �  �T    �  �t    �  ��    �  ��    �  ��    �  ��    �  ��    �  ��    �  �    �  �:    �  �h    �  ��    �  ��    �  ��    �  ��    �  �    �  �&    �  �H    �  �j    �  ��    �  ��    �  ��    �  �    '  �F    'W  �^    'Y  ��    'Z  ��    '[  ��    '\  ��    ']  ��    '^  ��    '_  ��    '`  �*    'a  �J    'b  �f    'c  ��    'd  ��    'e  ��    'h  ��    'i  �*    'j  ��    '�  ��    '�  ��    '�  ��    '�  �    '�  �:    '�  �R    '�  �f    '�  ��    '�  ��    '�  ��    '�  ��    '�  ��    '�  �    '�  �4    '�  �R    '�  �v    '�  ��    '�  �    '�  �*    '�  �H    '�  �h    '�  ��    '�  ��    '�  ��    '�  ��    '�  �    '�  �:    '�  ��    '�  ��    '�  �    '�  �P    '�  ��    '�  �\    '�  ��    '�  �8    (  �V    (  �p    (  ��    (  ��    (
  ��    (  ��    (  ��    (  �    (  �2    (#  �J    ($  �`    (%  �v    (&  ��    ('  ��    ((  ��    (*  ��    (1  ��    (2  �    (3  �    (4  �@    (5  �X    (6  �r    (7  ��    (8  ��    (<  ��    (=  ��    (>  ��    (A  �    (B  �4    (C  �N    (D  �l    (E  ��    (�  ��    (�  ��    (�  ��    (�  ��    (�  �    (�  �2    (�  �T    (�  ��    (�  ��    (�  ��    (�  ��    (�  �    )   �8    )  �b    )  ��    )  ��    )  ��    )  ��    )  �    )  �&    )  �D    )  �n    )  ��    )  ��    )   �    )!  �&    )"  �F    )#  �f    ),  �|    )-  ��    ).  ��    )/  ��    )0  ��    )1  �    )2  �     )4  �>    )5  �v    )6  ��    )7  ��    )8  ��    )9  ��    ):  �    )@  �0    )A  �D    )B  �\    )C  �t    )D  ��    )E  ��    )F  ��    )G  ��    )H  ��    )I  �    )J  �6    )K  �P    )L  �f    )M  �~    )N  ��    )O  ��    )Z  ��    )[  ��    )\  �     )]  �N    )^  �v    )_  ��    )`  ��    )c  ��    )d  �
    )e  �     )f  �6    )g  �L    )h  �`    )j  �x    )q  ��    )r  ��    )s  ��    )t  ��    )u  ��    )v  �    )w  �     )x  �<    )y  �X    )z  �t    ){  ��    )|  ��    )}  ��    )~  ��    )  ��    )�  �     )�  �    )�  �6    )�  �P    )�  �n    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �*    )�  �H    )�  �`    )�  �~    )�  ��    )�  ��    )�  ��    )�  ��    )�  �     )�  �D    )�  �b    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �.    )�  �N    )�  �j    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �<    )�  �d    )�  ��    )�  ��    )�  ��    )�  �    )�  �"    )�  �<    )�  �f    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �@    )�  �f    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �    )�  �8    )�  �N    )�  �d    )�  �x    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �    )�  �4    )�  �T    )�  �j    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �(    )�  �B    )�  �\    )�  �r    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �     )�  �    )�  �.    )�  �F    )�  �h    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �$    )�  �N    )�  �j    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �:    )�  �X    *   �n    *  ��    *  ��    *  ��    *  ��    *  �    *  �<    *  �\    *  ��    *	  ��    *
  ��    *  ��    *  �    *  �    *2  �>    *3  �f    *4  ��    *5  ��    *D  ��    *E  ��    *F  �    *G  �0    *H  �F    *I  �\    *J  �t    *K  ��    *L  ��    *Y  ��    *^  ��    *_  �
    *`  �6    *a  �`    *b  ��    *c  ��    *m  ��    *n  ��    *o  �    *p  �,    *q  �D    *r  �\    *s  �v    *t  ��    *u  ��    *v  ��    *w  ��    *�  ��    *�  �    *�  �>    *�  �d    *�  ��    *�  ��    *�  ��    *�  ��    *�  �    *�  �,    *�  �L    *�  �n    *�  ��    *�  ��    *�  ��    *�  ��    *�  ��    *�  �    *�  �0    *�  �J    *�  �d    *�  �~    *�  ��    *�  ��    *�  �    *�  �*    *�  �j    *�      *�  ¬    *�  ��    *�  ��    *�  ��    *�  �    *�  �,    *�  �B    *�  �Z    *�  �v    *�  Ö    *�  ò    *�  ��    *�  ��    +n  �    +o  �0    +p  �n    +r  Ĝ    +s  ��    +t  �    +u  �,    +v  �P    +w  �z    +x  Ţ    +y  ��    +z  ��    +{  �&    +|  �L    +}  �l    +~  ƞ    +�  ��    +�  ��    +�  �0    +�  �\    +�  ǎ    +�  ��    +�  �
    +�  �2    +�  �X    +�  �z    +�  Ȝ    +�  ��    +�  ��    +�  �     +�  �J    +�  �x    +�  ɮ    +�  �    +�  �V    +�  ��    +�  ��    +�  �l    +�  ��    +�  �"    +�  ̪    +�  ��    +�  ��    +�  �"    +�  �J    +�  �d    +�  ͨ    +�  ��    +�  �    +�  �D    +�  �v    ,  ά    ,  ��    ,  �    ,  �.    ,  �V    ,  �|    ,  �"    ,  Ј    ,  Т    ,  �    ,  �6    ,  �N    ,   �p    ,!  ъ    ,"  Ѥ    ,#  Ѻ    ,$  ��    ,%  ��    ,&  �    ,'  �$    ,(  �<    ,)  �T    ,*  �r    ,+  Ҕ    ,8  ��    ,9  ��    ,:  �    ,;  �:    ,<  �`    ,=  ӆ    ,>  Ӫ    ,?  ��    ,@  ��    ,B  �    ,C  �:    ,D  �T    ,E  �n    ,F  Ԇ    ,h  Ԡ    ,i  ��    ,j  ��    ,k  �    ,l  �4    ,v  �Z    ,w  �n    ,x  Ղ    ,y  Ֆ    ,z  լ    ,{  ��    ,|  ��    ,}  ��    ,~  �    .�  �8    .�  �R    .�  �t    .�  ֚    .�  ־    .�  ��    .�  �    .�  �*    .�  �N    .�  �~    .�  פ    .�  ��    .�  ��    .�  ��    .�  �    .�  �0    .�  �L    .�  �h    .�  ؄    .�  آ    .�  ��    .�  ��    .�  �     .�  �$    .�  �D    .�  �b    .�  ن    .�  ٬    .�  ��    .�  ��    .�  �     .�  �F    /   �f    /  ڌ    /  ڴ    /  ��    /  �    /  �.    /  �V    /  �r    /  ے    /	  ۴    /
  ��    /  ��    /  �    /  �>    /  �V    /  �r    /  ܐ    /  ܮ    /  ��    /  ��    /  �
    /  �$    /  �B    /  �b    /  ݂    /  ݠ    /  ��    /  ��    /  �    /  �"    /  �B    /  �`    /   �~    /!  ޒ    /"  ް    /#  ��    /$  ��    /%  �     /&  �     /'  �<    /(  �`    /)  �~    /*  ߘ    /+  ߴ    /,  ��    /-  ��    /.  �    //  �$    /0  �@    /1  �X    /2  �r    /3  ��    /4  �    /5  �    /6  ��    /7  ��    /D  �    /E  �*    /F  �N    /G  �t    /H  �    /I  ��    /J  ��    /K  �    /L  �<    /M  �t    /N  �    /O  �    /P  ��    /Q  ��    /R  �    /S  �,    /T  �H    /U  �d    /V  �    /W  �    /X  �    /Y  ��    /Z  �    /[  �$    /\  �F    /]  �f    /^  �    /_  �    /`  ��    /a  �    /b  �.    /c  �V    /d  �v    /e  �    /f  ��    /g  ��    /h  �    /i  �>    /j  �f    /k  �    /l  �    /m  ��    /n  ��    /o  �    /p  �0    /q  �T    /r  �l    /s  �    /t  �    /u  ��    /v  ��    /w  �    /x  �&    /y  �@    /z  �^    /{  �~    /|  �    /}  ��    /~  ��    /  �    /�  �*    /�  �L    /�  �n    /�  �    /�  �    /�  ��    /�  ��    /�  �    /�  �,    /�  �B    /�  �b    /�  �~    /�  �    /�  ��    /�  ��    /�  ��    /�  �    /�  �8    /�  �X    /�  �n    /�  �    /�  �    /�  �    /�  ��    /�  ��    /�  �
    /�  �*    /�  �F    1�  �f    1�  �    1�  �    1�  �    1�  ��    1�  ��    1�  �    1�  �<    1�  �X    1�  �|    1�  �    1�  ��    1�  ��    1�  ��    1�  �    1�  �(    1�  �@    1�  �`    1�  �    1�  �    1�  ��    1�  ��    1�  �    1�  �    1�  �<    1�  �`    1�  �|    1�  �    1�  ��    1�  ��    2   �
    2  �$    2  �>    2  �T    2  �p    2  �    2  ��    2  ��    2  �    2	  �2    2
  �\    2  �    :h  �    :p  ��    :q  ��    :r  �     :s  �    :t  �<    :u  �b    :v  �    :w  �    :z  ��    :{  ��    :|  ��    :  �    :�  �0    :�  �J    :�  �d    :�  �~    :�  �    :�  �    :�  ��    :�  ��    :�  ��    :�  �    :�  �8    :�  �\    :�  �p    :�  �    :�  ��    <   ��    <  ��    <  ��    <  �    <!  �8    <"  �P    <#  �l    <$  ��    <%  ��    <&  ��    <F  �,    <G  �N    <H  �p    <I  ��    <X  ��    <Y  ��    <Z  ��    <[  �    <\  �:    <]  �^    <^  �v    <_  ��    <`  ��    <a  ��    <b  ��    <c  ��    <d  �    <o  �&    <p  �>    <q  �V    <r  �n    <s  ��    <u  ��    C  ��    C  ��    FQ  �
    FR  �\    FS  ��    FT  ��    FU  �     FV  ��    FW  �`    FX  ��    FY  �P    F[  ��    F\  �    F]  �&    F^  �:    F_  �b    F`  ��    Fk  ��    Fl  ��    Fn  ��    Fo  �     Fp  �D    Ft  �j    Fu  ��    Fv  ��    Fw  ��    Fx  ��    Fy  �"    Fz  �L    F{  �j    F|  ��    F}  ��    F~  .    F  R    F�  n    F�  �    F�  �    F�  �    F�  �    F�  �    F�     F� @    F� b    F� �    F� �    F� �    F� �    F� 
    F� .    F� T    F� r    F� �    F� �    F� �    F� �    F�     F� 8    F� f    F� �    F� �    F� �    F�     F� *    F� F    F� Z    F� z    F� �    F� >    F� �    F� �    F�     F� T    F� �    F� �    F�     F� �    F� �    F� 2    F� p    F� �    F� �    F� �    F� �    F� 	
    F� 	(    F� 	T    F� 	j    F� 	~    F� 	�    F� 	�    F� 	�    F� 	�    F� 
    F� 
    F� 
2    F� 
L    F� 
f    F� 
�    F� 
�    F� 
�    F� 
�    F�     F� (    F� L    F� l    F� �    F� �    F� �    F�      F�      F� >    F� ^    F� �    F�     F� R    F� t    F� �    H� �    H� �    H� 8    H� |    � �    �     � D    � p    � �    � �    '� �    '�  *    '�  V    '�  ~    'b i~    'c kX    'd m8    'l m�    '� o4    '� o�    (� p�    FP r�    FQ u�    FR vb    FS {,    FT {n    FU �`    FV �V    FZ �f  #  'S  �  #  'T !  #  'U !r  #  'e !�  #  'f "*  #  'g "�  #  'i "�  #  'k #@  #  '� #�  #  '� #�  #  '� $V  #  '� $�  #  ( $�  #  ( %V  #  (� %�  #  (� &  #  (� &^  #  (� &�  #  (� '
  #  (� '`  #  (� '�  #  (� (  #  (� (b  #  (� (�  #  (� )$  #  (� )�  #  (� )�  #  (� *R  #  (� *�  #  )$ +  #  )% +z  #  )& +�  #  )' ,4  #  +� ,�  #  +� ,�  #  +� -\  #  +� -�  #  +� .*  #  +� .t  #  +� .�  #  +� /  #  ,  /T  #  , /�  #  , /�  #  , 0,  #  , 0t  #  , 0�  #  , 1  #  , 1L  #  , 1�  #  ,	 1�  #  ,
 2,  #  , 2t  #  , 2�  #  , 3  #  , 3R  #  , 3�  #  , 3�  #  , 4  #  ,, 4Z  #  ,- 4�  #  ,. 5&  #  ,/ 5�  #  ,0 5�  #  ,1 6Z  #  ,2 6�  #  ,3 7&  #  ,4 7�  #  ,6 7�  #  ,7 8T  #  ,G 8�  #  ,H 9  #  ,I 9�  #  ,J 9�  #  ,L :R  #  ,M :�  #  ,N ;&  #  ,O ;�  #  ,Q ;�  #  ,R <d  #  ,S <�  #  ,T =2  #  ,V =�  #  ,W =�  #  ,X >j  #  ,Y >�  #  ,[ ?>  #  ,\ ?�  #  ,] @  #  ,^ @n  #  ,_ @�  #  ,` A6  #  ,a A�  #  ,b B  #  ,c Bj  #  ,e B�  #  ,f C4  #  ,g C�  #  ,m C�  #  ,n DX  #  ,o D�  #  ,p E,  #  ,q E�  #  ,r F  #  ,s F@  #  ,t F~  #  ,u F�  $    F�  $  'Q H�  $  'R I�  $  FP J�  $  FQ M  D  (K M�  D  (M U�  D  (P ]�  D  (T e�  D  (W h�  T  '� i:  y  (G �  y  (H �  y  <v �  N4