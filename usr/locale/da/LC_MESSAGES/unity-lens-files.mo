��    2      �  C   <      H  	   I  >   S     �     �     �     �  	   �  	   �  1   �     �                 *   $     O     W     ^     l     t     y  	   ~  	   �     �     �     �     �     �     �     �     �     �  	   �  .   �  
      	   +  	   5     ?     E     Q  	   X  "   b     �  	   �     �     �     �     �     �     �  �  �  
   �  2   �     �     �     �     �  
   �  	   �  2   �     2	     @	     H	     N	  )   ^	     �	     �	     �	     �	     �	     �	  
   �	  
   �	     �	     �	     �	     �	     �	     �	     
     
     
  	   ,
  ,   6
     c
     p
  	   v
     �
     �
     �
     �
  !   �
     �
  
   �
     �
     �
     �
     �
     �
  
   	        -      2                 *                         1   0              
         &       '      /   	                       $         #   (   %   .                 )              "      !   +                                    ,       All Files Another Files Place daemon appears to be running.
Bailing out. April Audio August December Documents Downloads Error connecting to session bus: %s.
Bailing out. Favorite Folders February Files Files & Folders Find documents, downloads, and other files Folders Images Invalid Month January July June Last Week Last Year March May November October Other Past Six Months Presentations Recent Search the web September There are no $section_name in your Home folder This Month This Year This week Today Top Results Videos Yesterday You search did not match any files audio files documents files folders images other files presentations videos Project-Id-Version: unity-place-files
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-08-23 21:08+0200
PO-Revision-Date: 2010-08-23 19:54+0000
Last-Translator: Mikkel Kamstrup Erlandsen <mikkel.kamstrup@gmail.com>
Language-Team: Danish <da@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-08-25 07:12+0000
X-Generator: Launchpad (build Unknown)
 Alle filer Der kører allerede en anden fildæmon.
Afslutter. April Lyd August December Dokumenter Downloads Fejl ved opkobling til sessionsbus: %s.
Afslutter. Favoritmapper Februar Filer Filer og mapper Find dokumenter, downloads og andre filer Mapper Billeder Ugyldig måned Januar Juli Juni Sidste uge Sidste år Marts Maj November Oktober Andet Sidste seks måneder Præsentationer Seneste Søg på nettet September Der er ingen $section_name i din hjemmemappe Denne måned I år Denne uge I dag Topresultater Video I går Din søgning matchede ingen filer lydfiler dokumenter filer mapper billeder andre filer præsentationer videofiler 