plugin: local
_summary: Automated tests for firmware using Firmware Test Suite.
id: firmware/generator_fwts
estimated_duration: 0.5
requires: package.name == 'fwts'
_description: Automated tests for firmware using Firmware Test Suite.
command:
 cat << 'EOF' | run_templates -s 'fwts_test --list'
 estimated_duration: 1.2
 plugin: shell
 category_id: 2013.com.canonical.plainbox::firmware
 id: firmware/fwts_$1
 requires: package.name == 'fwts'
 user: root
 command: fwts_test -t $1 -l $PLAINBOX_SESSION_SHARE/fwts_$1.log
 _description: Run $1 test from Firmware Test Suite.
 _summary: Run $1 test from Firmware Test Suite.
 EOF

plugin: local
_summary: Automated tests for firmware using Firmware Test Suite.
id: firmware/generator_fwts_logs
estimated_duration: 0.5
requires: package.name == 'fwts'
_description: Automated tests for firmware using Firmware Test Suite.
command:
 cat << 'EOF' | run_templates -s 'fwts_test --list'
 estimated_duration: 1.2
 plugin: attachment
 category_id: 2013.com.canonical.plainbox::firmware
 id: firmware/fwts_$1.log
 requires: package.name == 'fwts'
 user: root
 command: [[ -e ${PLAINBOX_SESSION_SHARE}/fwts_$1.log ]] && cat ${PLAINBOX_SESSION_SHARE}/fwts_$1.log
 _description: Attach log for FWTS $1 test.
 _summary: Attach log for FWTS $1 test.
 EOF

plugin:shell
id: firmware/fwts_desktop_diagnosis
estimated_duration: 10.0
requires:
  package.name == 'fwts'
user: root
_description:
 Run Firmware Test Suite (fwts) QA-concerned desktop-specific diagnosis tests.
_summary: Run FWTS QA-concerned desktop-specific diagnosis tests.
environ: PLAINBOX_SESSION_SHARE
command:
 fwts_test --qa -l $PLAINBOX_SESSION_SHARE/fwts_desktop_diagnosis_results.log

plugin:shell
id: firmware/fwts_desktop_diagnosis_hwe
estimated_duration: 5.0
requires:
  package.name == 'fwts'
user: root
_description:
 Run Firmware Test Suite (fwts) HWE-concerned desktop-specific diagnosis tests.
_summary: Run FWTS HWE-concerned desktop-specific diagnosis tests.
environ: PLAINBOX_SESSION_SHARE
command:
 fwts_test --hwe -l $PLAINBOX_SESSION_SHARE/fwts_desktop_diagnosis_results_hwe.log

plugin: attachment
category_id: 2013.com.canonical.plainbox::firmware
estimated_duration: 0.5
id: firmware/fwts_desktop_diagnosis_results.log.gz
command:
 [ -f $PLAINBOX_SESSION_SHARE/fwts_desktop_diagnosis_results.log ] && gzip -c $PLAINBOX_SESSION_SHARE/fwts_desktop_diagnosis_results.log | base64
_description: Attaches the FWTS desktop diagnosis results log to the submission
_summary: Attach FWTS desktop diagnosis log to submission

plugin: attachment
category_id: 2013.com.canonical.plainbox::firmware
estimated_duration: 0.5
id: firmware/fwts_desktop_diagnosis_results_hwe.log.gz
command:
 [ -f $PLAINBOX_SESSION_SHARE/fwts_desktop_diagnosis_results_hwe.log ] && gzip -c $PLAINBOX_SESSION_SHARE/fwts_desktop_diagnosis_results_hwe.log | base64
_description: Attaches the FWTS desktop diagnosis results log to the submission (to HWE)
_summary: Attach FWTS desktop diagnosis log to submission (to HWE)

plugin: shell
category_id: 2013.com.canonical.plainbox::firmware
id: firmware/no_ACPI_REV_interface
_summary: No _REV interface in ACPI [DS]SDT tables
user: root
estimated_duration: 0.5
command: ! sudo grep -r "_REV" /sys/firmware/acpi/tables/* | grep [DS]SDT
_description: This Automated test checks misuse of the _REV interface in ACPI DSDT and SSDT tables
