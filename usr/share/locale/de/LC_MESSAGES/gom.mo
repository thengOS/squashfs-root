��          �      |      �     �                    #     (     A     H     V  	   Z     d     �     �  $   �     �     �          6     V     v  !   �  �  �      �     �     �     �     �  $   �     
             	   $     .     N  )   c     �     �     �  ;   �  -     !   J     l  &   �                                                     
                                  	          A pointer to a sqlite3_stmt. Adapter Filter Limit Mode No resources were found. Offset Resource Type SQL Statement The GomAdapter for the command. The SQL for the command. The adapter used for queries. The command does not contain any SQL The maximum number of results. The mode of the filter. The number of results to skip. The resource type to query for. The size of the resource group. The sql for the filter. sqlite3_prepare_v2 failed: %s: %s Project-Id-Version: gom master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gom&keywords=I18N+L10N&component=general
POT-Creation-Date: 2014-04-30 21:46+0000
PO-Revision-Date: 2014-05-01 13:35+0100
Last-Translator: Christian Kirbach <christian.kirbach@gmail.com>
Language-Team: German <gnome-de@gnome.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.5.4
 Ein Zeiger auf ein sqlite3_stmt. Adapter Filter Grenze Modus Es wurden keine Ressourcen gefunden. Versatz Ressourcentyp SQL Anweisung Der GomAdapter für den Befehl. Das SQL des Befehls. Der für die Anfragen verwendete Adapter. Der Befehl enthält kein SQL Die maximale Anzahl Ergebnisse. Der Modus des Filters Die Anzahl der Ergebnisse, die übersprungen werden sollen. Der Ressourcentyp, der angefragt werden soll. Die Größe der Ressourcengruppe. Das SQL für den Filter. sqlite3_prepare_v2 schlug fehl: %s: %s 