��    *      l  ;   �      �     �     �  '   �     �     �          :     A     G     Z     l          �     �  '   �     �  
   �     �     �  	   �          %     5     N     f  $   �     �     �     �     �       "   /     R     r     �  *   �  ,   �  '         ,  $   M  !   r  �  �     �	     �	  >   �	     �	     �	      
  	   6
     @
     F
     Y
     k
     ~
     �
  #   �
  ,   �
     �
  
   �
     �
       	             0     <     O     c     |     �     �     �     �     �  "     #   3     W     o  ,   �  ;   �  "   �       '   7      _           #   (         
              !         "                                                  %                                                     *   &   	         )      '                 $           A pointer to a sqlite3_stmt. Adapter Cannot save resource, no repository set Count Failed to access SQLite handle. Failed to open database at %s Filter Limit Many-to-Many Table Many-to-Many type Many-to-many table Many-to-many type Mode No resources were found. No result was returned from the cursor. Offset Repository Resource Type SQL Statement The GomAdapter for the command. The GomAdapter. The SQL for the command. The SQL for the filter. The adapter for the repository. The command does not contain any SQL The filter for the command. The maximum number of results. The mode of the filter. The number of results to skip. The query filter. The repository for object storage. The resource type to query for. The resources repository. The size of the resource group. The table to use for many-to-many queries. The table used to join a Many to Many query. The type for the join within m2m-table. The type of resources contained. The type used in the m2m-table join. sqlite3_prepare_v2 failed: %s: %s Project-Id-Version: gom master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gom&keywords=I18N+L10N&component=general
POT-Creation-Date: 2014-10-06 20:29+0000
PO-Revision-Date: 2014-10-15 13:11+0700
Last-Translator: Andika Triwidada <andika@gmail.com>
Language-Team: Indonesian <gnome-l10n-id@googlegroups.com>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
Plural-Forms: nplurals=1; plural=0;
 Sebuah pointer ke sqlite3_stmt. Adaptor Tak bisa menyimpan sumber daya, tak ada repositori yang ditata Cacah Gagal mengakses handle SQLite. Gagal membuka basis data pada %s Penyaring Batas Tabel Many-to-Many Tipe Many-to-Many Tabel many-to-many Tipe many-to-many Mode Tak ada sumber daya yang ditemukan. Tak ada hasil yang dikembalikan dari kursor. Ofset Repositori Tipe Sumber Daya SQL Statement GomAdapter bagi perintah. GomAdapter. SQL bagi perintah. SQL bagi penyaring. Adaptor bagi repositori. Perintah tak memuat SQL apapun Penyaring bagi perintah. Cacah maksimum dari hasil. Mode penyaring. Banyaknya hasil yang dilompati. Penyaring kuiri. Repositori bagi penyimpanan objek. Tipe sumber daya yang akan dikuiri. Repositori sumber daya. Ukuran dari grup sumber daya. Tabel yang dipakai untuk kuiri many-to-many. Tabel yang dipakai untuk men-join suatu kuiri Many to Many. Tipe bagi join di dalam m2m-table. Tipe sumber daya yang dimuat. Tipe yang dipakai dalam join m2m-table. sqlite3_prepare_v2 gagal: %s: %s 