��    ,      |  ;   �      �     �     �  '   �               <     Z     a     m     s     �     �     �     �     �  '   �       
   
          #  	   '     1     Q     a     z     �  $   �     �     �          *     I  "   [     ~     �     �  *   �  ,     '   0      X  $   y  3   �  !   �  �  �     �	     �	  6   �	     +
  9   2
  ,   l
     �
     �
     �
     �
     �
     �
     �
       "     $   9     ^     e  
   r     }  
   �     �     �     �     �     �     �               6     C     c  )   s     �     �     �  ,   �  :         S     t  &   �  &   �  -   �           $   )                       "         #                   !                      	       &                        +                             ,   '   
         *      (                 %           A pointer to a sqlite3_stmt. Adapter Cannot save resource, no repository set Count Failed to access SQLite handle. Failed to open database at %s Filter Is Writable Limit Many-to-Many Table Many-to-Many type Many-to-many table Many-to-many type Mode No resources were found. No result was returned from the cursor. Offset Repository Resource Type SQL Statement The GomAdapter for the command. The GomAdapter. The SQL for the command. The SQL for the filter. The adapter for the repository. The command does not contain any SQL The filter for the command. The maximum number of results. The mode of the filter. The number of results to skip. The query filter. The repository for object storage. The resource type to query for. The resources repository. The size of the resource group. The table to use for many-to-many queries. The table used to join a Many to Many query. The type for the join within m2m-table. The type of resources contained. The type used in the m2m-table join. Whether the group contains resources to be written. sqlite3_prepare_v2 failed: %s: %s Project-Id-Version: gom
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-11-05 13:41+0100
PO-Revision-Date: 2014-11-05 13:42+0100
Last-Translator: Piotr Drąg <piotrdrag@gmail.com>
Language-Team: Polish <gnomepl@aviary.pl>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-Language: Polish
X-Poedit-Country: Poland
 Wskaźnik do sqlite3_stmt. Adapter Nie można zapisać zasobu, nie ustawiono repozytorium Liczba Uzyskanie dostępu do obsługi SQLite się nie powiodło. Otwarcie bazy danych w %s się nie powiodło Filtr Jest zapisywalna Ograniczenie Tablica wiele-do-wielu Typ wiele-do-wielu Tablica wiele-do-wielu Typ wielu-do-wielu Tryb Nie odnaleziono żadnych zasobów. Brak wyników zwróconych z kursora. Offset Repozytorium Typ zasobu SQL Instrukcja GomAdapter dla polecenia. GomAdapter. SQL dla polecenia. SQL filtru. Adapter dla repozytorium. Polecenie nie zawiera SQL Filtr dla polecenia. Maksymalna liczba wyników. Tryb filtru. Liczba wyników do pominięcia. Filtr zapytań. Repozytorium do przechowywania obiektów. Typ zasobu, który odpytywać. Repozytorium zasobów. Rozmiar grupy zasobów. Tablica używana do zapytań wiele-do-wielu. Tablica używana do dołączania zapytania wiele-do-wielu. Typ do dołączania w m2m-table. Typ zawartych zasobów. Typ używany w dołączaniu m2m-table. Czy grupa zawiera zasoby do zapisania. sqlite3_prepare_v2 się nie powiodło: %s: %s 