��    ,      |  ;   �      �     �     �  '   �               <     Z     a     m     s     �     �     �     �     �  '   �       
   
          #  	   '     1     Q     a     z     �  $   �     �     �          *     I  "   [     ~     �     �  *   �  ,     '   0      X  $   y  3   �  !   �  U  �  3   J
     ~
  ^   �
     �
  J     Q   R     �     �     �  '   �  %     '   .  %   V  
   |  /   �  0   �     �     �          $  
   -  4   8      m     �  #   �  -   �  8   �  '   7  +   _      �  H   �     �  4     H   I  "   �  /   �  ^   �  X   D  S   �  1   �  l   #  M   �  B   �           $   )                       "         #                   !                      	       &                        +                             ,   '   
         *      (                 %           A pointer to a sqlite3_stmt. Adapter Cannot save resource, no repository set Count Failed to access SQLite handle. Failed to open database at %s Filter Is Writable Limit Many-to-Many Table Many-to-Many type Many-to-many table Many-to-many type Mode No resources were found. No result was returned from the cursor. Offset Repository Resource Type SQL Statement The GomAdapter for the command. The GomAdapter. The SQL for the command. The SQL for the filter. The adapter for the repository. The command does not contain any SQL The filter for the command. The maximum number of results. The mode of the filter. The number of results to skip. The query filter. The repository for object storage. The resource type to query for. The resources repository. The size of the resource group. The table to use for many-to-many queries. The table used to join a Many to Many query. The type for the join within m2m-table. The type of resources contained. The type used in the m2m-table join. Whether the group contains resources to be written. sqlite3_prepare_v2 failed: %s: %s Project-Id-Version: gom master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gom&keywords=I18N+L10N&component=general
POT-Creation-Date: 2014-11-28 21:14+0000
PO-Revision-Date: 2014-11-29 10:33+0200
Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>
Language-Team: Serbian <gnom@prevod.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Project-Style: gnome
 Показивач ка скулајт3_стању. Прилагођивач Не могу да сачувам извориште, ризнице нису подешене Укупност Нисам успео да приступим ручку СКуЛајта. Нисам успео да отворим базу података на „%s“ Пропусник Уписиво је Ограничење Табела много-за-много Врста много-за-много Табела много-за-много Врста много-за-много Режим Нисам пронашао изворишта. Нема резултата са курзора. Померај Ризница Врста изворишта СКуЛ Стање Гом прилагођивач за наредбу. Гом прилагођивач. СКуЛ за наредбу. СКуЛ за пропусника. Прилагођивач за ризницу. Наредба не садржи ниједан СКуЛ Пропусник за наредбу. Највећи број резултата. Режим пропусника. Број резултата који ће бити прескочени. Пропусник упита. Ризница за смештај предмета. Врста изворишта која ће бити пропитана. Ризница изворишта. Величина групе изворишта. Табела која ће се користити за упите много-за-много. Табела за придруживање са упитом много за много. Врста придруживања са табелом много-за-много. Врста садржаних изворишта. Врста која се користи у придруживању табеле много за много. Да ли група садржи изворишта за уписивање. скулајт3_припреми_и2 није успело: %s: %s 