��    ,      |  ;   �      �     �     �  '   �               <     Z     a     m     s     �     �     �     �     �  '   �       
   
          #  	   '     1     Q     a     z     �  $   �     �     �          *     I  "   [     ~     �     �  *   �  ,     '   0      X  $   y  3   �  !   �    �     
  	   !
  F   +
     r
  )   y
  &   �
     �
     �
     �
     �
               /     G     L  $   k     �     �     �     �  	   �     �     �     �       !   &     H     c  !   ~     �  $   �     �  &   �  '        B  !   ^  4   �  6   �  )   �  #     ,   :  2   g  !   �           $   )                       "         #                   !                      	       &                        +                             ,   '   
         *      (                 %           A pointer to a sqlite3_stmt. Adapter Cannot save resource, no repository set Count Failed to access SQLite handle. Failed to open database at %s Filter Is Writable Limit Many-to-Many Table Many-to-Many type Many-to-many table Many-to-many type Mode No resources were found. No result was returned from the cursor. Offset Repository Resource Type SQL Statement The GomAdapter for the command. The GomAdapter. The SQL for the command. The SQL for the filter. The adapter for the repository. The command does not contain any SQL The filter for the command. The maximum number of results. The mode of the filter. The number of results to skip. The query filter. The repository for object storage. The resource type to query for. The resources repository. The size of the resource group. The table to use for many-to-many queries. The table used to join a Many to Many query. The type for the join within m2m-table. The type of resources contained. The type used in the m2m-table join. Whether the group contains resources to be written. sqlite3_prepare_v2 failed: %s: %s Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gom&keywords=I18N+L10N&component=general
POT-Creation-Date: 2014-11-04 18:56+0000
PO-Revision-Date: 2015-01-12 19:02+0100
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: Español; Castellano <gnome-es-list@gnome.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.6
 Un puntero a sqlite3_stmt. Adaptador No se puede guardar el recurso, no hay ningún repositorio configurado Conteo Falló al acceder al manejador de SQLite. Falló al abrir la base de datos en %s Filtro Se puede escribir Límite Tabla muchos-a-muchos Tipo de muchos-a-muchos Tabla muchos-a-muchos Tipo de muchos-a-muchos Modo No se han encontrado recursos. El cursor no ha devuelto resultados. Desplazamiento Repositorio Tipo de recurso SQL Sentencia EL GomAdapter para el comando. El GomAdapter. El SQL para el comando. El SQL para el filtro. El adaptador para el repositorio. El comando no contiene SQL El filtro para el comando. El número máximo de resultados. El modo del filtro. El número de resultados que omitir. El filtro de la consulta. El repositorio para almacenar objetos. EL tipo de recurso por el que preguntar El repositorio de recursos. El tamaño del grupo de recursos. La tabla que usar para las consultas muchos-a-muchos La tabla usada para unir una consulta muchos a muchos. EL tipo para la unión con una tabla m2m. El tipo de los recursos contenidos. El tipo usado en la unión con la tabla m2m. Indica si el grupo contiene recursos que escribir. falló sqlite3_prepare_v2: %s: %s 