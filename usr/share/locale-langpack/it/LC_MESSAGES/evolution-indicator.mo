��          �      L      �     �     �     �     �     �       3        M     [     v  7   �  ,   �     �          '  #   ?  
   c  	   n  �  x  $   G     l     n     �     �  	   �  0   �     �  (   �       C   2  C   v     �  "   �     �  '        :      M                                                 	                                     
              %d New Message %d New Messages : Compose New Message Contacts Evolution Indicator Inbox Only create notifications for new mail in an Inbox. Pla_y a sound Play a sound for new mail. Show a notification bubble. Show new message count in the message indicator applet. Shows new mail count in a message indicator. When New Mail Arrives When new mail arri_ves in _Display a notification _Indicate new messages in the panel any Folder any Inbox Project-Id-Version: evolution-indicator
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-24 15:13+0000
PO-Revision-Date: 2013-04-11 09:03+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-10-09 11:41+0000
X-Generator: Launchpad (build 18227)
 %d nuovo messaggio %d nuovi messaggi : Componi nuovo messaggio Contatti Indicatore Evolution In arrivo Notifica solo i nuovi messaggi in «In arrivo». Riprod_urre un suono Riproduce un suono per i nuovi messaggi. Mostra una notifica. Mostra il numero di nuovi messaggi nell'applet indicatore messaggi. Mostra il conteggio delle nuove email nell'indicatore dei messaggi. All'arrivo di nuove email Quando arri_vano nuovi messaggi in _Visualizzare una notifica _Indicare i nuovi messaggi nel pannello qualunque cartella qualunque cartella «In arrivo» 