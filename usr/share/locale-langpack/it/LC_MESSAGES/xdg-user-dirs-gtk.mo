��          |      �             !     5  -   E  '   s  3   �  ,   �     �  �        �     �     �  �       �     �  5   �  6   �  =   0  1   n     �  �   �     �     �     �                            
      	                         Current folder name New folder name Note that existing content will not be moved. There was an error updating the folders Update common folders names to match current locale Update standard folders to current language? User folders update You have logged in in a new language. You can automatically update the names of some standard folders in your home folder to match this language. The update would change the following folders: _Don't ask me this again _Keep Old Names _Update Names Project-Id-Version: xdg-user-dirs-gtk
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-04 03:00+0000
PO-Revision-Date: 2012-04-04 13:07+0000
Last-Translator: Luca Ferretti <Unknown>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:21+0000
X-Generator: Launchpad (build 18115)
 Attuale nome cartella Nuovo nome cartelle Notare che il contenuto esistente non sarà spostato. Si è verificato un errore nell'aggiornare le cartelle Aggiorna i nomi di cartella comuni in base alla lingua in uso Aggiornare cartelle standard alla lingua attuale? Aggiorna cartelle utente È stato effettuato l'accesso usando una nuova lingua. È possibile aggiornare automaticamente i nomi di alcune cartelle nella propria cartella home in modo che corrispondano alla nuova lingua. L'aggiornamento cambierà le seguenti cartelle: _Non domandare più in futuro _Mantieni vecchi nomi A_ggiorna nomi 