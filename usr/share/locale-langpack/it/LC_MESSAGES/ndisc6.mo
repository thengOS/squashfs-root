��   f   0   `  �   �     �     �  �      �  �  �  �  �
  �  �  �  �     D  	   W     a  9   |     �     �  &   �          4     H     U     f     x     �     �     �     �  	         
     %     7     J     Z     h     p     �     �  $   �  (   �          !     5     K  ,   _     �     �     �  6   �  (        ;     P     p     |          �     �     �     �     �     �     
  &   '  %   N     t     �  )   �  )   �     �  �     
   �     �  "   �  �  �  �  �  u   �  T     �   p  `   '   =   �   0   �      �      !  	   !     %!     -!     ?!  
   S!  	   ^!     h!     m!     u!     �!     �!     �!     �!  =   �!     �!     	"     "     '"  ;   3"     o"  +   ~"     �"  4   �"  	   �"      #  �  #  �  �$  F  �&  t  �(  6  m-     �1  	   �1     �1  4   �1     2  !   12  -   S2     �2     �2     �2     �2     �2     �2     �2     3      >3     _3     t3     |3     �3     �3     �3     �3     �3     �3  #   4  !   %4  -   G4  0   u4  !   �4     �4     �4     5  I   !5  *   k5     �5     �5  6   �5  (   �5     '6      E6     f6     v6     y6     �6  !   �6     �6     �6     �6  !    7     "7  &   =7  4   d7     �7  #   �7  -   �7  +   8  &   .8  �   U8     9     +9  ,   39    `9  �  p;  z   l=  h   �=  �   P>  c   ?  N   y?  .   �?     �?     @  	   @  	   %@     /@     A@  	   U@     _@     g@  
   l@     w@     �@     �@     �@     �@  9   �@     �@     A     A     2A  D   ?A     �A  -   �A     �A  N   �A     'B     4B     [   A                              ?         C   ]   /   '   G   W           <   2               Z   `                 P   8      9      Y   d   B      %       a   V               T   R   )   H   :      >   N          b      &   U       .   S           D      K   
                  4   Q           I                         @       ^          \       +       E   ,   3   e       	       L       $   6   -      O   "   !       ;       =   #       5       F       M   X   7   0      c      J   f   (   1   _   *         ;B  �  �  AB             ����cB  '          ���� 
  -1, --single   display first response and exit
  -h, --help     display this help and exit
  -m, --multiple wait and display all responses
  -n, --numeric  don't resolve host names
  -q, --quiet    only print the %s (mainly for scripts)
  -r, --retry    maximum number of attempts (default: 3)
  -V, --version  display program version and exit
  -v, --verbose  verbose display (this is the default)
  -w, --wait     how long to wait for a response [ms] (default: 1000)

 
  -4  force usage of the IPv4 protocols family
  -6  force usage of the IPv6 protocols family
  -b  specify the block bytes size (default: 1024)
  -d  wait for given delay (usec) between each block (default: 0)
  -e  perform a duplex test (TCP Echo instead of TCP Discard)
  -f  fill sent data blocks with the specified file content
  -h  display this help and exit
  -n  specify the number of blocks to send (default: 100)
  -V  display program version and exit
  -v  enable verbose output
 
  -A  send TCP ACK probes
  -d  enable socket debugging
  -E  set TCP Explicit Congestion Notification bits in TCP packets
  -f  specify the initial hop limit (default: 1)
  -g  insert a route segment within a "Type 0" routing header
  -h  display this help and exit
  -I  use ICMPv6 Echo Request packets as probes
  -i  force outgoing network interface
  -l  display incoming packets hop limit
  -m  set the maximum hop limit (default: 30)
  -N  perform reverse name lookups on the addresses of every hop
  -n  don't perform reverse name lookup on addresses
  -p  override destination port
  -q  override the number of probes per hop (default: 3)
  -r  do not route packets
  -S  send TCP SYN probes
  -s  specify the source IPv6 address of probe packets
  -t  set traffic class of probe packets
  -U  send UDP probes (default)
  -V  display program version and exit
  -w  override the timeout for response in seconds (default: 5)
  -z  specify a time to wait (in ms) between each probes (default: 0)
 
  -A  send TCP ACK probes
  -d  enable socket debugging
  -E  set TCP Explicit Congestion Notification bits in probe packets
  -f  specify the initial hop limit (default: 1)
  -g  insert a route segment within a "Type 0" routing header
  -h  display this help and exit
  -i  force outgoing network interface
  -l  set probes byte size
  -m  set the maximum hop limit (default: 30)
  -N  perform reverse name lookups on the addresses of every hop
  -n  don't perform reverse name lookup on addresses
  -p  override source TCP port
  -q  override the number of probes per hop (default: 3)
  -r  do not route packets
  -S  send TCP SYN probes (default)
  -s  specify the source IPv6 address of probe packets
  -t  set traffic class of probe packets
  -V, --version  display program version and exit
  -w  override the timeout for response in seconds (default: 5)
  -z  specify a time to wait (in ms) between each probes (default: 0)
                          %3u     infinite (0xffffffff)
   DNS server lifetime     :    DNS servers lifetime    :    Pref. time              :    Route lifetime          :    Route preference        :       %6s
   Valid time              :   %3u%% completed...  %u.%03u ms   (      0x%02x)
  (%0.3f kbytes/s)  MTU                      :   Prefix                   :   Recursive DNS server     : %s
  Source link-layer address:   built %s on %s
  from %s
  unspecified (0x00000000)
 %12u (0x%08x) %s
 %s %lu %s in %f %s %s port %s: %s
 %s%s%s%s: %s
 %s: %s
 %s: invalid hop limit
 %s: invalid packet length
 %u hop max,  %u hops max,  %zu byte packets
 %zu bytes packets
 Cannot create %s (%m) - already running? Cannot find user "%s" Cannot run "%s": %m Cannot send data: %s
 Cannot write %s: %m Child process hung up unexpectedly, aborting Child process returned an error Configured with: %s
 Connection closed by peer Copyright (C) %u-%u Pierre Ynard, Remi Denis-Courmont
 Copyright (C) %u-%u Remi Denis-Courmont
 Fatal error (%s): %m Hop limit                 :     Input error No No response. Raw IPv6 socket Reachable time            :  Receive error: %s
 Received Receiving ICMPv6 packet Retransmit time           :  Router lifetime           :  Router preference         :       %6s
 Sending %ju %s with blocksize %zu %s
 Sending ICMPv6 packet Soliciting %s (%s) on %s...
 Stateful address conf.    :          %3s
 Stateful other conf.      :          %3s
 Target link-layer address:  This is free software; see the source for copying conditions.
There is NO warranty; not even for MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.
 Timed out. Transmitted Try "%s -h" for more information.
 Usage: %s [-4|-6] [hostnames]
Converts names to addresses.

  -4, --ipv4     only lookup IPv4 addresses
  -6, --ipv6     only lookup IPv6 addresses
  -c, --config   only return addresses for locally configured protocols
  -h, --help     display this help and exit
  -m, --multiple print multiple results separated by spaces
  -n, --numeric  do not perform forward hostname lookup
  -r, --reverse  perform reverse address to hostname lookup
  -V, --version  display program version and exit
 Usage: %s [OPTIONS]
Starts the IPv6 Recursive DNS Server discovery Daemon.

  -f, --foreground  run in the foreground
  -H, --merge-hook  execute this hook whenever resolv.conf is updated
  -h, --help        display this help and exit
  -p, --pidfile     override the location of the PID file
  -r, --resolv-file set the path to the generated resolv.conf file
  -u, --user        override the user to set UID to
  -V, --version     display program version and exit
 Usage: %s [options] <IPv6 address> <interface>
Looks up an on-link IPv6 node link-layer address (Neighbor Discovery)
 Usage: %s [options] <IPv6 hostname/address> [%s]
Print IPv6 network route to a host
 Usage: %s [options] <hostname/address> [service/port number]
Use the discard TCP service at the specified host
(the default host is the local system, the default service is discard)
 Usage: %s [options] [IPv6 address] <interface>
Solicits on-link IPv6 routers (Router Discovery)
 Warning: "%s" is too small (%zu %s) to fill block of %zu %s.
 Written by Pierre Ynard and Remi Denis-Courmont
 Written by Remi Denis-Courmont
 Yes [closed]  [open]  addrinfo %s (%s)
 advertised prefixes byte bytes from %s,  high invalid link-layer address low medium medium (invalid) millisecond milliseconds ndisc6: IPv6 Neighbor/Router Discovery userland tool %s (%s)
 packet length port  port %u, from port %u,  port number rdnssd: IPv6 Recursive DNS Server discovery Daemon %s (%s)
 second seconds tcpspray6: TCP/IP bandwidth tester %s (%s)
 traceroute to %s (%s)  traceroute6: TCP & UDP IPv6 traceroute tool %s (%s)
 undefined valid Project-Id-Version: ndisc6
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2011-02-20 10:14+0200
PO-Revision-Date: 2012-03-09 15:44+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 18:10+0000
X-Generator: Launchpad (build 18115)
 
  -1, --single Mostra la prima risposta ed esce
  -h, --help Mostra questo aiuto ed esce
  -m, --multiple Attende e mostra tutte le risposte
  -n, --numeric Non risolve i nomi host
  -q, --quiet Stampa solo %s (principalmente per script)
  -r, --retry Numero massimo di tentativi (default:3)
  -V, --version Nostra la versione del programma ed esce
  -v, --verbose Mostra i messaggi (predefinito)
  -w, --wait Tempo di attesa per una risposta [ms] (predefinito: 1000)

 
  -4 Forza l'uso della famiglia di protocolli IPv4
  -6 Forza l'uso della famiglia di protocolli IPv6
  -b Specifica la dimensione della dimensione dei blocchi di byte (predefinito: 1024)
  -d Aspetta per il ritardo fissato (microsec) tra ogni blocco (predefinito: 0)
  -e Esegue un test duplex (TCP Echo invece di TCP Discard)
  -f Riempie i blocchi dati inviati con il contenuto del file specificato
  -h Mostra questo aiuto ed esce
  -n Specifica il numero di blocchi da inviare (predefinito: 100)
  -V Mostra la versione del programma ed esce
  -v Abilita l'output dettagliato
 
  -A Invia prove ACK TCP
-d Abilita il debug del socket
-E Imposta i bit TCP Explicit Congestion Notification nei pacchetti di prova
-f Specifica il livello di hop iniziale (predefinito: 1)
-g Inserisce un segmento d'instradamento all'interno di un header d'instradamento "Type 0"
-h Mostra questo aiuto ed esce
-I Usa i pacchetti ICMPv6 Echo Request come prova
-i Forza l'uscita dall'interfaccia di rete
-l Mostra il limite di hop dei pacchetti in entrata
-m Imposta il massimo limite di hop (predefinito: 30)
-N Esegue ricerche inverse del nome dell'indirizzo di ogni hop
-n Non esegue ricerche inverse del nome degli indirizzi 
-p Scavalca la porta TCP sorgente
-q Scavalca il numero di prove per hop (predefinito: 3)
-r Non instradare pacchetti
-S Invia prove TCP SYN (predefinito)
-s Specifica l'indirizzo sorgente IPv6 dei pacchetti di prova
-t Imposta la classe del traffico dei pacchetti di prova
-U Invia prove UDP (predefinito)
-V, --version Mostra la versione del programma ed esce
-w Scavalca il tempo d'attesa per la risposta in secondi (predefinito: 5)
-z Specifica il tempo di attesa (in ms) tra ogni prova (predefinito: 0)
 
  -A Invia prove ACK TCP
  -d Abilita il debug del socket
  -E Imposta i bit TCP Explicit Congestion Notification nei pacchetti prova
  -f Specifica il livello di hop iniziale (predefinito: 1)
  -g Inserisce un segmento d'instradamento all'interno di un header d'instradamento "Type 0"
  -h Mostra questo aiuto ed esce
  -i Forza l'uscita dall'interfaccia di rete
  -l Imposta la misura dei byte di prova
  -m Imposta il massimo limite di hop (predefinito: 30)
  -N Esegue ricerche inverse del nome dell'indirizzo di ogni hop
  -n Non esegue ricerche inverse del nome degli indirizzi 
  -p Scavalca la porta TCP sorgente
  -q Scavalca il numero di prove per hop (predefinito: 3)
  -r Non instrada i pacchetti
  -S Invia prove TCP SYN (predefinito)
  -s Specifica l'indirizzo sorgente IPv6 dei pacchetti di prova
  -t Imposta la classe del traffico dei pacchetti di prova
  -V, --version Mostra la versione del programma ed esce
  -w Scavalca il tempo d'attesa per la risposta in secondi (predefinito: 5)
  -z Specifica il tempo d'attesa (in ms) tra ogni prova (predefinito: 0)
                          %3u     infinito (0xffffffff)
   Durata server DNS     :    Durata server DNS    :    Tempo di rec.              :    Durata istradamento          :    Preferenza istradamento        :       %6s
   Valid time              :   %3u%% completato...  %u.%03u ms   (      0x%02x)
  (%0.3f kbyte/s)  MTU                      :   Prefisso                   :   Server DNS ricorsivo     : %s
  Indirizzo link-layer sorgente:   compilato %s il %s
  da %s
  non specificato (0x00000000)
 %12u (0x%08x) %s
 %s %lu %s in %f %s %s porta %s:%s
 %s%s%s%s: %s
 %s: %s
 %s: limite hop non valido
 %s: lunghezza pacchetto non valida
 %u hop massimo,  %u hop massimi,  pacchetti di %zu byte
 pacchetti di %zu byte
 Impossibile creare %s (%m) - già in esecuzione? Impossibile trovare l'utente "%s" Impossibile avviare "%s": %m Impossibile inviare i dati: %s
 Impossibile scrivere %s: %m Il processo figlio si è bloccato in modo inatteso, interruzione in corso Il sottoprocesso ha risposto con un errore Configurato con: %s
 Connessione chiusa dal peer Copyright (C) %u-%u Pierre Ynard, Remi Denis-Courmont
 Copyright (C) %u-%u Remi Denis-Courmont
 Errore irreversibile (%s): %m Limite hop                 :     Errore di input No Nessuna risposta. Socket raw IPv6 Tempo raggiungibile            :  Errore nella ricezione: %s
 Ricevuti Pacchetto ICMPv6 in ricezione Tempo ritrasmissione           :  Durata router           :  Preferenza router         :       %6s
 Invio di %ju %s con dimensione del blocco di %zu %s
 Invio pacchetto ICMPv6 Invio richiesta a %s (%s) su %s...
 Config. indirizzo stateful    :          %3s
 Config. altro stateful      :          %3s
 Indirizzo link-layer di destinazione:  Questo è software libero; consultare il codice sorgente per le condizioni di copia.
Non esiste ALCUNA garanzia; neanche quella implicita della COMMERCIABILITÀ o
IDONEITÀ A UNO SCOPO PARTICOLARE.
 Tempo scaduto. Inviati Provare "%s -h" per ulteriori informazioni.
 Uso: %s [-4|-6] [nomi_host]
Converte i nomi in indirizzi.

  -4, --ipv4     cerca solo gli indirizzi IPv4
  -6, --ipv6     cerca solo gli indirizzi IPv6
  -c, --config   restituisce solo gli indirizzi per i protocolli configurati localmente
  -h, --help     mostra la guida ed esce
  -m, --multiple visualizza più risultati separati da spazi
  -n, --numeric  non esegue l'inoltro di ricerca nome host
  -r, --reverse  esegue l'indirizzo inverso alla ricerca nome host
  -V, --version  mostra la versione del programma ed esce
 Uso: %s [OPZIONI]
Avvia il demone di rilevamento del server DNS ricorsivo IPv6.

  -f, --foreground  avvia in primo piano
  -H, --merge-hook  esegue questa connessione ogniqualvolta resolv.conf viene aggiornato
  -h, --help        mostra la guida ed esce
  -p, --pidfile     sovrascrive la posizione del file PID
  -r, --resolv-file imposta il percorso al file resolv.conf generato
  -u, --user        sosvrascrive l'utente a cui impostare l'UID
  -V, --version     mostra la versione del programma ed esce
 Uso: %s [opzioni] <indirizzo IPv6> <interfaccia>
Cerca un indirizzo link-layer del nodo IPv6 on-link (Neighbor Discovery)
 Utilizzo: %s [opzioni] <nomehost/indirizzo IPv6> [%s]
Stampa l'instradamento di rete IPv6 verso un host
 Utilizzo: %s [opzioni] <nomehost/indirizzo> [servizio/numeroporta]
Usa il servizio discard TCP con l'host specificato
(l'host predefinito è il sistema locale, il servizio predefinito è discard)
 Uso: %s [opzioni] [indirizzo IPv6] <interfaccia>
Richiede i router IPv6 on-link (Router Discovery)
 Attenzione: "%s" è troppo piccolo (%zu %s) per riempire il blocco di %zu %s.
 Scritto da Pierre Ynard e Remi Denis-Courmont
 Scritto da Remi Denis-Courmont
 Sì [chiuso]  [aperto]  addrinfo %s (%s)
 prefissi dichiarati byte byte da %s,  alto non valido indirizzo link-layer basso medio medio (non valido) millisecondo millisecondi ndisc6: strumento di scoperta vicini/router IPv6 %s (%s)
 lunghezza pacchetto porta  porta %u, da porta %u,  numero porta rdnssd: demone di rilevamento del server DNS ricorsivo IPv6 %s (%s)
 secondo secondi tcpspray6: tester della banda TCP/IP %s (%s)
 rotta fino a %s (%s)  traceroute6: strumento per il tracciamento della rotta TCP e UDP IPv6 %s (%s)
 non definito valido PRIu8  Route                    : %s/%
  Istradamento                    : %s/%
 