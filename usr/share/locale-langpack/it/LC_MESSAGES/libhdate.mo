��    �      <  3  \      (  t  )  �  �+  E   `A  �   �A  ,   cB  W   �B  :   �B  
   #C     .C     BC     GC     NC     VC     ZC     `C     nC     rC     yC     |C     �C  
   �C     �C     �C     �C     �C     �C  	   �C     �C     �C     �C     �C     �C     D     
D     D     D     $D     ,D     2D     7D     <D     ID  
   YD     dD     hD     qD     uD     |D     �D     �D  �  �D  %   -K     SK     eK     wK     �K     �K     �K     �K     �K     �K     �K     �K     �K     �K  
   �K     �K     �K     �K     �K     �K  
   �K  
   	L     L     L     'L     +L     1L     7L     =L     IL  8   ML     �L     �L     �L  	   �L     �L     �L     �L     �L     �L     �L     �L     �L     �L     �L     �L     M     
M     M     M     !M     )M  	   0M  
   :M     EM     QM     YM     _M     rM     xM     �M     �M     �M     �M  	   �M     �M     �M     �M  
   �M     �M     �M     �M     �M     �M     N     N     N  	   "N     ,N     0N     7N     FN     MN     VN     ]N     lN     uN     }N     �N  �   �N     1O     5O     >O     JO     RO     YO  
   aO     lO     pO     xO     }O     �O  �   �O  ?  �P     �Q  
   �Q     �Q     �Q      R  	   R     R     R     "R     +R     4R  
   <R     GR     VR  	   ZR     dR     jR     �R     �R     �R  
   �R     �R     �R     �R     �R     �R     S     S  -   S     MS     SS  (   [S     �S     �S     �S     �S  "   �S  �  �S  3   �Z     �Z     �Z  #   �Z     [     $[     :[     G[  )   a[     �[     �[     �[     �[  
   �[     �[  	   �[     �[     �[     �[     �[     �[     �[     �[     \  3   \  4   R\  
   �\  >   �\  4   �\  4   ]     ;]  0   @]     q]     ]  �  �]  �  5_  �  |  l   �  �   W�  3   B�  f   v�  F   ݗ  
   $�     /�     C�     H�     O�     W�     [�     b�     p�     t�     {�     ~�     ��  
   ��     ��     ��     ��     ��     Ę  	   ͘     ט     ژ     �     �     ��     �     �     �     �     &�     .�     4�     9�     >�     K�     [�     q�     u�     ~�     ��     ��     ��     ��  �  ��  (   h�     ��     ��     ��     ��     ˡ     ѡ     ա     ݡ     �     �     �     �     ��  
   �     �     �     �     %�     5�  
   D�  
   O�     Z�     a�     m�     q�     w�     }�     ��     ��  J   ��     �     �     ��  	   �     �     �     �     �     %�     3�     9�     B�     U�     [�     _�     h�     l�     t�     }�     ��     ��  	   ��  
   ��     ��     ��     ��     £     �     �     ��     �     
�     �  	   �     �     '�     .�  
   6�     A�     H�     O�     ^�     f�     t�     ��     ��  	   ��     ��     ��     ��     ��     ��     Ť     ̤     ۤ     �     �     ��  �   ��     ʥ     Υ     ץ     �     �     �  
   ��     �     	�     �     �     %�    1�  S  :�     ��  
   ��     ��     ��     ��  	   ��     ˨     Ҩ     ۨ     �     ��  
   ��      �     �  
   �     �  &   $�     K�     Z�     f�  
   t�     �  )   ��  $   ��     ߩ     ��     �     �  9   #�     ]�     d�  >   p�     ��  
   ɪ     Ԫ     �  &   �  �  �  @   ��  
   �     �  '   ��  .   '�     V�     o�  %   ~�  9   ��     ޳     �     ��     �     
�     �  	   &�     0�  
   9�     D�     I�     R�  
   X�     c�     ��  9   ��  0   ̴  
   ��  ;   �  C   D�  C   ��     ̵  I   ѵ     �     +�         9   B   �   A          @       �       �   �      C   �   �   �   ]   %           �       �   �               �   8   N   (   �   "       ;                  *   �   �           p   +   �   ?   b   �   t       �   d          �   
       �   3       a   �   �   q       �       �   i   �   �       �       �   �   {   [   D   }       !   �   Z   $   �   �   I   �   �   �   �   7   �   �           �   S   �   W         X   .   �   �   �       �   '   �       �               o          E      c   s       �   �   �   �   P          �   �   �               _       �   5              ^       &               x       u   L      e       	   �       �   Q          �   �   \   v   J   �   �   F      0   �   �               �       �          U                    �       r   )   �   /   R           �       �   w   �       �   m   �   �       �   �   2   l   y               M   4   ~   �   V   �      �   �   =   |             Y   1      �                     �          ,       �   �       �   �   �   `   G   :   �   �   �   6         f   �   �   �           �      >   �   �   <   �   K       k   #   �   �          j       �   �   z   -   n   g       O   �   T   H   h          �       �    # configuration file for hcal - Hebrew calendar program
# part of package libhdate
#
# Should you mangle this file and wish to restore its default content,
# rename or delete this file and run hcal; hcal will automatically
# regenerate the default content.
#
# Your system administrator can set system-wide defaults for hcal by
# modifying file <not yet implemented>.
# You may override all defaults by changing the contents of this file.
#
# Version information
# This may be used by updates to hcal to determine how to parse the file
# and whether additional information and options should be appended to
# the end of this file.
VERSION=2.00
# Location awareness
# hcal wants to accurately highlight the current Hebrew day, including
# during the hours between sunset and secular midnight. If you don't
# provide it with latitude, longitude, and time zone information, hcal
# will try to guess the information, based upon your system timezone,
# and its (limited, and maybe biased) of the dominant Jewish community
# in that timezone. When hcal is forced to guess, it alerts the user
# with a message that includes the guessed location.
# hcal's guesses will also affect its default behaviour for output of
# Shabbat times, parshiot, and choice of Israel/diaspora hoidays
#SUNSET_AWARE=TRUE
# LATITUDE and LONGITUDE may be in decimal format or in the form
# degrees[:minutes[:seconds]] with the characters :'" as possible
# delimiters. Use negative values to indicate South and West, or
# use the abbreviated compass directions N, S, E, W.
#LATITUDE=
#LONGITUDE=
# TIMEZONE may may be in decimal format or in the form degrees[:minutes]
# with the characters :'" as possible delimiters.
#TIMEZONE=

# Israel and the diaspora
# If hcal guesses that you're not in Israel, the DIASPORA option will be
# set true. This will affect holiday and parasha output.  FORCE_ISRAEL
# forces hcal to display calendar information for Israel, using hcal's
# default coordinates foe Israel, or coordinates that you provide that
# seem legitmately within Israel.
# Thus, if you are living in Sao Paolo, and will be visiting Israel for
# Sukkot, set BOTH flags true in order to see holiday information for
# a non-resident vistor to Israel. The command line options for these
# features are --israel, -I, --diaspora, -d.
#FORCE_DIASPORA=FALSE;
#FORCE_ISRAEL=FALSE;

# Shabbat related
# Setting SHABBAT_INFO true will output parshiot and Shabbat times.
# The command line options for these features are -p (--parasha), and
# -s (--shabbat). The CANDLE_LIGHTING field can accept a value of 18 - 90 (minutes
# before sunset). The HAVDALAH field can accept a value of 20 - 90
# (minutes after sunset).
#PARASHA_NAMES=FALSE
#SHABBAT_INFO=FALSE
#CANDLE_LIGHTING=FALSE
#HAVDALAH=FALSE

# Holiday identification
# hcal flags holidays by replacing the gregorian/Hebrew date separator
# with assorted unhelpful cryptic symbols. Setting FOOTNOTES to true
# will have hcal output after the month's calendar, a list of the month's
# holidays along with the days on which they occur.
#FOOTNOTE=FALSE

# Output in hebrew characters
# hcal defaults to output all information in your default language, so
# if your default language is Hebrew, you're all set. Otherwise, you can
# set FORCE_HEBREW to true to output Hebrew information in Hebrew, while
# still outputting gregorian information in your default language. To
# output ALL information in Hebrew, run something like this:
#    LC_TEMP=LC_ALL; LC_ALL="he_IL.UTF-8"; hcal; LC_ALL=$LC_TEMP
# If setting FORCE_HEBREW to true results in 'garbage' or non-Hebrew
# output, you need to install a terminal font that includes the Hebrew
# character set (hint: unicode).
# The command line option for FORCE_HEBREW is either --hebrew or -H
#FORCE_HEBREW=FALSE

# The FORCE_HEBREW option outputs data that is 'correct' and 'logical'.
# Unfortunately, the world can not be depended upon to be either. Most
# Xwindow applications will display the data fine with FORCE_HEBREW; most
# xterm implementations will not. (in fact, the only xterm clone I know
# of that comes close is mlterm). If using FORCE_HEBREW results in
# Hebrew characters being displayed in reverse, set OUTPUT_BIDI to true.
# This will reverse the order of the Hebrew characters, so they will
# display 'visual'ly correct; however, such output will not be suitable
# for piping or pasting to many other applications. Setting OUTPUT_BIDI
# automatically forces hebrew.
# The command line option for OUTPUT_BIDI is either --bidi, --visual, or -b
#OUTPUT_BIDI=FALSE

# Display enhancements
# hcal defaults to display the current day in reverse video
# The command line option for this feature is --no-reverse
#SUPPRESS_REVERSE_VIDEO=FALSE;
# hcal can display its output "calming, muted tones". Note that piping
# colorized output may yield unexpected results.
#COLORIZE=FALSE

# HTML OUTPUT
#OUTPUT_HTML=FALSE
#USE_EXTERNAL_CSS_FILE="pathto/foo/bar"

# Suppress alerts and warnings
# hcal alerts the user via STDERR when it guesses the user's location.
#QUIET_ALERTS=FALSE

# Three month display
# hcal can display a previous, current and next month side-by-side. hcal
# can also display a calendar for an entire year in four rows of three
# months each. Note that this will display properly only if your console
# is set for at least 127 columns. Note also that setting this option to
# will cause options FOOTNOTES, SHABBAT, and PARASHA_NAMES to false.
#THREE_MONTH=FALSE

# User-defined menus
# You may specify here command-line strings to optionally be parsed
# by hcal at execution time. To do so, use the command line option -m
# (--menu). hcal will process first the settings of this config file,
# then the other settings of your command line, and then will prompt
# you for which menu item you would like to select. hcal will process
# your menu selection as if it were a new command line, further modifying
# all the prior settings.
# Only the first ten "MENU=" entries will be read. Each line will be
# truncated at one hundred characters
#MENU= -l -23.55 -L -46.61 -z -3      # parents in Sao Paolo
#MENU= -l 32 -L 34 -z 2               # son in bnei brak
#MENU= -fbc -l 43.71 -L -79.43 -z -5  # me in Toronto
#MENU= -l 22.26 -L 114.15 -z 8        # supplier in Hong Kong
 # configuration file for hdate - Hebrew date information program
# part of package libhdate
#
# Should you mangle this file and wish to restore its default content,
# rename or delete this file and run hdate; hdate will automatically
# regenerate the default content.
#
# Your system administrator can set system-wide defaults for hcal by
# modifying file <not yet implemented>.
# You may override all defaults by changing the contents of this file.
#
# Version information
# This may be used by updates to hcal to determine how to parse the file
# and whether additional information and options should be appended to
# the end of this file.
VERSION=2.00
#
# Location awareness
# hdate wants to accurately highlight the current Hebrew day, including
# during the hours between sunset and secular midnight. If you don't
# provide it with latitude, longitude, and time zone information, hdate
# will try to guess the information, based upon your system timezone,
# and its (limited, and maybe biased) of the dominant Jewish community
# in that timezone. When hdate is forced to guess, it alerts the user
# with a message that includes the guessed location.
# hdate's guesses will also affect its default behaviour for output of
# Shabbat times, parshiot, and choice of Israel/diaspora hoidays.
#SUNSET_AWARE=TRUE
# LATITUDE and LONGITUDE may be in decimal format or in the form
# degrees[:minutes[:seconds]] with the characters :'" as possible
# delimiters. Use negative values to indicate South and West, or
# use the abbreviated compass directions N, S, E, W.
#LATITUDE=
#LONGITUDE=
# TIMEZONE may may be in decimal format or in the form degrees[:minutes]
# with the characters :'" as possible delimiters.
#TIMEZONE=

# Output in hebrew characters
# hdate defaults to output all information in your default language, so
# if your default language is Hebrew, you're all set. Otherwise, you can
# set FORCE_HEBREW to true to output Hebrew information in Hebrew, while
# still outputting gregorian information in your default language. To
# output ALL information in Hebrew, run something like this:
#    LC_TEMP=LC_ALL; LC_ALL="he_IL.UTF-8"; hdate; LC_ALL=$LC_TEMP
# If setting FORCE_HEBREW to true results in 'garbage' or non-Hebrew
# output, you need to install a terminal font that includes the Hebrew
# character set (hint: unicode).
#FORCE_HEBREW=FALSE

# The FORCE_HEBREW option outputs data that is 'correct' and 'logical'.
# Unfortunately, the world can not be depended upon to be either. Most
# Xwindow applications will display the data fine with FORCE_HEBREW; most
# xterm implementations will not. (in fact, the only xterm clone I know
# of that comes close is mlterm). If using FORCE_HEBREW results in
# Hebrew characters being displayed in reverse, set OUTPUT_BIDI to true.
# This will reverse the order of the Hebrew characters, so they will
# display 'visual'ly correct; however, such output will not be suitable
# for piping or pasting to many other applications. Setting OUTPUT_BIDI
# automatically forces hebrew.
#OUTPUT_BIDI=FALSE

# The Hebrew language output of Hebrew information can also be 'styled'
# in the following ways:
# option YOM ~> yom shishi, aleph tishrei ...
# option LESHABBAT ~> yom sheni leshabbat miketz, kof kislev ...
# option LESEDER ~> yom sheni leseder miketz, kof kislev ...
#YOM=FALSE
#LESHABBAT=FALSE
#LESEDER=FALSE

#SUN_RISE_SET=FALSE
#TIMES_OF_DAY=FALSE
#SHORT_FORMAT=FALSE
#SEFIRAT_HAOMER=FALSE
#DIASPORA=FALSE


# Shabbat related
# Setting SHABBAT_INFO true will output parshiot and Shabbat times.
# The command line options for these features are -r (--parasha), and
# -c. The CANDLE_LIGHTING field can accept a value of 18 - 90 (minutes
# before sunset). The HAVDALAH field can accept a value of 20 - 90
# (minutes after sunset).
#PARASHA_NAMES=FALSE
#ONLY_IF_PARASHA_IS_READ=FALSE
#SHABBAT_INFO=FALSE

#CANDLE_LIGHTING=FALSE
#HAVDALAH=FALSE

# Holiday related
#HOLIDAYS=FALSE
#ONLY_IF_HOLIDAY=FALSE

# Tabular output
# This option has hdate output the information you request in a single
# comma-delimited line per day, suitable for piping or import to
# spreadsheet formatting applications, etc. To belabor the obvious,
# try running -   ./hdate 12 2011 -Rt --table |column -s, -t 
# The command line option for this feature is, um, --table
#TABULAR=FALSE

# iCal format
# hdate can output its information in iCal-compatable format
# ICAL=FALSE
# Suppress alerts and warnings
# hdate alerts the user via STDERR when it guesses the user's location.
#QUIET_ALERTS=FALSE

# Julian day number
# The Julian day number is a .... See ... for more details.
# setting the option JULIAN_DAY will have hdate output that number in
# the format JDN-nnnnnnn at the beginning of its first line of output.
#JULIAN_DAY=FALSE

# User-defined menus
# You may specify here command-line strings to optionally be parsed
# by hcal at execution time. To do so, use the command line option -m
# (--menu). hcal will process first the settings of this config file,
# then the other settings of your command line, and then will prompt
# you for which menu item you would like to select. hcal will process
# your menu selection as if it were a new command line, further modifying
# all the prior settings.
# Only the first ten "MENU=" entries will be read. Each line will be
# truncated at one hundred characters
#MENU= -bd -l -23.55 -L -46.61 -z -3 # parents in Sao Paolo
#MENU= -b -l 32 -L 34 -z 2           # son in bnei brak
#MENU= -bd -l 43.71 -L -79.43 -z -5  # me in Toronto
#MENU= -bd -l 22.26 -L 114.15 -z 8   # supplier in Hong Kong
 ALERT: -m (--menu) option specified, but no menu items in config file ALERT: The information displayed is for today's Hebrew date.
              Because it is now after sunset, that means the data is
              for the Gregorian day beginning at midnight. ALERT: guessing... will use co-ordinates for ALERT: options --parasha, --shabbat, --footnote are not supported in 'three-month' mode ALERT: time zone not entered, using system local time zone Achrei Mot Achrei Mot-Kedoshim Adar Adar I Adar II Apr April Asara B'Tevet Aug August Av Balak Bamidbar Bechukotai Beha'alotcha Behar Behar-Bechukotai Beijing Bereshit Beshalach Bo Buenos Aires Chanukah Chayei Sara Cheshvan Chukat Chukat-Balak Dec December Devarim Eikev Elul Emor Erev Shavuot Erev Yom Kippur Family Day Feb February Fri Friday Gregorian date Ha'Azinu Hebrew Date Hebrew calendar
OPTIONS:
   -1 --one-month     over-ride config file setting if you had set
                      option --three-month as a default there
   -3 --three-month   displays previous/next months
                      side by side. requires 127 columns
   -b --bidi          prints hebrew in reverse (visual)
      --visual
      --no-bidi       over-ride config file setting if you had set
      --no-visual     option -bidi as a default there
   -c --colorize      displays in calming, muted tones
      --no-color      over-ride a --colorize setting in your config file
   -d --diaspora      use diaspora reading and holidays.
   -f --footnote      print descriptive notes of holidays
      --no-footnote   over-ride a footnote setting in your config file
   -h --html          output in html format to stdout
   -H --hebrew        print hebrew information in Hebrew
   -i                 use external css file "./hcal.css".
   -I --israel        override a diaspora default
      --no-reverse    do not highlight today's date
   -m --menu          prompt user-defined menu from config file
   -p --parasha       print week's parasha on each calendar row
   -q --quiet-alerts  suppress warning messages
   -s --shabbat       print Shabbat times and parshiot
      --candles       modify default minhag of 20 minutes. (17<n<91)
      --havdalah      modify default minhag of 3 stars. (19<n<91 minutes)
   -z --timezone nn   timezone, +/-UTC
   -l --latitude yy   latitude yy degrees. Negative values are South
   -L --longitude xx  longitude xx degrees. Negative values are West

All options can be made default in the config file, or menu-ized for
easy selection. Hmmm, ... hate to do this, really ... Hol hamoed Pesach Hol hamoed Sukkot Honolulu Hoshana raba Iyyar Jan January Jul July Jun June Kedoshim Ki Tavo Ki Teitzei Ki Tisa Kislev Korach L (Longitude) L (Longitue) Lag B'Omer Lech-Lecha London Los Angeles Mar March Masei Matot Matot-Masei May Memorial day for fallen whose place of burial is unknown Metzora Mexico City Miketz Mishpatim Mon Monday Moscow Nasso New York City Nisan Nitzavim Nitzavim-Vayeilech Noach Nov November Oct October Parashat Paris Pekudei Pesach Pesach II Pesach VII Pesach VIII Pinchas Purim Rabin memorial day Re'eh Rosh Hashana I Rosh Hashana II Sat Saturday Sep September Sh'lach Sh'vat Shavuot Shavuot II Shemot Shmini Shmini Atzeret Shoftim Shushan Purim Simchat Torah Sivan Sukkot Sukkot II Sun Sunday Ta'anit Esther Tammuz Tashkent Tazria Tazria-Metzora Tel-Aviv Terumah Tetzaveh Tevet This seems to be to be your first time using this version.
Please read the new documentation in the man page and config
file. Attempting to create a config file ... Thu Thursday Tish'a B'Av Tishrei Toldot Tu B'Av Tu B'Shvat Tue Tuesday Tzav Tzom Gedaliah Tzom Tammuz Usage: hcal [options] [coordinates timezone] ] [[month] year]
       coordinates: -l [NS]yy[.xxx] -L [EW]xx[.xxx]
                    -l [NS]yy[:mm[:ss]] -L [EW]xx[:mm[:ss]]
       timezone:    -z nn[( .nn | :mm )]
Try 'hcal --help' for more information Usage: hdate [options] [coordinates timezone] [[[day] month] year]
       hdate [options] [coordinates timezone] [julian_day]

       coordinates: -l [NS]yy[.yyy] -L [EW]xx[.xxx]
                    -l [NS]yy[:mm[:ss]] -L [EW]xx[:mm[:ss]]
       timezone:    -z nn[( .nn | :mm )]
Try 'hdate --help' for more information Vaera Vaetchanan Vayakhel Vayakhel-Pekudei Vayechi Vayeilech Vayera Vayeshev Vayetzei Vayigash Vayikra Vayishlach Vezot Habracha Wed Wednesday Yitro Yitzhak Rabin memorial day Yom HaAtzma'ut Yom HaShoah Yom HaZikaron Yom Kippur Yom Yerushalayim Zeev Zhabotinsky day Zhabotinsky day candle-lighting config file created day degrees enter your selection, or <return> to continue error eve of  failure attempting to create config file failure closing first_light first_stars havdalah hcal - Hebrew calendar
version 1.6 hdate - display Hebrew date information
OPTIONS:
   -b --bidi          prints hebrew in reverse (visual)
      --visual
   -c                 print Shabbat start/end times.
      --candles       modify default minhag of 20 minutes. (17<n<91)
      --havdalah      modify default minhag of 3 stars. (19<n<91 minutes)
   -d --diaspora      use diaspora reading and holidays.
   -h --holidays      print holidays.
   -H                 print only it is a holiday.
   -i --ical          use iCal formated output.
   -j --julian        print Julian day number.
   -m --menu          prompt user-defined menu from config file
   -o --omer          print Sefirat Haomer.
   -q --quiet-alerts  suppress warning messages
   -r --parasha       print weekly reading on saturday.
   -R                 print only if there is a weekly reading on Shabbat.
   -s --sun           print sunrise/sunset times.
   -S --short-format  print using short format.
   -t                 print day times: first light, talit, sunrise,
                            midday, sunset, first stars, three stars.
   -T --table         tabular output, suitable for spreadsheets

   -z --timezone nn   timezone, +/-UTC
   -l --latitude yy   latitude yy degrees. Negative values are South
   -L --longitude xx  longitude xx degrees. Negative values are West

   --hebrew           forces Hebrew to print in Hebrew characters
   --yom              force Hebrew prefix to Hebrew day of week
   --leshabbat        insert parasha between day of week and day
   --leseder          insert parasha between day of week and day
   --not-sunset-aware don't display next day if after sunset

All options can be made default in the config file, or menu-ized for
easy selection. hdate - display Hebrew date information
version 1.6 holiday in the Omer is incompatible with a longitude of is non-numeric or out of bounds is not a valid option l (latitude) memory allocation failure menu selection received was out of bounds midday missing parameter month none omer count option parameter parashat sun_hour sunrise sunset talit three_stars time zone value of today is day too many arguments; after options max is dd mm yyyy too many parameters received. expected  [[mm] [yyyy] translator using co-ordinates for the equator, at the center of time zone valid latitude parameter missing for given longitude valid longitude parameter missing for given latitude year your custom menu options (from your config file) z (time zone) z (timezone) Project-Id-Version: libhdate
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2012-01-08 19:48-0500
PO-Revision-Date: 2014-02-15 11:15+0000
Last-Translator: Gianfranco Frisani <gfrisani@libero.it>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 # File di configurazione per hcal - Programma del calendario ebraico
# parte del pacchetto libhdate
#
# Nel caso questo file risultasse danneggiato questo file e dovesse essere necessario
# ripristinare il suo contenuto predefinito, rinominare o eliminare questo file ed eseguire
# hcal, che rigenererà automaticamente il contenuto predefinito.
#
# L'amministratore di sistema può effettuare impostazioni predefinite di sistema
# per hcal modificando il file <not yet implemented>.
# È possibile sovrascrivere tutte le impostazioni predefinite modificando il contenuto di questo file.
#
# Informazioni sulla versione
# Possono essere usate dagli aggiornamenti a hcal per determinare come analizzare il file
# e se delle informazioni e opzioni supplementari debbano essere aggiunte
# al termine di questo file.
VERSIONE=2.00
# Conoscenza della posizione
# hcal evidenzia con accuratezza il giorno ebraico corrente, anche durante
# le ore fra il tramonto e la mezzanotte laica. Se non vengono fornite
# le informazioni su latitudine, longitudine e fuso orario, hcal
# cerca di stimare le informazioni, in base al fuso orario del sistema,
# e alle informazioni (limitate e forse non obiettive) della comunità ebraica
# dominante in quel fuso orario. Quando hcal è costretto ad effettuare una
#  stima, ne dà notizia all'utente con un messaggio che include la posizione stimata.
# Le stime di hcal influenzano anche il suo comportamento predefinito per l'output
# dei periodi di Shabbat, parshiot e per la scelta delle festività di Israele o della diaspora
#SUNSET_AWARE=TRUE
# LATITUDE e LONGITUDE possono essere in formato decimale o nel modulo
# gradi[:minuti[:secondi]] con i caratteri :'" come possibili
# delimitatori. Usare valori negativi per indicare il Sud e l'Ovest o
# usare le direzioni abbreviate della bussola N, S, E, W.
#LATITUDE=
#LONGITUDE=
# TIMEZONE può essere in formato decimale o nel modulo gradi[:minuti]
# con i caratteri :'" come possibili delimitatori.
#TIMEZONE=

# Israele e la diaspora
# Se hcal stima che l'utente non sia in Israele, l'opzione DIASPORA verrà
# impostata su «vero». Questo influenza l'output di festività e parasha. FORCE_ISRAEL
# costringe hcal a visualizzare le informazioni del calendario per Israele, usando le
# coordinate predefinite di hcal per Israele o le coordinate fornite che
# sembrino legittimamente essere entro Israele.
# In questo modo, se l'utente vive a Sao Paolo e visiterà Israel per il
# Sukkot, imposterà ENTRAMBI i contrassegni su «vero» per visualizzare le informazioni sulle
# festività per un visitatore non residente in Israele. Le opzioni da riga di comando per queste
# caratteristiche sono --israel, -I, --diaspora, -d.
#FORCE_DIASPORA=FALSE;
#FORCE_ISRAEL=FALSE;

# In relazione allo Shabbat
# Impostando SHABBAT_INFO su «vero» verrà fornito un output sui periodi di parshiot e Shabbat.
# Le opzioni da riga di comando per queste caratteristiche sono -p (--parasha) e
# -s (--shabbat). Il campo CANDLE_LIGHTING accetta un valore di 18 - 90 (minuti
# prima del tramonto). Il campo HAVDALAH accetta un valore di 20 - 90
# (minuti dopo il tramonto).
#PARASHA_NAMES=FALSE
#SHABBAT_INFO=FALSE
#CANDLE_LIGHTING=FALSE
#HAVDALAH=FALSE

# Identificazione delle festività
# hcal contrassegna le festività sostituendo il separatore di data gregoriano o ebraico
# con un assortimento di inutili simboli criptici. Impostando FOOTNOTES su «vero»
# si otterrà che hcal fornisca dopo il calendario del mese un elenco delle festività
# del mese vicino ai giorni in cui queste hanno luogo.
#FOOTNOTE=FALSE

# Output in caratteri ebraici
# per impostazione predefinita hcal fornisce tutte le informazioni nel linguaggio predefinito
# dell'utente e quindi se la lingua predefinita è l'ebraico, è tutto a posto. In caso contrario, è
# possibile impostare FORCE_HEBREW su «vero» per ottenere le informazioni ebraiche in ebraico,
# mentre le informazioni gregoriane verranno sempre fornite nella lingua predefinita dell'utente.
# Per ottenere TUTTE le informazioni in ebraico, eseguire qualcosa del genere:
# LC_TEMP=LC_ALL; LC_ALL="he_IL.UTF-8"; hcal; LC_ALL=$LC_TEMP
# Se l'impostazione di FORCE_HEBREW su «vero» dà come risultato «spazzatura» o un output in lingua
# diversa dall'ebraico, è necessario installare un font per terminale che comprenda l'insieme
# di caratteri ebraico (suggerimento: unicode).
# L'opzione da riga di comando per FORCE_HEBREW è sia --hebrew che -H
#FORCE_HEBREW=FALSE

# L’opzione FORCE_HEBREW fornisce dati “corretti” e “logici”.
# Sfortunatamente, non si può fare affidamento sul fatto che anche il mondo sia tale. La maggior
# parte delle applicazioni Xwindow visualizzeranno correttamente i dati con FORCE_HEBREW; la
# maggior parte delle implementazioni xterm non lo faranno (infatti, l’unico clone conosciuto 
# xterm che ci si avvicina è mlterm). Se usando FORCE_HEBREW si ottengono caratteri ebraici
# visualizzati in senso inverso, impostare OUTPUT_BIDI su «vero».
# Ciò inverte l’ordine dei caratteri ebraici, in modo tale da mostrarli in
# modo “visivamente” corretto; tuttavia questo output non sarà adatto per
# il piping o l’unione in molte altre applicazioni. L’impostazione di OUTPUT_BIDI
# forza automaticamente l’ebraico.
# L’opzione da riga di comando per OUTPUT_BIDI è sia --bidi, --visual o -b
#OUTPUT_BIDI=FALSE

# Miglioramento della visualizzazione
# le impostazioni predefinite di hcal visualizzano il giorno corrente in un video al rovescio.
# L’opzione da riga di comando per questa caratteristica è --no-reverse
#SUPPRESS_REVERSE_VIDEO=FALSE;
# hcal può visualizzare Il suo output in “tonalità tranquille, rasserenanti”. Notare che un output
# con decorazioni a colori può produrre risultati inattesi.
#COLORIZE=FALSE

# HTML OUTPUT
#OUTPUT_HTML=FALSE
#USE_EXTERNAL_CSS_FILE="pathto/foo/bar"

# Eliminare allarmi e avvertimenti
# hcal avverte l’utente per mezzo di STDERR quando stima la posizione dell’utente.
#QUIET_ALERTS=FALSE

# Visualizzazione in base a tre mesi
# Hcal può visualizzare il mese precedente, quello corrente e successivo affiancati. Hcal
# può anche visualizzare il calendario di un intero anno in quattro righe di tre
# mesi ciascuna. Notare che ciò verrà correttamente visualizzato solo se la console
# è impostata per almeno 127 colonne. Notare anche che questa opzione imposta
# le opzioni FOOTNOTES, SHABBAT e PARASHA_NAMES su «falso».
#THREE_MONTH=FALSE

# Menù definiti dall’utente
# È possibile specificare qui stringhe da riga di comando che saranno analizzate in modo
# facoltativo da parte di hcal durante l’esecuzione. Per ottenere questo risultato, usare
# l’opzione da riga di comando -m (--menu). Hcal eseguirà prima le impostazioni del file
# di configurazione, quindi le altre impostazioni della riga di comando dell’utente e 
# richiederà all’utente quale voce del menu selezionare. Hcal eseguirà la selezione
# del menu come una nuova riga di comando, modificando ulteriormente tutte le 
# impostazioni precedenti.
# Vengono lette solo le prime dieci voci "MENU=". Ciascuna riga verrà
# troncata dopo cento caratteri
#MENU= -l -23.55 -L -46.61 -z -3 # genitori in Sao Paolo
#MENU= -l 32 -L 34 -z 2 # figlio in bnei brak
#MENU= -fbc -l 43.71 -L -79.43 -z -5 # utente in Toronto
#MENU= -l 22.26 -L 114.15 -z 8 # fornitore in Hong Kong
 # File di configurazione per hdate - Programma di informazione su date ebraiche
# parte del pacchetto libhdate
#
# Nel caso questo file risultasse danneggiato questo file e dovesse essere necessario
# ripristinare il suo contenuto predefinito, rinominare o eliminare questo file ed eseguire
# hdate, che rigenererà automaticamente il contenuto predefinito.
#
# L'amministratore di sistema può effettuare impostazioni predefinite di sistema
# per hcal modificando il file <not yet implemented>.
# È possibile sovrascrivere tutte le impostazioni predefinite modificando il contenuto di questo file.
#
# Informazioni sulla versione
# Possono essere usate dagli aggiornamenti a hcal per determinare come analizzare il file
# e se delle informazioni e opzioni supplementari debbano essere aggiunte
# al termine di questo file.
VERSIONE=2.00
# Conoscenza della posizione
# hdate evidenzia con accuratezza il giorno ebraico corrente, anche durante
# le ore fra il tramonto e la mezzanotte laica. Se non vengono fornite
# le informazioni su latitudine, longitudine e fuso orario, hdate
# cerca di stimare le informazioni, in base al fuso orario del sistema,
# e alle informazioni (limitate e forse non obiettive) della comunità ebraica
# dominante in quel fuso orario. Quando hdate è costretto ad effettuare una
#  stima, ne dà notizia all'utente con un messaggio che include la posizione stimata.
# Le stime di hdate influenzano anche il suo comportamento predefinito per l'output
# dei periodi di Shabbat, parshiot e per la scelta delle festività di Israele o della diaspora
#SUNSET_AWARE=TRUE
# LATITUDE e LONGITUDE possono essere in formato decimale o nel modulo
# gradi[:minuti[:secondi]] con i caratteri :'" come possibili
# delimitatori. Usare valori negativi per indicare il Sud e l'Ovest o
# usare le direzioni abbreviate della bussola N, S, E, W.
#LATITUDE=
#LONGITUDE=
# TIMEZONE può essere in formato decimale o nel modulo gradi[:minuti]
# con i caratteri :'" come possibili delimitatori.
#TIMEZONE=

# Output in caratteri ebraici
# per impostazione predefinita hdate fornisce tutte le informazioni nel linguaggio predefinito
# dell'utente e quindi se la lingua predefinita è l'ebraico, è tutto a posto. In caso contrario, è
# possibile impostare FORCE_HEBREW su «vero» per ottenere le informazioni ebraiche in ebraico,
# mentre le informazioni gregoriane verranno sempre fornite nella lingua predefinita dell'utente.
# Per ottenere TUTTE le informazioni in ebraico, eseguire qualcosa del genere:
# LC_TEMP=LC_ALL; LC_ALL="he_IL.UTF-8"; hdate; LC_ALL=$LC_TEMP
# Se l'impostazione di FORCE_HEBREW su «vero» dà come risultato «spazzatura» o un output in lingua
# diversa dall'ebraico, è necessario installare un font per terminale che comprenda l'insieme
# di caratteri ebraico (suggerimento: unicode).
# L'opzione da riga di comando per FORCE_HEBREW è sia --hebrew che -H
#FORCE_HEBREW=FALSE

# L’opzione FORCE_HEBREW fornisce dati “corretti” e “logici”.
# Sfortunatamente, non si può fare affidamento sul fatto che anche il mondo sia tale. La maggior
# parte delle applicazioni Xwindow visualizzeranno correttamente i dati con FORCE_HEBREW; la
# maggior parte delle implementazioni xterm non lo faranno (infatti, l’unico clone conosciuto 
# xterm che ci si avvicina è mlterm). Se usando FORCE_HEBREW si ottengono caratteri ebraici
# visualizzati in senso inverso, impostare OUTPUT_BIDI su «vero».
# Ciò inverte l’ordine dei caratteri ebraici, in modo tale da mostrarli in
# modo “visivamente” corretto; tuttavia questo output non sarà adatto per
# il piping o l’unione in molte altre applicazioni. L’impostazione di OUTPUT_BIDI
# forza automaticamente l’ebraico.
#OUTPUT_BIDI=FALSE

# L’output in lingua ebraica delle informazioni ebraiche può anche essere “stilizzato”
# nei modi seguenti:
# opzione YOM ~> yom shishi, aleph tishrei ... 
# opzione LESHABBAT ~> yom sheni leshabbat miketz, kof kislev ... 
# opzione LESEDER ~> yom sheni leseder miketz, kof kislev ... 
#YOM=FALSE
#LESHABBAT=FALSE
#LESEDER=FALSE

#SUN_RISE_SET=FALSE
#TIMES_OF_DAY=FALSE
#SHORT_FORMAT=FALSE
#SEFIRAT_HAOMER=FALSE
#DIASPORA=FALSE


# In relazione allo Shabbat
# Impostando SHABBAT_INFO su «vero» verrà fornito un output sui periodi di parshiot e Shabbat.
# Le opzioni da riga di comando per queste caratteristiche sono -p (--parasha) e
# -s (--shabbat). Il campo CANDLE_LIGHTING accetta un valore di 18 - 90 (minuti
# prima del tramonto). Il campo HAVDALAH accetta un valore di 20 - 90
# (minuti dopo il tramonto).
#PARASHA_NAMES=FALSE
#ONLY_IF_PARASHA_IS_READ=FALSE 
#SHABBAT_INFO=FALSE

#CANDLE_LIGHTING=FALSE
#HAVDALAH=FALSE

# In relazione alle festività 
#HOLIDAYS=FALSE
#ONLY_IF_HOLIDAY=FALSE

# Output sotto forma di tabella 
# Questa opzione fornisce le informazioni di hdate richieste in un’unica riga
# separate da virgole per ogni giorno, adatte per il piping o l’importazione in 
# un foglio di calcolo o in applicazioni di formattazione, ecc. Per accanirsi sulle
# cose ovvie, provare a eseguire - ./hdate 12 2011 -Rt --table |column -s, -t 
# L’opzione da riga di comando per questa opzione è --table
#TABULAR=FALSE

# Formato iCal
# hdate può fornire le informazioni in formato compatibile con iCal 
# ICAL=FALSE
# Eliminare allarmi e avvertimenti
# hcal avverte l’utente per mezzo di STDERR quando stima la posizione dell’utente.
#QUIET_ALERTS=FALSE
 
# Numero dei giorni Giuliani 
# Il numero dei giorni Giuliani è un …. Consultare … per ulteriori dettagli. 
# Impostare l’opzione JULIAN_DAY per ottenere un output di hdate nel formato 
# numeric JDN-nnnnnnn all’inizio della prima riga di output. 
#JULIAN_DAY=FALSE

# Menù definiti dall’utente
# È possibile specificare qui stringhe da riga di comando che saranno analizzate in modo
# facoltativo da parte di hcal durante l’esecuzione. Per ottenere questo risultato, usare
# l’opzione da riga di comando -m (--menu). Hcal eseguirà prima le impostazioni del file
# di configurazione, quindi le altre impostazioni della riga di comando dell’utente e 
# richiederà all’utente quale voce del menu selezionare. Hcal eseguirà la selezione
# del menu come una nuova riga di comando, modificando ulteriormente tutte le 
# impostazioni precedenti.
# Vengono lette solo le prime dieci voci "MENU=". Ciascuna riga verrà
# troncata dopo cento caratteri
#MENU= -l -23.55 -L -46.61 -z -3 # genitori in Sao Paolo
#MENU= -l 32 -L 34 -z 2 # figlio in bnei brak
#MENU= -fbc -l 43.71 -L -79.43 -z -5 # utente in Toronto
#MENU= -l 22.26 -L 114.15 -z 8 # fornitore in Hong Kong
 ATTENZIONE: specificata l'opzione -m (--menu), ma non esiste alcuna voce di menù nel file di configurazione ATTENZIONE: L'informazione visualizzata è relativa alla data ebraica odierna.
              Considerato che il tramonto è trascorso, ciò significa che la data si riferisce
              al giorno Gregoriano che inizia a mezzanotte. ATTENZIONE: stima ... verranno usate coordinate per ATTENZIONE: le opzioni --parasha, --shabbat, --footnote non sono supportate nella modalità "tre mesi" ATTENZIONE: fuso orario non immesso, viene usato il fuso orario locale Achrei Mot Achrei Mot-Kedoshim Adar Adar I Adar II Apr Aprile Asara B'Tevet Ago Agosto Av Balak Bamidbar Bechukotai Beha'alotcha Behar Behar-Bechukotai Pechino Bereshit Beshalach Bo Buenos Aires Chanukah Chayei Sara Cheshvan Chukat Chukat-Balak Dic Dicembre Devarim Eikev Elul Emor Erev Shavuot Erev Yom Kippur Giorno della famiglia Feb Febbraio Ven Venerdì Data gregoriana Ha'Azinu Data ebraica Calendario ebraico
OPZIONI:
   -1 --one-month sovrascrive le impostazioni del il file di configurazione nel caso
                      sia stata impostata come predefinita l'opzione --three-month
   -3 --three-month visualizza il mese precedente e quello successivo
                      affiancati; richiede 127 colonne
   -b --bidi stampa l'ebraico in senso inverso (visual)
      --visual
      --no-bidi sovrascrive le impostazioni del file di configurazione nel caso
      sia stata impostata come predefinita l'opzione --no-visual -bidi
   -c --colorize visualizzazione in tonalità tranquille, rasserenanti
      --no-color sovrascrive un'impostazione --colorize nel file di configurazione
   -d --diaspora usa la lettura della diaspora e delle festività.
   -f --footnote visualizza note descrittive delle festività.
      --no-footnote sovrascrive un'impostazione a piè di pagina nel file di configurazione
   -h --html output in formato html verso stdout
   -H --hebrew visualizza informazioni ebraiche in lingua ebraica
   -i usa il file css esterno "./hcal.css".
   -I --israel sovrascrive un'opzione predefinita diaspora
      --no-reverse non evidenzia la data di oggi
   -m --menu avvia un menù definito dall'utente nel file di configurazione
   -p --parasha mostra la parasha della settimana su ciascuna riga del calendario
   -q --quiet-alerts sopprime i messaggi d'allarme
   -s --shabbat mostra i periodi di Shabbat e le parasha
      --candles modifica i minhag predefiniti di 20 minuti (17<n<91).
      --havdalah modifica i minhag predefiniti del periodo delle 3 stelle (19<n<91 minutes).
   -z --timezone numero fusi orari, +/-UTC
   -l --latitude yy: yy gradi di latitudine; valori negativi indicano l'emisfero meridionale
   -L --longitude xx: xx gradi di longitudine; valori negativi indicano posizioni a Ovest di Greenwich

Tutte le opzioni possono essere predefinite nel file di configurazione o incluse
in un menù per una facile selezione.. Hmmm, ... odio far questo, veramente ... Hol hamoed Pesach Hol hamoed Sukkot Honolulu Hoshana raba Iyyar Gen Gennaio Lug Luglio Giu Giugno Kedoshim Ki Tavo Ki Teitzei Ki Tisa Kislev Korach L (Longitudine) L (Longitdine) Lag B'Omer Lech-Lecha Londra Los Angeles Mar Marzo Masei Matot Matot-Masei Maggio Giorno della memoria per i caduti il cui luogo di sepoltura è sconosciuto Metzora Città del Messico Miketz Mishpatim Lun Lunedì Mosca Nasso New York City Nisan Nitzavim Nitzavim-Vayeilech Noach Nov Novembre Ott Ottobre Parashat Parigi Pekudei Pesach Pesach II Pesach VII Pesach VIII Pinchas Purim Giorno della memoria per Rabin Re'eh Rosh Hashana I Rosh Hashana II Sab Sabato Set Settembre Sh'lach Sh'vat Shavuot Shavuot II Shemot Shmini Shmini Atzeret Shoftim Shushan Purim Simchat Torah Sivan Sukkot Sukkot II Dom Domenica Ta'anit Esther Tammuz Tashkent Tazria Tazria-Metzora Tel-Aviv Terumah Tetzaveh Tevet Sembra che sia a prima volta che si stia usando questa versione.
Leggere la nuova documentazione nella pagina del manuale e il file
di configurazione. Tentativo di creazione di un file di configurazione ... Gio Giovedì Tish'a B'Av Tishrei Toldot Tu B'Av Tu B'Shvat Mar Martedì Tzav Tzom Gedaliah Tzom Tammuz Uso: hcal [opzioni] [coordinate fuso orario] ] [[mese] anno]
       coordinate: -l [NS]yy[.xxx] -L [EW]xx[.xxx]
                    -l [NS]yy[:mm[:ss]] -L [EW]xx[:mm[:ss]]
       fuso orario: -z nn[( .nn | :mm )]
Consultare "hcal --help" per ulteriori informazioni Uso: hdate [opzioni] [coordinate fuso orario] [[[giorno] mese] anno]
       hdate [opzioni] [coordinate fuso orario] [giorno giuliano]

       coordinate: -l [NS]yy[.yyy] -L [EW]xx[.xxx]
                    -l [NS]yy[:mm[:ss]] -L [EW]xx[:mm[:ss]]
       fuso orario: -z nn[( .nn | :mm )]
Consultare "hdate --help" per maggiori informazioni Vaera Vaetchanan Vayakhel Vayakhel-Pekudei Vayechi Vayeilech Vayera Vayeshev Vayetzei Vayigash Vayikra Vayishlach Vezot Habracha Mer Mercoledì Yitro Giorno della memoria per Yitzhak Rabin Yom HaAtzma'ut Yom HaShoah Yom HaZikaron Yom Kippur Yom Yerushalayim Giorno della memoria per Zeev Zhabotinsky Giorno della memoria per Zhabotinsky Accensione_della_candela File di configurazione creato Giorno Gradi Inserire la selezione, o premere «Invio» per continuare Errore Vigilia di  Tentativo di creazione del file di configurazione non riuscito Arresto a causa di errore Prima luce Prime_stelle Havdalah hcal - calendario ebraico
versione 1.6 hdate - visualizza informazioni sulla data ebraica
OPZIONI:
   -b --bidi stampa l'ebraico in senso inverso (visual) 
      --visual 
   -c stampa l'inizio e il termine dei periodi di Shabbat.
     --candles modifica i minhag predefiniti di 20 minuti (17<n<91). 
      --havdalah modifica i minhag predefiniti del periodo delle 3 stelle (19<n<91 minutes). 
   -d --diaspora usa la lettura della diaspora e delle festività.
   -h --holidays stampa le festività.
   -H indica solo che è una festività.
   -i --ical usa un output in formato iCal.
   -j --julian stampa il numero del giorno Giuliano.
  -m --menu avvia un menù definito dall'utente nel file di configurazione
   -o --omer stampa Sefirat Haomer.
  -q --quiet-alerts sopprime i messaggi d'allarme
   -r --parasha stampa la lettura settimanale al sabato.
   -R indica solo che esiste una lettura settimanale allo Shabbat.
   -s --sun indica gli orari dell'alba e del tramonto.
   -S --short-format stampa usando un formato abbreviato.
   -t indica gli orari del giorno: prima luce, talit, alba,
                            mezzogiorno, tramonto, prime stelle, tre stelle.
   -T --table output in forma tabellare, adatto a fogli di calcolo

   -z --timezone nn fuso orario, +/-UTC
   -l --latitude yy: yy gradi di latitudine; valori negativi indicano l'emisfero meridionale 
   -L --longitude xx: xx gradi di longitudine; valori negativi indicano posizioni a Ovest di Greenwich

   --hebrew forza l'ebraico a stampare in caratteri ebraici
   --yom forza il prefisso ebraico nel giorno della settimana ebraico
   --leshabbat inserisce la parasha fra il giorno della settimana e il giorno
   --leseder inserisce la parasha fra il giorno della settimana e il giorno
   --not-sunset-aware non visualizza il giorno successivo se il tramonto è già trascorso

Tutte le opzioni possono essere rese predefinite nel file di configurazione
o inserite in un menù, una facile selezione. Hdate - visualizza informazioni sulle date ebraiche
versione 1.6 Festività Durante l'Omer è incompatibile con una longitudine di Non è un numero o è oltre i limiti cosentiti Non è un'opzione valida l (latitudine) Errore nell'allocazione della memoria La selezione del menù ricevuta è al di fuori dei limiti Mezzogiorno Parametro mancante Mese Nessuno Conteggio dell'Omer Opzione Parametro Parashat Ora_solare Alba Tramonto Talit Tre_stelle Un valore del fuso orario di Oggi è il giorno Troppi argomenti; dopo l'opzione il massimo è gg mm aaaa Ricevuti troppi parametri: previsti [[mm] [yyyy] Traduttore Uso di coordinate per l'equatore, al centro del fuso orario Per la longitudine data manca un parametro valido per la latitudine Per la latitudine data manca un parametro valido per la longitudine Anno Opzioni del menù personalizzato dell'utente (dal file di configurazione) Z (fuso orario) Z (fuso orario) 