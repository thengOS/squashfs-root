��             +         �     �     �     �  "   �               '     +     0     9  (   >     g     m     t     �     �     �     �     �     �     �               7  /   H     x     �  )   �     �     �     �  �       �     �  %   �  +        C     I     [     _     d     m  0   r     �     �      �     �     �     �     �  &   �  =   &      d     �     �     �  5   �  %   	  !   .	  .   P	     	     �	     �	                                                                 	                 
                                                              ARG DOUBLE Display brief usage message Display option defaults in message FLOAT Help options: INT LONG LONGLONG NONE Options implemented via popt alias/exec: SHORT STRING Show this help message Terminate options Usage: VAL [OPTION...] aliases nested too deeply config file failed sanity test error in parameter quoting invalid numeric value memory allocation failed missing argument mutually exclusive logical operations requested number too large or too small opt->arg should not be NULL option type (%u) not implemented in popt
 unknown errno unknown error unknown option Project-Id-Version: popt-1.14
Report-Msgid-Bugs-To: <popt-devel@rpm5.org>
POT-Creation-Date: 2010-02-17 13:35-0500
PO-Revision-Date: 2012-02-20 21:25+0000
Last-Translator: Luca Petrolati <Unknown>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:04+0000
X-Generator: Launchpad (build 18115)
First-Translator: Sandro Bonazzola <sandro.bonazzola@gmail.com>
 ARG DOUBLE Mostra un breve messaggio di utilizzo Mostra le opzioni predefinite nel messaggio FLOAT Opzioni di aiuto: INT LONG LONGLONG NONE Opzioni implementate tramite alias/exec di popt: SHORT STRING Mostra questo messaggio di aiuto Opzioni di terminazione Uso: VAL [OPZIONE...] alias nidificati troppo in profondità il file di configurazione non ha passato il test d'integrità errore nel quoting del parametro valore numerico non valido allocazione di memoria fallita argomento mancante richieste operazioni logiche reciprocamente esclusive numero troppo grande o troppo piccolo opt->arg non dovrebbe essere NULL tipo di opzione (%u) non implementato in popt
 errno sconosciuto errore sconosciuto opzione sconosciuta 