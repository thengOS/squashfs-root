��          T      �       �   ^   �   &     `   ?  :   �  #   �     �  �    a   �  %   *  d   P  9   �  /   �                                             Common name for the supplied codes. This may be the same as --name (only applies to ISO 3166). Name for the supplied codes (default). Official name for the supplied codes. This may be the same as --name (only applies to ISO 3166). Separate entries with a NULL character instead of newline. Show program version and copyright. Use this locale for output. Project-Id-Version: isoquery
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-05-08 10:15+0200
PO-Revision-Date: 2014-03-22 17:00+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:32+0000
X-Generator: Launchpad (build 18115)
 No comune del codice fornito; questo può essere lo stesso di --name (si applica solo a ISO 3166) Nome del codice fornito (predefinito) No ufficiale del codice fornito; questo può essere lo stesso di --name (si applica solo a ISO 3166) Separa le voci con il carattere NULL al posto del newline Mostra la versione del programma e il copyright Usa questa lingua per l'output 