��            )   �      �     �  <   �  9   �  3   (     \  /   y     �     �     �  2   �  '   #     K     h  $   w     �  0   �  0   �          3     P     k     }  7  �  �  �  M   y     �     �     �  �  �  -   �  J   �  (     8   @  )   y  L   �     �  1   �  #   +  =   O  1   �  #   �     �  /   �     #  8   A  9   z  %   �  '   �           #     9  �  V  �  %  _   �     U      p      w          
                      	                                                                                                           -- Press any key to continue -- An Ispell program was not given in the configuration file %s Are you sure you want to throw away your changes? (y/n):  Conversion of '%s' to character set '%s' failed: %s Error initialising libvoikko Error initializing character set conversion: %s File: %s Incomplete spell checker entry Missing argument for option %s Parse error in file "%s" on line %d, column %d: %s Parse error in file "%s" on line %d: %s Parse error in file "%s": %s Replace with:  Unable to open configuration file %s Unable to open file %s Unable to open file %s for reading a dictionary. Unable to open file %s for writing a dictionary. Unable to open temporary file Unable to set encoding to %s Unable to write to file %s Unknown option %s Unterminated quoted string Usage: %s [options] [file]...
Options: [FMNLVlfsaAtnhgbxBCPmSdpwWTv]

 -F <file>  Use given file as the configuration file.

The following flags are same for ispell:
 -v[v]      Print version number and exit.
 -M         One-line mini menu at the bottom of the screen.
 -N         No mini menu at the bottom of the screen.
 -L <num>   Number of context lines.
 -V         Use "cat -v" style for characters not in the 7-bit ANSI
            character set.
 -l         Only output a list of misspelled words.
 -f <file>  Specify the output file.
 -s         Issue SIGTSTP at every end of line.
 -a         Read commands.
 -A         Read commands and enable a command to include a file.
 -e[e1234]  Expand affixes.
 -c         Compress affixes.
 -D         Dump affix tables.
 -t         The input is in TeX format.
 -n         The input is in [nt]roff format.
 -h         The input is in sgml format.
 -b         Create backup files.
 -x         Do not create backup files.
 -B         Do not allow run-together words.
 -C         Allow run-together words.
 -P         Do not generate extra root/affix combinations.
 -m         Allow root/affix combinations that are not in dictionary.
 -S         Sort the list of guesses by probable correctness.
 -d <dict>  Specify an alternate dictionary file.
 -p <file>  Specify an alternate personal dictionary.
 -w <chars> Specify additional characters that can be part of a word.
 -W <len>   Consider words shorter than this always correct.
 -T <fmt>   Assume a given formatter type for all files.
 -r <cset>  Specify the character set of the input.
 Whenever an unrecognized word is found, it is printed on
a line on the screen. If there are suggested corrections
they are listed with a number next to each one. You have
the option of replacing the word completely, or choosing
one of the suggested words. Alternatively, you can ignore
this word, ignore all its occurrences or add it in the
personal dictionary.

Commands are:
 r       Replace the misspelled word completely.
 space   Accept the word this time only.
 a       Accept the word for the rest of this session.
 i       Accept the word, and put it in your personal dictionary.
 u       Accept and add lowercase version to personal dictionary.
 0-9     Replace with one of the suggested words.
 x       Write the rest of this file, ignoring misspellings,
         and start next file.
 q       Quit immediately.  Asks for confirmation.
         Leaves file unchanged.
 ^Z      Suspend program.
 ?       Show this help screen.
 [SP] <number> R)epl A)ccept I)nsert L)ookup U)ncap Q)uit e(X)it or ? for help \ at the end of a string aiuqxr yn Project-Id-Version: tmispell-voikko
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-02-07 18:46+0200
PO-Revision-Date: 2012-03-09 16:06+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:10+0000
X-Generator: Launchpad (build 18115)
 -- Premi un tasto qualunque per continuare -- Non è stato specificato un programma Ispell nel file di configurazione %s Scartare veramente le modifiche? (s/n):  Conversione di '%s' al set di caratteri '%s' fallita: %s Errore nell'inizializzazione di libvoikko Errore durante l'inizializzazione della conversione del set di caratteri: %s File: %s Inserimento incompleto nel correttore ortografico Argomento mancante per l'opzione %s Errore di analisi nel file "%s" sulla riga %d, colonna %d: %s Errore di analisi nel file "%s" sulla riga %d: %s Errore di analisi nel file "%s": %s Rimpiazza con:  Impossibile aprire il file di configurazione %s Impossibile aprire il file %s Impossibile aprire il file %s per leggere un dizionario. Impossibile aprire il file %s per scrivere un dizionario. Impossibile aprire il file temporaneo Impossibile impostare la codifica su %s Impossibile scrivere nel file %s Opzione %s sconoscuta Stringa citata non terminata Uso: %s [opzioni] [file]...
Opzioni: [FMNLVlfsaAtnhgbxBCPmSdpwWTv]

 -F <file>  Usa il file specificato come file di configurazione.

Le opzioni seguenti sono uguali per ispell:
 -v[v]      Mostra la versione ed esci.
 -M         Mini menu mono-linea nella parte bassa dello schermo.
 -N         Nessun mini menu mono-linea nella parte bassa dello schermo.
 -L <num>   Numero delle linee nel contesto.
 -V         Usa lo stile "cat -v" per i caratteri che non usano il set 7-bit ANSI.
 -l         Mostra solo una lista delle parole ortograficamente errate.
 -f <file>  Specifica il file in uscita.
 -s         SIGTSTP ad ogni fine linea.
 -a         Leggi comandi.
 -A         Leggi comandi e abilita un comando per includere un file.
 -e[e1234]  Expandi affissi.
 -c         Comprimi affissi.
 -D         Dump della tabella affissi.
 -t         L'input è in formato TeX.
 -n         L'input è nel formato [nt]roff.
 -h         L'input è nel formato sgml.
 -b         Crea file di backup.
 -x         Non creare file di backup.
 -B         Non permettere più parole contemporaneamente.
 -C         Permetti più parole contemporaneamente.
 -P         Non generare combinazioni root/affix in più.
 -m         Permetti combinazioni root/affix che non sono nel dizionario.
 -S         Ordina la lista in base alla probabile correttezza.
 -d <dict>  Specifica un file dizionario alternativo.
 -p <file>  Specifica un file dizionario personale alternativo.
 -w <chars> Specifa caratteri addizionali che possono far parte di una parola.
 -W <len>   Considera parole più corte rispetto a quando sono sempre corrette.
 -T <fmt>   Utilizza il tipo di formattazione fornito per tutti i file.
 -r <cset>  Specifica il set di caratteri dell'input.
 Quando viene trovata una parola non riconosciuta, viene
mostrata su una riga a schermo. Se ci sono correzioni
suggerite vengono elencate con un numero a fianco.
Hai la possibilità di sostituire completamente la parola,
o scegliere una delle parole suggerite. In alternativa, puoi
ignorare la parola, ignorare tutte le occorrenze o 
aggiungerla nel dizionario personale.

I comandi sono:
 r       Sostituisce completamente la parola errata
 spazio   Accetta la parola solo questa volta
 a       Accetta la parola per il resto di questa sessione
 i       Accetta la parola e aggiungila al dizionario personale
 u       Accetta e aggiunge la versione in minuscolo al dizionario
 0-9     Sostituisce con una delle parole suggerite
 x       Scrive il resto del documento, ignorando gli errori,
         e inizia un nuovo documento
 q       Abbandona immediatamente chiedendo conferma
         Non modifica il documento
 ^Z      Sospende il programma
 ?       Mostra questa aiuto
 [SP] <numero> R)ispondi A)ccetta I)nserisci C)erca S)enza limite aB)bandona E)sci o ? per aiuto \ alla fine di una stringa aiuqxr sn 