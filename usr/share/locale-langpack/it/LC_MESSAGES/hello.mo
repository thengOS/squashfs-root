��          �   %   �      P  l   Q     �  0   �       ,     ,   L  ,   y  '   �  -   �      �  (     (   F     o     �     �  ?   �     �  )   �     )     @     T     i     �     �     �  �  �  s   L     �  2   �     	  +   '  .   S  +   �  +   �  ,   �  !   	  ,   )	  *   V	  $   �	  $   �	     �	  D   �	     
  1    
     R
     o
     �
     �
     �
     �
     �
                                     
         	                                                                                 -h, --help          display this help and exit
  -v, --version       display version information and exit
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' General help using GNU software: <http://www.gnu.org/gethelp/>
 Hello, world! Print a friendly, customizable greeting.
 Report %s bugs to: %s
 Report bugs to: %s
 Unknown system error Usage: %s [OPTION]...
 ` memory exhausted write error Project-Id-Version: hello 1.3.37
Report-Msgid-Bugs-To: bug-hello@gnu.org
POT-Creation-Date: 2014-11-16 11:53+0000
PO-Revision-Date: 2015-12-05 00:19+0000
Last-Translator: Marco d'Itri <md@linux.it>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:33+0000
X-Generator: Launchpad (build 18115)
Language: it
   -h, --help          mostra questo aiuto ed esce
  -v, --version       mostra informazioni sulla versione ed esce
 Sito web di %s: <%s>
 Sito web di %s: <http://www.gnu.org/software/%s/>
 %s: opzione non valida -- %c
 %s: l'opzione "%c%s" non accetta argomenti
 %s: l'opzione "%s" è ambigua. Possibilità: %s: l'opzione "--%s" non accetta argomenti
 %s: l'opzione "--%s" richiede un argomento
 %s: l'opzione "-W %s" non accetta argomenti
 %s: l'opzione "-W %s" è ambigua
 %s: l'opzione "-W %s" richiede un argomento
 %s: l'opzione richiede un argomento -- %c
 %s: opzione "%c%s" non riconosciuta
 %s: opzione "--%s" non riconosciuta
 " Aiuto per l'utilizzo di software GNU: <http://www.gnu.org/gethelp/>
 Salve, mondo! Stampa una saluto amichevole e personalizzabile.
 Segnalare i bug di %s a: %s
 Segnalare i bug a: %s
 Errore di sistema sconosciuto Uso: %s [OPZIONI]...
 " memoria esaurita errore di scrittura 