��          �      �       H  (   I  '   r     �     �     �  K   �  +     %   3  %   Y  %     %   �  b   �     .  �  ;  .     .   F     u  	   ~     �  L   �  0   �  -     ,   G  &   t  &   �  M   �                               	                     
                  A cover is open on the printer “%s”. A door is open on the printer “%s”. Paused Printers Printing Problem The printer “%s” can’t be used, because required software is missing. The printer “%s” is currently off-line. The printer “%s” is low on paper. The printer “%s” is low on toner. The printer “%s” is out of paper. The printer “%s” is out of toner. You have %d job queued to print on this printer. You have %d jobs queued to print on this printer. _Settings… Project-Id-Version: indicator-printers
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-04 13:07+0000
PO-Revision-Date: 2012-03-08 12:59+0000
Last-Translator: Pier Franco Farina <liste.pf@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 19:14+0000
X-Generator: Launchpad (build 18115)
 Il coperchio della stampante «%s» è aperto. Lo sportello della stampante «%s» è aperto. In pausa Stampanti Problema di stampa La stampante «%s» non può essere utilizzata, manca il software richiesto. La stampante «%s» non è attualmente in linea. Carta in esaurimento sulla stampante «%s» . Toner in esaurimento sulla stampante «%s». Carta esaurita sulla stampante «%s». Toner esaurito sulla stampante «%s». %d lavoro in coda su questa stampante. %d lavori in coda su questa stampante. _Impostazioni... 