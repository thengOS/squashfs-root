��    m      �  �   �      @	  $   A	     f	     o	  	   v	  9   �	     �	     �	  ,   �	     �	     
     
     
  "   7
  -   Z
     �
  !   �
  	   �
  
   �
     �
     �
     �
     �
       %   (  *   N     y     �     �     �     �     �     �  	   �  #     ?   &     f     o     t  /   �     �     �  	   �     �     �  
   �               .     L  !   d     �     �  $   �  -   �     �           -     5     T  
   n  $   y  	   �  =   �  /   �          6     G     c     �     �     �     �  .   �     �  *   �  >   #  ,   b  5   �     �     �     �     �     �     �          )      >  	   _  &   i  
   �  .   �  
   �     �  3   �          !  "   0     S     _     |     �  !   �     �     �     �  !     .   0     _  �  n  )        C     L     S  A   _     �     �  C   �     �               #  .   ;  =   j     �  0   �  	   �     �     �  !     &   (     O  #   b  )   �  6   �  "   �  &   
     1     O  	   k     u     �  
   �  6   �  C   �            #   #  F   G     �     �     �     �     �  
   �     �     �  $         %  (   =     f  	   �  &   �  D   �     �        	   .      8  #   Y     }  '   �  	   �  H   �  ,     '   3     [  #   q  $   �     �     �     �     �  ?     	   N  0   X  ?   �  4   �  2   �     1     =     Q     j     v     z     �     �  '   �     �  D   �  
   2  0   =  
   n  	   y  8   �     �     �  1   �  
               7      R   #   n      �   "   �      �   )   �   2   !     F!             1   !   &      T   :      @   U      -   3          X   ]          ;          h   c   M       A   ,   "   H       L   m   b   Q   )       ?   O   C   I       J       E   F   *   Z      
             %                      /   `   	       $       6   D          9                       ^          i   e      S      2            W   (       Y   P   a   K   .   8   R   =   d   B   g   0       \   <           >   '               j       N   #   7   [   f              4           k       +             l                     _          V           G   5                     Usage     Device name
     0 mW   Core   Package  <ESC> Exit | <Enter> Toggle tunable | <r> Window refresh  CPU %i %7sW %s device %s has no runtime power management .... device %s 
 Actual Audio codec %s: %s Audio codec %s: %s (%s) Autosuspend for USB device %s [%s] Autosuspend for unknown USB device %s (%s:%s) Bad Bluetooth device interface status C0 active C0 polling CPU use Calibrating USB devices
 Calibrating backlight
 Calibrating idle
 Calibrating radio devices
 Calibrating: CPU usage on %i threads
 Calibrating: CPU wakeup power consumption
 Calibrating: disk usage 
 Cannot create temp file
 Cannot load from file Cannot save to file Category Description Device stats Disk IO/s Enable Audio codec power management Estimated power: %5.1f    Measured power: %5.1f    Sum: %5.1f

 Events/s Exit Failed to mount debugfs!
 Finishing PowerTOP power estimate calibration 
 Frequency stats GFX Wakeups/s GPU ops/s GPU ops/seconds Good Idle stats Intel built in USB hub Leaving PowerTOP Loaded %i prior measurements
 Measuring workload %s.
 NMI watchdog should be turned off Network interface: %s (%s) Overview Overview of Software Power Consumers PCI Device %s has no runtime power management PCI Device: %s PS/2 Touchpad / Keyboard / Mouse Package Parameters after calibration:
 Power Aware CPU scheduler Power est. Power est.    Usage     Device name
 PowerTOP  PowerTOP %s needs the kernel to support the 'perf' subsystem
 PowerTOP is out of memory. PowerTOP is Aborting Preparing to take measurements
 Radio device: %s Runtime PM for %s device %s Runtime PM for PCI Device %s SATA controller SATA disk: %s SATA link: %s Set refresh time out Starting PowerTOP power estimate calibration 
 Summary System baseline power is estimated at %sW
 Taking %d measurement(s) for a duration of %d second(s) each.
 The battery reports a discharge rate of %sW
 The estimated remaining time is %i hours, %i minutes
 Tunables USB device: %s USB device: %s (%s) Unknown Usage Usage: powertop [OPTIONS] VFS ops/sec and VM writeback timeout Wake-on-lan status for device %s Wakeups/s Wireless Power Saving for interface %s [=devnode] [=iterations] number of times to run each test [=seconds] [=workload] as well as support for trace points in the kernel:
 cpu package cpu package %i cpu_idle event returned no state?
 exiting...
 file to execute for workload generate a csv report generate a html report generate a report for 'x' seconds print this help menu print version information run in "debug" mode runs powertop in calibration mode uses an Extech Power Analyzer for measurements wakeups/second Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-11-05 16:49-0800
PO-Revision-Date: 2013-10-13 10:32+0000
Last-Translator: Claudio Arseni <claudio.arseni@gmail.com>
Language-Team: Italian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:21+0000
X-Generator: Launchpad (build 18115)
X-Poedit-SourceCharset: utf-8
               Uso       Nome dispositivo
     0 mW   Core   Pacchetto  <ESC> Esci | <Invio> Commuta regolazione | <r> Aggiorna finestra  CPU %i %7sW Il dispositivo %s %s non ha una gestione runtime dell'alimentazione .... dispositivo %s 
 Attuale Codec audio %s: %s Codec audio %s: %s (%s) Autosospensione per il dispositivo USB %s [%s] Autosospensione per il dispositivo USB sconosciuto %s (%s:%s) Negativo Stato dell'interfaccia del dispositivo Bluetooth C0 attivo C0 interrogazione uso CPU Calibrazione dei dispositivi USB
 Calibrazione della retroilluminazione
 Calibrazione idle
 Calibrazione dei dispositivi radio
 Calibrazione: uso della CPU su %i thread
 Calibrazione: consumo energetico del wakeup della CPU
 Calibrazione: utilizzo del disco 
 Impossibile creare un file temporaneo
 Impossibile caricare dal file Impossibile salvare su file Categoria Descrizione Stat. dispos. Disco IO/s Abilitazione della gestione energetica dei codec audio Potenza stimata: %5.1f    Potenza misurata: %5.1f    Somma: %5.1f

 Eventi/s Esci Montaggio di debugfs non riuscito.
 Completamento della calibrazione di PowerTOP per la stima energetica 
 Stat. frequenza GFX wakeup/s GPU op/s GPU op/secondo Positivo Stat. idle Hub USB interno Intel Uscita da PowerTOP Caricato %i prima delle misurazioni
 Misurazione carico %s.
 NMI watchdog dovrebbe essere disattivato Interfaccia di rete: %s (%s) Riepilogo Panoramica consumi energetici software Il dispositivo PCI %s non ha una gestione runtime dell'alimentazione Dispositivo PCI: %s PS/2 Touchpad / Tastiera / Mouse Pacchetto Parametri dopo la calibrazione:
 Pianificatore della CPU Power Aware Stima consumo Stima pot.    Uso     Nome dispositivo
 PowerTOP  PowerTOP %s ha bisogno del kernel per supportare il sottosistema "perf"
 Memoria esaurita per PowerTOP. Interruzione. Preparazione alla raccolta misurazioni
 Dispositivo radio: %s PM runtime per il dispositivo %s %s PM runtime per il dispositivo PCI %s Controller SATA Disco SATA: %s Collegamento SATA: %s Imposta tempo di aggiornamento Inizio della calibrazione di PowerTOP per la stima energetica 
 Riepilogo La potenza di base del sistema è stimata a %sW
 Raccolta di %d misurazioni per una durata di %d secondi l'una.
 La batteria riporta un tasso di scaricamento di %sW
 Il tempo stimato restate è di %i ore e %i minuti
 Regolazioni Dispositivo USB: %s Dispositivo USB: %s (%s) Sconosciuto Uso Uso: powertop [OPZIONI] VFS op/secondo e Timeout del VM writeback Stato wake-on-lan per il dispositivo %s Wakeup/s Risparmio energetico del dispositivo senza fili per l'interfaccia %s [=devnode] [=iterazioni] Numero di esecuzioni per ogni test [=secondi] [=carico] e anche il supporto per i tracepoint nel kernel stesso:
 pacchetto cpu pacchetto cpu %i L'evento cpu_idle non ha riportato alcuno stato?
 uscita...
 File da eseguire per il carico Genera un resoconto in CSV Genera un resoconto in HTML Genera un resoconto per "x" secondi Stampa questo aiuto Stampa informazioni sulla versione Esecuzione in modalità "debug" Esegue powertop in modalità calibrazione Utilizza un analizzatore Extech per le misurazioni wakeup/secondo 