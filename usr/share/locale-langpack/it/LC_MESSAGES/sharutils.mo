��    p      �  �         p	     q	  "   �	  -   �	     �	     �	     �	     
     %
     5
     O
     i
  ,   �
     �
  ,   �
  ,   �
  '   *  -   R      �  (   �  (   �     �          3     5     G  	   P     Z     g  	   o     y     �  !   �     �     �     �     �     �          '  %   9     _     s     �     �     �  (   �     �  @        H  2   P  )   �     �     �     �     �  1   �  )   (     R  H   g     �     �     �     �     �     �     �                     4     E     U  "   h     �     �     �     �     �     �          
     "     '     4     H     [     o     �  #   �  *   �     �     �     	     #     2     A     F     [     r     �  %   �     �     �     �       "   &  '   I  '   q     �     �     �  �  �     �  /   �  6   �     �          /     J     Y     q     �     �  +   �     �  &     +   6  +   b  ,   �  !   �  ,   �  )   
  $   4  $   Y     ~     �  	   �     �     �     �     �     �     �  5   �     3  "   Q     t     }  %   �     �     �  -   �          '  %   G     m     �  7   �     �  H   �     3  C   <  *   �     �     �     �     �  :   �     4     O  S   m     �     �     �     �     �     �          *     D  *   J     u     �     �  3   �     �       #        +     J     [     z     }     �     �     �     �     �  !        *  -   /  )   ]     �     �  !   �     �     �                     "   6      Y   %   r      �      �   "   �      �   0   !  8   6!  8   o!     �!  #   �!     �!     p       	   ;          J   B   D   #      b         S   I   7   <   @   ]      /       T                          O          G       d       e          )   Q      >   \   (   K      _           3   L   4           !       &           k       c   Y              l       V   A   h      R            X   
             1   H       8   ,   0   6   =      f   M      "   n          C   o                   :             9              Z   %   g      .   2   '   *   i      E   F       P   N       -       a   j   $   U   [   +   `      W      m       5      ?   ^          chmod %04o %s
 %s is probably not a shell archive %s looks like raw C code, not a shell archive %s: Illegal file name: %s %s: No `end' line %s: Not a regular file %s: Short file %s: Write error %s: illegal option -- %c
 %s: illegal option -- %s
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' '%s' not defined
 (binary) (bzipped) (compressed) (empty) (gzipped) (text) Cannot access %s Cannot get current directory name Cannot open file %s Cannot reopen %s as stdin Closing Continuing file %s
 Could not popen command Created %d files
 End of %s part %d End of part %d, continue with part %d File %s is complete File %s is continued in part %d Found no shell commands in %s MD5 check failed No input files No memory for duping translated strings
 No user '%s' Note: not verifying md5sums.  Consider installing GNU coreutils. Opening PLEASE avoid -X shars on Usenet or public networks Please unpack part ${shar_sequence} next! Please unpack part 1 first! SKIPPING %s Saving %s (%s) Starting file %s
 The '%s' option must appear before any file names Too many directories for mkdir generation Unknown system error You cannot specify an output file when processing
multiple input files.
 You have unpacked the last part ` all archive call cannot access %s chmod of %s continuing file %s empty error %d (%s) creating %s
 exit immediately explain choices extraction aborted failed to create %s lock directory format is too wide help invalid conversion character lock directory %s exists memory exhausted more than one format element no no conversion character none overwrite %s overwrite all files overwrite no files overwrite this file printf formatting error:  %s
 quit realloc of %d bytes at 0x%p failed
 restoration warning:  size of %s is not %s restore of %s failed shar fatal error:
 sharutils bug - no status skip this file standard input text unshar fatal error:
 uudecode fatal error:
 uudecoding file %s uuencode fatal error:
 x - SKIPPING %s (file already exists) x - STILL SKIPPING %s x - created directory %s. x - created lock directory %s. x - extracting %s %s x - failed to create directory %s. x - failed to create lock directory %s. x - failed to remove lock directory %s. x - overwriting %s x - removed lock directory %s. yes Project-Id-Version: sharutils 4.13.3
Report-Msgid-Bugs-To: bug-gnu-utils@gnu.org
POT-Creation-Date: 2015-05-30 08:36-0700
PO-Revision-Date: 2015-12-04 23:03+0000
Last-Translator: Marco Colombo <Unknown>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:17+0000
X-Generator: Launchpad (build 18115)
Language: it
   chmod %04o %s
 %s probabilmente non è un archivio della shell %s sembra codice C grezzo, non un archivio della shell %s: nome di file illecito: %s %s: riga "end" mancante %s: non è un file normale %s: file corto %s: errore di scrittura %s: opzione "%c" illegale
 %s: opzione "%s" illegale
 %s: opzione "%c" non valida
 %s: l'opzione "%c%s" non accetta argomenti
 %s: l'opzione "%s" è ambigua
 %s: l'opzione "%s" è ambigua; scelte: %s: l'opzione "--%s" non accetta argomenti
 %s: l'opzione "--%s" richiede un argomento
 %s: l'opzione "-W %s" non accetta argomenti
 %s: l'opzione "-W %s" è ambigua
 %s: l'opzione "-W %s" richiede un argomento
 %s: l'opzione "%c" richiede un argomento
 %s: opzione "%c%s" non riconosciuta
 %s: opzione "--%s" non riconosciuta
 " "%s" non definito
 (binario) (compresso con bzip) (compresso) (vuoto) (compresso con gzip) (testo) Impossibile accedere a %s Impossibile ottenere il nome della directory corrente Impossibile aprire il file %s Impossibile riaprire %s come stdin Chiusura Continuazione del file %s
 Impossibile fare popen per il comando Creati %d file
 Fine di %s parte %d Fine della parte %d, continua con la parte %d Il file %s è completo Il file %s continua in parte %d Nessun comando di shell trovato in %s Controllo MD5 fallito Nessun file di input Memoria insufficiente per copiare le stringhe tradotte
 Utente "%s" non esistente Nota: md5sums non verificati. Si suggerisce di installare GNU coreutils. Apertura PER FAVORE evitare di creare shar con -X su Usenet o reti pubbliche Adesso estrarre la parte ${shar_sequence}. Estrarre prima la parte 1. %s IGNORATO Salvataggio di %s (%s) Inizio file %s
 L'opzione "%s" deve essere indicata prima dei nomi di file Troppe directory da creare Errore di sistema sconosciuto Impossibile indicare un file di output quando si processano
diversi file di input.
 Ultima parte estratta " tutti archivio chiamata impossibile accedere a %s Cambiamento dei permessi di %s continuazione del file %s vuoto errore %d (%s) durante la creazione di %s
 uscire immediatamente spiega le scelte estrazione interrotta creazione della directory di lock "%s" non riuscita il formato è troppo largo aiuto carattere di conversione non valido la directory di lock %s esiste memoria esaurita più di un elemento di formato no nessun carattere di conversione nessuno sovrascrivere %s sovrascrivere tutti i file non sovrascrivere alcun file sovrascrivere questo file errore di formato in printf:  %s
 esci riallocazione di %d byte a 0x%p non riuscita
 attenzione: la dimensione di %s non è %s ripristino di %s non riuscito shar errore fatale:
 bug in sharutils - nessuno status ignorare questo file standard input testo unshar errore fatale:
 uuencode errore fatale:
 esecuzione di uudecode sul file %s uuencode errore fatale:
 x - %s IGNORATO (il file esiste già) x - %s IGNORATO ANCORA x - creata directory "%s". x - creata directory di lock "%s". x - estrazione di %s %s x - creazione della directory "%s" non riuscita. x - creazione della directory di lock "%s" non riuscita. x - rimozione della directory di lock "%s" non riuscita. x - sovrascrittura di %s x - directory di lock "%s" rimossa. sì 