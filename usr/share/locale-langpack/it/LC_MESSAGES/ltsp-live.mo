��          �   %   �      P  &   Q     x     �     �  (   �  (   �                0  %   H     n     ~     �     �     �     �     �     �     �  &   
     1  R   8  #   �     �  K   �  �    *   �     �     �       /   (  &   X       %   �     �  &   �                     #     >     W     g     x     �      �     �  D   �  '   	  (   6	  K   _	                                                           
              	                                                    Adding LTSP network to Network Manager Configuring DNSmasq Configuring LTSP Creating the guest users Enabling LTSP network in Network Manager Extracting thin client kernel and initrd Failed Installing the required packages LTSP-Live configuration LTSP-Live should now be ready to use! Network devices None Ready Restarting Network Manager Restarting openbsd-inetd Start LTSP-Live Starting DNSmasq Starting NBD server Starting OpenSSH server Starts an LTSP server from the live CD Status The selected network interface is already in use.
Are you sure you want to use it? Unable to configure Network Manager Unable to parse config Welcome to LTSP Live.
Please choose a network interface below and click OK. Project-Id-Version: ltsp
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-07-18 13:50-0400
PO-Revision-Date: 2015-07-20 15:23+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:19+0000
X-Generator: Launchpad (build 18115)
Language: it
 Aggiunta della rete LTSP a Network Manager Configurazione di DNSmasq Configurazione di LTSP Creazione dell'utente ospite Abilitazione della rete LTSP in Network Manager Estrazione kernel e initrd thin client Operazione non riuscita Installazione dei pacchetti richiesti Configurazione LTSP-lìLive È ora possibile utilizzare LTSP-Live. Dispositivi di rete Nessuno Pronto Riavvio di Network Manager Riavvio di openbsd-inetd Avvia LTSP-Live Avvio di DNSmasq Avvio server NBD Avvio del server OpenSSH Avvia un server LTSP dal CD Live Stato L'interfaccia di rete selezionata è già in uso.
Usarla ugualmente? Impossibile configurare Network Manager Impossibile analizzare la configurazione Benvenuti in LTSP Live.
Scegliere un'interfaccia di rete e fare clic su OK. 