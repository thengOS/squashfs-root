��    �      T  7  �      h     i  L   k  K   �  �     u   �  �   8  �   �  k   k  }   �  [   U  �   �  [   ?  �   �  �   *  (   �  (   �  (     1   :  B   l  %   �  %   �  3   �  (   /  (   X  2   �  E   �  ?   �  (   :  1   c     �     �     �  "   �  %   �  /     .   @  $   o     �     �     �     �     �  	         
  	   &     0     K     ^     t     �  (   �     �     �          $     ?     X     v     �     �  =   �  &      *   )      T   
   l   D   w   C   �   +    !  &   ,!     S!     i!  ,   �!  %   �!  )   �!  %   "  !   '"  !   I"  $   k"     �"  :   �"  1   �"  9   #  6   S#     �#     �#  "   �#     �#     �#     $     $     0$     F$  !   \$  '   ~$  '   �$     �$  7   �$     %  "   9%  #   \%     �%  )   �%  /   �%     �%  .   &     D&     Z&  %   l&  #   �&  +   �&  +   �&  1   '  1   @'  %   r'  +   �'  1   �'  1   �'     ((  !   D(  %   f(  "   �(  *   �(     �(  "   �(     )      2)  /   S)  #   �)     �)     �)     �)     �)      *  %   )*     O*     h*  +   }*     �*     �*  %   �*  $   +     )+  0   D+  0   u+  #   �+  7   �+  !   ,  !   $,  5   F,  "   |,  +   �,      �,      �,      -     .-  +   N-  2   z-  2   �-  )   �-  #   
.     ..     I.     Y.     t.     �.     �.     �.     �.  "   /     //     O/     l/     }/     �/     �/     �/     �/     �/  2   
0  %   =0     c0     y0  &   �0     �0     �0     �0     	1     1     11     B1     N1  !   Z1  ;   |1     �1  %   �1  +   �1  /   (2     X2     q2  &   �2  2   �2  2   �2  2   3  4   P3  ,   �3     �3  )   �3     �3  (   4     >4     [4  5   u4     �4     �4  -   �4  ;   5     ?5     W5  /   o5  9   �5  	   �5     �5     �5     �5     6     #6     >6      O6     p6  &   �6  �  �6     o8  P   q8  O   �8  �   9  �   �9  �   �:  �   h;  �   &<  �   �<  c   8=  �   �=  c   5>  �   �>  �   3?  ;   �?  ;   @  ;   K@  <   �@  L   �@  4   A  4   FA  8   {A  ,   �A  -   �A  =   B  O   MB  Q   �B  -   �B  C   C     aC     wC     �C  '   �C  *   �C  6   �C  5   #D  *   YD  %   �D     �D     �D  "   �D  "   �D     E  "   E     ?E  *   ME     xE     �E     �E  $   �E  3   �E     F  &   8F  .   _F     �F     �F  "   �F  !   �F     G     )G  T   IG  2   �G  3   �G     H  
   H  T   %H  S   zH  *   �H  %   �H  +   I  ,   KI  8   xI  2   �I  6   �I  2   J  .   NJ  .   }J  1   �J  "   �J  =   K  4   ?K  <   tK  A   �K  &   �K  $   L  3   ?L     sL     �L     �L     �L     �L     �L  '   M  +   +M  +   WM     �M  ?   �M  #   �M  #   N  $   (N  #   MN  ,   qN  ?   �N  *   �N  7   	O     AO     _O  1   vO  -   �O  <   �O  ;   P  B   OP  B   �P  -   �P  7   Q  D   ;Q  D   �Q  %   �Q  +   �Q  7   R  2   OR  =   �R  +   �R  5   �R     "S  8   BS  H   {S  )   �S     �S     T     %T      ET  /   fT  3   �T  %   �T      �T  B   U  +   TU  #   �U  +   �U  *   �U  &   �U  <   "V  <   _V  /   �V  G   �V  &   W  *   ;W  C   fW  2   �W  4   �W  +   X  +   >X  +   jX  %   �X  D   �X  K   Y  K   MY  <   �Y  0   �Y  !   Z     )Z  "   HZ  *   kZ  (   �Z  (   �Z  (   �Z  (   [  1   :[  1   l[  +   �[     �[  $   �[     
\     (\     H\     \\     w\  J   �\  2   �\     ]  "   .]  8   Q]  4   �]  !   �]  -   �]  #   ^     3^     S^     k^     x^  ,   �^  =   �^     �^  9   _  6   H_  :   _      �_     �_  2   �_  9   )`  9   c`  9   �`  ;   �`  3   a     Ga  4   aa  .   �a  7   �a  &   �a  #   $b  D   Hb     �b  %   �b  8   �b  H   c     Mc     bc  5   wc  ?   �c  
   �c     �c     d     d     ,d  *   Ed     pd  &   �d     �d  .   �d             Z       Q   �                �       =   �   O   ]         �   �   �   #      n   ^   �   �   �       �              3       �   %       A   u   ?       �   &       �       D   �       P   �   *   �   }       ;   �           �   o   W   �       b   F   �      v   �       �               �   .          m       �   r       �   �   �   �       U   �   �   `       �       X   �       7   �      �           f   �   E   �               "   �   K   N   �       C           y   ~   
   i       �   9   �             l   �   �      |                    �       6   �       �   H   �   1      �   $       0   �   2   g          �   �       s      �   _   �   !   @       	   )       t   J   �   �       �   �   {   �   �   x   �   �       �   j   �   �   :   V   �   �           �       �   �   d                 p               �   /   �       �   �   �   q   L       �   �       �   -   (   >      �       M   w      �   +   �   4   �   G      �       �       �   �   a   �      �   S   �   8   �           �   R   �   ,   c   �           �   �   I          <                         \   [   �       B   �   z   �   �   e       �   �   5   �   �   h       �   k   Y           �   T       �              '    
 
  For the options above, The following values are supported for "ARCH":
    
  For the options above, the following values are supported for "ABI":
    
  cp0-names=ARCH           Print CP0 register names according to
                           specified architecture.
                           Default: based on binary being disassembled.
 
  fpr-names=ABI            Print FPR names according to specified ABI.
                           Default: numeric.
 
  gpr-names=ABI            Print GPR names according to  specified ABI.
                           Default: based on binary being disassembled.
 
  hwr-names=ARCH           Print HWR names according to specified 
			   architecture.
                           Default: based on binary being disassembled.
 
  reg-names=ABI            Print GPR and FPR names according to
                           specified ABI.
 
  reg-names=ARCH           Print CP0 register and HWR names according to
                           specified architecture.
 
The following ARM specific disassembler options are supported for use with
the -M switch:
 
The following MIPS specific disassembler options are supported for use
with the -M switch (multiple options should be separated by commas):
 
The following PPC specific disassembler options are supported for use with
the -M switch:
 
The following S/390 specific disassembler options are supported for use
with the -M switch (multiple options should be separated by commas):
 
The following i386/x86-64 specific disassembler options are supported for use
with the -M switch (multiple options should be separated by commas):
   addr16      Assume 16bit address size
   addr32      Assume 32bit address size
   addr64      Assume 64bit address size
   att         Display instruction in AT&T syntax
   att-mnemonic
              Display instruction in AT&T mnemonic
   data16      Assume 16bit data size
   data32      Assume 32bit data size
   esa         Disassemble in ESA architecture mode
   i386        Disassemble in 32bit mode
   i8086       Disassemble in 16bit mode
   intel       Display instruction in Intel syntax
   intel-mnemonic
              Display instruction in Intel mnemonic
   suffix      Always display instruction suffix in AT&T syntax
   x86-64      Disassemble in 64bit mode
   zarch       Disassemble in z/Architecture mode
 # <dis error: %08lx> $<undefined> %02x		*unknown* %d unused bits in i386_cpu_flags.
 %d unused bits in i386_operand_type.
 %dsp16() takes a symbolic address, not a number %dsp8() takes a symbolic address, not a number %s: %d: Missing `)' in bitfield: %s
 %s: %d: Unknown bitfield: %s
 %s: Error:  %s: Warning:  (DP) offset out of range. (SP) offset out of range. (unknown) *unknown operands type: %d* *unknown* 21-bit offset out of range <function code %d> <illegal instruction> <illegal precision> <internal disassembler error> <internal error in opcode table: %s %s>
 <unknown register %d> Address 0x%s is out of bounds.
 Attempt to find bit index of 0 Bad case %d (%s) in %s:%d
 Bad immediate expression Bad register in postincrement Bad register in preincrement Bad register name Biiiig Trouble in parse_imm16! Bit number for indexing general register is out of range 0-15 Byte address required. - must be even. Don't know how to specify # dependency %s
 Don't understand 0x%x 
 Hmmmm 0x%x IC note %d for opcode %s (IC:%s) conflicts with resource %s note %d
 IC note %d in opcode %s (IC:%s) conflicts with resource %s note %d
 IC:%s [%s] has no terminals or sub-classes
 IC:%s has no terminals or sub-classes
 Illegal as 2-op instr Illegal as emulation instr Illegal limm reference in last instruction!
 Immediate is out of range -128 to 127 Immediate is out of range -32768 to 32767 Immediate is out of range -512 to 511 Immediate is out of range -7 to 8 Immediate is out of range -8 to 7 Immediate is out of range 0 to 65535 Internal disassembler error Internal error:  bad sparc-opcode.h: "%s", %#.8lx, %#.8lx
 Internal error: bad sparc-opcode.h: "%s" == "%s"
 Internal error: bad sparc-opcode.h: "%s", %#.8lx, %#.8lx
 Internal: Non-debugged code (test-case missing): %s:%d Invalid size specifier Label conflicts with `Rx' Label conflicts with register name Missing '#' prefix Missing '.' prefix Missing 'pag:' prefix Missing 'pof:' prefix Missing 'seg:' prefix Missing 'sof:' prefix No relocation for small immediate Only $sp or $15 allowed for this opcode Only $tp or $13 allowed for this opcode Operand is not a symbol Operand out of range. Must be between -32768 and 32767. Register list is not valid Register must be between r0 and r7 Register must be between r8 and r15 Register number is not valid Small operand was not an immediate number Special purpose register number is out of range Syntax error: No trailing ')' The percent-operator's operand is not a symbol Unknown bitfield: %s
 Unknown error %d
 Unrecognised disassembler option: %s
 Unrecognised register name set: %s
 Unrecognized field %d while building insn.
 Unrecognized field %d while decoding insn.
 Unrecognized field %d while getting int operand.
 Unrecognized field %d while getting vma operand.
 Unrecognized field %d while parsing.
 Unrecognized field %d while printing insn.
 Unrecognized field %d while setting int operand.
 Unrecognized field %d while setting vma operand.
 Value is not aligned enough Value of A operand must be 0 or 1 W keyword invalid in FR operand slot. Warning: rsrc %s (%s) has no chks
 Warning: rsrc %s (%s) has no chks or regs
 address writeback not allowed attempt to read writeonly register attempt to set HR bits attempt to set readonly register attempt to set y bit when using + or - modifier auxiliary register not allowed here bad instruction `%.50s' bad instruction `%.50s...' bad jump flags value bit,base is out of range bit,base out of range for symbol branch address not on 4 byte boundary branch operand unaligned branch to odd offset branch value not in range and to odd offset branch value out of range can't cope with insert %d
 can't create i386-init.h, errno = %s
 can't create i386-tbl.h, errno = %s
 can't find %s for reading
 can't find i386-opc.tbl for reading, errno = %s
 can't find i386-reg.tbl for reading, errno = %s
 can't find ia64-ic.tbl for reading
 cgen_parse_address returned a symbol. Literal required. class %s is defined but not used
 displacement value is not aligned displacement value is not in range and is not aligned displacement value is out of range don't know how to specify %% dependency %s
 dsp:16 immediate is out of range dsp:20 immediate is out of range dsp:24 immediate is out of range dsp:8 immediate is out of range expecting got relative address: got(symbol) expecting got relative address: gotoffhi16(symbol) expecting got relative address: gotofflo16(symbol) expecting gp relative address: gp(symbol) flag bits of jump address limm lost ignoring invalid mfcr mask illegal bitmask illegal use of parentheses imm:6 immediate is out of range immediate is out of range 0-7 immediate is out of range 1-2 immediate is out of range 1-8 immediate is out of range 2-9 immediate value cannot be register immediate value is out of range immediate value out of range impossible store index register in load range invalid %function() here invalid conditional option invalid constant invalid load/shimm insn invalid mask field invalid operand.  type may have values 0,1,2 only. invalid register for stack adjustment invalid register name invalid register number `%d' invalid register operand when updating invalid sprg number jump flags, but no .f seen jump flags, but no limm addr jump hint unaligned junk at end of line ld operand error missing `)' missing `]' missing mnemonic in syntax string most recent format '%s'
appears more restrictive than '%s'
 multiple note %s not handled
 must specify .jd or no nullify suffix no insns mapped directly to terminal IC %s
 no insns mapped directly to terminal IC %s [%s] not a valid r0l/r0h pair offset(IP) is not a valid form opcode %s has no class (ops %d %d %d)
 operand out of range (%ld not between %ld and %ld) operand out of range (%ld not between %ld and %lu) operand out of range (%lu not between %lu and %lu) operand out of range (0x%lx not between 0 and 0x%lx) operand out of range (not between 1 and 255) overlapping field %s->%s
 overwriting note %d with note %d (IC:%s)
 parse_addr16: invalid opindex. percent-operator operand is not a symbol register number must be even rsrc %s (%s) has no regs
 source and target register operands must be different st operand error store value must be zero syntax error (expected char `%c', found `%c') syntax error (expected char `%c', found end of instruction) too many long constants too many shimms in load unable to change directory to "%s", errno = %s
 unable to fit different valued constants into instruction undefined unknown unknown	0x%02lx unknown	0x%04lx unknown constraint `%c' unknown operand shift: %x
 unknown reg: %d
 unrecognized form of instruction unrecognized instruction warning: ignoring unknown -M%s option
 Project-Id-Version: opcodes-2.21.53
Report-Msgid-Bugs-To: bug-binutils@gnu.org
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2011-11-23 22:24+0000
Last-Translator: Sergio Zanchetta <primes2h@ubuntu.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-10-09 10:38+0000
X-Generator: Launchpad (build 18227)
Language: it
 
 
  Per "ARCH" sono supportati i seguenti valori per le opzioni di cui sopra:
    
  Per "ABI" sono supportati i seguenti valori per le opzioni di cui sopra:
    
  cp0-names=ARCH           Stampa i nomi dei registri CP0 secondo
                           l'architettura specificata.
                           Predefinito: basato sul binario in fase di disassemblamento.
 
  fpr-names=ABI            Stampa i nomi dei registri in virgola
                           mobile secondo l'ABI specificato.
                           Default: numerico.
 
  gpr-names=ABI            Stampa i nomi dei registri d'uso generale
                           secondo l'ABI specificato.
                           Predefinito: basato sul binario in fase di disassemblamento.
 
  hwr-names=ARCH           Stampa i nomi dei registri HWR
			   secondo l'architettura specificata.
                           Predefinito: basato sul binario in fase di disassemblamento.
 
  reg-names=ABI            Stampa i nomi dei registri d'uso generale e in
                           virgola mobile secondo l'ABI specificato.
 
  reg-names=ARCH           Stampa i nomi dei registri CP0 e HWR secondo
                           l'architettura specificata.
 
Le seguenti opzioni del disassemblatore specifiche per ARM possono essere
usate con l'opzione -M:
 
Le seguenti opzioni del disassemblatore specifiche per MIPS possono essere usate
con l'opzione -M (valori multipli devono essere separati da virgole):
 
Le seguenti opzioni del disassemblatore specifiche per PPC possono essere usate
con l'opzione -M:
 
Le seguenti opzioni del disassemblatore specifiche per S/390 possono essere usate
con l'opzione -M (valori multipli devono essere separati da virgole):
 
Le seguenti opzioni del disassemblatore specifiche per i386/x86-64 possono essere usate
con l'opzione -M (valori multipli devono essere separati da virgole):
   addr16      Assume 16bit come dimensione degli indirizzi
   addr32      Assume 32bit come dimensione degli indirizzi
   addr64      Assume 64bit come dimensione degli indirizzi
   att         Visualizza le istruzioni con la sintassi AT&T
   att-mnemonic
              Visualizza le istruzioni con lo mnemonico AT&T
   data16      Assume 16bit come dimensione dei dati
   data32      Assume 32bit come dimensione dei dati
   esa         Disassembla in modalità architettura ESA
   i386       Disassembla in modalità 32bit
   i8086       Disassembla in modalità 16bit
   intel       Visualizza le istruzioni con la sintassi Intel
   intel-mnemonic
              Visualizza le istruzioni con lo mnemonico Intel
   suffix      Visualizza sempre il suffisso dell'istruzione con la sintassi AT&T
   x86-64      Disassembla in modalità 64bit
   zarch       Disassembla in modalità architettura z/Architecture
 # <errore dis: %08lx> $<indefinito> %02x		*sconosciuto* %d bit inutilizzati in i386_cpu_flags.
 %d bit inutilizzati in i386_operand_type.
 %dsp16() accetta un indirizzo simbolico, non un numero %dsp8() accetta un indirizzo simbolico, non un numero %s: %d: ")" mancante nel campo di bit: %s
 %s: %d: campo di bit sconosciuto: %s
 %s: errore:  %s: attenzione:  offset (DP) fuori dall'intervallo. offset (SP) fuori dall'intervallo. (sconosciuto) *operando di tipo sconosciuto: %d* *sconosciuta* l'offset a 21 bit è fuori dall'intervallo <codice funzione %d> <istruzione non consentita> <precisione non consentita> <errore interno del disassemblatore> <errore interno nella tabella degli opcode: %s %s>
 <registro sconosciuto %d> L'indirizzo 0x%s è fuori dai limiti.
 Tentativo di trovare un indice di bit pari a 0 Caso errato %d (%s) in %s:%d
 Espressione di immediati errata Registro errato nel postincremento Registro errato nel preincremento Nome di registro errato Grosso problema in parse_imm16. Il numero del bit per indicizzare il registro generale è fuori dall'intervallo 0-15 Richiesto l'indirizzo in byte. - Deve essere pari. Non si conosce come specificare la dipendenza # %s
 0x%x non è chiaro 
 Hmmmm 0x%x la nota IC %d per l'opcode %s (IC:%s) è in conflitto con la risorsa %s con nota %d
 la nota IC %d nell'opcode %s (IC:%s) è in conflitto con la risorsa %s con nota %d
 IC:%s [%s] non ha terminali o sottoclassi
 IC:%s non ha terminali o sottoclassi
 Non consentita come istruzione a 2 operandi Non consentita come istruzione di emulazione Riferimento limm non consentito nell'ultima istruzione.
 L'immediato è fuori dall'intervallo da -128 a 127 L'immediato è fuori dall'intervallo da -32768 a 32767 L'immediato è fuori dall'intervallo da -512 a 511 L'immediato è fuori dall'intervallo da -7 a 8 L'immediato è fuori dall'intervallo da -8 a 7 L'immediato è fuori dall'intervallo da 0 a 65535 Errore interno del disassemblatore Errore interno:  sparc-opcode.h errato: "%s", %#.8lx, %#.8lx
 Errore interno: sparc-opcode.h errato: "%s" == "%s"
 Errore interno: sparc-opcode.h errato: "%s", %#.8lx, %#.8lx
 Errore interno: codice senza debug (caso di test mancante): %s:%d Specificatore di dimensione non valido L'etichetta è in conflitto con "Rx" L'etichetta è in conflitto con il nome di registro Prefisso "#" mancante Prefisso "." mancante Prefisso "pag:" mancante Prefisso "pof:" mancante Prefisso "seg:" mancante Prefisso "sof:" mancante Nessuna rilocazione per immediati small Per questo opcode è ammesso solo $sp o $15 Per questo opcode è ammesso solo $tp o $13 L'operando non è un simbolo Operando fuori dall'intervallo. Deve essere tra -32768 e 32767. L'elenco dei registri non è valido Il registro deve essere tra r0 e r7 Il registro deve essere tra r8 e r15 Il numero di registro non è valido L'operando small non era un numero immediato Il numero del registro di uso speciale è fuori dall'intervallo Errore di sintassi: nessun ")" di chiusura L'operando dell'operatore percentuale non è un simbolo Campo di bit sconosciuto: %s
 Errore sconosciuto %d
 Opzione del disassemblatore non riconosciuta: %s
 Set di nomi di registro non riconosciuto: %s
 Campo %d non riconosciuto durante la generazione dell'insn.
 Campo %d non riconosciuto durante la decodifica dell'insn.
 Campo %d non riconosciuto durante la ricezione dell'operando int.
 Campo %d non riconosciuto durante la ricezione dell'operando vma.
 Campo %d non riconosciuto durante l'analisi.
 Campo %d non riconosciuto durante la stampa dell'insn.
 Campo %d non riconosciuto durante l'impostazione dell'operando int.
 Campo %d non riconosciuto durante l'impostazione dell'operando vma.
 Il valore non è abbastanza allineato Il valore dell'operando A deve essere 0 o 1 Parola chiave w non valida nello slot FR dell'operando. Attenzione: la risorsa %s (%s) non ha impedimenti
 Attenzione: la risorsa %s (%s) non ha impedimenti o registri
 il writeback dell'indirizzo non è permesso tentativo di lettura di un registro in sola scrittura tentativo di impostare i bit HR tentativo di impostazione di un registro in sola lettura tentativo di impostare il bit y quando viene usato il modificatore + o - qui non è ammesso il registro ausiliario istruzione errata "%.50s" istruzione errata "%.50s..." valore dei flag di salto errato bit o base fuori dall'intervallo bit o base fuori dall'intervallo per il simbolo indirizzo di diramazione fuori del limite di 4 byte operando di diramazione non allineato diramazione su un offset dispari valore di diramazione fuori dall'intervallo e su un offset dispari Valore di diramazione fuori dall'intervallo impossibile occuparsi di insert %d
 impossibile creare i386-init.h, errno = %s
 impossibile creare i386-tbl.h, errno = %s
 impossibile trovare %s per la lettura
 impossibile trovare i386-opc.tbl per la lettura, errno = %s
 impossibile trovare i386-reg.tbl per la lettura, errno = %s
 impossibile trovare ia64-ic.tbl per la lettura
 cgen_parse_address ha restituito un simbolo. È richiesto un letterale. la classe %s è definita ma non usata
 il valore di sostituzione non è allineato il valore di sostituzione non è nell'intervallo e non è allineato il valore di sostituzione è fuori dall'intervallo non si conosce come specificare la dipendenza %% %s
 dsp:16 l'immediato è fuori dall'intervallo dsp:20 l'immediato è fuori dall'intervallo dsp:24 l'immediato è fuori dall'intervallo dsp:8 immediato fuori dall'intervallo atteso indirizzo relativo della tabella offset globali: got(simbolo) atteso indirizzo relativo della tabella offset globali: gotoffhi16(simbolo) atteso indirizzo relativo della tabella offset globali: gotofflo16(simbolo) atteso indirizzo relativo del puntatore globale: gp(simbolo) persi i bit di flag dell'indirizzo limm di salto maschera mfcr non valida ignorata maschera di bit non consentita uso non consentito delle parentesi imm:6 l'immediato è fuori dall'intervallo l'immediato è fuori dall'intervallo 0-7 l'immediato è fuori dall'intervallo 1-2 l'immediato è fuori dall'intervallo 1-8 l'immediato è fuori dall'intervallo 2-9 il valore dell'immediato non può essere registro il valore dell'immediato è fuori dall'intervallo valore dell'immediato fuori dall'intervallo memorizzazione impossibile registro indice nell'intervallo load qui %function() non è valida opzione condizionale non valida costante non valida insn load/shimm non valido campo di maschera non valido operando non valido,  il tipo corrispondente può solo avere valori 0,1,2. registro non valido per la regolazione dello stack Nome di registro non valido numero di registro non valido "%d" operando del registro non valido durante l'aggiornamento numero di registro di uso speciale (sprg) non valido flag di salto, ma .f non presente flag di salto, ma indirizzo limm non presente suggerimento di salto non allineato spazzatura alla fine della riga errore dell'operando ld ")" mancante "]" mancante Mnemonico mancante nella stringa di sintassi il formato più recente "%s"
appare più restrittivo di "%s"
 note multiple %s non gestite
 deve essere specificato .jd o un suffisso non invalidante nessun insn mappato direttamente sull'IC terminale %s
 nessun insn mappato direttamente sull'IC terminale %s [%s] non è una coppia r0l/r0h valida l'offset(IP) non è valido l'opcode %s non ha una classe (operandi %d %d %d)
 operando fuori dall'intervallo (%ld non è tra %ld e %ld) operando fuori dall'intervallo (%ld non è tra %ld e %lu) operando fuori dall'intervallo (%lu non è tra %lu e %lu) operando fuori dall'intervallo (0x%lx non è tra 0 e 0x%lx) operando fuori dall'intervallo (non è tra 1 e 255) campo sovrapposto %s->%s
 sovrascrittura della nota %d con la nota %d (IC:%s)
 parse_addr16: indice dell'operando non valido. l'operando dell'operatore percentuale non è un simbolo il numero di registro deve essere pari la risorsa %s (%s) non ha registri
 gli operandi del registro sorgente e obiettivo devono essere diversi errore dell'operando st il valore di memoria deve essere zero errore di sintassi (carattere atteso "%c", trovato "%c") errore di sintassi (carattere atteso "%c", trovata fine dell'istruzione) troppe costanti long troppi shimm in load impossibile cambiare la directory a "%s", errno = %s
 impossibile adattare costanti di valore diverso nell'istruzione indefinito sconosciuta sconosciuto	0x%02lx sconosciuto	0x%04lx vincolo sconosciuto "%c" scorrimento dell'operando sconosciuto: %x
 registro sconosciuto: %d
 forma dell'istruzione non riconosciuta istruzione non riconosciuta attenzione: opzione sconosciuta -M%s ignorata
 