��          t      �              +   1  #   ]  6   �     �     �  0   �  /   $  )   T  x   ~  �  �  $   �  6   �  &   �  >   $     c  #     2   �  +   �  /     r   2                                 
      	                     Could not create LUKS device %s Could not format device with file system %s Creating encrypted device on %s...
 Error: could not generate temporary mapped device name Error: device mounted: %s
 Error: invalid file system: %s
 Please enter your passphrase again to verify it
 The passphrases you entered were not identical
 This program needs to be started as root
 luksformat - Create and format an encrypted LUKS device
Usage: luksformat [-t <file system>] <device> [ mkfs options ]

 Project-Id-Version: cryptsetup
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-03 20:26+0100
PO-Revision-Date: 2012-09-27 09:13+0000
Last-Translator: Sergio Zanchetta <primes2h@ubuntu.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:25+0000
X-Generator: Launchpad (build 18115)
 Impossibile creare il device LUKS %s Impossibile formattare il device con il file system %s Creazione del device cifrato su %s...
 Errore: impossibile generare nomi temporanei di device mappati Errore: device montato: %s
 Errore: file system non valido: %s
 Inserire nuovamente la passphrase per verificarla
 Le passphrase inserite non erano identiche
 Questo programma deve essere avviato come root
 luksformat - Crea e formatta un device LUKS cifrato
Uso: luksformat [-t <file system>] <device> [ opzioni mkfs ]

 