��    s      �  �   L      �	  1  �	     �
       &        >  ,   F     s     �     �  �   �  �   M            	        (  F   0  *   w  :   �     �     �            ,   :     g     s  &   �  &   �  !   �     �     �       O   4  .   �     �     �     �     �     �  �     A   �     �  K     "   c      �     �     �     �     �          "  %   B  G   h     �  2  �  O   �     B     S     _     k  #   �  "   �     �  8   �  '     )   C     m     �     �     �  2   �  0   �     "     B     V  (   l  �   �  I   B  ;   �  "   �  �   �  ,   �  Q   �  �         �  A     ,   H     u     �     �     �     �  Y   �  O   ,  4   |     �  A   �  
   �       �     V   �  D   �  @   3  P   t  A   �  L     "   T     w  	   �     �     �     �     �  
   �     �  �  �  ?  �      "     +"  (   3"     \"  F   o"     �"     �"     �"  �   �"  �   �#     t$     �$  	   �$     �$  T   �$  :   %  A   A%      �%     �%      �%  #   �%  7   �%     0&     <&  #   Y&  4   }&  3   �&     �&     �&  &   
'  ]   1'  7   �'     �'  "   �'     �'  !    (  $   "(  �   G(  X   )     h)  @   ~)  #   �)  3   �)  $   *  '   <*  '   d*     �*     �*  )   �*  )   �*  j   +     }+  T  �+  U   �,     <-     W-     h-  %   x-  $   �-  /   �-     �-  :   .     I.     e.     �.     �.     �.  !   �.  2   �.  I   #/     m/     �/     �/  P   �/  �   0  g   �0  W   *1  $   �1  �   �1     f2  U   �2  �   �2  (   �3  H   �3  C   94     }4  $   �4     �4     �4     �4  Q   �4  M   I5  :   �5     �5  E   �5     &6     26  �   A6  m   �6  E   ]7  :   �7  @   �7  D   8  b   d8  &   �8     �8  	    9     
9     9     :9     N9     a9     j9        +                  $   W   7          k   '                @   g   K      9   R           3      6       D   C               <   .   f      L   l       	   Q   X       e          s      Z   ?   N   P   &   F          4           a   B   ]   [               (                 "   p   ,   c   b   1   U   j   5       /   A   :          #   G      E      d       
   _          S      h   -      n              J       m   \   M       `               2       )       >   T       0      i   *   %   o   ;   V   8             r   !   ^       =       I   q                            H   Y      O    
A normal upgrade can not be calculated, please run:
  sudo apt-get dist-upgrade


This can be caused by:
 * A previous upgrade which didn't complete
 * Problems with some of the installed software
 * Unofficial software packages not provided by Ubuntu
 * Normal changes of a pre-release version of Ubuntu %(size).0f kB %(size).0f kB %.1f MB %i obsolete entries in the status file %s base %s needs to be marked as manually installed. %s will be downloaded. .deb package A file on disk An unresolvable problem occurred while calculating the upgrade.

Please report this bug against the 'update-manager' package and include the following error message:
 An unresolvable problem occurred while initializing the package information.

Please report this bug against the 'update-manager' package and include the following error message:
 Building Updates List Cancel Changelog Changes Changes for %s versions:
Installed version: %s
Available version: %s

 Check if a new Ubuntu release is available Check if upgrading to the latest devel release is possible Checking for updates… Connecting... Copy Link to Clipboard Could not calculate the upgrade Could not initialize the package information Description Details of updates Directory that contains the data files Do not check for updates when starting Do not focus on map when starting Download Downloading changelog Downloading list of changes... Failed to download the list of changes. 
Please check your Internet connection. However, %s %s is now available (you have %s). Install Install All Available Updates Install Now Install missing package. Installing updates… It is impossible to install or remove any software. Please use the package manager "Synaptic" or run "sudo apt-get install -f" in a terminal to fix this issue at first. It’s safer to connect the computer to AC power before updating. No longer downloadable: No network connection detected, you can not download changelog information. No software updates are available. Not all updates can be installed Not enough free disk space Obsolete dpkg status entries Obsolete entries in dpkg status Open Link in Browser Other updates Package %s should be installed. Please wait, this can take some time. Remove lilo since grub is also installed.(See bug #314004 for details.) Restart _Later Run a partial upgrade, to install as many updates as possible.

    This can be caused by:
     * A previous upgrade which didn't complete
     * Problems with some of the installed software
     * Unofficial software packages not provided by Ubuntu
     * Normal changes of a pre-release version of Ubuntu Run with --show-unsupported, --show-supported or --show-all to see more details Security updates Select _All Settings… Show all packages in a list Show all packages with their status Show and install available updates Show debug messages Show description of the package instead of the changelog Show supported packages on this machine Show unsupported packages on this machine Show version and exit Software Updater Software Updates Software index is broken Software updates are no longer provided for %s %s. Some software couldn’t be checked for updates. Support status summary of '%s': Supported until %s: Technical description Test upgrade with a sandbox aufs overlay The changelog does not contain any relevant changes.

Please use http://launchpad.net/ubuntu/+source/%s/%s/+changelog
until the changes become available or try again later. The computer also needs to restart to finish installing previous updates. The computer needs to restart to finish installing updates. The computer will need to restart. The list of changes is not available yet.

Please use http://launchpad.net/ubuntu/+source/%s/%s/+changelog
until the changes become available or try again later. The software on this computer is up to date. The update has already been downloaded. The updates have already been downloaded. The upgrade needs a total of %s free space on disk '%s'. Please free at least an additional %s of disk space on '%s'. Empty your trash and remove temporary packages of former installations using 'sudo apt-get clean'. There are no updates to install. This update does not come from a source that supports changelogs. To stay secure, you should upgrade to %s %s. Unimplemented method: %s Unknown download size. Unsupported Unsupported:  Update is complete Updated software has been issued since %s %s was released. Do you want to install it now? Updated software is available for this computer. Do you want to install it now? Updated software is available from a previous check. Updates Upgrade using the latest proposed version of the release upgrader Upgrade… Version %s: 
 When upgrading, if kdelibs4-dev is installed, kdelibs5-dev needs to be installed. See bugs.launchpad.net, bug #279621 for details. You are connected via roaming and may be charged for the data consumed by this update. You have %(num)s packages (%(percent).1f%%) supported until %(time)s You have %(num)s packages (%(percent).1f%%) that are unsupported You have %(num)s packages (%(percent).1f%%) that can not/no-longer be downloaded You may not be able to check for updates or download new updates. You may want to wait until you’re not using a mobile broadband connection. You stopped the check for updates. _Check Again _Continue _Deselect All _Partial Upgrade _Remind Me Later _Restart Now… _Try Again updates Project-Id-Version: update-manager
Report-Msgid-Bugs-To: sebastian.heinlein@web.de
POT-Creation-Date: 2016-10-01 10:01+0000
PO-Revision-Date: 2016-10-01 16:47+0000
Last-Translator: Alessandro Ranaldi <ranaldialessandro@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-10-09 11:01+0000
X-Generator: Launchpad (build 18227)
Language: it
 
Non è possibile calcolare un normale aggiornamento, eseguire:
  sudo apt-get dist-upgrade


Questo può dipendere da:
 * un precedente aggiornamento non completato
 * problemi con alcuni programmi installati
 * pacchetti non ufficiali non forniti da Ubuntu
 * normali cambiamenti di una versione in sviluppo di Ubuntu %(size).0f kB %(size).0f kB %.1f MB %i voci non aggiornate nel file di stato Componenti base %s È necessario selezionare il pacchetto %s come installato manualmente. Verranno scaricati %s. Pacchetto .deb Un file su disco Si è verificato un errore irreversibile durante il controllo degli aggiornamenti.

Segnalare il problema del pacchetto «update-manager» includendo il seguente messaggio di errore:
 Si è verificato un problema irrisolvibile durante l'inizializzazione delle informazioni del pacchetto.

Segnalare questo bug per il pacchetto «update-manager» e includere il seguente messaggio d'errore:
 Creazione elenco aggiornamenti Annulla Changelog Cambiamenti Cambiamenti delle versioni di %s:
Versione installata: %s
Versione disponibile: %s

 Controlla la disponibilità di un nuovo rilascio di Ubuntu Verifica se è possibile avanzare all'ultima versione di sviluppo Controllo degli aggiornamenti… Connessione… Copia collegamento negli appunti Impossibile calcolare l'avanzamento Impossibile inizializzare le informazioni del pacchetto Descrizione Dettagli degli aggiornamenti Directory contenente i file di dati Non controlla la presenza di aggiornamenti all'avvio Avvia senza dare il focus alla finestra del gestore Da scaricare Scaricamento changelog Scaricamento elenco delle modifiche… Scaricamento dell'elenco dei cambiamenti non riuscito. 
Verificare la connessione a Internet. Tuttavia, %s %s è ora disponibile (versione in uso %s) Installa Installa aggiornamenti disponibili Installa ora Installare il pacchetto mancante. Installazione degli aggiornamenti… Impossibile installare o rimuovere alcun software. Utilizzare il gestore dei pacchetti «Synaptic» o eseguire «sudo apt-get install -f» in un terminale per correggere innanzitutto questo problema. Per sicurezza collegare il computer alla rete elettrica prima di eseguire aggiornamenti. Non più scaricabili: Nessuna connessione di rete, impossibile scaricare il changelog. Non sono disponibili aggiornamenti. Non è possibile installare tutti gli aggiornamenti Spazio libero su disco insufficiente Voci dello stato di dpkg non aggiornate Voci non aggiornate nello stato di dpkg Apri collegamento nel browser Altri aggiornamenti È necessario installare il pacchetto %s. Attendere, potrebbe richiedere del tempo. È necessario rimuovere lilo per la presenza di grub. Per ulteriori dettagli consultare il bug n° 314004. Riavvia in _seguito Eseguire un avanzamento parziale per installare il maggior numero possibile di aggiornamenti.

    Questo può dipendere da:
     * un precedente aggiornamento non completato
     * problemi con alcuni programmi installati
     * pacchetti non ufficiali non forniti da Ubuntu
     * normali cambiamenti di una versione in sviluppo di Ubuntu Eseguire con --show-unsupported, --show-supported o --show-all per ulteriori dettagli Aggiornamenti di sicurezza _Seleziona tutto Impostazioni… Mostra tutti i pacchetti in un elenco Mostra lo stato di tutti i pacchetti Mostra e installa gli aggiornamenti disponibili Mostra i messaggi di debug Mostra la descrizione del pacchetto al posto del changelog Mostra pacchetti supportati Mostra pacchetti non supportati Mostra la versione ed esce Aggiornamenti software Aggiornamenti software L'indice del software è rovinato Gli aggiornamenti non sono più forniti per %s %s. Non è stato possibile controllare gli aggiornamenti di alcuni programmi. Riepilogo supporto per «%s»: Supportati fino a %s: Descrizione tecnica Prova di avanzamento in ambiente sandbox con sovrapposizione di file system aufs Il changelog non contiene alcuna variazione rilevante.

Consultare http://launchpad.net/ubuntu/+source/%s/%s/+changelog
finché i cambiamenti non sono disponibili o riprovare più tardi. È inoltre necessario riavviare il computer per completare l'installazione di aggiornamenti precedenti. È necessario riavviare il computer per completare l'installazione degli aggiornamenti. Il computer dovrà essere riavviato. L'elenco dei cambiamenti non è ancora disponibile.

Utilizzare http://launchpad.net/ubuntu/+source/%s/%s/+changelog
finché le modifiche non saranno disponibili oppure riprovare più tardi. Il software è aggiornato. L'aggiornamento è già stato scaricato. Gli aggiornamenti sono già stati scaricati. L'avanzamento necessita di %s di spazio libero sul disco «%s»: liberare almeno altri %s di spazio sul disco «%s». Svuotare il cestino e rimuovere i pacchetti temporanei di precedenti installazioni con il comando «sudo apt-get clean». Non ci sono aggiornamenti da installare. Questo aggiornamento non proviene da una fonte che supporta i changelog. Per maggiore sicurezza, si dovrebbe eseguire l'avanzamento a %s %s. Metodo non implementato: %s Dimensione scaricamento sconosciuta. Non supportato Non supportati:  Aggiornamento completato Dal rilascio di %s %s sono stati forniti aggiornamenti software. Installarli ora? Sono disponibili aggiornamenti software per questo computer. Installarli ora? Sono disponibili aggiornamenti da un controllo precedente. Aggiornamenti Usa l'ultima versione proposta del sistema di avanzamento di versione Aggiorna… Versione %s: 
 Se è installato kdelibs4-dev, durante l'avanzamento sarà necessario installare kdelibs5-dev. Per maggiori informazioni, consultare il bug n° 279621 su bugs.launchpad.net. Connessione eseguita in roaming: è possibile che il costo sia basato sui dati scaricati per l'aggiornamento. Ci sono %(num)s pacchetti (%percent).1f%%) supportati fino a %(time)s Ci sono %(num)s pacchetti (%(percent).1f%%) non supportati Ci sono %(num)s pacchetti (%(percent).1f%%) non più scaricabili Potrebbe non essere possibile controllare e scaricare aggiornamenti. Si consiglia di attendere fino a quando non si starà usando una connessione a banda larga mobile. Controllo degli aggiornamenti fermato. _Controlla ancora _Continua _Deseleziona tutto Esegui avanzamento _parziale _Ricorda in seguito _Riavvia adesso… _Riprova aggiornamenti 