��    9      �  O   �      �     �               $     7  '   H  ;   p  �   �     V  !   c     �  +   �     �     �     �     �     �  %        .  4   I     ~     �     �      �     �     �     
          &     2     A  "   J      m     �     �     �     �      �     �     �     	     	  (   	     A	     M	     Z	  	   c	     m	     t	     |	     �	     �	     �	     �	     �	     �	  *  �	  !        *     >     V     f  :   v  9   �  �   �     �  *   �     �  D   �            	   '  ,   1  !   ^  5   �  *   �  @   �     "  "   :     ]  -   w     �  '   �     �     �             
   4     ?  ,   Z     �     �     �  
   �      �  %   �               *  /   6     f     z     �     �     �     �     �     �     �     �            b  1     .                3            ,                              6                7   9          	             -       %             +   !                      $         (       &           #   '          8   /   
   4          "       1      )            2       *   0       5            "%s" is not a valid folder %d day %d days %d item %d items %d month %d months %d year %d years A graphical tool to analyze disk usage. A list of URIs for partitions to be excluded from scanning. A simple application which can scan either specific folders (local or remote) or volumes and give a graphical representation including each directory size or percentage. Active Chart Apparent sizes are shown instead. Baobab Check folder sizes and available disk space Close Computer Contents Could not analyze disk usage. Could not analyze volume. Could not detect occupied disk sizes. Could not scan folder "%s" Could not scan some of the folders contained in "%s" Devices and locations Disk Usage Analyzer Excluded partitions URIs Failed to move file to the trash Failed to open file Failed to show help Folder Go to _parent folder Home folder Mo_ve to Trash Modified Print version information and exit Recursively analyze mount points Rings Chart Scan Folder… Select Folder Size The GdkWindowState of the window The initial size of the window Today Treemap Chart Unknown Which type of chart should be displayed. Window size Window state Zoom _in Zoom _out _About _Cancel _Copy Path to Clipboard _Help _Open _Open Folder _Quit storage;space;cleanup; translator-credits Project-Id-Version: baobab
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=baobab&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 14:31+0000
PO-Revision-Date: 2015-07-20 13:56+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:41+0000
X-Generator: Launchpad (build 18115)
Language: it
 «%s» non è una cartella valida %d giorno %d giorni %d elemento %d elementi %d mese %d mesi %d anno %d anni Uno strumento grafico per analizzare l'utilizzo del disco. Un elenco di URI di partizioni da escludere dall'analisi. Un'applicazione per analizzare delle cartelle specifiche (locali o remote) o volumi che fornisce una rappresentazione grafica delle directory, incluse le dimensioni. Grafico attivo Vengono mostrate le dimensioni ipotetiche. Baobab Controlla la dimensione delle cartelle e lo spazio disco disponibile Chiudi Computer Contenuti Impossibile analizzare l'utilizzo del disco. Impossibile analizzare il volume. Impossibile determinare lo spazio occupato sul disco. Impossibile analizzare la cartella «%s». Impossibile analizzare alcune delle cartelle contenute in «%s» Dispositivi e posizioni Analizzatore di utilizzo del disco URI di partizioni escluse Spostamento del file nel cestino non riuscito Apertura del file non riuscita Visualizzazione dell'aiuto non riuscita Cartella _Vai alla cartella superiore Cartella home S_posta nel cestino Modificato Stampa la versione ed esce Analizza ricorsivamente i punti di montaggio Grafico ad anelli Analizza cartella... Seleziona cartella Dimensione Il GdkWindowState della finestra La dimensione iniziale della finestra Oggi Grafico ad albero Sconosciuto Quale tipo di grafico deve essere visualizzato. Dimensione finestra Stato della finestra Aum_enta ingrandimento Rid_uci ingrandimento I_nformazioni A_nnulla Co_pia percorso negli appunti A_iuto _Apri _Apri cartella _Esci archiviazione;spazio;pulizia; Milo Casagrande <milo@ubuntu.com>
Andrea Zagli <azagli@libero.it>
Alessio Frusciante <algol@firenze.linux.it>
Lapo Calamandrei <lapo@it.gnome.org>
Luca Ferretti <elle.uca@infinito.it>
...e tutti i revisori del Translation Project.

Launchpad Contributions:
  Andrea Zagli https://launchpad.net/~azagli-libero
  Milo Casagrande https://launchpad.net/~milo 