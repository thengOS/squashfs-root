��    U      �  q   l      0  "   1     T  8   t  <   �  5   �  >      $   _  2   �  B   �  2   �  >   -	  G   l	  =   �	  ?   �	  :   2
  0   m
  %   �
     �
     �
  $   �
  Z        r     �  )   �     �  w   �     n     �  *   �     �  #   �  !     "   3      V     w     �  )   �  &   �  E   �  H   @  $   �  '   �  .   �  -     !   3     U  "   k  <   �  B   �  C     *   R  C   }  *   �  C   �  *   0      [  ,   |  D   �     �  F     /   N  -   ~     �  ;   �     �     
  !   "  $   D     i     �     �      �     �     �               8     U  .   s  -   �     �  9   �     *     F  �  f  %   9  '   _  H   �  M   �  `     c     /   �  7     G   K  Q   �  C   �  n   )  F   �  v   �  M   V  =   �  )   �  "        /  .   H  h   w  !   �  "     3   %  #   Y  �   }           7  ,   P     }  $   �  *   �  +   �  )     &   @     g  9   �  ,   �  I   �  L   7   0   �   *   �   1   �   E   !  %   X!     ~!  &   �!  B   �!  K   "  N   S"  4   �"  P   �"  8   (#  P   a#  8   �#  "   �#  '   $  ]   6$     �$  E   �$  <   �$  ;   3%     o%  J   �%     �%     �%  &   �%  )   &     F&  $   [&     �&      �&     �&     �&     �&  #   '  (   *'  )   S'  2   }'  .   �'  +   �'  <   (  "   H(  $   k(             >   P          D   %            K      1      8   O      ,       U   /   M   $      )   -       L   J   
              +   Q             '               *               R   	         !   :   9   5       0   4   #       ?          =       2          <               6   3           E       B      N         H   &       A      T   C   (              G       .          I   "   S      F              7   ;      @    !
Ensure that it has been loaded.
 %s: ASSERT: Invalid option: %d
 %s: Could not allocate memory for subdomain mount point
 %s: Could not allocate memory for subdomainbase mount point
 %s: Errors found during regex postprocess. Aborting.
 %s: Errors found in combining rules postprocessing. Aborting.
 %s: Errors found in file. Aborting.
 %s: Failed to compile regex '%s' [original: '%s']
 %s: Failed to compile regex '%s' [original: '%s'] - malloc failed
 %s: Illegal open {, nesting groupings not allowed
 %s: Internal buffer overflow detected, %d characters exceeded
 %s: Regex grouping error: Invalid close }, no matching open { detected
 %s: Regex grouping error: Invalid number of items between {}
 %s: Regex grouping error: Unclosed grouping, expecting close }
 %s: Sorry. You need root privileges to run this program.

 %s: Subdomain '%s' defined, but no parent '%s'.
 %s: Two SubDomains defined for '%s'.
 %s: Unable to add "%s".   %s: Unable to find  %s: Unable to parse input line '%s'
 %s: Unable to query modules - '%s'
Either modules are disabled or your kernel is too old.
 %s: Unable to remove "%s".   %s: Unable to replace "%s".   %s: Unable to write entire profile entry
 %s: Unable to write to stdout
 %s: Warning! You've set this program setuid root.
Anybody who can run this program can update your AppArmor profiles.

 %s: error near                %s: error reason: '%s'
 (ip_mode) Found unexpected character: '%s' Addition succeeded for "%s".
 AppArmor parser error, line %d: %s
 Assert: 'hat rule' returned NULL. Assert: `addresses' returned NULL. Assert: `netrule' returned NULL. Assert: `rule' returned NULL. Bad write position
 Couldn't copy profile Bad memory address
 Couldn't merge entries. Out of Memory
 Default allow subdomains are no longer supported, sorry. (domain: %s) Default allow subdomains are no longer supported, sorry. (domain: %s^%s) ERROR in profile %s, failed to load
 Error couldn't allocate temporary file
 Error: #include %s%c not found. line %d in %s
 Error: Can't add directory %s to search path
 Error: Could not allocate memory
 Error: Out of Memory
 Error: bad include. line %d in %s
 Error: could not allocate buffer for include. line %d in %s
 Error: exceeded %d levels of includes.  NOT processing %s include
 Exec qualifier 'i' invalid, conflicting qualifier already specified Exec qualifier 'i' must be followed by 'x' Exec qualifier 'p' invalid, conflicting qualifier already specified Exec qualifier 'p' must be followed by 'x' Exec qualifier 'u' invalid, conflicting qualifier already specified Exec qualifier 'u' must be followed by 'x' Found unexpected character: '%s' Internal: unexpected mode character in input Invalid mode, 'x' must be preceded by exec qualifier 'i', 'u' or 'p' Memory allocation error. Negative subdomain entries are no longer supported, sorry. (entry: %s) Network entries can only have one FROM address. Network entries can only have one TO address. Out of memory
 PANIC bad increment buffer %p pos %p ext %p size %d res %p
 Permission denied
 Profile already exists
 Profile does not match signature
 Profile doesn't conform to protocol
 Profile doesn't exist
 Profile version not supported
 Removal succeeded for "%s".
 Replacement succeeded for "%s".
 Unable to open %s - %s
 Unknown error
 Warning (line %d):  `%s' is not a valid ip address. `%s' is not a valid netmask. `/%d' is not a valid netmask. md5 signature given without execute privilege. missing an end of line character? (entry: %s) ports must be between %d and %d profile %s: has merged rule %s with multiple x modifiers
 unable to create work area
 unable to serialize profile %s
 Project-Id-Version: apparmor-parser
Report-Msgid-Bugs-To: <apparmor@lists.ubuntu.com>
POT-Creation-Date: 2005-03-31 13:39-0800
PO-Revision-Date: 2016-09-29 02:27+0000
Last-Translator: Claudio Arseni <claudio.arseni@gmail.com>
Language-Team: Novell Language <language@novell.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-10-09 11:30+0000
X-Generator: Launchpad (build 18227)
Language: it
 !
Verificare che sia stato caricato.
 %s: ASSERZIONE: opzione non valida: %d
 %s: impossibile allocare memoria per il punto di mount del sottodominio
 %s: impossibile allocare memoria per il punto di montaggio base sottodominio
 %s: errori individuati nella fase di post-elaborazione dell'espressione regolare. Interruzione.
 %s: individuati errori durante la post-elaborazione della combinazione delle regole. Interruzione.
 %s: errori individuati nel file. Interruzione.
 %s: Impossibile compilare regex '%s' [originale: '%s']
 %s: Impossibile compilare regex '%s' [originale: '%s'] - errore malloc
 %s: parantesi {di apertura non valida, annidamento raggruppamenti non consentito
 %s: individuato overflow del buffer interno, superati %d caratteri
 %s: errore raggruppamento regex: parentesi } di chiusura non valida, non è stata individuata alcuna { aperta
 %s: errore raggruppamento regex: numero di elementi non valido tra {}
 %s: errore nel raggruppamento dell'espressione regolare; raggruppamento non chiuso, attesa la parentesi di chiusura }
 %s: errore. Sono richiesti privilegi di root per eseguire questo programma.

 %s: sottodominio «%s» definito, ma nessun genitore «%s».
 %s: Definiti due sottodomini per «%s».
 %s: Impossibile aggiungere "%s".   %s: Impossibile trovare  %s: impossibile analizzare la riga input "%s"
 %s: Impossibile eseguire richieste su moduli - '%s'
I moduli sono disabilitati o il kernel è obsoleto.
 %s: Impossibile rimuovere "%s".   %s: Impossibile sostituire "%s".   %s: impossibile scrivere l'intera voce del profilo
 %s: Impossibile scrivere su stdout
 %s: attenzione. È stato impostato il root setuid di questo programma.
Chiunque possa eseguire questo programma può aggiornare i profili di AppArmor.

 %s: errore vicino                %s: motivo errore: '%s'
 (ip_mode) Trovato carattere imprevisto: '%s' Aggiunta riuscita per "%s".
 Errore parser AppArmor, riga %d: %s
 Asserzione: "hat rule" ha restituito NULL. Asserzione: `addresses' ha restituito NULL. Asserzione: `netrule' ha restituito NULL. Asserzione: "rule" ha restituito NULL. Posizione di scrittura errata
 Impossibile copiare profilo; indirizzo di memoria errato
 Impossibile unire le voci: memoria esaurita
 I sottodomini predefiniti "allow" non sono più supportati (dominio: %s). I sottodomini predefiniti "allow" non sono più supportati (dominio: %s^%s). ERRORE nel profilo %s, caricamento non riuscito
 Errore di allocazione del file temporaneo
 Errore: #include %s%c non trovato; riga %d in %s
 Errore: impossibile aggiungere la cartella %s al percorso di ricerca
 Errore: impossibile allocare memoria
 Errore: memoria insufficiente
 Errore: include errato; riga %d in %s
 Errore: impossibile allocare il buffer per include; riga %d in %s
 Errore: superati i %d livelli di include. %s include non saranno elaborati
 Qualificatore Exec "i" non valido: qualificatore in conflitto già specificato Il qualificatore Exec 'i' deve essere seguito da 'x' Qualificatore exec «p» non valido, qualificatore in conflitto già specificato Il qualificatore exec «p» deve essere seguito da «x» Qualificatore exec «u» non valido, qualificatore in conflitto già specificato Il qualificatore exec «u» deve essere seguito da «x» Trovato carattere imprevisto: "%s" Interno: carattere inaspettato in input Modalità non valida, «x» deve essere preceduto dai qualificatori exec «i», «u» o «p» Errore allocazione memoria. Le voci dei sottodomini negativi non sono più supportate (voce: %s). Le registrazioni di rete possono avere solo un indirizzo DA. Le registrazioni di rete possono avere solo un indirizzo A. Memoria esaurita
 ATTENZIONE buffer incremento errato %p pos %p est %p dimensione %d ris %p
 Permesso negato
 Profilo già esistente
 Il profilo non corrisponde alla firma
 Il profilo non è conforme al protocollo
 Profilo inesistente
 Versione del profilo non supportata
 Rimozione riuscita per "%s".
 Sostituzione riuscita per "%s".
 Impossibile aprire %s - %s
 Errore sconosciuto
 Attenzione (riga %d):  `%s' non è un indirizzo IP valido. `%s' non è una maschera di rete valida. `/%d' non è una maschera di rete valida. restituita firma md5 senza privilegi di esecuzione un carattere di fine riga mancante? (voce: %s) le porte devono essere comprese tra %d e %d profilo %s: presenta regola fusa %s con più modificatori x
 impossibile creare area di lavoro
 impossibile serializzare profilo %s
 