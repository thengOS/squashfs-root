��    =        S   �      8     9     N  ?   h     �     �  ,   �     �     �                    !  4   >     s     �     �     �     �     �     �     �     �     �  .        5     P     U  C   Z     �  �   �  `   L     �     �  w   �     <	  P   W	     �	     �	  �   �	     �
     �
     �
  	        (     7     ?     O     \     j     �  	   �     �     �  
   �     �     �     �     �     �     �  �  �     y     �  O   �       !   	  :   +     f     k     �     �     �  "   �  =   �                                     7     O     X     \  4   |     �     �     �  N   �     /  �   I  w        �     �  �   �  "   D  Y   g     �  1   �              8  )   R  	   |     �     �     �  
   �     �     �     �  	   �     �       
                  #     ,     2     6         8   <   -   4   /            )   5   1      =      7   "   !                        #                 6                               ;       
             +      9                 '                      (   :   $   .   %   2      ,                       	       &                 3      *   0    Add phrases in front Add phrases in the front. Always input numbers when number keys from key pad is inputted. Auto Auto move cursor Automatically move cursor to next character. Big5 Candidate per page Chewing Chewing component Chi Choose phrases from backward Choose phrases from the back, without moving cursor. ConfigureApply ConfigureCancel ConfigureClose ConfigureOk ConfigureSave Easy symbol input Easy symbol input. Editing Eng Esc clean all buffer Escape key cleans the text in pre-edit-buffer. Force lowercase in En mode Full Half Hsu's keyboard selection keys, 1 for asdfjkl789, 2 for asdfzxcv89 . Hsu's selection key Ignore CapsLock status and input lowercase by default.
It is handy if you wish to enter lowercase by default.
Uppercase can still be inputted with Shift. In plain Zhuyin mode, automatic candidate selection and related options are disabled or ignored. Keyboard Keyboard Type Keys used to select candidate. For example "asdfghjkl;", press 'a' to select the 1st candidate, 's' for 2nd, and so on. Maximum Chinese characters Maximum Chinese characters in pre-edit buffer, including inputing Zhuyin symbols Number of candidate per page. Number pad always input number Occasionally, the CapsLock status does not match the IM, this option determines how these status be synchronized. Valid values:
"disable": Do nothing.
"keyboard": IM status follows keyboard status.
"IM": Keyboard status follows IM status. Peng Huang, Ding-Yi Chen Plain Zhuyin mode Select Zhuyin keyboard layout. Selecting Selection keys Setting Space to select Syncdisable Synckeyboard Sync between CapsLock and IM UTF8 dachen_26 default dvorak dvorak_hsu eten eten26 gin_yieh hanyu hsu ibm Project-Id-Version: ibus-chewing 1.4.11
Report-Msgid-Bugs-To: Ding-Yi Chen <dchen at redhat.com>
POT-Creation-Date: 2016-01-21 05:14+0000
PO-Revision-Date: 2015-09-04 20:08+0000
Last-Translator: dchen <Unknown>
Language-Team: Italian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:39+0000
X-Generator: Launchpad (build 18115)
Language: it
 Aggiungi espressioni davanti Aggiungi espressioni davanti. Inserisci sempre i numeri quando i tasti numerici della tastiera sono inseriti. Auto Sposta automaticamente il cursore Sposta automaticamente il cursore al carattere successivo. Big5 Candidato per pagina Chewing Componente Chewing Chi Seleziona espressioni all'indietro Seleziona espressioni all'indietro senza spostare il cursore. Applica Annulla Chiudi OK Salva Input simbolo semplice Input simbolo semplice. Modifica Eng Pulisci con Esc tutto il buffer Il tasto Escape rimuove il testo in pre-edit-buffer. Forza minuscolo in modalità En Completo Metà Tasti di selezione della tastiera di Hsu, 1 per asdfjkl789, 2 per asdfzxcv89 . Tasto di selezione di Hsu Ignora CapsLock ed input minuscolo per impostazione predefinita. 
È utile se inserisci il minuscolo per impostazione predefinita.
Il maiuscolo può ancora essere inserito con il tasto Maiusc "Shift". In modalità semplice Zhuyin, la selezione del candidato automatica e le opzioni relative sono disabilitate o ignorate. Tastiera Tipo di tastiera Tasti usati per selezionare il candidato. Per esempio "asdfghjkl;", premere 'a' per selezionare il primo candidato, 's' per selezionare il secondo e così via. Numero massimo di caratteri cinesi Numero massimo di caratteri cinesi in pre-edit buffer, incluso l'input dei simboli Zhuyin Numero di candicati per pagina. Il tastierino numerico inserisce sempre il numero Talvolta lo stato di Capslock non corrisponde all'IM, questa opzione determina come sincronizzare lo stato. I valori validi sono:¶
"disable": Non fare niente.¶
"keyboard": lo stato IM segue lo stato della tastiera.¶
"IM": lo stato della tastiera segue lo stato IM. Peng Huang, Ding-Yi Chen Modalità Zhuyin semplice Seleziona il layout della tastiera Zhuyin Selezione Tasti di selezione Impostazione Spazio da selezionare Disabilita Tastiera Sincronizza tra CapsLock e IM UTF8 dachen_26 default dvorak dvorak_hsu eten eten26 gin_yieh hanyu hsu ibm 