��            )         �  "   �     �  &   �          #     A     Y     f     k      ~     �     �  
   �     �  
   �     �       %        D     T     n     �  e   �     �          5  	   D  !   N  /   p     �  �  �  #   U     y  6   �  #   �  &   �          .     :     C  #   ^  '   �  (   �     �     �  
   �  &   �     !	  1   .	     `	  /   p	     �	     �	  }   �	  !   ?
  %   a
     �
  
   �
  +   �
  +   �
  	   �
                                                   
                     	                                                                - GNOME mouse accessibility daemon Button Style Button style of the click-type window. Click-type window geometry Click-type window orientation Click-type window style Double Click Drag Enable dwell click Enable simulated secondary click Failed to Display Help Hide the click-type window Horizontal Hover Click Icons only Ignore small pointer movements Orientation Orientation of the click-type window. Secondary Click Set the active dwell mode Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Start mousetweaks as a daemon Start mousetweaks in login mode Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Project-Id-Version: mousetweaks
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 23:45+0000
PO-Revision-Date: 2012-03-13 08:17+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:24+0000
X-Generator: Launchpad (build 18115)
Language: it
 - Demone GNOME accessibilità mouse Stile dei pulsanti Stile dei pulsanti della finestra per il tipo di clic. Geometria finestra per tipo di clic Orientamento finestra per tipo di clic Stile finestra per tipo di clic Doppio-clic Trascina Abilita il clic automatico Abilita il clic secondario simulato Visualizzazione dell'aiuto non riuscita Nasconde la finestra per il tipo di clic Orizzontale Clic automatico Solo icone Ignora movimenti piccoli del puntatore Orientamento L'orientamento della finestra per il tipo di clic Clic secondario Imposta la modalità attiva del clic automatico Arresta mousetweaks Clic singolo Dimensione e posizione della finestra per il tipo di clic. Il formato è la classica stringa di geometria di X Window System. Avvia il programma come un demone Avvia il programma in modalità login Testo e icone Solo testo Tempo di attesa prima di un clic automatico Tempo di attesa prima di un clic secondario Verticale 