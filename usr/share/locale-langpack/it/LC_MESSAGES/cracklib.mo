��          �      ,      �     �  /   �     �     �  +   �      '     H  $   e  (   �  &   �     �     �  *   
  '   5  %   ]  +   �  �  �  !   �  ,   �     �  +   �  0     #   @     d  (   �  5   �  '   �     
           9  5   Z  '   �  ,   �               	                
                                               error loading dictionary it does not contain enough DIFFERENT characters it is WAY too short it is all whitespace it is based on a (reversed) dictionary word it is based on a dictionary word it is based on your username it is based upon your password entry it is derivable from your password entry it is derived from your password entry it is too short it is too simplistic/systematic it looks like a National Insurance number. it's derivable from your password entry it's derived from your password entry you are not registered in the password file Project-Id-Version: cracklib
Report-Msgid-Bugs-To: cracklib-devel@lists.sourceforge.net
POT-Creation-Date: 2014-10-05 10:58-0500
PO-Revision-Date: 2013-01-28 09:43+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Novell Language <language@novell.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:09+0000
X-Generator: Launchpad (build 18115)
Language: 
 errore nel caricare il dizionario Non contiene abbastanza caratteri DIFFERENTI È TROPPO breve È composta solo da caratteri di spaziatura Si basa su un termine (rovesciato) di dizionario Si basa su un termine di dizionario Si basa sul vostro nome utente Si basa sulla vostra password registrata Può essere derivata dalla vostra password registrata Deriva dalla vostra password registrata È troppo breve È troppo semplice/sistematica Sembra essere un codice fiscale. Può essere derivata dalla vostra password registrata Deriva dalla vostra password registrata Non siete registrati nel file delle password 