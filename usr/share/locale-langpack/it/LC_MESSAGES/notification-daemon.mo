��          |      �             !     9     R     h  (   ~     �     �     �     �       '     �  D     �          $     8  '   S  "   {     �     �  !   �  	   �  )   �                      	                          
           Clear all notifications Closes the notification. Display notifications Enable debugging code Exceeded maximum number of notifications Invalid notification identifier Notification Daemon Notification body text. Notification summary text. Notifications Replace a currently running application Project-Id-Version: notification-daemon
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-19 01:30+0200
PO-Revision-Date: 2016-03-15 08:24+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:46+0000
X-Generator: Launchpad (build 18115)
Language: it
 Cancella tutte notifiche Chiude la notifica Mostre le notifiche Abilita il codice di debug Superato il numero massimo di notifiche Identificatore notifica non valido Demone notifiche Testo della notifica Testo di riepilogo della notifica Notifiche Sostituisce un'applicazione in esecuzione 