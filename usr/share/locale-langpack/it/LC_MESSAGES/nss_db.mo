��          �      L      �     �  �   �  -   �  -   �  >   �     >  0   C  ;   t     �     �  "   �     �           :     H     d          �  �  �     X  �   v  4   Q  7   �  >   �     �  3     C   6     z     �  0   �  (   �  *   	     -	     >	     X	     t	     �	                                
                          	                                          Convert key to lower case Copyright (C) %s Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 Create simple DB database from textual input. Do not print messages while building database INPUT-FILE OUTPUT-FILE
-o OUTPUT-FILE INPUT-FILE
-u INPUT-FILE NAME Print content of database file, one entry a line Report bugs using the `glibcbug' script to <bugs@gnu.org>.
 Write output to file NAME Written by %s.
 cannot open database file `%s': %s cannot open input file `%s' cannot open output file `%s': %s duplicate key problems while reading `%s' while reading database: %s while writing database file: %s wrong number of arguments Project-Id-Version: libnss-db
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2000-09-10 16:37+0200
PO-Revision-Date: 2012-03-09 16:25+0000
Last-Translator: Gerardo Di Giacomo <Unknown>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:03+0000
X-Generator: Launchpad (build 18115)
 Converte i tasti in minuscolo Copyright (C) %s Free Software Foundation, Inc.
Questo è software libero; si veda il sorgente per le condizioni di copiatura.
NON c'è alcuna garanzia; neppure di COMMERCIABILITÀ o IDONEITÀ AD UN
PARTICOLARE SCOPO.
 Crea un semplice database DB da un testo in ingresso Non stampa i messaggi durante la creazione del database INPUT-FILE OUTPUT-FILE
-o OUTPUT-FILE INPUT-FILE
-u INPUT-FILE NOME Stampa il contenuto del database, una voce per riga Segnalare i bug utilizzando lo script "glibcbug" a <bugs@gnu.org>.
 Scrivi l'output sul file NOME Scritto da %s.
 impossibile aprire il file "%s" del database: %s impossibile aprire il file di input "%s" impossibile aprire file di output "%s": %s chiave duplicata problemi nel leggere "%s" nel leggere il database: %s nello scrivere il database: %s numero di argomenti errato 