��          �   %   �      p     q     �  +   �  @   �  5        M  ,   _     �     �     �  $   �  	   �  2   �  $   2     W     Z     l  #   �  !   �     �  %   �               8     <     E     Y  �  h     �       -   5  E   c  (   �     �  0   �          *     J  $   Z  
     >   �  -   �     �     �     	     )	  9   D	     ~	  0   �	     �	     �	     �	  	   �	     
     
                    
                                                                                          	             <b>%s</b>
Hardware present: %s <b>%s</b>
Invalid Driver! <b>Currently Installed Windows Drivers:</b> <span size="larger" weight="bold">Select <i>inf</i> file:</span> Are you sure you want to remove the <b>%s</b> driver? Configure Network Could not find a network configuration tool. Driver is already installed. Error while installing. Install Driver Is the ndiswrapper module installed? Location: Module could not be loaded. Error was:

<i>%s</i>
 Ndiswrapper driver installation tool No No file selected. Not a valid driver .inf file. Please drag an '.inf' file instead. Root or sudo privileges required! Select inf File Unable to see if hardware is present. Windows Wireless Drivers Wireless Network Drivers Yes _Install _Install New Driver _Remove Driver Project-Id-Version: ndisgtk
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-10-02 17:50+0200
PO-Revision-Date: 2010-02-04 07:32+0000
Last-Translator: Julian Andres Klode <juliank@ubuntu.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:44+0000
X-Generator: Launchpad (build 18115)
 <b>%s</b>
Hardware rilevato: %s <b>%s</b>
Driver non valido. <b>Driver Windows attualmente installati:</b> <span size="larger" weight="bold">Selezionare file <i>inf</i>:</span> Rimuovere veramente il driver <b>%s</b>? Configura rete Strumento di configurazione di rete non trovato. Driver già installato. Errore durante l'installazione. Installa driver Il modulo ndiswrapper è installato? Posizione: Impossibile caricare il modulo. L'errore è stato:

<i>%s</i>
 Strumento di installazione driver Ndiswrapper No Nessun file selezionato. File .inf non valido. Trascinare un file ".inf". Bisogna avere i privilegi di root o usare il comando sudo Seleziona file .inf Impossibile controllare la presenza di hardware. Driver wireless di Windows Driver della rete wireless Sì Ins_talla _Installa nuovo driver _Rimuovi driver 