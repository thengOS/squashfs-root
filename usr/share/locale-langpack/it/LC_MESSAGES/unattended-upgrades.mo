��    3      �  G   L      h     i     �     �  "   �  "   �  '   �  -        L  j   `     �  $   �            ,     M  =   m  o   �  N     B   j     �  $   �  $   �       "   /  '   R  /   z     �  ,   �  '   �  #   	  )   8	  !   b	  ?   �	     �	  .   �	  <   
  9   J
     �
  +   �
     �
     �
     �
  +     ,   8     e  ,   y     �  ?   �             M   )  �  w  "   X     {      �  /   �  3   �  3     9   P     �  �   �      9  1   Z  '   �  '   �  /   �  ]     �   j  ]   �  Z   T  !   �  '   �  +   �  %   %  %   K  .   q  <   �     �  5   �  $   +  (   P  3   y  3   �  @   �  !   "  7   D  D   |  W   �  &     :   @     {     �  #   �  <   �  <        B  4   [     �  f   �          .  S   J               !             	   0          "             -   .   &             #   /   (   +                                  
                            3                                  ,   )         1   2                       *       '       %   $    All upgrades installed Allowed origins are: %s An error occurred: '%s' Auto-removing the packages failed! Cache has broken packages, exiting Cache lock can not be acquired, exiting Download finished, but file '%s' not there?!? Error message: '%s' Found %s, but not rebooting because %s is logged in. Found %s, but not rebooting because %s are logged in. GetArchives() failed: '%s' Giving up on lockfile after %s delay Initial blacklisted packages: %s Initial whitelisted packages: %s Installing the upgrades failed! Lock could not be acquired (another package manager running?) No '/usr/bin/mail' or '/usr/sbin/sendmail',can not send mail. You probably want to install the 'mailx' package. No packages found that can be upgraded unattended and no pending auto-removals Package '%s' has conffile prompt and needs to be upgraded manually Package installation log: Packages that are auto removed: '%s' Packages that attempted to upgrade:
 Packages that were upgraded:
 Packages that will be upgraded: %s Packages were successfully auto-removed Packages with upgradable origin but kept back:
 Progress: %s %% (%s) Running unattended-upgrades in shutdown mode Simulation, download but do not install Starting unattended upgrades script The URI '%s' failed to download, aborting Unattended upgrade returned: %s

 Unattended-upgrade in progress during shutdown, sleeping for 5s Unattended-upgrades log:
 Unclean dpkg state detected, trying to correct Upgrade in minimal steps (and allow interrupting with SIGINT Warning: A reboot is required to complete this upgrade.

 Writing dpkg log to '%s' You need to be root to run this application [package on hold] [reboot required] dpkg --configure -a output:
%s dpkg returned a error! See '%s' for details dpkg returned an error! See '%s' for details error message: '%s' make apt/libapt print verbose debug messages package '%s' not upgraded package '%s' upgradable but fails to be marked for upgrade (%s) print debug messages print info messages {hold_flag}{reboot_flag} unattended-upgrades result for '{machine}': {result} Project-Id-Version: unattended-upgrades
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-21 04:07+0000
PO-Revision-Date: 2016-03-15 08:42+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 Installati tutti gli aggiornamenti Origini consentite sono: %s Si è verificato un errore: "%s" Rimozione automatica dei pacchetti non riuscita La cache contiene dei pacchetti danneggiati, uscita Impossibile impostare il blocco sulla cache, uscita Scaricamento completato, ma il file "%s" non è presente. Errore: "%s" Trovato %s, ma non viene eseguito il riavvio perché l'utente %s è collegato. Trovato %s, ma non viene eseguito il riavvio perché gli utenti %s sono collegati. GetArchives() non riuscita: "%s" File di blocco non considerato dopo %s di ritardo Pacchetti inizialmente in blacklist: %s Lista bianca iniziale dei pacchetti: %s Installazione degli aggiornamenti non riuscita. Impossibile impostare il blocco (un altro gestore di pacchetti potrebbe essere in esecuzione) Impossibile inviare email, "/usr/bin/mail" o "/usr/sbin/sendmail" non presenti. Probabilmente è necessario installare il pacchetto "mailx" Nessun pacchetto trovato che possa essere aggiornato e nessuna rimozione automatica in attesa Il pacchetto "%s" presenta un prompt conffile e necessita di essere aggiornato manualmente Registro installazione pacchetto: Pacchetti rimossi automaticamente: "%s" Pacchetti che si è tentato di aggiornare:
 Pacchetti che sono stati aggiornati:
 Pacchetti che verranno aggiornati: %s Pacchetti rimossi automaticamente con successo Pacchetti aggiornabili, ma mantenuti alla versione attuale:
 Avanzamento: %s %% (%s) Esecuzione di unattended-upgrade in modalità arresto Simulazione, scarica ma non installa Avvio script avanzamento senza controllo Scaricamento dall'URI "%s" non riuscito, interrotto L'aggiornamento non controllato ha restituito: %s

 Esecuzione di unattended-upgrade durante l'arresto, pausa per 5s Registro di unattended-upgrades:
 Rilevato stato unclean di dpkg, tentativo di correzione Aggiorna con passaggi minimali (e consente l'interruzione con SIGINT Attenzione: è necessario il riavvio del sistema per completare questo aggiornamento.

 Scrittura del registro di dpkg su "%s" È necessario essere root per eseguire questa applicazione [pacchetto bloccato] [richiesto riavvio] output di "dpkg --configure -a":
%s dpkg ha restituito un errore. Consultare "%s" per i dettagli dpkg ha restituito un errore. Consultare "%s" per i dettagli messaggio d'errore: "%s" Fa in modo che apt/libapt stampino messaggi di debug Pacchetto "%s" non aggiornato il pacchetto "%s" può essere aggiornato, ma non è possibile contrassegnarlo per l'aggiornamento (%s) Stampa messaggi di debug Stampa messaggi informativi {hold_flag}{reboot_flag} risultato di unattended-upgrades per "{machine}": {result} 