��          |      �          &   !  '   H  A   p  >   �  >   �  +   0  9   \     �  6   �  �   �  v   �  �  D  /   �  (   .  S   W  F   �  V   �  ,   I  >   v  '   �  Y   �    7  �   J	                   
                                    	    Change Screen Resolution Configuration Change the effect of Ctrl+Alt+Backspace Changing the Screen Resolution configuration requires privileges. Changing the effect of Ctrl+Alt+Backspace requires privileges. Could not connect to Monitor Resolution Settings DBUS service. Enable or disable the NVIDIA GPU with PRIME Enabling or disabling the NVIDIA GPU requires privileges. Monitor Resolution Settings Monitor Resolution Settings can't apply your settings. Monitor Resolution Settings has detected that the virtual resolution must be set in your configuration file in order to apply your settings.

Would you like Screen Resolution to set the virtual resolution for you? (Recommended) Please log out and log back in again.  You will then be able to use Monitor Resolution Settings to setup your monitors Project-Id-Version: screen-resolution-extra
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-18 12:57+0000
PO-Revision-Date: 2014-02-17 16:39+0000
Last-Translator: Claudio Arseni <claudio.arseni@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:34+0000
X-Generator: Launchpad (build 18115)
 Modifica configurazione risoluzione del monitor Modifica l'effetto di Ctrl+Alt+Backspace La modifica della configurazione di risoluzione del monitor richiede dei privilegi. La modifica dell'effetto di Ctrl+Alt+Backspace richiede dei privilegi. Impossibile connettersi al servizio D-Bus «Impostazioni di risoluzione del monitor». Abilita o disabilita la GPU NVIDIA con PRIME Abilitare o disabilitare la GPU NVIDIA richiede dei privilegi. Impostazioni di risoluzione del monitor «Impostazioni di risoluzione del monitor» non è in grado di applicare le impostazioni. «Impostazioni di risoluzione del monitor» ha rilevato che la risoluzione virtuale deve essere impostata nel file di configurazione affinché le impostazioni possano essere applicate.

Lasciare che il programma si occupi di impostare la risoluzione virtuale (raccomandato)? Terminare la sessione e rieseguire l'accesso. Sarà poi possibile usare «Impostazioni di risoluzione del monitor» per impostare i propri monitor 