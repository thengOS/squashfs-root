��    �     �  �  l+      �9     �9  L  :  D   ^;  m   �;  *   <  1   <<  6   n<  4   �<  @   �<  .   =  4   J=  >   =  K   �=  6   
>  @   A>  E   �>  E   �>  5   ?  +   D?  8   p?  7   �?  Q   �?  1   3@  +   e@  E   �@  <   �@  A   A  H   VA  N   �A  )   �A  8   B  M   QB  D   �B  2   �B  *   C  *   BC  +   mC  =   �C     �C  e   �C  e   OD  -   �D  ;   �D  4   E  ,   TE  4   �E  5   �E  8   �E  )   %F  3   OF  0   �F  H   �F  8   �F  -   6G  7   dG  )   �G  1   �G  2   �G  9   +H  8   eH  7   �H  7   �H  7   I  0   FI  6   wI  9   �I  2   �I  3   J  >   OJ  (   �J  /   �J  "   �J  /   
K  4   :K  5   oK  ?   �K     �K  @   �K  6   =L  -   tL  *   �L  5   �L  3   M  7   7M  5   oM  +   �M  2   �M  1   N     6N  B   SN  :   �N  6   �N  F   O  <   OO  -   �O  !   �O  '   �O  '   P  8   ,P  ;   eP  @   �P  (   �P  )   Q  *   5Q  (   `Q  J   �Q  J   �Q  -   R  -   MR  C   {R     �R  .   �R  ,   �R  %   ,S      RS      sS  0   �S  ?   �S  &   T  +   ,T  (   XT  /   �T  .   �T     �T  &   �T  0   U      LU  4   mU  $   �U  &   �U  (   �U  %   V  "   =V     `V      �V  B   �V     �V     W     !W  .   8W  :   gW  6   �W  4   �W  %   X  F   4X  K   {X  3   �X     �X  Q   Y  V   hY      �Y     �Y  *   �Y  !   Z  j   AZ  ?   �Z      �Z  !   [  )   /[     Y[  2   j[     �[  2   �[  /   �[  B   \  B   V\  "   �\  "   �\  ;   �\     ]     0]  +   M]  (   y]  #   �]     �]  J   �]  D   +^  2   p^  2   �^  ?   �^  *   _     A_  .   V_     �_  7   �_  ;   �_     `     1`     C`     W`     d`  
   s`     ~`     �`     �`     �`     �`     �`     �`     �`     a  	   a     a  
   #a     .a     Ja  3   [a  :   �a  6   �a  4   b  $   6b  D   [b  J   �b  D   �b     0c     ?c     \c     ec  ;   zc     �c  #   �c  B   �c     8d     Wd     od  3   �d  1   �d  )   �d  *   e  )   Be    le  �   rf     Vg     vg  <   �g  =   �g     h     ,h  1   Kh  #   }h     �h     �h     �h  (   �h     i     "i     9i  &   Qi     xi     �i  !   �i     �i  !   �i     j  B   ,j  <   oj  .   �j  ?   �j  "   k  9   >k  9   xk  2   �k  -   �k  )   l  *   =l  9   hl  +   �l     �l  <   �l  :   %m     `m  -   wm     �m  #   �m  �   �m     �n  #   �n  $   o     ,o     Jo  !   do  
   �o     �o     �o     �o  ,   �o  !   p     $p     Cp      bp     �p      �p     �p     �p  &   �p     q     #q     Aq     ^q  ;   xq  �   �q  )   �r  M   �r  &   s  4   ,s  0   as  >   �s  9   �s  4   t  D   @t  D   �t      �t     �t  >   
u  $   Iu  ?   nu     �u  4   �u  3   �u     .v     <v     Yv     xv      �v     �v  L   �v      w     w     5w  =   Nw  '   �w  *   �w  !   �w     x     x     5x  8   Nx  8   �x      �x      �x     y     y     8y  6   Ry  (   �y     �y  !   �y     �y     �y     �y     z     z     ?z     ^z     yz  6   �z  ;   �z  %   {     +{     A{  ;   \{     �{     �{  0   �{     �{  F   |  #   \|     �|  5   �|      �|     �|     }  &   }  *   C}  *   n}  (   �}  *   �}  *   �}  +   ~  +   D~  +   p~  )   �~  )   �~  (   �~  #        =     M      ]      ~     �  $   �      �  3   �     9�     V�  ,   b�  )   ��  #   ��  #   ݀     �     �     ,�     E�     ]�     t�     ��     ��          ݁     ��     �     )�     A�     [�     v�  0   ��  !   ��     �  )   ��  '    �     H�  "   d�      ��     ��     ��     σ  !   �     �  !   %�     G�  *   W�  -   ��     ��     ˄  .   �  2   �  5   J�  4   ��  .   ��  *   �     �     !�  2   A�  0   t�     ��     ��     ӆ     �      ��     �     8�     N�     c�  %   x�     ��  *   ��     �  3   ��  *   .�  "   Y�  )   |�  -   ��     Ԉ      �     �     ,�     B�  H   \�  *   ��     Љ     �  -   �  -   4�  1   b�  )   ��     ��  D   Ɗ  %   �  /   1�    a�    m�  @   �  F   .�  ;   u�    ��  �   ��    ��     ��  #   ̑  (   �     �  @   -�  9   n�     ��  �   ƒ     w�  ?   ��  &   ʓ  #   �  %   �  (   ;�  ,   d�  	   ��     ��     ��     ͔     �     ��  /   �     G�  '   \�     ��     ��     ��  $   ˕  )   �  F   �  :   a�  A   ��  G   ޖ  5   &�  S   \�  =   ��  $   �     �     +�     J�  .   ]�     ��     ��     Ř     �     �  $   �  ?   B�     ��     ��     ��  
   ��     ę     �     ��     �  ,   +�     X�     r�     ��     ��     ��     ǚ  (   ۚ  "   �  '   '�  *   O�  (   z�  $   ��  5   ț  "   ��  0   !�  "   R�  )   u�     ��     ��     ʜ     �     �     ��     �     ,�  �   <�  E   ��  V   �  '   Z�  4   ��  >   ��  ;   ��  1   2�  I   d�  M   ��  ,   ��  M   )�  3   w�  ;   ��  +   �  +   �     ?�  U   W�     ��  !   š     �  S   �  /   U�  )   ��  #   ��     Ӣ  '   �  H   �  
   b�  5   m�     ��     ��     ң  +   �  "   �  !   ;�  "   ]�     ��  2   ��  2   Ф  @   �  &   D�     k�     ��     ��  %   ��     ݥ  0   �  4   �     J�     S�  '   g�  1   ��     ��     ݦ     ��     �     �  H   <�     ��     ��     ��  4   ڧ  1   �  @   A�  =   ��     ��  &   ۨ  '   �  #   *�  3   N�  4   ��     ��  0   ֩     �  @   &�  .   g�  2   ��  -   ɪ  1   ��  ,   )�     V�  $   f�     ��     ��     ��     ʫ  0   �  "   �      6�  "   W�  	   z�  '   ��  "   ��      Ϭ     �     �  %   �  ,   C�     p�  !   ��     ��     ϭ     ׭     ��     ��     �  K   .�     z�  )   ��  :   ��  0   ��  :   +�     f�  	   ��     ��     ��     ��  )   ��  @   �  4   +�     `�     v�  �  ��  '   ��  �  ��  K   /�  �   {�  8   �  @   ?�  B   ��  U   õ  R   �  =   l�  ;   ��  Q   �  W   8�  C   ��  O   Է  T   $�  h   y�  K   �  6   .�  T   e�  I   ��  f   �  8   k�  /   ��  a   Ժ  L   6�  S   ��  ^   ׻  e   6�  3   ��  A   м  i   �  s   |�  .   �  3   �  6   S�  %   ��  \   ��     �  o    �  m   ��  2   ��  O   1�  A   ��  <   ��  9    �  8   :�  B   s�  :   ��  ;   ��  3   -�  Z   a�  E   ��  3   �  8   6�  9   o�  =   ��  >   ��  A   &�  =   h�  ?   ��  N   ��  S   5�  :   ��  C   ��  O   �  9   X�  C   ��  P   ��  ;   '�  <   c�  2   ��  B   ��  >   �  E   U�  F   ��     ��  J   ��  @   G�  >   ��  +   ��  N   ��  H   B�  N   ��  H   ��  <   #�  :   `�  7   ��  /   ��  S   �  L   W�  P   ��  `   ��  [   V�  6   ��  ,   ��  4   �  7   K�  X   ��  N   ��  U   +�  )   ��  2   ��  V   ��  P   5�  `   ��  `   ��  .   H�  .   w�  _   ��     �  D   �  >   \�  &   ��  *   ��  $   ��  J   �  c   ]�  0   ��  /   ��  -   "�  >   P�  ?   ��     ��  '   ��  C   �  %   T�  U   z�  0   ��  2   �  1   4�  ?   f�  3   ��  (   ��  )   �  j   -�  #   ��  "   ��      ��  <    �  K   =�  O   ��  8   ��  &   �  g   9�  n   ��  E   �     V�  w   u�  ~   ��  "   l�     ��  +   ��      ��  �   ��  g   ��  -   ��  *   �  .   G�     v�  6   ��     ��  ?   ��  V   �  `   v�  `   ��  0   8�  "   i�  E   ��     ��      ��  6   
�  /   A�  *   q�  '   ��  Z   ��  R   �  5   r�  5   ��  W   ��  B   6�     y�  p   ��  /   �  E   4�  I   z�  !   ��  %   ��     �     ,�     :�     J�     Y�     j�     ��     ��     ��  "   ��     ��     �     �     �     �     -�  "   9�     \�  D   n�  R   ��  ;   �  6   B�  &   y�  T   ��  h   ��  b   ^�     ��  &   ��      �     	�  R    �     s�  '   ��  F   ��  )   ��  %   (�      N�  :   o�  8   ��  +   ��  ,   �  +   <�  G  h�  -  ��  -   ��  #   �  Q   0�  R   ��  !   ��  1   ��  B   )�  .   l�     ��     ��     ��  B   ��     )�     H�     c�  7   ��     ��  !   ��  "   ��  $   �  +   ?�  '   k�  a   ��  \   ��  H   R�  j   ��  +   �  A   2�  N   t�  :   ��  <   ��  5   ;�  8   q�  G   ��  =   ��     0�  L   P�  <   ��     ��  ;   ��     .�  3   H�  �   |�  &   ]�  2   ��  3   ��  "   ��  -   �  #   <�     `�     l�  )   |�  (   ��  6   ��  '   �  $   .�  $   S�  '   x�     ��  &   ��     ��      ��  E   �  (   [�  ,   ��  -   ��  '   ��  Y   �  �   a�  (   \�  n   ��     ��  5   �  3   J�  H   ~�  6   ��  F   ��  M   E�  O   ��  ,   ��  >   �  G   O�  -   ��  J   ��     �  7   (�  9   `�     ��  (   ��  >   ��       '   $  !   L  m   n     �  !   �      c   4 .   � C   � *    )   6    `    � >   � >   � &    &   C    j "   �    � @   � L       Y $   t    �    �    �    � .   � -    !   I    k A   � Q   � *       J "   h L   � '   �      X    "   r ]   � *   �     F   7 '   ~    �    � -   � 1   	 1   7	 /   i	 1   �	 1   �	 2   �	 2   0
 2   c
 0   �
 0   �
 /   �
 "   (    K    ^ '   o '   � 0   � 3   � $   $ 5   I ,       � L   � .    )   G )   q    � (   � '   � &    %   3 '   Y *   � '   � )   � )   � '   ( &   P &   w (   � )   �    � L    5   [    � 7   � 5   �     *   8 %   c    �    � &   � +   � &    +   *    V )   m +   � (   � $   � U    :   g ;   � :   � -    9   G    � ,   � F   � @   	    J %   [    �    � &   �    �    �     !   $ >   F #   � 2   �    � F   � 1   < (   n <   � 4   �    	 #   $ #   H    l    � Y   � 6     %   7    ] ;   { )   � (   � @   
    K O   T >   � 0   � /   �  D I    T   [ :   � #  � �     �  �     ~" <   �" 7   �"     # C   4# T   x# )   �# �   �#    �$ R   % 4   `% /   �% /   �% 2   �% 2   (& 	   [&    e& '   �&    �&    �& #   �& 9   '    F' 5   d'    �'    �' #   �' 7   �' ?   $( R   d( L   �( T   ) ^   Y) J   �) i   * F   m* 5   �* &   �* '   +    9+ )   V+ "   �+    �+ $   �+ $   �+ ,   , &   :, b   a,    �,    �,    �,     - "   - #   4-    X-     v- =   �- !   �-    �-    .    (. 
   G.    R. )   h. )   �. 6   �. )   �. 4   / ;   R/ I   �/ 6   �/ 8   0 (   H0 <   q0 !   �0    �0 )   �0    1    1 4   .1    c1    w1 �   �1 T   P2 r   �2 7   3 H   P3 N   �3 Q   �3 8   :4 k   s4 u   �4 7   U5 v   �5 A   6 V   F6 C   �6 C   �6    %7 W   >7    �7 )   �7    �7 X   �7 /   L8 ;   |8 9   �8    �8 -   9 W   ?9    �9 8   �9    �9 &   �9    : ,   8: #   e: "   �: #   �:    �: :   �: :   ); v   d; @   �; .   < .   K<    z< 2   �<    �< 3   �< 7   =    <=    E= C   Y= I   �=    �= )   >    ,>    <> #   V> Q   z>    �> ,   �> %   ? D   >? 2   �? Q   �? M   @ '   V@ 8   ~@ G   �@ 5   �@ F   5A ?   |A    �A [   �A *   8B Z   cB .   �B 3   �B 0   !C 5   RC 3   �C    �C 8   �C    D .   &D    UD *   fD D   �D !   �D 0   �D &   )E    PE A   \E 8   �E .   �E    F    F )   7F @   aF 9   �F 6   �F    G    /G +   <G    hG *   oG &   �G T   �G    H /   0H I   `H 7   �H O   �H    2I 
   RI    ]I    iI *   zI %   �I F   �I >   J -   QJ 1   J    o                    �          �      S     ;   f  �       K   �  �  �  ]  N  t             �  �         {  E  �  ]      �   b  �  �   A      r      �         �  �   �   S  �  `  �     �         �              )         �  �  ~  :  
   <  N  �      �  ,       �   �   )  q      �      �          #  �  z   �   >         c  �   �   �  x  v  "  C      �       -  i   �   K       �  j  �   R      �       p  �  �  D               �       �   '               Y  �  �  U   Q      <  �  �          A  W  '        Z  �  �                Y      �    �   	          p           �   V          u          C   �     +  �       "    E           L  �  �   y  w   �  �  a  �        S         G   I  �   �  �   �   }   �  �  :   �   Z      �      �  c       �       �  -        �    ~     j   �      �   �      �   g  �  1    Y      G                            �     8  �    �  �  _      h         �       L   �  �  9      �  @  �      �      T      �  B  (   �    �           ]   A      Z       �  m      }  �   L         �   �   $       �  �  I       �      �  @  �   �    m   �       -           B   <   �      a   �          d  w      �   �   �   o       /   �              O            P  H  1   x           �        �   �          U                �      '  U  |           j  q   3  �          #       �   �           �   �   5      �    �      �  �   [      �      f   �  /  r   �  �          t      �      &      �      �   ^       �  �   u   �  �  �  {      �           ~  >  �   W  �   (  R  e  �                  �      �   �   2              l  y   X  �   e  E          \  o      %  �   �            �  +  [  �   �  �  �  K  �       5  �                �  �   �   ;        k  P      5       �              �  �  z      J       V  �            ?  l       �   D  F      I      t  p  a  �      �  �  �      ,    �   4  =  4       _  �  `      �     .       
       	      �  �      �          �  �  )        x        �       u        �   0  �  �  �  �      |         �      �  B  s      �  �           !   �  7  �       J  }  �                ?  �       n  �      3   �   �  6   �           �   �       �  �      �   �       �       |        �   �          i  �          !  �     �           �  ?   �   �           �  #  "   �  �  �   6  &     �  6  ;          c  �   \  �  *   �      
  n   %   i  �   l  �     $  �             �  e   O  �  �  �   �   b  >      k   �  �  s       �            H  �       �   r      �   J  �  �       9      �   X      v       0  b   �  �   M            =      �   2      �  �   V    T   �  !  n  Q   �  �  T  M      �   �          h  �  �   �       �  �              N   f  .      H   �                    �  %  �   �  �   �   �        q  �  �  �  �   h  {   �  �  w      ^      �  3  �  �   �   �  �  z  �  �   �      �          �   �   (  �  �  F  �  �   �       d   m      �   �   �   �     *      X     s  &   @       /          �  �  8     �  �   y      1      �    .  _   R   �  �  4  k          :  =   `   �    P   W   g   8   O   G  9   �   7    �  �  v     $    �       �  d  7   	  �  �  �  �  D  �  �           �  0       �  +   g  �  �    \   �      �    �   *  �  M   �          �  �  �   ^  �  �  F       2   ,      �  C  Q  �       �   [    
Caught SIGINT... bailing out.
 
However, if you HAVE changed your account details since starting the
fetchmail daemon, you need to stop the daemon, change your configuration
of fetchmail, and then restart the daemon.

The fetchmail daemon will continue running and attempt to connect
at each cycle.  No future notifications will be sent until service
is restored.       --auth        authentication type (password/kerberos/ssh/otp)
       --bad-header {reject|accept}
                    specify policy for handling messages with bad headers
       --bsmtp       set output BSMTP file
       --fastuidl    do a binary search for UIDLs
       --fetchdomains fetch mail for specified domains
       --fetchsizelimit set fetch message size limit
       --invisible   don't write Received & enable host spoofing
       --limitflush  delete oversized messages
       --lmtp        use LMTP (RFC2033) for delivery
       --nobounce    redirect bounces from user to postmaster.
       --nosoftbounce fetchmail deletes permanently undeliverable messages.
       --pidfile     specify alternate PID (lock) file
       --plugin      specify external command to open connection
       --plugout     specify external command to open smtp connection
       --port        TCP port to connect to (obsolete, use --service)
       --postmaster  specify recipient of last resort
       --principal   mail service principal
       --showdots    show progress dots even in logfiles
       --smtpname    set SMTP full name username@domain
       --softbounce  keep permanently undeliverable messages on server (default).
       --ssl         enable ssl encrypted session
       --sslcert     ssl client certificate
       --sslcertck   do strict server certificate check (recommended)
       --sslcertfile path to trusted-CA ssl certificate file
       --sslcertpath path to trusted-CA ssl certificate directory
       --sslcommonname  expect this CommonName from server (discouraged)
       --sslfingerprint fingerprint that must match that of the server's cert.
       --sslkey      ssl private key file
       --sslproto    force ssl protocol (SSL2/SSL3/TLS1)
       --syslog      use syslog(3) for most messages when running as a daemon
       --tracepolls  add poll-tracing information to Received header
     attacks by a dishonest service are possible.)
     cannot be sure you are talking to the
     prevent you logging in, but means you
     service that you think you are (replay
    Do linear search of UIDs during each poll (--fastuidl 0).
   %d UIDs saved.
   %d message  %d octets long deleted by fetchmail.   %d messages %d octets long deleted by fetchmail.   %d message  %d octets long skipped by fetchmail.   %d messages %d octets long skipped by fetchmail.   -?, --help        display this option help
   -B, --fetchlimit  set fetch limit for server connections
   -D, --smtpaddress set SMTP delivery domain to use
   -E, --envelope    envelope address header
   -F, --flush       delete old messages from server
   -I, --interface   interface required specification
   -K, --nokeep      delete new messages after retrieval
   -L, --logfile     specify logfile name
   -M, --monitor     monitor interface for activity
   -N, --nodetach    don't detach daemon process
   -P, --service     TCP service to connect to (can be numeric TCP port)
   -Q, --qvirtual    prefix to remove from local user id
   -S, --smtphost    set SMTP forwarding host
   -U, --uidl        force the use of UIDLs (pop3 only)
   -V, --version     display version info
   -Z, --antispam,   set antispam response values
   -a, --[fetch]all  retrieve old and new messages
   -b, --batchlimit  set batch limit for SMTP connections
   -c, --check       check for messages without fetching
   -d, --daemon      run as a daemon once per n seconds
   -e, --expunge     set max deletions between expunges
   -f, --fetchmailrc specify alternate run control file
   -i, --idfile      specify alternate UIDs file
   -k, --keep        save new messages after retrieval
   -l, --limit       don't fetch messages over given size
   -m, --mda         set MDA to use for forwarding
   -n, --norewrite   don't rewrite header addresses
   -p, --protocol    specify retrieval protocol (see man page)
   -q, --quit        kill daemon process
   -r, --folder      specify remote folder name
   -s, --silent      work silently
   -t, --timeout     server nonresponse timeout
   -u, --username    specify users's login on server
   -v, --verbose     work noisily (diagnostic output)
   -w, --warnings    interval between warning mail notification
   APOP secret = "%s".
   Address to be put in RCPT TO lines shipped to SMTP will be %s
   All available authentication methods will be tried.
   All messages will be retrieved (--all on).
   CRAM-MD5 authentication will be forced.
   Carriage-return forcing is disabled (forcecr off).
   Carriage-return forcing is enabled (forcecr on).
   Carriage-return stripping is disabled (stripcr off).
   Carriage-return stripping is enabled (stripcr on).
   Connection must be through interface %s.
   DNS lookup for multidrop addresses is disabled.
   DNS lookup for multidrop addresses is enabled.
   Default mailbox selected.
   Deletion interval between expunges forced to %d (--expunge %d).
   Delivered-To lines will be discarded (dropdelivered on)
   Delivered-To lines will be kept (dropdelivered off)
   Do binary search of UIDs during %d out of %d polls (--fastuidl %d).
   Do binary search of UIDs during each poll (--fastuidl 1).
   Domains for which mail will be fetched are:   End-to-end encryption assumed.
   Envelope header is assumed to be: %s
   Envelope-address routing is disabled
   Fetch message size limit is %d (--fetchsizelimit %d).
   Fetched messages will be kept on the server (--keep on).
   Fetched messages will not be kept on the server (--keep off).
   GSSAPI authentication will be forced.
   Host part of MAIL FROM line will be %s
   Idle after poll is disabled (idle off).
   Idle after poll is enabled (idle on).
   Interpretation of Content-Transfer-Encoding is disabled (pass8bits on).
   Interpretation of Content-Transfer-Encoding is enabled (pass8bits off).
   Kerberos V4 authentication will be forced.
   Kerberos V5 authentication will be forced.
   Listener connections will be made via plugout %s (--plugout %s).
   Local domains:   MIME decoding is disabled (mimedecode off).
   MIME decoding is enabled (mimedecode on).
   MSN authentication will be forced.
   Mail service principal is: %s
   Mail will be retrieved via %s
   Message size limit is %d octets (--limit %d).
   Message size warning interval is %d seconds (--warnings %d).
   Messages will be %cMTP-forwarded to:   Messages will be appended to %s as BSMTP
   Messages will be delivered with "%s".
   Messages with bad headers will be passed on.
   Messages with bad headers will be rejected.
   Multi-drop mode:    NTLM authentication will be forced.
   No SMTP message batch limit (--batchlimit 0).
   No UIDs saved from this host.
   No fetch message size limit (--fetchsizelimit 0).
   No forced expunges (--expunge 0).
   No interface requirement specified.
   No localnames declared for this host.
   No message size limit (--limit 0).
   No monitor interface specified.
   No plugin command specified.
   No plugout command specified.
   No poll trace information will be added to the Received header.
   No post-connection command.
   No pre-connection command.
   No prefix stripping
   No received-message limit (--fetchlimit 0).
   Nonempty Status lines will be discarded (dropstatus on)
   Nonempty Status lines will be kept (dropstatus off)
   Number of envelope headers to be skipped over: %d
   OTP authentication will be forced.
   Old messages will be flushed before message retrieval (--flush on).
   Old messages will not be flushed before message retrieval (--flush off).
   Only new messages will be retrieved (--all off).
   Options are as follows:
   Oversized messages will be flushed before message retrieval (--limitflush on).
   Oversized messages will not be flushed before message retrieval (--limitflush off).
   Pass-through properties "%s".
   Password = "%s".
   Password authentication will be forced.
   Password will be prompted for.
   Poll of this server will occur every %d interval.
   Poll of this server will occur every %d intervals.
   Poll trace information will be added to the Received header.
   Polling loop will monitor %s.
   Predeclared mailserver aliases:   Prefix %s will be removed from user id
   Protocol is %s   Protocol is KPOP with Kerberos %s authentication   RPOP id = "%s".
   Received-message limit is %d (--fetchlimit %d).
   Recognized listener spam block responses are:   Rewrite of server-local addresses is disabled (--norewrite on).
   Rewrite of server-local addresses is enabled (--norewrite off).
   SMTP message batch limit is %d.
   SSL encrypted sessions enabled.
   SSL key fingerprint (checked against the server key): %s
   SSL protocol: %s.
   SSL server CommonName: %s
   SSL server certificate checking enabled.
   SSL trusted certificate directory: %s
   SSL trusted certificate file: %s
   Selected mailboxes are:   Server aliases will be compared with multidrop addresses by IP address.
   Server aliases will be compared with multidrop addresses by name.
   Server connection will be brought up with "%s".
   Server connection will be taken down with "%s".
   Server connections will be made via plugin %s (--plugin %s).
   Server nonresponse timeout is %d seconds   Single-drop mode:    Size warnings on every poll (--warnings 0).
   Spam-blocking disabled
   This host will be queried when no host is specified.
   This host will not be queried when no host is specified.
   True name of server is %s.
  (%d body octets)  (%d header octets)  (%d octets)  (%d octets).
  (default)  (default).
  (forcing UIDL use)  (length -1)  (oversized)  (previously authorized)  (using default port)  (using service %s)  <empty>  and   flushed
  not flushed
  retained
 %cMTP connect to %s failed
 %cMTP error: %s
 %cMTP listener doesn't like recipient address `%s'
 %cMTP listener doesn't really like recipient address `%s'
 %d local name recognized.
 %d local names recognized.
 %d message (%d %s) for %s %d messages (%d %s) for %s %d message for %s %d messages for %s %d message waiting after expunge
 %d messages waiting after expunge
 %d message waiting after first poll
 %d messages waiting after first poll
 %d message waiting after re-poll
 %d messages waiting after re-poll
 %lu is unseen
 %s (log message incomplete)
 %s at %s %s at %s (folder %s) %s configuration invalid, LMTP can't use default SMTP port
 %s connection to %s failed %s error while fetching from %s@%s
 %s error while fetching from %s@%s and delivering to SMTP host %s
 %s fingerprints do not match!
 %s fingerprints match.
 %s key fingerprint: %s
 %s querying %s (protocol %s) at %s: poll completed
 %s querying %s (protocol %s) at %s: poll started
 %s's SMTP listener does not support ATRN
 %s's SMTP listener does not support ESMTP
 %s's SMTP listener does not support ETRN
 %s: The NULLMAILER_FLAGS environment variable is set.
This is dangerous as it can make nullmailer-inject or nullmailer's
sendmail wrapper tamper with your From:, Message-ID: or Return-Path: headers.
Try "env NULLMAILER_FLAGS= %s YOUR ARGUMENTS HERE"
%s: Abort.
 %s: The QMAILINJECT environment variable is set.
This is dangerous as it can make qmail-inject or qmail's sendmail wrapper
tamper with your From: or Message-ID: headers.
Try "env QMAILINJECT= %s YOUR ARGUMENTS HERE"
%s: Abort.
 %s: You don't exist.  Go away.
 %s: can't determine your host! %s: opportunistic upgrade to TLS failed, trying to continue
 %s: opportunistic upgrade to TLS failed, trying to continue.
 %s: upgrade to TLS failed.
 %s: upgrade to TLS succeeded.
 %s:%d: warning: found "%s" before any host names
 %s:%d: warning: unknown token "%s"
 %u is first unseen
 %u is unseen
 -- 
The Fetchmail Daemon --check mode enabled, not fetching mail
 ...rewritten version is %s.
 ATRN request refused.
 About to rewrite %s...
 All connections are wedged.  Exiting.
 Authentication required.
 Authorization OK on %s@%s
 Authorization failure on %s@%s%s
 BSMTP file open failed: %s
 BSMTP preamble write failed: %s.
 Bad base64 reply from server.
 Bad certificate: Subject Alternative Name contains NUL, aborting!
 Bad certificate: Subject CommonName contains NUL, aborting!
 Bad certificate: Subject CommonName too long!
 Both fetchall and keep on in daemon or idle mode is a mistake!
 Broken certification chain at: %s
 Buffer too small. This is a bug in the caller of %s:%lu.
 Cannot find my own host in hosts database to qualify it!
 Cannot handle UIDL response from upstream server.
 Cannot open fetchids file %s for writing: %s
 Cannot rename fetchids file %s to %s: %s
 Cannot resolve service %s to port number.
 Cannot switch effective user id back to original %ld: %s
 Cannot switch effective user id to %ld: %s
 Certificate at depth %d:
 Certificate chain, from root to peer, starting at depth %d:
 Certificate/fingerprint verification was somehow skipped!
 Challenge decoded: %s
 Checking if %s is really the same node as %s
 Command not implemented
 Connection errors for this poll:
%s Copyright (C) 2002, 2003 Eric S. Raymond
Copyright (C) 2004 Matthias Andree, Eric S. Raymond,
                   Robert M. Funk, Graham Wilson
Copyright (C) 2005 - 2012 Sunil Shetye
Copyright (C) 2005 - 2013 Matthias Andree
 Could not decode OTP challenge
 Couldn't get service name for [%s]
 Couldn't unwrap security level data
 Credential exchange complete
 Cygwin socket read retry
 Cygwin socket read retry failed!
 DNS lookup Deity error Deleting fetchids file.
 Digest text buffer too small!
 ERROR: no support for getpassword() routine
 ESMTP CRAM-MD5 Authentication...
 ESMTP LOGIN Authentication...
 ESMTP PLAIN Authentication...
 ETRN support is not configured.
 ETRN syntax error
 ETRN syntax error in parameters
 EVP_md5() failed!
 Enter password for %s@%s:  Error creating security level request
 Error deleting %s: %s
 Error exchanging credentials
 Error releasing credentials
 Error writing to MDA: %s
 Error writing to fetchids file %s, old file left in place.
 Fetchmail comes with ABSOLUTELY NO WARRANTY. This is free software, and you
are welcome to redistribute it under certain conditions. For details,
please see the file COPYING in the source or documentation directory.
 Fetchmail could not get mail from %s@%s.
 Fetchmail saw more than %d timeouts while attempting to get mail from %s@%s.
 Fetchmail was able to log into %s@%s.
 Fetchmail will direct error mail to the postmaster.
 Fetchmail will direct error mail to the sender.
 Fetchmail will forward misaddressed multidrop messages to %s.
 Fetchmail will masquerade and will not generate Received
 Fetchmail will show progress dots even in logfiles.
 Fetchmail will treat permanent errors as permanent (drop messages).
 Fetchmail will treat permanent errors as temporary (keep messages).
 File %s must be a regular file.
 File %s must be owned by you.
 File %s must have no more than -rwx------ (0700) permissions.
 File descriptor out of range for SSL For help, see http://www.fetchmail.info/fetchmail-FAQ.html#R15
 GSSAPI error %s: %.*s
 GSSAPI error in gss_display_status called from <%s>
 GSSAPI support is configured, but not compiled in.
 Get response
 Get response return %d [%s]
 Guessing from header "%-.*s".
 Hdr not 60
 IMAP support is not configured.
 Idfile is %s
 If you want to use GSSAPI, you need credentials first, possibly from kinit.
 Inbound binary data:
 Incorrect FETCH response: %s.
 Invalid APOP timestamp.
 Invalid SSL protocol '%s' specified, using default (SSLv23).
 Invalid authentication `%s' specified.
 Invalid bad-header policy `%s' specified.
 Invalid protocol `%s' specified.
 Invalid userid or passphrase Issuer CommonName: %s
 Issuer Organization: %s
 KERBEROS v4 support is configured, but not compiled in.
 KERBEROS v5 support is configured, but not compiled in.
 Kerberos V4 support not linked.
 Kerberos V5 support not linked.
 LMTP delivery error on EOM
 Lead server has no name.
 Lock-busy error on %s@%s
 Logfile "%s" does not exist, ignoring logfile option.
 Logfile "%s" is not writable, aborting.
 Logfile is %s
 MD5 being applied to data block:
 MD5 result is:
 MDA MDA died of signal %d
 MDA open failed
 MDA returned nonzero status %d
 Maximum GSS token size is %ld
 Mechanism field incorrect
 Merged UID list from %s: Message termination or close of BSMTP file failed: %s
 Messages inserted into list on server. Cannot handle this.
 Missing trust anchor certificate: %s
 New UID list from %s: No IP address found for %s No envelope recipient found, resorting to header guessing.
 No interface found with name %s No mail for %s
 No mailservers set up -- perhaps %s is missing?
 No messages waiting for %s
 No suitable GSSAPI credentials found. Skipping GSSAPI authentication.
 No, their IP addresses don't match
 Node %s not allowed: %s
 Not running in daemon mode, ignoring logfile option.
 ODMR support is not configured.
 Old UID list from %s: OpenSSL reported: %s
 Option --all is not supported with %s
 Option --check is not supported with ETRN
 Option --check is not supported with ODMR
 Option --flush is not supported with %s
 Option --flush is not supported with ETRN
 Option --flush is not supported with ODMR
 Option --folder is not supported with ETRN
 Option --folder is not supported with ODMR
 Option --folder is not supported with POP3
 Option --keep is not supported with ETRN
 Option --keep is not supported with ODMR
 Option --limit is not supported with %s
 Options for retrieving from %s@%s:
 Out of memory!
 Outbound data:
 POP2 support is not configured.
 POP3 support is not configured.
 Parsing Received names "%-.*s"
 Parsing envelope "%s" names "%-.*s"
 Pending messages for %s started
 Please specify the service as decimal port number.
 Poll interval is %d seconds
 Polling %s
 Progress messages will be logged via syslog
 Protocol identified as IMAP2 or IMAP2BIS
 Protocol identified as IMAP4 rev 0
 Protocol identified as IMAP4 rev 1
 Query status=%d
 Query status=0 (SUCCESS)
 Query status=1 (NOMAIL)
 Query status=10 (SMTP)
 Query status=11 (DNS)
 Query status=12 (BSMTP)
 Query status=13 (MAXFETCH)
 Query status=2 (SOCKET)
 Query status=3 (AUTHFAIL)
 Query status=4 (PROTOCOL)
 Query status=5 (SYNTAX)
 Query status=6 (IOERR)
 Query status=7 (ERROR)
 Query status=8 (EXCLUDE)
 Query status=9 (LOCKBUSY)
 Queuing for %s started
 RPA Failed open of /dev/urandom. This shouldn't
 RPA Session key length error: %d
 RPA String too long
 RPA User Authentication length error: %d
 RPA _service_ auth fail. Spoof server?
 RPA authorisation complete
 RPA error in service@realm string
 RPA rejects you, reason unknown
 RPA rejects you: %s
 RPA status: %02X
 RPA token 2 length error
 RPA token 2: Base64 decode error
 RPA token 4 length error
 RPA token 4: Base64 decode error
 Realm list: %s
 Received BYE response from IMAP server: %s Received malformed challenge to "%s GSSAPI"!
 Releasing GSS credentials
 Repoll immediately on %s@%s
 Required APOP timestamp not found in greeting
 Required LOGIN capability not supported by server
 Required NTLM capability not compiled into fetchmail
 Required OTP capability not compiled into fetchmail
 Restricted user (something wrong with account) Routing message version %d not understood. SDPS not enabled. SMTP listener refused delivery
 SMTP listener rejected local recipient addresses:  SMTP server requires STARTTLS, keeping message.
 SMTP transaction SMTP: (bounce-message body)
 SSL connection failed.
 SSL is not enabled SSL support is not compiled in.
 Saved error is still %d
 Scratch list of UIDs: Secret pass phrase:  Sending credentials
 Server CommonName mismatch: %s != %s
 Server busy error on %s@%s
 Server certificate verification error: %s
 Server certificate:
 Server name not set, could not verify certificate!
 Server name not specified in certificate!
 Server rejected the AUTH command.
 Server requires integrity and/or privacy
 Server responded with UID for wrong message.
 Service challenge (l=%d):
 Service chose RPA version %d.%d
 Service has been restored.
 Service timestamp %s
 Session key established:
 Strange: MDA pclose returned %d and errno %d/%s, cannot handle at %s:%d
 String '%s' is not a valid number string.
 Subject Alternative Name: %s
 Subject CommonName: %s
 Subject: Fetchmail oversized-messages warning Subject: fetchmail authentication OK on %s@%s Subject: fetchmail authentication failed on %s@%s Subject: fetchmail sees repeated timeouts Success TLS is mandatory for this session, but server refused CAPA command.
 Taking options from command line%s%s
 The CAPA command is however necessary for TLS.
 The attempt to get authorization failed.
Since we have already succeeded in getting authorization for this
connection, this is probably another failure mode (such as busy server)
that fetchmail cannot distinguish because the server didn't send a useful
error message. The attempt to get authorization failed.
This probably means your password is invalid, but some servers have
other failure modes that fetchmail cannot distinguish from this
because they don't send useful error messages on login failure.

The fetchmail daemon will continue running and attempt to connect
at each cycle.  No future notifications will be sent until service
is restored. The following oversized messages remain on server %s account %s: The following oversized messages were deleted on server %s account %s: The nodetach option is in effect, ignoring logfile option.
 This could mean that the root CA's signing certificate is not in the trusted CA certificate location, or that c_rehash needs to be run on the certificate directory. For details, please see the documentation of --sslcertpath and --sslcertfile in the manual page.
 This could mean that the server did not provide the intermediate CA's certificate(s), which is nothing fetchmail could do anything about.  For details, please see the README.SSL-SERVER document that ships with fetchmail.
 This could mean that your mailserver is stuck, or that your SMTP
server is wedged, or that your mailbox file on the server has been
corrupted by a server error.  You can run `fetchmail -v -v' to
diagnose the problem.

Fetchmail won't poll this mailbox again until you restart it.
 This is fetchmail release %s Timestamp syntax error in greeting
 Token Length %d disagrees with rxlen %d
 Token length error
 Too many mails skipped (%d > %d) due to transient errors for %s
 Try adding the --service option (see also FAQ item R12).
 Trying to connect to %s/%s... Trying to continue with unqualified hostname.
DO NOT report broken Received: headers, HELO/EHLO lines or similar problems!
DO repair your /etc/hosts, DNS, NIS or LDAP instead.
 Turnaround now...
 Unable to open kvm interface. Make sure fetchmail is SGID kmem. Unable to parse interface name from %s Unable to process ATRN request now
 Unable to queue messages for node %s
 Undefined protocol request in POP3_auth
 Unexpected non-503 response to LMTP EOM: %s
 Unicode:
 Unknown ETRN error %d
 Unknown Issuer CommonName
 Unknown ODMR error "%s"
 Unknown Organization
 Unknown Server CommonName
 Unknown login or authentication error on %s@%s
 Unknown system error Unwrapped security level flags: %s%s%s
 User authentication (l=%d):
 User challenge:
 Using service name [%s]
 Value of string '%s' is %s than %d.
 WARNING: Running as root is discouraged.
 Warning: "Maillennium POP3" found, using RETR command instead of TOP.
 Warning: Issuer CommonName too long (possibly truncated).
 Warning: Issuer Organization Name too long (possibly truncated).
 Warning: ignoring bogus data for message sizes returned by the server.
 Warning: multiple mentions of host %s in config file
 Warning: the connection is insecure, continuing anyways. (Better use --sslcertck!)
 We've run out of allowed authenticators and cannot continue.
 Write error on fetchids file %s: %s
 Writing fetchids file.
 Yes, their IP addresses match
 You have no mail.
 Your operating system does not support SSLv2.
 about to deliver with: %s
 activity on %s -noted- as %d
 activity on %s checked as %d
 activity on %s was %d, is %d
 analyzing Received line:
%s attempt to re-exec fetchmail failed
 attempt to re-exec may fail as directory has not been restored
 awakened at %s
 awakened by %s
 awakened by signal %d
 background bogus EXPUNGE count in "%s"! bogus message count in "%s"! bogus message count! can't even send to %s!
 can't raise the listener; falling back to %s cannot create socket: %s
 challenge mismatch
 client/server protocol client/server synchronization connected.
 connection failed.
 connection to %s:%s [%s/%s] failed: %s.
 could not decode BASE64 challenge
 could not decode BASE64 ready response
 could not decode initial BASE64 challenge
 could not get current working directory
 could not open %s to append logs to
 couldn't fetch headers, message %s@%s:%d (%d octets)
 couldn't find HESIOD pobox for %s
 couldn't find canonical DNS name of %s (%s): %s
 couldn't time-check %s (error %d)
 couldn't time-check the run-control file
 dec64 error at char %d: %x
 decoded as %s
 discarding new UID list
 dup2 failed
 end of input error writing message text
 execvp(%s) failed
 expunge failed
 fetchlimit %d reached; %d message left on server %s account %s
 fetchlimit %d reached; %d messages left on server %s account %s
 fetchmail: %s configuration invalid, RPOP requires a privileged port
 fetchmail: %s configuration invalid, specify positive port number for service or port
 fetchmail: %s fetchmail at %ld killed.
 fetchmail: Cannot detach into background. Aborting.
 fetchmail: Error: multiple "defaults" records in config file.
 fetchmail: another foreground fetchmail is running at %ld.
 fetchmail: background fetchmail at %ld awakened.
 fetchmail: can't accept options while a background fetchmail is running.
 fetchmail: can't check mail while another fetchmail to same host is running.
 fetchmail: can't find a password for %s@%s.
 fetchmail: can't poll specified hosts with another fetchmail running at %ld.
 fetchmail: elder sibling at %ld died mysteriously.
 fetchmail: error killing %s fetchmail at %ld; bailing out.
 fetchmail: error opening lockfile "%s": %s
 fetchmail: error reading lockfile "%s": %s
 fetchmail: fork failed
 fetchmail: interface option is only supported under Linux (without IPv6) and FreeBSD
 fetchmail: invoked with fetchmail: lock creation failed.
 fetchmail: malloc failed
 fetchmail: monitor option is only supported under Linux (without IPv6) and FreeBSD
 fetchmail: no mailservers have been specified.
 fetchmail: no other fetchmail is running
 fetchmail: removing stale lockfile
 fetchmail: socketpair failed
 fetchmail: thread sleeping for %d sec.
 fetchmail: warning: no DNS available to check multidrop fetches from %s
 foreground forwarding and deletion suppressed due to DNS errors
 forwarding to %s
 found Received address `%s'
 get_ifinfo: malloc failed get_ifinfo: sysctl (iflist estimate) failed get_ifinfo: sysctl (iflist) failed getaddrinfo("%s","%s") error: %s
 getaddrinfo(NULL, "%s") error: %s
 gethostbyname failed for %s
 id=%s (num=%d) was deleted, but is still present!
 id=%s (num=%u) was deleted, but is still present!
 incorrect header line found - see manpage for bad-header option
 interval not reached, not querying %s
 invalid IP interface address
 invalid IP interface mask
 kerberos error %s
 krb5_sendauth: %s [server says '%s']
 larger line accepted, %s is an alias of the mailserver
 line rejected, %s is not an alias of the mailserver
 line: %s lock busy on server lock busy!  Is another session active?
 mail expunge mismatch (%d actual != %d expected)
 mail from %s bounced to %s
 mailbox selection failed
 malloc failed
 mapped %s to local %s
 mapped address %s to local %s
 message %s@%s:%d was not the expected length (%d actual != %d expected)
 message has embedded NULs missing IP interface address
 missing or bad RFC822 header name %d: cannot create socket family %d type %d: %s
 name %d: connection to %s:%s [%s/%s] failed: %s.
 nameserver failure while looking for '%s' during poll of %s: %s
 nameserver failure while looking for `%s' during poll of %s.
 no Received address found
 no address matches; forwarding to %s.
 no address matches; no postmaster set.
 no local matches, forwarding to %s
 no recipient addresses matched declared local names non-null instance (%s) might cause strange behavior
 normal termination, status %d
 not swapping UID lists, no UIDs seen this query
 passed through %s matching %s
 poll of %s skipped (failed authentication or too many timeouts)
 post-connection command failed with status %d
 post-connection command terminated with signal %d
 pre-connection command failed with status %d
 pre-connection command terminated with signal %d
 principal %s in ticket does not match -u %s
 protocol error
 protocol error while fetching UIDLs
 re-poll failed
 reading message %s@%s:%d of %d realloc failed
 receiving message data
 recipient address %s didn't match any local name restarting fetchmail (%s changed)
 running %s (host %s service %s)
 search for unseen messages failed
 seen seen selecting or re-polling default folder
 selecting or re-polling folder %s
 server option after user options server recv fatal
 skipping message %s@%s:%d skipping message %s@%s:%d (%d octets) skipping poll of %s, %s IP address excluded
 skipping poll of %s, %s down
 skipping poll of %s, %s inactive
 sleeping at %s for %d seconds
 smaller smtp listener protocol error
 socket starting fetchmail %s daemon
 swapping UID lists
 syslog and logfile options are both set, ignoring syslog, and logging to %s terminated with signal %d
 timeout after %d seconds waiting for %s.
 timeout after %d seconds waiting for listener to respond.
 timeout after %d seconds waiting for server %s.
 timeout after %d seconds waiting to connect to server %s.
 timeout after %d seconds.
 undefined unknown unknown (%s) unsupported protocol selected.
 usage:  fetchmail [options] [server ...]
 warning: Do not ask for support if all mail goes to postmaster!
 warning: multidrop for %s requires envelope option!
 will idle after poll
 writing RFC822 msgblk.headers
 Project-Id-Version: fetchmail 6.3.19-pre1
Report-Msgid-Bugs-To: fetchmail-devel@lists.berlios.de
POT-Creation-Date: 2013-04-23 23:24+0200
PO-Revision-Date: 2015-12-04 23:10+0000
Last-Translator: vinz65 <vinz65@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 16:20+0000
X-Generator: Launchpad (build 18115)
Language: it
 
Intercettato SIGINT, uscita in corso.
 
Ad ogni modo, se le impostazioni dell'account sono cambiate dal momento
dell'avvio del demone di Fetchmail, è necessario arrestare il demone,
modificare la configurazione del programma e poi riavviare il demone.

Il demone rimane in esecuzione e continuerà a tentare di collegarsi
a ogni ciclo. Nessuna ulteriore notifica sarà inviata finché il servizio
non sarà stato ripristinato.       --auth            tipo di autenticazione (password/kerberos/ssh/otp)
       --bad-header {reject|accept}
                    specificare la politica per il trattamento di messaggi con intestazione non valida
       --bsmtp           imposta il file di output BSMTP
       --fastuidl        effettua una ricerca binaria degli UIDL
       --fetchdomains    scarica la posta per i domini specificati
       --fetchsizelimit  imposta il limite delle dimensioni dei messaggi da scaricare
       --invisible       non scrivere «Received» e abilita lo spoofing dell'host
       --limitflush      elimina i messaggi sovradimensionati
       --lmtp            usa LMTP (RFC2033) per la consegna
       --nobounce        reindirizza le mail rifiutate dall'utente al postmaster.
       --nosoftbounce fetchmail elimina i messaggi non recapitabili in modo permanente.
       --pidfile         specifica un file PID (blocco) alternativo
       --plugin          specifica il comando esterno per aprire la connessione
       --plugout         specifica il comando esterno per aprire la connessione SMTP
       --port            porta TCP alla quale collegarsi (obsoleto, usare «--service» in sostituzione)
       --postmaster      specifica il ricevente che funge da ultima istanza
       --principal       principal del server di posta
       --showdots        mostra i puntini di avanziamento anche nei file di registro
       --smtpname        imposta il nome completo SMTP nomeutente@dominio
       --softbounce  mantiene sul server in modo permanente i messaggi non recapitabili (predefinito).
       --ssl             abilita le sessioni cifrate SSL
       --sslcert         certificato client SSL
       --sslcertck       effettua un rigoroso controllo del certificato del server (raccomandato)
       --sslcertfile     percorso del file del certificato CA SSL affidabile
       --sslcertpath     percorso della directory del certificato CA SSL affidabile
       --sslcommonname   si aspetta questo nome comune dal server (assolutamente sconsigliato)
       --sslfingerprint  impronta digitale che deve coincidere con quella del certificato del server.
       --sslkey          file di chiave privata SSL
       --sslproto        forza il protocollo SSL (SSL2/SSL3/TLS1)
       --syslog          usa syslog(3) per la maggior parte dei messaggi durante l'esecuzione come demone
       --tracepolls      aggiunge le informazioni di tracciamento dell'interrogazione all'intestazione «Received»
     attacchi replay da un servizio disonesto)
     non può essere sicuro che si stia parlando al
     impedisce l'accesso dell'utente, ma significa che
     servizio che si crede (possibili
    Effettuare una ricerca binaria degli UID durante ogni interrogazione («--fastuidl 0»).
   %d UID salvati.
   %d messaggio lungo %d ottetti eliminato da Fetchmail.   %d messaggi lunghi %d ottetti eliminati da Fetchmail.   %d messaggio lungo %d ottetti ignorato da Fetchmail.   %d messaggi lunghi %d ottetti ignorati da Fetchmail.   -?, --help       mostra questa opzione di aiuto
   -B, --fetchlimit  imposta il limite di scaricamento per connessioni a server
   -D, --smtpaddress imposta il dominio di consegna SMTP da usare
   -E, --envelope    intestazione dell'indirizzo nella busta
   -F, --flush       elimina i vecchi messaggi dal server
   -I, --interface     specifica richiesta d'interfaccia
   -K, --nokeep      elimina i nuovi messaggi dopo lo scaricamento
   -L, --logfile    specifica il nome del file di registro
   -M, --monitor       monitora l'interfaccia per attività
   -N, --nodetach   non staccare il processo demone
   -P, --service     servizio TCP al quale collegarsi (può essere una porta TCP numerica)
   -Q, --qvirtual    prefisso da rimuovere dall'ID dell'utente locale
   -S, --smtphost    imposta l'host di inoltro SMTP
   -U, --uidl        forza l'uso di UIDL (solo per POP3)
   -V, --version    mostra le informazioni sulla versione
   -Z, --antispam    imposta i valori della risposta antispam
   -a, --[fetch]all  scarica sia i vecchi che i nuovi messaggi
   -b, --batchlimit  imposta il limite batch per connessioni SMTP
   -c, --check      controlla nuovi messaggi senza scaricarli
   -d, --daemon     esegui come demone una volta ogni n secondi
   -e, --expunge     imposta il numero massimo di cancellazioni fra ripuliture
   -f, --fetchmailrc     specifica un file di controllo dell'esecuzione alternativo
   -i, --idfile          specifica un file UID alternativo
   -k, --keep        conserva i nuovi messaggi dopo lo scaricamento
   -l, --limit       non scaricare i messaggi al di sopra della dimensione data
   -m, --mda         imposta l'MDA da usare per l'inoltro
   -n, --norewrite   non riscrivere gli indirizzi dell'intestazione
   -p, --protocol    specifica il protocollo di scaricamento (vedere pagina man)
   -q, --quit       termina forzatamente il processo demone
   -r, --folder      specifica il nome della cartella remota
   -s, --silent     lavora in modalità silenziosa
   -t, --timeout     tempo massimo per mancata risposta del server
   -u, --username    specifica il login dell'utente sul server
   -v, --verbose    lavora in modalità prolissa (output diagnostico)
   -w, --warnings    intervallo fra le notifiche degli avvisi di posta
   segreto APOP = «%s».
   L'indirizzo da inserire nelle righe «RCPT TO» inviate a SMTP sarà %s
   Tutti i metodi di autenticazione disponibili saranno provati.
   Tutti i messaggi verranno recuperati («--all» abilitato).
   L'autenticazione CRAM-MD5 sarà forzata.
   La forzatura dei ritorni a capo è disabilitata («forcecr» disabilitato).
   La forzatura dei ritorni a capo è abilitata («forcecr» abilitato).
   La rimozione dei ritorni a capo è disabilitata («stripcr» disabilitato).
   La rimozione dei ritorni a capo è abilitata («stripcr» abilitato).
   La connessione deve avvenire attraverso l'interfaccia %s.
   La ricerca DNS per indirizzi multidrop è disabilitata.
   La ricerca DNS per indirizzi multidrop è abilitata.
   Selezionata la casella di posta predefinita.
   L'intervallo di cancellazione fra ripuliture è forzato a %d («--expunge %d»).
   Le righe «Delivered-To» verranno scartate («dropdelivered» abilitato)
   Le righe «Delivered-To» verranno mantenute («dropdelivered» disabilitato)
   Effettuare una ricerca binaria degli UID durante %d di %d interrogazioni («--fastuidl %d»).
   Effettuare una ricerca binaria degli UID durante ogni interrogazione («--fastuidl 1»).
   I domini per i quali le mail vengono scaricate sono:   Cifratura fra le due estremità presunta.
   L'intestazione della busta è presunta essere: %s
   L'instradamento «envelope-address» è disabilitato
   Il limite della dimensione dei messaggi da scaricare è %d («--fetchsizelimit %d»).
   I messaggi recuperati verranno mantenuti sul server («--keep» abilitato).
   I messaggi recuperati non verranno mantenuti sul server («--keep» disabilitato).
   L'autenticazione GSSAPI sarà forzata.
   La parte host della riga «MAIL FROM» sarà %s
   La modalità inattiva dopo lo scaricamento è disabilitata («idle» disabilitato).
   La modalità inattiva dopo lo scaricamento è abilitata («idle» abilitato).
   L'interpretazione di «Content-Transfer-Econding» è disabilitata («pass8bits» abilitato).
   L'interpretazione di «Content-Transfer-Encoding» è abilitata («pass8bits» disabilitato).
   L'autenticazione Kerberos V4 sarà forzata.
   L'autenticazione Kerberos V5 sarà forzata.
   Le connessioni all'ascoltatore verranno effettuate tramite il plugout %s («--plugout %s»).
   Domini locali:   La decodifica MIME è disabilitata («mimedecode» disabilitato).
   La decodifica MIME è abilitata («mimedecode» abilitato).
   L'autenticazione MSN sarà forzata.
   Il principal del server di posta è: %s
   Le mail saranno recuperate via %s
   Il limite della dimensione dei messaggi è %d ottetti («--limit %d»).
   L'intervallo fra avvisi relativi alla dimensione dei messaggi è %d secondi («--warnings %d»).
   I messaggi verranno inoltrati tramite %cMTP a:   I messaggi verranno aggiunti a %s come BSMTP
   I messaggi verranno consegnati con «%s».
   I messaggi con un'intestazione non valida verranno passati.
   I messaggi con un'intestazione non valida verranno respinti.
   Modalità multi-drop:    L'autenticazione NTLM sarà forzata.
   Nessun limite della serie di messaggi SMTP («--batchlimit 0»).
   Nessun UID salvato da questo host.
   Nessun limite della dimensione dei messaggi da scaricare («--fetchsizelimit 0»).
   Nessuna ripulitura forzata («--expunge 0»).
   Nessun requisito specificato per l'interfaccia.
   Nessun nome locale dichiarato per questo host.
   Nessun limite della dimensione dei messaggi («--limit 0»).
   Nessuna interfaccia di monitoraggio specificata.
   Nessun comando di plugin specificato.
   Nessun comando di plugout specificato.
   Nessuna informazione di tracciamento dell'interrogazione verrà aggiunta all'intestazione «Received».
   Nessun comando post-connessione.
   Nessun comando pre-connessione.
   Nessuna rimozione di prefissi
   Nessun limite dei messaggi ricevuti («--fetchlimit 0»).
   Le righe di stato non vuote verranno scartate («dropstatus» abilitato)
   Le righe di stato non vuote verranno mantenute («dropstatus» disabilitato)
   Numero di intestazioni delle buste da tralasciare: %d
   L'autenticazione OTP sarà forzata.
   I vecchi messaggi verranno cancellati prima dello scaricamento dei messaggi («--flush» abilitato).
   I vecchi messaggi non verranno cancellati prima dello scaricamento dei messaggi («--flush» disabilitato).
   Verranno scaricati solo i nuovi messaggi («--all» disabilitato).
   Le opzioni sono come segue:
   I messaggi sovradimensionati verranno cancellati prima dello scaricamento dei messaggi («--limitflush» abilitato).
   I messaggi sovradimensionati non verranno cancellati prima dello scaricamento dei messaggi («--limitflush» disabilitato).
   Proprietà pass-through «%s».
   Password = «%s».
   L'autenticazione password sarà forzata.
   La password verrà richiesta.
   L'interrogazione di questo server sarà effettuata ogni %d intervallo.
   L'interrogazione di questo server sarà effettuata ogni %d intervalli.
   Le informazioni di tracciamento dell'interrogazione verranno aggiunte all'intestazione «Received».
   Il ciclo di interrogazione monitorerà %s.
   Alias predichiarati del server di posta:   Il prefisso %s sarà rimosso dall'ID utente
   Il protocollo è %s   Il protocollo è KPOP con autenticazione Kerberos %s   identità RPOP = «%s».
   Il limite dei messaggi ricevuti è %d («--fetchlimit %d»).
   Gli ascoltatori delle risposte di blocco della posta indesiderata riconosciuti sono:   La riscrittura degli indirizzi del server locale è disabilitata («--norewrite» abilitato).
   La riscrittura degli indirizzi del server locale è abilitata («--norewrite» disabilitato).
   Il limite della serie di messaggi SMTP è %d.
   Sessione cifrata SSL abilitata.
   Impronta digitale SSL (controllata con il server delle chiavi): %s
   Protocollo SSL: %s.
   NomeComune del server SSL: %s
   Controllo del certificato del server SSL abilitato.
   Directory dei certificati SSL affidabile: %s
   File del certificato SSL affidabile: %s
   Le caselle di posta selezionate sono:   Gli alias del server verranno confrontati per indirizzo IP con gli indirizzi multidrop.
   Gli alias del server verranno confrontati per nome con gli indirizzi multidrop.
   La connessione al server verrà aperta con «%s».
   La connessione al server verrà chiusa con «%s».
   Le connessioni al server verranno effettuate tramite il plugin %s («--plugin %s»).
   Il limite di tempo per mancata risposta del server è %d secondi   Modalità single-drop:    Gli avvisi relativi alla dimensione dei messaggi verranno mostrati ad ogni interrogazione («--warnings 0»).
   Blocco della posta indesiderata disabilitato
   Questo host verrà interrogato quando nessuno host è specificato.
   Questo host non verrà interrogato quando nessuno host è specificato.
   Il vero nome del server è %s.
  (%d ottetti nel corpo del messaggio)  (%d ottetti nell'intestazione)  (%d ottetti)  (%d ottetti).
  (predefinito)  (predefinito).
  (forzando l'uso di UIDL)  (lunghezza -1)  (sovradimensionato)  (autorizzato in precedenza)  (con uso della porta predefinita)  (con uso del servizio %s)  <vuoto>  e   eliminato
  non eliminato
  mantenuto
 Connessione di %cMTP a %s fallita
 Errore %cMTP: %s
 L'ascoltatore %cMTP non gradisce l'indirizzo di destinatario «%s»
 L'ascoltatore %cMTP non gradisce assolutamente l'indirizzo di destinatario «%s»
 %d nome locale riconosciuto.
 %d nomi locali riconosciuti.
 %d messaggio (%d %s) per %s %d messaggi (%d %s) per %s %d messaggio per %s %d messaggi per %s %d messaggio in attesa dopo la ripulitura
 %d messaggi in attesa dopo la ripulitura
 %d messaggio in attesa dopo la prima interrogazione
 %d messaggi in attesa dopo la prima interrogazione
 %d messaggio in attesa dopo la re-interrogazione
 %d messaggi in attesa dopo la re-interrogazione
 %lu non è stato visto
 %s (messaggio di registro incompleto)
 %s in %s %s in %s (cartella %s) La configurazione %s non è valida, LMTP non può usare la porta SMTP predefinita
 %s connessione a %s fallita Errore %s durante il recupero da %s@%s
 Errore %s durante il recupero da %s@%s e la consegna all'host SMTP %s
 Impronte digitali %s non corrispondenti.
 Impronte digitali %s corrispondenti.
 Chiave %s impronta digitale: %s
 %s interrogazione di %s (protocollo %s) su %s: completata
 %s interrogazione di %s (protocollo %s) su %s: iniziata
 L'ascoltatore SMTP di %s non supporta ATRN
 L'ascoltatore SMTP di %s non supporta ESMTP
 L'ascoltatore SMTP di %s non supporta ETRN
 %s: la variabile di ambiente «NULLMAILER_FLAGS» è impostata.
Questo è pericoloso, in quanto può consentire che qmail-inject o il contenitore di qmail, sendmail,
manomettano i campi «From:», «Message-ID» o «Return-Path» nelle intestazioni dei messaggi.
Usare «env NULLMAILER_FLAGS= %s ARGOMENTI-QUI»
%s: annullato.
 %s: la variabile di ambiente «QMAILINJECT» è impostata.
Questo è pericoloso, in quanto può consentire che qmail-inject o il contenitore di qmail,
sendmail, manomettano i campi «From:» o «Message-ID» nelle intestazioni dei messaggi.
Usare «env QMAILINJECT= %s ARGOMENTI-QUI».
%s: annullato.
 %s: esistenza non riscontrata. Allontanarsi.
 %s: impossibile determinare l'host. %s: aggiornamento opportunistico a TLS fallito, tentativo di continuare in corso
 %s: aggiornamento opportunistico a TLS fallito, tentativo di continuare in corso.
 %s: aggiornamento a TLS fallito.
 %s: aggiornamento a TLS effettuato con successo.
 %s:%d: attenzione: trovato «%s» prima di qualsiasi nome di host
 %s:%d: attenzione: gettone sconosciuto «%s»
 %u è il primo non visto
 %u non è stato visto
 --
Il demone di Fetchmail Modalità «--check» attivata, nessun recupero di posta in corso
 La versione riscritta è % s.
 Richiesta ATRN rifiutata.
 In procinto di riscrivere % s
 Tutte le connessioni sono incastrate. Uscita in corso.
 Autenticazione richiesta.
 Autorizzazione riuscita su %s@%s
 Autorizzazione fallita su %s@%s%s
 Apertura del file BSMTP fallita: %s
 Scrittura del preambolo BSMTP fallita: %s.
 Risposta base64 dal server non valida.
 Certificato non valido: il nome alternativo del «Subject» contiene NUL, interruzione in corso.
 Certificato non valido: il nome comune del «Subject» contiene NUL, interruzione in corso.
 Certificato non valido: il nome comune del «Subject» è troppo lungo.
 «fetchall» e «keep on» contemporaneamente nel demone o in modalità inattiva costituiscono un errore.
 Catena di certificazione interrotta in: %s
 Buffer troppo piccolo. Questo è un bug nel chiamante di %s:%lu.
 Impossibile trovare il proprio host nel database degli host per qualificarlo.
 Impossibile gestire la risposta UIDL dal server upstream.
 Impossibile aprire il file fetchids %s per la scrittura: %s
 Impossibile rinominare il file fetchids %s in %s: %s
 Impossibile collegare il servizio %s al numero di porta
 Impossibile riportare l'id dell'utente effettivo all'originale %ld: %s
 Impossibile modificare l'id dell'utente effettivo in %ld: %s
 Certificato in profondità %d:
 Catena del certificato, dalla radice al nodo, che inizia in profondità %d:
 Verifica del certificato o dell'impronta digitale ignorata.
 Prova decodificata: %s
 Controllo se %s è veramente lo stesso nodo di %s in corso
 Comando non implementato
 Errori di connessione per questa interrogazione:
%s Copyright (C) 2002, 2003 Eric S. Raymond
Copyright (C) 2004 Matthias Andree, Eric S. Raymond,
                   Robert M. Funk, Graham Wilson
Copyright (C) 2005 - 2012 Sunil Shetye
Copyright (C) 2005 - 2013 Matthias Andree
 Impossibile decodificare la prova OTP
 Impossibile trovare il nome del servizio per [%s]
 Impossibile aprire i dati del livello di sicurezza
 Scambio di credenziali completato
 Nuovo tentativo di lettura del socket Cygwin
 Lettura del socket Cygwin fallita.
 ricerca DNS Errore di Deity Eliminazione del file fetchids in corso.
 Buffer del testo digest troppo piccolo.
 ERRORE: nessun supporto per la funzione getpassword()
 Autenticazione ESMPT CRAM-MD5 in corso
 Autenticazione ESMTP LOGIN in corso
 Autenticazione ESMPT PLAIN in corso
 Il supporto a ETRN non è configurato.
 Errore di sintassi ETRN
 Errore di sintassi ETRN nei parametri
 EVP_md5() fallito.
 Inserire la password per %s@%s:  Errore durante la creazione della richiesta del livello di sicurezza
 Errore durante l'eliminazione di %s: %s
 Errore in fase di scambio delle credenziali
 Errore in fase di rilascio delle credenziali
 Errore durante la scrittura su MDA: %s
 Errore durante la scrittura del file fetchids %s, lasciato il vecchio file al suo posto.
 Fetchmail viene distribuito ASSOLUTAMENTE SENZA ALCUNA GARANZIA. Questo è software libero
e la sua ridistribuzione è incoraggiata a talune condizioni. Per dettagli
consultare il file «COPYING» nella directory del sorgente o della documentazione.
 Impossibile ottenere la posta da %s@%s.
 Si sono verificate più di %d situazioni di tempo scaduto durante il tentativo di ottenere la posta da %s@%s.
 L'accesso a %s@%s è riuscito.
 Le mail di errore saranno indirizzate al postmaster.
 Le mail di errore saranno indirizzate al mittente.
 Tutti i messaggi «multidrop» male indirizzati saranno inoltrati a %s.
 Fetchmail si maschererà e non genererà «Received»
 I puntini di avanzamento saranno mostrati anche nei file di registro.
 Fetchmail tratterà gli errori permanenti come permanenti (scarta messaggi).
 Fetchmail tratterà gli errori permanenti come temporanei (mantieni messaggi).
 Il file «%s» deve essere un file normale.
 Il proprietario del file «%s» deve essere l'utente attuale.
 Il file «%s» deve avere non più dei permessi «-rwx------ (0700)».
 Descrittore del file fuori tolleranza per SSL Per un aiuto, consultare http://www.fetchmail.info/fetchmail-FAQ.html#R15
 Errore GSSAPI %s: %.*s
 Errore GSSAPI in gss_display_status chiamato da «%s»
 Il supporto per GSSAPI è configurato, ma non compilato.
 Ottenere risposta
 Ottenere risposta ha restituito %d [%s]
 Tentativo in corso di indovinare dall'intestazione «%- *s».
 Hdr non è 60
 Il supporto a IMAP non è configurato.
 Il file di identificazione è %s
 Se di desidera utilizzare le GSSAPI è necessario possedere anzitutto le credenziali, se possibile da kinit.
 Dati binari in entrata:
 Risposta FETCH non corretta: %s.
 Data e ora APOP non valide.
 Specificato un protocollo SSL «%s» non valido, utilizzo di quello predefinito (SSLv23) in corso.
 Autenticazione specificata «%s» non valida.
 Specificata politica «%s» relativa alle intestazioni non valide.
 Protocollo specificato «%s» non valido.
 Identificativo utente o passphrase errato Nome comune dell'emittente: %s
 Organizzazione emittente: %s
 Il supporto per KERBEROS V4 è configurato, ma non compilato.
 Il supporto per KERBEROS V5 è configurato, ma non compilato.
 Supporto a Kerberos V4 non collegato.
 Supporto a Kerberos V5 non collegato.
 Errore di consegna LMTP su EOM
 Il server principale non ha nome.
 Errore di lock-busy su %s@%s
 File di registro "%s" non presente, ignorare l'opzione logfile.
 Non è possibile scrivere nel file di registro "%s", interruzione in corso.
 Il file di registro è %s
 MD5 viene applicato al blocco dati:
 Il risultato di MD5 è:
 MDA MDA terminato da segnale %d
 Apertura di MDA fallita
 MDA ha ritornato uno stato %d diverso da zero
 La dimensione massima del gettone GSS è %ld
 Campo di meccanismo non corretto
 Unito l'elenco degli UID da %s: Terminazione del messaggio o chiusura del file BSTMP fallita: %s
 Messaggi inseriti nell'elenco sul server. Impossibile gestire questa situazione.
 Certificato di affidabilità mancante: %s
 Nuovo elenco degli UID da %s: Nessun indirizzo IP trovato per %s Non è stato trovato alcun destinatario, ritorno all'intestazione in corso.
 Nessuna interfaccia trovata con nome %s Nessun messaggio per %s
 Nessuna impostazione dei server di posta. Probabilmente questo è dovuto a %s mancante.
 Nessun messaggio in attesa per %s
 Non è stata trovata alcuna credenziale GSSAPI adatta. L'autenticazione GSSAPI viene omessa.
 No, i loro indirizzi IP non corrispondono
 Nodo %s non ammesso: %s
 Non è in esecuzione in modalità demone, ignorare l'opzione logfile.
 Il supporto a ODMR non è configurato.
 Vecchio elenco UID da %s: Segnalato OpenSSL: %s
 L'opzione «--all» non è supportata con %s
 L'opzione «--check» non è supportata con ETRN
 L'opzione «--check» non è supportata con ODMR
 L'opzione «--flush» non è supportata con %s
 L'opzione «--flush» non è supportata con ETRN
 L'opzione «--flush» non è supportata con ODMR
 L'opzione «--folder» non è supportata con ETRN
 L'opzione «--folder» non è supportata con ODMR
 L'opzione «--folder» non è supportata con POP3
 L'opzione «--keep» non è supportata con ETRN
 L'opzione «--keep» non è supportata con ODMR
 L'opzione «--limit» non è supportata con %s
 Opzioni per il recupero da %s@%s:
 Memoria esaurita.
 Dati in uscita:
 Il supporto a POP2 non è configurato.
 Il supporto a POP3 non è configurato.
 Analisi dei nomi di Received «%- *s» in corso
 Analisi della busta «%s» nomi «%- *s» in corso
 Messaggi in sospeso per %s iniziati
 Specificare il servizio come numero intero di porta.
 L'intervallo d'interrogazione è %d secondi
 Interrogazione di %s in corso
 I messaggi di avanzamento saranno registrati tramite il registro di sistema
 Protocollo identificato come IMAP2 o IMAP2BIS
 Protocollo identificato come IMAP4 rev 0
 Protocollo identificato come IMAP4 rev 1
 Stato dell'interrogazione = %d
 Stato dell'interrogazione = 0 (SUCCESS)
 Stato dell'interrogazione = 1 (NOMAIL)
 Stato dell'interrogazione = 10 (SMTP)
 Stato dell'interrogazione = 11 (DNS)
 Stato dell'interrogazione = 12 (BSMTP)
 Stato dell'interrogazione = 13 (MAXFETCH)
 Stato dell'interrogazione = 2 (SOCKET)
 Stato dell'interrogazione = 3 (AUTHFAIL)
 Stato dell'interrogazione = 4 (PROTOCOL)
 Stato dell'interrogazione = 5 (SYNTAX)
 Stato dell'interrogazione = 6 (IOERR)
 Stato dell'interrogazione = 7 (ERROR)
 Stato dell'interrogazione = 8 (EXCLUDE)
 Stato dell'interrogazione = 9 (LOCKBUSY)
 Accodamento per %s iniziato
 RPA ha fallito l'apertura di «/dev/urandom». Questo non dovrebbe accadere
 Errore RPA di lunghezza della chiave di sessione: %d
 Stringa RPA troppo lunga
 Errore RPA di lunghezza dell'autenticazione utente: %d
 RPA _service_auth fallita. Probabile server fasullo.
 Autorizzazione RPA completata
 Errore RPA nella stringa servizio@dominio
 Rifiutato da RPA, motivo sconosciuto
 Rifiutato da RPA: %s
 Stato RPA: %02X
 Errore RPA di lunghezza del gettone 2
 RPA gettone 2: errore di decodifica Base64
 Errore RPA di lunghezza del gettone 4
 RPA gettone 4: errore di decodifica Base64
 Elenco dei domini: %s
 Ricevuta risposta BYE dal server IMAP: %s Ricevuta prova malformata a «%s GSSAPI».
 Rilascio delle credenziali GSS in corso
 Reinterrogazione immediata su %s@%s
 Data e ora APOP sono richieste, ma non sono state trovate nel messaggio di benvenuto
 La capacità LOGIN richiesta non è supportata dal server
 La capacità NTLM richiesta non è compilata nel programma
 La capacità OTP richiesta non è compilata nel programma
 Utente limitato (qualche errore nell'account) Indirizzamento del messaggio versione %d incomprensibile. SDPS non abilitato. L'ascoltatore SMTP ha rifiutato la consegna
 L'ascoltatore SMTP ha rifiutato gli indirizzi dei destinatari locali:  Il server SMTP richiede STARTTLS, il messaggio viene mantenuto.
 transazione SMTP SMTP: (corpo del messaggio respinto)
 Connessione SSL fallita.
 SSL non è abilitato Il supporto per SSL non è compilato.
 L'errore salvato è ancora %d
 Elenco grezzo degli UID: Passphrase segreta:  Invio delle credenziali in corso
 Errore di corrispondenza del nome comune del server: %s != %s
 Errore di server occupato su %s@%s
 Errore di verifica del certificato del server: %s
 Certificato del server:
 Nome del server non impostato, impossibile verificare il certificato.
 Nome del server non specificato nel certificato.
 Il server ha rifiutato il comando AUTH.
 Il server richiede l'integrità, la riservatezza o entrambe
 Il server ha risposto con UID per messaggio errato.
 Prova di servizio (l=%d):
 Servizio RPA scelto versione %d.%d
 Il servizio è stato ripristinato.
 Servizio di data e ora %s
 Chiave di sessione stabilita:
 Strano: MDA pclose ha restituito %d ed errore numero %d/%s, impossibile trattare a %s:%d
 La stringa «%s» non è una stringa numerica valida.
 Nome alternativo del «Subject»: %s
 Nome comune dell'oggetto: %s
 Oggetto: avviso di Fetchmail per messaggi sovradimensionati Oggetto: autenticazione riuscita su %s@%s Oggetto: autenticazione fallita su %s@%s Oggetto: si sono verificate ripetute situazioni di tempo scaduto Successo TLS è obbligatorio per questa sessione, ma il server rifiuta il comando CAPA.
 Ottenimento delle opzioni dalla riga di comando %s%s in corso
 Il comando CAPA è comunque necessario per TLS.
 Il tentativo di ottenere l'autorizzazione è fallito.
Poiché in precedenza questa connessione era stata autorizzata, questa
situazione è probabilmente dovuta ad altre cause, per esempio a server occupato,
che non è possibile determinare in quanto il server non ha inviato un messaggio
d'errore utile. Il tentativo di ottenere l'autorizzazione è fallito.
Questo è probabilmente dovuto a password non valida, ma taluni server
hanno altre modalità d'errore che il programma non può distinguere da
questa in quanto non inviano messaggi d'errore utili su errori di accesso.

Il demone di Fetchmail rimane in esecuzione e continuerà a tentare di
collegarsi ad ogni ciclo. Nessuna ulteriore notifica sarà inviata finché il
servizio non sarà stato ripristinato. I seguenti messaggi sovradimensionati rimangono sul server %s account %s: I seguenti messaggi sovradimensionati sono stati eliminati sul server %s account %s: L'opzione nodetach è attiva, ignorare l'opzione logfile.
 Questo potrebbe significare che il certificato firmato della CA principale non è nel percorso dei certificati attendibili CA, o che è necessario eseguire un c_rehash nella directory dei certificati. Per ulteriori dettagli consultare la pagina di --sslcertpath e --sslcertfile nel manuale.
 Questo potrebbe significare che il server non ha fornito il certificato(i) CA intermedio, non c'è niente che fetchmail possa fare al riguardo. Per ulteriori dettagli consultare il documento README.SSL-SERVER fornito con fetchmail.
 Questo potrebbe significare che il proprio server di posta è bloccato, o che il proprio server SMTP
è incastrato, oppure che il file della casella di posta sul server è stato
compromesso da un errore del server. Per diagnosticare il problema,
eseguire «fetchmail -v -v».

Questa casella di posta elettronica non sarà interrogata ulteriormente fino al prossimo riavvio del programma.
 Questo è Fetchmail rilascio %s Errore di sintassi di data e ora nel messaggio di benvenuto
 La lunghezza del gettone %d non corrisponde a rxlen %d
 Errore di lunghezza del gettone
 Troppe email saltate (%d > %d) a causa di errori transitori per %s
 Provare ad aggiungere l'opzione «--service» (vedere anche le FAQ, paragrafo R12).
 Tentativo di connessione a %s/%s in corso Tentativo di continuare a ricevere con nome di host non qualificato in corso.
NON si riportino intestazioni «Received:» interrotte, righe «HELO/EHLO» o problemi simili.
Si provveda, invece, alla riparazione del proprio /etc/hosts, DNS, NIS o LDAP.
 Capovolgimento in corso
 Impossibile aprire l'interfaccia kvm. Assicurarsi che Fetchmail sia «SGID kmem». Impossibile interpretare il nome d'interfaccia da %s Impossibile elaborare la richiesta ATRN adesso
 Impossibile accodare i messaggi per il nodo %s
 Richiesto un protocollo non definito in POP3_auth
 Risposta differente da 503 inattesa a LTP EOM: %s
 Unicode:
 Errore di ETRN sconosciuto %d
 Nome comune dell'emittente sconosciuto
 Errore ODMR sconosciuto «%s»
 Organizzazione sconosciuta
 Nome comune del server sconosciuto
 Errore sconosciuto di login o di autenticazione su %s@%s
 Errore di sistema sconosciuto Contrassegni del livello di sicurezza aperti: %s%s%s
 Autenticazione utente (l=%d):
 Prova utente:
 Con uso del nome del servizio [%s]
 Il valore della stringa «%s» è %s piuttosto che %d.
 ATTENZIONE: l'esecuzione come root è fortemente sconsigliata.
 Attenzione: trovato "Maillennium POP3", utilizzare il comando RETR invece di TOP.
 Attenzione: nome comune dell'emittente troppo lungo (possibile troncatura).
 Attenzione: nome dell'organizzazione emittente troppo lungo (possibile troncatura).
 Avviso: i dati fasulli per le dimensioni dei messaggi restituiti dal server vengono ignorati.
 Attenzione: l'host %s è menzionato più volte nel file di configurazione
 Attenzione: si continua malgrado la connessione non sia sicura (si raccomanda l'utilizzo di --sslcertck)
 Impossibile continuare, esauriti i metodi di autenticazione permessi.
 Errore durante la scrittura nel file fetchids: %s %s
 Scrittura del file fetchids in corso.
 Sì, i loro indirizzi IP corrispondono
 Non ci sono nuovi messaggi.
 Il sistema operativo non supporta SSLv2.
 in procinto di consegnare con: %s
 attività su %s notata come %d
 attività su %s controllata come %d
 l'attività su %s era %d, ora è %d
 analisi della riga «Received» in corso:
%s tentativo di nuova esecuzione fallito
 il tentativo di nuova esecuzione potrebbe fallire, poiché la directory non è stata ripristinata
 ridestato su %s
 ridestato da %s
 ridestato dal segnale %d
 in secondo piano conteggio di EXPUNGE errato in %s. conteggio di messaggi errato in %s. conteggio di messaggi errato. impossibile anche inviare a %s.
 impossibile raggiungere l'ascoltatore, ritorno su %s in corso impossibile creare il socket: %s
 differenza nella prova
 protocollo client/server sincronizzazione client/server connesso.
 connessione fallita.
 connessione a %s:%s [%s/%s] fallita: %s.
 impossibile decodificare la prova BASE64
 impossibile decodificare la risposta di pronto BASE64
 impossibile decodificare la prova BASE64
 impossibile ottenere la directory di lavoro attuale
 impossibile aprire %s per aggiungervi messaggi di registro
 impossibile recuperare le intestazioni, messaggio %s@%s: %d (%d ottetti)
 impossibile trovare la casella di posta HESIOD per %s
 impossibile trovare il nome canonico DNS di %s (%s): %s
 impossibile temporizzare %s (errore %d)
 impossibile temporizzare il file di controllo di esecuzione
 Errore dec64 in carattere %d: %x
 decodificato come %s
 il nuovo elenco degli UID viene scartato
 dup2 fallita
 fine dell'input errore durante la scrittura del testo del messaggio
 execvp(%s) fallita
 ripulitura fallita
 limite di scaricamento di Fetchmail %d raggiunto; %d messaggio lasciato sul server %s, account %s
 limite di scaricamento di Fetchmail %d raggiunto; %d messaggi lasciati sul server %s, account %s
 fetchmail: la configurazione %s non è valida, RPOP richiede una porta privilegiata
 fetchmail: la configurazione %s non è valida, specificare un numero di porta positivo per il servizio o la porta
 fetchmail: %s Fetchmail su %ld arrestato forzatamente.
 fetchmail: impossibile portare in secondo piano. Annullamento in corso.
 fetchmail: errore: molteplici record «defaults» nel file di configurazione.
 fetchmail: un'altra istanza di Fetchmail è in esecuzione in primo piano su %ld.
 fetchmail: Fetchmail in secondo piano ridestato su %ld.
 fetchmail: impossibile accettare opzioni mentre un'istanza di Fetchmail è in esecuzione in secondo piano.
 fetchmail: impossibile controllare la posta mentre un'altra istanza di Fetchmail è in esecuzione sul medesimo host.
 fetchmail: impossibile trovare una password per %s@%s.
 fetchmail: impossibile interrogare gli host specificati mentre un'altra istanza di Fetchmail è in esecuzione su %ld.
 fetchmail: il fratello maggiore su %ld è morto misteriosamente.
 fetchmail: errore in fase di arresto forzato di %s Fetchmail su %ld; uscita in corso.
 fetchmail: errore durante l'apertura del file di blocco «%s»: %s
 fetchmail: errore durante la lettura del file di blocco «%s»: %s
 fetchmail: fork fallita
 fetchmail: l'opzione di interfaccia è supportata solo in Linux (senza IPv6) e FreeBSD
 fetchmail: invocato con fetchmail: creazione del blocco fallita.
 fetchmail: malloc fallita
 fetchmail: l'opzione di monitoraggio è supportata solo in Linux (senza IPv6) e FreeBSD
 fetchmail: nessun server di posta specificato.
 fetchmail: nessun'altra istanza di Fetchmail in esecuzione
 fetchmail: rimozione del vecchio file di blocco in corso
 fetchmail: socketpair fallita
 fetchmail: processo in pausa per %d secondi.
 fetchmail: avviso: nessun DNS disponibile per controllare recuperi «multidrop» da %s
 in primo piano inoltro e cancellazione soppressi a causa di errori DNS
 inoltro a %s in corso
 trovato indirizzo «Received» «%s»
 get_ifinfo: malloc fallita get_ifinfo: sysctl (iflist estimate) fallita get_ifinfo: sysctl (iflist) fallita getaddrinfo("%s","%s") errore: %s
 getaddrinfo(NULL, "%s") errore: %s
 gethostbyname fallito per %s
 id=%s (num=%d) è stato eliminato, ma è sempre presente.
 id=%s (num=%u) è stato eliminato, ma è sempre presente.
 trovata riga d'intestazione non corretta. Consultare le pagine del manuale per opzioni dell'intestazione non corrette
 intervallo non raggiunto, nessuna interrogazione di %s in corso
 l'indirizzo dell'interfaccia IP non è valido
 la maschera dell'interfaccia IP non è valida
 errore di kerberos %s
 krb5_sendauth: %s [il server ha risposto «%s»].
 più grande riga accettata, %s è un alias del server di posta
 riga rifiutata, %s non è un alias del server di posta
 riga: %s lock busy su server Il blocco è occupato. Controllare se un'altra sessione è attiva.
 differenza durante la ripulitura della posta (%d effettivo != %d atteso)
 posta da %s respinta a %s
 selezione della casella di posta fallita
 malloc fallita
 %s collegato a %s locale
 indirizzo %s collegato a %s locale
 il messaggio %s@%s:%d non era della lunghezza attesa (%d effettiva != %d attesa)
 il messaggio incorpora dei NUL l'indirizzo dell'interfaccia IP è mancante
 intestazione RFC822 mancante o errata nome %d: impossibile creare la famiglia di socket %d di tipo %d: %s
 nome %d: connessione a %s:%s [%s/%s] fallita: %s.
 errore del nameserver nella ricerca di «%s» durante l'interrogazione di %s: %s
 errore del nameserver nella ricerca di «%s» durante l'interrogazione di %s
 nessuno indirizzo «Received» trovato
 nessun indirizzo corrispondente, inoltro a %s in corso.
 nessun indirizzo corrispondente; nessun postmaster è stato impostato.
 nessuna corrispondenza locale, inoltro a %s in corso
 nessun indirizzo di destinatario corrisponde ai nomi locali dichiarati l'istanza non nulla (%s) potrebbe causare strani comportamenti
 interruzione normale, stato %d
 nessuno scambio di elenchi degli UID in corso, nessun UID trovato durante l'interrogazione
 attraversamento di %s corrispondente a %s
 interrogazione di %s omessa (autenticazione fallita o troppe situazioni di tempo scaduto)
 comando post-connessione fallito con stato %d
 comando post-connessione è terminato con stato %d
 comando di pre-connessione fallito con stato %d
 comando di pre-connessione è terminato con stato %d
 il principal %s nel ticket non corrisponde a -u %s
 errore di protocollo
 errore di protocollo durante lo scaricamento degli UIDL
 re-interrogazione fallita
 lettura del messaggio %s@%s in corso: %d di %d realloc fallita
 ricezione dei dati del messaggio in corso
 l'indirizzo del destinatario %s non corrisponde ad alcun nome locale riavvio in corso (%s modificato)
 esecuzione di %s (host %s servizio %s) in corso
 ricerca di messaggi non visti fallita
 visto visti selezione o reinterrogazione della cartella predefinita in corso
 selezione o reinterrogazione della cartella %s in corso
 opzione del server dopo le opzioni dell'utente server recv fatale
 messaggio %s@%s ignorato: %d messaggio %s@%s ignorato: %d (%d ottetti) interrogazione di %s tralasciata, %s ha un indirizzo IP escluso
 interrogazione di %s tralasciata, %s risulta disconnesso
 interrogazione di %s tralasciata, %s risulta inattivo
 pausa su %s per %d secondi
 più piccolo errore di protocollo dell'ascoltatore SMTP
 socket avvio del demone di Fetchmail %s in corso
 scambio di elenchi degli UID in corso
 Le opzioni syslog e logfile sono entrambi impostate, ignorare syslog e accedere a %s terminato con segnale %d
 tempo scaduto dopo %d secondi in attesa di %s.
 tempo scaduto dopo %d secondi in attesa della risposta dell'ascoltatore.
 tempo scaduto dopo %d secondi in attesa del server %s.
 tempo scaduto dopo %d secondi durante l'attesa della connessione al server %s.
 tempo scaduto dopo %d secondi.
 indefinito sconosciuto sconosciuto (%s) selezionato un protocollo non supportato.
 Uso: fetchmail [opzioni] [server...]
 avviso: non richiedere supporto se tutte le mail vanno al postmaster.
 avviso: «multidrop» per %s richiede l'opzione «envelope».
 Entrata in inattività dopo l'interrogazione
 scrittura di intestazioni msgblk RFC822 in corso
 