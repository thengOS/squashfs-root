��    E      D  a   l      �     �  @   	  /   J  &   z  )   �  -   �     �     �          &     =     I      P     q  -   v  =   �  +   �       
   &     1  B   6     y     �  )   �     �  
   �     �  !   �      �  	   	     "	     /	  -   7	     e	     k	     r	  
   �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	  
   �	  #   
     ,
     2
     9
  !   A
  
   c
  )   n
  $   �
  (   �
  *   �
  -     $   ?  +   d  4   �  (   �     �     �                    (  �  .     .  =   K  *   �      �  &   �  %   �  
   "     -     J  )   R     |     �  #   �     �  *   �  Y   �  +   F     r     �     �  >   �  9   �       6        N     U     ]  *   b     �  	   �     �     �  )   �     �  
   �               #  	   4     >     Z     `     l     u     �     �     �  "   �     �  
   �  
   �     �     �  /     &   @  !   g  +   �  0   �  $   �  /     2   ;  ,   n     �  )   �     �     �     �     �        7                     B                 6   1      =   >       E               8       $              ;   %   -       ?   *             D       )   0       '              &      !      <   	   :   +   3                 A      /              C   
   #                          2                (   @   4       5             "       9   ,         .        A triangle Kaleidoscope Add a loopback alpha blending effector with rotating and scaling Add age to video input using scratches and dust Add more saturation to the video input Add some hallucination to the video input Add the ripple mark effect on the video input Bulge Bulges the center of the video Cartoon Cartoonify video input Che Guevara Chrome Detect radioactivity and show it Dice Dices the video input into many small squares Display video input like good old low resolution computer way Dissolves moving objects in the video input Distort the video input Distortion Edge Extracts edges in the video input through using the Sobel operator Fake heat camera toning Flip Flip the image, as if looking at a mirror Heat Historical Hulk Invert and slightly shade to blue Invert colors of the video input Invertion Kaleidoscope Kung-Fu Makes a square out of the center of the video Mauve Mirror Mirrors the video Noir/Blanc Optical Illusion Pinch Pinches the center of the video Quark Radioactive Ripple Saturation Sepia Sepia toning Shagadelic Show what was happening in the past Sobel Square Stretch Stretches the center of the video Time delay Traditional black-white optical animation Transform motions into Kung-Fu style Transform video input into a mauve color Transform video input into a metallic look Transform video input into a waveform monitor Transform video input into grayscale Transform video input into realtime goo'ing Transform video input into typical Che Guevara style Transform yourself into the amazing Hulk Twirl Twirl the center of the video Vertigo Warp Waveform X-Ray Project-Id-Version: cheese
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-video-effects&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-10-29 03:54+0000
PO-Revision-Date: 2014-04-10 07:12+0000
Last-Translator: Claudio Arseni <claudio.arseni@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:02+0000
X-Generator: Launchpad (build 18115)
 Un caleidoscopio triangolare Aggiunge una miscela loopback alpha con rotazioni e sfumature Invecchia il video usando graffi e polvere Aumenta la saturazione del video Aggiunge alcune allucinazioni al video Aggiunge un effetto ondulato al video Rigonfiato Rigonfia il centro del video Fumetto Trasforma il video con un effetto fumetto Che Guevara Cromato Trova la radioattività e la mostra A quadratini Traglia il video in tanti piccoli quadrati Mostra il video con un effetto con il buon vecchio stile dei computer a bassa risoluzione Dissolve gli oggetti in movimento nel video Distorce il video Distorto Bordato Estrae i bordi nel video attraverso l'uso dell'operatore Sobel Aggiunge un finto calore alle tonalità della videocamera Riflesso Riflette l'immagine, come fosse davanti a uno specchio Calore Storico Hulk Inverte e ombreggia leggermente con il blu Inverte i colori del video Invertito Caleidoscopico Square Inserisce un quadrato al centro del video Viola Specchiato Specchia il video Bianco e nero Illusione ottica Strozzato Strozza il centro del video Quark Radioattivo Ondulato Saturazione Seppia Tonalità seppia Psichedelico Mostra cosa è accaduto in passato Sobel Riquadrato Stiramento Stira il centro del video Tempo di ritardo Tradizionale animazione ottica in bianco e nero Trasforma i movimenti in stile Kung-Fu Aggiunge al video il colore viola Trasforma il video con un effetto metallico Trasforma il video in un monitor di forme d'onda Trasforma il video in toni di grigio Trasforma il video dando un effetto appiccicoso Trasforma il video con il tipico stile Che Guevara Trasforma l'immagine nello stupefacente Hulk Spirale Inserisce una spirale al centro del video Vertigo Distorsione Forme d'onda Raggi X 