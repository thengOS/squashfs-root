��    
      l      �       �   #   �   #        9      R     s  &   �     �      �  "   �  �    /   �  /   �       #   !     E  7   a     �  '   �  -   �                               
          	    Dump all parameters for all objects Enumerate objects paths for devices Exit after a small delay Exit after the engine has loaded Get the wakeup data Monitor activity from the power daemon Monitor with detail Show extra debugging information Show information about object path Project-Id-Version: UPower 011
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-15 11:28+0000
PO-Revision-Date: 2011-12-05 19:31+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-10-09 11:42+0000
X-Generator: Launchpad (build 18227)
 Riversa tutti i parametri per tutti gli oggetti Enumera i percorsi di oggetti per i dispositivi Esce dopo un piccolo ritardo Esce dopo il caricamento del motore Ottiene i dati di risveglio Monitoraggio dell'attività del demone di alimentazione Monitoraggio con dettagli Mostra informazioni di debug aggiuntive Mostra informazioni sul percorso dell'oggetto 