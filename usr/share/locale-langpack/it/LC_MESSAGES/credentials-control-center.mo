��          �   %   �      P      Q     r     �  #   �  =   �  @   �     5     B     O     \     l  '   �     �  #   �  1   �  2        A  F   P  $   �  "   �  [   �  :   ;  X   v  S   �  '   #  �  K  .        :     N  &   d  .   �  E   �      	     	     !	     -	     <	  /   Y	     �	  *   �	  :   �	  /   �	     '
  L   7
  /   �
  (   �
  g   �
  7   E  Q   }  Q   �  :   !                                                                                                    	               
           - Edit Web credentials settings Add account… All applications Another instance is already running Are you sure that you wish to remove this Ubuntu Web Account? Control whether this application integrates with Online Accounts Edit Options Grant access Legal notice Online Accounts Online Accounts preferences Online account credentials and settings Options Output version information and exit Please authorize Ubuntu to access your %s account Please authorize Ubuntu to access your %s account: Remove Account Run '%s --help' to see a full list of available command line options.
 Select to configure a new %s account Show accounts that integrate with: The Web Account which manages the integration of %s with your applications will be removed. The following applications integrate with your %s account: There are currently no account providers available which integrate with this application There are currently no applications installed which integrate with your %s account. Your online %s account is not affected. Project-Id-Version: gnome-control-center-signon
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-07-19 13:32+0000
PO-Revision-Date: 2013-03-19 20:53+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-10-09 11:50+0000
X-Generator: Launchpad (build 18227)
  - Modifica impostazioni delle credenziali web Aggiungi account... Tutte le applicazioni Un'altra istanza è già in esecuzione Rimuovere veramente questo account web Ubuntu? Controlla se questa applicazione si integra o meno con Account online Modifica opzioni Concedi accesso Note legali Account online Preferenze di Account online Credenziali e impostazioni degli account online Opzioni Stampa le informazioni di versione ed esce Autorizzare Ubuntu affinché possa accedere all'account %s Autorizzare Ubuntu per accedere all'account %s: Rimuovi account Eseguire «%s --help» per un elenco completo di opzioni a riga di comando.
 Selezionare per configurare un nuovo account %s Mostra gli account che si integrano con: Verrà rimosso l'account web che gestisce l'integrazione tra %s e le applicazioni installate in Ubuntu. Le seguenti applicazioni si integrano con l'account %s: Non è disponibile alcun account provider che si integra con questa applicazione. Al momento non ci sono applicazioni installate che si integrano con l'account %s. Il proprio account online %s resterà comunque inalterato. 