��    
      l      �       �   ,   �   �         �  �   �     r  7   �     �  /   �  ~     �  �  )   L  �   v  (   :  �   c  1   )  K   [  )   �  7   �  �   	                        	                 
    @info<link url='%1'>Previous Reports</link> @infoKubuntu can collect anonymous information that helps developers improve it. All information collected is covered by our <link url='%1'>privacy policy</link>. EMAIL OF TRANSLATORSYour emails Error reports include information about what a program was doing when it failed. You always have the choice to send or cancel an error report. NAME OF TRANSLATORSYour names Send a report automatically if a problem prevents login Send error reports to Canonical Send occasional system information to Canonical This includes things like how many programs are running, how much disk space the computer has, and what devices are connected. Project-Id-Version: kde-config-whoopsie
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-03-27 00:49+0000
PO-Revision-Date: 2014-05-05 10:39+0000
Last-Translator: Gianfranco Frisani <gfrisani@libero.it>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:31+0000
X-Generator: Launchpad (build 18115)
 <link url='%1'>Rapporti precedenti</link> Kubuntu è in grado di raccogliere informazioni anonime che aiutano gli sviluppatori a migliorarlo. Tutte le informazioni raccolte sono coperte dalla <link url='%1'>politica della privacy</link>. ,,gfrisani@libero.it,gio.scino@gmail.com Segnalazioni errori include informazioni su ciò che un programma stava eseguendo quando si è verificato l'errore. È sempre possibile scegliere se inviare o cancellare una segnalazione di errore.  ,Launchpad Contributions:,Gianfranco Frisani,Gio Inviare automaticamente una segnalazione se un problema impedisce l'accesso Inviare segnalazioni d'errore a Canonical Inviare occasionali informazioni di sistema a Canonical Ciò include elementi quali il numero di programmi in esecuzione, la quantità di spazio sul disco è a disposizione del computer e quali dispositivi sono collegati. 