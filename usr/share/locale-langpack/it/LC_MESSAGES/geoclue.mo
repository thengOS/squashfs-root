��          �      \      �  /   �               1  !   H  6   j  !   �     �     �     �     �          !  7   A     y  Y   |     �     �     �  �  �  B   �     �  &   �     !     ?  9   _  %   �     �     �     �     �  %     )   .  9   X     �  a   �  
   �                                                    
                     	                                    Allow '%s' to access your location information? Demo geoclue agent Demo geolocation application Display version number Enable submission of network data Exit after T seconds of inactivity. Default: 0 (never) Exit after T seconds. Default: 30 Find your current location GeoClue Geoclue Demo agent Geolocation Geolocation service in use
 Geolocation service not in use
 Nickname to submit network data under (2-32 characters) No Request accuracy level A. Country = 1, City = 4, Neighborhood = 5, Street = 6, Exact = 8. Where am I? Yes geolocation; Project-Id-Version: geoclue-2.0
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-12 09:11+0000
PO-Revision-Date: 2016-03-15 09:06+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:33+0000
X-Generator: Launchpad (build 18115)
 Consentire a «%s» di accedere alla informazioni sulla posizione? Agente geoclue demo Demo applicazione di geolocalizzazione Visualizza numero di versione Abilita l'invio di dati di rete Esce dopo T secondi di inattività (predefinito: 0 - mai) Esce dopo T secondi (predefinito: 30) Trova la posizione attuale GeoClue Agente geoclue demo Geolocalizzazione Servizio di geolocalizzazione in uso
 Servizio di geolocalizzazione non in uso
 Soprannome da usare per l'invio dei dati (2-32 caratteri) No Richiede il livello di precisione A (paese = 1, città = 4, vicinato = 5, strada = 6, esatta = 8) Dove sono? Sì posizione;geolocalizzazione; 