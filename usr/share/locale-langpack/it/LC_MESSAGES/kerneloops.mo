��          |      �             !  $   (  �   M  &   6     ]     c     o  �   r     G      K     l  �  ~     -  /   4    d  4   �     �     �     �  �   �     �  )   �     �     
                                            	          Always Connecting to system bus failed: %s
 Diagnostic information from your Linux kernel has been sent to <a href="http://oops.kernel.org">oops.kernel.org</a> for the Linux kernel developers to work on. 
Thank you for contributing to improve the quality of the Linux kernel.
 Kernel bug diagnostic information sent Never Never again No There is diagnostic information available for this failure. Do you want to submit this information to the <a href="http://oops.kernel.org/">www.oops.kernel.org</a> website for use by the Linux kernel developers?
 Yes Your system had a kernel failure kerneloops client Project-Id-Version: kerneloops
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-01-01 06:36-0800
PO-Revision-Date: 2016-03-25 15:29+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:32+0000
X-Generator: Launchpad (build 18115)
 Sempre Connessione al bus di sistema non riuscita: %s
 Informazioni diagnostiche riguardo alla propria versione del kernel Linux sono state inviate a <a href="http://oops.kernel.org">oops.kernel.org</a> affinché gli sviluppatori possano controllare il problema.
Grazie per il vostro contributo per migliorare la qualità del kernel Linux.
 Informazioni diagnostiche sul bug del kernel inviate Mai Mai più No Sono disponibili informazioni diagnostiche riguardo al problema. Inviarle al sito web <a href="http://oops.kernel.org/">oops.kernel.org</a> affinché gli sviluppatori del kernel Linux le possano utilizzare?
 Sì Si è verificato un fallimento del kernel Client kerneloops 