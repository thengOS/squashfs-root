��    	     d  �  �       �+  ~   �+  X    ,  H   y,     �,     �,     �,  &   �,  &   -  %   D-  $   j-  =   �-  '   �-     �-     .     .  �   +.  :   �.     �.     �.     �.     /     /  
   %/     0/     9/     F/  	   M/     W/  $   p/     �/     �/     �/     �/     �/  &   0  <   90     v0     �0     �0      �0  	   �0  2   �0  2   1     A1      I1  #   j1     �1     �1     �1  O   �1     2     12     =2     E2     N2     U2     g2     {2     �2     �2     �2     �2  "   �2     3  D   3  �   Z3  7   4  C   T4  3   �4     �4     �4     �4  f   5  (   �5      �5     �5  6   �5      6     <6     O6     f6     u6     |6     �6     �6     �6     �6     �6  	   �6     �6     �6     7     7     7     $7     47     :7     C7     c7     }7  #   �7  #   �7  +   �7     8     8     8     %8  
   28     =8     S8     e8     �8     �8     �8     �8     �8     �8     �8     �8      9     9  #   79     [9     y9     �9     �9     �9     �9     �9     �9  	   �9      :     :     1:     A:     E:     Y:     l:     t:     �:     �:     �:     �:     �:     �:     �:  	   ;     ;     2;     I;     b;     t;  
   �;      �;  !   �;     �;     �;  
   <     !<  :   5<  $   p<     �<     �<     �<     �<  $   �<     �<     �<     =     !=     7=  +   L=  "   x=     �=  1   �=     �=     >     &>     D>  �   [>     =?     \?      {?      �?     �?     �?     �?     �?  
   @     @     -@     J@     Z@  8   r@     �@     �@     �@     �@     �@     A     A     *A     0A     =A  	   IA  5   SA  %   �A  %   �A      �A     �A     B  $    B     EB     TB     bB     oB     }B  (   �B     �B     �B     �B     �B  �   C     �C  4   �C     �C     �C     �C     �C     D     D     1D  %   OD     uD     �D     �D     �D     �D     �D     E  $   'E     LE     PE     XE  �   pE  �   XF  �   EG     6H     FH     TH  )   jH     �H  (   �H     �H     �H     �H     �H  	   I  	   $I     .I     II     aI     gI     }I     �I     �I  /   �I  "   �I     J  ,   J     LJ     YJ      nJ     �J     �J     �J     �J     �J  
   �J  	   �J     �J     �J     �J     �J     K  �   $K  M   �K     �K     L     M     (M     @M     VM     kM     �M  �   �M     'N     AN     ^N     vN     �N     �N  v   �N     O     .O     3O     LO     dO     lO  
   tO     O     �O     �O  "   �O     �O  !   �O      �O  -   P     =P  �   ZP  "   ,Q     OQ     lQ  �   �Q     R     R  $   $R  
   IR  )   TR     ~R     �R     �R     �R     �R     �R     �R     �R     �R     �R     �R     S     S  H   +S  (   tS     �S     �S     �S     �S     �S     �S     �S     �S     �S     T  	   T     "T     )T  	   7T  
   AT     LT     XT     rT     yT     T  �   �T     U  7   U     QU     bU     qU     �U  !   �U     �U     �U     �U     �U     V     V     /V     MV     ]V  4   lV  $   �V     �V  "   �V     �V     W     W     W     ?W  
   FW     QW  ,   kW  +   �W     �W     �W     �W     X     X     $X     AX     JX     ZX     kX  (   �X     �X     �X     �X     �X     Y     Y     4Y     KY     [Y     xY     �Y     �Y  !   �Y  '   �Y  *   Z  -   -Z     [Z     zZ     �Z     �Z     �Z     �Z     �Z     �Z     �Z      [  -   [  9   @[  @   z[  r   �[     .\  A   E\  �   �\     =]     W]     d]     �]     �]     �]     �]  <   �]     �]  "   ^     $^     (^     =^  *   W^     �^     �^     �^     �^     �^     
_  !   _  N   1_     �_     �_  
   `     `     3`     G`     ``     y`     �`     �`     �`     �`     �`     	a     !a  `   )a  %   �a  ,   �a     �a     �a     �a  ,   b  	   >b     Hb  /   Xb  	   �b     �b     �b  -   �b  1   �b  "   &c  (   Ic     rc  �   �c  �   Ld  }   e  4   �e     �e     �e     �e     �e     f     f     !f  5   :f     pf     yf     f     �f  -   �f     �f     �f     �f     �f     �f     g     g     'g  ,   /g     \g  �  og  �   	i     �i  [   #j  '   j     �j     �j  +   �j  6   �j  6   "k  1   Yk  T   �k  "   �k     l      l     :l  �   Ul  C   m     Zm     gm     zm     �m     �m     �m     �m     �m     n     n  J   ,n  "   wn     �n      �n  )   �n     o      o  ,   8o  R   eo     �o     �o     �o     �o  	   p  A   p  @   `p     �p  &   �p  5   �p     q     %q     *q  a   Dq     �q     �q     �q     �q     �q     �q     �q     r     (r     Hr     Qr     jr      �r     �r  a   �r  
  s  K   t  T   et  R   �t  &   u      4u  ,   Uu  �   �u  >   v  +   Ov  ,   {v  X   �v  8   w     :w     Vw     uw     �w     �w     �w     �w     �w     �w  	   �w  	   �w     x     x      x     /x     Hx     Qx     bx     ix  )   rx  $   �x     �x  (   �x  (   y  0   +y  '   \y  
   �y  	   �y     �y     �y     �y     �y  #   �y  !   z     3z     <z     Wz     ]z     iz     yz     �z  (   �z  #   �z  5   �z  $   ${  !   I{     k{     {{  	   �{     �{     �{     �{     �{     �{     �{     �{     |     |     .|     N|  .   Z|     �|     �|     �|     �|     �|     �|  '   �|  
   }     }     2}     K}  #   a}     �}  
   �}  -   �}  3   �}  $   ~     4~     Q~     a~  3   p~  #   �~     �~     �~     �~     �~  E   �~  (   6      _  !   �     �     �  <   �  -   �     B�  7   a�  !   ��  !   ��  %   ݀     �  !   �  @   B�  =   ��  >   ��  ;    �     <�     Y�     k�  (   {�     ��     ��          �     �  W   �     ]�     j�  (   v�  >   ��  :   ބ     �     �     .�     5�     G�     ]�  E   r�  -   ��  &   �  '   �     5�     T�  .   `�     ��     ��     ��     ��  )   ҆  @   ��     =�     E�     R�     o�  �   ��     ;�  >   M�     ��  	   ��     ��     ��     ˈ      ވ  "   ��  '   "�     J�     ^�  )   ~�  '   ��  N   Љ  P   �     p�  :   ��     Ċ     Ȋ     ъ  �   �    �  �   �     �     �     �  0   .�  %   _�  3   ��     ��     Ŏ     ю  !   �  
   �     �  /   %�  &   U�     |�     ��     ��     ��      ʏ  4   �  /    �  '   P�  9   x�     ��     ��  <   ڐ     �     *�  1   3�     e�  $   k�     ��     ��     ��     ��     ��     ��  (   ё  �   ��  �   ��  )    �  (  J�     s�     ��     ��     Ô  5   ߔ     �  �   0�     ƕ  ,   ߕ     �     &�     F�     Z�  l   ]�     ʖ     �     �     ��     �     �  
   %�     0�     A�     P�  /   V�     ��      ��  ,   ��  (   ܗ     �  �   #�  $   �  '   2�  '   Z�  �   ��  
   $�     /�  I   4�     ~�  F   ��     ښ     �     ��  
   �     �     '�     C�     H�     Q�     m�  &   ~�     ��     ��  J   ��  =   �     F�  	   M�  *   W�     ��     ��     ��     ��     ��     Ϝ     �  
   ��     	�     �      �     )�  	   2�  "   <�     _�  
   s�     ~�  �   ��     t�  I   ��     ̞     �     ��  #   �  )   >�     h�  )   ��  )   ��  	   ן     �     ��  $   �     5�  8   J�  D   ��  1   Ƞ  @   ��  (   ;�  
   d�     o�     u�  &   ��  	   ��     ��     ͡  8   �  .   �  3   H�      |�     ��     ��     Ǣ  /   آ     �     �     !�  !   5�  :   W�  $   ��     ��     ʣ  1   ݣ  '   �  %   7�  %   ]�     ��  3   ��  !   פ  +   ��     %�  )   @�  I   j�  0   ��  0   �     �     4�     G�     e�     w�     ��  $   ��     ��  	   ٦     �  ,   ��  8   +�  ?   d�  z   ��      �  P   @�  �   ��     /�     N�  2   g�  /   ��     ʩ  	   ٩     �  K   �     O�  &   h�     ��     ��     ��  .   ˪     ��  0   �  %   G�  &   m�  *   ��     ��  7   ǫ  e   ��     e�  �   ��     V�  '   e�     ��  :   ��  8   ݭ  8   �     O�  +   g�  #   ��  %   ��  4   ݮ     �  
   +�  ]   6�  :   ��  =   ϯ     �  8   !�     Z�  ,   n�     ��     ��  0   ��  
   �     ��  (   �  8   ?�  9   x�  )   ��  +   ܱ  %   �  �   .�  �   -�  �   -�  K   ��     ��     �     0�     F�     M�     g�  &   n�  V   ��     �     ��  '    �     (�  )   8�     b�     x�     ��     ��     ��     ��     ��  
   Ӷ  .   ޶     �         �  �        i  -  �   q       �   �          [  �  �     [       )   �   �  7   �   (   �  �      Q   �   �      �  �           �  +   `   n      �  �      �  �     k   �  m   0      l       �  �  �  �   �          �  <   �  4             �   �   �     �  s  �          p          /               �  p  n   �       j  +      �   �     �  a   C   $   H  �   �           �   x      T      �   �          �   �   =   �  b  �          e  �   �  �  �                  i   �          |           }   V           w  �    �  �               �  r  �   �  �   �  W      �   �  w   |  l  �   �  �  1        d       �   #       �  g     �   �               �  �  �  g            �   �      9       �   �   �   �      t         8   0   �                 X      �   �             �       �               �           )  �  ^   �   "    3   �  �  �   �          %  �  A   M  �  ~  �  e   B  ;   �  �   �  �  4               	      �        �  o   *   �    �           �                    _      m  �     �  ~   b       L  �          �  �  �   �      R            T   �   �   �   \      %   �  �   N       �  A  �      �  @      ]   v     (  Z      O  �   h  �   J   O      �  �      �     �   �      �   U   �   =  r   E  �      {  �     &  .     D   {   z   �   �       >  �   �  G   �     �   �   �      �     E   �  ^  _   �          7  H   �  f       �   �         G  c  u      �   �   �   Z       	  	       ,           f   �   �      Q  �   �      ?   �   q  #  �  C            �       $  3  �   �  ]      �   �   �              �   y   �   '     �  �  �   �           
  �              B          x           �   `                 �  "   �  �  �  �  F    F   �  9  �              *  �      @       �    8  ?  �   M   �  \      �        �     s   �  �  <  D      P  �       V  d    �       &       �  S    z  �      �      �       �     �      �   t   W              o  P   �  �   �   ;      }    �  �   �       K  �   J  �   u   �       �   I  �  �         �       :   ,  -           �      L   �              �  5  �       R   �   /  �   �  y  �      �  �          Y  1   �       Y   �      �       �   �  �       �  �   �   k  S   �  �    �   �  �               �       �     K       �  �  �  �   �       !      �   �  �   �       �  �   v      6   >       �      h           
   �  �  �  �  �   5               U                    �   6  2     N  a  .         �   �    �  '       I   2        X  �       �   �                 !      �   :      �       �  j       c    

A good replacement for %s
is placing values for the user.name and
user.email settings into your personal
~/.gitconfig file.
 
* Untracked file clipped here by %s.
* To see the entire file, use an external editor.
 
This is due to a known issue with the
Tcl binary distributed by Cygwin. %s ... %*i of %*i %s (%3i%%) %s Repository %s of %s '%s' is not an acceptable branch name. '%s' is not an acceptable remote name. (Blue denotes repository-local tools) * Binary file (not showing content). * Untracked file is %d bytes.
* Showing only first %d bytes.
 A branch is required for 'Merged Into'. Abort Merge... Abort completed.  Ready. Abort failed. Abort merge?

Aborting the current merge will cause *ALL* uncommitted changes to be lost.

Continue with aborting the current merge? Aborted checkout of '%s' (file level merging is required). Aborting About %s Add Add New Remote Add New Tool Command Add Remote Add Tool Add globally Add... Adding %s Adding resolution for %s Always (Do not perform merge checks) Amend Last Commit Amended Commit Message: Amended Initial Commit Message: Amended Merge Commit Message: Annotation complete. Annotation process is already running. Any unstaged changes will be permanently lost by the revert. Apply/Reverse Hunk Apply/Reverse Line Arbitrary Location: Are you sure you want to run %s? Arguments Ask the user for additional arguments (sets $ARGS) Ask the user to select a revision (sets $REVISION) Author: Blame Copy Only On Changed Files Blame History Context Radius (days) Blame Parent Commit Branch Branch '%s' already exists. Branch '%s' already exists.

It cannot fast-forward to %s.
A merge is required. Branch '%s' does not exist. Branch Name Branch: Branches Browse Browse %s's Files Browse Branch Files Browse Branch Files... Browse Current Branch's Files Busy Calling commit-msg hook... Calling pre-commit hook... Calling prepare-commit-msg hook... Cancel Cannot abort while amending.

You must finish amending this commit.
 Cannot amend while merging.

You are currently in the middle of a merge that has not been fully completed.  You cannot amend the prior commit unless you first abort the current merge activity.
 Cannot determine HEAD.  See console output for details. Cannot fetch branches and objects.  See console output for details. Cannot fetch tags.  See console output for details. Cannot find HEAD commit: Cannot find git in PATH. Cannot find parent commit: Cannot merge while amending.

You must finish amending this commit before starting any type of merge.
 Cannot move to top of working directory: Cannot parse Git version string: Cannot resolve %s as a commit. Cannot resolve deletion or link conflicts using a tool Cannot use bare repository: Cannot write icon: Cannot write shortcut: Case-Sensitive Change Change Font Checked out '%s'. Checkout Checkout After Creation Checkout Branch Checkout... Choose %s Clone Clone Existing Repository Clone Type: Clone failed. Clone... Cloning from %s Close Command: Commit %s appears to be corrupt Commit Message Text Width Commit Message: Commit declined by commit-msg hook. Commit declined by pre-commit hook. Commit declined by prepare-commit-msg hook. Commit failed. Commit: Commit@@noun Commit@@verb Committer: Committing changes... Compress Database Compressing the object database Conflict file does not exist Continue Copied Or Moved Here By: Copy Copy All Copy Commit Copy To Clipboard Copying objects Could not add tool:
%s Could not start ssh-keygen:

%s Could not start the merge tool:

%s Couldn't find git gui in PATH Couldn't find gitk in PATH Counting objects Create Create Branch Create Desktop Icon Create New Branch Create New Repository Create... Created commit %s: %s Creating working directory Current Branch: Cut Database Statistics Decrease Font Size Default Default File Contents Encoding Delete Delete Branch Delete Branch Remotely Delete Branch... Delete Local Branch Delete Only If Delete Only If Merged Into Delete... Deleting branches from %s Destination Repository Detach From Local Branch Diff/Console Font Directory %s already exists. Directory: Disk space used by loose objects Disk space used by packed objects Displaying only %s of %s files. Do Full Copy Detection Do Nothing Do Nothing Else Now Do not know how to initialize repository at location '%s'. Don't show the command output window Done Edit Email Address Encoding Error loading commit data for amend: Error loading diff: Error loading file: Error retrieving versions:
%s Error: Command Failed Explore Working Copy Failed to add remote '%s' of location '%s'. Failed to completely save options: Failed to configure origin Failed to configure simplified git-pull for '%s'. Failed to create repository %s: Failed to delete branches:
%s Failed to open repository %s: Failed to rename '%s'. Failed to set current branch.

This working directory is only partially switched.  We successfully updated your files, but failed to update an internal Git file.

This should not have occurred.  %s will now close and give up. Failed to stage selected hunk. Failed to stage selected line. Failed to unstage selected hunk. Failed to unstage selected line. Failed to update '%s'. Fast Forward Only Fetch Immediately Fetch Tracking Branch Fetch from Fetching %s from %s Fetching new changes from %s Fetching the %s File %s already exists. File %s seems to have unresolved conflicts, still stage? File Browser File Viewer File level merge required. File type changed, not staged File type changed, staged File: Find Text... Find: Font Example Font Family Font Size Force overwrite existing branch (may discard changes) Force resolution to the base version? Force resolution to the other branch? Force resolution to this branch? Found a public key in: %s From Repository Full Copy (Slower, Redundant Backup) Further Action Garbage files Generate Key Generating... Generation failed. Generation succeeded, but no keys found. Git Gui Git Repository Git Repository (subproject) Git directory not found: Git version cannot be determined.

%s claims it is version '%s'.

%s requires at least Git 1.5.0 or later.

Assume '%s' is version 1.5.0?
 Global (All Repositories) Hardlinks are unavailable.  Falling back to copying. Help In File: Include tags Increase Font Size Index Error Initial Commit Message: Initial file checkout failed. Initialize Remote Repository and Push Initializing... Invalid GIT_COMMITTER_IDENT: Invalid date from Git: %s Invalid font specified in %s: Invalid global encoding '%s' Invalid repo encoding '%s' Invalid revision: %s Invalid spell checking configuration KiB LOCAL:
 LOCAL: deleted
REMOTE:
 Last scanned state does not match repository state.

Another Git program has modified this repository since the last scan.  A rescan must be performed before a merge can be performed.

The rescan will be automatically started now.
 Last scanned state does not match repository state.

Another Git program has modified this repository since the last scan.  A rescan must be performed before another commit can be created.

The rescan will be automatically started now.
 Last scanned state does not match repository state.

Another Git program has modified this repository since the last scan.  A rescan must be performed before the current branch can be changed.

The rescan will be automatically started now.
 Linking objects Loading %s... Loading annotation... Loading copy/move tracking annotations... Loading diff of %s... Loading original location annotations... Local Branch Local Branches Local Merge... Location %s already exists. Location: Main Font Match Tracking Branch Name Match Tracking Branches Merge Merge Commit Message: Merge Into %s Merge Verbosity Merge completed successfully. Merge failed.  Conflict resolution is required. Merge strategy '%s' not supported. Merge tool failed. Merge tool is already running, terminate it? Merged Into: Merging %s and %s... Minimum Letters To Blame Copy On Mirroring to %s Missing Modified, not staged Name: New Branch Name Template New Commit New Name: New... Next No No Suggestions No changes to commit. No changes to commit.

No files were modified by this commit and it was not a merge commit.

A rescan will be automatically started now.
 No changes to commit.

You must stage at least 1 file before you can commit.
 No default branch obtained. No differences detected.

%s has no changes.

The modification date of this file was updated by another application, but the content within the file was not changed.

A rescan will be automatically started to find other files which may have the same state. No keys found. No repository selected. No revision selected. No working directory Not a GUI merge tool: '%s' Not a Git repository: %s Note that the diff shows only conflicting changes.

%s will be overwritten.

This operation can be undone only by restarting the merge. Nothing to clone from %s. Number of Diff Context Lines Number of loose objects Number of packed objects Number of packs OK One or more of the merge tests failed because you have not fetched the necessary commits.  Try fetching from %s first. Online Documentation Open Open Existing Repository Open Recent Repository: Open... Options Options... Original File: Originally By: Other Packed objects waiting for pruning Paste Please select a branch to rename. Please select a tracking branch. Please select one or more branches to delete. Please supply a branch name. Please supply a commit message.

A good commit message has the following format:

- First line: Describe in one sentence what you did.
- Second line: Blank
- Remaining lines: Describe why this change is good.
 Please supply a name for the tool. Please supply a remote name. Portions staged for commit Possible environment issues exist.

The following environment variables are probably
going to be ignored by any Git subprocess run
by %s:

 Preferences Prev Prune Tracking Branches During Fetch Prune from Pruning tracking branches deleted from %s Push Push Branches Push to Push... Pushing %s %s to %s Pushing changes to %s Quit REMOTE:
 REMOTE: deleted
LOCAL:
 Reading %s... Ready to commit. Ready. Recent Repositories Recovering deleted branches is difficult.

Delete the selected branches? Recovering lost commits may not be easy. Redo Refresh Refreshing file status... Remote Remote Details Remote: Remove Remove Remote Remove Tool Remove Tool Commands Remove... Rename Rename Branch Rename... Repository Repository: Requires merge resolution Rescan Reset Reset '%s'? Reset changes?

Resetting the changes will cause *ALL* uncommitted changes to be lost.

Continue with resetting the current changes? Reset... Resetting '%s' to '%s' will lose the following commits: Restore Defaults Revert Changes Revert To Base Revert changes in file %s? Revert changes in these %i files? Reverting %s Reverting dictionary to %s. Reverting selected files Revision Revision Expression: Revision To Merge Revision expression is empty. Run Command: %s Run Merge Tool Run only if a diff is selected ($FILENAME not empty) Running %s requires a selected file. Running merge tool... Running thorough copy detection... Running: %s Save Scanning %s... Scanning for modified files ... Select Select All Setting up the %s (at %s) Shared (Fastest, Not Recommended, No Backup) Shared only available for local repository. Show Diffstat After Merge Show History Context Show Less Context Show More Context Show SSH Key Show a dialog before running Sign Off Source Branches Source Location: Spell Checker Failed Spell checker silently failed on startup Spell checking is unavailable Spelling Dictionary: Stage Changed Stage Changed Files To Commit Stage Hunk For Commit Stage Line For Commit Stage Lines For Commit Stage To Commit Staged Changes (Will Commit) Staged for commit Staged for commit, missing Staged for removal Staged for removal, still present Staging area (index) is already locked. Standard (Fast, Semi-Redundant, Hardlinks) Standard only available for local repository. Start git gui In The Submodule Starting Revision Starting gitk... please wait... Starting... Staying on branch '%s'. Success Summarize Merge Commits System (%s) Tag Target Directory: The 'master' branch has not been initialized. The following branches are not completely merged into %s: The following branches are not completely merged into %s:

 - %s There is nothing to amend.

You are about to create the initial commit.  There is no commit before this to amend.
 This Detached Checkout This is example text.
If you like this text, it can be your font. This repository currently has approximately %i loose objects.

To maintain optimal performance it is strongly recommended that you compress the database.

Compress the database now? Tool '%s' already exists. Tool Details Tool completed successfully: %s Tool failed: %s Tool: %s Tools Tracking Branch Tracking branch %s is not a branch in the remote repository. Transfer Options Trust File Modification Timestamps URL Unable to cleanup %s Unable to copy object: %s Unable to copy objects/info/alternates: %s Unable to display %s Unable to display parent Unable to hardlink object: %s Unable to obtain your identity: Unable to unlock the index. Undo Unexpected EOF from spell checker Unknown file state %s detected.

File %s cannot be committed by this program.
 Unlock Index Unmerged files cannot be committed.

File %s has merge conflicts.  You must resolve them and stage the file before committing.
 Unmodified Unrecognized spell checker Unstage From Commit Unstage Hunk From Commit Unstage Line From Commit Unstage Lines From Commit Unstaged Changes Unstaging %s from commit Unsupported merge tool '%s' Unsupported spell checker Untracked, not staged Update Existing Branch: Updated Updating the Git index failed.  A rescan will be automatically started to resynchronize git-gui. Updating working directory to '%s'... Use '/' separators to create a submenu tree: Use Local Version Use Merge Tool Use Remote Version Use thin pack (for slow network connections) User Name Verify Database Verifying the object database with fsck-objects Visualize Visualize %s's History Visualize All Branch History Visualize All Branch History In The Submodule Visualize Current Branch History In The Submodule Visualize Current Branch's History Visualize These Changes In The Submodule Working... please wait... You are in the middle of a change.

File %s is modified.

You should complete the current commit before starting a merge.  Doing so will help you abort a failed merge, should the need arise.
 You are in the middle of a conflicted merge.

File %s has merge conflicts.

You must resolve them, stage the file, and commit to complete the current merge.  Only then can you begin another merge.
 You are no longer on a local branch.

If you wanted to be on a branch, create one now starting from 'This Detached Checkout'. You must correct the above errors before committing. Your OpenSSH Public Key Your key is in: %s [Up To Parent] buckets commit-tree failed: error fatal: Cannot resolve %s fatal: cannot stat path %s: No such file or directory fetch %s files files checked out files reset git-gui - a graphical user interface for Git. git-gui: fatal error lines annotated objects pt. push %s remote prune %s update-ref failed: warning warning: Tcl does not support encoding '%s'. write-tree failed: Project-Id-Version: git-gui
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-01-26 15:47-0800
PO-Revision-Date: 2012-12-25 08:58+0000
Last-Translator: Michele Ballabio <barra_cuda@katamail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:28+0000
X-Generator: Launchpad (build 18115)
 

Una buona alternativa a %s
consiste nell'assegnare valori alle variabili di configurazione
user.name e user.email nel tuo file ~/.gitconfig personale.
 
* %s non visualizza completamente questo file non tracciato.
* Per visualizzare il file completo, usare un programma esterno.
 
Ciò è dovuto a un problema conosciuto
causato dall'eseguibile Tcl distribuito da Cygwin. %1$s ... %6$s: %2$*i di %4$*i (%7$3i%%) Archivio di %s %s di %s '%s' non è utilizzabile come nome di ramo. '%s' non è utilizzabile come nome di archivio remoto. (Il colore blu indica accessori per l'archivio locale) * File binario (il contenuto non sarà mostrato). * Il file non tracciato è di %d byte.
* Saranno visualizzati solo i primi %d byte.
 Si richiede un ramo per 'Fuso in'. Interrompi fusione... Interruzione completata. Pronto. Interruzione non riuscita. Interrompere fusione?

L'interruzione della fusione attuale causerà la perdita di *TUTTE* le modifiche non ancora presenti nell'archivio.

Continuare con l'interruzione della fusione attuale? Attivazione di '%s' fallita (richiesta una fusione a livello file). Interruzione Informazioni su %s Aggiungi Aggiungi nuovo archivio remoto Aggiungi un nuovo comando Aggiungi archivio remoto Aggiungi accessorio Aggiungi per tutti gli archivi Aggiungi... Aggiunta di %s in corso La risoluzione dei conflitti per %s è preparata per la prossima revisione Sempre (non verificare le fusioni) Correggi l'ultima revisione Messaggio di revisione corretto: Messaggio iniziale di revisione corretto: Messaggio di fusione corretto: Annotazione completata. Il processo di annotazione è già in corso. Tutte le modifiche non preparate per una nuova revisione saranno perse per sempre. Applica/Inverti sezione Applica/Inverti riga Posizione specifica: Vuoi davvero eseguire %s? Argomenti Chiedi all'utente di fornire argomenti aggiuntivi (imposta $ARGS) Chiedi all'utente di scegliere una revisione (imposta $REVISION) Autore: Ricerca copie solo nei file modificati Giorni di contesto nella cronologia delle annotazioni Annota la revisione precedente Ramo Il ramo '%s' esiste già. Il ramo '%s' esiste già.

Non può effettuare un 'fast-forward' a %s.
E' necessaria una fusione. Il ramo '%s' non esiste. Nome del ramo Ramo: Rami Esplora Esplora i file di %s Esplora i file del ramo Esplora i file del ramo... Esplora i file del ramo attuale Occupato Avvio commit-msg hook... Avvio pre-commit hook... Avvio prepare-commit-msg hook... Annulla Interruzione impossibile durante una correzione.

Bisogna finire di correggere questa revisione.
 Non è possibile effettuare una correzione durante una fusione.

In questo momento si sta effettuando una fusione che non è stata del tutto completata. Non puoi correggere la revisione precedente a meno che prima tu non interrompa l'operazione di fusione in corso.
 Impossibile determinare HEAD. Controllare i dettagli forniti dalla console. Impossibile recuperare rami e oggetti. Controllare i dettagli forniti dalla console. Impossibile recuperare le etichette. Controllare i dettagli forniti dalla console. Impossibile trovare la revisione HEAD: Impossibile trovare git nel PATH Impossibile trovare la revisione precedente: Non posso effettuare fusioni durante una correzione.

Bisogna finire di correggere questa revisione prima di iniziare una qualunque fusione.
 Impossibile spostarsi sulla directory principale del progetto: Impossibile determinare la versione di Git: Impossibile risolvere %s come una revisione. Non è possibile risolvere i conflitti per cancellazioni o link con un programma esterno Impossibile usare un archivio senza directory di lavoro: Impossibile scrivere icona: Impossibile scrivere shortcut: Distingui maiuscole Cambia Cambia caratteri Attivazione di '%s' completata. Attiva Attiva dopo la creazione Attiva ramo Attiva... Scegli %s Clona Clona archivio esistente Tipo di clone: Clonazione non riuscita. Clona... Clonazione da %s Chiudi Comando: La revisione %s sembra essere danneggiata Larghezza del messaggio di revisione Messaggio di revisione: Revisione rifiutata dal commit-msg hook. Revisione rifiutata dal pre-commit hook. Revisione rifiutata dal prepare-commit-msg hook. Impossibile creare una nuova revisione. Revisione: Revisione Nuova revisione Revisione creata da: Archiviazione modifiche... Comprimi l'archivio Compressione dell'archivio in corso Non esiste un file con conflitti. Continua Copiato o spostato qui da: Copia Copia tutto Copia revisione Copia negli appunti Copia degli oggetti Impossibile aggiungere l'accessorio:

%s Impossibile avviare ssh-keygen:

%s Impossibile avviare la risoluzione dei conflitti:

%s Impossibile trovare git gui nel PATH Impossibile trovare gitk nel PATH Calcolo oggetti Crea Crea ramo Crea icona desktop Crea nuovo ramo Crea nuovo archivio Crea... Creata revisione %s: %s Creazione directory di lavoro Ramo attuale: Taglia Statistiche dell'archivio Diminuisci dimensione caratteri Predefinito Codifica predefinita per il contenuto dei file Elimina Elimina ramo Elimina ramo remoto Elimina ramo... Elimina ramo locale Elimina solo se Cancella solo se fuso con un altro ramo Elimina... Cancellazione rami da %s Archivio di destinazione Stacca da ramo locale Caratteri per confronti e terminale La directory %s esiste già. Directory: Spazio su disco utilizzato da oggetti slegati Spazio su disco utilizzato da oggetti impacchettati Saranno mostrati solo %s file su %s. Ricerca accurata delle copie Non fare niente Non fare altro Impossibile inizializzare l'archivio posto in '%s'. Non mostrare la finestra di comando Fatto Modifica Indirizzo Email Codifica Errore durante il caricamento dei dati della revisione da correggere: Errore nel caricamento delle differenze: Errore nel caricamento del file: Errore: revisione non trovata:
%s Errore: comando non riuscito Esplora copia di lavoro Impossibile aggiungere l'archivio remoto '%s' posto in '%s'. Impossibile salvare completamente le opzioni: Impossibile configurare origin Impossibile configurare git-pull semplificato per '%s'. Impossibile creare l'archivio %s: Impossibile cancellare i rami:
%s Impossibile accedere all'archivio %s: Impossibile rinominare '%s'. Impossibile preparare il ramo attuale.

Questa directory di lavoro è stata convertita solo parzialmente. I file sono stati aggiornati correttamente, ma l'aggiornamento di un file di Git ha prodotto degli errori.

Questo non sarebbe dovuto succedere.  %s ora terminerà senza altre azioni. Impossibile preparare la sezione scelta per una nuova revisione. Impossibile preparare la riga scelta per una nuova revisione. Impossibile rimuovere la sezione scelta dalla nuova revisione. Impossibile rimuovere la riga scelta dalla nuova revisione. Impossibile aggiornare '%s'. Solo fast forward Recupera subito Recupera duplicato locale di ramo remoto Recupera da Recupero %s da %s Recupero nuove modifiche da %s Recupero %s Il file %s esiste già. Il file %s sembra contenere conflitti non risolti, preparare per la prossima revisione? File browser Mostra file E' richiesta una fusione a livello file. Tipo di file modificato, non preparato per una nuova revisione Tipo di file modificato, preparato per una nuova revisione File: Trova testo... Trova: Esempio caratteri Famiglia di caratteri Dimensione caratteri Sovrascrivi ramo esistente (alcune modifiche potrebbero essere perse) Imporre la risoluzione alla revisione comune? Imporre la risoluzione all'altro ramo? Imporre la risoluzione al ramo attuale? Chiave pubblica trovata in: %s Da archivio Copia completa (più lento, backup ridondante) Altra azione File inutili Crea chiave Creazione chiave in corso... Errore durante la creazione della chiave. La chiave è stata creata con successo, ma non è stata trovata. Git Gui Archivio Git Archivio Git (sottoprogetto) Non trovo la directory di git: La versione di Git non può essere determinata.

%s riporta che la versione è '%s'.

%s richiede almeno Git 1.5.0 o superiore.

Assumere che '%s' sia alla versione 1.5.0?
 Tutti gli archivi Impossibile utilizzare gli hardlink. Si ricorrerà alla copia. Aiuto Nel file: Includi etichette Aumenta dimensione caratteri Errore nell'indice Messaggio di revisione iniziale: Attivazione iniziale non riuscita. Inizializza l'archivio remoto e propaga Inizializzazione... GIT_COMMITTER_IDENT non valida: Git ha restituito una data non valida: %s Caratteri non validi specificati in %s: La codifica dei caratteri '%s' specificata per tutti gli archivi non è valida La codifica dei caratteri '%s' specificata per l'archivio attuale  non è valida Revisione non valida: %s La configurazione del correttore ortografico non è valida KiB LOCALE:
 LOCALE: cancellato
REMOTO:
 L'ultimo stato analizzato non corrisponde allo stato dell'archivio.

Un altro programma Git ha modificato questo archivio dall'ultima analisi.Bisogna effettuare una nuova analisi prima di poter effettuare una fusione.

La nuova analisi comincerà ora.
 L'ultimo stato analizzato non corrisponde allo stato dell'archivio.

Un altro programma Git ha modificato questo archivio dall'ultima analisi. Bisogna effettuare una nuova analisi prima di poter creare una nuova revisione.

La nuova analisi comincerà ora.
 L'ultimo stato analizzato non corrisponde allo stato dell'archivio.

Un altro programma Git ha modificato questo archivio dall'ultima analisi. Bisogna effettuare una nuova analisi prima di poter cambiare il ramo attuale.

La nuova analisi comincerà ora.
 Collegamento oggetti Caricamento %s... Caricamento annotazioni... Caricamento annotazioni per copie/spostamenti... Caricamento delle differenze di %s... Caricamento annotazioni per posizione originaria... Ramo locale Rami locali Fusione locale... Il file/directory %s esiste già. Posizione: Caratteri principali Appaia nome del duplicato locale di ramo remoto Appaia duplicati locali di rami remoti Fusione (Merge) Messaggio di fusione: Fusione in %s Prolissità della fusione Fusione completata con successo. Fusione non riuscita. Bisogna risolvere i conflitti. La strategia di fusione '%s' non è supportata. Risoluzione dei conflitti non riuscita. La risoluzione dei conflitti è già avviata, terminarla? Fuso in: Fusione di %s e %s in corso... Numero minimo di lettere che attivano la ricerca delle copie Mirroring verso %s Mancante Modificato, non preparato per una nuova revisione Nome: Modello per il nome di un nuovo ramo Nuova revisione Nuovo Nome: Nuovo... Succ No Nessun suggerimento Nessuna modifica per la nuova revisione. Nessuna modifica per la nuova revisione.

Questa revisione non modifica alcun file e non effettua alcuna fusione.

Si procederà subito ad una nuova analisi.
 Nessuna modifica per la nuova revisione.

Devi preparare per una nuova revisione almeno 1 file prima di effettuare questa operazione.
 Non è stato trovato un ramo predefinito. Non sono state trovate differenze.

%s non ha modifiche.

La data di modifica di questo file è stata cambiata da un'altra applicazione, ma il contenuto del file è rimasto invariato.

Si procederà automaticamente ad una nuova analisi per trovare altri file che potrebbero avere lo stesso stato. Chiavi non trovate. Nessun archivio selezionato. Nessuna revisione selezionata. Nessuna directory di lavoro '%s' non è una GUI per la risoluzione dei conflitti. %s non è un archivio Git. Si stanno mostrando solo le modifiche con conflitti.

%s sarà sovrascritto.

Questa operazione può essere modificata solo ricominciando la fusione. Niente da clonare da %s. Numero di linee di contesto nelle differenze Numero di oggetti slegati Numero di oggetti impacchettati Numero di pacchetti OK Impossibile verificare una o più fusioni: mancano le revisioni necessarie. Prova prima a recuperarle da %s. Documentazione sul web Apri Apri archivio esistente Apri archivio recente: Apri... Opzioni Opzioni... File originario: In origine da: Altro Oggetti impacchettati che attendono la potatura Incolla Scegliere un ramo da rinominare. Scegliere un duplicato locale di ramo remoto Scegliere uno o più rami da cancellare. Inserire un nome per il ramo. Bisogna fornire un messaggio di revisione.

Un buon messaggio di revisione ha il seguente formato:

- Prima linea: descrivi in una frase ciò che hai fatto.
- Seconda linea: vuota.
- Terza linea: spiega a cosa serve la tua modifica.
 Bisogna dare un nome all'accessorio. Inserire un nome per l'archivio remoto. Parti preparate per una nuova revisione Possibili problemi con le variabili d'ambiente.

Le seguenti variabili d'ambiente saranno probabilmente
ignorate da tutti i sottoprocessi di Git avviati
da %s:

 Preferenze Prec Effettua potatura dei duplicati locali di rami remoti durante il recupero Effettua potatura da Effettua potatura dei duplicati locali di rami remoti cancellati da %s Propaga (Push) Propaga rami Propaga verso Propaga... Propagazione %s %s a %s Propagazione modifiche a %s Esci REMOTO:
 REMOTO: cancellato
LOCALE:
 Lettura di %s... Pronto per creare una nuova revisione. Pronto. Archivi recenti Ripristinare rami cancellati è difficile.

Cancellare i rami selezionati? Ricomporre le revisioni perdute potrebbe non essere semplice. Ripeti Rinfresca Controllo dello stato dei file in corso... Remoto Dettagli sull'archivio remoto Remoto: Rimuovi Rimuovi archivio remoto Rimuovi accessorio Rimuovi i comandi accessori Rimuovi... Rinomina Rinomina ramo Rinomina Archivio Archivio: Richiede risoluzione dei conflitti Analizza nuovamente Ripristina Ripristinare '%s'? Ripristinare la revisione attuale e annullare le modifiche?

L'annullamento delle modifiche causerà la perdita di *TUTTE* le modifiche non ancora presenti nell'archivio.

Continuare con l'annullamento delle modifiche attuali? Ripristina... Ripristinare '%s' a '%s' comporterà la perdita delle seguenti revisioni: Ripristina valori predefiniti Annulla modifiche Ritorna alla revisione comune Annullare le modifiche nel file %s? Annullare le modifiche in questi %i file? Annullo le modifiche in %s Il dizionario è stato reimpostato su %s. Annullo le modifiche nei file selezionati Revisione Espressione di revisione: Revisione da fondere L'espressione di revisione è vuota. Avvia il comando: %s Avvia programma esterno per la risoluzione dei conflitti Avvia solo se è selezionata una differenza ($FILENAME non è vuoto) Bisogna selezionare un file prima di eseguire %s. Avvio del programma per la risoluzione dei conflitti in corso... Ricerca accurata delle copie in corso... Eseguo: %s Salva Analisi in corso %s... Ricerca di file modificati in corso... Seleziona Seleziona tutto Imposto %s (in %s) Shared (il più veloce, non raccomandato, nessun backup) Shared è disponibile solo per archivi locali. Mostra statistiche delle differenze dopo la fusione Mostra contesto nella cronologia Mostra meno contesto Mostra più contesto Mostra chave SSH Mostra una finestra di dialogo prima dell'avvio Sign Off Rami di origine Posizione sorgente: Errore nel correttore ortografico Il correttore ortografico ha riportato un errore all'avvio Correzione ortografica indisponibile Lingua dizionario: Prepara modificati Prepara i file modificati per una nuova revisione Prepara sezione per una nuova revisione Prepara linea per una nuova revisione Prepara linee per una nuova revisione Prepara per una nuova revisione Modifiche preparate (saranno nella nuova revisione) Preparato per una nuova revisione Preparato per una nuova revisione, mancante Preparato per la rimozione Preparato alla rimozione, ancora presente L'area di preparazione per una nuova revisione (indice) è già bloccata. Standard (veloce, semi-ridondante, con hardlink) Standard è disponibile solo per archivi locali. Avvia git gui nel sottomodulo Revisione iniziale Avvio di gitk... attendere... Avvio in corso... Si rimarrà sul ramo '%s'. Successo Riepilogo nelle revisioni di fusione Codifica di sistema (%s) Etichetta Directory di destinazione: Il ramo 'master' non è stato inizializzato. I rami seguenti non sono stati fusi completamente in %s: I rami seguenti non sono stati fusi completamente in %s:

 - %s Non c'è niente da correggere.

Stai per creare la revisione iniziale. Non esiste una revisione precedente da correggere.
 Questa revisione attiva staccata Questo è un testo d'esempio.
Se ti piace questo testo, scegli questo carattere. Questo archivio attualmente ha circa %i oggetti slegati.

Per mantenere buone prestazioni si raccomanda di comprimere l'archivio.

Comprimere l'archivio ora? L'accessorio '%s' esiste già. Dettagli sull'accessorio Il programma esterno è terminato con successo: %s Il programma esterno ha riportato un errore: %s Accessorio: %s Accessori Duplicato locale di ramo remoto Il duplicato locale del ramo remoto %s non è un ramo nell'archivio remoto. Opzioni di trasferimento Fidati delle date di modifica dei file URL Impossibile ripulire %s Impossibile copiare oggetto: %s Impossibile copiare oggetti/info/alternate: %s Impossibile visualizzare %s Impossibile visualizzare la revisione precedente Hardlink impossibile sull'oggetto: %s Impossibile ottenere la tua identità: Impossibile sbloccare l'accesso all'indice Annulla Il correttore ortografico ha mandato un EOF inaspettato Stato di file %s sconosciuto.

Questo programma non può creare una revisione contenente il file %s.
 Sblocca l'accesso all'indice Non è possibile creare una revisione con file non sottoposti a fusione.

Il file %s presenta dei conflitti. Devi risolverli e preparare il file per creare una nuova revisione prima di effettuare questa azione.
 Non modificato Correttore ortografico non riconosciuto Annulla preparazione Annulla preparazione della sezione per una nuova revisione Annulla preparazione della linea per una nuova revisione Annulla preparazione delle linee per una nuova revisione Modifiche non preparate %s non farà parte della prossima revisione Il programma '%s' non è supportato Correttore ortografico non supportato Non tracciato, non preparato per una nuova revisione Aggiorna ramo esistente: Aggiornato Impossibile aggiornare l'indice. Ora sarà avviata una nuova analisi che aggiornerà git-gui. Aggiornamento della directory di lavoro a '%s' in corso... Utilizza il separatore '/' per creare un albero di sottomenu: Usa versione locale Programma da utilizzare per la risoluzione dei conflitti Usa versione remota Utilizza 'thin pack' (per connessioni lente) Nome utente Verifica l'archivio Verifica dell'archivio con fsck-objects in corso Visualizza Visualizza la cronologia di %s Visualizza la cronologia di tutti i rami Visualizza la cronologia di tutti i rami nel sottomodulo Visualizza la cronologia del ramo attuale nel sottomodulo Visualizza la cronologia del ramo attuale Visualizza queste modifiche nel sottomodulo Elaborazione in corso... attendere... Sei nel mezzo di una modifica.

Il file %s è stato modificato.

Bisogna completare la creazione della revisione attuale prima di iniziare una fusione. In questo modo sarà più facile interrompere una fusione non riuscita, nel caso ce ne fosse bisogno.
 Sei nel mezzo di una fusione con conflitti.

Il file %s ha dei conflitti.

Bisogna risolvere i conflitti, preparare il file per una nuova revisione ed infine crearla per completare la fusione attuale. Solo a questo punto potrai iniziare un'altra fusione.
 Non si è più su un ramo locale

Se si vuole rimanere su un ramo, crearne uno ora a partire da 'Questa revisione attiva staccata'. Bisogna correggere gli errori suddetti prima di creare una nuova revisione. La tua chiave pubblica OpenSSH La chiave è in: %s [Directory superiore] bucket commit-tree non riuscito: errore errore grave: impossibile risolvere %s errore grave: impossibile effettuare lo stat del path %s: file o directory non trovata recupera da %s file file presenti nella directory di lavoro ripristino file git-gui - un'interfaccia grafica per Git. git-gui: errore grave linee annotate oggetti pt. propaga verso %s potatura remota di %s update-ref non riuscito: attenzione attenzione: Tcl non supporta la codifica '%s'. write-tree non riuscito: 