��    F      L  a   |         |        ~     �  #   �     �     �     �     �     	       {   2     �     �     �     �          (     C     T     n     }     �     �     �     �     �     �     �  R   	  	   Z	     d	     t	  )   z	     �	  r   �	  8   '
  "   `
  #   �
  &   �
     �
  )   �
  !        :     L     Q  (   i  )   �     �     �     �          (     :     N     ]  !   l  %   �     �     �  E   �  E   &  :   l     �     �  *   �  (        +     3  A   O  �  �  �   _     �       )   	     3     ?  ,   T     �     �     �  �   �  ,   P  $   }     �  '   �  "   �  "        &  !   ?     a     w  "   �     �     �  "   �       ,   #     P  c   h  	   �     �     �  :   �     (  �   ?  O   �  )   !  /   K  0   {  &   �  1   �  "        (     :  !   G     i  +   �  #   �  5   �  ,        <     V     m     �     �  )   �  $   �     �       M   +  L   y  H   �  "        2  ;   H  .   �     �  )   �  [   �     =   A   0   C   #   '       ,   -      :       !   	                  +            3      F         B   D   )      "   @       /          >       6   8   <      ;      4       &              9                          ?                $              
                2             *   .   %      E         (           5          7   1                       
This is free software; see the GNU General Public License version 2 or
later for copying conditions. There is NO warranty.
 %s does not exist %s file %s is not a supported variable name %s(l%s): %s %s(l%s): %s
LINE: %s (Use -d flag to override.) <standard input> <standard output> Debian %s version %s.
 This is free software; see the GNU General Public License version 2 or
later for copying conditions. There is NO warranty.
 badly formatted heading line badly formatted trailer line block device can't parse dependency %s cannot combine %s and %s cannot create directory %s cannot find '%s' cannot make %s executable cannot open %s cannot open file %s cannot opendir %s cannot read %s cannot remove %s cannot stat %s cannot write %s changelog parser %s character device current host architecture '%s' does not appear in package's architecture list (%s) directory dpkg-genchanges error failed to rename newly-extracted %s to %s failed to write fakeroot not found, either install the fakeroot
package, specify a command with the -r option, or run this as root filename, section and priority may contain no whitespace found blank line where expected %s found change data where expected %s found start of entry where expected %s found trailer where expected %s full upload (original source is included) gain-root-commmand '%s' not found host architecture info may not be empty string more than one file specified (%s and %s) must start with an alphanumeric character output format %s not supported output of changelog parser parsed version of changelog repeated key-value %s source changed by source distribution source package source version syntax error in %s at line %d: %s two commands specified: --%s and --%s unable to determine %s unable to execute %s unknown Debian architecture %s, you must specify GNU system type, too unknown GNU system type %s, you must specify Debian architecture, too unknown default GNU system type for Debian architecture %s unknown option or argument %s unrecognized line using a gain-root-command while being root version number does not start with digit warning write error on control data you can't combine 'count' or 'offset' with any other range option Project-Id-Version: dpkg
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-25 13:28+0100
PO-Revision-Date: 2007-11-10 17:44+0000
Last-Translator: Jecko Development <quintium@hotmail.it>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 16:13+0000
X-Generator: Launchpad (build 18115)
 
Questo è software libero; consultare la GNU General Public Licence versione 2
o successiva per le condizioni di copia. NON c'è alcuna garanzia.
 %s non esiste File %s %s non è un nome di variabile supportato %s(l%s): %s %s(l%s): %s
RIGA: %s (Utilizzare l'opzione -d per sovrascrivere). <input standard> <output standard> Versione  %s Debian %s.
 Questo è software libero; consultare la GNU General Public Licence versione 2
o successiva per le condizioni di copia. NON c'è alcuna garanzia.
 Riga di intestazione formattata erroneamente Riga trailer formattata erroneamente Dispositivo a blocchi impossibile analizzare la dipendenza %s Non è possibile combinare %s e %s Impossibile creare la directory %s Impossibile trovare '%s' Impossibile rendere eseguibile %s Impossibile aprire %s Impossibile aprire il file %s Impossibile aprire la directory %s Impossibile leggere %s Impossibile rimuovere %s Impossibile eseguire lo stat di %s Impossibile scrivere %s Analizzatore del registro delle modifiche %s Dispositivo a caratteri L'architettura dell'host corrente "%s" non appare nell'elenco delle architetture del pacchetto (%s) Directory dpkg-genchanges Errore Non è stato possibile rinominare %s appena estratto in %s Scrittura non riuscita Fakeroot non trovato, installare il pacchetto fakeroot,
specificare un comando con l'opzione -r, oppure eseguire con privilegi di amministrazione il nome del file, la sezione e la priorità non possono contenere spazi bianchi Trovata riga vuota quando era previsto %s Trovati dati modificati  quando era previsto %s Trovato inizio della voce quando era previsto %s Trovato trailer quando era previsto %s Caricamento completo (sorgente originale incluso) Gain-root-command '%s' non trovato Architettura host Informazioni non può essere una stringa vuota Specificati più file (%s e %s) deve iniziare con un carattere alfanumerico Formato di output %s non supportato Output dell'analizzatore del registro delle modifiche analizzata versione del registro cambiamenti Valore chiave %s ripetuto Sorgente modificata da Distribuzione sorgente Pacchetto sorgente Versione sorgente errore di sintassi in %s alla riga %d: %s Specificati due comandi: --%s e --%s Impossibile determinare %s Impossibile eseguire %s Architettura Debian %s sconosciuta, specificare anche il tipo di sistema GNU. Tipo di sistema GNU %s sconosciuto. specificare anche l'architettura Debian. Tipo di sistema GNU predefinito sconosciuto per l'architettura Debian %s Opzione o argomento %s sconosciuto Riga non riconosciuta Usare un gain-root-command con privilegi di amministrazione Il numero di versione non inizia con una cifra Avviso Errore di scrittura sui dati di controllo Non è possibile combinare "count" o "offset" con una qualsiasi altra opzione di intervallo 