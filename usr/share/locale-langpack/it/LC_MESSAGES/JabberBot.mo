��          �   %   �      p  <   q  9   �  @   �  C   )     m  N   s     �  Z   �  ?   .  X   n  #   �     �  )   �  .   !  t   P  E   �  ?     4   K     �     �     �  $   �  #   �  $   �  [     *   y  /   �  �  �  :   }	  K   �	  @   
  _   E
     �
  ^   �
       f     4   ~  Y   �  !        /  7   ;  0   s  �   �  L   '  6   t  ?   �     �     �       2     .   M  /   |  g   �  (     3   =                                                           
                                                     	           %(command)s - %(description)s

Usage: %(command)s %(params)s A serious error occured while processing your request:
%s An internal error has occured, please contact the administrator. Credentials check failed, you may be unable to see all information. Error Following detailed information on page "%(pagename)s" is available::

%(data)s Full-text search Hello there! I'm a MoinMoin Notification Bot. Available commands:

%(internal)s
%(xmlrpc)s Here's the page "%(pagename)s" that you've requested:

%(data)s Last author: %(author)s
Last modification: %(modification)s
Current version: %(version)s Please specify the search criteria. Search text Submit this form to perform a wiki search That's the list of pages accesible to you:

%s The "help" command prints a short, helpful message about a given topic or function.

Usage: help [topic_or_function] The "ping" command returns a "pong" message as soon as it's received. This command may take a while to complete, please be patient... This command requires a client supporting Data Forms Title search Unknown command "%s"  Wiki search You are not allowed to use this bot! You must set a (long) secret string You must set a (long) secret string! You've specified a wrong parameter list. The call should look like:

%(command)s %(params)s Your request has failed. The reason is:
%s searchform - perform a wiki search using a form Project-Id-Version: moin
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-07-17 03:14+0200
PO-Revision-Date: 2012-09-16 14:35+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:11+0000
X-Generator: Launchpad (build 18115)
 %(command)s - %(description)s

Uso: %(command)s %(params)s Si è verificato un errore grave durante l'elaborazione della richiesta:
%s Si è verificato un errore interno, contattare l'amministratore. Controllo delle credenziali non riuscito, potrebbe non essere possibile vedere le informazioni. Errore Sono disponibili le seguendi informazioni dettagliate sulla pagina «%(pagename)s»:

%(data)s Cerca testo Salve. Questo è il bot di notifiche di MoinMoin. I comandi disponibili sono:

%(internal)s
%(xmlrpc)s Ecco la pagina «%(pagename)s» richiesta:

%(data)s Ultimo autore: %(author)s
Ultima modifica: %(modification)s
Versione attuale: %(version)s Specificare i criteri di ricerca. Cerca testo Inviare questo formulario per eseguire una ricerca wiki Questo è l'elenco delle pagine accessibili:

%s Il comando «help» stampa un breve e utile messaggio riguardo un dato argomento o una funzione.

Uso: help [argomento_o_funzione] Il comando «ping» restituisce un messaggio «pong» appena viene ricevuto. Questo comando potrebbe richiedere tempo, attendere... Questo comando richiede un client con supporto per i Data Forms Cerca titolo Comando «%s» sconosciuto  Cerca Autorizzazione non disponibile a usare questo bot. È necessario impostare un segreto molto lungo È necessario impostare un segreto molto lungo. I parametri specificati non sono corretti. Il comando dovrebbe essere del tipo:

%(command)s %(params)s Richiesta non riuscita. Il motivo è:
%s searchform - esegue una ricerca wiki usando un form 