��    �     �  K  \      �$     �$     �$  	   �$     �$  #   �$  "   �$  �  !%     
+  �   $+  	   ",     ,,     =,     F,  
   N,     Y,  
   a,     l,     t,     �,  !   �,  <   �,     -  '   -      8-     Y-     x-  "   �-  "   �-     �-     �-     .     .     5.  G   K.     �.  /   �.  6   �.     /  ,   )/     V/  "   p/     �/  /   �/     �/      �/     0  *   70  /   b0  /   �0     �0     �0      �0  %   1     =1  #   X1  *   |1     �1     �1     �1  #   �1     2  ,   32  ,   `2  ,   �2  '   �2  -   �2      3  (   13  (   Z3  !   �3      �3  $   �3  ,   �3  .   4     G4     g4     �4  <   �4  (   �4  #   5  ;   )5  9   e5     �5  
   �5  -   �5  3   �5  1   6  0   @6     q6     �6     �6      �6  *   �6     �6  *   �6     %7     37  >   L7     �7  <   �7  +   �7  �   8    �8  j  �9      ;  j   ;     �;     �;     �;     �;     �;     �;  	   <  &   <  /   A<     q<     �<     �<     �<     �<      �<     =     !=     ==  ,   Y=  *   �=  �   �=     X>     r>  /   �>     �>     �>  )   �>  R   �>  =  J?     �@  @   �@  ,   �@     
A     A     *A  
   BA     MA     ]A  b  pA     �B     �B     C     &C  `   <C     �C     �C     �C  	   �C     �C     �C     D  4   #D  8   XD  2   �D  T  �D  @   G     ZG     yG  ;   �G  o   �G  �   6H  �   I     �I     J      J  ,   ;J     hJ     �J  !   �J     �J  #   �J  6   �J     .K     <K     MK     aK     }K  <   �K     �K     �K  >   �K  !   'L     IL     fL     ~L  
   �L     �L     �L  -   �L  <   �L     0M     KM     ]M     jM     M  "   �M  *   �M  %   �M     N     N     (N     ;N  S   VN  0   �N     �N     �N     O     +O     FO     `O     tO  ,   �O  (   �O  D   �O  0   'P  /   XP  1   �P  0   �P     �P  ;  Q  9   AR  �   {R     tS     �S     �S     �S  ,   �S  6   �S  �  T     �U     �U  '   �U  "   V  %   )V      OV     pV      �V  �  �V     cZ     ~Z     �Z     �Z  �   �Z     O[     f[  "   |[  )   �[     �[  %   �[  $   \     +\     @\     W\  k  l\  \  �]  
   5_     @_     [_     l_     �_  9   �_     �_     �_     `     !`     :`  !   O`     q`     �`     �`     �`     �`     �`     a     'a     Ca    Ya     ab  :   |b  ;   �b  A   �b  3   5c  �   ic  g   =d     �d     �d  '   �d      e  Z   e     he     �e     �e     �e     �e      �e     f  '   3f     [f  !   jf     �f      �f  :   �f  .   g     3g  3   Fg     zg  !   �g     �g     �g     �g     �g  "   �g    h     ;j     Tj     qj     �j  &   �j      �j     �j  �  �j     �l  )   �l     �l     �l     �l  d   m     tm  	   �m     �m     �m     �m     �m  +   �m     n  #   'n     Kn  %   Yn     n     �n     �n     �n     �n     �n    �n     p     p     (p  #   @p  8   dp  -   �p     �p  @   �p  =   !q     _q     q     �q  #   �q     �q     �q     �q     r     -r     @r     Vr     qr     �r     �r     �r     �r     �r     s  
   s     (s     >s     Qs     ]s     rs     �s     �s  1   �s     �s  (   �s     $t     1t     Et     Qt     dt     xt  #   �t     �t     �t     �t      �t     u     "u  $   Au     fu  )   }u  $   �u     �u     �u  ,   �u      %v     Fv     fv     vv     �v     �v     �v  6   �v     w      w     5w     Gw     aw     ww     �w     �w     �w     �w     �w     x     
x     &x     Cx     \x     kx     xx  �  �x  *   0z     [z     tz     �z  ,   �z  )   �z  F  �z     <�    Y�     k�     w�     ��     ��  
   ��     ��  
   ��     ��     ��      ׂ      ��  <   �     V�  (   b�  !   ��     ��     ̃     �     �     $�     ?�     S�     h�     ��  `   ��     ��  6   �  <   Q�     ��  4   ��     ��      ��     �  .   ;�     j�  "   ��  '   ��  +   Ԇ  .    �  5   /�     e�     �  +   ��  %   ʇ     ��  %   �  0   3�  &   d�     ��     ��  .   Ĉ     �  +   �  ,   >�  +   k�  +   ��  /   É  !   �  ,   �  ,   B�  %   o�  !   ��  .   ��  >   �  .   %�  $   T�  $   y�     ��  N   ��  0   
�  +   ;�  A   g�  =   ��     �  	   �  0   �  4   $�  2   Y�  4   ��     ��     ۍ     �      ��  +   �     I�  2   `�     ��     ��  E   ��     �  C   �  >   [�  �   ��    &�  z  3�     ��  �   ̒     T�  #   n�      ��     ��  #   ̓     �     �  /   
�  9   :�      t�      ��     ��     Δ  "   �  -   �     4�      R�     s�  I   ��  C   ؕ  �   �     ٖ     ��  =   �     R�  
   i�  -   t�  ^   ��  �  �     ��  Z   ��  4   ��     +�     9�     O�     l�     z�     ��  �  ��  &   g�  %   ��  %   ��     ڜ  s   ��     h�     ��  "   ��     ��  "     "   �     �  E   �  G   c�  C   ��  g  �  A   W�  '   ��     ��  F   ҡ  �   �  +  ��    �     ��     �  $   )�  .   N�     }�     ��  %   ��     ݥ  #   ��  <   �     Z�     o�     ��  '   ��     Ǧ  I   ݦ     '�     4�  Q   G�  -   ��  )   ǧ     �     �  
   �     *�     F�  6   Y�  E   ��  "   ֨     ��     �     &�     >�  &   U�  0   |�  +   ��     ٩     �     �     �  n   7�  9   ��  )   �     
�  *   )�     T�  '   q�     ��  &   ��  1   ֫  +   �  J   4�  9   �  8   ��  :   �  :   -�     h�  *  ��  <   ��  I  �     2�     C�     W�     o�  <   ��  F   ��  (  �     /�  	   K�  +   U�  &   ��  8   ��  .   �     �  9   0�  �  j�  .   �     5�     R�     k�  �   �       �     A�  !   Y�  -   {�     ��  ,   Ź  +   �     �     2�     M�  �  k�  �  �     ��     ƽ     ޽     ��     �  >   �     ^�     }�     ��     ��     ��     Ӿ     �     �  !   �     @�     \�     |�     ��     ��     ��  *  տ      �  =   �  :   W�  @   ��  3   ��  �   �     ��     j�     ��  .   ��     ��  o   ��     L�  #   h�     ��  $   ��     ��  !   ��  "   	�  (   ,�     U�  $   e�     ��  "   ��  A   ��  +   �     :�  @   O�     ��  *   ��     ��     ��     �     �  &   �  P  ?�     ��     ��     ��     ��  #   ��  (    �     I�  �  [�     �  ;   �  -   X�  %   ��  #   ��  �   ��     Q�  	   a�     k�     z�     ��     ��  &   ��     ��  .   �     0�  -   A�     o�     ��     ��     ��     ��     ��  -  ��     �     /�     I�  <   i�  >   ��  4   ��     �  A   /�  B   q�  "   ��     ��     ��  #   ��  ,   !�     N�     k�  !   ��     ��     ��  '   ��  #   �  '   0�     X�  *   j�  &   ��     ��     ��  
   ��     ��     �  
   #�  '   .�     V�     g�  "   }�  B   ��     ��  ,   ��     %�     1�  
   C�     N�     a�     y�  1   ��     ��     ��     ��  %   �     :�     P�  1   p�      ��  8   ��  2   ��     /�     O�  6   h�  #   ��     ��     ��  %   ��     �     8�     M�  7   Y�     ��     ��     ��  +   ��     ��     �     '�     A�  0   O�  !   ��  !   ��     ��  &   ��  !   ��     �     /�     @�     L�         5   g   �   �   �   �   �   :  �   a            t   M  _  �       I      �   �  �        �       �   *  W   q  C  {   i   �  �  �         �       �       �   {         K     S   }   u           E      Z      �   l   �   -           `  �   ,      Z   �   �  )   !     >  c   T   `       �          ~  �              �       x   _   �   N  =                 �   �   �   %   n       s  �  h  �      �   �   �   Q  �   |       �   u   o     /   	  9   G      �   5      R      [     <  �   #  .      H   P   )  �   2   �   f  $  m  ~               V  �   �      H  x  �       �  �      w   �   �   �   �       �       \   �      w  q          +       �   }  �   �   �     �   /  �  L  6       U   �  |  O      F   �  9          �  Y      �   �  �      r   �      �  �         �      �   �               �  �          D          8   A   �               �   !   �  .   �   �   �   ]  o  �  Y                      B           �  �      ^   �       �      M     �  X   �   �  �  �  (  �   ;   (   l      �  �  7   4   �  $   �  	          �       ]   -  �           �     �   ?                Q   z           v                 K   �  �       �       �  f   j  *       �  �   �   y              L         N   E   �   �   e   s   y       �  e  1           �   �      "  �           V   n    W  <       '       c  �     ?   "       �   �  p   �   �      �   �   �  
  r  �       P  B  �       �   O   �   �      �   �           '  @  �   j   �                  �   %  �   �   6          �   3  �       1      :   v   �       4  �               ,              z  �                k      �   2  �   �   S  �       
       �   k   =       >   �   �  �   +  \            �   �   �   �   �           �       p      U  �         m   7            �  R   b      �   0      �   �   �   �       �  �   d       J   �      &           �       b      3   �     A  �       �            g  �      d  F  T  �      G     �  8            �   �    #   C   J      �   �    �   X      ;          �   �   �  @       �      t  a   0   �           i  h   �   [        &  �      ^  D   �   �                       �       I              	Executing builtin `%s' [%s]
 	Repeat count: %d
 	Running
 	Waiting for command
 	Waiting for job [%d] to terminate
 	Waiting for termination of jobs:  
Mirror specified remote directory to local directory

 -c, --continue         continue a mirror job if possible
 -e, --delete           delete files not present at remote site
     --delete-first     delete old files before transferring new ones
 -s, --allow-suid       set suid/sgid bits according to remote site
     --allow-chown      try to set owner and group on files
     --ignore-time      ignore time when deciding whether to download
 -n, --only-newer       download only newer files (-c won't work)
 -r, --no-recursion     don't go to subdirectories
 -p, --no-perms         don't set file permissions
     --no-umask         don't apply umask to file modes
 -R, --reverse          reverse mirror (put files)
 -L, --dereference      download symbolic links as files
 -N, --newer-than=SPEC  download only files newer than specified time
 -P, --parallel[=N]     download N files in parallel
 -i RX, --include RX    include matching files
 -x RX, --exclude RX    exclude matching files
                        RX is extended regular expression
 -v, --verbose[=N]      verbose operation
     --log=FILE         write lftp commands being executed to FILE
     --script=FILE      write lftp commands to FILE, but don't execute them
     --just-print, --dry-run    same as --script=-

When using -R, the first directory is local and the second is remote.
If the second directory is omitted, basename of first directory is used.
If both directories are omitted, current local and remote directories are used.
  - not supported protocol  -w <file> Write history to file.
 -r <file> Read history from file; appends to current history.
 -c  Clear the history.
 -l  List the history (default).
Optional argument cnt specifies the number of history lines to list,
or "all" to list all entries.
  [cached] !<shell-command> %.0f B/s %.0fb/s %.1f KiB/s %.1fK/s %.2f MiB/s %.2fM/s %d address$|es$ found %ld $#l#byte|bytes$ cached %lld $#ll#byte|bytes$ transferred %lld $#ll#byte|bytes$ transferred in %ld $#l#second|seconds$ %s (filter) %s failed for %d of %d director$y|ies$
 %s failed for %d of %d file$|s$
 %s is a built-in alias for %s
 %s is an alias to `%s'
 %s ok, %d director$y|ies$ created
 %s ok, %d director$y|ies$ removed
 %s ok, %d file$|s$ removed
 %s ok, `%s' created
 %s ok, `%s' removed
 %s%d error$|s$ detected
 %s: %d - no such job
 %s: %s - no such cached session. Use `scache' to look at session list.
 %s: %s - not a number
 %s: %s. Use `set -a' to look at all variables.
 %s: %s: file already exists and xfer:clobber is unset
 %s: %s: no files found
 %s: -m: Number expected as second argument.  %s: -n: Number expected.  %s: -n: positive number expected.  %s: BUG - deadlock detected
 %s: GetPass() failed -- assume anonymous login
 %s: No queue is active.
 %s: Operand missed for `expire'
 %s: Operand missed for size
 %s: Please specify meta-info file or URL.
 %s: ambiguous source directory (`%s' or `%s'?)
 %s: ambiguous target directory (`%s' or `%s'?)
 %s: argument required.  %s: bookmark name required
 %s: cannot create local session
 %s: command `%s' is not compiled in.
 %s: date-time parse error
 %s: date-time specification missed
 %s: import type required (netscape,ncftp)
 %s: invalid block size `%s'
 %s: invalid option -- '%c'
 %s: no current job
 %s: no old directory for this site
 %s: no such bookmark `%s'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: received redirection to `%s'
 %s: regular expression `%s': %s
 %s: some other job waits for job %d
 %s: spaces in bookmark name are not allowed
 %s: summarizing conflicts with --max-depth=%i
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 %s: wait loop detected
 %s: warning: summarizing is the same as using --max-depth=0
 %sModified: %d file$|s$, %d symlink$|s$
 %sNew: %d file$|s$, %d symlink$|s$
 %sRemoved: %d director$y|ies$, %d file$|s$, %d symlink$|s$
 %sTotal: %d director$y|ies$, %d file$|s$, %d symlink$|s$
 ' (commands) **** FXP: giving up, reverting to plain copy
 **** FXP: trying to reverse ftp:fxp-passive-source
 **** FXP: trying to reverse ftp:fxp-passive-sscn
 **** FXP: trying to reverse ftp:ssl-protect-fxp
 , maximum size %ld
 , no size limit =1 =0|>1 Accepted connection from [%s]:%d Accepted data connection from (%s) port %u Access failed:  Account is required, set ftp:acct variable Added job$|s$ Ambiguous command `%s'.
 Ambiguous command `%s'. Use `help' to see available commands.
 Ambiguous command.  Attach the terminal to specified backgrounded lftp process.
 Cannot bind a socket for torrent:port-range Change current local directory to <ldir>. The previous local directory
is stored as `-'. You can do `lcd -' to change the directory back.
 Change current remote directory to <rdir>. The previous remote directory
is stored as `-'. You can do `cd -' to change the directory back.
The previous directory for each site is also stored on disk, so you can
do `open site; cd -' even after lftp restart.
 Change the mode of each FILE to MODE.

 -c, --changes        - like verbose but report only when a change is made
 -f, --quiet          - suppress most error messages
 -v, --verbose        - output a diagnostic for every file processed
 -R, --recursive      - change files and directories recursively

MODE can be an octal number or symbolic mode (see chmod(1))
 Changing remote directory... Close idle connections. By default only with current server.
 -a  close idle connections with all servers
 Closing HTTP connection Closing aborted data socket Closing control socket Closing data socket Closing idle connection Commands queued: Connected Connecting data socket to (%s) port %u Connecting data socket to proxy %s (%s) port %u Connecting to %s%s (%s) port %u Connecting to peer %s port %u Connecting... Connection idle Connection limit reached Could not parse HTTP status line Created a stopped queue.
 DNS resolution not trusted. Data connection established Data connection peer has mismatching address Data connection peer has wrong port number Define or undefine alias <name>. If <value> omitted,
the alias is undefined, else is takes the value <value>.
If no argument is given the current aliases are listed.
 Delaying before reconnect Delaying before retry Delete specified job with <job_no> or all jobs
 Deleted job$|s$ Done Execute commands recorded in file <file>
 Execute site command <site_cmd> and output the result
You can redirect its output
 Expand wildcards and run specified command.
Options can be used to expand wildcards to list of files, directories,
or both types. Type selection is not very reliable and depends on server.
If entry type cannot be determined, it will be included in the list.
 -f  plain files (default)
 -d  directories
 -a  all types
 FEAT negotiation... Failed to change mode of `%s' because no old mode is available.
 Failed to change mode of `%s' to %04o (%s).
 Fatal error Fetching headers... File cannot be accessed File moved File moved to ` File name missed.  Gets selected files with expanded wildcards
 -c  continue, resume transfer
 -d  create directories the same as in file names and get the
     files into them instead of current directory
 -E  delete remote files after successful transfer
 -a  use ascii mode (binary is the default)
 -O <base> specifies base directory or URL where files should be placed
 Getting directory contents Getting file list (%lld) [%s] Getting files information Getting meta-data: %s Group commands together to be executed as one command
You can launch such a group in background
 Handshaking... Hit EOF Host name lookup failure Interrupt Invalid IPv4 numeric address Invalid IPv6 numeric address Invalid command.  Invalid range format. Format is min-max, e.g. 10-20. Invalid time format. Format is <time><unit>, e.g. 2h30m. Invalid time unit letter, only [smhd] are allowed. LFTP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LFTP.  If not, see <http://www.gnu.org/licenses/>.
 LFTP | Version %s | Copyright (c) 1996-%d Alexander V. Lukyanov
 Launch shell or shell command
 Libraries used:  List cached sessions or switch to specified session number
 List remote file names.
By default, nlist output is cached, to see new listing use `renlist' or
`cache flush'.
 List remote files. You can redirect output of this command to file
or via pipe to external command.
By default, ls output is cached, to see new listing use `rels' or
`cache flush'.
See also `help cls'.
 Load module (shared object). The module should contain function
   void module_init(int argc,const char *const *argv)
If name contains a slash, then the module is searched in current
directory, otherwise in directories specified by setting module:path.
 Logging in... Login failed MLSD is disabled by ftp:use-mlsd MLST and MLSD are not supported by this site Making data connection... Making directory `%s' Making symbolic link `%s' to `%s' Mirroring directory `%s' Mode of `%s' changed to %04o (%s).
 Module for command `%s' did not register the command.
 Moved job$|s$ No address found No queued job #%i.
 No queued jobs match "%s".
 No queued jobs.
 No such command `%s'. Use `help' to see available commands.
 Not connected Now executing: Object is not cached and http:cache-control has only-if-cached Old directory `%s' is not removed Old file `%s' is not removed Operation not supported POST method failed Password:  Peer closed connection Persist and retry Print current remote URL.
 -p  show password
 Print help for command <cmd>, or list of available commands
 Proxy protocol unsupported Queue is stopped. Received all Received all (total) Received last chunk Received not enough data, retrying Received valid info about %d IPv6 peer$|s$ Received valid info about %d peer$|s$ Receiving body... Receiving data Receiving data/TLS Remove remote directories
 Remove remote files
 -r  recursive directory removal, be careful
 -f  work quietly
 Removes specified files with wildcard expansion
 Removing old directory `%s' Removing old file `%s' Removing old local file `%s' Rename <file1> to <file2>
 Resolving host address... Retrying mirror...
 Running connect program SITE CHMOD is disabled by ftp:use-site-chmod SITE CHMOD is not supported by this site Same as `cat <files> | more'. if PAGER is set, it is used as filter
 Same as cat, but filter each file through bzcat
 Same as cat, but filter each file through zcat
 Same as more, but filter each file through bzcat
 Same as more, but filter each file through zcat
 Seeding in background...
 Select a server, URL or bookmark
 -e <cmd>            execute the command just after selecting
 -u <user>[,<pass>]  use the user/password for authentication
 -p <port>           use the port for connection
 -s <slot>           assign the connection to this slot
 <site>              host name, URL or bookmark name
 Send bug reports and questions to the mailing list <%s>.
 Send the command uninterpreted. Use with caution - it can lead to
unknown remote state and thus will cause reconnect. You cannot
be sure that any change of remote state because of quoted command
is solid - it can be reset by reconnect at any time.
 Sending commands... Sending data Sending data/TLS Sending request... Server reply matched ftp:retry-530, retrying Server reply matched ftp:retry-530-anonymous, retrying Set variable to given value. If the value is omitted, unset the variable.
Variable name has format ``name/closure'', where closure can specify
exact application of the setting. See lftp(1) for details.
If set is called with no variable then only altered settings are listed.
It can be changed by options:
 -a  list all settings, including default values
 -d  list only default values, not necessary current ones
 Shows lftp version
 Shutting down:  Skipping directory `%s' (only-existing) Skipping file `%s' (only-existing) Skipping symlink `%s' (only-existing) Socket error (%s) - reconnecting Sorry, no help for %s
 Store failed - you have to reput Summarize disk usage.
 -a, --all             write counts for all files, not just directories
     --block-size=SIZ  use SIZ-byte blocks
 -b, --bytes           print size in bytes
 -c, --total           produce a grand total
 -d, --max-depth=N     print the total for a directory (or file, with --all)
                       only if it is N or fewer levels below the command
                       line argument;  --max-depth=0 is the same as
                       --summarize
 -F, --files           print number of files instead of sizes
 -h, --human-readable  print sizes in human readable format (e.g., 1K 234M 2G)
 -H, --si              likewise, but use powers of 1000 not 1024
 -k, --kilobytes       like --block-size=1024
 -m, --megabytes       like --block-size=1048576
 -S, --separate-dirs   do not include size of subdirectories
 -s, --summarize       display only a total for each argument
     --exclude=PAT     exclude files that match PAT
 Switching passive mode off Switching passive mode on Switching to NOREST mode TLS negotiation... There are running jobs and `cmd:move-background' is not set.
Use `exit bg' to force moving to background or `kill all' to terminate jobs.
 Timeout - reconnecting Too many redirections Total %d $file|files$ transferred
 Transfer of %d of %d $file|files$ failed
 Transferring file `%s' Try `%s --help' for more information
 Try `help %s' for more information.
 Turning on sync-mode Unknown command `%s'.
 Unknown system error Upload <lfile> with remote name <rfile>.
 -o <rfile> specifies remote file name (default - basename of lfile)
 -c  continue, reput
     it requires permission to overwrite remote files
 -E  delete local files after successful transfer (dangerous)
 -a  use ascii mode (binary is the default)
 -O <base> specifies base directory or URL where files should be placed
 Upload files with wildcard expansion
 -c  continue, reput
 -d  create directories the same as in file names and put the
     files into them instead of current directory
 -E  delete local files after successful transfer (dangerous)
 -a  use ascii mode (binary is the default)
 -O <base> specifies base directory or URL where files should be placed
 Usage: %s
 Usage: %s %s[-f] files...
 Usage: %s <cmd>
 Usage: %s <jobno> ... | all
 Usage: %s [-d #] dir
 Usage: %s [-e cmd] [-p port] [-u user[,pass]] <host|url>
 Usage: %s [-e] <file|command>
 Usage: %s [-p]
 Usage: %s [-v] [-v] ...
 Usage: %s [<exit_code>]
 Usage: %s [<jobno>]
 Usage: %s [OPTS] command args...
 Usage: %s [OPTS] file
 Usage: %s [OPTS] files...
 Usage: %s [OPTS] mode file...
 Usage: %s [options] <dirs>
 Usage: %s cmd [args...]
 Usage: %s command args...
 Usage: %s local-dir
 Usage: %s module [args...]
 Usage: cd remote-dir
 Usage: find [OPTS] [directory]
Print contents of specified directory or current directory recursively.
Directories in the list are marked with trailing slash.
You can redirect output of this command.
 -d, --maxdepth=LEVELS  Descend at most LEVELS of directories.
 Usage: mv <file1> <file2>
 Usage: reget [OPTS] <rfile> [-o <lfile>]
Same as `get -c'
 Usage: rels [<args>]
Same as `ls', but don't look in cache
 Usage: renlist [<args>]
Same as `nlist', but don't look in cache
 Usage: reput <lfile> [-o <rfile>]
Same as `put -c'
 Usage: sleep <time>[unit]
Sleep for given amount of time. The time argument can be optionally
followed by unit specifier: d - days, h - hours, m - minutes, s - seconds.
By default time is assumed to be seconds.
 Use specified info for remote login. If you specify URL, the password
will be cached for future usage.
 Valid arguments are: Validation: %u/%u (%u%%) %s%s Verify command failed without a message Verifying... Wait for specified job to terminate. If jobno is omitted, wait
for last backgrounded job.
 Waiting for TLS shutdown... Waiting for data connection... Waiting for meta-data... Waiting for other copy peer... Waiting for response... Waiting for transfer to complete Warning: chdir(%s) failed: %s
 Warning: discarding incomplete command
 [%d] Done (%s) [%u] Attached to terminal %s. %s
 [%u] Attached to terminal.
 [%u] Detached from terminal. %s
 [%u] Detaching from the terminal to complete transfers...
 [%u] Exiting and detaching from the terminal.
 [%u] Finished. %s
 [%u] Moving to background to complete transfers...
 [%u] Started.  %s
 [%u] Terminated by signal %d. %s
 [re]cls [opts] [path/][pattern] [re]nlist [<args>] ` `%s' at %lld %s%s%s%s `%s', got %lld of %lld (%d%%) %s%s `lftp' is the first command executed by lftp after rc files
 -f <file>           execute commands from the file and exit
 -c <cmd>            execute the commands and exit
 --help              print this help and exit
 --version           print lftp version and exit
Other options are the same as in `open' command
 -e <cmd>            execute the command just after selecting
 -u <user>[,<pass>]  use the user/password for authentication
 -p <port>           use the port for connection
 <site>              host name, URL or bookmark name
 alias [<name> [<value>]] ambiguous argument %s for %s ambiguous variable name announced via  anon - login anonymously (by default)
 assuming failed host name lookup bookmark [SUBCMD] bookmark command controls bookmarks

The following subcommands are recognized:
  add <name> [<loc>] - add current place or given location to bookmarks
                       and bind to given name
  del <name>         - remove bookmark with the name
  edit               - start editor on bookmarks file
  import <type>      - import foreign bookmarks
  list               - list bookmarks (default)
 cache [SUBCMD] cannot create socket of address family %d cannot get current directory cannot parse EPSV response cannot seek on data source cat - output remote files to stdout (can be redirected)
 -b  use binary mode (ascii is the default)
 cat [-b] <files> cd <rdir> cd ok, cwd=%s
 chdir(%s) failed: %s
 chmod [OPTS] mode file... chunked format violated copy: destination file is already complete
 copy: put is broken
 copy: received redirection to `%s'
 debug is off
 debug level is %d, output goes to %s
 depend module `%s': %s
 du [options] <dirs> eta: execl(/bin/sh) failed: %s
 execlp(%s) failed: %s
 execvp(%s) failed: %s
 exit - exit from lftp or move to background if jobs are active

If no jobs active, the code is passed to operating system as lftp
termination status. If omitted, exit code of last command is used.
`bg' forces moving to background if cmd:move-background is false.
 exit [<code>|bg] extra server response file name missed in URL file size decreased during transfer ftp over http cannot work without proxy, set hftp:proxy. ftp:fxp-force is set but FXP is not available ftp:proxy password:  ftp:skey-force is set and server does not support OPIE nor S/KEY ftp:ssl-force is set and server does not support or allow SSL get [OPTS] <rfile> [-o <lfile>] glob [OPTS] <cmd> <args> help [<cmd>] history -w file|-r file|-c|-l [cnt] host name resolve timeout integer overflow invalid argument %s for %s invalid argument for `--sort' invalid block size invalid boolean value invalid boolean/auto value invalid floating point number invalid mode string: %s
 invalid number invalid peer response format invalid server response format invalid unsigned number kill all|<job_no> lcd <ldir> lcd ok, local cwd=%s
 lftp [OPTS] <site> ls [<args>] max-retries exceeded memory exhausted mget [OPTS] <files> mirror [OPTS] [remote [local]] mirror: protocol `%s' is not suitable for mirror
 module name [args] modules are not supported on this system more <files> mput [OPTS] <files> mrm <files> mv <file1> <file2> next announce in %s next request in %s no closure defined for this setting no such %s service no such variable non-option arguments found only PUT and POST values allowed open [OPTS] <site> parse: missing filter command
 parse: missing redirection filename
 peer closed connection peer closed connection (before handshake) peer closed just accepted connection peer handshake timeout peer short handshake peer unexpectedly closed connection after %s pget [OPTS] <rfile> [-o <lfile>] pget: falling back to plain get pipe() failed:  pseudo-tty allocation failed:  put [OPTS] <lfile> [-o <rfile>] queue [OPTS] [<cmd>] quote <cmd> recls [<args>]
Same as `cls', but don't look in cache
 rename successful
 rm [-r] [-f] <files> rmdir [-f] <dirs> saw file size in response scache [<session_no>] seek failed set [OPT] [<var> [<val>]] source <file> the source file size is unknown the target file is remote this encoding is not supported total unknown address family `%s' unsupported network protocol user <user|URL> [<pass>] wait [<jobno>] zcat <files> zmore <files> Project-Id-Version: lftp 2.2.3
Report-Msgid-Bugs-To: lftp-bugs@lftp.yar.ru
POT-Creation-Date: 2015-06-17 17:10+0300
PO-Revision-Date: 2016-02-22 16:02+0000
Last-Translator: Giovanni Bortolozzo <Unknown>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:53+0000
X-Generator: Launchpad (build 18115)
Language: it
 	Esecuzione del comando interno `%s' [%s]
 	Ripetere conteggio: %d
 	In esecuzione
 	In attesa di un comando
 	Attendo la fine del job [%d] per terminare
 	In attesa del termine delle operazioni:  
Esegue il mirror della directory remota specificata nella directory locale

 -c, --continue se possibile riprende l'esecuzione del mirror
 -e, --delete elimina i file non presenti sul sito remoto
     --delete-first elimina i vecchi file prima di trasferire quelli nuovi
 -s, --allow-suid imposta i bit suid/sgid come nel sito remoto
     --allow-chown cerca di impostare il proprietario e i gruppi sui file
     --ignore-time ignora il tempo quando occorre decidere se effettuare lo scarico
 -n, --only-newer scarica solo i file più recenti (-c non funziona)
 -r, --no-recursion non considera le sottodirectory
 -p, --no-perms non imposta i permessi sui file
     --no-umask non applica l'umask ai permessi dei file
 -R, --reverse mirror inverso (effettua il put dei file)
 -L, --dereference scarica i collegamenti simbolici come file
 -N, --newer-than=SPEC scarica solo i file più recenti di un periodo specificato
 -P, --parallel[=N] scarica N file in parallelo
 -i RX, --include RX include i file corrispondenti
 -x RX, --exclude RX esclude i file corrispondenti
                        RX è un espressione regolare estesa
 -v, --verbose funzionamento prolisso
     --log=FILE scrive i comandi lftp che vengono eseguiti in FILE
     --script=FILE scrive i comandi lftp in FILE, senza eseguirli
     --just-print, --dry-run come --script=-

Quando si usa -R, la prima directory è quella locale mentre la seconda è quella remota.
Se è omessa la seconda directory, è usato il nome base della prima directory. Se sono
omesse entrambe le directory, sono usate le directory locale e remota correnti.
  - protocollo non supportato  -w <file> Scrive la storia sul file.
 -r <file> Legge la storia dal file; accoda alla storia attuale.
 -c Pulisce la storia.
 -l Elenca la storia (default).
Argomenti opzionali non possono specificare il numero di linee da elencare,
o "all" per elencare tutte le entrate.
  [in cache] !<comando-shell> %.0f B/s %.0fb/s %.1f KiB/s %.1fK/s %.2f MiB/s %.2fM/s %d address$|es$ trovato %ld byte salvat$#l#o|i$ in cache %lld $#ll#byte|bytes$ trasferiti %lld $#ll#byte|bytes$ trasferiti in %ld $#l#secondo|secondi$ %s (filtro) %s fallito per %d su %d director$y|ies$
 %s fallito per %d su %d file$|s$
 %s è un alias interno per %s
 %s è un alias per `%s'
 %s ok, create %d directory$|$
 %s ok, rimosse %d directory$|$
 %s ok, rimossi %d file$|$
 %s ok, creato `%s'
 %s ok, rimosso `%s'
 %s%d errore$|s$ rilevato
 %s: %d - job non esistente
 %s: %s - non c'è questa sessione in cache. Usare `scache' per vedere
un elenco delle sessioni.
 %s: %s - non è un numero
 %s: %s. Usare `set -a' per vedere tutte le variabili.
 %s: %s: il file esiste già e xfer:clobber non è impostato
 %s: %s: nessun file trovato
 %s: -m: Richiesto un numero come secondo argomento.  %s: -n: Ci vuole un numero.  %s: -n: atteso valore positivo.  %s: BUG - rilevato deadlock
 %s: GetPass() fallita -- assumo login anonimo
 %s: Nessuna coda è attivata.
 %s: Manca l'operando per `expire'
 %s: Manca l'operando per la dimensione
 %s: Specificare il file meta-info o l'URL.
 %s: directory sorgente ambigua ("%s" o "%s"?)
 %s: directory di destinazione ambigua ("%s" o "%s"?)
 %s: argomento richiesto.  %s: serve un nome di bookmark
 %s: impossibile creare una sessione locale
 %s: il comando '%s' non è previsto.
 %s: errore analisi data-ora
 %s: specificazione data-ora mancante
 %s: importa il tipo richiesto (netscape, ncftp)
 %s: dimensione blocco non valido `%s'
 %s: opzione non valida -- "%c"
 %s: nessun job corrente
 %s: nessuna vecchia directory per questo sito
 %s: bookmark `%s' inesistente
 %s: l'opzione "%c%s" non accetta argomenti
 %s: l'opzione "%s" è ambigua; possibilità: %s: l'opzione "--%s" non accetta argomenti
 %s: l'opzione "--%s" richiede un argomento
 %s: l'opzione "-W %s" non accetta un argomento
 %s: l'opzione "-W %s" è ambigua
 %s: l'opzione "-W %s" richiede un argomento
 %s: l'opzione richiede un argomento -- "%c"
 %s: ricevuto reindirizzamento a `%s'
 %s: espressione regolare `%s':%s
 %s: qualche altro job è in attesa del job %d
 %s: non sono permessi gli spazi bianchi nei nomi dei bookmark
 %s: confitto nel riepilogo con --max-depth=%i
 %s: opzione "%c%s" non riconosciuta
 %s: opzione "--%s" non riconosciuta
 %s: attendere, trovato loop
 %s: attenzione: il riepilogo è lo stesso di quando viene usato --max-depth=0
 %sModificati: %d file$|$, %d link simbolic$o|i$
 %sNuovi: %d file$|$, %d link simbolic$o|i$
 %sCancellati: %d directory$|$, %d file$|$, %d link simbolic$o|i$
 %sTotale: %d directory$|$, %d file$|$, %d link simbolic$o|i$
 ' (comandi) **** FXP: abbandono, ritorno alla copia normale
 **** FXP: tento di invertire ftp:fxp-passive-source
 **** FXP: tento di invertire ftp:fxp-passive-sscn
 **** FXP: cercando di invertire ftp:ssl-protect-fxp
 , dimensione massima %ld
 , dimensione illimitata =1 =0|>1 Connessione accettata da [%s]:%d Connessione dati accettata da (%s) porta %u Accesso non riuscito:  Account richiesto, impostare la variabile ftp:acct job$|s$ aggiunt$o|i$ `%s' comando ambiguo.
 `%s' comando ambiguo. Usare `help' per vedere i comandi disponibili.
 Comando ambiguo.  Collegare il terminale al processo lftp specificato in background.
 Impossibile associare un attacco per torrent: porta-intervallo Cambia la directory locale a <ldir>. La directory locale precedente è salvata
come `-'. Si può quindi fare `lcd -' per tornare indietro.
 Cambia la directory remota corrente a <rdir>. La directory remota precedente
è salvata come `-'. Si può fare `cd -' per tornare indietro. Su disco è
salvata la directory precedente di ogni sito, quindi si può fare 
`open sito; cd -' anche dopo il riavvio di lftp.
 Cambia i permessi di ogni FILE in MODE.

 -c, --changes - come verbose ma notifica solo se viene effettuato un cambiamento
 -f, --quiet - Ignora la maggior parte dei messaggi d'errore
 -v, --verbose - mostra un output di diagnostica per ogni file processato
 -R, --recursive - cambia file e directory ricorsivamente

MODE può essere di forma ottale o simbolica (vedi chmod(1))
 Cambio la directory remota... Chiude le connessioni inattive. Di default solo quelle con il server corrente.
 -a  chiude le connessioni inattive con tutti i server.
 Chiusura connessione HTTP Chiusura del socket dati interrotta Chiusura del socket di controllo Chiusura del socket dati Chiusura della connessione inattiva Comandi in coda: Connesso Connessione del socket dei dati a (%s) porta %u Connessione del socket dei dati al proxy %s (%s) porta %u Connessione a %s%s (%s) porta %u Collegamento al peer %s porta %u Connessione in corso... Connessione inattiva Raggiunto il limite di connessioni Impossibile analizzare la linea di stato HTTP Creata coda di terminazione.
 Risoluzione DNS non attendibile. Connessione dati stabilita L'altro estremo della connesione dati ha un indirizzo che non corrisponde L'altro estremo della connessione dati ha un numero di porta errato Definisce o elimina un alias <nome>. Se <valore> è omesso,
l'alias è eliminato, diversamente ha <valore> come valore.
Se non è dato alcun argomento vengono elencati gli alias correnti.
 Attesa prima della riconessione Attendi prima di riprovare Cancella il job specificato con <num_job> oppure tutti i job
 job$|s$ cancellat$o|i$ Completato Esegue i comandi specificati nel file <file>
 Esegue il comando site <site_cmd> e mostra il risultato
È possibile redirigere il suo output
 Espande i metacaratteri ed esegue il comando specificato.
Le opzioni possono essere usate per espandere i metacaratteri in un
elenco di file, di directory o di entrambi. La selezione del tipo non è
molto affidabile e dipende dal server. Se non può essere determinato
il tipo di una voce, sarà comunque inclusa nell'elenco.
 -f file normali (predefinito)
 -d directory
 -a tutti i tipi
 Negoziazione FEAT... Fallito il cambiamento di modo di `%s' perche` non è disponibile nessun modo precedente.
 Fallito il cambiamento di modo di `%s' a %04o (%s).
 Errore fatale Scarico gli header... Impossibile accedere al file File spostato File spostato in ` Manca il nome del file.  Ottiene i file selezionati con l'utilizzo di metacaratteri estesi
 -c  continua, riprende il trasferimento
 -d  crea directory con lo stesso nome del file e inserisce
     i file in queste directory invece che in quella corrente
 -E  elimina i files remoti dopo aver completato correttamente il trasferimento
 -a  usa la modalità ascii (l'impostazione predefinita è binaria)
 -O <base> specifica la directory base o l'URL dove posizionare i file
 Ricezione dei contenuti delle cartelle Scarico la lista dei file (%lld) [%s] Ricezione delle informazioni sui file Acquisizione metadati: %s Raggruppa assieme più comandi per eseguirli come fossero uno solo
È possibile lanciare tale gruppo in background
 Effettuazione handshake... Incontrata EOF Ricerca del nome dell'host fallita Interruzione Indirizzo numerico IPv4 non valido Indirizzo numerico IPv6 non valido Comando non valido.  Formato dell'intervallo non valido. Il formato è min-max, es. 10-20. Formato temporale non valido. Il formato è <tempo><unità>, es. 2h30m. Lettera per l'unità di tempo non valida, sono permesse solo [smhd] LFTP è un software libero: ridistribuibile e modificabile
nei termini della GNU General Public License pubblicati
dalla Free Software Foundation, sia nella versione 3 che 
(a scelta) nelle versioni successive.

Questo programma è distribuito nella speranza che possa essere utile,
ma SENZA ALCUNA GARANZIA; senza neppure la garanzia implicita di
COMMERCIABILITÀ o IDONEITÀ PER UN PARTICOLARE SCOPO. Consultare
la GNU General Public License per maggiori dettagli.

LFTP dovrebbe contenere una copia della GNU General Public License. 
In caso contrario, consultare <http://www.gnu.org/licenses/licenses.it.html>.
 LFTP | Versione %s | Copyright (c) 1996-%d Alexander V. Lukyanov
 Lancia una shell o un comando di shell
 Librerie usate:  Elenca le sessioni in cache o passa al numero di sessione specificato
 Elenca i file remoti.
Per impostazione predefinita, l'output di nlist viene messo in cache, 
per visualizzare un nuovo elenco usare "renlist" oppure "cache flush".
 Elenca i file remoti. È possibile redirigere l'output di questo comando
su un file o tramite una pipe a un comando esterno.
Per impostazione predefinita, l'output di ls viene messo in cache, per vedere
un nuovo elenco usare "rels" o "cache flush".
Per ulteriori informazioni consultare "help cls".
 Carica un modulo (oggetto condiviso).  Il modulo può contenere la funzione
   void module_init(int argc,const char *const *argv)
Se il nome contiene una barra, allora il modulo viene cercato nella
directory corrente, diversamente nelle directory specificate nel module:path.
 Accesso al server... Accesso non riuscito MLSD è disabilitato da ftp:use-mlsd MLST e MLSD non sono supportati da questo sito Creo la connessione dati... Creazione della cartella `%s' Creo il link simbolico da `%s' a `%s' Mirror della directory `%s' Modo di `%s' cambiato a %04o (%s).
 Il modulo per il comando `%s' non ha registrato il comando.
 job$|s$ spostat$o|i$ Nessun indirizzo trovato job #%i non accodato.
 Nessuno job in coda coincide con "%s".
 Nessuno job in coda.
 `%s' comando inesistente. Usare `help' per vedere i comandi disponibili.
 Non connesso Ora sto eseguendo: L'oggetto non è nella cache e la proprietà http:cache-control è only-if-cached La vecchia cartella `%s' non è stata rimossa Il vecchio file `%s' non è stato rimosso Operazione non supportata metodo POST fallito Password:  Connessione chiusa dal peer Persisti e riprova Mostra l'URL remoto corrente.
 -p  mostra la password
 Mostra un aiuto per il comando <cmd>, o elenca i comandi disponibili
 Protocollo di proxy non supportato La coda è stata fermata Sto ricevendo tutto Ricevuto tutto (totale) Ricevuto ultimo blocco Dati ricevuti non sufficienti, riprovo Ricevute informazioni valide da %d IPv6 peer$|s$ Ricevute informazioni valide da %d peer$|s$ Sto ricevendo i dati Sto ricevendo i dati Sto ricevendo i dati/TLS Cancella directory remote
 Cancella file remoti
 -r  cancellazione ricorsiva di directory, usare con cautela
 -f  lavora silenziosamente
 Cancella i file specificati espandendo i caratteri jolly
 Eliminazione della vecchia directory `%s' Eliminazione vecchio file `%s' Cancellazione del vecchio file locale `%s' Rinomina <file1> in <file2>
 Risoluzione dell'indirizzo dell'host... Ritento il mirror...
 Programma di connessione in esecuzione SITE CHMOD è disabilitato da ftp:user-site-chmod SITE CHMOD non è supportato da questo sito Uguale a `cat <file> | more'. Se PAGER è impostato, è usato come filtro
 Uguale a cat, ma filtra ognuno dei file attraverso bzcat
 Uguale a cat, ma filtra ognuno dei file attraverso zcat
 Uguale a more, ma filtra ognuno dei file attraverso bzcat
 Uguale a more, ma filtra ognuno dei file attraverso zmore
 Invia in background...
 Seleziona un server, un URL o un segnalibro
 -e <cmd> esegue il comando dopo la selezione
 -u <utente>[,<pass>]  usa utente/password per l'autenticazione
 -p <porta> porta da usare per la connessione
 -s <slot> assegna la connessione a questo slot
 <sito> nome dell'host, dell'URL o del segnalibro
 Invia segnalazioni di bug o domande alla mailing list <%s>.
 Invia il comando senza interpretarlo. Usare con cautela - può portare ad
uno stato remoto sconosciuto e quindi provocare la riconnessione. Non è
possibile essere sicuri che qualsiasi modifica dello stato remoto a causa
di un comando "quoted" sia stabile - può essere reinizializzato da una
riconnessione in qualsiasi istante.
 Invio comandi... Sto inviando i dati Sto inviando i dati/TLS Invio richiesta... La risposta del server corrisponde a ftp: retry-530, riprovo La risposta del server corrisponde a ftp: retry-530-anonymous, riprovo Imposta la variabile al valore specificato. Se è omesso il valore, allora
la cancella. Il nome della variabile è nel formato ``nome/chiusura'' dove
chiusura può specificare l'esatta applicazione dell'impostazione. Si veda
lftp(1) per i dettagli.
Se set è chiamato senza specificare una variabile allora sono mostrate solo
le impostazioni modificate.
Ciò può essere modificato con le opzioni:
 -a  elenca tutte le impostazioni, compresi i valori predefiniti
 -d  elenca solo i valori predefiniti, che non sono necessariamente quelli
     correnti
 Mostra la versione di lftp
 Arresto:  Salta la directory "%s" (solo se esistente) Salta il file "%s" (solo se esistente) Salta il collegamento simbolico "%s" (solo se esistente) Errore di socket (%s) - riconnessione in corso Spiacente, nessun aiuto per %s
 Salvataggio non riuscito - è necessario effettuare reput Rapporto sull'uso del disco.
 -a, --all conta tutti i file, non solo le cartelle
     --block-size=SIZ usa blocchi di SIZ-byte
 -b, --bytes mostra le dimensioni in byte
 -c, --total produce un totale generale
 -d, --max-depth=N mostra il totale per ogni directory (o file, con --all)
                       solo se è di N o meno livelli dalla riga
                       comando; --max-depth=0 è equivalente a
                       --summarize
 -F, --files stampa il numero di file invece che le dimensioni
 -h, --human-readable stampa la grandezza in formato leggibile dall'uomo (es., 1K 234M 2G)
 -H, simile a --si , ma usa potenze di 1000 e non di 1024
 -k, --kilobytes come --block-size=1024
 -m, --megabytes come --block-size=1048576
 -S, --separate-dirs non include le dimensioni delle sottocartelle
 -s, --summarize mostra solo un totale per ogni argomento
     --esclude=PAT esclude file che corrispondono a PAT
 Metto ad off (disabilito) la modalità passiva Abilito la modalità passiva Passo a modalita` NOREST Negoziazione TLS... Non ci sono jobs in esecuzione e `cmd:move-background' non è impostato.
Usa `exit bg' per muoverlo forzatamente in background o `killall' per terminare i job.
 Timeout - riconnessione in corso Troppi reindirizzamenti Trasferit$o|i$ %d file in totale
 Fallito il trasferimento di %d su %d file$|$
 Trasferimento del file `%s' Usare `%s --help' per maggiori informazioni
 Usare `help %s' per maggiori informazioni.
 Attivando sync-mode `%s' comando sconosciuto.
 Errore di sistema sconosciuto Copia <lfile> con nome remoto <rfile>.
 -o <rfile> specifica il nome remoto per il file (default - il nome di lfile)
 -c  continua, reput
     richiede il permesso per sovrascrivere i file remoti
 -E  cancella i file locali dopo averli trasferiti con successo (pericoloso)
 -a  usa la modalità ascii (quella predefinita è la binaria)
 -O <base> specifica la directory o l'URL base dove dovrebbero essere messi
     i file
 Copia i file sul sito remoto espandendo i caratteri jolly
 -c  continua, reput
 -d  crea le directory presenti nei nome dei file e mette i file lì dentro
     invece che nella directory corrente
 -E  cancella i file locali dopo averli trasferiti con successo (pericolosa)
 -a  usa la modalità ascii (quella predefinita è la binaria)
 -O <base> specifica la directory o l'URL base dove dovrebbero essere messi
     i file
 Uso: %s
 Uso: %s %s[-f] files..
 Uso: %s <cmd>
 Uso: %s <numjob> ...| all
 Usare: %s [-pd #] dir
 Uso: %s [-e cmd] [-p porta] [-u utente,[password]] <host|url>
 Usare: %s [-e] <file|command>
 Uso: %s [-p]
 Uso: %s [-v] [-v] ...
 Uso: %s [<codice_d'uscita>]
 Uso: %s [<numjob>]
 Uso: %s [OPZ] comando arg...
 Uso: %s [OPZ] <file>
 Uso: %s [OPZ] file...
 Usare: %s [OPZIONI] mode file...
 Usare: %s [opzioni] <dirs>
 Uso: %s comando [argomenti...]
 Uso: %s comando arg...
 Uso: %s dir-locale
 Uso: %s modulo [arg...]
 Uso: cd dir-remota
 Uso: find [OPTS] [directory]
Mostra ricorsivamente il contenuto della directory specificata della 
directory corrente.
Nell'elenco le directory sono marcate con uno slash finale.
È possibile redigire l'output di questo comando.
 -d, --maxdepth=LIVELLI Discendi al massimo LIVELLI nelle directory.
 Uso: mv <file1> <file2>
 Uso: reget [OPZIONI] <rfile> [-o <lfile>]
Stesso di 'get -c'
 Uso: rels [<args>]
Stesso di `ls', ma non guarda in cache
 Uso: renlist [<args>]
Stesso di `nlist', ma non guarda in cache
 Uso: reput <lfile> [-o <rfile>]
Stesso di `put -c'
 Uso: sleep <tempo>[unità]\n"
Dorme per il tempo specificato.  L'argomento tempo opzionalmente può essere
seguito da un'unità: d - giorni, h - ore, m - minuti, s - secondi.
Per default il tempo è assunto essere in secondi.
 Usa le informazioni specificate per il login remoto. Se si specifica l'URL, la password
sarà messa in cache per l'uso futuro.
 Sono argomenti validi: Convalida: %u/%u (%u%%) %s%s Comando di verifica fallito senza un messaggio Verificando... Attende la conclusione del job specificato.  Se è omesso il numjob,
attende l'ultimo job messo in background.
 In attesa di arresto TLS... In attesa della connessione dati... In attesa dei metadati... In attesa di altra copia da peer ... In attesa della risposta... Attendo la fine del trasferimento Attenzione: chdir(%s) fallita: %s
 Attenzione: ignorato comando incompleto
 [%d] Fatto (%s) [%u] Agganciato al terminale %s. %s
 [%u] Agganciato al terminale.
 [%u] Scollegato dal terminale. %s
 [%u] Scollegarsi dal terminale per completare i trasferimenti...
 [%u] Uscita e scollegamento dal terminale.
 [%u] Completato. %s
 [%u] Spostarsi in background per completare i trasferimenti ...
 [%u] Avviato.  %s
 [%u] Interrotto tramite il segnale %d. %s
 [re]cls [opts] [path/][pattern] [re]nlist [<args>] ` `%s' a %lld %s%s%s%s %s', ricevuti %lld di %lld (%d%%) %s%s `lftp' è il primo comando eseguito da lftp dopo i file rc
 -f <file>             esegue i comandi presenti nel file ed esce
 -c <cmd>              esegue i comandi ed esce
 --help                mostra questo messaggio ed esce
 --version             mostra la versione di lftp ed esce
Le altre opzioni sono analoghe a quelle del comando `open'
 -e <cmd>              esegue il comando subito dopo la selezione
 -u <utente>[,<pass>]  usa utente/password per l'autentificazione
 -p <porta>            usa la porta per la connessione
 <sito>                nome host, URL o nome di un bookmark
 alias [<nome> [<valore>]] Argomento ambiguo %s per %s nome della variabile ambiguo Annunciato per mezzo di  anon - login anonimo (predefinito)
 dò per fallita la ricerca del nome host bookmark [SUBCMD] Il comando bookmark controlla i segnalibri
Sono riconosciuti i seguenti comandi secondari:
  add <nome> [<loc>] - aggiunge la posizione corrente o quella specificata
                       ai segnalibri, assegnandole il nome specificato
  del <nome> - rimuove il segnalibro specificato dal nome
  edit - avvia il gestore sul file dei segnalibri
  import <tipo> - importa segnalibri esterni
  list - elenca i segnalibri (predefinito)
 cache [SUBCMD] impossibile creare il socket della famiglia di indirizzi %d impossibile determinare la directory corrente non posso analizzare la risposta EPSV non posso cercare nei dati sorgenti cat - visualizza i file remoti su stdout (può essere rediretto)
 -b  usa la modalità binaria (quella ascii è la predefinita)
 cat [-b] <file> cd <rdir> cd ok, cwd=%s
 chdir(%s) fallita: %s
 chmod [OPTS] mode file... violato il formato chunked copy: file destinazione gia` completo
 copy: put corrotto
 copy: ricevuto un reindirizzamento verso '%s'
 il debug è off
 il livello di debug è %d, l'output va su %s
 dipendenza modulo `%s': %s
 du [options] <dirs> eta: execl(/bin/sh) fallita: %s
 execlp(%s) fallita: %s
 execvp(%s) fallita: %s
 exit - esce da lftp o passa in background se ci sono job attivi

Se non ci sono job attivi, il codice è passato al sistema operativo come
stato di terminazione di lftp. Se omesso, è usato il codice d'uscita
dell'ultimo comando.
`bg' forza il passaggio in background se cmd:move-background è falso.
 exit [<code>|bg] risposta extra del server nell'URL manca il nome del file La dimensione del file è diminuita durante il trasferimento ftp su http non puo` lavorare senza proxy, imposta hftp:proxy. ftp:fxp-force è impostato ma FXP non è disponibile ftp:proxy password:  ftp:skey-force è impostata e il server non supporta OPIE p S/KEY ftp:ssl-force e` impostata e il server non supporta o permette SSL get [OPZIONI] <rfile> [-o <lfile>] glob [OPTS] <cmd> <args> help [<cmd>] history -w file|-r file|-c|-l [cnt] timeout nella risoluzione del nome dell'host Overflow di un numero intero Argomento non valido %s per %s Argomento non valido per `--sort' dimensione blocco non valida valore booleano non valido Valore booleano o automatico non valido numero in virgola mobile non valido Stringa della modalità non valida: %s
 numero non valido Formato della risposta del peer non valido formato risposta del server non valido Numero non firmato invalido kill all|<num_job> lcd <ldir> lcd ok, cwd locale=%s
 lftp [OPZIONI] <sito> ls [<arg>] superato il numero massimo di tentativi Memoria esaurita mget [OPZIONI] <file> mirror [OPZIONI] [remota [locale]] Mirror: il protocollo `%s' non è adatto per effettuare il mirror
 module nome [arg...] moduli non sono supportati su questo sistema more <file> mput [OPZ] <file> mrm <file> mv <file1> <file2> Prossimo annuncio in %s Richiesta successiva in %s nessuna chiusura definita per questa impostazione servizio %s inesistente variabile inesistente Trovati argomenti non opzionali sono ammessi solo i valori PUT e POST open [OPZIONI] <host> parse: manca il comando filtro
 parse: manca il nome del file per la redirezione
 Il peer ha chiuso la connessione Il peer ha chiuso la connessione (prima dell'handshake ) Il peer ha chiuso il collegamento appena accettato Timeout dell'handshake del peer Handshake breve del peer Connessione al peer chiusa in modo inaspettato dopo %s pget [OPZIONI] <rfile> [-o <lfile>] Pget: ritorno a get vuoto Pipe() non riuscita:  Allocazione pseudo-tty non riuscita:  put [OPZ] <lfile> [-o <rfile>] queue [OPTS] [<cmd>] quote <cmd> recls [<args>]
Stesso di `cls', ma non guarda in cache
 rinominazione riuscita
 rm [-r] [-f] <file> rmdir [-f] <dir> ottenuta in risposta la dimensione del file scache [<num_sessione>] ricerca non riuscita set [OPT] [<var> [<val>]] source <file> La dimensione del file di origine è sconosciuta Il file di destinazione è remoto questa codifica non è supportata totale famiglia di indirizzi `%s' sconosciuta protocollo di rete non supportato user <utente|URL> [<pass>] wait [<num_job>] zcat <file> zmore <file> 