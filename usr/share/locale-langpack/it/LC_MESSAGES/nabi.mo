��    G      T  a   �        /     `   A  �   �  &   �  %   �  #   �  %   �  
        (     1  	   F     P     Y     h     y  C   �     �     �  
   �  
   �     �  
   �  *   		     4	  	   G	  	   Q	     [	     d	     x	     �	     �	     �	     �	     �	     �	  �   �	  �   }
  �   	     �     �     �     �     �     �     �       f      `   �  d   �     M     S  
   X     c     p     �     �     �     �  	   �     �     �     �                    %     3     C     O     [  �  h  <     c   N  5  �  %   �  %     #   4  %   X     ~     �     �  	   �     �     �     �     �  c   �     X     h     n     ~     �     �  -   �     �  	   �  	   �               *  .   =     l     u     �     �  7   �  �   �  �   x  �        �     �     �     �                    5  j   R  o   �  i   -     �     �     �     �      �     �  %        +  "   D  	   g     q       
   �     �     �     �     �     �     �     �     �     2   6   5       >         ,      
          E       &         '   B       1       $   "   3                     A   0   .   %      /          #   !          ;      G         @                                             D             8       =   4   ?   (                     7          C      :          )   *   F      	   +   9               -       <    * You should restart nabi to apply above option <span size="large">An Easy Hangul XIM</span>
version %s

Copyright (C) 2003-2011 Choe Hwanjin
%s <span size="x-large" weight="bold">Can't load tray icons</span>

There are some errors on loading tray icons.
Nabi will use default builtin icons and the theme will be changed to default value.
Please change the theme settings. <span weight="bold">Connected</span>:  <span weight="bold">Encoding</span>:  <span weight="bold">Locale</span>:  <span weight="bold">XIM name</span>:  About Nabi Advanced Automatic reordering BackSpace Choseong Commit by word English keyboard Hangul Hangul input method: Nabi - You can input hangul using this program Hangul keyboard Hanja Hanja Font Hanja Lock Hanja Options Hanja keys Ignore fontset information from the client Input mode scope:  Jongseong Jungseong Keyboard Keypress Statistics Nabi Preferences Nabi keypress statistics Nabi: %s Nabi: error message Off keys Orientation Preedit string font:  Press any key which you want to use as candidate key. The key you pressed is displayed below.
If you want to use it, click "Ok", or click "Cancel" Press any key which you want to use as off key. The key you pressed is displayed below.
If you want to use it, click "Ok" or click "Cancel" Press any key which you want to use as trigger key. The key you pressed is displayed below.
If you want to use it, click "Ok" or click "Cancel" Select candidate key Select hanja font Select off key Select trigger key Shift Space Start in hangul mode The orientation of the tray. This key is already registered for <span weight="bold">candidate key</span>
Please  select another key This key is already registered for <span weight="bold">off key</span>
Please  select another key This key is already registered for <span weight="bold">trigger key</span>
Please  select another key Total Tray Tray icons Trigger keys Use dynamic event flow Use simplified chinese Use system keymap Use tray icon XIM Server is not running XIM name: _Hanja Lock _Hide palette _Reset _Show palette hangul(hanja) hanja hanja(hangul) per application per context per desktop per toplevel Project-Id-Version: nabi
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-01-26 17:31+0900
PO-Revision-Date: 2009-04-21 18:50+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:12+0000
X-Generator: Launchpad (build 18115)
 * Riavviare il programma per applicare l'opzione selezionata <span size="large">Un facile XIM hangul</span>
versione %s

Copyright (C) 2003-2011 Choe Hwanjin
%s <span size="x-large" weight="bold">Non è possibile caricare le icone nell'area di notifica</span>

Si sono verificati degli errori nel caricamento delle icone dell'area di notifica.
Il programma userà le icone predefinite e il tema sarà cambiato con quello predefinito.
Modificare le impostazioni del tema. <span weight="bold">Connesso</span>:  <span weight="bold">Codifica</span>:  <span weight="bold">Locale</span>:  <span weight="bold">Nome XIM</span>:  Informazioni su Nabi Avanzate Ridisposizione automatica BackSpace Choseong Operare per parola Tastiera inglese Hangul Metodo di inserimento hangul: Nabi - È possibile inserire caratteri hangul usando questo programma Tastiera hangul Hanja Carattere hanja Blocco hanja Opzioni hanja Tasti hanja Ignora le informazioni del fontset del client Ambito modalità di input:  Jongseong Jungseong Tastiera Statistiche dei tasti premuti Preferenze di Nabi Statistiche nabi dei tasti premuti da tastiera Nabi: %s Nabi: messaggio d'errore Tasti di spegnimento Orientamento Modificare preliminarmente il carattere delle stringhe  Premere un tasto qualsiasi che si vuole usare come tasto candidato: il tasto premuto è mostrato in basso.
Per usarlo, premere "Ok" o "Annulla" Premere qualsiasi tasto da usare come tasto di spegnimento: il tasto premuto è mostrato in basso.
Per usarlo, fare clic su "Ok" o su "Annulla" Premere qualsiasi tasto da usare come tasto di avvio: il tasto premuto è mostrato in basso.
Per usarlo fare clic su «Ok» o su «Annulla» Seleziona il tasto candidato Selezionare il carattere hanja Seleziona tasto di spegnimento Seleziona il tasto di avvio Maiusc Spazio Inizia in modalità hangul L'orientamento del pannello. Questo tasto è già registrato come <span weight="bold">tasto candidato</span>
Selezionare un altro tasto Questo tasto è già registrato come <span weight="bold">tasto di spegnimento</span>
Selezionare un altro tasto Questo tasto è già registrato come <span weight="bold">tasto di avvio</span>
Selezionare un altro tasto Totale Vassoio Icone della barra Tasti di avvio Usa il flusso di eventi dinamici Usare il cinese semplificato Usa la mappa dei caratteri di sistema Usa le icone della barra Il server XIM non è in esecuzione Nome XIM: Blocco _hanja _Nascondi tavolozza _Reimposta Mo_stra tavolozza hangul (hanja) hanja hanja (hangul) per applicazione per contesto per desktop per livello massimo 