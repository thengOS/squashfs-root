��    y     �  U  �'       5  �   5  c   �5  	   6     (6     .6  �   :6  �   �6  �   �7  �   U8  !   9     @9  	   D9     N9     ^9     d9  "   i9     �9  #   �9  E   �9     :  #   :  !   C:  5   e:  I   �:  8   �:  $   ;  3   C;  &   w;  +   �;     �;     �;  *   �;  /   $<  ?   T<  3   �<  2   �<  7   �<  /   3=  9   c=     �=  7   �=  .   �=  F   ">  D   i>  E   �>  C   �>  C   8?  =   |?  <   �?  8   �?  I   0@  A   z@  #   �@  
   �@     �@     �@     A  B   )A     lA  R   �A  a   �A  G   >B  L   �B     �B     �B     �B     C     C  *   2C     ]C     pC     C  	   �C  "   �C     �C     �C  (   �C  .   �C  /   &D  V   VD  <   �D  9   �D  7   $E  !   \E  )   ~E  &   �E  +   �E  )   �E  9   %F  Z   _F     �F  �   �F  M   TG  ?   �G  <   �G  >   H  i   ^H  k   �H  ]   4I     �I  #   �I     �I     �I  �   �I  -   �J  C   �J  +   	K     5K  ?   HK     �K     �K  
   �K  "   �K     �K     �K     �K     L     L     !L  2   9L  6   lL  N   �L  M   �L  P   @M  Q   �M  	   �M  
   �M  #   �M  %   N     BN     UN  J   ZN     �N  )   �N     �N  r   �N     OO     \O  )   bO     �O  
   �O     �O     �O  �   �O     iP     �P  3   �P     �P     �P     �P     Q     )Q     ;Q     GQ     LQ  )   aQ     �Q     �Q  G   �Q     �Q     R  :   R  f   JR  S   �R     S     S     %S  	   *S  $   4S     YS     mS  
   �S  %   �S     �S     �S     �S  	   �S     �S  	   T     T  :   T  7   XT  :   �T     �T     �T     �T     �T     U     U  (   *U     SU     `U  "   nU     �U     �U     �U     �U     �U     �U     �U     �U     �U     V  
   V      V  /   -V  *   ]V  )   �V  9   �V     �V      W     W  [   W     |Y  �   }Z  �  ,[  �   �\  �   m]  �   E^  �   /_  �   `  �   �`    ka  �   �b  �  6c  �   �d  �   �e  �   �f  �   3g  �   �g  (  �h    
k  T  )m  K  ~o  �   �q  �   �r  ?  ks  �  �t  K  �v  ~  �w  o  mz  >  �{  V  }  h  s~  �   �  �   ��  �   f�  )  c�  m  ��  m  ��  o  i�  q  ه  m  K�  n  ��  r  (�  q  ��  �  �  �  ��  �  S�  �  �  �  w�  �  �  �  ��  �  2�  U  ɛ  M  �  .  m�  \  ��    ��  |  �  �   ��  �  k�  �   �  �  ��  �  .�    Ƭ    خ  �   �  V  ű  \  �  <  y�    ��  R  ��  Z  �  �   m�  �  D�  �  �  `  ӽ  �   4�  �   �  �   ��    }�  ,  ��  �   ��    ��    ��  ?  ��    ��  �   �  �   ��  `  ��  �   +�  �  ��  �  ��  �  f�  S  L�  �   ��  �   M�  I  ��  �   9�  �  ��  4  ��  Q  �  �  `�  U  ��  ,  @�  e  m�  H  ��  �   �    ��    �  E  �  �   `�    +�    4�  �  H�  �   E�    )�  3  A�    u�    ��  �  ��  j   `�  i   ��  i   5�  h   ��  I  �    R�  �  e�  �   3�    -�  �  =�  �   ��  �   ��  �   �  �   ` �   2        �   , �   � �   � N  � @  � F  .
 9  u J  � �   � �   � �   �   �   � �   � +  u   �   � �  � �  � f  ` ]  � 5  % D  [ �  � �  >! �  �" f   �$    �$    % @   =%    ~%    �% 8   �% P   �%    && '   ;& 1   c&    �&    �&    �&    �&    �& #   �& :   #' *   ^'    �' (   �' -   �' 0   �'    $(    6( Z   >( .   �(    �(    �(    )    !)    ?) /   X)    �)    �)    �)    �)    �)    �)    * '   0* *   X* 2   �* 5   �* )   �*    +    /+    I+    d+    +    �+    �+ [   �+ X   5, 7   �, ?   �, $   -    +- A   9- 9   {- J   �- 1    . J   2. H   }. i   �. >   0/ J   o/ D   �/ (   �/    (0 
   E0 
   P0 
   [0    f0 .   }0 3   �0 .   �0 3   1    C1    H1    T1    c1 L  1    �2    �2    �2    �2    3 #   3 !   83    Z3    g3    z3    �3    �3    �3    �3    �3    �3    �3    �3    4    4 $   4 
   ?4    J4    d4    s4 @   �4 A   �4 B   5 B   P5 A   �5 B   �5 A   6 A   Z6 D   �6 B   �6 E   $7    j7 *   {7 �   �7 0   O8 #   �8 K   �8     �8 B   9 �   T9    �9 0   : !   3: `   U:    �: 7   �: F   ;    U; �   \; �   �; p   �< �   �< m   �= �   �= o   �> �    ? l   �? a   �? N   `@ �   �@ ;   ]A 	   �A    �A A  �A `   C 9   dC 0   �C o   �C �   ?D �   E }   �E �   F �   �F Z   )G W   �G �   �G �   �H :   qI *   �I B   �I �   J {   �J �   lK �   L {   �L �   M }   �M �   -N {   �N �   YO }   P �   �P {   5Q �   �Q    _R �   �R    �S �   T {   �T �   ?U E   �U F   3V �   zV �  )W Q   �X O    Y �   pY U   iZ    �Z    �Z    �Z D   �Z 	   @[    J[    R[ 	   [[    e[    t[    }[    �[    �[    �[ E   �[ 0   \ +   L\ &   x\ 3   �\    �\ @   �\ F   3] ?   z] /   �] @   �] 5   +^ V   a^ H   �^ e   _    g_    t_ �   �_    F`    T` ?   n`    �` 0   �`    �`    �`    �`    a 	   a    a    a    -a    3a    ?a    Da 5   Za    �a    �a A   �a 6   �a    6b    9b    >b 
   Cb     Nb !   ob @   �b    �b �  �b �   hd �   <e    �e    �e    �e �   �e �   nf �   Xg �   Bh &   ,i    Si 	   Wi    ai    ti    {i    �i     �i .   �i F   �i    6j +   Kj .   wj 9   �j Y   �j A   :k ,   |k 6   �k ,   �k 1   l    ?l !   ]l /   l 3   �l I   �l 8   -m 7   fm 5   �m *   �m @   �m     @n 8   an ;   �n P   �n P   'o O   xo O   �o K   p =   dp 5   �p 5   �p F   q J   Uq )   �q    �q    �q #   �q $   r P   3r ,   �r f   �r j   s a   �s P   �s    6t    Qt    dt    wt    �t 2   �t    �t    �t    u    u %   )u    Ou    au 5   su A   �u F   �u ~   2v _   �v H   w D   Zw 8   �w B   �w ,   x *   Hx 4   sx F   �x g   �x    Wy �   qy O   z R   Wz S   �z U   �z o   T{ r   �{ �   7|    �| 1   �|    }    } �   $} J   �} i   5~ <   �~    �~ I   �~    ; "   D 
   g 1   r    �    �    �    �    �    � 2   � 7   N� U   �� `   ܀ V   =� W   ��    �    �� %   
�    0�    O� 
   a� {   l�    � 1   �     � |   '�    ��    �� +   ��    �    �    �    #� �   :� )   ބ (   � 4   1� %   f�    ��    ��    ��    ΅    �    �    �� *   �    >�    Q� `   d�    ņ    ن [   �� �   U� w   �    Y�    m�    v�    �� %   ��    ��    ˈ    �     �    $� !   :�    \�    k�    z�    ��    �� 6   �� ?   � J   -�    x�    ��    ��    �� 	   Ԋ 	   ފ 0   �    �    *� %   ;�    a�    i�    q�    ��    ��    ��    ��    ɋ    Ջ    �    �     � ?   � '   N� )   v� ?   ��    ��    �� !   � �  8� @  ڏ �   � �  �� �   ��   n� U  ��   �� t   �   d� H  g� �   �� )  p� �   ��   n� �   �� �   T� 9  6� t  p� k  � �  Q� �  	� 0  �� �   � g  � M  J� Z  ��   � �  
� �  �� _  $� o  �� �   �� �   ��   �� <  �� �  �� �  |� �  �� �  �� �  
� �  �� �  � �  �� �  � �  �� �  �� �  l� �  � �  �� �  o� �  � y  �� q  C� �  �� �  M� }  �� �  O�   � �  � �  �� �  �� �  [� P  � �  h� �   � l  �� z  e� W  �� 9  8� �  r� �   �   �� �  �� B  w� �  � �   P "  ) �   L x   �  �   I	 K  i
 B  � �  � �  � �    4   �  8 �   �   �   � D  � �  - �   � �    �  W �   � Z  �  �  	# �  �$ �  K& �  ( �  �) �  ^+ �  ,-   �. _  �/ �  01 �  �2   �4 l  �5 O  J7 �  �8 )  ; �  G< �  �= �  p? �  $A �  �B w   �D v   WE w   �E v   FF �  �F y  hI :  �K   N �  1O �  �Q *  �S $  �T �   �U   �V �   �W T  �X c  BZ �   �[   a\   |] �  �^ y  !` |  �a o  c �  �d   	f   g �   &h J  &i P  qj   �k e  �l C  ,n j  po 7  �p X  s �  lu �  w �  �x �  @z �  �{ B  �}   � �    � +   �� (   ҂ G   ��    C�    Z� N   f� f   ��    � "   -� 0   P� #   �� #   �� &   Ʉ "   ��    � .   � 7   K� .   ��     �� 8   Ӆ .   � 5   ;�    q�    �� j   �� @   �� -   6� /   d� )   �� +   �� $   � 7   �    G�     d�    ��    ��    ��    ܈ %   �� 1    � 4   R� B   �� E   ʉ 3   �    D�     d� !   �� !   �� #   Ɋ %   � $   � k   8� d   �� N   	� L   X� /   ��    Ռ O   � O   9� V   �� <   �� W   � U   u� �   ˎ A   N� W   �� T   � 8   =� &   v�    ��    ��    ��    ɐ 2   � 7   � 2   L� 7   �    �� 
   ��    ȑ "   ܑ �  ��    �� 
   ��    ��    ��    �� /   ѓ +   �    -�    <�    P�    `�    }�    ��    ��    ��    Ҕ    �    �    ��    � /   �    >� 8   K�    ��     �� P   �� Q   � R   W� R   �� Q   �� R   O� Q   �� Q   �� T   F� R   �� U   �    D� 9   T� �   �� A   ;� -   }� X   �� #   � U   (� �   ~� $   � 0   A� )   r� v   �� )   � L   =� G   �� 
   ҝ �   ݝ �   �� �   @� �   ß �   e� �   � �   �� �   � �   �� v   C� `   �� �   � ?   � 	   '�    1� E  Q� h   �� A    � 3   B� n   v� �   � �   � �   s� �   �� �   � n   � m   �� �   �� �   ڬ =   ŭ ?   � Y   C� �   �� �   �� �   � �   �� �   O� �   ױ �   �� �   � �   س �   a� �   � �   �� �   ^� �   � �   �� �   )� �   � �   q� �   -� �   �� K   n� L   �� �   � �  ޼ [   v� [   Ҿ   .� h   J�    ��    ��    �� P   ��    I�    X�    d�    m�    ��    ��    ��    ��    ��    �� Y   � 8   i� 8   �� /   �� ;   � $   G� C   l� n   �� m   � :   �� S   �� ?   � `   \� \   �� �   �    ��    �� �   ��    ��    �� `   ��    9� H   O�    ��    ��    ��    �� 	   ��    ��    ��    ��    ��    �� (   � >   ,�    k� (   �� :   �� D   ��    *�    -�    3�    9� )   F� *   p� U   ��    ��            ?   4  K     L   f  �   #   �   �   �  =       7  D  0   �   �   -           �               6                 s  �       B     #      �     |           �  �           .   (      @  �  j   �  �  p    '  Z  "          <      �   Z     e       �   �         d   �   �   -  �         �   �  )      I      8   �   �  �         �                 �  �  �   U       �   �        �    7  �      O             �  v  �      /       �  �                `       �          �   x  D   ,  9  �  �      R  �              �      X  k  l    �      �       �  w   
   u  �   r    �  ]  �  B       �    w  a   �       X            �   M     �  y   �  +  4      h  �  �      %       2       �   1  �  H   <  �          ;      �   m  �  �  �            K  �          n  f   �  �   1  �       �       �   i  U          m   �          e  �   �   y  W      �   �        �   �  f  �           �   c  k       W     D  z  �   �  %              �   H      �   �   �       A  5   �  j  2      �   .  �  �     �      O         �   I       %      �  �  �       0  S         c          �  /      \   �   �        W  o  .  �   S  C      �   N       Z             -      G   b    �  ~  ,       �   $          �      3              T   d  �   q         �      ^           F   i           d  �  ]         �  )  	  >  �   �  �          �   �      �  j  w  �  o   `      �  E   \  V  �  g  �      �      2  ~   *  L  +   J  g   o  �   Q       N      �       $  E      3        P    v   �        &  ]      |      b  �   8  �  �          =  �         �  z   c   �     5  V  �      �  }     �   O      1   �  L  	  �  �  N  p  �      �  #  �  �           B  �  �   �   �  H  �       �           �   I    '   @       �   h  
  
  �           "   6   [       J      �  �   )   l   �  �   &   a    G    �     P          >  S      �  Q      �   �   R   �   �  T          9   �   �       �   $       u       E  3  (   �   �  �       �  x  X  �  �    �                [  n   �           �     K         �   Y  �   �  Y      P      x   �   t       �  >   �   �          �   r       @      :   A      �   �   �  �       t  �   '  �   _      {    t      �  �               �       �       �   l       �  h               �  �      �   6  �       �   Q  &        s   �              (  �  *  �      �    �  �   !          V   <   y  8  \  �      �  5  �  �    �   ^        �   �  �                   !   p      �   m      �  �  r  i      �   �      �    :  ?  "  9        �         �  �   �  �  e  7   0          �  ;   �   C  �  :  �   Y   M   �  q               s  �               R  �   F  A       �           M  ,  `  �   	   =  �   a  �   �   ^  �        �       �       {   v  �      �      �  �  �       g  �   �   �               �   �   �         J               �      F              !  U  [  �      +  �          �  �       *       �  C   q      �                  �  �               T  ?  �   _       u  k  �   �          }  �               4   /  b   ;  �   _  G         n   

Warning: Some tests could cause your system to freeze or become unresponsive. Please save all your work and close all other running applications before beginning the testing process.    Determine if we need to run tests specific to portable computers that may not apply to desktops.  Results   Run   Selection   Takes multiple pictures based on the resolutions supported by the camera and
 validates their size and that they are of a valid format.  Test that the system's wireless hardware can connect to a router using the
 802.11a protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11a protocol.  Test that the system's wireless hardware can connect to a router using the
 802.11b protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11b protocol.  Test that the system's wireless hardware can connect to a router using the
 802.11g protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11g protocol. %(key_name)s key has been pressed &No &Previous &Skip this test &Test &Yes 10 tests completed out of 30 (30%) Abort signal from abort(3) All required keys have been tested! Archives the piglit-summary directory into the piglit-results.tar.gz. Are you sure? Attach log from fwts wakealarm test Attach log from rendercheck tests Attaches a copy of /var/log/dmesg to the test results Attaches a dump of the udev database showing system hardware information. Attaches a list of the currently running kernel modules. Attaches a report of CPU information Attaches a report of installed codecs for Intel HDA Attaches a report of sysfs attributes. Attaches a tarball of gcov data if present. Attaches dmidecode output Attaches info on DMI Attaches information about disk partitions Attaches the FWTS results log to the submission Attaches the audio hardware data collection log to the results. Attaches the bootchart log for bootchart test runs. Attaches the bootchart png file for bootchart runs Attaches the contents of /proc/acpi/sleep if it exists. Attaches the contents of the /etc/modules file. Attaches the contents of the various modprobe conf files. Attaches the firmware version Attaches the graphics stress results to the submission. Attaches the installer debug log if it exists. Attaches the log from the 250 cycle Hibernate/Resume test if it exists Attaches the log from the 250 cycle Suspend/Resume test if it exists Attaches the log from the 30 cycle Hibernate/Resume test if it exists Attaches the log from the 30 cycle Suspend/Resume test if it exists Attaches the log from the single suspend/resume test to the results Attaches the log generated by cpu/scaling_test to the results Attaches the output of udev_resource, for debugging purposes Attaches the screenshot captured in graphics/screenshot. Attaches the screenshot captured in graphics/screenshot_fullscreen_video. Attaches very verbose lspci output (with central database Query). Attaches very verbose lspci output. Audio Test Audio tests Automated CD write test Automated DVD write test. Automated check of the hibernate log for errors discovered by fwts Automated optical read test. Automated test case to make sure that it's possible to download files through HTTP Automated test case to verify availability of some system on the network using ICMP ECHO packets. Automated test to store bluetooth device information in checkbox report Automated test to walk multiple network cards and test each one in sequence. Benchmark for each disk Benchmarks tests Bluetooth Test Bluetooth tests Bootchart information. Broken pipe: write to pipe with no readers Building report... CD write test. CPU Test CPU tests CPU utilization on an idle system. Camera Test Camera tests Check failed result from shell test case Check job is executed when dependency succeeds Check job is executed when requirements are met Check job result is set to "not required on this system" when requirements are not met Check job result is set to uninitiated when dependency fails Check logs for the stress poweroff (100 cycles) test case Check logs for the stress reboot (100 cycles) test case Check stats changes for each disk Check success result from shell test case Check that VESA drivers are not in use Check that hardware is able to run Unity 3D Check that hardware is able to run compiz Check the time needed to reconnect to a WIFI access point Check to see if CONFIG_NO_HZ is set in the kernel (this is just a simple regression check) Checkbox System Testing Checkbox did not finish completely.
Do you want to rerun the last test,
continue to the next test, or
restart from the beginning? Checks that a specified sources list file contains the requested repositories Checks the battery drain during idle.  Reports time until empty Checks the battery drain during suspend.  Reports time until Checks the battery drain while watching a movie.  Reports time Checks the length of time it takes to reconnect an existing wifi connection after a suspend/resume cycle. Checks the length of time it takes to reconnect an existing wired connection
 after a suspend/resume cycle. Checks the sleep times to ensure that a machine suspends and resumes within a given threshold Child stopped or terminated Choose tests to run on your system: Codec tests Collapse All Collect audio-related system information. This data can be used to simulate this computer's audio subsystem and perform more detailed tests under a controlled environment. Collect info on color depth and pixel format. Collect info on graphics modes (screen resolution and refresh rate) Combine with character above to expand node Command not found. Command received signal %(signal_name)s: %(signal_description)s Comments Common Document Types Test Components Configuration override parameters. Continue Continue if stopped DVD write test. Deselect All Deselect all Detailed information... Detects and displays disks attached to the system. Detects and shows USB devices attached to this system. Determine whether the screen is detected as a multitouch device automatically. Determine whether the screen is detected as a non-touch device automatically. Determine whether the touchpad is detected as a multitouch device automatically. Determine whether the touchpad is detected as a singletouch device automatically. Disk Test Disk tests Disk utilization on an idle system. Do you really want to skip this test? Don't ask me again Done Dumps memory info to a file for comparison after suspend test has been run Email Email address must be in a proper format. Email: Ensure the current resolution meets or exceeds the recommended minimum resolution (800x600). See here for details: Enter text:
 Error Exchanging information with the server... Executing %(test_name)s Expand All ExpressCard Test ExpressCard tests Failed to contact server. Please try
again or upload the following file name:
%s

directly to the system database:
https://launchpad.net/+hwdb/+submit Failed to open file '%s': %s Failed to process form: %s Failed to upload to server,
please try again later. Fingerprint reader tests Firewire Test Firewire disk tests Floating point exception Floppy disk tests Floppy test Form Further information: Gathering information from your system... Graphics Test Graphics tests Hangup detected on controlling terminal or death of controlling process Hibernation tests Hotkey tests I will exit automatically once all keys have been pressed. If a key is not present in your keyboard, press the 'Skip' button below it to remove it from the test. If your keyboard lacks one or more keys, press its number to skip testing that key. Illegal Instruction In Progress Info Info Test Information not posted to Launchpad. Informational tests Input Devices tests Input Test Internet connection fully established Interrupt from keyboard Invalid memory reference Key test Keys Test Kill signal LED tests List USB devices Lists the device driver and version for all audio devices. Make sure that the RTC (Real-Time Clock) device exists. Maximum disk space used during a default installation test Media Card Test Media Card tests Memory Test Memory tests Miscellanea Test Miscellaneous tests Missing configuration file as argument.
 Monitor Test Monitor tests Move a 3D window around the screen Ne&xt Ne_xt Network Information Networking Test Networking tests Next No Internet connection Not Resolved Not Started Not Supported Not Tested Not required One of debug, info, warning, error or critical. Open and close 4 3D windows multiple times Open and close a 3D window multiple times Open, suspend resume and close a 3D window multiple times Optical Drive tests Optical Test Optical read test. PURPOSE:
     Check that external line out connection works correctly
STEPS:
     1. Insert cable to speakers (with built-in amplifiers) on the line out port
     2. Open system sound preferences, 'Output' tab, select 'Line-out' on the connector list. Click the Test button
     3. On the system sound preferences, select 'Internal Audio' on the device list and click 'Test Speakers' to check left and right channel
VERIFICATION:
     1. Do you hear a sound in the speakers? The internal speakers should *not* be muted automatically
     2. Do you hear the sound coming out on the corresponding channel? PURPOSE:
    Block cap keys LED verification
STEPS:
    1. Press "Block Cap Keys" to activate/deactivate cap keys blocking
    2. Cap Keys LED should be switched on/off every time the key is pressed
VERIFICATION:
    Did the Cap Keys LED light as expected? PURPOSE:
    Camera LED verification
STEPS:
    1. Select Test to activate camera
    2. Camera LED should light for a few seconds
VERIFICATION:
    Did the camera LED light? PURPOSE:
    Check that external line in connection works correctly
STEPS:
    1. Use a cable to connect the line in port to an external line out source.
    2. Open system sound preferences, 'Input' tab, select 'Line-in' on the connector list. Click the Test button
    3. After a few seconds, your recording will be played back to you.
VERIFICATION:
    Did you hear your recording? PURPOSE:
    Check that the various audio channels are working properly
STEPS:
    1. Click the Test button
VERIFICATION:
    You should clearly hear a voice from the different audio channels PURPOSE:
    Check touchscreen drag & drop
STEPS:
    1. Double tap, hold, and drag an object on the desktop
    2. Drop the object in a different location
VERIFICATION:
    Does the object select and drag and drop? PURPOSE:
    Check touchscreen pinch gesture for zoom
STEPS:
    1. Place two fingers on the screen and pinch them together
    2. Place two fingers on the screen and move then apart
VERIFICATION:
    Does the screen zoom in and out? PURPOSE:
    Check touchscreen tap recognition
STEPS:
    1. Tap an object on the screen with finger. The cursor should jump to location tapped and object should highlight
VERIFICATION:
    Does tap recognition work? PURPOSE:
    Create jobs that use the CPU as much as possible for two hours. The test is considered passed if the system does not freeze. PURPOSE:
    HDD LED verification
STEPS:
    1. Select "Test" to write and read a temporary file for a few seconds
    2. HDD LED should light when writing to/reading from HDD
VERIFICATION:
    Did the HDD LED light? PURPOSE:
    Keep tester related information in the report
STEPS:
    1. Tester Information
    2. Please enter the following information in the comments field:
       a. Name
       b. Email Address
       c. Reason for this test run
VERIFICATION:
    Nothing to verify for this test PURPOSE:
    Manual detection of accelerometer.
STEPS:
    1. Look at the specifications for your system.
VERIFICATION:
    Is this system supposed to have an accelerometer? PURPOSE:
    Numeric keypad LED verification
STEPS:
    1. Press "Block Num" key to toggle numeric keypad LED
    2. Click on the "Test" button to open a window to verify your typing
    3. Type using the numeric keypad both when the LED is on and off
VERIFICATION:
    1. Numeric keypad LED status should toggle everytime the "Block Num" key is pressed
    2. Numbers should only be entered in the keyboard verification window when the LED is on PURPOSE:
    Power LED verification
STEPS:
    1. Power LED should be on while device is switched on
VERIFICATION:
    Does the power LED light as expected? PURPOSE:
    Power LED verification
STEPS:
    1. The Power LED should blink or change color while the system is suspended
VERIFICATION:
    Did the Power LED blink or change color while the system was suspended for the previous suspend test? PURPOSE:
    Take a screengrab of the current screen (logged on Unity desktop)
STEPS:
    1. Take picture using USB webcam
VERIFICATION:
    Review attachment manually later PURPOSE:
    Take a screengrab of the current screen after suspend (logged on Unity desktop)
STEPS:
    1. Take picture using USB webcam
VERIFICATION:
    Review attachment manually later PURPOSE:
    Take a screengrab of the current screen during fullscreen video playback
STEPS:
    1. Start a fullscreen video playback
    2. Take picture using USB webcam after a few seconds
VERIFICATION:
    Review attachment manually later PURPOSE:
    Tests that the systems wireless hardware can connect to a router using
    no security and the 802.11b/g protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the B and G wireless bands
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use no security
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using
    no security and the 802.11n protocol.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the N wireless band
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use no security
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using WPA
    security and the 802.11b/g protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the B and G wireless bands
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use WPA2 and ensure the PSK matches that set in ROUTER_PSK
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using WPA
    security and the 802.11n protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the N wireless band
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use WPA2 and ensure the PSK matches that set in ROUTER_PSK
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    This test checks that the manual plugin works fine
STEPS:
    1. Add a comment
    2. Set the result as passed
VERIFICATION:
    Check that in the report the result is passed and the comment is displayed PURPOSE:
    This test cycles through the detected video modes
STEPS:
    1. Click "Test" to start cycling through the video modes
VERIFICATION:
    Did the screen appear to be working for each mode? PURPOSE:
    This test tests the basic 3D capabilities of your video card
STEPS:
    1. Click "Test" to execute an OpenGL demo. Press ESC at any time to close.
    2. Verify that the animation is not jerky or slow.
VERIFICATION:
    1. Did the 3d animation appear?
    2. Was the animation free from slowness/jerkiness? PURPOSE:
    This test will check that a DSL modem can be configured and connected.
STEPS:
    1. Connect the telephone line to the computer
    2. Click on the Network icon on the top panel.
    3. Select "Edit Connections"
    4. Select the "DSL" tab
    5. Click on "Add" button
    6. Configure the connection parameters properly
    7. Click "Test" to verify that it's possible to establish an HTTP connection
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will check that a USB audio device works correctly
STEPS:
    1. Connect a USB audio device to your system
    2. Click "Test", then speak into the microphone
    3. After a few seconds, your speech will be played back to you
VERIFICATION:
    Did you hear your speech played back through the USB headphones? PURPOSE:
    This test will check that bluetooth connection works correctly
STEPS:
    1. Enable bluetooth on any mobile device (PDA, smartphone, etc.)
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. In the device write the PIN code automatically chosen by the wizard
    6. The device should pair with the computer
    7. Right-click on the bluetooth icon and select browse files
    8. Authorize the computer to browse the files in the device if needed
    9. You should be able to browse the files
VERIFICATION:
    Did all the steps work? PURPOSE:
    This test will check that headphones connector works correctly
STEPS:
    1. Connect a pair of headphones to your audio device
    2. Click the Test button to play a sound to your audio device
VERIFICATION:
    Did you hear a sound through the headphones and did the sound play without any distortion, clicks or other strange noises from your headphones? PURPOSE:
    This test will check that internal speakers work correctly
STEPS:
    1. Make sure that no external speakers or headphones are connected
       If testing a desktop, external speakers are allowed
    2. Click the Test button to play a brief tone on your audio device
VERIFICATION:
    Did you hear a tone? PURPOSE:
    This test will check that recording sound using an external microphone works correctly
STEPS:
    1. Connect a microphone to your microphone port
    2. Click "Test", then speak into the external microphone
    3. After a few seconds, your speech will be played back to you
VERIFICATION:
    Did you hear your speech played back? PURPOSE:
    This test will check that recording sound using the onboard microphone works correctly
STEPS:
    1. Disconnect any external microphones that you have plugged in
    2. Click "Test", then speak into your internal microphone
    3. After a few seconds, your speech will be played back to you.
VERIFICATION:
    Did you hear your speech played back? PURPOSE:
    This test will check that the built-in camera works
STEPS:
    1. Click on Test to display a still image from the camera for ten seconds.
VERIFICATION:
    Did you see the image? PURPOSE:
    This test will check that the built-in camera works
STEPS:
    1. Click on Test to display a video capture from the camera for ten seconds.
VERIFICATION:
    Did you see the video capture? PURPOSE:
    This test will check that the display is correct after suspend and resume
STEPS:
    1. Check that your display does not show up visual artifacts after resuming.
VERIFICATION:
    Does the display work normally after resuming from suspend? PURPOSE:
    This test will check that the system can switch to a virtual terminal and back to X
STEPS:
    1. Click "Test" to switch to another virtual terminal and then back to X
VERIFICATION:
    Did your screen change temporarily to a text console and then switch back to your current session? PURPOSE:
    This test will check that the system correctly detects
    the removal of a CF card from the systems card reader.
STEPS:
    1. Click "Test" and remove the CF card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a MS card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MS card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a MSP card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MSP card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a SDXC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SDXC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a xD card from the systems card reader.
STEPS:
    1. Click "Test" and remove the xD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of an SD card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of an SDHC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SDHC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of the MMC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MMC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the insertion of
    a USB 3.0 storage device
STEPS:
    1. Click "Test" and insert a USB 3.0 storage device (pen-drive/HDD) in
       a USB 3.0 port. (Note: this test will time-out after 20 seconds.)
    2. Do not unplug the device after the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the insertion of
    a USB storage device
STEPS:
    1. Click "Test" and insert a USB storage device (pen-drive/HDD).
       (Note: this test will time-out after 20 seconds.)
    2. Do not unplug the device after the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a CF card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the CF card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a MS card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MS card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a MSP card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MSP card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a xD card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the xD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of an MMC card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MMC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of an SDHC card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the SDHC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal of
    a USB 3.0 storage device
STEPS:
    1. Click "Test" and remove the USB 3.0 device.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal of
    a USB storage device
STEPS:
    1. Click "Test" and remove the USB device.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that you can record and hear audio using a bluetooth audio device
STEPS:
    1. Enable the bluetooth headset
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. In the device write the PIN code automatically chosen by the wizard
    6. The device should pair with the computer
    7. Click "Test" to record for five seconds and reproduce in the bluetooth device
VERIFICATION:
    Did you hear the sound you recorded in the bluetooth PURPOSE:
    This test will check that you can transfer information through a bluetooth connection
STEPS:
    1. Make sure that you're able to browse the files in your mobile device
    2. Copy a file from the computer to the mobile device
    3. Copy a file from the mobile device to the computer
VERIFICATION:
    Were all files copied correctly? PURPOSE:
    This test will check that you can use a BlueTooth HID device
STEPS:
    1. Enable either a BT mouse or keyboard
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. For mice, perform actions such as moving the pointer, right and left button clicks and double clicks
    6. For keyboards, click the Test button to lauch a small tool. Enter some text into the tool and close it.
VERIFICATION:
    Did the device work as expected? PURPOSE:
    This test will check that you can use a USB HID device
STEPS:
    1. Enable either a USB mouse or keyboard
    2. For mice, perform actions such as moving the pointer, right and left button clicks and double clicks
    3. For keyboards, click the Test button to lauch a small tool. Type some text and close the tool.
VERIFICATION:
    Did the device work as expected? PURPOSE:
    This test will check that your system detects USB storage devices.
STEPS:
    1. Plug in one or more USB keys or hard drives.
    2. Click on "Test".
INFO:
    $output
VERIFICATION:
    Were the drives detected? PURPOSE:
    This test will check the system can detect the insertion of a FireWire HDD
STEPS:
    1. Click 'Test' to begin the test. This test will
       timeout and fail if the insertion has not been detected within 20 seconds.
    2. Plug a FireWire HDD into an available FireWire port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the insertion of an eSATA HDD
STEPS:
    1. Click 'Test' to begin the test. This test will
       timeout and fail if the insertion has not been detected within 20 seconds.
    2. Plug an eSATA HDD into an available eSATA port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the removal of a FireWire HDD
STEPS:
    1. Click 'Test' to begin the test. This test will timeout and fail if
       the removal has not been detected within 20 seconds.
    2. Remove the previously attached FireWire HDD from the FireWire port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the removal of an eSATA HDD
STEPS:
    1. Click 'Test' to begin the test. This test will timeout and fail if
       the removal has not been detected within 20 seconds.
    2. Remove the previously attached eSATA HDD from the eSATA port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check to make sure your system can successfully hibernate (if supported)
STEPS:
    1. Click on Test
    2. The system will hibernate and should wake itself within 5 minutes
    3. If your system does not wake itself after 5 minutes, please press the power button to wake the system manually
    4. If the system fails to resume from hibernate, please restart System Testing and mark this test as Failed
VERIFICATION:
    Did the system successfully hibernate and did it work properly after waking up? PURPOSE:
    This test will check your CD audio playback capabilities
STEPS:
    1. Insert an audio CD in your optical drive
    2. When prompted, launch the Music Player
    3. Locate the CD in the display of the Music Player
    4. Select the CD in the Music Player
    5. Click the Play button to listen to the music on the CD
    6. Stop playing after some time
    7. Right click on the CD icon and select "Eject Disc"
    8. The CD should be ejected
    9. Close the Music Player
VERIFICATION:
    Did all the steps work? PURPOSE:
    This test will check your DVD  playback capabilities
STEPS:
    1. Insert a DVD that contains any movie in your optical drive
    2. Click "Test" to play the DVD in Totem
VERIFICATION:
    Did the file play? PURPOSE:
    This test will check your USB 3.0 connection.
STEPS:
    1. Plug a USB 3.0 HDD or thumbdrive into a USB 3.0 port in the computer.
    2. An icon should appear on the Launcher.
    3. Click "Test" to begin the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check your USB connection.
STEPS:
    1. Connect a USB storage device to an external USB slot on this computer.
    2. An icon should appear on the Launcher.
    3. Confirm that the icon appears.
    4. Eject the device.
    5. Repeat with each external USB slot.
VERIFICATION:
    Do all USB slots work with the device? PURPOSE:
    This test will check your USB connection.
STEPS:
    1. Plug a USB HDD or thumbdrive into the computer.
    2. An icon should appear on the Launcher.
    3. Click "Test" to begin the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check your monitor power saving capabilities
STEPS:
    1. Click "Test" to try the power saving capabilities of your monitor
    2. Press any key or move the mouse to recover
VERIFICATION:
    Did the monitor go blank and turn on again? PURPOSE:
    This test will check your wired connection
STEPS:
    1. Click on the Network icon in the top panel
    2. Select a network below the "Wired network" section
    3. Click "Test" to verify that it's possible to establish a HTTP connection
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will check your wireless connection.
STEPS:
    1. Click on the Network icon in the panel.
    2. Select a network below the 'Wireless networks' section.
    3. Click "Test" to verify that it's possible to establish an HTTP connection.
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will cycle through the detected display modes
STEPS:
    1. Click "Test" and the display will cycle trough the display modes
VERIFICATION:
    Did your display look fine in the detected mode? PURPOSE:
    This test will send the image 'JPEG_Color_Image_Ubuntu.jpg' to a specified device
STEPS:
    1. Click "Test" and you will be prompted to enter the Bluetooth device name of a device that can accept file transfers (It may take a few moments after entering the name for the file to begin sending)
    2. Accept any prompts that appear on both devices
VERIFICATION:
    Was the data correctly transferred? PURPOSE:
    This test will send the image 'JPEG_Color_Image_Ubuntu.jpg' to a specified device
STEPS:
    1. Make sure Bluetooth is enabled by checking the Bluetooth indicator applet
    2. Click "Test" and you will be prompted to enter the Bluetooth device name of a device that can accept file transfers (It may take a few moments after entering the name for the file to begin sending)
    3. Accept any prompts that appear on both devices
VERIFICATION:
    Was the data correctly transferred? PURPOSE:
    This test will test display rotation
STEPS:
    1. Click "Test" to test display rotation. The display will be rotated every 4 seconds.
    2. Check if all rotations (normal right inverted left) took place without permanent screen corruption
VERIFICATION:
    Did the display rotation take place without without permanent screen corruption? PURPOSE:
    This test will test the brightness key
STEPS:
    1. Press the brightness buttons on the keyboard
VERIFICATION:
    Did the brightness change following to your key presses? PURPOSE:
    This test will test the brightness key after resuming from suspend
STEPS:
    1. Press the brightness buttons on the keyboard
VERIFICATION:
    Did the brightness change following to your key presses after resuming from suspend? PURPOSE:
    This test will test the default display
STEPS:
    1. Click "Test" to display a video test.
VERIFICATION:
    Do you see color bars and static? PURPOSE:
    This test will test the mute key of your keyboard
STEPS:
    1. Click test to open a window on which to test the mute key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Does the mute key work as expected? PURPOSE:
    This test will test the mute key of your keyboard after resuming from suspend
STEPS:
    1. Click test to open a window on which to test the mute key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Did the volume mute following your key presses? PURPOSE:
    This test will test the sleep key
STEPS:
    1. Press the sleep key on the keyboard
    2. Wake your system up by pressing the power button
VERIFICATION:
    Did the system go to sleep after pressing the sleep key? PURPOSE:
    This test will test the sleep key after resuming from suspend
STEPS:
    1. Press the sleep key on the keyboard
    2. Wake your system up by pressing the power button
VERIFICATION:
    Did the system go to sleep after pressing the sleep key after resuming from suspend? PURPOSE:
    This test will test the super key of your keyboard
STEPS:
    1. Click test to open a window on which to test the super key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Does the super key work as expected? PURPOSE:
    This test will test the super key of your keyboard after resuming from suspend
STEPS:
    1. Click test to open a window on which to test the super key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Does the super key work as expected after resuming from suspend? PURPOSE:
    This test will test the wireless key after resuming from suspend
STEPS:
    1. Press the wireless key on the keyboard
    2. Press the same key again
VERIFICATION:
    Did the wireless go off on the first press and on again on the second after resuming from suspend? PURPOSE:
    This test will test your keyboard
STEPS:
    1. Click on Test
    2. On the open text area, use your keyboard to type something
VERIFICATION:
    Is your keyboard working properly? PURPOSE:
    This test will test your pointing device
STEPS:
    1. Move the cursor using the pointing device or touch the screen.
    2. Perform some single/double/right click operations.
VERIFICATION:
    Did the pointing device work as expected? PURPOSE:
    This test will verify that the GUI is usable after manually changing resolution
STEPS:
    1. Open the Displays application
    2. Select a new resolution from the dropdown list
    3. Click on Apply
    4. Select the original resolution from the dropdown list
    5. Click on Apply
VERIFICATION:
    Did the resolution change as expected? PURPOSE:
    This test will verify the default display resolution
STEPS:
    1. This display is using the following resolution:
INFO:
    $output
VERIFICATION:
    Is this acceptable for your display? PURPOSE:
    To make sure that stressing the wifi hotkey does not cause applets to disappear from the panel or the system to lock up
STEPS:
    1. Log in to desktop
    2. Press wifi hotkey at a rate of 1 press per second and slowly increase the speed of the tap, until you are tapping as fast as possible
VERIFICATION:
    Verify the system is not frozen and the wifi and bluetooth applets are still visible and functional PURPOSE:
    Touchpad LED verification
STEPS:
    1. Click on the touchpad button or press key combination to enable/disable touchpad button
    2. Slide your finger on the touchpad
VERIFICATION:
    1. Touchpad LED status should toggle everytime the button is clicked or the key combination is pressed
    2. When the LED is on, the mouse pointer should move on touchpad usage
    3. When the LED is off, the mouse pointer should not move on touchpad usage PURPOSE:
    Touchpad LED verification after resuming from suspend
STEPS:
    1. Click on the touchpad button or press key combination to enable/disable touchpad button
    2. Slide your finger on the touchpad
VERIFICATION:
    1. Touchpad LED status should toggle everytime the button is clicked or the key combination is pressed
    2. When the LED is on, the mouse pointer should move on touchpad usage
    3. When the LED is off, the mouse pointer should not move on touchpad usage PURPOSE:
    Touchpad horizontal scroll verification
STEPS:
    1. Select "Test" when ready and place your cursor within the borders of the displayed test window.
    2. Verify that you can move the horizontal slider by moving your finger right and left in the lower part of the touchpad.
VERIFICATION:
    Could you scroll right and left? PURPOSE:
    Touchpad manual detection of multitouch.
STEPS:
    1. Look at the specifications for your system.
VERIFICATION:
    Is the touchpad supposed to be multitouch? PURPOSE:
    Touchpad user-verify
STEPS:
    1. Make sure that touchpad is enabled.
    2. Move cursor using the touchpad.
VERIFICATION:
    Did the cursor move? PURPOSE:
    Touchpad vertical scroll verification
STEPS:
    1. Select "Test" when ready and place your cursor within the borders of the displayed test window.
    2. Verify that you can move the vertical slider by moving your finger up and down in the right part of the touchpad.
VERIFICATION:
    Could you scroll up and down? PURPOSE:
    Touchscreen manual detection of multitouch.
STEPS:
    1. Look at the specifications for your system.
VERIFICATION:
    Is the screen supposed to be multitouch? PURPOSE:
    Validate Wireless (WLAN + Bluetooth) LED operated the same after resuming from suspend
STEPS:
    1. Make sure WLAN connection is established and Bluetooth is enabled.
    2. WLAN/Bluetooth LED should light
    3. Switch WLAN and Bluetooth off from a hardware switch (if present)
    4. Switch them back on
    5. Switch WLAN and Bluetooth off from the panel applet
    6. Switch them back on
VERIFICATION:
    Did the WLAN/Bluetooth LED light as expected after resuming from suspend? PURPOSE:
    Validate that WLAN LED shuts off when disabled after resuming from suspend
STEPS:
    1. Connect to AP
    2. Use Physical switch to disable WLAN
    3. Re-enable
    4. Use Network-Manager to disable WLAN
VERIFICATION:
    Did the LED turn off then WLAN is disabled after resuming from suspend? PURPOSE:
    Validate that the Bluetooth LED turns on and off when BT is enabled/disabled
STEPS:
    1. Switch bluetooth off from a hardware switch (if present)
    2. Switch bluetooth back on
    3. Switch bluetooth off from the panel applet
    4. Switch bluetooth back on
VERIFICATION:
    Did the bluetooth LED turn off and on twice? PURPOSE:
    Validate that the Bluetooth LED turns on and off when BT is enabled/disabled after resuming from suspend
STEPS:
    1. Switch bluetooth off from a hardware switch (if present)
    2. Switch bluetooth back on
    3. Switch bluetooth off from the panel applet
    4. Switch bluetooth back on
VERIFICATION:
    Did the bluetooth LED turn off and on twice after resuming from suspend? PURPOSE:
    Validate that the Caps Lock key operates the same before and after resuming from suspend
STEPS:
    1. Press "Block Cap Keys" to activate/deactivate cap keys blocking
    2. Cap Keys LED should be switched on/off every time the key is pressed
VERIFICATION:
    Did the Cap Keys LED light as expected after resuming from suspend? PURPOSE:
    Validate that the External Video hot key is working as expected
STEPS:
    1. Plug in an external monitor
    2. Press the display hot key to change the monitors configuration
VERIFICATION:
    Check that the video signal can be mirrored, extended, displayed on external or onboard only. PURPOSE:
    Validate that the External Video hot key is working as expected after resuming from suspend
STEPS:
    1. Plug in an external monitor
    2. Press the display hot key to change the monitors configuration
VERIFICATION:
    Check that the video signal can be mirrored, extended, displayed on external or onboard only, after resuming from suspend. PURPOSE:
    Validate that the HDD LED still operates as expected after resuming from suspend
STEPS:
    1. Select "Test" to write and read a temporary file for a few seconds
    2. HDD LED should blink when writing to/reading from HDD
VERIFICATION:
    Did the HDD LED still blink with HDD activity after resuming from suspend? PURPOSE:
    Validate that the battery LED indicated low power
STEPS:
    1. Let system run on battery for several hours
    2. Monitor battery LED carefully
VERIFICATION:
    Does the LED light orange when battery is low? PURPOSE:
    Validate that the battery LED indicated low power after resuming from suspend
STEPS:
    1. Let system run on battery for several hours
    2. Monitor battery LED carefully
VERIFICATION:
    Does the LED light orange when battery is low after resuming from suspend? PURPOSE:
    Validate that the battery LED properly displays charged status
STEPS:
    1. Let system run on battery for a short time
    2. Plug in AC
    3. Let system run on AC
VERIFICATION:
    Does the orange battery LED shut off when system is fully charged? PURPOSE:
    Validate that the battery LED properly displays charged status after resuming from suspend
STEPS:
    1. Let system run on battery for a short time
    2. Plug in AC
    3. Let system run on AC
VERIFICATION:
    Does the orange battery LED still shut off when system is fully charged after resuming from suspend? PURPOSE:
    Validate that the battery light shows charging status
STEPS:
    1. Let system run on battery for a while
    2. Plug in AC plug
VERIFICATION:
    Did the battery indicator LED turn orange? PURPOSE:
    Validate that the battery light shows charging status after resuming from suspend
STEPS:
    1. Let system run on battery for a while
    2. Plug in AC plug
VERIFICATION:
    Did the battery indicator LED still turn orange after resuming from suspend? PURPOSE:
    Validate that the camera LED still works as expected after resuming from suspend
STEPS:
    1. Select Test to activate camera
    2. Camera LED should light for a few seconds
VERIFICATION:
    Did the camera LED still turn on and off after resuming from suspend? PURPOSE:
    Validate that the numeric keypad LED operates the same before and after resuming from suspend
STEPS:
    1. Press "Block Num" key to toggle numeric keypad LED
    2. Click on the "Test" button to open a window to verify your typing
    3. Type using the numeric keypad both when the LED is on and off
VERIFICATION:
    1. Numeric keypad LED status should toggle everytime the "Block Num" key is pressed
    2. Numbers should only be entered in the keyboard verification window when the LED is on PURPOSE:
    Validate that the power LED operated the same after resuming from suspend
STEPS:
    1. Power LED should be on while device is switched on
VERIFICATION:
    Does the power LED remain on after resuming from suspend? PURPOSE:
    Verify touchpad hotkey toggles touchpad functionality on and off
STEPS:
    1. Verify the touchpad is functional
    2. Tap the touchpad toggle hotkey
    3. Tap the touchpad toggle hotkey again
VERIFICATION:
    Verify the touchpad has been disabled and re-enabled. PURPOSE:
    Verify touchpad hotkey toggles touchpad functionality on and off after resuming from suspend
STEPS:
    1. Verify the touchpad is functional
    2. Tap the touchpad toggle hotkey
    3. Tap the touchpad toggle hotkey again
VERIFICATION:
    Verify the touchpad has been disabled and re-enabled. PURPOSE:
    Wake up by USB keyboard
STEPS:
    1. Enable "Wake by USB KB/Mouse" item in BIOS
    2. Press "Test" to enter suspend (S3) mode
    3. Press any key of USB keyboard to wake system up
VERIFICATION:
    Did the system wake up from suspend mode when you pressed a keyboard key? PURPOSE:
    Wake up by USB mouse
STEPS:
    1. Enable "Wake by USB KB/Mouse" item in BIOS
    2. Press "Test" to enter suspend (S3) mode
    3. Press any button of USB mouse to wake system up
VERIFICATION:
    Did the system wake up from suspend mode when you pressed the mouse button? PURPOSE:
    Wireless (WLAN + Bluetooth) LED verification
STEPS:
    1. Make sure WLAN connection is established and Bluetooth is enabled.
    2. WLAN/Bluetooth LED should light
    3. Switch WLAN and Bluetooth off from a hardware switch (if present)
    4. Switch them back on
    5. Switch WLAN and Bluetooth off from the panel applet
    6. Switch them back on
VERIFICATION:
    Did the WLAN/Bluetooth LED light as expected? PURPOSE:
   This is an automated stress test that will force the system to hibernate/resume for 250 cycles PURPOSE:
   This is an automated stress test that will force the system to hibernate/resume for 30 cycles PURPOSE:
   This is an automated stress test that will force the system to suspend/resume for 250 cycles. PURPOSE:
   This is an automated stress test that will force the system to suspend/resume for 30 cycles. PURPOSE:
   This test will verify that a USB DSL or Mobile Broadband modem works
STEPS:
   1. Connect the USB cable to the computer
   2. Right click on the Network icon in the panel
   3. Select 'Edit Connections'
   4. Select the 'DSL' (for ADSL modem) or 'Mobile Broadband' (for 3G modem) tab
   5. Click on 'Add' button
   6. Configure the connection parameters properly
   7. Notify OSD should confirm that the connection has been established
   8. Select Test to verify that it's possible to establish an HTTP connection
VERIFICATION:
   Was the connection correctly established? PURPOSE:
   This test will verify that a fingerprint reader can be used to unlock a locked system.
STEPS:
   1. Click on the Session indicator (Cog icon on the Left side of the panel) .
   2. Select 'Lock screen'.
   3. Press any key or move the mouse.
   4. A window should appear that provides the ability to unlock either typing your password or using fingerprint authentication.
   5. Use the fingerprint reader to unlock.
   6. Your screen should be unlocked.
VERIFICATION:
   Did the authentication procedure work correctly? PURPOSE:
   This test will verify that a network printer is usable
STEPS:
   1. Make sure that a printer is available in your network
   2. Click on the Gear icon in the upper right corner and then click on Printers
   3. If the printer isn't already listed, click on Add
   4. The printer should be detected and proper configuration values  should be displayed
   5. Print a test page
VERIFICATION:
   Were you able to print a test page to the network printer? PURPOSE:
   This test will verify that the desktop clock displays the correct date and time
STEPS:
   1. Check the clock in the upper right corner of your desktop.
VERIFICATION:
   Is the clock displaying the correct date and time for your timezone? PURPOSE:
   This test will verify that the desktop clock synchronizes with the system clock.
STEPS:
   1. Click the "Test" button and verify the clock moves ahead by 1 hour.
   Note: It may take a minute or so for the clock to refresh
   2. Right click on the clock, then click on "Time & Date Settings..."
   3. Ensure that your clock application is set to manual.
   4. Change the time 1 hour back
   5. Close the window and reboot
VERIFICATION:
   Is your system clock displaying the correct date and time for your timezone? PURPOSE:
   This test will verify that you can reboot your system from the desktop menu
STEPS:
   1. Click the Gear icon in the upper right corner of the desktop and click on "Shut Down"
   2. Click the "Restart" button on the left side of the Shut Down dialog
   3. After logging back in, restart System Testing and it should resume here
VERIFICATION:
   Did your system restart and bring up the GUI login cleanly? PURPOSE:
   This test will verify your system's ability to play Ogg Vorbis audio files.
STEPS:
   1. Click Test to play an Ogg Vorbis file (.ogg)
   2. Please close the player to proceed.
VERIFICATION:
   Did the sample play correctly? PURPOSE:
   This test will verify your system's ability to play Wave Audio files.
STEPS:
   1. Select Test to play a Wave Audio format file (.wav)
   2. Please close the player to proceed.
VERIFICATION:
   Did the sample play correctly? PURPOSE:
 If Recovery is successful, you will see this test on restarting checkbox, not
 sniff4.
STEPS:
 1. Click Yes
VERIFICATION:
 None Necessary, this is a bogus test PURPOSE:
 Simulates a failure by rebooting the machine
STEPS:
 1. Click test to trigger a reboot
 2. Select "Continue" once logged back in and checkbox is restarted
VERIFICATION:
 You won't see the user-verify PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
  1. Cut
  2. Copy
  3. Paste
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
  1. Memory set
  2. Memory reset
  3. Memory last clear
  4. Memory clear
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
 1. Simple math functions (+,-,/,*)
 2. Nested math functions ((,))
 3. Fractional math
 4. Decimal math
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator.
VERIFICATION:
 Did it launch correctly? PURPOSE:
 This test checks that gedit works.
STEPS:
 1. Click the "Test" button to open gedit, and re-open the file you created previously.
 2. Edit then save the file, then close gedit.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test checks that gedit works.
STEPS:
 1. Click the "Test" button to open gedit.
 2. Enter some text and save the file (make a note of the file name you use), then close gedit.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the AOL Instant Messaging (AIM) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Facebook Chat service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Google Talk (gtalk) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Jabber service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Microsoft Network (MSN) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a IMAP account.
VERIFICATION:
 Were you able to receive and read e-mail correctly? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a POP3 account.
VERIFICATION:
 Were you able to receive and read e-mail correctly? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a SMTP account.
VERIFICATION:
 Were you able to send e-mail without errors? PURPOSE:
 This test will check that Firefox can play a Flash video. Note: this may
 require installing additional software to successfully complete.
STEPS:
 1. Select Test to launch Firefox and view a short flash video.
VERIFICATION:
 Did the video play correctly? PURPOSE:
 This test will check that Firefox can play a Quicktime (.mov) video file.
 Note: this may require installing additional software to successfully
 complete.
STEPS:
 1. Select Test to launch Firefox with a sample video.
VERIFICATION:
 Did the video play using a plugin? PURPOSE:
 This test will check that Firefox can render a basic web page.
STEPS:
 1. Select Test to launch Firefox and view the test web page.
VERIFICATION:
 Did the Ubuntu Test page load correctly? PURPOSE:
 This test will check that Firefox can run a java applet in a web page. Note:
 this may require installing additional software to complete successfully.
STEPS:
 1. Select Test to open Firefox with the Java test page, and follow the instructions there.
VERIFICATION:
 Did the applet display? PURPOSE:
 This test will check that Firefox can run flash applications. Note: this may
 require installing additional software to successfully complete.
STEPS:
 1. Select Test to launch Firefox and view a sample Flash test.
VERIFICATION:
 Did you see the text? PURPOSE:
 This test will check that Gnome Terminal works.
STEPS:
 1. Click the "Test" button to open Terminal.
 2. Type 'ls' and press enter. You should see a list of files and folder in your home directory.
 3. Close the terminal window.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test will check that the file browser can copy a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the file called Test File 1 and click Copy.
 3. Right click in the white space and click Paste.
 4. Right click on the file called Test File 1(copy) and click Rename.
 5. Enter the name Test File 2 in the name box and hit Enter.
 6. Close the File Browser.
VERIFICATION:
 Do you now have a file called Test File 2? PURPOSE:
 This test will check that the file browser can copy a folder
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the folder called Test Folder and click on Copy.
 3. Right Click on any white area in the window and click on Paste.
 4. Right click on the folder called Test Folder(copy) and click Rename.
 5. Enter the name Test Data in the name box and hit Enter.
 6. Close the File browser.
VERIFICATION:
 Do you now have a folder called Test Data? PURPOSE:
 This test will check that the file browser can create a new file.
STEPS:
 1. Click Select Test to open the File Browser.
 2. Right click in the white space and click Create Document -> Empty Document.
 3. Enter the name Test File 1 in the name box and hit Enter.
 4. Close the File browser.
VERIFICATION:
 Do you now have a file called Test File 1? PURPOSE:
 This test will check that the file browser can create a new folder.
STEPS:
 1. Click Test to open the File Browser.
 2. On the menu bar, click File -> Create Folder.
 3. In the name box for the new folder, enter the name Test Folder and hit Enter.
 4. Close the File browser.
VERIFICATION:
 Do you now have a new folder called Test Folder? PURPOSE:
 This test will check that the file browser can delete a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the file called Test File 1 and click on Move To Trash.
 3. Verify that Test File 1 has been removed.
 4. Close the File Browser.
VERIFICATION:
  Is Test File 1 now gone? PURPOSE:
 This test will check that the file browser can delete a folder.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the folder called Test Folder and click on Move To Trash.
 3. Verify that the folder was deleted.
 4. Close the file browser.
VERIFICATION:
 Has Test Folder been successfully deleted? PURPOSE:
 This test will check that the file browser can move a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Click and drag the file called Test File 2 onto the icon for the folder called Test Data.
 3. Release the button.
 4. Double click the icon for Test Data to open that folder up.
 5. Close the File Browser.
VERIFICATION:
 Was the file Test File 2 successfully moved into the Test Data folder? PURPOSE:
 This test will check that the update manager can find updates.
STEPS:
 1. Click Test to launch update-manager.
 2. Follow the prompts and if updates are found, install them.
 3. When Update Manager has finished, please close the app by clicking the Close button in the lower right corner.
VERIFICATION:
 Did Update manager find and install updates (Pass if no updates are found,
 but Fail if updates are found but not installed) PURPOSE:
 This test will verify that the file browser can move a folder.
STEPS:
 1. Click Test to open the File Browser.
 2. Click and drag the folder called Test Data onto the icon called Test Folder.
 3. Release the button.
 4. Double click the folder called Test Folder to open it up.
 5. Close the File Browser.
VERIFICATION:
 Was the folder called Test Data successfully moved into the folder called Test Folder? PURPOSE:
 To sniff things out
STEPS:
 1. Click Yes
VERIFICATION:
 None Necessary, this is a bogus test Panel Clock Verification tests Panel Reboot Verification tests Parses Xorg.0.Log and discovers the running X driver and version Peripheral tests Piglit tests Ping ubuntu.com and restart network interfaces 100 times Play back a sound on the default output and listen for it on the  default input. Please choose (%s):  Please press each key on your keyboard. Please type here and press Ctrl-D when finished:
 Pointing device tests. Power Management Test Power Management tests Press any key to continue... Previous Print version information and exit. Provides information about displays attached to the system Provides information about network devices Quit from keyboard Record mixer settings before suspending. Record the current network before suspending. Record the current resolution before suspending. Rendercheck tests Restart Returns the name, driver name and driver version of any touchpad discovered on the system. Run Cachebench Read / Modify / Write benchmark Run Cachebench Read benchmark Run Cachebench Write benchmark Run Compress 7ZIP benchmark Run Compress PBZIP2 benchmark Run Encode MP3 benchmark Run Firmware Test Suite (fwts) automated tests. Run GLmark2 benchmark Run GLmark2-ES2 benchmark Run GnuPG benchmark Run Himeno benchmark Run Lightsmark benchmark Run N-Queens benchmark Run Network Loopback benchmark Run Qgears2 OpenGL gearsfancy benchmark Run Qgears2 OpenGL image scaling benchmark Run Qgears2 XRender Extension gearsfancy benchmark Run Qgears2 XRender Extension image scaling benchmark Run Render-Bench XRender/Imlib2 benchmark Run Stream Add benchmark Run Stream Copy benchmark Run Stream Scale benchmark Run Stream Triad benchmark Run Unigine Heaven benchmark Run Unigine Santuary benchmark Run Unigine Tropics benchmark Run a stress test based on FurMark (OpenGL 2.1 or 3.2) Fullscreen 1920x1080 no antialiasing Run a stress test based on FurMark (OpenGL 2.1 or 3.2) Windowed 1024x640 no antialiasing Run gtkperf to make sure that GTK based test cases work Run the graphics stress test. This test can take a few minutes. Run x264 H.264/AVC encoder benchmark Running %s... Runs a test that transfers 100 10MB files 3 times to a SDHC card. Runs a test that transfers 100 10MB files 3 times to usb. Runs all of the rendercheck test suites. This test can take a few minutes. Runs piglit tests for checking OpenGL 2.1 support Runs piglit tests for checking support for GLSL fragment shader operations Runs piglit tests for checking support for GLSL vertex shader operations Runs piglit tests for checking support for framebuffer object operations, depth buffer and stencil buffer Runs piglit tests for checking support for texture from pixmap Runs piglit tests for checking support for vertex buffer object operations Runs piglit_tests for checking support for stencil buffer operations Runs the piglit results summarizing tool SATA/IDE device information. SMART test Select All Select all Server Services checks Shorthand for --config=.*/jobs_info/blacklist. Shorthand for --config=.*/jobs_info/blacklist_file. Shorthand for --config=.*/jobs_info/whitelist. Shorthand for --config=.*/jobs_info/whitelist_file. Skip Smoke tests Sniff Sniffers Software Installation tests Some new hard drives include a feature that parks the drive heads after a short period of inactivity. This is a power-saving feature, but it can have a bad interaction with the operating system that results in the drive constantly parked then activated. This produces excess wear on the drive, potentially leading to early failures. Space when finished Start testing Status Stop process Stop typed at tty Stress poweroff system (100 cycles) Stress reboot system (100 cycles) Stress tests Submission details Submit results Successfully finished testing! Suspend Test Suspend tests System 
Testing System Daemon tests System Testing Tab 1 Tab 2 Termination signal Test Test ACPI Wakealarm (fwts wakealarm) Test Again Test and exercise memory. Test cancelled Test for clock jitter. Test if the atd daemon is running when the package is installed. Test if the cron daemon is running when the package is installed. Test if the cupsd daemon is running when the package is installed. Test if the getty daemon is running when the package is installed. Test if the init daemon is running when the package is installed. Test if the klogd daemon is running when the package is installed. Test if the nmbd daemon is running when the package is installed. Test if the smbd daemon is running when the package is installed. Test if the syslogd daemon is running when the package is installed. Test if the udevd daemon is running when the package is installed. Test if the winbindd daemon is running when the package is installed. Test interrupted Test offlining CPUs in a multicore system. Test that the /var/crash directory doesn't contain anything. Lists the files contained within if it does, or echoes the status of the directory (doesn't exist/is empty) Test that the X is not running in failsafe mode. Test that the X process is running. Test the CPU scaling capabilities using Firmware Test Suite (fwts cpufreq). Test the network after resuming. Test to check that a cloud image boots and works properly with KVM Test to check that virtualization is supported and the test system has at least a minimal amount of RAM to function as an OpenStack Compute Node Test to detect audio devices Test to detect the available network controllers Test to detect the optical drives Test to determine if this system is capable of running hardware accelerated KVM virtual machines Test to output the Xorg version Test to see if we can sync local clock to an NTP server Test to see that we have the same resolution after resuming as before. Tested Tests oem-config using Xpresser, and then checks that the user has been created successfully. Cleans up the newly created user after the test has passed. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11b/g protocols after the system has been suspended. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11b/g protocols. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11n protocol after the system has been suspended. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11n protocol. Tests that the systems wireless hardware can connect to a router using no security and the 802.11b/g protocols after the system has been suspended. Tests that the systems wireless hardware can connect to a router using no security and the 802.11b/g protocols. Tests that the systems wireless hardware can connect to a router using no security and the 802.11n protocol after the system has been suspended. Tests that the systems wireless hardware can connect to a router using no security and the 802.11n protocol. Tests the performance of a systems wireless connection through the iperf tool, using UDP packets. Tests the performance of a systems wireless connection through the iperf tool. Tests to see that apt can access repositories and get updates (does not install updates). This is done to confirm that you could recover from an incomplete or broken update. Tests whether the system has a working Internet connection. TextLabel The file to write the log to. The following report has been generated for submission to the Launchpad hardware database:

  [[%s|View Report]]

You can submit this information about your system by providing the email address you use to sign in to Launchpad. If you do not have a Launchpad account, please register here:

  https://launchpad.net/+login The generated report seems to have validation errors,
so it might not be processed by Launchpad. There is another checkbox running. Please close it first. This Automated test attempts to detect a camera. This attaches screenshots from the suspend/cycle_resolutions_after_suspend_auto test to the results submission. This is a fully automated version of mediacard/sd-automated and assumes that the system under test has a memory card device plugged in prior to checkbox execution. It is intended for SRU automated testing. This is an automated Bluetooth file transfer test. It sends an image to the device specified by the BTDEVADDR environment variable. This is an automated Bluetooth test. It emulates browsing on a remote device specified by the BTDEVADDR environment variable. This is an automated Bluetooth test. It receives the given file from a remote host specified by the BTDEVADDR environment variable This is an automated test to gather some info on the current state of your network devices. If no devices are found, the test will exit with an error. This is an automated test which performs read/write operations on an attached FireWire HDD This is an automated test which performs read/write operations on an attached eSATA HDD This is an automated version of usb/storage-automated and assumes that the server has usb storage devices plugged in prior to checkbox execution. It is intended for servers and SRU automated testing. This is an automated version of usb3/storage-automated and assumes that the server has usb 3.0 storage devices plugged in prior to checkbox execution. It is intended for servers and SRU automated testing. This is the automated version of suspend/suspend_advanced. This test checks cpu topology for accuracy This test checks that CPU frequency governors are obeyed when set. This test checks that the wireless interface is working after suspending the system. It disconnects all interfaces and then connects to the wireless interface and checks that the connection is working as expected. This test checks the amount of memory which is reporting in meminfo against the size of the memory modules detected by DMI. This test disconnects all connections and then connects to the wireless interface. It then checks the connection to confirm it's working as expected. This test grabs the hardware address of the bluetooth adapter after suspend and compares it to the address grabbed before suspend. This test is automated and executes after the mediacard/cf-insert test is run. It tests reading and writing to the CF card. This test is automated and executes after the mediacard/cf-insert-after-suspend test is run. It tests reading and writing to the CF card after the system has been suspended. This test is automated and executes after the mediacard/mmc-insert test is run. It tests reading and writing to the MMC card. This test is automated and executes after the mediacard/mmc-insert-after-suspend test is run. It tests reading and writing to the MMC card after the system has been suspended. This test is automated and executes after the mediacard/ms-insert test is run. It tests reading and writing to the MS card. This test is automated and executes after the mediacard/ms-insert-after-suspend test is run. It tests reading and writing to the MS card after the system has been suspended. This test is automated and executes after the mediacard/msp-insert test is run. It tests reading and writing to the MSP card. This test is automated and executes after the mediacard/msp-insert-after-suspend test is run. It tests reading and writing to the MSP card after the system has been suspended. This test is automated and executes after the mediacard/sd-insert test is run. It tests reading and writing to the SD card. This test is automated and executes after the mediacard/sd-insert-after-suspend test is run. It tests reading and writing to the SD card after the system has been suspended. This test is automated and executes after the mediacard/sdhc-insert test is run. It tests reading and writing to the SDHC card. This test is automated and executes after the mediacard/sdhc-insert-after-suspend test is run. It tests reading and writing to the SDHC card after the system has been suspended. This test is automated and executes after the mediacard/sdxc-insert test is run. It tests reading and writing to the SDXC card. This test is automated and executes after the mediacard/sdxc-insert-after-suspend test is run. It tests reading and writing to the SDXC card after the system has been suspended. This test is automated and executes after the mediacard/xd-insert test is run. It tests reading and writing to the xD card. This test is automated and executes after the mediacard/xd-insert-after-suspend test is run. It tests reading and writing to the xD card after the system has been suspended. This test is automated and executes after the usb/insert test is run. This test is automated and executes after the usb3/insert test is run. This test will check to make sure supported video modes work after a suspend and resume. This is done automatically by taking screenshots and uploading them as an attachment. This test will verify that the volume levels are at an acceptable level on your local system.  The test will validate that the volume is greater than or equal to minvol and less than or equal to maxvol for all sources (inputs) and sinks (outputs) recognized by PulseAudio.  It will also validate that the active source and sink are not muted.  You should not manually adjust the  volume or mute before running this test. This will attach any logs from the power-management/poweroff test to the results. This will attach any logs from the power-management/reboot test to the results. This will check to make sure that your audio device works properly after a suspend and resume.  This may work fine with speakers and onboard microphone, however, it works best if used with a cable connecting the audio-out jack to the audio-in jack. This will run some basic connectivity tests against a BMC, verifying that IPMI works. Timer signal from alarm(2) Touchpad tests Touchscreen tests Try to enable a remote printer on the network and print a test page. Type Text UNKNOWN USB Test USB tests Unknown signal Untested Usage: checkbox [OPTIONS] User Applications User-defined signal 1 User-defined signal 2 Validate that the Vector Floating Point Unit is running on ARM device Verifies that DNS server is running and working. Verifies that Print/CUPs server is running. Verifies that Samba server is running. Verifies that Tomcat server is running and working. Verifies that sshd is running. Verifies that the LAMP stack is running (Apache, MySQL and PHP). Verify USB3 external storage performs at or above baseline performance Verify system storage performs at or above baseline performance Verify that all CPUs are online after resuming. Verify that all memory is available after resuming from suspend. Verify that all the CPUs are online before suspending Verify that an installation of checkbox-server on the network can be reached over SSH. Verify that mixer settings after suspend are the same as before suspend. Verify that storage devices, such as Fibre Channel and RAID can be detected and perform under stress. View results Virtualization tests Welcome to System Testing!

Checkbox provides tests to confirm that your system is working properly. Once you are finished running the tests, you can view a summary report for your system. Wireless Test Wireless networking tests Wireless scanning test. It scans and reports on discovered APs. Working You can also close me by pressing ESC or Ctrl+C. _Deselect All _Exit _Finish _No _Previous _Select All _Skip this test _Test _Test Again _Yes and capacity as well. attaches the contents of various sysctl config files. eSATA disk tests empty and capacity as well. https://help.ubuntu.com/community/Installation/SystemRequirements installs the installer bootchart tarball if it exists. no skip test test again tty input for background process tty output for background process until empty and capacity as well.  Requires MOVIE_VAR to be set. yes Project-Id-Version: checkbox
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-01-27 17:46+0000
PO-Revision-Date: 2013-06-29 09:19+0000
Last-Translator: Claudio Arseni <claudio.arseni@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:34+0000
X-Generator: Launchpad (build 18115)
 

Attenzione: alcuni test potrebbero provocare il blocco o rendere non responsivo il sistema. Salvare il proprio lavoro e chiudere tutte le altre applicazioni in esecuzione prima di iniziare il processo di test.    Stabilisce se è necessario eseguire test specifici per i computer portatili che potrebbero non essere validi per quelli desktop.  Risultati   Esecuzione   Selezione   Scatta foto multiple in base alla risoluzione supportata dalla fotocamera, verificando
 le loro dimensioni e la validità del formato.  Verifica che il dispositivo senza fili del sistema possa connettersi a un router usando
 il protocollo 802.11a. Questo richiede che sia disponibile un router preconfigurato per
 rispondere solo alle richieste sul protocollo 802.11a.  Verifica che il dispositivo senza fili del sistema possa connettersi a un router usando
 il protocollo 802.11b. Questo richiede che sia disponibile un router preconfigurato per
 rispondere solo alle richieste sul protocollo 802.11b.  Verifica che il dispositivo senza fili del sistema possa connettersi a un router usando
 il protocollo 802.11g. Questo richiede che sia disponibile un router preconfigurato per
 rispondere solo alle richieste sul protocollo 802.11g. È stato premuto il tasto %(key_name)s &No &Indietro Sa&lta questo test &Prova &Sì 10 test su 30 completati (30%) Segnale di abbandono di abort(3) Tutti i tasti richiesti sono stati verificati. Archivia la directory di riepilogo di piglit in piglit-results.tar.gz. Procedere veramente? Allega il registro del test fwts wakealarm. Allega il registro dei test di «rendercheck» Allega una copia di /var/log/dmesg ai risultati del test. Allega un dump del database di udev che mostra le informazioni sull'hardware del sistema. Allega un elenco dei moduli del kernel attualmente in esecuzione. Allega un report con informazioni sulla CPU. Allega un report con i codec installati per Intel HDA. Allega un report con gli attributi di sysfs. Allega un archivio tar di dati gcov, se presenti. Allega l'output di dmidecode. Allega le informazioni sulla DMI. Allega informazioni sulle partizioni del disco. Allega il registro dei risultati di FWTS all'invio. Allega il registro di raccolta dei dati hardware dell'audio ai risultati. Allega il registro creato dalle esecuzioni di bootchart. Allega il file png creato dall'esecuzione di bootchart. Allega il contenuto di /proc/acpi/sleep, se presente. Allega il contenuto del file /etc/modules. Allega il contenuto dei vari file di configurazione di modprobe. Allega la versione del firmware. Allega i risultati dei test di stress grafico all'invio. Allega il registro di debug dell'installatore, se presente. Allega il registro del test di 250 cicli di ibernazione/ripristino, se presente. Allega il registro del test di 250 cicli di sospensione/ripristino, se presente. Allega il registro del test di 30 cicli di ibernazione/ripristino, se presente. Allega il registro del test di 30 cicli di sospensione/ripristino, se presente. Allega il registro del test di sospensione/ripristino singolo ai risultati. Allega il registro generato da cpu/scaling_test ai risultati. Allega l'output di udev_resource, per scopi di debug. Allega la schermata catturata in graphics/screenshot. Allega la schermata catturata in graphics/screenshot_fullscreen_video. Allega un output molto prolisso di lspci (con il database centrale Query). Allega un output di lspci molto prolisso. Test dell'audio Test dell'audio Test automatici di scrittura dei CD Test automatici di scrittura dei DVD Controllo automatico del registro di ibernazione per gli errori rilevati di fwts Test automatici di lettura da unità ottiche Caso di test automatico per verificare che sia possibile scaricare file attraverso il protocollo HTTP. Caso di test automatico per verificare la disponibilità in rete di sistemi che usano pacchetti ECHO ICMP. Test automatico per memorizzare le informazioni sui dispositivi Bluetooth nel report di Checkbox. Test automatico per effettuare verifiche su schede di rete multiple in sequenza. Benchmark di ciascun disco Test dei benchmark Test del Bluetooth Test del Bluetooth Informazioni di bootchart. Pipe rotta: scrittura su una pipe priva di lettori Generazione del report... Test di scrittura dei CD Test della CPU Test della CPU Uso della CPU in un sistema inattivo. Test della webcam Test della webcam Verifica i test falliti dal caso di test della shell. Verifica che il job sia eseguito quando la dipendenza ha successo Verifica che il job venga eseguito quando sono soddisfatti i requisiti Verifica che il risultato del job sia impostato come "non richiesto da questo sistema" quando non sono soddisfatti i requisiti Verifica che il risultato del job sia impostato come non iniziato quando la dipendenza fallisce Verifica i registri per il test di spegnimento sotto stress (100 cicli). Verifica i registri per il test di riavvio sotto stress (100 cicli). Verifica le modifiche alle statistiche di ciascun disco. Verifica i test passati con successo dal caso di test della shell. Verifica che i driver VESA non siano in uso. Verifica che l'hardware supporti Unity 3D. Verifica che l'hardware supporti gli effetti visivi. Verifica il tempo necessario per riconnettersi a un access point WIFI. Verifica se CONFIG_NO_HZ è impostato nel kernel (questa è solo una semplice verifica di regressione). Test del sistema Checkbox Il precedente test del sistema non è stato completato.
È possibile rieseguire l'ultimo test,
passare al successivo oppure
ricominciare dall'inizio. Controlla che il file sources.list specificato contenga i repository richiesti. Verifica la durata della batteria durante l'inattività. Indica il tempo rimanente Verifica la durata della batteria durante la sospensione. Indica il tempo rimanente Verifica la durata della batteria durante la riproduzione di un film. Indica il tempo Controlla il tempo impiegato per riconnettere una rete Wi-Fi esistente dopo un ciclo di sospensione/ripristino. Controlla il tempo impiegato per riconnettere una rete cablata esistente dopo
 un ciclo di sospensione/ripristino. Verifica il tempo di sospensione per garantire che il computer effettui la sospensione e il ripristino entro un tempo determinato. Figlio fermato o terminato Scegliere i test da eseguire sul proprio sistema: Test dei codec Contrai tutto Raccoglie informazioni relative al sistema audio. Questi dati possono essere usati per simulare il sottosistema audio del computer e per effettuare test più dettagliati in un ambiente controllato. Raccoglie informazioni sulla profondità di colore e il formato dei pixel. Raccoglie informazioni sulle modalità grafiche (risoluzione dello schermo e frequenza di aggiornamento). In combinazione con un carattere soprastante espande il nodo Comando non trovato. Il comando ha ricevuto il segnale %(signal_name)s: %(signal_description)s Commenti Test dei tipi di documento comuni. Componenti La configurazione ha la precedenza sui parametri. Continua Continua se fermato Test di scrittura dei DVD Deseleziona tutto Deseleziona tutto Informazioni dettagliate... Rileva e visualizza i dischi collegati al sistema. Rileva e mostra i dispositivi USB collegati al sistema. Stabilisce se lo schermo viene rilevato automaticamente come dispositivo multi-touch. Stabilisce se lo schermo viene rilevato automaticamente come dispositivo non sensibile al tocco. Stabilisce se il touchpad viene rilevato automaticamente come dispositivo multi-touch. Stabilisce se il touchpad viene rilevato automaticamente come dispositivo single-touch. Test del disco Test del disco Uso del disco in un sistema inattivo. Saltare veramente questo test? Non chiedere più Completato Esegue il dump delle informazioni di memoria in un file dopo l'esecuzione del test di sospensione, per poterle confrontare. Email L'indirizzo email deve avere un formato corretto. Email: Assicurarsi che la risoluzione attuale sia pari o superiore a quella minima raccomandata (800 x 600). Per maggiori dettagli: Inserire testo:
 Errore Scambio delle informazioni con il server... Esecuzione di %(test_name)s Espandi tutto Test ExpressCard Test della ExpressCard Impossibile collegarsi al server. Provare
di nuovo oppure caricare il seguente file:
%s

direttamente nel database del sistema:
https://launchpad.net/+hwdb/+submit Apertura del file «%s» non riuscita: %s Elaborazione non riuscita del modulo: %s Impossibile inviare al server,
riprovare più tardi. Test del lettore di impronte digitali Test Firewire Test del disco Firewire Eccezione di virgola mobile Test del disco floppy Test del floppy Modulo Ulteriori informazioni: Raccolta delle informazioni sul sistema... Test della grafica Test della grafica Rilevata disconnessione nel terminale di controllo oppure morte del processo di controllo stesso Test di ibernazione Test dei tasti di scelta rapida La finestra si chiuderà automaticamente una volta che tutti i tasti saranno stati premuti. Se uno dei tasti non è presente sulla tastiera, fare clic su sul pulsante «Salta» sotto il tasto corrispondente per rimuoverlo dal test. Se sulla tastiera non sono presenti uno o più tasti, premere il numero corrispondente per escludere il tasto dal test. Istruzione illecita In corso Informazioni Test informativi Informazioni non inviate a Launchpad. Test informativi Test dei dispositivi di input Test dispositivi di input Connessione a Internet stabilita Interrupt da tastiera Riferimento di memoria non valido Test dei tasti Test dei tasti Segnale di uccisione Test dei LED Elenca i dispositivi USB. Elenca driver e versione di tutti i dispositivi audio. Assicurarsi che esista il device RTC (orologio in tempo reale). Spazio massimo su disco usato durante un test di installazione predefinita Test schede di memoria Test delle schede multimediali Test della memoria Test della memoria Test vari Test vari Manca il file di configurazione come argomento.
 Test del monitor Test del monitor Sposta una finestra 3D nello schermo. &Avanti _Avanti Informazioni sulla rete Test di rete Test di rete Avanti Nessuna connessione a Internet Non risolto Non iniziato Non supportato Non effettuato Non richiesto Scegliere tra "debug", "info", "warning", "error" o "critical". Apre e chiude più volte 4 finestre 3D. Apre e chiude più volte una finestra 3D. Apre, sospende, ripristina e chiude più volte una finestra 3D. Test delle unità ottiche Test dei dispositivi ottici Test di lettura da unità ottiche SCOPO:
     Verificare il corretto funzionamento dell'uscita audio line-out.
PASSI:
     1. Collegare il cavo degli altoparlanti (con amplificatori integrati) alla porta audio line-out.
     2. Aprire le preferenze audio. Nella scheda «Uscita», selezionare «Line-out», quindi fare clic su «Prova».
     3. Nelle preferenze audio, selezionare «Audio interno» nell'elenco dei dispositivi e fare clic su «Suono di prova» per verificare i canali destro e sinistro.
VERIFICA:
     1. È possibile sentire un suono negli altoparlanti? Gli altoparlanti interni *non* devono essere esclusi automaticamente.
     2. È possibile sentire il suono dal canale corrispondente? SCOPO:
    Verificare il LED del tasto «Bloc Maiusc».
PASSI:
    1. Premere il tasto «Bloc Maiusc» per attivare o disattivare il blocco delle maiuscole.
    2. Il LED «Bloc Maiusc» dovrebbe accendersi e spegnersi ogni volta che viene premuto il tasto.
VERIFICA:
    Il LED «Bloc Maiusc» si accende come previsto? SCOPO:
    Verificare il LED della webcam.
PASSI:
    1. Fare clic su «Prova» per attivare la webcam.
    2. Il LED della webcam dovrebbe accendersi per qualche secondo.
VERIFICA:
    Si è acceso il LED della webcam? SCOPO:
    Verificare il corretto funzionamento dell'uscita audio line-in.
PASSI:
    1. Usare un cavo per connettere l'uscita audio line-in a una sorgente line-out esterna.
    2. Aprire le preferenze audio. Nella scheda «Ingresso», selezionare «Line-in», quindi fare clic su «Prova».
    3. Dopo alcuni secondi, la registrazione dovrebbe essere riprodotta.
VERIFICA:
    È possibile ascoltare la registrazione? SCOPO:
    Verifica che i vari canali audio funzionino correttamente.
PASSI:
    1. Fare clic sul pulsante «Prova».
VERIFICA:
    Si dovrebbe sentire distintamente una voce provenire dai diversi canali audio. SCOPO:
    Verificare il funzionamento del trascinamento sul tochscreen.
PASSI:
    1. Toccare due volte un oggetto sullo schermo, tenerlo premuto e trascinarlo.
    2. Rilasciare l'oggetto in una posizione differente.
VERIFICA:
    È stato possibile spostare l'oggetto selezionato? SCOPO:
    Verificare il funzionamento del gesto di pizzicare il touchscreen.
PASSI:
    1. Posizionare due dita sullo schermo e avvicinarle muovendole contemporaneamente.
    2. Posizionare due dita unite sullo schermo e allontanarle muovendole contemporaneamente.
VERIFICA:
    È stato possibile ridurre e aumentare lo zoom dello schermo? SCOPO:
    Verificare il riconoscimento del tocco sul touchscreen.
PASSI:
    1. Toccare un oggetto visibile sullo schermo con il dito. Il cursore deve spostarsi nella posizione del tocco e l'oggetto viene evidenziato.
VERIFICA:
    Il riconoscimento del tocco funziona? SCOPO:
    Creare job che usino la CPU per 2 ore al massimo regime. Il test ha successo se il sistema non si blocca. SCOPO:
    Verificare il LED del disco rigido.
PASSI:
    1. Fare clic su «Prova» per scrivere e leggere un file temporaneo per alcuni secondi.
    2. Il LED dovrebbe lampeggiare durante la scrittura e la lettura dal disco.
VERIFICA:
    Il LED si accende? SCOPO:
    Mantenere nel report le informazioni relative a chi esegue i test.
PASSI:
    1. Informazioni su chi esegue i test.
    2. Inserire le seguenti informazioni nel campo commenti:
       a. Nome
       b. Indirizzo email
       c. Motivo per l'esecuzione di questo test
VERIFICA:
    Nulla da verificare per questo test. SCOPO:
    Verificare manualmente la presenza di un accelerometro.
PASSI:
    1. Consultare le specifiche del proprio sistema.
VERIFICA:
     Si suppone che il sistema abbia un accelerometro? SCOPO:
    Verificare il LED del tastierino numerico.
PASSI:
    1. Premere il tasto «Bloc Num» per attivare il LED del tastierino numerico.
    2. Fare clic su «Prova» per aprire una finestra in cui verificare la digitazione.
    3. Digitare usando il tastierino numerico sia quando il LED è acceso che quando è spento.
VERIFICA:
    1. Lo stato del LED del tastierino numerico dovrebbe cambiare ogni volta che viene premuto il tasto «Bloc Num».
    2. Nella finestra di verifica dovrebbero essere immessi i numeri solo quando il LED è acceso. SCOPO:
    Verificare il LED dell'alimentazione.
PASSI:
    1. Il LED dell'alimentazione dovrebbe essere acceso quando è acceso il dispositivo.
VERIFICA:
    Il LED dell'alimentazione si illumina come previsto? SCOPO:
    Verificare il LED dell'alimentazione.
PASSI:
    1. Il LED dell'alimentazione dovrebbe lampeggiare o cambiare colore quando il sistema è sospeso ì.
VERIFICA:
    Il LED dell'alimentazione lampeggia o cambia colore mentre il sistema viene sospeso dal precedente test? SCOPO:
    Catturare un'istantanea della schermata corrente (accesso effettuato in Unity).
PASSI:
    Scattare un fotografia usando la webcam USB.
VERIFICA:
    Esaminare manualmente l'allegato in seguito. SCOPO:
    Catturare un'istantanea della schermata corrente dopo la sospensione (accesso effettuato in Unity).
PASSI:
    Scattare un fotografia usando la webcam USB.
VERIFICA:
    Esaminare manualmente l'allegato in seguito. SCOPO:
    Catturare un'istantanea della schermata corrente durante la riproduzione a schermo intero di un video.
PASSI:
    1. Iniziare la riproduzione a schermo intero di un video.
    2. Scattare un fotografia usando la webcam USB dopo alcuni secondi.
VERIFICA:
    Esaminare manualmente l'allegato in seguito. SCOPO:
    Verifica che il dispositivo senza fili del sistema possa connettersi a un router senza
    alcuna sicurezza usando i protocolli 802.11b/g.
PASSI:
    1. Aprire lo strumento di configurazione del router.
    2. Modificare le impostazioni per accettare solo le connessioni sulle bande «B» e «G».
    3. Assicurarsi che il parametro SSID sia impostato a ROUTER_SSID.
    4. Modificare le impostazioni per non usare alcuna sicurezza.
    5. Fare clic su «Prova» per creare una connessione con il router e verificarla.
VERIFICA:
    La verifica è automatica, non modificare il risultato selezionato automaticamente. SCOPO:
    Verifica che il dispositivo senza fili del sistema possa connettersi a un router senza
    alcuna sicurezza usando il protocollo 802.11n.
PASSI:
    1. Aprire lo strumento di configurazione del router.
    2. Modificare le impostazioni per accettare solo le connessioni sulla banda «N».
    3. Assicurarsi che il parametro SSID sia impostato a ROUTER_SSID.
    4. Modificare le impostazioni per non usare alcuna sicurezza.
    5. Fare clic su «Prova» per creare una connessione con il router e verificarla.
VERIFICA:
    La verifica è automatica, non modificare il risultato selezionato automaticamente. SCOPO:
    Verifica che il dispositivo senza fili del sistema possa connettersi a un router usando
    la sicurezza WPA e i protocolli 802.11b/g.
PASSI:
    1. Aprire lo strumento di configurazione del router.
    2. Modificare le impostazioni per accettare solo le connessioni sulle bande «B» e «G».
    3. Assicurarsi che il parametro SSID sia impostato a ROUTER_SSID.
    4. Modificare le impostazioni di sicurezza per usare la «WPA2» e assicurarsi che PSK corrisponda a quello impostato in ROUTER_PSK.
    5. Fare clic su «Prova» per creare una connessione con il router e verificarla.
VERIFICA:
    La verifica è automatica, non modificare il risultato selezionato automaticamente. SCOPO:
    Verifica che il dispositivo senza fili del sistema possa connettersi a un router usando
    la sicurezza WPA e il protocollo 802.11n.
PASSI:
    1. Aprire lo strumento di configurazione del router.
    2. Modificare le impostazioni per accettare solo le connessioni sulla banda «N».
    3. Assicurarsi che il parametro SSID sia impostato a ROUTER_SSID.
    4. Modificare le impostazioni di sicurezza per usare la «WPA2» e assicurarsi che PSK corrisponda a quello impostato in ROUTER_PSK.
    5. Fare clic su «Prova» per creare una connessione con il router e verificarla.
VERIFICA:
    La verifica è automatica, non modificare il risultato selezionato automaticamente. SCOPO:
    Questo test verifica il funzionamento corretto del plugin manuale.
PASSI:
    1. Aggiungere un commento.
    2. Impostare il risultato come passato con successo.
VERIFICA:
    Verificare che nel report il risultato sia impostato come superato con successo e che il commento venga visualizzato. SCOPO:
    Questo test effettua un ciclo attraverso le modalità video rilevate.
PASSI:
    1. Fare clic su «Prova» per iniziare il ciclo attraverso le modalità video.
VERIFICA:
    La risoluzione dello schermo risulta valida per ogni modalità? SCOPO:
    Questo test verifica le potenzialità 3D di base della scheda video.
PASSI:
    1. Fare clic su «Prova» per eseguire una demo OpenGL. Premere ESC in qualsiasi momento per chiuderla.
    2. Verificare che l'animazione non sia irregolare o rallentata.
VERIFICA:
    1. L'animazione 3D è comparsa?
    2. Era priva di rallentamenti o irregolarità? SCOPO:
    Questo test verifica che sia possibile configurare e connettere un modem DSL.
PASSI:
    1. Collegare la linea telefonica al computer.
    2. Fare clic sull'icona di rete nel pannello in alto.
    3. Selezionare «Modifica connessioni...».
    4. Selezionare la scheda «DSL».
    5. Fare clic sul pulsante «Aggiungi».
    6. Configurare correttamente i parametri di connessione.
    7. Fare clic su «Prova» per verificare che sia possibile stabilire una connessione HTTP.
VERIFICA:
    È stata visualizzata una notifica e la connessione è stata stabilita correttamente? SCOPO:
    Questo test verifica il corretto funzionamento di un dispositivo audio USB.
PASSI:
    1. Connettere un dispositivo audio USB al sistema.
    2. Fare clic su «Prova», quindi parlare nel microfono.
    3. Dopo pochi secondi la propria voce verrà riprodotta.
VERIFICA:
    La propria voce è stata riprodotta attraverso le cuffie USB? SCOPO:
    Questo test verifica il corretto funzionamento della connessione Bluetooth.
PASSI:
    1. Abilitare il Bluetooth su qualche dispositivo mobile (PDA, smartphone, ecc.).
    2. Fare clic sull'icona Bluetooth nella barra dei menù.
    3. Selezionare «Imposta nuovo dispositivo...».
    4. Cercare il dispositivo nell'elenco e selezionarlo.
    5. Digitare nel dispositivo il codice PIN scelto automaticamente dall'assistente alla configurazione.
    6. Il dispositivo dovrebbe associarsi al computer.
    7. Fare clic con il pulsante destro sull'icona Bluetooth e selezionare «Esplora file...».
    8. Autorizzare il computer a esplorare i file nel dispositivo, se necessario.
    9. Dovrebbe essere possibile esplorare i file.
VERIFICA:
    Le operazioni hanno avuto successo? SCOPO:
    Questo test verifica il corretto funzionamento del connettore delle cuffie.
PASSI:
    1. Connettere un paio di cuffie al proprio dispositivo audio.
    2. Fare clic sul pulsante «Prova» per riprodurre un suono sul proprio dispositivo audio.
VERIFICA:
    Si sente un suono attraverso le cuffie? In caso affermativo, è stato emesso senza alcuna distorsione, clic o altri strani rumori? SCOPO:
    Questo test verifica il corretto funzionamento degli altoparlanti interni.
PASSI:
    1. Assicurarsi che non sia connesso alcun altoparlante esterno o cuffia.
       Gli altoparlanti esterni sono ammessi solo se si sta provando un sistema desktop.
    2. Fare clic sul pulsante «Prova»  per riprodurre un breve suono sul proprio dispositivo audio.
VERIFICA:
    Si sente un suono? SCOPO:
    Questo test verifica il corretto funzionamento della registrazione di suoni attraverso un microfono esterno.
PASSI:
    1. Connettere un microfono al computer.
    2. Fare clic su «Prova», quindi parlare nel microfono esterno.
    3. Dopo pochi secondi la propria voce verrà riprodotta.
VERIFICA:
    La propria voce è stata riprodotta? SCOPO:
    Questo test verifica il corretto funzionamento della registrazione di suoni attraverso il microfono interno.
PASSI:
    1. Disconnettere qualsiasi microfono esterno collegato.
    2. Fare clic su «Prova», quindi parlare nel microfono interno.
    3. Dopo pochi secondi la propria voce verrà riprodotta.
VERIFICA:
    La propria voce è stata riprodotta? SCOPO:
    Questo test verifica che la webcam interna funzioni.
PASSI:
    1. Fare clic su «Prova» per visualizzare un fermo immagine dalla webcam per dieci secondi.
VERIFICA:
    Si vede l'immagine? SCOPO:
    Questo test verifica il funzionamento della webcam interna.
PASSI:
    1. Fare clic su «Prova» per visualizzare una cattura video di 10 secondi dalla webcam.
VERIFICA:
    La cattura video è stata visualizzata? SCOPO:
    Questo test verifica il corretto funzionamento dello schermo dopo una sospensione e un ripristino.
PASSI:
    1. Controllare che sullo schermo non siano presenti artefatti dopo il ripristino.
VERIFICA:
    Lo schermo funziona normalmente dopo il ripristino dalla sospensione? SCOPO:
    Questo test verifica che il sistema possa passare a un terminale virtuale e tornare a X.
PASSI:
    1. Fare clic su «Prova» per passare a un altro terminale virtuale e quindi tornare a X.
VERIFICA:
   La schermo è passato temporaneamente a una console di testo per poi ritornare alla sessione corrente? SCOPO:
    Questo test verifica che il sistema rilevi correttamente
    la rimozione della scheda CF dal lettore di schede del sistema.
PASSI:
    1. Fare clic su «Prova» e rimuovere la scheda CF dal lettore.
       (Nota: questo test va in timeout dopo 20 secondi)
VERIFICA:
    La verifica di questo test è automatica. Non modificare il
    risultato selezionato automaticamente. SCOPO:
    Questo test controlla che il sistema rilevi correttamente
    la rimozione di una scheda MS dal lettore di schede di sistema.
PASSI:
    1. Fare clic su «Prova» e rimuovere la scheda MS dal lettore.
       (Nota: questo test va in timeout dopo 20 secondi).
VERIFICA:
    La verifica di questo test è automatica. Non modificare il
    risultato selezionato automaticamente. SCOPO:
    Questo test verifica che il sistema rilevi correttamente
    la rimozione di una scheda MSP dal lettore di schede di sistema.
PASSI:
    1. Fare clic su «Prova» e rimuovere la scheda MSP dal lettore.
       (Nota: questo test va in timeout dopo 20 secondi).
VERIFICA:
    La verifica di questo test è automatica. Non modificare il
    risultato selezionato automaticamente. SCOPO:
    Questo test controlla che il sistema rilevi correttamente
    la rimozione di una scheda SDXC dal lettore di schede di sistema.
PASSI:
    1. Fare clic su «Prova» e rimuovere la scheda SDXC dal lettore.
       (Nota: questo test va in timeout dopo 20 secondi).
VERIFICA:
    La verifica di questo test è automatica. Non modificare il
    risultato selezionato automaticamente. SCOPO:
    Questo test verifica che il sistema rilevi correttamente
    la rimozione di una scheda xD dal lettore di schede di sistema.
PASSI:
    1. Fare clic su «Prova» e rimuovere la scheda xD dal lettore.
       (Nota: questo test va in timeout dopo 20 secondi).
VERIFICA:
    La verifica di questo test è automatica. Non modificare il
    risultato selezionato automaticamente. SCOPO:
    Questo test verifica che il sistema rilevi correttamente
    la rimozione della scheda SD dal lettore di schede del sistema.
PASSI:
    1. Fare clic su «Prova» e rimuovere la scheda SD dal lettore.
       (Nota: questo test va in timeout dopo 20 secondi)
VERIFICA:
    La verifica di questo test è automatica. Non modificare il
    risultato selezionato automaticamente. SCOPO:
    Questo test verifica che il sistema rilevi correttamente
    la rimozione della scheda SDHC dal lettore di schede del sistema.
PASSI:
    1. Fare clic su «Prova» e rimuovere la scheda SDHC dal lettore.
       (Nota: questo test va in timeout dopo 20 secondi)
VERIFICA:
    La verifica di questo test è automatica. Non modificare il
    risultato selezionato automaticamente. SCOPO:
    Questo test verifica che il sistema rilevi correttamente
    la rimozione della scheda MMC dal lettore di schede del sistema.
PASSI:
    1. Fare clic su «Prova» e rimuovere la scheda MMC dal lettore.
       (Nota: questo test va in timeout dopo 20 secondi)
VERIFICA:
    La verifica di questo test è automatica. Non modificare il
    risultato selezionato automaticamente. SCOPO:
    Questo test verifica che il sistema rilevi correttamente l'inserimento di
    un dispositivo di memorizzazione USB 3.0.
PASSI:
    1. Fare clic su «Prova» e inserire un dispositivo di memorizzazione USB (chiavetta/HDD) in
       una porta USB 3.0. (Nota: questo test va in timeout dopo 20 secondi.)
    2. Non disconnettere il dispositivo dopo il test.
VERIFICA:
    La verifica di questo test è automatica. Non modificare il risultato
    selezionato automaticamente. SCOPO:
    Questo test verifica che il sistema rilevi correttamente l'inserimento di
    un dispositivo di memorizzazione USB.
PASSI:
    1. Fare clic su «Prova» e inserire un dispositivo di memorizzazione USB (chiavetta/HDD).
       (Nota: questo test va in timeout dopo 20 secondi.)
    2. Non disconnettere il dispositivo dopo il test.
VERIFICA:
    La verifica di questo test è automatica. Non modificare il risultato
    selezionato automaticamente. SCOPO:
    Questo test verifica che il sistema rilevi correttamente la rimozione
    di una scheda CF dal lettore di schede del sistema dopo una sospensione del sistema stesso.
PASSI:
    1. Fare clic su «Prova» e rimuovere la scheda CF dal lettore.
       (Nota: questo test va in timeout dopo 20 secondi)
VERIFICA:
    La verifica di questo test è automatica. Non modificare il
    risultato selezionato automaticamente. SCOPO:
    Questo test controlla che il sistema rilevi correttamente la rimozione
    di una scheda MS dal lettore di schede di sistema dopo una sospensione del sistema stesso.
PASSI:
    1. Fare clic su «Prova» e rimuovere la scheda MS dal lettore.
       (Nota: questo test va in timeout dopo 20 secondi).
VERIFICA:
    La verifica di questo test è automatica. Non modificare il
    risultato selezionato automaticamente. SCOPO:
    Questo test controlla che il sistema rilevi correttamente la rimozione
    di una scheda MSP dal lettore di schede di sistema dopo una sospensione del sistema stesso.
PASSI:
    1. Fare clic su «Prova» e rimuovere la scheda MSP dal lettore.
       (Nota: questo test va in timeout dopo 20 secondi).
VERIFICA:
    La verifica di questo test è automatica. Non modificare il
    risultato selezionato automaticamente. SCOPO:
    Questo test controlla che il sistema rilevi correttamente la rimozione
    di una scheda xD dal lettore di schede di sistema dopo una sospensione del sistema stesso.
PASSI:
    1. Fare clic su «Prova» e rimuovere la scheda xD dal lettore.
       (Nota: questo test va in timeout dopo 20 secondi).
VERIFICA:
    La verifica di questo test è automatica. Non modificare il
    risultato selezionato automaticamente. SCOPO:
    Questo test verifica che il sistema rilevi correttamente la rimozione
    di una scheda MMC dal lettore di schede del sistema dopo una sospensione del sistema stesso.
PASSI:
    1. Fare clic su «Prova» e rimuovere la scheda MMC dal lettore.
       (Nota: questo test va in timeout dopo 20 secondi)
VERIFICA:
    La verifica di questo test è automatica. Non modificare il
    risultato selezionato automaticamente. SCOPO:
    Questo test verifica che il sistema rilevi correttamente la rimozione
    di una scheda SDHC dal lettore di schede del sistema dopo una sospensione del sistema stesso.
PASSI:
    1. Fare clic su «Prova» e rimuovere la scheda SDHC dal lettore.
       (Nota: questo test va in timeout dopo 20 secondi)
VERIFICA:
    La verifica di questo test è automatica. Non modificare il
    risultato selezionato automaticamente. SCOPO:
    Questo test verifica che il sistema rilevi correttamente la rimozione di
    un dispositivo di memorizzazione USB 3.0.
PASSI:
    1. Fare clic su «Prova» e rimuovere il dispositivo USB 3.0.
       (Nota: questo test va in timeout dopo 20 secondi.)
VERIFICA:
    La verifica di questo test è automatica. Non modificare il risultato
    selezionato automaticamente. SCOPO:
    Questo test verifica che il sistema rilevi correttamente la rimozione di
    un dispositivo di memorizzazione USB.
PASSI:
    1. Fare clic su «Prova» e rimuovere il dispositivo USB.
       (Nota: questo test va in timeout dopo 20 secondi.)
VERIFICA:
    La verifica di questo test è automatica. Non modificare il risultato
    selezionato automaticamente. SCOPO:
    Questo test verifica che sia possibile registrare e ascoltare suoni usando un dispositivo audio Bluetooth.
PASSI:
    1. Abilitare la cuffia Bluetooth.
    2. Fare clic sull'icona Bluetooth nella barra dei menù.
    3. Selezionare «Imposta nuovo dispositivo...».
    4. Cercare il dispositivo nell'elenco e selezionarlo.
    5. Digitare nel dispositivo il codice PIN scelto automaticamente dall'assistente alla configurazione.
    6. Il dispositivo dovrebbe associarsi al computer.
    7. Selezionare «Prova» per registrare per 5 secondi e riprodurli nel dispositivo Bluetooth.
VERIFICA:
    Il suono registrato si sente nel dispositivo Bluetooth? SCOPO:
    Questo test verifica che sia possibile trasferire informazioni attraverso una connessione Bluetooth.
PASSI:
    1. Assicurarsi di essere in grado di esplorare i file nel proprio dispositivo mobile.
    2. Copiare un file dal computer al dispositivo mobile.
    3. Copiare un file dal dispositivo mobile al computer.
VERIFICA:
    I file sono stati copiati tutti correttamente? SCOPO:
    Questo test verifica che sia possibile utilizzare un dispositivo Bluetooth HID.
PASSI:
    1. Abilitare un mouse e una tastiera Bluetooth.
    2. Fare clic sull'icona del Bluetooth nella barra dei menù.
    3. Selezionare «Imposta nuovo dispositivo».
    4. Cercare il dispositivo nel'elenco e selezionarlo.
    5. Per i mouse, eseguire operazioni come muovere il puntatore, fare clic con i pulsanti destro e sinistro e fare un doppio-clic.
    6. Per le tastiere, fare clic su «Prova» per lanciare un piccolo strumento. Inserire del testo nello strumento e chiuderlo.
VERIFICA:
    Il dispositivo funziona come previsto? SCOPO:
    Questo test verifica che sia possibile utilizzare un dispositivo USB HID.
PASSI:
    1. Abilitare un mouse e una tastiera USB.
    2. Per i mouse, eseguire operazioni come muovere il puntatore, fare clic con i pulsanti destro e sinistro e fare un doppio-clic.
    3. Per le tastiere, fare clic su «Prova» per lanciare un piccolo strumento. Inserire del testo nello strumento e chiuderlo.
VERIFICA:
    Il dispositivo funziona come previsto? SCOPO:
    Questo test verifica che il sistema rilevi i dispositivi di memorizzazione USB.
PASSI:
    1. Connettere una o più chiavette USB o unità disco.
    2. Fare clic su «Prova».
INFORMAZIONI:
    $output
VERIFICA:
    Le unità sono state rilevate? SCOPO:
    Questo test verifica che il sistema riesca a rilevare l'inserimento di un disco rigido Firewire.
PASSI:
    1. Fare clic su «Prova» per iniziare il test. Questo test va in
       timeout e fallisce se l'inserimento non viene rilevato entro 20 secondi.
    2. Collegare un disco rigido Firewire a una porta Firewire disponibile.
VERIFICA:
    La verifica di questo test è automatica. Non modificare il risultato selezionato
    automaticamente. SCOPO:
    Questo test verifica che il sistema rilevi l'inserimento di un disco rigido eSATA.
PASSI:
    1. Fare clic su «Prova» per iniziare il test. Questo test va in
       timeout e fallisce se l'inserimento non viene rilevato entro 20 secondi.
    2. Collegare un disco rigido eSATA a una porta eSATA disponibile.
VERIFICA:
    La verifica di questo test è automatica. Non modificare il risultato selezionato
    automaticamente. SCOPO:
   Questo test verifica che il sistema riesca a rilevare la rimozione di un disco rigido Firewire.
 PASSI:
   1. Fare clic su «Prova» per iniziare il test. Questo test va in timeout e fallisce se
      la rimozione non viene rilevata entro 20 secondi.
   2. Rimuovere dalla porta Firewire il disco collegato precedentemente.
 VERIFICA:
   La verifica di questo test è automatica. Non modificare il risultato selezionato
    automaticamente. SCOPO:
    Questo test verifica che il sistema rilevi la rimozione di un disco rigido eSATA.
PASSI:
    1. Fare clic su «Prova» per iniziare il test. Questo test va in timeout e fallisce se
       la rimozione non viene rilevata entro 20 secondi.
    2. Rimuovere dalla porta eSATA il disco rigido collegato precedentemente.
VERIFICA:
    La verifica di questo test è automatica. Non modificare il risultato selezionato
    automaticamente. SCOPO:
    Questo test verifica che il sistema possa essere ibernato con successo (se la funzione è supportata).
PASSI:
    1. Fare clic su «Prova».
    2. Il sistema verrà ibernato e dovrebbe essere ripristinato entro 5 minuti.
    3. Se il sistema non viene ripristinato dopo 5 minuti, premere il tasto di accensione per ripristinare manualmente il sistema.
    4. Se il sistema non viene ripristinato, avviare nuovamente «Test del sistema» e contrassegnare questo test come fallito.
VERIFICA:
    Il sistema è stato ibernato con successo e funziona correttamente dopo il ripristino? SCOPO:
    Questo test verifica la capacità di riproduzione dei CD audio.
PASSI:
    1. Inserire un CD audio nell'unità ottica.
    2. Quando richiesto, avviare il «Riproduttore musicale».
    3. Individuare il CD nel «Riproduttore musicale».
    4. Selezionare il CD nel «Riproduttore musicale».
    5. Fare clic sul pulsante «Riproduci» per ascoltare la musica del CD.
    6. Fermare la riproduzione dopo qualche secondo.
    7. Fare clic con il tasto destro del mouse sull'icona del CD e selezionare «Espelli disco».
    8. Il CD dovrebbe essere espulso.
    9. Chiudere il «Riproduttore musicale».
VERIFICA:
    Le operazioni hanno avuto successo? SCOPO:
    Questo test verifica le capacità di riproduzione di DVD.
PASSI:
    1. Inserire un DVD contenente un filmato nell'unità ottica.
    2. Fare clic su «Prova» per riprodurre il DVD in Totem.
VERIFICA:
    Il file è stato riprodotto? SCOPO:
    Questo test verifica la connessione USB 3.0.
PASSI:
    1. Connettere un disco rigido o una chiavetta a una porta USB 3.0 del computer.
    2. Dovrebbe comparire un'icona nel Launcher.
    2. Fare clic su «Prova» per iniziare il test.
VERIFICA:
    La verifica di questo test è automatica. Non modificare il risultato
    selezionato automaticamente. SCOPO:
    Questo test verifica le connessioni USB.
PASSI:
    1. Collegare un dispositivo di memorizzazione USB a una porta USB esterna al computer.
    2. Dovrebbe comparire un'icona nel Launcher.
    3. Confermare la comparsa dell'icona.
    4. Rimuovere il dispositivo.
    5. Ripetere per ogni porta USB esterna.
VERIFICA:
    Il dispositivo funziona su tutte le porte USB? SCOPO:
    Questo test verifica le connessioni USB.
PASSI:
    1. Collegare un disco rigido o un thumb drive USB al computer.
    2. Dovrebbe comparire un'icona nel Launcher.
    3. Fare clic su «Prova» per iniziare il test.
VERIFICA:
    La verifica di questo test è automatica. Non modificare il risultato
    selezionato automaticamente. SCOPO:
    Questo test verifica le capacità di risparmio energetico del monitor.
PASSI:
    1. Fare clic su «Prova» per verificare le capacità di risparmio energetico del monitor.
    2. Premere un tasto qualunque o muovere il mouse per riattivare il monitor.
VERIFICA:
    Il monitor si è spento e riacceso? SCOPO:
    Questo test verifica il funzionamento della connessione via cavo.
PASSI:
    1. Fare clic sull'icona di rete nel pannello in alto.
    2. Selezionare una rete nella sezione «Reti via cavo».
    3. Fare clic su «Prova» per verificare che sia possibile stabilire una connessione HTTP.
VERIFICA:
    È stata visualizzata una notifica e la connessione è stata stabilita correttamente? SCOPO:
    Questo test verifica il funzionamento della connessione senza fili.
PASSI:
    1. Fare clic sull'icona di rete nel pannello.
    2. Selezionare una rete nella sezione «Reti senza fili».
    3. Fare clic su «Prova» per verificare che sia possibile stabilire una connessione HTTP.
VERIFICA:
    È stata visualizzata una notifica e la connessione è stata stabilita in modo corretto? SCOPO:
    Questo test effettua un ciclo attraverso le modalità rilevate dello schermo.
PASSI:
    1. Fare clic su «Prova» per iniziare il ciclo attraverso le modalità dello schermo.
VERIFICA:
    Lo schermo è stato visualizzato correttamente nelle modalità rilevate? SCOPO:
    Questo test invia l'immagine «JPEG_Color_Image_Ubuntu.jpg» a un dispositivo specificato.
PASSI:
    1. Fare clic su «Prova», verrà richiesto di inserire il nome di un dispositivo Bluetooth che accetti il trasferimento di file (dopo l'inserimento del nome l'inizio del trasferimento del file potrebbe richiedere del tempo).
    2. Accettare qualsiasi richiesta compaia in entrambi i dispositivi.
VERIFICA:
    I dati sono stati trasferiti in modo corretto? SCOPO:
    Questo test invierà l'immagine «JPEG_Color_Image_Ubuntu.jpg» a un determinato dispositivo.
PASSI:
    1. Assicurarsi, controllando l'applet presente nel pannello, che il Bluetooth si abilitato.
    2. Fare clic su «Prova» e attendere la richiesta di immettere il nome del dispositivo Bluetooth che può accettare il trasferimento del file (potrebbe essere necessario qualche minuto tra l'immissione del nome e l'invio del file.
    3. Accettare tutte le richieste che compaiono su entrambi i dispositivi.
VERIFICA:
    I dati sono stati trasferiti correttamente? SCOPO:
    Questo test verifica il funzionamento della rotazione dello schermo.
PASSI:
    1. Fare clic su «Prova». Lo schermo effettuerà una rotazione ogni 4 secondi.
    2. Verificare che tutte le rotazioni (normale/oraria/180 gradi/antioraria) siano avvenute senza alterazioni permanenti dello schermo.
VERIFICA:
    La rotazione dello schermo è avvenuta senza alterazioni permanenti dello schermo? SCOPO:
    Questo test verifica il funzionamento dei tasti di luminosità.
PASSI:
    1. Premere i tasti della luminosità sulla tastiera.
VERIFICA:
    La luminosità è cambiata in seguito alla pressione dei tasti? SCOPO:
    Questo test verifica il funzionamento dei tasti di luminosità dopo il ripristino dalla sospensione.
PASSI:
    1. Premere i tasti della luminosità sulla tastiera.
VERIFICA:
    La luminosità è cambiata in seguito alla pressione dei tasti dopo il ripristino dalla sospensione? SCOPO:
    Questo test verifica la funzionalità dello schermo predefinito.
PASSI:
    1. Fare clic su «Prova» per visualizzare un test video.
VERIFICA:
    Sono visualizzate delle barre di colore e statica? SCOPO:
    Questo test verifica il tasto per la disattivazione dell'audio sulla tastiera.
PASSI:
    1. Fare clic su «Prova» per aprire una finestra di prova del tasto per la disattivazione dell'audio.
    2. Se il tasto funziona, il test sarà superato con successo e la finestra si chiuderà.
VERIFICA:
    Il tasto per la disattivazione dell'audio funziona come previsto? SCOPO:
    Questo test verifica il funzionamento del tasto per disattivare l'audio dopo il ripristino dalla sospensione.
PASSI:
    1. Fare clic su «Prova» per aprire una finestra nella quale verificare il funzionamento del tasto per disattivare l'audio.
    2. Se il tasto funziona, il test verrà contrassegnato come superato con successo.
VERIFICA:
    L'audio, viene attivato/disattivato correttamente alla pressione del tasto? SCOPO:
    Questo test verifica il funzionamento del tasto di sospensione.
PASSI:
    1. Premere il tasto di sospensione sulla tastiera.
    2. Ripristinare il sistema premendo il tasto di accensione.
VERIFICA:
    Il sistema è andato in sospensione dopo aver premuto il tasto relativo? SCOPO:
    Questo test verifica il funzionamento del tasto di sospensione dopo il ripristino.
PASSI:
    1. Premere il tasto di sospensione sulla tastiera.
    2. Ripristinare il sistema premendo il tasto di accensione.
VERIFICA:
    Il sistema è andato in sospensione dopo aver premuto il tasto relativo anche dopo il ripristino? SCOPO:
    Questo test verifica il funzionamento del tasto «Super» della tastiera.
PASSI:
    1. Fare clic su «Prova» per aprire una finestra nella quale provare il tasto «Super».
    2. Se il tasto funziona, il test avrà successo e la finestra verrà chiusa.
VERIFICA:
    Il tast «Super» funziona come previsto? SCOPO:
    Questo test verifica il funzionamento del tasto «Super» dopo il ripristino dalla sospensione.
PASSI:
    1. Fare clic su «Prova» per aprire una finestra nella quale verificare il funzionamento del tasto «Super».
    2. Se il tasto funziona, il test verrà contrassegnato come superato con successo.
VERIFICA:
     Il tasto «Super» funziona correttamente dopo il ripristino dalla sospensione. SCOPO:
    Questo test verifica il funzionamento del tasto della rete senza fili dopo il ripristino dalla sospensione.
PASSI:
    1. Premere il tasto della rete senza fili sulla tastiera.
    2. Premere nuovamente il tasto.
VERIFICA:
    La rete senza fili è stata disabilitata dopo la prima pressione del tasto e abilitata nuovamente dopo la seconda al rispristino dalla sospensione? SCOPO:
    Questo test verifica il funzionamento della tastiera.
PASSI:
    1. Fare clic su «Prova».
    2. Nella casella di testo visualizzata, usare la tastiera per digitare qualcosa.
VERIFICA:
    La tastiera funziona correttamente? SCOPO:
    Questo test verifica il funzionamento del dispositivo di puntamento.
PASSI:
    1. Muovere il cursore usando il dispositivo di puntamento o toccando lo schermo.
    2. Compiere qualche operazione tipo clic/doppio-clic/clic-destro.
VERIFICA:
    Il dispositivo di puntamento funziona come previsto? SCOPO:
    Questo test verifica che l'interfaccia grafica sia funzionante dopo la modifica manuale della risoluzione.
PASSI:
    1. Aprire l'applicazione «Monitor».
    2. Selezionare la nuova risoluzione dall'elenco a discesa.
    3. Fare clic su «Applica».
    4. Selezionare la risoluzione originaria dall'elenco a discesa.
    5. Fare clic su «Applica».
VERIFICA:
    La risoluzione è stata modificata come previsto? SCOPO:
    Questo test verifica la risoluzione predefinita dello schermo.
PASSI:
    1. Lo schermo sta usando la seguente risoluzione:
INFORMAZIONI:
    $output
VERIFICA:
    È accettabile per lo schermo? SCOPO:
    Assicurare che i test di stress dei tasti di scelta rapida della connessione Wi-Fi non facciano scomparire le applet del pannello e che il sistema non si blocchi.
PASSI:
    1. Accedere alla sessione desktop.
    2. Premere il tasto di scelta rapida della connessione Wi-Fi al ritmo di una pressione al secondo, aumentando lentamente la velocità fino a premere il più velocemente possibile.
VERIFICA:
    Controllare che il sistema non sia andato in blocco e che le applet Wi-Fi e Bluetooth siano ancora visibili e funzionanti. SCOPO:
    Verificare il LED del touchpad.
PASSI:
   1. Fare clic sul pulsante o sulla combinazione di tasti che abilitano/disabilitano il touchpad.
    2. Fare scorrere il dito sul touchpad.
VERIFICA:
    1. Lo stato del LED del touchpad dovrebbe variare ogni volta che il pulsante o la combinazione di tasti vengono premuti.
    2. Quando il LED è acceso, il puntatore del mouse dovrebbe spostarsi con l'utilizzo del touchpad.
    3. Quando il LED è spento, il puntatore del mouse non dovrebbe spostarsi con l'utilizzo del touchpad. SCOPO:
    Verificare il funzionamento del LED del touchpad dopo il ripristino dalla sospensione.
PASSI:
    1. Fare clic sul pulsante o usare la combinazione di tasti per abilitare/disabilitare il touchpad.
    2. Fare scorrere le dita sul touchpad.
VERIFICA:
    1. Il LED del touchpad dovrebbe attivarsi/disattivarsi ogni volta che il pulsante o la combinazione di tasti vengono premuti.
    2. Quando il LED è acceso, il puntatore del mouse dovrebbe muoversi usando il touchpad.
    3. Quando il LED è spento, il puntatore del mouse non dovrebbe muoversi usando il touchpad. SCOPO:
    Verificare lo scorrimento orizzontale del touchpad.
PASSI:
    1. Fare clic su «Prova» quando pronti e posizionare il cursore al'interno dei bordi della finestra visualizzata.
    2. Verificare che sia possibile spostare l'indicatore orizzontale muovendo il dito a destra e a sinistra nella parte bassa del touchpad.
VERIFICA:
    È possibile scorrere a destra e a sinistra? SCOPO:
    Verificare manualmente se il touchpad è un dispositivo multi-touch.
PASSI:
   1. Consultare le specifiche del proprio sistema.
VERIFICA:
    Si suppone che il touchpad supporti il multi-touch? SCOPO:
    Verificare manualmente il funzionamento del touchpad.
PASSI:
    1. Assicurarsi che il touchpad sia abilitato.
    2. Spostare il cursore usando il touchpad.
VERIFICA:
    È possibile muovere il cursore? SCOPO:
    Verificare lo scorrimento verticale del touchpad.
PASSI:
    1. Fare clic su «Prova» quando pronti e posizionare il cursore al'interno dei bordi della finestra visualizzata.
    2. Verificare che sia possibile spostare l'indicatore verticale muovendo il dito verso l'alto e verso il basso nella parte destra del touchpad.
VERIFICA:
    È possibile scorrere verso l'alto e verso il basso? SCOPO:
    Verificare manualmente il rilevamento del multi-touch sullo schermo.
PASSI:
    1. Consultare le specifiche del proprio sistema.
VERIFICA:
    Si suppone che lo schermo sia multi-touch? SCOPO:
    Verificare il LED delle connessioni senza fili (WLAN + Bluetooth) funzioni anche dopo il ripristino dalla sospensione.
PASSI:
    1. Assicurarsi che la connessione WLAN sia stabilita e che il Bluetooth sia abilitato.
    2. Il LED WLAN/Bluetooth dovrebbe essere acceso.
    3. Spegnere la WLAN e il Bluetooth tramite un interruttore hardware (se presente).
    4. Riaccenderli nuovamente.
    5. Spegnere la WLAN e il Bluetooth tramite l'applet del pannello.
    6. Riaccenderli nuovamente.
VERIFICA:
    Il LED WLAN/Bluetooth si è acceso come previsto dopo il ripristino dalla sospensione? SCOPO:
   Verificare che il LED si spenga quanto viene disabilitata la WLAN dopo il ripristino dalla sospensione.
PASSI:
    1. Connettersi a un AP (Acces Point).
    2. Usare l'interruttore fisico per disabilitare la WLAN.
    3. Abilitare nuovamente la rete.
    4. Usare il gestore di rete per disabilitare la rete WLAN.
VERIFICA:
   Il LED si spegne quando la WLAN viene disabilitata dopo il ripristino dalla sospensione? SCOPO:
    Confermare che il LED del Bluetooth si accenda e si spenga quando il Bluetooth è abilitato o disabilitato.
PASSI:
    1. Spegnere il Bluetooth tramite un interruttore hardware (se presente).
    2. Riaccendere il Bluetooth.
    3. Spegnere il Bluetooth tramite l'applet del pannello.
    4. Riaccendere nuovamente il bluetooth.
VERIFICA:
    Il LED del Bluetooth si è spento e acceso due volte? SCOPO:
    Verificare che il LED del Bluetooth si accenda e si spenga quando si attiva/disattiva il Bluetooth dopo il ripristino dalla sospensione.
PASSI:
    1. Spegnere il Bluetooth usando un interruttore fisico (se presente).
    2. Riaccendere il Bluetooth.
    3. Spegnere il Bluetooth dall'applet del pannello.
    4. Riaccendere il Bluetooth.
VERIFICA:
    Il LED del Bluetooth si accende e si spegne in entrambi i casi dopo il ripristino dalla sospensione? SCOPO:
    Verificare che il tasto «BlocMaiusc» funzioni allo stesso modo prima e dopo il ripristino dalla sospensione.
PASSI:
    1. Premere il tasto «BlocMaiusc» per attivare/disattivare il blocco delle maiuscole.
    2. Il LED del blocco delle maiuscole dovrebbe accendersi e spegnersi ogni volta che viene premuto il tasto.
VERIFICA:
    Il LED del blocco delle maiuscole funzione come dovrebbe anche dopo il ripristino dalla sospensione? SCOPO:
    Confermare che il tasto di scelta rapida per il monitor esterno funzioni come previsto.
PASSI:
    1. Collegare un monitor esterno.
    2. Premere il tasto di scelta rapida dello schermo per modificare la configurazione dei monitor.
VERIFICA:
    Controllare che il segnale video possa essere duplicato, esteso e che sia visibile solo sul monitor esterno o su quello interno. SCOPO:
    Confermare che il tasto di scelta rapida per il monitor esterno funzioni come previsto dopo il ripristino dalla sospensione.
PASSI:
    1. Collegare un monitor esterno.
    2. Premere il tasto di scelta rapida dello schermo per modificare la configurazione dei monitor.
VERIFICA:
    Controllare che il segnale video possa essere duplicato, esteso e che sia visibile solo sul monitor esterno o su quello interno, dopo il ripristino dalla sospensione. SCOPO:
    Verificare che il LED del disco rigido funzioni correttamente dopo il ripristino dalla sospensione.
PASSI:
    1. Fare clic su «Prova» per scrivere e leggere un file temporaneo per alcuni secondi.
    2. Il LED dovrebbe lampeggiare durante la scrittura e la lettura dal disco.
VERIFICA:
    Il LED lampeggia ancora durante le operazioni sul disco dopo il ripristino dalla sospensione? SCOPO:
    Verificare che il LED indichi la batteria scarica.
PASSI:
    1. Lasciare il sistema in esecuzione alimentato dalla batteria per alcune ore.
    2. Controllare attentamente il LED della batteria.
VERIFICA:
    Il LED, quando la batteria è scarica, appare arancione? SCOPO:
    Verificare che il LED indichi la batteria scarica dopo il ripristino dalla sospensione.
PASSI:
    1. Lasciare il sistema in esecuzione alimentato dalla batteria per alcune ore.
    2. Controllare attentamente il LED della batteria.
VERIFICA:
    Il LED, quando la batteria è scarica, appare arancione dopo il ripristino dalla sospensione? SCOPO:
    Verificare che l'indicatore luminoso della batteria mostri correttamente lo stato di carica.
PASSI:
    1. Lasciare il sistema in esecuzione alimentato dalla batteria per un breve periodo.
    2. Collegare l'alimentazione esterna.
    3. Lasciare il sistema in esecuzione alimentato dalla corrente elettrica.
VERIFICA:
    L'indicatore arancione della batteria è ancora spento al completamento della ricarica SCOPO:
    Verificare che l'indicatore luminoso della batteria mostri correttamente lo stato di carica dopo il ripsristino dalla sospensione.
PASSI:
    1. Lasciare il sistema in esecuzione alimentato dalla batteria per un breve periodo.
    2. Collegare l'alimentazione esterna.
    3. Lasciare il sistema in esecuzione alimentato dalla corrente elettrica.
VERIFICA:
    L'indicatore arancione della batteria è ancora spento al completamento della ricarica dopo il ripristino dalla sospensione? SCOPO:
    Verificare che l'indicatore luminoso della batteria mostri lo stato di carica.
PASSI:
    1. Lasciare il sistema in esecuzione alimentato dalla batteria per un po'.
    2. Collegare l'alimentazione esterna.
VERIFICA:
L'indicatore luminoso della batteria è arancione? SCOPO:
    Verificare che l'indicatore luminoso della batteria mostri lo stato di carica dopo il ripristino dalla sospensione.
PASSI:
    1. Lasciare il sistema in esecuzione alimentato dalla batteria  per un po'.
    2. Collegare l'alimentazione esterna.
VERIFICA:
    L'indicatore luminoso della batteria è ancora arancione dopo il ripristino dalla sospensione? SCOPO:
    Verificare che il LED della webcam funzioni correttamente anche dopo il ripristino dalla sospensione.
PASSI:
    1. Fare clic su «Prova» per attivare la webcam.
    2. Il LED della webcam dovrebbe accendersi per alcuni secondi.
VERIFICA:
    Il LED della webcam si accende e si spegne dopo il ripristino dalla sospensione? SCOPO:
    Verificare che il LED del tastierino numerico funzioni allo stesso modo prima e dopo il ripristino dalla sospensione.
PASSI:
    1. Premere il tasto «Bloc Num» per attivare/disattivare il LED.
    2. Fare clic sul pulsante «Prova» per aprire una finestra di verifica della digitazione.
    3. Scrivere usando il tastierino numerico sia quando il LED è acceso sia quando è spento.
VERIFICA:
    1. Il LED del tastierino numerico dovrebbe attivarsi/disattivarsi ogni volta che viene premuto il tasti «Bloc Num».
    2. I numeri dovrebbero essere immessi nella finestra di verifica della tastiera solo quando il LED è acceso. SCOPO:
    Verificare che il LED dell'alimentazione funzioni anche dopo il ripristino dalla sospensione.
PASSI:
    1. Il LED dell'alimentazione dovrebbe essere acceso mentre il dispositivo è in funzione.
VERIFICA:
    Il LED dell'alimentazione rimane acceso dopo il ripristino dalla sospensione? SCOPO:
    Verificare che il tasto di scelta rapida attivi e disattivi la funzionalità del touchpad.
PASSI:
    1. Verificare che il touchpad funzioni.
    2. Premere il tasto di scelta rapida per attivare/disattivare il touchpad.
    3. Premere nuovamente il tasto di scelta rapida del touchpad.
VERIFICA:
    Verificare che il touchpad sia stato disabilitato e abilitato nuovamente. SCOPO:
    Verificare che il tasto di scelta rapida attivi e disattivi la funzionalità del touchpad dopo il ripristino dalla sospensione.
PASSI:
    1. Verificare che il touchpad funzioni.
    2. Premere il tasto di scelta rapida per attivare/disattivare il touchpad.
    3. Premere nuovamente il tasto di scelta rapida del touchpad.
VERIFICA:
    Verificare che il touchpad sia stato disabilitato e abilitato nuovamente. SCOPO:
    Ripristinare il sistema dalla sospensione usando una tastiera USB.
PASSI:
    1. Abilitare nel BIOS la funzionalità per il ripristino dalla sospensione da tastiera o mouse USB.
    2. Fare clic su «Prova» per attivare la sospensione (modalità S3).
    3. Premere un qualunque tasto sulla tastiera USB per ripristinare il sistema.
VERIFICA:
    Il sistema è stato ripristinato dalla sospensione alla pressione del tasto? SCOPO:
    Ripristinare il sistema dalla sospensione usando un mouse USB.
PASSI:
    1. Abilitare nel BIOS la funzionalità per il ripristino dalla sospensione da tastiera o mouse USB.
    2. Fare clic su «Prova» per attivare la sospensione (modalità S3).
    3. Premere un qualunque pulsante del mouse USB per ripristinare il sistema.
VERIFICA:
    Il sistema è stato ripristinato dalla sospensione alla pressione del pulsante del mouse? SCOPO:
    Verificare il LED delle connessioni wireless (WLAN + Bluetooth).
PASSI:
    1. Assicurarsi che la connessione WLAN sia stabilita e che il Bluetooth sia abilitato.
    2. Il LED WLAN/Bluetooth dovrebbe essere acceso.
    3. Spegnere la WLAN e il Bluetooth tramite un interruttore hardware (se presente).
    4. Riaccenderli nuovamente.
    5. Spegnere la WLAN e il Bluetooth tramite l'applet del pannello.
    6. Riaccenderli nuovamente.
VERIFICA:
    Il LED WLAN/Bluetooth si è acceso come previsto? SCOPO:
   Questo è un test automatico di stress che forza il sistema a effettuare 250 cicli di ibernazione/ripristino. SCOPO:
   Questo è un test automatico di stress che forza il sistema a effettuare 30 cicli di ibernazione/ripristino. SCOPO:
   Questo è un test automatico di stress che forza il sistema a effettuare 250 cicli di sospensione/ripristino. SCOPO:
   Questo è un test automatico di stress che forza il sistema a effettuare 30 cicli di sospensione/ripristino. SCOPO:
   Questo test verifica che un modem DSL USB o a banda larga mobile funzioni.
PASSI:
   1. Connettere il cavo USB al computer.
   2. Clic-destro sull'icona della rete presente nel pannello.
   3. Selezionare «Modifica connessioni...».
   4. Selezionare la scheda «DSL» (per il modem ADSL) o «Banda larga mobile» (per il modem 3G).
   5. Fare clic su «Aggiungi».
   6. Configurare opportunamente i parametri della connessione.
   7. Una notifica sullo schermo dovrebbe confermare che la connessione è stata stabilita.
   8. Fare clic su «Prova» per verificare la possibilità di stabilire una connessione HTTP.
VERIFICA:
   La connessione è avvenuta correttamente? SCOPO:
   Questo test verifica che il lettore di impronte digitali possa essere usato per sbloccare il sistema.
PASSI:
   1. Fare clic sull'indicatore di sessione (icona a forma di ingranaggio sul lato destro del pannello).
   2. Selezionare «Blocca».
   3. Premere un qualunque tasto o muovere il mouse.
   4. Compare una finestra nella quale è possibile scegliere se sbloccare il sistema digitando la password o usando l'autenticazione tramite impronta digitale.
   5. Usare il lettore di impronte per sbloccare il sistema.
   6. Lo schermo dovrebbe sbloccarsi.
VERIFICA:
   La procedura di sblocco, ha funzionato correttamente? SCOPO:
   Questo test verifica il funzionamento di una stampante di rete.
PASSI:
   1. Assicurarsi che nella rete sia disponibile una stampante.
   2. Fare clic sull'icona dell'ingranaggio nell'angolo in alto a destra, quindi fare clic su «Stampanti».
   3. Se la stampante non è già presente nell'elenco, fare clic su «Aggiungi».
   4. La stampante dovrebbe essere rilevata e dovrebbero essere visualizzati i valori di configurazione adatti.
   5. Stampare una pagina di prova.
VERIFICA:
   È stato possibile stampare una pagina di prova sulla stampante di rete? SCOPO:
   Questo test verifica che l'orologio della Scrivania visualizzi correttamente la data e l'ora.
PASSI:
   1. Controllare l'orologio nell'angolo in alto a destra della Scrivania.
VERIFICA:
   L'orologio visualizza correttamente la data e l'ora del proprio fuso orario? SCOPO:
   Questo test verifica che l'orologio della Scrivania sia sincronizzato con l'orologio di sistema.
PASSI:
   1. Fare clic sul pulsante «Prova» e verificare che l'orologio si sposti avanti di 1 ora.
   Nota: l'aggiornamento dell'orologio potrebbe richiedere 1 minuto circa.
   2. Fare clic con il tasto destro sull'orologio, quindi fare clic su «Impostazioni data e ora...».
   3. Assicurarsi che l'impostazione dell'orologio sia su «Manualmente».
   4. Portare l'orologio un'ora indietro.
   5. Chiudere la finestra e riavviare.
VERIFICA:
   L'orologio di sistema visualizza correttamente la data e l'ora per il proprio fuso orario? SCOPO:
   Questo test verifica che sia possibile riavviare il sistema dal menù della Scrivania.
PASSI:
   1. Fare clic sull'icona dell'ingranaggio nell'angolo in alto a destra della Scrivania e fare clic su «Arresta».
   2. Fare clic sul pulsante «Riavvia» sul lato sinistro della finestra di arresto.
   3. Dopo aver effettuato nuovamente l'accesso, riavviare «Test del sistema» per ripartire da qui.
VERIFICA:
   Il sistema si è riavviato visualizzando correttamente l'interfaccia di accesso? SCOPO:
   Questo test verifica che il sistema sia in grado di riprodurre file audio Ogg Vorbis.
PASSI:
   1. Fare clic su «Prova» per riprodurre un file Ogg Vorbis (.ogg).
   2. Chiudere il riproduttore multimediale per proseguire.
VERIFICA:
   Il file campione è stato riprodotto correttamente? SCOPO:
   Questo test verifica che il sistema sia in grado di riprodurre file Wave Audio.
PASSI:
   1. Fare clic su «Prova» per riprodurre un file Wave Audio (.wav).
   2. Chiudere il riproduttore multimediale per proseguire.
VERIFICA:
   Il file campione è stato riprodotto correttamente? SCOPO:
 Se il ripristino è avvenuto correttamente, al riavvio di Checkbox verrà visualizzato questo test.
PASSI:
 1. Fare clic su «Sì».
VERIFICA:
 Non è necessaria alcuna verifica, questo è un finto test. SCOPO:
 Simulare un errore di sistema riavviando la macchina.
PASSI:
 1. Fare clic su «Prova» per eseguire un riavvio.
 2. Selezionare «Continua» dopo aver effettuato nuovamente l'accesso e riavviato Checkbox.
VERIFICA:
 All'utente non viene mostrata alcuna richiesta di verifica. SCOPO:
 Questo test verifica il funzionamento di gcalctool (calcolatrice).
PASSI:
 Fare clic sul pulsante «Prova» per aprire la calcolatrice ed eseguire:
  1. Taglia.
  2. Copia.
  3. Incolla.
VERIFICA:
 Le funzioni sono state eseguite come previsto? SCOPO:
 Questo test verifica il funzionamento di gcalctool (calcolatrice).
PASSI:
 Fare clic sul pulsante «Prova» per aprire la calcolatrice ed eseguire:
  1. Impostazione della memoria.
  2. Azzeramento della memoria.
  3. Pulizia dell'ultima memoria.
  4. Pulizia della memoria.
VERIFICA:
 Le funzioni sono state eseguite come previsto? SCOPO:
 Questo test verifica il funzionamento di gcalctool (calcolatrice).
PASSI:
 Fare clic sul pulsante «Prova» per aprire la calcolatrice ed eseguire:
 1. Funzioni matematiche semplici (+,-,/,*).
 2. Funzioni matematiche nidificate ((,)).
 3. Matematica con frazioni.
 4. Matematica decimale.
VERIFICA:
 Le funzioni sono state eseguite come previsto? SCOPO:
 Questo test verifica il funzionamento di gcalctool (calcolatrice).
PASSI:
 Fare clic sul pulsante «Prova» per aprire la calcolatrice.
VERIFICA:
 È stata avviata correttamente? SCOPO:
 Questo test verifica il funzionamento di gedit.
PASSI:
 1. Fare clic sul pulsante «Prova» per aprire gedit e riaprire il file creato precedentemente.
 2. Modificare e poi salvare il file, quindi chiudere gedit.
VERIFICA:
 Le operazioni sono state completate come previsto? SCOPO:
 Questo test verifica il funzionamento di gedit.
PASSI:
 1. Fare clic sul pulsante «Prova» per aprire gedit.
 2. Inserire del testo e salvare il file (prendere nota del nome usato per il file), quindi chiudere gedit.
VERIFICA:
 Le operazioni sono state completate come previsto? SCOPO:
 Questo test verifica la funzionalità del client di messaggistica Empathy.
PASSI:
 1. Selezionare «Prova» per avviare Empathy.
 2. Configurarlo per effettuare una connessione al servizio AIM (AOL Instant Messaging).
 3. Dopo aver completato il test, uscire da Empathy per continuare da qui.
VERIFICA:
 È stato possibile connettersi in modo corretto e inviare/ricevere messaggi? SCOPO:
 Questo test verifica la funzionalità del client di messaggistica Empathy.
PASSI:
 1. Selezionare «Prova» per avviare Empathy.
 2. Configurarlo per effettuare una connessione al servizio chat di Facebook.
 3. Dopo aver completato il test, uscire da Empathy per continuare da qui.
VERIFICA:
 È stato possibile connettersi in modo corretto e inviare/ricevere messaggi? SCOPO:
 Questo test verifica la funzionalità del client di messaggistica Empathy.
PASSI:
 1. Selezionare «Prova» per avviare Empathy.
 2. Configurarlo per effettuare una connessione al servizio gtalk (Google Talk).
 3. Dopo aver completato il test, uscire da Empathy per continuare da qui.
VERIFICA:
 È stato possibile connettersi in modo corretto e inviare/ricevere messaggi? SCOPO:
 Questo test verifica la funzionalità del client di messaggistica Empathy.
PASSI:
 1. Selezionare «Prova» per avviare Empathy.
 2. Configurarlo per effettuare una connessione al servizio Jabber.
 3. Dopo aver completato il test, uscire da Empathy per continuare da qui.
VERIFICA:
 È stato possibile connettersi in modo corretto e inviare/ricevere messaggi? SCOPO:
 Questo test verifica la funzionalità del client di messaggistica Empathy.
PASSI:
 1. Selezionare «Prova» per avviare Empathy.
 2. Configurarlo per effettuare una connessione al servizio MSN (Microsoft Network).
 3. Dopo aver completato il test, uscire da Empathy per continuare da qui.
VERIFICA:
 È stato possibile connettersi in modo corretto e inviare/ricevere messaggi? SCOPO:
 Questo test verifica le funzionalità di Evolution.
PASSI:
 1. Fare clic sul pulsante «Prova» per avviare Evolution.
 2. Configurarlo per effettuare una connessione a un account IMAP.
VERIFICA:
 È stato possibile ricevere e leggere le email in modo corretto? SCOPO:
 Questo test verifica il funzionamento di Evolution.
PASSI:
 1. Fare clic sull pulsante «Prova» per avviare Evolution.
 2. Configurarlo per effettuare una connessione a un account POP3.
VERIFICA:
 È stato possibile ricevere e leggere le email in modo corretto? SCOPO:
 Questo test verifica il funzionamento di Evolution.
PASSI:
 1. Fare clic sul pulsante «Prova» per avviare Evolution.
 2. Configurarlo per effettuare una connessione a un account SMTP.
VERIFICA:
 È stato possibile inviare una email senza errori? SCOPO:
 Questo test verifica che Firefox sia in grado di riprodurre un video Flash. Nota: per completare
 il test con successo potrebbe essere necessario installare software aggiuntivo.
PASSI:
 1. Selezionare «Prova» per avviare Firefox e visualizzare un breve video Flash.
VERIFICA:
 Il video è stato riprodotto correttamente? SCOPO:
 Questo test verifica che Firefox sia in grado di riprodurre un file video Quicktime (.mov).
 Nota: per completare il test con successo potrebbe essere necessario installare software
 aggiuntivo.
PASSI:
 1. Selezionare «Prova» per avviare Firefox con un video campione.
VERIFICA:
 Il video è stato riprodotto usando un plugin? SCOPO:
 Questo test verifica che Firefox sia in grado di visualizzare una pagina web di base.
PASSI:
 1. Selezionare «Prova» per avviare Firefox e visualizzare la pagina web di test.
VERIFICA:
 La pagina di test di Ubuntu è stata caricata in modo corretto? SCOPO:
 Questo test verifica che Firefox sia in grado di eseguire un'applet Java in una pagina web. Nota: per
 completare il test con successo potrebbe essere necessario installare software aggiuntivo.
PASSI:
 1. Selezionare «Prova» per avviare Firefox con la pagina di test Java e seguire le istruzioni a video.
VERIFICA:
 L'applet è stata visualizzata? SCOPO:
 Questo test verifica che Firefox sia in grado di eseguire applicazioni Flash. Nota: per completare
 il test con successo potrebbe essere necessario installare software aggiuntivo.
PASSI:
 1. Selezionare «Prova» per avviare Firefox e visualizzare un testo Flash campione.
VERIFICA:
 Il testo è stato visualizzato? SCOPO:
 Questo test verifica il funzionamento del terminale GNOME.
PASSI:
 1. Fare clic sul pulsante «Prova» per aprire il terminale.
 2. Digitare «ls» e premere Invio. Dovrebbe essere visualizzato un elenco di file e cartelle della propria directory Home.
 3. Chiudere la finestra del terminale.
VERIFICA:
 Le operazioni sono state completate come previsto? SCOPO:
 Questo test verifica che l'esploratore file riesca a copiare un file.
PASSI:
 1. Fare clic per aprire l'esploratore file.
 2. Fare clic con il tasto destro sul file di nome «File di prova 1», quindi fare clic su Copia.
 3. Fare clic con il tasto destro nello spazio bianco, quindi fare clic su Incolla.
 4. Fare clic sul file di nome «File di prova 1(copia)», quindi fare clic su Rinomina.
 5. Inserire «File di prova 2» come nome del file e premere Invio.
 6. Chiudere l'esploratore file.
VERIFICA:
 Ora è presente un file di nome «File di prova 2»? SCOPO:
 Questo test verifica che l'esploratore file riesca copiare una cartella.
PASSI:
 1. Fare clic su «Prova» per aprire l'esploratore file.
 2. Fare clic con il tasto destro sulla «Cartella di prova», quindi fare clic su Copia.
 3. Fare clic con il tasto destro su un'area vuota della finestra, quindi fare clic su Incolla.
 4. Fare clic con il tasto destro sulla «Cartella di prova(copia)», quindi fare clic su Rinomina.
 5. Inserire «Dati di prova» come nome della cartella e premere Invio.
 6. Chiudere l'esploratore file.
VERIFICA:
 È presente una cartella di nome «Dati di prova»? SCOPO:
 Questo test verifica che l'esploratore file riesca a creare un nuovo file.
PASSI:
 1. Fare clic su «Prova» per aprire l'esploratore file.
 2. Fare clic con il tasto destro nello spazio bianco, quindi fare clic su Crea documento → Documento vuoto.
 3. Inserire «File di prova 1» come nome per il file e premere Invio.
 4. Chiudere l'esploratore file.
VERIFICA:
 Ora è presente un file di nome «File di prova 1»? SCOPO:
 Questo test verifica che l'esploratore file riesca a creare una nuova cartella.
PASSI:
 1. Fae clic su «Prova» per aprire l'esploratore file.
 2. Nella barra dei menù, fare clic su File -> Crea cartella.
 3. Inserire «Cartella di prova» come nome della nuova cartella e premere Invio.
 4. Chiudere l'esploratore file.
VERIFICA:
 È presente una nuova cartella chiamata «Cartella di prova»? SCOPO:
 Questo test verifica che l'esploratore file riesca a eliminare un file.
PASSI:
 1. Fare clic su «Prova» per aprire l'esploratore file.
 2. Fare clic con il tasto destro sul file di nome «File di prova 1», quindi fare clic su Sposta nel cestino.
 3. Verificare che «File di prova 1» sia stato rimosso.
 4. Chiudere l'esploratore file.
VERIFICA:
  Il «File di prova 1» è stato eliminato? SCOPO:
 Questo test verifica che l'esploratore file riesca a eliminare una cartella.
PASSI:
 1. Fare clic su «Prova» per aprire l'esploratore file.
 2. Fare clic con il tasto destro sulla cartella di nome «Cartella di prova», quindi fare clic su Sposta nel cestino.
 3. Verificare che la cartella sia stata eliminata.
 4. Chiudere l'esploratore file.
VERIFICA:
 La «Cartella di prova» è stata eliminata con successo? SCOPO:
 Questo test verifica che l'esploratore file riesca a spostare un file.
PASSI:
 1. Fare clic su «Prova» per aprire l'esploratore file.
 2. Fare clic e trascinare il file di nome «File di prova 2» sopra l'icona della cartella di nome «Dati di prova».
 3. Rilasciare il tasto.
 4. Fare doppio clic sull'icona della cartella «Dati di prova» per aprirla.
 5. Chiudere l'esploratore file.
VERIFICA:
 Il «File di prova 2» è stato spostato con successo nella cartella «Dati di prova»? SCOPO:
 Questo test verifica che il «Gestore aggiornamenti» sia in grado di trovare aggiornamenti disponibili.
PASSI:
 1. Fare clic su «Prova» per avviare il «Gestore aggiornamenti».
 2. Seguire le indicazioni e installare gli aggiornamenti, se presenti.
 3. Quando il «Gestore aggiornamenti» ha completato, chiudere l'applicazione facendo clic sul pulsante «Chiudi» nell'angolo in basso a destra.
VERIFICA:
 «Gestore aggiornamenti» ha trovato e installato aggiornamenti (riuscito se non vengono trovati aggiornamenti,
 fallito se vengono trovati ma non installati). SCOPO:
 Questo test verifica che l'esploratore file riesca a spostare una cartella.
PASSI:
 1. Fare clic su «Prova» per aprire l'esploratore file.
 2. Fare clic e trascinare la cartella chiamata «Dati di prova» sopra l'icona di nome «Cartella di prova».
 3. Rilasciare il tasto.
 4. Fare doppio clic sulla cartella di nome «Cartella di prova» per aprirla.
 5. Chiudere l'esploratore file.
VERIFICA:
 La cartella «Dati di prova» è stata spostata con successo nella cartella di nome «Cartella di prova»? SCOPO:
 Scoprire alcune cose.
PASSI:
 1. Fare clic su «Sì».
VERIFICA:
 Non è necessaria alcuna verifica, questo è un finto test. Test di verifica dell'orologio del pannello Test di verifica del pannello di riavvio Analizza Xorg.0.Log e trova il driver X in esecuzione e la sua versione Test delle periferiche Test piglit Esegue il ping verso ubuntu.com e riavvia l'interfaccia di rete per 100 volte. Riprodurre un suono nell'uscita predefinita e ascoltare se viene riprodotto nell'ingresso predefinito. Scegliere (%s):  Premere ogni tasto sulla tastiera. Scrivere qui e premere Ctrl+D quando terminato:
 Test sui dispositivi di puntamento. Test di gestione dell'alimentazione Test della gestione dell'alimentazione Premere un tasto per continuare... Indietro Stampa le informazioni sulla versione ed esce. Fornisce informazioni sui monitor collegati al sistema. Fornisce informazioni sui dispositivi di rete. Segnale di uscita della tastiera Salva le impostazioni del mixer prima della sospensione. Salva la rete attuale prima della sospensione. Salva la risoluzione attuale prima della sospensione. Test rendercheck Riavvia Restituisce il nome del dispositivo e nome e versione del driver di tutti i touchpad rilevati nel sistema. Esegue il benchmark di lettura/modifica/scrittura di Cachebench. Esegue il benchmark di lettura di Cachebench. Esegue il benchmark di scrittura di Cachebench. Esegue il benchmark di compressione 7ZIP. Esegue il benchmark di compressione PBZIP2. Esegue il benchmark di codifica MP3. Esegue i test automatici di Firmware Test Suite (fwts). Esegue il benchmark GLmark2. Esegue il benchmark GLmark2-ES2. Esegue il benchmark GnuPG. Esegue il benchmark Himeno. Esegue il benchmark Lightsmark. Esegue il benchmark N-Queens. Esegue il benchmark Network Loopback. Esegue il benchmark gearsfancy OpenGL di Qgears2. Esegue il benchmark image scaling OpenGL di Qgears2. Esegue il benchmark gearsfancy delle XRender Extension di Qgears2. Esegue il benchmark image scaling delle XRender Extension di Qgears2. Esegue il benchmark XRender/Imlib2 di Render-Bench. Esegue il benchmark Stream Add. Esegue il benchmark Stream Copy. Esegue il benchmark Stream Scale. Esegue il benchmark Stream Triad. Esegue il benchmark Unigine Heaven. Esegue il benchmark Unigine Santuary. Esegue il benchmark Unigine Tropics. Esegue un test di stress basato su FurMark (OpenGL 2.1 o 3.2) a schermo intero 1920x1080 senza antialiasing Esegue un test di stress basato su FurMark (OpenGL 2.1 o 3.2) a finestre 1024x640 senza antialiasing Esegue gtkperf per assicurare il funzionamento dei casi di test basati su GTK. Esegue i test di stress grafici. Questo test può richiedere diversi minuti. Esegue il benchmark di codifica x264 H.264/AVC. Esecuzione di %s... Esegue un test che trasferisce a una scheda SDHC 100 file da 10 MB per 3 volte. Esegue un test che trasferisce a un supporto USB 100 file da 10 MB per 3 volte. Esegue l'intera suite di test «rendercheck». Questo test può durare diversi minuti. Esegue i test piglit per verificare il supporto a OpenGL 2.1 Esegue i test piglit per verificare il supporto alle operazioni GLSL di fragment shader Esegue i test piglit per verificare il supporto alle operazioni GLSL di vertex shader Esegue i test piglit per verificare il supporto alle operazioni di framebuffer object, buffer di stencil e profondità del buffer. Esegue i test piglit per verificare il supporto texture da pixmap Esegue i test piglit per verificare il supporto alle operazioni di vertex buffer object Esegue i test piglit per verificare il supporto alle operazioni di buffer di stencil Esegue lo strumento di riepilogo dei risultati di piglit Informazioni sui dispositivi SATA/IDE. Test SMART. Seleziona tutto Seleziona tutto Test dei servizi server Abbreviazione per --config=.*/jobs_info/blacklist. Abbreviazione per --config=.*/jobs_info/blacklist_file. Abbreviazione per --config=.*/jobs_info/whitelist. Abbreviazione per --config=.*/jobs_info/whitelist_file. Salta Test smoke Rileva i rilevatori Test di installazione del software Alcune nuove unità disco includono una funzionalità che permette di parcheggiare le testine dopo un breve periodo di inattività. Questa è una funzione di risparmio energetico, ma può interagire in maniera errata con il sistema operativo a causa di un continuo ciclo di inattività/attivazione. Ciò comporta una eccessiva usura dell'unità che potrebbe portare a guasti prematuri. Spazio quando terminato Avvia test Stato Ferma il processo Stop digitato da tty Spegnimento di sistema sotto stress (100 cicli) Riavvio di sistema sotto stress (100 cicli) Test di stress Dettagli sull'invio Invia risultati Test terminati con successo. Test di sospensione Test di sospensione Test del 
sistema Test dei demoni di sistema Test del sistema Scheda 1 Scheda 2 Segnale di termine Prova Esegue il test ACPI Wakealarm (fwts wakealarm). Prova ancora Esegue un test sulla memoria e la mantiene in esercizio. Test annullato Esegue il test del clock jitter. Verifica se il demone atd è in esecuzione quando il pacchetto viene installato. Verifica se il demone cron è in esecuzione quando il pacchetto viene installato. Verifica se il demone cupsd è in esecuzione quando il pacchetto viene installato. Verifica se il demone getty è in esecuzione quando il pacchetto viene installato. Verifica se il demone init è in esecuzione quando il pacchetto viene installato. Verifica se il demone klogd è in esecuzione quando il pacchetto viene installato. Verifica se il demone nmbd è in esecuzione quando il pacchetto viene installato. Verifica se il demone smbd è in esecuzione quando il pacchetto viene installato. Verifica se il demone syslogd è in esecuzione quando il pacchetto viene installato. Verifica se il demone udevd è in esecuzione quando il pacchetto viene installato. Verifica se il demone winbindd è in esecuzione quando il pacchetto viene installato. Test interrotto Esegue un test sulle CPU offline in un sistema multicore. Verifica che la directory /var/crash non contenga nulla. Elenca i file contenuti all'interno se sono presenti, oppure riporta lo stato della directory (non esiste/è vuota) Verifica che X non sia in esecuzione in modalità grafica sicura. Verifica che il processo X sia in esecuzione. Verifica la capacità di scalamento della CPU usando Firmware Test Suite (fwts cpufreq). Test della rete dopo il ripristino. Test per controllare che un'immagine cloud si avvii e funzioni correttamente con KVM. Test che verifica il supporto alla virtualizzazione e che il sistema abbia una quantità di RAM minima che permetta di usarlo come nodo di calcolo OpenStack. Test per rilevare periferiche audio. Test per rilevare le schede di rete disponibili. Test di rilevamento delle unità ottiche. Test per determinare se il sistema offre la possibilità di eseguire macchine virtuali KVM con accelerazione hardware. Test per visualizzare la versione di Xorg Test di verifica della sincronizzare dell'orologio locale con un server NTP. Test di verifica del mantenimento della risoluzione dopo il ripristino. Verificato Verifica la configurazione OEM usando Xpresser e controlla quindi che l'utente sia stato creato correttamente. Cancella l'utente appena creato dopo che il test è stato superato con successo. Verifica che il dispositivo senza fili del sistema possa connettersi a un router usando la sicurezza WPA e i protocolli 802.11b/g dopo la sospensione del sistema. Verifica che il dispositivo senza fili del sistema possa connettersi a un router usando la sicurezza WPA e i protocolli 802.11b/g. Verifica che il dispositivo senza fili del sistema possa connettersi a un router usando la sicurezza WPA e il protocollo 802.11n dopo la sospensione del sistema. Verifica che il dispositivo senza fili del sistema possa connettersi a un router usando la sicurezza WPA e il protocollo 802.11n. Verifica che il dispositivo senza fili del sistema possa connettersi a un router senza alcuna sicurezza usando i protocolli 802.11b/g dopo la sospensione del sistema. Verifica che il dispositivo senza fili del sistema possa connettersi a un router senza alcuna sicurezza usando i protocolli 802.11b/g. Verifica che il dispositivo senza fili del sistema possa connettersi a un router senza alcuna sicurezza usando il protocollo 802.11n dopo la sospensione del sistema. Verifica che il dispositivo senza fili del sistema possa connettersi a un router senza alcuna sicurezza e usando il protocollo 802.11n. Verifica le prestazioni di una connessione senza fili del sistema attraverso lo strumento iperf, usando pacchetti UDP. Verifica le prestazioni di una connessione senza fili del sistema attraverso lo strumento iperf. Verifica che apt possa accedere ai repository e scaricare gli aggiornamenti (non vengono installati). Serve per confermare la possibilità di poter ripristinare un aggiornamento incompleto o danneggiato. Verifica la presenza di una connessione a Internet funzionante. TextLabel Il file in cui scrivere il log. È stato generato il seguente report da inviare al database dell'hardware di Launchpad:

  [[%s|Visualizza report]]

Queste informazioni sul sistema possono essere inviate fornendo l'indirizzo email utilizzato per accedere a Launchpad. Se non si possiede un account Launchpad, registrarsi qui:

  https://launchpad.net/+login Il report generato sembra avere dei problemi di validazione,
potrebbe non essere elaborato da Launchpad. C'è un altra istanza del programma in esecuzione. Interromperla. Questo test automatico prova a rilevare una webcam. Allega le schermate provenienti dal test suspend/cycle_resolutions_after_suspend_auto ai risultati da inviare. Questa è una versione completamente automatica di mediacard/sd-automated e presuppone che il sistema da sottoporre a verifica sia dotato di un dispositivo per memory card connesso prima dell'esecuzione di Checkbox.  È destinato a test automatici di SRU. Questo è un test automatico di trasmissione file Bluetooth. Invia un'immagine al dispositivo specificato dalla variabile ambiente BTDEVADDR. Questo è un test automatico Bluetooth. Emula l'esplorazione su un dispositivo remoto specificato dalla variabile ambiente BTDEVADDR. Questo è un test automatico Bluetooth. Riceve il file fornito dal dispositivo remoto specificato dalla variabile ambiente BTDEVADDR. Questo è un test automatico per raccogliere informazioni sullo stato attuale dei dispositivi di rete. Se non vengono trovati, il test termina con errore. Questo è un test automatico che esegue operazioni di lettura/scrittura su un disco rigido Firewire collegato. Questo è un test automatico che esegue operazioni in lettura e scrittura su un disco rigido eSATA collegato. Questo test è la versione automatica di usb/storage-automated e presume che il server abbia dei dispositivi di memorizzazione USB collegati prima dell'esecuzione di Checkbox. È destinato a test automatici di server e di SRU. Questo test è la versione automatica di usb3/storage-automated e presuppone che il server abbia dei dispositivi di memorizzazione USB 3.0 collegati prima dell'esecuzione di Checkbox. È destinato a test automatici di server e di SRU. Questa è la versione automatica di suspend/suspend_advanced. Questo test controlla la topologia della CPU per la precisione. Questo test verifica che i controllori di frequenza della CPU rispettino le impostazioni. Questo test controlla che l'interfaccia senza fili funzioni dopo la sospensione del sistema. Disconnette tutte le interfacce, effettua la connessione all'interfaccia senza fili e verifica che la connessione stessa funzioni come atteso. Questo test verifica che la quantità di memoria riportata da meminfo corrisponda alla dimensione dei moduli di memoria rilevati da DMI. Questo test termina tutte le connessioni per poi connettersi all'interfaccia senza fili. Controlla quindi la connessione stessa per confermare che funzioni nel modo atteso. Questo test prende l'indirizzo hardware dell'adattatore Bluetooth dopo la sospensione e lo compare con quello rilevato prima della sospensione. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/cf-insert. Verifica la lettura e la scrittura sulla scheda CF. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/cf-insert-after-suspend. Verifica la lettura e la scrittura sulla scheda CF stessa dopo una sospensione del sistema. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/mmc-insert. Verifica la lettura e la scrittura sulla scheda MMC. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/mmc-insert-after-suspend. Verifica la lettura e la scrittura sulla scheda MMC dopo una sospensione del sistema. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/ms-insert. Verifica la lettura e la scrittura su una scheda MS. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/ms-insert-after-suspend. Verifica la lettura e la scrittura su una scheda MS dopo una sospensione del sistema. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/msp-insert. Verifica la lettura e la scrittura su una scheda MSP. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/msp-insert-after-suspend. Verifica la lettura e la scrittura su una scheda MSP dopo una sospensione del sistema. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/sd-insert. Verifica la lettura e la scrittura sulla scheda SD. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/sd-insert-after-suspend. Verifica la lettura e la scrittura sulla scheda SD dopo una sospensione del sistema. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/sdhc-insert. Verifica la lettura e la scrittura sulla scheda SDHC. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/sdhc-insert-after-suspend. Verifica la lettura e la scrittura sulla scheda SDHC dopo una sospensione del sistema. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/sdxc-insert. Verifica la lettura e la scrittura su una scheda SDXC. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/sdxc-insert-after-suspend. Verifica la lettura e la scrittura su una scheda SDXC dopo una sospensione del sistema. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/xd-insert. Verifica la lettura e la scrittura su una scheda xD. Questo test è automatico e viene avviato dopo l'esecuzione di mediacard/xd-insert-after-suspend. Verifica la lettura e la scrittura su una scheda xD dopo una sospensione del sistema. Questo test è automatico ed viene avviato dopo l'esecuzione di usb/insert. Questo test è automatico ed viene avviato dopo l'esecuzione di usb3/insert. Questo test verifica il funzionamento delle modalità video supportate dopo una sospensione e un ripristino del sistema. Viene eseguito in maniera automatica catturando alcune schermate e caricandole come allegati. Questo test verifica che i livelli di volume del proprio sistema siano accettabili, controllando che il volume sia maggiore o uguale al valore minimo e minore o uguale al valore massimo per tutte le sorgenti (ingressi) e i sink (uscite) riconosciuti da PulseAudio. Verificherà inoltre che la sorgente e il sink attivi non siano muti. Evitare di regolare manualmente il volume prima di eseguire questo test. Verranno allegati tutti i registri del test di gestione-alimentazione/arresto ai risultati. Verranno allegati tutti i registri del test di gestione-alimentazione/riavvio ai risultati. Questo test verifica che il proprio dispositivo audio funzioni in maniera corretta dopo una sospensione e un ripristino. Può funzionare bene con altoparlanti e microfoni interni, tuttavia funziona meglio usando un cavo che connette il jack di uscita audio al jack di ingresso audio. Verranno eseguiti alcuni test di connettività di base rispetto a BMC, per verificare che IPMI funzioni. Segnale del timer da alarm(2) Test del touchpad Test del touchscreen Prova ad abilitare una stampante remota nella rete e stampa una pagina di prova. Digitare testo SCONOSCIUTO Test USB Test delle porte USB Segnale sconosciuto Non verificato Uso: checkbox [OPZIONI] Applicazioni utente Segnale 1 definito dall'utente Segnale 2 definito dall'utente Conferma che l'unità vettoriale a virgola mobile sia in esecuzione su un dispositivo ARM Verifica che il server DNS sia in esecuzione e funzioni. Verifica che il server di stampa/CUPS sia in esecuzione. Verifica che il server Samba sia in esecuzione. Verifica che il server Tomcat sia in esecuzione e funzioni. Verifica che sshd sia in esecuzione. Verifica che lo stack LAMP sia in esecuzione (Apache, MySQL e PHP). Verifica che i supporti di memorizzazione esterni USB3 abbiano una prestazione pari o superiore a quella base. Verifica che i supporti di memorizzazione del sistema abbiano una prestazione pari o superiore a quella base. Verifica che tutte le CPU siano online dopo il ripristino. Verifica che tutta la memoria sia disponibile dopo il ripristino dalla sospensione. Verifica che tutte le CPU siano online prima della sospensione. Verifica che una installazione di checkbox-server nella rete possa essere raggiunta tramite SSH. Verifica che le impostazioni del mixer dopo la sospensione siano uguali a quelle precedenti. Verifica che i dispositivi di memorizzazione come Fibre Channel e RAID possano essere rilevati ed eseguiti in condizioni di stress. Visualizza risultati Test sulla virtualizzazione Benvenuti al test del sistema.

Checkbox fornisce dei test per confermare il corretto funzionamento del sistema. Alla fine dell'esecuzione dei test potrà essere visualizzato un report riassuntivo del sistema stesso. Test reti senza fili Test delle reti senza fili Test di ricerca delle reti senza fili. Ricerca e crea un resoconto sui punti di accesso trovati. Elaborazione in corso È inoltre possibile chiudere la finestra premendo «Esc» o «Ctrl+C». _Deseleziona tutti _Esci _Fine _No _Indietro _Seleziona tutti Sa_lta questo test _Prova _Prova ancora _Sì prima dello scaricamento e la capacità. Allega il contenuto dei vari file di configurazione di sysctl. Test dei dischi eSATA prima dello scaricamento e la capacità. http://wiki.ubuntu-it.org/Installazione/RequisitiDiSistema Installa l'archivio tar dell'installatore di bootchart, se presente. no salta prova prova ancora Input da tty per un processo nello sfondo Output da tty per un processo nello sfondo rimanente prima dello scaricamento e la capacità. È necessario impostare MOVIE_VAR. sì 