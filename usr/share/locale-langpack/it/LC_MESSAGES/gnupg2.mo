��    `     #  �  F      p]  -   q]    �]     �^  �   �^     �_     �_     �_     �_     �_     `     6`     S`  2   p`  7   �`  2   �`  #   a  D   2a  .   wa  I   �a  7   �a     (b     Eb     Qb     ib     �b     �b     �b     �b     �b     c     (c  +   :c  &   fc  #   �c  (   �c     �c     �c     d     ,d  (   Hd     qd     �d     �d  $   �d     �d     e  &   e     ?e  ,   Ze     �e     �e  !   �e     �e     �e     f     )f     <f     Hf     ef     qf     �f     �f     �f     �f     �f     �f     g  *   ,g  "   Wg  ]   zg     �g     �g     h      h     3h     Mh  !   ih  ,   �h  '   �h     �h     �h  ,   i     Bi     ]i  B   yi  =   �i     �i  "   j  %   <j  &   bj  !   �j  %   �j  "   �j  #   �j  '   k      @k     ak     ~k     �k     �k     �k     �k  (   �k  $   l     <l     Ql  #   el     �l  $   �l     �l     �l  !   m     -m     Jm      im     �m  &   �m  %   �m     �m  ,   �m     (n     ?n     Tn     rn     �n  '   �n     �n     �n     �n     o     "o     5o      Ho  '   io     �o  H   �o  	  �o     �p     q     q     !q  	   ?q     Iq     eq     �q     �q  W   �q  D   �q  -   <r  0   jr  .   �r  I   �r  *   s  +   ?s  '   ks     �s     �s     �s     �s     �s     �s     �s     t     4t     ;t     Ut     lt  3   �t     �t  4   �t  -   �t     #u  '   5u  .   ]u     �u  $   �u  	   �u     �u     �u     �u  ?   �u  4   7v  :   lv     �v     �v     �v      w     w  8   /w  1   hw     �w     �w     �w  #   �w  &   x  (   2x  &   [x     �x     �x     �x     �x  7   �x  6   �x  -   #y  3   Qy  9   �y  0   �y  <   �y  G   -z  @   uz  >   �z  +   �z  =   !{     _{     o{     w{  "   �{  :   �{     �{     |  #   |     @|     R|     e|  -   z|  $   �|  8   �|  &   }  5   -}  .   c}  5   �}  *   �}  !   �}  .   ~  0   D~  &   u~  ,   �~     �~     �~  
   �~  #   
     .     G     X  "   _  �   �  H   �  "   d�  (   ��  $   ��  E   Հ  1   �     M�     c�     ��     ��     ��  8   Ձ     �     "�     :�     O�  �   o�     �     ,�     ?�     [�     n�     ��     ��  B   ��     ��     �     %�     7�  %   N�     t�     |�     ��     ��     ��  .   Ԅ      �  (   $�      M�  
   n�     y�     |�     ��     ��     ��     ǅ     ��     �     �     (�     B�     ]�     q�  �   }�  "   �     (�     E�     W�     t�     ��     ��     Ǉ  F   ه       �  1   A�  %   s�     ��     ��     ��  %   ��     ވ     ��     	�     �  �   5�  E   ԉ  E   �  p   `�      ъ  -   �  �    �     ��  =   Ћ  I   �  S   X�  >   ��  <   �  5   (�  N   ^�  U   ��  )   �  c   -�     ��  D   ��  !   ��  #   �  -   <�  +   j�  )   ��  &   ��  �   �    ʐ     ё     �     �      �     2�     B�     K�     d�     m�  +   ��  ,   ��  3   �     �  1   "�     T�  (   j�  #   ��  +   ��  "   �  +   �  "   2�      U�     v�  ?   ��  %   ʔ     �     �     �     (�     8�     O�     l�     ��  
   ��  .   ��  ,   �  (   �      8�  }  Y�     ח     ��     �     $�  /   >�  0   n�     ��  !   ��     ژ     ߘ     �     ��  !   �     2�     H�     ^�     w�     ��  %   ��     Ι     �  	   ��      �     	�  j   (�  K   ��  F   ߚ  K   &�     r�  @   ��  -   ț  9   ��  0   0�  /   a�  5   ��  '   ǜ  .   �  3   �  -   R�     ��     ��     ��  !   ɝ  $   �  ,   �     =�     \�     |�     ��     ��     ɞ     ֞  =   �  =   &�  0   d�  +   ��     ��     ݟ     ��      �     3�     L�     h�      ��     ��  6   ��  %   ��     �  2   8�     k�  r   �  >   ^�  1   ��  =   Ϣ  I   �  4   W�     ��  #   ��  ?   ϣ  D   �  @   T�  +   ��  1   ��  N   �  I   B�  I   ��  %   ֥  ;   ��  ?   8�  H   x�  -   ��  "   �  H   �  �   [�     �  B    �  )   C�  C   m�  .   ��  >   �  2   �  =   R�  <   ��  @   ͩ  F   �  H   U�  U   ��  $   ��  -   �  0   G�  0   x�  4   ��     ޫ  :   ߬  6   �  5   Q�  .   ��     ��     ֭     ܭ     �  *   ��  1   )�  #   [�  2   �  �   ��  H   ?�  *   ��  =   ��  0   �  9   "�  "   \�  &   �  !   ��  >   Ȱ  %   �  ,   -�  5   Z�     ��     ��  _   ��  
   �  
   �  
   '�  
   2�  
   =�     H�  
   \�  
   g�     r�  	   y�     ��     ��  
   ��     ��  8   ��  S   �  =   E�  5   ��  4   ��  +   �     �     3�     B�     W�     d�     r�  .   ��     ��  
   ̴     ״     �     �  B   ,�     o�  5   ��     ��     ٵ     �     �     �     &�  (   )�     R�     k�     ��  ,   ��  %   Ӷ  2   ��  +   ,�  5   X�     ��  
   ��     ��     ��  ;   ̷  <   �  *   E�     p�  	   ��     ��     ��     ̸     �     ��     �     '�     A�     Z�  :   p�     ��     ˹     �     ��     �      �  &   1�     X�     n�  -   ��     ��     պ  %   �  <   	�  A   F�  (   ��     ��  "   ǻ     �     	�     %�     1�     M�     f�  1   ~�     ��     Ǽ     ޼     ��     �     �  ,   7�     d�  H   ��     ˽     �     �  *   ��  %   '�  /   M�     }�     ��     ��  &   Ӿ  %   ��      �     8�     S�     q�  3   ��  
   Ŀ     Ͽ     �     ��     �     �     ;�  0   Y�     ��  $   ��  %   ��  #   ��  -   �     D�  "   c�  #   ��  (   ��     ��  #   ��  (   �     ;�     R�     r�     ��  0   ��  !   ��  &   ��  *   &�     Q�  $   h�     ��     ��     ��     ��  /   ��     ��     �  ,   $�  /   Q�  "   ��  K   ��  (   ��  (   �  %   B�  $   h�  &   ��  $   ��     ��  D   ��     ;�  '   B�  1   j�  $   ��     ��  @   ��     	�     %�     *�  !   B�     d�     v�     ��  
   ��      ��  #   ��  6   ��  0   (�     Y�  !   b�  '   ��     ��     ��  /   ��     �  "   "�  &   E�  (   l�  %   ��  %   ��     ��  $   ��     �  .   %�     T�     r�     ��     ��     ��  2   ��     �     �     3�     K�  /   f�     ��  +   ��     ��  $   ��  &   �  $   3�  '   X�  ,   ��     ��     ��  >   ��     �      �     8�     P�  $   h�  ,   ��     ��     ��     ��  "   �  #   8�      \�  !   }�     ��     ��     ��     ��  *   �  -   9�  $   g�  '   ��      ��  .   ��  +   �  )   0�  #   Z�     ~�  -   ��  %   ��  6   ��  5   )�  /   _�     ��  "   ��  '   ��     ��  ;   �  0   Q�  "   ��  %   ��  /   ��  2   ��     .�  !   B�     d�  :   �  !   ��  !   ��  (   ��  +   '�  8   S�     ��  4   ��     ��     ��  2   �  6   G�  )   ~�  9   ��     ��     ��  G   �  H   U�  B   ��     ��     ��     �     ;�     Y�     v�     ��     ��  (   ��     ��     �      �     8�     L�     j�  	   ��     ��     ��     ��     ��     ��     �     �     5�     O�  '   `�     ��     ��     ��     ��  (   ��  2   ��  !    �     B�      S�     t�  ,   ��     ��     ��     ��  
   ��     ��     �     �     +�  %   >�  ,   d�     ��  &   ��  "   ��     ��     �     �     +�  !   K�     m�     ��     ��  %   ��     ��     ��  
   �     �     1�  &   4�  	   [�  )   e�     ��  $   ��     ��     ��     �  6   '�     ^�     z�  #   ��     ��  -   ��     �     �     8�     P�  "   f�     ��  *   ��     ��  !   ��  $   �     2�  2   D�     w�     z�     �  I   ��  F   ��  
   �     *�     I�     \�     t�  0   ��  1   ��  #   ��  #   �  +   :�  ,   f�  (   ��  2   ��     ��  $   
�  *   /�  2   Z�  .   ��     ��     ��     ��  *   	�     4�     <�     Z�     f�     }�     ��     ��     ��     ��     ��  #   �     A�     [�     k�  3   |�  %   ��  *   ��  %   �     '�  (   =�  -   f�  9   ��     ��     ��  6   �  :   ;�     v�  ,   ��  0   ��  /   ��     �  7   -�  *   e�  .   ��  7   ��  ;   ��  0   3�  1   d�  ;   ��     ��  %   ��     �  )   �  3   I�  6   }�  :   ��  
   ��     ��     �  9    �     Z�  5   y�  -   ��  "   ��      �       �     A�  -   U�     ��     ��     ��      ��  5   ��  $   �     4�  +   O�  $   {�  '   ��  0   ��  4   ��  %   .�  C   T�  *   ��  0   ��     ��     ��  .   �  -   I�  *   w�  �   ��  (   3�  +   \�  G   ��  2   ��  /   �  %   3�  F   Y�  #   ��     ��  !   ��     �  -    �  /   N�  ,   ~�  	   ��  "   ��  )   ��     �     !�      <�     ]�  2   w�  1   ��     ��     ��  #   �  -   &�     T�  :   s�  A   ��  "   ��     �  	   %�     /�     B�     J�     a�  #   ��     ��     ��      ��     ��     ��  	   �     #�     A�     T�  6   l�     ��      ��     ��     ��     �     �  *   /�  !   Z�     |�     ��  G   ��     ��     ��      �     &�  &   >�     e�  -   ��  ,   ��     ��     ��     �     '�     ?�     R�     U�  E   Y�  7   ��  6   ��  7   �  =   F�  >   ��     ��  $   ��     �     �  9   5�      o�     ��  )   ��     ��  $   ��     �  '   7�  
   _�  %   j�  '   ��  %   ��  $   ��     �     �  '   3�     [�     p�     ��     ��  *   ��  *   ��  �  	�  3   ��         - �   E *   $     O    p !   � "   �    �    �     :   0 =   k 7   � 7   � @    7   Z J   � E   �    #    = #   I &   m *   �     �     �     )       H    _ .   m )   � %   � .   �        5 !   M    o .   �    �     �    � &       :    R !   n     � !   �     �    � $   	    1	    G	    a	    z	    �	     �	    �	    �	     �	    
    6
    I
 #   [
 $   
 #   �
 7   �
 ,     m   -    �    �    �    � "   �      ;   < :   x B   � $   �      5   <    r    � I   � K   �    7 4   U 9   � >   � 3    ;   7 2   s 2   � 8   � .    $   A    f *   ~    �    �    � +   � *       H    _ &   y #   � )   � *   � *    /   D $   t &   � "   � $   � !    /   *    Z B   j    �    �    �    �     )       I    \    x    �    �    �     � '   �     Q   .    �    �    �    � %   �    � '     +   (    T    q O   z @   �     )   (    R E   k    �    �    � 	        
   (    3    E    \ "   { %   �    � $   � '   �     ;   6 	   r =   | 8   �    � 5    8   > 	   w 3   � 
   �    � %   �    � A   � 9   @ 8   z '   � !   �    �        7 B   M A   � *   � $   � '   " (   J *   s /   � +   �    �    �         N   . 1   } )   � *   � 5     -   :  =   h  >   �  4   �  +   !    F! D   a!    �!    �! !   �! 5   �! B   "    Y"    y" )   �"    �"    �"    �" /   # /   ;# A   k# &   �# >   �# 5   $ C   I$ 0   �$ #   �$ 6   �$ :   %    T% :   r%    �%    �%    �% -   �%    !&    6&    M& 3   T& �   �& [   ' 6   {' :   �' +   �' B   ( '   \(    �( "   �(    �(    �( %   �( ?   )    ])    t)    �)    �) �   �)    d*    �*    �*    �* $   �* *   �*    + R   ;+    �+    �+    �+ 3   �+ K   ,    _, &   k,    �,    �,    �, O   �, (   +- -   T- 3   �- 
   �-    �- &   �-    �-    .    !. "   ;.    ^. &   v.    �.    �.     �. "   �.    / �   -/ +   �/     �/    0 #   10    U0    l0 #   �0    �0 n   �0 (   41 <   ]1 0   �1    �1    �1    �1 1   �1 &   #2    J2    b2 #   n2 �   �2 =   Q3 I   �3 �   �3 #   _4 -   �4 �   �4    T5 8   q5 K   �5 X   �5 8   O6 7   �6 7   �6 a   �6 h   Z7 (   �7 s   �7 %   `8 C   �8    �8 &   �8 "   9 +   39    _9 '   9 �   �9 �   �: $   �;    �; "   �;    <    <    &< $   :< 	   _<    i< 5   �< >   �< 8   �<    4= 0   A=    r= ,   �= -   �= ;   �= ,   "> :   O> +   �> -   �>    �> H   �> *   G?    r?    �?    �?    �? *   �? &   �? &    @ )   G@    q@ C   �@ =   �@ <   A    @A �  _A !   �B     C    3C "   PC =   sC B   �C '   �C *   D    GD    MD    ]D    fD    xD    �D    �D    �D 3   �D    E 2   E    ME    gE    {E    �E $   �E t   �E Z   ,F d   �F W   �F    DG E   _G 7   �G 4   �G 5   H 4   HH :   }H (   �H 0   �H <   I :   OI    �I #   �I    �I    �I 2   J ;   6J     rJ    �J    �J    �J '   �J    K    K <   +K >   hK 1   �K 1   �K $   L    0L    OL %   lL #   �L     �L    �L !   �L    M 6   3M )   jM    �M @   �M �   �M �   �N J   6O B   �O C   �O J   P C   SP *   �P 2   �P E   �P e   ;Q T   �Q 2   �Q 6   )R ]   `R W   �R X   S 0   oS ]   �S W   �S Y   VT 8   �T 4   �T E   U �   dU )   V W   9V 7   �V T   �V ;   W W   ZW P   �W D   X C   HX \   �X S   �X c   =Y r   �Y 0   Z B   EZ A   �Z >   �Z F   	[ /  P[ A   �\ <   �\ 8   �\ 4   8] /   m]    �]    �] (   �] ,   �] A   �] /   @^ A   p^ �   �^ M   B_ 5   �_ S   �_ J   ` =   e` -   �` /   �` 2   a O   4a /   �a -   �a 6   �a    b    %b l   1b    �b    �b    �b    �b    �b    �b 
   c    c 	   c    %c    5c    >c    Kc 	   Zc J   dc d   �c @   d <   Ud B   �d ;   �d #   e    5e    Pe    ne    �e .   �e @   �e    f    $f &   2f '   Yf )   �f \   �f    g B    g &   cg    �g    �g    �g    �g    �g 7   �g !   h )   7h (   ah 6   �h 5   �h ?   �h 3   7i J   ki    �i 
   �i    �i    �i K   �i P   Cj 0   �j *   �j    �j     �j    k %   8k #   ^k    �k    �k ,   �k -   �k    l G   /l !   wl    �l    �l    �l    �l    �l .   m    ?m #   Vm ;   zm    �m    �m (   �m M   n X   dn +   �n    �n +   �n 2   +o !   ^o 
   �o     �o    �o    �o G   �o )   )p    Sp    pp    �p "   �p !   �p ,   �p +   q N   Dq *   �q    �q    �q B   �q -   #r -   Qr    r &   �r    �r +   �r +   s    4s "   Hs    ks    �s 2   �s    �s    �s    t )   #t    Mt    Zt #   vt =   �t    �t '   �t &   u -   Au 4   ou $   �u )   �u <   �u E   0v &   vv /   �v 8   �v    w ,   %w 0   Rw #   �w 5   �w ,   �w 4   
x ,   ?x $   lx .   �x &   �x    �x    �x    �x C   y    Ly (   ^y 5   �y 0   �y 1   �y i    z 0   �z 3   �z .   �z ;   { 7   Z{ 6   �{ ,   �{ y   �{    p| 3   x| D   �| -   �|    } L   '} !   t}    �} !   �}    �}    �} &   �}     ~    :~     G~ "   h~ O   �~ D   �~      #   ) )   M    w (   � 9   �    � 1   � 1   H� 2   z� ,   �� )   ڀ 
   � /   � !   ?� 5   a� "   �� (   ��    � *   �� "   !� 7   D� #   |�    �� #   �� (   � E   
�    P� 0   n�    �� -   �� 0   �� +   � *   =� 4   h�    ��    �� 2   Є    � -   �    5�    R� 7   q� ?   ��    � %   	� #   /�     S�     t� $   �� $   ��    ߆ $   �� "   #�    F� /   d� 0   �� -   Ň /   � (   #� 7   L� =   �� 9    2   �� /   /� 2   _� 1   �� <   ĉ :   � 5   <� 6   r� /   �� 9   ي "   � T   6� B   �� E   ΋ :   � V   O� P   ��    �� )   � #   >� ;   b� *   �� +   ɍ ;   �� :   1� <   l� $   �� ?   Ύ "   � $   1� A   V� =   �� 6   ֏ J   �    X� +   o� ]   �� ]   �� O   W� (   �� '   Б 0   �� 4   )� .   ^� +   �� +   �� 3   � 3   �    M�    _�    |�    �� (   ��     �    � '   �    <� "   X�    {�    ��    ��     ϔ    �    � .   � $   H�    m�    u� 	   �� +   �� 0   �� %   �    � ,   %� "   R� 9   u�    ��    ��    ̖    Ж 7   ٖ    � %   � #   :� <   ^� A   �� +   ݗ 4   	� *   >�    i�    ��    �� ,   �� <   ߘ ,   �    I� 3   b� ?   ��    ֙    �    �    �    %�    (�    H� +   P�    |� 0   ��    ̚ 2   � +   � >   K�    ��    �� (   Û    � -   �    5�    N�    k�    �� *   �� &   Ϝ -   �� #   $� 4   H� 2   }�    �� E   ʝ    �    �    � _   /� J   ��    ڞ /   �    �    5�    L� >   h� A   �� *   � )   � =   >� E   |� (     L   �     8� +   Y� '   �� 2   �� 6   �    � -   .�    \� +   {�    �� '   ��    آ !   � )   �    1� )   L�    v� &   �� $   �� 0   У    �    �    6� ?   N� 3   �� 6   ¤ 0   ��    *� 4   F� <   {� J   ��    � *   � :   F� :   ��    �� 2   ڦ 3   � 1   A� '   s� 8   �� 5   ԧ 6   
� D   A� E   �� @   ̨ ?   � ?   M� %   �� 6   ��    � :   �� 3   9� @   m� C   ��    �    �    � Q   ;� *   �� <   �� 3   ��    )�    I� *   c�    �� J   ��    �    ��    
� (   #� F   L� '   ��    �� )   ֭ %    � &   &� 5   M� 7   �� !   �� `   ݮ 9   >� @   x� 
   �� $   į >   � ?   (� 5   h� �   �� '   -� <   U� I   �� 6   ܱ 5   � 4   I� Q   ~� ,   в    �� "   �    ?� 6   \� 6   �� 8   ʳ    � (   � 3   8� &   l�     �� &   �� +   ۴ I   � N   Q�    �� &   �� *   е :   �� %   6� V   \� i   �� -   �    K�    a�    n�    �� "   �� *   �� &   ܷ    �    � /   0�    `� "   ��    ��    ��    ɸ    � =   � (   ?� "   h�    ��    ��    �� )   Թ 8   ��    7�    W�    e� V   n� #   ź    � &   ��    #� )   ?� +   i� 8   �� :   λ    	� 2   (�    [�    y�    ��    ��    �� R   �� G   � =   N� >   �� D   ˽ E   �    V� 7   q�    �� $   ƾ ?   � *   +�    V� +   u�    �� .   �� '   � ,   �    C� .   P� <   � 5   �� $   ��    �     6� 0   W�    ��    ��    ��    �� (   �� 0   �        )        �  �   f  �               �  }  s  �      �   �  �  6  �      #   �  ~  �  ;   z  �   �   �   �      !   3  �  �  �         !  W  `  �  g  �      ^  �  _  �      �      �    �  �  �     >       p  S  �  �  Z  /  �     L          2  B       �   E  �  �      �  �      *  ^            �  �                                   �          `  �  �         @  �          s      <  9  a   �  �  @        �                M  -    S  �  �   {              �  i  �    b     y  �   >  2   �    Y   �      	      �  5  :      �  �      n      =                 �  �  �      [      �  �  �  ?   �  �  S  N  �      u   �  �  �      �  �            8  D  �  �   V   }   �      #    e       "  |      �    �  �  �   -  �  (    ?  �  \        �      �  �  �  �      3  0   t  �  �      *  �  �   _  �  �         +  �  �      o  �  2    �      �         �                            �      �   �   �  �      g   5   �   o      �       �  2          
      �  c      3           �  �     !      �      �  .           �    a  �  '  �   J      4      �  [   �      V  j  �  �             L  �  H  U    �   �   �   9                c  (  5      W  �   �                   �  �  �   r  �  �  G    =   h  $      �  i  V  %   _  ?        3  �  I      �  \              �      �      �  x     �  �  -  p  �      �  "  I  Q  c      �  P  �      �      L      �  /   7  �  �  �  E  $          4  T       �      �  �      N  �   �  �      �  �   M    Z   h      �  �  �  �      }  �  f    �      �      y           �  �                �  �      :    P  �      1    C  �   &      �   �  W  �                  &  6      U  8  �           �      �  �  �   P   j      �   �   H  �   d  �      �      �   �   j  �      �  N          �  ,      �       P  4  �  ,   �   D   �       T  �      �  K  �  <      �  �    �  �     �      �   �   �     �  �     �  q  '  �   �   �   �   �    V  �            �          $     �   n  t     5  �  8   l  �         �   B  �    �  �  A  �  �     �  �  d  �      �      �       H               �  �  �  `  �   �       �     B      �  1      2  k   �  �  �  I  Y  �   �   h            
          B  �      �      4      r      �       1      B  P  S  �   e  A  z         L       \  J         �  �  �  �  O       �   �     �  �      �  �  n   ;    
  �  �   Q    �      �  �  �     �      W   �   �   A   ^  �  >  0      �      �  .  T  9   �      @       d   �   �    �  �  �   =  N     +  �  �   9     �  ?  #      o   &  �  ;      �  �  �   �  T         
  C   x  m  y  7          �              �      �   �  t    �  �  �  <  �  U  /  �  �      *        !          e  Y  ?  �           $   �  �  �     0  |  s  �          �   M      �  �  W    =              7  X  �                          O    G   /          M          +                �  �               5      K      n  ]      �  �  �  9  �      �      6  �  |   F          d  p       S       F  (  �  �  *  X      �   �  4      �  8      �  �      �   �   �   w  �     �  �      �  �  �  �  Z  v  {          �          �  �  �        �  $    �  .  �   �  J  ]       U   �                 �      .      �      r   <   Q  h      R  �      .  O  l      �  �      C          \  �           �  �      [  �   �  �  �                �   �      {       j   �  �   %       b          X   �  3      �      1   �  �  �   +   �  �   T  ~                  e          �              �  �  �  -    <  �  7   �                   �  �  �       �               %      �  >  �    �   w  `     u  �  #          �   R      �   R   J  >  %  f  �   m  �  �  �   �  \   �  �      k  a          )      �   �  m       �   �   J   G      v  �     �  C      #      �  )  N              D      F   w     �  A  �  O  �      �  o  m  �  }  Q  �  �   �   �    �   b  �       �  '   `  �   �   A  l      �  z  '    E  �      �  �  �  �  �  �  Z  �   �  _   1    u     �      a  �  �  �  �      Z  �    �  	  �  �      �  q             �      7  l       x      �       �  H          +  �       �   �      �    �  �  �      �      �  �   �   R      r  "  �        t  !    �  _  &          �      F  �   �        �   �  �     �  �  )   �  "          �  ]  ~      �     :   �       �  �      I   M                 �   *                 X  ]                  �            �  ]  �          G  ;        /  K     z  �  �           �   �      :  �          �   [  v  �  �      @  C              �      H  �  �  i  �  :  �  ^      R  �      �          �            ^   �      �  g  �       k  ;  E  0  ,  (   '  ~       �      �      �  �  U      �   	       b  f   O  @      s   x  6     i   X  q   D  |  Y     �           w  Q       6   V  �   �  E     8      �          =  D  �   L        &   �   )  �              �        v       u  ,        -   	        	  �              "   �   c           �        q  p         �       g  �  �   �   F  �  ,  �  �     I  �   (              �  �      �      �  �  
  �  �      �  G  �  �  �  �      k  �      �  %      �  y  �       Y  �   0  �  K  �      �  {    �      [    �  �   K   
Enter the user ID.  End with an empty line:  
Pick an image to use for your photo ID.  The image must be a JPEG file.
Remember that the image is stored within your public key.  If you use a
very large picture, your key will become very large as well!
Keeping the image close to 240x288 is a good size to use.
 
Supported algorithms:
 
You need a user ID to identify your key; the software constructs the user ID
from the Real Name, Comment and Email Address in this form:
    "Heinrich Heine (Der Dichter) <heinrichh@duesseldorf.de>"

                 aka "%s"                using %s key %s
               imported: %lu              unchanged: %lu
            new subkeys: %lu
           new user IDs: %lu
           not imported: %lu
           w/o user IDs: %lu
          "%s": preference for cipher algorithm %s
          "%s": preference for compression algorithm %s
          "%s": preference for digest algorithm %s
          (subkey on main key ID %s)          It is not certain that the signature belongs to the owner.
          The signature is probably a FORGERY.
          There is no indication that the signature belongs to the owner.
          This could mean that the signature is forged.
         new signatures: %lu
       "%s"
       Card serial no. =       Key fingerprint =       Subkey fingerprint:       secret keys read: %lu
       skipped new keys: %lu
       user IDs cleaned: %lu
      Subkey fingerprint:     signatures cleaned: %lu
    (%c) Finished
    (%c) Toggle the authenticate capability
    (%c) Toggle the encrypt capability
    (%c) Toggle the sign capability
    (%d) DSA (set your own capabilities)
    (%d) DSA (sign only)
    (%d) DSA and Elgamal
    (%d) Elgamal (encrypt only)
    (%d) RSA (encrypt only)
    (%d) RSA (set your own capabilities)
    (%d) RSA (sign only)
    (%d) RSA and RSA (default)
    (0) I will not answer.%s
    (1) I have not checked at all.%s
    (1) Signature key
    (2) Encryption key
    (2) I have done casual checking.%s
    (3) Authentication key
    (3) I have done very careful checking.%s
    new key revocations: %lu
   %d = I do NOT trust
   %d = I don't know or won't say
   %d = I trust fully
   %d = I trust marginally
   %d = I trust ultimately
   Unable to sign.
   aka "%s"
   m = back to the main menu
   q = quit
   s = skip this key
   secret keys imported: %lu
  (main key ID %s)  (non-exportable)  (non-revocable)  Primary key fingerprint:  secret keys unchanged: %lu
 "%s" not a key ID: skipping
 "%s" was already locally signed by key %s
 "%s" was already signed by key %s
 # List of assigned trustvalues, created %s
# (Use "gpg --import-ownertrust" to restore them)
 %lu keys processed so far
 %s %s stopped
 %s does not yet work with %s
 %s encrypted data
 %s encrypted session key
 %s encryption will be used
 %s is too old (need %s, have %s)
 %s keys may be between %u and %u bits long.
 %s keysizes must be in the range %u-%u
 %s makes no sense with %s!
 %s not allowed with %s!
 %s%%0A%%0AUse the reader's pinpad for input. %s/%s encrypted for: "%s"
 %s/%s signature from: "%s"
 %s: There is limited assurance this key belongs to the named user
 %s: There is no assurance this key belongs to the named user
 %s: directory does not exist!
 %s: error reading free record: %s
 %s: error reading version record: %s
 %s: error updating version record: %s
 %s: error writing dir record: %s
 %s: error writing version record: %s
 %s: failed to append a record: %s
 %s: failed to create hashtable: %s
 %s: failed to create version record: %s %s: failed to zero a record: %s
 %s: invalid file version %d
 %s: invalid trustdb
 %s: invalid trustdb created
 %s: keyring created
 %s: not a trustdb file
 %s: skipped: %s
 %s: skipped: public key already present
 %s: skipped: public key is disabled
 %s: trustdb created
 %s: unknown suffix
 %s: version record with recnum %lu
 %s:%d: deprecated option "%s"
 %s:%d: invalid auto-key-locate list
 %s:%d: invalid export options
 %s:%d: invalid import options
 %s:%d: invalid keyserver options
 %s:%d: invalid list options
 %s:%d: invalid verify options
 %u-bit %s key, ID %s, created %s (No description given)
 (Probably you want to select %d here)
 (This is a sensitive revocation key)
 (sensitive) (unless you specify the key by fingerprint)
 --clearsign [filename] --decrypt [filename] --edit-key user-id [commands] --encrypt [filename] --lsign-key user-id --output doesn't work for this command
 --passwd <user-id> --sign --encrypt [filename] --sign --symmetric [filename] --sign [filename] --sign-key user-id --store [filename] --symmetric --encrypt [filename] --symmetric --sign --encrypt [filename] --symmetric [filename] @
(See the man page for a complete listing of all commands and options)
 @
Examples:

 -se -r Bob [file]          sign and encrypt for user Bob
 --clearsign [file]         make a clear text signature
 --detach-sign [file]       make a detached signature
 --list-keys [names]        show keys
 --fingerprint [names]      show fingerprints
 @
Options:
  @Commands:
  @Options:
  ASCII armored output forced.
 Admin PIN Admin commands are allowed
 Admin commands are not allowed
 Admin-only command
 Allow An ssh process requested the use of key%%0A  %s%%0A  (%s)%%0ADo you want to allow this? Are you sure that you want to sign this key with your
key "%s" (%s)
 Are you sure you still want to add it? (y/N)  Are you sure you still want to revoke it? (y/N)  Are you sure you still want to sign it? (y/N)  Are you sure you want to appoint this key as a designated revoker? (y/N)  Are you sure you want to delete it? (y/N)  Are you sure you want to replace it? (y/N)  Are you sure you want to use it? (y/N)  Authenticate BAD signature from "%s" Bad PIN Bad Passphrase CA fingerprint:  CRC error; %06lX - %06lX
 CRL/OCSP check of certificates Can't check signature: %s
 Cancel Cardholder's given name:  Cardholder's surname:  Certificate chain valid Certificates leading to an ultimately trusted key:
 Certify Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?  Change (N)ame, (C)omment, (E)mail or (Q)uit?  Change passphrase Changing expiration time for a subkey.
 Changing expiration time for the primary key.
 Cipher:  Command expects a filename argument
 Comment:  Compression:  Configuration for Keyservers Correct Create a designated revocation certificate for this key? (y/N)  Create a revocation certificate for this key? (y/N)  Create a revocation certificate for this signature? (y/N)  Critical preferred keyserver:  Critical signature notation:  Critical signature policy:  Current allowed actions:  Current recipients:
 DSA requires the hash length to be a multiple of 8 bits
 DSA requires the use of a 160 bit hash algorithm
 Data decryption succeeded Data signing succeeded Data verification succeeded Delete this good signature? (y/N/q) Delete this invalid signature? (y/N/q) Delete this key from the keyring? (y/N)  Delete this unknown signature? (y/N/q) Deny Detached signature.
 Digest:  Dirmngr usable Displaying %s photo ID of size %ld for key %s (uid %d)
 Do you really want to delete the selected keys? (y/N)  Do you really want to delete this key? (y/N)  Do you really want to revoke the entire key? (y/N)  Do you really want to revoke the selected subkeys? (y/N)  Do you really want to revoke this subkey? (y/N)  Do you really want to set this key to ultimate trust? (y/N)  Do you want to issue a new signature to replace the expired one? (y/N)  Do you want to promote it to a full exportable signature? (y/N)  Do you want to promote it to an OpenPGP self-signature? (y/N)  Do you want to sign it again anyway? (y/N)  Do you want your signature to expire at the same time? (Y/n)  Email address:  Encrypt Encryption algorithm supported Enter JPEG filename for photo ID:  Enter an optional description; end it with an empty line:
 Enter new filename Enter new passphrase Enter number(s), N)ext, or Q)uit >  Enter passphrase
 Enter passphrase:  Enter the notation:  Enter the user ID of the designated revoker:  Enter your preferred keyserver URL:  Error: Combined name too long (limit is %d characters).
 Error: Double spaces are not allowed.
 Error: Login data too long (limit is %d characters).
 Error: Only plain ASCII is currently allowed.
 Error: Private DO too long (limit is %d characters).
 Error: The "<" character may not be used.
 Error: The trustdb is corrupted.
 Error: URL too long (limit is %d characters).
 Error: invalid characters in preference string.
 Error: invalid formatted fingerprint.
 Error: invalid length of preference string.
 Error: invalid response.
 Expired signature from "%s" Features:  Go ahead and type your message ...
 Good signature from "%s" Gpg-Agent usable Hash:  Hint: Select the user IDs to sign
 How carefully have you verified the key you are about to sign actually belongs
to the person named above?  If you don't know what to answer, enter "0".
 How much do you trust that this key actually belongs to the named user?
 I have checked this key casually.
 I have checked this key very carefully.
 I have not checked this key at all.
 IDEA cipher unavailable, optimistically attempting to use %s instead
 If that does not work, please consult the manual
 Included certificates Invalid character in comment
 Invalid character in name
 Invalid characters in PIN Invalid command  (try "help")
 Invalid key %s made valid by --allow-non-selfsigned-uid
 Invalid selection.
 Is this correct? (y/N)  Is this okay? (y/N)  Is this photo correct (y/N/q)?  It is NOT certain that the key belongs to the person named
in the user ID.  If you *really* know what you are doing,
you may answer the next question with yes.
 Key %s is already revoked.
 Key available at:  Key does not expire at all
 Key expires at %s
 Key generation canceled.
 Key generation failed: %s
 Key has been compromised Key has only stub or on-card key items - no passphrase to change.
 Key is no longer used Key is revoked. Key is superseded Key is valid for? (0)  Key not changed so no update needed.
 Keyring Keyserver no-modify LDAP server list Language preferences:  Login data (account name):  Make off-card backup of encryption key? (Y/n)  Name may not start with a digit
 Name must be at least 5 characters long
 Need the secret key to do this.
 NnCcEeOoQq No No audit log entries. No fingerprint No help available No reason specified No subkey with index %d
 No such user ID.
 No trust value assigned to:
 No user ID with hash %s
 No user ID with index %d
 Not a valid email address
 Not signed by you.
 Notations:  Note that this key cannot be used for encryption.  You may want to use
the command "--edit-key" to generate a subkey for this purpose.
 Note: This key has been disabled.
 Note: This key has expired!
 Nothing deleted.
 Nothing to sign with key %s
 Number of recipients OpenPGP card no. %s detected
 OpenPGP card not available: %s
 Overwrite? (y/N)  Owner trust may not be set while using a user provided trust database
 PIN callback returned error: %s
 PIN for CHV%d is too short; minimum length is %d
 PIN not correctly repeated; try again PIN too long PIN too short PUK PUK not correctly repeated; try again Parsing data succeeded Passphrase too long Passphrase: Please correct the error first
 Please decide how far you trust this user to correctly verify other users' keys
(by looking at passports, checking fingerprints from different sources, etc.)
 Please don't put the email address into the real name or the comment
 Please enter a domain to restrict this signature, or enter for none.
 Please enter a passphrase to protect the received secret key%%0A   %s%%0A   %s%%0Awithin gpg-agent's key storage Please enter name of data file:  Please enter the PIN%s%s%s to unlock the card Please enter the depth of this trust signature.
A depth greater than 1 allows the key you are signing to make
trust signatures on your behalf.
 Please enter the new passphrase Please enter the passphrase for the ssh key%%0A  %F%%0A  (%c) Please enter the passphrase or the PIN
needed to complete this operation. Please enter the passphrase to protect the imported object within the GnuPG system. Please enter the passphrase to protect the new PKCS#12 object. Please enter the passphrase to unprotect the PKCS#12 object. Please enter the passphrase to%0Aprotect your new key Please enter your PIN, so that the secret key can be unlocked for this session Please enter your passphrase, so that the secret key can be unlocked for this session Please insert the card with serial number Please note that the shown key validity is not necessarily correct
unless you restart the program.
 Please re-enter this passphrase Please remove the current card and insert the one with serial number Please report bugs to <@EMAIL@>.
 Please select exactly one user ID.
 Please select the reason for the revocation:
 Please select the type of key to generate:
 Please select what kind of key you want:
 Please select where to store the key:
 Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
 Please specify how long the signature should be valid.
         0 = signature does not expire
      <n>  = signature expires in n days
      <n>w = signature expires in n weeks
      <n>m = signature expires in n months
      <n>y = signature expires in n years
 Possible actions for a %s key:  Preferred keyserver:  Primary key fingerprint: Private DO data:  Proceed? (y/N)  Pubkey:  Public key is disabled.
 Quality: Quit without saving? (y/N)  RSA modulus missing or not of size %d bits
 RSA prime %s missing or not of size %d bits
 RSA public exponent missing or larger than %d bits
 Real name:  Really create the revocation certificates? (y/N)  Really create? (y/N)  Really delete this self-signature? (y/N) Really move the primary key? (y/N)  Really remove all selected user IDs? (y/N)  Really remove this user ID? (y/N)  Really revoke all selected user IDs? (y/N)  Really revoke this user ID? (y/N)  Really sign all user IDs? (y/N)  Really sign? (y/N)  Really update the preferences for the selected user IDs? (y/N)  Really update the preferences? (y/N)  Reason for revocation: %s
 Recipient %d Repeat this PIN Repeat this PUK Repeat this Reset Code Replace existing key? (y/N)  Replace existing keys? (y/N)  Requested keysize is %u bits
 Reset Code Reset Code is too short; minimum length is %d
 Reset Code not correctly repeated; try again Reset Code not or not anymore available
 Revocation certificate created.
 Revocation certificate created.

Please move it to a medium which you can hide away; if Mallory gets
access to this certificate he can use it to make your key unusable.
It is smart to print this certificate and store it away, just in case
your media become unreadable.  But have some caution:  The print system of
your machine might store the data and make it available to others!
 Root certificate trustworthy SETERROR %s (try %d of %d) Save changes? (y/N)  Secret key is available.
 Secret parts of primary key are not available.
 Secret parts of primary key are stored on-card.
 Set preference list to:
 Sex ((M)ale, (F)emale or space):  Sign Sign it? (y/N)  Signature %d Signature available Signature does not expire at all
 Signature expired %s
 Signature expires %s
 Signature expires at %s
 Signature is valid for? (%s)  Signature made %s
 Signature made %s using %s key ID %s
 Signature notation:  Signature policy:  Signer %d SsEeAaQq Subkey %s is already revoked.
 Syntax: gpg-check-pattern [options] patternfile
Check a passphrase given on stdin against the patternfile
 Syntax: gpg-preset-passphrase [options] KEYGRIP
Password cache maintenance
 Syntax: gpg-protect-tool [options] [args]
Secret key maintenance tool
 Syntax: gpgv [options] [files]
Check signatures against known trusted keys
 Take this one anyway The card will now be re-configured to generate a key of %u bits
 The minimum trust level for this key is: %s

 The self-signature on "%s"
is a PGP 2.x-style signature.
 The signature will be marked as non-exportable.
 The signature will be marked as non-revocable.
 There are no preferences on a PGP 2.x-style user ID.
 This JPEG is really large (%d bytes) !
 This command is not allowed while in %s mode.
 This command is only available for version 2 cards
 This is a secret key! - really delete? (y/N)  This key belongs to us
 This key has been disabled This key has expired! This key is due to expire on %s.
 This key may be revoked by %s key %s This key probably belongs to the named user
 This signature expired on %s.
 This will be a self-signature.
 To be revoked by:
 Total number processed: %lu
 URL to retrieve public key:  Uncompressed Unknown operation Usage: gpg-check-pattern [options] patternfile (-h for help)
 Usage: gpg-preset-passphrase [options] KEYGRIP (-h for help)
 Usage: gpg-protect-tool [options] (-h for help)
 Usage: gpgv [options] [files] (-h for help) Use this key anyway? (y/N)  User ID "%s" compacted: %s
 User ID "%s" is expired. User ID "%s" is not self-signed. User ID "%s" is revoked. User ID "%s" is signable.   User ID "%s": already clean
 User ID "%s": already minimized
 User ID is no longer valid WARNING: "%s" is a deprecated command - do not use it
 WARNING: "%s" is a deprecated option
 WARNING: %s overrides %s
 WARNING: Elgamal sign+encrypt keys are deprecated
 WARNING: This is a PGP 2.x-style key.  Adding a designated revoker may cause
         some versions of PGP to reject this key.
 WARNING: This is a PGP2-style key.  Adding a photo ID may cause some versions
         of PGP to reject this key.
 WARNING: This key has been revoked by its designated revoker!
 WARNING: This key has been revoked by its owner!
 WARNING: This key is not certified with a trusted signature!
 WARNING: This key is not certified with sufficiently trusted signatures!
 WARNING: This subkey has been revoked by its owner!
 WARNING: Using untrusted key!
 WARNING: We do NOT trust this key!
 WARNING: a user ID signature is dated %d seconds in the future
 WARNING: appointing a key as a designated revoker cannot be undone!
 WARNING: cipher algorithm %s not found in recipient preferences
 WARNING: digest algorithm %s is deprecated
 WARNING: encrypted message has been manipulated!
 WARNING: forcing compression algorithm %s (%d) violates recipient preferences
 WARNING: forcing digest algorithm %s (%d) violates recipient preferences
 WARNING: forcing symmetric cipher %s (%d) violates recipient preferences
 WARNING: invalid notation data found
 WARNING: key %s may be revoked: fetching revocation key %s
 WARNING: key %s may be revoked: revocation key %s not present.
 WARNING: message was encrypted with a weak key in the symmetric cipher.
 WARNING: message was not integrity protected
 WARNING: multiple plaintexts seen
 WARNING: multiple signatures detected.  Only the first will be checked.
 WARNING: no user ID has been marked as primary.  This command may
              cause a different user ID to become the assumed primary.
 WARNING: nothing exported
 WARNING: potentially insecure symmetrically encrypted session key
 WARNING: program may create a core file!
 WARNING: recipients (-r) given without using public key encryption
 WARNING: signature digest conflict in message
 WARNING: signing subkey %s has an invalid cross-certification
 WARNING: signing subkey %s is not cross-certified
 WARNING: the signature will not be marked as non-exportable.
 WARNING: the signature will not be marked as non-revocable.
 WARNING: this key might be revoked (revocation key not present)
 WARNING: unable to %%-expand notation (too large).  Using unexpanded.
 WARNING: unable to %%-expand policy URL (too large).  Using unexpanded.
 WARNING: unable to %%-expand preferred keyserver URL (too large).  Using unexpanded.
 WARNING: unable to fetch URI %s: %s
 WARNING: unable to refresh key %s via %s: %s
 WARNING: using experimental cipher algorithm %s
 WARNING: using experimental digest algorithm %s
 WARNING: using experimental public key algorithm %s
 We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
 What keysize do you want for the Authentication key? (%u)  What keysize do you want for the Encryption key? (%u)  What keysize do you want for the Signature key? (%u)  What keysize do you want for the subkey? (%u)  What keysize do you want? (%u)  Wrong Yes Yes, protection is not needed You are about to revoke these signatures:
 You can't change the expiration date of a v3 key
 You can't delete the last user ID!
 You did not specify a user ID. (you may use "-r")
 You have not entered a passphrase - this is in general a bad idea!%0APlease confirm that you do not want to have any protection on your key. You have not entered a passphrase!%0AAn empty passphrase is not allowed. You have signed these user IDs on key %s:
 You may not add a designated revoker to a PGP 2.x-style key.
 You may not add a photo ID to a PGP2-style key.
 You may try to re-create the trustdb using the commands:
 You must select at least one key.
 You must select at least one user ID.
 You must select exactly one key.
 You need a passphrase to unlock the secret key for
user: "%s"
 You selected this USER-ID:
    "%s"

 Your current signature on "%s"
has expired.
 Your current signature on "%s"
is a local signature.
 Your decision?  Your selection?  Your system can't display dates beyond 2038.
However, it will be correctly handled up to 2106.
 [  full  ] [  undef ] [ expired] [ revoked] [ unknown] [User ID not found] [filename] [marginal] [none] [not set] [revocation] [self-signature] [ultimate] [uncertain] a gpg-agent is already running - not starting a new one
 a notation name must have only printable characters or spaces, and end with an '='
 a notation name must not contain more than one '@' character
 a notation value must not use any control characters
 a user notation name must contain the '@' character
 access to admin commands is not configured
 add a key to a smartcard add a photo ID add a revocation key add a subkey add a user ID allow presetting passphrase anonymous recipient; trying secret key %s ...
 armor header:  armor: %s
 assume no on most questions assume yes on most questions assuming %s encrypted data
 assuming bad signature from key %s due to an unknown critical bit
 attr hash algorithm: %s automatically retrieve keys when verifying signatures bad data hash algorithm: %s batch mode: never ask be somewhat more quiet binary build_packet failed: %s
 cC can't access %s - invalid OpenPGP card?
 can't create socket: %s
 can't disable core dumps: %s
 can't do this in batch mode
 can't do this in batch mode without "--yes"
 can't handle public key algorithm %d
 can't handle text lines longer than %d characters
 can't handle this ambiguous signature data
 can't use a symmetric ESK packet due to the S2K mode
 canceled by user
 cancelled
 cancelled by user
 cancel|cancel cannot appoint a PGP 2.x style key as a designated revoker
 cannot avoid weak key for symmetric cipher; tried %d times!
 card does not support digest algorithm %s
 card is permanently locked!
 card-no:  certificate has been revoked certificate has expired change URL to retrieve key change a CA fingerprint change a card's PIN change a passphrase change card holder's name change card holder's sex change data on a card change the expiration date for the key or selected subkeys change the language preferences change the login name change the ownertrust change the passphrase check all programs check signatures checking created signature failed: %s
 checking the trustdb
 child aborted with status %i
 cipher algorithm %d%s is unknown or disabled
 class %s is not supported
 colon missing communication problem with gpg-agent
 compact unusable user IDs and remove all signatures from key compact unusable user IDs and remove unusable signatures from key completes-needed must be greater than 0
 conflicting commands
 could not open %s for writing: %s
 could not parse keyserver URL
 create ascii armored output created: %s creation timestamp missing
 csh-style command output data hash algorithm: %s data not saved; use option "--output" to save it
 dearmoring failed: %s
 decrypt data (default) decryption failed: %s
 decryption okay
 delete selected subkeys delete selected user IDs delete signatures from the selected user IDs deleting keyblock failed: %s
 depth: %d  valid: %3d  signed: %3d  trust: %d-, %dq, %dn, %dm, %df, %du
 detected card with S/N: %s
 disable key disabled disallow clients to mark keys as "trusted" display photo IDs during key listings display photo IDs during signature verification do not detach from the console do not grab keyboard and mouse do not make any changes do not update the trustdb after import do not use the PIN cache when signing do not use the SCdaemon does not match - try again don't use the terminal at all either %s or %s must be given
 elevate the trust of signatures with valid PKA data enable key enable putty support enable ssh support enarmoring failed: %s
 encrypt data encrypted with %lu passphrases
 encrypted with %s key, ID %s
 encrypted with %u-bit %s key, ID %s, created %s
 encrypted with 1 passphrase
 encrypted with unknown algorithm %d
 encryption only with symmetric cipher error allocating enough memory: %s
 error changing size of key %d to %u bits: %s
 error creating passphrase: %s
 error creating temporary file: %s
 error getting current key info: %s
 error getting key usage information: %s
 error getting new PIN: %s
 error getting nonce for the socket
 error getting serial number of card: %s
 error in trailer line
 error reading application data
 error reading fingerprint DO
 error reading keyblock: %s
 error reading list of trusted root certificates
 error reading nonce on fd %d: %s
 error retrieving CHV status from card
 error while asking for the passphrase: %s
 error writing key: %s
 error writing to temporary file: %s
 existing key will be replaced
 expired expired: %s expires: %s export attribute user IDs (generally photo IDs) export keys export keys to a key server export revocation keys marked as "sensitive" export signatures that are marked as local-only exporting secret keys not allowed
 external program calls are disabled due to unsafe options file permissions
 failed to acquire the pinentry lock: %s
 failed to create stream from socket: %s
 failed to initialize the TrustDB: %s
 failed to rebuild keyring cache: %s
 failed to store the creation date: %s
 failed to store the fingerprint: %s
 failed to store the key: %s
 failed to use default PIN as %s: %s - disabling further default use
 female fetch the key specified in the card URL fingerprint on card does not match requested one
 flag the selected user ID as primary forced forcing symmetric cipher %s (%d) violates recipient preferences
 fstat(%d) failed in %s: %s
 full generate a new key pair generate a revocation certificate generate new keys generating key failed
 generating new key
 gpg/card>  handler 0x%lx for fd %d started
 handler 0x%lx for fd %d terminated
 honor the PKA record set on a key when retrieving keys honor the preferred keyserver URL set on the key iImMqQsS ignore requests to change the TTY ignore requests to change the X display ignoring garbage line import keys from a key server import signatures that are marked as local-only import/merge keys importing secret keys not allowed
 include revoked keys in search results include subkeys when searching by key ID input line %u too long or missing LF
 input line longer than %d characters
 invalid invalid S2K mode; must be 0, 1 or 3
 invalid armor header:  invalid armor: line longer than %d characters
 invalid auto-key-locate list
 invalid clearsig header
 invalid command invalid dash escaped line:  invalid default preferences
 invalid default-cert-level; must be 0, 1, 2, or 3
 invalid export options
 invalid fingerprint invalid import options
 invalid keyserver options
 invalid keyserver protocol (us %d!=handler %d)
 invalid list options
 invalid min-cert-level; must be 1, 2, or 3
 invalid option invalid personal cipher preferences
 invalid personal compress preferences
 invalid personal digest preferences
 invalid radix64 character %02X skipped
 invalid structure of OpenPGP card (DO 0x93)
 invalid value
 invalid verify options
 it is strongly suggested that you update your preferences and
 key key "%s" not found on keyserver
 key "%s" not found: %s
 key %s has no user IDs
 key %s marked as ultimately trusted
 key %s occurs more than once in the trustdb
 key %s: "%s" %d new signatures
 key %s: "%s" %d new subkeys
 key %s: "%s" %d new user IDs
 key %s: "%s" %d signature cleaned
 key %s: "%s" %d signatures cleaned
 key %s: "%s" %d user ID cleaned
 key %s: "%s" %d user IDs cleaned
 key %s: "%s" 1 new signature
 key %s: "%s" 1 new subkey
 key %s: "%s" 1 new user ID
 key %s: "%s" not changed
 key %s: "%s" revocation certificate added
 key %s: "%s" revocation certificate imported
 key %s: PGP 2.x style key - skipped
 key %s: PKS subkey corruption repaired
 key %s: accepted as trusted key
 key %s: accepted non self-signed user ID "%s"
 key %s: can't locate original keyblock: %s
 key %s: can't read original keyblock: %s
 key %s: direct key signature added
 key %s: doesn't match our copy
 key %s: duplicated user ID detected - merged
 key %s: invalid direct key signature
 key %s: invalid revocation certificate: %s - rejected
 key %s: invalid revocation certificate: %s - skipped
 key %s: invalid self-signature on user ID "%s"
 key %s: invalid subkey binding
 key %s: invalid subkey revocation
 key %s: key material on-card - skipped
 key %s: new key - skipped
 key %s: no public key - can't apply revocation certificate
 key %s: no public key for trusted key - skipped
 key %s: no subkey for key binding
 key %s: no subkey for key revocation
 key %s: no subkey for subkey binding signature
 key %s: no subkey for subkey revocation signature
 key %s: no user ID
 key %s: no user ID for signature
 key %s: no valid user IDs
 key %s: non exportable signature (class 0x%02X) - skipped
 key %s: public key "%s" imported
 key %s: public key not found: %s
 key %s: removed multiple subkey binding
 key %s: removed multiple subkey revocation
 key %s: revocation certificate at wrong place - skipped
 key %s: secret key imported
 key %s: secret key with invalid cipher %d - skipped
 key %s: skipped subkey
 key %s: skipped user ID "%s"
 key %s: subkey signature in wrong place - skipped
 key %s: unexpected signature class (0x%02X) - skipped
 key %s: unsupported public key algorithm
 key %s: unsupported public key algorithm on user ID "%s"
 key already exists
 key export failed: %s
 key has been created %lu second in future (time warp or clock problem)
 key has been created %lu seconds in future (time warp or clock problem)
 key is not flagged as insecure - can't use it with the faked RNG!
 key not found on keyserver
 key operation not possible: %s
 keyserver receive failed: %s
 keyserver refresh failed: %s
 keyserver search failed: %s
 keyserver send failed: %s
 keysize invalid; using %u bits
 keysize rounded up to %u bits
 libgcrypt is too old (need %s, have %s)
 line too long line too long - skipped
 list all available data list all components list and check key signatures list key and user IDs list keys list keys and fingerprints list keys and signatures list preferences (expert) list preferences (verbose) list secret keys listen() failed: %s
 make a clear text signature make a detached signature make a signature make timestamp conflicts only a warning make_keysig_packet failed: %s
 male malformed CRC
 marginal marginals-needed must be greater than 1
 max-cert-depth must be in the range from 1 to 255
 menu to change or unblock the PIN missing argument move a backup key to a smartcard move a key to a smartcard moving a key signature to the correct place
 nN nested clear text signatures
 never never      next trustdb check due at %s
 no no CRL found for certificate no class provided
 no gpg-agent running in this session
 no keyserver known (use option --keyserver)
 no need for a trustdb check
 no remote program execution supported
 no revocation keys found for "%s"
 no secret key
 no signature found
 no signed data
 no suitable card key found: %s
 no ultimately trusted keys found
 no valid OpenPGP data found.
 no valid addressees
 no writable keyring found: %s
 no writable public keyring found: %s
 not a detached signature
 not an OpenPGP card not forced not human readable oO okay, we are the anonymous recipient.
 okay|okay old encoding of the DEK is not supported
 old style (PGP 2.x) signature
 only accept updates to existing keys original file name='%.*s'
 ownertrust information cleared
 ownertrust value missing passphrase generated with unknown digest algorithm %d
 pinentry.qualitybar.tooltip please do a --check-trustdb
 please see %s for more information
 please use "%s%s" instead
 please wait while key is being generated ...
 premature eof (in CRC)
 premature eof (in trailer)
 premature eof (no CRC)
 print the card status problem handling encrypted packet
 prompt before overwriting public and secret key created and signed.
 public key %s not found: %s
 public key decryption failed: %s
 public key encrypted data: good DEK
 public key is %s
 public key of ultimately trusted key %s not found
 qQ quit quit this menu quoted printable character in armor - probably a buggy MTA has been used
 re-distribute this key to avoid potential algorithm mismatch problems
 read error reading public key failed: %s
 reading stdin ...
 reason for revocation:  receiving line failed: %s
 remove as much as possible from key after import remove as much as possible from key during export remove keys from the public keyring remove keys from the secret keyring remove unusable parts from key after import remove unusable parts from key during export removing stale lockfile (created by %d)
 repair damage from the pks keyserver during import requesting key %s from %s
 requesting key %s from %s server %s
 response does not contain the RSA modulus
 response does not contain the RSA public exponent
 response does not contain the public key data
 revocation comment:  revoke key or selected subkeys revoke selected user IDs revoke signatures on the selected user IDs revoked revoked by your key %s on %s
 revoked: %s rounded up to %u bits
 run in daemon mode (background) run in server mode run in server mode (foreground) save and quit search for keys on a key server secret key "%s" not found: %s
 secret key parts are not available
 seems to be not encrypted select subkey N select user ID N selected certification digest algorithm is invalid
 selected cipher algorithm is invalid
 selected compression algorithm is invalid
 selected digest algorithm is invalid
 sending key %s to %s
 set a notation for the selected user IDs set preference list for the selected user IDs set the preferred keyserver URL for the selected user IDs sh-style command output shadowing the key failed: %s
 show IETF standard notations during signature listings show IETF standard notations during signature verification show admin commands show all notations during signature listings show all notations during signature verification show expiration dates during signature listings show key fingerprint show only the primary user ID in signature verification show policy URLs during signature listings show policy URLs during signature verification show preferred keyserver URLs during signature listings show preferred keyserver URLs during signature verification show revoked and expired subkeys in key listings show revoked and expired user IDs in key listings show revoked and expired user IDs in signature verification show selected photo IDs show the keyring name in key listings show this help show user ID validity during key listings show user ID validity during signature verification show user-supplied notations during signature listings show user-supplied notations during signature verification sign a key sign a key locally sign or edit a key sign selected user IDs [* see below for related commands] sign selected user IDs locally sign selected user IDs with a non-revocable signature sign selected user IDs with a trust signature signature verification suppressed
 signatures created so far: %lu
 signed by your key %s on %s%s%s
 signing failed: %s
 signing subkey %s is already cross-certified
 signing: skipped "%s": %s
 skipped "%s": duplicated
 skipped: public key already set
 skipped: public key already set as default recipient
 skipped: secret key already present
 skipping block of type %d
 skipping v3 self-signature on user ID "%s"
 ssh handler 0x%lx for fd %d started
 ssh handler 0x%lx for fd %d terminated
 ssh keys greater than %d bits are not supported
 standalone revocation - use "gpg --import" to apply
 standalone signature of class 0x%02x
 subkey %s does not sign and so does not need to be cross-certified
 subpacket of type %d has critical bit set
 system error while calling external program: %s
 textmode the available CRL is too old the given certification policy URL is invalid
 the given preferred keyserver URL is invalid
 the given signature policy URL is invalid
 the signature could not be verified.
Please remember that the signature file (.sig or .asc)
should be the first file given on the command line.
 the status of the certificate is unknown there is a secret key for public key "%s"!
 this is a PGP generated Elgamal key which is not secure for signatures! this key has already been designated as a revoker
 this may be caused by a missing self-signature
 this message may not be usable by %s
 this platform requires temporary files when calling external programs
 toggle the signature force PIN flag too many cipher preferences
 too many compression preferences
 too many digest preferences
 trust record %lu is not of requested type %d
 trust record %lu, req type %d: read failed: %s
 trust record %lu, type %d: write failed: %s
 trust: %s trustdb rec %lu: lseek failed: %s
 trustdb rec %lu: write failed (n=%d): %s
 trustdb transaction too large
 trustdb: lseek failed: %s
 trustdb: read failed (n=%d): %s
 trustdb: sync failed: %s
 trustlevel adjusted to FULL due to valid PKA info
 trustlevel adjusted to NEVER due to bad PKA info
 ultimate unable to display photo ID!
 unable to execute external program
 unable to read external program response: %s
 unable to set exec-path to %s
 unable to update trustdb version record: write failed: %s
 unable to use unknown trust model (%d) - assuming %s trust model
 unblock the PIN using a Reset Code uncompressed|none undefined unexpected armor:  unknown unknown armor header:  unknown default recipient "%s"
 unnatural exit of external program
 unspecified unsupported algorithm: %s update all keys from a keyserver update failed: %s
 update the trust database usage: %s use a log file for the server use as output file use canonical text mode use option "--delete-secret-keys" to delete it first.
 use strict OpenPGP behavior user ID "%s" is already revoked
 user ID: "%s"
 using %s trust model
 using cipher %s
 using default PIN as %s
 using subkey %s instead of primary key %s
 validate signatures with PKA data validity: %s verbose verification of Admin PIN is currently prohibited through this command
 verify CHV%d failed: %s
 verify a signature verify the PIN and list all data waiting for lock %s...
 waiting for lock (held by %d%s) %s...
 weak key created - retrying
 weird size for an encrypted session key (%d)
 will not run with insecure memory due to %s
 writing direct signature
 writing key binding signature
 writing new key
 writing self signature
 writing to stdout
 yY yes you can update your preferences with: gpg --edit-key %s updpref save
 you cannot appoint a key as its own designated revoker
 you cannot use --symmetric --encrypt while in %s mode
 you cannot use --symmetric --encrypt with --s2k-mode 0
 you cannot use --symmetric --sign --encrypt while in %s mode
 you cannot use --symmetric --sign --encrypt with --s2k-mode 0
 you found a bug ... (%s:%d)
 you may not use %s while in %s mode
 |AN|New Admin PIN |A|Please enter the Admin PIN |A|Please enter the Admin PIN%%0A[remaining attempts: %d] |FD|write status info to this FD |FILE|read options from FILE |FILE|take the keys from the keyring FILE |FILE|write output to FILE |NAME|use NAME as default secret key |NAME|use cipher algorithm NAME |NAME|use message digest algorithm NAME |N|New PIN |N|expire cached PINs after N seconds |N|set compress level to N (0 disables) |PGM|use PGM as the PIN-Entry program |PGM|use PGM as the SCdaemon program |RN|New Reset Code |USER-ID|encrypt for USER-ID |USER-ID|use USER-ID to sign or decrypt |pinentry-label|PIN: |pinentry-label|_Cancel |pinentry-label|_OK ||Please enter the PIN ||Please enter the PIN%%0A[sigs done: %lu] ||Please enter the Reset Code for the card Project-Id-Version: gnupg 1.1.92
Report-Msgid-Bugs-To: translations@gnupg.org
POT-Creation-Date: 2016-01-26 13:55+0100
PO-Revision-Date: 2016-04-08 16:20+0000
Last-Translator: Marco d'Itri <md@linux.it>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:31+0000
X-Generator: Launchpad (build 18115)
Language: it
 
Inserire l'ID utente, termina con una riga vuota:  
Scegliere un'immagine da usare per l'identificazione fotografica: deve essere
un file JPEG. L'immagine viene salvata nella propria chiave pubblica, se viene
usata un'immagine molto grande, anche la tua chiave lo diventerà.
Dimensioni vicine a 240x288 sono una buona scelta.
 
Algoritmi supportati:
 
È necessario un ID utente per identificare la propria chiave; il software
costruisce l'ID utente a partire da nome reale, commento e indirizzo email
in questa forma:
    "Mario Rossi (commento) mario.rossi@example.net"

                 conosciuto anche come "%s"                con %s chiave %s
               importate: %lu              non modificate: %lu
            nuove sottochiavi: %lu
           nuovi ID utente: %lu
           non importate: %lu
           senza ID utente: %lu
          "%s": preferenza per l'algoritmo di cifratura %s
          "%s": preferenze per l'algoritmo di compressione %s
          "%s": preferenza per l'algoritmo di digest %s
          (sottochiave sull'ID della chiave primaria %s)          Non è sicuro che la firma appartenga al proprietario.
          La firma è probabilmente una falsificazione.
          Non ci sono indicazioni che la firma appartenga al proprietario.
          Questo può significare che la chiave è stata falsificata.
         nuove firme: %lu
       "%s"
       Numero di serie della scheda=       Impronta digitale della chiave =       Impronta digitale della sottochiave:       chiavi segrete lette: %lu
       nuove chiavi saltate: %lu
       ID utente puliti: %lu
      Impronta digitale della sottochiave:     firme pulite: %lu
    (%c) Esce
    (%c) Commuta la funzione di autenticazione
    (%c) Commuta la funzione di cifratura
    (%c) Commuta la funzione di firma
    (%d) DSA (imposta funzioni personalizzate)
    (%d) DSA (solo firma)
    (%d) DSA ed Elgaman
    (%d) Elgamal (solo cifratura)
    (%d) RSA (solo cifratura)
    (%d) RSA (imposta funzioni personalizzate)
    (%d) RSA (solo firma)
    (%d) RSA e RSA (predefinito)
    (0) Nessuna risposta.%s
    (1) Nessun controllo effettuato.%s
    (1) Chiave di firma
    (2) Chiave di cifratura
    (2) Controllo superficiale.%s
    (3) Chiave di autenticazione
    (3) Controllo approfondito.%s
    nuove revoche di chiavi: %lu
   %d = nessuna fiducia
   %d = non si sa o non si vuol dire
   %d = fiducia piena
   %d = fiducia marginale
   %d = fiducia completa
   Impossibile firmare.
   conosciuto anche come "%s"
   m = torna al menù principale
   u = uscire
   s = salta questa chiave
   chiavi segrete importate: %lu
  (ID chiave principale %s)  (non esportabile)  (non-revocabile)  Impronta digitale chiave primaria:  chiavi segrete non modificate: %lu
 "%s" non è ID di chiave: ignorato
 "%s" era già stata firmata localmente dalla chiave %s
 "%s" era già stata firmata dalla chiave %s
 # Elenco dei valori di fiducia assegnati, creato il %s
# (Usare "gpg --import-ownertrust" per ripristinarli)
 esaminate finora %lu chiavi
 %s %s terminato
 %s non funziona ancora con %s
 dati cifrati con %s
 chiave di sessione cifrata con %s
 sarà usato il cifrario %s
 %s è troppo obsoleto (è necessario %s, si dispone di %s)
 La lunghezza delle chiavi %s è compresa tra %u e %u bit.
 Le dimensioni delle chiavi %s devono essere nell'intervallo %u-%u
 %s non ha senso usato assieme a %s.
 %s non consentito assieme a %s.
 %s%%0A%%0AUsare il tastierino del lettore come input. %s/%s cifrato per: "%s"
 Firma %s/%s da: "%s"
 %s: c'è poca sicurezza che questa chiave appartenga all'utente indicato
 %s: non c'è alcuna sicurezza che la chiave appartenga all'utente indicato
 %s: la directory non esiste.
 %s: errore durante la lettura del record libero: %s
 %s: errore durante la lettura del record di versione: %s
 %s: errore durante l'aggiornamento del record di versione: %s
 %s: errore durante la scrittura del record dir: %s
 %s: errore durante la scrittura del record di versione: %s
 %s: aggiunta in coda a un record non riuscita: %s
 %s: creazione della tabella hash non riuscita: %s
 %s: creazione del record della versione non riuscita: %s %s: azzeramento di un record non riuscito: %s
 %s: versione %d del file non valida
 %s: trustdb non valido
 %s: è stato creato un trustdb non valido
 %s: portachiavi creato
 %s: non è un file di trustdb
 %s: saltata: %s
 %s: saltata: chiave pubblica già presente
 %s: saltata: chiave pubblica disabilitata
 %s: creato il trustdb
 %s: suffisso sconosciuto
 %s: record di versione con recnum %lu
 %s:%d "%s" è un'opzione deprecata
 %s:%d: elenco auto-key-locate non valido
 %s:%d: opzioni di esportazione non valide
 %s:%d: opzioni di importazione non valide
 %s:%d: opzioni del server di chiavi non valide
 %s:%d: opzioni di elenco non valide
 %s:%d: opzioni di verifica non valide
 %u-bit chiave %s, ID %s, creata %s (Non è stata data una descrizione)
 (Probabilmente la scelta era %d)
 (Questa è una chiave di revoca confidenziale)
 (confidenziale) (a meno di non specificare la chiave tramite l'impronta digitale)
 --clearsign [NOMEFILE] --decrypt [NOMEFILE] --edit-key user-id [COMANDI] --encrypt [NOMEFILE] --lsign-key user-id --output non funziona con questo comando
 --passwd <user-id> --sign --encrypt [NOMEFILE] --sign --symmetric [NOMEFILE] --sign [NOMEFILE] --sign-key user-id --store [NOMEFILE] --symmetric --encrypt [NOMEFILE] --symmetric --sign --encrypt [NOMEFILE] --symmetric [NOMEFILE] @
(Consultare la pagina man per un elenco completo di tutti i comandi e opzioni)
 @
Esempi:

 -se -r Mario [FILE]        Firma e cifra per l'utente Mario
 --clearsign [FILE]         Crea una firma con testo in chiaro
 --detach-sign [FILE]       Crea una firma separata
 --list-keys [FILE]         Mostra le chiavi
 --fingerprint [FILE]       Mostra le impronte digitali
 @
Opzioni:
  @Comandi:
  @Opzioni:
  Forzato l'output con armatura ASCII.
 PIN amministrazione I comandi amministrativi sono permessi
 I comandi amministrativi non sono permessi
 Solo comandi amministrativi
 Consenti Un processo ssh ha richiesto l'uso della chiave%%0A %s%%0A (%s)%%0AConsentirlo? Firmare veramente questa chiave con la propria
chiave "%s" (%s)
 Aggiungerlo comunque? (s/N)  Procedere veramente con la revoca? (s/N)  Firmare comunque? (s/N)  Nominare veramente questa chiave come un revocatore designato? (s/N)  Eliminare veramente? (s/N)  Sostituire veramente? (s/N)  Usarlo veramente? (s/N)  Autentica Firma non corretta di "%s" PIN errato Passphrase errata Impronta digitale CA:  errore nel CRC; %06lX - %06lX
 controllo CRL/OCSP dei certificati Impossibile controllare la firma: %s
 Annulla Nome del proprietario della scheda:  Cognome del proprietario della scheda:  Catena di certificati valida Certificati che portano a chiavi completamente affidabili:
 Certifica Modificare (n)ome, (c)ommento, (e)mail oppure (o)k/(u)scire?  Modificare (n)ome, (c)ommento, (e)mail oppure (u)scire?  Cambia la passphrase Modifica della data di scadenza per una sottochiave.
 Modifica della data di scadenza per la chiave primaria.
 Cifrari:  Il comando richiede un nome di file come argomento
 Commento:  Compressione:  Configurazione per i Server di chiavi Corretto Creare un certificato di revoca assegnato a questa chiave? (s/N)  Creare un certificato di revoca per questa chiave? (s/N)  Creare un certificato di revoca per questa firma? (s/N)  Server delle chiavi critico preferito:  Annotazione critica della firma:  Politica critica di firma:  Azioni attualmente permesse:  Destinatari attuali:
 DSA richiede che la lunghezza dello hash sia un multiplo di 8 bit
 DSA richiede l'uso di un algoritmo di hashing con almeno 160 bit
 Decifratura dei dati avvenuta con successo Firma dei dati eseguita con successo Verifica dei dati eseguita con successo Eliminare questa firma corretta? (s/N/e) Eliminare questa firma non valida? (s/N/e) Eliminare questa chiave dal portachiavi? (s/N)  Eliminare questa firma sconosciuta? (s/N/e) Nega Firma separata.
 Digest:  Dirmngr utilizzabile Visualizzazione ID fotografico %s di dimensione %ld per la chiave %s (UID %d)
 Eliminare veramente le chiavi selezionate? (s/N)  Eliminare veramente questa chiave? (s/N)  Revocare veramente l'intera chiave? (s/N)  Revocare veramente le sottochiavi selezionate? (s/N)  Revocare veramente questa sottochiave? (s/N)  Impostare questa chiave come completamente affidabile? (S/N)  Emettere una nuova firma per sostituire quella scaduta? (s/N)  Promuoverla a una firma completa esportabile? (s/N)  Promuoverla a un'auto-firma OpenPGP? (s/N)  Firmare nuovamente? (s/N)  Fare in modo che la propria firma scada nello stesso momento? (S/n)  Indirizzo email:  Cifra Algoritmo di cifratura supportato Inserire il nome del file JPEG per l'ID fotografico:  Inserire una descrizione opzionale; terminare con una riga vuota:
 Inserire il nuovo nome del file Inserire la nuova passphrase Digitare numero/i, s)uccessivo o e)sci >  Digitare la passphrase
 Inserisci la passphrase:  Inserire l'annotazione:  Inserire l'ID utente del revocatore designato:  Inserire l'URL del server di chiavi preferito:  Errore: nome combinato troppo lungo (il limite è %d caratteri).
 Errore: non sono ammessi doppi spazi.
 Errore: dati login troppo lunghi (il limite è %d caratteri).
 Errore: attualmente è permesso solo ASCII semplice.
 Errore: dati DO privati troppo lunghi (il limite è %d caratteri).
 Errore: Il carattere "<" non può essere usato.
 Errore: Il trustdb è danneggiato.
 Errore: URL troppo lungo (il limite è %d caratteri).
 Errore: carattere non valido nella stringa di preferenze.
 Errore: impronta non valida.
 Errore: lunghezza della stringa di preferenze non valida.
 Errore: risposta non valida.
 Firma scaduta da "%s" Caratteristiche:  Procedere e digitare il proprio messaggio...
 Firma valida da "%s" Gpg-Agent utilizzabile Hash:  Suggerimento: selezionare gli ID utente da firmare
 Con quanta attenzione è stato verificato che la chiave da firmare
appartiene veramente alla persona indicata sopra?
In casa di dubbio, digitare "0".
 Quanta fiducia si ha nel fatto che questa chiave appartenga realmente all'utente indicato?
 Verifica di questa firma eseguita in modo noncurante.
 Verifica di questa firma eseguita in modo molto accurato.
 Nessuna verifica eseguita su questa firma.
 cifrario IDEA non disponibile, tentativo di usare %s al suo posto
 Se non funziona, consultare il manuale
 Certificati inclusi Carattere non valido nel commento
 Carattere non valido nel nome
 Caratteri non validi nel PIN Comando non valido  (provare "help")
 Chiave %s non valida resa valida da --allow-non-selfsigned-uid
 Selezione non valida.
 È tutto corretto? (s/N)  È esatto? (s/N)  Questa foto è giusta? (s/N/q)  NON è sicuro che la chiave appartenga all'utente indicato
nell'ID utente. Se si è *davvero* sicuri di cosa si sta facendo,
rispondere sì alla prossima domanda.
 La chiave %s è già revocata.
 Chiave disponibile presso:  La chiave non scade
 La chiave scade il %s
 Generazione della chiave annullata.
 Generazione della chiave non riuscita: %s
 La chiave è stata compromessa La chiave ha solo voci incomplete o su scheda - nessuna passphrase da modificare.
 La chiave non è più usata La chiave è revocata. La chiave è stata sostituita Per quanto tempo deve essere valida la chiave? (0)  La chiave non è stata modificata quindi non sono necessari aggiornamenti.
 Portachiavi Server delle chiavi: nessuna modifica. Elenco server LDAP Preferenze della lingua:  Dati login (nome account):  Effettuare il backup della chiave di cifratura al di fuori della scheda? (S/n)  Il nome non può iniziare con una cifra
 Il nome deve essere lungo almeno 5 caratteri
 Per questa azione è necessaria la chiave segreta.
 NnCcEeOoUu No Nessuna voce nel registro di controllo Nessuna impronta digitale Non è disponibile un aiuto Nessun motivo specificato Nessuna sottochiave con indice %d
 ID utente inesistente.
 Nessun valore affidabile assegnato a:
 Nessun ID utente con hash %s
 Nessun ID utente con indice %d
 L'indirizzo email non è valido
 Non firmato dalla propria chiave.
 Annotazioni:  Notare che questa chiave non può essere usata per cifrare. È possibile
usare il comando "--edit-key" per generare una sottochiave atta a tale scopo.
 Nota: questa chiave è stata disabilitata.
 Nota: questa chiave è scaduta.
 Non è stato eliminato nulla.
 Niente da firmare con la chiave %s
 Numero dei destinatari rilevata scheda OpenPGP n. %s
 scheda OpenPGP non disponibile: %s
 Sovrascrivere? (s/N)  Non è possibile impostare l'affidabilità del proprietario usando un database di fiducia fornito dall'utente
 PIN callback ha restituito l'errore: %s
 Il PIN per CHV%d è troppo corto; la lunghezza minima è %d
 PIN ripetuto non correttamente; provare di nuovo PIN troppo lungo PIN troppo corto PUK PUK non digitato correttamente, provare di nuovo. Analisi dei dati eseguita con successo Passphrase troppo lunga Passphrase: In primo luogo correggere l'errore
 Decidere quanta fiducia riporre nella capacità di questo utente di verificare
le chiavi di altri utenti (consultando passaporti, controllando le impronte
digitali da diverse sorgenti, ecc)
 Non inserire l'indirizzo email nel nome reale o nel commento
 Inserire un dominio per limitare questa firma, oppure Invio per nessuno.
 Inserire la passphrase per proteggere la chiave segreta ricevuta%%0A %s%%0A %s%%0Anel sistema di archiviazione di chiavi di gpg-agent Inserire il nome del file di dati:  Inserire il PIN%s%s%s per sbloccare la scheda Inserire il livello di affidabilità di questa firma.
Un livello superiore a 1 consente alla chiave che si sta firmando
di creare firme affidabili a vostro nome.
 Inserire la nuova passphrase Inserire la passphrase per la chiave ssh%%0A %F%%0A (%c) Inserire la passphrase o il PIN
necessari per completare questa operazione. Inserire la passphrase per proteggere l'oggetto importato all'interno del sistema GnuPG. Inserire la passphrase per proteggere l'oggetto PKCS#12. Inserire la passphrase per sbloccare l'oggetto PKCS#12. Inserire la passphrase per%0Aproteggere la nuova chiave Inserire il proprio PIN, in modo che la chiave segreta possa essere sbloccata per questa sessione Inserire la propria passphrase, in modo che la chiave segreta possa essere sbloccata per questa sessione Inserire la scheda con il numero seriale Notare che la validità della chiave mostrata non è necessariamente corretta
finché non si riavvia il programma.
 Inserire nuovamente questa passphrase Rimuovere la scheda attuale e inserire quella con il numero seriale Riportare il bug a <@EMAIL@>.
 Selezionare esattamente un ID utente.
 Scegliere il motivo della revoca:
 Selezionare il tipo di chiave da generare:
 Selezionare il tipo di chiave:
 Selezionare dove archiviare la chiave:
 Specificare la durata di validità della chiave.
         0 = la chiave non scade
      <N>  = la chiave scade dopo N giorni
      <N>w = la chiave scade dopo N settimane
      <N>m = la chiave scade dopo N mesi
      <N>y = la chiave scade dopo N anni
 Specificare la durata di validità della firma.
         0 = la firma non scade
      <N>  = la firma scade dopo N giorni
      <N>w = la firma scade dopo N settimane
      <N>m = la firma scade dopo N mesi
      <N>y = la firma scade dopo N anni
 Azioni possibili per una chiave %s:  Server di chiavi preferito:  Impronta digitale chiave primaria: Dati DO privati:  Procedere? (s/N)  A chiave pubblica:  La chiave pubblica è disabilitata.
 Qualità: Uscire senza salvare? (s/N)  modulo RSA mancante o non della dimensione di %d bit
 numero primo RSA %s mancante o non della dimensione di %d bit
 esponente RSA pubblico mancante o più grande di %d bit
 Nome reale:  Creare veramente i certificati di revoca? (s/N)  Creare veramente? (s/N)  Eliminare veramente questa auto-firma? (s/N) Spostare veramente la chiave primaria? (s/N)  Rimuovere veramente tutti gli ID utente selezionati? (s/N)  Rimuovere veramente questo ID utente? (s/N)  Revocare veramente tutti gli ID utente selezionati? (s/N)  Revocare veramente questo ID utente? (s/N)  Firmare veramente tutti gli ID utente? (s/N)  Firmare veramente? (s/N)  Aggiornare veramente le preferenze per gli ID utente selezionati? (s/N)  Aggiornare veramente le preferenze? (s/N)  Motivo della revoca: %s
 Destinatario %d Ripetere questo PIN Digitare nuovamente questo PUK Digitare nuovamente questo codice di reset Sostituire la chiave esistente? (s/N)  Sostituire le chiavi esistenti? (s/N)  La dimensione chiave richiesta è %u bit
 Codice di reset Il codice di ripristino è troppo corto; la lunghezza minima è %d
 Codice di reset non digitato correttamente, provare di nuovo. Codice di ripristino non disponibile o non più disponibile
 Certificato di revoca creato.
 Creato un certificato di revoca.

Spostarlo su un dispositivo che può essere nascosto con sicurezza. Se
qualcuno entrasse in possesso di questo certificato potrebbe usarlo
per rendere inutilizzabile la chiave. È inoltre una buona idea stamparlo
e archiviarlo, nel caso il dispositivo diventasse illeggibile.
Attenzione: il sistema di stampa potrebbe salvare i dati e renderli disponibili
ad altri.
 Certificato principale affidabile SETERROR %s (tentativo %d di %d) Salvare le modifiche? (s/N)  La chiave segreta è disponibile.
 Le parti segrete della chiave primaria non sono disponibili.
 Le parti segrete della chiave primaria sono archiviate su scheda.
 Impostare l'elenco delle preferenze a:
 Sesso ((m)aschile, (f)emminile o spazio):  Firma Firmare? (s/N)  Firma %d Firma disponibile La firma non scade
 Firma scaduta il %s
 Questa firma scadrà il %s
 La firma scade il %s
 Per quanto tempo deve essere valida la firma? (%s)  Firma eseguita %s
 Firma eseguita in data %s usando %s, ID chiave %s
 Annotazione della firma:  Politica di firma:  Firmatario %d FfCcAaEe La sottochiave %s è già revocata.
 Sintassi: gpg-check-pattern [opzioni] patternfile
Controlla una passphrase fornita in stdin rispetto al patternfile
 Sintassi: gpg-agent-passphrase [opzioni] KEYGRIP
Mantenimento della cache per la password
 Sintassi: gpg-protect-tool [opzioni] [argomenti]
Strumento per il mantenimento della chiave segreta
 Sintassi: gpgv [opzioni][file]
Controlla le firme confrontandole con chiavi affidabili
 Conservare questa comunque La scheda verrà ora riconfigurata per generare una chiave di %u bit
 Il livello minimo di fiducia per questa chiave è: %s

 L'auto-firma su "%s"
è una firma in stile PGP 2.x.
 La firma verrà contrassegnata come non esportabile.
 La firma verrà contrassegnata come non revocabile.
 Non esistono preferenze su un ID utente in stile PGP 2.x.
 Questo JPEG è troppo grande (%d byte).
 Questo comando non è permesso in modalità %s.
 Questo comando è disponibile solo per schede di versione 2
 Questa è una chiave segreta: eliminarla veramente? (s/N)  Questa chiave ci appartiene
 Questa chiave è stata disabilitata Questa chiave è scaduta. Questa chiave scadrà il %s.
 Questa chiave può essere revocata da %s chiave %s Questa chiave probabilmente appartiene all'utente indicato
 Questa chiave è scaduta il %s.
 Questa sarà un'auto-firma.
 Revocabile da:
 Numero totale esaminato: %lu
 URL per recuperare la chiave pubblica:  Non compresso Operazione sconosciuta Uso: gpg-check-pattern [opzioni] patternfile (-h per aiuto)
 Uso: gpg-preset-passphrase [opzioni] KEYGRIP (-h per l'aiuto)
 Uso: gpg-protect-tool [opzioni] (-h per l'aiuto)
 Uso: gpgv [OPZIONE...] [FILE...] (-h per l'aiuto) Usare comunque questa chiave? (s/N)  ID utente "%s" compattato: %s
 L'ID utente "%s" è scaduto. L'ID utente "%s" non è auto-firmato. L'ID utente "%s" è stato revocato. L'ID utente "%s" è firmabile.   ID utente "%s": già pulito
 ID utente "%s": già minimizzato
 L'user ID non è più valido ATTENZIONE: "%s" è un comando deprecato - non usarlo
 ATTENZIONE: "%s" è un'opzione deprecata
 ATTENZIONE: %s prevale su %s
 ATTENZIONE: le chiavi Elgaman di firma+cifratura sono deprecate
 ATTENZIONE: questa è una chiave in stile PGP 2.x. Aggiungere un revocatore
            designato può causarne il rifiuto da parte di alcune versioni di
            PGP.
 ATTENZIONE: questa è una chiave in stile PGP2. Aggiungere un ID fotografico
            può causarne il rifiuto da parte di alcune versioni di PGP.
 ATTENZIONE: questa chiave è stata revocata dal suo revocatore designato.
 ATTENZIONE: questa chiave è stata revocata dal suo proprietario.
 ATTENZIONE: questa chiave non è certificata con una firma fidata.
 ATTENZIONE: questa chiave non è certificata con firme abbastanza fidate.
 ATTENZIONE: questa sottochiave è stata revocata dal proprietario.
 ATTENZIONE: uso di una chiave non fidata.
 ATTENZIONE: impossibile fidarsi di questa chiave.
 ATTENZIONE: una firma dell'ID utente è datata %d secondi nel futuro
 ATTENZIONE: la nomina di una chiave come revocatore designato non può essere
            annullata.
 ATTENZIONE: algoritmo di cifratura %s non trovato nelle preferenze del destinatario
 ATTENZIONE: l'algoritmo di digest %s è deprecato
 ATTENZIONE: il messaggio cifrato è stato manipolato.
 ATTENZIONE: forzare l'algoritmo di compressione %s (%d) viola le preferenze del destinatario
 ATTENZIONE: forzare l'algoritmo di digest %s (%d) viola le preferenze del destinatario
 ATTENZIONE: forzare il cifrario simmetrico %s (%d) viola le preferenze del destinatario
 ATTENZIONE: trovati dati di una nota non validi
 ATTENZIONE: la chiave %s può essere stata revocata: acquisizione della chiave di revoca %s.
 ATTENZIONE: la chiave %s può essere stata revocata: chiave di revoca %s non presente.
 ATTENZIONE: il messaggio è stato cifrato con una chiave debole nel cifrario simmetrico.
 ATTENZIONE: l'integrità del messaggio non era protetta
 ATTENZIONE: rilevate molte parti di testo in chiaro
 ATTENZIONE: trovate firme multiple, sarà controllata solo la prima.
 ATTENZIONE: nessun user ID è stato indicato come primario. Questo comando
            potrebbe far diventare un altro ID utente quello primario
            predefinito.
 ATTENZIONE: non è stato esportato nulla
 ATTENZIONE: la chiave di sessione cifrata simmetricamente è potenzialmente non sicura
 ATTENZIONE: il programma potrebbe creare un file core.
 ATTENZIONE: indicati destinatari (-r) senza usare la crittografia a chiave pubblica
 ATTENZIONE: conflitto del digest delle firme nel messaggio
 ATTENZIONE: la sottochiave per la firma %s ha una certificazione incrociata non valida
 ATTENZIONE: la sottochiave per la firma %s non ha una certificazione incrociata
 ATTENZIONE: la firma non sarà contrassegnata come non esportabile.
 ATTENZIONE: la firma non sarà contrassegnata come non revocabile.
 ATTENZIONE: questa chiave può essere stata revocata (la chiave di revoca non è presente).
 ATTENZIONE: impossibile espandere i %% nell'URL (troppo grande), usato non espanso
 ATTENZIONE: impossibile espandere i %% nell'URL della politica (troppo grande), usato non espanso.
 ATTENZIONE: impossibile espandere i %% nell'URL del server di chiavi preferito (troppo grande), usato non espanso
 ATTENZIONE: impossibile recuperare l'URI %s: %s
 ATTENZIONE: impossibile aggiornare la chiave %s attraverso %s: %s
 ATTENZIONE: utilizzo dell'algoritmo di cifratura sperimentale %s
 ATTENZIONE: utilizzo dell'algoritmo di digest sperimentale %s
 ATTENZIONE: utilizzo dell'algoritmo a chiave pubblica sperimentale %s
 È necessario generare molti dati casuali: per fare ciò è utile eseguire
qualche altra azione (scrivere sulla tastiera, muovere il mouse, usare i
dischi) durante la generazione dei numeri primi; questo fornisce al
generatore di numeri casuali migliori possibilità di raccogliere abbastanza
entropia.
 Quale dimensione impostare per la chiave di autenticazione? (%u)  Quale dimensione impostare per la chiave di cifratura? (%u)  Quale dimensione impostare per la chiave di firma? (%u)  Quale dimensione impostare per la sottochiave? (%u)  Quale dimensione impostare per la chiave? (%u)  Errato Sì Sì, non è necessaria alcuna protezione Si è in procinto di revocare queste firme:
 Non è possibile modificare la data di scadenza di una chiave v3
 Non è possibile eliminare l'ultimo ID utente.
 Non è stato specificato un ID utente (è possibile usare "-r").
 Non è stata inserita alcuna passphrase; questa non è una buona idea!%0AConfermare che non si desidera alcuna protezione sulla propria chiave. Non è stata inserita alcuna passphrase!%0ANon sono ammesse passphrase vuote. Questi ID utente sono stati firmati sulla chiave %s:
 Non è possibile aggiungere un revocatore designato a una chiave in stile
PGP 2.x.
 Non è possibile aggiungere un ID fotografico a una chiave in stile PGP2.
 È possibile provare a ricreare il trustdb usando i comandi:
 È necessario selezionare almeno una chiave.
 È necessario selezionare almeno un ID utente.
 È necessario selezionare esattamente una chiave.
 È necessaria una passphrase per sbloccare la chiave segreta
dell'utente: "%s"
 È stato selezionato questo USER-ID:
    "%s"

 La propria firma attuale su "%s"
è scaduta.
 La propria firma attuale su "%s"
è una firma locale.
 Cosa fare?  Selezione?  Il sistema in uso non può mostrare date oltre il 2038.
Comunque, sarà gestita correttamente fino al 2106.
 [   piena    ] [ indefinita ] [  scaduta   ] [  revocata  ] [  non nota  ] [ID utente non trovato] [NOMEFILE] [ marginale  ] [nessuno] [non impostato] [revoca] [auto-firma] [  completa  ] [incerta] un'istanza di gpg-agent è già in esecuzione - non ne partirà una nuova
 il nome di una nota deve essere formato solo da caratteri stampabili o
spazi e terminare con un '='
 Il nome di una nota non deve contenere più di un carattere "@"
 il valore di una nota non deve usare caratteri di controllo
 il valore di una nota dell'utente deve contenere il carattere '@'
 l'accesso ai comandi di amministrazione non è configurato
 Aggiunge una chiave a una smartcard Aggiunge un ID fotografico Aggiunge una chiave di revoca Aggiunge una sottochiave Aggiunge un ID utente Consentire la preimpostazione della passphrase destinatario sconosciuto; viene provata la chiave segreta %s...
 header dell'armatura:  armatura: %s
 risponde "no" a quasi tutte le domande risponde "sì" a quasi tutte le domande i dati sono probabilmente cifrati con %s
 la firma della chiave %s non viene considerata valida a causa di un bit critico sconosciuto
 algoritmo hash attr: %s Recupera automaticamente le chiavi durante la verifica delle firme cattivo algoritmo di hash dei dati: %s modo batch: non fa domande Meno prolisso binario build_packet non riuscita: %s
 aA impossibile accedere a %s - scheda OpenPGP non valida?
 Impossibile creare il socket: %s
 impossibile disabilitare i core dump: %s
 impossibile eseguire in modalità batch
 impossibile eseguire in modalità batch senza "--yes"
 impossibile gestire l'algoritmo a chiave pubblica %d
 impossibile gestire righe di testo più lunghe di %d caratteri
 impossibile gestire i dati ambigui di questa firma
 impossibile usare un pacchetto ESK simmetrico a causa della modalità S2K
 Annullato dall'utente
 Annullato
 annullato dall'utente
 Annulla impossibile nominare come revocatore designato una chiave in stile PGP 2.x
 Impossibile evitare una chiave debole per il cifrario simmetrico: %d tentativi.
 la scheda non supporta l'algoritmo di digest %s
 la scheda è bloccata in modo permanente.
 num-scheda:  Il certificato è stato revocato Il certificato è scaduto Cambia l'URL per recuperare la chiave Cambia l'impronta digitale di un CA Modifica il PIN di una scheda cambia una passphrase Cambia il nome del proprietario della scheda Cambia il sesso del proprietario della scheda Modifica i dati su una scheda Modifica la data di scadenza per la chiave o le sottochiavi selezionate Cambia le preferenze della lingua Cambia il nome di login Cambia il valore di fiducia Cambia la passphrase controlla tutti i programmi Verifica le firme controllo della firma creata non riuscito: %s
 controllo del trustdb
 Discendente annullato con stato %i
 l'algoritmo di cifratura %d%s è sconosciuto o disattivato
 la classe %s non è supportata
 due punti (:) mancanti problema di comunicazione con gpg-agent
 Compatta gli ID utente non utilizzabili e rimuove tutte le firme dalla chiave Compatta gli ID utente non utilizzabili e rimuove le firme non utilizzabili dalla chiave completes-needed deve essere maggiore di 0
 comandi in conflitto
 Impossibile aprire %s per la scrittura: %s
 impossibile analizzare l'URL del server di chiavi
 Crea un output ascii con armatura creata: %s timestamp di creazione mancante
 output comandi stile csh algoritmo hash dei dati: %s i dati non sono stati salvati; usare l'opzione "--output" per salvarli
 rimozione dell'armatura non riuscita: %s
 Decifra i dati (predefinito) de-cifratura non riuscita: %s
 de-cifratura corretta
 Elimina le sottochiavi selezionate Elimina gli ID utente selezionati Elimina le firme dagli ID utente selezionati eliminazione del keyblock non riuscita: %s
 livello: %d  valido: %3d  firmato: %3d  fiducia: %d-, %dq, %dn, %dm, %df, %du
 individuata carta con numero di serie: %s
 Disabilita la chiave disabilitato Non consentire ai client di contrassegnare le chiavi come "fidate" Mostra ID fotografici nell'elencare le chiavi Mostra ID fotografici nel verificare le firme non staccare dalla console non esegue il grab di tastiera e mouse Non esegue alcuna modifica Non aggiorna il trustdb dopo l'importazione non usare la cache del PIN durante la firma non usa il SCdaemon non corrispondono - provare ancora non usa per niente il terminale deve essere fornito %s o %s
 Innalza la fiducia delle firme con dati PKA validi Abilita la chiave Abilita il supporto per putty Abilita il supporto ssh creazione dell'armatura non riuscita: %s
 Cifra i dati cifrato con %lu passphrase
 cifrato con la chiave %s con ID %s
 cifrato con chiave %2$s a %1$u-bit, ID %3$s,  creato il %4$s
 cifrato con 1 passphrase
 cifrato con l'algoritmo sconosciuto %d
 Cifratura solo con cifrario simmetrico errore nell'allocare sufficiente memoria: %s
 errore nella modifica della chiave %d in %u bit: %s
 errore nel creare la passhprase: %s
 errore nel creare il file temporaneo: %s
 errore nell'ottenere informazioni sulla chiave corrente: %s
 Errore nell'ottenimento delle informazioni sull'uso della chiave: %s
 errore nell'ottenere il nuovo PIN: %s
 Errore nella ricerca di un nonce per il socket
 errore nell'ottenere il numero di serie della carta: %s
 errore nella riga del trailer
 errore nel leggere i dati dell'applicazione
 errore nel leggere il DO dell'impronta digitale
 errore nel leggere il keyblock: %s
 Errore nella lettura dei certificati root affidabili
 Errore nella lettura del nonce su fd %d: %s
 errore nel recuperare lo stato del CHV dalla scheda
 errore nella richiesta della passphrase: %s
 errore nello scrivere la chiave: %s
 errore nello scrivere sul file temporaneo: %s
 la chiave esistente verrà sostituita
 scaduta scaduta: %s scadenza: %s esporta gli attributi degli ID utente (generalmente ID fotografici) Esporta le chiavi Esporta le chiavi su un server di chiavi esporta le chiavi revocate impostate come "sensibili" esporta le firme contrassegnate come solo locali esportazione delle chiavi segrete non consentita
 le chiamate a programmi esterni sono disabilitate a causa dei permessi non sicuri del file delle opzioni
 acquisizione del lock pinentry non riuscita: %s
 creazione dello stream dal socket non riuscita: %s
 inizializzazione del TrustDB non riuscita: %s
 ricostruzione della cache del portachiavi non riuscita: %s
 archiviazione della data di creazione non riuscita: %s
 archiviazione dell'impronta digitale non riuscita: %s
 archiviazione della chiave non riuscita: %s
 impossibile usare come PIN predefinito %s: %s - l'utilizzo di altre impostazioni predefinite sta per essere disabilitato
 femmina Preleva la chiave specificata nell'URL della scheda l'impronta digitale sulla scheda non corrisponde a quella richiesta
 Imposta l'ID utente selezionato come primario forzato forzare il cifrario simmetrico %s (%d) viola le preferenze del destinatario
 fstat(%d) non riuscita in %s: %s
 piena Genera una nuova coppia di chiavi Genera un certificato di revoca Genera nuove chiavi generazione della chiave non riuscita
 generazione di una nuova chiave
 gpg/scheda>  Gestore 0x%lx per fd %d avviato
 Gestore 0x%lx per fd %d terminato
 Rispetta il record PKA impostato su una chiave durante il recupero delle chiavi Rispetta l'URL del server di chiavi preferito impostato sulla chiave iImMuUsS ignora le richieste di cambiare TTY ignora le richieste di cambiare display X ignorata stringa senza senso Importa le chiavi da un server di chiavi Importa le firme che sono contrassegnate come solo-locale Importa/Incorpora delle chiavi importazione delle chiavi segrete non consentita
 Include chiavi di revoca nei risultati di ricerca Include sottochiavi nella ricerca per ID di chiave riga di input %u troppo lunga o LF mancante
 riga di input più lunga di %d caratteri
 non valida modalità S2K non valido; deve essere 0, 1 o 3
 header dell'armatura non valido:  armatura non valida: riga più lunga di %d caratteri
 elenco auto-key-locate non valido
 header della firma in chiaro non valido
 Comando non valido riga protetta con il trattino non valida:  preferenze predefinite non valide
 default-cert-level non valido; deve essere 0, 1, 2 o 3
 opzioni di esportazione non valide
 impronta digitale non valida opzioni di importazione non valide
 opzioni del server di chiavi non valide
 protocollo del server di chiavi non valido (nostro %d != handler %d)
 opzioni di elenco non valide
 min-cert-level non valido; deve essere 1, 2 o 3
 Opzione non valida preferenze personali del cifrario non valide
 preferenze personali di compressione non valide
 preferenze personali del digest non valide
 carattere radix64 non valido %02x saltato
 struttura della scheda OpenPGP non valida (DO 0x93)
 valore non valido
 opzioni di verifica non valide
 è consigliato aggiornare le proprie preferenze e
 key chiave "%s" non trovata sul server di chiavi
 chiave "%s" non trovata: %s
 la chiave %s non ha ID utente
 chiave %s contrassegnata come completamente affidabile
 la chiave %s è ripetuta più volte nel database della fiducia
 chiave %s: "%s" %d nuove firme
 chiave %s: "%s" %d nuove sottochiavi
 chiave %s: "%s" %d nuovi ID utente
 chiave %s: "%s" %d firma pulita
 chiave %s: "%s" %d firme pulite
 chiave %s: "%s" %d ID utente pulito
 chiave %s: "%s" %d ID utente puliti
 chiave %s: "%s" 1 nuova firma
 chiave %s: "%s" 1 nuova sottochiave
 chiave %s: "%s" 1 nuovo ID utente
 chiave %s: "%s" non cambiata
 chiave %s: certificato di revoca "%s" aggiunto
 chiave %s: importato certificato di revoca "%s"
 chiave %s: chiave in stile PGP 2.x - saltata
 chiave %s: riparato errore per sottochiave PKS
 chiave %s: accettata come chiave fidata
 chiave %s: accettato l'ID utente "%s" non auto-firmato
 chiave %s: impossibile individuare il keyblock originale: %s
 chiave %s: impossibile leggere il keyblock originale: %s
 chiave %s: aggiunta una firma alla chiave diretta
 chiave %s: non corrisponde alla copia presente
 chiave %s: trovato un ID utente duplicato - unito
 chiave %s: firma diretta della chiave non valida
 chiave %s: certificato di revoca non valido: %s - rifiutato
 chiave %s: certificato di revoca non valido: %s - saltata
 chiave %s: auto-firma non valida sull'ID utente "%s"
 chiave %s: collegamento con la sottochiave non valido
 chiave %s: revoca della sottochiave non valida
 chiave %s: materiale della chiave sulla scheda - saltata
 chiave %s: nuova chiave - saltata
 chiave %s: nessuna chiave pubblica - impossibile applicare il certificato di revoca
 chiave %s: nessuna chiave pubblica per la chiave fidata - saltata
 chiave %s: non ci sono sottochiavi per il collegamento con la chiave
 chiave %s: nessuna sottochiave per la revoca della chiave
 chiave %s: non c'è una sottochiave per il collegamento della firma della sottochiave
 chiave %s: non c'è una sottochiave per la revoca della firma della sottochiave
 chiave %s: nessun ID utente
 chiave %s: nessun ID utente per la firma
 chiave %s: nessun ID utente valido
 chiave %s: firma non esportabile (classe 0x%02X) - saltata
 chiave %s: chiave pubblica "%s" importata
 chiave %s: chiave pubblica non trovata: %s
 chiave %s: rimossi i collegamenti con sottochiave multipla
 chiave %s: rimosse le revoche per le sottochiavi multiple
 chiave %s: certificato di revoca nel posto errato - saltata
 chiave %s: chiave segreta importata
 chiave %s: chiave segreta con cifrario %d non valido - saltata
 chiave %s: saltata la sottochiave
 chiave %s: saltato l'ID utente "%s"
 chiave %s: firma della sottochiave nel posto sbagliato - saltata
 chiave %s: classe della firma inaspettata (0x%02X) - saltata
 chiave %s: algoritmo a chiave pubblica non supportato
 chiave %s: algoritmo a chiave pubblica non supportato sull'ID utente "%s"
 la chiave esiste già
 esportazione della chiave non riuscita: %s
 la chiave è stata creata %lu secondo nel futuro (salto nel tempo o problema
con l'orologio)
 la chiave è stata creata %lu secondi nel futuro (salto nel tempo o problema
con l'orologio)
 la chiave non è indicata come insicura - impossibile usarla con il RNG finto.
 chiave non trovata sul server di chiavi
 operazione su chiave non possibile: %s
 ricezione dal server di chiavi non riuscita: %s
 aggiornamento dal server di chiavi non riuscito: %s
 ricerca nel server di chiavi non riuscita: %s
 invio al server di chiavi non riuscito: %s
 dimensione chiave non valida; usati %u bit
 dimensione chiave arrotondata per eccesso a %u bit
 libgcrypt è troppo vecchio (serve %s, ha già %s)
 riga troppo lunga riga troppo lunga - saltata
 Elenca tutti i dati disponibili elenco di tutti i componenti Elenca e controlla le firme delle chiavi Elenca le chiavi e gli ID utente Elenca le chiavi Elenca le chiavi e le impronte digitali Elenca le chiavi e le firme Elenca le preferenze (per esperti) Elenca le preferenze (prolisso) Elenca le chiavi segrete Listen() non riuscito: %s
 appone una firma in testo chiaro Crea una firma separata appone una firma Segnala i conflitti di data solo con un avviso make_keysig_packet non riuscita: %s
 maschio CRC malformato
 marginale marginals-needed deve essere maggiore di 1
 max-cert-depth deve essere compreso tra 1 e 255
 Menù per cambiare o sbloccare il PIN Argomento mancante Sposta una chiave di backup su una smartcard Sposta una chiave su una smartcard spostamento della firma di una chiave nel posto corretto
 nN firme in chiaro annidate
 mai mai      il prossimo controllo del trustdb sarà eseguito il %s
 no nessun CRL trovato per il certificato non è stata fornita alcuna classe
 nessuna istanza di gpg-agent in funzione in questa sessione
 nessun server di chiavi conosciuto (usare l'opzione --keyserver)
 non è necessario un controllo del trustdb
 non è supportata l'esecuzione remota dei programmi
 nessuna chiave di revoca trovata per "%s"
 nessuna chiave segreta
 nessuna firma trovata
 non ci sono dati firmati
 Nessuna scheda di chiave adatta trovata: %s
 non è stata trovata alcuna chiave completamente affidabile
 non sono stati trovati dati OpenPGP validi.
 nessun indirizzo valido
 non è stato trovato un portachiavi scrivibile: %s
 non è stato trovato alcun portachiavi pubblico scrivibile: %s
 non è una firma separata
 non è una scheda OpenPGP non forzato non leggibile oO siamo il destinatario anonimo.
 okay|ok la vecchia codifica del DEK non è gestita
 firma vecchio stile (PGP 2.x)
 Accetta solo aggiornamenti alle chiavi esistenti nome del file originale="%.*s"
 informazioni di fiducia del possessore cancellate
 valore di fiducia del proprietario mancante passphrase generata con un algoritmo di digest %d sconosciuto
 pinentry.qualitybar.tooltip eseguire un --check-trustdb
 consultare %s per maggiori informazioni
 usare "%s%s" al suo posto
 attendere mentre la chiave viene generata...
 eof prematura (nel CRC)
 eof prematura (nel trailer)
 eof prematura (nessun CRC)
 Stampa lo stato della scheda problema nel gestire il pacchetto cifrato
 Chiede conferma prima di sovrascrivere chiavi pubbliche e segrete create e firmate.
 chiave pubblica %s non trovata: %s
 de-cifratura della chiave pubblica non riuscita: %s
 dati cifrati con la chiave pubblica: DEK corretto
 la chiave pubblica è %s
 chiave pubblica della chiave completamente affidabile %s non trovata
 eE esci Abbandona questo menù carattere quoted printable nell'armatura - probabilmente è stato usato
un MTA con qualche bug
 ridistribuire la chiave per prevenire potenziali problemi con l'algoritmo
 Errore di lettura lettura della chiave pubblica non riuscita: %s
 viene letto lo stdin...
 ragione della revoca:  ricezione riga fallita: %s
 Rimuove quanto più possibile dalla chiave dopo l'importazione rimuove quanto più possibile dalla chiave durante l'esportazione Rimuove le chiavi dal portachiavi pubblico Rimuove le chiavi dal portachiavi privato Rimuove le parti non usabili dalla chiave dopo l'importazione rimuove le parti non utilizzabili dalla chiave durante l'esportazione Rimozione file di blocco (creato da %d)
 Ripara gli errori causati dal server delle chiavi pks durante l'importazione richiesta della chiave %s da %s
 richiesta della chiave %s dal server %s %s
 la risposta non contiene il modulo RSA
 la risposta non contiene l'esponente pubblico RSA
 la risposta non contiene i dati della chiave pubblica
 commento alla revoca:  Revoca la chiave o le sottochiavi selezionate Revoca l'ID utente selezionato Revoca le firme sugli ID utente selezionati revocata revocato dalla propria chiave %s il %s
 revocata: %s arrotondata per eccesso a %u bit
 eseguire in modalità demone (background) esegue in modalità server eseguire in modalità server (foreground) Salva ed esci Cerca le chiavi su un server di chiavi chiave segreta "%s" non trovata: %s
 parti della chiave segreta non sono disponibili
 sembra non essere cifrato Seleziona la sottochiave N Seleziona l'ID utente N l'algoritmo di certificazione digest selezionato non è valido
 l'algoritmo di cifratura selezionato non è valido
 l'algoritmo di compressione selezionato non è valido
 l'algoritmo di digest selezionato non è valido
 invio della chiave %s a %s
 Imposta un'annotazione per gli ID utente selezionati Imposta l'elenco di preferenze per gli ID utente selezionati Imposta l'URL del server di chiavi preferito per gli ID utente selezionati output comandi stile sh oscuramento della chiave non riuscito: %s
 Mostra le annotazioni standard IETF nell'elencare le firme Mostra l'annotazione standard IETF nel verificare le firme Mostra comandi amministrativi Mostra tutte le annotazioni nell'elencare le firme Mostra tutte le annotazioni nel verificare le firme Mostra le date di scadenza nell'elencare le firme Mostra l'impronta digitale della chiave Mostra solo l'ID utente primario nel verificare le firme Mostra gli URL delle politiche nell'elencare le firme Mostra gli URL delle politiche nel verificare le firme Mostra gli URL dei server di chiavi preferiti nell'elencare le firme Mostra gli URL dei server di chiavi preferiti nel verificare le firme Mostra le sottochiavi revocate e scadute nell'elencare le chiavi Mostra gli ID utenti revocati e scaduti nell'elencare le chiavi Mostra gli ID utenti revocati o scaduti nel verificare le firme Mostra gli ID fotografici selezionati Mostra il nome del portachiavi nell'elencare le chiavi Mostra questo aiuto Mostra la validità dell'ID utente nell'elencare le chiavi Mostra la validità dell'ID nel verificare le firme Mostra le annotazioni fornite dall'utente nell'elencare le firme Mostra le annotatazioni fornite dall'utente nel verificare le firme Firma una chiave Firma una chiave localmente Firma o modifica una chiave Firma gli ID utente selezionati [* controllare più sotto per i relativi comandi] Firma localmente gli ID utente selezionati Firma gli ID utente selezionati con una firma non revocabile Firma gli ID utente selezionati con un firma fidata verifica della firma soppressa
 firme create finora: %lu
 firmato dalla propria chiave %s su %s%s%s
 firma non riuscita: %s
 la sottochiave di firma %s ha già ricevuto una certificazione incrociata
 firma: saltato "%s": %s
 saltato "%s": duplicato
 saltata: chiave pubblica già impostata
 saltata: chiave pubblica già impostata come destinatario predefinito
 saltata: chiave pubblica già presente
 saltato blocco di tipo %d
 omessa auto-firma v3 sull'ID utente "%s"
 Gestore ssh  0x%lx per fd %d avviato
 Gestore ssh 0x%lx per fd %d terminato
 non sono supportate chiavi ssh più grandi di %d bit
 revoca solitaria - usare "gpg --import" per applicarla
 firma solitaria di classe 0x%02x
 la sottochiave %s non firma e perciò non è necessario che abbia una
certificazione incrociata
 il sottopacchetto di tipo %d ha un bit critico impostato
 errore di sistema durante la chiamata del programma esterno: %s
 modo testo il CRL disponibile è troppo vecchio l'URL della politica di certificazione indicato non è valido
 l'URL fornito per il server di chiavi principale non è valido
 l'URL della politica di firma indicato non è valido
 non è possibile verificare la firma.
Ricordare che il file di firma (.sig or .asc) deve
essere il primo file indicato sulla riga di comando.
 Lo stato del certificato è sconosciuto è presente una chiave segreta per la chiave pubblica "%s".
 questa è una chiave Elgamal generata da PGP: non è sicura per le firme. questa chiave è stata già designata come revocatore
 questo può essere causato da un'auto-firma mancante
 questo messaggio può non essere utilizzabile da %s
 questa piattaforma richiede file temporanei quando si chiamano programmi esterni
 Commuta il flag del PIN per forzare la firma troppe preferenze di cifrario
 troppe preferenze di compressione
 troppe preferenze di digest
 il record di fiducia %lu non è del tipo %d richiesto
 record fiducia %lu, tipo %d: lettura non riuscita: %s
 record fiducia %lu, tipo %d: scrittura non riuscita: %s
 fiducia: %s trustdb rec %lu: lseek non riuscita: %s
 trustdb rec %lu: scrittura non riuscita (n=%d): %s
 transazione del trustdb troppo grande
 trustdb: lseek non riuscita: %s
 trustdb: read non riuscita (n=%d): %s
 trustdb: sincronizzazione non riuscita: %s
 livello di fiducia regolato su FULL in base alle informazioni PKA valide
 livello di fiducia regolato su NEVER in basa alle informazioni PKA non valide
 completa impossibile mostrare l'ID fotografico
 impossibile eseguire il programma esterno
 impossibile leggere la risposta del programma esterno: %s
 impossibile impostare exec-path a %s
 impossibile aggiornare la versione del record del trustdb: scrittura non riuscita: %s
 impossibile utilizzare un modello di fiducia sconosciuto (%d) - viene assunto %s come modello di fiducia
 Sblocca il PIN usando un codice di ripristino non compresso|nessuno non definita armatura inattesa:  sconosciuto header dell'armatura sconosciuto:  destinatario predefinito "%s" sconosciuto
 uscita anormale del programma esterno
 non specificato algoritmo non supportato: %s Aggiorna tutte le chiavi da un server di chiavi aggiornamento non riuscito: %s
 Aggiorna il database della fiducia uso: %s usa un file log per il server Usa come file di output Usa la modalità testo canonico usare prima l'opzione "--delete-secret-keys" per eliminarla.
 Utilizza un rigido comportamento OpenGPG l'ID utente "%s" è già revocato
 ID utente: "%s"
 modello di fiducia %s in uso
 utilizzo del cifrario %s
 viene utilizzato come PIN predefinito %s
 usata la sottochiave %s invece della chiave primaria %s
 Convalida le firme con dati PKA validità: %s Prolisso la verifica del PIN amministratore è momentaneamente proibita tramite questo comando
 verifica di CHV%d non riuscita: %s
 Verifica una firma Verifica il PIN ed elenca tutti i dati Attesa del blocco di %s...
 Attesa del blocco (tenuto da %d%s) %s...
 creata una chiave debole - nuovo tentativo
 la chiave di sessione cifrata ha dimensioni strane (%d)
 non verrà eseguito con la memoria insicura a causa di %s
 scrittura della firma diretta
 scrittura della firma di collegamento alla chiave
 scrittura della nuova chiave
 scrittura dell'auto-firma
 scrittura su stdout
 sS si|sì è possibile aggiornare le proprie preferenze con: gpg --edit-key %s updpref save
 impossibile nominare una chiave come revocatore designato di se stessa
 non è possibile usare --symmetric --encrypt in modalità %s
 non è possibile usare --symmetric --encrypt con --s2k-mode 0
 non è possibile usare --symmetric --sign --encrypt in modalità %s
 non è possibile usare --symmetric --sign --encrypt con --s2k-mode 0
 trovato un bug... (%s:%d)
 non è possibile usare %s mentre si è in modalità %s
 |AN|Nuovo PIN amministrativo |A|Inserire il PIN di amministratore |A|Inserire il PIN di Amministratore%%0A[tentativi rimasti: %d] |FD|Scrive le informazioni di stato sul FD |FILE|legge le opzioni da FILE |FILE|estrae le chiavi dal portachiavi FILE |FILE|scrive l'output in FILE |NOME|usa NOME come chiave segreta predefinita |NOME|usa l'algoritmo di cifratura NOME |NOME|usa l'algoritmo di message digest NOME |N|Nuovo PIN |N|Fa scadere i PIN memorizzati dopo N secondi |N|imposta il livello di compressione su N (0 lo disabilita) |PGM|usa PGM come programma per l'inserimento del PIN |PGM|usa PGM come programma SCdaemon |RN|Nuovo codice di ripristino |USER-ID|cifrato per NOME-UTENTE |USER-ID|usa NOME-UTENTE per firmare o decifrare pinentry-label|PIN: |pinentry-label|__Annulla |pinentry-label|__OK ||Inserire il PIN ||Inserire il PIN%%0A [firme create:%lu] ||Inserire il codice di ripristino per la scheda 