��    /      �  C                A   /     q     �  &   �     �     �                :     K  �   b     �                2     O     e     s     �  "   �     �     �     �  +   �  ,   "     O  !   f     �     �     �     �  �   �     �  U   �     	     7	     N	    ]	     u
     ~
     �
  	   �
     �
     �
  
   �
  �  �
     �  [   �  &     *   7  -   b     �     �     �  !   �            �   7     �  '   �          .     H     ]     l     �  &   �     �     �     �  5   	  :   ?     z  (   �  '   �     �     �     	         "  d   :  3   �     �     �  R       T     f     n     �  3   �  )   �     �     	           "          #       *         
      /          !                                                            -                     &   ,                      +         )                $       %      '   .   (                    Resume normal boot (Use arrows/PageUp/PageDown keys to scroll and TAB key to select) === Detailed disk usage === === Detailed memory usage === === Detailed network configuration === === General information === === LVM state === === Software RAID state === === System database (APT) === CPU information: Check all file systems Continuing will remount your / filesystem in read/write mode and mount any other filesystem defined in /etc/fstab.
Do you wish to continue? Database is consistent: Drop to root shell prompt Enable networking Finished, please press ENTER IP and DNS configured IP configured Network connectivity: No LVM detected (vgscan) No software RAID detected (mdstat) Physical Volumes: Read-only mode Read/Write mode Recovery Menu (filesystem state: read-only) Recovery Menu (filesystem state: read/write) Repair broken packages Revert to old snapshot and reboot Run in failsafe graphic mode Snapshot System mode: System summary The option you selected requires your filesystem to be in read-only mode. Unfortunately another option you selected earlier, made you exit this mode.
The easiest way of getting back in read-only mode is to reboot your system. Try to make free space Trying to find packages you don't need (apt-get autoremove), please review carefully. Unknown (must be run as root) Update grub bootloader Volume Groups: You are now going to exit the recovery mode and continue the boot sequence. Please note that some graphic drivers require a full graphical boot and so will fail when resuming from recovery.
If that's the case, simply reboot from the login screen and then perform a standard boot. no (BAD) none not ok (BAD) ok (good) unknown (must be run as root) unknown (read-only filesystem) yes (good) Project-Id-Version: friendly-recovery
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2012-03-08 14:38-0500
PO-Revision-Date: 2012-03-15 21:04+0000
Last-Translator: Claudio Arseni <claudio.arseni@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:23+0000
X-Generator: Launchpad (build 18115)
    Ripristina l'avvio normale (Usare i tasti freccia, Pag↑e Pag↓ per scorrere l'elenco, il tasto TAB per selezionare) === Utilizzo dettagliato del disco === === Utilizzo dettagliato della memoria === === Configurazione dettagliata della rete === === Informazioni generali === === Stato LVM === === Stato del software RAID === === Database di sistema (APT) === Informazioni sulla CPU: Controlla tutti i filesystem Continuando il filesystem / verrà nuovamente montato in modalità lettura/scrittura e verranno montati tutti gli altri filesystem presenti in /etc/fstab.
Continuare? Database consistente: Passa a una shell con privilegi di root Abilitare la rete Completato, premere INVIO IP e DNS configurati IP configurato Connettività di rete: Nessun LVM rilaveto (vgscan) Nessun software RAID rilevato (mdstat) Volumi fisici: Modalità solo lettura Modalità lettura/scrittura Menù ripristino (stato del filesystem: sola lettura) Menù ripristino (stato del filesystem: lettura/scrittura) Ripara i pacchetti danneggiati Ripristina la vecchia immagine e riavvia Avvia in modalità grafica di emergenza Immagine Modalità di sistema: Riepilogo del sistema L'opzione selezionata richiede che il filesystem sia in modalità di sola lettura. Sfortunatamente un'altra opzione, selezionata in precedenza, annulla questa modalità.
Il modo più semplice per ottenere la modalità di sola lettura è riavviare il sistema. Prova a liberare spazio Tentativo di rilevamento dei pacchetti non necessari (apt-get autoremove). Esaminare con attenzione. Sconosciuto (deve essere eseguito come utente root) Aggiorna il bootloader grub Gruppi di volumi: Si sta per uscire dalla modalità di rispristino e continuare la sequenza di avvio. Notare che alcuni driver grafici richiedono un avvio grafico completo ed è quindi possibile che non funzionino correttamente all'avvio dopo un rispristino.
In questo caso, riavviare semplicemente dalla schermata di accesso ed eseguire un avvio standard. no (non corretto) nessuna errato (non corretto) ok (corretto) sconosciuto (deve essere eseguito come utente root) sconosciuto (file system in sola lettura) si (corretto) 