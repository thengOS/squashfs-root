��    '      T  5   �      `  ,   a  '   �  (   �  	   �  z   �     d     w     �     �     �     �     	     !     ;     R  !   i     �  $   �     �  -   �  "     "   6  #   Y     }  &   �     �     �     �  (        .     J     i       +   �     �  .   �  ,   �  A   ,  �  n  4   0
  4   e
  (   �
  
   �
  �   �
     `     u     �  #   �     �      �  #     #   :  #   ^  !   �  &   �  !   �  5   �  "   #  4   F  %   {  !   �  +   �  $   �  (        =     \     o  %   �  $   �  #   �     �       .        H  6   ^  4   �  \   �                                         
                    %                          !                 '                              	   $                        &   "         #    'since' option specifies most recent version 'until' option specifies oldest version Copyright (C) 2005 by Frank Lichtenheld
 FATAL: %s This is free software; see the GNU General Public Licence version 2 or later for copying conditions. There is NO warranty. WARN: %s(l%s): %s
 WARN: %s(l%s): %s
LINE: %s
 bad key-value after `;': `%s' badly formatted heading line badly formatted trailer line badly formatted urgency value can't close file %s: %s can't load IO::String: %s can't lock file %s: %s can't open file %s: %s changelog format %s not supported couldn't parse date %s fatal error occured while parsing %s field %s has blank lines >%s< field %s has newline then non whitespace >%s< field %s has trailing newline >%s< found blank line where expected %s found change data where expected %s found eof where expected %s found start of entry where expected %s found trailer where expected %s handle is not open ignored option -L more than one file specified (%s and %s) no changelog file specified output format %s not supported repeated key-value %s too many arguments unknown key-value key %s - copying to XS-%s unrecognised line you can only specify one of 'from' and 'since' you can only specify one of 'to' and 'until' you can't combine 'count' or 'offset' with any other range option Project-Id-Version: libparse-debianchangelog-perl
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-08-25 20:38+0000
PO-Revision-Date: 2016-03-15 08:39+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 l'opzione "since" specifica la versione più recente l'opzione "until" specifica la versione più vecchia Copyright (C) 2005 by Frank Lichtenheld
 ERRORE: %s Questo è software libero; consultare la GNU General Public License versione 2 o successiva per le condizioni di copia. NON c'è alcuna garanzia. AVVISO: %s(l%s): %s
 AVVISO: %s(l%s): %s
RIGA: %s
 key-value errato dopo ";": "%s" riga di intestazione mal formattata riga terminale mal formattata valore di urgenza mal formattato impossibile chiudere il file %s: %s impossibile caricare IO::String: %s impossibile bloccare il file %s: %s impossibile aprire il file %s: %s formato di changelog %s non supportato impossibile analizzare la data %s si è verificato un errore critico nell'analizzare %s il campo %s ha una riga vuota >%s< il campo %s ha un newline e dopo non uno spazio >%s< il campo %s ha un newline finale >%s< trovata riga vuota dove attesa %s trovati dati cambiamento dove era atteso %s trovata fine del file dove attesa %s trovato inizio della voce dove attesa %s trovato termine dove atteso %s gestore non aperto opzione -L ignorata specificato più di un file (%s e %s) nessun file di changelog specificato formato di output %s non supportato key-value %s ripetuto troppi argomenti chiave %s sconosciuta - viene copiata in XS-%s riga non riconosciuta è possibile specificare uno solo tra "from" e "since" è possibile specificare uno solo tra "to" e "until" non è possibile combinare "count" od "offset" con una qualsiasi altra opzione di intervallo 