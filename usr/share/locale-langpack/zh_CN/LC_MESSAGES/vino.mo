��    �      D  �   l      8     9     >     X  *   j  -   �     �     �  A   �  =   4  O   r     �  4   �  '   �     '     F     ^  (   u  $   �  5   �  )   �  0   #  >   T  7   �  	   �     �  ,   �  5        Q     a     }  %   �  
   �     �     �  =   �  �   $  C   �  #        1     K     a     ~  /   �     �  %   �  !   �          /     L  �   O  �     �   �  �   �  �   @  �   �  Z   �  @   �  L   .  �   {  �       �  5  �  )   +     U     u     �     �  /   �  E   �  +   3  5   _  *   �  .   �     �       8   ,     e     x  M   �  M   �     /     6     ?     [  #   x     �     �     �     �  )   �        +   <      h   :   �      �     �   �   �!  8   e"  =   �"  -   �"  )   
#  &   4#  '   [#    �#  �   �$  &   $%     K%     j%  *   {%  $   �%  x   �%  >   D&  B   �&  8   �&  \   �&  6   \'     �'     �'     �'  *   �'     �'     �'     �'     (     "(  )   *(  -   T(     �(     �(  !   �(  ,   �(     �(  >  )     L+     U+     r+  )   �+  &   �+     �+     �+  D   ,  D   R,  S   �,  
   �,  <   �,  %   3-     Y-     o-     -  !   �-  !   �-  *   �-  &   .  3   ..  4   b.  ;   �.  	   �.     �.  $   �.  -   /     M/     Z/     p/  !   �/     �/     �/     �/  2   �/  =   0  :   I0     �0     �0     �0     �0     �0  !   1     '1  $   .1  +   S1     1     �1     �1  �   �1  �   k2  �   �2  �   �3  n   ]4  �   �4  R   _5  D   �5  M   �5  �   E6  y  �6     L9  �   _9  3   ?:     s:  $   �:     �:     �:     �:     �:  !   ;  =   4;  "   r;     �;  !   �;     �;  -   �;     <  "   *<  I   M<  H   �<     �<     �<     �<     =      =     ?=     F=     _=     r=     �=  %   �=  $   �=     �=  B   >     Q>  �   a>  w   S?  ;   �?  G   @  !   O@     q@  *   �@  .   �@  �   �@  �   �A  )   SB      }B     �B      �B     �B  P   �B  0   9C  -   jC  1   �C  G   �C  -   D     @D  
   YD  
   dD  (   oD  
   �D  
   �D     �D     �D  
   �D  (   �D  4   E  �  EE     �G  $   �G  $   H     CH     ?      �   {       X   �                  H   Y   3       @   r      Z       ;   p           q       d   +   w   P   t   x           ,         8             W   :               �   C               z              i       O   >                  0      -       G   
       |   E   ~      &       J   o   u   "   `   v   (   g           A      }   a          U   9   6   _   n           s   4              )             F         h   N   b   l      c   =   Q   f   R       B      �       7   #   y   M       *           K   D             j   I   �   V       5   ]   !          $   S   	           <   k   �   .      /      2   \   1   ^       '   e   L   %   m       [            T        or  %s's remote desktop on %s '%s' disconnected '%s' is remotely controlling your desktop. '%s' rejected the desktop sharing invitation. - Updates Vino password - VNC Server for GNOME A user on the computer '%s' is remotely controlling your desktop. A user on the computer '%s' is remotely viewing your desktop. A user on the computer '%s' is trying to remotely view or control your desktop. Al_ways All remote users will be disconnected. Are you sure? Allow other users to _view your desktop Allowed authentication methods Alternative port number An error has occurred: Another user is controlling your desktop Another user is viewing your desktop Application does not accept documents on command line Are you sure you want to disconnect '%s'? Are you sure you want to disconnect all clients? Automatically _configure UPnP router to open and forward ports Can't pass document URIs to a 'Type=Link' desktop entry Cancelled Changing Vino password.
 Checking the connectivity of this machine... Choose how other users can remotely view your desktop Desktop Sharing Desktop Sharing Preferences Desktop sharing is enabled Disable connection to session manager Disconnect Disconnect %s Disconnect all E-mail address to which the remote desktop URL should be sent ERROR: Maximum length of password is %d character. Please, re-enter the password. ERROR: Maximum length of password is %d characters. Please, re-enter the password. ERROR: You do not have enough permissions to change Vino password.
 Enable remote access to the desktop Enter new Vino password:  Error displaying help Error displaying preferences Error initializing libnotify
 Error while displaying notification bubble: %s
 FILE Failed to open connection to bus: %s
 File is not a valid .desktop file GNOME Desktop Sharing GNOME Desktop Sharing Server ID If not set, the server will listen on all network interfaces.

Set this if you want to accept connections only from some specific network interface. For example, eth0, wifi0, lo and so on. If true, allows remote access to the desktop via the RFB protocol. Users on remote machines may then connect to the desktop using a VNC viewer. If true, do not use the XDamage extension of X.org. This extension does not work properly on some video drivers when using 3D effects. Disabling it will make Vino work in these environments, with slower rendering as a side effect. If true, remote users accessing the desktop are not allowed access until the user on the host machine approves the connection. Recommended especially when access is not password protected. If true, remote users accessing the desktop are only allowed to view the desktop. Remote users will not be able to use the mouse or keyboard. If true, remote users accessing the desktop are required to support encryption. It is highly recommended that you use a client which supports encryption unless the intervening network is trusted. If true, request that a UPnP-capable router should forward and open the port used by Vino. If true, show a notification when a user connects to the system. If true, the screen will be locked after the last remote client disconnects. If true, the server will listen on another port, instead of the default (5900). The port must be specified in the 'alternative-port' key. Licensed under the GNU General Public License Version 2

Vino is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

Vino is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
 Listen on an alternative port Lists the authentication methods with which remote users may access the desktop.

There are two possible authentication methods; "vnc" causes the remote user to be prompted for a password (the password is specified by the vnc-password key) before connecting and "none" which allows any remote user to connect. Lock the screen when last user disconnect Network interface for listening Nobody can access your desktop. Not a launchable item Notify on connect One person is connected %d people are connected One person is viewing your desktop %d people are viewing your desktop Only allow remote users to view the desktop Others can access your computer using the address %s. Password required for "vnc" authentication Prompt the user before completing a connection Received signal %d, exiting. Remote desktop sharing password Remote users are able to control your mouse and keyboard Require encryption Retype new Vino password:  Run 'vino-passwd --help' to see a full list of available command line options Run 'vino-server --help' to see a full list of available command line options Screen Security Session management options: Share my desktop information Share your desktop with other users Sharing Show Notification Area Icon Show Vino version Show session management options Some of these preferences are locked down Sorry, passwords do not match.
 Specify file containing saved configuration Specify session management ID Start in tube mode, for the ‘Share my Desktop’ feature Starting %s The password which the remote user will be prompted for if the "vnc" authentication method is used. The password specified by the key is base64 encoded.

The special value of 'keyring' (which is not valid base64) means that the password is stored in the GNOME keyring. The port which the server will listen to if the 'use-alternative-port' key is set to true. Valid values are in the range of 5000 to 50000. The remote user '%s' will be disconnected. Are you sure? The remote user from '%s' will be disconnected. Are you sure? The router must have the UPnP feature enabled The screen on which to display the prompt There was an error displaying help:
%s There was an error showing the URL "%s" This key controls the behavior of the status icon. There are three options: "always" - the icon will always be present; "client" - the icon will only be present when someone is connected (this is the default behavior); "never" - the icon will not be present. This key specifies the e-mail address to which the remote desktop URL should be sent if the user clicks on the URL in the Desktop Sharing preferences dialog. Unrecognized desktop file Version '%s' Unrecognized launch option: %d VINO Version %s
 Waiting for '%s' to connect to the screen. When the status icon should be shown When true, disable the desktop background and replace it with a single block of color when a user successfully connects. Whether a UPnP router should be used to forward and open ports Whether to disable the desktop background when a user is connected Whether we should disable the XDamage extension of X.org Your XServer does not support the XTest extension - remote desktop access will be view-only
 Your desktop is only reachable over the local network. Your desktop will be shared _About _Allow _Allow other users to control your desktop _Help _Never _Only when someone is connected _Preferences _Refuse _Require the user to enter this password: _You must confirm each access to this machine translator-credits vino-mdns:showusername vino-passwd: password unchanged.
 vino-passwd: password updated successfully.
 vnc;share;remote; Project-Id-Version: vino HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=vino&keywords=I18N+L10N&component=Preferences Dialog
POT-Creation-Date: 2016-02-18 07:50+0000
PO-Revision-Date: 2014-04-02 02:36+0000
Last-Translator: shijing <jingshi@ubuntukylin.com>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2016-06-27 17:21+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
  或者  %s 在 %s 上的远程桌面 “%s”已经断开连接 “%s”正在远程控制您的桌面。 “%s”拒绝了桌面共享邀请。 - 更新 Vino 密码 - GNOME 的 VNC 服务器 位于计算机“%s”上的用户正在远程控制您的桌面。 位于计算机“%s”上的用户正在远程查看您的桌面。 计算机“%s”上的一位用户正在尝试远程查看或控制您的桌面。 总是(_W) 将要断开与所有远程用户的连接。您确定吗？ 允许其他人查看您的桌面(_V) 允许的验证方法 替代端口号 发生了一个错误： 其他人正在控制您的桌面 其它人正在查看您的桌面 应用程序在命令行下不接受文档 您确定要断开连接“%s”吗？ 您确定要断开与所有客户端的连接吗？ 自动配置 UPnP 路由器开放和转发端口(_C) 不能将文档 URI 传递给一个“Type=Link”桌面项 已取消 正在更改 Vino 密码。
 正在检查本机的联网状态... 选择其他人如何远程查看您的桌面 桌面共享 桌面共享首选项 桌面共享已开启 禁用到会话管理器的连接 断开连接 断开连接 %s 断开所有连接 发送远程桌面 URL 地址的电子邮箱地址 错误：密码最长为 %d 字符。请重新输入密码。 错误：您没有足够的权限来更改 Vino 密码。
 允许远程访问桌面 输入新的 Vino 密码：  显示帮助时出错 显示首选项时出错 初始化 libnotify 出错
 显示气泡通知时出错：%s
 文件 打开到总线的连接失败：%s
 文件不是一个有效的 .desktop 文件 GNOME 桌面共享 GNOME 桌面共享服务器 ID 如果不设置该选项，服务器将监听所有的网络接口。

如果您只想接受某个特定网络接口的连接，例如：eth0 ，wifi0 ， lo 等等，设置该选项。 如果为 true，允许通过 RFB 协议远程访问桌面。之后远程机器的的用户可以使用 VNC 查看器连接到桌面。 如果为 true，不要使用 X.org 的 XDamage 扩展。在使用 3D 效果时，该扩展不能在一些视频驱动程序上正常工作。禁用它会让 Vino 在这些环境中工作，但渲染速度慢。 如果为 true，直到主机的用户批准了连接，远程用户才可以访问远程桌面。特别推荐使用于没有密码保护的访问。 如果为 true，远程用户访问桌面时只允许查看桌面。远程用户不能使用鼠标与键盘。 如果为 true，要求访问桌面的远程用户支持加密。强烈推荐您使用支持加密的客户端，除非中间网络足够可信。 如果为 true，要求 UPnP-capable 路由器转发并使用 Vino 打开端口。 如果为 true，当用户连接到系统时，显示通知信息。 如果为 true，屏幕会在最后的远程客户端断开连接后锁定。 如果为 true，服务器将会监听另一个端口，而不是默认 5900 端口。端口必须在 “alternative-port” 键中指定。 在 GNU GPL V2 下授权

Vino 是自由软件；您可以在自由软件基金会发布的 GNU 通用公共
许可证的条款下重新发布或修改它；您应当使用协议的第二版或更高的版本。

Vino 发布的目的是希望它对您有用，但没有任何保证；对于以任何
用途使用它所造成的任何直接或间接后果都不承担任何责任。请参
看 GNU GPL 协议中的细节。

您应该在收到本程序的同时收到了 GNU GPL 协议的副本；如果您
没有收到的话，请给自由软件基金会写信，地址是
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 监听替代端口 列出远程用户访问使用的验证方法。

有两种可能的验证方法：“vnc”在连接之前提示远程用户输入密码(密码由 vnc-password 键指定)或者“none”，允许任何远程用户连接。 最后一个远程用户断开连接时锁住屏幕 用于监听的网络接口 没有人可以访问您的桌面。 不是一个可加载项 连接时提示 %d 个用户已连接 %d 人正在查看您的桌面 仅允许远程用户查看桌面 其他人可以使用这个地址 %s 来访问您的电脑。 “vnc” 认证时所需的密码 完成连接前提示用户 收到信号 %d，正在退出。 远程桌面共享密码： 远程用户能够控制您的鼠标和键盘 请求加密 再输一次新的 Vino 密码：  运行 “vino-passwd --help”查看可用命令行选项的完整列表 运行“vino-server --help”查看可用命令行选项的完整列表 屏幕 安全 会话管理器选项： 共享我的桌面信息 与其它人共享您的桌面 共享 显示通知区域图标 显示 Vino 版本 显示会话管理器选项 某些首选项已被锁定 对不起，两次密码不一致。
 指定包含已保存配置的文件 指定会话管理器 ID 使用“共享我的桌面”功能时，在 Tube 模式中启动 正在启动 %s 在使用“vnc”验证方法时，提示远程用户输入的密码。由此键指定的密码会用 base64 进行编码。

如果为特殊值“keyring”(这不是有效的 base64 编码值)，表示密码存储在 GNOME 密钥环中。 如果 “use-alternative-port” 键设置为真，服务器将要监听端口的有效值在 5000 到 50000 之间。 将要断开远程用户“%s”的连接。您确定吗？ 将要断开与来自“%s”的远程用户的连接。您确定吗？ 路由器必须启用 UPnP 功能 显示提示信息的屏幕 显示帮助时出现了一个错误：
%s 显示网址 “%s” 时出现了一个错误 此键控制状态图标的行为。有三个选项：“总是” - 图标将始终存在；“客户” - 当有连接时，图标才会出现 (默认行为)；“没有” - 图标将不会出现。 此键指定接收远程桌面 URL 的电子邮箱地址。在用户点击了桌面共享首选项对话框内的 URL 时，会向此邮箱发送对应的 URL。 不可识别的桌面文件版本“%s” 不可识别的加载选项：%d VINO 版本 %s
 等待“%s”连接到屏幕。 何时显示状态图标 如果为 true，当用户连接成功时自动将桌面背景替换为单色。 是否使用 UPnP 路由器转发和开放端口 当用户连接时，是否禁用桌面背景 我们是否应当禁用 X.org 的 XDamage 扩展 您的 XServer 不支持 XTest 扩展 - 远程桌面访问只能查看
 您的桌面仅可在本地网络中访问。 您的桌面将被共享 关于(_A) 允许(_A) 允许其他用户控制您的桌面(_A) 帮助(_H) 永不(_N) 只在有其他人连接时(_O) 首选项(_P) 拒绝(_R) 要求远程用户输入此密码(_R)： 必须为对本机器的每次访问进行确认(_Y) Li Shaojie <storm-119@163.com>, 2004
Yang Zhang <zyangmath@gmail.com>, 2007
Deng Xiyue <manphiz@gmail.com>, 2008
甘露(Gan Lu) <rhythm.gan@gmail.com>, 2009
Tao Wei <weitao1979@gmail.com>
Lele Long <schemacs@gmail.com>, 2011

Launchpad Contributions:
  Chen Ming https://launchpad.net/~chenming
  Feng Chao https://launchpad.net/~chaofeng
  Ray Wang https://launchpad.net/~raywang
  Tao Wei https://launchpad.net/~weitao1979
  Xiyue Deng https://launchpad.net/~manphiz
  YunQiang Su https://launchpad.net/~wzssyqa
  Zhang YANG https://launchpad.net/~zyang
  shijing https://launchpad.net/~shijing
  storm https://launchpad.net/~storm-119
  甘露 (Lu Gan) https://launchpad.net/~rhythm-gan 0 vino-passwd：密码没有更改。
 vino-passwd：密码更新成功。
 vnc;share;remote;桌面共享; 