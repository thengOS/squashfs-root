��          �   %   �      p     q     �  +   �  @   �  5        M  ,   _     �     �     �  $   �  	   �  2   �  $   2     W     Z     l  #   �  !   �     �  %   �               8     <     E     Y  �  h     �      	  0   *  B   [  *   �     �  '   �     �          *  (   =  	   f  0   p  #   �     �     �  $   �      	     %	     =	  !   M	     o	     �	     �	     �	     �	     �	                    
                                                                                          	             <b>%s</b>
Hardware present: %s <b>%s</b>
Invalid Driver! <b>Currently Installed Windows Drivers:</b> <span size="larger" weight="bold">Select <i>inf</i> file:</span> Are you sure you want to remove the <b>%s</b> driver? Configure Network Could not find a network configuration tool. Driver is already installed. Error while installing. Install Driver Is the ndiswrapper module installed? Location: Module could not be loaded. Error was:

<i>%s</i>
 Ndiswrapper driver installation tool No No file selected. Not a valid driver .inf file. Please drag an '.inf' file instead. Root or sudo privileges required! Select inf File Unable to see if hardware is present. Windows Wireless Drivers Wireless Network Drivers Yes _Install _Install New Driver _Remove Driver Project-Id-Version: ndisgtk
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-10-02 17:50+0200
PO-Revision-Date: 2009-10-06 21:48+0000
Last-Translator: Tao Wei <weitao1979@gmail.com>
Language-Team: Finnish <fi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:44+0000
X-Generator: Launchpad (build 18115)
 <b>%s</b>
硬件存在: %s <b>%s</b>
无效的驱动程序! <b>当前已经安装的Windows驱动程序:</b> <span size="larger" weight="bold">选择 <i>inf</i> 文件:</span> 你确定要卸载 <b>%s</b> 驱动程序? 配置网络 无法找到一个网络配置工具。 驱动程序已经安装。 安装错误。 安装驱动程序 模块 ndiswrapper 是否已经安装？ 位置： 模块不能被加载。错误是：

<i>%s</i>
 Ndiswrapper驱动程序安装工具 否 没有选择文件。 不是有效的驱动 .inf 文件。 请拖动一个 ".inf" 文件。 需要Root用户权限! 选择INF文件 不能确定硬件是否存在。 Windows无线驱动程序 无线网络驱动程序 是 安装 安装新的驱动程序 卸载驱动程序 