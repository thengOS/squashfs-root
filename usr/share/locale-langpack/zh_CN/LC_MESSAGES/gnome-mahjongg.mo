��    @        Y         �  *   �  �  �     H  6   M  .   �  �   �     9  �   H     �  ;   
	     F	     N	  
   W	     b	     h	     �	     �	     �	     �	     �	     �	     �	  !   �	     
     
     0
     7
     <
     P
     a
     n
  O   �
     �
     �
     �
  	   �
                 	   (  	   2  	   <     F     S     Y     b     p     x     �     �     �     �  #   �     �               9  !   T     v     �     �     �     �      $     5  <  	   r  $   |  0   �  x   �     K  �   [  !   �  <     	   N  	   X     b  	   o     y     �  	   �     �     �     �  	   �     �     �          #  	   <     F     M     `     m  !   �  B   �  
   �     �  
     
          
   ,     7     E     S     d     r  
   �     �     �  
   �  
   �     �  
   �  4   �               %     ,     3     :     A     Q     X     h  K  o     �        /   4                       ?   $   :                      ,                 2         #   ;              <   6   1         %   -   9   @           	      (         +                      8       3      )          5      "                                     *   &   >      .           
       !   0   7       '   =       A matching game played with Mahjongg tiles A solitaire version of the classic Eastern tile game. Tiles are stacked on the board at the start of the game. The goal is to remove all the tiles in as little time as possible. Select two matching tiles and they will disappear from the board, but you can only select a tile if there is an empty space to its left or right at the same level. Be careful: tiles that look alike may actually be slightly different. Date Disassemble a pile of tiles by removing matching pairs Do you want to start a new game with this map? Each puzzle has at least one solution.  You can undo your moves and try and find the solution, restart this game, or start a new one. GNOME Mahjongg GNOME Mahjongg features a variety of starting layouts, some easy and some difficult. If you get stuck, you can ask for a hint, but this adds a large time penalty. Height of the window in pixels If you continue playing the next game will use the new map. Layout: Mahjongg Main game: Maps: Match tiles and clear the board Moves Left: New Game OK Pause the game Paused Preferences Print release version and exit Receive a hint for your next move Redo your last move There are no more moves. Tiles: Time Undo your last move Unpause the game Use _new map Width of the window in pixels You can also try to reshuffle the game, but this does not guarantee a solution. _About _Background color: _Close _Contents _Continue playing _Help _Layout: _Mahjongg _New Game _New game _Preferences _Quit _Restart _Restart Game _Scores _Shuffle _Theme: _Undo game;strategy;puzzle;board; mahjongg map nameCloud mahjongg map nameConfounding Cross mahjongg map nameDifficult mahjongg map nameEasy mahjongg map nameFour Bridges mahjongg map nameOverpass mahjongg map namePyramid's Walls mahjongg map nameRed Dragon mahjongg map nameThe Ziggurat mahjongg map nameTic-Tac-Toe translator-credits true if the window is maximized Project-Id-Version: gnome-games master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-mahjongg&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:17+0000
PO-Revision-Date: 2016-04-15 14:22+0000
Last-Translator: liushuyu <Unknown>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:21+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 用麻将牌玩的对对碰游戏。 一款经典的中国麻将牌游戏。游戏开始时麻将牌摞在牌桌上。游戏的目标是以最快的速度移除所有的牌。选择花色相同的两张牌，它们就会从牌桌上消失，但只有在这张牌左侧或右侧是空的才能选中它。留意有些花色很像但并不一样。 日期： 移除匹配的牌对以拆除牌堆 您想要使用这张地图启动新游戏吗？ 每局都至少有一种解法。您可以撤销几次操作来尝试寻找解法，也可以重新开始一次游戏。 GNOME 麻将牌 GNOME 麻将牌有好多种开局，有些容易，有些困难。如果困住了，您可以寻求提示，但会受到时间上的严重惩罚。 窗口的高度，以像素计。 如果您继续游戏，下一盘游戏将使用新地图。 布局： 对对碰 主游戏： 地图： 匹配麻将牌清空版面 剩下的着法: 新游戏 确认 暂停游戏 暂停 首选项 显示发布版本并退出 获取您的下一步的提示 重做最后一着 没有更多着法了。 牌数： 时间 撤消最后一着 重回游戏 使用新地图(_N) 窗口的宽度，以像素计。 您可以尝试从新洗牌，但是洗牌后不保证有解法。 关于(_A) 背景颜色(_B)： 关闭(_C) 目录(_C) 继续游戏(_C) 帮助(_H) 布局(_L)： 对对碰(_M) 新建游戏(_N) 新游戏(_N) 首选项(_P) 退出(_Q) 重新开始(_R) 重启游戏(_R) 得分(_S) 洗牌(_S) 主题(_T)： 撤消(_U) game;strategy;puzzle;board;游戏;策略;拼图;板; 云 讨厌的十字 困难 简单 四桥 天桥 金字塔之墙 赤龙 巴比伦神塔 踢踏 Yang Zhang <zyangmath@gmail.com>, 2007
Ping Z <zpsigma@gmail.com>, 2007
Xhacker Liu <liu.dongyuan@gmail.com>, 2010

Launchpad Contributions:
  Rockworld https://launchpad.net/~rockrock2222222
  Sphinx Jiang https://launchpad.net/~yishanj
  liushuyu https://launchpad.net/~liushuyu-011-y
  tuhaihe https://launchpad.net/~wangdianjin Ture 如果窗口最大化 