��    o      �  �         `	     a	     t	     �	     �	     �	  *   �	  @   �	  B   
     `
     h
     o
     w
  	   �
     �
     �
     �
     �
     �
     �
          "     )     0     E     _     h     p     �  U   �  	   �       
             $     +     =     K     S     `     h     p     �  
   �  ,   �     �  K   �  %        @     G  Y   P     �     �     �     �     �     �  
   �     �  	   �                     -  H   3  0   |  '   �  P   �  h   &     �     �     �     �  %   �     �          
     #     *  $   C      h     �     �  
   �     �     �     �     �     �     �     �                 �        �     �     �     �  	   �     �     �     �          +     /     7     F     M     S  .   c  ?  �  
   �  
   �     �     �  	   �       ,     ;   E     �     �     �     �     �     �     �     �     �     �     �     
     $     +     2     K     a     h     o     �  X   �  	   �     �     
                    3     G  	   N     X     _     f     y     �  $   �     �  E   �      �          '  3   .     b     r     y     �     �     �  	   �     �     �  	   �     �     �       7     $   E  (   j  L   �  ^   �  	   ?     I     V     o  )        �  	   �     �     �     �     �          7     K     j     z  	   �     �  	   �     �     �  	   �     �  	   �     �  t   �  	   [     e     i     �     �  
   �  
   �     �     �  
   �  
   �       
     
        *     ;     #       )      ,   7   _   Z   a      I   ?   W   o   A      -          X           f      M       0   
   "   k   1      T   '   B   F              5      V      ^   9   Q          O                     4      +      =         %                     (   3   g   G   &       P       i   n   R          H           /   `   c       <   $   >       8   6   Y                          N   ]       *   L   e      b   !          \   D      .   E   C   S   l              m       [       2   ;          :   j         d           @          K       J       	   U       h    %'d hour %'d hours %'d minute %'d minutes %'d second %'d seconds %d B/s %d kB/s %u transfer complete %u transfers complete '%s' wants to connect with this device. Do you want to allow it? '%s' wants to pair with this device. Do you want to allow pairing? ADDRESS Accept Address All categories All types Allow An unknown error occurred Audio device Bluetooth File Transfer Bluetooth Pairing Request Bluetooth Transfer Bluetooth file transfer from %s Camera Cancel Choose files to send Click to select device… Computer Confirm Confirm Bluetooth Connection Confirm Bluetooth PIN Confirm the Bluetooth PIN for '%s'. This can usually be found in the device's manual. Connected Connecting… Connection Decline Device Device _category: Device _type: Devices Disconnected Dismiss Display File reception complete From: Headphones Headphones, headsets and other audio devices Headset If you remove the device, you will have to set it up again before next use. Input devices (mice, keyboards, etc.) Joypad Keyboard Make sure that the remote device is switched on and that it accepts Bluetooth connections Modem Mouse NAME Network No No adapters available Not Set Up Not paired or trusted Open File Paired Paired or trusted Pairing '%s' Phone Please confirm that the following PIN matches the one displayed on '%s'. Please confirm the PIN that was entered on '%s'. Please enter the following PIN on '%s'. Please enter the following PIN on '%s'. Then press “Return” on the keyboard. Please move the joystick of your iCade in the following directions. Then press any of the white buttons. Printer Remote control Remote device to use Remote device's name Remove '%s' from the list of devices? Reveal File Scanner Searching for devices… Select Select device to send to Select the device category to filter Select the device type to filter Send _Files… Send files via Bluetooth Sending %s Sending file %d of %d Show: Tablet There was an error To: Toy Trusted Type Unknown Video device Visible as “%s” and available for Bluetooth file transfers. Transferred files are placed in the <a href="%s">Downloads</a> folder. Wearable Yes You received "%s" via Bluetooth You received a file [FILE...] _Cancel _Close _Keyboard Settings _Mouse & Touchpad Settings _OK _Remove _Remove Device _Retry _Send _Sound Settings approximately %'d hour approximately %'d hours Project-Id-Version: gnome-bluetooth master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-bluetooth&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-01-28 16:59+0000
PO-Revision-Date: 2016-03-25 00:55+0000
Last-Translator: YunQiang Su <wzssyqa@gmail.com>
Language-Team: Chinese (China) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2016-06-27 17:29+0000
X-Generator: Launchpad (build 18115)
Language: 
 %'d 小时 %'d 分钟 %'d 秒 %d 字节/秒 %d kB/秒 %u 传输完成 '%s' 要连接到此设备。是否允许？ “%s”希望与此设备配对，您是否允许配对？ 地址 接受 地址 全部分类 所有类型 允许 发生未知错误 音频设备 蓝牙文件传送 蓝牙配对请求 蓝牙传送 从 %s 蓝牙传输文件 相机 取消 选择要发送的文件 点击选取设备... 电脑 确认 确认蓝牙连接 确认蓝牙 PIN 码 请确认“%s”的蓝牙 PIN 码。这一般可以在蓝牙设备的手册中查到。 已连接 正在连接... 连接 拒绝 设备 设备分类(_C)： 设备类型(_T)： 设备 已断开 拒绝 显示 文件接收完成 从： 耳机 耳机、耳麦及其他音频设备 耳麦 如果您移除此设备，下次使用它之前必须重新设置。 输入设备(鼠标、键盘等) 游戏手柄 键盘 请确认远端设备已打开并接收蓝牙连接 调制解调器 鼠标 名称 网络 没有 无可用适配器 未设置 非配对也非信任的 打开文件 配对的 配对或信任的 正在与“%s”配对 手机 请确认下面的 PIN 与“%s”上显示的一致。 请确认“%s”上输入的 PIN。 请在“%s”上输入以下 PIN 码。 请在“%s”上输入以下 PIN 码并按下键盘中的“回车”键。 请按以下方向移动您 iCade 的游戏操作杆。然后按下任意一个白色按钮。 打印机 远程控制 要使用的远程设备 远端设备名 将“%s”从设备列表中移除么？ 显示文件 扫描仪 正在搜索设备... 选择 选择要发送到的设备 选择要筛选的设备分类 选择要筛选的设备类型 发送文件(_F)... 通过蓝牙设备发送文件 正在发送 %s 正在发送文件 %d / %d 显示： 平板 出错了 到： 玩具 信任的 类型 未知的 视频设备 标记为 “%s” 并且可以进行文件传送。传送后的文件放置于 <a href="%s">下载</a> 文件夹。 可穿戴 是 通过蓝牙收到了“%s” 收到了一个文件 [文件...] 取消(_C) 关闭(_C) 键盘设置(_K) 鼠标与触摸板设置(_M) 确定(_O) 移除(_R) 移除设备(_R) 重试(_R) 发送(_S) 声音设置(_S) 约 %'d 小时 