��    t      �  �   \      �	  �   �	     ~
     �
     �
     �
     �
     �
     �
     �
     �
                &     C     \     n     �  $   �  +   �     �  
   �               &     ,     8     M     U  #   f  3   �     �     �     �     �     �               &     B     `     y     �  
   �     �     �     �     �     �     �                    &     ?     W     j     }     �  :   �  %   �     �     �            +   '  *   S     ~     �     �     �     �     �  
   �  !   �  !   
     ,  	   2     <     Y     r      �      �  .   �     �                     7  $   ?  *   d  *   �  2   �     �     �  
     	     	   #  	   -     7     =     C     I     O     ]     c     w          �     �     �     �     �     �     �     �  �    �   �     @     P     c     j  
   q     |     �     �     �     �     �  "   �               )  	   6     @  $   ^     �     �     �     �     �     �     �     �     �  $   �  -        J     W     ^     c     �     �     �     �  !   �     �               *     ;  	   ?     I     \     l      y     �     �     �     �  *   �     �                  *   !     L     h     x          �  !   �  $   �     �     �     �          #     <     I     Z     x  
   �     �     �     �     �     �     �  *     	   -     7     >     T     s     z  $   �  $   �  )   �               0     =  
   K     V  
   e  
   p  
   {  
   �     �  
   �     �  
   �  
   �     �     �               %  	   2  �   <     �     X   H   L                 =       s           r              p         g             Y      t       R   @   d   `          "   U   :   )   .   ^                  0   J      c   n   V   5   q      M          Z       %   8          h      e       S   &       K   '   ?   7   B   i                     <                     G           2   #       o   l          j   A   $       3   _       	      D   C                 I   +   W          a   Q   m       4       ,   P           >   ]   N   b      T   9   6       \   
   ;      !       *              /   (   f   1   k   O   [   F   E      -        A NULL-terminated array of interfaces which a server must support in order to be listed in the selector. Defaults to "IDL:Bonobo/Embeddable:1.0" if no interfaces are listed About this application About this program... Active All B_oth Bonobo Browser Bonobo Component Browser Bonobo GUI support Bonobo component browser BonoboUI-Hello. CLASS Cannot init libbonoboui code Close the current window Component Browser Component Details Configure UI Copyright 2001, The GNOME Foundation Could not display help for this application Could not initialize Bonobo UI Customi_ze Customize the toolbar DISPLAY Debug Description Detailed Information Details Dock the toolbar Don't use X shared memory extension Dump the entire UI's XML description to the console Event Forwarding FLAGS GTK+ Gdk debugging flags to set Gdk debugging flags to unset General Gnome Hello Gtk+ debugging flags to set Gtk+ debugging flags to unset Has a maximum Zoom level Has a minimum Zoom level Hello, World! Hide t_ips IID Inactive Interface required entry Interfaces required Is continuous Load an additional Gtk module Location Look MODULE Make X calls synchronous Make all warnings fatal Maximum Zoom level Minimum Zoom level NAME Name Name of the window - used for configuration serialization. One of the interfaces that's required Open a new window Orientation Preferred height Preferred width Program class as used by the window manager Program name as used by the window manager SCREEN Select Select a file to open Select a filename to save Select files to open Shadow type Show t_ips Shows available Bonobo components Style of bevel around the toolbar T_ext Text only The User interface container The degree of enlargment The factory pointer The maximum degree of enlargment The minimum degree of enlargment This does nothing; it is only a demonstration. Toolbars Type Undock the toolbar View help for this application Visible Whether X events should be forwarded Whether we have a valid maximum zoom level Whether we have a valid minimum zoom level Whether we zoom continuously (as opposed to jumps) X display to use X screen to use Zoom level _About... _Contents _Dump XML _Edit _File _Help _Hide _Hide toolbar _Icon _Priority text only _Select _Show _Text and Icon _View tooltips corba UI container corba factory is floating toolbars translator-credits whether the toolbar is floating Project-Id-Version: bonobo
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 23:04+0000
PO-Revision-Date: 2008-01-28 20:18+0000
Last-Translator: Funda Wang <Unknown>
Language-Team: zh_CN <i18n-translation@lists.linux.net.cn>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:55+0000
X-Generator: Launchpad (build 18115)
 界面以 NULL 结尾的数组，服务器必须支持该数组才能列在选择器中。如果没有列出界面，默认值为“IDL:Bonobo/Embeddable:1.0” 关于本程序 关于本程序... 激活 全部 二者(_O) Bonobo 浏览器 Bonobo 组件浏览器 Bonobo GUI 支持 Bonobo 组件浏览器 BonoboUI-Hello。 类 无法初始化 libbonoboui 代码 关闭当前窗口 组件浏览器 组件细节 配置 UI 版权 2001，GNOME 基金会 无法显示此应用程序的帮助 无法初始化 Bonobo UI 自定义(_Z) 自定义工具栏 显示 调试 描述 细节信息 细节 停靠工具栏 不使用 X 共享内存扩展技术 将全部 UI 的 XML 描述转存到控制台 事件转发 标记 GTK+ 要设置的 Gdk 调试标志 要取消的 Gdk 调试标志 常规 Gnome Hello 设置 Gtk+ 调试标记 取消 Gtk+ 调试标记的设置 有最大缩放级别 有最小缩放级别 Hello，World！ 隐藏提示(_I) IID 非激活 界面需要输入 必需的界面 是否连续 载入一个附加的 Gtk 模块 位置 外观 模块 使 X 调用同步 使所有的警告消息成为致命错误 最大缩放级别 最小缩放级别 名称 名字 窗口的名称 - 用于配置串行化。 其中一个必需的界面 打开新窗口 方向 首选高度 首选宽度 窗口管理器使用的程序类 由窗口管理器使用的程序名 屏幕 选择 选择要打开的文件 选择要保存的文件名 选择要打开的文件 阴影类型 显示提示(_I) 显示可用的 Bonobo 组件 工具栏立体效果的样式 文本(_E) 仅有文本 用户界面容器 缩放程度 车间指针 最大缩放级别 最小缩放程度 不做任何事；它仅仅是个演示。 工具栏 类型 取消停靠工具栏 查看此应用程序的帮助 可见 是否应转发 X 事件 是否有有效的最大缩放级别 是否有有效的最小缩放级别 是否可连续缩放(和跳跃式相反) 使用的 X 显示 要使用的 X 屏幕 缩放级别 关于(_A)... 目录(_C) 转存 XML(_D) 编辑(_E) 文件(_F) 帮助(_H) 隐藏(_H) 隐藏工具栏(_H) 图标(_I) 仅优先级文本(_P) 选择(_S) 显示(_S) 文本和图标(_T) 查看工具提示(_V) Corba UI 容器 Corba 车间 是浮动的 工具栏 开源软件国际化之简体中文组，http://i18n.linux.net.cn

Launchpad Contributions:
  Feng Chao https://launchpad.net/~chaofeng
  Funda Wang https://launchpad.net/~fundawang 工具栏是否浮动 