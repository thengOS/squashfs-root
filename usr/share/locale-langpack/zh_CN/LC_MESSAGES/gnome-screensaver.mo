��    7      �  I   �      �     �     �     �     �  )   
     4  (   @     i          �  "   �  E   �       %   +     Q     Z     b  )   �     �  *   �     �  /   �     #  	   6  8   @  "   y     �     �     �     �     �     �       D   9  `   ~     �     �  )   	     A	  *   S	  #   ~	  	   �	     �	  D   �	  D   
     R
  !   q
  ,   �
  B   �
  
          '         >  +   _  M  �     �     �     �       !   -     O     _     ~     �     �     �  A   �       !   0  
   R     ]     e     �     �     �     �  *   �     
  	     -   $  !   R     t     �     �     �     �     �     �  ?     &   R  "   y  %   �  "   �     �      �          2     ?  /   X  .   �     �  '   �  *   �  6   (     _  
   m     x     �  *   �         -              0                	      1              4      %       '      *      6                 !   &          #         7   
   "       (      5   /   +              )   2                           ,   $         .            3                         (current) Unix password: Authentication failed. Can't set PAM_TTY=%s Cannot get username Causes the screensaver to exit gracefully Checking… Command to invoke from the logout button Don't become a daemon Enable debugging code Enter new Unix password: Error while changing NIS password. If the screensaver is active then deactivate it (un-blank the screen) Incorrect password. Launch screensaver and locker program Log _Out MESSAGE Message to show in the dialog No longer permitted to access the system. No password supplied Not permitted to gain access at this time. Not used Password has been already used. Choose another. Password unchanged Password: Query the length of time the screensaver has been active Query the state of the screensaver Retype new Unix password: S_witch User… Screensaver Show debugging output Show the logout button Show the switch user button Sorry, passwords do not match Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Time has expired. Turn the screensaver on (blank the screen) Unable to establish service %s: %s
 Username: Version of this application You are required to change your password immediately (password aged) You are required to change your password immediately (root enforced) You have the Caps Lock key on. You must choose a longer password You must wait longer to change your password Your account has expired; please contact your system administrator _Password: _Unlock failed to register with the message bus not connected to the message bus screensaver already running in this session Project-Id-Version: gnome-screensaver master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-01-04 14:27+0000
PO-Revision-Date: 2016-01-05 00:35+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
 当前 UNIX 密码： 验证失败。 不能设置 PAM_TTY=%s 无法获取用户名 使屏幕保护程序正常退出 正在检查... 注销按钮所调用的命令 不成为守护进程 启用调试代码 输入新 UNIX 密码： 更改 NIS 密码出错。 如果屏幕保护程序正活动则使之不活动(解除黑屏) 密码不正确。 加载屏幕保护并锁定程序 注销(_O) MESSAGE 在对话框中显示的消息 不再允许访问系统。 没有提供密码 目前不允许获得访问。 没有使用 密码已经被使用。请重新选择。 密码未改变 密码： 查询屏幕保护程序已经活动的时间 查询屏幕保护程序的状态 重新输入新 UNIX 密码： 切换用户(_W) 屏幕保护程序 显示调试输出 显示注销按钮 显示切换用户按钮 对不起，密码不匹配 通知正在运行的屏幕保护程序进程立即锁定屏幕 屏幕保护程序已激活 %d 秒。
 屏幕保护程序状态为活动
 屏幕保护程序状态为不活动
 屏幕保护当前没有激活。
 时间已到。 开启屏幕保护程序(黑屏) 无法建立服务 %s：%s
 用户名： 此应用程序的版本 您必须立即更改您的密码(密码到期) 您必须立即更改您的密码(root 强制) 您打开了 Caps Lock 键。 你必须选择一个更长些的密码 您必须等待更久以修改您的密码 您的帐户已过期；请联系您的系统管理员 密码(_P)： 解锁(_U) 注册到消息总线失败 未连接到消息总线 本会话中屏幕保护程序已经运行 