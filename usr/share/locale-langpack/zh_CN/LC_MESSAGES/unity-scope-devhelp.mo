��          \      �       �      �      �      �   ;   �      !  �   5       �  ,     �     �        9        A  �   N  7                                          Devhelp Search Devhelp Show Sorry, there are no Devhelp results that match your search. Technical Documents This is an Ubuntu search plugin that enables information from Devhelp to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. devhelp;dev;help;doc; Project-Id-Version: unity-scope-devhelp
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-28 09:22+0000
PO-Revision-Date: 2014-04-10 14:33+0000
Last-Translator: Luo Lei <luolei@ubuntukylin.com>
Language-Team: Chinese (Simplified) <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Devhelp 搜索 Devhelp 显示 对不起，没有在 Devhelp 搜索到相符的内容。 技术文档 这是一个 Ubuntu 搜索插件，能够在 Dash 的”代码“栏目中搜索来自 Devhelp 的信息 。如果您不希望搜索这个内容的来源，可以禁用此插件。 devhelp;dev;help;doc;开发手册;帮助;文档;开发; 