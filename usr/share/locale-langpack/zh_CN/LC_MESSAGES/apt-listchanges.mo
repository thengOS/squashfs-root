��            )         �  $   �     �  g   �     Z     c  *   r     �     �     �  (   �               '     6     B      U  *   v  �   �  ;   *     f  0   |  <   �  r   �  2   ]     �     �  "   �     �       #     �  C  !   	     5	  V   M	     �	     �	  $   �	     �	     �	     
  &   
     =
     J
     W
     n
     ~
  -   �
  '   �
  �   �
  D   ~     �  5   �  <     �   I  /   �          6  #   Q      u     �  '   �                                                                                                              
                  	        %s: Version %s has already been seen %s: will be newly installed <big><b>Changelogs</b></big>

The following changes are found in the packages you are about to install: Aborting Changes for %s Confirmation failed, don't save seen state Continue Installation? Do you want to continue? [Y/n]  Done Ignoring `%s' (seems to be a directory!) Informational notes List the changes Mailing %s: %s News for %s Reading changelogs Reading changelogs. Please wait. The %s frontend is deprecated, using pager The gtk frontend needs a working python-gtk2 and python-glade2.
Those imports can not be found. Falling back to pager.
The error is: %s
 The mail frontend needs a installed 'sendmail', using pager Unknown frontend: %s
 Unknown option %s for --which.  Allowed are: %s. Usage: apt-listchanges [options] {--apt | filename.deb ...}
 Wrong or missing VERSION from apt pipeline
(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version set to 2?)
 You can abort the installation if you select 'no'. apt-listchanges: Changelogs apt-listchanges: News apt-listchanges: changelogs for %s apt-listchanges: news for %s database %s failed to load.
 didn't find any valid .deb archives Project-Id-Version: apt-listchanges
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-17 14:04+0000
PO-Revision-Date: 2016-02-18 04:47+0000
Last-Translator: Xiyue Deng <manphiz@gmail.com>
Language-Team: Debian Chinese GB <debian-chinese-gb@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:57+0000
X-Generator: Launchpad (build 18115)
Generated-By: pygettext.py 1.4
 %s：版本 %s 已经被看过了 %s：将被全新安装 <big><b>更新记录</b></big>

在你将要安装的软件包中发现如下改变： 正在终止 %s 的变更 确认失败，未保存所见状态 继续安装？ 您想继续吗？[Y/n]？  完成 忽略“%s” (似乎是个目录！) 信息备忘 列出更改 正在邮寄给 %s: %s %s 的新消息 读取变更记录(changelogs) 读取变更记录(changelogs)。请稍候。 %s 前端已被废除，改用分页器 Gtk 前端需要可工作的 python-gtk2 和 python-glade2。
无法找到这些组件。回退到使用分页器。
错误信息为：%s
 邮件前端需要一个已安装的“sendmail”，改用分页器 未知前端：%s
 未知选项 %s 用于 --which。允许的有：%s。 用法: apt-listchanges [选项] {--apt | filename.deb ...}
 从 apt 传送来的信息缺少版本号(VERSION)或者存在错误的版本号
(Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version 设为 2 了吗？)
 如果您选择“no”您可以中止安装。 apt-listchanges: 变更日志 apt-listchanges: 新内容 apt-listchanges: %s 的变更日志 apt-listchanges: %s 的新内容 数据库 %s 加载失败。
 没有找到任何有效的 .deb 档案 