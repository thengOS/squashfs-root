��    S      �  q   L             F   %  F   l  B   �  7   �  1   .  @   `  ?   �  F   �  �   (	  ,   �	  J   �	  F   #
  <   j
  7   �
  H   �
  ?   (     h     ~     �     �     �  ,   �  7   �  S     S   e  !   �     �  4   �  ,   (     U     b     u     �  A   �     �     �     �          +     3     ?  B   E  &   �  >   �  4   �     #     )  4   2     g  -   v  (   �     �     �  %   �               +     =     E  (   R     {     �     �  "   �  L   �       7   !  &   Y  W   �  .   �                    '     .     4     8     =     C     [     l  �  s     (  J   ;  Z   �  P   �  3   2  1   f  ?   �  @   �  K        e  -   �  S     @   g  ;   �  3   �  <     B   U     �     �  	   �     �     �  ,   �  /     h   7  P   �  "   �       5   ,  '   b     �     �     �     �  B   �          ,     8     U     c     j  	   q  L   {  %   �  H   �  >   7  	   v     �  3   �     �  .   �  *   �     (     6  #   =     a     o     {     �     �      �     �     �     �      �  R        X  ;   r  !   �  _   �  -   0     ^     c     h     {     �     �     �     �     �     �  	   �            0                 J   .   :                       %   A       E       O          4      -           D       H          *          >            S   /       <      (   3              !      @       B   5       ;   8   Q   N       "   K           #   ,      L   '   &           $   C   +         1      P      G                         7          2      R   ?       
          9   )   	   6      I   F   M       =    
Audio Device:   %s   --audio-buffer n        Use an output audio buffer of 'n' kilobytes
   -@ file, --list file    Read playlist of files and URLs from "file"
   -K n, --end n           End at 'n' seconds (or hh:mm:ss format)
   -R, --remote            Use remote control interface
   -V, --version           Display ogg123 version
   -Z, --random            Play files randomly until interrupted
   -b n, --buffer n        Use an input buffer of 'n' kilobytes
   -d dev, --device dev    Use output device "dev". Available devices:
   -f file, --file file    Set the output filename for a file device
                          previously specified with --device.
   -h, --help              Display this help
   -k n, --skip n          Skip the first 'n' seconds (or hh:mm:ss format)
   -p n, --prebuffer n     Load n%% of the input buffer before playing
   -q, --quiet             Don't display anything (no title)
   -r, --repeat            Repeat playlist indefinitely
   -v, --verbose           Display progress and other status information
   -z, --shuffle           Shuffle list of files before playing
  Input Buffer %5.1f%%  Output Buffer %5.1f%% %sPaused (NULL) (none) --- Cannot open playlist file %s.  Skipped.
 --- Driver %s specified in configuration file invalid.
 255 channels should be enough for anyone. (Sorry, but Vorbis doesn't support more)
 === Could not load default driver and no driver specified in config file. Exiting.
 === Incorrect option format: %s.
 === No such device %s.
 === Option conflict: End time is before start time.
 === Vorbis library reported a stream error.
 Author:   %s Available codecs:  Available options:
 Avg bitrate: %5.1f Big endian 24 bit PCM data is not currently supported, aborting.
 Cannot open %s.
 Comments: %s Couldn't initialise resampler
 Decode options
 Default Description Done. Error opening %s using the %s module.  The file may be corrupted.
 Error: Could not create audio buffer.
 Error: Out of memory in decoder_buffered_metadata_callback().
 Error: Out of memory in new_print_statistics_arg().
 File: File: %s Input buffer size smaller than minimum size of %dkB. Input options
 Internal error parsing command line options.
 Memory allocation error in stats_init()
 Miscellaneous options
 Name Ogg Vorbis stream: %d channel, %ld Hz Output options
 Playing: %s Playlist options
 Success System error The file format of %s is not supported.
 Time: %s Type Unknown error Unrecognised advanced option "%s"
 Usage: ogg123 [options] file ...
Play Ogg audio files and network streams.

 Vorbis format: Version %d Warning from playlist %s: Could not read directory %s.
 Warning: Could not read directory %s.
 Warning: OggEnc does not support this type of AIFF/AIFC file
 Must be 8 or 16 bit PCM.
 Warning: Unexpected EOF in reading WAV header
 bool char default output device double float int none other repeat playlist forever shuffle playlist string Project-Id-Version: vorbis-tools
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-03-26 03:08-0400
PO-Revision-Date: 2010-02-21 08:21+0000
Last-Translator: Feng Chao <chaofeng111@gmail.com>
Language-Team: Simplified Chinese <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:21+0000
X-Generator: Launchpad (build 18115)
 
音频设备：%s   --audio-buffer n        使用大小为 'n' KB 的音频输出缓冲区
   -@ file, --list file    从文件 "file" 读取由文件名和URL 组成的播放列表
   -K n, --end n           在 'n' 秒处结束(或者使用 hh:mm:ss 的格式)
   -R, --remote            使用远程控制界面
   -V, --version           显示ogg123 的版本
   -Z, --random            随机播放文件，直到被中断
   -b n, --buffer n        使用大小为 'n'KB 的输入缓冲
   -d dev, --device dev    使用输出设备 "dev"。可用的设备有：
   -f file, --file file    为文件设备指定输出文件名
                          之前通过 --device 选项指定。
   -h, --help              显示这个帮助
   -k n, --skip n          跳过开始的 'n' 秒(或者使用 hh:mm:ss 的格式)
   -p n, --prebuffer n     播放前先载入输入缓冲的 n%%
   -q, --quiet             不显示任何东西(无标题)
   -r, --repeat            无限循环播放列表
   -v, --verbose           显示进度和其他状态信息
   -z, --shuffle           播放前首先打乱列表中的文件
  输入缓存 %5.1f%%  输出缓存 %5.1f%% %s 暂停 (空) (无) --- 无法打开播放列表 %s。跳过。
 --- 配置文件中指定的驱动%s 无效。
 255 个通道对任何人来说应当都已经足够了。(对不起，Vorbis 真的不支持更多了)
 === 无法装入默认驱动，而且配置文件中未指定驱动。退出。
 === 错误的选项格式：%s。
 === 无该设备 %s。
 === 选项冲突：结束时间早于开始时间。
 === Vorbis 库报告一个流错误。
 作者：%s 可用的编解码器：  可用选项：
 平均码率：%5.1f 大端字节序的 24 位PCM 数据目前尚未支持，退出。
 无法打开%s。
 评论：%s 无法初始化重采样器
 解码选项
 默认 描述 完成。 使用模块 %2$s 打开 %1$s 发生错误。文件内容可能不正确。
 错误：无法创建音频缓存。
 错误：调用 decoder_buffered_metadata_callback() 时内存不足。
 错误：调用 new_print_statistics_arg() 时内存不足。
 文件： 文件：%s 输入缓冲区的大小小于 %dkB 的最小值。 输入选项
 解析命令行参数时发生内部错误。
 调用 stats_init() 时内存分配错误
 其他选项
 名称 Ogg Vorbis 流：%d 频道，%ld Hz 输出选项
 播放：%s 播放列表选项
 成功 系统错误 不支持文件 %s 的格式。
 时间：%s 类型 未知错误 无法识别的高级选项"%s"
 用法：ogg123 [选项] 文件 ...
播放 Ogg 音频文件和网络流媒体。

 Vorbis 格式：版本 %d 来自播放列表 %s 的警告：无法读取目录 %s。
 警告：无法读取目录%s。
 警告：OggEnc 不支持这种类型的 AIFF/AIFC 文件
 必须是 8 位或者 16 位 PCM。
 警告：读取 WAV 头时出现意外结束
 bool char 默认输出设备 double float 整型 无 其他 不断重复播放列表 打乱播放列表 字符串 