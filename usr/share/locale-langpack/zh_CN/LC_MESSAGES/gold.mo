��    A      $  Y   ,      �     �     �     �     �     �     	          '     5  /   =  /   m     �     �     �  ,   �  .     (   3  *   \  -   �     �  $   �  #   �       1   %  &   W  '   ~  	   �     �     �     �  %   �     �     	  +   #	     O	     n	  &   v	     �	     �	     �	     �	  #   �	     �	     
     !
  #   4
  %   X
     ~
     �
     �
     �
     �
     �
     �
     �
     �
  �   �
     �      �  #   �          $     8     G  �  N          0     C     Y     o     ~     �     �     �  0   �  3   �          !     =  4   Y  (   �  *   �  *   �  -        ;     N  $   d     �  6   �      �  *         +     2     K  	   R  -   \     �     �  &   �     �     �  -   �     (     >     E     R  0   Y     �     �     �  $   �  $   �  
     	   "     ,     3     :     K     a     w     �  �   �     G      ]  $   ~  	   �     �     �  
   �     .   ?   9   ,                    4       $                            '   *   "   &   A   6      @                 7   %   2   =             	   -                 >       8   1   /   
                  :   (       ;      )       <          0                 3                 5       +          !             #               
Allocating common symbols
 
Cross Reference Table

 
Discarded input sections

   Supported emulations:
 %s failed: %s %s: %s %s: supported targets: %s: warning:  ADDRESS Allow unresolved references in shared libraries Auxiliary filter for shared object symbol table COUNT Call SYMBOL at load-time Call SYMBOL at unload-time Common symbol       size              file

 Copyright 2014 Free Software Foundation, Inc.
 Create a position independent executable Create an output file even if errors occur Default search path for Solaris compatibility Define a symbol Do not link against shared libraries Do not list removed unused sections Do not page align data Do not page align data, do not make text readonly Don't remove unused sections (default) Don't warn about mismatched input files EMULATION Export all dynamic symbols FILE FILENAME Filter for shared object symbol table Generate relocatable output Ignored Ignored for GCC linker option compatibility Ignored for SVR4 compatibility LIBNAME List removed unused sections on stderr Output cross reference table PATH PLUGIN PROGRAM Page align data, make text readonly Print default output format Read linker script Report bugs to %s
 Report unresolved symbols as errors Report unresolved symbols as warnings SECTION=ADDRESS SHLIB SIZE SYMBOL SYMBOL=EXPRESSION Search for library LIBNAME Set output file name Strip all symbols Symbol This program is free software; you may redistribute it under the terms of
the GNU General Public License version 3 or (at your option) a later version.
This program has absolutely no warranty.
 Treat warnings as errors Use wrapper functions for SYMBOL Warn about duplicate common symbols [=STYLE] cannot open %s: %s: no input files reloc  Project-Id-Version: gold 2.24.90
Report-Msgid-Bugs-To: bug-binutils@gnu.org
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2015-11-10 10:03+0000
Last-Translator: Mingye Wang <arthur2e5@aosc.xyz>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:03+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 
分配公共符号
 
交叉引用表

 
舍弃的输入节

   支持的仿真：
 %s 失败：%s %s：%s %s：支持的目标： %s：警告：  地址 允许共用函数库中有无法解析的参照 指定为某共享对象符号表的辅助过滤器 计数 在加载时间调用符号 在卸载时间调用符号 公共符号            大小              文件

 著作权 2014 自由软件基金会。
 生成一个位置无关的可执行文件 即使发生错误也要创建输出文件 为了 Solaris 兼容性的缺省搜索路径 定义一个符号 不链接到共享库 不要列出已移除的未使用节 不将数据对齐至页边界 不将数据对齐至页边界，不将 text 节只读 不删除未使用的节(默认) 不为不匹配的输入文件发出警告 仿真 导出所有动态符号 文件 文件名 指定为某共享对象符号表的过滤器 生成可重新定位的输出 忽略 为 GCC 链接器选项兼容性忽略 为 SVR4 兼容性所忽略 库名 于标准勘误列出已移除的未使用节 输出交叉引用表 路径 插件程序 程序 将数据对齐至页边界，令 text 节只读 印出缺省输出格式 读取链接脚本 将错误报告到 %s
 将不能解析的符号视作错误 将不能解析的符号视作警告 节=地址 共享库 大小 符号 符号=表达式 搜索库“库名” 设置输出文件名 剔除所有符号信息 符号 这个程序是自由软件；您可以遵循GNU 通用公共授权版本 3 或
(您自行选择的) 稍后版本以再次散布它。
这个程序完全没有任何担保。
 将警告当作错误 使用包装函数作为[符号] 为重复的公共符号给出警告 [=风格] 无法打开%s：%s： 没有输入文件 重定位  