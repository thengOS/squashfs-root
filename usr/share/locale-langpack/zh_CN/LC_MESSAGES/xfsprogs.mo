��    �      ,    <      X  �  Y  �  H  +   �  (   �     #     1     @     Q  &   `  4   �  $   �     �  9      2   :  2   m  7   �  )   �  +     *   .  )   Y  !   �     �  #   �  >   �  '   "     J  "   b     �  1   �  (   �  )   �  ,   '     T  )   o  !   �     �     �  $   �           1  $   L  6   q     �     �  
   �     �  %   �       1   2     d     �  (   �  )   �  $   �  (     )   8     b  D   |  #   �  3   �  %        ?     W  %   s  "   �  '   �  (   �          )     :     K     ^     p     u     �     �     �     �     �  	   �  	   �  	   �  (   �  )     0   7  !   h  3   �  <   �  B   �     >   3   K   :      :   �   6   �      ,!  6   A!     x!     �!     �!     �!  &   �!  *   �!  +    "     L"  G   k"  3   �"  B   �"  B   *#     m#  	   t#     ~#     �#  !   �#      �#     �#     $     $  +   $  ,   >$     k$  <   s$  3   �$  *   �$  !   %  2   1%     d%     �%     �%  "   �%  (   �%     &  +   &  8   B&  !   {&     �&     �&  8   �&  8   '     H'  /   V'  %   �'  $   �'  #   �'  #   �'     (  
   ,(     7(     S(  #   l(  "   �(     �(  "   �(     �(     )  $   )     C)     L)     Q)     U)  0   ])  .   �)  (   �)     �)  #   *  /   **  	   Z*  
   d*  -   o*  6   �*  *   �*  +   �*  .   ++  /   Z+  "   �+     �+     �+  -   �+      ,  8   ,     ;,  %   B,  7   h,     �,     �,  *   �,  +   �,  .   
-  #   9-  !   ]-     -  .   �-  7   �-     �-  �  �-  �  �/  �  H1  6   #4  &   Z4     �4     �4     �4     �4  #   �4  5   �4  '   5     ?5  8   _5  2   �5  >   �5  A   
6  *   L6  ,   w6  +   �6  *   �6  >   �6  !   :7  !   \7  E   ~7  '   �7     �7  (   8  (   08  3   Y8  %   �8  %   �8  %   �8     �8  "   9  $   >9  1   c9     �9  $   �9  (   �9     �9  $   :  4   4:     i:     :     �:  !   �:  1   �:     �:  6   ;     9;     X;     r;     �;  &   �;  -   �;  -   <     3<  ?   F<  '   �<  *   �<     �<     �<     =  "   =     A=     ]=     |=     �=     �=     �=     �=     �=     �=     >     !>     5>     :>     >>     Z>     f>     o>     ~>     �>  $   �>  /   �>     �>  C   ?  I   U?  R   �?  	   �?  -   �?  4   *@  Y   _@  U   �@     A  3   "A     VA     cA     A     �A  *   �A  6   �A  6   B  "   HB  L   kB  @   �B  =   �B  7   7C     oC     |C     �C     �C     �C  !   �C     �C      D     D  $   D  3   8D  	   lD  E   vD  '   �D  '   �D  $   E  1   1E  !   cE     �E     �E  +   �E  ,   �E     F  -    F  3   NF  &   �F  #   �F  "   �F  3   �F  3   $G     XG  6   gG  $   �G     �G  A   �G  5   %H     [H     oH     H     �H  %   �H     �H     �H  "   I     <I     YI     vI  	   �I     �I     �I     �I  4   �I  4   �I  .   J  '   NJ  5   vJ  4   �J     �J  	   �J  0   �J  9   #K  *   ]K  )   �K  6   �K  6   �K  "    L     CL     PL  !   bL     �L  *   �L  	   �L  !   �L  3   �L     M     M  '   %M  $   MM  -   rM  $   �M     �M     �M  0   �M  9   N     VN        [   �   �   �       �   �       -   }   (   l       �   O       �   �   �   �   ^   	   �   s   �       �       8          <   �   I       4               7   j   �   9           �   {   �   �   �       3   �   \       K          �      �       F   r   '   �       w              X       t       B   a   �   _       u   �   R           �   �   �   �      .   C           x   P                         v   6      T      M   �   q       �       �   �   �   ~   )   �       :       G   �   �   d      Y   �          %   1                       ;   �   �           �       A   �   �                     �   �             J       �   �           V   �   &       f   +       k   �   �      �   N   �   W   2   �       /   !   �   �   �   b   L       e         �   m   #       �          h   |   D   `      p   E   $   �   
               0   �       ]             �      �   �   �   y      "   U          ,   n       ?   �   >   z   g   Z   o   S                     =   @   �   5   i   �   *   H   Q   c                   
 flushes a range of bytes in the current memory mapping

 Writes all modified copies of pages over the specified range (or entire
 mapping if no range specified) to their backing storage locations.  Also,
 optionally invalidates so that subsequent references to the pages will be
 obtained from their backing storage locations (instead of cached copies).
 -a -- perform asynchronous writes (MS_ASYNC)
 -i -- invalidate mapped pages (MS_INVALIDATE)
 -s -- perform synchronous writes (MS_SYNC)

 
 maps a range within the current file into memory

 Example:
 'mmap -rw 0 1m' - maps one megabyte from the start of the current file

 Memory maps a range of a file for subsequent use by other xfs_io commands.
 With no arguments, mmap shows the current mappings.  The current mapping
 can be set by using the single argument form (mapping number or address).
 If two arguments are specified (a range), a new mapping is created and the
 following options are available:
 -r -- map with PROT_READ protection
 -w -- map with PROT_WRITE protection
 -x -- map with PROT_EXEC protection
 If no protection mode is specified, all are used by default.

 
Use 'help commandname' for extended help.
     %*.*o Unwritten preallocated extent
  %lld blocks
  FLAG Values:
  at offset %lld
 %s version %s
 %s:  could not write to logfile "%s".
 %s:  lseek64 error on target %d "%s" at offset %lld
 %s:  lseek64 failure at offset %lld
 %s:  offset was probably %lld
 %s:  thread %d died unexpectedly, target "%s" incomplete
 %s:  write error on target %d "%s" at offset %lld
 %s: %s appears to contain a partition table (%s).
 %s: %s appears to contain an existing filesystem (%s).
 %s: XFS_IOC_FSGEOMETRY xfsctl failed: %s
 %s: XFS_IOC_FSGROWFSDATA xfsctl failed: %s
 %s: XFS_IOC_FSGROWFSLOG xfsctl failed: %s
 %s: XFS_IOC_FSGROWFSRT xfsctl failed: %s
 %s: bad chattr command, not +/-X
 %s: bad format string %s
 %s: cannot allocate space for file
 %s: cannot determine geometry of filesystem mounted at %s: %s
 %s: cannot freeze filesystem at %s: %s
 %s: cannot open %s: %s
 %s: cannot read attrs on "%s": %s
 %s: cannot realloc %d bytes
 %s: cannot unfreeze filesystem mounted at %s: %s
 %s: failed to access data device for %s
 %s: failed to access external log for %s
 %s: failed to access realtime device for %s
 %s: failed to open %s: %s
 %s: growfs operation in progress already
 %s: log growth not supported yet
 %s: malloc of %d bytes failed.
 %s: no extents
 %s: premature EOF in prototype file
 %s: proto file %s premature EOF
 %s: read failed on %s: %s
 %s: realtime growth not implemented
 %s: specified file ["%s"] is not on an XFS filesystem
 %s: unknown flag
 ,append-only ,real-time 0x%lx  %lu pages (%llu : %lu)
 Aborting XFS copy - no more targets.
 Aborting XFS copy - reason Aborting XFS copy -- logfile error -- reason: %s
 Aborting target %d - reason All copies completed.
 Allocation of the realtime bitmap failed Allocation of the realtime summary failed Check logfile "%s" for more details
 Completion of the realtime bitmap failed Completion of the realtime summary failed Directory creation failed Due to stripe alignment, the internal log size (%lld) is too large.
 Error completing the realtime space Error encountered creating file from prototype file Error initializing the realtime space Inode allocation failed Inode pre-allocation failed Must fit within an allocation group.
 Pre-allocated file creation failed Realtime bitmap inode allocation failed Realtime summary inode allocation failed See "%s" for more details.
 [-adlpv] [-n nx] [-ais] [off len] [-dnrsw] [off len] [-drsw] [off len] [-f] [-r] [-S seed] [off len] [-r] [off len] [-v] [N] [N] | [-rwx] [off len] [blocks] [command] [off len] [tag ...] advisory commands for sections of a file allocates zeroed space for part of a file assert error:  buf->length = %d, buf->size = %d
 available reserved blocks = %llu
 bad argument count %d to %s, expected %d arguments
 bad argument count %d to %s, expected at least %d arguments
 bad argument count %d to %s, expected between %d and %d arguments
 block device both data su and data sw options must be specified
 both data sunit and data swidth options must be specified
 calls fdatasync(2) to flush the files in-core data to disk calls fsync(2) to flush all in-core file state to disk cannot reserve space change extended inode flags on the currently open file char device close the current open file command "%s" not found
 command %s not found
 data blocks changed from %lld to %lld
 data size %lld too large, maximum is %lld
 data size %lld too small, old size is %lld
 data size unchanged, skipping
 data stripe width (%d) must be a multiple of the data stripe unit (%d)
 data su must be a multiple of the sector size (%d)
 data su/sw must not be used in conjunction with data sunit/swidth
 data sunit/swidth must not be used in conjunction with data su/sw
 direct directory directory create error directory createname error error allocating space for a file error reserving space for a file exit the program external fifo find mapping pages that are memory resident flush a region in the current memory mapping foreign foreign file active, %s command is for XFS filesystems only
 frees reserved space associated with part of a file frees space associated with part of a file freeze filesystem of current file get and/or set count of reserved filesystem blocks give advice about use of memory help for one or all commands inject errors into a filesystem inode max pct unchanged, skipping
 inode max percent changed from %d to %d
 internal list current open files and memory mappings list extended inode flags set on the currently open file log blocks changed from %d to %d
 log changed from %s to %s
 log size unchanged, skipping
 log su should not be used in conjunction with log sunit
 log sunit should not be used in conjunction with log su
 lseek64 error mmap a range in the current file, show mappings need at least %lld allocation groups
 need at most %lld allocation groups
 no files are open, try 'help open'
 no mapped regions, try 'help mmap'
 no such tag -- %s
 non-direct non-numeric argument -- %s
 non-numeric bsize -- %s
 non-numeric extsize argument -- %s
 non-numeric length argument -- %s
 non-numeric mode -- %s
 non-numeric offset argument -- %s
 non-numeric seed -- %s
 non-numeric skip -- %s
 non-numeric truncate argument -- %s
 non-sync none off off len offset (%lld) is before start of mapping (%lld)
 offset (%lld) is beyond end of mapping (%lld)
 offset address (%p) is not page aligned
 open the file specified by path print block mapping for an XFS file range (%lld:%lld) is beyond mapping (%lld:%ld)
 read-only read-write reads a number of bytes at a specified offset reads data from a region in the current memory mapping realtime blocks changed from %lld to %lld
 realtime extent size changed from %d to %d
 realtime size %lld too large, maximum is %lld
 realtime size %lld too small, old size is %lld
 realtime size unchanged, skipping
 regular file reserved blocks = %llu
 reserves space associated with part of a file s shuts down the filesystem where the current file resides socket statistics on the currently open file statistics on the filesystem of the currently open file symbolic link sync too few allocation groups for size = %lld
 too many allocation groups for size = %lld
 truncates the current file at the given offset unfreeze filesystem of current file unmaps the current memory mapping write error writes a number of bytes at a specified offset writes data into a region in the current memory mapping xfs Project-Id-Version: xfsprogs
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-11-27 10:38+1100
PO-Revision-Date: 2008-02-02 18:18+0000
Last-Translator: Tao Wei <weitao1979@gmail.com>
Language-Team: Chinese (China) <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:23+0000
X-Generator: Launchpad (build 18115)
 
 将一定范围内的字节数据刷新到当前的内存映射

 把指定范围内所有修改过的页面副本 (不具体指定则映射全部) 写到相应的回溯存储单元。
 不过，您也可以选择不缓存页面副本，废除页面与回溯存储单元的后续关联性。
 -a -- 执行异步写入动作 (MS_ASYNC)
 -i -- 废除页面映射 (MS_INVALIDATE)
 -s -- 执行同步写入动作 (MS_SYNC)

 
 将当前文件内的特定范围域映射到内存

 示例：
 “mmap -rw 0 1m” - 此命令会将当前文件自起始处撷取 1M 字节的数据进行映射

 在这里所得的内存映射可以被接下来的其它 xfs_io 命令所利用。
 如果不附加参数，mmap 将显示当前的映射镜像。
 此映射镜像可使用单一参数形式 (映射数值或地址) 予以分配。
 如果同时指定了两个参数 (即范围)，程序会创建一个新的映射镜像。以下列出的是有效选项：
 -r -- 映射时采用 PROT_READ 保护模式
 -w -- 映射时采用 PROT_WRITE 保护模式
 -x -- 映射时采用 PROT_EXEC 保护模式
 若不指定以上任一保护模式，默认将选用所有。

 
使用 “help 命令名” 以获取更动帮助。
     %*.*o 未写入的预分配区域
  %lld 个块
  FLAG 值：
  在偏移  %lld
 %s 版本 %s
 %s:无法写入日志文件"%s"。
 %s: 目标 %d "%s" 偏移 %lld 发生 lseek64 错误
 %s 在偏移%lld处发生lseek64错误
 %s : 偏移位置大概为%lld
 %s: 线程 %d 意外终止, 目标设备 "%s" 未完成
 %s: 目标 %d "%s" 偏移 %lld 发生写入错误
 %s：%s 内似乎已包含有一个既存的分区表 (%s)。
 %s：%s 内似乎已包含有一个既存的文件系统 (%s)。
 %s：XFS_IOC_FSGEOMETRY 操控失败：%s
 %s：XFS_IOC_FSGROWFSDATA 操控失败：%s
 %s：XFS_IOC_FSGROWFSLOG 操控失败：%s
 %s：XFS_IOC_FSGROWFSRT 操控失败：%s
 %s：无效的 chattr 命令，它未遵循 +/-X 语法格式
 %s：紊乱的格式字符串 %s
 %s：无法为文件分配空间
 %s：无法确定挂载在 %s 上的文件系统的内部信息：%s
 %s：无法封结 %s 文件系统：%s
 %s：无法打开 %s：%s
 %s：无法读取“%s”的属性：%s
 %s：无法重现分配 %d 字节空间
 %s：无法解封挂载在 %s 的文件系统：%s
 %s：访问 %s 的数据设备失败
 %s：访问 %s 的外部日志失败
 %s：访问 %s 的实时设备失败
 %s：打开 %s 失败：%s
 %s：growfs 操作已正在进行
 %s：尚不支持日志扩充特性
 %s：尝试分配 %d 字节内存空间失败。
 %s：无区域
 %s：样板文件的结束符超前
 %s：样板文件 %s 的结束符超前
 %s：读取 %s 失败：%s
 %s：尚未实现实时扩充特性
 %s：规格文件 ["%s"] 不在 XFS 文件系统上
 %s：未知的标记
 ,只可追加 ,实时 0x%lx %lu 个页面 (%llu : %lu)
 终止 XFS 复制-因为无更多的目标设备
 终止XFS复制－原因 终止 XFS 复制 -- 日志文件错误 -- 原因: %s
 终止目标设备 %d - 原因 全部复制都已完成
 分配实时节点位图失败 分配实时节点摘要失败 更多信息请查看日志文件"%s"
 试图完成实时节点位图创建时失败 试图完成实时节点摘要创建时失败 目录创建失败 根据条码分布显示，内部日志大小 (%lld) 过大。
 试图完成实时空间创建时出错 从样板文件创建真实文件时出错 初始化实时空间出错 节点分配失败 节点预分配失败 必须用分配的簇进行填充
 预分配文件创建失败 实时节点位图分配失败 实时节点摘要分配失败 详细请看"%s"
 [-adlpv] [-n nx] [-ais] [偏移长度] [-dnrsw] [off len] [-drsw] [偏移长度] [-f] [-r] [-S seed] [偏移长度] [-r] [偏移长度] [-v] [N] [N] | [-rwx] [偏移长度] [块数量] [命令] [偏移长度] [标记 ...] 文件分片通告命令 将零字节空间分配为文件块 断言错误: buf->length = %d, buf->size = %d
 可用储备块 = %llu
 参数 %2$s 的数量 %1$d 有误，此处需要 %3$d 个参数。
 参数 %2$s 的数量 %1$d 有误，此处需要至少 %3$d 个参数。
 参数 %2$s 的数量 %1$d 有误，此处需要 %3$d 至 %4$d 之间个参数。
 块设备 必须同时指定数据的 su 和 sw 选项
 必须同时指定数据的 sunit 和 swidth 选项
 调用 fdatasync(2) 以将所有驻留在核心的文件数据更动操作同步到磁盘 调用 fsync(2) 以将所有驻留在核心的文件状态更动操作同步到磁盘 无法储备空间 更改当前打开文件的节点扩展属性标记 字符设备 关闭当前打开的文件 命令 “%s” 未找到
 命令 %s 未找到
 数据块尺寸已由 %lld 更改为 %lld
 数据尺寸 %lld 过大，允许的最大值为 %lld
 数据尺寸 %lld 过小，允许的最小值为 %lld
 数据尺寸未改变，已跳过
 数据条码宽度 sw (%d) 必须是数据条码单元 su (%d) 的倍乘数
 数据条码单元 (su) 必须是扇区大小 (%d) 的倍乘数
 数据 su/sw 选项不能和 sunit/swidth 选项结合使用
 数据 sunit/swidth 不能和 su/sw 选项结合使用
 直接访问 目录 创建目录时出错 创建目录名称时出错 为文件分配空间出错 为文件规划储备空间出错 退出程序 外部 fifo 管道 查找常驻于内存的映射页面 刷新当前内存映射里指定范围域的数据 外部的 打开了一个外部文件，%s 命令只能用于 XFS 文件系统
 释放关联到文件块的储备空间 释放关联到文件块的部分空间 封结对指定文件系统的访问 获取和/或设定文件系统储备块的数量 给出内存使用状况的通告 一个或所有命令的帮助 向文件系统注入错误 节点最大使用率未改变，已跳过
 节点最大使用率已由 %d 更改为 %d
 内部 列出当前已打开的文件和内存映射 列出当前打开文件的节点扩展属性标记 日志块尺寸已由 %d 更改为 %d
 日志位置已由 %s 更改为 %s
 日志尺寸未更改，已跳过
 日志 su 选项不应和 sunit 选项结合使用
 日志 sunit 选项不应和 su 选项结合使用
 lseek64 错误 从当前文件中映射出一定范围数据并显示 为尺寸 = %lld 分配的簇过少
 需要至少 %lld 个分配簇
 没有指定要打开的文件，请尝试执行 “help open”
 没有映射区域，请尝试执行 “help mmap”
 无此标记 -- %s
 非直接访问 非数值的参数 -- %s
 非数值的块尺寸 -- %s
 非数值的区域尺寸参数 -- %s
 非数字的长度参数 -- %s
 非数字的模式号 -- %s
 非数字的偏移量参数 -- %s
 非数值的种子数 -- %s
 非数值的跳跃数 -- %s
 非数值的截断参数 -- %s
 非同步 无 off 偏移长度 偏移量 (%lld) 超前于映射的起始点 (%lld)
 偏移量 (%lld) 延后于映射的终结点 (%lld)
 偏移量地址 (%p) 没有经过页面校准
 打开由指定路径所定位的文件 输出 XFS 文件系统上的一个文件的块映射 范围指标 (%lld:%lld) 延后于映射 (%lld:%ld)
 只读 可读写 在指定的偏移位置读取一组字节数据 从当前内存映射中指定一个区域范围并读出 实时块尺寸已由 %lld 更改为 %lld
 实时区域尺寸已由 %d 更改为 %d
 实时尺寸 %lld 过大，允许的最大值为 %lld
 实时尺寸 %lld 过小，允许的最小值为 %lld
 实时尺寸未更改，已跳过
 一般文件 储备块 = %llu
 储备关联到文件块的空间 s 关闭当前文件所驻留的文件系统 套接字 统计当前打开文件的状态 统计当前打开文件所属文件系统的状态 符号链接 同步 为尺寸 = %lld 分配了过多的簇
 为尺寸 = %lld 分配的簇过多
 在给定的偏移量位置截断当前文件 解封对指定文件系统的访问 撤除当前内存映射 写入错误 在指定的偏移位置写入一组字节数据 向当前内存映射里写入一个区域范围的数据 xfs 