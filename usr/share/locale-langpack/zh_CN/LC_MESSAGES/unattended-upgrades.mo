��            )   �      �     �     �  "   �  '   �  -        9  $   T      y     �  =   �  B   �     ;  $   U  /   z  '   �  #   �  )   �  !      ?   B     �  9   �     �  +   �  +        G     [  ?   u     �  �  �     �     �  *   �     �  -   	     A	     `	      �	     �	  8   �	  5   �	     &
  &   ?
  /   f
     �
  !   �
  #   �
     �
  9        R  5   o     �  -   �  .   �          5  D   S     �                                                              
                                                          	                  All upgrades installed Allowed origins are: %s Cache has broken packages, exiting Cache lock can not be acquired, exiting Download finished, but file '%s' not there?!? GetArchives() failed: '%s' Giving up on lockfile after %s delay Initial blacklisted packages: %s Installing the upgrades failed! Lock could not be acquired (another package manager running?) Package '%s' has conffile prompt and needs to be upgraded manually Package installation log: Packages that are auto removed: '%s' Packages with upgradable origin but kept back:
 Simulation, download but do not install Starting unattended upgrades script The URI '%s' failed to download, aborting Unattended upgrade returned: %s

 Unattended-upgrade in progress during shutdown, sleeping for 5s Unattended-upgrades log:
 Warning: A reboot is required to complete this upgrade.

 Writing dpkg log to '%s' You need to be root to run this application dpkg returned a error! See '%s' for details error message: '%s' package '%s' not upgraded package '%s' upgradable but fails to be marked for upgrade (%s) print debug messages Project-Id-Version: unattended-upgrades
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-21 04:07+0000
PO-Revision-Date: 2009-09-23 14:19+0000
Last-Translator: Wylmer Wang <Unknown>
Language-Team: Simplified Chinese <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 安装了所有的更新 允许的源：%s 缓存存在不完整的软件包，退出 无法获取缓存锁，退出 下载完成，但 '%s' 文件没在那里?!? GetArchives()失败：‘%s’ %s 延迟后放弃 锁定文件 初始化黑名单软件包：%s 安装更新失败！ 无法获取锁(另一个软件包管理器在运行？) 软件包‘%s’有冲突提示，需要手工更新 软件包安装日志： 自动移除了的软件包：“%s” 可更新但保留了原始版本的软件包:
 模拟，下载但不安装 启动无人照管的更新脚本 下载‘%s’失败，异常中断 无人照管更新返回：%s

 关机时无人照管更新正在进行，睡眠 5 秒种 无人照管更新日志：
 警告：需要重新启动以完成此次升级。

 写 dpkg 日志到‘%s’ 您需要 roo t的权限来运行这个程序 dpkg 返回错误！详细信息参见‘%s’ 错误信息：‘%s’ 软件包‘%s’没有更新 软件包 '%s' 可以升级，但是标记为升级时失败（%s） 打印调试信息 