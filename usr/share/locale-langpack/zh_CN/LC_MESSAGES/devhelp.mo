��    k      t  �   �       	  I   !	  J   k	  F   �	  A   �	     ?
     D
     I
     N
     S
     X
     ]
     b
     f
  $   j
     �
     �
  
   �
     �
     �
     �
     �
     �
  K   �
  �   F               .     K  
   a     l     t  !   y  -   �     �     �  6   �  "   (     K     Q     Y     b     t     �  ,   �     �     �     �     �  #        '     -  4   I     ~     �     �     �     �     �     �  2   �       #   .     R     i  '   p  "   �  '   �  "   �  #        *  "   I  '   l     �     �     �     �     �  5   �  0   "  1   S  /   �  5   �     �  "   	     ,     E     d     ~     �     �     �     �     �     �     �     �               "     )  
   /     :     L  /   ^  Z   �     �    �  M     J   Q  S   �  P   �     A     F     K     P     U     Z     _     d     h     l     �     �     �  	   �     �  "   �     �     �  @   �  �   ?     �     �          "  	   2  	   <     F  $   M  4   r     �     �  9   �  $        2     9     @     G     W     j  /   z  	   �  	   �     �     �     �     �        *        >     O     `     |  	   �     �     �  3   �     �  3        :  	   L     V     r     �     �     �     �     �     �          1     8     ?     R  *   n     �  $   �  $   �  6        9     L     h     x     �     �     �  
   �     �  
   �  
   �     
          2     C     T  
   b  
   m     x     �     �  N   �  Z      �  \      ^   U      ]          M   V   .          "   4       L   `   >   -   d   6                       _   0   H       3   i      J      !      ;   2                      1   G               '   Q             g   
       Z       b   7   [   f             	      e         ,   &   5       D   \   R   a           S       %          j       9       <             B   h       I           $   /      A       E           T   )   C       *   F      W       c   @   P   K       +               O         Y          8          #      :                X   k   =   (      ?   N    "name" and "link" elements are required inside '%s' on line %d, column %d "name" and "link" elements are required inside <sub> on line %d, column %d "title", "name" and "link" elements are required at line %d, column %d "type" element is required inside <keyword> on line %d, column %d 100% 125% 150% 175% 200% 300% 400% 50% 75% A developers' help browser for GNOME Back Book Book Shelf Book: Books disabled Cannot uncompress book '%s': %s Developer's Help program Devhelp Devhelp integrates with other applications such as Glade, Anjuta, or Geany. Devhelp is an API documentation browser. It provides an easy way to navigate through libraries, search by function, struct, or macro. It provides a tabbed interface and allows to print results. Devhelp support Devhelp — Assistant Display the version and exit Documentation Browser Empty Page Enabled Enum Error opening the requested link. Expected '%s', got '%s' at line %d, column %d Font for fixed width text Font for text Font for text with fixed width, such as code examples. Font for text with variable width. Fonts Forward Function Group by language Height of assistant window Height of main window Invalid namespace '%s' at line %d, column %d KEYWORD Keyword Language: %s Language: Undefined List of books disabled by the user. Macro Main window maximized state Makes F2 bring up Devhelp for the word at the cursor New _Tab New _Window Opens a new Devhelp window Page Preferences Quit any running Devhelp S_maller Text Search and display any hit in the assistant window Search for a keyword Selected tab: "content" or "search" Show API Documentation Struct The X position of the assistant window. The X position of the main window. The Y position of the assistant window. The Y position of the main window. The height of the assistant window. The height of the main window. The width of the assistant window. The width of the index and search pane. The width of the main window. Title Type Use system fonts Use the system default fonts. Whether books should be grouped by language in the UI Whether the assistant window should be maximized Whether the assistant window should be maximized. Whether the main window should start maximized. Which of the tabs is selected: "content" or "search". Width of the assistant window Width of the index and search pane Width of the main window X position of assistant window X position of main window Y position of assistant window Y position of main window _About _About Devhelp _Close _Find _Fixed width: _Group by language _Larger Text _Normal Size _Preferences _Print _Quit _Side pane _Use system fonts _Variable width:  documentation;information;manual;developer;api; https://wiki.gnome.org/Apps/Devhelp?action=AttachFile&#x26;do=view&#x26;target=devhelp.png translator-credits Project-Id-Version: devhelp master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=devhelp&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-16 12:08+0000
PO-Revision-Date: 2016-02-18 04:57+0000
Last-Translator: Tong Hui <Unknown>
Language-Team: Chinese Simplified <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:10+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 在“%s”内第%d行，第%d列，需要指定元素“name”和“link” 在<sub>内第%d行，第%d列，需要指定元素“name”和“link” 在第%d行，第%d列，需要指定元素“title”，“name”，和“link” 在 <keyword> 内第%d行，第%d列，需要指定元素“name”和“link” 100% 125% 150% 175% 200% 300% 400% 50% 75% GNOME 开发者帮助浏览器 后退 书籍 书架 书籍： 已禁用的书籍 无法解压缩书籍“%s”：%s 开发者帮助程序 Devhelp Devhelp 与 Glade、Anjuta 或 Geany 等软件实现了集成。 Devhelp 是一款 API 文档浏览器。它提供了简便的查看库，搜索函数、结构体或宏的功能。它拥有标签页式的界面，并可以打印结果。 Devhelp 支持 Devhelp - 助手 显示版本后退出 文档浏览器 空白页 已启用 枚举 在打开需要的链接时出错。 需要“%s”但得到“%s”(第%d行，第%d列) 固定宽度文本字体 普通文本字体 固定宽度文本使用的字体，例如示例代码。 变动宽度文本使用的字体。 字体 前进 函数 按语言分组 辅助窗口高度 主窗口高度 非法命名空间“%s”(第%d行，第%d列) 关键字 关键词 语言：%s 语言：未定义 用户禁用的书籍列表。 宏 主窗口最大化 使用 F2 为光标处单词启动 Devhelp 新建标签(_T) 新建窗口(_W) 打开新的 Devhelp 窗口 页面 首选项 退出正在运行的 Devhelp 缩小文本(_M) 在辅助窗口中搜索并显示任何命中内容 搜索一个关键词 所选的标签，是“内容”还是“搜索” 显示 API 文档 结构体 辅助窗口的 X 坐标。 主窗口的 X 坐标。 辅助窗口的 Y 坐标。 主窗口的 Y 坐标。 辅助窗口高度。 主窗口高度。 辅助窗口宽度。 索引和搜索面板宽度。 主窗口宽度。 标题 类型 使用系统字体 使用系统默认字体。 在界面上是否应按语言将书分组 是否最大化辅助窗口。 是否最大化显示辅助窗口。 是否以最大化启动主窗口。 选中哪个标签，“内容”还是“搜索”？ 辅助窗口宽度 索引和搜索面板宽度 主窗口宽度 辅助窗口的 X 坐标 主窗口的 X 坐标 辅助窗口的 Y 坐标 主窗口的 Y 坐标 关于(_A) 关于 Devhelp(_A) 关闭(_C) 查找(_F) 等宽字体(_F)： 按语言分组(_G) 放大文本(_L) 正常大小(_N) 首选项(_P) 打印(_P) 退出(_Q) 侧面板(_S) 使用系统字体(_U) 变宽字体(_V)：  documentation;information;manual;developer;api;文档;信息;说明书;开发; https://wiki.gnome.org/Apps/Devhelp?action=AttachFile&#x26;do=view&#x26;target=devhelp.png Zipeco <Zipeco@btamail.net.cn>, 2002
Funda Wang <fundawang@linux.net.cn>, 2004
YangZhang <zyangmath@gmail.com>, 2007
Den Xiyue <manphiz@gmail.com>, 2008
Richard Ma <richard.ma.19850509@gmail.com>, 2009
Tao Wang <dancefire@gmail.com>, 2010
Aron Xu <aronxu@gnome.org>, 2010
Cheng Lu <chenglu1990@gmail.com>, 2012
Sphinx Jiang <yishanj13@gmail.com>, 2013

Launchpad Contributions:
  Alex Xu https://launchpad.net/~xuhj
  Aron Xu https://launchpad.net/~happyaron
  ChenYi https://launchpad.net/~sgrchen
  Cheng Lu https://launchpad.net/~dawndiy
  Funda Wang https://launchpad.net/~fundawang
  Hinker Liu https://launchpad.net/~hinkerliu-gmail
  Liu Qishuai https://launchpad.net/~lqs
  Qiu Haoyu https://launchpad.net/~timothyqiu
  Richard Ma https://launchpad.net/~richard-ma
  Sphinx Jiang https://launchpad.net/~yishanj13
  Tao Wei https://launchpad.net/~weitao1979
  Tong Hui https://launchpad.net/~tonghuix
  Wylmer Wang https://launchpad.net/~wantinghard
  Xhacker Liu https://launchpad.net/~xhacker
  Xiyue Deng https://launchpad.net/~manphiz
  Zhang YANG https://launchpad.net/~zyang
  ZhangCheng https://launchpad.net/~xxzc
  stone_unix https://launchpad.net/~gaoghy
  甘露 (Lu Gan) https://launchpad.net/~rhythm-gan 