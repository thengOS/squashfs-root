��    "      ,  /   <      �     �     
       
        #  .   :     i  	   |     �     �     �  !   �     �     �     �     �          .  ,   E     r     �     �     �     �  $   �     �     �       �   '     �     �     �     �  �  �     �     �     �     �     �  $   �          *     1     D     K     ^     w     {  	   �     �     �     �  +   �     �     	     	     	     4	     ;	     T	     g	     w	  �   �	     
  	   
  
    
  
   +
                                                                                 	             
         !                                             "          Add input method Addon Advance Appearance Available Input Method Cannot load currently used user interface info Clear font setting Configure Current Input Method Default Default keyboard layout Didn't install related component. Empty Global Config Input Method Input Method Configuration Input Method Default Input method settings: Keyboard layout to use when no input window: Keyboard layout: Language No configuration option for %s. Only Show Current Language Other Please press the new key combination Search Addon Search Input Method Show Advance Option The first input method will be inactive state. Usually you need to put <b>Keyboard</b> or <b>Keyboard - <i>layout name</i></b> in the first place. Unknown Unspecified _Cancel _OK Project-Id-Version: fcitx
Report-Msgid-Bugs-To: fcitx-dev@googlegroups.com
POT-Creation-Date: 2013-10-18 03:07-0400
PO-Revision-Date: 2015-03-16 03:09+0000
Last-Translator: csslayer <Unknown>
Language-Team: Chinese (China) (http://www.transifex.com/projects/p/fcitx/language/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:00+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 添加输入法 附加组件 高级 外观 可用的输入法 无法加载当前用户界面信息 清除字体设置 配置 当前的输入法 默认 默认键盘布局 未安装相关组件。 空 全局配置 输入法 输入法配置 输入法默认 输入法设置： 无输入窗口时的使用的键盘布局: 键盘布局： 语言 %s 无配置选项。 仅显示当前语言 其他 请按下新按键组合 搜索附加组件 搜索输入法 显示高级选项 第一个输入法将作为非激活状态。通常您需要将<b>键盘</b>或<b>键盘 - <i>布局名称</i></b>放在第一个。 未知 未指定 取消(_C) 确认(_O) 