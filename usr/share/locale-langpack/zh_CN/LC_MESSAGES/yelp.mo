��    h      \  �   �      �  '   �  )   �  4   	  '   P	  #   x	     �	  &   �	     �	     �	     �	     
     &
     2
  	   7
     A
     R
     ^
  &   x
     �
     �
     �
     �
  !   �
     �
                    3     @     L     Q     Y     k     �     �     �  
   �  	   �     �  )   �  )   �          6     H     a     o     �  	   �     �  
   �     �     �     �  
   �     �     �  	          
   #     .     >     R     n       7   �     �  0   �  '     %   .  %   T  0   z  <   �  (   �  &        8  S   Q  P   �  r   �  !   i  #   �  !   �     �  #   �  9     :   M  "   �  3   �     �     �     �     	       -        K     [  E   d     �  
   �     �  
   �     �     �  !   
    ,  5   L  4   �  2   �  $   �       %   -  *   S     ~     �     �     �     �     �     �     �  	   �     �          .     A  
   Q     \  !   l  	   �     �     �     �     �     �     �     �     �     �               +     2  	   ?  	   I  +   S  "        �     �     �     �     �     �               )  	   6  	   @     J  	   W     a     x     �     �     �     �     �     �     �       '   &     N  !   [  %   }     �     �  (   �  4     !   6     X     s  C   �  A   �  a        n     �  !   �     �     �  )   �  /     !   M  $   o     �     �     �     �     �     �     �       F        S     j     {     �     �     �  6   �     A          `       "   ^   E   )       ]   c       6   :       9       0   2   K       &   3       S   4       	   .          [   F       G       ,         7   ?   b   @   Y   H              ;   8   <           #   V             !              O   T   g       _           C   I   %   1       a   5      N   f   B   >         M          (   +             e      Z   =   '          D   $      P                     *   X          \   -       L       W   d          J                 Q      R          h         
      /   U    A GtkIconTheme object to get icons from A GtkSettings object to get settings from A YelpApplication instance that controls this window A YelpBookmarks implementation instance A YelpUri with the current location A YelpView instance to control A size adjustment to add to font sizes Add Bookmark All Help All Help Documents An unknown error occurred. Application Back Bookmarks C_opy Code Block Cannot Read Could not load a document Could not load a document for ‘%s’ Database filename Document Not Found Document URI Editor Mode Enable features useful to editors Find… Font Adjustment Forward Get help with Unity GtkIconTheme GtkSettings Help Indexed Install <string/> Invalid compressed data Larger Text Loading State Menu New Window Next Page No bookmarks No href attribute found on yelp:document
 No matching help pages found in “%s”. No matching help pages found. Not enough memory Open Link in New _Window Out of memory Page Description Page ID Page Icon Page Not Found Page Title Previous Page Print… Remove Bookmark Root Title S_end Image To… S_end Video To… Save Code Save Code _Block As… Save Image Search (Ctrl+S) Search for “%s” Search results for “%s” Send email to %s Show Text Cursor Show the text cursor or caret for accessible navigation Smaller Text The ID of the root page of the page being viewed The URI does not point to a valid page. The URI which identifies the document The URI ‘%s’ could not be parsed. The URI ‘%s’ does not point to a valid page. The XSLT stylesheet ‘%s’ is either missing or not valid. The description of the page being viewed The directory ‘%s’ does not exist. The file does not exist. The file ‘%s’ could not be parsed because it is not a well-formed XML document. The file ‘%s’ could not be parsed because it is not a well-formed info page. The file ‘%s’ could not be parsed because one or more of its included files is not a well-formed XML document. The file ‘%s’ does not exist. The filename of the sqlite database The icon of the page being viewed The loading state of the view The location of the XSLT stylesheet The page ‘%s’ was not found in the document ‘%s’. The requested page was not found in the document ‘%s’. The title of the page being viewed The title of the root page of the page being viewed Turn on editor mode Unknown Unknown Error Unknown Error. View Whether the document content has been indexed XSLT Stylesheet Yelp URI You do not have PackageKit. Package install links require PackageKit. _Copy Link Location _Copy Text _Install Packages _Open Link _Save Image As… _Save Video As… documentation;information;manual; Project-Id-Version: yelp master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=yelp&keywords=I18N+L10N&component=General
POT-Creation-Date: 2016-03-21 09:28+0000
PO-Revision-Date: 2016-03-21 23:40+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:23+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 从以下位置获得图标的 GtkIconTheme 对象： 从以下位置获得设置的 GtkSettings 对象： 一个控制此窗口的 YelpApplication 实例。 一个 YelpBookmarks 实现的实例 表示当前位置的 YelpUri 一个用于控制的 YelpView 实例 附加于字号的字符尺寸调整属性 添加书签 所有帮助 所有帮助文档 发生了未知错误。 应用程序 后退 书签 复制代码块(_O) 不可读 无法加载文档 无法加载“%s”的文档 数据库文件名 文档未找到 文档 URI 编辑器模式 启用对编辑器有用的特性 查找… 字体调整 前进 获取 Unity 的帮助 GtkIconTheme GtkSettings 帮助 索引 安装 <string/> 无效的压缩数据 更大字体 载入状态 菜单 新建窗口 下一页 无书签 在 yelp:document 中未发现 href 属性
 未在 %s 中找到帮助页面。 未找到帮助页面。 内存不足 在新窗口打开链接(_W) 内存不足 页面描述 页面编号 页面图标 页面未找到 页面标题 上一页 打印… 删除书签 根标题 图像发送到(_E)... 视频发送到(_E)... 保存代码 代码块另存为(_B)... 保存图像 搜索(Ctrl+S) 搜索“%s” “%s”的搜索结果 给 %s 发送电子邮件 显示文本光标 为辅助功能导航启用文本光标 更小字体 当前浏览页面的根页面 ID URI 未指向一个有效的页面。 用以识别文档的 URI URI“%s”无法解析。 URI %s 未指向一个有效的页面。 XSLT 样式表“%s”可能不存在，或无效。 正在被阅览的页面的描述 目录“%s”不存在。 文件不存在。 无法解析文件“%s”，因为它不是有效的 XML 文档。 无法解析文件“%s”，因为它不是有效的信息页。 无法解析文件“%s”，因为它的一个或多个包含文件不是有效的 XML 文档。 文件“%s”不存在。 Sqlite 数据库文件名 正在被阅览的页面的图标 视图的载入状态 XSLT 样式表的位置 文档 %2$s 中没有找到页面 %1$s。 文档“%s”中没有找到请求的页面。 正在被阅览的页面的标题 当前浏览页面的根页面标题 打开编辑器模式 未知 未知错误 未知错误。 查看 文档内容是否已被索引 XSLT 样式表 Yelp URI 您没有安装 PackageKit，使用包安装链接需要 PackageKit。 复制链接位置(_C) 复制文本(_C) 安装软件包(_I) 打开链接(_O) 图像另存为(_S)... 视频另存为(_S)... documentation;information;manual;文档;信息;手册; 