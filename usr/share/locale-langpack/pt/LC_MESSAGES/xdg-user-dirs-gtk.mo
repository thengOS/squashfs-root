��          |      �             !     5  -   E  '   s  3   �  ,   �     �  �        �     �     �  �       �     �  0   �  &   �  G     0   e  %   �  �   �     �     �     �                            
      	                         Current folder name New folder name Note that existing content will not be moved. There was an error updating the folders Update common folders names to match current locale Update standard folders to current language? User folders update You have logged in in a new language. You can automatically update the names of some standard folders in your home folder to match this language. The update would change the following folders: _Don't ask me this again _Keep Old Names _Update Names Project-Id-Version: 2.20
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-04 03:00+0000
PO-Revision-Date: 2008-02-20 12:19+0000
Last-Translator: portugal <drakferion@gmail.com>
Language-Team: Portuguese <gnome_pt@yahoogroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:21+0000
X-Generator: Launchpad (build 18115)
 Nome atual da pasta Novo nome da pasta Note que o conteúdo da pasta não será movido. Ocorreu um erro ao atualizar as pastas Atualizar os nomes de pastas comuns para corresponderem ao idioma atual Atualizar as pastas padrão para o idioma atual? Atualização de pastas do utilizador Iniciou uma sessão com um novo idioma. Pode atualizar automaticamente os nomes de algumas pastas padrão dentro da sua pasta pessoal para este idioma. A atualização alteraria as seguintes pastas: _Não perguntar novamente _Manter os nomes antigos _Atualizar os nomes 