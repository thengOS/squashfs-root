��    E      D  a   l      �     �  @   	  /   J  &   z  )   �  -   �     �     �          &     =     I      P     q  -   v  =   �  +   �       
   &     1  B   6     y     �  )   �     �  
   �     �  !   �      �  	   	     "	     /	  -   7	     e	     k	     r	  
   �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	  
   �	  #   
     ,
     2
     9
  !   A
  
   c
  )   n
  $   �
  (   �
  *   �
  -     $   ?  +   d  4   �  (   �     �     �                    (  �  .     �  9   �  :     &   Z  &   �  .   �     �  "   �       *        <     H  !   P     r  +   {  D   �  '   �          &  	   2  <   <  %   y     �  9   �     �  
   �     �  '   �        	   ;     E     T  -   \     �     �     �     �     �     �  $   �     �     �                    *     D  -   Q          �     �     �     �  .   �  (   �  *     &   C  (   j  '   �     �  5   �           1  $   :     _     h     q     w        7                     B                 6   1      =   >       E               8       $              ;   %   -       ?   *             D       )   0       '              &      !      <   	   :   +   3                 A      /              C   
   #                          2                (   @   4       5             "       9   ,         .        A triangle Kaleidoscope Add a loopback alpha blending effector with rotating and scaling Add age to video input using scratches and dust Add more saturation to the video input Add some hallucination to the video input Add the ripple mark effect on the video input Bulge Bulges the center of the video Cartoon Cartoonify video input Che Guevara Chrome Detect radioactivity and show it Dice Dices the video input into many small squares Display video input like good old low resolution computer way Dissolves moving objects in the video input Distort the video input Distortion Edge Extracts edges in the video input through using the Sobel operator Fake heat camera toning Flip Flip the image, as if looking at a mirror Heat Historical Hulk Invert and slightly shade to blue Invert colors of the video input Invertion Kaleidoscope Kung-Fu Makes a square out of the center of the video Mauve Mirror Mirrors the video Noir/Blanc Optical Illusion Pinch Pinches the center of the video Quark Radioactive Ripple Saturation Sepia Sepia toning Shagadelic Show what was happening in the past Sobel Square Stretch Stretches the center of the video Time delay Traditional black-white optical animation Transform motions into Kung-Fu style Transform video input into a mauve color Transform video input into a metallic look Transform video input into a waveform monitor Transform video input into grayscale Transform video input into realtime goo'ing Transform video input into typical Che Guevara style Transform yourself into the amazing Hulk Twirl Twirl the center of the video Vertigo Warp Waveform X-Ray Project-Id-Version: 3.8
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-10-29 03:54+0000
PO-Revision-Date: 2015-11-10 03:42+0000
Last-Translator: Duarte Loreto <Unknown>
Language-Team: Portuguese <gnome_pt@yahoogroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:02+0000
X-Generator: Launchpad (build 18115)
Language: pt
 Um caleidoscópio triangular Adiciona um efeito de mistura alfa com rotação e escala Adicionar uma aparência antiga ao vídeo com riscos e pó Aumenta a saturação de cor do vídeo Adiciona alguma alucinação ao vídeo Adiciona o efeito de pingos de chuva no vídeo Relevo Cria um relevo no centro do vídeo Banda Desenhada Dá um aspeto de banda desenhada ao vídeo Che Guevara Cromado Deteta e apresenta radioatividade Pedaços Corta o vídeo em vários pedaços pequenos Apresenta o vídeo com baixa resolução estilo computadores antigos Dissolve objetos em movimento no vídeo Distorce o vídeo Distorção Contornos Extrai as extremidades do vídeo utilizando o operador Sobel Colorização estilo câmara de calor Espelhar Inverter a imagem, como se estivesse a vê-la num espelho Calor Histórico Hulk Inverte e sombreia ligeiramente em azul Inverte as cores do vídeo Inversão Caleidoscópio Kung-Fu Cria um quadrado a partir do centro do vídeo Lilás Espelho Espelha o vídeo Branco/Negro Ilusão de Ótica Beliscar Belisca ou repuxa o centro do vídeo Quark Radioatividade Chuva Saturação Acastanhado Colorização acastanhada Psicadélico Apresenta o que estava a acontecer no passado Sobel Quadrado Esticar Estica o centro do vídeo Atraso temporal Animação ótica tradicional a preto e branco Transforma o movimento em estilo Kung-Fu Transforma o vídeo numa tonalidade lilás Transforma o vídeo num aspeto cromado Transforma o vídeo num monitor de ondas Transforma o vídeo em escalas de cinza Derrete o vídeo em tempo-real Transforma o vídeo num estilo típico do Che Guevara Transforma-o no maravilhoso Hulk Remoinho Cria um remoinho no centro do vídeo Vértice Derreter Ondas Raio-X 