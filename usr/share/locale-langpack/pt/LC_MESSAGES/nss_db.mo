��          �      L      �     �  �   �  -   �  -   �  >   �     >  0   C  ;   t     �     �  "   �     �           :     H     d          �  �  �  "   h  �   �  4   j  ;   �  X   �     4  F   9  >   �      �     �  <   �  2   .	  5   a	     �	      �	      �	  4   �	     
                                
                          	                                          Convert key to lower case Copyright (C) %s Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 Create simple DB database from textual input. Do not print messages while building database INPUT-FILE OUTPUT-FILE
-o OUTPUT-FILE INPUT-FILE
-u INPUT-FILE NAME Print content of database file, one entry a line Report bugs using the `glibcbug' script to <bugs@gnu.org>.
 Write output to file NAME Written by %s.
 cannot open database file `%s': %s cannot open input file `%s' cannot open output file `%s': %s duplicate key problems while reading `%s' while reading database: %s while writing database file: %s wrong number of arguments Project-Id-Version: libnss-db
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2000-09-10 16:37+0200
PO-Revision-Date: 2006-08-07 10:02+0000
Last-Translator: Susana Pereira <susana.pereira@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:03+0000
X-Generator: Launchpad (build 18115)
 Converter a chave para minúsculas Copyright (C) %s Free Software Foundation, Inc.
Este é um programa livre; veja o código fonte para as condições de cópia. NÃO há
garantia; nem mesmo para COMERCIALIZAÇÃO ou ADEQUAÇÃO A UM PROPÓSITO PARTICULAR.
 Cria uma BD simples a partir de uma entrada textual. Não mostra mensagens enquanto se constrói a base de dados FICHEIRO-ENTRADA FICHEIRO-SAÍDA
-o FICHEIRO-SAÍDA FICHEIRO-ENTRADA
-u FICHEIRO-ENTRADA NOME Mostra o conteúdo do ficheiro da base de dados, uma entrada por linha Reporte erros usando o script `glibcbug' para <bugs@gnu.org>.
 Escreve a saida no ficheiro NOME Escrito por %s.
 não é possível abrir o ficheiro da base de dados `%s': %s não é possível abrir o ficheiro de entrada `%s' não é possível abrir o ficheiro de saída `%s': %s chave duplicada problema durante leitura de `%s' enquando lê a base de dados: %s enquanto se escreve no ficheiro da base de dados: %s número de argumentos errado 