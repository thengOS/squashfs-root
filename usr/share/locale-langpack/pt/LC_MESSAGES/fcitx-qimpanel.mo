��    @        Y         �     �     �     �     �     �     �     �  	   �  
   �     �  	   �  
   �  
   �     �                    ,     8  S   I     �     �     �     �     �     �  
   �  
   �  
   �               $     :  
   G     R  	   ^     h  	   w     �     �  
   �     �     �  )   �     �     �                 	   %     /     8     A  -   N     |  +   �     �     �     �     �     �     �     �  �  	     �
  	   �
     �
     �
  
   �
     �
     �
     �
  
                  *     >     R     Z     g     z     �     �  j   �               /     @     S     g  
   {     �     �     �     �     �     �     �                     6     F     a  	   p     z     �  .   �     �     �  	   �     �               #     2     H  9   g  #   �  '   �     �     �     �               %     +     6           &   @            #   ,       >       ;   '                  $   <            5       %           !           /      	          :             +       =   2                 
             )       (   ?       3   7         4       0             .       8                 1   "                 *   9       -           &Apply &Cancel &Cannel &Ok &Refresh AdjustHeight AdjustWidth All Skins Appearance Author BackArrow BackArrowX BackArrowY BackImg CandFontSize Character Map ConfigureFcitx ConfigureIM ConfigureIMPanel Description: Click to select and preview, double-click local to edit, save locally. Exit FirstCandColor FontSize ForwardArrow ForwardArrowX ForwardArrowY Horizontal IndexColor InputColor InputStringPosX InputStringPosY Keyboard Layout Chart MarginBottom MarginLeft MarginRight MarginTop Mozc Edit mode Mozc Tool No input window Online &Help! OtherColor OutputCandPosX OutputCandPosY Please install fcitx-qimpanel-configtool! Preview Qimpanel Settings Restart Skin Skin Design Skin Type SkinFont SkinInfo SkinInputBar Sougo Skin does not support preview and edit! Text Entry Settings... The default configuration has been restored TipsImg Version Vertical Vertical List Virtual Keyboard Warning tips Project-Id-Version: fcitx-qimpanel
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-10 02:31+0000
PO-Revision-Date: 2015-09-24 14:34+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:29+0000
X-Generator: Launchpad (build 18115)
 &Aplicar eCancelar &Canal &Ok &Atualizar AjustarAltura AjustarLargura Todos os Temas Aparência Autor Seta Voltar Atrás Seta Voltar AtrásX Seta Voltar AtrásY BackImg CandFontSize Mapa de Caracteres ConfigurarFcitx ConfigurarIM ConfigurarIMPanel Descrição: Clique para selecionar e pré-visualizar, duplo clique local para edidar, guardar localmente. Saír PrimeiraCorCandidata Tamanho da fonte Seta Para a Frente Seta Para a FrenteX Seta Para a FrenteY Horizontal Cor do indicador Cor de entrada InputStringPosX InputStringPosY Guia de Esquema de Teclado Margem inferior Margem esquerda Margem direita Margem superior Modo de edição Mozc Ferramenta Mozc Sem janela de introdução &Ajuda online! Outra cor OutputCandPosX OutputCandPosY Por favor instale o fcitx-qimpanel-configtool! Pré-visualização Definições do Qimpanel Reiniciar Tema Aparência do tema Tipo de tema Fonte de temas Informação de temas Barra de introdução de temas O Tema Suogo não suporta pré-visualização e edição! Definições de Entrada de Texto... A configuração padrão foi restaurada TipsImg Versão Vertical Lista Vertical Teclado Virtual Aviso Dicas 