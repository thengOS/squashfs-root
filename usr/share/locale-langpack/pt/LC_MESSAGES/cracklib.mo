��          �      ,      �     �  /   �     �     �  +   �      '     H  $   e  (   �  &   �     �     �  *   
  '   5  %   ]  +   �  �  �     ^  .   x     �     �  2   �  &      $   '  *   L  6   w  4   �     �  #   �  +     6   B  4   y  >   �               	                
                                               error loading dictionary it does not contain enough DIFFERENT characters it is WAY too short it is all whitespace it is based on a (reversed) dictionary word it is based on a dictionary word it is based on your username it is based upon your password entry it is derivable from your password entry it is derived from your password entry it is too short it is too simplistic/systematic it looks like a National Insurance number. it's derivable from your password entry it's derived from your password entry you are not registered in the password file Project-Id-Version: cracklib.pt
Report-Msgid-Bugs-To: cracklib-devel@lists.sourceforge.net
POT-Creation-Date: 2014-10-05 10:58-0500
PO-Revision-Date: 2013-02-19 23:05+0000
Last-Translator: Filipe André Pinho <Unknown>
Language-Team: portuguese
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:09+0000
X-Generator: Launchpad (build 18115)
Language: 
 erro ao ler o dicionário não contém caracteres DIFERENTES suficientes é MESMO muito curta é tudo espaços é baseada numa palavra (invertida) do dicionário é baseada numa palavra do dicionário é baseado no seu nome de utilizador é baseado na entrada da sua palavra passe é derivável a partir da entrada da sua palavra passe é derivado a partir da entrada da sua palavra passe é muito curta é demasiado simplista/sistemática parece ser um número da Segurança Social. é derivável a partir da entrada da sua palavra passe é derivado a partir da entrada da sua palavra passe você não se encontra registado no ficheiro de palavras passe 