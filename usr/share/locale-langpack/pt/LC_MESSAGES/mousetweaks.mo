��            )         �  "   �     �  &   �          #     A     Y     f     k      ~     �     �  
   �     �  
   �     �       %        D     T     n     �  e   �     �          5  	   D  !   N  /   p     �  �  �  +   E     q  -   �  %   �  (   �  "   �     "     /     8  %   R     x  #   �  
   �     �     �  '   �     
	  )   	     A	  "   T	     w	     �	  z   �	  $   
  3   <
     p
     �
  -   �
  8   �
     �
                                                   
                     	                                                                - GNOME mouse accessibility daemon Button Style Button style of the click-type window. Click-type window geometry Click-type window orientation Click-type window style Double Click Drag Enable dwell click Enable simulated secondary click Failed to Display Help Hide the click-type window Horizontal Hover Click Icons only Ignore small pointer movements Orientation Orientation of the click-type window. Secondary Click Set the active dwell mode Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Start mousetweaks as a daemon Start mousetweaks in login mode Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Project-Id-Version: 3.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 23:45+0000
PO-Revision-Date: 2012-06-04 11:16+0000
Last-Translator: Mykas0 <Mykas0@gmail.com>
Language-Team: Portuguese <gnome_pt@yahoogroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:24+0000
X-Generator: Launchpad (build 18115)
Language: pt
 - Daemon de acessibilidade do rato do GNOME Estilo do botão Estilo do botão da janela de tipo de clique. Geometria da janela de tipo de clique Orientação da janela de tipo de clique Estilo da janela de tipo de clique Duplo clique Arrastar Ativar o movimento-clique Ativar um clique secundário simulado Falha ao mostar a Ajuda Esconder a janela do tipo de clique Horizontal Clique ao sobrevoar Apenas ícones Ignorar movimentos pequenos do ponteiro Orientação Orientação da janela de tipo de clique. Clique cecundário Definir o modo de movimento-clique Desligar o mousetweaks Clique único Tamanho e posição da janela de tipo de clique. O formato é uma expressão padrão de geometria do sistema de janelas X. Iniciar o mousetweaks como um daemon Iniciar o mousetweaks no modo de início de sessão Texto e ícones Apenas texto Tempo a aguardar antes de um movimento-clique Tempo a aguardar antes de um clique secundário simulado Vertical 