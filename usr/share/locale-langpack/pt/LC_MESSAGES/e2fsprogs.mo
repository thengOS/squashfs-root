��    W      �     �      �     �  &   �     �  .   �          "     )     :     Q     n  	   �     �     �     �  5   �  $   �     	     	     #	      +	     L	     R	     d	     p	     �	  )   �	     �	     �	     �	     �	     �	  (   �	     
  (   =
     f
     �
     �
     �
     �
     �
     �
     �
     �
     �
  
   �
  	                       #     ,     >  /   G     w  >   �     �     �  	   �  
   �     �          	               &     2     D  	   M     W     c          �  
   �     �     �     �     �  -   �  "   +  *   N     y     �     �     �     �     �  �  	     �  /   �       B   3     v     �     �     �     �     �     �                 >   %  '   d     �     �     �     �     �     �     �     �  	   �  :        C     I     R     b  	   w  +   �     �  0   �  $   �     #     ,     =     B     U     ^     p     }     �     �  	   �     �     �     �     �     �     �  2   �     ,  C   >     �     �  	   �  
   �     �     �     �     �     �     �     �     �  	                  8     V     _     k     w          �  3   �  %   �  0        B     W     q     �  ,   �     �         %   S              =          @   <                     W      N               /   1   H       '           L   ;   0       $   )   D   >   B   Q       ?      T      *       	   5       F          K              ,       
       V                  -   4                         6      8            A         M   P      2   3   C   J   U          .      :          E       9          G       +       !                 (   O       R       "      &      #       I       7       %s: journal too short
 %s: no valid journal superblock found
 %s: recovering journal
 %s: won't do journal recovery while read-only
 %u inodes scanned.
 (NONE) <The NULL inode> <The bad blocks inode> <The group descriptor inode> <The undelete directory inode> ALLOCATED Abort Aerror allocating Allocate BLKFLSBUF ioctl not supported!  Can't flush buffers.
 Bad block %u out of range; ignored.
 Bbitmap CLEARED CREATED Cconflicts with some other fs @b Clear Clear HTree index Clear inode Connect to /lost+found Continue Couldn't allocate block buffer (size=%d)
 Create Ddeleted Delete file E@e '%Dn' in %p (%i) EXPANDED Error reading block %lu (%s) while %s.   Error reading block %lu (%s).   Error writing block %lu (%s) while %s.   Error writing block %lu (%s).   Expand FILE DELETED FIXED Ffor @i %i (%Q) is Fix Force rewrite Ignore error Iillegal Illegal number of blocks!
 Lis a link RELOCATED Recreate Relocate Split Ssuper@b Suppress messages Truncate Usage: %s [-F] [-I inode_buffer_blocks] device
 Usage: %s disk
 Warning: illegal block %u found in bad block inode.  Cleared.
 aextended attribute bblock ccompress ddirectory ffilesystem ggroup hHTREE @d @i iinode jjournal llost+found mmultiply-claimed ninvalid oorphaned pproblem in reading journal superblock
 returned from clone_file_block rroot @i sshould be uunattached vdevice while getting next inode while opening %s for flushing while reading in list of bad blocks from file while reading the bad blocks inode while sanity checking the bad blocks inode while trying popen '%s' while trying to flush %s while trying to open %s while trying to re-open %s while updating bad block inode zzero-length Project-Id-Version: e2fsprogs
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-05-17 20:31-0400
PO-Revision-Date: 2010-10-03 23:17+0000
Last-Translator: Carlos Manuel <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 16:07+0000
X-Generator: Launchpad (build 18115)
 %s: journal muito pequeno
 %s: encontrado journal superblock não válido
 %s: a recuperar journal
 %s: não dá para recuperar journal enquanto é de só de leitura
 %u inodes rastreados.
 (Nenhum) <The NULL inode> <The bad blocks inode> <The group descriptor inode> <The undelete directory inode> Alocado Interromper alocar Aerror Allocate BLKFLSBUF ioctl não suportado! Não se pode nivelar buffers.
 Bloco mau %u fora da escala; ignorado.
 Bbitmap Limpo Criado Cconflicts com outros fs @b Apagar Esvaziar indice HTree Limpar inode Conectar a /lost+found Continuar Não foi possível alocar o buffer de blocos (tamanho=%d)
 Criar Ddeleted Apagar ficheiro E@e '%Dn' em %p (%i) Expandido Erro ao ler o bloco %lu (%s) enquanto %s.   Erro ao ler o bloco %lu (%s).   Erro ao escrever o bloco %lu (%s) enquanto %s.   Erro ao escrever o bloco %lu (%s).   Expandir FICHEIRO APAGADO Fixo Ffor @i %i (%Q) é Corrigir Forçar reescrita Ignorar erro Ilegal Número ilegal de blocos!
 Lis uma ligação REALOCADO Recriar Realocar Dividir Ssuper@b Suprimir menssagens Truncar Uso: %s [-F] [-I inode_buffer_blocks] dispositivo
 Uso: disco de %s
 Atenção: bloco ilegal %u encontrado em inode de bloco defeituoso
 aextended atributo bbloco ccompress ddirectory ffilesystem ggroup hHTREE @d @i iinode jjournal llost+found mmultiply-claimed ninvalid oorphaned pproblem em lendo journal superblock
 regressou de clone_file_block rroot @i sshould ser uunattached vdevice ao obter o próximo inode Enquanto abre %s para nivelar ao ler a lista de blocos defeituosos desde ficheiro ao ler o inode dos blocos defeituosos Enquanto sanity verifica o inode dos blocos maus ao tentar popen '%s' Enquanto tenta nivelar %s ao tentar abrir %s ao tentar re-abrir %s ao actualizar o inode dos blocos defeituosos zzero-length 