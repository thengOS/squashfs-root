��          �   %   �      p  4   q     �     �     �      �     �       $   -     R     `  *     T   �  <   �  8   <  V   u     �  �   �  �   �  5   �  :   �     �     �     	  #         D  �   X  	   "  �  ,  C   �	     
      #
     D
  "   [
  %   ~
     �
  '   �
     �
  )   �
  6   %  `   \  <   �  8   �  r   3     �  �   �  �   �  3   �  K   �  	     
        (  2   H     {    �  	   �                                                  	                                                       
                  Authentication is needed to diagnose graphics issues Debug Diagnose Graphics Issues Disable _PAT memory Disable _VESA framebuffer driver Disable bootloader _graphics Display boot messages Enable automatic crash bug reporting Error Message Extra graphics _debug messages List dates on which updates were performed Makes dmesg logs more verbose with details about 3d, plymouth, and monitor detection Only include entries from this date and earlier (YYYY/MM/DD) Only include entries from this date forward (YYYY/MM/DD) Removes splash and quiet from kernel options so you can see kernel details during boot Report an Xorg Bug The grub bootloader has a graphics mode using the VESA framebuffer driver which can sometimes interfere with later loading of the proper video driver.  Checking this forces grub to use text mode only. This pagetable extension can interfere with the memory management of proprietary drivers under certain situations and cause lagging or failures to allocate video memory, so turning it off can prevent those problems. Troubleshoot failure to start a graphical X11 session Turns on the Apport crash detection and bug reporting tool View Errors Workarounds X Diagnostics Settings X.org Diagnostic and Repair Utility Xorg Error Messages vesafb is loaded early during boot so the boot logo can display, but can cause issues when switching to a real graphics driver.  Checking this prevents vesafb from loading so these issues do not occur. xdiagnose Project-Id-Version: xdiagnose
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-04-07 16:55+0000
PO-Revision-Date: 2014-04-28 10:57+0000
Last-Translator: IvoGuerreiro <ivoguerreiro@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:56+0000
X-Generator: Launchpad (build 18115)
 Autenticação é necessária para diagnosticar problemas gráficos Depurar Diagnosticar problemas gráficos Desativar memoria _PAT Desativar _VESA driver framebuffer Desativar os _gráficos do bootloader Mostrar mensagens de arranque Ativar relatório automático de falhas Mensagem de erro Mensagens de gráficos extra _depuração Listar datas em que as atualizações foram realizadas Faz registros dmesg com mais mensagens e com detalhes sobre 3d, plymouth, e deteção do monitor Incluir apenas entradas desta data e anteriores (YYYY/MM/DD) Incluir apenas entradas a partir desta data (YYYY/MM/DD) Remove o splash e o quiet das opções do kernel para que você possa ver os detalhes do kernel durante o arranque Reportar uma falha do Xorg O bootloader do grub tem um modo gráfico que usa o driver VESA framebuffer que algumas vezes pode interferir mais tarde com o carregamento do driver próprio. Marcando isto força o grub a usar o modo de texto apenas. Esta extensão PageTable pode interferir com a gestão de memória de drivers proprietários, em determinadas situações e por sua vez causar atraso ou falhas de alocação de memória de vídeo, como tal desligá-lo pode evitar esses problemas. Solucionar falha a iniciar uma sessão gráfica X11 Liga a detecção de colisão Apport e a ferramenta de relatórios de erros Ver erros Soluções Definições de diagnósticos X Utilitário de diagnóstico e reparação do X.org Mensagens de erro do Xorg vesafb é carregado durante a logo no principio da inicialização para que o logo de boot possa exibir, mas pode causar problemas quando se muda para a placa gráfica. Ao se verificar isto, impede-se o vesafb de carregar, para que esses problemas não ocorrem. xdiagnose 