��    -     �	  �  �      0     1     F      e      �     �     �  T   �     (  3   D  8   x  ;   �  :   �  *   (  <   S  :   �  /   �  D   �  2   @  4   s  ,   �  4   �  <   
  5   G  7   }  5   �  3   �       +   7  8   c  9   �  8   �  8     0   H  0   y  2   �  '   �  8      "   >   0   a   J   �   9   �   7   !  L   O!  2   �!  :   �!  ?   
"  >   J"  >   �"  6   �"  7   �"  8   7#  <   p#  I   �#  )   �#  =   !$  >   _$  7   �$  3   �$  *   
%  "   5%     X%     t%     �%  "   �%  +   �%  #   �%      &  "   3&  "   V&      y&     �&     �&  "   �&     �&      '     '     1'  +   M'  #   y'  *   �'  (   �'  /   �'  (   !(  &   J(  .   q(  3   �(  -   �(  ,   )  1   /)  @   a)  >   �)  .   �)  )   *  6   :*  Q   q*     �*  1   �*  -   +  4   @+  5   u+  I   �+  -   �+  2   #,     V,     f,  5   u,  7   �,  /   �,  Q   -  )   e-    �-  �   �/     S0     o0     �0     �0     �0  
   �0     �0     �0      1  !   #1     E1     Q1  (   g1  3   �1  7   �1     �1     
2     2     <2     T2     s2     �2     �2     �2     �2     �2  $   �2     #3     23     O3  *   f3     �3     �3     �3      �3      �3  	   4     !4     /4     A4     Y4     m4     �4     �4     �4     �4     �4  %   5  &   ,5     S5     l5     �5     �5  2   �5     �5     6     6     06     G6     c6     �6     �6  B   �6     7     7     &7     ;7     R7  (   m7     �7     �7     �7     �7  :   8     A8     \8     {8  `   �8  P   �8     ?9  *   X9     �9  5   �9  4   �9  4   :     9:  ,   S:     �:     �:     �:     �:     �:     �:  %   ;     );     B;     U;  ?   t;  7   �;  3   �;  ;    <     \<     k<  +   �<     �<     �<     �<     �<  &   =  !   ==      _=  +   �=     �=     �=     �=     �=  +   >  %   C>  *   i>  +   �>  4   �>  +   �>     !?     2?      P?  +   q?     �?  (   �?     �?     �?     @  '   @  2   @@  7   s@  +   �@      �@  #   �@  &   A  )   CA  #   mA     �A  *   �A  #   �A  &    B  &   'B     NB     aB  -   xB     �B     �B     �B     �B     �B     C     C     4C     OC     gC  +   ~C     �C     �C     �C     �C     �C     D     D     .D     @D     OD     _D     ~D     �D  )   �D     �D     �D     E     E     -E     DE     TE     rE     �E     �E     �E  '   �E  &   �E  '   "F  2   JF  �  }F     2H  #   GH  '   kH  .   �H     �H     �H  V   �H     LI  6   hI  ?   �I  F   �I  @   &J  (   gJ  F   �J  O   �J  8   'K  L   `K  7   �K  D   �K  6   *L  5   aL  H   �L  4   �L  =   M  =   SM  4   �M     �M     �M  E   �M  @   EN  ,   �N  ,   �N  >   �N  ?   O  7   _O  (   �O  5   �O     �O  )   P  R   <P  J   �P  :   �P  j   Q  0   �Q  .   �Q  J   �Q  Q   +R  A   }R  ?   �R  '   �R  %   'S  @   MS  L   �S      �S  /   �S  H   ,T  0   uT  3   �T  4   �T     U     $U  &   AU     hU  !   �U     �U     �U     �U     �U  )   �U  /   V     JV     _V     V     �V     �V  ,   �V  *   W  0   .W  *   _W     �W  (   �W  .   �W  (   �W  #   X  )   >X  J   hX  .   �X  $   �X  3   Y  ?   ;Y  3   {Y  ,   �Y  $   �Y  0   Z  w   2Z     �Z  #   �Z  &   �Z  0   [  3   9[  V   m[  &   �[  %   �[     \     #\  D   4\  :   y\  5   �\  f   �\  ,   Q]    ~]  �   �_     `  (   3`     \`     h`     �`     �`     �`     �`  /   �`  .   a  
   Aa     La  ,   ga  E   �a  :   �a     b     !b  4   8b  %   mb  %   �b     �b     �b     �b  /    c     0c  ,   Pc  5   }c     �c     �c     �c  ,   �c     +d  (   Jd  4   sd  &   �d  $   �d  	   �d     �d     e     %e     De      de     �e  /   �e  /   �e  )   �e     "f  ,   @f  /   mf     �f      �f  %   �f  !   g  @   $g     eg     �g     �g     �g  !   �g  %   �g  $   h  $   ;h  M   `h     �h     �h     �h     �h  $   i  /   (i  %   Xi  $   ~i  $   �i     �i  Q   �i  !   0j  +   Rj     ~j  /   �j  .   �j      �j  :   k  &   Kk  D   rk  C   �k  B   �k  !   >l  G   `l     �l     �l     �l     �l     m      &m  *   Gm     rm     �m  %   �m  N   �m  9   n  :   Mn  B   �n     �n  $   �n  8   o  )   @o     jo     �o     �o  6   �o  2   �o  0   *p  A   [p  1   �p  $   �p     �p  #   
q  7   .q  %   fq  .   �q  3   �q  5   �q  -   %r     Sr  /   hr  *   �r  7   �r  !   �r  =   s     [s      {s  
   �s  /   �s  >   �s  B   t  1   Yt  )   �t  +   �t  2   �t  1   u  )   Fu  +   pu  +   �u  ,   �u  8   �u  6   .v     ev     |v  4   �v     �v     �v     �v     w     #w  
   9w  $   Dw  )   iw  ,   �w  (   �w  ?   �w     )x     >x  '   Xx  $   �x  "   �x      �x     �x      y     )y     =y  (   [y     �y     �y  :   �y     �y     z     %z     @z     Uz     sz  *   �z  /   �z  *   �z  "   {  
   .{  6   9{  0   p{  +   �{  D   �{         �   �   �   �         �       �       �   �   O      M       �   �   �   #      �   �   $   �          U   �   �           �          �   x      �   �   [           z       c   �   �   �              �   %  �               ~   �   	  V   "         �   �            n   �     �     �   b      L          X       �       +     �   0       s       (  �   �   �     �   �   @        �   d   �       ,   e   '   C       /              �   �   5   4   �                  f   �   ^               �          ,      #        �   �   r          �       �   A   �       �   �   )  �           Y   �       H     �   t      p            $          &  �   +   %   �       v   �   *   h   !     �       g   �   	   ;   .   >   &   T       \           !  �      J   �   �   j   �   �   �          �   �   �       (   �   
  {   :                S   m                 �     �   �   3       �       �   �   }   R   K                           �   �   �     o               D         E   `   a           �     i     �                  u   �   w       �   F   �   �   �       7   B       �   �       ]   �   '      �           �   �       2   �   �   �   �   "  �       I   )       �       -   �   _       �   8       �     k       P                    �   �   �               �   �   �          �      �       |   �   y         1   �   �   �      �   <   ?     �   �   N   W       =         �      q   �   G   -  �     *  �     9   �      l   �   �   �   Z   �   Q   6          �       �   
              �      

Symbols from %s:

 

Undefined symbols from %s:

 
 The Directory Table is empty.
 
 The File Name Table is empty.
 
Archive index:
 
The %s section is empty.
         possible <machine>: arm[_interwork], i386, mcore[-elf]{-le|-be}, ppc, thumb
        %s -M [<mri-script]
       --exclude-symbols <list> Don't export <list>
       --export-all-symbols   Export all symbols to .def
       --no-default-excludes  Clear default exclude symbols
       --no-export-all-symbols  Only export listed symbols
     Offset   Begin    End      Expression
    --add-indirect         Add dll indirects to export file.
    --base-file <basefile> Read linker generated base file
    --def <deffile>        Name input .def file
    --dllname <name>       Name of input dll to put into output lib.
    --dlltool-name <dlltool> Defaults to "dlltool"
    --driver-flags <flags> Override default ld flags
    --driver-name <driver> Defaults to "gcc"
    --dry-run              Show what needs to be run
    --entry <entry>        Specify alternate DLL entry point
    --exclude-symbols <list> Exclude <list> from .def
    --export-all-symbols     Export all symbols to .def
    --image-base <base>    Specify image base address
    --implib <outname>     Synonym for --output-lib
    --machine <machine>
    --mno-cygwin           Create Mingw DLL
    --no-default-excludes    Zap default exclude symbols
    --no-export-all-symbols  Only export .drectve symbols
    --no-idata4           Don't generate idata$4 section
    --no-idata5           Don't generate idata$5 section
    --output-def <deffile> Name output .def file
    --output-exp <outname> Generate export file.
    --output-lib <outname> Generate input library.
    --quiet, -q            Work quietly
    --target <machine>     i386-cygwin32 or i386-mingw32
    --verbose, -v          Verbose
    --version              Print dllwrap version
    -D --dllname <name>       Name of input dll to put into interface lib.
    -F --linker-flags <flags> Pass <flags> to the linker.
    -L --linker <name>        Use <name> as the linker.
    -M --mcore-elf <outname>  Process mcore-elf object files into <outname>.
    -U                     Add underscores to .lib
    -V --version              Display the program version.
    -a --add-indirect         Add dll indirects to export file.
    -b --base-file <basefile> Read linker generated base file.
    -d --input-def <deffile>  Name of .def file to be read in.
    -e --output-exp <outname> Generate an export file.
    -h --help                 Display this information.
    -k                     Kill @<n> from exported names
    -l --output-lib <outname> Generate an interface library.
    -m --machine <machine>    Create as DLL for <machine>.  [default: %s]
    -v --verbose              Be verbose.
    -x --no-idata4            Don't generate idata$4 section.
    -z --output-def <deffile> Name of .def file to be created.
    @<file>                   Read options from <file>.
    @<file>                Read options from <file>
   @<file>      - read options from <file>
   DWARF Version:               %d
   Entry	Dir	Time	Size	Name
   Extended opcode %d:    Generic options:
   Initial value of 'is_stmt':  %d
   Length:                              %ld
   Length:                      %ld
   Length:                   %ld
   Line Base:                   %d
   Minimum Instruction Length:  %d
   No emulation specific options
   Options for %s:
   Options passed to DLLTOOL:
   Prologue Length:             %d
   Set ISA to %lu
   Set basic block
   Set epilogue_begin to true
   Set prologue_end to true
   Size of area in .debug_info section: %ld
   Unknown opcode %d with operands:    Version:                             %d
   [-X32]       - ignores 64 bit objects
   [-X32_64]    - accepts 32 and 64 bit objects
   [-X64]       - ignores 32 bit objects
   [-g]         - 32 bit small archive
   [N]          - use instance [count] of name
   [P]          - use full path names when matching
   [S]          - do not build a symbol table
   [V]          - display the version number
   [a]          - put file(s) after [member-name]
   [b]          - put file(s) before [member-name] (same as [i])
   [c]          - do not warn if the library had to be created
   [f]          - truncate inserted file names
   [o]          - preserve original dates
   [s]          - create an archive index (cf. ranlib)
   [u]          - only replace files that are newer than current archive contents
   [v]          - be verbose
   d            - delete file(s) from the archive
   m[ab]        - move file(s) in the archive
   p            - print file(s) found in the archive
   q[f]         - quick append file(s) to the archive
   r[ab][f][u]  - replace existing or insert new file(s) into the archive
   t            - display contents of archive
   x[o]         - extract file(s) from the archive
  (start == end)  (start > end)  Convert addresses into line number/file name pairs.
  Convert an object file into a NetWare Loadable Module
  Generate an index to speed access to archives
  If no addresses are specified on the command line, they will be read from stdin
  Removes symbols and sections from files
  The options are:
  -I --input-target=<bfdname>   Set the input binary file format
  -O --output-target=<bfdname>  Set the output binary file format
  -T --header-file=<file>       Read <file> for NLM header information
  -l --linker=<linker>          Use <linker> for any linking
  -d --debug                    Display on stderr the linker command line
  @<file>                       Read options from <file>.
  -h --help                     Display this information
  -v --version                  Display the program's version
  The options are:
  @<file>                Read options from <file>
  -h --help              Display this information
  -v --version           Display the program's version

  [without DW_AT_frame_base]  command specific modifiers:
  commands:
  emulation options: 
  generic modifiers:
 #lines %d  %s exited with status %d %s is not a valid archive %s: Can't open input archive %s
 %s: Can't open output archive %s
 %s: Error:  %s: Matching formats: %s: Multiple redefinition of symbol "%s" %s: Path components stripped from image name, '%s'. %s: Symbol "%s" is target of more than one redefinition %s: Warning:  %s: bad number: %s %s: can't find module file %s
 %s: can't open file %s
 %s: file %s is not an archive
 %s: fread failed %s: invalid output format %s: invalid radix %s: no archive map to update %s: no open archive
 %s: no open output archive
 %s: no output archive specified yet
 %s: no symbols %s: supported architectures: %s: supported targets: %s:%d: Ignoring rubbish found on this line '%s': No such file (Unknown location op) (User defined location op) (declared as inline and inlined) (declared as inline but ignored) (inlined) (not inlined) <OS specific>: %d <no .debug_str section> <offset is too big> <processor specific>: %d <unknown>: %d Added exports to output file Adding exports to output file BFD header file version %s
 C++ base class not defined C++ base class not found in container C++ data member not found in container C++ object has no fields Can't have LIBRARY and NAME Can't open def file: %s Can't open file %s
 Cannot produce mcore-elf dll from archive file: %s Contents of the %s section:

 Created lib file Creating library file: %s Creating stub file: %s Current open archive is %s
 Deleting temporary base file %s Deleting temporary def file %s Deleting temporary exp file %s Displaying the debug contents of section %s is not yet supported.
 Done reading %s End of Sequence

 Excluding symbol: %s Generated exports file Generating export file: %s Internal error: Unknown machine type: %d Keeping temporary base file %s Keeping temporary def file %s Keeping temporary exp file %s LIBRARY: %s base: %x Location list starting at offset 0x%lx is not terminated.
 Machine '%s' not supported Multiple renames of section %s NAME: %s base: %x Name                  Value           Class        Type         Size             Line  Section

 Name                  Value   Class        Type         Size     Line  Section

 No entry %s in archive.
 No location lists in .debug_info section!
 No member named `%s'
 Offset 0x%lx is bigger than .debug_loc section size.
 Only DWARF 2 and 3 aranges are currently supported.
 Only DWARF 2 and 3 pubnames are currently supported
 Opened temporary file: %s Path components stripped from dllname, '%s'. Processed def file Processed definitions Processing def file: %s Processing definitions Report bugs to %s
 Scanning object file %s Sucking in info from %s section in %s Supported architectures: Supported targets: Syntax error in def file %s:%d The line info appears to be corrupt - the section is too small
 There is a hole [0x%lx - 0x%lx] in .debug_loc section.
 There is an overlap [0x%lx - 0x%lx] in %s section.
 There is an overlap [0x%lx - 0x%lx] in .debug_loc section.
 Tried file: %s Unable to open base-file: %s Unable to open temporary assembler file: %s Unhandled data length: %d
 Unknown FORM value: %lx Unknown TAG value: %lx Unrecognized form: %lu
 Usage %s <option(s)> <object-file(s)>
 Usage: %s <option(s)> in-file(s)
 Usage: %s [option(s)] [addr(s)]
 Usage: %s [option(s)] [in-file [out-file]]
 Usage: %s [option(s)] in-file
 Usage: %s [options] archive
 Using file: %s Value for `N' must be positive. Warning, ignoring duplicate EXPORT %s %d,%d Warning: '%s' is not an ordinary file Warning: changing type size from %d to %d
 Warning: could not locate '%s'.  reason: %s `N' is only meaningful with the `x' and `d' options. `u' is only meaningful with the `r' option. bad ATN65 record bad C++ field bit pos or size bad type for C++ method function badly formed extended line op encountered!
 blocks left on stack at end can't set BFD default target to `%s': %s cannot delete %s: %s cannot open '%s': %s creating %s debug_end_common_block: not implemented debug_find_named_type: no current compilation unit debug_get_real_type: circular debug information for %s
 debug_make_undefined_type: unsupported kind debug_name_type: no current file debug_record_label: not implemented debug_record_variable: no current file debug_start_common_block: not implemented debug_tag_type: extra tag attempted debug_tag_type: no current file debug_write_type: illegal type encountered error: the input file '%s' is empty failed to open temporary head file: %s failed to open temporary tail file: %s illegal type index illegal variable index internal error -- this option not implemented internal stat error on %s make .bss section make .nlmsections section make section missing required ATN65 no children no entry %s in archive
 no entry %s in archive %s! no input file specified no operation specified no type information for C++ method function set .bss vma set .data size set .nlmsection contents set .nlmsections size set section alignment set section flags set section size set start address stack overflow stack underflow subprocess got fatal signal %d support not compiled in for %s supported flags: %s two different operation options specified undefined variable in TY unexpected number unexpected record type unknown BB type unknown C++ visibility unknown TY code unknown demangling style `%s' unrecognized C++ object spec unrecognized section flag `%s' unsupported C++ object type wait: %s warning: CHECK procedure %s not defined warning: EXIT procedure %s not defined warning: START procedure %s not defined warning: symbol %s imported but not in import list Project-Id-Version: binutils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2006-05-27 18:59+0000
Last-Translator: José Pedro Paulino Afonso <Pandziura@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:01+0000
X-Generator: Launchpad (build 18115)
 

Símbolos de %s:

 

Símbolos de %s não definidos:

 
 A Tabela de Directorias está vazia.
 
 A Tabela de Nomes de Ficheiros está vazia.
 
Indíce do arquivo:
 
A secção %s está vazia.
         <máquina> possível: arm[_interwork], i386, mcore[-elf]{-le|-be}, ppc, thumb
        %s -M [<mri-script]
       --exclude-symbols <lista> Não exportar <lista>
       --export-all-symbols Export todos os símbolos para .def
       --no-default-excludes Limpa os símbolos excluídos por defeito
       --no-export-all-symbols apenas exporta símbolos listados
     Deslocamento Início Fim Expressão
    --add-indirect Adiciona indirects dll ao ficheiro de exportação.
    --base-file <ficheirobase> Lê o ficheiro base gerado pelo ligador (linker)
    --def <ficheirodef> Nome do ficheiro de entrada .def
    --dllname <nome> Nome do dll de entrada a colocar na livraria de saída.
    --dlltool-name <dlltool> Por defeito para "dlltool"
    --driver-flags <opções> Sobrescreve opções por defeito de ld
    --driver-name <controlador> por defeito para "gcc"
    --dry-run Mostra o que se necessita para executar
    --entry <entrada> Especifica ponto de entrada alternativo para a DLL
    --exclude-symbols <lista> Exclui <lista> de .def
    --export-all-symbols Exporta todos os símbolos para .def
    --image-base <base> Especifica o endereço base da imagem
    --implib <nomesaida> Sinónimo para --output-lib
    --machine <máquina>
    --mno-cygwin Cria DLL Mingw
    --no-default-excludes Elimina os símbolos excluídos por defeito
    --no-export-all-symbols Apenas exporta os símbolos .drectve
    --no-idata4 não gera a secção idata$4
    --no-idata5 Não gera a secção idata$5
    --output-def <ficheirodef> Nome do ficheiro de saída .def
    --output-exp <nomesaída> Gera um ficheiro de exportação.
    --output-lib <nomesaída> Gera livraria de entrada.
    --quiet, -q Trabalha silenciosamente
    --target <máquina> i386-cygwin32 ou i386-mingw32
    --verbose, -v Detalhado
    --version Mostra a versão de dllwrap
    -D --dllname <nome> Nome do dll de entrada a colocar na livraria de interface.
    -F --linker-flags <opções> passa <opções> para o linker (ligador).
    -L --linker <nome> Usa <nome> como o ligador (linker).
    -M --mcore-elf <namesaida> Processa os ficheiros object mcore-elf até
                    <nomesaida>
    -U Adiciona sublinhados (underscores) a .lib
    -V --version Mostra a versão do programa.
    -a --add-indirect Adiciona dll indirectos ao ficheiro de exportação.
    -b --base-file <ficheirobase> Lê ficheiro base gerado pelo ligador (linker).
    -d --input-def <ficheirodef> Nome do ficheiro .deb para lêr.
    -e --output-exp <outname> Gera um ficheiro de exportação.
    -h --help Mostra esta informação.
    -k Kill @<n> dos nomes exportados
    -l --output-lib <nomesaída> gera uma livraria de interface.
    -m --machine <máquina> Criar com DLL para <máquina>. [Por defeito: %s]
    -v --verbose Modo detalhado.
    -x --no-idata4 Não gerar secção idata$4.
    -z --output-def <ficheirodef> Nome do ficheiro .deb para ser criado.
    @<ficheiro> Mostra opções desde <ficheiro>
    @<ficheiro> Lê opções a partir do <ficheiro>
   @<ficheiro> - Lê opções a partir do <ficheiro>
   versão DWARF: %d
   Entrada	Dir	Hora	Tam	Nome
   Código de operação extendido %d:    Opções Genéricas:
   Valor inicial de 'is_stmt': %d
   Comprimento: %ld
   Comprimento: %ld
   Comprimento: %ld
   Linha Base: %d
   Comprimento mínimo de Instrução: %d
   Não há opções específicas da emulação
   Opções para %s:
   Passadas opções a DLLTOOL:
   Comprimento do Prólogo: %d
   Estabelece ISA para %lu
   Estabelecer bloco básico
   Estabelece epilogue_begin para verdadeiro
   Estabelece prologue_end para verdadeiro
   Tamanho da área na secção .debug_info: %ld
   Código de operação %d com operandos:    Versão: %d
   [-X32] - ignora os objectos de 64 bit
   [-X32_64] - aceita objectos de 32 e 64 bits
   [-X64] - ignora os objectos de 32 bit
   [-g] - arquivo pequeno de 32 bit
   [N] - usa a instância [conta] do nome
   [P] - usa nomes completos de caminho (path) quando busca coincidências
   [S] - não constrói uma tabela de simbolos
   [V] - mostra o número da versão
   [a] - coloca ficheiro(s) depois de [nome-membro]
   [b] - coloca ficheiro(s) antes [nome-membro] (mesmo que [i])
   [c] - não avisa se tem de ser criada a livraria
   [f] - trunca nomes de ficheiros inseridos
   [o] - preserva as datas originais
   [s] - cria um indíce do arquivo (cf. ranlib)
   [u] - apenas substitui ficheiros que sejam mais recentes que o conteúdo
                          actual do arquivo
   [v] - detalhado
   d - apaga ficheiro(s) do arquivo
   m[ab] - move ficheiro(s) no arquivo
   p - mostra ficheiro(s) encontrados no arquivo
   q[f] - agrega rapidamente ficheiro(s) ao arquivo
   r[ab][f][u] - substitui ficheiros existentes ou inserta nosvos ficheiros ao arquivo
   t - mostra os conteúdos do arquivo
   x[o] - extrai ficheiros do arquivo
  (início == fim)  (início > fim)  Converte endereços em pares de números de linhas/ nome ficheiro.
  Converte um ficheiro objecto num NetWare Loadable Module
  Gera um índice para acelerar o acesso aos arquivos
  Se não se especificam endereços na linha de comandos, serão lidos desde a entrada padrão (stdin)
  Remove símbolos e secções dos ficheiros
  As opções são:
  -I --input-target=<nomebfd> Estabelece o formato do ficheiro binário de entrada
  -O --output-target=<nomebfd> Estabelece o formato do ficheiro binário de saída
  -T --header-file=<ficheiro> Lê <ficheiro> para a informação do cabeçalho NLM
  -l --linker=<linker> Usa <linker> para qualquer ligação
  -d --debug Mostra no stderr o linker da linha de comandos
  @<ficheiro> Lê as opções desde o <ficheiro>.
  -h --help Mostra esta informação
  -v --version Mostra a versão do programa
  As opções são:
  @<ficheiro> Lê as opções desde <ficheiro>
  -h --help Mostra esta informação
  -v --version Mostra a versão do programa

  [sem DW_AT_frame_base]  modificadores específicos do comando:
  comandos:
  opções de emulação: 
  modificadores genéricos:
 #linhas %d  %s saíu com o estado %d %s não é um arquivo válido %s: Não se pode abrir o arquivo de entrada %s
 %s: Não se pode abrir o arquivo de saída %s
 %s: Erro:  %s: Formatos coincidentes: %s: Redefinição múltipla do símbolo "%s" %s: Componentes do caminho (path) eliminados do nome da imagem, '%s'. %s: Símbolo "%s" é alvo de mais do que uma redefinição %s: Aviso:  %s: número errado: %s %s: não se pode encontrar o ficheiro de módulo %s
 %s: Não se pode abrir o ficheiro %s
 %s: o ficheiro %s não é um arquivo
 %s: fread falhou %s: formato de saída inválido %s: radical inválido %s: não existe mapa de arquivo para actualizar %s: não há um arquivo aberto
 %s: não existe um arquivo de saída aberto
 %s: ainda não foi especificado um arquivo de saída
 %s: não há símbolos %s: arquitecturas suportadas: %s: alvos suportados: %s:%d: A ignorar lixo encontrado nesta linha '%s': não existe tal ficheiro (operador de localização desconhecido) (Operador de localização definido pelo utilizador) (declarado como inline e está inline) (declarado como inline mas ignorado) (inlined) (não inlined) <específico do SO>: %d <não há secção .debug_str> <deslocamento demasiado grande> <específico do processador>: %d <desconhecido>: %d Exportações adicionadas ao ficheiro de saída A adicionar exportações ao ficheiro de saída ficheiro de versão do cabeçalho BFD %s
 classe base C++ não definida classe base C++ não encontrada no contentor dados membros C++ não encontrados no contentor O objecto C++ não tem campos Não se pode ter NOME e LIVRARIA Não se pode abrir o ficheiro def: %s Não se pode abrir o ficheiro %s
 Não é possível produzir mcore-elf dll do ficheiro arquivo: %s Conteúdos da secção %s:

 Ficheiro lib criado A criar ficheiro livraria: %s A criar ficheiro stub: %s Arquivo actualmente aberto é %s
 A apagar ficheiro base temporário %s A apagar ficheiro def temporário %s A apagar ficheiro exp temporário %s Ainda não é suportado mostrar os conteúdos de depuração da secção %s.
 %s Lido Fim de sequência

 Excluindo o símbolo: %s Ficheiro de exportação gerado A gerar ficheiro de exportação: %s Erro Interno: Tipo de máquina desconhecida: %d A manter ficheiro base temporário %s A manter ficheiro def temporário %s A manter ficheiro exp temporário %s LIVRARIA: %s base: %x A lista de localização que começa no deslocamento 0x%lx não está terminada.
 A máquina '%s' não é suportada Múltiplas mudanças de nome na secção %s NOME: %s base: %x Nome Valor Classe Tipo Tamanho Linha Secção

 Nome Vaor Classe Tipo Tamanho Linha Secção

 Não há entrada %s no arquivo.
 Não há listas de localização na secção .debug_info!
 Não existe um membro com o nome `%s'
 O deslocamento 0x%lx é maior que o tamanho da secção .debug_loc.
 Apenas os intervalos-a de DWARF 2 e 3 actualmente são suportados.
 Apenas são actualmente suportados os nomes públicos DWARF 2 e 3
 Aberto o ficheiro temporário: %s Os componentes do caminho (path) foram eliminados do nome de dll, '%s'. Ficheiro def processado Definições processadas A processar o ficheiro def: %s A processar definições Reportar erros a %s
 A explorar o ficheiro objecto %s Chupando informação da secção %s em %s Arquitecturas suportadas: Alvos suportados: Erro de sintaxe no ficheiro def %s:%d A informação da linha parece estar corrompida - a secção é muito pequena
 Existe um buraco [0x%lx - 0x%lx] na secção .debug_loc.
 Existe uma sobreposição [0x%lx - 0x%lx] na secção %s.
 Existe uma sobreposição [0x%lx - 0x%lx] na secção .debug_loc.
 Tentado o ficheiro: %s Incapaz de abrir o ficheiro-base: %s Incapaz de abrir ficheiro temporário de assemblador: %s Comprimento de dados não manejável: %d
 Valor FORM desconhecido: %lx Valor TAG desconhecido: %lx Forma não reconhecida: %lu
 Utilização %s <opção(ões)> <ficheiro(s)-objecto>
 Utilização: %s <opção(s)> ficheiro(s)-entrada
 Utilização: %s [opção(ões)] [endereço(s)]
 Utilização: %s [opções] [ficheiro-entrada [ficheiro-saída]]
 Utilização %s [opção(ões)] ficheiro-entrada
 Utilização: %s [opções] arquivo
 A usar o ficheiro: %s O valor para `N' deve ser positivo. Aviso, a ignorar as EXPORT(ações) duplicadas %s %d,%d Aviso: '%s' não é um ficheiro comum Aviso: a alterar o tamanho de tipo de %d a %d
 Aviso: Não foi possível localizar '%s'. razão %s `N' apenas tem significado com as opções `x' e `d'. `u' apenas tem significado com a opção `r'. registo ATN65 errado posição ou tamanho errado no campo de bit C++ tipo errado para a função de método C++ Encontrado um operador de linha extendido mal formado!
 deixados blocos no final da pilha não se pode estabelecer o alvo BFD por defeito para `%s': %s Não é possível apagar %s: %s não é possível abrir '%s': %s a criar %s debug_end_common_block: não está implementado debug_find_named_type: não há unidade de compilação actual debug_get_real_type: informação de depuração circular para %s
 debug_make_undefined_type: género não suportado debug_name_type: não há ficheiro actual debug_record_label: não está implementado debug_record_variable: não há um ficheiro actual debug_start_common_block: não está implementado debug_tag_type: tentado um marcador extra debug_tag_type: não existe ficheiro actual debug_write_type: encontrado um tipo ilegal erro: o ficheiro de entrada '%s' está vazio Falhou a abertura do início do ficheiro temporário: %s Falhou a abertura do final do ficheiro temporário: %s indíce de tipo ilegal indíce variável ilegal erro interno -- esta opção não está implementada erro interno de stat em %s fazer secção .bss fazer secção .nlmsections secção make falta ATN65 requerido sem filhos não existe a entrada %s no arquivo
 Não existe uma entrada %s no arquivo %s! não foi especificado um ficheiro de entrada não foi especificada nenhuma operação não existe informação de tipo para a função de método C++ estabelecer vma .bss estabelecer tamanho .data estabelece os conteúdos de .nlmsection estabelece o tamanho de .nlmsections estabelece alinhamento da secção estabelece atributos da secção estabelece tamanho da secção estabelecer endereço de início transbordo da pilha transbordo por baixo da pilha o sub-processo recebeu um sinal fatal %d suporte não compilado para %s atributos suportados: %s foram especificadas duas opções de operação diferentes variável indefinida em TY número inesperado tipo de registo inesperado tipo BB desconhecido visibilidade C++ desconhecida código TY ilegal estilo de reconstrução desconhecido '%s' especificação de objecto C++ não reconhecida atributo de secção `%s' não reconhecido tipo de objecto C++ não suportado espera: %s aviso: o procedimento VERIFICAR %s não está definido aviso: procedimento SAIDA %s não está definido aviso: procedimento INICIO %s não definido aviso: foi importado o símbolo %s mas não na lista de importação 