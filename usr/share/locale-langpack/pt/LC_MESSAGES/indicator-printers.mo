��          �      �       H  (   I  '   r     �     �     �  K   �  +     %   3  %   Y  %     %   �  b   �     .  �  ;  *     *   @     k     t     �  I   �  "   �  ,     &   1  &   X  )     X   �                               	                     
                  A cover is open on the printer “%s”. A door is open on the printer “%s”. Paused Printers Printing Problem The printer “%s” can’t be used, because required software is missing. The printer “%s” is currently off-line. The printer “%s” is low on paper. The printer “%s” is low on toner. The printer “%s” is out of paper. The printer “%s” is out of toner. You have %d job queued to print on this printer. You have %d jobs queued to print on this printer. _Settings… Project-Id-Version: indicator-printers
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-04 13:07+0000
PO-Revision-Date: 2016-04-12 09:41+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 19:14+0000
X-Generator: Launchpad (build 18115)
 Está aberta uma tampa na impressora "%s". Está aberta uma porta na impressora "%s". Em pausa Impressoras Problema de impressão A impressora "%s" não pode ser usada, pois falta o software necessário. A impressora "%s" está desligada. A impressora “%s” está com pouco papel. A impressora “%s” tem pouco toner. A impressora “%s” está sem papel. A impressora “%s” já não tem toner. Tem %d trabalho em espera nesta impressora. Tem %d trabalhos em espera nesta impressora. _Definições... 