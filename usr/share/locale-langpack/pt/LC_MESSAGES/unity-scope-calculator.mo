��          \      �       �   	   �   
   �      �      �   3   �   �   ,  !   �  �       �     �     �  	   �  0     �   9  '                                          Calculate Calculator Open in calculator Result Sorry, there are no results that match your search. This is an Ubuntu search plugin that enables Calculator results to be displayed in the Dash underneath the Info header. If you do not wish to search this content source, you can disable this search plugin. calculator;result;calculate;calc; Project-Id-Version: unity-scope-calculator
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-28 09:22+0000
PO-Revision-Date: 2014-04-10 15:58+0000
Last-Translator: IvoGuerreiro <ivoguerreiro@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:07+0000
X-Generator: Launchpad (build 18115)
 Calcular Calculadora Abrir na calculadora Resultado Sem resultados que coincidam com a sua pesquisa. Este é um plugin de busca Ubuntu que permite que os resultados da calculadora possam ser exibido no Dash em baixo do info do cabeçalho. Se não desejar pesquisar essa fonte de conteúdo, pode desativar este plugin de busca. calculadora, resultado; calcular; calc; 