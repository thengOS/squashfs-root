��    t      �  �   \      �	  ]   �	     /
  �   6
              8  #   X  .   |  2   �  )   �  %        .  #   D  �   h  '   C  )   k  8   �     �     �     �  =     ,   Y  )   �     �  !   �     �     �       =   5  �   s     (  "   7  '   Z  ?   �  G   �  G   
  ?   R  P   �  H   �      ,  3   M  -   �  "   �     �  1   �  6   $     [  *   n     �     �     �     �     �  '     &   9  &   `  7   �     �  $   �     �  1     B   >     �     �      �     �  B   �  :   ?  A   z     �     �  /   �     ,      J  '   k  @   �  .   �  <     8   @     y  '   �  ?   �        +     -   K  -   y     �  �  �     �  (   �  J   �  ?   J  7   �  =   �  Y      )   Z  6   �  %   �  %   �  &     ,   .  �  [     V#  �  k#  �   5'     �'     �'  2   �'  -   +(     Y(     u(     �(  0   �(  !   �(  (   )  �  .)  �   +     �+  �   �+  (  �,  &   �-  '   �-  +   �-  ;   +.  ?   g.  8   �.  6   �.  "   /  0   :/  �   k/  7   M0  5   �0  U   �0  ,   1     >1     R1  =   r1  ?   �1  5   �1  -   &2  "   T2     w2  )   �2  .   �2  C   �2  �   !3     �3  %   �3  3   4  P   84  `   �4  W   �4  G   B5  _   �5  O   �5  0   :6  ;   k6  3   �6  &   �6  &   7  Q   )7  I   {7     �7  )   �7     8  !   8     ;8     T8      p8  ,   �8  .   �8  .   �8  M   9     j9  '   �9     �9  0   �9  C   �9     0:      G:  "   h:      �:  C   �:  >   �:  ?   /;     o;     �;  6   �;     �;     �;  1   <  ;   L<  4   �<  ?   �<  B   �<     @=  )   _=  J   �=     �=  ,   �=  3   !>  @   U>  #   �>    �>     �@  /   �@  Q   A  =   nA  7   �A  E   �A  Z   *B  +   �B  7   �B  /   �B  %   C  0   ?C  .   pC  �  �C     ?H  H  TH  �   �L     0M     EM  >   bM  1   �M  #   �M  #   �M  '   N  3   CN     wN  +   �N     ;          `       W   <   :       ^   g                  .       *      (      X       6      j       G           K   7      9   S      R   P   b   %   H       T   #   0       l          5       F   @       O       ,   +   t       U      J   N           p   )   ?   &   C   Q   A              	                  Y      _      2       B      '           4              \       D      i   3   V   E      8       >          
       e   1   h   ]   Z   s   -   =   c   I       /       "       m   d   o          !       $      L   q       a           M               f   k         r   n      [       %s failed with return code 15, shadow not enabled, password aging cannot be set. Continuing.
 %s: %s %s: Please enter a username matching the regular expression configured
via the NAME_REGEX[_SYSTEM] configuration variable.  Use the `--force-badname'
option to relax this check or reconfigure NAME_REGEX.
 %s: To avoid problems, the username should consist only of
letters, digits, underscores, periods, at signs and dashes, and not start with
a dash (as defined by IEEE Std 1003.1-2001). For compatibility with Samba
machine accounts $ is also supported at the end of the username
 Adding group `%s' (GID %d) ...
 Adding new group `%s' (%d) ...
 Adding new group `%s' (GID %d) ...
 Adding new user `%s' (%d) with group `%s' ...
 Adding new user `%s' (UID %d) with group `%s' ...
 Adding new user `%s' to extra groups ...
 Adding system user `%s' (UID %d) ...
 Adding user `%s' ...
 Adding user `%s' to group `%s' ...
 Adds a user or group to the system.
  
Copyright (C) 1997, 1998, 1999 Guy Maor <maor@debian.org>
Copyright (C) 1995 Ian Murdock <imurdock@gnu.ai.mit.edu>,
                   Ted Hajek <tedhajek@boombox.micro.umn.edu>

 Allowing use of questionable username.
 Backing up files to be removed to %s ...
 Cannot deal with %s.
It is not a dir, file, or symlink.
 Cannot handle special file %s
 Caught a SIG%s.
 Copying files from `%s' ...
 Copyright (C) 2000 Roland Bauerschmidt <roland@copyleft.de>

 Could not find program named `%s' in $PATH.
 Couldn't create home directory `%s': %s.
 Couldn't parse `%s', line %d.
 Creating home directory `%s' ...
 Done.
 Enter a group name to remove:  Enter a user name to remove:  If you really want this, call deluser with parameter --force
 In order to use the --remove-home, --remove-all-files, and --backup features,
you need to install the `perl-modules' package. To accomplish that, run
apt-get install perl-modules.
 Internal error Is the information correct? [Y/n]  Looking for files to backup/remove ...
 No GID is available in the range %d-%d (FIRST_GID - LAST_GID).
 No GID is available in the range %d-%d (FIRST_SYS_GID - LAST_SYS_GID).
 No UID is available in the range %d-%d (FIRST_SYS_UID - LAST_SYS_UID).
 No UID is available in the range %d-%d (FIRST_UID - LAST_UID).
 No UID/GID pair is available in the range %d-%d (FIRST_SYS_UID - LAST_SYS_UID).
 No UID/GID pair is available in the range %d-%d (FIRST_UID - LAST_UID).
 No options allowed after names.
 Not backing up/removing `%s', it is a mount point.
 Not backing up/removing `%s', it matches %s.
 Not creating home directory `%s'.
 Only one or two names allowed.
 Only root may add a user or group to the system.
 Only root may remove a user or group from the system.
 Permission denied
 Removes users and groups from the system.
 Removing crontab ...
 Removing directory `%s' ...
 Removing files ...
 Removing group `%s' ...
 Removing user `%s' ...
 Removing user `%s' from group `%s' ...
 Selecting GID from range %d to %d ...
 Selecting UID from range %d to %d ...
 Setting quota for user `%s' to values of user `%s' ...
 Setting up encryption ...
 Specify only one name in this mode.
 Stopped: %s
 Stopping now without having performed any action
 The --group, --ingroup, and --gid options are mutually exclusive.
 The GID %d does not exist.
 The GID %d is already in use.
 The GID `%s' is already in use.
 The UID %d is already in use.
 The group `%s' already exists and is not a system group. Exiting.
 The group `%s' already exists as a system group. Exiting.
 The group `%s' already exists, but has a different GID. Exiting.
 The group `%s' already exists.
 The group `%s' does not exist.
 The group `%s' is not a system group. Exiting.
 The group `%s' is not empty!
 The group `%s' was not created.
 The home dir must be an absolute path.
 The home directory `%s' already exists.  Not copying from `%s'.
 The system user `%s' already exists. Exiting.
 The user `%s' already exists with a different UID. Exiting.
 The user `%s' already exists, and is not a system user.
 The user `%s' already exists.
 The user `%s' already exists. Exiting.
 The user `%s' does not exist, but --system was given. Exiting.
 The user `%s' does not exist.
 The user `%s' is already a member of `%s'.
 The user `%s' is not a member of group `%s'.
 The user `%s' is not a system user. Exiting.
 The user `%s' was not created.
 This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License, /usr/share/common-licenses/GPL, for more details.
 Try again? [y/N]  Unknown variable `%s' at `%s', line %d.
 Usually this is never required as it may render the whole system unusable
 WARNING: You are just about to delete the root account (uid 0)
 Warning: The home dir %s you specified already exists.
 Warning: The home dir %s you specified can't be accessed: %s
 Warning: The home directory `%s' does not belong to the user you are currently creating.
 Warning: group `%s' has no more members.
 You may not remove the user from their primary group.
 `%s' does not exist. Using defaults.
 `%s' exited from signal %d. Exiting.
 `%s' returned error code %d. Exiting.
 `%s' still has `%s' as their primary group!
 adduser [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]
[--firstuid ID] [--lastuid ID] [--gecos GECOS] [--ingroup GROUP | --gid ID]
[--disabled-password] [--disabled-login] [--encrypt-home] USER
   Add a normal user

adduser --system [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]
[--gecos GECOS] [--group | --ingroup GROUP | --gid ID] [--disabled-password]
[--disabled-login] USER
   Add a system user

adduser --group [--gid ID] GROUP
addgroup [--gid ID] GROUP
   Add a user group

addgroup --system [--gid ID] GROUP
   Add a system group

adduser USER GROUP
   Add an existing user to an existing group

general options:
  --quiet | -q      don't give process information to stdout
  --force-badname   allow usernames which do not match the
                    NAME_REGEX[_SYSTEM] configuration variable
  --extrausers      uses extra users as the database
  --help | -h       usage message
  --version | -v    version number and copyright
  --conf | -c FILE  use FILE as configuration file

 adduser version %s

 deluser USER
  remove a normal user from the system
  example: deluser mike

  --remove-home             remove the users home directory and mail spool
  --remove-all-files        remove all files owned by user
  --backup                  backup files before removing.
  --backup-to <DIR>         target directory for the backups.
                            Default is the current directory.
  --system                  only remove if system user

delgroup GROUP
deluser --group GROUP
  remove a group from the system
  example: deluser --group students

  --system                  only remove if system group
  --only-if-empty           only remove if no members left

deluser USER GROUP
  remove the user from a group
  example: deluser mike students

general options:
  --quiet | -q      don't give process information to stdout
  --help | -h       usage message
  --version | -v    version number and copyright
  --conf | -c FILE  use FILE as configuration file

 deluser is based on adduser by Guy Maor <maor@debian.org>, Ian Murdock
<imurdock@gnu.ai.mit.edu> and Ted Hajek <tedhajek@boombox.micro.umn.edu>

 deluser version %s

 fork for `find' failed: %s
 fork for `mount' to parse mount points failed: %s
 getgrnam `%s' failed. This shouldn't happen.
 invalid argument to option
 invalid combination of options
 passwd file busy, try again
 pipe of command `mount' could not be closed: %s
 unexpected failure, nothing done
 unexpected failure, passwd file missing
 Project-Id-Version: adduser 3.112+nmu2
Report-Msgid-Bugs-To: adduser-devel@lists.alioth.debian.org
POT-Creation-Date: 2015-07-02 22:08+0200
PO-Revision-Date: 2015-10-15 10:58+0000
Last-Translator: Alexandre Fidalgo <alexandremagnos15@gmail.com>
Language-Team: Portuguese <traduz@debianpt.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:56+0000
X-Generator: Launchpad (build 18115)
Language: pt
 %s falhou com código de retorno 15, shadow não activado, envelhecimento da palavra-passe não pode ser definido. A continuar.
 %s: %s %s: Por favor insira um nome de utilizador correspondente à expressão regular configurada
via NAME_REGEX[_SYSTEM] variável de configuração. Use o `- force-badname '
opção para melhorar esta verificação ou reconfigurar NAME_REGEX.
 %s: Para evitar problemas, o nome de utilizador deve consistir apenas de
letras, dígitos, underscores, pontos, arrobas e traços, e não começar por
um traço (como definido pelo IEEE Std 1003.1-2001). Para compatibilidade
com contas Samba o $ também é suportado no fim do nome de utilizador
 A adicionar o grupo `%s' (GID %d) ...
 A adicionar o novo grupo `%s' (%d) ...
 A adicionar o novo grupo `%s' (GID %d) ...
 A adicionar o novo utilizador `%s' (%d) com grupo `%s' ...
 A adicionar o novo utilizador `%s' (UID %d) com grupo `%s' ...
 A adicionar o novo utilizador `%s' aos grupos extra ...
 A adicionar o utilizador de sistema `%s' (UID %d) ...
 A adicionar o utilizador `%s' ...
 A adicionar o utilizador `%s' ao grupo `%s' ...
 Adiciona um utilizador ou grupo ao sistema.
 
Copyright (C) 1997, 1998, 1999 Guy Maor <maor@debian.org>
Copyright (C) 1995 Ian Murdock <imurdock@gnu.ai.mit.edu>,
                   Ted Hajek <tedhajek@boombox.micro.umn.edu>

 A permitir o uso dum nome de utilizador questionável.
 A salvaguardar ficheiros a serem removidos em %s ...
 Não é possível lidar com %s.
Não é um directório, ficheiro ou link simbólico.
 Incapaz de lidar com o ficheiro especial %s
 Apanhado um SIG%s.
 A copiar ficheiros de `%s' ...
 Copyright (C) 2000 Roland Bauerschmidt <roland@copyleft.de>

 Não foi possível encontrar o programa chamado `%s' na $PATH.
 Não se conseguiu criar o directório home `%s': %s.
 Não foi possível processar `%s', linha %d.
 A criar directório home `%s' ...
 Concluído.
 Introduza um nome de grupo para remover:  Introduza um nome de utilizador para remover:  Se realmente deseja isto, chame o deluser com o parâmetro --force
 Para usar as funcionalidades --remove-home, --remove-all-files e --backup,
precisa de instalar o pacote `perl-modules'. Para fazer isso, execute
apt-get install perl-modules.
 Erro interno Esta informação é correcta? [Y/n]  A procurar ficheiros para salvaguardar/remover ...
 Não há nenhum GID disponível no intervalo %d-%d (PRIMEIRO_GID - ULTIMO_GID).
 Não há nenhum GID disponível no intervalo %d-%d (PRIMEIRO_GID_SISTEMA - ULTIMO_GID_SISTEMA).
 Nenhum UID disponível no intervalo %d-%d (PRIMEIRO_UID_SISTEMA - ULTIMO_UID_SISTEMA).
 Nenhum UID disponível no intervalo %d-%d (PRIMEIRO_UID - ULTIMO_UID).
 Nenhum par UID/GID disponível no intervalo %d-%d (PRIMEIRO_UID_SISTEMA - ULTIMO_UID_SISTEMA).
 Nenhum par UID/GID disponível no intervalo %d-%d (PRIMEIRO_UID - ULTIMO_UID).
 Não são permitidas opções depois dos nomes.
 A não salvaguardar/remover `%s', é um ponto de montagem.
 A não salvaguardar/remover `%s', coincide com %s.
 A não criar o directório home `%s'.
 Só são permitidos um ou dois nomes.
 Apenas o utilizador root poderá adicionar um grupo ou um utilizador ao sistema.
 Apenas o utilizador root pode remover um utilizador ou grupo do sistema.
 Permissão negada
 Remove utilizadores e grupos do sistema.
 A remover crontab ...
 A remover o directório `%s' ...
 A remover ficheiros ...
 A remover o grupo `%s' ...
 A remover o utilizador `%s' ...
 A remover utilizador `%s' do grupo `%s' ...
 A seleccionar um GID no intervalo %d a %d ...
 A seleccionar um UID no intervalo %d a %d ...
 A definir quota para o utilizador `%s' com os valores do utilizador `%s' ...
 A preparar encriptação...
 Especifique apenas um nome neste modo.
 Parado: %s
 A parar agora sem ter executado nenhuma acção
 As opções --group, --ingroup e --gid são mutuamente exclusivas.
 O GID %d não existe.
 O GID %d já está a ser usado.
 O GID `%s' já está a ser usado.
 O UID %d já está a ser usado.
 O grupo `%s' já existe e não é um grupo de sistema. A terminar.
 O grupo `%s' já existe como um grupo de sistema. A terminar.
 O grupo `%s' já existe, mas tem um GID diferente. A terminar.
 O grupo `%s' já existe.
 O grupo `%s' não existe.
 O grupo `%s' não é um grupo de sistema. A terminar.
 O grupo `%s' não está vazio!
 O grupo `%s' não foi criado.
 A diretoria home tem de ser um caminho absoluto.
 O directório home `%s' já existe. A não copiar de `%s'.
 O utilizador de sistema `%s' já existe. A terminar
 O utilizador `%s' já existe com um UID diferente. A terminar.
 O utilizador `%s' já existe, e não é um utilizador de sistema.
 O utilizador `%s' já existe.
 O utilizador `%s' já existe. A terminar
 O utilizador `%s' não existe, mas --system foi especificado. A terminar.
 O utilizador `%s' não existe.
 O utilizador `%s' já é um membro de `%s'.
 O utilizador `%s' não é um membro do grupo `%s'.
 O utilizador `%s' não é um utilizador de sistema. A terminar.
 O utilizador `%s' não foi criado.
 Este programa é software livre; você pode redistribuí-lo e/ou modificá-lo
sob os termos da GNU General Public License conforme publicada pela Free
Software Foundation; quer a versão 2 da licença, ou (conforme você escolha)
qualquer versão posterior.

Este programa é distribuído com a esperança de que seja útil, mas SEM
QUALQUER GARANTIA; mesmo sem a garantia implícita de COMERCIALIZAÇÃO OU
ADEQUAÇÃO A UM DETERMINADO PROPÓSITO. Para mais detalhes, veja a
GNU General Public License em /usr/share/common-licenses/GPL.
 Tentar de novo? [y/N]  Variável desconhecida `%s' em `%s', linha %d.
 Normalmente isto nunca é necessário e pode tornar todo o sistema inutilizável
 AVISO: Você está  prestes a apagar a conta de root (uid 0)
 Aviso: já existe a diretoria home %s que especificou.
 Aviso: a diretoria home %s que especificou não pode ser acedida: %s
 Aviso: O directório home '%s' não pertence ao utilizador que está actualmente a criar.
 Aviso: O grupo `%s' não tem mais membros.
 Não pode remover o utilizador do seu grupo principal.
 `%s' não existe. Usando valores predefinidos.
 `%s' saiu pelo sinal %d. A terminar.
 `%s' devolveu o código de erro %d. A terminar.
 `%s' ainda tem `%s' como seu grupo principal!
 adduser [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]
[--firstuid ID] [--lastuid ID] [--gecos GECOS] [--ingroup GRUPO | --gid ID]
[--disabled-password] [--disabled-login] [--encrypt-home] Utilizador
   Adicionar um utilizador normal

adduser --system [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]
[--gecos GECOS] [--group | --ingroup GRUPO | --gid ID] [--disabled-password]
[--disabled-login] UTILIZADOR
   Adicionar um utilizador de sistema

adduser --group [--gid ID] GRUPO
addgroup [--gid ID] GRUPO
   Adicionar um grupo de utilizador

addgroup --system [--gid ID] GRUPO
   Adicionar um grupo de sistema

adduser UTILIZADOR GRUPO
   Adicionar um utilizador existente a um grupo existente

opções gerais:
  --quiet | -q      não disponibiliza processo de informação para stdout
  --force-badname   permite nomes de utilizador os quais não combinam com a variável de configuração NAME_REGEX[_SYSTEM]
  --extrausers     utiliza utilizadores adicionais como a base de dados
  --help | -h       mensagem de utilização
  --version | -v    número da versão e direitos de autor
  --conf | -c FICHEIRO  usar FICHEIRO como ficheiro de configuração

 adduser versão %s

 deluser UTILIZADOR
  remove um utilizador normal do sistema
  exemplo: deluser miguel

  --remove-home             remove o directório home e spool de mail do utilizador
  --remove-all-files        remove todos os ficheiros do utilizador
  --backup                  salvaguarda ficheiros antes de remover.
  --backup-to <DIR>         directório onde salvaguardar.
                            Por predefinição é o directório actual.
  --system                  remove apenas se for um utilizador de sistema

delgroup GRUPO
deluser --group GRUPO
  remove um grupo do sistema
  exemplo: deluser --group alunos

  --system                  remove apenas se for grupo de sistema
  --only-if-empty           remove apenas se não tiver mais membros

deluser UTILIZADOR GRUPO
  remove o utilizador de um grupo
  exemplo: deluser miguel alunos

opções gerais:
  --quiet | -q      não passa informação de processo para o stdout
  --help | -h       mensagem de utilização
  --version | -v    número de versão e copyright
  --conf | -c FICHEIRO  usa FICHEIRO como ficheiro de configuração

 deluser é baseado em adduser por Guy Maor <maor@debian.org>, Ian Murdock
<imurdock@gnu.ai.mit.edu> e Ted Hajek <tedhajek@boombox.micro.umn.edu>

 deluser versão %s

 fork para `find' falhou: %s
 fork para `mount' para analisar pontos de montagem falhou: %s
 getgrnam `%s' falhou. Isto não devia acontecer.
 argumento inválido para a opção
 combinação inválida de opções
 ficheiro passwd ocupado, tente de novo
 pipe do comando `mount' não pôde ser fechado: %s
 erro inesperado, nada feito
 falha inesperada, ficheiro passwd em falta
 