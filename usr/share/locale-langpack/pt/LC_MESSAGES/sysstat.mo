��    7      �  I   �      �     �  %   �  9   �     (  &   ?     f  %        �    �  -   �  )   �       2   <     o     �  )   �     �  %   �     �  $     "   ;     ^  .   r  -   �  "   �  '   �  -   	  6   H	     	     �	     �	  !   �	  '   �	     �	     

  8   %
     ^
  +   y
  g   �
  {     �   �  .   k  >   �     �  +   �  #   #  .   G     v  $   �  1   �  J   �  ?   0  >   p     �  �  �      �  .   �  =   �       1   ,  ,   ^  4   �  /   �  E  �  :   6  2   q  &   �  C   �  &     $   6  5   [     �  ,   �     �  1   �  &        =  E   W  G   �  +   �  2     >   D  A   �     �  
   �      �  0     2   6     i  #   �  H   �     �  9   	  l   C  }   �  �   .  3     C   F  !   �  2   �  ,   �  9        F  5   f  =   �  Q   �  V   ,  D   �     �        6      1          $   )      	          '   *      +   
                 2          &                    "   !          3   4   .   %                 7       /   0             (              #   5                                       ,       -              	-B	Paging statistics
 	-H	Hugepages utilization statistics
 	-I { <int> | SUM | ALL | XALL }
		Interrupts statistics
 	-R	Memory statistics
 	-S	Swap space utilization statistics
 	-W	Swapping statistics
 	-b	I/O and transfer rate statistics
 	-d	Block devices statistics
 	-m { <keyword> [,...] | ALL }
		Power management statistics
		Keywords are:
		CPU	CPU instantaneous clock frequency
		FAN	Fans speed
		FREQ	CPU average clock frequency
		IN	Voltage inputs
		TEMP	Devices temperature
		USB	USB devices plugged into the system
 	-q	Queue length and load average statistics
 	-u [ ALL ]
		CPU utilization statistics
 	-v	Kernel tables statistics
 	-w	Task creation and system switching statistics
 	-y	TTY devices statistics
 	[Unknown activity format] -f and -o options are mutually exclusive
 Average: Cannot append data to that file (%s)
 Cannot find disk data
 Cannot find the data collector (%s)
 Cannot handle so many processors!
 Cannot open %s: %s
 Cannot write data to system activity file: %s
 Cannot write system activity file header: %s
 End of data collecting unexpected
 End of system activity file unexpected
 Error while reading system activity file: %s
 File created by sar/sadc from sysstat version %d.%d.%d File time:  Host:  Inconsistent input data
 Invalid system activity file: %s
 Invalid type of persistent device name
 List of activities:
 Main options and reports:
 Not reading from a system activity file (use -f option)
 Not that many processors!
 Number of CPU for last samples in file: %u
 Options are:
[ -A ] [ -u ] [ -V ] [ -I { SUM | CPU | SCPU | ALL } ]
[ -P { <cpu> [,...] | ON | ALL } ]
 Options are:
[ -C <comment> ] [ -D ] [ -F ] [ -L ] [ -V ]
[ -S { INT | DISK | IPV6 | POWER | SNMP | XDISK | ALL | XALL } ]
 Options are:
[ -d ] [ -h ] [ -I ] [ -l ] [ -R ] [ -r ] [ -s ] [ -t ] [ -U [ <username> ] ]
[ -u ] [ -V ] [ -v ] [ -w ] [ -C <command> ] [ -G <process_name> ]
[ -p { <pid> [,...] | SELF | ALL } ] [ -T { TASK | CHILD | ALL } ]
 Options are:
[ -h ] [ -k | -m ] [ -t ] [ -V ]
 Options are:
[ -h ] [ -k | -m ] [ -t ] [ -V ] [ --debuginfo ]
 Other devices not listed here Please check if data collecting is enabled
 Requested activities not available
 Requested activities not available in file %s
 Size of a long int: %d
 System activity data file: %s (%#x)
 Usage: %s [ options ] [ <interval> [ <count> ] ]
 Usage: %s [ options ] [ <interval> [ <count> ] ] [ <datafile> | -[0-9]+ ]
 Usage: %s [ options ] [ <interval> [ <count> ] ] [ <outfile> ]
 Using a wrong data collector from a different sysstat version
 sysstat version %s
 Project-Id-Version: sysstat 1.1
Report-Msgid-Bugs-To: sysstat <at> orange.fr
POT-Creation-Date: 2015-12-21 08:54+0100
PO-Revision-Date: 2016-05-12 21:12+0000
Last-Translator: Frederic L. W. Meunier <Unknown>
Language-Team: Brazilian portuguese <lie-br@conectiva.com.br>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:36+0000
X-Generator: Launchpad (build 18115)
 	-B	Estatística de paginação
 	-H	Estatística de utilização de Hugepages
 	-I { <int> | SUM | ALL | XALL }
		Interrompe a estatística
 	-R	Estatística de Memória
 	-S	Estatística de utilização de espaço Swap
 	-W	Estatística de alternância (swapping)
 	-b	Estatística de taxa de I/O e de transferência
 	-d	Bloquear as estatísticas dos dispositivos
 	-m { <keyword> [,...] | ALL }
		Estatísticas de gestão de energia
		Palavras-chave são:
		CPU	relógio instantâneo de frequência do CPU 
		FAN	velocidade das ventoinhas
		FREQ	relógio da frequência média do CPU
		IN	Entrada de tensão
		TEMP	temperatura dos dispositivos
		USB	dispositivos de USB ligados ao sistema
 	-q	Estatística de comprimento de fila e média de carga
 	-u [ ALL ]
		Estatística de utilização de CPU
 	-v	Tabelas de estatística do Kernel
 	-w	Estatística de alternância de sistema e criação de tarefas
 	-y	Estatística dos dispositivos TTY
 	[Formato de atividade desconhecida] As opções -f e -o não podem ser utilizadas juntas
 Média: Não pode anexar dados a esse ficheiro (%s)
 Não encontra dados de disco
 Não consegue encontrar o colector de dados (%s)
 Não posso usar tantos processadores!
 Impossível abrir %s: %s
 Impossível escrever os dados no arquivo de atividade do sistema: %s
 Impossível escrever cabeçalho do arquivo de atividade do sistema: %s
 Fim não esperado de angariação de dados
 Fim do arquivo de atividade do sistema inesperado
 Erro durante a leitura do arquivo de atividade do sistema: %s
 Ficheiro criado por sar/sadc a partir da versão sysstat %d.%d.%d Tempo do Ficheiro:  Servidor:  Entrada de dados inconsistentes
 Ficheiro de actividade de sistema inválido: %s
 tipo inválido de nome de dispositivo persistente
 Lista de actividades:
 Principais opções e relatórios:
 Não lendo de um arquivo de atividade do sistema (utilize a opção -f)
 Não tantos processadores!
 Número de CPU para as últimas amostras no ficheiro: %u
 As opções são:
[ -A ] [ -u ] [ -V ] [ -I { SUM | CPU | SCPU | ALL } ]
[ -P { <cpu> [,...] | ON | ALL } ]
 Opções são:
[ -C <comment> ] [ -D ] [ -F ] [ -L ] [ -V ]
[ -S { INT | DISK | IPV6 | POWER | SNMP | XDISK | ALL | XALL } ]
 Opções são:
[ -d ] [ -h ] [ -I ] [ -l ] [ -R ] [ -r ] [ -s ] [ -t ] [ -U [ <username> ] ]
[ -u ] [ -V ] [ -v ] [ -w ] [ -C <command> ] [ -G <process_name> ]
[ -p { <pid> [,...] | SELF | ALL } ] [ -T { TASK | CHILD | ALL } ]
 As opções são:
[ -h ] [ -k | -m ] [ -t ] [ -V ]
 As opções são:
[ -h ] [ -k | -m ] [ -t ] [ -V ] [ --debuginfo ]
 Outros dispositivos listados aqui Verifique se a colheita de dados está habilitada
 Atividades pedidas não estão disponíveis
 Actividades necessárias não disponíveis no arquivo %s
 Tamanho de um int comprido: %d
 Ficheiro de dados de actividade do sistema: %s (%#x)
 Utilização: %s [ opções ] [ <intervalo> [ <contagem> ] ]
 Utilização: %s [ options ] [ <interval> [ <count> ] ] [ <datafile> | -[0-9]+ ]
 Utilização: %s [ opções ] [ <intervalo> [ <contagem> ] ] [ <ficheiro de saída> ]
 A usar um colector de dados errado de uma versão sysstat diferente
 sysstat versão %s
 