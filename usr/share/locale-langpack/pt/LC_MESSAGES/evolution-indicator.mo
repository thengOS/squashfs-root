��          �      L      �     �     �     �     �     �       3        M     [     v  7   �  ,   �     �          '  #   ?  
   c  	   n  �  x  #   `     �     �  	   �     �     �  G   �       <   *     g  F   ~  ;   �       $        D  "   _     �     �                                                 	                                     
              %d New Message %d New Messages : Compose New Message Contacts Evolution Indicator Inbox Only create notifications for new mail in an Inbox. Pla_y a sound Play a sound for new mail. Show a notification bubble. Show new message count in the message indicator applet. Shows new mail count in a message indicator. When New Mail Arrives When new mail arri_ves in _Display a notification _Indicate new messages in the panel any Folder any Inbox Project-Id-Version: evolution-indicator
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-24 15:14+0000
PO-Revision-Date: 2014-07-26 14:22+0000
Last-Translator: Alexandre Fidalgo <alexandremagnos15@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 18:39+0000
X-Generator: Launchpad (build 18115)
 %d Mensagem Nova %d Mensagens Novas : Criar nova mensagem Contactos Indicador do Evolution Na Caixa de Entrada Criar notificações somente para mensagens novas numa Caixa de Entrada _Reproduzir um som Reproduzir som para novas mensagens de correio electrónico. Notificar visualmente. Mostrar o número de mensagens novas no applet indicador de mensagens. Mostrar número de novas mensagens no indicador de correio. Quando chegam novas mensagens Quando as novas mensagens cheg_am à _Mostrar uma notificação _Indicar novas mensagens no painel qualquer Pasta qualquer Caixa de Entrada 