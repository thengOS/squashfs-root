��          �      \      �  "   �     �  #        7     R  3   d  0   �  &   �  0   �  ,   !  $   N  (   s  /   �     �  9   �  9     2   H  )   {     �  �  �  .   T  #   �  #   �  #   �     �  7   
  0   B  +   s  ;   �  ;   �  ,     8   D  6   }     �  A   �  A   �  7   >	  -   v	     �	                                   	                                    
                            %s blocks must have an action part No previous regular expression Premature end of regular expression Regular expression too big Unmatched ) or \) `%s' is a built-in function, it cannot be redefined `delete(array)' is a non-portable tawk extension `return' used outside function context attempt to use scalar parameter `%s' as an array can't open source file `%s' for reading (%s) delete: index `%s' not in array `%s' duplicate case values in switch body: %s each rule must have a pattern or an action part from %s regexp constant `/%s/' looks like a C comment, but is not regexp constant `//' looks like a C++ comment, but is not regular expression on left of `~' or `!~' operator regular expression on right of comparison source file `%s' is empty Project-Id-Version: gawk
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-05-19 16:06+0300
PO-Revision-Date: 2010-12-31 17:51+0000
Last-Translator: Almufadado <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:25+0000
X-Generator: Launchpad (build 18115)
 os blocos de %s devem ter uma parte de acção Nenhuma expressão regular anterior Fim prematuro de expressão regular Expressão regular demasiado grande ) ou \) não correspondido `%s' é uma função built-in, não pode ser redefinida `delete(array)' não é extensão tawk portável `return' usado fora do contexto da função tentativa de usar o parâmetro escalar `%s'  como um vector Não consegui abrir o ficheiro fonte `%s' para leitura (%s) eliminar: posição `%s' não no vector `%s' valores de condições duplicados no corpo do switch: %s cada regra deve ter um padrão ou uma parte de acção de %s constante regexp `/%s/' parece um comentário de C, mas não o é constante regexp `//' parece um comentário de C++, mas não o é expressão regular à esquerda dos operador `~' ou `!~' expressão regular à direita da comparação ficheiro fonte `%s' está vazio 