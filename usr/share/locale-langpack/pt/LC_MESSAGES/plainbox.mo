��          �   %   �      `  *   a     �  )   �     �  0   �       (        =     P     l     �     �  	   �  *   �     �  4     %   ;  (   a  J   �     �  6   �  &        ?     T     f     n  �  v  1   -      _  &   �  
   �  :   �     �  1         2  '   Q     y     �  %   �     �  *   �     	  2   	  $   R	  +   w	  O   �	  
   �	  <   �	  0   ;
     l
  "   �
     �
     �
                                           
                                                  	                               Environment variables for scripts and jobs Estimated test duration: Executable %r invoked with %r has crashed LOGGER None of the providers had a whitelist named '{}' Problem list Unable to find any of the executables {} [Desired Job List] [Estimated Duration Report] [General Statistics] [Interactivity Report] action for exception %r is %s automatic be more verbose (same as --log-level=INFO) caught SystemExit, exiting crash on SIGINT/KeyboardInterrupt, useful with --pdb display DEBUG messages in the console enable DEBUG messages on the root logger enable DEBUG messages on the specified logger (can be used multiple times) interactive jump into pdb (python debugger) when a command crashes show program's version number and exit starting debugger... too few arguments unknown usage:  Project-Id-Version: plainbox
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-12 10:02+0000
PO-Revision-Date: 2014-10-12 23:02+0000
Last-Translator: Luís Louro <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 19:20+0000
X-Generator: Launchpad (build 18115)
Language: pt
 Variaveis de ambiente para documentos e trabalhos Estimativa de duração do teste Executável %r invocado com %r crashou UTILIZADOR Nenhum dos provedores possui uma lista branca chamada '{}' Lista de problemas Incapaz de encontrar qualquer dos executáveis {} [Lista de Trabalhos Desejados] [Relatório de Estimativa da Duração] [Estatísticas Gerais] [Relatório de Interatividade] a ação para a a excepção %r é %s automático seja mais completo (como --log-level=INFO) SystemExit, saindo crash no SIGINT/KeyboardInterrupt, útil com --pdb apresente mensagens DEBUG na consola autorize mensagens DEBUG no utilizador root ativar mensagens de DEBUG no logger especificado (pode ser usado várias vezes) interativo saltar para o pdb (python debugger) quando um comando crasha apresentar número da versão do programa e sair começando o debugger número insuficiente de argumentos desconhecido utilização:  