��    �      T  �   �      `     a     o       �  �     =     J     Q     W     \     i     |     �  2   �     �     �     �       
     
        *  %   D     j  %   �     �  #   �     �     �  (   
     3     I     U     a     x     �     �  	   �     �     �     �     �               !     0     <      L  (   m  :   �     �     �     �  #   �          4     F     X  ,   ]  %   �     �     �     �     �     �     �  =         K     l     q     t     }     �  /   �  3   �          #  
   3     >     B     K     T     [     l  4   y  &   �  +   �       $   	  %   .     T     m     �     �     �     �     �     �  
   �     �     
       
   :     E     Z  	   _     i     o  -   v  &   �  -   �     �  
               
   %     0     ?     K     P     i     z     ~  	   �     �     �     �     �  	   �     �     �     �  8   �  	   #     -     F  �  ^                5  �  L     �          
               !      5     V  2   b     �     �     �     �     �  	   �     �  5     %   O  -   u     �  1   �  #   �       0        M  
   c  
   n     y     �     �     �     �     �     �       	   !     +  
   >     I     W     j     �  +   �  7   �          
       -   +  !   Y     {     �     �  3   �  +   �                 $      <      M      `   L   s   (   �      �      �      �   *   �       '!  4   H!  A   }!  	   �!     �!     �!     �!     �!  	   �!     �!     "     "  D   &"  3   k"  (   �"     �"  -   �"  /   �"  %   .#     T#     s#     �#     �#  
   �#     �#     �#     �#  $   �#     $  &   %$  	   L$     V$     m$  	   u$     $  	   �$  F   �$  ,   �$  8   %     =%  
   M%     X%     _%  
   l%     w%     �%     �%     �%     �%     �%     �%     �%     �%     �%  
   �%     �%     &     &     -&     =&  /   B&  	   r&     |&     �&     r   %   2   I   3              V   "      T           v   C   H       l   ^       +           *   m   {   (             J      [            �   :       �   B   	      D      i       j   $   -   <   Y   
           e          E   q   R          s           f       S   X       #   0   6              |       `   U      Q          �   \      x       �   b   K   ~   M              _   W              >          �   '       �   N       d   a   �      w                  .   =   t   u   8      G       n   k       p              Z   F   g   A   O   &   4          /      �   !              7   c   h       9   ;       y   o   }   ,   L   ?                 ]   z       1   5   @   )   P             Disk usage:   Memory usage:   Network usage: ### This is a yaml representation of the configuration.
### Any line starting with a '# will be ignored.
###
### A sample configuration looks like:
### name: container1
### profiles:
### - default
### config:
###   volatile.eth0.hwaddr: 00:16:3e:e9:f8:7f
### devices:
###   homedir:
###     path: /extra
###     source: /home/user
###     type: disk
### ephemeral: false
###
### Note that the name is shown but cannot be changed %s (%d more) (none) ALIAS ARCH ARCHITECTURE Accept certificate Admin password for %s:  Aliases: An environment variable of the form HOME=/home/foo Architecture: %s Auto update: %s Available commands: Bytes received Bytes sent CREATED AT Can't read from stdin: %s Cannot provide container name to list Certificate fingerprint: %x Client certificate stored at server:  Columns Connection refused; is LXD running? Container name is mandatory Container name is: %s Container published with fingerprint: %s Copying the image: %s Created: %s Creating %s Creating the container DESCRIPTION Device %s added to %s Device %s removed from %s EPHEMERAL EXPIRY DATE Enables debug mode. Enables verbose mode. Environment: Ephemeral container Expires: %s Expires: never FINGERPRINT Fingerprint: %s Force the container to shutdown. Force the removal of stopped containers. Generating a client certificate. This may take a minute... IPV4 IPV6 Image copied successfully! Image imported with fingerprint: %s Invalid configuration key Invalid source %s Invalid target %s Ips: Keep the image up to date after initial copy LXD socket not found; is LXD running? Log: Make image public Make the image public Memory (current) Memory (peak) Missing summary. More than one file to download, but target is not a directory Must supply container name for:  NAME NO Name: %s No certificate provided to add No fingerprint specified. Only https URLs are supported for simplestreams Only https:// is supported for remote image import. Options: Output is in %s PERSISTENT PID PROFILES PROTOCOL PUBLIC Packets received Packets sent Path to an alternate client configuration directory. Path to an alternate server directory. Permisson denied, are you in the lxd group? Pid: %d Press enter to open the editor again Press enter to start the editor again Print debug information. Print verbose information. Processes: %d Profile %s created Profile %s deleted Profiles: %s Properties: Public image server Public: %s Remote admin password Remove %s (yes/no):  Require user confirmation. Resources: Retrieving image: %s SIZE SNAPSHOTS STATE STATIC Server doesn't trust us after adding our cert Server protocol (lxd or simplestreams) Show all commands (not just interesting ones) Size: %.2fMB Snapshots: Source: Starting %s Status: %s Swap (current) Swap (peak) TYPE The device doesn't exist Type: persistent URL Uploaded: %s Usage: %s YES default disabled enabled error: %v error: unknown command: %s got bad version no not all the profiles from the source exist on the target ok (y/n)? remote %s already exists remote %s doesn't exist Project-Id-Version: lxd
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-05-11 18:51-0400
PO-Revision-Date: 2016-04-10 14:35+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:34+0000
X-Generator: Launchpad (build 18115)
   Utilização de disco:   Utilização de memória:   Utilização da Rede ### Este é um exemplo da configuração do yaml
### Cada linha é iniciada com um '# será ignorada
### Um exemplo de configuração é semelhante a:
### nome: container1
### perfis:
### - padrão
### config:
### volatile.eth0.hwaddr: 00:16:3e:e9:f8:7f
### dispositivos:
### homedir:
### path: /extra
### source: /home/user
### tipo: disco
### ephemeral: false
### Note-se que o nome é mostrado mas não pode ser alterado %s (%d mais) (nenhum) ALIAS ARCH ARQUITETURA Aceitar certificado Palavra-passe de admin para %s:  Sinónimos: Uma variável de ambiente da forma HOME =/home/foo Arquitetura: %s Auto atualização: %s Comandos disponíveis: Bytes recebidos Bytes enviados CRIADO EM Impossível ler de stdin: %s Impossível fornecer o nome do contentor para a lista Certificado de impressão digital: %x Certificado de cliente guardado no servidor:  Colunas Ligação rejeitada; o LXD está a ser executado? O nome do contentor é obrigatório O nome do contentor é: %s Contentor publicado com a impressão digital: %s A copiar a imagem: %s Criado: %s A criar %s A criar o contentor DESCRIÇÃO Dispositivo %s adicionado a %s Dispositivo %s removido de %s EFÊMERO DATA DE VALIDADE Ativa o modo de depuração. Ativa modo verboso. Ambiente: Contentor efêmero Expira: %s Expira: nunca IMPRESSÃO DIGITAL Impressão digital: %s Forçar o contentor a desligar. Forçar a remoção de contentores parados. A gerar um certificado de cliente. Isto pode demorar... IPV4 IPV6 Imagem copiada com sucesso! Imagem importada com a impressão digital: %s Chave de configuração inválida Fonte inválida %s Alvo inválido %s Ips: Manter a imagem atualizada depois da cópia inicial Sem sockets LXD; o LXD está em execução? Registo: Tornar imagem publica Tornar a imagem publica Memória (atual) Memória (máxima) Sumário em falta. Mais do que um ficheiro para transferir, mas o destino não é um diretório Deve fornecer um nome para o contentor:  NOME NÃO Nome: %s Sem certificados fornecidos para adicionar Sem impressão digital definida. Apenas https URLs são suportados para simplestreams Apenas https:// é suportado para importação remota de imagens. Opções: Saída é em %s PERSISTENTE PID PERFIS PROTOCOLO PÚBLICO Pacotes recebidos Pacotes enviados Caminho para um diretório de configuração de cliente alternativo. Caminho para um diretório de servidor alternativo. Permissão recusada, está no grupo lxd? Pid: %d Pressione enter para abrir novamente o editor Pressione enter para iniciar novamente o editor Imprimir informação de depuração. Imprimir informação verbosa. Processos: %d Perfil %s criado Perfil %s eliminado Perfis: %s Propriedades: Servidor de imagem pública Pública: %s Palavra-passe de aministrador remoto Remover %s (sim/nã0):  Requer da confirmação do utilizador. Recursos: A recuperar imagem: %s TAMANHO SNAPSHOTS ESTADO ESTÁTICO O servidor não confia em nós depois de adicionar o nosso certificado Protocolo de servidor (lxd ou simplestreams) Mostrar todos os comandos (não apenas os interessantes) Tamanho: %.2fMB Snapshots: Fonte: A iniciar %s Estado: %s Swap (atual) Swap (máximo) TIPO O dispositivo não existe Tipo: persistente URL Carregado: %s Utilização: %s SIM omissão desativado ativo erro: %v erro: comando desconhecido: %s tem má versão não nem todos os perfis da fonte existem no destino ok (s/n)? %s remoto já existe %s remoto não existe 