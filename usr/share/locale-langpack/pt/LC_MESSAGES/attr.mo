��            )         �     �  �  �  �  �  !      !   B  /   d  =   �  2   �  $        *  %   C  .   i  +   �  *   �  .   �     	     9	     U	     s	     �	  &   �	     �	     �	  @   �	  3   /
  R  c
     �     �     �       �        �  �  �  a  M  $   �  %   �  B   �  R   =  <   �  &   �     �  7     0   H  2   y  .   �  6   �  &     '   9  (   a  (   �     �  -   �     �       F   %  6   l  I  �     �       "   #     F                                                                                     	                                      
                    %s %s
   -n, --name=name         get the named extended attribute value
  -d, --dump              get all extended attribute values
  -e, --encoding=...      encode values (as 'text', 'hex' or 'base64')
      --match=pattern     only get attributes with names matching pattern
      --only-values       print the bare values only
  -h, --no-dereference    do not dereference symbolic links
      --absolute-names    don't strip leading '/' in pathnames
  -R, --recursive         recurse into subdirectories
  -L, --logical           logical walk, follow symbolic links
  -P  --physical          physical walk, do not follow symbolic links
      --version           print version and exit
      --help              this help text
   -n, --name=name         set the value of the named extended attribute
  -x, --remove=name       remove the named extended attribute
  -v, --value=value       use value as the attribute value
  -h, --no-dereference    do not dereference symbolic links
      --restore=file      restore extended attributes
      --version           print version and exit
      --help              this help text
 %s %s -- get extended attributes
 %s %s -- set extended attributes
 %s: %s: No filename found in line %d, aborting
 %s: No filename found in line %d of standard input, aborting
 %s: Removing leading '/' from absolute path names
 %s: invalid regular expression "%s"
 -V only allowed with -s
 A filename to operate on is required
 At least one of -s, -g, -r, or -l is required
 Attribute "%s" had a %d byte value for %s:
 Attribute "%s" has a %d byte value for %s
 Attribute "%s" set to a %d byte value for %s:
 Could not get "%s" for %s
 Could not list "%s" for %s
 Could not remove "%s" for %s
 Could not set "%s" for %s
 No such attribute Only one of -s, -g, -r, or -l allowed
 Unrecognized option: %c
 Usage: %s %s
 Usage: %s %s
       %s %s
Try `%s --help' for more information.
 Usage: %s %s
Try `%s --help' for more information.
 Usage: %s [-LRSq] -s attrname [-V attrvalue] pathname  # set value
       %s [-LRSq] -g attrname pathname                 # get value
       %s [-LRSq] -r attrname pathname                 # remove attr
       %s [-LRq]  -l pathname                          # list attrs 
      -s reads a value from stdin and -g writes a value to stdout
 getting attribute %s of %s listing attributes of %s setting attribute %s for %s setting attributes for %s Project-Id-Version: attr
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-03 13:57+0000
PO-Revision-Date: 2009-04-06 12:59+0000
Last-Translator: Pedro Flores <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:59+0000
X-Generator: Launchpad (build 18115)
        %s %s
   -n, --name=nome obtém o valor do atributo estendido nomeado
  -d, --dump obtém os valores de todos os atributos estendidos
  -e, --encoding=... codifica valores (como 'text', 'hex' ou 'base64')
      --match=padrão obtém somente atributos com nomes que atendem ao padrão
      --only-values imprime somente os valores
  -h, --no-dereference não segue link simbólicos
      --absolute-names não remove a '/' inicial dos nomes de caminho
  -R, --recursive segue por sub diretórios
  -L, --logical segue link simbólicos
  -P, --physical não segue link simbólicos
      --version imprime a versão e sai
      --help este texto de ajuda
   -n, --name=nome define o valor do atributo estendido nomeado
  -x, --remove=nome remove o atributo estendido nomeado
  -v, --value=valor usa valor como o valor do atributo
  -h, --no-dereference não segue links simbólicos
      --restore=arquivo restaura atributos estendidos
      --version imprime a versão e sai
      --help este texto de ajuda
 %s%s - obtenha atributos extendidos
 %s%s -- definir atributos extendidos
 %s: %s: Nenhum nome de ficheiro encontrado na linha %d, abortando
 %s: Nenhum nome de ficheiro encontrado na linha %d de entrada standard, abortando
 %s: A remover os '/' iniciais de nomes de caminho absolutos
 %s: expressão regular inválida "%s"
 -V só é permitido com -s
 É necessário um nome de arquivo com o qual trabalhar
 Pelo menos um -s, -g, -r, ou -l é necessário.
 O atributo "%s" tem um valor de %d bytes para %s:
 Atributo "%s" tem um valor de byte %d para %s
 O atributo "%s" atribui um valor de %d bytes para %s:
 Não foi possível obter "%s" para %s
 Não foi possível listar "%s" para %s
 Não foi possível remover "%s" para %s
 Não é possível atribuir "%s" para %s
 Atributo inexistente Apenas um dos -s, -g, -r, ou -l é permitido
 Opção desconhecida: %c
 Utilização: %s %s
 Uso: %s%s
       %s%s
Experimente '%s --help' para mais informação.
 Uso: %s %s
Tentar `%s --help' para mais informação.
 Utilização: %s [-LRSq] -s nomedoatributo [-V valor] caminho # define valor
       %s [-LRSq] -g nomedoatributo caminho # obtém valor
       %s [-LRSq] -r nomedoatributo caminho # remove atributo
       %s [-LRq] -l caminho # lista atributos
      -s lê um valor da entrada padrão e -g escreve um valor para a saída padrão
 a obter atributo %s de %s Listando os atributos de %s A definir atributo para %s para %s A definir atributos para %s 