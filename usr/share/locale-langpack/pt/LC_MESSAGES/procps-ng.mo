��          �      �       H  1   I  .   {     �  *   �  .   �  0     .   D  B   s  .   �  /   �               "  �  .  =   �  2        Q  .   a  2   �  8   �  2   �  K   /  2   {  5   �     �     �     �                                             
             	            --si            use powers of 1000 not 1024
      --tera          show output in terabytes
  %s [options]
  -b, --bytes         show output in bytes
  -g, --giga          show output in gigabytes
  -h, --human         show human-readable output
  -k, --kilo          show output in kilobytes
  -l, --lohi          show detailed low and high memory statistics
  -m, --mega          show output in megabytes
  -t, --total         show total for RAM + swap
 Swap: Total: write error Project-Id-Version: procps
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-09-23 13:53+0200
PO-Revision-Date: 2014-09-12 22:21+0000
Last-Translator: Alexandre Fidalgo <alexandremagnos15@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:04+0000
X-Generator: Launchpad (build 18115)
      --si            utilize potências de 1000 não de 1024
      --tera          mostra a saída em terabytes
  %s [opções]
  -b, --bytes         mostra a saída em bytes
  -g, --giga          mostra a saída em gigabytes
  -h, --human         mostra saída humanamente-legivél
  -k, --kilo          mostra a saída em kilobytes
  -l, --lohi         mostra estatísticas de memoria baixa e alta detalhada
  -m, --mega          mostra a saída em megabytes
  -t, --total         mostrar total para a RAM + swap
 Swap: Total: erro de escrita 