��    9      �  O   �      �     �               $     7  '   H  ;   p  �   �     V  !   c     �  +   �     �     �     �     �     �  %        .  4   I     ~     �     �      �     �     �     
          &     2     A  "   J      m     �     �     �     �      �     �     �     	     	  (   	     A	     M	     Z	  	   c	     m	     t	     |	     �	     �	     �	     �	     �	     �	    �	     �          %     6     G  >   V  >   �  �   �     �  3   �     �  ?   �       
     	   (  -   2     `  -     !   �  :   �     
     (     >  #   ]     �     �     �     �     �     �  
   �  '   �  *   "     M     `     r     �     �     �     �     �     �  *   �          (     9     B     K  	   R  .   \     �     �     �     �     �  6  �     .                3            ,                              6                7   9          	             -       %             +   !                      $         (       &           #   '          8   /   
   4          "       1      )            2       *   0       5            "%s" is not a valid folder %d day %d days %d item %d items %d month %d months %d year %d years A graphical tool to analyze disk usage. A list of URIs for partitions to be excluded from scanning. A simple application which can scan either specific folders (local or remote) or volumes and give a graphical representation including each directory size or percentage. Active Chart Apparent sizes are shown instead. Baobab Check folder sizes and available disk space Close Computer Contents Could not analyze disk usage. Could not analyze volume. Could not detect occupied disk sizes. Could not scan folder "%s" Could not scan some of the folders contained in "%s" Devices and locations Disk Usage Analyzer Excluded partitions URIs Failed to move file to the trash Failed to open file Failed to show help Folder Go to _parent folder Home folder Mo_ve to Trash Modified Print version information and exit Recursively analyze mount points Rings Chart Scan Folder… Select Folder Size The GdkWindowState of the window The initial size of the window Today Treemap Chart Unknown Which type of chart should be displayed. Window size Window state Zoom _in Zoom _out _About _Cancel _Copy Path to Clipboard _Help _Open _Open Folder _Quit storage;space;cleanup; translator-credits Project-Id-Version: 3.8
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=baobab&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 14:31+0000
PO-Revision-Date: 2016-03-30 19:46+0000
Last-Translator: Bruno Nova <Unknown>
Language-Team: Portuguese <gnome_pt@yahoogroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:41+0000
X-Generator: Launchpad (build 18115)
Language: pt
 "%s" não é uma pasta válida %d dia % dias %d item %d itens %d mês %d meses %d ano %d anos Uma ferramenta gráfica para analisar a utilização do disco. A lista de URIs de partições a serem excluídas da análise. Uma aplicação simples que pode analisar pastas específicas (locais ou remotas) ou volumes e fornecer uma representação gráfica que inclui o tamanho ou percentagem de cada pasta. Gráfico ativo Tamanhos aparentes são mostrados alternativamente. Baobab Verificar o tamanho das pastas e o espaço disponível em disco Fechar Computador Conteúdo Impossível analisar a utilização do disco. Impossível analisar o volume. Impossível detetar tamanho ocupado do disco. Impossível analisar a pasta "%s" Impossível analisar algumas das pastas contidas em  "%s". Dispositivos e localizações Utilização do disco URIs de partições excluídas Falha ao mover ficheiro para o lixo Falha ao abrir ficheiro Falha ao mostrar a ajuda Pasta Ir para a _pasta mãe Pasta pessoal Mo_ver para o lixo Modificado Imprimir informação de versão e sair Analisar pontos de montagem recursivamente Gráfico de anéis Analisar pasta… Selecione uma pasta Tamanho O GdkWindowState da janela O tamanho inicial da janela Hoje Gráfico de árvore Desconhecido Que tipo de gráfico deverá ser mostrado. Tamanho da janela Estado da janela _Ampliar _Reduzir So_bre _Cancelar _Copiar caminho para a área de transferência A_juda _Abrir _Abrir pasta _Sair armazenamento;espaço;limpar; Duarte Loreto <happyguy_pt@hotmail.com>
Pedro Albuquerque73@openmailbox.org

Launchpad Contributions:
  Bruno Nova https://launchpad.net/~brunonova
  Duarte Loreto https://launchpad.net/~happyguy-pt-hotmail
  Ivo Xavier https://launchpad.net/~ivoxavier
  Pedro Albuquerque https://launchpad.net/~pmralbuquerque 