��          �   %   �      0     1  )   B  #   l  "   �  *   �  $   �  !     =   %  $   c     �     �  E   �  (        .     H  %   ]  *   �  "   �     �  %   �           0  %   F  �  l       2   2  )   e  (   �  6   �  0   �  0      D   Q  :   �     �     �  U   	  ,   d	     �	     �	  /   �	  (   �	  #   
     @
  %   ^
  '   �
     �
  (   �
                                                                          	                   
                                  %s: %s (code %d) %s: connection was closed by proxy server %s: connection was closed by server Certificate verification error: %s Could not authenticate to proxy server: %s Could not authenticate to server: %s Could not determine file size: %s Digest mutual authentication failure: request-digest mismatch Failed reading request body file: %s GSSAPI authentication error:  GSSAPI failure (code %u) Negotiate response verification failed: invalid response header token Response did not include requested range could not parse challenge ignored %s challenge ignoring empty Negotiate continuation incompatible algorithm in Digest challenge initial Digest challenge was stale invalid Negotiate token missing parameter in Digest challenge missing realm in Basic challenge rejected %s challenge unknown algorithm in Digest challenge Project-Id-Version: neon27
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-09-22 16:25+0100
PO-Revision-Date: 2014-07-26 18:27+0000
Last-Translator: Alexandre Fidalgo <alexandremagnos15@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:25+0000
X-Generator: Launchpad (build 18115)
 %s: %s (código %d) %s: a ligação foi fechada pelo servidor de proxy %s: a ligação foi fechada pelo servidor Erro na verificação de certificado: %s Não se conseguiu autenticar para o servidor proxy: %s Não se conseguiu autenticar para o servidor: %s Impossível determinar o tamanho do ficheiro: %s Falha na autenticação mutua do Digest: pedido-digest incompatível Falha na solicitação da leitura do corpo do ficheiro: %s Erro de autenticação GSSAPI:  Falha de GSSAPI (código %u) Verificação da resposta negociada falhou: resposta inválida do cabeçalho de token a resposta nao inclui o intervalo solicitado não pôde analisar o desafio desafio %s ignorado Ignorar a continuação de verificação vazia. algoritmo incompativel em desafio Digest desafio inicial Digest era estável negociaçao de token invalida parâmetro em falta em desafio Digest âmbito desaparecido em desafio básico desafio %s rejeitado algoritmo desconhecido em desafio Digest 