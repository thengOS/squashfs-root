��    �        �   
      �     �  6   �  (   �  -      -   .      \  &   }  %   �     �  *   �  !   	     +  2   <     o  "   �     �     �     �     �     �          !     .     ;     T     j     ~  	   �     �  
   �  4   �  $   �          
  ;        [  #   v  3   �  1   �  *      )   +     U     u  &   �     �  !   �  -   �  
   &      1     R      e     �  )   �  M   �  &         @     a      o      �     �     �     �     �  0   �       #   ;     _      x  &   �  *   �  (   �  *     )   ?  (   i      �      �      �     �       ,        K     \     v     �     �     �     �     �          '     B     [     s     �     �     �     �     �                ?     [  -   w  %   �    �    K     d  %   �     �  /   �  5   �     $  $   C  ?   h     �     �     �  
   �     �     �       "   4  (   W  5   �  "   �     �  M   �  ,   6     c  /   {  )   �  
   �     �  &   �     $   H   7      �   =   �      �   .   �   -   )!     W!  "   v!  	   �!  '   �!  "   �!     �!  %   "     ."     5"  )   P"  :   z"  0   �"  :   �"     !#  	   <#     F#     N#  �  n#     4%  D   L%  .   �%  /   �%  /   �%  !    &  ,   B&  +   o&     �&  5   �&     �&     �&  4   '     C'  !   V'     x'  '   �'     �'     �'     �'     �'     (     &(     :(     T(     s(     �(     �(     �(     �(  8   �(  (   �(     )      )  P   4)     �)     �)  ?   �)  ?   �)  '   9*  &   a*  &   �*  /   �*  1   �*     +  "   ,+  2   O+     �+  ,   �+     �+  %   �+     �+  5   ,  f   L,  '   �,  ,   �,  "   -  &   +-  &   R-  $   y-  '   �-     �-     �-  H   �-  %   -.  &   S.     z.  )   �.  )   �.  -   �.  +   /  -   C/  ,   q/  +   �/  )   �/  ,   �/  &   !0  ,   H0     u0  8   �0     �0     �0     �0     1     21     O1  !   n1     �1      �1      �1     �1     2     .2     L2      l2     �2  *   �2     �2     �2     �2  "   3     63  6   R3  .   �3  �  �3  [  _5     �6  6   �6     7  6   ,7  G   c7     �7  /   �7  d   �7     `8     p8     �8     �8  "   �8     �8      �8  4    9  :   59  G   p9  ;   �9     �9  �   :  =   �:     �:  E   �:  5   );  	   _;     i;  -   �;     �;  M   �;  (   <  L   A<     �<  /   �<  /   �<     =  #   .=     R=  :   ^=  0   �=     �=  '   �=     >     >  2   0>  F   c>  ;   �>  F   �>  "   -?  
   P?     [?  ,   h?            y   N      �   �           �      i       t   r      c          W                  s              �   �   2   `   D   �   z          M       .   �      �       �   p   G   R              u   P   _           j       /       d      )   �   �   e           �   0   k       �       >   !              S   �       Z   +   %       <   g   x   (   B      w   n      �      |   l   �   F   C   @   �   �   L   $                          '   	   8   [         h   ]   4      =   �   3   I       K       �           
   ,       �   �   U       5   Q       �      �           6   *       �   o   \   A   #      �       �   �   f       X   1   b   ~   v   ;   �   {         V   Y          q       O      J                                 "   E               -              &       9          �   ^   7   a       �       }   �           T   m   ?   :           H      APOP secret = "%s".
   All available authentication methods will be tried.
   GSSAPI authentication will be forced.
   Kerberos V4 authentication will be forced.
   Kerberos V5 authentication will be forced.
   Mail will be retrieved via %s
   NTLM authentication will be forced.
   OTP authentication will be forced.
   Password = "%s".
   Password authentication will be forced.
   Password will be prompted for.
   Protocol is %s   Protocol is KPOP with Kerberos %s authentication   RPOP id = "%s".
   SSL encrypted sessions enabled.
   SSL protocol: %s.
   True name of server is %s.
  (%d body octets)  (%d header octets)  (%d octets).
  (forcing UIDL use)  (length -1)  (oversized)  (previously authorized)  (using default port)  (using service %s)  and   flushed
  not flushed
  retained
 %d message (%d %s) for %s %d messages (%d %s) for %s %d message for %s %d messages for %s %s at %s %s at %s (folder %s) %s configuration invalid, LMTP can't use default SMTP port
 %s connection to %s failed %s error while fetching from %s@%s
 %s querying %s (protocol %s) at %s: poll completed
 %s querying %s (protocol %s) at %s: poll started
 %s's SMTP listener does not support ESMTP
 %s's SMTP listener does not support ETRN
 %s: You don't exist.  Go away.
 %s: can't determine your host! All connections are wedged.  Exiting.
 Authorization OK on %s@%s
 Authorization failure on %s@%s%s
 Checking if %s is really the same node as %s
 DNS lookup ETRN support is not configured.
 ETRN syntax error
 ETRN syntax error in parameters
 Enter password for %s@%s:  Fetchmail could not get mail from %s@%s.
 Fetchmail saw more than %d timeouts while attempting to get mail from %s@%s.
 Fetchmail was able to log into %s@%s.
 IMAP support is not configured.
 Idfile is %s
 Kerberos V4 support not linked.
 Kerberos V5 support not linked.
 Lead server has no name.
 Logfile is %s
 MDA No mail for %s
 No mailservers set up -- perhaps %s is missing?
 No messages waiting for %s
 No, their IP addresses don't match
 Node %s not allowed: %s
 ODMR support is not configured.
 Option --all is not supported with %s
 Option --check is not supported with ETRN
 Option --flush is not supported with %s
 Option --flush is not supported with ETRN
 Option --keep is not supported with ETRN
 Option --limit is not supported with %s
 POP2 support is not configured.
 POP3 support is not configured.
 Pending messages for %s started
 Poll interval is %d seconds
 Polling %s
 Progress messages will be logged via syslog
 Query status=%d
 Query status=0 (SUCCESS)
 Query status=1 (NOMAIL)
 Query status=10 (SMTP)
 Query status=11 (DNS)
 Query status=12 (BSMTP)
 Query status=13 (MAXFETCH)
 Query status=2 (SOCKET)
 Query status=3 (AUTHFAIL)
 Query status=4 (PROTOCOL)
 Query status=5 (SYNTAX)
 Query status=6 (IOERR)
 Query status=7 (ERROR)
 Query status=8 (EXCLUDE)
 Query status=9 (LOCKBUSY)
 Queuing for %s started
 Repoll immediately on %s@%s
 SMTP transaction SSL connection failed.
 SSL support is not compiled in.
 Server busy error on %s@%s
 Service has been restored.
 Subject: fetchmail authentication OK on %s@%s Taking options from command line%s%s
 The attempt to get authorization failed.
This probably means your password is invalid, but some servers have
other failure modes that fetchmail cannot distinguish from this
because they don't send useful error messages on login failure.

The fetchmail daemon will continue running and attempt to connect
at each cycle.  No future notifications will be sent until service
is restored. This could mean that your mailserver is stuck, or that your SMTP
server is wedged, or that your mailbox file on the server has been
corrupted by a server error.  You can run `fetchmail -v -v' to
diagnose the problem.

Fetchmail won't poll this mailbox again until you restart it.
 This is fetchmail release %s Unable to queue messages for node %s
 Unknown ETRN error %d
 Unknown login or authentication error on %s@%s
 Warning: multiple mentions of host %s in config file
 Yes, their IP addresses match
 attempt to re-exec fetchmail failed
 attempt to re-exec may fail as directory has not been restored
 awakened at %s
 awakened by %s
 awakened by signal %d
 background bogus message count! client/server protocol client/server synchronization could not decode BASE64 challenge
 could not get current working directory
 couldn't fetch headers, message %s@%s:%d (%d octets)
 couldn't find HESIOD pobox for %s
 decoded as %s
 fetchmail: can't check mail while another fetchmail to same host is running.
 fetchmail: can't find a password for %s@%s.
 fetchmail: invoked with fetchmail: no mailservers have been specified.
 fetchmail: no other fetchmail is running
 foreground gethostbyname failed for %s
 interval not reached, not querying %s
 kerberos error %s
 message %s@%s:%d was not the expected length (%d actual != %d expected)
 missing or bad RFC822 header nameserver failure while looking for `%s' during poll of %s.
 normal termination, status %d
 post-connection command failed with status %d
 pre-connection command failed with status %d
 reading message %s@%s:%d of %d restarting fetchmail (%s changed)
 seen seen selecting or re-polling default folder
 selecting or re-polling folder %s
 skipping message %s@%s:%d skipping message %s@%s:%d (%d octets) socket terminated with signal %d
 timeout after %d seconds waiting for %s.
 timeout after %d seconds waiting for listener to respond.
 timeout after %d seconds waiting for server %s.
 timeout after %d seconds waiting to connect to server %s.
 timeout after %d seconds.
 undefined unknown unsupported protocol selected.
 Project-Id-Version: fetchmail
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-04-23 23:24+0200
PO-Revision-Date: 2010-02-11 22:34+0000
Last-Translator: Carlos Manuel <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 16:21+0000
X-Generator: Launchpad (build 18115)
   segredo APOP = "%s".
   Todos os métodos de autenticação disponíveis serão tentados.
   Será forçada a autenticação por GSSAPI.
   Será forçada a autenticação Kerberos V4.
   Será forçada a autenticação Kerberos V5.
   O correiro será obtido via %s
   Será forçada a autenticação por NTLM.
   Será forçada a autenticação por OTP.
   Senha ="%s".
   Será forçada a autenticação através de senha.
   Será pedida a senha.
   O protocolo é %s   O protocolo é KPOP com autenticação Kerberos %s   id RPOP = "%s".
   Sessões cifradas SSL activas.
   Protocolo SSL: %s.
   O verdadeiro nome do servidor é %s.
  (%d octetos do corpo)  (%d octetos de cabeçalho)  (%d octetos).
  (a forçar o uso de UIDL)  (comprimento -1)  (demasiado grande)  (previamente autorizado)  (a usar o porto por omissão)  (a usar o serviço %s)  e   eliminada
  não eliminada
  retido
 %d mensagem (%d %s) para %s %d mensagens (%d %s) para %s %d mensagem para %s %d mensagens para %s %s em %s %s em %s (pasta %s) %s configuração inválida, o LMTP não pode usar o porto por omissão do SMTP
 %s ligação a %s falhou %s erro ao obter de %s@%s
 %s a consultar %s (protocolo %s) em %s: verificação completa
 %s a consultar %s (protocolo %s) em %s: verificação iniciada
 Servidor SMTP de %s não suporta ESMTP
 Servidor SMTP de %s não suporta ETRN
 %s: Você não existe. Vá-se embora.
 %s: não foi possível determinar sua máquina! Todas as ligações estão travadas. A terminar.
 Autorização OK em %s@%s
 Falha de autorização em %s@%s%s
 A verificar se %s é realmente o mesmo nó que %s
 procura DNS o suporte para ETRN não está configurado.
 Erro de sintaxe ETRN
 Erro de sintaxe ETRN nos parâmetros
 Introduza a senha para %s@%s:  O fetchmail não conseguiu obter o correio de %s@%s.
 O Fetchmail encontrou mais de %d excessos de tempo de espera enquanto tentava obter correio de %s@%s.
 O fetchmail consegui ligar-se a %s@%s.
 o suporte para IMAP não está configurado.
 Ficheiro de identificação é %s
 Suporte a Kerberos V4 não incluído.
 Suporte a Kerberos V5 não incluído.
 O servidor principal não tem nome.
 O ficheiro de registos (logfile) é %s
 MDA Não há correio para %s
 Nenhum servidor de correio electrónico configurado -- talvez falte %s?
 Não há mensagens em espera para %s
 Não,  os endereços IP não combinam
 Nó %s não permitido: %s
 O suporte a ODMR não está configurado.
 A opção --all não é suportada com %s
 A opção --check não é suportada com ETRN
 A opção --flush não é suportada com %s
 A opção --flush não é suportada com ETRN
 A opção --keep não é suportada com ETRN
 A opção --limit não é suportada com %s
 o suporte a POP2 não está configurado.
 o suporte para POP3 não está configurado.
 Mensagens pendentes para %s iniciadas
 O intervalo de verificação é %d segundos
 A verificar %s
 As mensagens de progresso serão registadas pelo syslog
 Estado da consulta=%d
 Estado da consulta=0 (SUCESSO)
 Estado da consulta=1 (NOMAIL)
 Estado da consulta=10 (SMTP)
 Estado da consulta=11 (DNS)
 Estado da consulta=12 (BSMTP)
 Estado da consulta=13 (MAXFETCH)
 Estado da consulta=2 (SOCKET)
 Estado da consulta=3 (AUTHFAIL)
 Estado da consulta=4 (PROTOCOL)
 Estado da consulta=5 (SYNTAX)
 Estado da consulta=6 (IOERR)
 Estado da consulta=7 (ERROR)
 Estado da consulta=8 (EXCLUDE)
 Estado da consulta=9 (LOCKBUSY)
 Fila para %s iniciada
 Voltar a verificar imediatamente em %s@%s
 transacção SMTP ligação SSL falhou.
 Nao existe suporte a SSL.
 Erro de servidor ocupado em %s@%s
 O serviço foi restaurado.
 Assunto: autenticação do fetchmail está OK em %s@%s Utilizando opções da linha de comandos %s%s
 A tentativa de autorização falhou.
Isto provavelmente indica que a sua senha está incorrecta, mas alguns servidores têm
outros tipos de falha que o fetchmail não consegue distinguir desta
porque eles não enviam uma mensagem de erro válida quando o login falha.

O serviço fetchmail continuará a executar e a tentar ligar
a cada ciclo. Nenhuma notificação futura será enviada até o serviço
ser reestabelecido. Isto pode significar que o seu servidor de correio está travado, ou que o seu servidor SMTP
está encalhado, ou que o seu ficheiro de caixa de correio no servidor foi corrompido por um erro no servidor. Pode executar `fetchmail -v -v' para diagnosticar o problema.

O Fetchmail não irá consultar mais esta caixa de correio até que o reinicie.
 Este é o fetchmail versão %s Impossível construir fila de mensagens para o nó %s
 Erro ETRN desconhecido %d
 Login desconhecido ou erro de autenticação em %s@%s
 Aviso: multiplas mensões ao servidor %s no ficheiro de configuração
 Sim, os endereços IP combinam
 a tentativa de re-executar o fetchmail falhou.
 a tentativa de re-execução poderá falhar visto que o directório não foi restaurado com sucesso
 acordado em %s
 despertado por %s
 acordado pelo sinal %d
 fundo contador de mensagens sem sentido! protocolo cliente/servidor sincronização cliente/servidor não foi possível descodificar o desafio em BASE64
 não foi possível obter o directório de trabalho actual
 Não foi possivel obter os cabeçalhos, mensagem %s@%s:%d (%d octetos)
 não foi possível encontrar a caixa postal HESIOD para %s
 descodificado como %s
 Fecthmail : Impossivel verificar correio electrónico enquanto outra aplicação fetchmail estiver a correr no mesmo computador.
 fetchmail: não é possível encontrar uma senha para %s@%s.
 fetchmail: invocado com fetchmail: nenhum servidor de correio electrónico foi especificado.
 fetchmail: mais nenhum fetchmail se está a executar
 1º plano gethostbyname falhou para %s
 intervalo não atingido, não consultando %s
 erro de kerberos %s
 mensagem %s@%s:%d não tinha o comprimento esperado (%d real != %d esperado)
 cabeçalho RFC822 corrompido ou em falta falha na resolução de nomes a procurar por `%s' durante a consulta de %s.
 terminação normal, status %d
 comando de pós-ligação falhou com estado %d
 comando de pré-ligação falhou com estado %d
 A ler a mensagem %s@%s:%d de %d a reiniciar o fetchmail (%s mudou)
 visto visto a seleccionar ou verificar novamente a pasta por omissão
 a seleccionar ou verificar novamente a pasta %s
 Saltando mensagem %s@%s:%d Saltando mensagem %s@%s:%d (%d octetos) 'socket' terminado com o sinal %d
 tempo esgotado após %d segundos à espera de %s.
 tempo esgotado após %d segundos esperando pela resposta do receptor.
 tempo esgotado após %d segundos à espera do servidor %s.
 tempo esgotado após %d segundos à espera para ligar ao servidor %s.
 tempo esgotado após %d segundos.
 indefinido Desconhecido o protocolo seleccionado não é suportado.
 