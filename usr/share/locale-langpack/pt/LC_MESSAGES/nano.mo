��    p     �  �        �     �     �     �       (   ,     U     c  =   ~  D   �  	                     *      8   	   F      P      c      y      �   (   �   $   �      �      �      �      !     !     #!     2!     G!     X!     i!     x!     �!     �!  
   �!     �!     �!  
   �!  	   �!     �!     �!  �   �!     �"     �"  #   �"     #     ##     C#     b#  &   g#  	   �#     �#     �#  	   �#     �#     �#  
   �#     �#  -  $  �  2%     �&  )   �&  $   '     ,'     F'     `'     g'  	   �'  3   �'  2   �'  	   �'     �'  .   (  �   3(     �(  )   �(     &)     B)  +   `)      �)     �)  "   �)     �)  	   *  3   *     K*     a*  !   p*  +   �*  0   �*  -   �*     +     %+     .+  3   M+  2   �+  
   �+  
   �+     �+  
   �+     �+  .   �+  %   ,  &   6,  (   ],  "   �,     �,     �,  '   �,  '   -     ,-     G-     e-     x-     �-     �-     �-     �-     �-  
   �-     �-      .     %.     ?.     S.     l.     �.     �.     �.      �.     �.      /  /  /     @0     Y0     ^0     m0  �  �0     ;2     R2     j2     }2     �2  )   �2  9   �2     3     3  
   63  
   A3  &   L3  (   s3     �3     �3  
   �3     �3  	   �3     �3  
   �3    �3  
   �4     �4     5     "5     ;5  7   O5     �5  8   �5     �5     �5     
6     6     :6     J6      ^6      6     �6     �6     �6     �6  #   7  	   ?7     I7  %   N7  �   t7  �   *8     �8     �8     9  �  9  '   �:  #   �:  (   ;  "   A;  5   d;     �;  &   �;     �;     �;     <  #   <  	   A<  	   K<  (   U<  
   ~<  �  �<     >>  	   G>  
   Q>     \>     o>     �>     �>     �>     �>  
   �>     �>  	   �>  	   �>  	   �>  	   �>  
   �>     ?     
?  
   ?  !   ?     :?     T?     h?     �?     �?     �?     �?  !   �?     �?      @  &   ,@     S@     [@  $   u@  	   �@  	   �@  	   �@  
   �@  "   �@     �@     �@  �   A  d   �A  �   B  T   �B  �   C  T   �C  	   �C     �C      D  %   "D  )   HD     rD     �D     �D     �D     �D  #   �D  3   �D     $E     +E  (   3E     \E     sE  0   �E  #   �E  "   �E     �E  #   F     ,F  !   KF  =   mF     �F  	   �F  %   �F  1   �F  /   G     IG  �  PG     �H  +   �H     I  #   3I  2   WI     �I     �I     �I  �  �I     \K     vK  !   �K     �K  
   �K     �K  "   �K     L  !   L  %   <L     bL     fL  ,   �L     �L     �L  �  �L     �N     �N     �N     �N     �N  &   �N     O  )   !O     KO     hO     �O  %   �O     �O  %   �O  .   P     6P     HP     MP     eP     sP     �P     �P  0   �P     �P  &   �P       Q     AQ     _Q     nQ     sQ     �Q     �Q     �Q  %   �Q  
   �Q  �  �Q     �S     �S     �S     �S     T      T     $T     'T     DT     MT     \T  <   dT  
   �T  	   �T     �T     �T     �T     �T     �T     �T  
   U     U  �  U     �V     �V     W     6W  9   MW     �W      �W  S   �W  U   X     eX     }X     �X     �X     �X     �X     �X  0   �X     #Y     :Y  .   OY  +   ~Y     �Y     �Y  
   �Y     �Y     �Y     �Y     �Y  !   Z     8Z     IZ     iZ     {Z     �Z  
   �Z     �Z     �Z  
   �Z  	   �Z     �Z     �Z  #  �Z     �[  '   �[  (   \     D\     R\  %   r\  
   �\  +   �\     �\     �\     �\  
   ]     ]  !   /]  
   Q]     \]  ;  i]  �  �^  $   n`  0   �`  *   �`  #   �`     a     3a     <a  	   Wa  @   aa  F   �a     �a     �a  %   b  �   *b  !   �b  4   �b     3c     Oc  +   kc  /   �c  ,   �c  +   �c      d     ;d  4   Hd     }d     �d  /   �d  A   �d  1   e  H   Ke     �e     �e  '   �e  8   �e  9   
f     Df     Vf     hf     mf     yf  '   �f     �f  4   �f  :   �f     7g     Ug  ,   pg  -   �g  +   �g  !   �g  #   h     =h  /   Wh      �h     �h     �h     �h     �h  
   �h     �h  .   i     1i     Ki     `i     zi     �i     �i     �i  *   �i  ,   �i     ,j  Y  =j     �k     �k     �k     �k    �k  #   n  '   %n     Mn  #   kn     �n  +   �n  J   �n  	   "o  %   ,o     Ro     do  4   so  5   �o     �o     �o     �o     p     p     $p     9p  )  Ip     sq     �q     �q     �q     �q  `   �q  !   6r  M   Xr     �r     �r     �r  #   �r     s     5s  $   Ns  $   ss  #   �s  #   �s  #   �s  $   t  $   )t     Nt     \t  +   bt  �   �t  �   }u     bv     sv     �v  �  �v  -   Rx  &   �x      �x  %   �x  A   �x  $   0y  0   Uy  
   �y     �y     �y  (   �y     �y     z  3   z     Jz  �  Vz     J|     [|  	   h|     r|     �|     �|     �|  
   �|     �|     �|     �|     }     }     )}     :}  
   K}     V}     Y}     ^}  #   q}  %   �}     �}  )   �}  +    ~     ,~     @~     ^~  (   r~     �~      �~  <   �~       +     (   D     m     |     �  	   �  &   �     �     �  �   �  h   ��  �   �  X   ��  �   �  Y   ��     �     �      #�  D   D�  C   ��     ̓     �     �  %   	�  
   /�     :�  ;   S�     ��  
   ��  9   ��     ��     �  :   �  0   Q�  )   ��     ��     ��  <   څ  *   �  L   B�     ��     ��  '   ��  $   ކ  "   �     &�  �  /�     و  ;   �  !   '�  /   I�  H   y�          ى     �  �  �  (   �  *   �  &   >�  	   e�  
   o�  *   z�  *   ��     Ќ  '   �  +   
�     6�     B�  .   \�     ��     ��    ƍ     ɏ     ��     ��  
   �     �  A   .�     p�  =   ��     ː     �      �  '   (�      P�  *   q�  /   ��     ̑     �     �     �     �     *�     E�  ;   Z�     ��  2   ��  '   �     �     0�     @�  '   D�  
   l�     w�  #   ��  /   ��     ܓ  �  �  !   ٕ  '   ��  &   #�     J�     g�     ��     ��  &   ��     ��     ��     Ж  <   ٖ     �     &�     3�     L�     \�     i�     v�     ��     ��     ��     '  �       �   0                   g    o         �   *      �           `          �   >  �           6      �   �   �      ?       �   �   b  H  �   #           �   �   �   w   S   E  [      A       .   =   k       �   �         v      �   �   r   �   �         �     �                  p   �   <   F   (   s       �   �            &                  �   G        �   �           "             �   -   �   X  �     �   �           B  _      O   �          m   N   5   ]   �   T   a   �   [          (  �   ?      N          :           l  H   P  M     )   �      �   �   q   �   	  �   �   =      W      .  �   >   e   ^          �               �   �       �   �           �   �   z   �   �       :  V      G   8    �   �              /       	   J          �   P   +   !  �   �   C  ,   �   X       D          S      �   2      `  a  4  6           o  ~   �   ^       �   U   �       �   �       @  C   #  �   d  t   Q                �   \   &  -  U  �       K   �           L  �   j  �   �   1      ;   %      
  L   O  I   1   �   !   8           �   7      �       2          �   e          9  |   y   b   �   B   5  *       �   i  +  �   Q      f  $  n         0       �     V          �       T          ]  �   7   �       p  M          K  3  �   �   �   }   �       <  �     R  d         4       m  �   �          W  c  9          �   D  �   �       x   �   �       c         u           �     J   �   �      �         �   
   l   k     �           �           E   _   f   h  �   h   '   i       F      $       �       �   3   Z  �   @           {   �   �     �       /  �     �       �                 I  Y  �   Z         g      ;  �   �   �       \  �   �   �     R   Y              �     %       A  �       �   n   �   ,  �       �           �   j           "  )      �        
 Compiled options: 
Buffer not written to %s: %s
 
Buffer not written: %s
 
Buffer written to %s
 
Press Enter to continue starting nano.
  (to replace)  (to replace) in selection  Email: nano@nano-editor.org	Web: http://www.nano-editor.org/  The following function keys are available in Browser Search mode:

  [Backup]  [Backwards]  [Case Sensitive]  [DOS Format]  [Mac Format]  [Regexp] "%.*s%s" not found "%s" is a device file "%s" is a directory "%s" not found "start=" requires a corresponding "end=" %sWords: %lu  Lines: %ld  Chars: %lu (dir) (more) (parent dir) +LINE,COLUMN --backupdir=<dir> --fill=<#cols> --operatingdir=<dir> --quotestr=<str> --speller=<prog> --syntax=<str> --tabsize=<#cols> -C <dir> -Q <str> -T <#cols> -Y <str> -o <dir> -r <#cols> -s <prog> Aa All Also, pressing Esc twice and then typing a three-digit decimal number from 000 to 255 will enter the character with the corresponding value.  The following keystrokes are available in the main editor window.  Alternative keys are shown in parentheses:

 Append Append Selection to File Argument '%s' has an unterminated " Auto indent Auto save on exit, don't prompt Automatically indent new lines Back Background color "%s" cannot be bright Backspace Backup File Backup files Backwards Bad quote string %s: %s Bad regex "%s": %s Beg of Par Brought to you by: Browser Go To Directory Help Text

 Enter the name of the directory you would like to browse to.

 If tab completion has not been disabled, you can use the Tab key to (attempt to) automatically complete the directory name.

 The following function keys are available in Browser Go To Directory mode:

 Browser Search Command Help Text

 Enter the words or characters you would like to search for, and then press Enter.  If there is a match for the text you entered, the screen will be updated to the location of the nearest match for the search string.

 The previous search string will be shown in brackets after the search prompt.  Hitting Enter without entering any text will perform the previous search.

 Can now UnJustify! Can't go outside of %s in restricted mode Can't insert file from outside of %s Can't move up a directory Can't write outside of %s Cancel Cancel the current function Cancelled Cannot add a color command without a syntax command Cannot add a header regex without a syntax command Case Sens Close Close the current file buffer / Exit from nano Color "%s" not understood.
Valid colors are "green", "red", "blue",
"white", "yellow", "cyan", "magenta" and
"black", with the optional prefix "bright"
for foreground colors. Color syntax highlighting Command "%s" not allowed in included file Command "%s" not understood Command to execute [from %s]  Command to execute in new buffer [from %s]  Constant cursor position display Constantly show cursor position Conversion of typed tabs to spaces Convert typed tabs to spaces Copy Text Copy the current line and store it in the cutbuffer Could not create pipe Could not fork Could not get size of pipe buffer Couldn't reopen stdin from keyboard, sorry
 Count the number of words, lines, and characters Creating misspelled word list, please wait... Cur Pos Cut Text Cut from cursor to end of line Cut from the cursor position to the end of the file Cut the current line and store it in the cutbuffer Cut to end CutTillEnd DIR: DOS Format Delete Delete the character to the left of the cursor Delete the character under the cursor Detect word boundaries more accurately Directory for saving unique backup files Display the position of the cursor Display this help text Do quick statusbar blanking Don't add newlines to the ends of files Don't convert files from DOS/Mac format Don't look at nanorc files Don't show the two help lines Edit a replacement Enable alternate speller Enable smart home key Enable soft line wrapping Enable suspension Enable the use of the mouse End End of Par Enter Enter line number, column number Error in %s on line %lu:  Error invoking "%s" Error invoking "sort -f" Error invoking "spell" Error invoking "uniq" Error reading %s: %s Error writing %s: %s Error writing backup file %s: %s Error writing temp file: %s Execute Command Execute Command Help Text

 This mode allows you to insert the output of a command run by the shell into the current buffer (or a new buffer in multiple file buffer mode).  If you need another blank buffer, do not enter any command.

 The following function keys are available in Execute Command mode:

 Execute external command Exit Exit from nano Exit from the file browser File Browser Help Text

 The file browser is used to visually browse the directory structure to select a file for reading or writing.  You may use the arrow keys or Page Up/Down to browse through the files, and S or Enter to choose the selected file or enter the selected directory.  To move up one level, select the directory called ".." at the top of the file list.

 The following function keys are available in the file browser:

 File Name to Append to File Name to Prepend to File Name to Write File exists, OVERWRITE ?  File to insert [from %s]  File to insert into new buffer [from %s]  File was modified since you opened it, continue saving ?  File: Finished checking spelling First File First Line Fix Backspace/Delete confusion problem Fix numeric keypad key confusion problem For ncurses: Forward FullJstify Get Help Go To Dir Go To Directory Go To Line Go To Line Help Text

 Enter the line number that you wish to go to and hit Enter.  If there are fewer lines of text than the number you entered, you will be brought to the last line of the file.

 The following function keys are available in Go To Line mode:

 Go To Text Go back one character Go back one word Go forward one character Go forward one word Go just beyond end of paragraph; then of next paragraph Go to beginning of current line Go to beginning of paragraph; then of previous paragraph Go to directory Go to end of current line Go to file browser Go to line and column number Go to next line Go to previous line Go to the first file in the list Go to the first line of the file Go to the last file in the list Go to the last line of the file Go to the matching bracket Go to the next file in the list Go to the previous file in the list Help mode Home I can't find my home directory!  Wah! If you have selected text with the mark and then search to replace, only matches in the selected text will be replaced.

 The following function keys are available in Search mode:

 If you need another blank buffer, do not enter any filename, or type in a nonexistent filename at the prompt and press Enter.

 The following function keys are available in Insert File mode:

 In Selection:   Indent Text Indent the current line Insert File Help Text

 Type in the name of a file to be inserted into the current file buffer at the current cursor location.

 If you have compiled nano with multiple file buffer support, and enable multiple file buffers with the -F or --multibuffer command line flags, the Meta-F toggle, or a nanorc file, inserting a file will cause it to be loaded into a separate buffer (use Meta-< and > to switch between file buffers).   Insert a newline at the cursor position Insert a tab at the cursor position Insert another file into the current one Insert the next keystroke verbatim Internal error: unknown type.  Please save your work. Invalid line or column number Invoke the spell checker, if available Justify Justify the current paragraph Justify the entire file Key invalid in non-multibuffer mode Last File Last Line Log & read search/replace string history Mac Format Main nano help text

 The nano editor is designed to emulate the functionality and ease-of-use of the UW Pico text editor.  There are four main sections of the editor.  The top line shows the program version, the current filename being edited, and whether or not the file has been modified.  Next is the main editor window showing the file being edited.  The status line is the third line from the bottom and shows important messages.   Mark Set Mark Text Mark Unset Missing color name Missing key name Missing regex string Missing syntax name Modified Mouse support New Buffer New File Next File Next Line Next Page Next Word NextHstory Nn No No Replace No conversion from DOS/Mac format No current search pattern No matching bracket No more open file buffers Non-blank characters required Not a bracket Nothing in undo buffer! Nothing to re-do! Option		GNU long option		Meaning
 Option		Meaning
 Option "%s" requires an argument Option is not a valid multibyte string Prepend Prepend Selection to File Preserve XON (^Q) and XOFF (^S) keys Prev Line Prev Page Prev Word PrevHstory Print version information and exit Quoting string Read %lu line Read %lu lines Read %lu line (Converted from DOS and Mac format - Warning: No write permission) Read %lu lines (Converted from DOS and Mac format - Warning: No write permission) Read %lu line (Converted from DOS and Mac format) Read %lu lines (Converted from DOS and Mac format) Read %lu line (Converted from DOS format - Warning: No write permission) Read %lu lines (Converted from DOS format - Warning: No write permission) Read %lu line (Converted from DOS format) Read %lu lines (Converted from DOS format) Read %lu line (Converted from Mac format - Warning: No write permission) Read %lu lines (Converted from Mac format - Warning: No write permission) Read %lu line (Converted from Mac format) Read %lu lines (Converted from Mac format) Read File Reading File Reading from stdin, ^C to abort
 Recall the next search/replace string Recall the previous search/replace string Received SIGHUP or SIGTERM
 Redid action (%s) Redo Redo the last undone operation Refresh Refresh (redraw) the current screen Regex strings must begin and end with a " character Regexp Replace Replace a string or a regular expression Replace this instance? Replace with Replaced %lu occurrence Replaced %lu occurrences Requested fill size "%s" is invalid Requested tab size "%s" is invalid Restricted mode Reverse the direction of the search Save backups of existing files Save file under DIFFERENT NAME ?  Save modified buffer (ANSWERING "No" WILL DESTROY CHANGES) ?  Scroll Down Scroll Up Scroll by line instead of half-screen Scroll down one line without scrolling the cursor Scroll up one line without scrolling the cursor Search Search Command Help Text

 Enter the words or characters you would like to search for, and then press Enter.  If there is a match for the text you entered, the screen will be updated to the location of the nearest match for the search string.

 The previous search string will be shown in brackets after the search prompt.  Hitting Enter without entering any text will perform the previous search.   Search Wrapped Search for a string or a regular expression Set operating directory Set width of a tab to #cols columns Silently ignore startup issues like rc file errors Smart home key Smooth scrolling Special thanks to: Spell Check Help Text

 The spell checker checks the spelling of all text in the current file.  When an unknown word is encountered, it is highlighted and a replacement can be edited.  It will then prompt to replace every instance of the given misspelled word in the current file, or, if you have selected text with the mark, in the selected text.

 The following function keys are available in Spell Check mode:

 Spell checking failed: %s Spell checking failed: %s: %s Start at line LINE, column COLUMN Suspend Suspension Switch to the next file buffer Switch to the previous file buffer Switched to %s Syntax "%s" has no color commands Syntax definition to use for coloring Tab Thank you for using nano! The "default" syntax must take no extensions The "none" syntax is reserved The Free Software Foundation The bottom two lines show the most commonly used shortcuts in the editor.

 The notation for shortcuts is as follows: Control-key sequences are notated with a caret (^) symbol and can be entered either by using the Control (Ctrl) key or pressing the Escape (Esc) key twice.  Escape-key sequences are notated with the Meta (M-) symbol and can be entered using either the Esc, Alt, or Meta key depending on your keyboard setup.   The nano text editor This is the only occurrence To Files To Spell Toggle appending Toggle backing up of the original file Toggle prepending Toggle the case sensitivity of the search Toggle the use of DOS format Toggle the use of Mac format Toggle the use of a new buffer Toggle the use of regular expressions Too many backup files? Two single-column characters required Uncut from the cutbuffer into the current line Undid action (%s) Undo Undo the last operation Unicode Input Unindent Text Unindent the current line Unknown Command Usage: nano [OPTIONS] [[+LINE,COLUMN] FILE]...

 Use "fg" to return to nano.
 Use bold instead of reverse video text Use of one more line for editing Use one more line for editing Verbatim Input View View mode (read-only) Where Is WhereIs Next Whitespace display Window size is too small for nano...
 Word Count Write File Help Text

 Type the name that you wish to save the current file as and press Enter to save the file.

 If you have selected text with the mark, you will be prompted to save only the selected portion to a separate file.  To reduce the chance of overwriting the current file with just a portion of it, the current filename is not the default in this mode.

 The following function keys are available in Write File mode:

 Write Selection to File Write the current file to disk Wrote %lu line Wrote %lu lines XOFF ignored, mumble mumble XON ignored, mumble mumble Yes Yy and anyone else we forgot... disabled enable/disable enabled line %ld/%ld (%d%%), col %lu/%lu (%d%%), char %lu/%lu (%d%%) line break line join nano is out of memory! text add text cut text delete text insert text replace text uncut version Project-Id-Version: nano
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-25 16:41-0500
PO-Revision-Date: 2014-05-06 10:40+0000
Last-Translator: IvoGuerreiro <ivoguerreiro@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:13+0000
X-Generator: Launchpad (build 18115)
 
 Opções compiladas: 
Buffer não escrito no %s:%s
 
Buffer não escrito: %s
 
Buffer escrito no %s
 
Pressionar Enter para continuar com o arranque do nano.
  (para substituir)  (para substituir) na selecção  Correio Electrónico: nano@nano-editor.org	Sítio Web: http://www.nano-editor.org/  As teclas de função seguintes estão disponíveis no modo Navegador de Pesquisa:

  [Cópia de segurança]  [Retroceder]  [Sensível a Mais/Minus]  [Formato DOS]  [Formato Mac]  [Expressão regular] "%.*s%s" não encontrado "%s" é um ficheiro de dispositivo (device-file) "%s" é uma directoria "%s" não encontrada "start=" necessita de um correspondente "end=" %sPalavras: %lu Linhas: %ld Caracteres: %lu (dir) (mais) (dir. pai) +LINHA,COLUNA --backupdir=<directório> --fill=<#cols> --operatingdir=<directório> --quotestr=<cadeia_de_caracteres> --speller=<prog> --syntax=<cadeia_de_caracteres> --tabsize=<#cols> -C <dir> -Q <str> -T <#cols> -Y <str> -o <dir> -r <#cols> -s <prog> Aa Todos Pressionando <ESC> duas vezes ou digitando um número de três digitos de 000 a 255 irá inserir um caracter correspondente ao respectivo código ASCII. Os seguintes atalhos do teclado estão disponíveis na janela principal do editor. Teclas alternativas são mostradas entre parênteses:

 Anexar Adicionar Selecção ao Fim do Ficheiro O argumento '%s' tem um " não terminado Auto indentar Guardar ao sair, não perguntar Indentar automaticamente novas linhas Retroceder A cor de fundo "%s" não pode ser brilhante Tecla Backspace Ficheiro de Cópia Ficheiros de cópia Para trás Má string de citação %s:%s Expressão regular errada "%s":%s Beg of Par Trazido por: Texto de Ajuda do Navegador

 Insira o nome do directório que gostaria de navegar.

 Se a completação <TAB> foi desactivada, pode utilizar a tecla <TAB> para (tentar) completar automaticamente o nome do directório.

 As seguintes teclas de funções estão disponíveis no modo Navegador Ir para Directórios:

 Texto de ajuda do comando de Navegador de Pesquisa

 Introduza as palavras ou caracteres que deseja procurar e depois pressione Enter. Se existir um resultado para o texto que introduziu, o ecrã será actualizado para a localização do resultado mais próximo da procura.

 A cadeia de caracteres anterior será apresentada entre parentesis após a linha de procura. Ao carregar em Enter sem introduzir qualquer texto irá executar a pesquisa anterior.

 É possível desfazer Justificação Impossível mover-se fora de %s no modo restrito Não pode inserir um ficheiro externo a %s Impossível de mover um directório Impossível escrever fora de %s Cancelar Cancelar a função actual Cancelado Não posso adicionar um comando de cor sem um comando de sintaxe Impossível adicionar um cabeçalho de regex sem um comando de sintaxe Sens. a Maius/Minus Fechar Fechar o buffer actual / Sair do nano Cor "%s" não compreendida.
Cores válidas são "green", "red", "blue",
"white", "yellow", "cyan", "magenta" e
"black", com o prefixo opcional "bright"
para cores de foreground. Realçamento sintáctico colorido Comando "%s" não é permitido no ficheiro incluído Comando "%s" não entendido Comando a executar [de %s]  Comando a executar num novo buffer [de %s]  Apresentação constante da posição do cursor Mostrar constantemente a posição do cursor Conversão de tipos tabulados para espaços Converter tabs em espaços Copiar Texto Copiar a linha actual e guarda-la no buffer de corte Impossível criar o duto Não pode suster Impossível avaliar o tamanho do buffer do duto Desculpe, não foi possível reabrir o stdin a partir do teclado
 Contar o número de palavras, linhas e caracteres Criando a lista de palavras com erros ortográficos, por favor espere... Pos Cur Cortar Texto Recortar do cursor até ao fim de linha Cortar desde posição do cursor até ao fim do ficheiro Cortar a linha actual e arquivá-la na memória de cortes Cortar para o fim RecortarAtéAoFim DIR: Formato DOS Apagar Apagar o caracter à esquerda do cursor Apagar o caracter sob o cursor Detectar com maior precisão os limites das palavras Directório para gravar ficheiros de cópias de segurança Mostrar a posição do cursor Exibir este texto de ajuda Fazer uma limpeza rápida da linha de estado Não adicionar newlines no fím dos ficheiros Não converter ficheiros no formato DOS/Mac Não visualizar o ficheiro nanorc Não mostar as duas linhas de ajuda Editar uma substituição Activar um verificador ortográfico alternativo Activar tecla <HOME> inteligente Activar line wrapping suave Permitir suspensão Permitir uso do rato Fim Fim de Par Enter Introduzir número da linha, número da coluna Erro no %s na linha %lu:  Erro ao invocar "%s" Erro ao invocar "sort -f" Erro ao invocar "spell" Erro ao invocar "uniq" Erro ao ler %s: %s Erro ao escrever %s: %s Erro ao escrever ficheiro de backup %s: %s Erro ao escrever no ficheiro temporário: %s Executar Comando Texto de ajuda do comando Executar

 Este modo permite-lhe inserir o resultado de um comando executado na shell no buffer actual (ou num buffer novo no modo de buffers de ficheiro múltiplos). Se precisar de um novo buffer vazio, não introduza nenhum comando.

 As seguintes teclas de função estão disponíveis no modo de Comando Executar:

 Executar um comando externo Sair Sair do nano Sair do navegador de ficheiro Texto de Ajuda do Navegador de Ficheiros

 O Navegador de ficheiros é utilizado para procurar visualmente a estrutura do directório onde seleccionar um ficheiro para leitura ou gravação. Pode utilizar as teclas de setas ou <Page Up/Down> para navegar atravéss dos ficheiros, e S ou <Enter> para escolher um ficheiro,  ou entrar num directório, selecionado.  Para subir um nível,  seleccione o directório chamado ".." no topo da lista de ficheiros.

 As seguintes teclas de funções estão disponí­veis no Navegador de Ficheiros:

 Nome do Ficheiro a Adicionar no Fim Nome do Ficheiro a Adicionar no Início Nome do Ficheiro para Escrita O ficheiro já existe. SUBSTITUIR?  Ficheiro a inserir [de %s]  Ficheiro a inserir num novo buffer [de %s]  O ficheiro foi modificado desde que o abriu, continuar com a gravação ?  Ficheiro: Verificação ortográfica concluída Primeiro Ficheiro Pirmeira Linha Corrigir o problema da confusão do Backspace/Delete Resolver o problema de confusão do teclado numérico Para o ncurses Avançar JustificarTudo Obter Ajuda Ir Para Directoria Ir Para a Directoria Ir Para a Linha Texto de Ajuda do Ir para Linha

 Introduza o número da linha para onde quer ir e pressione <Enter>. Se existirem menos linhas de texto do que o número que introduziu, será enviado para a última linha do ficheiro.

 As seguintes teclas de funções estão disponíveis no modo Ir para Linha:

 Ir Para o Texto Recuar um caractere Recuar uma palavra Avançar um caractere Avançar uma palavra Ir exactamente após o fim do parágrafo; depois, exactamente após o fim do próximo parágrafo Ir para o início da linha actual Ir para o início do parágrafo; depois para o início do parágrafo anterior Ir para o directório Ir para o fim da linha actual Ir para navegador de ficheiro Ir para o número de linha e coluna Ir para a linha seguinte Ir para a linha anterior Ir para o primeiro ficheiro da lista Ir para a primeira linha do ficheiro Ir para o último ficheiro da lista Ir para a última linha do ficheiro Ir para o parentesis correspondente Ir para o próximo ficheiro na lista Ir para o ficheiro anterior na lista Modo de Ajuda Local Não consigo encontrar a tua pasta pessoal! Se seleccionou texto com a marcação e depois efectua uma pesquisa para substituição, apenas as correspondências no texto seleccionado serão substituídas.

 As seguintes teclas de funções estão disponíveis no modo de Pesquisa:

 Se precisar de um buffer em branco, não insira nenhum nome de ficheiro, ou digite um nome de ficheiro inexistente no prompt e pressione <Enter>.

 As seguintes teclas de funções estão disponíveis no modo Inserir Ficheiro:

 Em Selecção:   Indentar texto Indentar a linha actual Texto de Ajuda do Inserir Ficheiro

 Digite o nome do ficheiro que será inserido no buffer do ficheiro actual na posição actual do cursor.

 Se compilou o nano com suporte para buffers múltiplos, com buffers múltiplos activados com o -F o comando --multibuffer, a tecla de alternância Meta-F, ou um ficheiro nanorc, ao inserir um ficheiro este será carregado num buffer separado(utilize Meta-< e > para alternar entre os buffers).   Inserir uma nova linha na posição do cursor Inserir uma tab na posição do cursor Inserir outro ficheiro no actual Inserir a próxima tecla textualmente Erro interno: tipo desconhecido. Por favor guarde o seu trabalho. Número de linha ou coluna inválido Invocar o corrector ortográfico, se disponível Justificar Justificar o parágrafo actual Justificar o ficheiro inteiro Tecla inválida no modo não-multibuffer Último Ficheiro Última Linha Log & ler pesquisa/substituição string histórico Formato Mac Texto de ajuda principal do nano

 O editor nano foi desenhado para emular a funcionalidade e facilidade de uso do editor de texto UW Pico. Existem quatro secções principais do editor. A linha superior mostra a versão do programa, o nome do ficheiro a ser actualmente editado e se o ficheiro foi ou não modificado. A seguir vem a janela principal de edição, mostrando o ficheiro que está a ser editado. A linha de estado é a terceira linha a contar do fundo e mostra mensagens importantes.   Marcação Feita Marcar texto Desmarcar Nome de cor em falta Falta o nome da tecla Expressão regular em falta Falta o nome da sintaxe Modificado Suporte para Rato Novo Buffer Novo Ficheiro Ficheiro Seguinte Linha Seguinte Página Seguinte Palavra Seguinte HistSeguin Nn Não Sem Substituição Sem conversão para formato DOS/Mac Nenhum padrão de procura actualmente Sem chaveta correspondente Não há mais buffers de ficheiro abertos É necessário caracteres sem ser em branco Não é uma chaveta Nada no buffer de retrocesso! Nada para re-fazer! Opção		Opção longa GNU		Significado
 Opção		Significado
 Opção "%s" requer um argumento A opção não é uma cadeia de caracteres multibyte válida Prefixar Adicionar Selecção ao Início do Ficheiro Preservar as teclas XON (^Q) e XOFF (^S) Linha Anterior Página Anterior Palavra Anterior HistAnter Mostrar informação da versão e sair Citando String %lu linha lida %lu linhas lidas Lida %lu linha (Convertida dos formatos DOS e MAC - Aviso: Sem permissão de escrita) Lidas %lu linhas (Convertidas dos formatos DOS e MAC - Aviso: Sem permissão de escrita) %lu linha lida (Convertida dos formatos DOS e Mac) %lu linhas lidas (Convertidas dos formatos DOS e Mac) Lida %lu linha (Convertida do formato DOS - Aviso: Sem permissão de escrita) Lidas %lu linhas (Convertidas do formato DOS - Aviso: Sem permissão de escrita) %lu linha lida (Convertida do formato DOS) %lu linhas lidas (Convertidas do formato DOS) Lida %lu linha (Convertida do formato MAC - Aviso: Sem permissão de escrita) Lidas %lu linhas (Convertidas do formato MAC - Aviso: Sem permissão de escrita) %lu linha lida (Convertidas do formato Mac) %lu linhas lidas (Convertidas do formato Mac) Ler Ficheiro A Ler Ficheiro A ler de stdin, ^C para abortar
 Recuperar a próxima cadeia de caracteres de pesquisa/substituição Recuperar a última cadeia de caracteres de pesquisa/substituição Foi recebido SIGHUP ou SIGTERM
 Refazer ação (%s) Refazer Refazer a última operação desfeita Actualizar Actualizar a tela actual Expressão regular deve começar e terminar com um caracter Expressão regular (Regexp) Substituir Substituir uma cadeia de caracteres ou expressão regular Substituir este termo? Substituir por Substituído %lu ocorrência Substituído %lu ocorrências O tamanho de enchimento "%s" pedido é inválido O tamanho de tab "%s" pedido é inválido Modo restrito Inverter a direção da procura Fazer Cópia de Segurança de Ficheiros existentes ao gravar Guardar o ficheiro com um NOME DIFERENTE?  Gravar o buffer modificado (RESPONDER "Não" Irá destruir modificações)?  Deslocar para Baixo Deslocar para Cima Deslizar por linha em vez de meio ecrã Descer uma linha sem descer o cursor Subir uma linha sem subir o cursor Pesquisa Texto de Ajuda do Comando Procura

 Introduza as palavras ou caracteres que gostaria de pesquisar, e depois pressione <Enter>. Se existir uma correspondência com o texto que você inseriu, a janela irá actualizar para a localização da correspondência mais próxima.

 A string da pesquisa irá aparecer entre parênteses depois da promt. Se pressionar <Enter> sem inserir nenhum texto irá efectuar a pesquisa anteior.   Pesquisa Ajustada Procurar por uma cadeia de caracteres ou expressão regular Definir o directório de trabalho Definir a largura de uma tab para #cols colunas Ignorar silenciosamente problemas de arranque como erros em ficheiros rc Tecla Home inteligente Deslocamento suave Especial obrigado para: Texto de ajuda do Corrector Ortográfico

 O corrector ortográfico verifica a ortografia de todo o texto no ficheiro actual. Quando é encontrada uma palavra desconhecida, ela é realçada e pode ser editada uma substituição. Será, então, interrogado se deseja substituir todas as ocorrências dessa palavra no ficheiro actual ou, se selecionou texto com o marcador, no texto selecionado.

 As seguintes teclas de função estão disponíveis no modo de Correção Ortográfica:

 A verificação de ortografia falhou: %s Falha na verificação ortográfica: %s:%s Começar na linha LINHA, coluna COLUNA Suspender Suspensão Mudar para a memória de ficheiro seguinte Mudar para a memória de ficheiro anterior Alternado para %s A sintaxe "%s" não tem comandos de cor Definições de sintaxe a usar para colorir Tabulação Obrigado por usar o nano! A sintaxe "default" não pode levar extensões A sintaxe "none" é reservada The Free Software Foundation As duas últimas linhas mostram os atalhos de comando normalmente mais utilizados no editor.

 A notação para atalhos é a seguinte: Sequências com a tecla Control são anotadas com um acento circunflexo (^) e podem ser introduzidas quer pela utilização da tecla Control (Ctrl), quer pressionando a tecla Escape (Esc) duas vezes. Sequências com a tecla Escape são anotadas com o simbolo Meta (M-) e podem ser introduzidas pressionando a tecla Esc, Alt ou Meta, dependendo da configuração do seu teclado.   O editor de texto nano Esta é a única ocorrência Para os Ficheiros Ortografia Alternar inserção Alternar a criação de cópia de segurança do ficheiro original Alternar adição no início Alternar a diferença de maiúsculas e minúsculas na procura Alternar o uso do formato DOS Alternar o uso do formato MAC Alternar o uso de um buffer novo Alternar o uso de expressões regulares Demasiadas copias de segurança? É necessário dois caracteres de controlo Mover da memória de cortes para a linha actual Desfazer ação (%s) Desfazer Desfazer a última operação Entrada Unicode Desindentar texto Desindentar a linha actual Comando Desconhecido Utilização: nano [OPCOES] [[+LINHA,COLUNA] FICHEIRO]...

 Use "fg" para voltar ao nano.
 Utilizar texto a negrito em vez de video invertido Uso de uma ou mais linhas para edição Usar mais uma linha para editar Entrada Literal Ver Modo de visualização (apenas-leitura) Onde está Próximo WhereIs Apresentação dos espaços brancos Tamanho da janela muito pequeno para o nano...
 Contador de Palavras texto de Ajuda do Escrever Ficheiro

 Digite o nome pelo qual gostaria de gravar o ficheiro actual e pressione <Enter> para gravá-lo.

 Se seleccionou texto com a marcação, será questionado para gravar somente a porção seleccionada para um ficheiro separado. Para reduzir as hipóteses de sobrescrever o ficheiro actual por apenas a porção seleccionada, o nome do ficheiro actual é o nome padrão neste modo.

 As seguintes teclas estão disponíveis no modo Escrever Arquivo:

 Escrever Selecção para Ficheiro Escrever o ficheiro actual para o disco Escreveu %lu linha Escreveu %lu linhas XOFF ignorado, mumble mumble XON ignorado, mumble mumble Sim Yy e qualquer outro que nos esquecemos... desactivado activar/desactivar activado linha %ld/%ld (%d%%), col %lu/%lu (%d%%), car %lu/%lu (%d%%) quebra de linha juntar linha nano está sem memória! adicionar texto cortar texto apagar texto inserir texto substituir texto texto não cortado versão 