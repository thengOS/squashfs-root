��          �      l      �     �     �  
   �                          ,  	   =     G     N     S     a     o     ~  �   �  �   w     M     Y     e  �  l     D     S  
   Z     e     w     �     �     �     �     �  
   �     �     �     �     �      �        �  
   	     	                                       
                                                       	    %d min %d mins Cast Dimensions Director Directors Format Free Genre Genres More suggestions My videos Online Play Remote videos Search videos Show in Folder Size This is an Ubuntu search plugin that enables information from various video providers to be searched and displayed in the Dash underneath the Video header. If you do not wish to search these content sources, you can disable this search plugin. This is an Ubuntu search plugin that enables local videos to be searched and displayed in the Dash underneath the Video header. If you do not wish to search this content source, you can disable this search plugin. Uploaded by Uploaded on Videos Project-Id-Version: unity-lens-video
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-12 20:02+0000
PO-Revision-Date: 2014-09-23 10:18+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 19:08+0000
X-Generator: Launchpad (build 18115)
 %d min %d mins Elenco Dimensões Diretor Diretores Formatar Grátis Género Géneros Mais sugestões Os meus vídeos Online Reproduzir Vídeos remotos Procurar vídeos Mostrar na pasta Tamanho Este é um plugin de pesquisa do Ubuntu que disponibiliza a informação de vários distribuidores, para ser procurada e mostrada na Dash, sob o separador de Vídeo. Se não desejar a fazer a pesquisa de conteúdos nestas fontes, pode desativar este plugin. Este é um plugin de pesquisa do Ubuntu que permite que a informação de vídeos locais seja procurada e mostrada na Dash, sob o separador de Vídeo. Se não desejar a fazer a pesquisa de conteúdos nestas fontes, pode desativar este plugin. Enviado por Enviado em Vídeos 