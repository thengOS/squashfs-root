��         �  �  �!      �,  ;  �,     0  0   0  0   I0  2   z0  *   �0  -   �0  $   1  *   +1  2   V1  %   �1  ,   �1  &   �1  )   2  &   -2  ,   T2  #   �2  $   �2  !   �2  &   �2  (   3  <   <3  3   y3  !   �3  �   �3     �4     �4     �4  0   �4  P   5  /   \5  ]   �5  G   �5  &   26  !   Y6     {6     �6     �6     �6  
   �6  +   �6  +   �6     7  #   =7  #   a7     �7  0   �7  0   �7     8  0   $8  (   U8  (   ~8     �8      �8  '   �8  %   9  )   69     `9  '   �9  2   �9  <   �9     :     -:     K:     j:  7   �:     �:  0   �:  (   �:  %   &;     L;     i;     �;  ,   �;  ,   �;  ,   �;  '   )<  -   Q<      <  (   �<  (   �<     �<     =     .=     N=  �   P=  ^   �=     >>  W   B>    �>     �?     �?     �?      @  B   @  !   V@  0   x@  <   �@  D   �@  !   +A  P   MA  ]   �A  H   �A     EB     eB  $   �B  D   �B     �B  �   C     �C  	   �C  L   �C  >   �C     %D  !   BD  G   dD  (   �D  <   �D  @   E  2   SE  "   �E  B   �E  2   �E     F  0   &F  ]   WF  =   �F  I   �F     =G  Z   PG  o  �G  "   I  *   >I     iI  V   �I  Q   �I  4   0J  5   eJ  (   �J     �J     �J     K     !K     @K     \K     yK  )   �K  -   �K  $   �K     L  <   5L  =   rL     �L     �L     �L     M     0M     PM     mM     �M  '   �M     �M  8   �M  8   N  �   XN     DO     _O     lO  
   |O     �O     �O     �O     �O     �O  �   �O  &   �P     �P  (   �P  B   Q  $   DQ     iQ     �Q     �Q     �Q  :   �Q  \   R  =   vR  ?   �R  >   �R     3S     DS  &   XS     S     �S     �S  �   �S  >   TT  *   �T  L   �T  =   U  ;   IU  +   �U  H   �U     �U     V     V      V  
   &V  K   1V  c   }V  ?   �V     !W     -W     IW     XW     lW  �   �W  I  aX  G   �Y  U   �Y     IZ  -   PZ    ~Z     �[     �[     �[     �[     �[     �[     \  4    \  4   U\     �\  $   �\     �\     �\  )   �\  K   %]  l   q]  :   �]  D   ^  m   ^^     �^     �^     �^     _     &_     ?_     P_     `_  �   {_     `     `     !`  
   6`     A`     D`     V`     f`     o`     �`     �`  8   �`     �`  �   a     �a     �a     �a  +   �a  /   �a  /   -b     ]b  0   lb     �b     �b  ?   �b  T   c  .   Wc  )   �c  .   �c  N   �c  >   .d     md     �d  )   �d     �d     �d  -   �d     e  �   %e  "   f  #   ?f     cf     {f     �f  $   �f     �f  �   �f     ug  ,   �g     �g  7   �g  *   �g  +   $h     Ph     Vh     ]h     eh  H   }h  M   �h  >   i  D   Si  X   �i  >   �i     0j  �   Lj  �   �j  R   �k  �   l  <   �l  H   �l  <    m  �   ]m  �   �m  F   bn  /   �n  ^   �n  p   8o  h   �o  H   p  H   [p      �p  *   �p  v   �p  0  gq  x   �r  �   s  �   �s  ,   .t     [t     yt  �  �t  �   Bv  @   �v  @   :w  R   {w  1   �w  E    x  g   Fx  /   �x  t   �x  7   Sy  p   �y  ;   �y  <   8z  :   uz  �   �z  f   4{  U   �{     �{     |     *|  &   =|  �   d|  0   �|  1   }  �   I}  1   �}  2   ~     7~     <~  (   M~  (   v~  &   �~  t   �~  &   ;  (   b  /   �  A   �     �     �  3   '�     [�     a�     i�     ��     ��     ��     Ѐ     �     �     �  3   �  3   G�  /   {�     ��     Á  $   ف  B   ��     A�  �   a�  )   �     ,�  	   @�     J�  #   _�  #   ��     ��     ��     Ճ  H   ݃  +   &�  ?   R�  �   ��     �     8�  ;   O�  3   ��  /   ��  +   �  '   �  #   C�     g�     ��     ��     ��     ̆  #   І  b   �  ~   W�     և     ܇     �     �  W   ��     H�     e�     }�     ��  	   ��     ��  !   ��     ӈ     �  %   ��     $�     @�  !   E�     g�     |�     ��     ��  Y   ��  "   ��  )   !�  .   K�  .   z�     ��     ��  O   ��     �  
   
�     �     0�  $   K�     p�     ��     ��     ��     ��  +   ��     ы     ً     ݋     �     ��     ��  Q   �     Y�  9   `�     ��     ��     ��     ��  F   ��  #   ��     #�     '�  
   *�     5�     N�     V�  U   [�  )   ��  	   ۍ     �     �  *   �     �  �   #�     ��  5   ��     �  
   ��     �     �  S   %�     y�  )   ��     ��  @   ��     �     �  
   �     �     2�  B   9�     |�  L   ��  	   ͐     א     �  Q   �     ^�     c�  U   j�     ��  E   ő     �  k   �     �  "   ��     ��     ɒ  �  �  �  ��     (�  ]   B�  ]   ��  _   ��  [   ^�  a   ��  N   �  Z   k�  m   ƚ  X   4�  [   ��  N   �  S   8�  Q   ��  ^   ޜ  T   =�  H   ��  J   ۝  ^   &�  R   ��     ؞  l   X�     ş  �   �     ��     ؠ     �  C   �  L   U�  ;   ��  �   ޡ  m   h�  8   ֢  &   �  !   6�     X�     e�     w�     }�  9   ��  7   ��  '   ��  :    �  7   [�  -   ��  ;   ��  J   ��     H�  .   d�  -   ��  0   ��  (   �  &   �  7   B�  -   z�  2   ��  !   ۦ  4   ��  :   2�  F   m�     ��  %   ͧ  %   �     �  K   2�     ~�  5   ��  %   Ψ  1   ��  '   &�      N�     o�  -   ��  -   ��  -   �  )   �  .   B�      q�  *   ��  (   ��  $   �  %   �  $   1�     V�  �   X�  d   ��     Z�  ]   ^�  (  ��  /   �     �     5�     T�  L   h�  !   ��  0   ׮  @   �  U   I�  %   ��  n   ů  �   4�  [   ��  6   �  ,   N�  0   {�  ]   ��  '   
�  �   2�     �  	   ��  Y   ��  V   X�  &   ��  !   ֳ  H   ��  +   A�  J   m�  @   ��  ;   ��  &   5�  O   \�  G   ��     ��  N   ��  [   L�  W   ��  R    �     S�  w   f�    ޷  1   �  G   �  3   a�  v   ��  v   �  F   ��  G   ʻ  A   �  &   T�  &   {�  &   ��  &   ɼ  %   �  )   �  *   @�  ?   k�  C   ��  4   �  5   $�  Q   Z�  R   ��  +   ��  +   +�  +   W�  +   ��  *   ��  *   ڿ  -   �     3�  /   K�     {�  ;   ��  =   ��    �  %   )�     O�     ]�     y�     ��     ��     ��     ��     ��  >  ��  +   �     9�  ,   M�  @   z�  #   ��  !   ��  *   �  #   ,�      P�  L   q�  a   ��  A    �  G   b�  ;   ��     ��     ��  ,   	�     6�  
   <�     G�  �   \�  [   /�  6   ��  h   ��  D   +�  >   p�  7   ��  Z   ��     B�     `�     i�     ~�     ��  b   ��  �   ��  P   ��     ��  )   ��     	�     �     8�  �   Q�  e  -�  F   ��  Z   ��     5�  5   =�  &  s�     ��     ��     ��  )   ��  %    �     &�     B�  =   U�  =   ��  &   ��  '   ��      �     <�  3   Y�  M   ��  j   ��  K   F�  k   ��  u   ��     t�     ��  #   ��     ��     ��     �     �  %   (�  �   N�     �     
�     �     ;�     H�     M�     a�     �  #   ��  +   ��  #   ��  ?   �  3   K�  �   �     3�     ;�  	   >�  :   H�  >   ��  >   ��     �  /   �     D�     Z�  O   u�  n   ��  4   4�  0   i�  7   ��  h   ��  H   ;�     ��     ��  -   ��     ��     ��  D   �     W�    k�  5   ��  #   ��     ��  #   �     &�  )   E�     o�  �   w�     !�  3   ;�     o�  F   w�  A   ��  @    �     A�  	   I�     S�  #   \�  \   ��  [   ��  N   9�  [   ��  o   ��  N   T�  -   ��  �   ��  �   ^�  \   S�  �   ��  <   J�  S   ��  9   ��  �   �  �   ��  @   G�  4   ��  n   ��  {   ,�  �   ��  ^   ?�  ^   ��  %   ��  4   #�  �   X�  y  ��  �   T�  �   ��  �   ��  6   )�  &   `�  "   ��  �  ��  �   ��  D   c�  A   ��  p   ��  9   [�  f   ��  {   ��  >   x�  v   ��  :   .�  x   i�  S   ��  T   6�  8   ��  �   ��  y   f�  g   ��  !   H�  "   j�     ��  +   ��  }   ��  6   P�  7   ��  �   ��  ;   S�  ;   ��     ��     ��  .   ��  3   �  ,   C�  �   p�  1   ��  ,   .�  3   [�  M   ��     ��  #   ��  ;   �     R�     [�     h�  '   ��  )   ��     ��     ��     �     1�     N�  F   f�  H   ��  G   ��     >�     ^�  &   {�  F   ��  #   ��  �   �  4   ��     ��  
          %   7  %   ]     �  "   �     �  ^   �  3   ' H   [ �   �    .    L 9   b 1   � .   � )   � %   ' !   M    o    �    �     �    � #   � �    �   �            #    % a   1 !   �    �    �    � 	   �     (    !   1    S (   f +   �    � 0   �    �          	   $ x   . &   � 1   � 7     7   8    p    v V   { 	   �    �    �    	 '   %	    M	    a	    q	    v	    z	 @   �	    �	    �	    �	 	   �	    �	    �	 _   �	    _
 =   f
    �
    �
    �
    �
 L   �
 (       8    D    G    X 
   u    � s   � 4   � 	   .    8 	   = A   G    � 5  �    � 9   �        	        9 �   @    � =   �     C       \     a    � %   � 
   � Q   �     =    
   U    ` !   y u   �         D       b R   j    � �   �    d +   t    �     �    x   �      �      �   z      �  �      �       
  �  �   �  4            t  �   S      �   =          \       �   �  �      �   �  �   V               -   �  a  p       �                      .  �   �       W  $       �        A  K  {    �  �  �     �  .   �   �  �  �  5   �  L    "  �      ^     z           �   J      e      �  +   ;  �      �  :     �  �   �   X   Z      �   6      �       P   �      �       �  �      �      *   ?   	  b   �           �       �  f     <         �      #  �   9   �                      �  �                      M   �       �         R   �  �   �                  �      �  �   �  =  o   S  �         E         !              %   6     �   �  �             �       \  �  �  0  �   �   y    �           �          �   `     �   �   Q              /          �  w       �               _      �   �   �   �   K       P     �  �  C                      2   �  q  �   h      #   �  �   �  9  '   �  �   �  )     �  �  ]    )  r  �  �         ~      X  �          �  �   p                R  !  �  �          2  3     N   �   �  7     �   �      �       �      �       r         �          �  �          �   �   T   �  �   _   L   �     �  �  i       1  �  �   �   �   F  
  ^       �          [      �   �   &   �   T  �   U  �  c   �  �  �      a   �     `   $  �  I   �  d  Q   �   �       >   Y  B  �  j   �          -      �   �  �          B               @     �  �   '  l  D              �     1   @  ,  �   k   I      �      7         	   0   �       n  �   �  �  }   �  u   /                    �  8     >  �  �           e   �   <  �      �  �       l   �          q   �  V  �      �  &      x  �   �  W   �  �   �   t   �   �       �            �   �       �   �  �  A   �  �   H  �   �      g   �       �    �       �          �      �   ?      b  �  �   i        �   �   G   �            �         D       �           �   N  3  �      O  J   8  �   *      %  �             w          v   +  �  �  {   n       
       �   [     k      �         Y   �       �           �   }      �   �  s   M  �   d   �      �  �             G     :      �   m   U   �       �        c  �  �           y   4  �   O   j              m        �   �      f   �   E  �  h     �      �      �   �   �  �    ]   (     �   �   �      �          �  v        �  �  s         �   Z       ~  |       ,   �  �              H   ;   C   �  g  �        �         o  �       u        �   �   �         (  �  F           �   5  "   �   �      �  	  �      �  �   �   �             |  �   �      �   

You found a bug in GNU Parted! Here's what you have to do:

Don't panic! The bug has most likely not affected any of your data.
Help us to fix this bug by doing the following:

Check whether the bug has already been fixed by checking
the last version of GNU Parted that you can find at:

	http://ftp.gnu.org/gnu/parted/

Please check this version prior to bug reporting.

If this has not been fixed yet or if you don't know how to check,
please visit the GNU Parted website:

	http://www.gnu.org/software/parted

for further information.

Your report should contain the version of this release (%s)
along with the error message below, the output of

	parted DEVICE unit co print unit s print

and the following history of commands you entered.
Also include any additional information about your setup you
consider important.
 
Command History:
 
Error: A general SIGFPE signal was encountered. 
Error: A general SIGILL signal was encountered. 
Error: A general SIGSEGV signal was encountered.
 
Error: FPE_FLTDIV (Float: divide by zero) 
Error: FPE_FLTINV (Float: invalid operation) 
Error: FPE_FLTOVF (Float: overflow) 
Error: FPE_FLTRES (Float: inexact result) 
Error: FPE_FLTSUB (Float: subscript out of range) 
Error: FPE_FLTUND (Float: underflow) 
Error: FPE_INTDIV (Integer: divide by zero) 
Error: FPE_INTOVF (Integer: overflow) 
Error: ILL_BADSTK (Internal Stack Error) 
Error: ILL_COPROC (Coprocessor Error) 
Error: ILL_ILLADR (Illegal addressing mode) 
Error: ILL_ILLOPC (Illegal Opcode) 
Error: ILL_ILLOPN (Illegal Operand) 
Error: ILL_ILLTRP (Illegal Trap) 
Error: ILL_PRVOPC (Privileged Opcode) 
Error: ILL_PRVREG (Privileged Register) 
Error: SEGV_ACCERR (Invalid permissions for mapped object)
 
Error: SEGV_MAPERR (Address not mapped to object)
 
Is this still acceptable to you? 
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

 
Report bugs to %s
 
Report bugs to <%s>.
 
Report bugs to: %s
 
When no DEVICE is given, probe all partitions.
   NUMBER    : display more detailed information about this particular partition
   devices   : display all active block devices
   free      : display information about free unpartitioned space on the current block device
   list, all : display the partition tables of all active block devices
 "%s" has invalid syntax for locations. %0.0send of file while reading %s %0.f%%	(time left %.2d:%.2d) %d aligned
 %d not aligned
 %s  %s %s  %s  %s %s : Bad checksum on block %llu of type %s
 %s : Bad checksum on block %llu of type %s. %s : Couldn't read block %llu
 %s : Couldn't read boot block %llu
 %s : Couldn't read root block %llu
 %s : Couldn't write block %d
 %s : Didn't find rdb block, should never happen
 %s : Didn't find rdb block, should never happen. %s : Failed to allocate block
 %s : Failed to allocate disk_specific rdb block
 %s : Failed to allocate id list element
 %s : Failed to allocate partition block
 %s : Failed to list bad blocks. %s : Failed to list boot blocks. %s : Failed to list file system blocks. %s : Failed to list partition blocks. %s : Failed to read partition block %llu
 %s : Loop detected at block %d. %s : The %s list seems bad at block %s. %s disk labels do not support extended partitions. %s disk labels don't support logical or extended partitions. %s during read on %s %s during seek for read on %s %s during seek for write on %s %s during write on %s %s has no extended partition (volume header partition). %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s is %dk, but it has %d clusters (%dk). %s is too small for a Mac disk label! %s trying to sync %s to disk %s%s argument '%s' too large %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognised disk label %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' 'mkpart' makes a partition without creating a new file system on the partition.  FS-TYPE may be specified to set an appropriate partition ID.
 'version' displays copyright and version information corresponding to this copy of GNU Parted
 (C) A %s %s partition was found at %s -> %s.  Do you want to add it to the partition table? A bug has been detected in GNU Parted.  Refer to the web site of parted http://www.gnu.org/software/parted/parted.html for more information of what could be useful for bug submitting!  Please email a bug report to %s containing at least the version (%s) and the following message:   A data set name is corrupted API version mismatch ATA over Ethernet Device ATARAID Controller An error occurred while looking for the mandatory bad blocks file. An extent has not been relocated. Assertion (%s) at %s:%d in function %s() failed. Attempt to write sectors %ld-%ld outside of partition on %s. BIOS cylinder,head,sector geometry: %d,%d,%d.  Each cylinder is %s.
 Backtrace has %d calls on stack:
 Bad FAT: cluster %d is cross-linked for %s.  You should run dosfsck or scandisk. Bad FAT: cluster %d outside file system in chain for %s.  You should run dosfsck or scandisk. Bad FAT: unterminated chain for %s.  You should run dosfsck or scandisk. Bad block list header checksum. Bad blocks could not be read. Bad blocks list could not be loaded. Bad directory entry for %s: first cluster is the end of file marker. Bad journal checksum. Both the primary and backup GPT tables are corrupt.  Try making a fresh table, and using Parted's rescue feature to recover partitions. Bug COMMANDs: Can't add a logical partition to %s, because there is no extended partition. Can't add another partition -- the partition map is too small! Can't add another partition. Can't create any more partitions. Can't have a logical partition outside of the extended partition on %s. Can't have a partition outside the disk! Can't have a primary partition inside an extended partition. Can't have logical partitions outside of the extended partition. Can't have more than one extended partition on %s. Can't have overlapping partitions. Can't have the end before the start! (start sector=%jd length=%jd) Can't write to %s, because it is opened read-only. Cancel Cannot get unit size for special unit 'COMPACT'. Changing the name of a root or swap partition will prevent Linux from recognising it as such. Checksum is wrong, indicating the partition table is corrupt. Cluster start delta = %d, which is not a multiple of the cluster size %d. Compaq Smart Array Conflicting partition map entry sizes!  Entry 1 says it is %d, but entry %d says it is %d! Copyright (C) 1998 - 2006 Free Software Foundation, Inc.
This program is free software, covered by the GNU General Public License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

 Corrupted Sun disk label detected. Could not cache the file system in memory. Could not detect file system. Could not determine physical sector size for %s.
Using the logical sector size (%lld). Could not determine sector size for %s: %s.
Using the default sector size (%lld). Could not find sector %lli of HFS file with CNID %X. Could not find sector %lli of HFS+ file with CNID %X. Could not get identity of device %s - %s Could not read VTOC FMT1 DSCB. Could not read VTOC FMT4 DSCB. Could not read VTOC FMT5 DSCB. Could not read VTOC FMT7 DSCB. Could not read VTOC labels. Could not read volume label. Could not retrieve API version. Could not retrieve blocksize information. Could not retrieve disk geometry information. Could not retrieve disk information. Could not stat device %s - %s. Could not update the extent cache for HFS file with CNID %X. Could not update the extent cache for HFS+ file with CNID %X. Could not write VTOC FMT1 DSCB. Could not write VTOC FMT4 DSCB. Could not write VTOC FMT5 DSCB. Could not write VTOC FMT7 DSCB. Could not write VTOC FMT9 DSCB. Could not write VTOC labels. Could not write volume label. DAC960 RAID controller DEVICE is usually /dev/hda or /dev/sda
 Data relocation has failed. Data relocation left some data at the end of the volume. Data relocation left some data in the end of the volume. Device %s has multiple (%d) logical sectors per physical sector.
GNU Parted supports this EXPERIMENTALLY for some special disk label/file system combinations, e.g. GPT and ext2/3.
Please consult the web site for up-to-date information. Device verification failed Disk %s: %s
 Disk Flags: %s
 Disk Image Disk is in use End End? Error Error fsyncing/closing %s: %s Error informing the kernel about modifications to partition %s -- %s.  This means Linux won't know about any changes you made to %s until you reboot -- so you shouldn't mount it or use it in any way before rebooting. Error initialising SCSI device %s - %s Error opening %s: %s Error while writing the allocation file. Error while writing the compatibility part of the allocation file. Error writing to the root directory. Expecting a disk label type. Expecting a file system type. Expecting a partition number. Expecting a partition type. Extended partitions cannot be hidden on msdos disk labels. FAT %d media %x doesn't match the boot sector's media %x.  You should probably run scandisk. FAT boot sector says clusters are 0 sectors.  This is weird.  FAT boot sector says logical sector size is 0.  This is weird.  FAT boot sector says there are no FAT tables.  This is weird.  FLAG is one of:  FS-TYPE is one of:  Failed to write partition block at %d. Fatal Fatal error File system File system doesn't have expected sizes for Windows to like it.  Cluster size is %dk (%dk expected); number of clusters is %d (%d expected); size of FATs is %d sectors (%d expected). File system has an invalid cluster size for a FAT file system. File system has an invalid number of FATs. File system has an invalid number of reserved sectors for a FAT file system. File system has an invalid sector size for a FAT file system. File system has an invalid signature for a FAT file system. File system is FAT12, which is unsupported. File system is reporting the free space as %d clusters, not %d clusters. File system type? Fix Flag to Invert? Flags Free Space GNU Parted cannot resize this partition to this size.  We're working on it! GNU Parted was miscompiled: the FAT boot sector should be 512 bytes.  FAT support will be disabled. General help using GNU software: <http://www.gnu.org/gethelp/>
 Generic IDE Generic SD/MMC Storage Card I2O Controller IBM S390 DASD drive IBM iSeries Virtual DASD If you convert to FAT16, and MS Windows is installed on this partition, then you must re-install the MS Windows boot loader.  If you want to do this, you should consult the Parted manual (or your distribution's manual). If you convert to FAT32, and MS Windows is installed on this partition, then you must re-install the MS Windows boot loader.  If you want to do this, you should consult the Parted manual (or your distribution's manual).  Also, converting to FAT32 will make the file system unreadable by MS DOS, MS Windows 95a, and MS Windows NT. If you leave your file system as FAT16, then you will have no problems. If you leave your file system as FAT32, then you will not introduce any new problems. Ignore Incorrect magic values in the journal header. Inform the operating system about partition table changes.

  -d, --dry-run    do not actually inform the operating system
  -s, --summary    print a summary of contents
  -h, --help       display this help and exit
  -v, --version    output version information and exit
 Information Invalid VTOC. Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid number. Invalid partition table - recursive partition on %s. Invalid partition table on %s -- wrong signature %x. Invalid partition table on %s. Invalid preceding regular expression Invalid range end Invalid regular expression Invalid signature %x for Mac disk labels. Invalid size of a transaction block while replaying the journal (%i bytes). It seems there is an error in the HFS wrapper: the bad blocks file doesn't contain the embedded HFS+ volume. Journal offset or size is not multiple of the sector size. Journal size mismatch between journal info block and journal header. Journal stored outside of the volume are not supported.  Try to desactivate the journal and run Parted again. LABEL-TYPE is one of:  Linux Software RAID Array Linux device-mapper (%s) Loopback device Memory allocation failed Memory exhausted Model: %s (%s)
 NAME is any word you want
 NUMBER is the partition number used by Linux.  On MS-DOS disk labels, the primary partitions number from 1 to 4, logical partitions from 5 onwards.
 Name New device? New disk label type? New state? No No Implementation No device found No match No previous regular expression No room for partition info. No room for volume label. No valid HFS[+X] signature has been found while opening. No valid partition map found. Not all of the space available to %s appears to be used, you can fix the GPT to use all of the space (an extra %llu blocks) or continue with the current setting?  Number OK OPTIONs: Only logical partitions can be a boot file. Only primary partitions can be root partitions. Only primary partitions can be swap partitions. Out of memory. PART-TYPE is one of: primary, logical, extended
 Packaged by %s
 Packaged by %s (%s)
 Parted can't resize partitions managed by Windows Dynamic Disk. Parted can't use HFS file systems on disks with a sector size not equal to %d bytes. Partition %d has an invalid length of 0 bytes! Partition %d has an invalid signature %x. Partition %d is %s, but the file system is %s. Partition %d isn't aligned to cylinder boundaries.  This is still unsupported. Partition %s is being used. Are you sure you want to continue? Partition Table: %s
 Partition doesn't exist. Partition map has no partition map entry! Partition name? Partition number? Partition too big/small for a %s file system. Partition type? Partition(s) %s on %s have been written, but we have been unable to inform the kernel of the change, probably because it/they are in use.  As a result, the old partition(s) will remain in use.  You should reboot now before making further changes. Partition(s) on %s are being used. Premature end of regular expression Promise SX8 SATA Device Regular expression too big Report %s bugs to: %s
 Resizing the HFS+ volume has failed. Retry START and END are disk locations, such as 4GB or 10%.  Negative values count from the end of the disk.  For example, -1s specifies exactly the last sector.
 STATE is one of: on, off
 Sector size (logical/physical): %lldB/%lldB
 Size Some header fields are not multiple of the sector size. Sorry, HFS cannot be resized that way yet. Sorry, HFS+ cannot be resized that way yet. Start Start? Success Sun disk label is full. Support for adding partitions to AIX disk labels is not implemented yet. Support for duplicating partitions in AIX disk labels is not implemented yet. Support for reading AIX disk labels is is not implemented yet. Support for setting flags in AIX disk labels is not implemented yet. Support for setting system type of partitions in AIX disk labels is not implemented yet. Support for writing AIX disk labels is is not implemented yet. Syntax error in config file The FATs don't match.  If you don't know what this means, then select cancel, run scandisk on the file system, and then come back. The Whole Disk partition is the only available one left.  Generally, it is not a good idea to overwrite this partition with a real one.  Solaris may not be able to boot without it, and SILO (the sparc boot loader) appreciates it as well. The backup GPT table is corrupt, but the primary appears OK, so that will be used. The backup GPT table is not at the end of the disk, as it should be.  Fix, by moving the backup to the end (and removing the old backup)? The boot region doesn't start at the start of the partition. The current API version '%d' doesn't match dasd driver API version '%d'! The data region doesn't start at the start of the partition. The device %s is so small that it cannot possibly store a file system or partition table.  Perhaps you selected the wrong device? The disk CHS geometry (%d,%d,%d) reported by the operating system does not match the geometry stored on the disk label (%d,%d,%d). The disk has %d cylinders, which is greater than the maximum of 65536. The disk label describes a disk bigger than %s. The driver descriptor says the physical block size is %d bytes, but Linux says it is %d bytes. The existing disk label on %s will be destroyed and all data on this disk will be lost. Do you want to continue? The file %s is marked as a system file.  This means moving it could cause some programs to stop working. The file system can only be resized to this size by converting to FAT16. The file system can only be resized to this size by converting to FAT32. The file system contains errors. The file system is bigger than its volume! The file system's CHS geometry is (%d, %d, %d), which is invalid.  The partition table's CHS geometry is (%d, %d, %d). The file system's CHS geometry is (%d, %d, %d), which is invalid.  The partition table's CHS geometry is (%d, %d, %d).  If you select Ignore, the file system's CHS geometry will be left unchanged.  If you select Fix, the file system's CHS geometry will be set to match the partition table's CHS geometry. The format of the GPT partition table is version %x, which is newer than what Parted can recognise.  Please report this! The information sector has the wrong signature (%x).  Select cancel for now, and send in a bug report.  If you're desperate, it's probably safe to ignore. The journal is not empty.  Parted must replay the transactions before opening the file system.  This will modify the file system. The location %s is outside of the device %s. The maximum head value is %d. The maximum sector value is %d. The partition table cannot be re-read.  This means you need to reboot before mounting any modified partitions.  You also need to reinstall your boot loader before you reboot (which may require mounting modified partitions).  It is impossible do both things!  So you'll need to boot off a rescue disk, and reinstall your boot loader from the rescue disk.  Read section 4 of the Parted User documentation for more information. The partition table on %s cannot be re-read (%s).  This means the Hurd knows nothing about any modifications you made.  You should reboot your computer before doing anything with %s. The partition's boot region doesn't occupy the entire partition. The partition's data region doesn't occupy the entire partition. The primary GPT table is corrupt, but the backup appears OK, so that will be used. The resize command has been removed in parted 3.0 The resulting partition is not properly aligned for best performance. The sector size stored in the journal is not 512 bytes.  Parted only supports 512 bytes length sectors. The specified device is not a valid DASD device The volume header or the master directory block has changed while replaying the journal.  You should restart Parted. There are no possible configurations for this FAT type. There's not enough room in the root directory for all of the files.  Either cancel, or ignore to lose the files. This HFS volume has no catalog file.  This is very unusual! This HFS+ volume has no catalog file.  This is very unusual! This command does not make sense in non-interactive mode.
 This file system has a logical sector size of %d.  GNU Parted is known not to work properly with sector sizes other than 512 bytes. This is not a real %s check.  This is going to extract special low level files for debugging purposes. This libparted doesn't have write support for %s.  Perhaps it was compiled read-only. Too many primary partitions Too many primary partitions. Trailing backslash Try `%s --help' for more information.
 Trying to move an extent from block Ox%X to block Ox%X, but another one already exists at this position.  This should not happen! Trying to read HFS file with CNID %X behind EOF. Trying to read HFS+ file with CNID %X behind EOF. Trying to register an extent starting at block 0x%X, but another one already exists at this position.  You should check the file system! Trying to write HFS file with CNID %X behind EOF. Trying to write HFS+ file with CNID %X behind EOF. Type UNIT is one of:  Unable to allocate a bsd disklabel slot. Unable to allocate a dasd disklabel slot Unable to allocate a partition number. Unable to determine geometry of file/device %s.  You should not use Parted unless you REALLY know what you're doing! Unable to determine the dm type of %s. Unable to determine the size of %s (%s). Unable to determine the start and length of %s. Unable to open %s read-write (%s).  %s has been opened read-only. Unable to open %s. Unable to probe store. Unable to satisfy all constraints on the partition. Unit? Unknown Unknown disk flag, %d. Unknown file system type "%s". Unknown partition flag, %d. Unknown system error Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ Unrecognised new style linux swap signature '%10s'. Unrecognised old style linux swap signature '%10s'. Unrecognised swsusp linux swap signature '%9s'. Unsupported disk format Unsupported disk type Updating the HFS wrapper has failed. Usage: %s [-hlmsv] [-a<align>] [DEVICE [COMMAND [PARAMETERS]]...]
 Usage: %s [OPTION] [DEVICE]...
 Usage: parted [OPTION]... [DEVICE [COMMAND [PARAMETERS]...]...]
Apply COMMANDs with PARAMETERS to DEVICE.  If no COMMAND(s) are given, run in
interactive mode.
 Use a smaller unit instead of a value < 1 User-Mode Linux UBD Using %s
 Valid arguments are: Version %d of HFS+ isn't supported. Version %d of HFSX isn't supported. Virtio Block Device Volume label is corrupted Warning Weird block size on device descriptor: %d bytes is not divisible by 512. Weird!  There are 2 partitions map entries! Welcome to GNU Parted! Type 'help' to view a list of commands.
 Without arguments, 'print' displays the entire partition table. However with the following arguments it performs various other actions.
 Would you like to use FAT32? Written by %s and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, %s, and others.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
and %s.
 Written by %s, %s, %s,
%s, %s, %s, and %s.
 Written by %s, %s, %s,
%s, %s, and %s.
 Written by %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
and %s.
 Written by %s, %s, and %s.
 Written by %s.
 Xen Virtual Block Device Yes You may need to update /etc/fstab.
 You need %s of free disk space to shrink this partition to this size.  Currently, only %s is free. You should reinstall your boot loader before rebooting.  Read section 4 of the Parted User documentation for more information. ^[nN] ^[yY] ` align-check align-check TYPE N                        check partition N for TYPE(min|opt) alignment alignment for new partitions alignment type(min/opt) ambiguous argument %s for %s atvrecv bios_grub boot cannot create any more partitions checking for bad blocks cylinder_alignment desired alignment: minimum or optimal device is too small for GPT diag displays machine parseable output displays the version displays this help message esp extended failed to set dvh partition name to %s:
Only logical partitions (boot files) have a name. failed to translate partition name fat_table_alloc_cluster: no free clusters fat_table_get: cluster %ld outside file system fat_table_set: cluster %ld outside file system free help help [COMMAND]                           print general help, or help on COMMAND hidden hp-service invalid %s%s argument '%s' invalid argument %s for %s invalid suffix in %s%s argument '%s' invalid token: %s ioctl() error irst lba legacy_boot lists partition layout on all block devices logical lvm memory exhausted metadata minimal mklabel mklabel,mktable LABEL-TYPE               create a new disklabel (partition table) mkpart mkpart PART-TYPE [FS-TYPE] START END     make a partition mktable msftdata msftres name name NUMBER NAME                         name partition NUMBER as NAME never prompts for user intervention off on open error opening of device failed optimal palo partition length of %jd sectors exceeds the %s-partition-table-imposed maximum of %jd ped_device_new()  Unsupported device type pmbr_boot prep primary primary partition table array CRC mismatch print print [devices|free|list,all|NUMBER]     display the partition table, available devices, free space, all found partitions, or a particular partition quit quit                                     exit program raid read error reading from device failed rescue rescue START END                         rescue a lost partition near START and END resize resizing %s file systems is not supported rm rm NUMBER                                delete partition NUMBER root searching for file systems seek error seeking on device failed select select DEVICE                            choose the device to edit set set NUMBER FLAG STATE                    change the FLAG on partition NUMBER shrinking shrinking HFS wrapper shrinking embedded HFS+ volume starting sector number, %jd exceeds the %s-partition-table-imposed maximum of %jd swap toggle toggle [NUMBER [FLAG]]                   toggle the state of FLAG on partition NUMBER unit unit UNIT                                set the default unit to UNIT version version                                  display the version number and copyright information of GNU Parted write error writing HFS Master Directory Block writing HFS+ Volume Header writing to device failed Project-Id-Version: parted 1.4.20
Report-Msgid-Bugs-To: bug-parted@gnu.org
POT-Creation-Date: 2014-07-28 23:04-0400
PO-Revision-Date: 2016-02-12 02:19+0000
Last-Translator: Gonçalo Silva <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:04+0000
X-Generator: Launchpad (build 18115)
Language: pt
 

Você encontrou um erro (bug) no GNU Parted! Eis aqui o que deve fazer: 

Não entre em pânico! O bug provavelmente não afectou nenhum de seus dados.
Ajude-nos a corrigir esse bug fazendo o seguinte: 

Verifique se o bug já foi corrigido verificando a última versão do
GNU Parted que pode ser encontrada em:

	http://ftp.gnu.org/gnu/parted/

Por favor use esta versão antes de relatar o bug.

Se ele ainda não foi corrigido ou você não sabe como verificar isso,
por favor visite o sitio web do GNU Parted:

	http://www.gnu.org/software/parted

para informações mais detalhadas.

O seu relatório de erro deve conter a versão deste programa (%s)
junto com a mensagem de erro abaixo, que é a saída do comando

	parted DISPOSITIVO unit co print unit s print

e o histórico de comandos que você utilizou.
Também inclua qualquer informação adicional sobre suas configurações
que você considere importante.
 
Histórico de comandos:
 
Error: A general SIGFPE signal was encountered.
Erro : Foi encontrado um sinal geral SIGFPE. 
Error: A general SIGILL signal was encountered.
Erro : Foi encontrado um sinal geral SIGILL. 
Error: A general SIGSEGV signal was encountered.
Erro : Um sinal geral SIGSEGV foi encontrado
 
Error: FPE_FLTDIV (Float: divide by zero)
Erro : FPE_FLTDIV (Flutuante: divisão por zero) 
Error: FPE_FLTINV (Float: invalid operation)
Erro : FPE_FLTINV (Flutuante: operação inválida) 
Error: FPE_FLTOVF (Float: overflow)
Erro : FPE_FLTOVF (Flutuante: transbordo) 
Error: FPE_FLTRES (Float: inexact result)
Erro: FPE_FLTRES (Flutuante: resultado inexato) 
Error: FPE_FLTSUB (Float: subscript out of range)
Erro : FPE_FLTSUB (Flutuante: subscript fora do intervalo) 
Error: FPE_FLTUND (Float: underflow)
Erro : FPE_FLTUND (Flutuante: transbordo negativo) 
Error: FPE_INTDIV (Integer: divide by zero)
Erro : FPE_INTDIV (Inteiro: divisão por zero) 
Error: FPE_INTOVF (Integer: overflow)
Erro : FPE_INTOVF (Inteiro: transbordo) 
Error: ILL_BADSTK (Internal Stack Error)
Erro : ILL_BADSTK (Erro na Pilha Interna) 
Error: ILL_COPROC (Coprocessor Error)
Erro : ILL_COPROC (Erro do co-processador) 
Error: ILL_ILLADR (Illegal addressing mode)
Erro : ILL_ILLADR (Modo ilegal de endereçamento) 
Error: ILL_ILLOPC (Illegal Opcode)
Erro : ILL_ILLOPC (Código de operação ilegal) 
Error: ILL_ILLOPN (Illegal Operand)
Erro : ILL_ILLOPN (Operador Ilegal) 
Error: ILL_ILLTRP (Illegal Trap)	
Erro : ILL_ILLTRP (Interupção ilegal) 
Error: ILL_PRVOPC (Privileged Opcode)	
Erro : ILL_PRVOPC (Código de operação privilegiado) 
Error: ILL_PRVREG (Privileged Register)
Erro : ILL_PRVREG (Registro privilegiado) 
Error: SEGV_ACCERR (Invalid permissions for mapped object)
Erro : SEGV_ACCERR (Permissões inválidas para o objecto mapeado)
 
Error: SEGV_MAPERR (Address not mapped to object)
Erro : SEGV_MAPERR (Endereço não mapeado para objecto)
 
Isto é aceitável para si? 
Licença GPLv3+: GNU GPL versão 3 ou posterior <http://gnu.org/licenses/gpl.html>.
Isto é software livre: você é livre para modificá-lo e distribuí-lo.
Não tem GARANTIA, até ao limite garantido por lei.

 
Relatar erros (bugs) para: %s
 
Reportar erros a <%s>.
 
Relate os erros (bugs) em: %s
 
Quando não for dado um DISPOSITIVO, sondar todas as partições.
   NUMBER    : apresenta mais informações detalhadas sobre esta partição
   devices   : exibe todos os dispositivos de bloco activos
   free      : apresenta informações sobre o espaço livre não particionado 
                disponível no atual dispositivo de bloco
   list, all : apresenta as tabelas de partições de todos os dispositivos 
                de bloco activos
 "%s" possui uma sintaxe invália para as localizações. %0.0s fim do ficheiro enquanto lido %s %0.f%%	(tempo restante %.2d:%.2d) %d alinhado
 %d não alinhado
 %s %s %s %s %s %s : Soma de controlo inválida no bloco %llu do tipo %s
 %s: Soma de controlo com erro no bloco %llu do tipo %s. %s : Não foi possível ler bloco %llu
 %s : Não foi possível ler bloco de arranque (boot) %llu
 %s : Não foi possível ler bloco da raíz (root) %llu
 %s : Não foi possível escrever no bloco %d
 %s : Não foi encontrado o bloco rdb, nunca pode acontecer
 %s: Não foi possível encontrar o bloco rdb, o que nunca devia acontecer. %s : Falha ao alocar bloco
 %s : Falha ao alocar bloco rdb disk_especific
 %s : Falha ao alocar elemento da lista de id
 %s : Falhou a alocação de bloco de partição
 %s : Falha ao listar blocos danificados. %s : Falha ao listar os blocos de boot %s : Falha ao listar os blocos do sistema de ficheiros. %s : Falha ao listar os blocos de partição. %s : Falhou a leitura do bloco de partição %llu
 %s : Ciclo detectado no bloco %d. %s : A lista %s parece estar danificada no bloco %s. Rótulos de disco %s não suportam partições estendidas. Rótulos de disco %s não suportam partições lógicas ou estendidas. %s durante leitura em %s %s durante procura para leitura em %s %s durante procura para escrita em %s %s durante escrita em %s %s não tem nenhuma partição estendida (partição cabeçalho de volume). Página web de %s : <%s>
 %s sítio oficial: <http://www.gnu.org/software/%s/>
 %s é %dk, mas tem %d clusters (%dk). %s é muito pequeno para um rótulo de disco Mac! %s a tentar sincronizar %s para o disco %s%s argumento '%s' muito grande %s: opção inválida -- '%c'
 %s: opção '%c%s' não permite um argumento
 %s: opção '%s' é ambígua; possibilidades: %s: opção '--%s' não permite um argumento
 %s: a opção '--%s' requer um argumento
 %s: opção '-W %s' não permite um argumento
 %s: opção '-W %s' é ambígua
 %s: a opção '-W %s' requer um argumento
 %s: opção requer um argumento -- '%c'
 %s: rótulo de disco irreconhecível %s: a opção '%c%s' é desconhecida
 %s: opção não reconhecida '--%s'
 " 'mkpart' cria uma partição sem criar um novo sistema de ficheiros nela. O TIPO-FS pode ser especificado para definir uma ID apropriada para a partição.
 'version' exibe informações de copyright e de versão correspondentes a esta cópia do GNU Parted
 (C) Uma partição %s %s foi encontrada em %s -> %s.  Quer adicioná-la à tabela de partições? Um erro foi detectado no GNU Parted. Consulte o web site do parted http://www.gnu.org/software/parted/parted.html para obter mais informações do que poderia ser útil para submeter o erro! Por favor, envie um relatório do erro para%s contendo pelo menos a versão (%s) e a seguinte mensagem:   O nome de um conjunto de dados está corrompido Versão da API não corresponde Dispositivo ATA sobre Ethernet Controlador ATARAID Ocorreu um erro na procura pelo ficheiro obrigatório de blocos defeituosos. Uma extensão não foi realocada. Assertion (%s) at %s:%d in function %s() failed. Tentativa de escrever sectores %ld-%ld fora da partição em %s. Geometria na BIOS de cilindros, cabeças e sectores : %d,%d,%d. Cada cilindro é %s.
 Rastreador tem %d chamadas na pilha:
 FAT estragada: o cluster %d tem uma referência cruzada para %s. Você deveria correr o dosfsck ou o scandisk. FAT estragada: cluster %d fora do sistema de ficheiros na cadeia para %s. Você deveria utilizar os utilitários: dosfsck ou scandisk. FAT estragada: cadeia não terminada para %s. Você deveria correr o dosfsck ou o scandisk. Checksum do cabeçalho da lista de blocos defeituosos. Sectores danificados não poderam ser lidos. A lista de maus blocos não pôde ser carregada. Entrada de diretório inválida para %s: o primeiro cluster é o fim do marcador de ficheiro. Soma de controlo do journal incorrecta. Ambas as tabelas GPT (primária e cópia de segurança) estão corrompidas. Tente fazer uma nova tabela e, utilizando as funcionalidades de recuperação do Parted, recupere as partições. Bug COMANDOs: Não pode adicionar uma partição lógica em %s, pois não há uma partição estendida. Não é possível adicionar outra partição - o mapa de partições é muito pequeno! Não posso adicionar outra partição. Não pode criar mais partições. Não pode ter uma partição lógica fora da partição estendida em %s. Não pode ter uma partição fora do disco! Não pode ter uma partição primária dentro de uma partição estendida. Não pode ter partições lógicas fora da partição estendida. Não pode haver mais do que uma partição estendida em %s. Não pode ter partições sobrepostas. Não pode ter o final antes do início! (sector de início=%jd comprimento=%jd) Não posso gravar em %s, pois ela foi aberta como somente para leitura. Cancelar Não é possível obter o tamanho unitário para a unidade especial 'COMPACT'. Mudar o nome de uma partição root ou swap irá impedir o Linux de reconhecê-la como tal. A soma de controlo está errada, indicando que a tabela de partição está corrompida. O cluster inicia em delta = %d, que não é um múltiplo do tamanho de cluster %d. Compaq Smart Array Conflito nos tamanhos de entradas do mapa de partição! Entrada 1 informa que é %d, mas entrada %d informa que é %d! Copyright (C) 1998 - 2006 Free Software Foundation, Inc.
Este programa é software livre, coberto pela Licença Pública Geral (GPL) do GNU.
This is a non-official translation. Isto é uma tradução não oficial. Consulte a versão em inglês.

Este programa é distribuído na expectativa de ser útil,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
COMERCIALIZAÇÃO ou de ADEQUAÇÃO A UM FIM ESPECIFICO.
Consulte a GNU General Public License (Licença Pública Geral GNU) para 
obter mais detalhes.

 Foi detectado um rótulo de disco Sun corrompido. Não foi possível colocar na cache o sistema de ficheiros na memória. Não foi possível detectar o sistema de Ficheiros. Não foi possível determinar o tamanho do sector físico para %s.
Será utilizado o tamanho de sector lógico (%lld). Não foi possível determinar o tamanho do sector para %s: %s.
Será utilizado o tamanho de sector predefinido (%lld). Não foi possivel encontrar o sector %lli do ficheiro HFS com CNID %X. Não foi possivel encontrar o sector %lli do ficheiro HFS+ com CNID %X. Não foi possível obter a identificação do dispositivo %s - %s Não foi possível ler VTOC FMT1 DSCB. Não foi possível ler VTOC FMT4 DSCB. Não foi possível ler VTOC FMT5 DSCB. Não foi possível ler VTOC FMT7 DSCB. Não foi possível ler rótulos VTOC. Não foi possível ler rótulo do volume. Não foi possível obter a versão da API. Não foi possível obter informação sobre o tamanho do bloco. Não foi possível obter a informação sobre a geometria do disco. Não foi possível obter informação sobre o disco. Não foi possível inicializar o dispositivo %s - %s. Não foi possivel atualizar a extensão da cache para o ficheiro HFS com CNID %X. Não foi possivel atualizar a extensão da cache para o ficheiro HFS+ com CNID %X. Não foi possível escrever VTOC FMT1 DSCB. Não foi possível escrever VTOC FMT4 DSCB. Não foi possível escrever VTOC FMT5 DSCB. Não foi possível escrever VTOC FMT7 DSCB. Não é possível escrever VTOC FMT9 DSCB. Não foi possível escrever rótulos VTOC. Não foi possivel escrever rótulo do volume. Controlador RAID DAC960 DISPOSITIVO é usualmente /dev/hda ou /dev/sda
 Recolocação dos dados falhou! Deslocação de dados deixou alguns dados no fim do volume. Recolocação dos dados deixou alguns dados no fim do volume. O dispositivo %s tem múltiplos (%d) setores lógicos por cada setor físico.
O GNU Parted suporta isso EXPERIMENTALMENTE para algumas combinações de rótulo de disco/sistema de ficheiros, ex. GPT e ext2/3.
Por favor, consulte o sitio web para informação mais atualizada. Falha na verificação do dispositivo Disco %s: %s
 Sinalizadores do Disco: %s
 Imagem de Disco O disco está em uso Final Acabar? Erro Erro fsyncing/closing %s: %s Erro ao informar o kernel das modificações feitas à partição %s -- %s. Isto significa que o Ubuntu não sabe das modificações que fez a %s até que o computador seja reiniciado -- como tal, não deverá montar ou usar esta partição em qualquer circunstância antes de completar a reinicialização do sistema. Erro inicializando dispositivo SCSI %s - %s Erro abrindo %s: %s Erro na escrita do ficheiro de atribuição. Erro a escrever a parte compatível do ficheiro de atribuição. Erro escrevendo no diretório raiz. Tipo de rótulo de disco exigido. Esperando um tipo de ficheiros de sistema. Esperando um número de partição. Esperando um tipo de partição. Partições estendidas não podem ser escondidas em rótulos de disco MSDOS. FAT %d média %x não coincide com o sector de boot médio %x. Você deveria executar o scandisk. Sector de boot FAT diz que os clusters têm 0 setores. Estranho.  Sector de boot FAT diz que o tamanho lógico do sector é 0. Estranho.  Sector de boot FAT diz que não há tabelas FAT. Estranho.  FLAG é um de:  TIPO-FS é um de:  Falha ao escrever bloco de partição em %d. Fatal Erro fatal Sistema de ficheiros O sistema de ficheiros não tem o tamanho esperado para que o Windows o utilize. O tamanho do cluster é %dk (%dk esperado); número de clusters é %d (%d esperado); tamanho da FAT é %d sectores (%d esperado). O sistema de ficheiros tem um tamanho de sector inválido para um sistema de ficheiros FAT. Sistema de ficheiros tem um número inválido de FATs. O sistema de ficheiros tem um número de sectores reservados inválido para um sistema de ficheiros FAT. Sistema de ficheiros tem um tamanho de sector inválido para um FAT. Sistema de ficheiros tem uma assinatura inválida para um FAT. O sistema de ficheiros é FAT12, que não é suportado. O sistema de ficheiros reporta que o espaço livre é de %d clusters, não de %d clusters. Tipo do sistema de ficheiros? Corrigir Flag para inverter ? Sinalizadores Espaço Livre O GNU Parted não pode redimensionar esta partição para esse tamanho. Estamos a trabalhar nisso! O GNU Parted foi compilado com erros: o sector de arranque da FAT deveria ser de 512 bytes. O suporte para FAT será desabilitado. Ajuda geral sobre a utilização de software GNU: <http://www.gnu.org/gethelp/>
 IDE genérico Cartão de armazenamento SD/MMC genérico Controlador I20 Unidade de dados IBM S390 DASD IBM iSeries Virtual DASD Se você converter para FAT16, e o MS Windows estiver instalado nesta partição, deverá reinstalar o gestor de boot do MS Windows. Se quiser fazer isso, consulte o manual do Parted (ou o manual de sua distribuição). Se você converter para FAT32, e o MS Windows estiver instalado nesta partição, deverá reinstalar o gestor de boot do MS Windows. Se quiser fazer isso, consulte o manual do Parted (ou o manual de sua distribuição). Também, convertendo para FAT32 fará com que o sistema de ficheiros já não seja reconhecido pelo MS DOS, Windows 95a, e MS Windows NT. Se deixar seu sistema de ficheiros como FAT16, não haverá problemas. Se deixar seu sistema de ficheiros como FAT32, você não terá mais nenhum problema novo. Ignorar Valores mágicos incorretos no cabeçalho do journal. Informar  sobre as modificações à tabela de partições.

  -d, --dry-run    não informar de fato o sistema operativo
  -s, --summary    imprimir a um resumo dos conteúdos
  -h, --help       apresentar esta ajuda e sair
  -v, --version    apresentar a informações sobre a versão e sair
 Informação VTOC inválido. Referência anterior inválida Nome de classe de caracteres é inválido Caractere de agrupamento é inválido Conteúdo de \{\} inválido Número inválido. Tabela de partições inválida - partição recursiva em %s. Tabela de partições inválida em %s - assinatura errada %x. Tabela de partições inválida em %s. Expressão regular precedente inválida Fim de  intervalo inválido Expressão regular inválida Assinatura inválida %x para rótulos de disco Mac. Tamanho inválido do bloco de transação ao reproduzir o journal (%i bytes). Parece haver um erro no NFS wrapper: o ficheiro de blocos defeituosos não contém o volume HFS+ embutido. O deslocamento ou tamanho do journal não é múltiplo do tamanho do setor. Tamanho do journal sem correspondência entre o bloco de informação do journal e o respectivo cabeçalho. Os Journals armazenados fora do volume não são suportados. Tente desactivar o journal e execute o Parted novamente. TIPO_ROTULO é um de:  Matriz de Raid Software Linux Mapeador de dispositivos Linux (%s) Dispositivo em loopback Falha de alocação de memória Memória esgotada Modelo: %s (%s)
 NAME é qualquer palavra que desejar
 NUMERO é o numero da partição usada pelo Ubuntu. Nos rótulo de disco MSDOS, o número das partições primárias varia de 1 a 4, nas partições lógicas vai de 5 para a frente.
 Nome Novo dispositivo? Novo tipo de rótulo de disco? Novo estado? Não Sem Implementação Nenhum dispositivo encontrado Sem correspondência(s) Nenhuma expressão regular anterior Não há espaço para a info da partição. Sem espaço para rótulo do volume. Não foi encontrada uma assinatura HFS[+X] válida na abertura. Não foi encontrado um mapa de partições válido. Nem todo o espaço disponível para %s parece estar a ser usado. Prefere corrigir o GPT para usar todo o espaço (%llu blocos adicionais) ou continuar com a configuração atual?  Número OK OPÇÕES: Apenas as partições lógicas podem ser ficheiros 'boot'. Apenas as partições primárias podem ser partições 'root'. Apenas as partições primárias podem ser partições 'swap'. Não há memória. TIPO-PART é um de: primary, logical, extended
 Pacote criado por %s
 Pacote criado por %s (%s)
 O Parted não pode redimensionar partições geridas pelo Windows Dynamic Disk. O Parted não pode utilizar sistemas de arquivos HFS em discos com um tamanho de sector diferente de %d bytes. A partição %d tem um tamanho inválido de 0 bytes! A partição %d tem uma assinatura inválida %x. A partição %d é %s mas o sistema de ficheiros é %s. A partição %d não está alinhada com o limite do cilindro. De momento essa acção não é suportada. A partição %s está a ser utilizada. Tem a certeza que quer continuar? Tabela de Partições: %s
 A partição não existe. Mapa de partições não tem nenhuma entrada! Nome da partição? Número da partição? Partição demasiado grande/pequena para um sistema de ficheiros %s. Tipo de partição? A(s) partição(ões) %s em %s foram escritas mas não nos é possível informar o kernel sobre as alterações, provavelmente devido a ela(s) estar(em) em uso. Como resultado, a(s) antiga(s) partição(ões) continuaram em uso. Tem de reiniciar agora antes de fazer mais modificações. Partição(ões) em %s está(ão) a ser utilizada(s). Fim prematuro da expressão regular Dispositivo SATA Promise SX8 Expressão regular demasiado grande Relate %s erros (bugs) em: %s
 O redimensionamento do volume HFS+ falhou Repetir INICIO e FIM são locais em disco, tais como 4GB ou 10%. Valores negativos contam-se a partir do fim do disco. Por exemplo, -1s especifica exactamente o último sector.
 ESTADO é um de: on, off
 Tamanho de sector (lógico / físico): %lldB/%lldB
 Tamanho Alguns campos do cabeçalho não são múltiplos do tamanho do sector. Desculpe, o HFS ainda não pode ser redimensionada dessa maneira. Desculpe, HFS+ ainda não pode ser redimensionado dessa maneira. Início Começar? Successo O rótulo de disco Sun está cheio. O suporte para adicionar partições em rótulos de disco AIX ainda não está implementada. O suporte para duplicar partições em rótulos de disco AIX ainda não está implementada. O suporte para leitura de rótulos de disco AIX ainda não está implementado. O suporte para definição de flags em rótulos de disco AIX não está ainda implementado. O suporte para definir o tipo de sistema de partições em rótulos de disco AIX ainda não está implementada. O suporte para escrita de rótulos de disco AIX ainda não está implementado. Erro de sintaxe no ficheiro de configuração As FATs não estão iguais. Se não sabe o que isto significa, selecione cancelar, execute o scandisk no sistema de ficheiros, depois volte. A partição de Todo o Disco é a única que resta. Regra geral, não é boa ideia sobrescrever esta partição com uma verdadeira. O Solaris poderá não ser capaz de arrancar sem ela e o SILO (o gestor de arranque do Sparc) também a aprecia. A tabela GPT de backup está corrompida, mas a primária parece OK, esta ultima será usada. A tabela GPT cópia de segurança não é no final do disco, como deve ser. Arranje movendo o backup para o fim (e remover a velha cópia de segurança)? A região de arranque não começa no início da partição. A versão "%d" da API atual não coincide com a versão "%d" da API do driver DASD! A região de dados não começa no início da partição. O dispositivo %s é tão pequeno que não é possível armazenar um ficheiro de sistema ou uma tabela de partição. Talvez tenha selecionado o dispositivo errado? A geometria CHS do disco (%d,%d,%d) declarada pelo sistema operativo não corresponde à geometria armazenada no rótulo do disco (%d,%d,%d). O disco tem %d cilindros, o que é superior ao máximo de 65536. O rotulo do disco afirma que o disco é maior que %s O descritor do driver informa que o tamanho físico de bloco é %d bytes, mas o Linux informa que é %d bytes. O rótulo de disco existente em %s será destruído e todos os dados existentes no disco serão perdidos. Deseja continuar? O ficheiro %s está marcado como sendo um ficheiro de sistema. Isto significa que movê-lo poderá fazer com que alguns programas deixem de funcionar. O sistema de ficheiros só pode ser redimensionado para esse tamanho se convertido para FAT16. O sistema de ficheiros só pode ser redimensionado para esse tamanho se convertido para FAT32. O sistema de ficheiros contém erros. O sistema de ficheiros é maior do que o seu volume! A CHS geometria do sistema de arquivos é (%d, %d, %d) que é inválido. CHS A geometria da tabela de partição é (%d, %d, %d). A Geometria CHS do Sistema de Ficheiros é (%d, %d, %d), o que é inválido. A geometria CHS da tabela de partições é (%d, %d, %d). Se selecionar Ignore, a geometria CHS do sistema de ficheiros será deixada sem alterações. Se selecionar Corrigir, a geometria CHS do sistema de ficheiros será alterada, de modo a que coincida com a geometria CHS da tabela de partições. O formato de tabela de partição de GPT é a versão%x, que é mais recente do que a do Parted pode reconhecer. Por favor, reporte isto! O setor de informações tem uma assinatura inválida (%x). Selecione cancelar agora, e mande um relatório de bug. Se você está desesperado, é provável que seja seguro ignorar. O journal não está vazio. O Parted deve repetir as transações antes de abrir o sistema de ficheiros. Isto irá modificar o sistema de ficheiros. A localização %s encontra-se fora do dispositivo %s. O máximo valor do cabeçalho é de %d O valor máximo do sector é de %d A tabela de partição não pôde ser relida. Isto significa que tem de reiniciar o computador antes de montar qualquer das partições modificadas. Tem também de reinstalar o gestor de arranque (ex. grub-install) antes de reiniciar (o que pode requerer a montagem das partições modificadas). É impossível fazer as duas coisas! Sendo assim, tem de reiniciar a partir de um CD Live. Para mais informações, leia a secção 4 da documentação do utilizador do Parted. A tabela de partições em %s não pôde ser lida (%s). Isto significa que o Hurd não sabe das modificações que fez até que o computador seja reiniciado. Deverá reinicilizar o sistema antes de fazer qualquer uso de %s. A região de arranque da partição não ocupa a partição inteira. A região de dados da partição não ocupa a partição inteira. A tabela GPT primária está corrompida mas a cópia de segurança aparenta estar OK, por isso esta será usada. O comando de redimensionamento foi removido no parted 3.0 A partição daqui resultante não ficará alinhada convenientemente para 
obter o melhor desempenho. O tamanho de sector armazenado no journal não é 512 bytes. O Parted suporta apenas sectores com 512 bytes de comprimento. O dispositivo especificado não é um dispositivo DASD válido O cabeçalho do volume no bloco de directório mestre foi modificado ao reproduzir o journal. Deve reiniciar o Parted. Não há configurações possíveis para este tipo de FAT. Não há espaço suficiente no directório raiz para todos os ficheiros. Ou cancele, ou ignore para perder os ficheiros. Este volume HFS não possui qualquer ficheiro de catálogo. Isto é muito invulgar! Este volume HFS+ não possui qualquer ficheiro de catálogo. Isto é muito invulgar! Este comando não faz sentido no modo não-interactivo.
 Este sistema de ficheiros tem um tamanho de sector lógico de %d. O GNU Parted é conhecido por não funcionar corretamente com sectores diferentes de 512 bytes. Isto não é verdadeiramente uma %s verificação. Isto irá extrair ficheiros especiais de baixo nivel para depuração. Esta libparted não possui suporte de escrita para %s. Talvez tenha sido compilada apenas para leitura. Demasiadas partições primárias Demasiadas partições primárias. Barra invertida no final Tente `%s --help' para mais informações.
 A tentar mover uma extensão do sector 0x%X para o sector 0x%X, mas já existe um nesta posição. Isto não devia acontecer! A tentar ler o ficheiro HFS com CNID %X atrás de EOF. A tentar ler o ficheiro HFS+ com CNID %X atrás de EOF. Tentando registar uma estendida começando no bloco 0x%X, mas outra já existe nessa posição. Você deveria verificar o seu sistema de ficheiros! A tentar escrever o ficheiro HFS com CNID %X atrás de EOF. A tentar escrever o ficheiro HFS+ com CNID %X atrás de EOF Tipo UNIT é um de:  Impossível localizar um rótulo de disco bsd. Impossível alocar um slot de rótulo de disco dasd Impossível alocar um número de partição. Impossível determinar a geometria do ficheiro/dispositivo %s. Não deve usar o Parted, a não ser que REALMENTE saiba o que está a fazer! Não foi possível determinar o tipo de dm de %s. Impossível determinar o tamanho de %s (%s). Incapaz de determinar o início e comprimento de %s Não posso abrir %s para gravação (%s). %s foi aberto somente para leitura. Incapaz de abrir %s. Impossível sondar o armazenamento. Impossível satisfazer todas as restrições na partição. Unidade? Desconhecido Flag de disco desconhecida, %d. Sistema de ficheiros desconhecido "%s". A flag da partição é desconhecida, %d. Erro desconhecido de sistema ( ou \( sem correspondência ) ou \) sem correspondência [ ou [^ sem correspondência \{ sem correspondência Não é reconhecida a assinatura '%10s' da swap Linux em formato novo. Não é reconhecida a assinatura '%10s' da swap Linux em formato antigo. Não é reconhecida a assinatura '%9s' do swap Linux em formato swsusp. Formato de disco não suportado Tipo de disco não suportado A atualização do HFS wrapper falhou. Uso: %s [-hlmsv] [-a<align>] [DISPOSITIVO [COMANDO [PARÂMETROS]]...]
 Uso: %s [OPÇÃO] [DISPOSITIVO]...
 Uso: parted [OPÇÕES]... [DISPOSITIVO [COMANDO [PARÂMETROS]...]...]
Aplica o COMANDO com os PARÂMETROS ao DISPOSITIVO. Se não for dado nenhum
COMANDO, executa em modo interactivo.
 Utilize uma unidade mais pequena em vez do valor < 1 User-Mode Linux UBD Usando %s
 Os argumentos válidos são: Versão %d do HFS+ não é suportada. Versão %d de HFSX não é suportada. Dispositivo de Bloco Virtio Rótulo do volume está corrompido Aviso tamanho de bloco estranho no descritor de dispositivo: %d bytes não são divisíveis por 512. Estranho! Existem 2 entradas de mapa de partição! Bem-vindo(a) ao GNU Parted! Digite 'help' para ver a lista de comandos.
 Sem argumentos, 'print' apresenta a tabela de partições inteira. Porém, com os seguintes argumentos realiza diversas outras acções.
 Você gostaria de usar FAT32? Escrito por %s e %s.
 Escrito por %s, %s, %s,
%s, %s, %s, %s,
%s, %s e outros.
 Escrito por %s, %s, %s,
%s, %s, %s, %s,
%s e %s.
 Escrito por %s, %s, %s,
%s, %s, %s, %s,
e %s.
 Escrito por %s, %s, %s,
%s, %s, %s e %s.
 Escrito por %s, %s, %s,
%s, %s e %s.
 Escrito por %s, %s, %s,
%s e %s.
 Escrito por %s, %s, %s
e %s.
 Escrito por %s, %s e %s.
 Escrito por %s.
 Dispositivo de Bloco Virtual Xen Sim Pode ter que atualizar /etc/fstab.
 Você necessita de %s de espaço livre para diminuir esta partição para este tamanho. Atualmente, apenas estão disponíveis %s. Deverá reinstalar seu gestor de boot antes de reinicializar. Leia a secção 4 da documentação do utilizador do Parted para mais informações. ^[nN] ^[sS] " align-check align-check TYPE N                        verificar partição N se tem alinhamento TYPE(min|opt) alinhamento das novas partições tipo de alinhamento (min/opt) argumento %s ambíguo para %s atvrecv bios_grub boot não é possível criar mais partições procurando por blocos danificados cylinder_alignment alinhamento desejado: mínimo ou óptimo o dispositivo é demasiado pequeno para GPT diag mostra a saída de dados parseável por máquina mostra a versão mostra esta mensagem de ajuda esp extendida falha ao definir o nome da partição dvh como %s:
Apenas as partições lógicas (ficheiros de boot) podem ter um nome. falha ao traduzir o nome da partição fat_table_alloc_cluster: não há clusters livres fat_table_get: cluster %ld fora do sistema de ficheiros fat_table_set: cluster %ld fora do sistema de ficheiros livre help help [COMANDO]                           exibe a ajuda geral, ou a ajuda sobre COMANDO escondida serviço hp inválido %s%s argumento '%s' argumento inválido %s para %s sufixo inválido no %s%s argumento '%s' token inválido: %s erro de ioctl() irst lba legacy_boot lista o layout das partições em todos os dispositivos de bloco lógica lvm memória esgotada metadados mínimo mklabel mklabel,mktable LABEL-TYPE               criar um novo rótulo de disco (tabela de partições) mkpart mkpart TIPO-PART [TIPO-FS] INICIO FIM    criar uma partição mktable msftdata msftres name name NUMERO NOME                         nomear a partição NUMERO com NOME nunca pede a intervenção do utilizador desactivado on erro de abertura falha ao abrir o dispositivo optimizado palo o tamanho da partição de %jd sectores excede o valor máximo imposto para a tabela de partição %s que é de %jd ped_device_new()  Tipo de dispositivo não suportado pmbr_boot prep primária o CRC do array da tabela da partição primária não corresponde print print [devices|free|list,all|NUMERO]     apresenta a tabela de partições, 
                                         dispositivos disponíveis, espaço livre,
                                         todas as partições encontradas ou uma
                                         partição em particular quit quit                                     sair do programa raid erro de leitura falha na leitura do dispositivo rescue rescue INICIO FIM                        recuperar uma partição perdida 
                                           perto de INICIO e do FIM resize redimensionamento de sistemas de arquivos%s não é suportado rm rm NUMERO                                apagar a partição NUMERO root procurando sistemas de ficheiros erro de busca falha ao fazer a busca no dispositivo selecionar selecione DISPOSITIVO                            seleciona o dispositivo a editar set definir NUMERO FLAG ESTADO altera a FLAG da partição NUMERO diminuindo A encolher o HFS wrapper A encolher o volume HFS+ embutido número de sector de inicio, %jd sectores excede o valor máximo imposto para a tabela de partição %s que é de %jd swap toggle definir [NUMERO [FLAG]] altera o estado da FLAG da partição NUMERO unidade unit UNIDADE                             define a unidade predefinida como UNIDADE version version                                   exibe o número da versão e informações
                                           sobre copyright do GNU Parted erro de escrita a escrever HFS Master Directory Block (MDB) a escrever o HFS+ Volume Header falha ao escrever no dispositivo 