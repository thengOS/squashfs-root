��    *      l  ;   �      �  p   �  #        >  )   [  	   �     �      �  ,   �  $   �           3     T     t  #   �  !   �     �     �  %        ,     K     f     }     �     �  &   �     �          (  �   ?     "     9     R     c  H   }  (   �  E  �  F   5
  -   |
     �
     �
     �
  �  �
  u   �  (     '   +  /   S     �  "   �  /   �  7   �  @        ^  '   |  +   �  *   �  8   �  (   4     ]  %   z  %   �  -   �     �          *     E     e  )   �  (   �     �     �  �        
     '     ?     Y  H   y  '   �  n  �  C   Y  ;   �     �     �                                       &             !      	          )             
                "                                  #                    *       (                       %       '            $      -Z,--context REGEXP kill only process(es) having context
                      (must precede other arguments)
 %*s USER        PID ACCESS COMMAND
 %s is empty (not mounted ?)
 %s: unknown signal; %s -l lists signals.
 (unknown) Bad regular expression: %s
 Can't get terminal capabilities
 Cannot allocate memory for matched proc: %s
 Cannot find socket's device number.
 Cannot find user %s
 Cannot open /proc directory: %s
 Cannot open /proc/net/unix: %s
 Cannot open a network socket.
 Cannot open protocol file "%s": %s
 Cannot resolve local port %s: %s
 Cannot stat %s: %s
 Cannot stat file %s: %s
 Copyright (C) 2007 Trent Waddington

 Could not kill process %d: %s
 Error attaching to pid %i
 Invalid namespace name Kill %s(%s%d) ? (y/N)  Kill process %d ? (y/N)  Killed %s(%s%d) with signal %d
 Namespace option requires an argument. No process specification given No processes found.
 No such user name: %s
 PSmisc comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it under
the terms of the GNU General Public License.
For more information about these matters, see the files named COPYING.
 Press return to close
 Signal %s(%s%d) ? (y/N)  TERM is not set
 Unknown local port AF %d
 Usage: killall [-Z CONTEXT] [-u USER] [ -eIgiqrvw ] [ -SIGNAL ] NAME...
 Usage: killall [OPTION]... [--] NAME...
 Usage: peekfd [-8] [-n] [-c] [-d] [-V] [-h] <pid> [<fd> ..]
    -8 output 8 bit clean streams.
    -n don't display read/write from fd headers.
    -c peek at any new child processes too.
    -d remove duplicate read/writes from the output.
    -V prints version info.
    -h prints this help.

  Press CTRL-C to end output.
 You cannot search for only IPv4 and only IPv6 sockets at the same time all option cannot be used with silent option. fuser (PSmisc) %s
 peekfd (PSmisc) %s
 pstree (PSmisc) %s
 Project-Id-Version: psmisc 1.0
Report-Msgid-Bugs-To: csmall@small.dropbear.id.au
POT-Creation-Date: 2014-02-02 17:04+1100
PO-Revision-Date: 2016-02-08 00:37+0000
Last-Translator: Edesio Costa e Silva <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:14+0000
X-Generator: Launchpad (build 18115)
   -Z,--context REGEXP termina só processo(s) com contexto
                      (tem de preceder outros argumentos)
 %*s COMANDO PID DE ACESSO DO UTILIZADOR
 %s está vazio (não estará montado?)
 %s: sinal desconhecido; %s -| lista de sinais.
 (desconhecido) Expressão regular incorrecta: %s
 Não é possivel obter capacidades de terminal
 Impossível alocar memória para o proc pretendido: %s
 Não é possível encontrar o número de dispositivo do socket.
 Não encontrou utilizador %s
 Impossível abrir directoria /proc: %s
 Não é possível abrir /proc/net/unix: %s
 Não é possível abrir a tomada de rede.
 Não é possível abrir ficheiro de protocolo  "%s": %s
 Impossível resolver porta local %s: %s
 Impossível começar %s: %s
 Impossível começar ficheiro %s: %s
 Copyright (C) 2007 Trent Waddington

 Não foi possível abortar o processo %d: %s
 Erro ao anexar ao pid %i
 Nome de namespace inválido Terminar %s(%s%d) ? (y/N)  Terminar o processo %d ? (y/n)  Abortado %s(%s%d) com sinal %d
 Opção de namespace requer um argumento. Nenhuma especificação de processo dada Nenhum processo encontrado.
 Utilizador desconhecido: %s
 PSmisc vem ABSOLUTAMENTE SEM NENHUMA GARANTIA.
Este é um software livre, e você é bem-vindo a redistribuí-lo sob os
termos da licença GNU General Public License.
Para mais informações sobre a licença, veja os ficheiros com o nome COPYING.
 Pressione Enter para fechar
 Sinal %s(%s%d) ? (s/N)  TERM não está definido
 Porta local AF desconhecida %d
 Uso: killall [-Z CONTEXTO] [-u UTILIZADOR] [-elgiqrvw] [-SINAL] NOME...
 Uso: killall [OPçÃO]... [--] NOME...
 Utilização: peekfd [-8] [-n] [-c] [-d] [-V] [-h] <pid> [<fd> ..]
    -8 saída 8 fluxos limpos 8 bit.
    -n não exibir ler/gravar a partir de cabeçalhos fd.
    -c espreitar quaisquer novos processos filhos também.
    -d remover ler/gravar duplicados da saída.
    -V imprimir informação de versão.
    -h exibe esta ajuda.

  Press CTRL-C to end output.
 Não pode procurar sockets apenas IPv4 e apenas IPv6 ao mesmo tempo opção "todos" não pode ser usada com opção silenciosa. fuser (PSmisc) %s
 peekfd (PSmisc) %s
 pstree (PSmisc) %s
 