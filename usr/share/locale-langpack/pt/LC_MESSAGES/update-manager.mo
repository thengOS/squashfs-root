��    s      �  �   L      �	  1  �	     �
       &        >  ,   F     s     �     �  �   �  �   M            	        (  F   0  *   w  :   �     �     �            ,   :     g     s  &   �  &   �  !   �     �     �       O   4  .   �     �     �     �     �     �  �     A   �     �  K     "   c      �     �     �     �     �          "  %   B  G   h     �  2  �  O   �     B     S     _     k  #   �  "   �     �  8   �  '     )   C     m     �     �     �  2   �  0   �     "     B     V  (   l  �   �  I   B  ;   �  "   �  �   �  ,   �  Q   �  �         �  A     ,   H     u     �     �     �     �  Y   �  O   ,  4   |     �  A   �  
   �       �     V   �  D   �  @   3  P   t  A   �  L     "   T     w  	   �     �     �     �     �  
   �     �  �  �  P  �      #"     ?"  +   G"     s"  5   {"     �"     �"     �"  �   �"  �   �#  #   F$     j$     s$     �$  N   �$  6   �$  M   %     l%  
   �%  (   �%  $   �%  0   �%     &     &  ,   ;&  ,   h&     �&  
   �&  '   �&  '   �&  `   '  <   q'     �'  -   �'     �'     �'     (  �   +(  P   �(  "   E)  J   h)  ,   �)  0   �)  %   *  $   7*  $   \*     �*     �*     �*  /   �*  T   �*     T+  O  j+  R   �,     -     *-     <-  #   L-  )   p-  .   �-      �-  ?   �-  )   *.  /   T.     �.     �.     �.  $   �.  E   �.  =   2/  &   p/     �/     �/  C   �/  �   0  \   �0  L   A1  (   �1  �   �1     }2  N   �2  �   �2  !   �3  Q   �3  8   J4     �4  '   �4     �4     �4     �4  6   5  =   ?5  N   }5     �5  :   �5     6     #6  �   16  [   �6  >   '7  >   f7  j   �7  t   8  V   �8  (   �8     9  
   9     %9     69     M9     d9     x9     �9        +                  $   W   7          k   '                @   g   K      9   R           3      6       D   C               <   .   f      L   l       	   Q   X       e          s      Z   ?   N   P   &   F          4           a   B   ]   [               (                 "   p   ,   c   b   1   U   j   5       /   A   :          #   G      E      d       
   _          S      h   -      n              J       m   \   M       `               2       )       >   T       0      i   *   %   o   ;   V   8             r   !   ^       =       I   q                            H   Y      O    
A normal upgrade can not be calculated, please run:
  sudo apt-get dist-upgrade


This can be caused by:
 * A previous upgrade which didn't complete
 * Problems with some of the installed software
 * Unofficial software packages not provided by Ubuntu
 * Normal changes of a pre-release version of Ubuntu %(size).0f kB %(size).0f kB %.1f MB %i obsolete entries in the status file %s base %s needs to be marked as manually installed. %s will be downloaded. .deb package A file on disk An unresolvable problem occurred while calculating the upgrade.

Please report this bug against the 'update-manager' package and include the following error message:
 An unresolvable problem occurred while initializing the package information.

Please report this bug against the 'update-manager' package and include the following error message:
 Building Updates List Cancel Changelog Changes Changes for %s versions:
Installed version: %s
Available version: %s

 Check if a new Ubuntu release is available Check if upgrading to the latest devel release is possible Checking for updates… Connecting... Copy Link to Clipboard Could not calculate the upgrade Could not initialize the package information Description Details of updates Directory that contains the data files Do not check for updates when starting Do not focus on map when starting Download Downloading changelog Downloading list of changes... Failed to download the list of changes. 
Please check your Internet connection. However, %s %s is now available (you have %s). Install Install All Available Updates Install Now Install missing package. Installing updates… It is impossible to install or remove any software. Please use the package manager "Synaptic" or run "sudo apt-get install -f" in a terminal to fix this issue at first. It’s safer to connect the computer to AC power before updating. No longer downloadable: No network connection detected, you can not download changelog information. No software updates are available. Not all updates can be installed Not enough free disk space Obsolete dpkg status entries Obsolete entries in dpkg status Open Link in Browser Other updates Package %s should be installed. Please wait, this can take some time. Remove lilo since grub is also installed.(See bug #314004 for details.) Restart _Later Run a partial upgrade, to install as many updates as possible.

    This can be caused by:
     * A previous upgrade which didn't complete
     * Problems with some of the installed software
     * Unofficial software packages not provided by Ubuntu
     * Normal changes of a pre-release version of Ubuntu Run with --show-unsupported, --show-supported or --show-all to see more details Security updates Select _All Settings… Show all packages in a list Show all packages with their status Show and install available updates Show debug messages Show description of the package instead of the changelog Show supported packages on this machine Show unsupported packages on this machine Show version and exit Software Updater Software Updates Software index is broken Software updates are no longer provided for %s %s. Some software couldn’t be checked for updates. Support status summary of '%s': Supported until %s: Technical description Test upgrade with a sandbox aufs overlay The changelog does not contain any relevant changes.

Please use http://launchpad.net/ubuntu/+source/%s/%s/+changelog
until the changes become available or try again later. The computer also needs to restart to finish installing previous updates. The computer needs to restart to finish installing updates. The computer will need to restart. The list of changes is not available yet.

Please use http://launchpad.net/ubuntu/+source/%s/%s/+changelog
until the changes become available or try again later. The software on this computer is up to date. The update has already been downloaded. The updates have already been downloaded. The upgrade needs a total of %s free space on disk '%s'. Please free at least an additional %s of disk space on '%s'. Empty your trash and remove temporary packages of former installations using 'sudo apt-get clean'. There are no updates to install. This update does not come from a source that supports changelogs. To stay secure, you should upgrade to %s %s. Unimplemented method: %s Unknown download size. Unsupported Unsupported:  Update is complete Updated software has been issued since %s %s was released. Do you want to install it now? Updated software is available for this computer. Do you want to install it now? Updated software is available from a previous check. Updates Upgrade using the latest proposed version of the release upgrader Upgrade… Version %s: 
 When upgrading, if kdelibs4-dev is installed, kdelibs5-dev needs to be installed. See bugs.launchpad.net, bug #279621 for details. You are connected via roaming and may be charged for the data consumed by this update. You have %(num)s packages (%(percent).1f%%) supported until %(time)s You have %(num)s packages (%(percent).1f%%) that are unsupported You have %(num)s packages (%(percent).1f%%) that can not/no-longer be downloaded You may not be able to check for updates or download new updates. You may want to wait until you’re not using a mobile broadband connection. You stopped the check for updates. _Check Again _Continue _Deselect All _Partial Upgrade _Remind Me Later _Restart Now… _Try Again updates Project-Id-Version: update-manager
Report-Msgid-Bugs-To: sebastian.heinlein@web.de
POT-Creation-Date: 2016-04-12 03:58+0000
PO-Revision-Date: 2016-01-08 15:14+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Ubuntu Portuguese Team <ubuntu-pt.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:19+0000
X-Generator: Launchpad (build 18115)
Language: 
 
Um upgrade normal não pode ser verificado, por favor execute:
  sudo apt-get dist-upgrade


 Isto pode ser originado por:
     * Um upgrade anterior não terminado
     * Problemas com algum software instalado
     * Pacotes de software não fornecidos pelo Ubuntu
     * Mudanças normais de uma versão de pré-lançamento do Ubuntu %(size).0f kB %(size).0f kB %.1f MB %i entradas obsoletas no ficheiro de status %s base %s precisa de ser marcado como instalado manualmente. %s vai ser transferido. Pacote .deb Um ficheiro no disco Um problema sem resolução ocorreu durante a análise do upgrade .

Por favor, reporte este erro do 'gestor de atualizações' e inclua a seguinte mensagem de erro:
 Ocorreu um problema impossível de resolver ao inicializar a informação do pacote.

Por favor, reporte este erro no pacote 'update-manager' e inclua a seguinte mensagem de erro:
 A construir lista de atualizações Cancelar Registo de alterações Alterações Alterações para %s versões:
Versão instalada: %s
Versão disponível: %s

 Verificar se uma nova versão Ubuntu está disponível Verifique se é possível atualizar para a última versão em desenvolvimento A procurar atualizações... A ligar... Copiar link para área de transferência Impossível calcular a atualização Impossível inicializar a informação do pacote Descrição Detalhes das atualizações Diretório que contém os ficheiros de dados Não verificar por atualizações ao iniciar Não focar o mapa quando inicia Transferir A descarregar o registo de alterações A transferir a lista de alterações... Falha ao descarregar a lista de alterações. 
Por favor, verifique a sua ligação à Internet. No entanto, %s %s está disponível (versão instalada: %s). Instalar Instalar todas as atualizações disponíveis Instalar agora Instalar pacote em falta. A instalar atualizações... É impossível instalar ou remover qualquer software. Por favor, utilize o gestor de pacotes "Synaptic" ou execute "sudo apt-get install -f" num terminal para corrigir este problema em primeiro lugar. É mais seguro ligar o seu portátil à corrente antes de fazer a atualização. Não disponível para descarregar: Nenhuma conexão detetada, não pode transferir a informação do registo. Sem atualizações de software disponíveis. Nem todas as atualizações podem ser instaladas Sem espaço livre suficiente no disco Entradas obsoletas no estado do dpkg Entradas obsoletas no estado do dpkg Abrir link no navegador Outras atualizações O pacote %s deve ser instalado. Por favor aguarde, isto pode levar algum tempo. Remova o LILO porque o GRUB já está instalado. (Veja o bug #314004 para detalhes.) Reiniciar _mais tarde Executar um upgrade parcial, para instalar o maior número de atualizações possíveis.

    Isto pode ser originado por:
     * Um upgrade anterior não terminado
     * Problemas com algum software instalado
     * Pacotes de software não fornecidos pelo Ubuntu
     * Mudanças normais de uma versão de pré-lançamento do Ubuntu Executar --show-unsupported, --show-supported ou --show-all para ver mais detalhes Atualizações de segurança Selecionar _todas Definições... Mostrar todos os pacotes numa lista Mostrar todos os pacotes com o seu estado Mostrar e instalar atualizações disponíveis Mostrar mensagens de depuração Mostrar descrição do pacote em vez do registo de alterações Mostrar pacotes suportados nesta máquina Mostrar pacotes não suportados nesta máquina. Mostrar a versão e sair Atualizações Atualizações de Software O índice do software está quebrado Atualizações de software já não são disponibilizadas para %s %s. Impossível procurar por atualizações para todo o software. Sumário de estado do suporte de '%s': Suportado até %s: Descrição técnica Testar atualização com uma sobreposição de aufs na área segura O registo de alterações não contém quaisquer alterações relevantes.

Por favor utilize o http://launchpad.net/ubuntu/+source/%s/%s/+changelog
até que as alterações estejam disponíveis ou tente novamente mais tarde. O computador necessita de reiniciar para acabar a instalação de atualizações anteriores. Deve reiniciar o computador para terminar a instalação das atualizações. O computador irá precisar de reiniciar. A lista de alterações ainda não está disponível.

Por favor use http://launchpad.net/ubuntu/+source/%s/%s/+changelog
até que as alterações estejam disponíveis ou volte a tentar mais tarde. O software está atualizado. A atualização já foi transferida. As atualizações já foram transferidas. A atualização precisa de um total de %s de espaço livre no disco %s. Por favor, liberte pelo menos %s de espaço em disco em %s. Esvazie a reciclagem e remova pacotes temporários de instalações antigas usando 'sudo apt-get clean'. Sem atualizações para instalar. Esta atualização não provém de uma fonte que suporte registo de alterações. Para permanecer seguro, você deve atualizar para %s %s. Método não implementado: %s Tamanho da transferência desconhecido. Não suportado Não suportado:  Atualização está concluída %s %s tem novas atualizações. Deseja instalar agora? Software atualizado está disponível. Deseja instalar agora? Está disponível uma atualização de software de uma verificação anterior. Atualizações Atualizar usando a última versão proposta do atualizador Atualizar... Versão %s: 
 Ao atualizar, se o kdelibs4-dev estiver instalado, o kdelibs5-dev precisa de ser instalado. Consulte bugs.launchpad.net, erro #279621 para mais detalhes. Está ligado por roaming e poderá ser cobrado o tráfego consumido por esta atualização. Tem %(num)s pacotes (%(percent).1f%%) suportados até %(time)s Tem %(num)s pacotes (%(percent).1f%%) que não são suportados Tem %(num)s pacotes (%(percent).1f%%) que não podem/não estão mais disponíveis para serem transferidos Pode não ser capaz de verificar a existência de atualizações ou fazer a transferências de novas atualizações. Poderá querer esperar até não estar a utilizar uma ligação de banda larga móvel. Parou a verificação de atualizações. _Verificar novamente _Continuar _Desmarcar todas _Atualização parcial _Lembrar-me mais tarde _Reiniciar agora… _Tentar novamente atualizações 