��          �   %   �      `  
   a     l     �     �     �     �     �     �     �          .     5     G     S  
   Y     d  C   |     �     �     �  �   �     �     �     �     �     �  �  �  
   �  #   �     �     �  
   �     �          +  #   G     k     �     �     �     �     �     �  j   �     E     _       �   �  (   6	     _	  !   w	     �	     �	                      	                       
                                                                               <b>Etc</b> <b>Hangul toggle key</b> <b>Hanja key</b> <b>Keyboard Layout</b> Advanced Automatic _reordering Commit in _word unit Configure hangul engine Enable/Disable Hangul mode Enable/Disable Hanja mode Hangul Hangul _keyboard: Hangul mode Hanja Hanja lock IBus Hangul Preferences IBus daemon is not running.
Hangul engine settings cannot be saved. IBusHangul Setup Korean Input Method Korean input method Press any key which you want to use as hanja key. The key you pressed is displayed below.
If you want to use it, click "Ok" or click "Cancel" Select Hangul toggle key Select Hanja key Set IBus Hangul Preferences Setup Start in _hangul mode Project-Id-Version: ibus-hangul
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-03 22:49+0000
PO-Revision-Date: 2012-06-04 12:40+0000
Last-Translator: Mykas0 <Mykas0@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:38+0000
X-Generator: Launchpad (build 18115)
 <b>Etc</b> <b>Tecla de alternância Hangul</b> <b>Chave Hanja</b> <b>Disposição de teclado</b> Avançadas Reordenação_automática Confirmar na unidade _word Configurar mecanismo hangul Habilitar/Desabilitar o modo Hangul Ativar/desativar modo Hanja Hangul Hangul _teclado: Modo Hangul Hanja Bloqueio Hanja Preferências IBus Hangul IBus daemon não está a ser executado.
As configurações do mecanismo de Hangul não podem ser gravados. Configuração IBusHangul Método de introdução coreano Método de introdução coreano Pressione qualquer tecla se quer usar como a tecla hanja. A tecla pressionada é exibida em baixo.
Se a quer usar, pressione "Ok" ou clique "Cancelar" Selecione a tecla de alternância Hangul Selecione a tecla Hanja Definir preferências IBus Hangul Configuração Começar no modo_hangul 