��    3      �  G   L      h     i     �     �  "   �  "   �  '   �  -        L  j   `     �  $   �            ,     M  =   m  o   �  N     B   j     �  $   �  $   �       "   /  '   R  /   z     �  ,   �  '   �  #   	  )   8	  !   b	  ?   �	     �	  .   �	  <   
  9   J
     �
  +   �
     �
     �
     �
  +     ,   8     e  ,   y     �  ?   �             M   )  �  w  "   R     u     �  "   �  '   �  )   �  5        M  u   d     �  4   �  "   *  %   M  )   s  <   �  �   �  ^   ]  f   �  #   #  '   G  '   o      �  #   �  ,   �  :   	     D  7   Z  /   �  /   �  )   �  )     @   F      �  6   �  D   �  U   $  $   z  :   �     �     �        7     .   W     �  5   �  !   �  L   �     B  "   ]  Q   �               !             	   0          "             -   .   &             #   /   (   +                                  
                            3                                  ,   )         1   2                       *       '       %   $    All upgrades installed Allowed origins are: %s An error occurred: '%s' Auto-removing the packages failed! Cache has broken packages, exiting Cache lock can not be acquired, exiting Download finished, but file '%s' not there?!? Error message: '%s' Found %s, but not rebooting because %s is logged in. Found %s, but not rebooting because %s are logged in. GetArchives() failed: '%s' Giving up on lockfile after %s delay Initial blacklisted packages: %s Initial whitelisted packages: %s Installing the upgrades failed! Lock could not be acquired (another package manager running?) No '/usr/bin/mail' or '/usr/sbin/sendmail',can not send mail. You probably want to install the 'mailx' package. No packages found that can be upgraded unattended and no pending auto-removals Package '%s' has conffile prompt and needs to be upgraded manually Package installation log: Packages that are auto removed: '%s' Packages that attempted to upgrade:
 Packages that were upgraded:
 Packages that will be upgraded: %s Packages were successfully auto-removed Packages with upgradable origin but kept back:
 Progress: %s %% (%s) Running unattended-upgrades in shutdown mode Simulation, download but do not install Starting unattended upgrades script The URI '%s' failed to download, aborting Unattended upgrade returned: %s

 Unattended-upgrade in progress during shutdown, sleeping for 5s Unattended-upgrades log:
 Unclean dpkg state detected, trying to correct Upgrade in minimal steps (and allow interrupting with SIGINT Warning: A reboot is required to complete this upgrade.

 Writing dpkg log to '%s' You need to be root to run this application [package on hold] [reboot required] dpkg --configure -a output:
%s dpkg returned a error! See '%s' for details dpkg returned an error! See '%s' for details error message: '%s' make apt/libapt print verbose debug messages package '%s' not upgraded package '%s' upgradable but fails to be marked for upgrade (%s) print debug messages print info messages {hold_flag}{reboot_flag} unattended-upgrades result for '{machine}': {result} Project-Id-Version: unattended-upgrades
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-21 04:07+0000
PO-Revision-Date: 2016-01-20 18:26+0000
Last-Translator: Bruno Ramalhete <bram.512@gmail.com>
Language-Team: Portuguese <traduz@debianpt.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
Language: pt
 Todas as atualizações instaladas Origens permitidas: %s Um erro ocorreu: '%s' Auto-remoção dos pacotes falhou! A cache tem pacotes corrompidos, a sair Não foi possível bloquear cache, a sair Acabou o download, mas o ficheiro '%s' não existe?!? Mensagem de erro: '%s' Encontrado %s, mas não reinicia porque %s está ligado. Encontrados %s, mas não reiniciam porque %s estão ligados. Falhou GetArchives(): '%s' A desistir do ficheiro de lock após um atraso de %s Lista negra inicial de pacotes: %s Pacotes iniciais com lista branca: %s Falhou a instalação das atualizações! Não foi possível bloquear (outro gestor de pacotes ativo?) Sem '/usr/bin/mail' ou '/usr/sbin/sendmail',não é possível enviar o email. Provavelmente irá querer instalar o pacote 'mailx'. Sem pacotes encontrados que podem ser atualizados sozinhos e sem dependendo de auto-remoções O pacote '%s' tem uma questão de ficheiro de configuração e necessita de ser atualizado manualmente Registo de instalação de pacotes: Pacotes automaticamente removidos: '%s' Pacotes que tentaram ser actualizados:
 Pacotes que foram actualizados:
 Pacotes que serão actualizados: %s Pacotes foram auto-removidos sucessivamente. Pacotes com possibilidade de actualização mas mantidos:
 Progresso: %s %% (%s) Executar atualizações-abandonadas em modo de desligar Simulação, fazer o download mas não instalar A iniciar o script de atualização automática Falhou o download do URI '%s', a terminar Actualização automática retornou: %s

 Unattended-upgrade em progresso durante o fecho, a dormir por 5s Registo do Unattended-upgrades:
 Detetado estado "unclean" de dpkg, a tentar correção Atualizar em pequenos passos (e permitir interrupções com o SIGINT Atenção: É necessário reiniciar o computador para completar esta atualização.

 A escrever registo do dpkg para '%s' Necessita de acesso de root para executar esta aplicação [pacote em espera] [requer reinício] dpkg --configure -a output:
%s dpkg retornou um erro! Veja '%s' para mais informação dpkg encontrou um erro! Ver '%s' para detalhes mensagem de erro: '%s' make apt/libapt imprime mensagens de debug detalhadas o pacote '%s' não foi atualizado o pacote '%s' é atualizável mas não está marcado para atualização (%s) mostrar mensagens de debug imprimir informação de mensagens {hold_flag}{reboot_flag} unattended-upgrades resultado para '{machine}': {result} 