��    �      �  �   �      �  :   �            ;   (  %   d  "   �  $   �  '   �  3   �     .     :     N     [     v  (   z     �  %   �  )   �          %  &   D  $   k  8   �     �     �       '     (   F     o     �  $   �  #   �  .   �          4     M     k  ,   |  ,   �  #   �     �  	             )     8     M  )   d     �  '   �     �     �     �  -     <   /     l     �  (   �     �     �       3   "     V     n  "   �  #   �     �     �  3        <     W     o     }  )   �     �  
   �     �  *   �  %        6  6   Q  !   �      �     �  "   �  !   �       )   +  0   U     �  2   �     �     �     �          %     ;     S     p       '   �     �  4   �  8         9  
   B  �   M       *   '     R     b     n     �     �     �  J   �          *     D  #   Z     ~     �     �  *   �     �  &   �          6     Q  +   n     �     �  -   �  b   �  N   Z   E   �      �   "   !  )   (!     R!     `!     q!     �!     �!  &   �!  +   �!  &   �!  2   "  	   P"  /   Z"     �"  $   �"     �"  1   �"  2   	#  ;   <#  "   x#  $   �#     �#     �#     �#     �#  /   $  6   @$  !   w$     �$     �$     �$  |   �$  X   Z%  #   �%  *   �%  3   &  *   6&     a&     y&     �&  #   �&     �&     �&  	   �&     �&  )   �&     '     ''     /'     ?'     S'  �  _'  >   N)     �)     �)  ?   �)     �)      *  '   .*  (   V*  ;   *     �*     �*     �*  !   �*     +  '   +  !   7+  *   Y+  .   �+     �+     �+  *   �+  )   	,  <   3,  (   p,     �,  #   �,  :   �,  1   -     I-  !   f-  )   �-  '   �-  /   �-  &   
.     1.     P.     n.  -   �.  /   �.  5   �.     /     &/     6/     I/     W/     p/  H   �/  "   �/  2   �/     (0     =0     U0  <   n0  Z   �0  .   1  4   51  D   j1  1   �1     �1  ,    2  K   -2     y2     �2  .   �2  '   �2  &   �2     %3  =   D3  (   �3  #   �3     �3     �3  /   �3  (   4     :4     K4  0   P4  /   �4      �4  ?   �4  #   5  (   65     _5  4   n5  %   �5     �5  /   �5  J   
6  .   U6  7   �6     �6     �6     �6     �6     7     -7  "   G7     j7     {7  3   �7     �7  C   �7  G   +8  	   s8     }8  �   �8     �9  0   �9     �9     �9     �9     :     $:     9:  ^   U:     �:     �:     �:  9   
;     D;  	   ^;  #   h;  +   �;     �;  @   �;  "   <  0   (<  &   Y<  D   �<  $   �<     �<  7   =  s   ==  Y   �=  J   >     V>  1   r>  1   �>     �>     �>     �>     ?     ?  -   4?  B   b?  (   �?  @   �?  
   @  2   @     M@  (   ]@     �@  7   �@  8   �@  D   A  &   ZA  '   �A  (   �A     �A     �A     �A  8   B  I   PB  (   �B  *   �B  -   �B     C  �   "C  c   �C  ,   D  G   4D  S   |D  3   �D     E     E     8E  ,   DE     qE     yE  
   �E     �E  ;   �E     �E     �E     �E     F     &F                       �   �   o       9               p   �       q   H      D   :           |       i      �   �   g   )   �   �   �              z       }       ;      =   M   *   �   W   �   �   ]   �              �   u   �   f   �   �           �   Y       �   �   %       w   J       m   (   .      �   3       �   C   �       e            -   k          �      �   �   d       K   #   �   N   8           @      �   A          v   �   ,   �   &          >   R   �   �   l           �   6   '   	   �   �                      �       �       �   j   x          �   a   {   h       ^   �   <   �          �       Q               t           7       �       �   �       \          1      2   �   F   T   �   I   0   E   U   �   "       c   �   L      
   y   Z           X       �   S       5   �   �      �   ~   /   n      !   `   4   b   _   �   �   B   �   P   �   �      $       [       �                �   �   r   �       ?               s          V   O   +   G    
    The file is already fully retrieved; nothing to do.

 
%*s[ skipping %sK ] 
%s received.
 
Originally written by Hrvoje Niksic <hniksic@xemacs.org>.
 
REST failed, starting from scratch.
   Issued certificate has expired.
   Issued certificate not yet valid.
   Self-signed certificate encountered.
   Unable to locally verify the issuer's authority.
  (%s bytes)  (unauthoritative)
  [following] %d redirections exceeded.
 %s
 %s (%s) - Connection closed at byte %s.  %s (%s) - Data connection: %s;  %s (%s) - Read error at byte %s (%s). %s (%s) - Read error at byte %s/%s (%s).  %s ERROR %d: %s.
 %s has sprung into existence.
 %s request sent, awaiting response...  %s: %s, closing control connection.
 %s: %s: Failed to allocate %ld bytes; memory exhausted.
 %s: %s:%d: unknown token "%s"
 %s: %s; disabling logging.
 %s: Cannot read %s (%s).
 %s: Cannot resolve incomplete link %s.
 %s: Couldn't find usable socket driver.
 %s: Error in %s at line %d.
 %s: Invalid URL %s: %s
 %s: No certificate presented by %s.
 %s: Syntax error in %s at line %d.
 %s: WGETRC points to %s, which doesn't exist.
 %s: cannot stat %s: %s
 %s: corrupt time-stamp.
 %s: illegal option -- `-n%c'
 %s: missing URL
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: unknown/unsupported file type.
 (no description) (try:%2d) , %s (%s) remaining , %s remaining ==> CWD not needed.
 ==> CWD not required.
 Address family for hostname not supported All requests done Already have correct symlink %s -> %s

 Bad port number Bad value for ai_flags Bind error (%s).
 Can't be verbose and quiet at the same time.
 Can't timestamp and not clobber old files at the same time.
 Cannot back up %s as %s: %s
 Cannot convert links in %s: %s
 Cannot get REALTIME clock frequency: %s
 Cannot initiate PASV transfer.
 Cannot open %s: %s Cannot parse PASV response.
 Cannot specify both --inet4-only and --inet6-only.
 Connecting to %s:%d...  Connecting to %s|%s|:%d...  Continuing in background, pid %d.
 Continuing in background, pid %lu.
 Continuing in background.
 Control connection closed.
 Could not seed PRNG; consider using --random-file.
 Creating symlink %s -> %s
 Data transfer aborted.
 Directories:
 Directory    Disabling SSL due to encountered errors.
 Download quota of %s EXCEEDED!
 Download:
 ERROR ERROR: Redirection (%d) without location.
 Error in proxy URL %s: Must be HTTP.
 Error in server greeting.
 Error in server response, closing control connection.
 Error matching %s against %s: %s
 Error parsing proxy URL %s: %s.
 FTP options:
 Failed reading proxy response: %s
 Failed writing HTTP request: %s.
 File         File `%s' already there; not retrieving.
 Found %d broken link.

 Found %d broken links.

 Found no broken links.

 GNU Wget %s, a non-interactive network retriever.
 Giving up.

 HTTP options:
 HTTPS (SSL/TLS) options:
 IPv6 addresses not supported Index of /%s on %s:%d Interrupted by a signal Invalid IPv6 numeric address Invalid PORT.
 Invalid host name Invalid name of the symlink, skipping.
 Invalid user name Last-modified header invalid -- time-stamp ignored.
 Last-modified header missing -- time-stamps turned off.
 Length:  Length: %s License GPLv3+: GNU GPL version 3 or later
<http://www.gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
 Link         Loading robots.txt; please ignore errors.
 Location: %s%s
 Logged in!
 Logging and input file:
 Logging in as %s ...  Login incorrect.
 Malformed status line Mandatory arguments to long options are mandatory for short options too.

 Memory allocation failure Name or service not known No URLs found in %s.
 No address associated with hostname No data received.
 No error No headers, assuming HTTP/0.9 Non-recoverable failure in name resolution Not sure     Parameter string not correctly encoded Processing request in progress Proxy tunneling failed: %s Read error (%s) in headers.
 Recursion depth %d exceeded max. depth %d.
 Recursive accept/reject:
 Recursive download:
 Remote file does not exist -- broken link!!!
 Remote file exists and could contain further links,
but recursion is disabled -- not retrieving.

 Remote file exists and could contain links to other resources -- retrieving.

 Remote file exists but does not contain any link -- not retrieving.

 Remote file exists.

 Remote file is newer, retrieving.
 Removing %s since it should be rejected.
 Removing %s.
 Request canceled Request not canceled Resolving %s...  Retrying.

 Reusing existing connection to %s:%d.
 Server error, can't determine system type.
 Servname not supported for ai_socktype Spider mode enabled. Check if remote file exists.
 Startup:
 Syntax error in Set-Cookie: %s at position %d.
 System error Temporary failure in name resolution The server refuses login.
 The sizes do not match (local %s) -- retrieving.
 The sizes do not match (local %s) -- retrieving.

 To connect to %s insecurely, use `--no-check-certificate'.
 Try `%s --help' for more options.
 Unable to establish SSL connection.
 Unknown authentication scheme.
 Unknown error Unknown host Unknown system error Unknown type `%c', closing control connection.
 Unsupported listing type, trying Unix listing parser.
 Unterminated IPv6 numeric address Usage: %s NETRC [HOSTNAME]
 Usage: %s [OPTION]... [URL]...
 WARNING WARNING: combining -O with -r or -p will mean that all downloaded content
will be placed in the single file you specified.

 WARNING: timestamping does nothing in combination with -O. See the manual
for details.

 WARNING: using a weak random seed.
 Warning: wildcards not supported in HTTP.
 Will not retrieve dirs since depth is %d (max %d).
 Write failed, closing control connection.
 ai_family not supported ai_socktype not supported connected.
 couldn't connect to %s port %d: %s
 done.
 done.   done.     failed: %s.
 failed: No IPv4/IPv6 addresses for host.
 failed: timed out.
 ignored nothing to do.
 time unknown        unspecified Project-Id-Version: wget 1.14.128
Report-Msgid-Bugs-To: bug-wget@gnu.org
POT-Creation-Date: 2016-06-14 08:19+0000
PO-Revision-Date: 2016-01-09 04:08+0000
Last-Translator: Helder Correia <Unknown>
Language-Team: Portuguese <translation-team-pt@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:22+0000
X-Generator: Launchpad (build 18115)
Language: pt
 
    O ficheiro já está todo transferido; nada para fazer.

 
%*s[ a saltar %sK ] 
%s recebido.
 
Originalmente escrito por Hrvoje Niksic <hniksic@xemacs.org>.
 
REST falhou, a reiniciar.
   Certificado emitido expirado.
   Certificado emitido ainda inválido.
   Encontrado certificado auto-assinado.
   Incapaz de verificar localmente a autoridade do emissor.
  (%s bytes)  (não autoritário)
  [a seguir] %d redireccionamentos excedidos.
 %s
 %s (%s) - Conexão fechada ao byte %s.  %s (%s) - conexão de dados: %s;  %s (%s) - Erro de leitura no byte %s (%s). %s (%s) - Erro de leitura no byte %s/%s (%s).  %s ERRO %d: %s.
 %s formou-se de repente.
 Pedido %s enviado, a aguardar resposta...  %s: %s, a fechar a conexão de controlo.
 %s: %s: Falha ao reservar %ld bytes; memória insuficiente.
 %s: %s:%d: expressão desconhecida "%s"
 %s: %s; a desactivar registo.
 %s: Não é possível ler %s (%s).
 %s: Não é possível resolver a ligação incompleta %s.
 %s: 'socket driver' utilizável não encontrado.
 %s: Erro em %s na linha %d.
 %s: Endereço '%s' inválido: %s
 %s: Certificado não apresentado por %s.
 %s: Erro de sintaxe em %s na linha %d.
 %s: WGETRC aponta para %s, o qual não existe.
 %s: não é possível analisar %s: %s
 %s: selo temporal corrompido.
 %s: opção ilegal -- '-n%c'
 %s: URL em falta
 %s: opção '%s' é ambígua; possibilidades: %s: a opção '--%s' não permite um argumento
 %s: tipo de ficheiro desconhecido ou não suportado.
 (sem descrição) (tentativa:%2d) , %s (%s) em falta , %s em falta ==> CWD desnecessário.
 ==> CWD não requerido.
 Família de endereços não suportada para o nome da máquina (hostname) Todos os pedidos foram completados Já tem a ligação simbólica correcta %s -> %s

 Mau número de porto Mau valor para ai_flags Erro de cobertura (%s).
 Não é possível ser simultaneamente verboso e silencioso.
 Não é possível marcar com selo temporal e sobrepor ficheiros antigos, simultaneamente.
 Não é possível salvaguardar %s como %s: %s
 Não é possível converter as ligações em %s: %s
 Não é possível obter a frequência de relógio de tempo real: %s
 Não é possível iniciar a transferência PASV.
 Não é possível abrir %s: %s Não é possível analisar a resposta PASV.
 Não é possível especificar simultaneamente --inet4-only e --inet6-only.
 A conectar %s:%d...  A conectar %s|%s|:%d...  A continuar em segundo plano (fundo), pid %d.
 A continuar em segundo plano, pid %lu.
 A continuar em segundo plano (fundo).
 Conexão de controlo fechada.
 Não foi possível gerar PRNG; considere usar --random-file.
 A criar a ligação simbólica %s -> %s
 Transferência de dados cancelada.
 Pastas:
 Pasta    A desactivar o SSL devido a erros encontrados.
 Quota de transferência de %s EXCEDIDA!
 Transferência:
 ERRO ERRO: Redireccionamento (%d) sem localização.
 Erro no URL %s do 'proxy': Necessita ser HTTP.
 Erro na saudação do servidor.
 Erro na resposta do servidor, a fechar a conexão de controlo.
 Erro ao corresponder %s com %s: %s
 Erro ao analisar URL %s do 'proxy': %s.
 Opções FTP:
 Falha ao ler a resposta do procurador ('proxy'): %s
 Falha ao escrever o pedido HTTP: %s.
 Ficheiro         O ficheiro '%s' já existe; a não transferir.
 Encontrada %d ligação quebrada.

 Encontradas %d ligações quebradas.

 Não foram encontradas ligações quebradas.

 GNU Wget %s, um transferidor de rede não interactivo.
 A desistir.

 Opções HTTP:
 Opções HTTPS (SSL/TLS):
 Endereços IPv6 não suportados Índice de /%s em %s:%d Interrompido por um sinal Endereço numérico IPv6 inválido PORT inválido.
 Nome de máquina inválido Nome da ligação simbólica inválido, a ignorar.
 Nome de utilizador inválido Último cabeçalho modificado inválido -- selo temporal ignorado.
 Falta o último cabeçalho modificado -- selos temporais desactivados.
 Tamanho:  Tamanho: %s Licença GPLv3+: GNU GPL versão 3 ou posterior
<http://www.gnu.org/licenses/gpl.html>.
Este software é livre: é livre de o alterar e redistribuir.
Não é dada QUALQUER GARANTIA para o programa, até aos limites permitidos por lei aplicável.
 Ligação         A carregar robots.txt; por favor, ignore erros.
 Localização: %s%s
 Entrada com sucesso!
 Registo e ficheiro de entrada:
 A entrar como %s ...  Entrada incorrecta.
 Linha de estado mal-formada Argumentos mandatórios para opções longas são também mandatórios para opções curtas.

 Falha ao alocar memória Nome ou serviço desconhecidos URLs não encontrados em %s.
 Nenhum endereço associado ao nome da máquina (hostname) Nenhuns dados recebidos.
 Sem erros Sem cabeçalhos, a assumir HTTP/0.9 Falha irrecuperável na resolução de nome Incerto     Cadeia de caracteres do parâmetro não codificada correctamente Pedido de processamento a decorrer Falhou o 'túnel' com o procurador ('proxy'): %s Erro de leitura (%s) nos cabeçalhos.
 Profundidade de recursividade %d excedeu a profundidade máxima %d.
 Aceitação/Rejeitação recursiva:
 Transferência recursiva:
 O ficheiro remoto não existe -- ligação quebrada!!!
 O ficheiro remoto existe e pode conter mais ligações,
mas recursividade está desactivada -- a não transferir.

 O ficheiro remoto existe e pode conter ligações para outros recursos -- a transferir.

 O ficheiro remoto existe mas não contém ligações -- não transferir.

 O ficheiro remoto existe.

 O ficheiro remoto é mais recente, a transferir.
 A remover %s, uma vez que deveria ser rejeitado.
 A remover %s.
 Pedido cancelado Pedido não cancelado A resolver %s...  A tentar novamente.

 A reutilizar a conexão existente com %s:%d.
 Erro do servidor, não é possível determinar o tipo de sistema.
 Servname não suportado para ai_socktype Modo de aranha activado. Verificar se o ficheiro remoto existe.
 Arranque:
 Erro de sintaxe em Set-Cookie: %s na posiçao %d.
 Erro do sistema Falha temporária na resolução de nome O servidor recusa a entrada.
 Os tamanhos não coincidem (local %s) -- a transferir.
 Os tamanhos não coincidem (local %s) -- a transferir.

 Para conectar a %s de forma insegura, use '--no-check-certificate'.
 Tente '%s --help' para mais opções.
 Incapaz de estabelecer a conexão SSL.
 Esquema de autenticação desconhecido.
 Erro desconhecido Máquina desconhecida Erro de sistema desconhecido Tipo '%c' desconhecido, a feito a conexão de controlo.
 Tipo de listagem não suportado, a tentar o analisador de listagem Unix.
 Endereço numérico IPv6 não concluído Utilização: %s NETRC [NOME-DA-MÁQUINA]
 Utilização: %s [OPÇÃO]... [ENDEREÇO]...
 AVISO AVISO: combinar -0 com -r ou -p significa que todos os dados transferidos
serão colocados no ficheiro único que especificou.

 AVISO: marcação de tempo não tem acção quando combinado com -O. Veja o
manual para detalhes.

 AVISO: a usar uma semente aleatória fraca.
 Aviso: carácteres de expansão ('wildcards') não suportados no HTTP.
 As pastas não serão transferidas, uma ves que a profundidade é %d (máximo %d).
 A escrita falhou, a fechar a conexão de controlo.
 ai_family não suportada ai_socktype não suportado conectado.
 não foi possível conectar %s porto %d: %s
 feito.
 feito.   feito.     falhou: %s.
 falhou: Endereços IPv4/IPv6 inexistentes para a máquina.
 falhou: terminou o tempo.
 ignorado nada para fazer.
 tempo desconhecido        não especificado 