��    �      L  �   |
      �  7   �     1     ?     X     u  /   �  '   �     �  .   �     .     C  "   O  #   r     �      �      �  *   �  3        K     j  !   ~  C   �  $   �  )   	     3  %   M  "   s  #   �     �     �     �  F   �     ?     ^  `   q  "   �  &   �  0     +   M     y      �     �     �  $   �  "   �  )        I  4   i  5   �  E   �  /     .   J  &   y  (   �  +   �     �       6   .  !   e  "   �     �     �     �     �               <     W     q     �     �     �     �     �  #         ;     \     {     �  (   �     �     �  2   �  #     +   B     n  &   �     �     �     �     �       *   &  C   Q  3   �  0   �  %   �        %   ?     e  +        �  "   �     �     �          #     =  ,   V  3   �  /   �     �     �  (        :  (   N  +   w     �     �  '   �          "     ?     \     {     �     �     �     �  '        +  $   F  2   k      �     �  ,   �  -         5   	   C      M      i   !   |   &   �      �   !   �      !     !  0   &!  !   W!     y!     �!     �!     �!  %   �!  F   �!     /"     F"     b"     |"  "   �"     �"     �"     �"  <   �"  -   :#  �  h#  <    %     =%     Y%  !   w%      �%  <   �%  +   �%     #&  2   C&     v&     �&  ,   �&  -   �&     �&  '   '  ,   8'  +   e'  5   �'  "   �'     �'  $   �'  _   $(  )   �(  .   �(     �(  $   �(  %   )  *   B)     m)     �)     �)  H   �)  (   	*     2*  x   L*  (   �*  )   �*  6   +  1   O+     �+  '   �+     �+     �+  !   �+  $   ,  '   +,      S,  C   t,  >   �,  [   �,  ?   S-  2   �-  '   �-  )   �-  6   .  &   O.  %   v.  A   �.  +   �.  )   
/  "   4/  "   W/      z/  "   �/  %   �/  )   �/  &   0  %   50  %   [0  &   �0  *   �0  %   �0  )   �0      #1  /   D1  #   t1      �1     �1  !   �1  ;   �1     (2     C2  8   a2  %   �2  6   �2  $   �2  +   3     H3     `3     y3  *   �3     �3  2   �3  H   4  5   _4  2   �4  &   �4     �4  2   5     A5  2   a5     �5  1   �5  "   �5     6  #    6  $   D6  "   i6  7   �6  <   �6  ,   7     .7  ,   I7  5   v7     �7  D   �7  :   8  4   H8     }8  ,   �8     �8  4   �8  &   9  &   A9     h9  &   �9  7   �9  -   �9     :  6   +:     b:  .   �:  @   �:  '   �:  )   ;  3   C;  4   w;     �;     �;     �;     �;  *   �;  '   '<     O<  #   j<     �<  #   �<  6   �<  *   �<     "=     0=     E=     _=  :   m=  K   �=     �=  $   >     3>     R>  +   o>     �>     �>     �>  I   �>  6   9?         %   Y       ~   C            �       9   F   "   1           \          c   B   !   )   �   T   l   :   &           `   S      �                -   �      +          @   H   �   n   �      h   �                     y   Z   8   �      D   �   �   |           e       m   2       ;   X           O   k       �   �           b      <   �   N   Q   =   �          �      (   ^      �   q          ]          
           �      �           *         _   J          ,       0   6   M       L   �               �   w   �   �           z   '              �   ?   a   I       d   }   U       /       A   	      �   G               v   V   i   �   P   o   3           �   s   7   �   x   �       4   R              u   >   K   �   p   [       �   {      �   �   r   f   �       5   g       �   W           E       �   t   �       .   �   �   j   #   �          $    

********** beginning dump of nfa with start state %d
 

DFA Dump:

 

Equivalence Classes:

 

Meta-Equivalence Classes:
 
 jam-transitions: EOF    %d (%d saved) hash collisions, %d DFAs equal
   %d backing-up (non-accepting) states
   %d empty table entries
   %d epsilon states, %d double epsilon states
   %d protos created
   %d rules
   %d sets of reallocations needed
   %d state/nextstate pairs created
   %d table entries
   %d templates created, %d uses
   %d total table entries needed
   %d/%d (peak %d) nxt-chk entries created
   %d/%d (peak %d) template nxt-chk entries created
   %d/%d DFA states (%d words)
   %d/%d NFA states
   %d/%d base-def entries created
   %d/%d character classes needed %d/%d words of storage, %d reused
   %d/%d equivalence classes created
   %d/%d meta-equivalence classes created
   %d/%d start conditions
   %d/%d unique/duplicate transitions
   Beginning-of-line patterns used
   Compressed tables always back-up
   No backing up
   no character classes
   scanner options: -  and may be the actual source of other reported performance penalties
  associated rule line numbers:  out-transitions:  %%option yylineno entails a performance penalty ONLY on rules that can match newline characters
 %array incompatible with -+ option %d backing up (non-accepting) states.
 %option yyclass only meaningful for C++ scanners %option yylineno cannot be used with REJECT %s %s
 %s version %s usage statistics:
 %s: fatal internal error, %s
 ********** end of dump
 *Something Weird* - tok: %d val: %d
 -Cf and -CF are mutually exclusive -Cf/-CF and -Cm don't make sense together -Cf/-CF and -I are incompatible -Cf/-CF are incompatible with lex-compatibility mode -I (interactive) entails a minor performance penalty
 -l AT&T lex compatibility option entails a large performance penalty
 -s option given but default rule can be matched Allocation of buffer for line directive failed Allocation of buffer for m4 def failed Allocation of buffer for m4 undef failed Allocation of buffer to print string failed Can't use -+ with -CF option Can't use -+ with -l option Can't use --reentrant or --bison-bridge with -l option Can't use -f or -F with -l option Compressed tables always back up.
 Could not write ecstbl Could not write eoltbl Could not write ftbl Could not write ssltbl Could not write yyacc_tbl Could not write yyacclist_tbl Could not write yybase_tbl Could not write yychk_tbl Could not write yydef_tbl Could not write yymeta_tbl Could not write yynultrans_tbl Could not write yynxt_tbl Could not write yynxt_tbl[][] Definition name too long
 Definition value for {%s} too long
 EOF encountered inside an action EOF encountered inside pattern End Marker
 Input line too long
 Internal error. flexopts are malformed.
 No backing up.
 Option line too long
 Options -+ and --reentrant are mutually exclusive. REJECT cannot be used with -f or -F REJECT entails a large performance penalty
 State #%d is non-accepting -
 Try `%s --help' for more information.
 Unknown error=(%d)
 Unmatched '{' Unrecognized option `%s'
 Usage: %s [OPTIONS] [FILE]...
 Usage: %s [OPTIONS]...
 Variable trailing context rule at line %d
 Variable trailing context rules entail a large performance penalty
 [:^lower:] is ambiguous in case insensitive scanner [:^upper:] ambiguous in case insensitive scanner allocation of macro definition failed allocation of sko_stack failed attempt to increase array size failed bad <start condition>: %s bad character '%s' detected in check_char() bad character class bad character class expression: %s bad character inside {}'s bad character: %s bad iteration values bad line in skeleton file bad start condition list bad state type in mark_beginning_as_normal() bad transition character detected in sympartition() bison bridge not supported for the C++ scanner. can't open %s can't open skeleton file %s consistency check failed in epsclosure() could not create %s could not create backing-up info file %s could not create unique end-of-buffer state could not write tables header dangerous trailing context dynamic memory failure in copy_string() empty machine in dupmachine() error closing backup file %s error closing output file %s error closing skeleton file %s error creating header file %s error deleting output file %s error writing backup file %s error writing output file %s fatal parse error found too many transitions in mkxtion() incomplete name definition input error reading skeleton file %s input rules are too complicated (>= %d NFA states) iteration value must be positive malformed '%top' directive memory allocation failed in allocate_array() memory allocation failed in yy_flex_xmalloc() missing quote missing } name "%s" ridiculously long name defined twice negative range in character class option `%s' doesn't allow an argument
 option `%s' is ambiguous
 option `%s' requires an argument
 premature EOF rule cannot be matched scanner requires -8 flag to use the character %s start condition %s declared twice state # %4d	 state # %d accepts:  state # %d accepts: [%d]
 state # %d:
 symbol table memory allocation failed the character range [%c-%c] is ambiguous in a case-insensitive scanner too many rules (> %d)! trailing context used twice undefined definition {%s} unknown -C option '%c' unknown error processing section 1 unrecognized %%option: %s unrecognized '%' directive unrecognized rule variable trailing context rules cannot be used with -f or -F yymore() entails a minor performance penalty
 Project-Id-Version: flex
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-11-17 11:17-0500
PO-Revision-Date: 2010-02-14 11:37+0000
Last-Translator: Carlos Manuel <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:21+0000
X-Generator: Launchpad (build 18115)
 

********** começando despejo de nfa com início estado%d
 

Descarregamento da DFA:

 

Classes de Equivalência:

 

Classes de Meta-Equivalência:
 
 transições de bloqueio: EOF    %d (%d guardadas) colisões na dispersão, %d DFAs iguais
   %d a fazer backup (não aceitar) estados
   %d entradas vazias na tabela
   %d estados épsilon, %d estados épsilon duplos
   %d prototipos criados
   %d regras
   %d conjuntos de realocação necessários
   %d pares de estado/próximo-estado criados
   %d entradas na tabela
   %d modelos criados, %d utilizações
   %d entradas totais necessárias na tabela
   %d/%d (pico %d) entradas nxt-chk criadas
   %d/%d (pico %d) entradas nxt-chk de modelo criadas
   %d/%d estados DFA (%d palavras)
   %d/%d estados NFA
   %d/%d entradas `base-def' criadas
   %d/%d classes de caracteres necessitaram de %d/%d palavras de armazenamento, %d reutilizadas
   %d/%d classes de equivalência criadas
   %d/%d classes de meta equivalência criadas
   %d/%d start conditions
   %d/%d transições únicas/duplas
   Padrões de inicio de linha usados
   Tabelas comprimidas fazer sempre backup
   Sem cópia de segurança.
   sem classes de caracteres
   opções do scanner: -  e pode ser a fonte real de outras penalidades de desempenho reportadas
  números de linhas associadas à regra:  transições para fora:  %%option yylineno implica uma penalidade de desempenho SÓ em regras que possam aceitar caracteres de mudança de linha
 %array é incompatível com a opção -+ %d a fazer backup (não aceitar) estados
 %option yyclass só é significativa para scanners C++ a %option yylineno não pode ser usada com REJECT %s %s
 estatísticas do uso da %s versão %s:
 %s: erro interno fatal, %s
 ********** fim do dump
 *Algo Estranho* -tok: %d val: %d
 -Cf e -CF são mutuamente exclusivas -Cf/-CF e -Cm não fazem sentido juntos -Cf/-CF e -I são incompatíveis -Cf/-CF são incompatíveis com o modo de compatibilidade com o lex -I (interactivo) implica uma pequena penalidade de desempenho
 a opção de compatibilidade com o lex AT&T -l implica uma grande penalidade de desempenho
 opção -s dada mas a regra por omissão pode ser correspondida Alocação de buffer para linha de diretiva falhou Alocação de buffer para m4 def falhou Alocação de buffer para m4 undef falhou Alocação de buffer para a linha de impressão falhou Não se pode usar -+ com a opção -CF Não se pode usar -+ com a opção -l Não se pode usar --reentrant nem --bison-bridge com a opção -l Não se pode usar -f ou -F com a opção -l Tabelas comprimidas fazer sempre backup.
 Não foi possível escrever ecstbl Não foi possível escrever eoltbl Não foi possível escrever ftbl Não foi possível escrever ssltbl Não foi possível escrever yyacc_tbl Não foi possível escrever yyacclist_tbl Não foi possível escrever yybase_tbl Não foi possível escrever yychk_tbl Não foi possível escrever yydef_tbl Não foi possível escrever yymeta_tbl Não foi possível escrever yynultrans_tbl Não foi possível escrever yynxt_tbl Não foi possível escrever yynxt_tbl[][] Definição de nome muito longo
 Definição do valor para {%s} demasiado longa
 Encontrou EOF dentro de uma acção EOF encontrado dentro de padrão Marcador de Fim
 Linha de entrada demasiado longa
 Erro interno. Os flexopts estão não estão bem formados.
 Sem cópia de segurança.
 Linha de opção muito longa
 As opções -+ e --reentrant são mutuamente exclusivas. REJECT não se pode usar com -f ou -F REJECT implica uma grande penalização no desempenho
 O estado #%d não está a aceitar -
 Tente `%s --help' para mais informações.
 Erro desconhecido=(%d)
 Sem correspondência '{' Opção não reconhecida `%s'
 Utilização: %s [OPÇÕES] [FICHEIRO]...
 Utilização: %s [OPÇÕES]...
 Regra com contexto seguinte variável na linha %d
 Contexto seguinte variável implica uma grande penalidade de velocidade
 [:^lower:] é ambígua no caso de scanner insensível [:^upper:] ambígua no caso de scanner insensível Alocação de definição macro falhou alocação de sko_stack falhou a tentativa de aumentar o tamanho do vector falhou mau <condição de início>: %s caracter incorrecto '%s' detectado em check_char() classe de caracteres inválida expressão de classe de caracteres incorrecta: %s carcater incorrecto dentro de {}'s caracter incorrecto: %s valores incorrectos para iteração Linha com erro no ficheiro esqueleto lista de condições mal iniciadas tipo de estado incorrecto em mark_beginning_as_normal() caracter de transição erróneo detectado em sympartition() ponte bison não suportada para scanner C++. não é possível abrir %s não conseguiu abrir o ficheiro esqueleto %s verificação de consistência falhou em epsclosure() não foi possível criar %s não pôde criar ficheiro de informação de cópia de segurança %s não foi possível criar um estado de fim-de-buffer único não foi possível escrever o cabeçalho das tabelas contexto de arrasto perigoso falha de memória dinâmica em copy_string() maquina vazia em dupmachine() erro ao fechar o ficheiro de cópia de segurança %s erro ao fechar o ficheiro de saída %s erro ao fechar o ficheiro esqueleto %s erro ao criar o header file %s erro ao apagar o ficheiro de saída %s erro ao escrever no ficheiro de cópia de segurança %s erro ao escrever para o ficheiro de saída %s erro fatal de análise foram encontradas demasiadas transições em mkxtion() definição de nome incompleta erro de entrada ao ler o ficheiro esqueleto %s as regras de entrada são demasiado complexas (>=%d estados NFA) o valor da iteração deve ser positivo a directiva '%top' não está bem formada falhou a alocação de memória em allocate_array() a alocação de memória falhou em yy_flex_xmalloc() falta uma plica falta } nome "%s" ridiculamente grande nome definido duas vezes intervalo negativo em classe de caracteres a opção `%s' não permite argumentos
 a opção `%s' é ambigua
 a opção `%s' requer um argumento
 EOF prematuro a regra não pode ser correspondida o scanner requer a opção -8 para usar o caractere %s condição inicial %s declarada duas vezes estado # %4d	 estado # %d aceita:  estado # %d aceita: [%d]
 estado # %d:
 falhou a alocação de memória para a tabela de símbolos o intervalo de caracteres [%c-%c] é ambíguo  num scanner case-insensitive demasiadas regras (> %d)! contexto de arrasto usado duas vezes definição não definida {%s} opção -C desconhecida '%c' erro desconhecido ao processar a secção 1 %%option não reconhecida: %s directiva '%' não reconhecida regra não reconhecida regras com contexto seguinte variável não podem ser usadas com -f ou -F yymore() implica uma pequena penalidade de desempenho
 