��   f   0   `  �   �     �     �  �      �  �  �  �  �
  �  �  �  �     D  	   W     a  9   |     �     �  &   �          4     H     U     f     x     �     �     �     �  	         
     %     7     J     Z     h     p     �     �  $   �  (   �          !     5     K  ,   _     �     �     �  6   �  (        ;     P     p     |          �     �     �     �     �     �     
  &   '  %   N     t     �  )   �  )   �     �  �     
   �     �  "   �  �  �  �  �  u   �  T     �   p  `   '   =   �   0   �      �      !  	   !     %!     -!     ?!  
   S!  	   ^!     h!     m!     u!     �!     �!     �!     �!  =   �!     �!     	"     "     '"  ;   3"     o"  +   ~"     �"  4   �"  	   �"      #  �  #  �  �$    �&  5  �(  �  -     �0  	   �0     	1  7   $1     \1     m1     �1     �1     �1     �1     �1     �1     �1     �1     2      !2     B2     Z2      b2     �2     �2     �2     �2     �2     �2      �2     3  #   #3  4   G3  .   |3  #   �3  #   �3      �3  4   4     I4     i4     ~4  6   �4  (   �4     �4     5     5     /5     45     B5     R5     h5     {5     �5     �5     �5     �5  ,   �5     6     ,6     J6     j6     �6  �   �6     G7     ^7  -   j7    �7  �  �9  }   �;  W   (<  �   �<  j   6=  J   �=  /   �=      >     =>  
   A>  	   L>     V>     h>  
   |>     �>     �>  	   �>     �>     �>     �>     �>     �>  =   �>     ,?     >?     E?     ]?  ;   n?     �?  :   �?     �?  4   @  
   D@     O@     [   A                              ?         C   ]   /   '   G   W           <   2               Z   `                 P   8      9      Y   d   B      %       a   V               T   R   )   H   :      >   N          b      &   U       .   S           D      K   
                  4   Q           I                         @       ^          \       +       E   ,   3   e       	       L       $   6   -      O   "   !       ;       =   #       5       F       M   X   7   0      c      J   f   (   1   _   *         W@  �  �  ]@             ����@            ���� 
  -1, --single   display first response and exit
  -h, --help     display this help and exit
  -m, --multiple wait and display all responses
  -n, --numeric  don't resolve host names
  -q, --quiet    only print the %s (mainly for scripts)
  -r, --retry    maximum number of attempts (default: 3)
  -V, --version  display program version and exit
  -v, --verbose  verbose display (this is the default)
  -w, --wait     how long to wait for a response [ms] (default: 1000)

 
  -4  force usage of the IPv4 protocols family
  -6  force usage of the IPv6 protocols family
  -b  specify the block bytes size (default: 1024)
  -d  wait for given delay (usec) between each block (default: 0)
  -e  perform a duplex test (TCP Echo instead of TCP Discard)
  -f  fill sent data blocks with the specified file content
  -h  display this help and exit
  -n  specify the number of blocks to send (default: 100)
  -V  display program version and exit
  -v  enable verbose output
 
  -A  send TCP ACK probes
  -d  enable socket debugging
  -E  set TCP Explicit Congestion Notification bits in TCP packets
  -f  specify the initial hop limit (default: 1)
  -g  insert a route segment within a "Type 0" routing header
  -h  display this help and exit
  -I  use ICMPv6 Echo Request packets as probes
  -i  force outgoing network interface
  -l  display incoming packets hop limit
  -m  set the maximum hop limit (default: 30)
  -N  perform reverse name lookups on the addresses of every hop
  -n  don't perform reverse name lookup on addresses
  -p  override destination port
  -q  override the number of probes per hop (default: 3)
  -r  do not route packets
  -S  send TCP SYN probes
  -s  specify the source IPv6 address of probe packets
  -t  set traffic class of probe packets
  -U  send UDP probes (default)
  -V  display program version and exit
  -w  override the timeout for response in seconds (default: 5)
  -z  specify a time to wait (in ms) between each probes (default: 0)
 
  -A  send TCP ACK probes
  -d  enable socket debugging
  -E  set TCP Explicit Congestion Notification bits in probe packets
  -f  specify the initial hop limit (default: 1)
  -g  insert a route segment within a "Type 0" routing header
  -h  display this help and exit
  -i  force outgoing network interface
  -l  set probes byte size
  -m  set the maximum hop limit (default: 30)
  -N  perform reverse name lookups on the addresses of every hop
  -n  don't perform reverse name lookup on addresses
  -p  override source TCP port
  -q  override the number of probes per hop (default: 3)
  -r  do not route packets
  -S  send TCP SYN probes (default)
  -s  specify the source IPv6 address of probe packets
  -t  set traffic class of probe packets
  -V, --version  display program version and exit
  -w  override the timeout for response in seconds (default: 5)
  -z  specify a time to wait (in ms) between each probes (default: 0)
                          %3u     infinite (0xffffffff)
   DNS server lifetime     :    DNS servers lifetime    :    Pref. time              :    Route lifetime          :    Route preference        :       %6s
   Valid time              :   %3u%% completed...  %u.%03u ms   (      0x%02x)
  (%0.3f kbytes/s)  MTU                      :   Prefix                   :   Recursive DNS server     : %s
  Source link-layer address:   built %s on %s
  from %s
  unspecified (0x00000000)
 %12u (0x%08x) %s
 %s %lu %s in %f %s %s port %s: %s
 %s%s%s%s: %s
 %s: %s
 %s: invalid hop limit
 %s: invalid packet length
 %u hop max,  %u hops max,  %zu byte packets
 %zu bytes packets
 Cannot create %s (%m) - already running? Cannot find user "%s" Cannot run "%s": %m Cannot send data: %s
 Cannot write %s: %m Child process hung up unexpectedly, aborting Child process returned an error Configured with: %s
 Connection closed by peer Copyright (C) %u-%u Pierre Ynard, Remi Denis-Courmont
 Copyright (C) %u-%u Remi Denis-Courmont
 Fatal error (%s): %m Hop limit                 :     Input error No No response. Raw IPv6 socket Reachable time            :  Receive error: %s
 Received Receiving ICMPv6 packet Retransmit time           :  Router lifetime           :  Router preference         :       %6s
 Sending %ju %s with blocksize %zu %s
 Sending ICMPv6 packet Soliciting %s (%s) on %s...
 Stateful address conf.    :          %3s
 Stateful other conf.      :          %3s
 Target link-layer address:  This is free software; see the source for copying conditions.
There is NO warranty; not even for MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.
 Timed out. Transmitted Try "%s -h" for more information.
 Usage: %s [-4|-6] [hostnames]
Converts names to addresses.

  -4, --ipv4     only lookup IPv4 addresses
  -6, --ipv6     only lookup IPv6 addresses
  -c, --config   only return addresses for locally configured protocols
  -h, --help     display this help and exit
  -m, --multiple print multiple results separated by spaces
  -n, --numeric  do not perform forward hostname lookup
  -r, --reverse  perform reverse address to hostname lookup
  -V, --version  display program version and exit
 Usage: %s [OPTIONS]
Starts the IPv6 Recursive DNS Server discovery Daemon.

  -f, --foreground  run in the foreground
  -H, --merge-hook  execute this hook whenever resolv.conf is updated
  -h, --help        display this help and exit
  -p, --pidfile     override the location of the PID file
  -r, --resolv-file set the path to the generated resolv.conf file
  -u, --user        override the user to set UID to
  -V, --version     display program version and exit
 Usage: %s [options] <IPv6 address> <interface>
Looks up an on-link IPv6 node link-layer address (Neighbor Discovery)
 Usage: %s [options] <IPv6 hostname/address> [%s]
Print IPv6 network route to a host
 Usage: %s [options] <hostname/address> [service/port number]
Use the discard TCP service at the specified host
(the default host is the local system, the default service is discard)
 Usage: %s [options] [IPv6 address] <interface>
Solicits on-link IPv6 routers (Router Discovery)
 Warning: "%s" is too small (%zu %s) to fill block of %zu %s.
 Written by Pierre Ynard and Remi Denis-Courmont
 Written by Remi Denis-Courmont
 Yes [closed]  [open]  addrinfo %s (%s)
 advertised prefixes byte bytes from %s,  high invalid link-layer address low medium medium (invalid) millisecond milliseconds ndisc6: IPv6 Neighbor/Router Discovery userland tool %s (%s)
 packet length port  port %u, from port %u,  port number rdnssd: IPv6 Recursive DNS Server discovery Daemon %s (%s)
 second seconds tcpspray6: TCP/IP bandwidth tester %s (%s)
 traceroute to %s (%s)  traceroute6: TCP & UDP IPv6 traceroute tool %s (%s)
 undefined valid Project-Id-Version: ndisc6
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2011-02-20 10:14+0200
PO-Revision-Date: 2014-04-14 16:03+0000
Last-Translator: IvoGuerreiro <ivoguerreiro@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 18:10+0000
X-Generator: Launchpad (build 18115)
 
  -1, --exibição única primeira resposta e sair
  -h, --ajuda exibir esta ajuda e sair
  -m, --espera múltipla e mostrar todas as respostas
  -n, --numérico não resolve host names
  -q, --quit só imprime o %s (principalmente para scripts)
  -r, --repetir o número máximo de tentativas (padrão: 3)
  -V, --versão mostra a versão do programa e sair
  -v, --detalhada visualização detalhada (esta é o padrão)
  -w, --esperar o tempo a esperar por uma resposta [ms] (padrão: 1000)

 
  -4 uso de força da família de protocolos IPv4
  -6 uso de força da família de protocolos IPv6
  -b especificar o tamanho do bloco em bytes (padrão: 1024)
  -d esperar um determinado atraso (useg) entre cada bloco (default: 0)
  -e realizar teste duplex (eco TCP em vez de descartar TCP)
  -f preencher blocos de dados enviados com o conteúdo do arquivo especificado
  -h exibir esta ajuda e sair
  -n especificar o número de blocos a enviar (padrão: 100)
  -V exibir versão do programa e sair
  -v habilitar verbose output
 
  -A enviar TCP ACK probes
  -d habilitar socket debugging
  -E definir decongestionamento explícito TCP bits em probe packets
  -f especificar o hop limit inical (padrão: 1)
  -g iserir um segmento route dentro de um routing header "type 0"
  -h exibir esta ajuda e sair
  -I utilizar ICMPv6 Echo Request packets como probes
  -i forçar a interface de rede de saída
  -l exibir os packets de entrada do hop limit
  -m definir o máximo hop limit (padrão: 30)
  -N realizar pesquisa de nome inversa sobre os endereços de cada hop
  -n não realizar pesquisa de nome inversa em endereços
  -p override a porta de destino
  -q override o número de probes por hop (padrão: 3)
  -r não fazer route packets
  -S enviar TCP SYN probes (padrão)
  -s especificar a source do endereço IPv6 da probe packets
  -t definir a classe tráfego da probe packets
  -V, --exibir versão do programa e sair
  -U enviar UDP probes (padrão)
  -w override o tempo limite para resposta em segundos (padrão: 5)
  -z especificar um tempo de espera (em ms) entre cada probes (padrão: 0)
 
  -A enviar TCP ACK probes
  -d habilitar socket debugging
  -E definir decongestionamento explícito TCP  bits em probe packets
  -f especificar o hop limit inical (padrão: 1)
  -g iserir um segmento route dentro de um routing header "type 0"
  -h exibir esta ajuda e sair
  -i forçar a interface de rede de saída
  -l definir tamanho em byte das probes
  -m definir o máximo hop limit (padrão: 30)
  -N realizar pesquisa de nome inversa sobre os endereços de cada hop
  -n não realizar pesquisa de nome inversa em endereços
  -p override source TCP port
  -q override o número dep robes por hop (padrão: 3)
  -r não fazer route packets
  -S enviar TCP SYN probes (padrão)
  -s especificar a source do endereço IPv6 da probe packets
  -t definir a classe tráfego da probe packets
  -V, --exibir versão do programa e sair
  -w override o tempo limite para resposta em segundos (padrão: 5)
  -z especificar um tempo de espera (em ms) entre cada probes (padrão: 0)
                          %3u     infinito (0xffffffff)
   Servidor DNS duração :    Servidor DNS duração :    Pref. tempo :    Duração route :    Preferências route : %6s
   Tempo valido :   %3u%% concluído...  %u.%03u ms   ( 0x%02x)
  (%0.3f kbytes/s)  MTU :   Prefixo :   Recursive DNS server : %s
  Origem do endereço link-layer   construído %s em % s
  de %s
  não especificado (0x00000000)
 %12u (0x%08x) %s
 %s %lu %s in %f %s %s port %s: %s
 %s%s%s%s: %s
 %s: %s
 %s: inválido hop limit
 %s: tamanho do pacote inválido
 %u hop max,  %u hops max,  %zu byte pacote
 %zu bytes pacotes
 Não é possível criar %s (%m) - já em execução? Não foi possível encontrar o utilizador "%s" Não é possível executar "%s": %m Não é possível enviar dados: %s
 Não é possivel escrever %s: %m O processo Child desligou inesperadamente, a abortar Processo child devolveu um erro Configurado com: %s
 Ligação terminada por peer Copyright (C) %u-%u Pierre Ynard, Remi Denis-Courmont
 Copyright (C) %u-%u Remi Denis-Courmont
 Erro fatal (%s): %m Hop limit :     Erro de entrada Não Sem resposta. Raw IPv6 socket Tempo alcançável :  Erro recebido: %s
 Recebido A Receber pacote ICMPv6 retransmissão de tempo :  Duração router :  Preferências Router : %6s
 A enviar %ju %s bloco com tamanho de %zu %s
 A enviar pacotes ICMPv6 A solicitar %s (%s) em %s...
 Conf endereço Stateful. : %3s
 Outra conf Stateful.: %3s
 Endereço do alvo link-layer  Este é um software livre, veja a fonte para condições de cópia.
Não há nenhuma garantia, nem mesmo de COMERCIALIZAÇÃO ou
ADEQUAÇÃO A UM DETERMINADO FIM.
 Tempo limite excedido. Transmitido Tente "%s -h" para obter mais informações.
 Utilização: %s [-4|-6] [hostnames]
Converter nomes para endereços.

  -4, --ipv4 só procurar endereços IPv4
  -6, --ipv6 só procurar endereços IPv6
  -c, --configuração só retornar endereços de protocolos configurados localmente
  -h, --ajuda mostrar esta ajuda e sair
  -m,--multiplos imprimir vários resultados separados por espaços
  -n, --numérico não executa a pesquisa do hostname para a frente
  -r, --inverter executar endereço inverso para pesquisa de hostname
  -V, --versão mostrar a versão do programa e sair
 Utilização: %s [opções]
Iniciar IPv6 Recursive DNS Server discovery Daemon.
  -f, --corrida em primeiro plano em primeiro plano
  -H, --merge-hook executar este hook sempre que resolv.conf é atualizado
  -h, --help mostra esta ajuda e sair
  -p, --pidfile substituir a localização do arquivo de PID
  -r, --Resolv-file definir o caminho para o arquivo resolv.conf gerado
  -u, --utilizador substituir o utilizador para definir a UID
  -V, --versão Ver versão do programa de exibição e sair
 Utilização: %s [opções] <IPv6 address> <interface>
Looks up an on-link IPv6 node link-layer address (Neighbor Discovery)
 Uso: %s [opção] <IPv6 endereço/hostname> [%s]
Imprimir IPv6 route de rede a um host
 Uso: %s [opção] <Endereço/hostname> [serviço/numero da porta]
Use o serviço discard TCP no host especificado
(o host padrão é o sistema local, o serviço padrão é discard)
 utilização: %s [opções] [endereço IPv6] <interface>
Solicits on-link IPv6 routers (Router Discovery)
 Aviso: "%s" é demasiado pequeno (%zu %s) para preencher bloco de %zu %s.
 Escrito por Pierre Ynard e Remi Denis-Courmont
 Escrito por Remi Denis-Courmont
 Sim [fechado]  [aberto]  addrinfo %s (%s)
 prefixos anunciados byte bytes de %s,  alto inválida Endereço link-layer baixo médio médio (inválido) milissegundo milissegundos ndisc6: IPv6 Neighbor/Router Discovery userland tool %s (%s)
 tamanho do pacote porta  porta %u, da porta %u,  número da porta rdnssd: IPv6 Recursive DNS Server discovery Daemon %s (%s)
 segundo segundos tcpspray6: TCP/IP verificador de largura de banda %s (%s)
 traceroute para %s (%s)  traceroute6: TCP & UDP IPv6 traceroute tool %s (%s)
 indefinido válido PRIu8  Route                    : %s/%
  Route : %s/%
 