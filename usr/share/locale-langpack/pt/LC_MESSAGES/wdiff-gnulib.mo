��             +         �     �  ,   �  ,     ,   ?  '   l  -   �      �  (   �  (        5     U     u     �     �     �  $   �               /     @     I  #   h     �     �     �     �     �     �     �            �  +     �  /     -   1  /   _  )   �  0   �  "   �  *   	  *   8	  %   c	  $   �	     �	  )   �	  "   �	     
  '   6
     ^
     z
     �
     �
  #   �
  #   �
  "   
     -     5     N     k     �     �     �     �                                                             	                                          
                                         %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid preceding regular expression Invalid range end Invalid regular expression Memory exhausted No match No previous regular expression Premature end of regular expression Regular expression too big Success Trailing backslash Unknown system error Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ memory exhausted Project-Id-Version: sh-utils 1.12i
Report-Msgid-Bugs-To: bug-gnulib@gnu.org
POT-Creation-Date: 2014-04-14 22:15+0200
PO-Revision-Date: 2016-02-07 18:38+0000
Last-Translator: António João Serras Rendas <Unknown>
Language-Team: Português <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:40+0000
X-Generator: Launchpad (build 18115)
Language: pt
 %s: opção inválida -- '%c'
 %s: a opção '%c%s' não permite um argumento
 %s: opção '%s' é ambígua; possibilidades: %s: a opção '--%s' não permite um argumento
 %s: a opção '--%s' requer um argumento
 %s: a opção '-W %s' não permite um argumento
 %s: a opção '-W %s' é ambígua
 %s: a opção '-W %s' requer um argumento
 %s: a opção requer um argumento -- '%c'
 %s: a opção '%c%s' é desconhecida
 %s: opção não reconhecida '--%s'
 Referência anterior inválida Nome de classe de caracteres é inválido Carácter de agrupamento inválido Conteúdo de \{\} inválido Expressão regular precedente inválida Fim de  intervalo inválido Expressão regular inválida Memória esgotada Nenhuma correspondência Nenhuma expressão regular anterior Fim prematuro da expressão regular Expressão regular demasiado longa Sucesso Barra invertida no final Erro desconhecido de sistema ( ou \( sem correspondência ) ou \) sem correspondência [ ou [^ sem correspondência \{ sem correspondência memória esgotada 