��    Y      �     �      �     �     �     �     �     �     �            ,     7   G  *     -   �  <   �  S   	  +   i	  Q   �	  !   �	     	
  4   !
  *   V
  ,   �
     �
     �
     �
     �
  	   �
            #   &     J     R     ^  (   d  :   �     �  4   �       B   ,     o  &   �  >   �  4   �     #     4     ;     A  4   J  -        �  (   �     �     �  *   �       %   0     V     d  '   p     �     �     �  (   �     �     �     �     �       7   $  "   \  3     &   �  *   �  ,     /   2  &   b  /   �  .   �     �     �     �                              $     *     ;  �  B     �     �          -     3     <     N     U  E   ^  N   �  2   �  8   &  R   _  �   �  F   ?  n   �  '   �  "     E   @  2   �  6   �     �  	          #   0     T     a     x  )   �     �     �  
   �  ;   �  B         X  0   y     �  L   �     
  *   '  H   R  >   �     �     �  	   �       F     :   X     �  /   �     �     �  V   �     C  (   `     �     �  '   �     �     �     �  ,   �  	        "     '     9     T  ?   l  #   �  9   �  &   
  ?   1  A   q  6   �  /   �  <     ;   W     �     �     �     �     �     �     �     �     �     �     �               (   ?       P   '       F   Q   $       )   U      2   M          *       
   1   +           R   5       ,                    S       K      %         :   L       	          ;          &                  #       9   G         E          /   T   .           <       "                     X       H   Y          7             4      0   J   N          V       >           @           =      8   C   I   6   B          -          O   !   3   D              A   W       
Audio Device:   %s  Input Buffer %5.1f%%  Output Buffer %5.1f%% %sEOS %sPaused %sPrebuf to %.1f%% (NULL) (none) --- Cannot open playlist file %s.  Skipped.
 --- Driver %s specified in configuration file invalid.
 --- Hole in the stream; probably harmless
 --- Prebuffer value invalid. Range is 0-100.
 === Cannot specify output file without specifying a driver.
 === Could not load default driver and no driver specified in config file. Exiting.
 === Driver %s is not a file output driver.
 === Error "%s" while parsing config option from command line.
=== Option was: %s
 === Incorrect option format: %s.
 === No such device %s.
 === Option conflict: End time is before start time.
 === Parse error: %s on line %d of %s (%s)
 === Vorbis library reported a stream error.
 AIFF/AIFC file reader Author:   %s Available options:
 Bad type in options list Bad value Cannot open %s.
 Comments: %s Could not skip %f seconds of audio. Default Description Done. ERROR: Cannot open file %s for writing.
 ERROR: Could not allocate memory in malloc_buffer_stats()
 ERROR: File %s already exists.
 ERROR: This error should never happen (%d).  Panic!
 Encoded by: %s Error opening %s using the %s module.  The file may be corrupted.
 Error renaming %s to %s
 Error: Could not create audio buffer.
 Error: Out of memory in decoder_buffered_metadata_callback().
 Error: Out of memory in new_print_statistics_arg().
 FLAC file reader FLAC,  File: File: %s Input buffer size smaller than minimum size of %dkB. Internal error parsing command line options.
 Key not found Memory allocation error in stats_init()
 Name No key No module could be found to read from %s.
 Ogg FLAC file reader Ogg Vorbis stream: %d channel, %ld Hz Ogg Vorbis.

 Playing: %s Skipping chunk of type "%s", length %d
 Speex,  Success System error The file format of %s is not supported.
 Time: %s Type Unknown error Vorbis format: Version %d WAV file reader Warning from playlist %s: Could not read directory %s.
 Warning: AIFF-C header truncated.
 Warning: Can't handle compressed AIFF-C (%c%c%c%c)
 Warning: Could not read directory %s.
 Warning: No SSND chunk found in AIFF file
 Warning: No common chunk found in AIFF file
 Warning: Truncated common chunk in AIFF header
 Warning: Unexpected EOF in AIFF chunk
 Warning: Unexpected EOF in reading AIFF header
 Warning: Unexpected EOF in reading WAV header
 bool char default output device double float int none of %s other shuffle playlist string Project-Id-Version: vorbis-tools
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-03-26 03:08-0400
PO-Revision-Date: 2011-02-13 15:13+0000
Last-Translator: Tiago Silva <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:21+0000
X-Generator: Launchpad (build 18115)
 
Dispositivo Áudio: %s  Buffer de entrada %5.1f%%  Buffer de saída %5.1f%% %sEOS %sParado %sPrebuf a %.1f%% (NULL) (nenhum) --- Impossível abrir o ficheiro com lista de músicas %s. A saltar.
 --- O controlador %s especificado no ficheiro de configuração é inválido.
 --- Falha na sequência; provavelmente inofensiva
 --- Valor de prebuffer inválido. O intervalo é 0-100.
 === Impossível especificar um ficheiro de saída sem especificar um controlador.
 === Não foi possível carregar o controlador padrão e não está especificado qualquer controlador no ficheiro de configuração. A sair.
 === O controlador %s não é um controlador de saída de ficheiros  .
 === Erro "%s" aquando do parseamento da opção de configuração na linha de comandos.
=== A opção foi: %s
 === Opção de formato incorrecta: %s.
 === O dispositivo %s não existe.
 === Conflito de opção: O tempo final é anterior ao tempo inicial.
 === Erro de parseamento: %s on line %d of %s (%s)
 === A livraria Vorbis retornou um erro da sequência.
 Leitor de ficheiros AIFF/AIFC Autor: %s Opções disponíveis:
 Tipo inválido na lista de opções Valor errado Impossível abrir %s.
 Comentários: %s Impossível saltar %f segundos de áudio. Predefinição Descrição Terminado. ERRO: Não foi possível abrir o ficheiro %s para escrita.
 ERRO: Não foi possível alocar memória em malloc_buffer_stats()
 ERRO: O ficheiro %s já existe.
 ERRO: Este nunca deveria ocorrer (%d). Pânico!
 Codificado por: %s Erro ao abrir %s utilizando o módulo %s. O ficheiro pode estar corrompido.
 Erro ao renomear %s para %s
 Erro: Impossível criar buffer de áudio.
 Erro: Sem memória disponível em decoder_buffered_metadata_callback().
 Erro: Sem memória disponível em new_print_statistics_arg().
 Leitor de ficheiros FLAC FLAC,  Ficheiro: Ficheiro: %s O tamanho do buffer de entrada é menor que o tamanho mínimo de %dkB. Erro interno ao parsear as opções da linha de comandos.
 Tecla não encontrada Erro de alocação de memória em stats_init()
 Nome Nenhuma tecla Não foi encontrado um módulo a partir do qual seja possível fazer a leitura de %s.
 Leitor de ficheiros Ogg FLAC Sequência Ogg Vorbis: canal %d , %ld Hz Ogg Vorbis.

 A tocar: %s A saltar lixo do tipo "%s", tamanho %d
 Speex,  Sucesso Erro de sistema O formato do ficheiro %s não é suportado.
 Tempo: %s Tipo Erro desconhecido Formato Vorbis: Versão %d Leitor de ficheiros WAV Aviso da lista de músicas %s: Impossível lêr directoria %s.
 Aviso: Cabeçalho AIFF-C truncado.
 Aviso: Incapaz de manipular AIFF-C (%c%c%c%c) comprimido
 Aviso: Impossível ler directoria %s.
 Aviso: Não foram encontrados fragmentos SSND no ficheiro AIFF
 Aviso: Não foram encontrados fragmentos comuns no ficheiro AIFF
 Aviso: Fragmentos comuns truncados no cabeçalho AIFF
 Aviso: Fim de ficheiro inesperado em lixo AIFF
 Aviso: Fim de ficheiro inesperado ao lêr o cabeçalho AIFF
 Aviso: Fim do ficheiro inesperado ao lêr o cabeçalho WAV
 bool char Dispositivo de saída padrão double float int nenhum de %s outro Lista de músicas misturadas string 