��    M      �  g   �      �     �     �     �  �   �  G   |  :   �  ?   �  >   ?  "   ~  (   �     �  (   �  7   	  6   I	     �	     �	  -   �	  (   �	     
     -
  +   K
  $   w
  %   �
  2   �
  (   �
  "     %   A  .   g  A   �  -   �       0      >   Q  2   �  -   �     �       @        X     m  "   �  B   �  5  �  
     ;   *  :   f     �     �  (   �  4   �     )  B   ;  *   ~     �  �   �     E  &   V     }  �   �  :   G  &   �  $  �  K   �  -        H  %   J     p      �     �     �     �     �     �     �     �     �  �  �     w  &   �     �  �   �  K   �  @   �  Q     L   _  (   �  4   �  *   
  ;   5  C   q  ;   �  '   �  )     2   C  '   v  "   �  0   �  0   �  1   #  1   U  9   �  .   �  .   �  ,     ;   L  e   �  .   �       0   <  J   m  4   �  6   �  !   $     F  [   c  "   �  #   �  1      Y   8   �  �   	   B#  <   L#  =   �#     �#     �#  5   �#  C   ,$     p$  G   �$  0   �$      %  �   %     �%  *   �%     �%  �   &  C   �&  .   '  :  >'  c   y(  3   �(     )  *   )     >)     [)     {)     �)     �)     �)     �)     �)     �)     �)        G              D            '      J   A   1       5   !   4       
      M   H          E   :   0           K          ?              (   I          #                                 -          +       &   )          "       *       >   9         C          /         3       7      ,   %   @          <         $   .         =   L   B       2   ;   6   	       8      F       	%s		File: %s

 
-- Type space to continue -- 
 
Commands are:

 
Copyright (C) 2002-2014 László Németh. License: MPL/GPL/LGPL.

Based on OpenOffice.org's Myspell library.
Myspell's copyright (C) Kevin Hendricks, 2001-2002, License: BSD.

 
[SPACE] R)epl A)ccept I)nsert U)ncap S)tem Q)uit e(X)it or ? for help
   --check-apostrophe	check Unicode typographic apostrophe
   --check-url	check URLs, e-mail addresses and directory paths
   -1		check only first field in lines (delimiter = tabulator)
   -D		show available dictionaries
   -G		print only correct words or lines
   -H		HTML input file format
   -L		print lines with misspelled words
   -O		OpenDocument (ODF or Flat ODF) input file format
   -P password	set password for encrypted dictionaries
   -X		XML input file format

   -a		Ispell's pipe interface
   -d d[,d2,...]	use d (d2 etc.) dictionaries
   -h, --help	display this help and exit
   -i enc	input encoding
   -l		print misspelled words
   -m 		analyze the words of the input text
   -n		nroff/troff input file format
   -p dict	set dict custom dictionary
   -r		warn of the potential mistakes (rare words)
   -s 		stem the words of the input text
   -t		TeX/LaTeX input file format
   -v, --version	print version number
   -vv		print Ispell compatible version number
   -w		print misspelled words (= lines) from one word/line input.
 0-n	Replace with one of the suggested words.
 ?	Show this help screen.
 A	Accept the word for the rest of this session.
 AVAILABLE DICTIONARIES (path is not mandatory for -d option):
 Are you sure you want to throw away your changes?  Bug reports: http://hunspell.sourceforge.net
 Can't create tempfile Can't open %s.
 Can't open affix or dictionary files for dictionary named "%s".
 Can't open inputfile Can't open outputfile Cannot update personal dictionary. Check spelling of each FILE. Without FILE, check standard input.

 Example: hunspell -d en_US file.txt    # interactive spelling
         hunspell -i utf-8 file.txt    # check UTF-8 encoded file
         hunspell -l *.odt             # print misspelled words of ODF files

         # Quick fix of ODF documents by personal dictionary creation

         # 1 Make a reduced list from misspelled and unknown words:

         hunspell -l *.odt | sort | uniq >words

         # 2 Delete misspelled words of the file by a text editor.
         # 3 Use this personal dictionary to fix the deleted words:

         hunspell -p words *.odt

 FORBIDDEN! Hunspell has been compiled without Ncurses user interface.
 I	Accept the word, and put it in your private dictionary.
 LOADED DICTIONARY:
%s
%s
 Line %d: %s ->  Model word (a similar dictionary word):  Model word must be in the dictionary. Press any key! New word (stem):  Q	Quit immediately. Asks for confirmation. Leaves file unchanged.
 R	Replace the misspelled word completely.
 Replace with:  S	Ask a stem and a model word and store them in the private dictionary.
	The stem will be accepted also with the affixes of the model word.
 SEARCH PATH:
%s
 Space	Accept the word this time only.
 Spelling mistake? This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE,
to the extent permitted by law.
 U	Accept and add lowercase version to private dictionary.
 Usage: hunspell [OPTION]... [FILE]...
 Whenever a word is found that is not in the dictionary
it is printed on the first line of the screen.  If the dictionary
contains any similar words, they are listed with a number
next to each one.  You have the option of replacing the word
completely, or choosing one of the suggested words.
 X	Write the rest of this file, ignoring misspellings, and start next file.
 ^Z	Suspend program. Restart with fg command.
 a error - %s exceeds dictionary limit.
 error - iconv_open: %s -> %s
 error - iconv_open: UTF-8 -> %s
 error - missing HOME variable
 i q r s u x y Project-Id-Version: hunspell
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-06-02 13:54+0200
PO-Revision-Date: 2014-08-23 13:06+0000
Last-Translator: Pedro Flores <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
 	%s		Ficheiro: %s

 
-- Digite espaço para continuar -- 
 
Os comandos são:

 
Copyright (C) 2002-2014 László Németh. Licença: MPL/GPL/LGPL.

Baseado na biblioteca do Myspell OpenOffice.org
Myspell's copyright (C) Kevin Hendricks, 2001-2002, Licença: BSD.

 
[SPACE] R)epl A)ceitar I)nserir U)ncap S)tem Q)uit e(X)it ou ? para ajuda
   --check-apostrophe	verifica o apóstrofe tipográfico Unicode
   --verificar-url	 verifica URL's, endereços de e-mail e caminhos de diretório
   -1		verificar apenas o primeiro campo em linhas (delimitador = tabulador)
   -D		mostrar dicionários disponíveis
   -G		imprimir somente palavras ou linhas correctas
   -H		Formato de ficheiro de entrada HTML
   -L		imprimir linhas com palavras com erros ortográficos
   -O		OpenDocument (ODF ou Flat ODF) formato de arquivo de entrada
   -P password	definir senha para dicionários encriptados.
   -X		formato de entrada ficheiro XML

   -a		tubulação de interface de Ispell
   -d d[,d2,...]	utilizar d (d2 etc.) dicionários
   -h, --ajuda	exibir esta ajuda e sair
   -i enc	codificação de entrada
   -l		imprimir palavras com erros ortográficos
   -m 		analisar as palavras do texto de entrada
   -n		nroff/troff formato de ficheiro de entrada
   -p dict	definir dict dicionário personalizado
   -r		alertar sobre os possíveis erros (palavras raras)
   -s 		conter as palavras do texto de entrada
   -t		TeX/LaTeX formato de arquivo de entrada
   -v, --version	imprimir número da versão
   -vv		imprimir o número da versão compatível do Ispell
   -w		imprimir palavras com erros ortográficos (= linhas) a partir de uma palavra/entrada de linha.
 0-n	Substitua com uma das palavras sugeridas.
 ?	Mostra este ecrã de ajuda.
 A	Aceitar a palavra para o resto desta sessão.
 DICIONÁRIOS DISPONÍVEIS (caminho não é obrigatório opção para -d):
 Tem a certeza que deseja desistir das alterações?  Relatórios de erros: http://hunspell.sourceforge.net
 Não foi possível criar tempfile Não é possível abrir %s.
 Não é possível abrir afixo ou ficheiros de dicionário para o dicionário chamado "%s".
 Não foi possível abrir inputfile Não foi possível abrir outputfile Não é possível actualizar dicionário pessoal. Verificar a ortografia de cada ficheiro. Sem ficheiro, verifique a entrada predefinida.

 Exemplo: hunspell -d en_US file.txt    # ortografia interativa
         hunspell -i utf-8 file.txt    # verifica o ficheiro codificado UTF-8
         hunspell -l *.odt             # imprimir os erros ortográficos dos ficheiros ODF

         # Arranjo rápido dos documentos ODF pela criação de um dicionário problema

         # 1 Fazer uma lista reduzida a partir das palavras com erros ortográficos e desconhecidas:

         hunspell -l *.odt | sort | uniq >words

         # 2 Apagar as palavras com erros ortográficos do ficheiro com um editor de texto.
         # 3 Utilizar este dicionário pessoal para consertar as palavras apagadas:

         hunspell -p palavras *.odt

 PROIBIDO! Hunspell foi compilado sem interface de utilizador Ncurses.
 I	Aceitar a palavra, e coloque-a no seu dicionário privado.
 DICIONÁRIO CARREGADO:
%s
%s
 Linha %d: %s ->  Palavra Modelo (uma palavra similar do dicionário):  Palavra modelo deve estar no dicionário. Pressione qualquer tecla! Nova palavra (raíz):  Q	Sai imediatamente. Pede  confirmação. Deixa o ficheiro inalterado.
 R	Substituí a palavra incorreta completamente.
 Substituir por:  S	 Perguntar se um hifens numa palavra modelo e armazená-los no dicionário privado.
	 Os hifens serão aceites também com os afixos da palavra modelo.
 Caminho de pesquisa:
%s
 Espaço	 Aceitar a palavra só desta vez.
 Erro ortográfico? Este é um software livre, veja a fonte para condições de cópia. Não há NENHUMA 
garantia, nem mesmo de COMERCIALIZAÇÃO ou ADEQUAÇÃO A UM DETERMINADO FIM, 
até ao limite permitido por lei.
 U	Aceitar e adicionar a versão minúscula de dicionário privado.
 Modo uso: hunspell [OPÇÃO]... [FICHEIRO]...
 Quando uma palavra é encontrada que não se encontra no dicionário
é apresentada na primeira linha do ecrã. Se o dicionário
contiver palavras semelhantes, elas são listadas com um número
ao lado de cada uma delas. Tem a opção de substituir a palavra
completamente, ou escolher uma das palavras sugeridas.
 X	 Escrever o resto deste ficheiro, ignorando erros ortográficos, e começar o próximo ficheiro.
 ^Z	Suspende o programa. Reinicia com o comando fg.
 a erro - %s excede o limite do dicionário.
 erro - iconv_open: %s -> %s
 erro - iconv_open: UTF-8 -> %s
 erro - falta variável HOME
 i q r s u x s 