��          |      �             !     9     R     h  (   ~     �     �     �     �       '     #  D     h     �     �     �  *   �  (   �     '      ?     `           �                      	                          
           Clear all notifications Closes the notification. Display notifications Enable debugging code Exceeded maximum number of notifications Invalid notification identifier Notification Daemon Notification body text. Notification summary text. Notifications Replace a currently running application Project-Id-Version: notification-daemon master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=notification-daemon&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-19 01:30+0200
PO-Revision-Date: 2016-03-17 13:25+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Português <palbuquerque73@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:46+0000
X-Generator: Launchpad (build 18115)
Language: pt
 Limpar todas as notificações Fecha a notificação. Mostrar notificações Ativar código de depuração Número máximo de notificações excedido Identificador de notificação inválido Daemon de notificação Texto do corpo da notificação. Texto resumo da notificação. Notificações Substituir uma app em execução 