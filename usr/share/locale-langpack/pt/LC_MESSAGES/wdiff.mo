��    x      �  �   �      (
  ]   )
  K   �
  9   �
  ?        M     Z     o     �  8   �  1   �  5     ;   8  '   t  %   �  '   �  )   �  ?     =   T  >   �  <   �  C     9   R  9   �  @   �  $     ;   ,  ?   h  +   �  G   �  <     9   Y  7   �  >   �  9   
  #   D  9   h  A   �  B   �  B   '  7   j  C   �  C   �  @   *  8   k  /   �  8   �  >     G   L  -   �  >   �  B     2   D  ;   w  ?   �  G   �  A   ;  G   }  A   �  B        J     e     z     �     �     �     �     �  &   
  '   1     Y     \     s     �  /   �     �  I   �       
     %   )     O     W  �   g  E   �  d  E  &   �  "   �  B   �     7      U  6   v     �  $   �  $   �  -     -   3  &   a  .   �     �     �     �  $   �  '   $  "   L  "   o  �   �         &   7   #   ^      �   0   �   7   �   !   !     '!  %   ?!     e!  ?   y!  <   �!  �   �!  (   �"  �  �"  p   �$  K   �$  9   A%  ?   {%     �%     �%     �%     �%  9   &  2   L&  4   &  C   �&  )   �&  #   "'  '   F'  )   n'  J   �'  @   �'  A   $(  >   f(  K   �(  @   �(  =   2)  I   p)  $   �)  K   �)  A   +*  -   m*  T   �*  @   �*  A   1+  ?   s+  G   �+  >   �+  $   :,  >   _,  A   �,  K   �,  K   ,-  2   x-  K   �-  K   �-  J   C.  B   �.  6   �.  =   /  @   F/  N   �/  9   �/  H   0  C   Y0  6   �0  A   �0  G   1  W   ^1  S   �1  W   
2  S   b2  F   �2     �2     3     53     J3     c3  (   |3     �3     �3  0   �3  0   4     <4     ?4     V4     d4  8   m4     �4  S   �4     5     5  )   5  	   G5     Q5  �   c5  L   6  }  h6  +   �8  2   9  I   E9  )   �9  ,   �9  A   �9     (:  -   <:  .   j:  8   �:  =   �:  1   ;  7   B;     z;     �;     �;  (   �;  3   �;  (   &<  &   O<  �   v<     	=  /   '=  .   W=  '   �=  @   �=  ;   �=  )   +>      U>  *   v>     �>  A   �>  D   �>  �   >?  8   �?     _             (      !   :   Q           k   n   l                  E   -   $   C   q              T      <             K   =       p   w   5      J   U   c      `   u          0                 6   h       x   @          g   t   v           M          H          #       b              +      )      [      e       O   .   /   R       ]   1   9   8   m   X   B   3      Z   S   D   "   d      P   L   j   ^       N         f   s   I   F                   '                          \       
          o   G   i       W      4           Y   	       ,   r   V   *   >   ?   ;           &           a   2   %   A   7       
Copyright (C) 1992, 1997, 1998, 1999, 2009, 2010, 2011, 2012 Free Software
Foundation, Inc.
 
Copyright (C) 1992, 1997, 1998, 1999, 2010 Free Software Foundation, Inc.
 
Copyright (C) 1994, 1997 Free Software Foundation, Inc.
 
Copyright (C) 1997, 1998, 1999 Free Software Foundation, Inc.
 
Debugging:
 
Formatting output:
 
Operation modes:
 
Word mode options:
 
Written by Franc,ois Pinard <pinard@iro.umontreal.ca>.
 
Written by Wayne Davison <davison@borland.com>.
       --help             display this help then exit
       --version          display program version then exit
   %d %.0f%% changed   %d %.0f%% changed   %d %.0f%% common   %d %.0f%% common   %d %.0f%% deleted   %d %.0f%% deleted   %d %.0f%% inserted   %d %.0f%% inserted   -0, --debugging   output many details about what is going on
   -1, --no-deleted           inhibit output of deleted words
   -2, --no-inserted          inhibit output of inserted words
   -3, --no-common            inhibit output of common words
   -=, --use-equals       replace spaces by equal signs in unidiffs
   -A, --auto-pager           automatically calls a pager
   -C, --copyright            display copyright then exit
   -O, --item-regexp=REGEXP   compare items as defined by REGEXP
   -P                     same as -p
   -S, --string[=STRING]   take note of another user STRING
   -T, --initial-tab       produce TAB instead of initial space
   -U                     same as -p and -u
   -V, --show-links        give file and line references in annotations
   -W, --word-mode            compare words instead of lines
   -a, --auto-pager           automatically calls a pager
   -c, --context-diffs    force output to context diffs
   -d, --diff-input           use single unified diff as input
   -e, --echo-comments    echo comments to standard error
   -h                     (ignored)
   -h, --help                 display this help then exit
   -i, --ignore-case          fold character case while comparing
   -k, --less-mode            variation of printer mode for "less"
   -l, --less-mode            variation of printer mode for "less"
   -l, --paginate          paginate output through `pr'
   -m, --avoid-wraps          do not extend fields through newlines
   -n, --avoid-wraps          do not extend fields through newlines
   -o, --old-diffs        output old-style diffs, no matter what
   -o, --printer              overstrike as for printers
   -p, --patch-format     generate patch format
   -p, --printer              overstrike as for printers
   -q, --quiet                inhibit the `mdiff' call message
   -s, --statistics           say how many words deleted, inserted etc.
   -s, --strip-comments   strip comment lines
   -t, --expand-tabs       expand tabs to spaces in the output
   -t, --terminal             use termcap as for terminal displays
   -u, --unidiffs         force output to unidiffs
   -v, --verbose          report a few statistics on stderr
   -v, --version              display program version then exit
   -w, --start-delete=STRING  string to mark beginning of delete region
   -x, --end-delete=STRING    string to mark end of delete region
   -y, --start-insert=STRING  string to mark beginning of insert region
   -z, --end-insert=STRING    string to mark end of insert region
   -z, --terminal             use termcap as for terminal displays
  %d cluster,  %d clusters,  %d file,  %d files,  %d item
  %d items
  %d member
  %d members
  %d member,  %d members,  %d overlap
  %d overlaps
 %s (for regexp `%s') %s: %d word %s: %d words %s: input program killed by signal %d
 %s: output program killed by signal %d
 '
 , %d item
 , %d items
 , clustering , done
 If FILE is not specified, read standard input.
 Launching `%s Mandatory arguments to long options are mandatory for short options too.
 Read summary: Reading %s Report bugs to <wdiff-bugs@gnu.org>.
 Sorting Sorting members This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 This program also tells how `mdiff' could have been called directly.
 This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 Try `%s --help' for more information.
 Usage: %s [OPTION]... FILE1 FILE2
 Usage: %s [OPTION]... FILE1 FILE2
   or: %s -d [OPTION]... [FILE]
 Usage: %s [OPTION]... [FILE]
 Usage: %s [OPTION]... [FILE]...
 With no FILE, or when FILE is -, read standard input.
 Work summary: cannot use -t, termcap not available cannot use -z, termcap not available context diff missing `new' header at line %ld context diff missing `old' header at line %ld could not access the termcap data base could not find a name for the diff at line %ld directories not supported error redirecting stream failed to execute %s ignoring option %s (not implemented) invalid unified diff header at line %ld malformed context diff at line %ld malformed unified diff at line %ld mdiff - Studies multiple files and searches for similar sequences, it then
produces possibly detailed lists of differences and similarities.
 missing file arguments no suitable temporary directory exists only one file may be standard input only one filename allowed options -123RSYZ meaningful only when two inputs select a terminal through the TERM environment variable terminal type `%s' is not defined too many file arguments try `%s --help' for more information
 unable to open `%s' unify - Transforms context diffs into unidiffs, or vice-versa.
 wdiff - Compares words in two files and report differences.
 wdiff - Compute word differences by internally launching `mdiff -W'.
This program exists mainly to support the now oldish `wdiff' syntax.
 word merging for two files only (so far) Project-Id-Version: wdiff
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-04-14 22:15+0200
PO-Revision-Date: 2014-08-09 23:24+0000
Last-Translator: Alexandre Fidalgo <alexandremagnos15@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:40+0000
X-Generator: Launchpad (build 18115)
 
Todos os direitos reservados (C) 1992, 1997, 1998, 1999, 2009, 2010, 2011, 2012 Free Software
Foundation, Inc.
 
Copyright (C) 1992, 1997, 1998, 1999, 2010 Free Software Foundation, Inc.
 
Copyright (C) 1994, 1997 Free Software Foundation, Inc.
 
Copyright (C) 1997, 1998, 1999 Free Software Foundation, Inc.
 
Depuração:
 
A formatar saída:
 
Modos de operação:
 
Opçoes de modo Palavras:
 
Escrito por François Pinard <pinard@iro.umontreal.ca>.
 
Escrito por Wayne Davison <davison@borland.com>.
       --help             mostrar esta ajuda ao sair
       --version          mostrar versão de programa e depois sair
   %d %.0f%% alterado   %d %.0f%% alterado   %d %.0f%% comum   %d %.0f%% comum   %d %.0f%% apagado   %d %.0f%% apagado   %d %.0f%% inserido   %d %.0f%% inserido   -0, --debugging   mostrar muitos detalhes sobre o que está a acontecer
   -1, --no-deleted           inibir saída de palavras apagadas
   -2, --no-deleted           inibir saída de palavras inseridas
   -3, --no-common            inibir saída de palavras comuns
   -=, --use-equals       substituir espaços por sinais iguais em unidiffs
   -A, --auto-pager           automaticamente chama um paginador
   -C, --copyright            mostrar copyright e depois sair
   -O, --item-regexp=REGEXP   comparar itens tal como definido por REGEXP
   -P                     igual a -p
   -S, --string[=EXPRESSÃO]   tomar nota da EXPRESSÃO de outro utilizador
   -T, --initial-tab       produzir TAB em vez de espaço inicial
   -U                     o mesmo que -p e -u
   -V, --show-links        fornecer referências de ficheiro e linha nas anotações
   -W, --word-mode            comparar palavras em vez de linhas
   -a, --auto-pager           chamar automaticamente um paginador
   -c, --context-diffs    forçar saída para diffs de contexto
   -d, --diff-input           usar diff única e unificada como entrada
   -e, --echo-comments    ecoar comentários para erro padrão
   -h                     (ignorado)
   -h, --help                 mostrar esta ajuda e depois sair
   -i, --ignore-case          ignorar capitalização ao comparar
   -k, --less-mode            variação do modo de impressora para "menos"
   -l, --less-mode            variação do modo de impressora para "menos"
   -l, --paginate          paginar saída com `pr'
   -m, --avoid-wraps          não estender campos através de novas linhas
   -n, --avoid-wraps          não estender campos através de novas linhas
   -o, --old-diffs        saída de diffs ao estilo antigo, apesar de tudo
   -o, --printer              linha através como para impressoras
   -p, --patch-format     gerar formato de correcção
   -p, --printer              linha através para impressoras
   -q, --quiet                inibir mensagem de chamada `mdiff'
   -s, --statistics           dizer quantas palavras apagadas, inseridas, etc.
   -s, --strip-comments   eliminar linhas de comentários
   -t, --expand-tabs       expandir tabulações para espaços na saída
   -t, --terminal             usar termcap para janelas de terminal
   -u, --unidiffs         forçar saída para unidiffs
   -v, --verbose          relatar algumas estatísticas em stderr
   -v, --version              mostrar versão do programa e depois sair
   -w, --start-delete=EXPRESSÃO  expressão que marca início da região de apagamento
   -x, --end-delete=EXPRESSÃO    expressão que marca fim da região de apagamento
   -y, --start-insert=EXPRESSÃO  expressão que marca início da região de inserção
   -z, --end-insert=EXPRESSÃO    expressão que marca fim da região de inserção
   -z, --terminal             usar termcap para o visor de um terminal
  %d cluster,  %d clusters,  %d ficheiro,  %d ficheiros,  %d item
  %d items
  %d membro
  %d membros
  %d membro,  %d membros,  %d sobreposição
  %d sobreposições
 %s (para regexp `%s') %s: %d palavra %s: %d palavras %s: programa de entrada terminado pelo sinal %d
 %s: saída de programa terminado pelo sinall %d
 '
 , %d item
 , %d items
 , agrupamento , feito
 Se FICHEIRO não for especificado, ler entrada padrão.
 A lançar `%s Argumentos obrigatórios para opções longas também o são para opções curtas.
 Ler resumo: A ler %s Relatar erros para <wdiff-bugs@gnu.org>.
 A ordenar A ordenar membros Este programa é livre; consulte o código fonte para saber as condições de cópia.
NÃO HÁ GARANTIA; nem mesmo de COMERCIALIZAÇÃO ou de UTILIDADE PARA UM PROPÓSITO PARTICULAR.
 Este programa também diz como `mdiff' podia ter sido chamado directamente.
 Este programa é software grátis: pode redistribuí-lo e/ou modificá-lo
sob os termos da Licença Pública Geral GNU tal como publicado pela
Free Software Foundation, ou na versão 3 da licença, ou
outra versão posterior (na sua opção).

Este programa é distribuído na esperança que lhe seja útil,
mas SEM QUALQUER GARANTIA; sem sequer a garantia implícita
de COMERCIALIZAÇÃO ou ADEQUAÇÃO PARA UM DETERMINADO. Consulte
a Licença Pública Geral GNU para mais detalhes.

Você deverá ter recebido uma cópia da Licença Pública Geral GNU
junto com este programa. Se não recebeu, consulte <http://www.gnu.org/licenses/>.
 Tente `%s --help' para mais informações.
 Utilização: %s [OPÇÃO]... FICHEIRO1 FICHEIRO2
 Utilização: %s [OPTION]... FILE1 FILE2
   ou: %s -d [OPTION]... [FILE]
 Utilização: %s [OPÇÃO]... [FICHEIRO]
 Utilização: %s [OPÇÃO]... [FICHEIRO]...
 Com FICHEIRO, ou quando FICHEIRO está -, ler a entrada padrão.
 Resumo de Trabalho: impossível usar -t, termcap não disponível não se pode usar -z, termcap não disponível falta cabeçalho `novo' em diff de contexto na linha %ld diff de contexto com falta de cabeçalho `velho' na linha %ld não foi possível aceder a base de dados termcap não conseguiu encontrar um nome para diff na linha %ld directórios não suportados erro ao redirecionar o strem falha ao excluír %s a ignorar opção %s (não implementada) cabeçalho inválido de diff unificada na linha %ld diff de contexto malformada na linha %ld diff unificada malformada na linha %ld mdiff - Estuda múltiplos ficheiros e procura sequências similares. Depois
produz listas possivelmente detalhadas de diferenças e semelhanças.
 faltam argumentos de ficheiro não existe um directório temporário adequado apenas um ficheiro pode ser de entrada padrão apenas um nome de ficheiro é permitido opções -123RSYZ significativas apenas quando há duas entradas escolher um terminal através da variável de ambiente TERM tipo de terminal `%s' não está definido demasiados argumento de ficheiro Tente `%s --help' para mais informações
 incapaz de abrir `%s' unify - Transforma diffs de contexto em unidiffs, ou vice-versa.
 wdiff - Compara palavras em dois ficheiros e relata as diferenças.
 wdiff - Computar diferenças de palavras lançando internamente `mdiff -W'.
Este programa existe principalmente para permitir a sintaxe antiga de `wdiff'.
 fusão de palavras em dois ficheiros apenas (até agora) 