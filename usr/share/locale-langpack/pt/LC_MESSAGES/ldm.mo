��          �      \      �     �     �                     '  &   6     ]     f     w     �  (   �  ,   �     �  	          !   &     H     U  �  ]           ,     :     I     U     j  (   �     �     �     �     �  2   �  0   ,     ]  	   z     �  .   �     �  
   �                                  
                     	                                            Automatic login in %d seconds Change _Language Change _Session Default Failsafe xterm Login as Guest No response from server, restarting... Password Select _Host ... Select _Language ... Select _Session ... Select the host for your session to use: Select the language for your session to use: Select your session manager: Shut_down Username Verifying password.  Please wait. _Preferences _Reboot Project-Id-Version: ldm 2.2.1-1
Report-Msgid-Bugs-To: sbalneav@ltsp.org
POT-Creation-Date: 2013-11-24 08:57+0200
PO-Revision-Date: 2012-05-03 14:40+0000
Last-Translator: Pedro Ribeiro <Unknown>
Language-Team: Portuguese <traduz@debianpt.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:26+0000
X-Generator: Launchpad (build 18115)
Language: pt
 Login automático em %d segundos Mudar _Idioma Mudar _Sessão Predefinido Xterm de salvaguarda Entrar como Convidado Sem resposta do servidor, a reiniciar... Palavra-passe Escolher _Máquina ... Escolher _Idioma ... Escolher _Sessão ... Escolher a máquina a utilizar para a sua sessão: Escolher o idioma a utilizar para a sua sessão: Escolher gestor de sessões: _Desligar Nome de utilizador A verificar a palavra-passe. Espere por favor. _Preferências _Reiniciar 