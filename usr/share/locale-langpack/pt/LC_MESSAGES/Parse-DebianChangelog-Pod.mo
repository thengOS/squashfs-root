��    3      �  G   L      h     i     {     �     �     �     �     �     �     �     �     �     �  '   �  !   �          *     /  1   <  	   n     x     �     �     �     �     �     �     �     �     �     �  )   �                    $     )     2     7     <     E     N     R  
   Y     d     j     p     �     �     �     �  �  �     J     Z     j     |     �     �     �     �     �     �     �     �  '   �  #   �     �     
	     	  1   	  	   P	     Z	     c	     l	     q	     ~	     �	     �	     �	     �	  	   �	     �	  2   �	     
     
     *
     1
     6
     ?
     B
     G
     P
     \
     a
  
   h
     s
     y
     �
     �
     �
     �
     �
               *             '       	                     3                1                  
   %      $                                       )      !   /          ,   -      0               2   (                 "          #             .   +      &    (and B<dpkg_str>) (and B<html_str>) (and B<rfc822_str>) (and B<xml_str>) * 1. 2. 3. 4. AUTHOR BUGS Changes Copyright (C) 2005 by Frank Lichtenheld Creates a new object, no options. DESCRIPTION Date Distribution Frank Lichtenheld, E<lt>frank@lichtenheld.deE<gt> Functions METHODS Methods NAME SEE ALSO See L<dpkg>. See L<html>. See L<rfc822>. See L<xml>. Source Urgency Version add_filter, delete_filter, replace_filter all apply_filters count dpkg dpkg_str from html html_str is_empty new rfc822 rfc822_str since style the original line to until xml xml_str Project-Id-Version: libparse-debianchangelog-perl
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-08-25 20:38+0000
PO-Revision-Date: 2009-06-13 19:03+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 (e B<dpkg_str>) (e B<html_str>) (e B<rfc822_str>) (e B<xml_str>) * 1. 2. 3. 4. AUTOR ERROS Alterações Copyright (C) 2005 by Frank Lichtenheld Cria um novo objecto, sem opções. DESCRIÇÃO Data Distribuição Frank Lichtenheld, E<lt>frank@lichtenheld.deE<gt> Funções MÉTODOS Métodos NOME VEJA TAMBÉM Consultar L<dpkg>. Consultar L<html>. Consulte L<rfc822>. Consultar L<xml>. Fonte Urgência Versão adicionar_filtro, apagar_filtro, substituir_filtro tudo aplicar_filtros contar dpkg dpkg_str de html html_str está_vazio novo rfc822 rfc822_str desde estilo a linha original para até xml xml_str 