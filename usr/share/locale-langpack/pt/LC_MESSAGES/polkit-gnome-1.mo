��          �      ,      �     �     �     �     �  �   �  �   �  ~        �  /   �     �     �     �  ?        C     Q  
   c  �  n     J     R  !   q     �  �   �  �   W  �   �  
   {  9   �     �     �     �  R   	     X	     d	     t	        	                                                                  
    %s (%s) <small><b>Action:</b></small> <small><b>Vendor:</b></small> <small><b>_Details</b></small> An application is attempting to perform an action that requires privileges. Authentication as one of the users below is required to perform this action. An application is attempting to perform an action that requires privileges. Authentication as the super user is required to perform this action. An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Authenticate Authentication dialog was dismissed by the user Click to edit %s Click to open %s Select user... Your authentication attempt was unsuccessful. Please try again. _Authenticate _Password for %s: _Password: Project-Id-Version: policykit-gnome
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-04 00:05+0000
PO-Revision-Date: 2015-12-04 21:26+0000
Last-Translator: António Lima <amrlima@gmail.com>
Language-Team: gnome_pt@yahoogroups.com
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:41+0000
X-Generator: Launchpad (build 18115)
X-Poedit-Country: PORTUGAL
Language: pt
X-Poedit-Language: Portuguese
 %s (%s) <small><b>Acção:</b></small> <small><b>Fornecedor:</b></small> <small><b>_Detalhes</b></small> Uma aplicação está a tentar executar uma acção que requer privilégios. É necessária uma autenticação como um dos utilizadores para executar esta acção. Uma aplicação está a tentar executar uma acção que requer privilégios. É necessária autenticação como super utilizador para executar esta acção. Uma aplicação está a tentar executar uma acção que requer privilégios. É necessária autenticação para executar esta acção. Autenticar A janela de autenticação foi dispensada pelo utilizador Clique para editar %s Clique para abrir %s Seleccionar utilizador... A sua tentativa de autenticação não foi bem sucedida. Por favor, tente de novo. _Autenticar _Senha para %s: _Senha: 