��    #      4  /   L           	     $  #   *  %   N     t     �     �     �     �     �     �     �          
           -     N     V     c     o     �  \   �  ?   �  #   2  '   V     ~     �     �     �     �     �     �     �     �  !  �  *   �       .     +   J     v  %   �  '   �     �     �     �     	     	  	   	     &	     ;	  3   V	  
   �	     �	     �	  *   �	     �	  ]   �	  F   M
  ,   �
  #   �
     �
     �
  	   �
     �
                    !     0                         #            
          "                                                                !            	                                  - libpeas demo application About Additional plugins must be disabled An additional plugin must be disabled An error occurred: %s Dependency '%s' failed to load Dependency '%s' was not found Disable Plugins E_nable All Enabled Failed to load Peas Gtk Plugin Plugin Manager Plugin Manager View Plugin loader '%s' was not found Plugins Pr_eferences Preferences Run from build directory Show Builtin The '%s' plugin depends on the '%s' plugin.
If you disable '%s', '%s' will also be disabled. The following plugins depend on '%s' and will also be disabled: The plugin '%s' could not be loaded There was an error displaying the help. View _About _Cancel _Close _Disable All _Enabled _Help _Preferences _Quit Project-Id-Version: libpeas master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libpeas&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-24 00:33+0000
PO-Revision-Date: 2016-02-24 09:17+0000
Last-Translator: Pedro Albuquerque <Unknown>
Language-Team: Português <palbuquerque73@openmailbox.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:58+0000
X-Generator: Launchpad (build 18115)
X-Project-Style: gnome
Language: pt
 - aplicação de demonstração de libpeas Sobre É necessário desativar extensões adicionais É necessário desativar mais uma extensão Ocorreu um erro: %s Falha ao carregar a dependência "%s" Não foi encontrada a dependência "%s" Desativar extensões Ativar _Todos Ativo Falha ao carregar Peas Gtk Extensão Gestor de extensões Vista Gestor de extensões O carregador de extensões "%s" não foi encontrado Extensões _Preferências Preferências Executar a partir de pasta de compilação Mostrar embutidos A extensão "%s" depende da extensão "%s".
Se desativar "%s", "%s" também será desativada. As seguintes extensões dependem de "%s" e também serão desativadas: Não foi possível carregar a extensão "%s" Ocorreu um erro ao mostrar a ajuda. Ver _Sobre _Cancelar _Fechar _Desativar todos _Ativo A_Juda _Preferências _Sair 