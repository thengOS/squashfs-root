��    !      $  /   ,      �  $   �       /   *  g   Z     �     �  *   �       M        j     �  (   �     �     �     �     �     �        *   ,  �   W  ;   �       0   2  <   c  r   �  2        F     b  "   x     �     �  #   �  �  �     �	     �	  7   �	  x   /
  	   �
     �
  5   �
     �
  a        y     �  ,   �     �     �     �            4   .  0   c  �   �  K   ,     x  >   �  D   �  |     /   �     �     �  #   �  "     $   B  ,   g     !                                                                   	                                
                                                       %s: Version %s has already been seen %s: will be newly installed --since=<version> expects a only path to a .deb <big><b>Changelogs</b></big>

The following changes are found in the packages you are about to install: Aborting Changes for %s Confirmation failed, don't save seen state Continue Installation? Could not run apt-changelog (%s), unable to retrieve changelog for package %s Do you want to continue? [Y/n]  Done Ignoring `%s' (seems to be a directory!) Informational notes List the changes Mailing %s: %s News for %s Reading changelogs Reading changelogs. Please wait. The %s frontend is deprecated, using pager The gtk frontend needs a working python-gtk2 and python-glade2.
Those imports can not be found. Falling back to pager.
The error is: %s
 The mail frontend needs a installed 'sendmail', using pager Unknown frontend: %s
 Unknown option %s for --which.  Allowed are: %s. Usage: apt-listchanges [options] {--apt | filename.deb ...}
 Wrong or missing VERSION from apt pipeline
(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version set to 2?)
 You can abort the installation if you select 'no'. apt-listchanges: Changelogs apt-listchanges: News apt-listchanges: changelogs for %s apt-listchanges: news for %s database %s failed to load.
 didn't find any valid .deb archives Project-Id-Version: apt-listchanges
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-17 14:04+0000
PO-Revision-Date: 2014-04-14 16:05+0000
Last-Translator: IvoGuerreiro <ivoguerreiro@gmail.com>
Language-Team: Portuguese <traduz@debianpt.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:57+0000
X-Generator: Launchpad (build 18115)
Generated-By: pygettext.py 1.5
 %s: A versão %s já foi vista %s: será novamente instalado --since=<version> espera um caminho único para um .deb <big><b>Registos de alterações</b></big>

Foram encontradas as seguintes alterações nos pacotes que deseja instalar: A abortar Alterações para %s A confirmação falhou, não gravar o estado de visto Continuar a Instalação? Não foi possível executar apt-changelog (%s), incapaz de recuperar o changelog para o pacote %s Deseja continuar? [S/n]  Feito A ignorar `%s' (parece ser um directório !) Notas informativas Listar as alterações A enviar %s por mail: %s Novidades para %s A ler os changelogs A ler os registos de alterações. Por favor aguarde O frontend %s foi depreciado, a utilizar o pager O frontend gtk necessita de um python-gtk2 e python-glade2 funcionais.
Estes 'import' não puderam ser encontrados. A retornar ao pager.
O erro é: %s
 O frontend de mail necessita de um 'sendmail' instalado, a utilizar o pager Frontend desconhecido: %s
 Opção %s desconhecida para --which.  As permitidas são: %s. Utilização: apt-listchanges [opções] {--apt | ficheiro.deb ...}
 VERSÃO errada ou em falta a partir do apt
(Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version está definido para 2?)
 Pode abortar a instalação se escolher 'não'. apt-listchanges: Changelogs apt-listchanges: Novidades apt-listchanges: changelogs para %s apt-listchanges: novidades para %s falhou carregar a base de dados %s.
 não encontrei qualquer arquivo .deb válido 