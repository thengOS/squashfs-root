��    I      d  a   �      0     1  4  N     �  R   �  g   �  Z   W	  '   �	     �	     �	     �	     
  	   '
  1   1
  	   c
  	   m
  (   w
  )   �
     �
     �
     �
  #      '   $  "   L     o     �     �     �     �     �     �     �      �          	             !   /  '   Q  %   y  	   �     �  '   �     �  P   �     1     H     X     e  
   t  ;     3   �  Q   �     A     P  &   g     �      �  #   �      �  *   	      4      U  /   v     �  =   �  	         
               #  (   0     Y  �  i        i  ,     �  h   �  p     ]   �  %   �       !        ?     ]  
   w  4   �  	   �     �  *   �  *   �  *   %     P     ^  ,   t  ;   �  &   �               <     B     X     ^     m     ~  )   �     �     �     �     �     �  5     .   B     q     z  -   �  	   �  U   �          !     3     ?  
   R  D   ]  :   �  V   �     4     G  ,   b     �     �  )   �  %   �  7     ;   D     �  =   �     �  E   �  
   ;     F     X     j     q  *        �         )      +   I   E   A       #   D   =      "          ;          C   *           0      /              H            9   4      	      
              >   <   8       $           -                   B   ,          3       ?              :   &          5   2   7                @          6                           G   1      '   %   F   .   !       (     - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Add Startup Program Additional startup _programs: Allow logout Browse… Choose what applications to start when you log in Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Could not display help document Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don't prompt for user confirmation Edit Startup Program Enable debugging code Enabled Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Icon Ignoring any existing inhibitors Log out No description No name Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Select Command Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Applications Startup Applications Preferences The startup command cannot be empty The startup command is not valid This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: 3.18
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-05-18 08:40+0000
PO-Revision-Date: 2016-05-26 01:02+0000
Last-Translator: Pedro Albuquerque <Unknown>
Language-Team: Pedro Albuquerque
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:39+0000
X-Generator: Launchpad (build 18115)
X-Project-Style: gnome
Language: pt
  - o gestor de sessões do GNOME %s [OPÇÃO...] COMANDO

Executar COMANDO ao mesmo tempo que inibe alguma funcionalidade de sessão.

  -h, --help        Mostrar esta ajuda
  --version         Mostrar a versão da aplicação
  --app-id ID       A id de aplicação a usar
                    ao inibir (opcional)
  --reason REASON   O motivo para inibir (opcional)
  --inhibit ARG     Funcionalidades a inibir, lista separada por vírgulas de:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Não iniciar COMANDO e esperar indefinidamente

Se não for especificada nenhuma opção --inhibit, é assumida idle.
 %s requer um argumento
 Ocorreu um problema e o sistema é incapaz de recuperar.
Por favor, termine a sessão e tente novamente. Ocorreu um problema e o sistema é incapaz de recuperar. Por precaução, foram desativadas todas as extensões. Ocorreu um problema e o sistema é incapaz de recuperar. Contacte um administrador do sistema Já existe uma sessão designada "%s" PASTA_INICIOAUTO Adicionar aplicação de arranque _Apps de arranque adicionais: Permitir terminar sessão Navegar… Escolha que apps devem iniciar quando inicia sessão Co_mando: Com_entário: Impossível ligar-se ao gestor de sessões Impossível criar socket de escuta ICE: %s Incapaz de apresentar o documento de ajuda Personalizado Sessão personalizada Desativar teste de aceleração por hardware Não carregar as aplicações especificadas pelo utilizador Não pedir confirmação ao utilizador Editar apps de arranque Ativar o código de depuração Ativo Falha ao executar %s
 GNOME GNOME de teste GNOME em Wayland Ícone A ignorar quaisquer inibidores existentes Terminar sessão Sem descrição Sem nome Não está a responder Ocorreu um erro inesperado. Especificar novas pastas base de arranque automático Selecione uma sessão personalizada a executar Desligar Aplicação Aplicação invocada com conflito de opções Reiniciar A recusar uma nova ligação cliente pois a sessão está atualmente a ser terminada
 App lembrada Reno_mear sessão NOME_SESSAO Selecionar comando Sessão %d Não é permitido que o nome de uma sessão contenha o carácter "/" Não é permitido que o nome de uma sessão comece por "." Não é permitido que o nome de uma sessão comece por "." ou contenha o carácter "/" Sessão a utilizar Mostrar aviso de extensão Mostrar o diálogo de falha total para teste Apps que iniciam com a sessão Preferências O comando de arranque não pode ser vazio O comando de arranque não é válido Esta entrada permite-lhe selecionar uma sessão gravada Esta aplicação está a impedir o encerramento da sessão. Esta é uma sessão no GNOME Esta sessão faz com que entre no GNOME, utilizando o Wayland Versão desta aplicação _Recordar automaticamente as apps em execução ao terminar a sessão _Continuar _Terminar sessão _Terminar sessão _Nome: _Nova sessão _Recordar as apps atualmente em execução _Remover sessão 