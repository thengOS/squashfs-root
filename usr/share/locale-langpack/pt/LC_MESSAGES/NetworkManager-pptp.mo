��    I      d  a   �      0     1     G     \     r     ~     �     �     �     �     �  n   �     ]     y     �  +   �  J   �  E   +  �   q  r   �  k   n	     �	     �	  5   �	  "   &
  <   I
     �
  #   �
     �
  )   �
     �
  <      3   =      q     �     �     �      �      �     �  
        "     @     P  �   g            ;   3  -   o  	   �  (   �  u   �  h   F     �  `   �     '     5  %   Q     w  
   �  D   �  	   �  
   �  6   �  6   ,  -   c     �     �  �   �  6   E  ;   |  &   �     �  �  �     �     �     �  
   �     �     �               5      C  �   d  "   �  &     "   7  1   Z  Z   �  U   �  �   =  �   �     g     �     �  :   �  /   :  \   j  )   �  &   �       (         I  O   M  /   �  #   �     �     �             -   8  "   f     �  #   �     �     �  �   �     �     �  J   �  +        J  +   Y  �   �  v        �  {   �     %  #   3  (   W  /   �     �  C   �  	         
  B     A   Z  7   �     �  "   �  �      @   �   E   �   ,   !  '   J!     *          %                          E      '   4   +       0          :   ;   1          &      I                         F      $   B   (   9      7   @   G       8   C   
      ?   2       3                   ,   )   5   H             .       =       #       A   /             "                  6              D   <   !   >         	   -               128-bit (most secure) 40-bit (less secure) <b>Authentication</b> <b>Echo</b> <b>General</b> <b>Misc</b> <b>Optional</b> <b>Security and Compression</b> Ad_vanced... All Available (Default) Allow MPPE to use stateful mode. Stateless mode is still attempted first.
config: mppe-stateful (when checked) Allow _BSD data compression Allow _Deflate data compression Allow st_ateful encryption Allow the following authentication methods: Allow/disable BSD-Compress compression.
config: nobsdcomp (when unchecked) Allow/disable Deflate compression.
config: nodeflate (when unchecked) Allow/disable Van Jacobson style TCP/IP header compression in both the transmit and the receive directions.
config: novj (when unchecked) Allow/disable authentication methods.
config: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap Append the domain name <domain> to the local host name for authentication purposes.
config: domain <domain> Authenticate VPN CHAP Compatible with Microsoft and other PPTP VPN servers. Could not find pptp client binary. Could not find secrets (connection invalid, no vpn setting). Could not find the pppd binary. D-Bus name to use for this instance Default Don't quit when VPN connection terminates EAP Enable custom index for ppp<n> device name.
config: unit <n> Enable verbose debug logging (may expose passwords) Invalid or missing PPTP gateway. MSCHAP MSCHAPv2 Missing VPN gateway. Missing or invalid VPN password. Missing or invalid VPN username. Missing required option '%s'. NT Domain: No VPN configuration options. No VPN secrets! No cached credentials. Note: MPPE encryption is only available with MSCHAP authentication methods. To enable this checkbox, select one or more of the MSCHAP authentication methods: MSCHAP or MSCHAPv2. PAP PPTP Advanced Options PPTP server IP or name.
config: the first parameter of pptp Password passed to PPTP when prompted for it. Password: Point-to-Point Tunneling Protocol (PPTP) Require the use of MPPE, with 40/128-bit encryption or all.
config: require-mppe, require-mppe-128 or require-mppe-40 Send LCP echo-requests to find out whether peer is alive.
config: lcp-echo-failure and lcp-echo-interval Send PPP _echo packets Set the name used for authenticating the local system to the peer to <name>.
config: user <name> Show password Use TCP _header compression Use _Point-to-Point encryption (MPPE) Use custom _unit number: User name: You need to authenticate to access the Virtual Private Network '%s'. _Gateway: _Security: couldn't convert PPTP VPN gateway IP address '%s' (%d) couldn't look up PPTP VPN gateway IP address '%s' (%d) invalid boolean property '%s' (not yes or no) invalid gateway '%s' invalid integer property '%s' nm-pptp-service provides integrated PPTP VPN capability (compatible with Microsoft and other implementations) to NetworkManager. no usable addresses returned for PPTP VPN gateway '%s' no usable addresses returned for PPTP VPN gateway '%s' (%d) property '%s' invalid or not supported unhandled property '%s' type %s Project-Id-Version: 2.26
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-04-14 07:51+0000
PO-Revision-Date: 2016-04-29 09:29+0000
Last-Translator: PsyKick_RuhYn <psykick.ruhyn@gmail.com>
Language-Team: Portuguese <gnome_pt@yahoogroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:03+0000
X-Generator: Launchpad (build 18115)
 128-bit (mais segura) 40-bit (menos segura) <b>Autenticação</b> <b>Eco</b> <b>Geral</b> <b>Outros</b> <b>Opcional</b> <b>Segurança e compressão</b> A_vançado... Todos os disponíveis (omissão) Permite ao MPPE utilizar o modo com estado. O modo sem estado é sempre tentado primeiro.
configuração: mppe-stateful (quando marcado) Permitir compressão de dados _BSD Permitir compressão de dados _Deflate Permitir encriptação com est_ado Permitir os seguintes métodos de autenticação: Permite/desativa a compressão BSD-Compress.
configuração: nobsdcomp (quando desmarcado) Permite/desativa a compressão Deflate.
configuração: nodeflate (quando desmarcado) Permite/desativa a compressão de cabeçalho TCP/IP do estilo Van Jacobson em ambas as direções de transmissão e receção.
configuração: novj (quando desmarcado) Permite/desativa métodos de autenticação.
configuração: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap Acrescenta o nome de domínio <domain> ao nome de máquina local para fins de autenticação.
configuração: domínio <domain> Autenticação VPN CHAP Compatível com servidores VPN PPTP da Microsoft e outros. Impossível encontrar binário do cliente pptp. Impossível encontrar as informações secretas (ligação inválida, sem definições VPN). Impossível encontrar o binário do pppd. Nome D-Bus a usar para esta instância Padrão Não sair quando a ligaçãp VPN termina EAP Ativar índice personalizado para ppp<n> nome do dispositivo.
config:unidade<n> Ativar depurador descritivo (pode expor senhas) Gateway PPTP inválido ou em falta. MSCHAP MSCHAPv2 Gateway VPN em falta. Senha VPN inválida ou em falta. Nome de utilizador VPN inválido ou em falta. Opção necessária "%s" em falta. Domínio NT: Sem opções de configuração VPN. Sem segredos VPN! Sem credenciais em cache. Nota: encriptação MPPE encontra-se apenas disponível com métodos de autenticação MSCHAP. Para ativar esta caixa de seleção, selecione um ou mais métodos de autenticação MSCHAP: MSCHAP ou MSCHAPv2. PAP Opções avançadas PPTP Nome ou IP do servidor PPTP.
configuração: o primeiro parâmetro de pptp Senha transmitida ao PPTP quando é pedida. Palavra-passe: Protocolo de Tunneling ponto-a-ponto (PPTP) Requer a utilização de MPPE, com encriptação com 40 ou 128 bits ou os dois.
configuração: require-mppe, require-mppe-128 ou require-mppe-40 Envia os pedidos eco LCP a fim de saber se o cliente está ativo.
configuração: lcp-echo-failure e lcp-echo-interval Enviar pacotes de _Eco PPP Define o nome utilizado para autenticar o sistema local para comunicação com <nome>.
configuração: <nome> de utilizador Mostrar senha Usar compressão do cabeçal_ho TCP Usar encriptação _Ponto-a-Ponto (MPPE) Utilizar um número de  _Unidade personalizado: Utilizador: Tem de se autenticar para aceder à rede privada virtual(VPN) "%s". _Gateway: _Segurança: Impossível converter o endereço IP "%s" (%d) do gateway VPN PPTP Impossível procurar o endereço IP "%s" (%d) do gateway VPN PPTP propriedade booliana "%s" inválida (nem sim, nem não) gateway "%s" inválido propriedade inteira "%s" inválida nm-pptp-service fornece funcionalidades integradas de VPN PPTP ao NetworkManager (compatível com Microsoft e outras implementações). nenhum endereço utilizável obtido para o gateway VPN PPTP "%s" nenhum endereço utilizável obtido para o gateway VPN PPTP "%s" (%d) propriedade "%s" inválida ou não suportada propriedade "%s" de tipo %s não gerida 