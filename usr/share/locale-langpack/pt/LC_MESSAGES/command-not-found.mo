��          �   %   �      p  $   q     �     �  -   �  1   �  :   +      f     �  4   �  $   �  9   �     2  B   Q  _   �  8   �  -   -  f   [  W   �          5  -   S  1   �     �  /   �  8     #   :     ^  �  `  !   	  "   -	     P	  4   l	  6   �	  <   �	     
     -
  <   I
  1   �
  :   �
     �
  X     ]   k  @   �  2   
  t   =  i   �          ;  +   X  +   �      �  6   �  =     6   F     }                                                 
                                                                      	     Command '%s' from package '%s' (%s) %prog [options] <command-name> %s: command not found Ask your administrator to install one of them Command '%(command)s' is available in '%(place)s' Command '%(command)s' is available in the following places Do you want to install it? (N/y) Exception information: No command '%s' found, but there are %s similar ones No command '%s' found, did you mean: Please include the following information with the report: Python version: %d.%d.%d %s %d Sorry, command-not-found has crashed! Please file a bug report at: The command could not be located because '%s' is not included in the PATH environment variable. The program '%s' can be found in the following packages: The program '%s' is currently not installed.  This is most likely caused by the lack of administrative privileges associated with your user account. To run '%(command)s' please ask your administrator to install the package '%(package)s' Try: %s <selected package> You can install it by typing: You will have to enable component called '%s' You will have to enable the component called '%s' command-not-found version: %s don't print '<command-name>: command not found' ignore local binaries and display the available packages use this path to locate data fields y Project-Id-Version: command-not-found
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-20 16:54+0000
PO-Revision-Date: 2013-06-27 22:15+0000
Last-Translator: Filipe André Pinho <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:59+0000
X-Generator: Launchpad (build 18115)
  Comando '%s' do pacote '%s' (%s) %prog [opções] <nome-do-comando> %s: comando não encontrado Solicite ao seu administrador para instalar um deles Comando '%(command)s' está disponível em '%(place)s' Comando '%(command)s' está disponível nos seguintes locais Quer instalá-lo? (N/s) Informação da Excepção: O comando '%s' não foi encontrado mas existe %s semelhantes O comando '%s' não foi encontrado. Referia-se a: Por favor inclua a seguinte informação com o relatório: versão Python: %d.%d.%d %s %d *edimos desculpa mas o command-not-found teve um problema! Por favor reporte a falha em: O comando não foi localizado porque '%s' não está incluído na variável de ambiente PATH. A aplicação '%s' poderá ser encontrada nos seguintes pacotes: O programa '%s' não está instalado actualmente.  Provavelmente terá sido causado pela falta de privilégios administrativos associados a conta de utilizador actual. Para correr o '%(command)s', por favor solicite ao seu administrador para instalar o pacote '%(package)s' Tente: %s <pacote selecionado> Pode instalá-lo escrevendo: Terá que activar o componente chamado '%s' Terá que activar o componente chamado '%s' versão do command-not-found: %s não mostrar '<command-name>: comando não encontrado' ignorar os binários locais e mostrar os pacotes disponíveis Utilize este caminho para localizar os campos de dados s 