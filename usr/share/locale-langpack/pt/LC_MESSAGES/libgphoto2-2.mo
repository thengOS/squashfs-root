��    _                   4   	  	   >     H  �   M  �   �  '   �	  9   �	     �	     
     !
     6
     9
    <
     J     P     Y  k   a  A   �          &  "   9     \  <   {  <   �     �          -     L  L   Z     �  5   �  	   �     �     
  	     
   $  	   /     9  
   B  
   M     X  1   _     �     �     �     �  %   �     �     �     �     	       !   -  G   O     �     �     �  #   �     �  +   �                <     E     I     _     w     �     �     �     �     �     �     �     
            '   0  �  X  	   E     O  
   R     ]  n   c  +   �  r   �  N   q  Q   �  L     P   _  ?   �     �  "     N   %  �  t  J   8     �     �  �   �  �   R  '   *  @   R     �     �     �     �     �    �     �     �  
     }     E   �  #   �     �  %        <  I   Z  H   �  2   �  ,         M     m  c   {     �  F   �     @      R      d      x      �      �      �      �      �   
   �   1   �   "   !     ?!     S!     f!  /   v!     �!     �!     �!     �!     �!  &   �!  X   "     d"     l"     z"  0   �"     �"  2   �"     #  !   #     0#     8#     <#     V#     o#     �#     �#     �#     �#     �#     �#     $     $     $     ($  3   <$  8  p$  	   �'     �'     �'     �'  �   �'  7   R(  �   �(  N   )  Q   _)  L   �)  N   �)  A   M*     �*  1   �*  \   �*        !             &              *                 	   $             U   A   ,   C       E   <             @                         Q   "       M                  /      4                +           G   X   Z   
       I   7   :           R          .   #   Y       L       0   P       1   O   %   >   S   K      2   =       B      F   J   3      \      ^   -   8       [   ;      5       H   _      9   6   N             W   ?   (   V      ]       )              D   T   '    %s is a file type for which no thumbnail is provided *UNKNOWN* AUTO AX203 USB picture frame driver
Hans de Goede <hdegoede@redhat.com>
This driver allows you to download, upload and delete pictures
from the picture frame. AX203 based picture frames come with a variety of resolutions.
The gphoto driver for these devices allows you to download,
upload and delete pictures from the picture frame. Adc65
Benjamin Moos <benjamin@psnw.com> Aox generic driver
Theodore Kilgore <kilgota@auburn.edu>
 Auto focus: AI focus Auto focus: AI servo Auto focus: one-shot Av BW Barbie/HotWheels/WWF
Scott Fritzinger <scottf@unr.edu>
Andreas Meyer <ahm@spies.com>
Pete Zaitcev <zaitcev@metabyte.com>

Reverse engineering of image data by:
Jeff Laing <jeffl@SPATIALinfo.com>

Implemented using documents found on
the web. Permission given by Vision. Beach Beep off Beep on Camera appears to not be using CompactFlash storage
Unfortunately we do not support that at the moment :-(
 Camera has taken %d pictures, and is using CompactFlash storage.
 Camera unavailable: %s Compatibility Mode Configuration for your FUJI camera Could not create directory %s. Could not extract JPEG thumbnail from data: Data is not JFIF Could not extract JPEG thumbnail from data: No beginning/end Could not get disk info: %s Could not get disk name: %s Could not remove directory %s. Digital macro Don't know how to handle camera->port->type value %i aka 0x%x in %s line %i. Error capturing image FATAL ERROR: initial CRC value for length %d unknown
 Far scene Fast shutter File protected. Fireworks Flash auto Flash off Flash on Full Image Gray scale Indoor Internal error #1 in get_file_func() (%s line %i) Kids and pets Large Fine JPEG Large Normal JPEG Long shutter Lower case letters in %s not allowed. M Macro Manual focus Medium Fine JPEG Medium Normal JPEG NULL parameter "%s" in %s line %i Name '%s' (%li characters) too long, maximum 30 characters are allowed. Neutral Night scene Night snapshot No audio file could be found for %s No reason available Number of pictures: %i
Firmware Version: %s P Picture Frame Configuration Portrait RAW RAW + Large Fine JPEG RAW + Large Normal JPEG RAW + Medium Fine JPEG RAW + Medium Normal JPEG RAW + Small Fine JPEG RAW + Small Normal JPEG Sepia Slow shutter Small Fine JPEG Small Normal JPEG Snow Super macro Switching Camera Off Synchronize frame data and time with PC This is the driver for Canon PowerShot, Digital IXUS, IXY Digital,
 and EOS Digital cameras in their native (sometimes called "normal")
 mode. It also supports a small number of Canon digital camcorders
 with still image capability.
It includes code for communicating over a serial port or USB connection,
 but not (yet) over IEEE 1394 (Firewire).
It is designed to work with over 70 models as old as the PowerShot A5
 and Pro70 of 1998 and as new as the PowerShot A510 and EOS 350D of
 2005.
It has not been verified against the EOS 1D or EOS 1Ds.
For the A50, using 115200 bps may effectively be slower than using 57600
If you experience a lot of serial transmission errors, try to have your
 computer as idle as possible (i.e. no disk activity)
 Thumbnail Tv Underwater Vivid Your USB camera has an Aox chipset.
Number of lo-res PICs = %i
Number of hi-res PICs = %i
Number of PICs = %i
 Your USB picture frame has a AX203 chipset
 agfa_cl20
The Agfa CL20 Linux Driver People!
     Email us at cl20@poeml.de    
 Visit us at http://cl20.poeml.de  canon_int_capture_image: final canon_usb_list_all_dirs() failed with status %i canon_int_capture_image: initial canon_usb_list_all_dirs() failed with status %li canon_int_list_directory: ERROR: initial message too short (%i < minimum %i) canon_int_list_directory: Reached end of packet while examining the first dirent canon_int_list_directory: truncated directory entry encountered lock keys failed. unable to guess initial CRC value
 warning: CRC not checked (add len %d, value 0x%04x) #########################
 Project-Id-Version: libgphoto2
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2012-04-15 17:43+0200
PO-Revision-Date: 2010-12-21 13:15+0000
Last-Translator: Almufadado <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 16:59+0000
X-Generator: Launchpad (build 18115)
 %s é um tipo de ficheiro para o qual não foi fornecida nenhuma miniatura *DESCONHECIDO* AUTO Controlador de Moldura Digital USB com chipset AX203
Hans de Goede <hdegoede@redhat.com>
Este controlador permite transferir da, enviar para e apagar na 
moldura digital as suas imagens. As Molduras Digitais USB baseadas no chipset AX203 incluem
uma variedade de resoluções. O controlador gphoto para estes
dispositivos permite transferir da, enviar para e apagar na 
moldura digital as suas imagens. Adc65
Benjamin Moos <benjamin@psnw.com> Controlador genérico Aox
Theodore Kilgore <kilgota@auburn.edu>
 Auto-foco: Focagem IA Auto-foco: Motor IA Auto-foco: uma fotografia Av P&B Barbie/HotWheels/WWF
Scott Fritzinger <scottf@unr.edu>
Andreas Meyer <ahm@spies.com>
Pete Zaitcev <zaitcev@metabyte.com>

Engenharia reversa de dados de imagens por:
Jeff Laing <jeffl@SPATIALinfo.com>

Implementado usando documentos encontrados
na web. Permissão dada por Vision. Praia Bip desligado Bip ligado A camera aparenta não estar a usar o armazenamento CompactFlash
Infelizmente nós não suportamos o mesmo nesse momento :-(
 A camera tirou %d fotos e está a usar o armazenamento CompactFlash.
 A camera não está disponível: %s Modo de Compatibilidade Configuração para a sua Camera FUJI Impossível criar arquivo %s. Não foi possível extrair miniatura JPEG dos dados: Dados não são JFIF Não foi possível extrair uma miniatura JPEG dos dados: Sem começo/fim Não foi possível obter informação do disco: %s Não foi possível obter o nome do disco: %s Impossível remover arquivo %s. Macro digital Não sei como lidar com camera->porta->tipo valor %i também conhecido como 0x%x em %s na linha %i. Erro ao capturar a imagem ERRO FATAL: o valor inicial CRC para o comprimento %d é desconhecido
 Paisagem distante Obturador rápido Ficheiro protegido. Fogos de artifício Flash automático Flash desligado Flash ligado Imagem Completa Escala de cinzentos Interiores Internal error #1 in get_file_func() (%s line %i) Crianças e Animais de estimação JPEG Óptimo Grande JPEG Normal Grande Obturador longo Não são permitidas letras minúsculas em %s . M Macro Focagem manual JPEG Óptimo Médio JPEG Normal Médio Parâmetro NULL "%s" em %s na linha %i Nome '%s' (%li caracteres) é demasiado longo; são permitidos no máximo 30 caracteres. Neutras Cena nocturna Fotografia noturna Nenhum ficheiro de áudio foi encontrado para %s Nenhuma razão atribuível Número de fotografias: %i
Versão de firmware: %s P Configuração de Moldura Digital Retrato RAW RAW + JPEG Óptimo Grande RAW + JPEG Normal Grande RAW + JPEG Óptimo Médio RAW + JPEG Normal Médio RAW + JPEG Óptimo Pequeno RAW + JPEG Normal Pequeno Sépia Obturador Lento JPEG Óptimo Pequeno JPEG Normal Pequeno Neve Super macro Desligando a Camera Sincronizar a data/hora da Moldura Digital com o PC Este é o driver para cameras Canon PowerShot, IXUS digitais, IXY digitais,
 e EOS digitais no seu formato nativo (algumas vezes chamados "normal").
 Também suporta um pequeno número de Camcorders digitais Canon com 
 capacidade de fotos.
Inclui código para comunicação através de porta série ou conexão USB,
 mas não (ainda) sobre conexão IEEE 1394 (Firewire).
É projectado para trabalhar com mais de 70 modelos tão antigos quanto a 
 PowerShot A5 e a Pro70 de 1998 e tão novos quanto a PowerShot A510
e a EOS 350D de 2005.
Não foi verificado com a EOS 1D nem com a EOS 1Ds.
Para a A50, utilizando 115200 bps pode ser efectivamente mais lento que 
utilizando 57600
Se ocorrerem erros de transmissão pela porta série, tente manter o seu 
 computador sem actividades a decorrer (ex. sem actividades de disco)
 Miniatura Tv Subaquático Vivas A tua camera tem um chipset Aox
Número de PICS de baixa resolução = %i
Número de PICS de alta resolução = %i
Número de PICS = %i
 A sua Moldura Digital USB está a usar o chipset AX203
 agfa_cl20
Os criadores do controlador Agfa CL20 para Linux!
     Contacta-nos: cl20@poeml.de    
 Visita-nos em http://cl20.poeml.de  canon_int_capture_image: final canon_usb_list_all_dirs() failed with status %i canon_int_capture_image: initial canon_usb_list_all_dirs() failed with status %li canon_int_list_directory: ERROR: initial message too short (%i < minimum %i) canon_int_list_directory: Atingi o fim do pacote ao examinar o primeiro dirent canon_int_list_directory: encontrado nome de directório truncado As chaves de bloqueio falharam. não foi possível adivinhar o valor CRC inicial
 AVISO: o CRC não foi verificado (adicionar tam %d, valor 0x%04x) #########################
 