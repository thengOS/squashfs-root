��    1      �  C   ,      8     9  b   H     �  	   �     �     �  
   �     �     �     �     �     �                    &  	   /     9     ?     M     [     b     w     ~     �     �     �     �     �     �  -   �  +        ;     D     S  2   X     �     �  �   �  �   �  �   d  �   F	     *
     /
  	   4
     >
     F
  	   N
  �  X
     �  s   
     ~  
   �     �     �  
   �     �     �     �     �     �     �     �                    3     ?     R     a      m     �     �     �     �     �     �     �  %   �  =     ;   Z     �     �     �  G   �             �     �   �  �   �  �   �  
   �     �  	   �     �     �  	   �         #                     &   -      %                    	          /              '       $          "                                    !   0       ,         +      1   .      
   *         (              )                           %s x %s pixels <b>This photo is missing.</b>
You can open Shotwell to retrieve it or remove it from your library. Album By %s, %s Camera Date Dimensions Email Facebook Flickr Friends Photos Last 30 days Last 6 months Last 7 days License Location My Photos Older Online Photos Open Shotwell Photos Photos search plugin Picasa Print Recent Search Facebook Search Flickr Search Picasa Search Shotwell Search your Picasa photos Search your and your friends' Facebook photos Search your and your friends' Flickr photos Shotwell Show in Folder Size Sorry, there are no photos that match your search. Tags This Computer This is an Ubuntu search plugin that enables information from Facebook to be searched and displayed in the Dash underneath the Photos header. If you do not wish to search this content source, you can disable this search plugin. This is an Ubuntu search plugin that enables information from Flickr to be searched and displayed in the Dash underneath the Photos header. If you do not wish to search this content source, you can disable this search plugin. This is an Ubuntu search plugin that enables information from Picasa to be searched and displayed in the Dash underneath the Photos header. If you do not wish to search this content source, you can disable this search plugin. This is an Ubuntu search plugin that enables information from Shotwell to be searched and displayed in the Dash underneath the Photos header. If you do not wish to search this content source, you can disable this search plugin. View With facebook; flickr; picasa; shotwell; Project-Id-Version: unity-lens-photos
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-18 15:04+0000
PO-Revision-Date: 2014-04-04 09:36+0000
Last-Translator: Paulo Moniz <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:19+0000
X-Generator: Launchpad (build 18115)
 %s x %s pixels <b>Esta fotografia não foi encontrada.</b>
Poderá abrir o Shotwell para a recuperar ou remover da sua biblioteca. Álbum Por %s, %s Câmara Data Dimensões Email Facebook Flickr Fotografias de amigos Últimos 30 dias Últimos 6 meses Últimos 7 dias Licença Localização As minhas fotografias Mais antigo Fotografias online Abrir Shotwell Fotografias Plugin de procura de fotografias Picasa Imprimir Recente Procurar no Facebook Procurar no Flickr Procurar no Picasa Procurar no Shotwell Procure as suas fotografias do Picasa Procure as suas fotografias do Facebook e as dos seus amigos. Procure as suas fotografias do Flickr e as dos seus amigos. Shotwell Mostrar na pasta Tamanho Desculpe, mas não existem fotografias que coincidam com a sua procura. Marcas Este computador Isto é um plugin de pesquisa do Ubuntu que permite que a informação do Facebook seja pesquisada e apresentada no Dash logo abaixo do cabeçalho das Fotografias. Se não quiser fazer estas pesquisas pode desativar o plugin. Isto é um plugin de pesquisa do Ubuntu que permite que a informação do Flickr seja pesquisada e apresentada no Dash logo abaixo do cabeçalho das Fotografias. Se não quiser fazer estas pesquisas pode desativar o plugin. Isto é um plugin de pesquisa do Ubuntu que permite que a informação do Picasa seja pesquisada e apresentada no Dash logo abaixo do cabeçalho das Fotografias. Se não quiser fazer estas pesquisas pode desativar o plugin. Isto é um plugin de pesquisa do Ubuntu que permite que a informação do Shotwell seja pesquisada e apresentada no Dash logo abaixo do cabeçalho das Fotografias. Se não quiser fazer estas pesquisas pode desativar o plugin. Visualizar Com facebook; flickr; picasa; shotwell; 