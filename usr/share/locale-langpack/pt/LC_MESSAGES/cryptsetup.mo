��    *      l  ;   �      �     �     �     �     �     �               5     :     O     i     }     �  ?   �  4   �       %   <  &   b     �     �     �  "   �  6   �  >        W     n  +   �  6   �     �  .         /     F     f  ,   |     �     �     �     �     �  (   �  1     �  Q     �	     �	     
      &
  %   G
     m
     t
     �
     �
  "   �
     �
     �
     �
  8   	  >   B  .   �  -   �  ,   �          &     /  #   N  7   r  H   �     �  #     ;   0  <   l     �  ,   �  #   �  &        7  3   S     �  !   �      �     �     �  ;     B   =                                                                    
   '          )   *                                          "      %                $          !   #   &              	         (           
<action> is one of:
 %s: requires %s as arguments <device> <device> <key slot> <device> [<new key file>] <name> Argument <action> missing. BITS Command successful.
 Create a readonly mapping Display brief usage Do not ask for confirmation Help options: How many sectors of the encrypted data to skip at the beginning How often the input of the passphrase can be retried Key %d not active. Can't wipe.
 Key size must be a multiple of 8 bits PBKDF2 iteration time for LUKS (in ms) Print package version SECTORS Show this help message Shows more detailed error messages The cipher used to encrypt the disk (see /proc/crypto) The hash used to create the encryption key from the passphrase The size of the device The size of the encryption key This will overwrite data on %s irrevocably. Timeout for interactive passphrase prompt (in seconds) Unknown action. Verifies the passphrase by asking for it twice add key to LUKS device dump LUKS partition information formats a LUKS device memory allocation error in action_luksFormat msecs print UUID of LUKS device resize active device secs show device status tests <device> for LUKS partition header wipes key with number <key slot> from LUKS device Project-Id-Version: cryptsetup
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-08-10 16:19+0200
PO-Revision-Date: 2011-04-16 21:04+0000
Last-Translator: João Mouro <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:25+0000
X-Generator: Launchpad (build 18115)
 
<acção>:
 %s: requer %s como argumentos <dispositivo> <dispositivo> <ranhura da chave> <dispositivo> [<novo ficheiro chave>] <nome> Argumento <acção> em falta. BITS Comando bem sucedido.
 Criar um mapeamento só de leitura Mostrar uso Não pedir confirmação Opções de ajuda: Quantos sectores dos dados encriptados saltar ao início Número de tentativas permitido para a escrita da frase-passe. Chave %d inactiva. Não foi possível apagar.
 Tamanho da chave deve ser múltiplo de 8 bits Tempo de iteração PBKDF2 para LUKS (em ms) Imprimir versão do pacote SECTORES Mostrar esta mensagem de ajuda Mostra mensagens de erro detalhadas A cifra usada para encriptar o disco (ver /proc/crypto) A hash usada para criar a chave de encriptação a partir da frase-passe O tamanho do dispositivo O tamanho da chave de encriptação Isto vai sobrescrever os dados em %s de forma irrevogável. Tempo máximo de espera para linha de comando da frase-passe Acção desconhecida. Verificar a frase-passe pedindo-a duas vezes adicionar chave ao dispositivo LUKS mostar informação da partição LUKS formata um dispositivo LUKS erro de alocação de memória em action_luksFormat milissegundos imprimir UUID do dispositivo LUKS redimensionar dispositivo activo segundos mostrar o estado do dispositivo testar o <dispositivo> para o cabeçalho de partição LUKS apaga a chave com o número <ranhura da chave> do dispositivo LUKS 