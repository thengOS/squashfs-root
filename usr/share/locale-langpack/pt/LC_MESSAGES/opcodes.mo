��    ,      |  ;   �      �     �     �     �     �  	   �     �          &     ?     Q  !   i     �     �     �     �  "   �  #        8     U  !   g  "   �  #   �     �  !   �          "     @     ^  "   |     �     �     �     �          (     <  ,   H     u     �  -   �     �  	   �     �  �       �	     �	  
   �	     �	     �	     �	     
     
     7
     K
  '   b
     �
  "   �
     �
  #   �
  -     #   5  "   Y     |  #   �  -   �  %   �  )   
  (   4     ]  #   w  #   �  #   �  $   �  %        .     N     l     �     �  	   �  1   �     �     	  9   )     c  
   �     �         $   "              )      #      (         '   +                             *                     ,              
      &                        	           !       %                                  
 $<undefined> %s: Error:  %s: Warning:  (unknown) <illegal precision> <unknown register %d> Bad immediate expression Bad register name Don't understand 0x%x 
 Immediate is out of range -8 to 7 Internal disassembler error Invalid size specifier Operand is not a symbol Register list is not valid Register must be between r0 and r7 Register must be between r8 and r15 Register number is not valid Unknown error %d
 Value of A operand must be 0 or 1 attempt to read writeonly register auxiliary register not allowed here bad jump flags value class %s is defined but not used
 illegal use of parentheses immediate is out of range 1-2 immediate is out of range 1-8 immediate is out of range 2-9 immediate value cannot be register immediate value is out of range immediate value out of range invalid conditional option invalid register number `%d' invalid sprg number junk at end of line missing `]' operand out of range (not between 1 and 255) st operand error store value must be zero syntax error (expected char `%c', found `%c') too many long constants undefined unrecognized instruction Project-Id-Version: binutils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2014-07-26 18:31+0000
Last-Translator: Alexandre Fidalgo <alexandremagnos15@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:04+0000
X-Generator: Launchpad (build 18115)
 
 $<indefinido> %s: Erro:  %s: Aviso:  (desconhecido) <precisão ilegal> <registo desconhecido %d> Má expressão imediata Mau nome de registo Não compreende 0x%x 
 Imeditado está fora do domínio -8 a 7 Erro interno do desmontador Especificador de tamanho inválido Operando não é símbolo A lista de registos não é válida Número de registo tem de estar entre r0 e r7 O registo deve estar entre r8 e r15 Número de registo não é válido Erro desconhecido %d
 Valor do operando A deve ser 0 ou 1 tentativa de ler um registo apenas de escrita registo auxiliar não autorizado aqui valores de sinalizadores de salto errados classe %s está definida mas não usada
 uso ilegal de parêntises imediato está fora do domínio 1-2 imediato está fora do domínio 1-8 imediato está fora do domínio 2-9 Valor imediato não pode ser registo Valor imediato está fora do domínio Valor imediato fora do domínio opção condicional inválida registo número `%d' inválido número sprg inválido lixo no final da linha falta `]' opeando fora do limite (não está entre 1 e 255) erro no operador st o valor a guardar deve ser zero erro de sintaxe (caracter esperado `%c', encontrado `%c') demasiadas constantes longas indefinido instrução não reconhecida 