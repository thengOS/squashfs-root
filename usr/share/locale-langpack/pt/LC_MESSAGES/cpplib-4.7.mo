��          �            h  '   i     �     �     �  9   �  /   &  %   V  
   |  (   �  5   �  2   �  C     6   ]  7   �  �  �  +   i     �     �      �  G   �  4   >  7   s     �  +   �  9   �  4     G   R  :   �  @   �                   
                           	                                  %.*s is not a valid universal character '$' in identifier or number Character %x might not be NFKC In _cpp_valid_ucn but not a UCN character 0x%lx is not in the basic source character set
 conversion from %s to %s not supported by iconv converting to execution character set iconv_open incomplete universal character name %.*s no iconv implementation, cannot convert from %s to %s the meaning of '\%c' is different in traditional C universal character %.*s is not valid at the start of an identifier universal character %.*s is not valid in an identifier universal character names are only valid in C++ and C99 Project-Id-Version: gcc-4.7
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2012-01-27 16:12+0000
PO-Revision-Date: 2014-06-06 15:10+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:08+0000
X-Generator: Launchpad (build 18115)
 %.*s não é um carácter universal válido '$' no identificador ou número Carácter %x pode não ser NFKC Em_cpp_valid_ucn mas não um UCN carácter 0x%lx não está no conjunto básico de caracteres de origem
 conversão de %s para %s não é suportada por iconv a converter para a execução do conjunto de caracteres iconv_abrir nome de carácter universal incompleto %.*s iconv não implementado, não pode converter de%s para %s o significado de '\%c' é diferente no tradicional C carácter universal %.*s não é válido no início de um identificador carácter universal %.*s não é válido num identificador nomes de caracteres universais são válidos apenas em C++ e C99 