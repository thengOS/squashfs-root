��             +         �     �     �     �  "   �               '     +     0     9  (   >     g     m     t     �     �     �     �     �     �     �               7  /   H     x     �  )   �     �     �     �  �       �     �  ,   �  3   �               0     4     9     B  +   G     s     y     �     �     �     �     �     �  5   �  !   $     F     `     ~  8   �  #   �     �  5   
	     @	     S	     e	                                                                 	                 
                                                              ARG DOUBLE Display brief usage message Display option defaults in message FLOAT Help options: INT LONG LONGLONG NONE Options implemented via popt alias/exec: SHORT STRING Show this help message Terminate options Usage: VAL [OPTION...] aliases nested too deeply config file failed sanity test error in parameter quoting invalid numeric value memory allocation failed missing argument mutually exclusive logical operations requested number too large or too small opt->arg should not be NULL option type (%u) not implemented in popt
 unknown errno unknown error unknown option Project-Id-Version: popt 1.11
Report-Msgid-Bugs-To: <popt-devel@rpm5.org>
POT-Creation-Date: 2010-02-17 13:35-0500
PO-Revision-Date: 2014-04-30 16:36+0000
Last-Translator: IvoGuerreiro <ivoguerreiro@gmail.com>
Language-Team: pt <morais@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:04+0000
X-Generator: Launchpad (build 18115)
 ARG DOUBLE Mostrar uma mensagem de utilização sucinta Mostrar valor por omissão das opções na mensagem FLOAT Opções de ajuda: INT LONG LONGLONG NONE Opções implementadas via popt alias/exec: SHORT STRING Mostrar esta mensagem de ajuda Terminar opções Utilização: VAL [OPÇÃO...] 'aliases' demasiado aninhados arquivo de configuração falhou no teste de sanidade erros no 'quoting' de parâmetros valor númerico inválido alocação de memória falhou falta um argumento foram pedidas operações lógicas mutuamente exclusivas número demasiado grando ou pequeno opt->arg não deve ser NULL Tipo de opção (%u) não está implementado no popt
 errno desconhecido erro desconhecido opção desconhecida 