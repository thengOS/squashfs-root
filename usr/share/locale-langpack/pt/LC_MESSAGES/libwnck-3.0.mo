��    �      \  7  �      x     y     ~  	   �     �     �     �     �     �  "   �     �     �     �                7     J     `     r     �     �  N   �  g   �  G   S  :   �  E   �  A     *   ^  *   �  )   �  (   �  7     s   ?  '   �  ?   �  <        X     l     �  ,   �  $   �     �            /   .  8   ^  \   �  b   �  d   W  ]   �  d     f     b   �  m   I  `   �       "   0  !   S  /   u     �     �     �  
   �  >   �  G     5   `     �     �  `   �  G   &  	   n     x     �      �  #   �  #   �        5   (   !   ^   $   �   '   �   '   �   9   �   $   /!  )   T!  5   ~!      �!     �!     �!  	   "     "      "  <   4"  <   q"  :   �"     �"     #     #     5#     M#     c#     h#  )   o#  ,   �#  	   �#     �#     �#     �#  #   $     5$  %   G$  (   m$  (   �$  +   �$     �$  )   �$     %  �   4%     &  	   &     )&     <&     L&  *   ]&  -   �&  -   �&  0   �&     '     &'  (   ?'  *   h'     �'  
   �'     �'     �'  &   �'     �'  !   (     4(     F(     Z(     j(  "   v(     �(      �(     �(     �(     �(     )     )  L   *)  8   w)     �)     �)     �)     �)     �)     *  E   *     [*     h*     w*  :   �*     �*     �*     �*     �*  <   �*  .   <+     k+  	   o+     y+     {+     �+  
   �+     �+     �+     �+     �+     �+     �+     �+     �+     ,     #,     ),     1,     ?,     M,  
   S,  
   ^,  
   i,     t,     },     �,  	   �,     �,     �,     �,  	   �,     �,     �,     -     -     -     --     1-     8-     ?-     C-     I-  
   P-     [-     i-     w-     �-     �-     �-     �-     �-     �-     �-  
   �-     �-     �-  
   .     .     .     !.     ).     8.     D.  �  S.     P0     U0  	   b0     l0     t0     }0     �0     �0  *   �0     �0     �0     �0     1     1      1     21     E1     X1     h1     ~1  X   �1  W   �1  R   62  :   �2  I   �2  D   3  '   S3  '   {3     �3  #   �3  2   �3  t   4  (   �4  L   �4  K   5     P5     k5     �5  /   �5     �5  $   �5     6     /6  *   =6  2   h6  a   �6  f   �6  m   d7  _   �7  [   28  ]   �8  b   �8  l   O9  b   �9     :  $   3:  $   X:  2   }:     �:     �:     �:     �:  D   �:  D   3;  >   x;     �;     �;  e   �;  G   R<  
   �<     �<     �<     �<  &   �<  0   =  -   H=  =   v=  )   �=  #   �=  +   >  :   .>  B   i>  %   �>  -   �>  0    ?      1?     R?     c?  
   �?     �?     �?  2   �?  2   �?  =   @     S@     m@     �@     �@     �@     �@     �@  &   �@  (   A  	   >A     HA     ^A     vA  %   �A     �A  (   �A  1   �A  3    B  3   TB     �B  2   �B     �B  �   �B     �C     �C     �C     D     D  6   $D  <   [D  >   �D  A   �D     E     )E  4   CE  7   xE     �E     �E     �E     �E  0   �E  &   F  &   EF     lF     �F     �F  
   �F      �F     �F     �F  
   G     G     #G     =G     UG  W   hG  A   �G  (   H     +H     3H     DH     [H     mH  C   �H  
   �H     �H     �H  F   �H     0I     EI     ]I     sI  7   uI  .   �I     �I  	   �I     �I     �I     J     J     J     %J     ;J     JJ     [J     aJ     sJ     zJ     �J     �J     �J     �J     �J     �J     �J     �J     �J  	   K     K     2K  
   JK     UK     pK  	   �K  
   �K     �K     �K     �K     �K     �K     �K     �K     �K     �K     L  	   L     L     .L     GL     ZL     aL     gL     nL     L  
   �L     �L     �L  	   �L     �L     �L  	   	M  
   M     M     0M     9M     OM     WM     �       x   &   }   R   �   �   	   �   u       �   �           �          �               T   �       �   P       �   �   �          �       .   �   �   �      �          =       *           �   a   j   4   �   b   ?   h      �           �       1      �          �       �   �   l   W   B       �   �       �   �   o   "   7          %      \      L   I       �   ;   �   [   n      z       r       �   �   �   A   J               g          S           M   �       �   -   $   O   �   >   �   �   �   d   p          (   �       {       �   ~   '   V   G   #       +   �       �       �               �      �   i           �   9   0       H       v       �       �           �           �       �   �       �          /       Z       �   �   �   �   6   �   �      m   U   �   c   @   q   �   �          �   s   �   �   �      �   �   )   �   �   �      |   ^      <              
   �       N   �   �   �   �       �   :          �   t   �   �       X   �   �   5   Y   �   �   ]   !   ,       C       Q   �                   k   �   �   w                     �   K   �          �   8   �   �   �       �          `   _   E      2   f   y   �      �   3   �   F           �   D      e   �       �   �   �    "%s" %1$s%2$s%3$s %d ("%s") %d: %s
 %lu (%s) %lu: %s
 ,  <name unset> <no EWMH-compliant window manager> <no viewport> <unset> Action not allowed
 Activate the window Activate the workspace Active Window: %s
 Active Workspace: %s
 Alias of --window Always On _Top Bottom Neighbor: %s
 CLASS Cannot change the workspace layout on the screen: the layout is already owned
 Cannot interact with application having its group leader with XID %lu: the application cannot be found
 Cannot interact with class group "%s": the class group cannot be found
 Cannot interact with screen %d: the screen does not exist
 Cannot interact with window with XID %lu: the window cannot be found
 Cannot interact with workspace %d: the workspace cannot be found
 Change the X coordinate of the window to X Change the Y coordinate of the window to Y Change the height of the window to HEIGHT Change the name of the workspace to NAME Change the number of workspaces of the screen to NUMBER Change the type of the window to TYPE (valid values: normal, desktop, dock, dialog, toolbar, menu, utility, splash) Change the width of the window to WIDTH Change the workspace layout of the screen to use NUMBER columns Change the workspace layout of the screen to use NUMBER rows Class Group ID: %s
 Class Group Name: %s
 Class Instance: %s
 Class resource of the class group to examine Click this to switch to workspace %s Click to start dragging "%s" Click to switch to "%s" Close the window Conflicting options are present: --%s and --%s
 Conflicting options are present: --%s or --%s, and --%s
 Conflicting options are present: a window should be interacted with, but --%s has been used
 Conflicting options are present: an application should be interacted with, but --%s has been used
 Conflicting options are present: class group "%s" should be interacted with, but --%s has been used
 Conflicting options are present: screen %d should be interacted with, but --%s has been used
 Conflicting options are present: windows of an application should be listed, but --%s has been used
 Conflicting options are present: windows of class group "%s" should be listed, but --%s has been used
 Conflicting options are present: windows of workspace %d should be listed, but --%s has been used
 Conflicting options are present: windows or workspaces of screen %d should be listed, but --%s has been used
 Conflicting options are present: workspace %d should be interacted with, but --%s has been used
 Current workspace: "%s" Error while parsing arguments: %s
 Geometry (width, height): %d, %d
 Geometry (x, y, width, height): %d, %d, %d, %d
 Group Leader: %lu
 HEIGHT Icon Name: %s
 Icons: %s
 Invalid argument "%d" for --%s: the argument must be positive
 Invalid argument "%d" for --%s: the argument must be strictly positive
 Invalid argument "%s" for --%s, valid values are: %s
 Invalid value "%s" for --%s Left Neighbor: %s
 List windows of the application/class group/workspace/screen (output format: "XID: Window Name") List workspaces of the screen (output format: "Number: Workspace Name") Ma_ximize Ma_ximize All Make the window always on top Make the window appear in pagers Make the window appear in tasklists Make the window below other windows Make the window fullscreen Make the window have a fixed position in the viewport Make the window not always on top Make the window not appear in pagers Make the window not appear in tasklists Make the window not below other windows Make the window not have a fixed position in the viewport Make the window quit fullscreen mode Make the window visible on all workspaces Make the window visible on the current workspace only Maximize horizontally the window Maximize the window Maximize vertically the window Mi_nimize Mi_nimize All Minimize the window Move the viewport of the current workspace to X coordinate X Move the viewport of the current workspace to Y coordinate Y Move the window to workspace NUMBER (first workspace is 0) Move to Another _Workspace Move to Workspace R_ight Move to Workspace _Down Move to Workspace _Left Move to Workspace _Up NAME NUMBER NUMBER of the screen to examine or modify NUMBER of the workspace to examine or modify Name: %s
 No Windows Open Number of Windows: %d
 Number of Workspaces: %d
 On Screen: %d (Window Manager: %s)
 On Workspace: %s
 Options to list windows or workspaces Options to modify properties of a screen Options to modify properties of a window Options to modify properties of a workspace PID: %s
 Position in Layout (row, column): %d, %d
 Possible Actions: %s
 Print or modify the properties of a screen/workspace/window, or interact with it, following the EWMH specification.
For information about this specification, see:
	http://freedesktop.org/wiki/Specifications/wm-spec Right Neighbor: %s
 Role: %s
 Screen Number: %d
 Session ID: %s
 Shade the window Show options to list windows or workspaces Show options to modify properties of a screen Show options to modify properties of a window Show options to modify properties of a workspace Show the desktop Showing the desktop: %s
 Start moving the window via the keyboard Start resizing the window via the keyboard Startup ID: %s
 State: %s
 Stop showing the desktop TYPE Tool to switch between visible windows Tool to switch between windows Tool to switch between workspaces Top Neighbor: %s
 Transient for: %lu
 Un_minimize All Unma_ximize Unmaximize horizontally the window Unmaximize the window Unmaximize vertically the window Unmi_nimize Unminimize the window Unshade the window Untitled application Untitled window Viewport cannot be moved: the current workspace does not contain a viewport
 Viewport cannot be moved: there is no current workspace
 Viewport position (x, y): %s
 WIDTH Window List Window Manager: %s
 Window Selector Window Type: %s
 Window cannot be moved to workspace %d: the workspace does not exist
 Workspace %d Workspace %s%d Workspace 1_0 Workspace Layout (rows, columns, orientation): %d, %d, %s
 Workspace Name: %s
 Workspace Number: %d
 Workspace Switcher X X window ID of the group leader of an application to examine X window ID of the window to examine or modify XID XID: %lu
 Y _Always on Visible Workspace _Close _Close All _Move _Only on This Workspace _Resize _Unmaximize All above all workspaces below change fullscreen mode change workspace close desktop dialog window dock or panel false fullscreen make above make below maximize maximize horizontally maximize vertically maximized maximized horizontally maximized vertically minimize minimized move needs attention no action possible normal normal window pin pinned resize set shade shaded skip pager skip tasklist splash screen startupIDnone stick sticky tearoff menu tearoff toolbar true unmake above unmake below unmaximize unmaximize horizontally unmaximize vertically unminimize unpin unshade unstick utility window windownone workspacenone Project-Id-Version: 3.2
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libwnck&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-04-15 12:52+0000
PO-Revision-Date: 2016-04-16 01:43+0000
Last-Translator: Pedro Albuquerque <Unknown>
Language-Team: Pedro Albuquerque
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:02+0000
X-Generator: Launchpad (build 18115)
X-Project-Style: gnome
Language: pt
 "%s" %1$s%2$s%3$s %d ("%s") %d: %s
 %lu (%s) %lu: %s
 e  <nome por definir> <gestor de janelas não cumpre norma EWMH> <sem vista de desktop> <por definir> Ação não permitida
 Ativar janela Ativar desktop Janela ativa: %s
 Desktop Ativo: %s
 Aliás de --window Sempre no _topo Vizinho inferior: %s
 CLASSE Incapaz de alterar a disposição do desktops no ecrã: a disposição já é possuída
 Impossível interagir com a app cujo líder de grupo é o XID %lu: app não encontrada
 Impossível interagir com o grupo de classe "%s": grupo de classe não encontrado
 Impossível interagir com o ecrã %d: o ecrã não existe
 Impossível interagir com a janela com o XID %lu: janela não encontrada
 Incapaz de interagir com o desktop %d: incapaz de encontrar a área
 Alterar a coordenada X da janela para X Alterar a coordenada Y da janela para Y A altura da janela para ALTURA Alterar o nome do desktop para NOME Alterar o número de desktop do ecrã para NÚMERO Alterar o tipo da janela para TIPO (valores válidos: normal, desktop, dock, dialog, toolbar, menu, utility, splash) Alterar a largura da janela para LARGURA Alterar a disposição dos desktops do ecrã para utilizarem NÚMERO colunas Alterar a disposição dos desktops do ecrã para utilizarem NÚMERO linhas ID do grupo de classe: %s
 Nome do grupo de classe: %s
 Instância da classe: %s
 Recurso de classe do grupo de classe a examinar Clique para ir para desktop %s Clique para começar a arrastar "%s" Clique para ir para "%s" Fechar janela Existem opções em conflito: --%s e --%s
 Existem opções em conflito: --%s ou --%s e --%s
 Existem opções em conflito: uma janela deveria ser alvo de interação, mas foi utilizado --%s
 Existem opções em conflito: uma aplicação deveria ser alvo de interação, mas foi utilizado --%s
 Existem opções em conflito: o grupo de classe "%s" deveria ser alvo de interação, mas foi utilizado --%s
 Existem opções em conflito: ecrã %d deveria ser alvo de interação, mas foi utilizado --%s
 Existem opções em conflito: janelas de uma app deveriam ser listadas, mas foi usado --%s
 Existem opções em conflito: o grupo de classe "%s" deveria ser listado, mas foi usado --%s
 Existem opções em conflito: janelas do desktop %d deveriam ser listadas, mas foi utilizado --%s
 Existem opções em conflito: janelas ou desktops do ecrã %d deveriam ser listadas, mas foi utilizado --%s
 Existem opções em conflito: desktops %d deveria ser alvo de interação, mas foi utilizado --%s
 Desktop atual: "%s" Erro ao processar os argumentos: %s
 Geometria (largura, altura): %d, %d
 Geometria (x, y, largura, altura): %d, %d, %d, %d
 Líder do grupo: %lu
 ALTURA Nome do ícone: %s
 Ícones: %s
 Argumento "%d" inválido para --%s: o argumento tem de ser positivo
 Argumento "%d" inválido para --%s: o argumento tem de ser positivo
 Argumento "%s" inválido para --%s, valores válidos são: %s
 Valor "%s" inválido para --%s Vizinho esquerdo: %s
 Listar janelas do grupo/desktop/ecrã da aplicação/classe (formato da lista: "XID: Nome da janela") Listar desktops do ecrã (formato da lista: "Número: Nome do desktop") Ma_Ximizar Ma_ximizar todas Colocar a janela sempre no topo Mostrar a janela no pager Mostrar a janela nas listas de tarefas Colocar a janela sempre abaixo de outras janelas Expandir a janela para modo de ecrã completo Forçar a janela a ter uma posição fixa na vista de desktop Deixar de colocar a janela sempre no topo Não mostrar a janela no alternador Não mostrar a janela nas listas de tarefas Deixar de colocar a janela sempre abaixo de outras janelas Não forçar a janela a ter uma posição fixa na vista de desktop Sair do modo ecrã completo da janela Tornar a janela visível em todos os desktops Tornar a janela visível apenas no desktop atual Maximizar janela horizontalmente Maximizar janela Maximizar janela verticalmente Mi_Nimizar Mi_nimizar todas Minimizar janela Mover a vista do desktop atual para X coordenada X Mover a vista do desktop atual para Y coordenada Y Mover a janela para o desktop NÚMERO (primeira área é a 0) Mover para outro _desktop Mover para desktop à _direita Mover para desktop a_baixo Mover para desktop à _esquerda Mover para desktop _Acima NOME NÚMERO NÚMERO do ecrã a examinar ou alterar NÚMERO de desktop a examinar ou alterar Nome: %s
 Nenhuma janela aberta Número de janelas: %d
 Número de desktops: %d
 No ecrã: %d (Gestor de janelas: %s)
 No desktop: %s
 Opções para listar janelas ou desktops Opções para alterar as propriedades de um ecrã Opções para alterar as propriedades de uma janela Opções para alterar as propriedades de um desktop PID: %s
 Posição na disposição (linha, coluna): %d, %d
 Ações possíveis: %s
 Imprimir ou alterar as propriedades de um ecrã/área de trabalho/janela ou interagir com ela, segundo a especificação EWMH.
Para informações sobre esta especificação, consulte:
	http://freedesktop.org/wiki/Specifications/wm-spec Vizinho direito: %s
 Função: %s
 Número do ecrã: %d
 ID de sessão: %s
 Sombrear janela Apresentar as opções para listar janelas ou desktops Mostrar as opções para alterar as propriedades de um ecrã Mostrar as opções para alterar as propriedades de uma janela Apresentar as opções para alterar as propriedades de um desktop Mostrar desktop A apresentar desktop: %s
 Iniciar a movimentação da janela usando o teclado. Iniciar o redimensionamento da janela usando o teclado. ID de início: %s
 Estado: %s
 Parar de mostrar desktop TIPO Ferramenta para alternar entre janelas visíveis Ferramenta para alternar entre janelas Ferramenta para alternar entre desktop Vizinho superior: %s
 Transitório para: %lu
 _Restaurar todas Resta_Urar Restaurar janela horizontalmente Restaurar janela Restaurar janela verticalmente Resta_Urar Restaurar janela Deixar de sombrear janela Aplicação sem título Janela sem título A vista de área não pode ser movida: o desktop atual não contém uma vista de área
 A vista de área não pode ser movida: não existe desktop atual
 Posição da vista de desktop (x,y): %s
 LARGURA Lista de janelas Gestor de janelas: %s
 Seletor de janela Tipo de janela: %s
 Incapaz de mover a janela para o desktop %d: o desktop não existe
 Desktop %d Desktop %s%d Desktop 1_0 Disposição dos desktops (linhas, colunas, orientação): %d, %d, %s
 Nome do desktop: %s
 Número do desktop: %d
 Alternador do desktop X ID de janela X do líder de grupo de uma app a examinar ID de X window da janela a examinar ou alterar XID XID: %lu
 Y _Sempre visível no desktop _Fechar _Fechar todas _Mover _Apenas neste desktop _Redimensionar _Restaurar todas acima todas os desktops abaixo alterar modo de ecrã completo alterar o desktop fechar desktop janela de diálogo doca ou painel falso ecrã completo colocar acima colocar abaixo maximizar maximizar horizontalmente maximizar verticalmente maximizada maximizada horizontalmente maximizada verticalmente minimizar minimizada mover requer atenção nenhuma ação possível normal janela normal prender presa redimensionar definido sombrear sombreada ignorar alternador ignorar lista de tarefas ecrã de logótipo nenhum colar colada menu destacável barra destacável verdadeiro deixar de colocar acima deixar de colocar abaixo restaurar restaurar horizontalmente restaurar verticalmente restaurar desprender remover sombreado descolar janela de utilitário nenhuma nenhuma 