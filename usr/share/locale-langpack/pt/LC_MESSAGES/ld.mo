��    �     �    �      `!     a!     }!     �!     �!     �!     �!  I   "  O   R"  �   �"  N   6#  M   �#  �   �#  @   ^$  L   �$  �   �$  �   �%  �   g&  �   '  K   �'  M   (  K   h(  M   �(  8   )  B   ;)  I   ~)  F   �)  �   *  J   �*  E   �*  O   ,+  K   |+  F   �+  P   ,  =   `,  L   �,  ;   �,  C   '-  K   k-  H   �-  N    .  C   O.     �.     �.  !   �.  9   �.     /  -   #/  P   Q/  >   �/     �/      �/  !   0     80     O0     l0     �0  5   �0  8   �0  6   1     E1  2   `1  #   �1  6   �1  %   �1  %   2  $   :2  )   _2  8   �2  s   �2  )   63     `3  6   x3     �3     �3  ,   �3     4  3   84  !   l4  .   �4  ?   �4     �4     5     :5     V5     u5     �5     �5     �5  4   �5  <   6  ,   E6  J   r6  $   �6  <   �6  6   7  5   V7  5   �7  )   �7  )   �7     8  .   08     _8  4   |8  "   �8  "   �8  b   �8  *   Z9     �9  2   �9  0   �9  !   :  /   $:  %   T:  &   z:      �:  $   �:     �:     ;  ,   %;  "   R;  %   u;  $   �;  B   �;  '   <  5   +<  3   a<     �<  /   �<     �<  )   �<      )=     J=     i=  6   �=     �=     �=  :   �=     ->  -   K>     y>     �>  "   �>  '   �>  2   �>  &   0?  &   W?  !   ~?     �?  H   �?  .   @  +   7@  5   c@  &   �@  7   �@  %   �@  6   A  E   UA     �A     �A  3   �A  +   	B  1   5B  0   gB     �B  3   �B  -   �B  $   C     ,C  1   JC  6   |C  5   �C  ;   �C  D   %D  7   jD  J   �D  -   �D  (   E  B   DE     �E     �E     �E  /   �E  "   �E  "   F  7   ;F  "   sF  /   �F  G   �F  3   G  /   BG     rG  "   �G  7   �G  !   �G  +   H  6   8H  -   oH  2   �H  -   �H  /   �H  3   .I  ;   bI  8   �I     �I  (   �I  '   J  %   DJ  1   jJ  7   �J  +   �J      K  #   K  "   BK     eK      xK     �K     �K  "   �K     �K     L     L  :   L  $   OL      tL     �L     �L  
   �L  /   �L  '   	M     1M  *   PM     {M     �M     �M  .   �M  ,   �M  (   N     :N  *   RN     }N  2   �N     �N  	   �N  -   �N     !O  #   1O     UO     pO  )   �O     �O  2   �O  1   P  +   9P     eP     �P  $   �P  #   �P     �P  1   �P  *   1Q     \Q     rQ  -   �Q  &   �Q  '   �Q  -   R  	   =R     GR  %   SR  &   yR     �R     �R     �R     �R     �R  %   �R  "   S  )   &S  
   PS     [S     tS  ,   �S     �S     �S     �S     T  +   $T  2   PT  1   �T     �T      �T     �T     �T     �T     U     #U  &   >U     eU     jU  5   vU     �U  	   �U     �U     �U  &   �U  %   V     <V     AV  #   IV  !   mV     �V     �V  '   �V     �V     W     W     :W     LW     _W     wW  4   �W  0   �W  (   �W     &X  #   9X  %   ]X     �X     �X     �X     �X     �X     �X  (   �X     �X     Y     4Y     QY     nY  -   Y     �Y  #   �Y  (   �Y     Z  &   Z     DZ  .   VZ  .   �Z  *   �Z  (   �Z     [  (   &[  '   O[     w[  (   �[     �[     �[  #   �[     �[     \     \     !\     2\     K\     d\     }\  4   �\  5   �\  (   ]     .]  !   L]      n]      �]  #   �]  0   �]  $   ^  1   *^  '   \^  #   �^     �^  '   �^     �^      _  
   	_     _     _  	   %_     /_     J_     h_     �_     �_  	   �_     �_     �_     �_  	   �_  �  �_     �a  "   b  !   %b  )   Gb     qb     �b  6   �b  I   �b  �   $c  X   �c  Q   2d  �   �d  4   re  9   �e  �   �e  �   of  �   Rg  �   �g  8   �h  L   �h  M   i  S   mi  @   �i  4   j  E   7j  1   }j  �   �j  M   �k  H   �k  ]   'l  N   �l  I   �l  ^   m  :   }m  F   �m  A   �m  6   An  R   xn  <   �n  H   o  A   Qo     �o     �o  +   �o  <   �o     "p  7   ;p  _   sp  L   �p      q  (   1q  +   Zq     �q  "   �q     �q     �q  4   �q  7   -r  0   er     �r  0   �r  $   �r  9   s  #   @s  %   ds  $   �s  )   �s  O   �s  �   )t  )   �t      �t  :   �t  $   7u  "   \u  .   u  )   �u  A   �u  !   v  :   <v  U   wv      �v  !   �v     w  #   *w  &   Nw  &   uw     �w     �w  H   �w  T   x  7   ]x  Z   �x  (   �x  E   y  9   _y  =   �y  9   �y  -   z  -   ?z     mz  >   �z     �z  9   �z  !   {  !   A{  l   c{  1   �{     |  8   "|  6   [|  !   �|  1   �|  %   �|  &   }      3}  (   T}  '   }}  /   �}  >   �}  0   ~  2   E~  .   x~  O   �~  ,   �~  ?   $  ;   d  !   �  6   �     �  5   �  &   M�  (   t�  !   ��  =   ��  %   ��     #�  K   C�  #   ��  :   ��     �  &   �  '   4�  ,   \�  B   ��  3   ̂  .    �  2   /�  "   b�  R   ��  )   ؃  4   �  7   7�  )   o�  @   ��  .   ڄ  =   	�  O   G�  #   ��  #   ��  3   ߅  +   �  1   ?�  @   q�     ��  ;   ��  2   ��  ,   .�  "   [�  >   ~�  >   ��  A   ��  ]   >�  k   ��  :   �  S   C�  .   ��  &   Ɖ  ^   �     L�     ]�  "   q�  5   ��  $   ʊ  $   �  7   �  $   L�  5   q�  Z   ��  4   �  >   7�      v�  "   ��  ;   ��  &   ��  *   �  :   H�  :   ��  ;   ��  9   ��  6   4�  =   k�  S   ��  >   ��  &   <�  :   c�  2   ��  4   я  >   �  7   E�  0   }�     ��  %   ː  .   �      �  )   =�     g�     ��  (   ��     Ƒ  	   �     �  H   ��  9   =�  /   w�  '   ��      ϒ  	   �  @   ��  2   ;�  $   n�  3   ��     Ǔ     Г  +   �  D   �  )   ]�  .   ��     ��  4   ֔  &   �  @   2�  #   s�  
   ��  @   ��     �  /   ��  #   '�  #   K�  6   o�  (   ��  C   ϖ  E   �  :   Y�      ��  $   ��  )   ڗ  /   �     4�  D   Q�  /   ��  %   Ƙ  )   �  4   �  5   K�  8   ��  5   ��  
   �     ��  ,   �  2   ;�  &   n�     ��     ��     ��  	   ��  <   Ś  *   �  /   -�  
   ]�  !   h�     ��  1   ��     ՛  (   ޛ  '   �  '   /�  0   W�  /   ��  <   ��     ��  ,   �     0�     C�  $   O�     t�     ��  6   ��     �     �  P   ��     I�  
   f�     q�  (   x�  /   ��  0   ў     �     
�  :   �  +   N�  2   z�     ��  2   ɟ  %   ��  &   "�  &   I�     p�     ��  !   ��  '   ��  G   �  ?   /�  6   o�     ��  -   ��  .   �     �     -�     3�     ;�     D�  &   X�  5   �  $   ��  %   ڢ  %    �  '   &�     N�  A   c�     ��  -   ��  >   �  "   %�  ?   H�     ��  >   ��  ?   �  1   $�  9   V�  *   ��  0   ��  /   �     �  .   -�     \�      w�  )   ��     ¦     ۦ     �  ,   �     �     5�     N�  (   f�  :   ��  9   ʧ  3   �  %   8�  ,   ^�  )   ��  )   ��  (   ߨ  1   �  0   :�  ?   k�  .   ��  2   ک     �  (   ,�     U�     r�     ~�  
   ��  	   ��     ��     ��  %   ɪ  ,   �     �     !�     2�     B�  '   `�  '   ��     ��     �   Y  _      �   �       k  �   x  T                      �   i  *         �   �       _   R         �   0  �      "  �   M   H  S       �   �   �   �       �   8          �        �       �   :      �   u  x   k   5  �       �  "   $   �   �      w  K  
          �  �            P  E  �   9   �   }       8  \    �     `   e   �   V   q   #  �       ]              >  .      #   �    )   ?                     X   G   l  B   �       �   ;      ;   �   <      p   o     U  �   r   y      �   �   c   �     {   `      �   �   |       �   g   �   �   d   F              [       �         u   (      S  �  n   �          D                �          �   �       Q  b   B  c      �           �   �   Z  z     K   t       A      =     |  f   �   a      T  p          a   L   7   �       m  �   �  )  ~  �                   6   �   �     �   �      �       �           �      -       �   X  @       j      '   !  4          r  �  g      l   �       F       �   	   Z   �       �     N   A   v  �             �       �   n  �      �   �              V          %  C         h  1               ?        �       Q   �   O             W   �  3     �   �          6  	  e  �          �       �       �   �   �   �      �     /  0          �       %   �       �        D   w   �   O   (     ^  �           s    �       �           �   j   �   �   �       5   �     [  P       R   
   J                   �   -  �   �   '      �       L  �      �       �   �   >   �   G  �  M     �   2      @             m   }    \   �           �                  �         ^   *   &   {      t              �   =  +       �   W          :   �     3   �   f      �       z   �   �  �   &  s   q  /   U           v   +  d  ~   7  1     Y   I      H   �   N  �       o        I       !   C                  �   �   �   �          E              �   $  �   �           �   h   ,      y   4  J      ]   ,     �  �   i   �   2           �   <   .   �       �   9  �       b  �   �           
Allocating common symbols
 
Cross Reference Table

 
Discarded input sections

 
Linker script and memory map

 
Memory Configuration

 
Set                 Symbol

   --add-stdcall-alias                Export symbols with and without @nn
   --base_file <basefile>             Generate a base file for relocatable DLLs
   --compat-implib                    Create backward compatible import libs;
                                       create __imp_<SYMBOL> as well.
   --disable-auto-image-base          Do not auto-choose image base. (default)
   --disable-auto-import              Do not auto-import DATA items from DLLs
   --disable-runtime-pseudo-reloc     Do not add runtime pseudo-relocations for
                                       auto-imported DATA.
   --disable-stdcall-fixup            Don't link _sym to _sym@nn
   --dll                              Set image base to the default for DLLs
   --dll-search-prefix=<string>       When linking dynamically to a dll without
                                       an importlib, use <string><basename>.dll
                                       in preference to lib<basename>.dll 
   --enable-auto-image-base           Automatically choose image base for DLLs
                                       unless user specifies one
   --enable-extra-pe-debug            Enable verbose debug output when building
                                       or linking to DLLs (esp. auto-import)
   --enable-runtime-pseudo-reloc      Work around auto-import limitations by
                                       adding pseudo-relocations resolved at
                                       runtime.
   --enable-stdcall-fixup             Link _sym to _sym@nn without warnings
   --exclude-libs lib,lib,...         Exclude libraries from automatic export
   --exclude-symbols sym,sym,...      Exclude symbols from automatic export
   --export-all-symbols               Automatically export all globals to DLL
   --file-alignment <size>            Set file alignment
   --heap <size>                      Set initial size of the heap
   --image-base <address>             Set start address of the executable
   --kill-at                          Remove @nn from exported symbols
   --large-address-aware              Executable supports virtual addresses
                                       greater than 2 gigabytes
   --major-image-version <number>     Set version number of the executable
   --major-os-version <number>        Set minimum required OS version
   --major-subsystem-version <number> Set minimum required OS subsystem version
   --minor-image-version <number>     Set revision number of the executable
   --minor-os-version <number>        Set minimum required OS revision
   --minor-subsystem-version <number> Set minimum required OS subsystem revision
   --out-implib <file>                Generate import library
   --output-def <file>                Generate a .DEF file for the built DLL
   --section-alignment <size>         Set section alignment
   --stack <size>                     Set size of the initial stack
   --subsystem <name>[:<version>]     Set required OS subsystem [& version]
   --support-old-code                 Support interworking with old code
   --thumb-entry=<symbol>             Set the entry point to be Thumb <symbol>
   --warn-duplicate-exports           Warn about duplicate exports.
   @FILE   Supported emulations:
   no emulation specific options.
  additional relocation overflows omitted from the output
  load address 0x%V  relocation truncated to fit: %s against `%T'  relocation truncated to fit: %s against symbol `%T' defined in %A section in %B  relocation truncated to fit: %s against undefined symbol `%T' %8x something else
 %B%F: could not read relocs: %E
 %B%F: could not read symbols: %E
 %B: In function `%T':
 %B: file not recognized: %E
 %B: matching formats: %B: warning: common is here
 %B: warning: common of `%T' overridden by definition
 %B: warning: common of `%T' overridden by larger common
 %B: warning: common of `%T' overriding smaller common
 %B: warning: defined here
 %B: warning: definition of `%T' overriding common
 %B: warning: larger common is here
 %B: warning: more undefined references to `%T' follow
 %B: warning: multiple common of `%T'
 %B: warning: previous common is here
 %B: warning: smaller common is here
 %B: warning: undefined reference to `%T'
 %C: Cannot get section contents - auto-import exception
 %C: variable '%T' can't be auto-imported. Please read the documentation for ld's --enable-auto-import for details.
 %C: warning: undefined reference to `%T'
 %D: first defined here
 %D: warning: more undefined references to `%T' follow
 %F%B: file not recognized: %E
 %F%B: final close failed: %E
 %F%B: member %B in archive is not an object
 %F%P: %s not found for insert
 %F%P: attempted static link of dynamic object `%s'
 %F%P: bfd_record_phdr failed: %E
 %F%P: cannot create split section name for %s
 %F%P: cannot perform PE operations on non PE output file '%B'.
 %F%P: clone section failed: %E
 %F%P: final link failed: %E
 %F%P: internal error %s %d
 %F%P: invalid BFD target `%s'
 %F%P: invalid data statement
 %F%P: invalid reloc statement
 %F%S %% by zero
 %F%S / by zero
 %F%S can not PROVIDE assignment to location counter
 %F%S cannot move location counter backwards (from %V to %V)
 %F%S invalid assignment to location counter
 %F%S: non constant or forward reference address expression for section %s
 %F%S: nonconstant expression for %s
 %F%S: undefined MEMORY region `%s' referenced in expression
 %F%S: undefined section `%s' referenced in expression
 %F%S: undefined symbol `%s' referenced in expression
 %F%S: unknown constant `%s' referenced in expression
 %P%F: -F may not be used without -shared
 %P%F: -f may not be used without -shared
 %P%F: -pie not supported
 %P%F: -r and -shared may not be used together
 %P%F: -shared not supported
 %P%F: BFD backend error: BFD_RELOC_CTOR unsupported
 %P%F: Failed to create hash table
 %P%F: Illegal use of `%s' section
 %P%F: Relocatable linking with relocations from format %s (%B) to format %s (%B) is not supported
 %P%F: bad --unresolved-symbols option: %s
 %P%F: bad -rpath option
 %P%F: bfd_hash_allocate failed creating symbol %s
 %P%F: bfd_hash_lookup failed creating symbol %s
 %P%F: bfd_hash_lookup failed: %E
 %P%F: bfd_hash_lookup for insertion failed: %E
 %P%F: bfd_hash_table_init failed: %E
 %P%F: bfd_link_hash_lookup failed: %E
 %P%F: bfd_new_link_order failed
 %P%F: can not create hash table: %E
 %P%F: can't relax section: %E
 %P%F: can't set start address
 %P%F: cannot open linker script file %s: %E
 %P%F: cannot open map file %s: %E
 %P%F: cannot open output file %s: %E
 %P%F: cannot represent machine `%s'
 %P%F: error: no memory region specified for loadable section `%s'
 %P%F: failed creating section `%s': %E
 %P%F: group ended before it began (--help for usage)
 %P%F: invalid argument to option "--section-start"
 %P%F: invalid hex number `%s'
 %P%F: invalid hex number for PE parameter '%s'
 %P%F: invalid number `%s'
 %P%F: invalid section sorting option: %s
 %P%F: invalid subsystem type %s
 %P%F: invalid syntax in flags
 %P%F: missing argument to -m
 %P%F: missing argument(s) to option "--section-start"
 %P%F: multiple STARTUP files
 %P%F: no input files
 %P%F: output format %s cannot represent section called %s
 %P%F: please report this bug
 %P%F: strange hex info for PE parameter '%s'
 %P%F: target %s not found
 %P%F: unknown format type %s
 %P%F: unrecognized -a option `%s'
 %P%F: unrecognized -assert option `%s'
 %P%F: use the --help option for usage information
 %P%F:%s: can not make object file: %E
 %P%F:%s: can not set architecture: %E
 %P%F:%s: can't set start address
 %P%F:%s: hash creation failed
 %P%X: %s architecture of input file `%B' is incompatible with %s output
 %P%X: %s does not support reloc %s for set %s
 %P%X: --hash-size needs a numeric argument
 %P%X: Different object file formats composing set %s
 %P%X: Different relocs used in set %s
 %P%X: Internal error on COFF shared library section %s
 %P%X: Unsupported size %d for set %s
 %P%X: failed to merge target specific data of file %B
 %P: Disabling relaxation: it will not work with multiple definitions
 %P: Error closing file `%s'
 %P: Error writing file `%s'
 %P: `-retain-symbols-file' overrides `-s' and `-S'
 %P: internal error: aborting at %s line %d
 %P: internal error: aborting at %s line %d in %s
 %P: link errors found, deleting executable `%s'
 %P: mode %s
 %P: skipping incompatible %s when searching for %s
 %P: symbol `%T' missing from main hash table
 %P: unrecognised emulation mode: %s
 %P: unrecognized option '%s'
 %P: warning, file alignment > section alignment.
 %P: warning: '--thumb-entry %s' is overriding '-e %s'
 %P: warning: bad version number in -subsystem option
 %P: warning: cannot find entry symbol %s; defaulting to %V
 %P: warning: cannot find entry symbol %s; not setting start address
 %P: warning: changing start of section %s by %lu bytes
 %P: warning: could not find any targets that match endianness requirement
 %P: warning: dot moved backwards before `%s'
 %P: warning: global constructor %s used
 %P: warning: no memory region specified for loadable section `%s'
 %S HLL ignored
 %S SYSLIB ignored
 %W (size before relaxing)
 %X%B: more undefined references to `%T' follow
 %X%B: undefined reference to `%T'
 %X%C: multiple definition of `%T'
 %X%C: prohibited cross reference from %s to `%T' in %s
 %X%C: undefined reference to `%T'
 %X%D: more undefined references to `%T' follow
 %X%P: anonymous version tag cannot be combined with other version tags
 %X%P: bfd_hash_table_init of cref table failed: %E
 %X%P: can't set BFD default target to `%s': %E
 %X%P: cref alloc failed: %E
 %X%P: cref_hash_lookup failed: %E
 %X%P: duplicate expression `%s' in version information
 %X%P: duplicate version tag `%s'
 %X%P: error: duplicate retain-symbols-file
 %X%P: section `%s' assigned to non-existent phdr `%s'
 %X%P: unable to find version dependency `%s'
 %X%P: unable to open for destination of copy `%s'
 %X%P: unable to open for source of copy `%s'
 %X%P: unable to read .exports section contents
 %X%P: unknown language `%s' in version information
 %X%P:%S: section has both a load address and a load region
 %X%S: unresolvable symbol `%s' referenced in expression
 %XCan't open .lib file: %s
 %XCannot export %s: invalid export name
 %XCannot export %s: symbol not defined
 %XCannot export %s: symbol not found
 %XCannot export %s: symbol wrong type (%d vs %d)
 %XError, duplicate EXPORT with ordinals: %s (%d vs %d)
 %XError, ordinal used twice: %d (%s vs %s)
 %XError: %d-bit reloc in dll
 %XUnsupported PEI architecture: %s
 %s: Can't open output def file %s
 %s: data size %ld
 %s: emulation specific options:
 %s: supported emulations:  %s: supported targets: %s: total time in link: %ld.%06ld
 ; no contents available
 ADDRESS ARCH Accept input files whose architecture cannot be determined Add DIRECTORY to library search path Add data symbols to dynamic list Address of section %s set to  Allow multiple definitions Attributes Auxiliary filter for shared object symbol table Bind global function references locally Bind global references locally Build global constructor/destructor tables COUNT Call SYMBOL at load-time Call SYMBOL at unload-time Check section addresses for overlaps (default) Common symbol       size              file

 Create a position independent executable Create a shared library Create an output file even if errors occur Create default symbol version Create default symbol version for imported symbols Creating library file: %s
 DIRECTORY Default search path for Solaris compatibility Define a symbol Demangle symbol names [using STYLE] Disallow undefined version Discard all local symbols Discard temporary local symbols (default) Display target specific options Do not allow unresolved references in object files Do not allow unresolved references in shared libs Do not check section addresses for overlaps Do not define Common storage Do not demangle symbol names Do not link against shared libraries Do not list removed unused sections Do not page align data Do not page align data, do not make text readonly Do not strip symbols in discarded sections Do task level linking Don't discard any local symbols Don't merge input [SECTION | orphan] sections Don't remove unused sections (default) Don't warn about mismatched input files Don't warn on finding an incompatible library EMULATION End a group Errors encountered processing file %s Errors encountered processing file %s
 Export all dynamic symbols FILE FILENAME Fail with %d
 File
 Filter for shared object symbol table Force common symbols to be defined Force generation of file with .exe suffix GNU ld %s
 Generate embedded relocs Generate relocatable output How many tags to reserve in .dynamic section Ignored Ignored for Linux compatibility Ignored for SVR4 compatibility Ignored for SunOS compatibility Include all objects from following archives Info: resolving %s by linking to %s (auto-import)
 Just link symbols (if directory, same as --rpath) KEYWORD Keep only symbols listed in FILE LIBNAME Length Link against shared libraries Link big-endian objects Link little-endian objects List removed unused sections on stderr Name No symbols
 Only set DT_NEEDED for following dynamic libs if used Optimize output file Options:
 Origin Output cross reference table Output lots of information during link Override the default sysroot location PATH PROGRAM Page align data, make text readonly Print map file on standard output Print memory usage statistics Print option help Print version and emulation information Print version information Read MRI format linker script Read default linker script Read dynamic list Read linker script Read options from FILE
 Read version information script Reduce memory overheads, possibly taking much longer Reject input files whose architecture is unknown Remove unused sections (on some targets) Report bugs to %s
 Report unresolved symbols as errors Report unresolved symbols as warnings SECTION=ADDRESS SHLIB SIZE SYMBOL SYMBOL=EXPRESSION Search for library LIBNAME Set PROGRAM as the dynamic linker to use Set address of .bss section Set address of .data section Set address of .text section Set address of named section Set architecture Set default hash table size close to <NUMBER> Set emulation Set internal name of shared library Set link time shared library search path Set output file name Set runtime shared library search path Set start address Shared library control for HP/UX compatibility Small data size (if no size, same as --shared) Sort sections by name or maximum alignment Specify target for following input files Specify target of output file Split output sections every COUNT relocs Split output sections every SIZE octets Start a group Start with undefined reference to SYMBOL Strip all symbols Strip debugging symbols Strip symbols in discarded sections Supported emulations:  Symbol TARGET Trace file opens Trace mentions of SYMBOL Treat warnings as errors Turn off --whole-archive Usage: %s [options] file...
 Use --disable-stdcall-fixup to disable these fixups
 Use --enable-stdcall-fixup to disable these warnings
 Use C++ operator new/delete dynamic list Use C++ typeinfo dynamic list Use less memory and more disk I/O Use same format as native linker Use wrapper functions for SYMBOL Warn about duplicate common symbols Warn if global constructors/destructors are seen Warn if shared object has DT_TEXTREL Warn if start of section changes due to alignment Warn if the multiple GP values are used Warn only once per undefined symbol Warning, duplicate EXPORT: %s
 Warning: resolving %s by linking to %s
 Write a map file [=COUNT] [=SECTION] [=SIZE] [=STYLE] alignment attempt to open %s failed
 attempt to open %s succeeded
 cannot find script file %s
 name name|alignment no symbol opened script file %s
 using external linker script: using internal linker script: warning:  Project-Id-Version: binutils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2011-04-16 21:05+0000
Last-Translator: João Rocha <joaoesperanco@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:03+0000
X-Generator: Launchpad (build 18115)
X-Poedit-Country: PORTUGAL
X-Poedit-Language: Portuguese
 
Distribuindo símbolos comuns
 
Tabela de referências cruzadas

 
Descartar secções de entrada

 
Script de ligação e mapa de memória

 
Configuração de Memória

 
Definir Símbolo

   --add-stdcall-alias Exporta símbolos com e sem @nn
   --base_file <ficheirobase> Gera um arquivo base para DLLs realocáveis
   --compat-implib Criar libs retro compatíveis;
                                                                                                         
__imp_<SÍMBOLO>também.
   --disable-auto-image-base Não escolher automaticamente a imagem base. (por omissão)
   --disable-auto-import Não importar automaticamente os elementos DATA dos DLLs
   --disable-runtime-pseudo-reloc Não adicionar pseudo-recolocações em tempo de execução para
                                                                                                        
DADOS importados automaticamente.
   --disable-stdcall-fixup Não ligar _sym a _sym@nn
   --dll Configura a imagem base para o padrão para DLLs
   --dll-search-prefix=<string> Ao ligar dinamicamente a um dll sem

importlib, use <string><basename>.dll

preferência a lib<basename>.dll 
   --enable-auto-image-base Escolher automáticamente uma imagem base para DLLs
                                                                                                         
a menos que o utilizador especifique uma
   --enable-extra-pe-debug activar saída de informação de depuração detalhada ao construir

ligando a DLLs (esp. auto-import)
   --enable-runtime-pseudo-reloc Resolver limitações de importação automática através da

adição de pseudo-recolocações resolvidas em                               

tempo de execução
   --enable-stdcall-fixup Ligar _sym a _sym@nn sem aviso
   --exclude-libs lib,lib,... Exclui bibliotecas da exportação automática
   --exclude-symbols sym,sym,... Exclui símbolos da exportação automática
   --export-all-symbols Exporta automaticamente todos os símbolos globais para DLL
   ----file-alignment <tamanho> Define o alinhamento do ficheiro
   --heap <tamanho> Define o tamanho inicial do heap
   --image-base <endereço> Define o endereço inicial do executável
   --kill-at Remover @nn dos símbolos exportados
   --large-address-aware Executável suporta endereço virtual
                                                                                                                                       maiores do que 2 gigabytes
   --major-image-version <número> Define o número da versão do executável
   --major-os-version <número> Define a versão mínima requerida do SO
   --major-subsystem-version <número> Define a versão mínima requerida do subsistema do SO
   --minor-image-version <número> Define o número de revisão do executável
   --minor-os-version <número> Define a revisão mínima requerida do SO
   --minor-subsystem-version <número> Define a revisão mínima requerida do subsistema do SO
   --out-implib <ficheiro> Gera biblioteca de importação
   --output-def <ficheiro> Gera um arquivo .DEF para a DLL construída
   --section-alignment <tamanho> Define o alinhamento da secção
   --stack <tamanho> Define o tamanho inicial da pilha
   --subsystem <nome>[:<versão>] Define o subsistema  do SO requerido [& versão]
   --support-old-code Suporta interoperar com código antigo
   --thumb-entry=<símbolo>Define o ponto de entrada para ser <símbolo>
   --warn-duplicate-exports Avisa sobre exportações duplicadas.
   @FICHEIRO   Emulações suportadas
   sem opções específicas de emulação.
  transferências adicionais de overflows omitidas da saída
  carregar endereço 0x%V  transferência truncada para ir: %s de encontro a `%T'  transferência truncada para ir: %s de encontro ao símbolo `%T' definido na secção %A em %B  transferência truncada para ir: %s de encontro ao símbolo indefinido `%T' %8x outra coisa
 %B%F: não foi possível ler relocs: %E
 %B%F: Não foi possível ler símbolos: %E
 %B: na função `%T':
 %B: ficheiro não reconhecido: %E
 %B: formatos compatíveis: %B: aviso: comum está aqui
 %B: aviso: comum de `%T' sobreposto por definição
 %B: aviso: comum de `%T' sobreposto por um comum maior
 %B: aviso: comum de `%T' sobrepondo comum menor
 %B: aviso: definido aqui
 %B: aviso: definição de `%T' a sobrepor comum
 %B: aviso: o comum maior está aqui
 %B: aviso: seguem-se mais referências indefinidas a `%T
 %B: aviso: comum múltiplo de `%T'
 %B: aviso: comum anterior está aqui
 %B: aviso: o comum menor está aqui
 %B: aviso: referência indefinida a `%T'
 %C: não é possível adquirir o conteúdo da secção - auto-import exception
 %C: variável '%T' não pode ser auto-importada. Por favor leia a documentação para o --enable-auto-import do Id para mais detalhes.
 %C: aviso: referência indefinida a `%T'
 %D: definido primeiramente aqui
 %D: aviso: seguem-se mais referências indefinidas a `%T'
 %F%B: ficheiro não reconhecido: %E
 %F%B: fechamento final falhou: %E
 %F%B: membro %B no arquivo não é um objecto
 %F%P: %s não encontrado para inserção
 %F%P: tentativa de ligação estática de objecto dinâmico `%s'
 %F%P: bfd_record_phdr falhou: %E
 %F%P: impossível criar nome de secção dividida para %s
 %F%P: não é possível efectuar operações PE num ficheiro de saída não PE '%B'.
 %F%P: secção clone falhou: %E
 %F%P: ligação final falhou: %E
 %F%P: erro interno %s %d
 %F%P: objectivo BFD inválido `%s'
 %F%P: declaração de dados inválida
 %F%P: declaração de reloc inválida
 %F%S %% por zero
 %F%S / por zero
 %F%S não é possível PROVIDENCIAR tarefa à localização do contador
 %F%S não é possível mover a localização do contador para trás (de %V para %V)
 %F%S tarefa inválida para o contador da localização
 %F%S: não constante ou referência da expressão de endereço adiante para a secção %s
 %F%S: expressão não constante para %s
 %F%S: região MEMÓRIA não definida `%s' referenciado na expressão
 %F%S: secção não definida `%s' referida na expressão
 %F%S: símbolo não definido `%s' referenciado na expressão
 %F%S: constante desconhecida `%s' referida na expressão
 %P%F: -F não pode ser utilizado sem -shared
 %P%F: -f não pode ser utilizado sem -shared
 %P%F: -pie não suportada
 %P%F: -r e -shared não podem ser utilizados simultâneamente
 %P%F: -shared não suportada
 %P%F: BFD erro de backend: BFD_RELOC_CTOR não suportado
 %P%F: Falha ao criar tabela hash
 %P%F: Uso ilegal de `%' secção
 %P%F: Ligação recolocável com recolocações do formato %s (%B) para o formato %s (%B) não é suportado
 %P%F: opção --unresolved-symbols inválida: %s
 %P%F: opção -rpath inválida
 %P%F: bfd_hash_allocate criação de símbolo falhou %s
 %P%F: bfd_hash_lookup criação de símbolo falhou %s
 %P%F: bfd_hash_lookup falhou: %E
 %P%F: bfd_hash_lookup para inserção falhou: %E
 %P%F: bfd_hash_table_init falhou: %E
 %P%F: bfd_link_hash_lookup falhou: %E
 %P%F: bfd_new_link_order falhou
 %P%F: impossível criar tabela hash: %E
 %P%F: impossível relaxar secção: %E
 %P%F: impossível definir endereço de início
 %P%F: impossível abrir ligação para ficheiro de script %sE
 %P%F: impossível abrir mapa de ficheiro %s: %E
 %P%F: impossível abrir ficheiro de saída %s: %E
 %P%F: impossível representar a máquina `%s'
 %P%F: erro: região especificada sem memória para a secção carregável `%s'
 %P%F: criação de secção falhou `%s': %E
 %P%F: groupo terminou antes de ser iniciado (--help for usage)
 %P%F: argumento inválido para a opção "--section-start"
 %P%F: número hex inválido `%s'
 %P%F: número hex inválido para o parâmetro PE '%s'
 %P%F: número inválido `%s'
 %P%F: opção de filtragem de secção inválida: %s
 %P%F: tipo inválido de subsistema %s
 %P%F: sintaxe inválida nas marcações
 %P%F: argumento em falta para -m
 %P%F: argumento(s) em falta para a opção "--section-start"
 %P%F: arquivos de STARTUP múltiplos
 %P%F: sem ficheiros de entrada
 %P%F: formato de saída %s não consegue representar a secção chamada %s
 %P%F: por favor, reporte este erro
 %P%F: informação hex estranha para o parâmetro PE '%s'
 %P%F: alvo %s não encontrado
 %P%F: tipo de formato %s desconhecido
 %P%F: opção -a não reconhecida `%s'
 %P%F: opção -assert não reconhecida `%s'
 %P%F: utilizar a opção --help para informação de utilização
 %P%F:%s: impossível criar ficheiro de objecto: %S
 %P%F:%s: impossível definir arquitectura: %E
 %P%F:%s: impossível definir endereço de início
 %P%F:%s: criação de hash falhou
 %P%X: %s arquitectura do ficheiro de entrada `%B'é incompatível com a saída %s
 %P%X: %s não suporta reloc %s para set%
 %P%X: --hash-size precisa de um argumento numérico
 %P%X: Diferentes formatos de arquivo compondo o set %s
 %P%X: relocs diferentes usadas no set %s
 %P%X: Erro interno em COFF secção de biblioteca partilhada %s
 %P%X: Tamanho não suportado %d para o set %s
 %P%X: falha ao unir os dados alvo específicos do arquivo %B
 %P: Desabilitando relaxamento: não irá funcionar com múltiplas definições
 %P: Erro ao fechar o ficheiro `%s'
 %P: Erro ao escrever ficheiro `%s'
 %P: `-retain-symbols-file' sobrepostos `-s' e `-S'
 %P: erro interno: abortando na %s linha %d
 %P: erro interno: abortando na %s linha %d em %s
 %P: erros de ligação encontrados, eliminando executável `%s'
 %P: modo %s
 %P: passando à frente %s incompatível ao procurar por %s
 %P: symbol `%T' em falta na tabela hash principal
 %P: modo de emulação não reconhecido: %s
 %P: opção não reconhecida '%s'
 %P: aviso, alinhamento do ficheiro > alinhamento da secção.
 %P: aviso: '--thumb-entry %s' está a prevalecer sobre '-e %'
 %P: warning: número de versão incorrecto na opção -subsystem
 %P: aviso: não é possível encontrar símbolo de entrada %s definindo por omissão para %V
 %P: aviso: não é possível encontrar símbolo de entrada %s; endereço de início não irá ser definido
 %P: aviso: alterando secção de início %s por %lu bytes
 %P: aviso: não foi possível encontrar nenhum alvo que cumprisse os requerimentos
 %P: aviso: ponto movido para trás antes `%s'
 %P: aviso: construtor global %s usado
 %P: aviso: sem secção de região de memória especificada para a secção carregável  `%s'
 %S HLL ignorado
 %S SYSLIB ignorado
 %W (tamanho antes do relaxamento)
 %X%B: seguem-se mais referências indefinidas a `%T'
 %X%B: referência indefinida a `%T'
 %X%C: múltipla definição de `%T'
 %X%C: referência cruzada proibida de %s to `%T' in %s
 %X%C: referência indefinida a `%T'
 %X%D: seguem-se mais referências indefinidas a `%T'
 %X%P: marcação de versão anónima não pode ser combinada com outras marcas de versão
 %X%P: bfd_hash_table_init da tabela cref falhou: %E
 %X%P: impossível definir BFD alvo por omissão para `%s': %E
 %X%P:alocação cref falhou: %E
 %X%P: cref_hash_lookup falhou: %E
 %X%P: expressão duplicada `%s' na informação da versão
 %X%P: marca de versão duplicada `%s'
 %X%P: erro: duplicado retain-symbols-file
 %X%P: secção `%s' atribuída a phdr não existente `%s'
 %X%P: impossível encontrar dependências da versão `%s'
 %X%P: impossível abrir a partir do destino da cópia `%s'
 %X%P: impossível abrir a partir da fonte da cópia `%s'
 %X%P: impossível ler conteúdos da secção .exports
 %X%P: linguagem desconhecida `%s' na informação da versão
 %X%P:%S: a secção tem um endereço de carregamento e uma região de carregamento
 %X%S: símbolo não resolvido `%s' referenciado na expressão
 %XImpossível abrir ficheiro .lib: %s
 %XImpossível exportar %s: nome de exportação inválido
 %XImpossível exportar %s: símbolo não definido
 %XImpossível exportar %s: símbolo não encontrado
 %XImpossível exportar %s: tipo de símbolo errado (%d vs %d)
 %XErro, EXPORTAR duplicado com ordinais: %s (%d vs %d)
 %XErro, ordinal usado duas vezes: %d (%s vs %s)
 %XErro: %d-bit reloc em dll
 %XArquitetura PEI não suportada: %s
 %s: Impossível abri ficheiro def de saida %s
 %s: dimensão dos dados %ld
 %s: opções específicas de emulação:
 %s: emulações suportadas:  %s: destinos suportados: %s: tempo total na ligação: %ld.%06ld
 ; sem conteúdos disponíveis
 ENDEREÇO ARCH Aceitar ficheiros de entrada cuja arquitectura não pode ser determinada Adicionar DIRECTORIO ao caminho de pesquisa da biblioteca Adicionar símbolos de dados à lista dinâmica Endereço da secção %s definido para  Permitir definições múltiplas Atributos Filtro auxiliar para tabela de símbolos de objectos partilhados Ligar referências globais de funções localmente Unir referências globais localmente Construir tabelas construtoras/destruidoras globais CONTAGEM Chamar SÍMBOLO ao carregar Chamar SÍMBOLO ao efectuar descarregamento Verificar o endereço de secção por sobreposições (por omissão) Símbolos comuns do tamanho do ficheiro

 Criar um executável independente da posição Criar uma biblioteca partilhada Criar um ficheiro de saída mesmo se ocorrerem erros Criar versão de símbolo por omissão Criar versão de símbolo por omissão para símbolos importados A criar ficheiro de biblioteca: %s
 DIRECTORIO Caminho de procura por omissão para compatibilidade com Solaris Definir um símbolo Reconstituir nomes de símbolos [usando ESTILO] Não permitir versão não definida Descartar todos os símbolos locais Descartar símbolos locais temporários (por omissão) Mostrar opções específicas do destino Não permitir referências não resolvidas em objectos de ficheiros Não permitir referências não resolvidas em bibliotecas partilhadas Não verificar o endereço de secção por sobreposições Não definir armazenamento Comum Não reconstituir nomes de símbolos Não ligar contra bibliotecas partilhadas Não listar secções removidas não utilizadas Não paginar dados alinhados Não paginar dados alinhados, não definir texto como só de leitura Não retirar símbolos em secções descartadas Fazer ligações ao nível de tarefas Não descartar quaisquer símbolos locais Não fundir secções [SECÇÃO | orfão] de entrada Não remover secções não utilizadas (por omissão) Não avisar sobre ficheiros de entrada que não combinam Não avisar ao encontrar uma biblioteca incompatível EMULAÇÃO Finalizar um grupo Erros encontrados ao processar o ficheiro %s Erros encontrados no processamento do ficheiro %s
 Exportar todos os símbolos dinâmicos FICHEIRO NOMEDEFICHEIRO Falha com %d
 Ficheiro
 Filtro para para tabela de símbolos de objectos partilhados Forçar símbolos comuns a serem definidos Forçar criação de ficheiro com o sufixo .exe GNU ld %s
 Gerar recolocações incorporados Gerar saída realocável Quantas etiquetas a reservar na secção .dynamic Ignorado Ignorado para compatíbilidade com Linux Ignorado para compatíbilidade com SVR4 Ignorado para compatibilidade com SunOS Incluir todos os objectos dos seguintes arquivos Info: resolvendo %s a ligar a %s (auto-import)
 Apenas ligar símbolos (se directório, o mesmo que --rpath) PALAVRA CHAVE Manter apenas símbolos listados em FICHEIRO NOME DA BIBLIOTECA Comprimento Ligar contra bibliotecas partilhadas Ligar objectos big-endian Ligar objectos little-endian Listar secções removidas e não utilizadas em stderr Nome : Sem símbolos
 Apenas definir DT_NEEDED para as bibliotecas dinâmicas seguintes, se utilizadas Optimizar ficheiro de saída Opções:
 Origem Exportar tabela de referências cruzadas Exportar muita informação durante a ligação Sobrepor a localização por omissão de sysroot CAMINHO PROGRAMA Paginar dados alinhados, definir texto como só de leitura Imprimir mapa de ficheiro na saída padrão Imprimir estatísticas de utilização de memória Imprimir a ajuda da opção Informação de versão de impressão e emulação Informação de versão de impressão Ler script de ligação de formato MRI Ler o script de ligação por omissão Ler lista dinâmica Ler scripts de ligação Ler opções apartir de FICHEIRO
 Ler o script de informação da versão Reduzir overheads de memória, possivelmente demorando muito mais tempo Rejeitar ficheiros de entrada cuja arquitectura é desconhecida Remover secções não utilizadas (em alguns destinos) Reportar erros a %s
 Reportar símbolos não resolvidos como erros Reportar símbolos não resolvidos como avisos [SECÇÃO=ENDEREÇO] SHLIB TAMANHO SÍMBOLO SÍMBOLO=EXPRESSÃO Procurar biblioteca NOME DA BIBLIOTECA Define PROGRAMA como a ligação dinâmica a utilizar Definir o endereço da secção .bss Definir o endereço da secção .data Definir o endereço da secção .text Definir o endereço da secção nomeada Definir arquitectura Definir tamanho por omissão da tabela hash próximo de <NÚMERO> Definir emulação Define o nome interno da bibioteca partilhada Definir caminho de busca da biblioteca de ligação partilhada Definir nome de ficheiro de saída Definir caminho de busca da biblioteca de execução partilhada Definir endereço de início Controlo de bibliotecas partilhadas para compatibilidade HP/UX Pequeno tamanho de dados (se sem tamanho, o mesmo que --shared) Filtrar secções por nome ou alinhamento máximo Destino especifico para os seguintes ficheiros de entrada Específicar destino do ficheiro de saída Dividir secções de saída cada CONTAGEM relocs Dividir secções de saída cada TAMANHO octets Iniciar um grupo Iniciar com referências indefinida a SÍMBOLO Retirar todos os símbolos Retirar símbolos de depuração Retirar símbolos em secções decartadas Emulações suportadas:  Símbolo DESTINO Seguir a execução da abertura de ficheiros Seguir mencões a SÍMBOLOS Tratar avisos como erros Desligar --todo-arquivo Utilização: %s [opções] ficheiro...
 Use --disable-stdcall-fixup para desabilitar estes fixups
 Use --enable-stdcall-fixup para desabilitar estes avisos
 Utilizar operador C++ nova/eliminar lista dinâmica Utilizar C++ typeinfo lista dinâmica Usar menos memória e mais disco rígido I/O Utilizar o mesmo formato do linker nativo Utilizar funcções wrapper para SÍMBOLO Avisar sobre símbolos comuns duplicados Avisar se construtores/destrutores são avistados Avisar se o objecto partilhado possui DT_TEXTREL Avisar se o início da secção se altera devido ao alinhamento Avisar se os valores múltiplos GP são usados Avisar apenas uma vez por cada símbolo indefinido Aviso, EXPORTAR duplicado: %s
 Warning: resolvendo %s vínculo para %s
 Escrever um mapa de ficheiro [=CONTAGEM] [=SECÇÃO] [=TAMANHO] [=ESTILO] alinhamento tentativa de abrir %s falhou
 tentativa para abrir %s bem sucedida
 impossível encontrar ficheiro de script %s
 nome nome|alinhamento nenhum símbolo ficheiro de script aberto %s
 a utilizar ligação de script externa: a utilizar ligação de script interna: aviso:  