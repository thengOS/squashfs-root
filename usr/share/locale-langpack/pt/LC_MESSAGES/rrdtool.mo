��          �   %   �      0  '  1  A   Y  @   �  D   �  =   !  :   _  l   �          !  ,   ;     h  %   �  ,   �  -   �        &   (     O     o  t   �  �     �   �  �   7	  0   �	  �  	
  '  �  =   �  I     D   U  ?   �  ;   �  k        �     �  .   �     �  %     0   ,  0   ]  "   �  (   �  %   �  %      x   &  �   �  �   P  �   �  5   �                          
                                                                      	                               		[CDEF:vname=rpn-expression]
		[VDEF:vdefname=rpn-expression]
		[PRINT:vdefname:format]
		[GPRINT:vdefname:format]
		[COMMENT:text]
		[SHIFT:vname:offset]
		[TEXTALIGN:{left|right|justified|center}]
		[TICK:vname#rrggbb[aa][:[fraction][:legend]]]
		[HRULE:value#rrggbb[aa][:legend]]
		[VRULE:value#rrggbb[aa][:legend]]
		[LINE[width]:vname[#rrggbb[aa][:[legend][:STACK]]]]
		[AREA:vname[#rrggbb[aa][:[legend][:STACK]]]]
		[PRINT:vname:CF:format] (deprecated)
		[GPRINT:vname:CF:format] (deprecated)
		[STACK:vname[#rrggbb[aa][:legend]]] (deprecated)
  * cd - changes the current directory

	rrdtool cd new directory
  * ls - lists all *.rrd files in current directory

	rrdtool ls
  * mkdir - creates a new directory

	rrdtool mkdir newdirectoryname
  * pwd - returns the current working directory

	rrdtool pwd
  * quit - closing a session in remote mode

	rrdtool quit
  * resize - alter the length of one of the RRAs in an RRD

	rrdtool resize filename rranum GROW|SHRINK rows
 %s: illegal option -- %c
 %s: invalid option -- %c
 %s: option `%c%s' doesn't allow an argument
 %s: option `%s' is ambiguous
 %s: option `%s' requires an argument
 %s: option `--%s' doesn't allow an argument
 %s: option `-W %s' doesn't allow an argument
 %s: option `-W %s' is ambiguous
 %s: option requires an argument -- %c
 %s: unrecognized option `%c%s'
 %s: unrecognized option `--%s'
 * graph - generate a graph from one or several RRD

	rrdtool graph filename [-s|--start seconds] [-e|--end seconds]
 * graphv - generate a graph from one or several RRD
           with meta data printed before the graph

	rrdtool graphv filename [-s|--start seconds] [-e|--end seconds]
 * restore - restore an RRD file from its XML form

	rrdtool restore [--range-check|-r] [--force-overwrite|-f] filename.xml filename.rrd
 RRDtool is distributed under the Terms of the GNU General
Public License Version 2. (www.gnu.org/copyleft/gpl.html)

For more information read the RRD manpages
 Valid remote commands: quit, ls, cd, mkdir, pwd
 Project-Id-Version: rrdtool
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-04-06 16:14+0000
PO-Revision-Date: 2014-07-04 18:46+0000
Last-Translator: Gonçalo Silva <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:05+0000
X-Generator: Launchpad (build 18115)
 		[CDEF:vname=rpn-expression]
		[VDEF:vdefname=rpn-expression]
		[PRINT:vdefname:format]
		[GPRINT:vdefname:format]
		[COMMENT:text]
		[SHIFT:vname:offset]
		[TEXTALIGN:{left|right|justified|center}]
		[TICK:vname#rrggbb[aa][:[fraction][:legend]]]
		[HRULE:value#rrggbb[aa][:legend]]
		[VRULE:value#rrggbb[aa][:legend]]
		[LINE[width]:vname[#rrggbb[aa][:[legend][:STACK]]]]
		[AREA:vname[#rrggbb[aa][:[legend][:STACK]]]]
		[PRINT:vname:CF:format] (deprecated)
		[GPRINT:vname:CF:format] (deprecated)
		[STACK:vname[#rrggbb[aa][:legend]]] (deprecated)
  * cd - muda o directório actual

	rrdtool cd new directory
  * ls - lista todos os arquivos *.rrd no directório actual

	rrdtool ls
  * mkdir - cria um novo directório
	rrdtool mkdir newdirectoryname
  * pwd - mostra o directório actual de trabalho

	rrdtool pwd
  * quit - fechar uma sessão em modo remoto

	rrdtool quit
  * resize - alterar o comprimento de um dos RRAs num RRD

	rrdtool resize filename rranum GROW|SHRINK rows
 %s: opção ilegal -- %c
 %s: opção inválida -- %c
 %s: a opção `%c%s' não admite um argumento
 %s: opção `%s' é ambígua
 %s: opção `%s' requer um argumento
 '%s': a opção `--%s' não admite um argumento
 %s: a opção `-W %s' não permite um argumento
 %s: a opção `-W %s' é ambígua
 %s: a opção requer um argumento -- %c
 %s: a opção '%c%s' é desconhecida
 %s: a opção '--%s' é desconhecida
 * graph - criar um gráfico a partir de um ou mais RRD

	rrdtool graph filename [-s|--start seconds] [-e|--end seconds]
 * graphv - criar um gráfico a partir de um ou mais RRD
           com meta-dados impressos antes do gráfico

	rrdtool graphv filename [-s|--start seconds] [-e|--end seconds]
 * restore - restore um ficheiro RRD a partir de XML form

	rrdtool restore [--range-check|-r] [--force-overwrite|-f] filename.xml filename.rrd
 RRDtool é distribuído sob os Termos da Licença Pública
Geral GNU Versão 2. (www.gnu.org/copyleft/gpl.html)

Para mais informação, consulte o manual de RRD
 Comandos remotos Válidos : quit, ls, cd, mkdir, pwd
 