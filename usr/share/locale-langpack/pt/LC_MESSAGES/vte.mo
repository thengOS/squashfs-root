��          �      \      �  $   �  )   �        6   9     p  /   �  (   �     �  +   �     $     B  $   ^  -   �  +   �  7   �       &   -  /   T     �  �  �  4     ?   N     �  K   �     �  4     )   =     g  =   �     �      �  +   �  ?   *  ,   j  S   �     �  3   	  7   ?	     w	                                  	                      
                                           Attempt to set invalid NRC map '%c'. Attempt to set invalid wide NRC map '%c'. Could not open console.
 Could not parse the geometry spec passed to --geometry Duplicate (%s/%s)! Error (%s) converting data for child, dropping. Error compiling regular expression "%s". Error creating signal pipe. Error reading PTY size, using defaults: %s. Error reading from child: %s. Error setting PTY size: %s. Got unexpected (key?) sequence `%s'. No handler for control sequence `%s' defined. Unable to convert characters from %s to %s. Unable to send data to child, invalid charset convertor Unknown pixel mode %d.
 Unrecognized identified coding system. _vte_conv_open() failed setting word characters can not run %s Project-Id-Version: 2.32
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-10-06 10:12+0000
PO-Revision-Date: 2016-02-08 00:37+0000
Last-Translator: Duarte Loreto <Unknown>
Language-Team: pt <gnome_pt@yahoogroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:07+0000
X-Generator: Launchpad (build 18115)
 Tentativa de definição de mapa NRC inválido '%c'. Tentativa de definição de mapa abrangente NRC inválido '%c'. Incapaz de abrir consola.
 Incapaz de processar a especificação de geometria indicada por --geometry Duplicado (%s/%s)! Erro (%s) a converter dados do filho, a ignorá-los. Erro ao compilar expressão regular "%s". Erro ao criar canal de sinal. Erro ao ler tamanho PTY, a utilizar valores por omissão: %s. Erro ao ler do filho: %s. Erro ao definir tamanho PTY: %s. Obtida sequência (chave?) inesperada `%s'. Nenhum manipulador definido para a sequência de controlo `%s'. Incapaz de converter caracteres de %s em %s. Incapaz de enviar dados para o filho, conversor de conjunto de caracteres inválido Modo de pixel %d desconhecido.
 Sistema de codificação identificado desconhecido. _vte_conv_open() falha ao definir caracteres de palavra incapaz de executar %s 