��    "      ,  /   <      �  &   �  %      #   F  %   j  
   �     �     �  	   �     �     �     �     �     �     �  	     	             %     6     J     V     \     b          �     �     �  	   �     �     �     �     �     �  �    &   �  *   �  *   �  %     
   @  	   K     U  
   o     z     �     �     �     �     �  	   �  	   �     �     �     �     
               %     B     H     P  "   j  	   �     �     �     �     �     �                  
                                             	             "                       !                                                           <span weight="bold">Connected</span>:  <span weight="bold">Encoding</span>:  <span weight="bold">Locale</span>:  <span weight="bold">XIM name</span>:  About Nabi Advanced Automatic reordering BackSpace Choseong English keyboard Hangul Hangul keyboard Hanja Hanja Options Jongseong Jungseong Keyboard Nabi Preferences Nabi: error message Orientation Shift Space The orientation of the tray. Total Tray Use simplified chinese XIM Server is not running XIM name: hangul(hanja) hanja hanja(hangul) per application per context Project-Id-Version: nabi
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-01-26 17:31+0900
PO-Revision-Date: 2008-10-24 23:30+0000
Last-Translator: Mykas0 <Mykas0@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:12+0000
X-Generator: Launchpad (build 18115)
 <span weight="bold">Conectado</span>:  <span weight="bold">Codificação</span>:  <span weight="bold">Localização</span>:  <span weight="bold">Nome XIM</span>:  Sobre Nabi Avançado Reordenar automaticamente Retroceder Choseong Teclado Inglês Hangul Teclado Hangul Hanja Opções de Hanja Jongseong Jungseong Teclado Preferências do Nabi Nabi: mensagem de erro Orientação Shift Espaço A orientação do tabuleiro. Total Bandeja Usar chinês simplificado O Servidor XIM não está a correr nome XIM: hangul(hanja) hanja hanja(hangul) por aplicação por contexto 