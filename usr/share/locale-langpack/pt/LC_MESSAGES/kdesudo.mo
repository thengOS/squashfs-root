��    (      \  5   �      p  "   q  ,   �     �     �     �     �  "        $  /   9      i  )   �     �     �     �     �  :   �  4   '     \     i  ]   �  $   �       	   '  6   1     h  *   v     �     �  2   �            )   2     \  2   w  6   �  !   �  !     	   %     /  �  A  "   
  ;   +
     g
     v
     �
     �
  #   �
     �
  -   �
  �     5   �     �               &  J   .  R   y     �  �   �  w   �  4     )   H     r  7   ~     �  <   �            .   5  #   d     �  ;   �  #   �  H     I   P  3   �  +   �     �  '                                                                         '       !   %       
   #      "                  (   	   &                                          $              (C) 2007 - 2008 Anthony Mercatante <b>Incorrect password, please try again.</b> <b>Warning: </b> Anthony Mercatante Command not found! Command: Do not display « ignore » button Do not keep password Do not show the command to be run in the dialog EMAIL OF TRANSLATORSYour emails Fake option for KDE's KdeSu compatibility Forget passwords Harald Sitter Jonathan Riddell KdeSudo Makes the dialog transient for an X app specified by winid Manual override for automatic desktop file detection Martin Böhm NAME OF TRANSLATORSYour names No command arguments supplied!
Usage: kdesudo [-u <runas>] <command>
KdeSudo will now exit... Please enter password for <b>%1</b>. Please enter your password. Priority: Process priority, between 0 and 100, 0 the lowest [50] Robert Gruber Specify icon to use in the password dialog Sudo frontend for KDE The command to execute The comment that should be displayed in the dialog Use existing DCOP server Use realtime scheduling Use target UID if <file> is not writeable Wrong password! Exiting... Your user is not allowed to run sudo on this host! Your user is not allowed to run the specified command! Your username is unknown to sudo! needs administrative privileges.  realtime: sets a runas user Project-Id-Version: kdesudo 3.4.2.1-1
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-05 00:10+0000
PO-Revision-Date: 2014-02-20 08:21+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Portuguese <traduz@debianpt.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:23+0000
X-Generator: Launchpad (build 18115)
 (C) 2007 - 2008 Anthony Mercatante <b>Palavra-passe Incorrecta, por favor tente outra vez.</b> <b>Aviso: </b> Anthony Mercatante Comando não encontrado! Comando: Não mostrar o botão « ignorar » Não manter a palavra-passe Não mostrar o comando a executar no diálogo ,,,,joelcalado@gmail.com,marcodasilva@gmail.com,Mykas0@gmail.com,,,,,diogolavareda9@hotmail.com,joelcalado@gmail.com,marcodasilva@gmail.com,Mykas0@gmail.com Opção falsa para compatibilidade com o KdeSu do KDE Esquecer as palavras-passe Harald Sitter Jonathan Riddell KdeSudo Torna transitório o diálogo de uma aplicação X especificada pelo winid Substituição manual da definição de detecção automática de ficheiro desktop Martin Böhm ,Launchpad Contributions:,Almufadado,Américo Monteiro,Joel Calado,Marco da Silva,Mykas0, ,Launchpad Contributions:,Almufadado,Américo Monteiro,Diogo Lavareda,Joel Calado,Marco da Silva,Mykas0 Nenhum argumento de comando fornecido!
Utilização: kdesudo [-u <correr_como>] <comando>
KdeSudo vai terminar agora... Por favor, introduza a palavra-passe para <b>%1</b>. Por favor, introduza a sua palavra-passe. Prioridade: Prioridade do processo, entre 0 e 100, 0 o mínimo [50] Robert Gruber Especificar o ícone a utilizar no diálogo da palavra-passe Frontend do sudo para KDE O comando a ser executado O comentário que deve ser exibido no diálogo Utilizar um servidor DCOP existente Usar agendamento em tempo-real Usar UID de destino se não se puder escrever no <ficheiro> Palavra-passe errada! A terminar... O seu utilizador não está autorizado a executar o sudo nesta máquina! O seu utilizador não está autorizado a executar o comando especificado! O seu nome de utilizador é desconhecido pelo sudo! necessita de privilégios administrativos.  tempo real: define um utilizador para "correr como" 