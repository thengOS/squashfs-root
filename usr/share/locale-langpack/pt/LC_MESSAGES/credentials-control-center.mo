��          �   %   �      P      Q     r     �  #   �  =   �  @   �     5     B     O     \     l  '   �     �  #   �  1   �  2        A  F   P  $   �  "   �  [   �  :   ;  X   v  S   �  '   #  �  K  ,        2     E  #   S  )   w  9   �     �     �     �     	     	  -   4	     b	  &   k	  5   �	  6   �	     �	  P   
  +   ^
  *   �
  J   �
  7      >   8  7   w  &   �                                                                                                    	               
           - Edit Web credentials settings Add account… All applications Another instance is already running Are you sure that you wish to remove this Ubuntu Web Account? Control whether this application integrates with Online Accounts Edit Options Grant access Legal notice Online Accounts Online Accounts preferences Online account credentials and settings Options Output version information and exit Please authorize Ubuntu to access your %s account Please authorize Ubuntu to access your %s account: Remove Account Run '%s --help' to see a full list of available command line options.
 Select to configure a new %s account Show accounts that integrate with: The Web Account which manages the integration of %s with your applications will be removed. The following applications integrate with your %s account: There are currently no account providers available which integrate with this application There are currently no applications installed which integrate with your %s account. Your online %s account is not affected. Project-Id-Version: gnome-control-center-signon
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-01 12:56+0000
PO-Revision-Date: 2016-02-18 09:33+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:14+0000
X-Generator: Launchpad (build 18115)
  - Editar configurações de credenciais Web Adicionar conta... Todas as apps Outra instância já em execução. Quer mesmo remover esta conta Ubuntu Web? Controla se esta aplicação se integra com contas online Editar opções Permitir acesso Aviso legal Contas online Preferências de contas online Credenciais e configurações de conta online Opções Mostrar informação de versão e sair Por favor, autorize o Ubuntu a aceder à sua conta %s Por favor, autorize o Ubuntu a aceder à sua conta %s: Remover conta Digite '%s --help' para ver a lista completa das opções de linha de comandos.
 Selecione para configurar uma nova conta %s Mostrar contas que estejam integradas com: A conta Web que gere a integração de %s com as suas apps será removida. As seguintes apps estão integradas com a sua conta %s: Sem provedores de conta disponíveis que integrem com esta app Sem apps instaladas que se integrem com a sua conta %s. A sua conta online %s não é afetada. 