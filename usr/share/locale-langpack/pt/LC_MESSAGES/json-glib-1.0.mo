��    -      �  =   �      �     �     �          <     \     n  I   �  &   �  :   �     4     9  !   L     n     �  $   �     �     �     �  +        F     Y      y  5   �  4   �  1     )   7  6   a     �  ,   �  I   �  =     0   ]  1   �  2   �  3   �  M   '	  N   u	  E   �	  %   

  '   0
  !   X
     z
  (   �
  8   �
  �  �
     �  #   �  '   �  &   �       #   6  R   Z  7   �  7   �          &  &   ?     f        /   �      �  '   �  $     6   >     u  -   �  %   �  7   �  9     8   T  )   �  8   �     �  +     E   -  8   s  1   �  1   �  2     2   C  E   v  F   �  ?     *   C  )   n      �     �  '   �  5   �           *              (          )         +   %   ,                                  !   "                       $      -                    &   
                               #      '             	              %s: %s: error closing: %s
 %s: %s: error opening file: %s
 %s: %s: error parsing file: %s
 %s: %s: error writing to stdout %s: missing files %s:%d:%d: Parse error: %s A GVariant dictionary entry expects a JSON object with exactly one member Error parsing commandline options: %s
 Expecting a JSON object, but the root node is of type `%s' FILE Format JSON files. GVariant class '%c' not supported Indentation spaces Invalid GVariant signature Invalid array index definition '%*s' Invalid first character '%c' Invalid set definition '%*s' Invalid slice definition '%*s' Invalid string value converting to GVariant JSON data is empty JSON data must be UTF-8 encoded Malformed slice expression '%*s' Missing closing symbol ')' in the GVariant tuple type Missing elements in JSON array to conform to a tuple Missing member name or wildcard after . character No node available at the current position Only one root node is allowed in a JSONPath expression Prettify output Root node followed by invalid character '%c' The current node is of type '%s', but an array or an object was expected. The current node is of type '%s', but an object was expected. The current position does not hold a string type The current position holds a '%s' and not a value The current position holds a '%s' and not an array The current position holds a '%s' and not an object The index '%d' is greater than the size of the array at the current position. The index '%d' is greater than the size of the object at the current position. The member '%s' is not defined in the object at the current position. Try "%s --help" for more information. Unexpected extra elements in JSON array Unexpected type '%s' in JSON node Validate JSON files. json-glib-format formats JSON resources. json-glib-validate validates JSON data at the given URI. Project-Id-Version: json-glib
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-03-01 22:13+0000
PO-Revision-Date: 2015-02-19 19:44+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:32+0000
X-Generator: Launchpad (build 18115)
 %s: %s: erro ao fechar: %s
 %s: %s: erro ao abrir ficheiro: %s
 %s: %s: erro ao processar ficheiro: %s
 %s: %s: erro ao escrever para o stdout %s: ficheiros em falta %s:%d:%d: Erro de processamento: %s Uma entrada de dicionário GVariant espera um objeto JSON com exatamente um membro Erro ao processar as opções da linha de comandos: %s
 Esperado um objeto JSON, mas o nó raiz é do tipo `%s' FICHEIRO Formatar ficheiros JSON. Classe GVariant '%c' não é suportada Espaços da indentação Assinatura GVariant é inválida Definição inválida de índice de lista '%*s' Primeiro caracter '%c' inválido Definição inválida de conjunto '%*s' Definição inválida de fatia '%*s' Valor inválido de expressão ao converter em GVariant Dados JSON estão vazios Dados JSON têm de estar codificados em UTF-8 Expressão '%*s' de fatia mal-formada Falta o símbolo de fecho ')' no tipo de tupla GVariant Elementos em falta na lista JSON para respeitar uma tuple Falta nome de membro ou caracter global após caracter . Nenhum nó disponível na posição atual Apenas um nó raiz é permitido numa expressão JSONPath Embelezar saída Nó raiz seguido do caracter inválido '%c' O nó atual é do tipo '%s', mas era esperada uma lista ou um objeto. O nó atual é do tipo '%s', mas era esperado um objeto. A posição atual não contém um tipo expressão A posição atual contém um '%s' e não um valor A posição atual contém um '%s' e não uma lista A posição atual contém um '%s' e não um objeto O índice '%d' é maior do que o tamanho da lista na posição atual. O índice '%d' é maior do que o tamanho do objeto na posição atual. O membro '%s' não está definido no objeto da posição atual. Tente "%s --help" para mais informações. Elementos extra inesperados na lista JSON Tipo '%s' inesperado no nó JSON Validar ficheiros JSON. json-glib-format formata recursos JSON. json-glib-validate valida dados JSON no URI indicado. 