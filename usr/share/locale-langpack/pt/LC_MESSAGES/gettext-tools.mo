��    �      ,  �   <
      �  B   �  9   �  F   6  @   }  +   �     �  ,   �           @     Y  "   x     �     �     �  !   �  *   �     #  3   3     g      |     �     �  .   �  4   �  ?   !  !   a     �  	   �  ?   �  7   �  O     *   ]  1   �     �     �     �  b   �  #   ]  m   �  l   �  <   \  <   �  <   �  F     8   Z  E   �  >   �  8     L   Q  N   �  S   �  X   A  N   �  K   �  G   5  T   }     �     �        "   $     G     ]  9   q  ?   �     �  I        Q     g  !   x  #   �  "   �  *   �  U     Z   b  F   �  >     &   C  9   j  -   �  M   �  4      G   U  =   �  B   �  O        n     �     �     �     �     �  <   �  >   *     i  $   �     �     �  7   �           -   6   @   $   w      �   '   �   *   �   ;   !     K!     h!     �!     �!     �!     �!  $   �!  $   "     2"     K"  $   i"     �"     �"     �"     �"     �"     #     /#  "   ?#     b#  *   y#     �#     �#     �#     �#     �#  5   $     M$     b$     s$     �$     �$     �$  N   �$  .   %  -   C%     q%     �%     �%     �%  ;   �%  +   &  ,   3&  *   `&  '   �&     �&     �&     �&  +   �&  =   +'  !   i'     �'     �'     �'  1   �'  �  �'  :   �)  %   *  >   6*  ?   u*     �*     �*  -   �*  "   +     )+  !   E+  %   g+     �+     �+     �+  *   �+  3   �+     1,  ;   ?,     {,  &   �,     �,     �,  7   �,  <   -  H   L-  %   �-     �-  
   �-  G   �-  K   .  e   b.  5   �.  C   �.  -   B/     p/  
   �/  b   �/  (   �/  l   0  �   �0  A   1  =   W1  <   �1  K   �1  ?   2  D   ^2  ?   �2  4   �2  I   3  H   b3  X   �3  W   4  U   \4  S   �4  G   5  U   N5     �5  (   �5  1   �5  3   6  &   H6      o6  P   �6  U   �6  $   77  S   \7     �7     �7  0   �7  2   8  1   G8  .   y8  M   �8  ^   �8  E   U9  ?   �9  -   �9  A   	:  (   K:  O   t:  0   �:  M   �:  D   C;  H   �;  P   �;     "<  "   ?<  (   b<     �<     �<     �<  F   �<  H    =     I=  1   g=  +   �=     �=  :   �=  +   >     ;>  G   S>  1   �>  4   �>  )   ?  ;   ,?  J   h?  !   �?      �?     �?     @     1@  !   B@  ,   d@  ,   �@     �@  '   �@  &   A     ,A  )   FA  .   pA     �A     �A     �A     �A  -   �A     +B  1   JB     |B     �B     �B      �B     �B  ;   �B     ;C     [C     mC     �C  $   �C  $   �C  Y   �C  1   BD  1   tD     �D      �D     �D  $   �D  >   E  5   ZE  '   �E  .   �E  +   �E     F     )F  ,   IF  9   vF  9   �F  &   �F     G  $   "G     GG  =   bG     !          l   t   E       �       �       j   u   _       �   �       |   w   �   e         >   �       7   �   m   h   G   v       T   q       *      2       b   d   s   �                   K   -       Q   P       g   ;              L   �      @   �   r   8   '       H       �   �   k   �       �       $      +   (       [   y   <              `      }   i       &   \      /   A       a   J          C       ]       9   M   �             �   �           �   �       f              %           o      ?   6       �       �       �   B   �         Y   W   �   I   )   =   �   �   V      �      "       :   ,           N   
      S   R                  x      F   D   �           �       p       0   5   Z      #   z   4       �       	   1       O   �   �   3           ~          n   U   ^   �   .   {      X            �       c              �              -V, --version               output version information and exit
   -h, --help                  display this help and exit
   -i, --indent                write the .po file using indented style
   -p, --properties-output     write out a Java .properties file
   def.po                      translations
  done.
 %d translated message %d translated messages %s and %s are mutually exclusive %s is only valid with %s %s is only valid with %s or %s %s is only valid with %s, %s or %s %s subprocess %s subprocess I/O error %s subprocess failed %s subprocess got fatal signal %d %s subprocess terminated with exit code %d %s%s: warning:  %s: warning: source file contains fuzzy translation %s:%d: iconv failure %s:%d: invalid string definition %s:%d: missing number after # ' , %d fuzzy translation , %d fuzzy translations , %d untranslated message , %d untranslated messages --join-existing cannot be used when output is written to stdout ...but this definition is similar <stdin> <unnamed> Applies a filter to all translations of a translation catalog.
 By default the input files are assumed to be in ASCII.
 By default the language is guessed depending on the input file name extension.
 C# compiler not found, try installing pnet C# virtual machine not found, try installing pnet Choice of input file language:
 Continuing anyway. Created %s.
 Creates a new PO file, initializing the meta information with values from the
user's environment.
 English translations for %s package Filters the messages of a translation catalog according to their attributes,
and manipulates the attributes.
 Found no .pot file in the current directory.
Please specify the input .pot file through the --input option.
 In the directive number %u, "%s" is not followed by a comma. In the directive number %u, '%c' is not followed by a digit. In the directive number %u, ',' is not followed by a number. In the directive number %u, '{' is not followed by an argument number. In the directive number %u, a choice contains no number. In the directive number %u, both the @ and the : modifiers are given. In the directive number %u, flags are not allowed before '%c'. In the directive number %u, the argument %d is negative. In the directive number %u, the argument number 0 is not a positive integer. In the directive number %u, the character '%c' is not a digit between 1 and 9. In the directive number %u, the character '%c' is not a valid conversion specifier. In the directive number %u, the precision's argument number 0 is not a positive integer. In the directive number %u, the substring "%s" is not a valid date/time style. In the directive number %u, the substring "%s" is not a valid number style. In the directive number %u, the token after '<' is not followed by '>'. In the directive number %u, the width's argument number 0 is not a positive integer. Informative output:
 Input file interpretation:
 Input file location in C# mode:
 Input file location in Java mode:
 Input file location:
 Input file syntax:
 Java compiler not found, try installing gcj or set $JAVAC Java virtual machine not found, try installing gij or set $JAVA Language specific options:
 Mandatory arguments to long options are mandatory for short options too.
 Operation modifiers:
 Output details:
 Output file location in C# mode:
 Output file location in Java mode:
 Output file location in Tcl mode:
 Report bugs to <bug-gnu-gettext@gnu.org>.
 The character that terminates the directive number %u is not a digit between 1 and 9. The character that terminates the directive number %u is not a valid conversion specifier. The directive number %u ends with an invalid character instead of '}'. The directive number %u starts with | but does not end with |. The result is written back to def.po.
 The string contains a lone '}' after directive number %u. The string ends in the middle of a directive. The string ends in the middle of a directive: found '{' without matching '}'. The string ends in the middle of a ~/.../ directive. The string refers to argument number %u but ignores argument number %u. The string refers to argument number %u in incompatible ways. The string refers to the argument named '%s' in incompatible ways. The string starts in the middle of a directive: found '}' without matching '{'. Unknown system error Usage: %s [OPTION] INPUTFILE
 Usage: %s [OPTION] [FILE]...
 Valid arguments are: _open_osfhandle failed ` a format specification for argument %u doesn't exist in '%s' a format specification for argument '%s' doesn't exist in '%s' ambiguous argument %s for %s at least two files must be specified at most one input file allowed backup type cannot create a temporary directory using template "%s" cannot create output file "%s" cannot create pipe cannot find a temporary directory, try setting $TMPDIR cannot remove temporary directory %s cannot remove temporary file %s communication with %s subprocess failed domain name "%s" not suitable as file name domain name "%s" not suitable as file name: will use prefix duplicate message definition end-of-file within string end-of-line within string error after reading "%s" error reading "%s" error reading current directory error while opening "%s" for reading error while opening "%s" for writing error while reading "%s" error while writing "%s" file error while writing to %s subprocess error writing stdout exactly 2 input files required exactly one input file required expected two arguments failed to create "%s" failed to create directory "%s" fdopen() failed file "%s" is not in GNU .mo format file "%s" is truncated found %d fatal error found %d fatal errors iconv failure inconsistent use of #~ invalid argument %s for %s invalid control sequence invalid plural expression invalid target_version argument to compile_java_class keyword "%s" unknown memory exhausted missing command name missing filter name no input file given no input files given plural expression can produce arithmetic exceptions, possibly division by zero plural expression can produce division by zero plural expression can produce negative values plural form has wrong index read from %s subprocess failed standard input standard output the argument to %s should be a single punctuation character this file may not contain domain directives this is the location of the first definition this message is used but not defined in %s this message is used but not defined... too many arguments too many errors, aborting warning: PO file header fuzzy
 warning: PO file header missing or invalid
 warning: older versions of msgfmt will give an error on this
 warning: this message is not used write error write to %s subprocess failed write to stdout failed xgettext cannot work without keywords to look for Project-Id-Version: gettext 0.10.23
Report-Msgid-Bugs-To: bug-gnu-gettext@gnu.org
POT-Creation-Date: 2015-12-28 08:26+0900
PO-Revision-Date: 2016-02-11 20:37+0000
Last-Translator: Nuno Oliveira <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 16:29+0000
X-Generator: Launchpad (build 18115)
Language: pt
   -V, --version mostra informação sobre a versão e sai
   -h, --help mostra esta ajuda e sai
   -i, --indent escreve o ficheiro .po usando estilo indentado
   -p, --properties-output escreve um ficheiro Java .properties
   traduções def.po
  terminado.
 %d mensagem traduzida %d mensagens traduzidas %s e %s são mutuamente exclusivas %s é apenas válido com %s %s é apenas válido com %s ou %s %s é apenas válido com %s, %s ou %s subprocesso %s Erro de I/O no subprocesso %s O subprocesso %s falhou O subprocesso %s recebeu um sinal fatal %d Subprocesso %s terminado com o código de saída %d %s%s: aviso:  %s: aviso: o ficheiro fonte contém traduções aproximadas %s:%d: falha de iconv %s:%d: definição de string inválida %s:%d: falta número após # ' , %d tradução aproximada , %d traduções aproximadas , %d mensagens não traduzida , %d mensagens não traduzidas --join-existing não pode ser usada quando a saída
é escrita no stdout ...mas esta definição é semelhante <stdin> <sem nome> Aplica um filtro a todas as traduções de um catálogo de tradução.
 Por predefinição os ficheiros de entrada assumem-se que estão em ASCII.
 Por predefinição a linguagem é adivinhada dependendo da extensão do nome do ficheiro de entrada.
 Compilador de C# não encontrado, tente instalar pnet A máquina virtual de C# não foi encontrada, tente instalar o pnet Escolha da linguagem do ficheiro de entrada:
 Continuando na mesma. Criou %s.
 Cria um novo ficheiro PO, inicializando a meta informação com valore do
ambiente do utilizador.
 Traduções portuguesas para o pacote %s Filtra as mensagens de um catálogo de tradução de acordo com os seus atributos,
e manipula os atributos.
 Não encontrou nenhum ficheiro .pot na directoria corrente.
Por favor especifique o ficheiro .pot de entrada através da opção --input.
 Na directiva numero %u, "%s" não está seguido por uma vírgula. Na directiva número %u, '%c' não é seguido por um dígito. Na directiva número %u, ',' não é seguido por um número. Na directiva número %u, '{' não está seguido de um número de argumento. Na directiva número %u, a escolha não contém nenhum número. Na directiva número %u, tanto os modificadores @ como : são dados. Na directiva numero %u, flags não são são aceites antes %c'. Na directiva número %u, o argumento %d é negativo. Na directiva número %u, o argumento número 0 não é um valor positivo. Na directiva número %u, o caracter '%c' não é um dígito entre 1 e 9. Na directiva numero %u, o carácter '%c' não é um especificador de conversão válido. Na directiva número %u, o argumento de precisão numero 0 não é um inteiro positivo. Na directiva número %u, a subexpressão "%s" não é um estilo de data/hora válido. Na directiva número %u, a subexpressão "%s" não é um estilo de número válido. Na directiva número %u, o token depois de '<' não é seguido por '>'. Na directiva número %u, o argumento de largura numero 0 não é um inteiro positivo. Saída informativa:
 Interpretação do ficheiro de entrada:
 Localização do ficheiro de entrada em modo C#:
 Localização do ficheiro de entrada em modo Java:
 Localização do ficheiro de entrada:
 Sintaxe do ficheiro de entrada:
 O compilador de java não foi encontrado, tente instalar o gcj ou definir $JAVAC A máquina virtual de Java não foi encontrada, tente instalar o gij ou definir $JAVA Opções específicas de linguagem:
 Argumentos obrigatórios para opções longas também o são para opções curtas.
 Modificadores de operação:
 Detalhes de saída:
 Localização do ficheiro de saída em modo C#:
 Localização do ficheiro de saída em modo Java:
 Localização do ficheiro de saída em modo Tcl:
 Reporte erros para <bug-gnu-gettext@gnu.org>.
 O caracter que termina a directiva número %u não é um dígito entre 1 e 9. O carácter que termina a directiva número %u não é um especificador de conversão válido. A directiva número %u acaba com um caracter inválido em vez de '}'. O número de directiva %u começa com | mas não termina com |. O resultado é escrito de volta para def.po.
 A expressão contém um '}' sozinho após a directiva número %u. A string acaba no meio de uma directiva. A expressão termina no meio de uma directiva: encontrado '{' sem encontrar '}' A string acaba no meio de uma directiva ~/.../ . A string refere-se ao argumento número %u mas ignora o argumento número %u. A string refere-se ao argumento número %u de maneira incompatível. A string refere-se ao argumento chamado '%s' em maneiras incompatíveis. A expressão começa no meio de uma directiva: encontrado '}' sem encontrar '{'. Erro desconhecido do sistema Uso: %s [OPÇÃO] FICHEIROENTRADA
 Utilização: %s [OPÇÃO][FICHEIRO]...
 Argumentos válidos: _open_osfhandle falhou ` uma especificação de formato para o argumento %u não existe em '%s' uma especificação de fromato para o argumento '%s' não existe em '%s' argumento %s ambíguo para %s pelo menos dois ficheiros devem ser especificados no máximo um ficheiro de entrada permitido tipo de backup impossível criar a pasta temporária usando o modelo "%s" impossível criar o ficheiro de saída "%s" impossível criar canal impossivel encontrar uma pasta temporária, experimente definir $TMPDIR não foi possível remover a pasta temporária %s não foi possível remover o ficheiro temporário %s Comunicação com o subprocesso %s falhou nome de domínio "%s" não apropriado como nome de ficheiro nome de domínio "%s" não apropriado como nome de ficheiro:
prefixo usado definição de mensagem duplicada fim-de-ficheiro dentro da cadeia fim-de-linha dentro da cadeia erro após a leitura de "%s" erro ao ler "%s" erro ao ler a directoria corrente erro durante a abertura de "%s" para leitura erro durante a abertura de "%s" para escrita erro durante a leitura de "%s" erro durante a escrita do ficheiro "%s" erro ao escrever para o subprocesso %s Erro a escrever em stdout são necessários exactamente 2 ficheiros exactamente um ficheiro de entrada necessário esperava dois argumentos falhou ao criar "%s" falhou ao criar directoria "%s" fdopen() falhou o ficheiro "%s" não está no formato .mo GNU o ficheiro "%s" está truncado encontrou %d erro fatal encontrou %d erros fatais falha iconv Uso inconsistente de #~ argumento inválido %s para %s sequência de controlo inválida expressão de plural inválida argumento de versão_alvo inválido para compile_java_class palavra chave "%s" desconhecida memória esgotada nome do comando em falta nome de filtro em falta nenhum ficheiro de entrada fornecido ficheiros de entrada não fornecidos expressão plural pode produzir excepções aritméticas, possivelmente divisão por zero expressão plural pode produzir divisão por zero expressão plural pode produzir valores negativos forma plural tem index errado Leitura do subprocesso %s falhou entrada standard canal de saída por defeito (stdout) o argumento para %s deve ser um único caracter de pontuação este ficheiro não pode conter directivas de domínio este é o local da primeira definição esta mensagem é usada mas não definida em %s esta mensagem é usada mas não definida... demasiados argumentos demasiados erros, interrompendo aviso: cabeçalho do ficheiro PO aproximado
 cuidado: cabeçalho do ficheiro PO em falta ou inválido
 aviso: versões antigas do msgfmt vão dar um erro nisto
 atenção: esta mensagem não é usada error de escrita escrita para o subprocesso %s falhou escrita para stdout falhou xgettext não pode funcionar sem palavras-chave para procurar 