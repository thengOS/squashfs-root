��    b      ,  �   <      H  "   I  �   l     �     	  p   	  	   �	     �	     �	     �	  
   �	     �	  	   �	     �	     �	     �	     �	  :   �	  	   
     '
     4
     D
     L
     T
     Z
     c
     h
     p
     u
  	   z
  -   �
  #   �
     �
     �
     �
  	          	         *  	   0  	   :     D     I     O     l     s     �     �     �     �     �     �     �     �     �  	   �     �     �     �       	   "     ,     9     G     U  '   j     �     �  1   �     �  9   �  9     7   K     �     �     �     �     �  
   �     �     �     �     �  	   �     �     �  	   �     �     �     �     �          
       
        &     4     =  �  B  "   �  �        �     �  p   �  
   =     H     O     V  
   b     m  
   u     �     �     �     �  @   �     �     �     
          $     +  	   3     =  
   C     N     ^  
   l  ?   w  -   �     �     �                '     7     G     P     ]  	   m     w     }     �     �     �     �     �     �     �     �     �          !     *  
   6  
   A     L     Z     v     �     �     �     �  "   �     �     �  /        8  !   ?     a          �     �  	   �     �     �     �     �     �     �     �     �     �  
     
             #  	   )  
   3     >     E  	   L     V     c     s     z                     C          ;          <   7   Q       0   9       @   M      "   B   /             A      ]         &       K   ^   O           G   H   #   4   Z                 T   =      a   6       :             [   -   E   
                     	   F   V       2              J   Y   b           P       *          _            !   (           5       $   '      X      U      >   S             .   \   R      `   I   ,       )      ?   1   D   8   3   %   W   N   L                  +    1KB;100KB;1MB;10MB;100MB;1GB;>1GB; Accessories;Education;Games;Graphics;Internet;Fonts;Office;Media;Customisation;Accessibility;Developer;Science & Engineering;Dash plugins;System Albums Applications Blues;Classical;Country;Disco;Funk;Rock;Metal;Hip-hop;House;New-wave;R&B;Punk;Jazz;Pop;Reggae;Soul;Techno;Other; Bookmarks Books Boxes Calendar Categories Code Community Dash plugins Date Decade Documentation Documents;Folders;Images;Audio;Videos;Presentations;Other; Downloads Encyclopedia Files & Folders Folders Friends Genre Graphics Help History Home Info Installed Last 7 days;Last 30 days;Last 6 months;Older; Last 7 days;Last 30 days;Last year; Last modified Local Local apps;Software center Locations More suggestions Most used Music My photos My videos News Notes Old;60s;70s;80s;90s;00s;10s; Online Online photos People Photos Popular online Radio Recent Recent apps Recently played Recently used Recipes Reference Results Scholar Search applications Search files & folders Search in Search music Search photos Search videos Search your computer Search your computer and online sources Size Songs Sorry, there is nothing that matches your search. Sources There are no photos currently available on this computer. There are no videos currently available on this computer. There is no music currently available on this computer. Top Type Upcoming Updates Videos Vocabulary Weather Web books; boxes; calendar; code; files; graphics; help; info; music; news; notes; photos; recipes; reference; videos;video; weather; web; Project-Id-Version: unity-scope-home
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-12 20:00+0000
PO-Revision-Date: 2016-02-11 12:22+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:22+0000
X-Generator: Launchpad (build 18115)
 1KB;100KB;1MB;10MB;100MB;1GB;>1GB; Acessórios;Educação;Jogos;Gráficos;Internet;Tipos de Letra;Escritório;Multimédia;Personalização;Acessibilidade;Desenvolvimento;Ciência e Engenharia;Scope;Sistema Álbuns Apps Blues;Clássica;Country;Disco;Funk;Rock;Metal;Hip-hop;House;New-wave;R&B;Punk;Jazz;Pop;Reggae;Soul;Techno;Outro; Marcadores Livros Caixas Calendário Categorias Código Comunidade Scopes do Dash Data Década Documentação Documentos;Pastas;Imagens;Áudio;Vídeos;Apresentações;Outros; Transferências Enciclopédia Ficheiros e pastas Pastas Amigos Género Gráficos Ajuda Histórico Página inicial Informações Instaladas Últimos 7 dias;Últimos 30 dias;Últimos 6 meses;Mais antigas; Últimos 7 dias;Últimos 30 dias;Último ano; Última modificação Local Apps locais;Centro de Software Locais Mais sugestões Mais utilizadas Músicas Minhas fotos Os meus vídeos Notícias Notas Antigas;60;70;80;90;00;10; Online Fotos na internet Pessoas Fotos Populares na internet Rádio Recentes Apps recentes Reproduzidas recentemente Usadas recentemente Receitas Referência Resultados Académico Procurar apps Procurar ficheiros e pastas Procurar em Procurar música Procurar fotografias Procurar vídeos Procurar no seu computador Procurar neste computador e online Tamanho Músicas Sem resultados que coincidam com a sua procura. Fontes Sem fotografias neste computador. Sem vídeos neste computador. Sem músicas neste computador. Topo Tipo Próximos Atualizações Vídeos Vocabulário Tempo Web livros; caixas; calendário; código; ficheiros; gráficos; ajuda; info; músicas; notícias; notas; fotos; receitas; referência; vídeos;vídeo; tempo; web; 