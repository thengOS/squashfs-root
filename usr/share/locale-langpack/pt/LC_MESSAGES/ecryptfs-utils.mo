��          D      l       �      �   !   �   &   �   R  �   �  >     �  3     (   L  Q  u                          Access Your Private Data Record your encryption passphrase Setup Your Encrypted Private Directory To encrypt your home directory or "Private" folder, a strong passphrase has been automatically generated. Usually your directory is unlocked with your user password, but if you ever need to manually recover this directory, you will need this passphrase. Please print or write it down and store it in a safe location. If you click "Run this action now", enter your login password at the "Passphrase" prompt and you can display your randomly generated passphrase. Otherwise, you will need to run "ecryptfs-unwrap-passphrase" from the command line to retrieve and record your generated passphrase. Project-Id-Version: ecryptfs-utils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-27 00:09+0000
PO-Revision-Date: 2013-02-23 11:27+0000
Last-Translator: Luis Fernandes <luisfernandes@luisfernandes.org>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:21+0000
X-Generator: Launchpad (build 18115)
 Aceder aos seus dados privados Guarde a sua palavra-passe secreta de encriptação Configure a sua pasta privada encriptada Para encriptar o seu diretório /home ou a sua pasta "Private", uma senha secreta foi gerada automaticamente. Normalmente o diretório é desbloqueado com a sua palavra-passe de utilizador, mas se precisar de o recuperar manualmente, precisará desta senha. Por favor, guarde-a num local seguro. Se escolher a opção "Executar esta ação agora", digite a sua palavra-passe de login no campo "Passphrase" para exibir a palavra-passe gerada aleatoriamente. Caso contrário, terá que executar "ecryptfs-unwrap-passphrase" a partir da linha de comandos para recuperar e registar a senha gerada. 