��    /      �  C                A   /     q     �  &   �     �     �                :     K  �   b     �                2     O     e     s     �  "   �     �     �     �  +   �  ,   "     O  !   f     �     �     �     �  �   �     �  U   �     	     7	     N	    ]	     u
     ~
     �
  	   �
     �
     �
  
   �
  �  �
     �  Y   �       !   '  (   I     r     �     �  &   �     �  (   �  �   %     �  (   �        +   .     Z     p       %   �  (   �     �     �       F   %  G   l     �  /   �  1   �     0     @     Q    e     q  d   �  )   �          :  [  M  
   �     �     �     �  )   �  2   �  	   2     	           "          #       *         
      /          !                                                            -                     &   ,                      +         )                $       %      '   .   (                    Resume normal boot (Use arrows/PageUp/PageDown keys to scroll and TAB key to select) === Detailed disk usage === === Detailed memory usage === === Detailed network configuration === === General information === === LVM state === === Software RAID state === === System database (APT) === CPU information: Check all file systems Continuing will remount your / filesystem in read/write mode and mount any other filesystem defined in /etc/fstab.
Do you wish to continue? Database is consistent: Drop to root shell prompt Enable networking Finished, please press ENTER IP and DNS configured IP configured Network connectivity: No LVM detected (vgscan) No software RAID detected (mdstat) Physical Volumes: Read-only mode Read/Write mode Recovery Menu (filesystem state: read-only) Recovery Menu (filesystem state: read/write) Repair broken packages Revert to old snapshot and reboot Run in failsafe graphic mode Snapshot System mode: System summary The option you selected requires your filesystem to be in read-only mode. Unfortunately another option you selected earlier, made you exit this mode.
The easiest way of getting back in read-only mode is to reboot your system. Try to make free space Trying to find packages you don't need (apt-get autoremove), please review carefully. Unknown (must be run as root) Update grub bootloader Volume Groups: You are now going to exit the recovery mode and continue the boot sequence. Please note that some graphic drivers require a full graphical boot and so will fail when resuming from recovery.
If that's the case, simply reboot from the login screen and then perform a standard boot. no (BAD) none not ok (BAD) ok (good) unknown (must be run as root) unknown (read-only filesystem) yes (good) Project-Id-Version: friendly-recovery
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2012-03-08 14:38-0500
PO-Revision-Date: 2014-03-03 23:34+0000
Last-Translator: Hugo.Batel <hugo.batel@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:23+0000
X-Generator: Launchpad (build 18115)
    Continuar arranque normal (Use as teclas direcionais/PageUP/PageDown para movimentar e a tecla TAB para selecionar) === Uso detalhado do disco === === Uso detalhado da memória === === Configuração detalhada da rede === === Informação geral === === Estado LVM === === Estado do software RAID === === Base de dados de Sistema (APT) === Informação de CPU: Verificar todos os sistemas de ficheiros Se continuar, irá remontar o seu sistema de ficheiros / em modo de leitura/escrita, e montará quaisquer outros sistemas de ficheiros definidos em /etc/fstab.
Pretende continuar? Base de dados é consistente: Alternar para a linha de comando de root Ativar a rede Trabalho acabado, por favor pressione ENTER IP e DNS configurados IP configurado Conetividade da rede: Não foi detetado nenhum LVM (vgscan) Não foi detetado software RAID (mdstat) Volumes Físicos: Modo de apenas leitura Modo de leitura/escrita Menu de Recuperação (estado do sistema de ficheiros: apenas leitura) Menu de Recuperação (estado do sistema de ficheiros: leitura/escrita) Reparar pacotes quebrados Voltar à imagem capturada anterior e reiniciar Correr gráficos em modo de configuração segura Capturar Imagem Modo do sistema: Sumário do sistema A opção que escolheu requer que o seu sistema de ficheiros esteja em modo apenas de leitura. Infelizmente, outra opção que escolheu anteriormente fez com que saísse deste modo.
A maneira mais fácil de voltar ao modo de apenas leitura é reiniciar o seu sistema. Tente arranjar espaço livre A tentar encontrar pacotes que não precisa (apt-get autoremove), por favor verifique com atenção. Desconhecido (deve ser corrido como root) Atualizar bootloader do grub Grupos de volumes: Irá agora sair do modo de recuperação e continuar com a sequência de arranque. Por favor note que alguns controladores gráficos requerem um arranque gráfico completo, e portanto irão falhar quando retomarem após a recuperação.
Se for este o caso, reinicie apenas a partir do ecrã de início de sessão e faça então um arranque normal. não (MAU) nenhum incorrecto (MAU) ok (bom) desconhecido (deve ser corrido como root) desconhecido (sistema de ficheiros só de leitura) sim (bom) 