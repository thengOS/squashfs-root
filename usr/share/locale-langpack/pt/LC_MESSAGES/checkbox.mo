��    �      �    L      �  	   �     �     �     �     �  5   �  $   .  3   S  &   �  ?   �  /   �  9        X     v  G   �     �     �     �  *   	     4  	   G  "   Q     t  &   �  +   �  )   �  9   �     8     P     l  �   x  -   $     R  "   m     �     �     �  2   �  
   �  #   �       )   !  r   K     �     �  )   �  �   �     �  3   �     �     �          '     9     E  )   Z     �  G   �     �     �     �       $        8  %   L     r     �     �     �  7   �  :   �     3     D     Q  (   e     �  "   �     �     �     �     �  /   �  *   "  )   M     w  [  �  �   �    �    �   K  �!  o  #  >  ~$  V  �%  h  '     }(  P   �(     �(  1   �(     &)     =)     Z)  #   c)     �)  0   �)  .   �)     �)     *     7*     S*     q*     �*     �*     �*     �*     �*     �*     +  '   2+  *   Z+  2   �+  5   �+  )   �+     ,     1,     K,     f,     �,     �,     �,  [   �,  X   7-  7   �-  $   �-     �-     �-  
   .  
   #.     ..     J.     X.     e.     w.     �.     �.     �.     �.     �.     �.     �.  
   /     /     &/  @   =/  A   ~/  B   �/  B   0  A   F0  B   �0  A   �0  A   1  D   O1  B   �1  E   �1  *   2  0   H2  #   y2      �2     �2     �2  F   �2  0   B3     s3  9   �3  0   �3  Z   �3  W   W4  *   �4  �  �4     6  	   �6     �6  	   �6     �6     �6     �6     �6     7  ?   7  e   ]7     �7  �   �7     �8     �8     �8     �8  	   �8     �8     �8     �8     �8     �8  5   �8  A   49     v9     y9     ~9  
   �9      �9  !   �9     �9  �  �9     �;     �;     �;     �;     �;  :   �;  *   <  7   7<  +   o<  E   �<  .   �<  F   =     W=     s=  g   �=     �=     >     >  1   2>     d>     ~>  )   �>     �>  ;   �>  4   ?  2   :?  L   m?     �?  '   �?     �?  �   @  D   �@  $   *A  ,   OA  	   |A     �A     �A  -   �A     �A  +   �A  
   B  7   'B  }   _B     �B     �B  (   �B  �   C  $   �C  <   �C  &   5D     \D      uD     �D     �D     �D  &   �D     �D  N   	E     XE     oE     �E     �E  )   �E  "   �E  -   �E     F  !   7F     YF     eF  =   }F  O   �F     G     %G     8G  4   HG     }G     �G  	   �G     �G     �G     �G  <   �G  *   7H  +   bH     �H  ^  �H  �   K    �K     �L  [  �M  �  OO  �  �P  h  mR  �  �S     sU  I   �U     �U  9   �U     'V  &   DV     kV  (   tV     �V  /   �V  <   �V  (   W  (   CW  ,   lW  .   �W  '   �W     �W     X     *X  %   CX     iX  '   �X  #   �X  ,   �X  2    Y  7   3Y  :   kY  .   �Y     �Y     �Y     Z     3Z  !   SZ  #   uZ  "   �Z  j   �Z  i   '[  G   �[  5   �[     \  )    \     J\     V\  "   h\     �\     �\     �\     �\     �\     �\     ]      ]     9]     J]     `]     g]     x]     �]  M   �]  N   ^  O   P^  L   �^  K   �^  L   9_  K   �_  K   �_  N   `  L   m`  O   �`  5   
a  9   @a  '   za      �a  *   �a  #   �a  k   b  <   ~b  0   �b  M   �b  3   :c  o   nc  _   �c  /   >d  �  nd     0f     Pf     ]f     jf     xf     �f     �f      �f      �f  X   g  �   Zg     �g  �   �g     �h     �h  
   �h     �h  	   �h     i     i     +i     3i     Ei  @   Ji  A   �i     �i     �i     �i     �i  "   �i  %   j     :j         �   C   �   0   �   S   g   �   j   1   �   t   "       Z              �   K       h       �       �   N   y   d   �   �   H   �       s          �   �   !          �   u   �   m   �       �       G   	       n   x          
   �   B   w   q   Q          �   �   M              �       }               U   �   z   �          �   �   �   l       �   �   �       ;          i   �   �             �   �          /   �   �   �   4   �   �   {   �   �      T   �       P   <           ]   6   �       �   -   [       o                   f       p   ?       O   a       #   �   v   �   �   �         Y   I   A   3   :   F   �   �       ,   �                   �          _   \       �   9   �   )   &       2             �          J   �   �   '      �   �       �   �                      ^                     r   V   %   �   $   (       +       .      ~      �   5   R   �   �   �   �   �   E              k   @       �   �   `   �   �   X   �   7   e              �   �   8       �   �   W              b      �   �      =   D   >   *   �   �   �           �       �   �   L   c       |     Results   Run   Selection  &Skip this test Abort signal from abort(3) Attaches a copy of /var/log/dmesg to the test results Attaches a report of CPU information Attaches a report of installed codecs for Intel HDA Attaches a report of sysfs attributes. Attaches the audio hardware data collection log to the results. Attaches the contents of the /etc/modules file. Attaches the contents of the various modprobe conf files. Attaches the firmware version Audio tests Automated test to store bluetooth device information in checkbox report Benchmark for each disk Bluetooth tests Bootchart information. Broken pipe: write to pipe with no readers Building report... CPU tests CPU utilization on an idle system. Camera tests Check that VESA drivers are not in use Check that hardware is able to run Unity 3D Check that hardware is able to run compiz Check the time needed to reconnect to a WIFI access point Checkbox System Testing Child stopped or terminated Codec tests Collect audio-related system information. This data can be used to simulate this computer's audio subsystem and perform more detailed tests under a controlled environment. Collect info on color depth and pixel format. Common Document Types Test Configuration override parameters. Continue Continue if stopped Deselect All Detects and displays disks attached to the system. Disk tests Disk utilization on an idle system. Done Email address must be in a proper format. Ensure the current resolution meets or exceeds the recommended minimum resolution (800x600). See here for details: Enter text:
 Error Exchanging information with the server... Failed to contact server. Please try
again or upload the following file name:
%s

directly to the system database:
https://launchpad.net/+hwdb/+submit Failed to process form: %s Failed to upload to server,
please try again later. Fingerprint reader tests Firewire disk tests Floating point exception Floppy disk tests Floppy test Further information: Gathering information from your system... Graphics tests Hangup detected on controlling terminal or death of controlling process Hibernation tests Hotkey tests Illegal Instruction Info Information not posted to Launchpad. Input Devices tests Internet connection fully established Interrupt from keyboard Invalid memory reference Kill signal List USB devices Make sure that the RTC (Real-Time Clock) device exists. Maximum disk space used during a default installation test Media Card tests Memory tests Miscellaneous tests Missing configuration file as argument.
 Monitor tests Move a 3D window around the screen Ne_xt Networking tests Next No Internet connection One of debug, info, warning, error or critical. Open and close 4 3D windows multiple times Open and close a 3D window multiple times Optical Drive tests PURPOSE:
     Check that external line out connection works correctly
STEPS:
     1. Insert cable to speakers (with built-in amplifiers) on the line out port
     2. Open system sound preferences, 'Output' tab, select 'Line-out' on the connector list. Click the Test button
     3. On the system sound preferences, select 'Internal Audio' on the device list and click 'Test Speakers' to check left and right channel
VERIFICATION:
     1. Do you hear a sound in the speakers? The internal speakers should *not* be muted automatically
     2. Do you hear the sound coming out on the corresponding channel? PURPOSE:
    Check that the various audio channels are working properly
STEPS:
    1. Click the Test button
VERIFICATION:
    You should clearly hear a voice from the different audio channels PURPOSE:
    DisplayPort audio interface verification
STEPS:
    1. Plug an external DisplayPort device with sound (Use only one HDMI/DisplayPort interface at a time for this test)
    2. Click the Test button
VERIFICATION:
    Did you hear the sound from the DisplayPort device? PURPOSE:
    HDMI audio interface verification
STEPS:
    1. Plug an external HDMI device with sound (Use only one HDMI/DisplayPort interface at a time for this test)
    2. Click the Test button
VERIFICATION:
    Did you hear the sound from the HDMI device? PURPOSE:
    This test will check that a USB audio device works correctly
STEPS:
    1. Connect a USB audio device to your system
    2. Click "Test", then speak into the microphone
    3. After a few seconds, your speech will be played back to you
VERIFICATION:
    Did you hear your speech played back through the USB headphones? PURPOSE:
    This test will check that headphones connector works correctly
STEPS:
    1. Connect a pair of headphones to your audio device
    2. Click the Test button to play a sound to your audio device
VERIFICATION:
    Did you hear a sound through the headphones and did the sound play without any distortion, clicks or other strange noises from your headphones? PURPOSE:
    This test will check that internal speakers work correctly
STEPS:
    1. Make sure that no external speakers or headphones are connected
       If testing a desktop, external speakers are allowed
    2. Click the Test button to play a brief tone on your audio device
VERIFICATION:
    Did you hear a tone? PURPOSE:
    This test will check that recording sound using an external microphone works correctly
STEPS:
    1. Connect a microphone to your microphone port
    2. Click "Test", then speak into the external microphone
    3. After a few seconds, your speech will be played back to you
VERIFICATION:
    Did you hear your speech played back? PURPOSE:
    This test will check that recording sound using the onboard microphone works correctly
STEPS:
    1. Disconnect any external microphones that you have plugged in
    2. Click "Test", then speak into your internal microphone
    3. After a few seconds, your speech will be played back to you.
VERIFICATION:
    Did you hear your speech played back? Peripheral tests Play back a sound on the default output and listen for it on the  default input. Please choose (%s):  Please type here and press Ctrl-D when finished:
 Power Management tests Press any key to continue... Previous Print version information and exit. Quit from keyboard Record the current resolution before suspending. Run Cachebench Read / Modify / Write benchmark Run Cachebench Read benchmark Run Cachebench Write benchmark Run Compress 7ZIP benchmark Run Compress PBZIP2 benchmark Run Encode MP3 benchmark Run GLmark2 benchmark Run GLmark2-ES2 benchmark Run GnuPG benchmark Run Himeno benchmark Run Lightsmark benchmark Run N-Queens benchmark Run Network Loopback benchmark Run Qgears2 OpenGL gearsfancy benchmark Run Qgears2 OpenGL image scaling benchmark Run Qgears2 XRender Extension gearsfancy benchmark Run Qgears2 XRender Extension image scaling benchmark Run Render-Bench XRender/Imlib2 benchmark Run Stream Add benchmark Run Stream Copy benchmark Run Stream Scale benchmark Run Stream Triad benchmark Run Unigine Heaven benchmark Run Unigine Santuary benchmark Run Unigine Tropics benchmark Run a stress test based on FurMark (OpenGL 2.1 or 3.2) Fullscreen 1920x1080 no antialiasing Run a stress test based on FurMark (OpenGL 2.1 or 3.2) Windowed 1024x640 no antialiasing Run gtkperf to make sure that GTK based test cases work Run x264 H.264/AVC encoder benchmark Running %s... SATA/IDE device information. SMART test Select All Software Installation tests Start testing Stop process Stop typed at tty Submission details Submit results Successfully finished testing! Suspend tests System Daemon tests System Testing Termination signal Test Test Again Test and exercise memory. Test for clock jitter. Test if the atd daemon is running when the package is installed. Test if the cron daemon is running when the package is installed. Test if the cupsd daemon is running when the package is installed. Test if the getty daemon is running when the package is installed. Test if the init daemon is running when the package is installed. Test if the klogd daemon is running when the package is installed. Test if the nmbd daemon is running when the package is installed. Test if the smbd daemon is running when the package is installed. Test if the syslogd daemon is running when the package is installed. Test if the udevd daemon is running when the package is installed. Test if the winbindd daemon is running when the package is installed. Test offlining CPUs in a multicore system. Test that the X is not running in failsafe mode. Test that the X process is running. Test the network after resuming. Test to detect audio devices Test to output the Xorg version Test to see that we have the same resolution after resuming as before. Test your system and submit results to Launchpad The file to write the log to. There is another checkbox running. Please close it first. This Automated test attempts to detect a camera. This is an automated test which performs read/write operations on an attached FireWire HDD This is an automated test which performs read/write operations on an attached eSATA HDD This test checks cpu topology for accuracy This test will verify that the volume levels are at an acceptable level on your local system.  The test will validate that the volume is greater than or equal to minvol and less than or equal to maxvol for all sources (inputs) and sinks (outputs) recognized by PulseAudio.  It will also validate that the active source and sink are not muted.  You should not manually adjust the  volume or mute before running this test. Timer signal from alarm(2) Type Text UNKNOWN USB tests Unknown signal Usage: checkbox [OPTIONS] User Applications User-defined signal 1 User-defined signal 2 Verify system storage performs at or above baseline performance Verify that storage devices, such as Fibre Channel and RAID can be detected and perform under stress. View results Welcome to System Testing!

Checkbox provides tests to confirm that your system is working properly. Once you are finished running the tests, you can view a summary report for your system. Wireless networking tests _Deselect All _Finish _No _Previous _Select All _Skip this test _Test _Test Again _Yes attaches the contents of various sysctl config files. https://help.ubuntu.com/community/Installation/SystemRequirements no skip test test again tty input for background process tty output for background process yes Project-Id-Version: checkbox
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-01-27 17:46+0000
PO-Revision-Date: 2015-05-14 19:11+0000
Last-Translator: Paulo Ventura <paulocventura@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:34+0000
X-Generator: Launchpad (build 18115)
  Resultados   Correr   Selecção  &Saltar este teste Abortar sinal de abort(3) Anexa uma cópia de /var/log/dmesg aos resultados do teste Anexa um relatório da informação do CPU Anexa um relatório de codecs instalados para Intel HDA Anexa um relatório dos atributos do sysfs. Anexa o registo de recolha de dados de hardware audio aos resultados. Anexa o conteúdo do ficheiro em /etc/modules. Anexa o conteúdo dos vários ficheiros de configuração do modprobe. Anexa a versão do firmware Testes de áudio Teste automatizado para armazenar informações sobre o dispositivo de bluetooth no relatório checkbox Avaliação para cada disco Testes Bluetooth Informações Bootchart. Canal quebrado: escrita para o canal sem leitores A construir relatório... testes  de CPU Utilização do CPU num sistema inactivo. Testes de câmara Verifica que controladores VESA não estão em utilização Verifica que o hardware consegue executar o Unity 3D Verifica que o hardware consegue executar o compiz Verificar o tempo necessário para voltar a ligar a um ponto de acesso Wi-Fi Checkbox - Testes de Sistema Processo dependente parado ou terminado Testes de codec Recolhe informação relacionada com a componente audio do sistema. Estes dados podem ser utilizados para simular o sub-sistema audio deste computador e levar a cabo testes mais detalhados num ambiente controlado. Recolhe informações sobre a profundidade da cor e formato de pixel Testar os Tipos de Documentos Comuns Substituir os parâmetros de configuração. Continuar Continuar se parado Desmarcar todos Detecta e exibe discos conectados ao sistema. Testes de disco rígido Utilização do disco num sistema inactivo. Concluído O endereço de email deve estar num formato apropriado. Assegurar que a resolução atual atende ou excede a resolução mínima recomendada (800x600). Veja aqui para mais detalhes: Introduza o texto:
 Erro A trocar informações com o servidor... Falhou ao contactar servidor. Por favor tente
outra vez ou envie o seguinte nome de ficheiro:
%s

directamente para a base de dados do sistema:
https://launchpad.net/+hwdb/+submit Falha ao processar o formulário: %s Falha ao enviar para o servidor,
por favor tente mais tarde. Testes de leitor de impressão digital Testes de disco firewire Excepção de vírgula flutuante Testes à disquete Teste à floppy Mais informações: A obter informação do seu sistema... Testes aos gráficos Desligamento detectado na consola de controlo ou morte do processo de controlo testes de hibernação testes das teclas de atalho Instrução Ilegal Info Informação não publicada no Launchpad. Testes aos dispositivos de entrada Ligação à Internet totalmente estabelecida Interrupção do teclado Referência de memória inválida Matar sinal Listar dispositivos USB Certifique-se que o dispositivo RTC (Real-Time Clock) existe. Utilização máxima de espaço em disco durante uma instalação teste padrão testes dos cartões media Testes à memória Testes diversos Falta do ficheiro de configuração como argumento.
 Testes de monitor Move uma janela 3D pelo ecrã Pró_ximo testes ao funcionamento da rede Próximo Sem ligação à Internet Um de depuração , informação , aviso , erro  ou critico. Abre e fecha 4 janelas 3D múltiplas vezes Abre e fecha uma janela 3D múltiplas vezes testes à drive óptica OBJETIVO:
     Verifica se a conexão externa funciona corretamente
PASSOS:
     1. Insere o cabo na linha verda (com amplificadores embutidos)
     2. Abre as preferências de som, seleciona a aba 'Output', seleciona 'Line-Out' na lista de ligações. Clica no botão de teste.
     3. Nas preferências de som, seleciona 'Áudio Interno' na lista de ligação e clica no 'Teste' para verificação o canal esquerdo e direito.
VERIFICAÇÃO:
     1. Ouviste algum som nas colunas/amplificadores? As colunas *não* podem estar mudas automaticamente
     2. Ouviste algum som saindo do canal correspondente? OBJECTIVO:
    Verificar que os vários canais de áudio funcionam correctamente
PASSOS:
    1. Clique no botão de teste
VERIFICAÇÃO:
    Deverá ouvir uma voz claramente desde os diferentes canais de áudio OBJETIVO:
    Verificação da interface de audio DisplayPort
PASSOS:
    1. Ligue um dispositivo DisplayPort externo com som (Use apenas uma interface DisplayPort de cada vez para este teste)
    2. Prima o botão "Teste"
VERIFICAÇÃO:
    Ouviu o som do dispositivo DisplayPort? OBJETIVO:
    Verificação da interface de audio HDMI
PASSOS:
    1. Ligue um dispositivo HDMI externo com som (Use apenas uma interface de HDMI de cada vez para este teste)
    2. Prima o botão "Teste"
VERIFICAÇÃO:
    Ouviu o som do dispositivo HDMI? OBJECTIVO:
    Este teste irá verificar se o dispositivo de áudio USB funciona correctamente
PASSOS:
    1. Ligue um dispositivo áudio USB ao seu sistema
    2. Clique em "Testar" e fale para o microfone
    3. Após alguns segundos, o que disse irá ser reproduzido
VERIFICAÇÃO:
    Ouviu alguma coisa que tenha dito pelos auscultadores USB? PROPÓSITO:
    ESte teste verifica se o conetor dos headphones funciona corretamente
INSTRUÇÕES:
    1. Ligue um par de headphones ao seu dispositivo de áudio
    2. Clique no botão Teste para tocar um som para o seu dispositivo de áudio
VERIFICAÇÃO:
    Conseguiu ouvir um som através dos headphones e o som tocou sem distorções, clicks ou outros ruídos estranhos a partir dos headphones? PROPÓSITO:
    Este  teste verifica se os altifalantes internos funcionam corretamente:
INSTRUÇÕES:
    1. Confirme que não estão ligados altifalantes externos ou headphones
       caso de trate de um computador de secretária, são permitidos altifalantes externos
    2. Clique no botão TESTE para ouvir um breve tom no seu dispositivo de audio
VERIFICAÇÃO:
    Conseguiu ouvir um tom? OBJECTIVO:
    Este teste irá verificar se a gravação de sons utilizando um microfone externo funciona correctamente
PASSOS:
    1. Ligue um microfone a porta de entrada de microfone
    2. Clique em "Testar" e fale para o microfone externo
    3. Após alguns segundos, o que disse irá ser reproduzido
VERIFICAÇÃO:
    Ouviu alguma coisa que tenha dito? PROPÓSITO:
    Este teste verifica que a gravação de som através do microfone interno funciona corretamente
INSTRUÇÕES:
    1. desconete qualquer microfone extern que esteja conetado
    2. Clique em "Teste", depois fale para o seu microfone interno
    3. Após alguns segundos, o seu discurso vai ser reproduzo de volta para si. 
VERIFICAÇÃO:
    Conseguiu ouvir a reprodução do discurso que efetuou? Testes de periféricos Reproduzir um som na saída predefinida e ouvi-la na entrada predefinida. Por favor escolha (%s):  Por favor escreva aqui e pressione Ctrl-D quando acabar:
 Testes à gestão de energia Prima qualquer tecla para continuar... Anterior Imprimir informação de versão e sair. Sair do teclado Guarda a resolução actual antes de suspender. Correr o benchmark de Leitura/Moficação/Leitura Cachebench Correr o benchmark de leitura Cachebench Correr o benchmark de escrita Cachebench Executar teste de benchmark ao Compress 7ZIP Executar teste de benchmark ao Compress PBZIP2 Correr o benchmark de codificação MP3 Correr o benchmark GLmark2 Correr o benchmark GLmark2-ES2 Executar benchmark GnuPG Executar teste de benchmark ao Himeno Correr o benchmark Lightsmark Executar teste de benchmark ao N-Queens Correr o benchmark Network Loopback Correr o benchmark Qgears2 OpenGL gearsfancy Correr o benchmark de image scaling Qgears2 OpenGL Correr o benchmark Qgears2 XRender Extension gearsfancy Correr o benchmark Qgears2 XRender Extension image scaling Correr o benchmark Render-Bench XRender/Imlib2 Correr o benchmark  Stream Add Correr o benchmark Stream Copy Correr o benchmark Stream Scale Correr o benchmark Stream Triad Correr o benchmark Unigine Heaven Correr o benchmark Unigine Santuary Correr o benchmark Unigine Tropics Correr um teste de stress baseado em FurMark (OpenGL 2.1 ou 3.2) Ecrã completo 1920x1080 sem antialiasing Correr um teste de stress baseado em FurMark (OpenGL 2.1 ou 3.2) Modo de janela 1024x640 sem antialiasing Execute o gtkperf para garantir que os testes baseados em GTK funcionam Executar teste de benchmark ao x264 H.264/AVC encoder A executar %s... Informações do dispositivo SATA / IDE . Teste SMART Seleccionar Todos Testes da instalação do software Iniciar testes Parar um processo Parou de digitar em tty Detalhes de apresentação Enviar resultados Foram concluídos os testes ! Suspender testes Testes do Sistema Daemon Teste de Sistema Sinal de terminação Testar Testar Novamente Teste de esforço à memória Testar o ruído do relógio. Verificar se o servidor atd está em execução quando o pacote é instalado. Verificar se o servidor cron está em execução quando o pacote é instalado. Verificar se o servidor cupsd está em execução quando o pacote é instalado. Testar se o servidor getty está em execução quando o pacote é instalado. Testar se o servidor init está em execução quando o pacote é instalado. Testar se o servidor klogd está em execução quando o pacote é instalado. Testar se o servidor nmbd está em execução quando o pacote é instalado. Testar se o servidor smbd está em execução quando o pacote é instalado. Testar se o servidor syslogd está em execução quando o pacote é instalado. Testar se o servidor udevd está em execução quando o pacote é instalado. Testar se o servidor winbindd está em execução quando o pacote é instalado. Testar o desligamento dos CPUs num sistema multicore. testar que o X não está a correr em modo de segurança. testar que o processo X está a correr. Testar a rede depois de resumir. Teste para detectar dispositivos de  audio Teste para a saída da versão Xorg Teste para verificar se continuamos com a mesma resolução de ecrã depois do retornar do estado anterior. Teste o seu sistema e submita os resultados para o Launchpad O ficheiro utilizado para escrever o relatório. Existe outra caixa de selecção a ser executada. Por favor feche-a primeiro. Este teste automatizado tenta detectar uma câmera. Isto é um teste automatizado que executa operações de leitura/escrita a um disco rígido ligado por Firewire Isto é um teste automatizado que executa operações de leitura/escrita num disco eSATA ligado Este teste verifica o tipo de cpu com precisão Este teste irá verificar se os níveis de volume são aceitáveis no seu sistema local. Este teste irá validar se o volume é maior ou igual do que o mínimo do volume e menor ou igual ao máximo do volume de todas as fontes (inputs) e sinks? (outputs) reconhecidos pela aplicação PulseAudio. Também irá validar se a fonte ativa e sink? não estão mudos. Não mudes nem ajustes o volume manualmente nem ponhas em modo mudo durante este teste. Sinal temporizador do alarme(2) Digite Texto DESCONHECIDO testes ao USB Sinal desconhecido Uso: checkbox [OPÇÕES] Aplicações de Utilizador Sinal 1 definido pelo utilizador Sinal 2 definido pelo utilizador Verificar se o desempenho do armazenamento do sistema é igual ou superior à linha base Verificar que os dispositivos de armazenamento, como Fibre Channel e RID, são detetados e conseguem desempenhar sob carga intensa. Ver resultados Bem-vindo ao Sistema de Testes|

O Checkbox fornece testes para confirmar que o seu sistema funciona correctamente. Quando acabar de executar os testes, pode consultar um relatório sumário sobre o seu sistema. Testes à rede sem fios _Desmarcar Todos _Finalizar _Não _Anterior _Seleccionar Todos _Saltar este teste _Testar _Testar novamente _Sim anexa o conteúdo de vários ficheiros de configuração sysctl. https://help.ubuntu.com/community/Installation/SystemRequirements não saltar testar testar novamente entrada tty para processo de fundo saída de tty para processos de fundo sim 