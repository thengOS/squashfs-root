��          �   %   �      p  l   q     �  0   �     #  ,   ?  ,   l  ,   �  '   �  -   �        (   =  (   f     �     �     �  �   �  ?   �     	  )        A     X     l     �     �     �     �     �  �  �  g   p     �  5   �     (	  -   G	  -   u	  -   �	  )   �	  .   �	      *
  *   K
  (   v
  $   �
  $   �
     �
  
  �
  P   �     G  (   T     }     �     �     �     �     �     �                                          
         	                                                                               -h, --help          display this help and exit
  -v, --version       display version information and exit
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' Copyright (C) %d Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
 General help using GNU software: <http://www.gnu.org/gethelp/>
 Hello, world! Print a friendly, customizable greeting.
 Report %s bugs to: %s
 Report bugs to: %s
 Unknown system error Usage: %s [OPTION]...
 ` hello, world memory exhausted write error Project-Id-Version: hello 1.3.4
Report-Msgid-Bugs-To: bug-hello@gnu.org
POT-Creation-Date: 2014-11-16 11:53+0000
PO-Revision-Date: 2015-12-05 00:19+0000
Last-Translator: António João Rendas <Unknown>
Language-Team: Português <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:33+0000
X-Generator: Launchpad (build 18115)
Language: pt
   -h, --help mostra esta ajuda e termina
  -v, --version mostra informação sobre a versão e termina
 Página web de %s : <%s>
 %s sítio oficial: <http://www.gnu.org/software/%s/>
 %s: opção inválida -- '%c'
 %s: opção '%c%s' não permite um argumento
 %s: opção '%s' é ambígua; possibilidades: %s: opção '--%s' não permite um argumento
 %s: a opção '--%s' requer um argumento
 %s: opção '-W %s' não permite um argumento
 %s: opção '-W %s' é ambígua
 %s: a opção '-W %s' requer um argumento
 %s: opção requer um argumento -- '%c'
 %s: opção não reconhecida '%c%s'
 %s: opção não reconhecida '--%s'
 " Copyright (C) %d Free Software Foundation, Inc.
Licença GPLv3+: GNU GPL versão 3 ou mais recente <http://gnu.org/licenses/gpl.html>
Este é um software livre: você é livre para mudar e redistribuí-lo.
Não há NENHUMA GARANTIA, até o limite permitido por lei.
 Ajuda geral sobre a utilização de software GNU: <http://www.gnu.org/gethelp/>
 Olá, mundo! Imprime um agradecimento personalizado.
 Relate %s erros (bugs) em: %s
 Relatar Bugs em: %s
 Erro desconhecido de sistema Utilização: %s [OPÇÃO]...
 " olá, mundo memória esgotada erro de escrita 