��          t      �              +   1  #   ]  6   �     �     �  0   �  /   $  )   T  x   ~  �  �  .   �  G   �  (   ,  F   U     �  (   �  8   �  (     2   F  �   y                                 
      	                     Could not create LUKS device %s Could not format device with file system %s Creating encrypted device on %s...
 Error: could not generate temporary mapped device name Error: device mounted: %s
 Error: invalid file system: %s
 Please enter your passphrase again to verify it
 The passphrases you entered were not identical
 This program needs to be started as root
 luksformat - Create and format an encrypted LUKS device
Usage: luksformat [-t <file system>] <device> [ mkfs options ]

 Project-Id-Version: cryptsetup
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-03 20:26+0100
PO-Revision-Date: 2014-04-30 16:24+0000
Last-Translator: Natan de Oliveira Pereira da Silva <natan673@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:25+0000
X-Generator: Launchpad (build 18115)
 Não foi possível criar o dispositivo LUKS %s Não foi possível formatar o dispositivo com o sistema de ficheiros %s A criar dispositivo encriptado em %s...
 Erro: não foi possível gerar nome do dispositivo mapeado temporário Erro: dispositivo montado: %s
 Erro: ficheiro de sistema inválido: %s
 Por favor, digite sua senha novamente para verificá-la
 As senhas que digitou, não são iguais
 Este programa necessita de ser iniciado como root
 luksformat - Criar e formatar um dispositivo encriptado com LUKS
Utilização: luksformat [-t <sistema ficheiros>] <dispositivo> [mkfs opções]

 