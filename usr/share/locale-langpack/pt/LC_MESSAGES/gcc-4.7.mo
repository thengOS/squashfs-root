��    %      D  5   l      @  0   A  .   r  ;   �  ]   �  y   ;  ,   �  *   �  ,        :     F     T     `     n       ,   �  )   �     �     �     	          0     ?     N     ]     |     �     �     �     �     �     �     �     �  *        ;  /   Z  �  �  4   X	  0   �	  =   �	  a   �	  |   ^
  6   �
  6     9   I  
   �     �  
   �     �     �     �  2   �  )        D     S     b     q     �     �     �     �     �     �     �               #     6     E     T  1   l      �  5   �                            #                                          $                
                  "                 %           	                                       !        
write_c_file - output name is %s, prefix is %s
 %<-fobjc-gc%> is ignored for %<-fgnu-runtime%> %<-fobjc-sjlj-exceptions%> is ignored for %<-fgnu-runtime%> %<-fobjc-sjlj-exceptions%> is ignored for %<-fnext-runtime%> when %<-fobjc-abi-version%> >= 2 %<-fobjc-sjlj-exceptions%> is the only supported exceptions system for %<-fnext-runtime%> with %<-fobjc-abi-version%> < 2 %d constructor found
 %d constructors found
 %d destructor found
 %d destructors found
 %d frame table found
 %d frame tables found
 (anonymous) ({anonymous}) <anonymous> [Leaving %s]
 [cannot find %s] collect2 version %s
 creating selector for nonexistent method %qE expected %<#pragma omp section%> or %<}%> expected %<(%> expected %<)%> expected %<,%> expected %<,%> or %<)%> expected %<.%> expected %<:%> expected %<;%> expected %<;%>, %<,%> or %<)%> expected %<=%> expected %<>%> expected %<@end%> expected %<[%> expected %<]%> expected %<while%> expected %<{%> expected %<}%> expected end of line non-objective-c type '%T' cannot be caught return not followed by barrier trying to encode non-integer type as a bitfield Project-Id-Version: gcc-4.7
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2012-09-19 14:50+0000
PO-Revision-Date: 2014-07-26 17:22+0000
Last-Translator: Ygor Rocha <ygorreis@hotmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 19:11+0000
X-Generator: Launchpad (build 18115)
 
write_c_file - nome de saída é %s, prefixo é %s
 %<-fobjc-gc%> é ignorado para %<-fgnu-runtime%> %<-fobjc-sjlj-exceptions%> é ignorado para %<-fgnu-runtime%> %<-fobjc-sjlj-exceptions%> é ignorada para %<-fnext-runtime%> quando %<-fobjc-abi-version%> >= 2 %<-fobjc-sjlj-exceptions%> é a única exceção de sistema suportada para %<-fnext-runtime%> com %<-fobjc-abi-version%> < 2 %d construtor encontrado
 %d construtores encontrados
 %d destruidor encontrado
 %d destruidores encontrados
 %d tabela frame encontrada
 %d tabelas frame encontradas
 (anónimo) ({anónimo}) <anónimo> [A deixar %s]
 [não consegue encontrar %s] versão collect2 %s
 criando selector para o método não existente %qE esperado %<#pragma omp section%> ou %<}%> esperado %<(%> esperado %<)%> esperado %<,%> esperado %<,%> ou %<)%> esperado %<.%> esperado %<:%> esperado %<;%> esperado %<;%>, %<,%> ou %<)%> esperado %<=%> esperado %<>%> expected %<@end%> esperado %<[%> esperado %<]%> esperado %<while%> esperado %<{%> esperado %<}%> final de linha esperado tipo non-objective-c '%T' não pode ser capturado retorno não seguido de barreira tentando codificar um tipo não inteiro como bitfield 