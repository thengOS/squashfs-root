��          �   %   �      P     Q     k     �     �      �     �     �          $     ?     Z     y     �  1  �     �     �     �  "     4   :     o      v     �  6   �  (   �  1     �  D     �     �  #     *   ;  $   f     �     �     �  "   �  #   �  '   	      5	     V	  &  p	     �
     �
  +   �
     �
  <     	   L  #   V      z  ?   �  7   �  8                                                	                       
                                                         %s: illegal argument: %s
 Error mapping into memory Error retrieving chunk extents Error retrieving file stat Error retrieving page cache info Error unmapping from memory Error while reading Error while tracing Failed to set CPU priority Failed to set I/O priority File vanished or error reading Ignored far too long path Ignored relative path PATH should be the location of a mounted filesystem for which files should be read.  If not given, the root filesystem is assumed.

If PATH is not given, and no readahead information exists for the root filesystem (or it is old), tracing is performed instead to generate the information for the next boot. Pack data error Pack too old Read required files in advance Unable to determine pack file name Unable to obtain rotationalness for device %u:%u: %s [PATH] detach and run in the background dump the current pack file how to sort the pack file when dumping [default: path] ignore existing pack and force retracing maximum time to trace [default: until terminated] Project-Id-Version: ureadahead
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-29 15:05+0000
PO-Revision-Date: 2010-03-05 00:41+0000
Last-Translator: Tiago Silva <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:47+0000
X-Generator: Launchpad (build 18115)
 %s: argument inválido: %s
 Erro ao mapear na memória Erro ao obter extensões de bocados Erro ao recuperar estatística do ficheiro Erro ao obter informações de cache Erro ao desmapear da memória Erro ao ler Erro ao rastrear Falha ao definir prioridade da CPU Falhou ao definir prioridade de I/O Ficheiro desapareceu ou erro de leitura Caminho demasiado longo ignorado Caminho relativo ignorado O CAMINHO para achar o sistema tem que ser verdadeiro. Se não for achado o sistema raiz é assumido.

Se CAMINHO não é dado, e nenhuma informação existe para o sistema de arquivos raiz (ou é velho), o rastreio é realizado em vez de gerar as informações para a próxima inicialização. Erro nos dados de pacote Pacote muito antigo leia ficheiros necessários antecipadamente O pacote não pode ser nomeado Incapaz de obter rotationalness para dispositivo % u:% u:% s [CAMINHO] separar e executar em segundo plano apagar o pacote do arquivo atual como ordenar o ficheiro de pacote ao remover [padrão: caminho] ignorar pacote existente e forçar o rastreio novamente tempo máximo para procura [padrão: até ter terminado] 