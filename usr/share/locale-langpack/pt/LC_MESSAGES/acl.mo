��    5      �  G   l      �     �     �     �     �     �  )   �  )   %     O  �   d  7  0  �  h  B   J
  l  �
  �   �  Z     '   �  '        *  $   H     m     �  &   �  2   �  3   �  /   2  /   b  =   �     �  %   �  2        D  $   \  &   �  +   �  '   �  ,   �  &   )  '   P  *   x  +   �     �     �     �          #     :  &   X          �     �     �     �  �  �     �     �     �     �     �  ,   �  ,        H  �   \  (  #  O  L  B   �  j  �  x   J  l   �  7   0  :   h  "   �  3   �     �  $     3   7  5   k  ;   �  B   �  7      R   X     �  +   �  <   �  '   0  '   X  1   �  :   �  7   �  =   %   8   c   >   �   +   �   1   !     9!     P!     h!     z!     �!  !   �!  0   �!     "     "     "     4"     T"                 	              
   5                                 -           !          1      &             *      3   +      $                    )       "              %   #         ,                         '   /   .         4          0   2   (    	%s -B pathname...
 	%s -D pathname...
 	%s -R pathname...
 	%s -b acl dacl pathname...
 	%s -d dacl pathname...
 	%s -l pathname...	[not IRIX compatible]
 	%s -r pathname...	[not IRIX compatible]
 	%s acl pathname...
       --set=acl           set the ACL of file(s), replacing the current ACL
      --set-file=file     read ACL entries to set from file
      --mask              do recalculate the effective rights mask
   -R, --recursive         recurse into subdirectories
  -L, --logical           logical walk, follow symbolic links
  -P, --physical          physical walk, do not follow symbolic links
      --restore=file      restore ACLs (inverse of `getfacl -R')
      --test              test mode (ACLs are not modified)
   -a,  --access           display the file access control list only
  -d, --default           display the default access control list only
  -c, --omit-header       do not display the comment header
  -e, --all-effective     print all effective rights
  -E, --no-effective      print no effective rights
  -s, --skip-base         skip files that only have the base entries
  -R, --recursive         recurse into subdirectories
  -L, --logical           logical walk, follow symbolic links
  -P, --physical          physical walk, do not follow symbolic links
  -t, --tabular           use tabular output format
  -n, --numeric           print numeric user/group identifiers
  -p, --absolute-names    don't strip leading '/' in pathnames
   -d, --default           display the default access control list
   -m, --modify=acl        modify the current ACL(s) of file(s)
  -M, --modify-file=file  read ACL entries to modify from file
  -x, --remove=acl        remove entries from the ACL(s) of file(s)
  -X, --remove-file=file  read ACL entries to remove from file
  -b, --remove-all        remove all extended ACL entries
  -k, --remove-default    remove the default ACL
   -n, --no-mask           don't recalculate the effective rights mask
  -d, --default           operations apply to the default ACL
   -v, --version           print version and exit
  -h, --help              this help text
 %s %s -- get file access control lists
 %s %s -- set file access control lists
 %s: %s in line %d of file %s
 %s: %s in line %d of standard input
 %s: %s: %s in line %d
 %s: %s: Cannot change mode: %s
 %s: %s: Cannot change owner/group: %s
 %s: %s: Malformed access ACL `%s': %s at entry %d
 %s: %s: Malformed default ACL `%s': %s at entry %d
 %s: %s: No filename found in line %d, aborting
 %s: %s: Only directories can have default ACLs
 %s: No filename found in line %d of standard input, aborting
 %s: Option -%c incomplete
 %s: Option -%c: %s near character %d
 %s: Removing leading '/' from absolute path names
 %s: Standard input: %s
 %s: access ACL '%s': %s at entry %d
 %s: cannot get access ACL on '%s': %s
 %s: cannot get access ACL text on '%s': %s
 %s: cannot get default ACL on '%s': %s
 %s: cannot get default ACL text on '%s': %s
 %s: cannot set access acl on "%s": %s
 %s: cannot set default acl on "%s": %s
 %s: error removing access acl on "%s": %s
 %s: error removing default acl on "%s": %s
 %s: malloc failed: %s
 %s: opendir failed: %s
 Duplicate entries Invalid entry type Missing or wrong entry Multiple entries of same type Try `%s --help' for more information.
 Usage:
 Usage: %s %s
 Usage: %s [-%s] file ...
 preserving permissions for %s setting permissions for %s Project-Id-Version: acl
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-07 11:10+0000
PO-Revision-Date: 2009-11-05 07:16+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:54+0000
X-Generator: Launchpad (build 18115)
 	%s -B caminho...
 	%s -D caminho...
 	%s -R caminho...
 	%s -b acl dacl caminho...
 	%s -d dacl caminho...
 	%s -l caminho...	 [incompatível com IRIX]
 	%s -r caminho... 	[incompatível com IRIX]
 	%s caminho acl...
       --set=acl define os ACL dos ficheiros, substituindo a ACL existente
      --set-file=file Ler as entradas de ACL a definir do ficheiro 
      --mask Recalcular a máscara de direitos efectiva
   -R, --recursive espelha-se nas subdirectorias
  -L, --logical Caminho lógico, segue ligações simbólicas
  -P, --physical caminho físico, não segue ligações simbólicas
      --restore=file restaura ACLs (inverso de  `getfacl -R')
      --test modo de teste (ACLs não são modificados)
   -a,  --access           exibir apenas a lista de controlo de acesso ao ficheiro
  -d, --default           mostrar apenas a lista de controlo de acesso por defeito
  -c, --omit-header       não mostrar o cabeçalho de comentários
  -e, --all-effective     imprimir todos os direitos efetivos
  -E, --no-effective      não imprimir direitos efetivos
  -s, --skip-base         saltar ficheiros que têm apenas entradas de base
  -R, --recursive         regredir nos subdiretórios
  -L, --logical           andar simbólico, seguir ligações simbólicas
  -P, --physical          andar físico, não seguir ligações simbólicas
  -t, --tabular           usar formato de saída tabular
  -n, --numeric           imprimir identificadores numéricos de utilizador/grupo
  -p, --absolute-names    não eliminar '/' iniciais nos nomes de caminhos
   -d, --default exibe a lista de controlo de acessos por omissão
   -m, --modify=acl modifica os ACL(s) dos ficheiro(s)
  -M, --modify-file=file Ler as entradas ACL a modificar no ficheiro
  -x, --remove=acl Remover as entradas dos ACL(s) dos ficheiro(s)
  -X, --remove-file=file ler entradas ACL a remover do ficheiro
  -b, --remove-all remover todas as entradas ACL extendidas
  -k, --remove-default Remover o ACL por defeito
   -n, --no-mask sem recalcular a máscara de direitos efectiva
  -d, --default As operações aplicam o ACL por defeito
   -v, --version           mostra a versão e sai do programa
  -h, --help              esta janela de ajuda
 %s %s -- receber listas de controlo acesso a ficheiros
 %s %s -- ajustar listas de controle a acesso de ficheiros
 %s: %s na linha %d do ficheiro %s
 %s: %s na linha  %d do método de entrada standard
 %s: %s: %s na linha %d
 %s: %s: Não pode mudar de modo: %s
 %s: %s: Não é possível modificar dono/grupo: %s
 %s: %s: Acesso ACL malformado `%s': %s na entrada %d
 %s: %s: ACL por omissão malformado `%s': %s na entrada %d
 %s: %s: Nenhum nome de ficheiro encontrado na linha %d, abortando
 %s: %s: Apenas directorias podem ter ACLs por omissão
 %s: Nenhum nome de ficheiro encontrado na linha %d de entrada standard, abortando
 %s: Opção -%c incompleta
 %s: Opção -%c: %s caracter aproximado %d
 %s: Removendo os '/' iniciais de nomes de caminho absolutos
 %s: Método de entrada por defeito: %s
 %s: accesso ACL '%s': %s na entrada %d
 %s: não é possível ter acesso ACL no '%s': %s
 %s: não é possível ter acesso ao texto ACL no '%s': %s
 %s: não é possível ter ACL por omissão no '%s': %s
 %s: não é possível ter texto ACL por omissão no '%s': %s
 %s: não é possível configurar acesso acl no "%s": %s
 %s: não é possível configurar acl por omissão no "%s": %s
 %s: erro ao remover acesso acl no "%s": %s
 %s: erro ao remover acl por omissão no "%s": %s
 %s: falhou malloc: %s
 %s: falhou opendir: %s
 Entrada duplicada Tipo de entrada inválida Erro na entrada ou inexistente Múltiplas entradas do mesmo tipo Experimente `%s --help' para mais informação.
 Uso:
 Uso: %s %s
 Uso: %s [-%s] ficheiro ...
 Preservando permissões para %s Definindo permissões para %s 