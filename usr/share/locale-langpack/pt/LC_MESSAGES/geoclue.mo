��          �      �       H  /   I     y     �  !   �  6   �  !        $     7     S  7   s  Y   �            �    C   �          "  "   ?  8   b  "   �     �  $   �  )   �  ;   !  U   ]     �     �                  	                                
               Allow '%s' to access your location information? Demo geoclue agent Demo geolocation application Enable submission of network data Exit after T seconds of inactivity. Default: 0 (never) Exit after T seconds. Default: 30 Geoclue Demo agent Geolocation service in use
 Geolocation service not in use
 Nickname to submit network data under (2-32 characters) Request accuracy level A. Country = 1, City = 4, Neighborhood = 5, Street = 6, Exact = 8. Where am I? geolocation; Project-Id-Version: geoclue-2.0
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-12 09:11+0000
PO-Revision-Date: 2016-03-31 08:31+0000
Last-Translator: Bruno Ramalhete <bram.512@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:33+0000
X-Generator: Launchpad (build 18115)
 Permitir '%s' para dar acesso à sua informação de localização? Agente geoclue demo App de geolocalização Demo Ativar submissão de rede de dados Sair após T segundos de inatividade. Padrão: 0 (nunca) Sair após T segundos. Padrão: 30 Agente Geoclue Demo Serviço de Geolocalização em uso
 Serviço de Geolocalização não em uso
 Alcunha para submeter dados de rede sobre (2-32 caracteres) Pedir precisão nível A. País = 1, Cidade = 4, Vizinhança = 5, Rua = 6, Exato = 8. Onde estou? geolocalização; 