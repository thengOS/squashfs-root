��    b      ,  �   <      H     I  )   Y     �     �     �     �     �     	     	  '   .	  (   V	     	     �	  0   �	     �	     
     
     '
     E
     \
  8   r
  6   �
  $   �
  %     %   -     S  	   j  +   t  !   �  6   �     �  (     J   B  ,   �     �  #   �     �     
  (   "  )   K  W   u  O   �  /     7   M  #   �     �  )   �  %   �  8     -   P  '   ~  &   �  "   �  5   �  -   &  4   T  3   �  %   �  ,   �  "        3  A   O  !   �     �     �  
   �     �  	   �  L   �  !   :     \     k     y     �  I   �  �  �  1   �          ?     V     m  *   �     �     �     �  
   �     �     �  3   �               %     *     1     6     U     [  �  b       7   *  +   b     �  $   �  %   �     �          /  ,   A  ,   n      �     �  :   �          1     =     W     w     �  5   �  5   �  &     *   @  (   k     �     �  ?   �  *   �  ?   $  #   d  ,   �  N   �  =        B  /   a  &   �  !   �  9   �  .     \   C  Y   �  6   �  :   1   +   l   *   �   9   �   9   �   @   7!  E   x!  -   �!  /   �!  ,   "  A   I"  3   �"  2   �"  D   �"  6   7#  =   n#  /   �#  (   �#  N   $  "   T$     w$     ~$  
   �$     �$     �$  N   �$  &   
%     1%     @%     N%     m%  K   �%    �%  3   �(  !   )     ))     @)     W)  *   p)     �)     �)     �)     �)     �)     �)  (   �)     �)     *     *     *     *  '   $*     L*     R*     V   :       [              S      M   -                 )               +   %   C   D   @         B   H       J       U   \      6   >   7   4   	   a   ;   P   <       .   /   5               9           3       &             E         A   L   I   `      Z   b      2       ]           W              '   _   1       X   N       Q          $   "   #   F       ,   
             =   R   Y   G                 *   8       ^       T           0          O       (   ?      K         !           			Call graph

 		     Call graph (explanation follows)

 	%d basic-block count record
 	%d basic-block count records
 	%d call-graph record
 	%d call-graph records
 	%d histogram record
 	%d histogram records
 


flat profile:
 

Top %d Lines:

     Line      Count

 
%9lu   Total number of line executions
 
Each sample counts as %g %s.
 
Execution Summary:

 
granularity: each sample hit covers %ld byte(s)  <cycle %d as a whole> [%d]
  <cycle %d>  for %.2f%% of %.2f %s

  for %.2f%% of %.2f seconds

  no time accumulated

  no time propagated

 %6.6s %5.5s %7.7s %11.11s %7.7s %7.7s     <spontaneous>
 %6.6s %5.5s %7.7s %7.7s %7.7s %7.7s     <spontaneous>
 %9.2f   Average executions per line
 %9.2f   Percent of the file executed
 %9ld   Executable lines in this file
 %9ld   Lines executed
 %c%c/call %s: %s: found bad tag %d (file corrupted?)
 %s: %s: not in executable format
 %s: %s: unexpected EOF after reading %u of %u samples
 %s: %s: unexpected end of file
 %s: -c not supported on architecture %s
 %s: Only one of --function-ordering and --file-ordering may be specified.
 %s: address size has unexpected value of %u
 %s: can't do -c
 %s: can't find .text section in %s
 %s: could not locate `%s'
 %s: could not open %s.
 %s: debugging not supported; -d ignored
 %s: different scales in histogram records %s: dimension abbreviation changed between histogram records
%s: from '%c'
%s: to '%c'
 %s: dimension unit changed between histogram records
%s: from '%s'
%s: to '%s'
 %s: don't know how to deal with file format %d
 %s: file '%s' does not appear to be in gmon.out format
 %s: file `%s' has bad magic cookie
 %s: file `%s' has no symbols
 %s: file `%s' has unsupported version %d
 %s: file too short to be a gmon file
 %s: found a symbol that covers several histogram records %s: gmon.out file is missing call-graph data
 %s: gmon.out file is missing histogram
 %s: incompatible with first gmon file
 %s: overlapping histogram records
 %s: profiling rate incompatible with first gmon file
 %s: ran out room for %lu bytes of text space
 %s: somebody miscounted: ltab.len=%d instead of %ld
 %s: sorry, file format `prof' is not yet supported
 %s: unable to parse mapping file %s.
 %s: unexpected EOF after reading %d/%d bins
 %s: unknown demangling style `%s'
 %s: unknown file format %s
 %s: warning: ignoring basic-block exec counts (use -l or --line)
 %s:%d: (%s:0x%lx) %lu executions
 %time *** File %s:
 <cycle %d> <indirect child> <unknown> Based on BSD gprof, copyright 1983 Regents of the University of California.
 File `%s' (version %d) contains:
 Flat profile:
 GNU gprof %s
 Index by function name

 Report bugs to %s
 This program is free software.  This program has absolutely no warranty.
 Usage: %s [-[abcDhilLsTvwxyz]] [-[ACeEfFJnNOpPqSQZ][name]] [-I dirs]
	[-d[num]] [-k from/to] [-m min-count] [-t table-length]
	[--[no-]annotated-source[=name]] [--[no-]exec-counts[=name]]
	[--[no-]flat-profile[=name]] [--[no-]graph[=name]]
	[--[no-]time=name] [--all-lines] [--brief] [--debug[=level]]
	[--function-ordering] [--file-ordering] [--inline-file-names]
	[--directory-path=dirs] [--display-unused-functions]
	[--file-format=name] [--file-info] [--help] [--line] [--min-count=n]
	[--no-static] [--print-path] [--separate-files]
	[--static-call-graph] [--sum] [--table-length=len] [--traditional]
	[--version] [--width=n] [--ignore-non-functions]
	[--demangle[=STYLE]] [--no-demangle] [--external-symbol-table=name] [@FILE]
	[image-file] [profile-file...]
 [cg_tally] arc from %s to %s traversed %lu times
 [find_call] %s: 0x%lx to 0x%lx
 [find_call] 0x%lx: bsr [find_call] 0x%lx: jal [find_call] 0x%lx: jalr
 [find_call] 0x%lx: jsr%s <indirect_child>
 called calls children cumulative descendants index index %% time    self  children    called     name
 name parents self self   time time is in ticks, not seconds
 total total  Project-Id-Version: binutils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2014-05-06 11:00+0000
Last-Translator: Susana Pereira <susana.pereira@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:03+0000
X-Generator: Launchpad (build 18115)
 			Gráfico de chamadas

 		     Gráfico de chamadas (segue-se a explicação)

 	%d registo da contagem de blocos básicos
 	%d basic-block count records
 	%d registo do gráfico de chamadas
 	%d registos do gráfico de chamadas
 	%d registo de histograma
 	%d registos de histograma
 


perfil plano:
 

Linhas %d Topo:

     Contagem de Linhas

 
%9lu Número total de execuções da linha
 
Cada amostra conta como %g %s.
 
Sumário de Execução:

 
granularidade: cada elemento da amostra cobre %ld byte(s)  <ciclo %d como um todo> [%d]
  <ciclo %d>  para %.2f%% de %.2f %s

  para %.2f%% de %.2f segundos

  não há tempo acumulado

  nenhum tempo propagado

 %6.6s %5.5s %7.7s %11.11s %7.7s %7.7s <espontâneos>
 %6.6s %5.5s %7.7s %11.11s %7.7s %7.7s <espontâneos>
 %9.2f Média de execuções por linha
 %9.2f Percentagem de ficheiros executados
 %9ld Linhas executáveis neste ficheiro
 %9ld Linhas executadas
 %c%c/chamada %s: %s: encontrou um etiqueta errada %d (ficheiro corrompido?)
 %s: %s: não está em formato executável
 %s: %s: fim de ficheiro inesperado após ler %u de %u amostras
 %s: %s: fim inesperado de ficheiro
 %s: -c não suportado nesta arquitectura %s
 %s: Só é possível especificar um de --function-ordering e --file-ordering.
 %s: o tamanho do endereço contém um valor inesperado de %u
 %s: impossivel de executar -c
 %s: não foi encontrada a secção .text em %s
 %s: não foi possível encontrar `%s'
 %s: não foi possível abrir %s.
 %s: não há suporte para depuração; -d será ignorado
 %s: escalas difrentes no registo do histograma %s: unidade de abreviação mudada entre os registo do histograma
%s: de '%c'
%s: para '%c'
 %s: unidade de dimensão mudada entre os registo do histograma
%s: de '%s'
%s: para '%s'
 %s: não sabe como lidar com o ficheiro de formato %d
 %s: o ficheiro '%s' não parece estar no formato gmon.out
 %s: ficheiro`%s' tem um cookie mágico mau
 %s: ficheiros `%s' não contêm símbolos
 %s: o ficheiro `%s' tem versão %d que não é suportada
 %s: ficheiro demasiado pequeno para ser um ficheiro gmon
 %s: símbolo encontrado que cobre vários registos de histograma %s: o ficheiro gmon.out não possui os dados do gráfico de chamadas
 %s: falta um histograma no ficheiro gmon.out
 %s: incompatível com o primeiro ficheiro gmon
 %s: sobreposição do registo do histograma
 %s: rácio de perfis incompatíveis com o primeiro ficheiro gmon
 %s: sem espaço para %lu bytes de espaço de texto
 %s: alguém contou mal: ltab.len=%d em vez de %ld
 %s: desculpe, o formato de ficheiro `prof'  ainda não é suportado
 %s: impossível analisar o ficheiro de mapeamento %s.
 %s: fim de ficheiro inesperado depois de ler %d/%d binários
 %s: estilo desembaralhamento desconhecido `%s'
 %s: formato de ficheiro %s desconhecido
 %s: aviso: a ignorar contagem de execução do basic-block (use -l ou --line)
 %s:%d: (%s:0x%lx) %lu execuções
 %tempo *** Ficheiro %s:
 <ciclo %d> <filho indirecto> <deconhecido> Baseado no gprof BSD, copyright 1983 Regents of the University of California.
 O ficheiro `%s' (versão %d) contém:
 Perfil plano:
 GNU gprof %s
 Indexar por nome de função

 Reportar erros a %s
 Este programa é software livre. Este programa não tem qualquer garantia.
 Utilização: %s [-[abcDhilLsTvwxyz]] [-[ACeEfFJnNOpPqSQZ][name]] [-I dirs]
	[-d[num]] [-k from/to] [-m min-count] [-t table-length]
	[--[no-]annotated-source[=name]] [--[no-]exec-counts[=name]]
	[--[no-]flat-profile[=name]] [--[no-]graph[=name]]
	[--[no-]time=name] [--all-lines] [--brief] [--debug[=level]]
	[--function-ordering] [--file-ordering] [--inline-file-names]
	[--directory-path=dirs] [--display-unused-functions]
	[--file-format=name] [--file-info] [--help] [--line] [--min-count=n]
	[--no-static] [--print-path] [--separate-files]
	[--static-call-graph] [--sum] [--table-length=len] [--traditional]
	[--version] [--width=n] [--ignore-non-functions]
	[--demangle[=STYLE]] [--no-demangle] [--external-symbol-table=name] [@FILE]
	[image-file] [profile-file...]
 [cg_tally] arco de %s para %s percorrido %lu vezes
 [find_call] %s: 0x%lx até 0x%lx
 [find_call] 0x%lx: bsr [find_call] 0x%lx: jal [find_call] 0x%lx: jalr
 [find_call] 0x%lx: jsr%s <filho_indireto>
 chamou chamadas filhos acumulativo descendentes índice index %% time self children called name
 nome pais próprio sí próprio   tempo tempo está em ticks, não em segundos
 total total  