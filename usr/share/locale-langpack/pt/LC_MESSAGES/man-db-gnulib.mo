��    7      �  I   �      �     �     �  .   �  .   �  %   #     I     a  ,   }  ,   �  ,   �  '     -   ,      Z  (   {  (   �     �     �  "     4   0  3   e     �     �     �     �       $        C     U  s   p     �     �     �     	  #   "	     F	     a	     u	     z	     �	  6   �	     �	     �	     �	     
     
     $
  -   +
     Y
     t
  $   �
     �
     �
     �
  *   �
  �       �     �  0   �  .   ,  +   [     �     �  -   �  -   �  -     '   K  .   s      �  (   �  &   �  $     $   8  .   ]  ;   �  2   �     �        &   6      ]     ~  '   �     �     �  �   �     �     �     �  "   �  %   �          %     <     A     M  =   b     �     �     �     �            5   *  &   `     �  -   �     �     �     �  4                  "   /       2                                  &   6           -   1          $          )   	              %      #      
   3      '         ,   (                  0      +         !          5   .   7      4          *                                or:   [OPTION...] %.*s: ARGP_HELP_FMT parameter must be positive %.*s: ARGP_HELP_FMT parameter requires a value %.*s: Unknown ARGP_HELP_FMT parameter %s: Too many arguments
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 (PROGRAM ERROR) No version known!? (PROGRAM ERROR) Option should have been recognized!? ARGP_HELP_FMT: %s value is less than or equal to %s Garbage in ARGP_HELP_FMT: %s Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid preceding regular expression Invalid range end Invalid regular expression Mandatory or optional arguments to long options are also mandatory or optional for any corresponding short options. Memory exhausted NAME No match No previous regular expression Premature end of regular expression Regular expression too big Report bugs to %s.
 SECS Success Trailing backslash Try '%s --help' or '%s --usage' for more information.
 Unknown system error Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ Usage: failed to return to initial working directory give a short usage message give this help list hang for SECS seconds (default 3600) memory exhausted print program version set the program name unable to record current working directory Project-Id-Version: gnulib 3.0.0.6062.a6b16
Report-Msgid-Bugs-To: bug-gnulib@gnu.org
POT-Creation-Date: 2015-11-06 15:42+0000
PO-Revision-Date: 2015-12-03 18:32+0000
Last-Translator: Luís Oliveira <Unknown>
Language-Team: Portuguese <translation-team-pt@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:11+0000
X-Generator: Launchpad (build 18115)
Language: pt
   ou:   [OPÇÃO...] %.*s: ARGP_HELP_FMT parâmetro deve ser positivo %.*s: ARGP_HELP_FMT parâmetro requer um valor %.*s: ARGP_HELP_FMT parâmetro desconhecido %s: Demasiados argumentos
 %s: opção inválida -- '%c'
 %s: opção '%c%s' não permite um argumento
 %s: opção '%s' é ambígua; possibilidades: %s: opção '--%s' não permite um argumento
 %s: opção '--%s' requer um argumento
 %s: opção '-W %s' não permite um argumento
 %s: opção '-W %s' é ambígua
 %s: opção '-W %s' requer um argumento
 %s: opção requer um argumento -- %c
 %s: opção não reconhecida '%c%s'
 %s: opção não reconhecida '--%s'
 (ERRO DO PROGRAMA) Nenhuma versão conhecida!? (ERRO DO PROGRAMA) A opção deveria ter sido reconhecida!? ARGP_HELP_FMT: valor %s é menos que ou igual a %s Lixo em ARGP_HELP_FMT: %s Referência de retorno inválida Nome de classe de caracteres inválido Caractere de colação inválido Conteúdo de \{\} inválido Expressão regular precedente inválida Final de alcance inválido Expressão regular inválida Os argumentos obrigatórios ou opcionais para as opções longas são também obrigatórios ou opcionais para qualquer opção curta correspondente. Memória esgotada NOME Nenhuma equivalência Nenhuma expressão regular prévia Final prematuro da expressão regular Expressão regular muito grande Reporte bugs para %s.
 SEGS Com sucesso Backslash de arrasto Tente '% s - help' ou '% s - usage' para mais informações.
 Erro de sistema desconhecido ( ou \( não equivalente ) ou \) não emparelhado [ ou [^ não equivalente \{ não equivalente Utilização: falha ao regressar ao directório de trabalho inicial dá uma mensagem curta de utilização dá esta lista de ajuda pára por SEGS segundos (predefinição 3600) memória esgotada escreve a versão do programa define o nome do programa incapaz de registar o directório de trabalho actual 