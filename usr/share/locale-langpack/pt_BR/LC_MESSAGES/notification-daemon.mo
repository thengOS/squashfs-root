��          |      �             !     9     R     h  (   ~     �     �     �     �       '     +  D     p     �     �      �  ,   �  (   
     3      K  !   l     �  1   �                      	                          
           Clear all notifications Closes the notification. Display notifications Enable debugging code Exceeded maximum number of notifications Invalid notification identifier Notification Daemon Notification body text. Notification summary text. Notifications Replace a currently running application Project-Id-Version: notification-daemon master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=notification-daemon&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-19 01:30+0200
PO-Revision-Date: 2016-02-19 09:14+0000
Last-Translator: Rafael Fontenelle <Unknown>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:46+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 Limpar todas as notificações Fecha a notificação. Exibir notificações Habilitar código de depuração Excedido o número máximo de notificações Identificador da notificação inválido Daemon de Notificação Texto do corpo da notificação. Texto do resumo da notificação. Notificações Substituir um aplicativo atualmente em execução 