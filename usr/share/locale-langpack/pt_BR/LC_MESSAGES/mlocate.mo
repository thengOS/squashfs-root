��    4      �  G   \      x  4   y  F   �  #   �          2     G  1   \  �   �     @     N     e       *   �  �  �  �  �  +        �  #   �  0   �            =  )   ^  '   �     �     �  %   �            &   *  .   Q     �     �     �  )   �  !   �  8     $   J     o     �  '   �     �  "   �  .        0  $   L  "   q     �  "   �  !   �     �         $  B   (  c   k  $   �     �          ,  ?   I  �   �     @     T     l     �  )   �  p  �  \  B   1   �$  '   �$  7   �$  B   1%  (   t%  1   �%  <   �%  9   &  '   F&  )   n&  ;   �&      �&     �&  9   '  =   M'  +   �'      �'     �'  8   �'  %   )(  R   O(  *   �(  %   �(     �(  +   )     >)  '   O)  4   w)  %   �)  2   �)  '   *     -*  (   I*  !   r*     �*  !   �*     	   2   "       +       #            %                1                    ,                 *   )   (       3             
   '                        4   /      !                         -       &   .                 0                            $    	%'ju byte in file names
 	%'ju bytes in file names
 	%'ju byte used to store database
 	%'ju bytes used to store database
 	%'ju directory
 	%'ju directories
 	%'ju file
 	%'ju files
 
Report bugs to %s.
 --%s specified twice --%s would override earlier command-line argument Copyright (C) 2007 Red Hat, Inc. All rights reserved.
This software is distributed under the GPL v.2.

This program is provided with NO WARRANTY, to the extent permitted by law. Database %s:
 I/O error reading `%s' I/O error seeking in `%s' I/O error while writing to `%s' I/O error while writing to standard output Usage: locate [OPTION]... [PATTERN]...
Search for entries in a mlocate database.

  -A, --all              only print entries that match all patterns
  -b, --basename         match only the base name of path names
  -c, --count            only print number of found entries
  -d, --database DBPATH  use DBPATH instead of default database (which is
                         %s)
  -e, --existing         only print entries for currently existing files
  -L, --follow           follow trailing symbolic links when checking file
                         existence (default)
  -h, --help             print this help
  -i, --ignore-case      ignore case distinctions when matching patterns
  -l, --limit, -n LIMIT  limit output (or counting) to LIMIT entries
  -m, --mmap             ignored, for backward compatibility
  -P, --nofollow, -H     don't follow trailing symbolic links when checking file
                         existence
  -0, --null             separate entries with NUL on output
  -S, --statistics       don't search for entries, print statistics about each
                         used database
  -q, --quiet            report no error messages about reading databases
  -r, --regexp REGEXP    search for basic regexp REGEXP instead of patterns
      --regex            patterns are extended regexps
  -s, --stdio            ignored, for backward compatibility
  -V, --version          print version information
  -w, --wholename        match whole path name (default)
 Usage: updatedb [OPTION]...
Update a mlocate database.

  -f, --add-prunefs FS           omit also FS
  -n, --add-prunenames NAMES     omit also NAMES
  -e, --add-prunepaths PATHS     omit also PATHS
  -U, --database-root PATH       the subtree to store in database (default "/")
  -h, --help                     print this help
  -o, --output FILE              database to update (default
                                 `%s')
      --prune-bind-mounts FLAG   omit bind mounts (default "no")
      --prunefs FS               filesystems to omit from database
      --prunenames NAMES         directory names to omit from database
      --prunepaths PATHS         paths to omit from database
  -l, --require-visibility FLAG  check visibility before reporting files
                                 (default "yes")
  -v, --verbose                  print paths of files as they are found
  -V, --version                  print version information

The configuration defaults to values read from
`%s'.
 `%s' does not seem to be a mlocate database `%s' has unknown version %u `%s' has unknown visibility flag %u `%s' is locked (probably by an earlier updatedb) `=' expected after variable name can not change directory to `%s' can not change group of file `%s' to `%s' can not change permissions of file `%s' can not drop privileges can not find group `%s' can not get current working directory can not lock `%s' can not open `%s' can not open a temporary file for `%s' can not read two databases from standard input can not stat () `%s' configuration is too large error replacing `%s' file name length %zu in `%s' is too large file name length %zu is too large file system error: zero-length file name in directory %s invalid empty directory name in `%s' invalid regexp `%s': %s invalid value `%s' of --%s invalid value `%s' of PRUNE_BIND_MOUNTS missing closing `"' no pattern to search for specified non-option arguments are not allowed with --%s unexpected EOF reading `%s' unexpected data after variable value unexpected operand on command line unknown variable `%s' value in quotes expected after `=' variable `%s' was already defined variable name expected warning: Line number overflow Project-Id-Version: mlocate
Report-Msgid-Bugs-To: https://fedorahosted.org/mlocate/
POT-Creation-Date: 2012-09-22 04:14+0200
PO-Revision-Date: 2013-09-12 05:04+0000
Last-Translator: Jefferson H. Xavier <Unknown>
Language-Team: Portuguese (Brazil) <trans-pt_br@lists.fedoraproject.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 18:27+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 	%'ju byte em nomes de arquivos
 	%'ju bytes em nomes de arquivos
 	%'ju byte usado para armazenar a base de dados
 	%'ju bytes usados para armazenar a base de dados
 	%'ju diretório
 	%'ju diretórios
 	%'ju arquivo
 	%'ju arquivos
 
Relatar erros para %s.
 --%s especificado duas vezes --%s pode sobrescrever o argumento de linha de comando anterior Copyright (C) 2007 Red Hat, Inc. Direitos Reservados.
Este software é distribuído sob a licença GPL v.2.

Este programa é fornecido sem garantias na medida do permitido pela lei. Banco de dados %s:
 Erro de E/S ao ler `%s' Erro de E/S ao procurar `%s' Erro de E/S ao gravar em `%s' erro de E/S ao escrever na saída padrão Utilização: locate [OPÇÃO]... [CRITÉRIOS]...
Procura por entradas em uma base de dados mlocate.

  -A, --all              apenas exibe entradas que atendem todos os critérios
  -b, --basename         confronta apenas os nomes base do caminho
  -c, --count            apenas exibe o número de entradas encontradas
  -d, --database DBPATH  utiliza DBPATH ao invés da base dados padrão (que é
                         %s)
  -e, --existing         apenas exibe as entradas para arquivos que atualmente existe
  -L, --follow           segue rastros de links simbólicos ao verificar a existência do
                         arquivo (padrão)
  -h, --help             exibe esta ajuda
  -i, --ignore-case      ignora distinções de caso quando atender os critérios
  -l, --limit, -n LIMIT  limita a saída (ou conta) para LIMITAR entredas
  -m, --mmap             ignorado, para compatibilidade anterior
  -P, --nofollow, -H     não seguir rastros de links simbólicos ao verificar a existência de
                         arquivos
  -0, --null             separar entradas com NUL na saída
  -S, --statistics       não procurar por entradas, exibe estatística sobre cada
                         base de dados usada
  -q, --quiet            não reportar mensagens de erro sobre erros de leitura de base de dados
  -r, --regexp REGEXP    procurar por regexp REGEXP ao invés de critérios
      --regex            critérios são regexps extendidos
  -s, --stdio            ignorado, para compatibilidade anterior
  -V, --version          exibe informação de versão
  -w, --wholename        compara todo o nome do caminho (padrão)
 Uso: updatedb [OPÇÕES]...
Atualiza uma base de dados do mlocate.

  -f, --add-prunefs FS           omite algum FS
  -n, --add-prunenames NAMES     omite alguns NAMES
  -e, --add-prunepaths PATHS     omite alguns PATHS
  -U, --database-root PATH       O subdiretório a armazenar na base de dados(padrão "/")
  -h, --help                     mostra essa ajuda
  -o, --output ARQUIVO              base de dados a atualizar (padrão
                                 `%s')
      --prune-bind-mounts FLAG   omite vínculos montados (padrão "no")
      --prunefs FS               sistemas de arquivos a serem omitidos da base de dados
      --prunenames NAMES         nome dos diretórios a omitir da base de dados
      --prunepaths PATHS         caminhos a omitir da base de dados
  -l, --require-visibility FLAG  verificar visibilidade antes de relatar arquivos
                                 (padrão "yes")
  -v, --verbose                  mostra o caminho dos arquivos quando localiza-los
  -V, --version                  mostra informações da versão

A configuração padrão para valores de leitura
"%s".
 `%s' não parece ser um banco de dados do mlocate `%s' tem uma versão desconhecida de %u `%s' tem um sinalizador de visibilidade %u desconhecido `%s' está bloqueado (provavelmente devido a um updatedb anterior) `=' esperado depois do nome da variável não foi possível alterar o diretório para `%s' não foi possível alterar o grupo do arquivo `%s' para `%s' não foi possível alterar as permissões do arquivo `%s' não foi possível retirar privilégios não foi possível localizar o grupo `%s' Não foi possível encontrar o atual diretório de trabalho Não foi possível bloquear `%s' não foi possível abrir `%s' não foi possível abrir um arquivo temporário para `%s' Não foi possível ler duas bases de dados da entrada padrão Não foi possível realizar stat () de `%s' a configuração é muito grande erro ao substituir `%s' o tamanho do nome do arquivo %zu em `%s' é muito grande o nome do arquivo %zu é muito grande erro do sistema de arquivos: arquivo com nome de comprimento zero no diretório %s nome de diretório vazio inválido em `%s' expressão regular `%s' inválida: %s valor `%s' inválido para --%s valor `%s' inválido para PRUNE_BIND_MOUNTS falta fechar `"' nenhum padrão de pesquisa especificado argumentos sem opção não são permitidos com --%s Fim do arquivo inesperado ao ler `%s' informação inesperada após o valor da variável operador inesperado na linha de comando variável desconhecida `%s' valor entre aspas esperado depois do `=' a variável `%s' já foi definida nome de variável esperado Aviso: Número de linhas excedido 