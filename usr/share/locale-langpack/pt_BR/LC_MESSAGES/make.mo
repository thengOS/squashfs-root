��         4  [  L      �     �  &   �     �     	  !        4      F     g  '   }  #   �     �     �     �  "        /      K  t   l  L   �  G   .  P   v  M   �  @     o   V  o   �  P   6  F   �  J   �  �     9   �  C   �  f     _     ;   �  :     O   V  J   �  }   �  u   o  ~   �  >   d  M   �  C   �  2   5   F   h   I   �   ;   �   	   5!  "   ?!     b!     o!     ~!     �!     �!     �!  	   �!     �!     �!     �!  %   �!     %"  
   ."  7   9"     q"     �"  /   �"     �"     �"     �"     #  (   1#     Z#  &   o#  *   �#     �#  #   �#  #   �#  )   $  -   H$  ,   v$     �$     �$  $   �$  9   �$     0%  3   O%     �%  .   �%     �%     �%  !   �%  =   &  "   X&     {&     �&     �&  "   �&  ;   �&  !    '     "'  +   >'     j'     �'  &   �'  .   �'     �'     (  �   (  &   �(  %   )     4)  #   @)  >   d)  	   �)  +   �)     �)  1   �)  2   *     F*     R*     k*  "   �*  *   �*     �*     �*     �*     �*     +  	   '+     1+     I+     i+     v+  %   �+     �+  $   �+     �+  	   ,     ,     +,  +   E,     q,  !   �,     �,     �,     �,     �,     �,     -     -     .-  )   G-     q-     �-     �-     �-     �-     �-     �-  	   �-     �-     
.     .     /.     E.  -   Q.  -   .     �.  
   �.  *   �.  "   �.  	   	/  6   /     J/     X/  -   p/     �/     �/     �/  "   �/  #   �/     0     *0     D0     ]0  "   i0  '   �0     �0  "   �0  #   �0     1     )1     21     :1     M1     U1     f1     z1  +   �1  
   �1     �1     �1     �1     2     &2     >2  !   S2     u2     �2     �2     �2     �2     �2  	   �2  9   �2     23  3   N3     �3     �3     �3     �3     �3  !   �3     �3     4     4  8   ,4  0   e4     �4     �4     �4     �4  )   �4  ;   !5  3   ]5     �5     �5     �5     �5  '   �5     6     -6  0   06  1   a6  *   �6  0   �6     �6  $   �6  .   #7     R7  ,   o7     �7     �7     �7  =   �7  9   #8  1   ]8  /   �8     �8  �  �8     �:  5   �:      ;     0;  +   <;     h;  )   ;      �;  9   �;  0   <     5<     G<     V<  )   u<  %   �<  *   �<  ~   �<  W   o=  V   �=  b   >  X   �>  J   �>  r   %?  {   �?  [   @  T   p@  S   �@  �   A  =   �A  I   �A  j   2B  |   �B  <   C  <   WC  ^   �C  [   �C  �   OD  y   �D  �   ZE  H   �E  g   5F  @   �F  5   �F  Y   G  J   nG  :   �G  	   �G  "   �G     !H     .H     EH     YH     wH     �H  	   �H     �H     �H     �H  &   �H     I  
   I  D   %I     jI     yI  >   �I      �I     �I     J      *J  0   KJ      |J  *   �J  /   �J     �J  3   K  -   HK  &   vK  1   �K  -   �K     �K     L  )   -L  @   WL  #   �L  :   �L     �L  2   M  #   FM     jM  )   }M  ?   �M  $   �M     N     N     N  +   8N  J   dN  )   �N  -   �N  ,   O     4O  !   SO  (   uO  2   �O     �O     �O  �    P  /   �P  -   Q     @Q  +   MQ  5   yQ  	   �Q  *   �Q     �Q  >   �Q  >   ,R     kR     wR     �R  (   �R  )   �R     S     S     %S  $   BS     gS     �S     �S  /   �S     �S     �S  *   �S  +    T  *   LT     wT     �T  "   �T  #   �T  1   �T     U  '   4U  
   \U  $   gU     �U  	   �U     �U     �U     �U     �U  (   
V     3V     PV     \V  	   jV     tV     �V     �V     �V     �V  
   �V     �V     W     'W  4   6W  0   kW     �W     �W  5   �W  4   �W  
   X  =    X     ^X     yX  7   �X     �X     �X     �X  *   Y  *   0Y     [Y     wY     �Y     �Y  4   �Y  2   �Y  #   %Z  .   IZ  +   xZ     �Z     �Z     �Z     �Z     �Z     �Z     �Z     [  :   [  	   U[     _[  $   v[     �[  #   �[  &   �[     �[     \     4\     G\     f\     �\     �\     �\     �\  F   �\  #   ]  @   C]     �]      �]     �]  "   �]     �]  ?   �]     /^     G^     P^  @   `^  2   �^     �^     �^  *   _  !   8_  (   Z_  H   �_  <   �_     	`  "   `     5`  *   R`  7   }`     �`     �`  @   �`  A   a  6   ]a  5   �a     �a  (   �a  0   b     >b  %   Zb     �b  %   �b  '   �b  G   �b  ?   )c  ;   ic  /   �c     �c     �   �       E       �   @       m   �       �   �   q   �           s   �   �   �           c   U           �   ,      $   �       �   <       �       �   R          �      b   ?   �       J   :               �   V      �   z                   �   �   a   �   �   �       �   -   `       k   �       �   _   O   \   B   ;   �      �               �   7   v       �   �   �   �   t                   �      N   �   h   �       �   �   �   C   �   �   %   �   H                  �               �   �       �       �   �   #   4   I   W   {      (   �      '               G   �          �   �       �   �       [   M   Q   �   &   �   ^       6           =   �   �      ~   �       )      X       �   �      Z      3   �   �   x       �   r   �   f   D   �           �       8       �       �   �           �   �   9   	      �       �   i   �   �      �   l   �   �   �        �   �   �   *   �   �   �   >       n   |   �          �   u   e       j   �       �   �   T   5   "   �   .   �                  y   S       w   �       g      �       �   �   }      �      A   2   �       �       L   �   K   �   !   �   �   /   �   �          �           �         1   Y   �   �                 �   �   �   
   �   +   �                   �   �       �   F   �   �       P   o   �   ]          �       0          �   d   �   �              �   �   p    
# %u implicit rules, %u 
# %u pattern-specific variable values 
# Directories
 
# Files 
# Finished Make data base on %s
 
# Implicit Rules 
# Make data base, printed on %s 
# No implicit rules. 
# No pattern-specific variable values. 
# Pattern-specific Variable Values 
# VPATH Search Paths
 
# Variables
 
# files hash-table stats:
#  
Counted %d args in failed launch
 
This program built for %s
 
This program built for %s (%s)
 
Unhandled exception filter called from program %s
ExceptionCode = %lx
ExceptionFlags = %lx
ExceptionAddress = 0x%p
   --debug[=FLAGS]             Print various types of debugging information.
   --eval=STRING               Evaluate STRING as a makefile statement.
   --no-print-directory        Turn off -w, even if it was turned on implicitly.
   --warn-undefined-variables  Warn when an undefined variable is referenced.
   -B, --always-make           Unconditionally make all targets.
   -C DIRECTORY, --directory=DIRECTORY
                              Change to DIRECTORY before doing anything.
   -I DIRECTORY, --include-dir=DIRECTORY
                              Search DIRECTORY for included makefiles.
   -L, --check-symlink-times   Use the latest mtime between symlinks and target.
   -R, --no-builtin-variables  Disable the built-in variable settings.
   -S, --no-keep-going, --stop
                              Turns off -k.
   -W FILE, --what-if=FILE, --new-file=FILE, --assume-new=FILE
                              Consider FILE to be infinitely new.
   -b, -m                      Ignored for compatibility.
   -d                          Print lots of debugging information.
   -e, --environment-overrides
                              Environment variables override makefiles.
   -f FILE, --file=FILE, --makefile=FILE
                              Read FILE as a makefile.
   -h, --help                  Print this message and exit.
   -i, --ignore-errors         Ignore errors from recipes.
   -j [N], --jobs[=N]          Allow N jobs at once; infinite jobs with no arg.
   -k, --keep-going            Keep going when some targets can't be made.
   -l [N], --load-average[=N], --max-load[=N]
                              Don't start multiple jobs unless load is below N.
   -n, --just-print, --dry-run, --recon
                              Don't actually run any recipe; just print them.
   -o FILE, --old-file=FILE, --assume-old=FILE
                              Consider FILE to be very old and don't remake it.
   -p, --print-data-base       Print make's internal database.
   -q, --question              Run no recipe; exit status says if up to date.
   -r, --no-builtin-rules      Disable the built-in implicit rules.
   -s, --silent, --quiet       Don't echo recipes.
   -t, --touch                 Touch targets instead of remaking them.
   -v, --version               Print the version number of make and exit.
   -w, --print-directory       Print the current directory.
   Date %s   uid = %d, gid = %d, mode = 0%o.
  (built-in):  (core dumped)  (don't care)  (name might be truncated)  (no default goal)  (no ~ expansion)  (remote)  (search path)  files,   impossibilities  impossibilities in %lu directories.
  so far.  terminal. #  A default, MAKEFILES, or -include/sinclude makefile. #  Also makes: #  Command line target. #  Dependencies recipe running (THIS IS A BUG). #  Failed to be updated. #  File does not exist. #  File has been updated. #  File has not been updated. #  File is an intermediate prerequisite. #  File is very old. #  Implicit rule search has been done. #  Implicit rule search has not been done. #  Last modified %s
 #  Modification time never checked. #  Needs to be updated (-q is set). #  Phony target (prerequisite of .PHONY). #  Precious file (prerequisite of .PRECIOUS). #  Recipe currently running (THIS IS A BUG). #  Successfully updated. #  recipe to execute # %s (device %d, inode [%d,%d,%d]):  # %s (device %d, inode [%d,%d,%d]): could not be opened.
 # %s (device %ld, inode %ld):  # %s (device %ld, inode %ld): could not be opened.
 # %s (key %s, mtime %d):  # %s (key %s, mtime %d): could not be opened.
 # %s: could not be stat'd.
 # Not a target: # variable set hash-table stats:
 %s (line %d) Bad shell context (!unixy && !batch_mode_shell)
 %s is suspending for 30 seconds... %s%s: %s %s: %s %s: Command not found %s: Entering an unknown directory
 %s: Interrupt/Exception caught (code = 0x%lx, addr = 0x%p)
 %s: Leaving an unknown directory
 %s: Shell program not found %s: Timestamp out of range; substituting %s %s: illegal option -- %c
 %s: invalid option -- %c
 %s: option requires an argument -- %c
 %s: user %lu (real %lu), group %lu (real %lu)
 %sBuilt for %s
 %sBuilt for %s (%s)
 %sLicense GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
%sThis is free software: you are free to change and redistribute it.
%sThere is NO WARRANTY, to the extent permitted by law.
 %s[%u]: Entering an unknown directory
 %s[%u]: Leaving an unknown directory
 *** Break.
 *** Waiting for unfinished jobs.... -warning, you may have to re-enable CTRL-Y handling from DCL.
 .  Stop.
 .DEFAULT_GOAL contains more than one target Aborted Access violation: read operation at address 0x%p
 Access violation: write operation at address 0x%p
 Alarm clock Append %.*s and cleanup
 Append output to %s
 Avoiding implicit rule recursion.
 BUG: num_pattern_rules is wrong!  %u != %u BUILTIN CD %s
 BUILTIN [%s][%s]
 Bad system call Batch file contents:%s
	%s
 Broken pipe Bus error CPU time limit exceeded Cannot create a temporary file
 Child access Child exited Circular %s <- %s dependency dropped. Cleaning up temp batch file %s
 Cleaning up temporary batch file %s
 Collisions=%ld/%ld=%.0f%% Continued Could not restore stdin
 Could not restore stdout
 Couldn't change back to original directory. CreatePipe() failed (e=%ld)
 Creating temporary batch file %s
 Current time Customs won't export: %s
 Danger signal EMT trap Error spawning, %d
 Error, empty command
 Executing %s instead
 File size limit exceeded Floating point co-processor not available Floating point exception Hangup I/O possible IOT trap Illegal Instruction Information request Initialized access Interrupt Jobserver client (fds %d,%d)
 Killed Live child %p (%s) PID %s %s
 Load=%ld/%ld=%.0f%%,  Make access Makefile from standard input specified twice. Malformed target-specific variable definition No No targets No targets specified and no makefile found Obtained token for child %p (%s).
 Options:
 Parallel jobs (-j) are not supported on this platform. Power failure Profiling timer expired Putting child %p (%s) PID %s%s on the chain.
 Quit Re-executing[%u]: Reading makefiles...
 Reaping losing child %p PID %s %s
 Reaping winning child %p PID %s %s
 Redirected error to %s
 Redirected input from %s
 Redirected output to %s
 Rehash=%d,  Released token for child %p (%s).
 Removing child %p PID %s%s from chain.
 Removing intermediate files...
 Report bugs to <bug-make@gnu.org>
 Resetting to single job (-j1) mode. Resource lost SIGPHONE SIGWIND Segmentation fault Stopped Stopped (signal) Stopped (tty input) Stopped (tty output) Symbolic links not supported: disabling -L. Terminated Trace/breakpoint trap Unknown builtin command '%s'
 Unknown error %d Updating goal targets....
 Updating makefiles....
 Urgent I/O condition Usage: %s [options] [target] ...
 User access User defined signal 1 User defined signal 2 Virtual timer expired Warning: Empty redirection
 Window changed automatic can't allocate %lu bytes for hash table: memory exhausted cannot enforce load limit:  cannot enforce load limits on this operating system command line creating jobs pipe default done sleep(30). Continuing.
 dup jobserver empty string invalid as file name empty variable name environment environment under -e find_and_set_shell() path search set default_shell = %s
 find_and_set_shell() setting default_shell = %s
 fopen (temporary file) fwrite (temporary file) init jobserver pipe invalid syntax in conditional lbr$ini_control() failed with status = %d lbr$set_module() failed to extract module info, status = %d make reaped child pid %s, still waiting for pid %s
 makefile missing rule before recipe missing target pattern mixed implicit and normal rules mixed implicit and static pattern rules multiple target patterns no no more file handles: could not duplicate stdin
 no more file handles: could not duplicate stdout
 prerequisites cannot be defined in recipes process_easy() failed to launch process (e=%ld)
 read jobs pipe recipe commences before first target spawnvpe: environment space might be exhausted sys$search() failed with %d
 touch archive member is not available on VMS unknown signal unlink (temporary file):  unterminated variable reference warning:  Clock skew detected.  Your build may be incomplete. warning: -jN forced in submake: disabling jobserver mode. warning: NUL character seen; rest of line ignored windows32_openpipe(): process_init_fd() failed
 write jobserver Project-Id-Version: GNU make 3.82
Report-Msgid-Bugs-To: bug-make@gnu.org
POT-Creation-Date: 2014-10-05 12:25-0400
PO-Revision-Date: 2016-02-15 18:16+0000
Last-Translator: Fábio Henrique F. Silva <Unknown>
Language-Team: Brazilian Portuguese <ldpbr-translation@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:58+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
X-Poedit-SourceCharset: iso-8859-1
 
# %u regras implícitas, %u 
# %u valores para variável de padrões específicos 
# Diretórios
 
# Arquivos 
# Banco de dados do Make finalizado em %s
 
# Regras implícitas. 
# Banco de dados do Make, impresso em %s 
# Faltam as regras implícitas. 
# Faltam valores para variável de padrões específicos 
# Valores da Variável de padrões específicos 
# Caminho VPATH
 
# Variáveis
 
# tabela hash de arquivos:
#  
Contados %d args na falha de execução
 
Este programa foi compilado para %s
 
Este programa foi compilado para %s (%s)
 
Não pôde tratar o filtro de exceção chamado por %s
CódigoExceção = %lx
SinalExceção = %lx
EndereçoExceção = 0x%p
   --debug[=OPÇÕES]            Imprime vários tipos de informações de depuração.
   --eval=STRING               Avalia a STRING como uma declaração para um makefile.
   --no-print-directory        Desativa a opção -w, mesmo que ela esteja implicitamente ativada.
   --warn-undefined-variables  Avisa quando um variável não definida for referenciada.
   -B, --always-make           Processa todos os alvos incondicionalmente.
   -C DIRETÓRIO, --directory=DIRETÓRIO
                              Muda para o DIRETÓRIO antes de fazer algo.
   -I DIRETÓRIO, --include-dir=DIRETÓRIO
                              Pesquisa o DIRETÒRIO por arquivos make a incluir.
   -L, --check-symlink-times  Usa o tempo mais antigo entre o vínculo simbólico e o alvo.
   -R, --no-builtin-variables  Desabilita a configuração das variáveis embutidas.
   -S, --no-keep-going, --stop
                              Desativa a opção -k.
   -W ARQUIVO, --what-if=ARQUIVO, --new-file=ARQUIVO, --assume-new=ARQUIVO
                              Considera o ARQUIVO infinitamente novo.
   -b, -m                      Ignorado para compatibilidade.
   -d                          Imprime muita informação de depuração.
   -e, --environment-overrides
                              Assume os valores das variáveis de ambiente.
   -f ARQUIVO, --file=ARQUIVO, --makefile=ARQUIVO
                              Lê o ARQUIVO como se fosse um arquivo make.
   -h, --help                   Imprime esta mensagem e sai.
   -i, --ignore-errors         Ignora os erros dos comandos.
   -j [N], --jobs[=N]          Permite N tarefas de uma vez; tarefas infinitas sem argumentos.
   -k, --keep-going            Continua mesmo que alguns alvos não possam ser processados.
   -l [N], --load-average[=N], --max-load[=N]
                              Não inicia múltiplas tarefas a menos que a carga seja menor que N.
   -n, --just-print, --dry-run, --recon
                              Não executa quaisquer comandos; apenas imprime-os.
   -o ARQUIVO, --old-file=ARQUIVO, --assume-old=ARQUIVO
                              Considera o ARQUIVO como muito antigo e não reprocessá-o.
   -p, --print-data-base       Imprime o banco de dados interno do make.
   -q, --question              Não executa os comandos; O código de saida indica se está atualizado.
   -r, --no-builtin-rules      Desabilita as regras implícitas.
   -s, --silent, --quiet       Não ecoa os comandos.
   -t, --touch                 Executa um `touch' nos alvos ao invés de reprocessá-los.
   -v, --version               Imprime o número de versão do make e sai.
   -w, --print-directory       Imprime o diretório atual.
   Data %s   uid = %d, gid = %d, modo = 0%o.
  (embutido):  (arquivo core criado)  (sem importância)  (o nome pode estar truncado)  (não há objetivo padrão)  (sem expansão ~)  (remoto)  (caminho de pesquisa)  arquivos,   impossibilidades  impossibilidades em %lu diretórios.
  até agora.  terminal. # Um arquivo MAKEFILES padrão ou arquivo makefile -include/sinclude # Também faz: # Linha de Comando do Alvo. # Comandos de dependências em execução (ISTO É UMA FALHA). # Problemas com a atualização. # O Arquivo não existe. # O Arquivo foi atualizado. # O Arquivo não foi atualizado. # O arquivo é um pré-requisito intermediário. # O Arquivo está desatualizado. # Pesquisa por regra implícita concluida. # Pesquisa por regra implícita não concluida. # Última modificação %s
 # O Período da modificação nunca foi verificado. # Precisa ser atualizado (-q está definido). # Alvo Falso (prerequisito de .PHONY). # Arquivo importante (prerequisito de .PRECIOUS). # Comandos em execução (ISTO É UMA FALHA). # Atualizado com sucesso. # comandos a executar # %s (dispositivo %d, inode [%d,%d,%d]):  # %s (dispositivo %d, inode [%d,%d,%d]): não pôde ser aberto.
 # %s (dispositivo %ld, inode %ld):  # %s (dispositivo %ld, inode %ld): não pôde ser aberto.
 # %s (chave %s, mtime %d):  # %s (chave %s, mtime %d): não pôde ser aberto.
 # %s: não pôde ser estabelecido.
 # Não é um alvo: # tabela hash do conjunto de variávies:
 %s (linha %d) contexto inválido (!unixy && !batch_mode_shell)
 %s está suspenso por 30 segundos... %s%s: %s %s: %s %s: Comando não encontrado %s: Entrando em um diretório desconhecido
 %s: Interrupção/Exceção capturada (código = 0x%lx, endereço = 0x%p)
 %s: Saindo de um diretório desconhecido
 %s: Interpretador de comandos não encontrado %s: Data/Hora fora de faixa; substituindo %s %s: a opção é ilegal -- %c
 %s: a opção é inválida -- %c
 %s: a opção requer um argumento -- %c
 %s: usuário %lu (real %lu), grupo %lu (real %lu)
 %sCompilado para %s
 %sCompilado para %s (%s)
 %sLicença GPLv3+: GNU GPL versão 3 ou posterior <http://gnu.org/licenses/gpl.html>
%sIsto é um aplicativo livre: você pode alterá-lo e redistribui-lo livremente.
%sNÃO HÁ GARANTIAS, exceto o que for permitido por lei.
 %s[%u]: Entrando em um diretório desconhecido
 %s[%u]: Saindo de um diretório desconhecido
 *** Quebra.
 ** Esperando que outros processos terminem. -warning, pode ser preciso reativar o CTRL-Y no DCL.
 .  Pare.
 . DEFAULT_GOAL contém mais do que um alvo Abortado Violação de acesso: operação de leitura no endereço 0x%p
 Violação de acesso: operação de escrita no endereço 0x%p
 Despertador Acrescentado %.*s e limpo
 Saida redirecionada para %s
 Evitando recursão em regra implícita.
 ERRO: num_pattern_rules errada!  %u != %u CD EMBUTIDO %s
 EMBUTIDO [%s][%s]
 Chamada de sistema inválida Conteúdo do arquivo de lote:%s
	%s
 Canalização interrompida Erro de barramento Tempo de CPU excedido Não é possível criar um arquivo temporário
 Acesso filho O Filho saiu Dependência circular %s <- %s abandonada. Apagando o arquivo de lote temporário: %s
 Apagando o arquivo de lote temporário %s
 Colisões=%ld/%ld=%.0f%% Continuação Não é possível restaurar stdin
 Não é possível restaurar stdout
 Não foi possível voltar ao diretório original. CreatePipe() falhou (e=%ld)
 Criando arquivo de lote temporário %s
 Hora atual Customizações não exportadas: %s
 Sinal perigoso Aviso EMT Erro de execução, %d
 Erro, comando vazio
 Executando %s ao invés de
 Tamanho do arquivo excedido Co-processador aritmético indisponível Exceção de ponto flutuante Desconectar Possível E/S Aviso IOT Instrução ilegal Solicitação de informação Acesso inicializado Interrupção Cliente Jobserver (fds %d,%d)
 Finalizado Filho ativo %p (%s) PID %s %s
 Carga=%ld/%ld=%.0f%%,  Acesso do make Makefile na entrada padrão especificado duas vezes. Definição de variável para o alvo mau formada Não Sem alvo Nenhum alvo indicado e nenhum arquivo make encontrado Obtido o sinalizador para o processo filho %p (%s).
 Opções:
 Tarefas paralelas (-j) não são suportadas nesta plataforma. Falha na Energia Elétrica Temporizador de perfil expirou Colocando o processo filho %p (%s) PID %s%s na cadeia.
 Sair Re-executando[%u]: Lendo arquivos makefile ...
 Descarregando processo filho %p PID %s %s
 Descarregando processo filho %p PID %s %s
 Erro redirecionado para %s
 Entrada de %s redirecionada
 Saida redirecionada para %s
 Rehash=%d,  Liberado sinalizador para o processo filho %p (%s).
 Removendo o processo filho %p PID %s%s da cadeia.
 Apagando arquivo intermediário...
 Informe os problemas para <bug-make@gnu.org>.
 Reiniciando no modo de tarefa única (-j1). Recurso perdido SIGPHONE SIGWIND Falha de segmentação Parado Parado (sinal) Parado (entrada tty) Parado (saida tty) Vínculos simbólicos não são suportados: desabilite -L. Terminado Aviso Trace/breakpoint Comando embutido desconhecido `%s'.
 Erro desconhecido %d Atualizando os objetivos finais...
 Atualizando os arquivos makefiles ...
 Condição de E/S urgente Uso: %s [opções] [alvo] ...
 Acesso do usuário Sinal 1 definido pelo usuário Sinal 2 definido pelo usuário Temporizador virtual expirou Aviso: Redireção vazia
 Janela alterada automático não foi possível alocar %lu bytes para a tabela hash: memória cheia não pôde forçar a carga limite:  não pôde forçar os limites de carga neste sistema operacional linha de comando criando canalização de tarefas padrão sleep(30) concluido. Continuando.
 dup jobserver Cadeia de caracteres vazia não é válida como nome de arquivo nome de variável vazio ambiente ambiente sob -e find_and_set_shell(), caminho de pesquisa do default_shell = %s
 find_and_set_shell() definiu o default_shell = %s
 fopen (arquivo temporário) fwrite (arquivo temporário) inicializando a canalização do jobserver síntaxe inválida na condicional lbr$ini_control() falhou com estado = %d o lbr$set_module() falhou ao obter informações do módulo, estado = %d processo filho descarregado: pid %s, aguardando pelo pid %s
 makefile falta uma regra antes dos comandos faltando o padrão dos alvos As regras implícitas e normais misturadas As regras implícitas e de padrão estático misturadas múltiplos padrões para o alvo não sem manipuladores de arquivos: não é possível duplicar stdin
 sem manipuladores de arquivos: não é possível duplicar stdout
 os pré-requisitos não podem ser definidos no comando process_easy() falhou ao executar o processo (e=%ld)
 tarefas canalizadas lidas comandos começam antes do primeiro alvo spawnvpe: o espaço de ambiente pode estar cheio sys$search() falhou com %d
 o touch não está disponível no VMS sinal desconhecido desvinculado (arquivos temporário):  referência a variável não finalizada aviso: O relógio está errado. Sua compilação pode ficar incompleta. aviso: -jN forçado no submake: desabilitando o modo jobserver. aviso: caracter NUL detetado; o resto da linha foi ignorado windows32_openpipe(): process_init_fd() falhou
 gravar jobserver 