��          D      l       �      �   !   �   &   �   R  �   �  >       (     .   E  �  t                          Access Your Private Data Record your encryption passphrase Setup Your Encrypted Private Directory To encrypt your home directory or "Private" folder, a strong passphrase has been automatically generated. Usually your directory is unlocked with your user password, but if you ever need to manually recover this directory, you will need this passphrase. Please print or write it down and store it in a safe location. If you click "Run this action now", enter your login password at the "Passphrase" prompt and you can display your randomly generated passphrase. Otherwise, you will need to run "ecryptfs-unwrap-passphrase" from the command line to retrieve and record your generated passphrase. Project-Id-Version: ecryptfs-utils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-27 00:09+0000
PO-Revision-Date: 2012-12-30 20:29+0000
Last-Translator: gabriell nascimento <gabriellhrn@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:21+0000
X-Generator: Launchpad (build 18115)
 Acesse seus dados pessoais Gravar sua frase secreta de criptografia Configure seu diretório privado criptografado Para criptografar seu diretório pessoal ou a pasta "Private", uma frase secreta forte foi gerada automaticamente. Normalmente seu diretório é desbloqueado com sua senha de usuário, mas se você precisar recuperá-lo manualmente, vai precisar desta frase secreta. Por favor, imprima-a ou escreva-a e armazene-a em um local seguro. Se você clicar em "Executar esta ação agora", digite sua senha de login na solicitação de "Senha" e você poderá exibir sua senha gerada aleatoriamente. Caso contrário, você terá que executar "ecryptfs-unwrap-passphrase" a partir da linha de comando para recuperar e registrar sua frase secreta gerada. 