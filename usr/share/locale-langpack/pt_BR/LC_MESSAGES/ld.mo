��    �       �         (     !(     =(     V(     r(     �(     �(  P   �(  P   )  P   j)  I   �)  O   *  �   U*  N   �*  M   8+  �   �+  �   ,  @   �,  L   �,  �   $-  r   .  �   �.  �   /  �   �/  �   >0  �   �0  K   �1  O   �1  M   ,2  +   z2  K   �2  M   �2  8   @3  4   y3  B   �3  I   �3  F   ;4  �   �4  J   5  E   Y5  O   �5  K   �5  F   ;6  P   �6  &   �6  L   �6  [   G7  B   �7  =   �7  L   $8  ;   q8  C   �8  K   �8  H   =9  A   �9  K   �9  N   :  <   c:  C   �:  *   �:     ;     ;  !   0;  9   R;     �;  -   �;  P   �;  >   <     ]<      q<  !   �<     �<     �<     �<     �<  5   =  8   Q=  6   �=     �=  2   �=  #   >  6   3>  %   j>  %   �>  $   �>  )   �>  8   ?  s   >?  )   �?     �?  6   �?     +@     J@  ,   h@     �@  3   �@  !   �@  .   
A     9A  ?   YA     �A     �A     �A     �A     B     /B  $   NB  0   sB  A   �B  9   �B      C     1C  4   AC  <   vC  ,   �C  J   �C  $   +D  <   PD  6   �D  5   �D  5   �D  %   0E  7   VE  .   �E  )   �E  )   �E     F  .   +F     ZF  4   wF  .   �F  "   �F  "   �F  b   !G  *   �G     �G     �G  2   �G  0   H  !   JH  /   lH  %   �H  &   �H      �H  $   
I     /I     NI  ,   mI  "   �I  %   �I  $   �I  B   J  '   KJ  B   sJ  5   �J  3   �J  0    K     QK  /   pK     �K  )   �K      �K     L     %L  6   CL     zL     �L  :   �L     �L  -   M     5M  )   PM     zM  "   �M  '   �M  2   �M  &   N  &   =N  !   dN     �N  H   �N  .   �N  +   O  5   IO  &   O  7   �O  %   �O  6   P  C   ;P  E   P     �P     �P  3   �P     3Q     GQ     dQ     �Q  +   �Q  1   �Q  0   �Q     )R  3   6R  -   jR  $   �R     �R  1   �R  =   S  6   KS  b   �S  A   �S  �   'T  5   �T  ;   3U  D   oU  /   �U  7   �U  J   V  -   gV  (   �V  B   �V  0   W  4   2W     gW     wW     �W  /   �W  "   �W  "   �W  7   X  "   SX  /   vX     �X  <   �X  2   Y  @   6Y  G   wY  3   �Y  /   �Y     #Z  "   @Z  7   cZ  !   �Z  +   �Z  *   �Z  I   [  6   ^[  -   �[  2   �[  -   �[  /   $\     T\  3   p\  R   �\  ;   �\  8   3]  &   l]  /   �]     �]  (   �]  '   ^  %   0^  1   V^  7   �^  +   �^     �^  3   
_  #   >_     b_  "   v_     �_      �_     �_     �_  "   �_     "`     ;`     G`     O`     T`  :   X`  $   �`      �`     �`     �`  /   a  h   Ba  
   �a  /   �a  '   �a     b  *   -b     Xb     ^b     wb  .   �b  ,   �b  6   �b  (   %c     Nc  *   fc     �c  2   �c     �c  	   �c  -   d     5d  #   Ed     id     �d  )   �d     �d  2   �d  1   e  +   Me  =   ye     �e     �e  $   �e  #   f     :f  1   Qf  *   �f  )   �f  4   �f     g     #g  -   Cg  &   qg  '   �g  -   �g  	   �g     �g  %   h  &   *h  7   Qh     �h     �h     �h     �h     �h  %   �h  "   �h  )   i  
   9i     Di     ]i  ,   yi  �   �i     \j  (   dj     �j     �j     �j  +   �j  2   k  1   Kk     }k      �k     �k     �k     �k     �k     �k  &   l     -l     ?l     Dl  5   Pl  Z   �l     �l  	   �l      m     m  &   $m  %   Km     qm     vm     }m  #   �m     �m  !   �m     �m     n  '   n     ?n     Yn     wn     �n     �n     �n     �n  7   �n  4   'o  0   \o  (   �o     �o  #   �o  %   �o     p     #p     )p     .p     5p     Gp     bp  (   �p     �p     �p     �p      q     q     9q  -   Jq     xq  #   �q  (   �q     �q  &   �q     r  .   !r  .   Pr  5   r  *   �r  (   �r     	s  (   's  '   Ps     xs  (   �s     �s     �s  #   �s     �s     t     t  d   "t  �   �t     Iu     Zu     su     �u  #   �u     �u  4   �u  5   v  (   Qv     zv  !   �v      �v      �v  #   �v  0    w  0   Qw  $   �w  1   �w  '   �w  #   x     %x  '   Dx     lx     }x  	   �x  
   �x     �x     �x     �x  	   �x  	   �x     �x     �x     y  $   -y  
   Ry     ]y     by  	   qy     {y     �y     �y  	   �y  �  �y     �{      �{  "   �{  +   �{     |     <|  P   S|  [   �|  ]    }  6   ^}  I   �}  �   �}  W   j~  H   �~  �     �   �  :   O�  7   ��      �   ȁ  �   V�  �   �  �   ��  �   G�  �   ߄  E   ܅  [   "�  L   ~�  +   ˆ  M   ��  S   E�  <   ��  J   և  5   !�  H   W�  0   ��     ш  P   Q�  K   ��  `   �  Q   O�  L   ��  a   �  (   P�  H   y�  d     M   '�  9   u�  E   ��  ?   ��  5   5�  T   k�  @   ��  G   �  _   I�  H   ��  D   �  A   7�  (   y�  
   ��     ��  )   Ə  <   ��     -�  0   H�  V   y�  D   А     �  (   $�  *   M�     x�  !   ��     ��     ɑ  8   �  <   !�  4   ^�     ��  <   ��  $   �  :   �  %   J�  "   p�  $   ��  ,   ��  9   �  �   �  ,   ��      ؔ  >   ��  #   8�  "   \�  -   �  '   ��  D   Օ  !   �  E   <�  1   ��  S   ��  "   �     +�     J�     d�  &   ��  &   ��  '   ї  6   ��  D   0�  I   u�     ��     ј  =   �  Q    �  1   r�  [   ��  (    �  F   )�  <   p�  ;   ��  =   �  8   '�  F   `�  <   ��  )   �  )   �     8�  0   S�  '   ��  9   ��  :   �  &   !�  !   H�  x   j�  7   �  '   �     C�  4   c�  2   ��  !   ˞  1   �  %   �  &   E�      l�  4   ��  ,     -   �  :   �  8   X�  :   ��  4   ̠  O   �  (   Q�  C   z�  D   ��  :   �  @   >�  )   �  6   ��     �  .   ��  &   -�  "   T�  !   w�  <   ��  -   ֣  !   �  Q   &�  #   x�  B   ��     ߤ  5   ��  &   4�  #   [�  (   �  9   ��  .   �  6   �  8   H�  $   ��  S   ��  5   ��  4   0�  H   e�  *   ��  ?   ٧  0   �  C   J�  I   ��  Q   ب     *�  !   J�  1   l�  $   ��  -   é  1   �  (   #�  +   L�  1   x�  @   ��     �  2   ��  =   +�  (   i�     ��  <   ��  @   �  ?   /�  g   o�  Q   ׬  �   )�  B   �  U   P�  _   ��  D   �  ?   K�  V   ��  9   �  &   �  N   C�  6   ��  =   ɰ     �     �     ,�  5   E�  '   {�  $   ��  9   ȱ  '   �  5   *�      `�  M   ��  2   ϲ  J   �  V   M�  J   ��  >   �  7   .�  "   f�  ;   ��  #   Ŵ  *   �  .   �  Y   C�  6   ��  H   Ե  6   �  5   T�  =   ��      ȶ  =   �  f   '�  ]   ��  >   �  7   +�  G   c�  %   ��  :   Ѹ  2   �  4   ?�  >   t�  ;   ��  0   �  '    �  D   H�  %   ��     ��  8   Ǻ      �  )   �     C�     `�  #   y�     ��     ��  	   λ     ػ     ݻ  K   �  6   -�  0   d�  &   ��     ��  D   ܼ  t   !�  	   ��  A   ��  0   �  $   �  7   8�     p�     y�     ��  =   ��  '   �  5   �  -   O�  "   }�  1   ��  "   ҿ  ;   ��  "   1�  
   T�  9   _�     ��  0   ��      ��  "   ��  1   !�  '   S�  <   {�  A   ��  8   ��  G   3�  "   {�  '   ��  /   ��  *   ��     !�  G   ?�  .   ��  '   ��  ?   ��  #   �  "   B�  5   e�  /   ��  1   ��  5   ��  
   3�     >�  (   P�  1   y�  F   ��  %   ��     �      �     0�     >�  8   G�  ,   ��  ,   ��  
   ��     ��     ��  .   �  �   E�     �  3   �  '   A�      i�  /   ��  .   ��  D   ��  E   .�     t�  -   ��     ��     ��  *   ��     ��     �  /   1�     a�     z�     �  T   ��  Z   ��     >�  
   Y�     d�  $   k�  2   ��  -   ��     ��     ��     �  =   
�  "   H�  *   k�  '   ��     ��  2   ��  %   
�      0�     Q�     n�     ��     ��  &   ��  <   ��  ?   �  =   T�  1   ��     ��  0   ��  1   �     @�     T�     Z�     b�     k�      �  2   ��  4   ��  !   �  "   *�  "   M�  $   p�  '   ��     ��  7   ��     �  0   �  J   P�  !   ��  >   ��     ��  B   �  L   Y�  @   ��  0   ��  9   �  )   R�  4   |�  4   ��     ��  0   ��     '�     A�  )   a�     ��     ��     ��  v   ��  �   )�     ��     �     /�     H�  %   b�     ��  :   ��  9   ��  4   �  3   Q�  '   ��  &   ��  *   ��  (   ��  C   (�  >   l�  1   ��  >   ��  4   �  .   Q�     ��  (   ��     ��     ��  	   ��  
   ��  
   �  	   �     �     2�  	   >�     H�  #   f�  %   ��  3   ��     ��     ��     ��     �     �     0�      N�     o�     �   \   C   �   �   �   �      �  �   �   G  !   �  �   �     H         G           &   
          �       v       1   v      p      S  m      �   �   �   �           �           �   >  �   /  \  7  �   t     r  �  �  �   ,      �      �      �   �         &          �  e  �  j      �  d  Y       �   I  �  �   �         3       �  -  �  �  H      �      �   ;       B   a   �   �     %  �         h   X       o              �  �   �         �   �      d   2   �  �       }   k  L     �   �          f  �   n  �   }  y  �      4      `  �    E       z  �          �   9   �   Z   A   �  ;  X  7       �   �   �   �   �   �   �  ]   �       i  +  .       F   �              �   �  �   �      a  �                      D  �   R  l       �   s  �   m   �      �       �  �   �   $   �        3  T   �       Q   �  h  �       V  �    J      �  x  �   �   �       �           �  I   b          �  �          Y         �   K  �   t       �  g   |     �   Q  C  �   �      �       �  �   @      ?       �  �   ^    �    �          u  2  B  �  �  <   �         c   �  M  �   |   �  �   �          @      �              �  �   �     S     �                 �   i   	   �   %           R   �   <  �  �       {           +   c  '  .  l  �       �    �     '   �  �              y         s              8       1  �      !  )   �   D   -                   �       {  O           n   4   N   �  Z  �       =  �   �  �   �         �  A      �       �   ,    �          �   q   ^   �      �    �  u     �  �       �   (  �   :      e   �      8  �            W  `      �  T      �  z           �   �      �  
   �   �          �   9          F  �   *   >   	  0  �   [  j   �  ~  �       :       V          �       �      �   6  �                q    �  _   p   �                #      f       w  �   �    �   �  �  "  *  "      �   �          �       N  o           ~   �   �   �  �   �   x   k       �   �  �   �  �  �   �  �  �      K   U  �  w   U   �   �       �  M   �  �          /   5       �     �   �   �        �     (   �               =   �  �    L  ]              P   �   �  J   b       �       �   �  r       O  �   #   �   $  �   �  g       �   )          E            �     5  W   ?         _      �       �       �  6       P      �  �             �   �      0       [    
Allocating common symbols
 
Cross Reference Table

 
Discarded input sections

 
Linker script and memory map

 
Memory Configuration

 
Set                 Symbol

                                      Exclude objects, archive members from auto
                                      export, place into import library instead.
   --[no-]leading-underscore          Set explicit symbol underscore prefix mode
   --add-stdcall-alias                Export symbols with and without @nn
   --base_file <basefile>             Generate a base file for relocatable DLLs
   --compat-implib                    Create backward compatible import libs;
                                       create __imp_<SYMBOL> as well.
   --disable-auto-image-base          Do not auto-choose image base. (default)
   --disable-auto-import              Do not auto-import DATA items from DLLs
   --disable-long-section-names       Never use long COFF section names, even
                                       in object files
   --disable-runtime-pseudo-reloc     Do not add runtime pseudo-relocations for
                                       auto-imported DATA.
   --disable-stdcall-fixup            Don't link _sym to _sym@nn
   --dll                              Set image base to the default for DLLs
   --dll-search-prefix=<string>       When linking dynamically to a dll without
                                       an importlib, use <string><basename>.dll
                                       in preference to lib<basename>.dll 
   --dynamicbase			 Image base address may be relocated using
				       address space layout randomization (ASLR)
   --enable-auto-image-base           Automatically choose image base for DLLs
                                       unless user specifies one
   --enable-auto-import               Do sophisticated linking of _sym to
                                       __imp_sym for DATA references
   --enable-extra-pe-debug            Enable verbose debug output when building
                                       or linking to DLLs (esp. auto-import)
   --enable-long-section-names        Use long COFF section names even in
                                       executable image files
   --enable-runtime-pseudo-reloc      Work around auto-import limitations by
                                       adding pseudo-relocations resolved at
                                       runtime.
   --enable-stdcall-fixup             Link _sym to _sym@nn without warnings
   --exclude-all-symbols              Exclude all symbols from automatic export
   --exclude-libs lib,lib,...         Exclude libraries from automatic export
   --exclude-modules-for-implib mod,mod,...
   --exclude-symbols sym,sym,...      Exclude symbols from automatic export
   --export-all-symbols               Automatically export all globals to DLL
   --file-alignment <size>            Set file alignment
   --forceinteg		 Code integrity checks are enforced
   --heap <size>                      Set initial size of the heap
   --image-base <address>             Set start address of the executable
   --kill-at                          Remove @nn from exported symbols
   --large-address-aware              Executable supports virtual addresses
                                       greater than 2 gigabytes
   --major-image-version <number>     Set version number of the executable
   --major-os-version <number>        Set minimum required OS version
   --major-subsystem-version <number> Set minimum required OS subsystem version
   --minor-image-version <number>     Set revision number of the executable
   --minor-os-version <number>        Set minimum required OS revision
   --minor-subsystem-version <number> Set minimum required OS subsystem revision
   --no-bind			 Do not bind this image
   --no-isolation		 Image understands isolation but do not isolate the image
   --no-seh			 Image does not use SEH. No SE handler may
				       be called in this image
   --nxcompat		 Image is compatible with data execution prevention
   --out-implib <file>                Generate import library
   --output-def <file>                Generate a .DEF file for the built DLL
   --section-alignment <size>         Set section alignment
   --stack <size>                     Set size of the initial stack
   --subsystem <name>[:<version>]     Set required OS subsystem [& version]
   --support-old-code                 Support interworking with old code
   --support-old-code          Support interworking with old code
   --thumb-entry=<sym>         Set the entry point to be Thumb symbol <sym>
   --thumb-entry=<symbol>             Set the entry point to be Thumb <symbol>
   --tsaware                  Image is Terminal Server aware
   --warn-duplicate-exports           Warn about duplicate exports.
   --wdmdriver		 Driver uses the WDM model
   @FILE   Supported emulations:
   no emulation specific options.
  additional relocation overflows omitted from the output
  load address 0x%V  relocation truncated to fit: %s against `%T'  relocation truncated to fit: %s against symbol `%T' defined in %A section in %B  relocation truncated to fit: %s against undefined symbol `%T' %8x something else
 %B%F: could not read relocs: %E
 %B%F: could not read symbols: %E
 %B: In function `%T':
 %B: file not recognized: %E
 %B: matching formats: %B: warning: common is here
 %B: warning: common of `%T' overridden by definition
 %B: warning: common of `%T' overridden by larger common
 %B: warning: common of `%T' overriding smaller common
 %B: warning: defined here
 %B: warning: definition of `%T' overriding common
 %B: warning: larger common is here
 %B: warning: more undefined references to `%T' follow
 %B: warning: multiple common of `%T'
 %B: warning: previous common is here
 %B: warning: smaller common is here
 %B: warning: undefined reference to `%T'
 %C: Cannot get section contents - auto-import exception
 %C: variable '%T' can't be auto-imported. Please read the documentation for ld's --enable-auto-import for details.
 %C: warning: undefined reference to `%T'
 %D: first defined here
 %D: warning: more undefined references to `%T' follow
 %F%B: file not recognized: %E
 %F%B: final close failed: %E
 %F%B: member %B in archive is not an object
 %F%P: %s not found for insert
 %F%P: attempted static link of dynamic object `%s'
 %F%P: bfd_record_phdr failed: %E
 %F%P: cannot create split section name for %s
 %F%P: cannot open base file %s
 %F%P: cannot perform PE operations on non PE output file '%B'.
 %F%P: clone section failed: %E
 %F%P: final link failed: %E
 %F%P: internal error %s %d
 %F%P: invalid BFD target `%s'
 %F%P: invalid data statement
 %F%P: invalid reloc statement
 %F%P: no sections assigned to phdrs
 %F%P:%S: error: alias for default memory region
 %F%P:%S: error: memory region `%s' for alias `%s' does not exist
 %F%P:%S: error: redefinition of memory region alias `%s'
 %F%S %% by zero
 %F%S / by zero
 %F%S can not PROVIDE assignment to location counter
 %F%S cannot move location counter backwards (from %V to %V)
 %F%S invalid assignment to location counter
 %F%S: non constant or forward reference address expression for section %s
 %F%S: nonconstant expression for %s
 %F%S: undefined MEMORY region `%s' referenced in expression
 %F%S: undefined section `%s' referenced in expression
 %F%S: undefined symbol `%s' referenced in expression
 %F%S: unknown constant `%s' referenced in expression
 %P%F: %s: non-ELF symbol in ELF BFD!
 %P%F: %s: plugin reported error after all symbols read
 %P%F: %s: plugin reported error claiming file
 %P%F: -F may not be used without -shared
 %P%F: -f may not be used without -shared
 %P%F: -pie not supported
 %P%F: -r and -shared may not be used together
 %P%F: -shared not supported
 %P%F: BFD backend error: BFD_RELOC_CTOR unsupported
 %P%F: Could not define common symbol `%T': %E
 %P%F: Failed to create hash table
 %P%F: Illegal use of `%s' section
 %P%F: Relocatable linking with relocations from format %s (%B) to format %s (%B) is not supported
 %P%F: bad --unresolved-symbols option: %s
 %P%F: bad -plugin-opt option
 %P%F: bad -rpath option
 %P%F: bfd_hash_allocate failed creating symbol %s
 %P%F: bfd_hash_lookup failed creating symbol %s
 %P%F: bfd_hash_lookup failed: %E
 %P%F: bfd_hash_lookup for insertion failed: %E
 %P%F: bfd_hash_table_init failed: %E
 %P%F: bfd_link_hash_lookup failed: %E
 %P%F: bfd_new_link_order failed
 %P%F: can not create hash table: %E
 %P%F: can't relax section: %E
 %P%F: can't set start address
 %P%F: cannot open linker script file %s: %E
 %P%F: cannot open map file %s: %E
 %P%F: cannot open output file %s: %E
 %P%F: cannot represent machine `%s'
 %P%F: error: no memory region specified for loadable section `%s'
 %P%F: failed creating section `%s': %E
 %P%F: gc-sections requires either an entry or an undefined symbol
 %P%F: group ended before it began (--help for usage)
 %P%F: invalid argument to option "--section-start"
 %P%F: invalid common section sorting option: %s
 %P%F: invalid hex number `%s'
 %P%F: invalid hex number for PE parameter '%s'
 %P%F: invalid number `%s'
 %P%F: invalid section sorting option: %s
 %P%F: invalid subsystem type %s
 %P%F: invalid syntax in flags
 %P%F: missing argument to -m
 %P%F: missing argument(s) to option "--section-start"
 %P%F: multiple STARTUP files
 %P%F: no input files
 %P%F: output format %s cannot represent section called %s
 %P%F: please report this bug
 %P%F: strange hex info for PE parameter '%s'
 %P%F: target %s not found
 %P%F: unknown ELF symbol visibility: %d!
 %P%F: unknown format type %s
 %P%F: unrecognized -a option `%s'
 %P%F: unrecognized -assert option `%s'
 %P%F: use the --help option for usage information
 %P%F:%s: can not make object file: %E
 %P%F:%s: can not set architecture: %E
 %P%F:%s: can't set start address
 %P%F:%s: hash creation failed
 %P%X: %s architecture of input file `%B' is incompatible with %s output
 %P%X: %s does not support reloc %s for set %s
 %P%X: --hash-size needs a numeric argument
 %P%X: Different object file formats composing set %s
 %P%X: Different relocs used in set %s
 %P%X: Internal error on COFF shared library section %s
 %P%X: Unsupported size %d for set %s
 %P%X: failed to merge target specific data of file %B
 %P: %B: symbol `%s' definition: %d, visibility: %d, resolution: %d
 %P: Disabling relaxation: it will not work with multiple definitions
 %P: Error closing file `%s'
 %P: Error writing file `%s'
 %P: `-retain-symbols-file' overrides `-s' and `-S'
 %P: cannot find %s
 %P: cannot find %s (%s): %E
 %P: cannot find %s inside %s
 %P: cannot find %s: %E
 %P: internal error: aborting at %s line %d
 %P: internal error: aborting at %s line %d in %s
 %P: link errors found, deleting executable `%s'
 %P: mode %s
 %P: skipping incompatible %s when searching for %s
 %P: symbol `%T' missing from main hash table
 %P: unrecognised emulation mode: %s
 %P: unrecognized option '%s'
 %P: warning, file alignment > section alignment.
 %P: warning: %s contains output sections; did you forget -T?
 %P: warning: '--thumb-entry %s' is overriding '-e %s'
 %P: warning: --export-dynamic is not supported for PE targets, did you mean --export-all-symbols?
 %P: warning: address of `%s' isn't multiple of maximum page size
 %P: warning: auto-importing has been activated without --enable-auto-import specified on the command line.
This should work unless it involves constant data structures referencing symbols from auto-imported DLLs.
 %P: warning: bad version number in -subsystem option
 %P: warning: cannot find entry symbol %s; defaulting to %V
 %P: warning: cannot find entry symbol %s; not setting start address
 %P: warning: cannot find thumb start symbol %s
 %P: warning: changing start of section %s by %lu bytes
 %P: warning: could not find any targets that match endianness requirement
 %P: warning: dot moved backwards before `%s'
 %P: warning: global constructor %s used
 %P: warning: no memory region specified for loadable section `%s'
 %P:%S: warning: memory region `%s' not declared
 %P:%S: warning: redeclaration of memory region `%s'
 %S HLL ignored
 %S SYSLIB ignored
 %W (size before relaxing)
 %X%B: more undefined references to `%T' follow
 %X%B: undefined reference to `%T'
 %X%C: multiple definition of `%T'
 %X%C: prohibited cross reference from %s to `%T' in %s
 %X%C: undefined reference to `%T'
 %X%D: more undefined references to `%T' follow
 %X%H: dangerous relocation: %s
 %X%H: reloc refers to symbol `%T' which is not being output
 %X%P: %B section `%s' will not fit in region `%s'
 %X%P: address 0x%v of %B section `%s' is not within region `%s'
 %X%P: anonymous version tag cannot be combined with other version tags
 %X%P: bfd_hash_table_init of cref table failed: %E
 %X%P: can't set BFD default target to `%s': %E
 %X%P: cref alloc failed: %E
 %X%P: cref_hash_lookup failed: %E
 %X%P: duplicate expression `%s' in version information
 %X%P: duplicate version tag `%s'
 %X%P: error: duplicate retain-symbols-file
 %X%P: region `%s' overflowed by %ld bytes
 %X%P: section %s loaded at [%V,%V] overlaps section %s loaded at [%V,%V]
 %X%P: section `%s' assigned to non-existent phdr `%s'
 %X%P: unable to find version dependency `%s'
 %X%P: unable to open for destination of copy `%s'
 %X%P: unable to open for source of copy `%s'
 %X%P: unable to read .exports section contents
 %X%P: unknown feature `%s'
 %X%P: unknown language `%s' in version information
 %X%P:%S: PHDRS and FILEHDR are not supported when prior PT_LOAD headers lack them
 %X%P:%S: section has both a load address and a load region
 %X%S: unresolvable symbol `%s' referenced in expression
 %X%s(%s): can't find member in archive %X%s(%s): can't find member in non-archive file %XCan't open .lib file: %s
 %XCannot export %s: invalid export name
 %XCannot export %s: symbol not defined
 %XCannot export %s: symbol not found
 %XCannot export %s: symbol wrong type (%d vs %d)
 %XError, duplicate EXPORT with ordinals: %s (%d vs %d)
 %XError, ordinal used twice: %d (%s vs %s)
 %XError: %d-bit reloc in dll
 %XError: can't use long section names on this arch
 %XUnsupported PEI architecture: %s
 %Xbfd_openr %s: %E
 %s: Can't open output def file %s
 %s: data size %ld
 %s: emulation specific options:
 %s: supported emulations:  %s: supported targets: %s: total time in link: %ld.%06ld
 ; no contents available
 <no plugin> ADDRESS ARCH ARG Accept input files whose architecture cannot be determined Add DIRECTORY to library search path Add data symbols to dynamic list Address of section %s set to  Allow multiple definitions Allow unresolved references in shared libraries Always set DT_NEEDED for dynamic libraries mentioned on
                                the command line Attributes Auxiliary filter for shared object symbol table Bind global function references locally Bind global references locally Build global constructor/destructor tables COUNT Call SYMBOL at load-time Call SYMBOL at unload-time Check section addresses for overlaps (default) Common symbol       size              file

 Copy DT_NEEDED links mentioned inside DSOs that follow Create a position independent executable Create a shared library Create an output file even if errors occur Create default symbol version Create default symbol version for imported symbols Creating library file: %s
 DIRECTORY Default search path for Solaris compatibility Define a symbol Demangle symbol names [using STYLE] Disallow undefined version Discard all local symbols Discard temporary local symbols (default) Display target specific options Do not allow unresolved references in object files Do not allow unresolved references in shared libs Do not check section addresses for overlaps Do not copy DT_NEEDED links mentioned inside DSOs that follow Do not define Common storage Do not demangle symbol names Do not link against shared libraries Do not list removed unused sections Do not page align data Do not page align data, do not make text readonly Do not strip symbols in discarded sections Do not treat warnings as errors (default) Do not use relaxation techniques to reduce code size Do task level linking Don't discard any local symbols Don't merge input [SECTION | orphan] sections Don't remove unused sections (default) Don't warn about mismatched input files Don't warn on finding an incompatible library EMULATION End a group Errors encountered processing file %s Errors encountered processing file %s
 Errors encountered processing file %s for interworking
 Export all dynamic symbols FILE FILENAME Fail with %d
 File
 Filter for shared object symbol table Force common symbols to be defined Force generation of file with .exe suffix GNU ld %s
 Generate embedded relocs Generate relocatable output How many tags to reserve in .dynamic section How to handle unresolved symbols.  <method> is:
                                ignore-all, report-all, ignore-in-object-files,
                                ignore-in-shared-libs Ignored Ignored for GCC LTO option compatibility Ignored for Linux compatibility Ignored for SVR4 compatibility Ignored for SunOS compatibility Include all objects from following archives Info: resolving %s by linking to %s (auto-import)
 Just link symbols (if directory, same as --rpath) KEYWORD Keep only symbols listed in FILE LIBNAME Length Link against shared libraries Link big-endian objects Link little-endian objects List removed unused sections on stderr Load named plugin Name No symbols
 Only set DT_NEEDED for following dynamic libs if used Only use library directories specified on
                                the command line Optimize output file Options:
 Origin Output cross reference table Output lots of information during link Override the default sysroot location PATH PLUGIN PROGRAM Page align data, make text readonly Print default output format Print map file on standard output Print memory usage statistics Print option help Print version and emulation information Print version information Read MRI format linker script Read default linker script Read dynamic list Read linker script Read options from FILE
 Read version information script Reduce code size by using target specific optimizations Reduce memory overheads, possibly taking much longer Reject input files whose architecture is unknown Remove unused sections (on some targets) Report bugs to %s
 Report unresolved symbols as errors Report unresolved symbols as warnings SECTION=ADDRESS SHLIB SIZE SYMBOL SYMBOL=EXPRESSION Search for library LIBNAME Send arg to last-loaded plugin Set PROGRAM as the dynamic linker to use Set address of .bss section Set address of .data section Set address of .text section Set address of named section Set address of text segment Set architecture Set default hash table size close to <NUMBER> Set emulation Set internal name of shared library Set link time shared library search path Set output file name Set runtime shared library search path Set start address Shared library control for HP/UX compatibility Small data size (if no size, same as --shared) Sort common symbols by alignment [in specified order] Sort sections by name or maximum alignment Specify target for following input files Specify target of output file Split output sections every COUNT relocs Split output sections every SIZE octets Start a group Start with undefined reference to SYMBOL Strip all symbols Strip debugging symbols Strip symbols in discarded sections Supported emulations:  Symbol TARGET Take export symbols list from .exports, using
                                SYMBOL as the version. This program is free software; you may redistribute it under the terms of
the GNU General Public License version 3 or (at your option) a later version.
This program has absolutely no warranty.
 Trace file opens Trace mentions of SYMBOL Treat warnings as errors Turn off --whole-archive Undo the effect of --export-dynamic Usage: %s [options] file...
 Use --disable-stdcall-fixup to disable these fixups
 Use --enable-stdcall-fixup to disable these warnings
 Use C++ operator new/delete dynamic list Use C++ typeinfo dynamic list Use less memory and more disk I/O Use same format as native linker Use wrapper functions for SYMBOL Warn about duplicate common symbols Warn if an object has alternate ELF machine code Warn if global constructors/destructors are seen Warn if shared object has DT_TEXTREL Warn if start of section changes due to alignment Warn if the multiple GP values are used Warn only once per undefined symbol Warning, duplicate EXPORT: %s
 Warning: resolving %s by linking to %s
 Write a map file [=COUNT] [=NUMBER] [=SECTION] [=SIZE] [=STYLE] [=ascending|descending] alignment ascending attempt to open %s failed
 attempt to open %s succeeded
 cannot find script file %s
 could not create dummy IR bfd: %F%E
 descending name name|alignment no symbol opened script file %s
 using external linker script: using internal linker script: warning:  Project-Id-Version: binutils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2013-03-07 23:28+0000
Last-Translator: Neliton Pereira Jr. <nelitonpjr@gmail.com>
Language-Team: Portuguese (Brazil) <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:03+0000
X-Generator: Launchpad (build 18115)
 
Alocando simbolos comuns
 
Tabela de Referência Cruzada

 
Seções de entrada descartadas

 
Script do link-editor e mapa de memória

 
Configuração de Memória

 
Configurar Símbolo

                                      Exclui objetos, membros do arquivo de auto
                                      exporta, coloca dentro da biblioteca de importação.
   --[no-]leading-underscore          Define o símbolo explícito sublinhado no modo prefixo
   --add-stdcall-alias Exporta símbolos com e sem @nn
   --base_file <arquivo-base> Gera um arquivo base para DLLs realocáveis
   --compat-implib Criar libs de importação compativeis para trás;
                                       Criar __imp_<SYMBOL> também.
   --disable-auto-image-base Não escolhe uma imagem base automaticamente. (default)↵
   --disable-auto-import Não importe automaticamente itens DATA de DLLS
   --disable-long-section-names       Nunca usa nomes de seção COFF longos, mesmo
                                       em arquivos objeto
   --disable-runtime-pseudo-reloc Não adicionar pseudo-relocações de tempo de execução para
                                       referências DATA importados automaticamente.
   --disable-stdcall-fixup Não vincula _sym to _sym@nn↵
   --dll Configura imagem base para o padrão para DLLs
   --dll-search-prefix=<string> Ao link-editar dinamicamente com uma dll sem
                                       uma biblioteca de importação, use preferivelmente <string><basename>.dll
                                       ao invés de lib<basename>.dll 
   --dynamicbase			 O endereço base da imagem pode ser realocado usando
				 o layout aleatório de espaço de endereços (em inglês: ASLR)
   --enable-auto-image-base Seleciona imagem base para DLLs automáticamente
                                       a menos que o usuário especifique uma
   --enable-auto-import               Faz vinculações sofisticadas de _sym a
                                       __imp_sym para referências de DADOS
   --enable-extra-pe-debug Ativa saída de depuração detalhada ao criar
                                       ou link-editar a DLLs (especialmente no caso de importação automática)
   --enable-long-section-names        Usa nomes de seção COFF longos mesmo em
                                       arquivos de imagens executáveis
   --enable-runtime-pseudo-reloc Contorno de limitações  da importação automática através
                                       da inclusão de pseudo-relocações a serem resolvidas em
                                       tempo de execução.
   --enable-stdcall-fixup Vincula _sym to _sym@nn without warnings↵
   --exclude-all-symbols              Exclui todos os símbolos da exportação automática
   --exclude-libs lib,lib,... Exclui bibliotecas da exportação automática
   --exclude-modules-for-implib mod,mod,...
   --exclude-symbols sym,sym,... Exclui símbolos da exportação automática
   --export-all-symbols Exporta automaticamente todos os símbolos globais para DLL
   --file-alignment <tam> Configura o alinhamento do arquivo
   --forceinteg		 São executadas verificações de integridade de código
   --heap <tamanho> Define o tamanho inicial da pilha
   --image-base <endereço> Configura o endereço inicial do executável
   --kill-at Remove @nn dos símbolos exportados
   --large-address-aware Executável suporta endereços virtuais
                                       maiores que 2 gigabytes
   --major-image-version <número> Configura o número de versão do executável
   --major-os-version <número> Configura a versão mínima requerida do SO
   --major-subsystem-version <número> Configura a versão mínima requerida do subsistema do SO
   --minor-image-version <número> Configura o número de revisão do executável
   --minor-os-version <número> Configura a revisão mínima requerida do SO
   --minor-subsystem-version <número> Configura a revisão mínima requerida do subsistema do SO
   --no-bind			 Não vincule esta imagem
   --no-isolation		 Imagem entende a isolação, mas não isola a imagem
   --no-seh			 A imagem não usa SEH. Nenhum manipulador SE pode
				       ser chamado nesta imagem
   --nxcompat		 Imagem é compatível com a prevenção de execução de dado
   --out-implib <arquivo> Gera biblioteca de importação
   --output-def <arquivo> Gera um arquivo .DEF para a DLL construída
   --section-alignment <tam> Configura o alinhamento de seção
   --stack <tam> Configura o tamanho inicial da pilha
   --subsystem <nome>[:<versão>] Configura o subsistema do SO requerido [& versão]
   --support-old-code Suporta trabalho interno com código velho
   --support-old-code          Suporta funcionamento com código antigo
   --thumb-entry=<sym>         Define o ponto de entrada para ser o símbolo de Miniatura <sym>
   --thumb-entry=<symbol> Configura o ponto de entrada para ser <symbol>
   --tsaware                  Imagem é um Terminal Server conhecido
   --warn-duplicate-exports Avisa sobre exportações duplicadas.
   --wdmdriver		 Driver usa o modelo WDM
   @ARQUIVO   Emulação suportada:
   sem opções de emulação especifica.
  estouros de alocação adicionais são omitidos da saída.
  carregando endereço 0x%V  relocação truncada para caber: %s contra `%T'  relocação truncada para caber: %s contra símbolo `%T' definido em seção %A em %B  relocação truncada para caber: %s contra símbolo indefinido `%T' %8x algo mais
 %B%F: não foi possível ler relocs: %E
 %B%F: Não é possível ler símbolos: %E
 %B: na função `%T':
 %B: arquivo não reconhecido: %E
 %B: formatos que casam: %B: aviso: a comum está aqui
 %B: aviso: a comum de  `%T' subtituida pela definição
 %B: aviso: a comum de `%T' substituida por uma commum maior
 %B: aviso: a comum de `%T' substituindo comum menor
 %B: aviso: definido aqui
 %B: aviso: a definição de `%T' está substituindo a comum
 %B: aviso: a comum maior está aqui
 %B: aviso: mais referências indefinidas para `%T' seguir
 %B: aviso: múltiplas comuns de `%T'
 %B: aviso: comum anterior é aqui
 %B: aviso: a comum menor está aqui
 %B: aviso: referência indefinida para `%T'
 %C: Não pode começar conteúdo - auto-import exception
 %C: variável '%T' não pôde ser auto-importada. Por favor, leia a documentação para opção --enable-auto-import do ld, para detalhes.
 %C: aviso: referência indefinida para `%T'
 %D: definido primeiramente aqui
 %D: atenção: mais referências indefinidas para seguir `%T'
 %F%B: arquivo não reconhecido: %E
 %F%B: fechamento final falhou: %E
 %F%B: membro %B no arquivo não é um objeto
 %F%P: %s não disponível para inserir
 %F%P: tentativa de link-edição estática do objeto dinâmico `%s'
 %F%P: bfd_record_phdr falhou: %E
 %F%P: não foi possível definir um nome para a seção divida em %s
 %F%P: não foi possível abrir o arquivo base %s
 %F%P: não é possível executar operações PE no arquivo de saída não-PE '%B'.
 %F%P: falha ao clonar seção: %E
 %F%P: falha no link final: %E
 %F%P: erro interno %s %d
 %F%P: alvo BFD inválido `%s'
 %F%P: declaração de dados inválido
 %F%P: declaração de reloc inválida
 %F%P: sem seções atribuídas a phdrs
 %F%P:%S: erro: alias para região padrão da memória
 %F%P:%S: erro: região da memória `%s' para alias `%s' não existe
 %F%P:%S: erro: redefinição do suposto nome da região da memória `%s'
 %F%S %% por zero
 %F%S / por zero
 %F%S não pode FORNECER a atribuição para o contador local
 %F%S não é possível mover o contador de endereços para trás (de %V para %V)
 %F%S atribuição invalida para o contador local
 %F%S: referência não constante ou consecutiva de expressão de endereço para seção %s
 %F%S: expressão não constante para %s
 %F%S: região de MEMÓRIA indefinida `%s' referênciada na expressão
 %F%S: seção não definida `%s' na expressão referenciada
 %F%S: símbolo indefinido `%s' referênciado na expressão
 %F%S: constante desconhecida `%s' referenciada na expressão
 %P%F: %s: símbolo que não pertence ao ELF em ELF BFD!
 %P%F: %s: o plug-in reportou um erro depois de ler todos os símbolos
 %P%F: %s: o plug-in relatou um erro do arquivo reivindicado
 %P%F: -F não pode ser usado sem -shared
 %P%F: -f não pode ser usado sem -shared
 %P%F: -pie não suportada
 %P%F: -r e -shared não podem ser usadas juntas
 %P%F: -compartilhamento não suportado
 %P%F: Erro de backend BFD: BFD_RELOC_CTOR não suportado
 %P%F: Não é possível definir o símbolo comum `%T': %E
 %P%F: Falha ao criar a tabela de hash
 %P%F: Uso ilegal de `%s' seção
 %P%F: Vinculações relocáveis com referências eliminadas do formato %s (%B) para o dormato %s (%B) não é suportado
 %P%F: opção inválida  --simbolos-não resolvido: %s
 %P%F: opção inválida de -plugin-opt
 %P%F: opção inválida -rpath
 %P%F: bfd_hash_allocate falhou ao criar símbolo %s
 %P%F: bfd_hash_lookup falhou ao criar símbolo %s
 %P%F: bfd_hash_lookup falhou: %E
 %P%F: bfd_hash_lookup para inserção falhou: %E
 %P%F: bfd_hash_table_init falhou: %E
 %P%F: bfd_link_hash_lookup falhou: %E
 %P%F: bfd_new_link_order falhou
 %P%F: não foi possível criar a tabela de hash: %E
 %P%F: não é possível relaxar seção: %E
 %P%F: não foi possível o endereço inicial
 %P%F: não foi possível abrir o arquivo de script %s: %E
 %P%F: não foi possível abrir o arquivo de mapa %s: %E
 %P%F: não foi possível abrir o arquivo de saída %s: %E
 %P%F: não foi possível representar a maquina `%s'
 %P%F: erro: região da memória não especificada para carregar a seção `%s'
 %P%F: falha ao criar a seção `%s': %E
 %P%F: cada seção-gc requer uma entrada ou um símbolo indefinido
 %P%F: o grupo terminou antes que começou (--ajuda para tratamento)
 %P%F: argumento inválido para opção"--iniciar-sessão"
 %P%F: opção de classificação de seção comum inválida: %s
 %P%F: Número hexadecimal inválido `%s'
 %P%F: número hex inválido para o parâmetro PE '%s'
 %P%F: número inválido `%s'
 %P%F: sessão inválida ordenando opção: %s
 %P%F: tipo inválido de subsistema %s
 %P%F: sintaxe inválida nas flags
 %P%F: faltando argumento para -m
 %P%F: Falta argumento(s) para a opção "--iniciar-sessão"
 %P%F: múltiplos arquivos de INICIALIZAÇÃO
 %P%F: Nenhum arquivos de entrada
 %P%F: não é possível representar no formato de saída %s a seção de nome %s
 %P%F: por favor, reporte este erro
 %P%F: informação hexadecimal estranha para o parâmetro PE '%s'
 %P%F: alvo %s não localizado
 %P%F: visibilidade de símbolo ELF desconhecida: %d!
 %P%F: tipo de formato %s desconhecido
 %P%F: opção desconhecida -a `%s'
 %P%F: opção desconhecida -assert `%s'
 %P%F: use a opção --help para obter mais informações
 %P%F:%s: não pode criar o arquivo objeto: %E
 %P%F:%s: não foi possível ajustar a arquitetura: %E
 %P%F:%s: não foi possível ajustar o endereço inicial
 %P%F:%s: falha na criação do hash
 %P%X: a arquitetura %s do arquivo de entrada `%B' é incompatível com a saída %s
 %P%X: %s reloc não suportado %s para definição %s
 %P%X: --hash-size precisa de um argumento numérico
 %P%X: Formatos diferentes de arquivo objeto que compôem a coleção %s
 %P%X: Diferente relocs usado no ajuste %s
 %P%X: Erro interno na biblioteca compartilhada COFF seção %s
 %P%X: Tamanho não suportado %d para ajustes %s
 %P%X: falhou o alvo especificado ao mesclar do arquivo de dados %B
 %P: %B: símbolo `%s' definição: %d, visibilidade: %d, resolução: %d
 %P: Desativação de relaxamento: não vai trabalhar com múltiplas definições
 %P: Erro fechando arquivo `%s'
 %P: Erro escrevendo arquivo `%s'
 %P: `-retain-symbols-file' ignorados `-s' e `-S'
 %P: não foi possível encontrar %s
 %P: não foi possível encontrar %s (%s): %E
 %P: não foi possível encontrar %s dentro de %s
 %P: não foi possível encontrar %s: %E
 %P: erro interno: abortando em %s linha %d
 %P: erro interno: abortando em %s linha %d em %s
 %P: encontrado erros de referência, removendo executável `%s'
 %P: modo %s
 %P: ignorando %s incompatível ao procurar por %s
 %P: símbolo `%T' que faltava da mistura da tabela principal
 %P: modo de emulação desconhecido: %s
 %P: opção desconhecida '%s'
 %P: aviso, alinhamento do arquivo > alinhamento da seção.
 %P: aviso: %s contém seções de saída; você se esqueceu -T?
 %P: aviso: '--thumb-entry %s' está prevalecendo sobre '-e %s'
 %P: atenção: --export-dynamic não é suportado por alvos PE, você quis dizer --export-all-symbols?
 %P: atenção: endereço de `%s' não é múltiplo do tamanho máximo da página
 %P: aviso: a auto importação foi ativada sem --enable-auto-import ser especificado na linha de comando.
Isso deve funcionar a menos que envolva estruturas de dados constantes que referencie a DLLs importadas automaticamente.
 %P: atenção: número de versão incorreto na opção -subsystem
 %P: aviso: não foi possível encontrar símbolo de entrada %s; padronizando para %V
 %P: aviso: impossível encontrar  o símbolo de entrada  %s; sem endereço de inicio escolhido
 %P: aviso: não foi possível encontrar o símbolo thumb inicial %s
 %P: atenção: alterando o início da seção %s por %lu bytes
 %P: aviso: não foi possível encontrar um alvo que casa com o requisito "endianness"
 %P: atenção: não movido para trás anteriormente `%s'
 %P: aviso: usado construtor global %s
 %P: aviso: região da memória não especificada para carregar a seção `%s'
 %P:%S: aviso: região da memória `%s' não declarada
 %P:%S: atenção: redeclaração da região da memória `%s'
 %S HLL ignorado
 %S SYSLIB ignorado
 %W (tamanho de relaxar)
 %X%B: mais referências indefinidas para `%T' seguir
 %X%B: referência indefinida para `%T'
 %X%C: múltipla definição de `%T'
 %X%C: proibida referência cruzada de %s para `%T' em %s
 %X%C: referência indefinida para `%T'
 %X%D: mais referências indefinidas para seguir `%T'
 %X%H: realocação perigosa: %s
 %X %H: reloc se refere a símbolo "%T" que não está sendo gerado na saída
 %X%P: %B a seção `%s' não dabe na região `%s'
 %X%P: endereço 0x%v da %B seção `%s' não está dentro da região `%s'
 %X%P: marca de versão anônima não pode ser combinada com outras marcas de versões
 %X%P: falha em bfd_hash_table_init na tabela de referências cruzadas: %E
 %X%P: impossível definir o alvo padrão do BFD para `%s': %E
 %X%P: falha na alocação de referências cruzadas: %E
 %X%P: cref_hash_lookup falhou: %E
 %X%P: expressão duplicada `%s' na informação da versão
 %X%P: versão da tag duplicada`%s'
 %X%P: erro: duplicado retain-symbols-file
 %X%P: região `%s' sobrecarregou em %ld bytes
 %X%P: seção %s carregada em [%V,%V] seção de sobreposições %s carregada em [%V,%V]
 %X%P: seção `%s' atribuída a phdr inexistente `%s'
 %X%P: não foi possível encontrar dependências para esta versão `%s'
 %X%P: impossível abrir para o destino da cópia `%s'
 %X%P: não foi possível abrir origem da cópia `%s'
 %X%P: incapaz de ler. seção de exportações de conteúdos
 %X%P: recurso desconhecido `%s'
 %X%P: linguagem desconhecida `%s' na versão da informação
 %X%P:%S: PHDRS e FILEHDR não são suportados quando os cabeçalhos PT_LOAD previstos estão faltando
 %X%P:%S: a seção tem tanto um endereço de carregamento quanto uma região de carregamento
 %X%S: simbolo não resolvido `%s' referênciado na expressão
 %X%s(%s): não é possível localizar membro em arquivo %X%s(%s): não foi possível encontrar membro em um arquivo não-pacote %XImpossível abrir arquivo .lib: %s
 %XImpossível exportar %s: nome inválido da exportação
 %XImpossível exportar %s: símbolo não definido
 %XImpossível exportar %s: símbolo não encontrado
 %XImpossível exportar %s: tipo errado de símbolo (%d vs %d)
 %XErro, EXPORTAÇÃO duplicada com ordinais: %s (%d vs %d)
 %XErro, ordinal usado duas vezes: %d (%s vs %s)
 %XErro: realocação de %d bits em dll
 %XErro: não é possível usar nomes de seção longos neste pacote
 %XArquitetura PEI não suportada: %s
 %Xbfd_openr %s: %E
 %s: não foi possível abrir o arquivo de saída def %s
 %s: tamanho do dado %ld
 %s: opções específicas de emulação:
 %s: emulações suportadas:  %s: destinos suportados: %s: tempo total no link: %ld.%06ld
 ; nenhum conteúdo disponível
 <nenhum plug-in> ENDEREÇO ARCH ARG Aceitar os arquivos de entrada cuja a arquitetura não pode ser determinada Adicionar DIRETÓRIO ao caminho de busca da biblioteca Adicionar símbolos dos dados à lista dinâmica Endereço da seção %s ajustado para  Permitir definições multiplas Permitir referências não resolvidas nas bibliotecas compartilhadas Sempre definir DT_NEEDED para bibliotecas dinâmicas mencionadas na
                                linha de comando Atributos Filtro auxiliar para tabela de objetos simbólicos compartilhados Vincular função global referenciada localmente Vincular referências globais locais Configurar tabelas globais de construção/destruição CONTAGEM Chamar SIMBOLO no load-time Chamar SIMBOLO no unload-time Analisar sobreposições nas seções de endereços (padrão) Tamanho comum do arquivo de símbolos

 Copiar DT_NEEDED links mencionados em DSOs que seguem Cria um executável independente da posição Criar uma biblioteca compartilhada Criar um arquivo de log quando ocorrer algum erro Criar a versão do simbolo padrão Criar a versão do simbolo padrão para símbolos importado Criando arquivo de biblioteca: %s
 DIRETÓRIO Caminho padrão de busca para compatibilidade com Solaris Definir um simbolo Decodificar os nomes dos simbolos [usando STYLE] Não permitir versão indefinida Rejeitar todos os símbolos locais Descartar símbolos locais temporários (Padrão) Exibir opções específicas de destino Não permitir referencias não resolvidas em arquivos objeto Não permitir referências não resolvidas em libs compartilhadas Não analisar sobreposições nas seções de endereços Não copiar os links de DT_NEEDED mencionados dentro dos seguintes DSOs Não definir o armazenamento comum Não decodificar os nomes dos símbolos Não vincular contra bibliotecas compartilhadas Não listar seções removidas não usadas Não alinhar dados da página Não alinhar dados por página, não definir texto como somente-leitura Não separar símbolos em seções descartadas Não tratar avisos como erros (padrão) Não use técnicas de descaso para reduzir o tamanho do código Faça ligação do nível de tarefa Não rejeitar nenhum simbolo local Não mesclar entradas de seções [SEÇÃO | órfão] Não remover seções não utilizadas (padrão) Não avisar sobre arquivos de entrada inadequados Não avisar ao encontrar uma biblioteca incompatível EMULAÇÃO Finaliza um grupo Erros encontrados processando arquivo %s Erros encontrados no processamento do arquivo %s
 erros encontrados no processamento do arquivo %s para interoperação
 Exportar todos os simbolos dinâmicos ARQUIVO NOME DO ARQUIVO Falha com %d
 Arquivo
 Filtro para tabela de objetos simbólicos compartilhados Forçar que símbolos comuns sejam definidos Forçar geração de arquivo com sufixo .exe GNU ld %s
 Gerar relocs embarcados Gerar saída realocável Quantas etiquetas reservar na seção .dynamic Como lidar com símbolos não resolvidos. <method> são:
                                ignore-all, report-all, ignore-in-object-files,
                                ignore-in-shared-libs Ignorado Ignorar para opção de compatibilidade com GCC LTO Ignorado para compatibilidade com Linux Ignorar compatibilidade com SVR4 Ignorado para manter comptabilidade com o SunOS Incluir todos os objetos dos seguintes pacotes Informação: resolvendo %s ligando a %s (importar-automaticamente)
 Vincular apenas símbolos (se diretórios, da mesma forma que -rpath) PALAVRA CHAVE Mantenha apenas símbolos listados no ARQUIVO NOME DA BIBLIOTECA Tamanho Vínculo contra bibliotecas compartilhadas Vincular objetos big-endian Vincular objetos little-endian Listar seções removidas não usadas na stderr Carregar plug-in nomeado Nome Sem símbolos
 Definir somente DT_NEEDED para as seguintes bibliotecas dinâmicas que estão em uso Usar somente diretórios especificados na
                                linha de comando Otimizar arquivo de saída Opções:
 Origem Exibir tabela de referência cruzada Saída de informações em lotes durante ligação Substituir a localização padrão do sysroot CAMINHO PLUG-IN PROGRAMA Alinhar dados por página, definir texto como somente-leitura Imprimir formato de saída padrão Imprimir mapa do arquivo na saída padrão Mostrar estatisticas de uso da memória Mostrar opção de ajuda Mostrar a versão e as informações da emulação Mostrar informações sobre a versão Ler script do linker formato MRI Ler script do linker padrão Ler lista dinâmica Ler script do linker Ler opções do arquivo
 Ler informações da versão do script Reduzir tamanho do código usando otimizações específicas Reduzir os gastos de memória, possivelmente levando mais tempo Rejeitar arquivos de entrada cuja arquitetura é desconhecida Remova sessões não utilizadas (em alguns alvos) Relate os erros para %s
 Informar os símbolos não resolvidos como erros Informar os símbolos não resolvidos como avisos [SEÇÃO=ENDEREÇO] SHLIB TAMANHO SÍMBOLO SÍMBOLO=EXPRESSÃO Pesquisar por biblioteca LIBNAME Enviar argumentos para o último plug-in carregado Definir PROGRAMA como referência dinâmica para uso Definir endereço da seção .bss Definir endereço da seção .data Definir endereço da seção .text Ajustar endereço do nome da seção Defina o endereço do segmento de texto Configurar arquitetura Definir o tamanho da tabela de hash próximo a <NUMERO> Definir emulação Definir nome interno da biblioteca compartilhada Definir tempo de ligação da biblioteca compartilhada no caminho de busca Definir nome do arquivo de saída Ajustar o caminho de busca runtime da biblioteca compartilhada Definir endereço inicial Compatibilidade da biblioteca compartilhada de controle para HP/UX Tamanho de dados pequeno (se tamanho não for definido, o mesmo de --shared) Ordenar símbolos comuns por alinhamento [em ordem especificada] Ordenar seções por nome ou alinhamento máximo Especifique um alvo para os seguintes arquivos de entrada Especificar alvo para o arquivo de saída Dividir as seções de saída a cada CONTAGEM relocs Dividir as seções de saída a cada TAMANHO octetos Inicia um grupo Iniciar com referencia indefinida para o SIMBOLO Retirar todos os simbolos Retirar simbolos de depuração Separar símbolos em seções descartadas Emulações suportadas:  Símbolo ALVO Obter a lista de exportação de símbolos de .exports, usando
                                SIMBOLO como a versão. Este programa é software livre; você pode redistribuí-lo sob os termos da 
GNU General Public License versão 3 ou (a seu critério) uma versão mais recente.
Este programa não possui nenhuma garantia.
 Analisar arquivos abertos Rastrear menções de SYMBOL Tratar avisos como erros Desativar --whole-archive Desfazer o efeito de --export-dynamic Uso: %s [opções] arquivo...
 Use --disable-stdcall-fixup para desabilitar estes fixups
 Use --enable-stdcall-fixup para desabilitar estes avisos
 Usar o operador C++ new/delete para lista dinâmicas Usar lista dinamica de informações de tipo do C++ Usar menos memória e mais disco de E/S Usar mesmo formato que o linker nativo Use as funções empacotadas para SIMBOLOS Avisar sobre símbolos comuns duplicados Alertar se um objeto possuir um código de máquina ELF alternativo Avisar caso construtores/destrutores globais forem encontrados Avisar caso o objeto compartilhado for DT_TEXTREL Avisar se iniciou as mudanças de sessão devido o alinhamento Avisar caso os valores múltiplos do GP forem usados Avisar somente uma vez por símbolo indefinido Aviso, EXPORT duplicado: %s
 Warning: resolvendo %s vínculo para %s
 Escrever um mapa do arquivo [=CONTAGEM] [=NUMBER] [=SEÇÃO] [=TAMANHO] [=ESTILO] [=ascendente|descendente] alinhamento crescente tentativa de abrir %s falhou
 tentativa de abrir %s bem sucedida
 arquivo de script não localizado %s
 não foi possível criar um IR bfd fictício: %F%E
 decrescente nome nome|alinhamento sem símbolo arquivo de script aberto %s
 usando script linker externo: usando o script com link interno aviso:  