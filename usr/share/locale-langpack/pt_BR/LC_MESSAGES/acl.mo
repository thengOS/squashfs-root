��    5      �  G   l      �     �     �     �     �     �  )   �  )   %     O  �   d  7  0  �  h  B   J
  l  �
  �   �  Z     '   �  '        *  $   H     m     �  &   �  2   �  3   �  /   2  /   b  =   �     �  %   �  2        D  $   \  &   �  +   �  '   �  ,   �  &   )  '   P  *   x  +   �     �     �     �          #     :  &   X          �     �     �     �  �  �     �     �     �     �     �  1   �  1   +     ]  �   q    H  8  `  =   �  �  �  v   c  `   �  9   ;  ?   u  !   �  &   �     �  -     <   D  6   �  7   �  E   �  4   6  T   k     �  -   �  ;   
     F  &   _  ,   �  5   �  -   �  3      /   K   -   {   #   �   +   �      �      !     (!     <!     V!  !   t!  -   �!     �!     �!     �!     �!     "                 	              
   5                                 -           !          1      &             *      3   +      $                    )       "              %   #         ,                         '   /   .         4          0   2   (    	%s -B pathname...
 	%s -D pathname...
 	%s -R pathname...
 	%s -b acl dacl pathname...
 	%s -d dacl pathname...
 	%s -l pathname...	[not IRIX compatible]
 	%s -r pathname...	[not IRIX compatible]
 	%s acl pathname...
       --set=acl           set the ACL of file(s), replacing the current ACL
      --set-file=file     read ACL entries to set from file
      --mask              do recalculate the effective rights mask
   -R, --recursive         recurse into subdirectories
  -L, --logical           logical walk, follow symbolic links
  -P, --physical          physical walk, do not follow symbolic links
      --restore=file      restore ACLs (inverse of `getfacl -R')
      --test              test mode (ACLs are not modified)
   -a,  --access           display the file access control list only
  -d, --default           display the default access control list only
  -c, --omit-header       do not display the comment header
  -e, --all-effective     print all effective rights
  -E, --no-effective      print no effective rights
  -s, --skip-base         skip files that only have the base entries
  -R, --recursive         recurse into subdirectories
  -L, --logical           logical walk, follow symbolic links
  -P, --physical          physical walk, do not follow symbolic links
  -t, --tabular           use tabular output format
  -n, --numeric           print numeric user/group identifiers
  -p, --absolute-names    don't strip leading '/' in pathnames
   -d, --default           display the default access control list
   -m, --modify=acl        modify the current ACL(s) of file(s)
  -M, --modify-file=file  read ACL entries to modify from file
  -x, --remove=acl        remove entries from the ACL(s) of file(s)
  -X, --remove-file=file  read ACL entries to remove from file
  -b, --remove-all        remove all extended ACL entries
  -k, --remove-default    remove the default ACL
   -n, --no-mask           don't recalculate the effective rights mask
  -d, --default           operations apply to the default ACL
   -v, --version           print version and exit
  -h, --help              this help text
 %s %s -- get file access control lists
 %s %s -- set file access control lists
 %s: %s in line %d of file %s
 %s: %s in line %d of standard input
 %s: %s: %s in line %d
 %s: %s: Cannot change mode: %s
 %s: %s: Cannot change owner/group: %s
 %s: %s: Malformed access ACL `%s': %s at entry %d
 %s: %s: Malformed default ACL `%s': %s at entry %d
 %s: %s: No filename found in line %d, aborting
 %s: %s: Only directories can have default ACLs
 %s: No filename found in line %d of standard input, aborting
 %s: Option -%c incomplete
 %s: Option -%c: %s near character %d
 %s: Removing leading '/' from absolute path names
 %s: Standard input: %s
 %s: access ACL '%s': %s at entry %d
 %s: cannot get access ACL on '%s': %s
 %s: cannot get access ACL text on '%s': %s
 %s: cannot get default ACL on '%s': %s
 %s: cannot get default ACL text on '%s': %s
 %s: cannot set access acl on "%s": %s
 %s: cannot set default acl on "%s": %s
 %s: error removing access acl on "%s": %s
 %s: error removing default acl on "%s": %s
 %s: malloc failed: %s
 %s: opendir failed: %s
 Duplicate entries Invalid entry type Missing or wrong entry Multiple entries of same type Try `%s --help' for more information.
 Usage:
 Usage: %s %s
 Usage: %s [-%s] file ...
 preserving permissions for %s setting permissions for %s Project-Id-Version: acl
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-07 11:10+0000
PO-Revision-Date: 2010-04-04 03:48+0000
Last-Translator: André Gondim <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:54+0000
X-Generator: Launchpad (build 18115)
 	%s -B caminho...
 	%s -D caminho...
 	%s -R caminho...
 	%s -b acl dacl caminho...
 	%s -d dacl caminho...
 	%s -l caminho... [não é compatível com IRIX]
 	%s -r caminho... [não é compatível com IRIX]
 	%s acl caminho...
       --set=acl muda o ACL do(s) arquivo(s), substituindo o atual ACL
      --set-file=arquivo lê as entradas ACL para mudá-las a partir do arquivo
      --mask recalcula a eficiência da máscara de permissões
   -R, --recursive caminha nos subdiretórios
  -L, --logical caminho lógico, segue links simbólicos
  -P, --physical caminho físico, não segue links simbólicos
      --restore=file restaura ACLs (inverso de `getfacl -R')
      --test modo teste (ACLs não são modificadas)
   -a,  --access           exibir apenas o arquivo da lista de controle de acesso 
  -d, --default           exibir apenas a lista padrão do controle de acesso
  -c, --omit-header       não exibir o comentário do cabeçalho
  -e, --all-effective     imprimir todos os  direitos efetivos
  -E, --no-effective      não imprimir todos os direitos efetivos
  -s, --skip-base         pular arquivos que tem apenas entradas base
  -R, --recursive         recursivo nos subdiretórios
  -L, --logical           caminho lógico, seguir links simbólicos
  -P, --physical          caminho físico, não seguir lniks simbólicos
  -t, --tabular           usar tabulação no formato de saída
  -n, --numeric           imprimir identificador número de usuário/grupo
  -p, --absolute-names    não tira liderança em caminhos '/'
   -d, --default mostra a lista de controle de acesso padrão
   -m, --modify=acl modifica a(s) ACL(s) corrente(s) do(s) arquivo(s)
  -M, --modify-file=file lê as entradas de ACL do arquivo para modificação
  -x, --remove=acl remove as entradas da(s) ACL(s) do(s) arquivo(s)
  -X, --remove-file=file lê as entradas de ACL do arquivo para remoção
  -b, --remove-all remove todas as entradas extendidas da ACL
  -k, --remove-default remove a ACL padrão
   -n, --no-mask não recalcula as máscaras de direitos efetivos
  -d, --default operações aplicadas à ACL padrão
   -v, --version           imprime a versão e sai
  -h, --help              este texto de ajuda
 %s %s -- pega o arquivo das listas de controle de acesso
 %s %s -- configurar as listas de controle de acesso a arquivos
 %s: %s em linha %d do arquivo %s
 %s: %s na linha %d da entrada padrão
 %s: %s: %s em linha %d
 %s: %s: Não é possível alterar o modo: %s
 %s: %s: Não foi possível trocar o proprietário/grupo: %s
 %s: %s: Acesso ACL mal formado `%s': %s na entrada %d
 %s: %s: Padrão ACL mal formado `%s': %s na entrada %d
 %s: %s: Nenhum nome de arquivo foi encontrado na linha %d, abortando
 %s: %s: Somente diretórios podem ter ACLs padrões
 %s: Nenhum nome de arquivo foi encontrado na linha %d da entrada padrão, abortando
 %s: Opção -%c incompleta
 %s: Opção -%c: %s próxima ao caractere %d
 %s: Removendo a '/' inicial dos nomes de caminho absolutos
 %s: Entrada padrão: %s
 %s: acesso ACL '%s': %s na entrada %d
 %s: impossível obter acesso ACL em '%s':%s
 %s: impossível obter acesso ao ACL texto em '%s':%s
 %s: impossível obter ACL padrão em '%s':%s
 %s: impossível obter ACL texto padrão em '%s':%s
 %s: impossível mudar acesso ao acl em "%s":%s
 %s: impossível mudar acl padrão em "%s":%s
 %s: erro removendo acl em "%s": %s
 %s: erro removendo acl padrão em "%s": %s
 %s: malloc falhou: %s
 %s: opendir falhou: %s
 Entradas duplicadas Tipo de entrada inválido Entrada faltando ou incorreta Entradas múltiplas do mesmo tipo Tente`%s --help' para maiores informações.
 Uso:
 Uso: %s %s
 Uso: %s [-%s] arquivo ...
 preservando permissões para %s definindo permissões para %s 