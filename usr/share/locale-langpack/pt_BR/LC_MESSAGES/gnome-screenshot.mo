��    P      �  k         �  (   �     �     �          #     *     8  M   K  N   �     �       =     ]   N     �     �  >   �     	     #	  $  4	  .  Y
  *   �     �  7   �  u        x     �     �     �     �     �     �  <   �  %   .  '   T     |  -   �  ;   �               /     4  "   M     p  ,   u     �  0   �     �  %   �  
        %     6     K     f     |     �     �  '   �     �     �  $   �  '     %   G  2   m  0   �  =   �  >     ;   N     �     �  '   �     �     �     �     �     �     �       "        /  1  B  *   t     �  %   �     �     �     �  &   �  N     O   i  $   �     �  A   �  ^   1     �     �  I   �  #   �       O  ,  M  |  ,   �     �  3     q   H     �     �     �     �     �          !  :   1  "   l  "   �     �  +   �  B   �     :  +   X     �  !   �  %   �     �  +   �       4        P  4   b     �     �     �     �     �          -     A  9   a     �  	   �  *   �  *   �  *     A   >  >   �  @   �  6       E   7   0   }      �   4   �   	   �      �      �      !     
!     !     !!  ,   *!  �  W!                  C   3       '   	      K                                  I            @      
   >   N                                J   2       E                7   4       9       "          =              O       ,   F   M              P   1      (   8      G          +   :              !              ;   .   ?   H       )   6   <   /   L   %   -   D   *   &   0   5      #       $   A   B    A file named "%s" already exists in "%s" About All possible methods failed Apply _effect: Border Border Effect C_opy to Clipboard Conflicting options: --area and --delay should not be used at the same time.
 Conflicting options: --window and --area should not be used at the same time.
 Default file type extension Drop shadow Effect to add to the border (shadow, border, vintage or none) Effect to add to the outside of a border. Possible values are "shadow", "none", and "border". Effects Error creating file Error creating file. Please choose another location and retry. Error loading the help page GNOME Screenshot GNOME Screenshot allows you to take screenshots even when it's not open: just press the PrtSc button on your keyboard, and a snapshot of your whole screen will be saved to your Pictures folder. Hold Alt while pressing PrtSc and you will get a screenshot of only the currently selected window. GNOME Screenshot is a simple utility that lets you take pictures of your computer screen. Screenshots can be of your whole screen, any specific application, or a selected rectangular area. You can also copy the captured screenshot directly into the GNOME clipboard and paste it into other applications. Grab a window instead of the entire screen Grab after a _delay of Grab an area of the screen instead of the entire screen Grab just the current window, rather than the whole desktop. This key has been deprecated and it is no longer in use. Grab the current _window Grab the whole sc_reen Help Include Border Include ICC Profile Include Pointer Include _pointer Include the ICC profile of the target in the screenshot file Include the pointer in the screenshot Include the pointer with the screenshot Include the window _border Include the window border with the screenshot Include the window manager border along with the screenshot Interactively set options Last save directory None Overwrite existing file? Print version information and exit Quit Remove the window border from the screenshot Save Screenshot Save images of your screen or individual windows Save in _folder: Save screenshot directly to this file Screenshot Screenshot delay Screenshot directory Screenshot from %s - %d.%s Screenshot from %s.%s Screenshot taken Screenshot.png Select _area to grab Send the grab directly to the clipboard Take Screenshot Take _Screenshot Take a Screenshot of a Selected Area Take a Screenshot of the Current Window Take a Screenshot of the Whole Screen Take screenshot after specified delay [in seconds] The default file type extension for screenshots. The directory where the screenshots will be saved by default. The last directory a screenshot was saved in interactive mode. The number of seconds to wait before taking the screenshot. Unable to capture a screenshot Vintage Window-specific screenshot (deprecated) _Cancel _Help _Name: _Save effect filename seconds snapshot;capture;print;screenshot; translator-credits Project-Id-Version: gnome-screenshot
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screenshot&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:26+0000
PO-Revision-Date: 2015-07-10 11:23+0000
Last-Translator: Tiago Hillebrandt <tiagohillebrandt@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:14+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 Um arquivo chamado "%s" já existe em "%s" Sobre Todos os métodos possíveis falharam Aplicar _efeito: Bordar Efeito de borda _Copiar para a área de transferência Opções conflitantes: --area e --delay não devem ser usadas ao menos tempo.
 Opções conflitantes: --window e --area não devem ser usadas ao menos tempo.
 Extensão do tipo de arquivo padrão Sombra projetada Efeito para adicionar à borda (sombra, bordar, retrô ou nenhum) Efeito para adicionar uma borda externa. Valores permitidos são: "shadow", "none" e "border". Efeitos Erro criando arquivo Erro criando arquivo. Por favor, selecione outro local e tente novamente. Erro ao carregar a página de ajuda GNOME Screenshot GNOME Screenshot permite que você obtenha captura de telas mesmo quando não estiver aberto: é somente pressionar o botão PrtSc no seu teclado, e um snapshot da sua tela inteira será salvo na sua pasta Imagens. Segure o botão Alt enquanto pressiona PrtSc e você terá uma captura de tela somente da janela atualmente selecionada. GNOME Screenshot é uma simples ferramenta que o permite tirar fotos da tela de seu computador. É possível obter capturas de tela inteira, qualquer aplicativo específico, ou uma área retangular selecionada. Você pode também copiar a tela capturado diretamente da área de transferência do GNOME e colar em outras aplicações. Captura uma janela ao invés da tela inteira Capturar após _intervalo de Captura uma área da tela ao invés da tela inteira Capturar somente a janela atual, e não toda a área de trabalho. Esta chave está obsoleta e não é mais usada. Capturar a _janela atual Capturar _toda a janela Ajuda Incluir borda Incluir perfil ICC Incluir cursor Incluir cu_rsor Incluir o perfil ICC do alvo no arquivo de captura de tela Inclui o cursor na captura de tela Inclui o cursor na captura de tela Incluir _borda da janela Inclui a borda da janela na captura de tela Inclui a borda do gerenciador de janelas dentro da captura de tela Grupo de opções interativas Último diretório de captura de tela salva Nenhum Sobrescrever o arquivo existente? Exibe a informação da versão e sai Sair Remove a borda da janela na captura de tela Salvar captura de tela Salvar imagens da sua tela ou de janelas individuais Salvar na _pasta: Salva capturas de tela diretamente para este arquivo Captura de tela Atraso na captura de tela Diretório de captura de tela Captura de tela de %s - %d.%s Captura de tela de %s.%s Captura de tela tomada Captura-de-tela.png _Selecionar área para capturar Enviar a captura diretamente para área de transferência Capturar imagem da tela C_apturar Capturar a imagem de uma área selecionada Tira uma imagem de captura da janela atual Tira uma imagem de captura da tela inteira Captura imagem da tela após um atraso [em segundos] especificado A extensão do tipo de arquivo padrão para capturas de telas. Diretório em que as capturas de tela serão salvas por padrão. Diretório em que a última captura de tela foi salva. O número de segundos a aguardar antes de realizar a captura de tela. Não foi possível capturar uma imagem da janela Retrô Captura de tela de uma janela específica (obsoleto) _Cancelar _Ajuda _Nome: _Salvar efeito nome de arquivo segundos captura de tela;captura;capturar;screenshot; Alexandre Hautequest <hquest@fesppr.br>.
Ariel Bressan da Silva <ariel@conectiva.com.br>.
Francisco Petrúcio Cavalcante Junior <fpcj@impa.br>.
Evandro Fernandes Giovanini <evandrofg@ig.com.br>.
Welther José O. Esteves <weltherjoe@yahoo.com.br>.
Afonso Celso Medina <medina@maua.br>.
Guilherme de S. Pastore <gpastore@colband.com.br>.
Luiz Fernando S. Armesto <luiz.armesto@gmail.com>.
Leonardo Ferreira Fontenelle <leonardof@gnome.org>.
Og Maciel <ogmaciel@gnome.org>.
Rodrigo Flores <rodrigomarquesflores@gmail.com>.
Hugo Doria <hugodoria@gmail.com>.
Djavan Fagundes <dnoway@gmail.com>.
Krix Apolinário <krixapolinario@gmail.com>.
Antonio Fernandes C. Neto <fernandesn@gnome.org>.
Rodrigo Padula de Oliveira <contato@rodrigopadula.com>.
André Gondim (In memoriam) <In memoriam>.
Enrico Nicoletto <liverig@gmail.com>.
Rafael Ferreira <rafael.f.f1@gmail.com>.

Launchpad Contributions:
  Antonio Fernandes C. Neto https://launchpad.net/~fernandesn
  Enrico Nicoletto https://launchpad.net/~liverig
  Hriostat https://launchpad.net/~hriostat
  Neliton Pereira Jr. https://launchpad.net/~nelitonpjr
  Pablo Diego Moço https://launchpad.net/~pablodm89
  Rafael Fontenelle https://launchpad.net/~rffontenelle
  Rafael Neri https://launchpad.net/~rafepel
  Renato Krupa https://launchpad.net/~renatokrupa
  Ricardo Barbosa https://launchpad.net/~barbosa-cisco
  Tiago Hillebrandt https://launchpad.net/~tiagohillebrandt
  gabriell nascimento https://launchpad.net/~gabriellhrn 