��          t      �              +   1  #   ]  6   �     �     �  0   �  /   $  )   T  x   ~  �  �  /   �  F   �  +   +  N   W     �  (   �  .   �  ;     -   Y  �   �                                 
      	                     Could not create LUKS device %s Could not format device with file system %s Creating encrypted device on %s...
 Error: could not generate temporary mapped device name Error: device mounted: %s
 Error: invalid file system: %s
 Please enter your passphrase again to verify it
 The passphrases you entered were not identical
 This program needs to be started as root
 luksformat - Create and format an encrypted LUKS device
Usage: luksformat [-t <file system>] <device> [ mkfs options ]

 Project-Id-Version: cryptsetup
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-03 20:26+0100
PO-Revision-Date: 2012-09-25 18:20+0000
Last-Translator: Julian Fernandes <julian@copyblogger.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:25+0000
X-Generator: Launchpad (build 18115)
 Não foi possível criar um dispositivo LUKS %s Não foi possível formatar o dispositivo com o sistema de arquivos %s Criando dispositivo criptografado em %s...
 Error: não foi possível gerar um nome de dispositivo mapeado temporariamente Erro: dispositivo montado: %s
 Erro: sistema de arquivos inválido: %s
 Digite sua senha novamente para verificação
 As senhas não são idênticas, por favor, tente novamente
 Este programa precisa ser iniciado como root
 luksformat - Cria e formata um dispositivo LUKS criptografado
Uso: luksformat [-t <sistema de arquivos>] <dispositivo> [ opções do mkfs ]

 