��         �  o  �      �     �     �     �     �     �                :     V     g     m     z  
   �     �     �     �  $   �  �   �     l  $   ~  #   �     �  -   �            #   /     S     r     �  /   �  *   �            ;     T   S  !   �     �     �  4   �  A        Z  /   q  #   �     �     �     �     �     
  0   &     W  -   m  .   �  0   �     �          "     @     R     g     z     �  3   �  !   �  5         9     Z  1   t  %   �     �     �  #        &     ?     ]  &   s     �  $   �     �  :   �  +   #      O      h      �   "   �      �   7   �      !     /!     L!  $   f!     �!  '   �!     �!     �!     �!  $   "     3"     S"     b"     t"     �"     �"     �"  
   �"  
   �"  x   �"  @   X#  *   �#  1   �#  ,   �#  '   #$     K$  @   Z$  &   �$  ,   �$  I   �$  .   9%  ,   h%  
   �%  1   �%     �%  8   �%  5   )&     _&  �   a&  O   5'     �'  $   �'  !   �'  (   �'  Z   (     n(  $   �(  #   �(  %   �(     �(  -   )  �   :)  (   �)  *   *     ;*     [*  3   j*     �*  /   �*     �*  B   �*  l   5+     �+     �+     �+  
   �+     �+     �+  ?   ,  (   O,     x,      �,  8   �,     �,  +   -     --  $   ;-  4   `-     �-  &   �-     �-  -   �-  1   .     N.  =   b.  B   �.  &   �.  1   
/  B   </     /  (   �/  �   �/     �0     �0     �0      �0  5   1  4   H1  ,   }1  -   �1  ,   �1  <   2  �   B2  �   3  +   �3  /   �3  *   4  '   94  (   a4  W   �4  '   �4  .   
5  %   95     _5  )   ~5  U   �5  C   �5     B6     ^6     x6     �6     �6     �6     �6     �6     7     "7     >7  ;   Y7     �7     �7     �7     �7  #   8  "   (8  !   K8  &   m8  R   �8  &   �8  1   9     @9     R9  %   n9  "   �9     �9  $   �9     �9  ;   
:      F:     g:     {:  V   �:  !   �:     ;  ,   1;  ,   ^;  ~   �;  8   
<     C<  <   W<     �<     �<     �<     �<  [   �<  '   =  <   E=  <   �=  )   �=  ,   �=     >     ">     (>  I   .>     x>     �>     �>  #   �>     �>      ?  0   ?  E   I?  7   �?    �?     �A     �A     B     B     .B     BB     `B     {B     �B     �B     �B     �B     �B     �B     �B     �B  5   C  �   >C     �C  .   �C  7   D     PD  4   eD  +   �D     �D  ,   �D  *   E  +   9E  &   eE  ;   �E  7   �E      F     	F  ?   F  e   [F  #   �F     �F     �F  C   G  K   LG     �G  1   �G  $   �G     H     ,H     FH     _H  %   qH  g   �H     �H  6   I  0   UI  ;   �I     �I     �I  !   �I     
J     J     +J     <J  ,   XJ  @   �J  .   �J  ;   �J  *   1K  "   \K  ;   K  .   �K  "   �K      L  3   .L  "   bL  #   �L     �L  3   �L     �L  *   M     7M  ?   MM  :   �M     �M     �M  )   �M  1   'N     YN  6   pN     �N  &   �N     �N  +   
O     6O  *   =O     hO  !   �O     �O  4   �O  '   �O     P     9P     VP     fP     �P     �P     �P     �P  {   �P  Q   RQ  -   �Q  ;   �Q  )   R  *   8R     cR  @   pR  (   �R  8   �R  _   S  4   sS  +   �S     �S  :   �S  "   T  \   BT  C   �T     �T  �   �T  T   �U  #   V  %   ;V  '   aV  )   �V  n   �V     "W  7   ;W  +   sW  *   �W     �W  0   �W  �   X  ;   �X  5   Y  /   NY     ~Y  B   �Y     �Y  2   �Y     "Z  H   3Z  �   |Z  "   [     6[     O[     X[     h[  "   ~[  A   �[  3   �[     \  "   &\  =   I\     �\  /   �\     �\  (   �\  4   ]     E]  *   `]  '   �]  C   �]  ;   �]     3^  J   K^  N   �^  !   �^  >   _  D   F_  "   �_  ,   �_  �   �_     �`  #   �`     a  !   1a  ?   Sa  >   �a  :   �a  ,   b  -   :b  <   hb  �   �b  �   �c  <   -d  6   jd  &   �d  %   �d  '   �d  f   e  *   }e  )   �e  %   �e  #   �e  4   f  Y   Qf  I   �f  "   �f  !   g     :g  !   Sg     ug     �g     �g     �g     �g  %   h  !   ,h  D   Nh     �h     �h     �h  )   �h  -   i  #   Bi  &   fi  .   �i  f   �i  4   #j  R   Xj     �j  &   �j  8   �j  9    k     Zk  ,   mk  !   �k  F   �k  -   l     1l  $   Nl  j   sl  ,   �l  )   m  ,   5m  4   bm  �   �m  7   )n     an  K   |n     �n     �n     �n     �n  c   o  +   fo  B   �o  B   �o  2   p  9   Kp     �p     �p     �p  G   �p     �p     �p     q     5q     Nq     fq  7   q  M   �q  9   r     �   -   �       �   3          b       �   �   0       �   $   �   V   _   �              Q   �   o                   
   �   �   S   �   �   �      �   O      W   �   �       r                       N   �     =       �      )   1   5   �   ^   �            x   �   w   Z   �   d       R   �   s   e      "   q   L   �                  M   !   �   �   C   X       �      \   K   �   �       z   >   �   `       (   �   �       [   �       @   �   �          �   �                  :   7   �   �   c   �   D       �   i     �   *       �          2       �   �   �   �                  �       9   P   }   p   u       �       �   {           �   ;       �       '          I   ?   �       �   �   �         ,   /       �         �                 �   �       T   �      �   B       �   F   A   	   �       �   �       �   �   �   �   �       �   v           m   �       �     �          8   �      �   �      �                   h       �         �   �   �   �       �       %                  ~   �   �       U   &         +   Y   .   t   �      6       <      �   �   �   �     �   �           
  �       �      l      �       �      �   �           �      �   �       �   �           �   �       �       j      �   y   E     �       �   �   ]   n   �   	  g   f           �   �       |              �   k           �   �   a   H   �              �   �       �   G   4   �   �       �   �   J   #       �      Candidate:    Installed:    Missing:    Mixed virtual packages:    Normal packages:    Package pin:    Pure virtual packages:    Single virtual packages:    Version table:  Done  [Installed]  [Not candidate version]  [Working]  failed.  or %lu downgraded,  %lu not fully installed or removed.
 %lu package was automatically installed and is no longer required.
 %lu packages were automatically installed and are no longer required.
 %lu reinstalled,  %lu to remove and %lu not upgraded.
 %lu upgraded, %lu newly installed,  %s (due to %s) %s can not be marked as it is not installed.
 %s has no build depends.
 %s set on hold.
 %s set to automatically installed.
 %s set to manually installed.
 %s was already not hold.
 %s was already set on hold.
 %s was already set to automatically installed.
 %s was already set to manually installed.
 (none) (not found) --fix-missing and media swapping is not currently supported A proxy server was specified but no login script, Acquire::ftp::ProxyLogin is empty. A response overflowed the buffer. Abort. Aborting install. After this operation, %sB disk space will be freed.
 After this operation, %sB of additional disk space will be used.
 Arguments not in pairs At least one invalid signature was encountered. Authentication warning overridden.
 Bad default setting! Bad header data Bad header line Broken packages Build command '%s' failed.
 Cache is out of sync, can't x-ref a package file Canceled hold on %s.
 Cannot initiate the connection to %s:%s (%s). Check if the 'dpkg-dev' package is installed.
 Configure build-dependencies for source packages Connecting to %s Connecting to %s (%s) Connection closed prematurely Connection failed Connection timed out Connection timeout Correcting dependencies... Could not bind a socket Could not connect data socket, connection timed out Could not connect passive socket. Could not connect to %s:%s (%s), connection timed out Could not connect to %s:%s (%s). Could not create a socket Could not create a socket for %s (f=%u t=%u p=%u) Could not determine the socket's name Could not listen on the socket Could not resolve '%s' Couldn't determine free space in %s Couldn't find package %s Data socket connect timed out Data socket timed out Data transfer failed, server said '%s' Disk not found. Distribution upgrade, see apt-get(8) Do you want to continue? Do you want to erase any previously downloaded .deb files? Download complete and in download only mode Download source archives EPRT failed, server said: %s Erase downloaded archive files Erase old downloaded archive files Error reading from server Error reading from server. Remote end closed connection Error writing to file Error writing to output file Error writing to the file Executing dpkg failed. Are you root? Failed Failed to create IPC pipe to subprocess Failed to fetch %s  %s
 Failed to fetch some archives. Failed to mount '%s' to '%s' Failed to process build dependencies Failed to set modification time Failed to stat Failed to stat %s Fetch source %s
 Fetched %sB in %s (%sB/s)
 File not found Follow dselect selections Get:%lu %s Hit:%lu %s Hmm, seems like the AutoRemover destroyed something which really
shouldn't happen. Please file a bug report against apt. How odd... The sizes didn't match, email apt@packages.debian.org However the following packages replace it: Install new packages (pkg is libc6 not libc6.deb) Install these packages without verification? Internal Error, AutoRemover broke stuff Internal error Internal error, InstallPackages was called with broken packages! Internal error, Ordering didn't finish Internal error, problem resolver broke stuff Internal error: Good signature, but could not determine key fingerprint?! Invalid URI, local URIS must not start with // List the names of all packages in the system Logging in Login script command '%s' failed, server said: %s Merging available information Must specify at least one package to check builddeps for Must specify at least one package to fetch source for N NOTE: This is only a simulation!
      apt-get needs root privileges for real execution.
      Keep also in mind that locking is deactivated,
      so don't depend on the relevance to the real current situation! NOTICE: '%s' packaging is maintained in the '%s' version control system at:
%s
 Need to get %sB of archives.
 Need to get %sB of source archives.
 Need to get %sB/%sB of archives.
 Need to get %sB/%sB of source archives.
 No architecture information available for %s. See apt.conf(5) APT::Architectures for setup No packages found Note, selecting '%s' for regex '%s'
 Note, selecting '%s' for task '%s'
 Note, selecting '%s' instead of '%s'
 PASS failed, server said: %s Package %s is a virtual package provided by:
 Package %s is not available, but is referred to by another package.
This may mean that the package is missing, has been obsoleted, or
is only available from another source
 Package %s version %s has an unmet dep:
 Package '%s' has no installation candidate Package file %s is out of sync. Package files: Packages need to be removed but remove is disabled. Perform an upgrade Picking '%s' as source package instead of '%s'
 Pinned packages: Please provide a name for this Disc, such as 'Debian 5.0.3 Disk 1' Please use apt-cdrom to make this CD-ROM recognized by APT. apt-get update cannot be used to add new CD-ROMs Problem hashing file Protocol corruption Query Read error Recommended packages: Regex compilation error - %s Reinstallation of %s is not possible, it cannot be downloaded.
 Remove automatically all unused packages Remove packages Remove packages and config files Repeat this process for the rest of the CDs in your set. Retrieve new lists of packages Search the package list for a regex pattern Select failed Selected version '%s' (%s) for '%s'
 Selected version '%s' (%s) for '%s' because of '%s'
 Server closed the connection Show a readable record for the package Show policy settings Show raw dependency information for a package Show reverse dependency information for a package Show source records Skipping %s, it is already installed and upgrade is not set.
 Skipping %s, it is not installed and only upgrades are requested.
 Skipping already downloaded file '%s'
 Skipping unpack of already unpacked source in %s
 Some errors occurred while unpacking. Packages that were installed Some files failed to download Some packages could not be authenticated Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming. Suggested packages: Supported modules: TYPE failed, server said: %s Temporary failure resolving '%s' The HTTP server sent an invalid Content-Length header The HTTP server sent an invalid Content-Range header The HTTP server sent an invalid reply header The following NEW packages will be installed: The following held packages will be changed: The following information may help to resolve the situation: The following package disappeared from your system as
all files have been overwritten by other packages: The following packages disappeared from your system as
all files have been overwritten by other packages: The following package was automatically installed and is no longer required: The following packages were automatically installed and are no longer required: The following packages have been kept back: The following packages have unmet dependencies: The following packages will be DOWNGRADED: The following packages will be REMOVED: The following packages will be upgraded: The following signatures couldn't be verified because the public key is not available:
 The following signatures were invalid:
 The server refused the connection and said: %s The update command takes no arguments This APT has Super Cow Powers. This HTTP server has broken range support This command is deprecated. Please use 'apt-mark auto' and 'apt-mark manual' instead. This command is deprecated. Please use 'apt-mark showauto' instead. Total Desc/File relations:  Total Provides mappings:  Total dependencies:  Total distinct descriptions:  Total distinct versions:  Total globbed strings:  Total package names:  Total package structures:  Total slack space:  Total space accounted for:  Total ver/file relations:  Trivial Only specified but this is not a trivial operation. USER failed, server said: %s Unable to accept connection Unable to change to %s Unable to correct dependencies Unable to correct missing packages. Unable to determine the local name Unable to determine the peer name Unable to fetch file, server said '%s' Unable to fetch some archives, maybe run apt-get update or try with --fix-missing? Unable to find a source package for %s Unable to get build-dependency information for %s Unable to invoke  Unable to locate package %s Unable to lock the download directory Unable to minimize the upgrade set Unable to read %s Unable to read the cdrom database %s Unable to send PORT command Unable to unmount the CD-ROM in %s, it may still be in use. Unknown address family %u (AF_*) Unknown date format Unknown error executing apt-key Unmet dependencies. Try 'apt-get -f install' with no packages (or specify a solution). Unmet dependencies. Try using -f. Unpack command '%s' failed.
 Verify that there are no broken dependencies Virtual packages like '%s' can't be removed
 WARNING: The following essential packages will be removed.
This should NOT be done unless you know exactly what you are doing! WARNING: The following packages cannot be authenticated! Waiting for headers We are not supposed to delete stuff, can't start AutoRemover Write error Wrong CD-ROM Y Yes, do as I say! You are about to do something potentially harmful.
To continue type in the phrase '%s'
 ?]  You don't have enough free space in %s. You might want to run 'apt-get -f install' to correct these. You might want to run 'apt-get -f install' to correct these: You must give at least one search pattern You should explicitly select one to install. [IP: %s %s] [Y/n] [y/N] above this message are important. Please fix them and run [I]nstall again but %s is installed but %s is to be installed but it is a virtual package but it is not going to be installed but it is not installable but it is not installed getaddrinfo was unable to get a listening socket or errors caused by missing dependencies. This is OK, only the errors will be configured. This may result in duplicate errors Project-Id-Version: apt
Report-Msgid-Bugs-To: APT Development Team <deity@lists.debian.org>
POT-Creation-Date: 2016-03-25 09:30+0100
PO-Revision-Date: 2016-06-01 22:43+0000
Last-Translator: Felipe Augusto van de Wiel (faw) <Unknown>
Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 15:57+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
   Candidato:    Instalado:    Faltando:    Pacotes virtuais misturados:    Pacotes normais:    Pacote alfinetado ("pin"):    Pacotes puros virtuais:    Pacotes virtuais únicos:    Tabela de versão:  Pronto  [Instalado]  [Sem versão candidata]  [Trabalhando]  falhou.  ou %lu revertidos,  %lu pacotes não totalmente instalados ou removidos.
 O pacote %lu foi instalado automaticamente e não é mais necessário:
 Os pacotes %lu foram instalados automaticamente e não são mais necessários:
 %lu reinstalados,  %lu a serem removidos e %lu não atualizados.
 %lu pacotes atualizados, %lu pacotes novos instalados,  %s (por causa de %s) %s não pode ser marcado pois não está instalado.
 %s não tem dependências de construção.
 %s definido com suspenso.
 %s definido para instalado automaticamente.
 %s configurado para instalar manualmente.
 %s já estava definido como não-suspenso.
 %s já estava definido como suspenso.
 %s já estava definido para ser instalado automaticamente.
 %s já estava definido para ser instalado manualmente.
 (nenhum) (não encontrado) --fix-missing e troca de mídia não são suportados atualmente Um servidor proxy foi especificado mas não um script de login, Acquire::ftp::ProxyLogin está vazio. Uma resposta sobrecarregou o buffer Abortar. Abortando a instalação. Depois desta operação, %sB de espaço em disco serão liberados.
 Depois desta operação, %sB adicionais de espaço em disco serão usados.
 Argumentos não estão em pares Ao menos uma assinatura inválida foi encontrada. Aviso de autenticação sobreposto.
 Configuração padrão ruim! Dados de cabeçalho ruins Linha de cabeçalho ruim Pacotes quebrados Comando de construção '%s' falhou.
 O cache está fora de sincronia, não foi possível fazer a referência cruzada de um arquivo de pacote Cancelada a suspensão de %s.
 Não foi possível iniciar a conexão para %s:%s (%s). Confira se o pacote 'dpkg-dev' está instalado.
 Configura as dependências de compilação de pacotes fonte Conectando a %s Conectando em %s (%s) Conexão encerrada prematuramente Conexão falhou Conexão expirou Conexão expirou Corrigindo dependências... Não foi possível fazer "bind" de um socket Não foi possível conectar um socket de dados, conexão expirou Não foi possível conectar um socket passivo. Não foi possível conectar em %s:%s (%s), conexão expirou Não foi possível conectar em %s:%s (%s). Não foi possível criar um socket Não foi possível criar um socket para %s (f=%u t=%u p=%u) Não foi possível determinar o nome do socket Não foi possível ouvir no socket Não foi possível resolver '%s' Não foi possível determinar o espaço livre em %s Não foi possível achar pacote %s Conexão do socket de dados expirou Socket de dados expirou Transferência de dados falhou, servidor disse '%s' Disco não encontrado. Atualiza a distribuição, veja apt-get(8) Você quer continuar? Você quer apagar quaisquer arquivos .deb previamente baixados? Baixar completo e no modo somente baixar ("download only") Baixa arquivos fonte EPRT falhou, servidor disse: %s Apaga arquivos baixados para instalação Apaga arquivos antigos baixados para instalação Erro lendo do servidor Erro lendo do servidor. Ponto remoto fechou a conexão Erro escrevendo para arquivo Erro escrevendo para arquivo de saída Erro escrevendo para o arquivo Execução do dpkg falhou. Você é o root? Falhou Falhou ao criar pipe IPC para sub-processo Falhou ao buscar %s  %s
 Falhou ao buscar alguns arquivos. Falha ao montar '%s' para '%s' Falhou ao processar as dependências de construção Falhou ao definir hora de modificação Falhou ao executar "stat" Falhou ao executar "stat" %s Obter fonte %s
 Baixados %sB em %s (%sB/s)
 Arquivo não encontrado Segue as seleções do dselect Obter:%lu %s Atingido:%lu %s Hmm, parece que o AutoRemover destruiu algo o que realmente não deveria
acontecer. Por favor, reporte um bug contra o apt. Que estranho... Os tamanhos não batem, mande e-mail para apt@packages.debian.org No entanto, os pacotes a seguir o substituem: Instala novos pacotes (um pacote é libc6 e não libc6.deb) Instalar estes pacotes sem verificação? Erro Interno, o AutoRemover quebrou coisas Erro interno Erro interno, InstallPackages foi chamado com pacotes quebrados! Erro interno, Ordenação não finalizou Erro interno, o solucionador de problemas quebrou coisas Erro interno: Assinatura boa, mas não foi possível determinar a impressão digital da chave?! URI inválida, URIs locais não devem iniciar com // Lista o nome de todos os pacotes no sistema Efetuando login Comando de script de login '%s' falhou, servidor disse: %s Mesclando informação disponível Deve-se especificar pelo menos um pacote para que se cheque as dependências de construção Deve-se especificar pelo menos um pacote para que se busque o fonte N NOTA: Esta é apenas uma simulação!
O apt-get precisa de privilégios de root para uma execução real.
Mantenha em mente que a trava está desativada,
portanto não dependa da relevância para a situação real atual! OBSERVAÇÃO: \"%s\" pacote mantido na versão \"%s\" do sistema de controle em:
%s
 É preciso baixar %sB de arquivos.
 Preciso obter %sB de arquivos fonte.
 É preciso baixar %sB/%sB de arquivos.
 Preciso obter %sB/%sB de arquivos fonte.
 Nenhum informação de arquitetura disponível for %s. Veja apt.conf(5) APT:: Arquiteturas para configuração Nenhum pacote encontrado Nota, selecionando '%s' para a expressão regular '%s'
 Nota, selecionando '%s' para a tarefa '%s'
 Nota, selecionando '%s' ao invés de '%s'
 PASS falhou, servidor disse: %s O pacote %s é um pacote virtual fornecido por:
 O pacote %s não está disponível, mas é referenciado por outro pacote.
Isto pode significar que o pacote está faltando, ficou obsoleto ou
está disponível somente a partir de outra fonte
 O pacote %s versão %s tem uma dependência desencontrada:
 O pacote '%s' não possui candidato para instalação O arquivo de pacote %s está fora de sincronia. Arquivos de pacote: Pacotes precisam ser removidos mas a remoção está desabilitada. Realiza uma atualização Escolhendo '%s' como pacote fonte, em vez de '%s'
 Pacotes fixados: Por favor, forneça um nome para este disco, como 'Debian 5.0.3 Disco 1' Por favor, use o apt-cdrom para fazer com que este CD-ROM seja reconhecido pelo APT. O apt-get update não pode ser usado para adicionar novos CD-ROMs Problema criando o hash do arquivo Corrupção de protocolo Pesquisa Erro de leitura Pacotes recomendados: Erro de compilação de regex - %s A reinstalação de %s não é possível, não pode ser baixado.
 Remove automaticamente todos os pacotes não usados Remove pacotes Remove e expurga ("purge") pacotes Repita este processo para o restante dos CDs em seu conjunto. Obtém novas listas de pacotes Procura a lista de pacotes por um padrão regex Seleção falhou Versão selecionada '%s' (%s) para '%s'
 Versão selecionada '%s' (%s) para '%s' porque '%s'
 Servidor fechou a conexão Mostra um registro legível sobre o pacote Mostra as configurações de políticas Mostra informações de dependências não processadas de um pacote Mostra informações de dependências reversas de um pacote Mostra registros fontes Pulando %s, já está instalado e a atualização não está configurada.
 Pulando %s, não está instalado e foram necessárias somente atualizações.
 Pulando arquivo já baixado '%s'
 Pulando o desempacotamento de fontes já desempacotados em %s
 Ocorreram alguns erros ao descompactar. Pacotes que foram instalados Alguns arquivos falharam ao baixar Alguns pacotes não puderam ser autenticados Alguns pacotes não puderam ser instalados. Isto pode significar que
você solicitou uma situação impossível ou, se você está usando a
distribuição instável, que alguns pacotes requeridos não foram
criados ainda ou foram retirados da "Incoming". Pacotes sugeridos: Módulos para os quais há suporte: TYPE falhou, servidor disse: %s Falha temporária resolvendo '%s' O servidor HTTP enviou um cabeçalho "Content-Length" inválido O servidor HTTP enviou um cabeçalho "Content-Range" inválido O servidor HTTP enviou um cabeçalho de resposta inválido Os NOVOS pacotes a seguir serão instalados: Os seguintes pacotes mantidos serão mudados: A informação a seguir pode ajudar a resolver a situação: O seguinte pacote desapareceu de seu sistema assim como
todos os arquivos foram sobrescritos por outros pacotes: Os seguintes pacotes desapareceram de seu sistema assim como
todos os arquivos foram sobrescritos por outros pacotes: O seguinte pacote foi automaticamente instalado e não é mais necessário: Os seguintes pacotes foram automaticamente instalados e não são mais necessários: Os pacotes a seguir serão mantidos em suas versões atuais: Os pacotes a seguir têm dependências desencontradas: Os pacotes a seguir serão REVERTIDOS: Os pacotes a seguir serão REMOVIDOS: Os pacotes a seguir serão atualizados: As assinaturas a seguir não puderam ser verificadas devido à chave pública não estar disponível:
 As seguintes assinaturas eram inválidas:
 O servidor recusou a conexão e disse: %s O comando update não leva argumentos Este APT tem Poderes de Super Vaca. Este servidor HTTP possui suporte a "range" quebrado Este comando é obsoleto. Por favor use 'apt-mark auto' e 'apt-mark manual' em vez disso. Este comando é obsoleto. Por favor use 'apt-mark showauto' em vez disso. Total de relações Desc/Arquivo:  Total de mapeamentos "Provides":  Total de dependências:  Total de descrições distintas:  Total de versões distintas:  Total de strings "globbed":  Total de Nomes de Pacotes:  Total de estruturas de pacote:  Total de espaço frouxo:  Total de espaço contabilizado para:  Total de relações ver/arquivo:  "Trivial Only" especificado mas esta não é uma operação trivial. USER falhou, servidor disse: %s Impossível aceitar conexão Impossível mudar para %s Não foi possível corrigir dependências Não foi possível corrigir pacotes ausentes. Impossível determinar o nome local Impossível determinar o nome do ponto Impossível obter arquivo, servidor disse '%s' Não foi possível buscar alguns arquivos, talvez executar apt-get update ou tentar com --fix-missing? Não foi possível encontrar um pacote fonte para %s Não foi possível conseguir informações de dependência de construção para %s Impossível invocar  Não foi possível localizar pacote %s Não foi possível criar trava no diretório de download Não foi possível minimizar o conjunto de atualizações Impossível ler %s Impossível ler o banco de dados de cdrom %s Impossível enviar o comando PORT Impossível desmontar o CD-ROM em %s, o mesmo ainda pode estar em uso. Família de endereços %u desconhecida (AF_*) Formato de data desconhecido Erro desconhecido executando apt-key Dependências desencontradas. Tente 'apt-get -f install' sem nenhum pacote (ou especifique uma solução). Dependências desencontradas. Tente usar -f. Comando de desempacotamento '%s' falhou.
 Verifica se não há dependências quebradas Pacotes virtuais como '%s' não podem ser removidos
 AVISO: Os pacotes essenciais a seguir serão removidos.
Isso NÃO deveria ser feito a menos que você saiba exatamente o que você está fazendo! AVISO: Os pacotes a seguir não podem ser autenticados! Aguardando por cabeçalhos Nós não deveríamos apagar coisas, não foi possível iniciar AutoRemover Erro de escrita CD-ROM errado S Sim, faça o que eu digo! Você está prestes a fazer algo potencialmente destrutivo.
Para continuar digite a frase '%s'
 ?]  Você não possui espaço suficiente em %s. Você pode querer executar 'apt-get -f install' para corrigí-los. Você deve querer executar 'apt-get -f install' para corrigí-los: Você deve fornecer pelo menos um padrão de busca Você deveria selecionar explicitamente um para instalar. [IP: %s %s] [S/n] [s/N] são importantes. Por favor, conserte-os e execute [I]nstalar novamente mas %s está instalado mas %s está para ser instalado mas é um pacote virtual mas não será instalado mas não é instalável mas não está instalado getaddrinfo não foi capaz de obter um socket de escuta dependências faltantes. Isto está OK, somente os erros acima desta mensagem será configurado. Isso pode resultar em erros duplicados 