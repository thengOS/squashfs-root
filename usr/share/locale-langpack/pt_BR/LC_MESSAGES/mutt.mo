��    R     �"  �  <E      `\     a\     s\     �\  '   �\  $   �\     �\     ]  
   !]  �  ,]  �   (_  
   `  �  `  2   �a  �  b     �c     �c  	   �c     �c     �c     �c      d     d  
   $d     /d  7   7d     od     �d     �d     �d  6   �d     e     3e     <e  %   Ee  #   ke     �e     �e  ,   �e     �e     f     .f     If     cf     zf     �f  	   �f     �f     �f     �f     �f     �f     g  6   +g     bg     {g     �g     �g     �g     �g     �g     �g     h     !h     7h     Qh     mh  )   �h     �h     �h     �h     �h      i  +   ,i     Xi     di  '   mi     �i     �i  (   �i     �i     �i  *   j     <j     Rj     hj     kj     zj     �j  $   �j  #   �j     �j      k     "k  !   9k     [k     _k  	   ck  	   mk  !   wk     �k     �k     �k     �k     �k     	l  	   &l     0l  
   =l     Hl  7   Pl  4   �l  -   �l      �l     m     m     2m  "   Im     lm     xm     �m     �m     �m     �m     �m     �m     n     /n     Hn     fn     �n  '   �n     �n     �n     �n  !   �n     o     "o     1o     Mo     bo     vo     �o     �o     �o     �o     �o     p      p     5p     Jp     ^p     zp  B   �p  >   �p      q  (   9q     bq     uq  !   �q     �q  #   �q     �q     r      r     ;r     Wr  "   ur  *   �r     �r  1   �r     s  %   s     Ds  &   Xs  )   s     �s     �s     �s  *   �s      t     8t  !   Wt     yt     �t  #   �t  1   �t  &   �t  #   !u      Eu     fu  
   lu     wu     �u  =   �u  
   �u     �u  #   v     )v  '   <v  (   dv  (   �v  	   �v     �v     �v     �v     w  )   w     Hw     `w  $   |w  	   �w     �w     �w     �w     �w     x  .  #x  �   Rz     C{     a{  "   x{      �{     �{  2   �{     |     *|  )   J|  "   t|     �|     �|     �|  !   �|     }  
   }  +   }     J}  4   [}     �}     �}     �}     �}     �}     ~     $~     6~     I~     M~      T~  +   u~     �~     �~  ?   �~          !     ?     ]     u     �     �      �     �     �     �     �     �      +�     L�     d�     }�     ��     ��     ր     �     
�  *   "�     M�     j�     ��  !   ��     ��     ҁ     �  !   ��     �     5�  ,   Q�  (   ~�     ��  -   ��  %   �  &   �     9�     R�     l�  $   ��  9   ��     �  3   �     6�  *   O�  (   z�     ��     ��  +   ڄ  %   �     ,�     L�  )   `�     ��     ��     ��  
   ��  
   ��     ƅ  !   Յ     ��  ,   �     @�     ^�  &   v�  &   ��     Ć  '   ܆     �     �     5�     Q�     e�  0   q�  #   ��  8   Ƈ     ��     �     3�     D�     R�  -   b�     ��     ��     ��     Ո  .   �     �     :�     Q�     f�  %   l�     ��     ��     ��       (   �  	   �     �     0�     P�     a�     ~�     ��  6   ��     �     ��     �      �  *   ?�  *   j�  5   ��  
   ˋ     ֋     �      �     �     +�     A�     Y�     k�     ��  !   ��     ��     ό     �      �     �  	    �  '   *�     R�     _�     |�     ��  '   ��     č     �  	    �  (   
�     3�     O�     ]�  !   k�     ��     ��  /   ��     �     ��     ��  
   �     �     ,�     ;�     L�     ]�     q�      ��     ��     ��     Џ     �     ��     �      '�  5   H�     ~�     ��  "   ��  
   А     ې     ��     �     �  8   ,�     e�     x�     ��     ��     ��     ב     �     ��     �     �     ;�     Q�     b�  ,   u�  +   ��     Β     �     �  	   �     �  
   .�     9�     F�     `�     e�  $   l�  .   ��     ��  0   ܓ     �     �     6�     L�     k�     ��     ��     ��     Δ     ۔  5   ��     ,�     I�     c�  2   {�     ��     ʕ     �     ��  (   �     7�     S�     c�     }�  %   ��     ��     ז     �     �     %�     @�     S�     i�     x�     ��     ��     ��     З     �     ��     �     �  +   0�  
   \�     g�     u�     ��  8   ��  4   ��     ��     �  #   !�     E�     T�  A   s�  6   ��     �  )   ��     "�     ?�     Q�     i�  #   ��     ��  $   ��  $   �  
   	�  "   �     7�     P�      j�  3   ��     ��     ؛     �     ��     �  	   �     �  H   8�     ��     ��     ��     Ɯ     �     �     	�     �     !�     0�     L�     c�     {�     ��     ��  
   ��     ��     ܝ     �  
   �     ��  "   �     %�     A�  '   [�     ��  +   ��     ��     ؞     �     ��     ��  S   �     b�  9   w�  
   ��  I   ��  ,   �  /   3�  "   c�     ��  9   ��  '   ՠ  '   ��     %�     @�     \�  !   q�  +   ��     ��     ס  &   ��      �  5   ?�  $   u�     ��     ��  &   ��     �     �     �     �     .�  "   @�  	   c�     m�     |�     ��  '   ��  $   ��     ݣ  (   �     �     4�     K�     X�     t�     {�     ��  $   ��  (   ¤     �     ��      �     �     *�     =�  #   \�     ��     ��     ��     ��  	   ��     ¥  O   Х  1    �     R�     e�     w�     ��     ��     ��  $   Ԧ     ��     �     0�  )   J�  *   t�  :   ��  $   ڧ     ��     �     0�  8   O�     ��     ��     ��  1   ߨ     �  8   �      X�     y�     ��  -   ��  -   Щ  t   ��  %   s�     ��     ��  
   ͪ     ت  )   ��     !�  #   @�     d�     y�  6   ��  #   «  #   �     
�     "�     A�     G�     d�  
   l�     w�     ��     ��     ��     Ҭ  
   �     ��     �  &   '�     N�     g�     x�     ��  
   ��     ��     ��     ح  2   �  S   �  4   m�  ,   ��  '   Ϯ  ,   ��  3   $�  1   X�  D   ��  Z   ϯ     *�     G�     g�     �  4   ��  %   а  "   ��  *   �  2   D�  B   w�  :   ��  #   ��  /   �  )   I�  4   s�  *   ��     Ӳ     �     ��     �  1   !�  2   S�  1   ��     ��     Գ     �     �     *�     E�     b�     |�     ��     ��  '   ϴ  )   ��     !�     3�     P�     j�     }�     ��     ��  #   ӵ  "   ��  $   �     ?�     V�  !   o�     ��     ��     Ѷ  '   ��  2   �  %   H�  "   n�  #   ��  F   ��  9   ��  6   6�  3   m�  0   ��  9   Ҹ  &   �  B   3�  4   v�  0   ��  2   ܹ  =   �  /   M�  0   }�  ,   ��  -   ۺ  &   	�     0�  /   K�     {�  ,   ��  -   û  4   �  8   &�  ?   _�     ��     ��  )   ��  /   �  /   �  
   J�  	   U�  	   _�  	   i�     s�     ��     ��     ��  +   ��  +   ܽ  +   �  &   4�     [�     s�  !   ��      ��     վ     �     
�  "   "�     E�     d�  ,   x�     ��     ��     ƿ     ܿ  "   ��     �     8�  "   X�     {�     ��     ��     ��  *   ��     �     0�  
   O�      Z�  %   {�  ,   ��  )   ��      ��  %   �  	   ?�     I�     h�     m�     ��      ��     ��  '   ��  3   �     B�     Q�  "   c�  &   ��      ��     ��  &   ��  &   �  
   5�     @�     R�  )   q�  *   ��  #   ��     ��     ��     ��     �  !   +�  #   M�     q�     ~�     ��     ��     ��     ��     ��     ��     �     *�      ?�     `�  
   n�  #   y�     ��  .   ��     ��      ��  !   �  !   8�  %   Z�      ��     ��     ��     ��     ��      �  )   (�  "   R�     u�  )   ��     ��     ��     ��     ��     ��     ��     ��     �     �  )   .�     X�  (   e�  )   ��     ��     ��  %   ��     �       �  !   A�     c�  !   y�     ��     ��     ��      ��     �     #�  !   ;�  !   ]�     �     ��  &   ��     ��     ��     �      2�  *   S�  #   ~�     ��     ��  &   ��     ��     �      �     =�     W�     q�  #   ��  4   ��     ��  )   ��     )�     =�     \�  "   t�     ��     ��     ��     ��     ��     �     #�     ;�     Z�     y�  )   ��  *   ��  ,   ��  &   �     >�     ]�     u�     ��     ��     ��     ��     ��  "   �     .�     I�  &   c�     ��  ,   ��  .   ��     �     �     �     �     5�     D�     V�     e�     u�     y�  )   ��     ��  9  ��     �  *   &�     Q�     n�     ��  $   ��     ��     ��      ��  &   �     @�     ]�     p�     ��     ��     ��     ��     ��     ��      ��  )    �     J�     j�     ��     ��     ��  $   ��     ��     ��  "   �  )   5�     _�     �  +   ��     ��  #   ��     �     �  3   .�     b�     ��     ��     ��  #   ��  %   ��  %   �     ,�     4�     L�     Z�     y�     ��  1   ��     ��     ��  (   	�  @   2�     s�     ��     ��     ��     ��  #   ��     
�     (�  ,   F�  
   s�  "   ~�     ��  0   ��  ,   ��  /   �  .   N�     }�     ��  .   ��  "   ��     ��  "   �     4�  "   R�     u�     ��     ��  $   ��     ��  +   ��  -   &�     T�     r�  ,   ��  !   ��  $   ��  �  ��  3   ��     ��     ��     ��  0   �  	   6�     @�     W�     v�     ��     ��  
   ��  "  ��  �  ��     f�     ��     ��  /   ��  -   ��  !   �     =�     U�  b  b�  �   ��     ��  +  ��  F   ��  �  �     ��     �  	   !�     +�     =�  0   M�     ~�     ��     ��     ��  A   ��     ��      �     >�  %   X�  @   ~�  "   ��     ��     ��  %   ��  -   �     H�     c�  6   ��     ��     ��     ��     �     4�  "   N�      q�     ��     ��     ��     ��     ��     �      �  F   :�     ��     ��     ��     ��     ��     ��  $   �     B�     [�     v�  '   ��  )   ��     ��  3   ��  '   +�     S�     f�     ��     ��  3   ��     ��  
   �  9   �     G�     d�  /   ��     ��     ��  +   ��     �     &�     A�  	   D�     N�     [�  $   l�     ��     ��      ��     ��  !   �     '�     +�     /�  	   >�  1   H�     z�     ��     ��  "   ��  #   ��     �     !�     -�     A�     Q�  D   Z�  N   ��  G   ��     6�     V�  !   ]�     �  ,   ��     ��  "   ��     ��     �     �     �     -�     D�     _�     x�     ��      ��     ��  0   ��     �     +�     F�  5   Z�  #   ��      ��  '   ��     ��     �     .�  !   H�     j�     ��  %   ��  &   ��     ��     �  $   $�  "   I�  $   l�  /   ��  _   ��  _   !�  *   ��  +   ��     ��  -   ��  3   $�  !   X�  !   z�  "   ��  )   ��  .   ��  2   �  2   K�  8   ~�  B   ��  !   ��  D   �  *   a�  7   ��  $   ��  5   ��  8     1   X  !   �      �  C   �         ) &   G    n    � "   � -   � '   � #    ,   7 
   d    o    | "   � G   �    �     )   !    K ,   _ -   � -   � 	   �    �        *    = -   T    �    � !   � 
   �    � #        (    I    i @  } �   � &   � $   � 1   
	 0   <	 .   m	 N   �	 !   �	 5   
 :   C
 (   ~
    �
 0   �
 %   �
 *       E 	   c :   m    � :   �    � 1    -   H .   v *   �    �    �              "   % -   H    v     � ?   �    � )    $   ,    Q    n 	       � !   �    �     �    �    
 $       ?    ]    x    �     � *   �             7 2   N    �    �    � %   �        "    9 -   R    � !   � C   � @       D 3   ] /   � 5   �    �        / +   G E   s "   � <   �     -   6 *   d    �    � /   � '   � !   %    G -   _    �    � %   �    �    �    � !   �     6   ,    c    � 8   � 1   �     =   &    d    y $   �    �    � 4   � '    F   D    � -   �    �    �    � <       P    e    �    � 0   �    �            5 .   ;    j    p    ~    � $   �    �    � (       6    O    o !   � 5   �    �         "   % C   H C   � 4   �            )    I    c    {    �    �    �    � '       -    ? ,   S    �    �    � ,   �    � !   �         0   4 "   e #   �    � 3   �     �            +   .     Z     k  >   �     �     �     �     �     !    *!    E!    `! &   {! !   �! -   �! #   �! *   " &   A"    h"    �" $   �" 1   �" C   �"    A# )   M# -   w#    �# '   �# "   �#    �#    $ I   $    d$ "   w$ #   �$    �$    �$    �$    %    %    $% '   9%    a%    {%    �% 7   �% .   �%    	& "   (&    K&    Y&    g&    ~&    �&    �&    �&    �& 3   �& @   �&    @' =   `'    �' "   �'    �' 4   �' )   '(    Q(    m( "   �(    �( "   �( J   �( .   1) &   `) "   �) E   �)    �) %   *    6*    R* 5   o* &   �*    �*    �*    �* *   + (   C+ '   l+ &   �+    �+    �+    �+    ,    $,    :, (   S,    |,    �,    �,    �,    �,    -    - 0   )-    Z-    k-    z-    �- :   �- 6   �-    �-     . #   5.    Y.    l. S   �. S   �.    2/ )   C/ #   m/    �/ !   �/ !   �/ &   �/    0 )   /0 &   Y0 
   �0 L   �0 !   �0 &   �0 &   !1 9   H1    �1    �1    �1    �1    �1    �1    �1 Z   2    g2    |2     �2 "   �2 *   �2    �2    3    3    3    03    N3    k3    �3    �3    �3    �3 "   �3 
   �3    
4    4    4 #   )4 "   M4    p4 *   �4    �4 =   �4 %   5    -5    =5 	   ]5    g5 `   |5 #   �5 ?   6    A6 Z   M6 ,   �6 ?   �6 )   7    ?7 =   ]7 ,   �7 )   �7    �7    8    )8    E8 )   e8 '   �8     �8 8   �8 #   9 ?   59 +   u9    �9    �9 %   �9    �9 !   �9    :    .:    B: (   b:    �:    �:    �:    �: 0   �: 4   �:    $; +   8;    d;    �;    �;    �;    �; 
   �; '   �; +   < +   2<    ^<    q<    x<    �<    �<    �< 1   �<    =    0=    >=    Q=    Y=    h= \   {= 9   �=    >    %> '   9>    a>    n>     ~> *   �>    �> $   �>    	?    '? $   F? J   k? -   �?    �?    �?    	@ B   '@    j@    �@ !   �@ ?   �@    A @   A *   WA '   �A    �A B   �A ?   B �   AB 1   �B !   
C +   ,C    XC    fC (   �C !   �C %   �C    �C    D D   0D 9   uD 0   �D /   �D /   E 	   @E #   JE    nE    {E    �E    �E    �E    �E #   �E     F    0F    EF 0   aF    �F    �F    �F    �F    �F    �F %   G 	   ;G 8   EG D   ~G 9   �G +   �G &   )H 4   PH 7   �H 8   �H C   �H ]   :I     �I '   �I    �I     �I 6   J +   TJ .   �J #   �J .   �J H   K <   KK    �K 6   �K .   �K /   L -   >L    lL    �L    �L    �L 2   �L 8   �L 7   /M     gM !   �M    �M #   �M    �M     N    /N    ON    lN    �N &   �N ,   �N    �N !   O    'O    AO 3   QO !   �O $   �O 2   �O .   �O .   .P !   ]P    P .   �P )   �P "   �P    Q /   7Q ;   gQ -   �Q '   �Q +   �Q O   %R 9   uR ;   �R C   �R ?   /S J   oS (   �S K   �S :   /T =   jT ;   �T H   �T :   -U 9   hU 6   �U 7   �U ,   V    >V /   TV    �V 4   �V A   �V C   W 9   UW 4   �W    �W    �W 9   �W D   X G   dX    �X 
   �X    �X 
   �X    �X    �X    Y    Y 1   "Y 7   TY 8   �Y 1   �Y    �Y    Z *   6Z    aZ    �Z    �Z    �Z ,   �Z )   �Z 1   ![ 5   S[    �[    �[    �[ (   �[ *   �[    \    1\    Q\    o\ &   �\ &   �\ %   �\ 1   �\ #   .] #   R]    v] (   �] 2   �] 6   �] 0   ^ .   F^ 5   u^    �^ #   �^    �^ $   �^ "   _ *   %_ &   P_ <   w_ >   �_    �_    ` (   ` %   A`    g`    �` .   �` #   �` 
   �`    �` (   a 6   7a /   na -   �a    �a    �a    �a    �a *   b +   2b    ^b )   nb    �b    �b $   �b    �b    c    #c #   4c    Xc    nc    �c    �c $   �c    �c ;   �c    )d )   Dd #   nd (   �d /   �d $   �d    e    -e    @e     We %   xe -   �e '   �e    �e 4   f    Df    Kf    Sf    [f    cf    kf    �f    �f &   �f .   �f    �f 4   	g 3   >g    rg '   �g &   �g    �g     �g !   h    .h     ?h    `h !   ~h    �h &   �h #   �h    i    +i    Hi    di "   i /   �i #   �i    �i '   j "   >j *   aj '   �j     �j    �j -   �j $   k .   ?k '   nk "   �k    �k    �k 6   �k H   )l )   rl 1   �l    �l !   �l    
m &   )m "   Pm    sm #   �m    �m    �m    �m    �m #   n "   /n "   Rn *   un )   �n (   �n $   �n    o    3o    Po    no    �o    �o    �o    �o *   �o    (p    Ep *   bp    �p 9   �p 9   �p    q    "q    @q "   [q    ~q    �q    �q    �q    �q    �q -   �q /   r b  Gr    �s C   �s     t    t    4t B   Lt *   �t    �t )   �t 1   �t     0u    Qu "   iu '   �u     �u    �u    �u    �u    �u    v 8   4v +   mv    �v    �v    �v    �v '   �v    w    )w    8w 9   Xw "   �w    �w 3   �w '   	x (   1x    Zx    tx E   �x +   �x    �x    
y    $y -   >y 1   ly ,   �y 	   �y     �y    �y !   z    )z    <z 9   Pz    �z     �z 0   �z N   �z (   E{    n{    �{    �{ 	   �{ &   �{ $   �{ (   | )   9| 
   c| *   n| 7   �| :   �| &   } 7   3} 4   k}    �}    �} -   �} 6   �} 0   )~ "   Z~    }~ -   �~ )   �~    �~     +       I ;   b @   � '   �    � 1   � +   K� 4   w� �  �� 7   V�    �� "   ��    ˂ 6   ��    � *   #�     N�    o�    ��    �� 	   �� L  ��      8  R      �  R        �   A       �   �      Z  }      �  Q       �       �    �  �  �      u  �    �  �  �  Z      �     �   �      �  W  v                   g  T  �  K  �       5      �  �     �      �   �  �   �     �  r   |    �  �  !           �  �       �  �  7  �  �  ^  �  }  �          !       �      �   �     &  `  �  N              l   h        H  �  �  �   �    )  �  �  <      �   �  �  y  �  O  �          B      �  �  X  �     a  �  �  �   *  �  e  [   �  .        /  "  �   I       �  �          N  �                �  C      0    J  �   �  �   .    B  �  �    D  �  z   ;   �  �  �  ,  �  �  K           �  �   �      �      @   �   �   ~       c       �  �          
  E   C    �  )       �  r  1                      �  �  �                     �    c                      6    �  �  �   /       
   �  0   P  �      �  �   `         �    �  j  �   �   �  R      *  w  �  �   {   l    Q             �  X  7          ]            �         3  �  �  k  Y  h  �  E  �  3   �      m  M      m  #  	  �  f   E  p        M  �    �    �      �   �   n  �      m      h   N               3      �      P      �  �  y  �  �    :      �       ]   �           �   �  P   �      �   �               �   �  p     .  �         �       <   4      2  �    u   $      A  2          I      �   �  y  �   7  �  V  g  �          �  b  �      %  �  �       #  �  "   [        �            q           �          =   K  �      W  �    �  k  �      �  �  5   �          �   �    �  �  �  F  J  �    �      e  	      �  �  �  �  o  �            �  �  I  .  e   �               C     p  �  �  �    {        8              -   ~  a   ,      �  �   �  f  b  ;  )  �  .             �   �      4  �  S  =  E  e          >     �  G   �   �      +       _      �   U                       �  �      �     9  �   S   7       �      )  ,  :                  �            �  �  8             �  |  �   �       �      $  �  '    n          O  �      D  7        �  L      o      �  �  �         �  �      E  T    �      q     �  k  N          �   �   �            �      �  �  h  f                      2       ^   �       C      �        t      �  4  �  �   �      �  $       �  �  y   �   G  �  �          ^  �      	       !  k   �      �              &    {  i   $  &      �  �  �       i     �  �          �  G  #      �      +  }  �      �            !          0  �          �  �                  �  �   �  �  �  �          �      �       �          �      V   �      �   �      ~              �  �  <  3  t  '        �  �   �  �       �  +  �  �    5    �      �   /      \           Y  �  �    �   0  q  5      �      �  �  r      �  R  ;      '    �   {  �   %  =  �  :  U   i  +           �      X        �  �      H  �      ?  �      �  P      �  �  >   �  �      �  �  �  �  ;  K                  �  -  K  u  �  �          �  �   �  [  �      |  @          �  �        �   $  �  Q      �   �    �  �          �  H  '      t  �  D  �  s  l        M      ~  4         �      B  +  /  6  �      �   `         �      d      �        ?   �  �   R      �  �      �   0  �  �   �      �                     �    �   �       <      �  S      }  �  (  J      �         -      x    �  o   o    �      �   D   [      �   ?  �                  �  �  �  U          d      �   	  V  !            X      (  :   4      %   �  �  a  1  �         -  �  _  *  ;  H  �  �      /  >      =  �          9          �  �  g  b   A  c  
            �  �  �  �            �   #  z          �  �   �       8   )  f  �  a  �  �  	  �      �    �  �  �               *  �  p  �  �  s    z      �    �      T       �   �  O    �               �    �      �   Q  #   B  V  m   �  �  \        �              �      �  �  L  �          c  A          �  �  t   �  N      A  v  L  (  �    �     l  �   �       ?  �       �      �            �  r  �  �  
  �  1     @  %  �  >  �      �  @     �  �           �   "  -  F  ^  I  �  �      �   Q      i      �     5      F   "              �   �            �      �       �    �  �  P      �      �       �   �      �  2  �   �   Y  �  Z   6  `          �     �           �  \  d       �   W   s   9  �  %  d  �   �  �  j  �  '   �              9   �  �       
  �       j  �          _  u  T      �  �      �  �  M  �  j   �  �    �  g   6   8  B       6  &    \    1  *   n            �       W    �  �   :  �                �  U  ,  �   �          �   <  �  �  �  �  w  3      H                   �           �      x  J   1  �  x   �   �  S  �  �       �      �  �  ?           �   Z  x  �  �   �                  �   �         �   �  �  ]  �       �    2  �       �  �  �       �  �  �          �  (   G  �    v  q  z  �  >        �   �  �   �      �    C  L   �   L    Y            �  9  (  F    ]  w   �   �   �   _     ,     =  �        �  �  v  J  �  �  �  �      |   O  �  b  �               "  �  �  �      s  F  w  &   �  I  �          �  �   �      �            �  G  �  n   O      �   D  M   @   
Compile options: 
Generic bindings:

 
Unbound functions:

 
[-- End of S/MIME encrypted data. --]
 
[-- End of S/MIME signed data. --]
 
[-- End of signed data --]
                expires:       to %s     This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
     You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
    from %s   -Q <variable>	query a configuration variable
  -R		open mailbox in read-only mode
  -s <subj>	specify a subject (must be in quotes if it has spaces)
  -v		show version and compile-time definitions
  -x		simulate the mailx send mode
  -y		select a mailbox specified in your `mailboxes' list
  -z		exit immediately if there are no messages in the mailbox
  -Z		open the first folder with new message, exit immediately if none
  -h		this help message   -d <level>	log debugging output to ~/.muttdebug0   -e <command>	specify a command to be executed after initialization
  -f <file>	specify which mailbox to read
  -F <file>	specify an alternate muttrc file
  -H <file>	specify a draft file to read header and body from
  -i <file>	specify a file which Mutt should include in the body
  -m <type>	specify a default mailbox type
  -n		causes Mutt not to read the system Muttrc
  -p		recall a postponed message  ('?' for list):   (PGP/MIME)  (S/MIME)  (current time: %c)  (inline PGP)  Press '%s' to toggle write  aka ......:   in this limited view  sign as:   tagged "crypt_use_gpgme" set but not built with GPGME support. %c: invalid pattern modifier %c: not supported in this mode %d kept, %d deleted. %d kept, %d moved, %d deleted. %d messages have been lost. Try reopening the mailbox. %d: invalid message number.
 %s "%s". %s <%s>. %s Do you really want to use the key? %s [#%d] modified. Update encoding? %s [#%d] no longer exists! %s [%d of %d messages read] %s authentication failed, trying next method %s does not exist. Create it? %s has insecure permissions! %s is an invalid IMAP path %s is an invalid POP path %s is not a directory. %s is not a mailbox! %s is not a mailbox. %s is set %s is unset %s isn't a regular file. %s no longer exists! %s...  Exiting.
 %s: Unknown type. %s: color not supported by term %s: command valid only for index, body, header objects %s: invalid mailbox type %s: invalid value %s: invalid value (%s) %s: no such attribute %s: no such color %s: no such function %s: no such function in map %s: no such menu %s: no such object %s: too few arguments %s: unable to attach file %s: unable to attach file.
 %s: unknown command %s: unknown editor command (~? for help)
 %s: unknown sorting method %s: unknown type %s: unknown variable %sgroup: missing -rx or -addr. %sgroup: warning: bad IDN '%s'.
 (End message with a . on a line by itself)
 (continue)
 (i)nline (need 'view-attachments' bound to key!) (no mailbox) (r)eject, accept (o)nce (r)eject, accept (o)nce, (a)ccept always (size %s bytes)  (use '%s' to view this part) *** Begin Notation (signature by: %s) ***
 *** End Notation ***
 *BAD* signature from: ,  -- Attachments ---Attachment: %s ---Attachment: %s: %s ---Command: %-20.20s Description: %s ---Command: %-30.30s Attachment: %s -group: no group name 1: AES128, 2: AES192, 3: AES256  1: DES, 2: Triple-DES  1: RC2-40, 2: RC2-64, 3: RC2-128  468 895 <UNKNOWN> <default> A policy requirement was not met
 A system error occurred APOP authentication failed. Abort Abort unmodified message? Aborted unmodified message. Accept the chain constructed Address:  Alias added. Alias as:  Aliases All available protocols for TLS/SSL connection disabled All matching keys are expired, revoked, or disabled. All matching keys are marked expired/revoked. Anonymous authentication failed. Append Append a remailer to the chain Append messages to %s? Argument must be a message number. Attach file Attaching selected files... Attachment filtered. Attachment saved. Attachments Authenticating (%s)... Authenticating (APOP)... Authenticating (CRAM-MD5)... Authenticating (GSSAPI)... Authenticating (SASL)... Authenticating (anonymous)... Available CRL is too old
 Bad IDN "%s". Bad IDN %s while preparing resent-from. Bad IDN in "%s": '%s' Bad IDN in %s: '%s'
 Bad IDN: '%s' Bad history file format (line %d) Bad mailbox name Bad regexp: %s Bottom of message is shown. Bounce message to %s Bounce message to:  Bounce messages to %s Bounce tagged messages to:  CRAM-MD5 authentication failed. CREATE failed: %s Can't append to folder: %s Can't attach a directory! Can't create %s. Can't create %s: %s. Can't create file %s Can't create filter Can't create filter process Can't create temporary file Can't decode all tagged attachments.  MIME-encapsulate the others? Can't decode all tagged attachments.  MIME-forward the others? Can't decrypt encrypted message! Can't delete attachment from POP server. Can't dotlock %s.
 Can't find any tagged messages. Can't get mixmaster's type2.list! Can't invoke PGP Can't match nametemplate, continue? Can't open /dev/null Can't open OpenSSL subprocess! Can't open PGP subprocess! Can't open message file: %s Can't open temporary file %s. Can't save message to POP mailbox. Can't sign: No key specified. Use Sign As. Can't stat %s: %s Can't verify due to a missing key or certificate
 Can't view a directory Can't write header to temporary file! Can't write message Can't write message to temporary file! Cannot %s: Operation not permitted by ACL Cannot create display filter Cannot create filter Cannot delete root folder Cannot toggle write on a readonly mailbox! Caught %s...  Exiting.
 Caught signal %d...  Exiting.
 Certificate host check failed: %s Certificate is not X.509 Certificate saved Certificate verification error (%s) Changes to folder will be written on folder exit. Changes to folder will not be written. Char = %s, Octal = %o, Decimal = %d Character set changed to %s; %s. Chdir Chdir to:  Check key   Checking for new messages... Choose algorithm family: 1: DES, 2: RC2, 3: AES, or (c)lear?  Clear flag Closing connection to %s... Closing connection to POP server... Collecting data... Command TOP is not supported by server. Command UIDL is not supported by server. Command USER is not supported by server. Command:  Committing changes... Compiling search pattern... Connecting to %s... Connecting with "%s"... Connection lost. Reconnect to POP server? Connection to %s closed Content-Type changed to %s. Content-Type is of the form base/sub Continue? Convert to %s upon sending? Copy%s to mailbox Copying %d messages to %s... Copying message %d to %s... Copying to %s... Copyright (C) 1996-2007 Michael R. Elkins <me@mutt.org>
Copyright (C) 1996-2002 Brandon Long <blong@fiction.net>
Copyright (C) 1997-2008 Thomas Roessler <roessler@does-not-exist.org>
Copyright (C) 1998-2005 Werner Koch <wk@isil.d.shuttle.de>
Copyright (C) 1999-2009 Brendan Cully <brendan@kublai.com>
Copyright (C) 1999-2002 Tommi Komulainen <Tommi.Komulainen@iki.fi>
Copyright (C) 2000-2002 Edmund Grimley Evans <edmundo@rano.org>
Copyright (C) 2006-2009 Rocco Rutte <pdmef@gmx.net>

Many others not mentioned here contributed code, fixes,
and suggestions.
 Copyright (C) 1996-2009 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.
 Could not connect to %s (%s). Could not copy message Could not create temporary file %s Could not create temporary file! Could not decrypt PGP message Could not find sorting function! [report this bug] Could not find the host "%s" Could not flush message to disk Could not include all requested messages! Could not negotiate TLS connection Could not open %s Could not reopen mailbox! Could not send the message. Could not synchronize mailbox %s! Couldn't lock %s
 Create %s? Create is only supported for IMAP mailboxes Create mailbox:  DEBUG was not defined during compilation.  Ignored.
 Debugging at level %d.
 Decode-copy%s to mailbox Decode-save%s to mailbox Decrypt-copy%s to mailbox Decrypt-save%s to mailbox Decrypting message... Decryption failed Decryption failed. Del Delete Delete a remailer from the chain Delete is only supported for IMAP mailboxes Delete messages from server? Delete messages matching:  Deletion of attachments from encrypted messages is unsupported. Descrip Directory [%s], File mask: %s ERROR: please report this bug Edit forwarded message? Empty expression Encrypt Encrypt with:  Encrypted connection unavailable Enter PGP passphrase: Enter S/MIME passphrase: Enter keyID for %s:  Enter keyID:  Enter keys (^G to abort):  Error allocating SASL connection Error bouncing message! Error bouncing messages! Error connecting to server: %s Error extracting key data!
 Error finding issuer key: %s
 Error in %s, line %d: %s Error in command line: %s
 Error in expression: %s Error initialising gnutls certificate data Error initializing terminal. Error opening mailbox Error parsing address! Error processing certificate data Error reading alias file Error running "%s"! Error saving flags Error saving flags. Close anyway? Error scanning directory. Error seeking in alias file Error sending message, child exited %d (%s). Error sending message, child exited %d.
 Error sending message. Error setting SASL external security strength Error setting SASL external user name Error setting SASL security properties Error talking to %s (%s) Error trying to view file Error while writing mailbox! Error. Preserving temporary file: %s Error: %s can't be used as the final remailer of a chain. Error: '%s' is a bad IDN. Error: certification chain to long - stopping here
 Error: copy data failed
 Error: decryption/verification failed: %s
 Error: multipart/signed has no protocol. Error: no TLS socket open Error: score: invalid number Error: unable to create OpenSSL subprocess! Error: value '%s' is invalid for -d.
 Error: verification failed: %s
 Evaluating cache... Executing command on matching messages... Exit Exit   Exit Mutt without saving? Exit Mutt? Expired    Expunge failed Expunging messages from server... Failed to figure out sender Failed to find enough entropy on your system Failed to parse mailto: link
 Failed to verify sender Failure to open file to parse headers. Failure to open file to strip headers. Failure to rename file. Fatal error!  Could not reopen mailbox! Fetching PGP key... Fetching list of messages... Fetching message headers... Fetching message... File Mask:  File exists, (o)verwrite, (a)ppend, or (c)ancel? File is a directory, save under it? File is a directory, save under it? [(y)es, (n)o, (a)ll] File under directory:  Filling entropy pool: %s...
 Filter through:  Fingerprint:  Fingerprint: %s First, please tag a message to be linked here Follow-up to %s%s? Forward MIME encapsulated? Forward as attachment? Forward as attachments? Function not permitted in attach-message mode. GSSAPI authentication failed. Getting folder list... Good signature from: Group Header search without header name: %s Help Help for %s Help is currently being shown. I don't know how to print that! I dont know how to print %s attachments! I/O error ID has undefined validity. ID is expired/disabled/revoked. ID is not valid. ID is only marginally valid. Illegal S/MIME header Illegal crypto header Improperly formatted entry for type %s in "%s" line %d Include message in reply? Including quoted message... Insert Insert a remailer into the chain Integer overflow -- can't allocate memory! Integer overflow -- can't allocate memory. Internal error. Inform <roessler@does-not-exist.org>. Invalid    Invalid POP URL: %s
 Invalid SMTP URL: %s Invalid day of month: %s Invalid encoding. Invalid index number. Invalid message number. Invalid month: %s Invalid relative date: %s Invalid server response Invalid value for option %s: "%s" Invoking PGP... Invoking S/MIME... Invoking autoview command: %s Issued By .:  Jump to message:  Jump to:  Jumping is not implemented for dialogs. Key ID: 0x%s Key Type ..: %s, %lu bit %s
 Key Usage .:  Key is not bound. Key is not bound.  Press '%s' for help. LOGIN disabled on this server. Limit to messages matching:  Limit: %s Lock count exceeded, remove lock for %s? Logged out of IMAP servers. Logging in... Login failed. Looking for keys matching "%s"... Looking up %s... MD5 Fingerprint: %s MIME type not defined.  Cannot view attachment. Macro loop detected. Mail Mail not sent. Mail sent. Mailbox checkpointed. Mailbox closed Mailbox created. Mailbox deleted. Mailbox is corrupt! Mailbox is empty. Mailbox is marked unwritable. %s Mailbox is read-only. Mailbox is unchanged. Mailbox must have a name. Mailbox not deleted. Mailbox renamed. Mailbox was corrupted! Mailbox was externally modified. Mailbox was externally modified.  Flags may be wrong. Mailboxes [%d] Mailcap Edit entry requires %%s Mailcap compose entry requires %%s Make Alias Marking %d messages deleted... Marking messages deleted... Mask Message bounced. Message can't be sent inline.  Revert to using PGP/MIME? Message contains:
 Message could not be printed Message file is empty! Message not bounced. Message not modified! Message postponed. Message printed Message written. Messages bounced. Messages could not be printed Messages not bounced. Messages printed Missing arguments. Mixmaster chains are limited to %d elements. Mixmaster doesn't accept Cc or Bcc headers. Move read messages to %s? Moving read messages to %s... Name ......:  New Query New file name:  New file:  New mail in  New mail in this mailbox. Next NextPg No (valid) certificate found for %s. No Message-ID: header available to link thread No authenticators available No boundary parameter found! [report this error] No entries. No files match the file mask No from address given No incoming mailboxes defined. No limit pattern is in effect. No lines in message.
 No mailbox is open. No mailbox with new mail. No mailbox.
 No mailboxes have new mail No mailcap compose entry for %s, creating empty file. No mailcap edit entry for %s No mailcap path specified No mailing lists found! No matching mailcap entry found.  Viewing as text. No messages in that folder. No messages matched criteria. No more quoted text. No more threads. No more unquoted text after quoted text. No new mail in POP mailbox. No new messages No output from OpenSSL... No postponed messages. No printing command has been defined. No recipients are specified! No recipients specified.
 No recipients were specified. No subject specified. No subject, abort sending? No subject, abort? No subject, aborting. No such folder No tagged entries. No tagged messages are visible! No tagged messages. No thread linked No undeleted messages. No unread messages No visible messages. None Not available in this menu. Not enough subexpressions for spam template Not found. Not supported Nothing to do. OK One or more parts of this message could not be displayed Only deletion of multipart attachments is supported. Open mailbox Open mailbox in read-only mode Open mailbox to attach message from Out of memory! Output of the delivery process PGP (e)ncrypt, (s)ign, sign (a)s, (b)oth, %s format, or (c)lear?  PGP (e)ncrypt, (s)ign, sign (a)s, (b)oth, or (c)lear?  PGP Key %s. PGP already selected. Clear & continue ?  PGP and S/MIME keys matching PGP keys matching PGP keys matching "%s". PGP keys matching <%s>. PGP message successfully decrypted. PGP passphrase forgotten. PGP signature could NOT be verified. PGP signature successfully verified. PGP/M(i)ME PKA verified signer's address is:  POP host is not defined. POP timestamp is invalid! Parent message is not available. Parent message is not visible in this limited view. Passphrase(s) forgotten. Password for %s@%s:  Personal name:  Pipe Pipe to command:  Pipe to:  Please enter the key ID:  Please set the hostname variable to a proper value when using mixmaster! Postpone this message? Postponed Messages Preconnect command failed. Preparing forwarded message... Press any key to continue... PrevPg Print Print attachment? Print message? Print tagged attachment(s)? Print tagged messages? Problem signature from: Purge %d deleted message? Purge %d deleted messages? Query Query '%s' Query command not defined. Query:  Quit Quit Mutt? Reading %s... Reading new messages (%d bytes)... Really delete mailbox "%s"? Recall postponed message? Recoding only affects text attachments. Rename failed: %s Rename is only supported for IMAP mailboxes Rename mailbox %s to:  Rename to:  Reopening mailbox... Reply Reply to %s%s? Rev-Sort (d)ate/(f)rm/(r)ecv/(s)ubj/t(o)/(t)hread/(u)nsort/si(z)e/s(c)ore/s(p)am?:  Reverse search for:  Reverse sort by (d)ate, (a)lpha, si(z)e or do(n)'t sort?  Revoked    S/MIME (e)ncrypt, (s)ign, encrypt (w)ith, sign (a)s, (b)oth, or (c)lear?  S/MIME already selected. Clear & continue ?  S/MIME certificate owner does not match sender. S/MIME certificates matching "%s". S/MIME keys matching S/MIME messages with no hints on content are unsupported. S/MIME signature could NOT be verified. S/MIME signature successfully verified. SASL authentication failed SASL authentication failed. SHA1 Fingerprint: %s SMTP authentication requires SASL SMTP server does not support authentication SMTP session failed: %s SMTP session failed: read error SMTP session failed: unable to open %s SMTP session failed: write error SSL Certificate check (certificate %d of %d in chain) SSL disabled due the lack of entropy SSL failed: %s SSL is unavailable. SSL/TLS connection using %s (%s/%s/%s) Save Save a copy of this message? Save attachments in Fcc? Save to file:  Save%s to mailbox Saving changed messages... [%d/%d] Saving... Scanning %s... Search Search for:  Search hit bottom without finding match Search hit top without finding match Search interrupted. Search is not implemented for this menu. Search wrapped to bottom. Search wrapped to top. Searching... Secure connection with TLS? Select Select   Select a remailer chain. Select the next element of the chain Select the previous element of the chain Selecting %s... Send Sending in background. Sending message... Serial-No .: 0x%s
 Server certificate has expired Server certificate is not yet valid Server closed connection! Set flag Shell command:  Sign Sign as:  Sign, Encrypt Sort (d)ate/(f)rm/(r)ecv/(s)ubj/t(o)/(t)hread/(u)nsort/si(z)e/s(c)ore/s(p)am?:  Sort by (d)ate, (a)lpha, si(z)e or do(n)'t sort?  Sorting mailbox... Subkey ....: 0x%s Subscribed [%s], File mask: %s Subscribed to %s Subscribing to %s... Tag messages matching:  Tag the messages you want to attach! Tagging is not supported. That message is not visible. The CRL is not available
 The current attachment will be converted. The current attachment won't be converted. The message index is incorrect. Try reopening the mailbox. The remailer chain is already empty. There are no attachments. There are no messages. There are no subparts to show! This IMAP server is ancient. Mutt does not work with it. This certificate belongs to: This certificate is valid This certificate was issued by: This key can't be used: expired/disabled/revoked. Thread broken Thread cannot be broken, message is not part of a thread Thread contains unread messages. Threading is not enabled. Threads linked Timeout exceeded while attempting fcntl lock! Timeout exceeded while attempting flock lock! To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.
 To view all messages, limit to "all". Toggle display of subparts Top of message is shown. Trusted    Trying to extract PGP keys...
 Trying to extract S/MIME certificates...
 Tunnel error talking to %s: %s Tunnel to %s returned error %d (%s) Unable to attach %s! Unable to attach! Unable to fetch headers from this IMAP server version. Unable to get certificate from peer Unable to leave messages on server. Unable to lock mailbox! Unable to open temporary file! Undel Undelete messages matching:  Unknown Unknown    Unknown Content-Type %s Unknown SASL profile Unsubscribed from %s Unsubscribing from %s... Untag messages matching:  Unverified Uploading message... Usage: set variable=yes|no Use 'toggle-write' to re-enable write! Use keyID = "%s" for %s? Username at %s:  Valid From : %s
 Valid To ..: %s
 Verified   Verify PGP signature? Verifying message indexes... View Attachm. WARNING!  You are about to overwrite %s, continue? WARNING: It is NOT certain that the key belongs to the person named as shown above
 WARNING: PKA entry does not match signer's address:  WARNING: Server certificate has been revoked WARNING: Server certificate has expired WARNING: Server certificate is not yet valid WARNING: Server hostname does not match certificate WARNING: Signer of server certificate is not a CA WARNING: The key does NOT BELONG to the person named as shown above
 WARNING: We have NO indication whether the key belongs to the person named as shown above
 Waiting for fcntl lock... %d Waiting for flock attempt... %d Waiting for response... Warning: '%s' is a bad IDN. Warning: At least one certification key has expired
 Warning: Bad IDN '%s' in alias '%s'.
 Warning: Couldn't save certificate Warning: One of the keys has been revoked
 Warning: Part of this message has not been signed. Warning: Server certificate was signed using an insecure algorithm Warning: The key used to create the signature expired at:  Warning: The signature expired at:  Warning: This alias name may not work.  Fix it? Warning: message contains no From: header What we have here is a failure to make an attachment Write failed!  Saved partial mailbox to %s Write fault! Write message to mailbox Writing %s... Writing message to %s ... You already have an alias defined with that name! You already have the first chain element selected. You already have the last chain element selected. You are on the first entry. You are on the first message. You are on the first page. You are on the first thread. You are on the last entry. You are on the last message. You are on the last page. You cannot scroll down farther. You cannot scroll up farther. You have no aliases! You may not delete the only attachment. You may only bounce message/rfc822 parts. [%s = %s] Accept? [-- %s output follows%s --]
 [-- %s/%s is unsupported  [-- Attachment #%d [-- Autoview stderr of %s --]
 [-- Autoview using %s --]
 [-- BEGIN PGP MESSAGE --]

 [-- BEGIN PGP PUBLIC KEY BLOCK --]
 [-- BEGIN PGP SIGNED MESSAGE --]

 [-- Begin signature information --]
 [-- Can't run %s. --]
 [-- END PGP MESSAGE --]
 [-- END PGP PUBLIC KEY BLOCK --]
 [-- END PGP SIGNED MESSAGE --]
 [-- End of OpenSSL output --]

 [-- End of PGP output --]

 [-- End of PGP/MIME encrypted data --]
 [-- End of PGP/MIME signed and encrypted data --]
 [-- End of S/MIME encrypted data --]
 [-- End of S/MIME signed data --]
 [-- End signature information --]

 [-- Error:  Could not display any parts of Multipart/Alternative! --]
 [-- Error: Inconsistent multipart/signed structure! --]

 [-- Error: Unknown multipart/signed protocol %s! --]

 [-- Error: could not create a PGP subprocess! --]

 [-- Error: could not create temporary file! --]
 [-- Error: could not find beginning of PGP message! --]

 [-- Error: decryption failed: %s --]

 [-- Error: message/external-body has no access-type parameter --]
 [-- Error: unable to create OpenSSL subprocess! --]
 [-- Error: unable to create PGP subprocess! --]
 [-- The following data is PGP/MIME encrypted --]

 [-- The following data is PGP/MIME signed and encrypted --]

 [-- The following data is S/MIME encrypted --]
 [-- The following data is S/MIME encrypted --]

 [-- The following data is S/MIME signed --]
 [-- The following data is S/MIME signed --]

 [-- The following data is signed --]

 [-- This %s/%s attachment  [-- This %s/%s attachment is not included, --]
 [-- This is an attachment  [-- Type: %s/%s, Encoding: %s, Size: %s --]
 [-- Warning: Can't find any signatures. --]

 [-- Warning: We can't verify %s/%s signatures. --]

 [-- and the indicated access-type %s is unsupported --]
 [-- and the indicated external source has --]
[-- expired. --]
 [-- name: %s --]
 [-- on %s --]
 [Can't display this user ID (invalid DN)] [Can't display this user ID (invalid encoding)] [Can't display this user ID (unknown encoding)] [Disabled] [Expired] [Invalid] [Revoked] [invalid date] [unable to calculate] aka:  alias: no address ambiguous specification of secret key `%s'
 append new query results to current results apply next function ONLY to tagged messages apply next function to tagged messages attach a PGP public key attach file(s) to this message attach message(s) to this message attachments: invalid disposition attachments: no disposition bind: too many arguments break the thread in two cannot get certificate common name cannot get certificate subject capitalize the word certificate owner does not match hostname %s certification change directories check for classic PGP check mailboxes for new mail clear a status flag from a message clear and redraw the screen collapse/uncollapse all threads collapse/uncollapse current thread color: too few arguments complete address with query complete filename or alias compose a new mail message compose new attachment using mailcap entry convert the word to lower case convert the word to upper case converting copy a message to a file/mailbox could not create temporary folder: %s could not truncate temporary mail folder: %s could not write temporary mail folder: %s create a new mailbox (IMAP only) create an alias from a message sender created:  cycle among incoming mailboxes dazn default colors not supported delete all chars on the line delete all messages in subthread delete all messages in thread delete chars from cursor to end of line delete chars from the cursor to the end of the word delete message delete message(s) delete messages matching a pattern delete the char in front of the cursor delete the char under the cursor delete the current entry delete the current mailbox (IMAP only) delete the word in front of the cursor dfrsotuzcp display a message display full address of sender display message and toggle header weeding display the currently selected file's name display the keycode for a key press drac dt edit attachment content type edit attachment description edit attachment transfer-encoding edit attachment using mailcap entry edit message edit the BCC list edit the CC list edit the Reply-To field edit the TO list edit the file to be attached edit the from field edit the message edit the message with headers edit the raw message edit the subject of this message empty pattern encryption end of conditional execution (noop) enter a file mask enter a file to save a copy of this message in enter a muttrc command error adding recipient `%s': %s
 error allocating data object: %s
 error creating gpgme context: %s
 error creating gpgme data object: %s
 error enabling CMS protocol: %s
 error encrypting data: %s
 error in expression error in pattern at: %s error reading data object: %s
 error rewinding data object: %s
 error setting PKA signature notation: %s
 error setting secret key `%s': %s
 error signing data: %s
 error: unknown op %d (report this error). esabfc esabfci esabmfc esabpfc eswabfc exec: no arguments execute a macro exit this menu extract supported public keys filter attachment through a shell command flag message force retrieval of mail from IMAP server force viewing of attachment using mailcap format error forward a message with comments get a temporary copy of an attachment gpgme_new failed: %s gpgme_op_keylist_next failed: %s gpgme_op_keylist_start failed: %s has been deleted --]
 imap_sync_mailbox: EXPUNGE failed invalid header field invoke a command in a subshell jump to an index number jump to parent message in thread jump to previous subthread jump to previous thread jump to the beginning of the line jump to the bottom of the message jump to the end of the line jump to the next new message jump to the next new or unread message jump to the next subthread jump to the next thread jump to the next unread message jump to the previous new message jump to the previous new or unread message jump to the previous unread message jump to the top of the message keys matching link tagged message to the current one link threads list mailboxes with new mail logout from all IMAP servers macro: empty key sequence macro: too many arguments mail a PGP public key mailcap entry for type %s not found maildir_commit_message(): unable to set time on file make decoded (text/plain) copy make decoded copy (text/plain) and delete make decrypted copy make decrypted copy and delete mark message(s) as read mark the current subthread as read mark the current thread as read mismatched brackets: %s mismatched parenthesis: %s missing filename.
 missing parameter missing pattern: %s mono: too few arguments move entry to bottom of screen move entry to middle of screen move entry to top of screen move the cursor one character to the left move the cursor one character to the right move the cursor to the beginning of the word move the cursor to the end of the word move to the bottom of the page move to the first entry move to the first message move to the last entry move to the last message move to the middle of the page move to the next entry move to the next page move to the next undeleted message move to the previous entry move to the previous page move to the previous undeleted message move to the top of the page multipart message has no boundary parameter! mutt_restore_default(%s): error in regexp: %s
 no no certfile no mbox nospam: no matching pattern not converting null key sequence null operation number overflow oac open a different folder open a different folder in read only mode open next mailbox with new mail options:
  -A <alias>	expand the given alias
  -a <file> [...] --	attach file(s) to the message
		the list of files must be terminated with the "--" sequence
  -b <address>	specify a blind carbon-copy (BCC) address
  -c <address>	specify a carbon-copy (CC) address
  -D		print the value of all variables to stdout out of arguments pipe message/attachment to a shell command prefix is illegal with reset print the current entry push: too many arguments query external program for addresses quote the next typed key recall a postponed message remail a message to another user rename the current mailbox (IMAP only) rename/move an attached file reply to a message reply to all recipients reply to specified mailing list retrieve mail from POP server ro roa run ispell on the message save changes to mailbox save changes to mailbox and quit save message/attachment to a mailbox/file save this message to send later score: too few arguments score: too many arguments scroll down 1/2 page scroll down one line scroll down through the history list scroll up 1/2 page scroll up one line scroll up through the history list search backwards for a regular expression search for a regular expression search for next match search for next match in opposite direction secret key `%s' not found: %s
 select a new file in this directory select the current entry send the message send the message through a mixmaster remailer chain set a status flag on a message show MIME attachments show PGP options show S/MIME options show currently active limit pattern show only messages matching a pattern show the Mutt version number and date signing skip beyond quoted text sort messages sort messages in reverse order source: error at %s source: errors in %s source: reading aborted due too many errors in %s source: too many arguments spam: no matching pattern subscribe to current mailbox (IMAP only) sync: mbox modified, but no modified messages! (report this bug) tag messages matching a pattern tag the current entry tag the current subthread tag the current thread this screen toggle a message's 'important' flag toggle a message's 'new' flag toggle display of quoted text toggle disposition between inline/attachment toggle new toggle recoding of this attachment toggle search pattern coloring toggle view all/subscribed mailboxes (IMAP only) toggle whether the mailbox will be rewritten toggle whether to browse mailboxes or all files toggle whether to delete file after sending it too few arguments too many arguments transpose character under cursor with previous unable to determine home directory unable to determine username unattachments: invalid disposition unattachments: no disposition undelete all messages in subthread undelete all messages in thread undelete message undelete message(s) undelete messages matching a pattern undelete the current entry unhook: Can't delete a %s from within a %s. unhook: Can't do unhook * from within a hook. unhook: unknown hook type: %s unknown error unsubscribe from current mailbox (IMAP only) untag messages matching a pattern update an attachment's encoding info usage: mutt [<options>] [-z] [-f <file> | -yZ]
       mutt [<options>] [-x] [-Hi <file>] [-s <subj>] [-bc <addr>] [-a <file> [...] --] <addr> [...]
       mutt [<options>] [-x] [-s <subj>] [-bc <addr>] [-a <file> [...] --] <addr> [...] < message
       mutt [<options>] -p
       mutt [<options>] -A <alias> [...]
       mutt [<options>] -Q <query> [...]
       mutt [<options>] -D
       mutt -v[v]
 use the current message as a template for a new one value is illegal with reset verify a PGP public key view attachment as text view attachment using mailcap entry if necessary view file view the key's user id wipe passphrase(s) from memory write the message to a folder yes yna {internal} ~q		write file and quit editor
~r file		read a file into the editor
~t users	add users to the To: field
~u		recall the previous line
~v		edit message with the $visual editor
~w file		write message to file
~x		abort changes and quit editor
~?		this message
.		on a line by itself ends input
 Project-Id-Version: 1.1.5i
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-08-30 10:25-0700
PO-Revision-Date: 2016-02-22 14:46+0000
Last-Translator: Marcus Brito <Unknown>
Language-Team: LIE-BR (http://lie-br.conectiva.com.br)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:12+0000
X-Generator: Launchpad (build 18115)
Language: 
 
Opções de compilação: 
Associações genéricas:

 
Funções sem associação:

 
[-- Fim dos dados encriptados com S/MIME. --]
 
[-- Fim dos dados assinados  com S/MIME.--]
 
[-- Fim dos dados assinados --]
                expira:       para %s     Este programa é um software livre; você pode redistribuir e/ou modifica-lo 
    sob os temos do GNU General Public License como publicado pela
    Free Software Foundation; através da versão 2 da Licença ou
    (por sua opção) uma versão superior.

    Este programa e distribuído com a esperança de ser útil, 
porém SEM NENHUM GARANTIA;
     Você deve ter recebido uma cópia da GNU General Public License
    junto com esse programa; Se não recebeu, escreve para Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
    de %s   -Q <variável>	consulta uma variável no arquivo de configuração

-R		abre uma caixa de mensagem em modo somente leitura

-s <assunto>	especifica um assunto (precisa estar em aspas caso contenha espaços)

-v		mostra a versão e a data de compilação

-x		simula o modo de envio do mailx

-y		seleciona uma caixa de mensagen especificada em sua lista de `caixas de mensagens'

-z		sai imediatamente se não existir mensagens na caixa de mensagens

-Z		abre a primeira pasta com novas mensagens, sai imediatamente se não houver

-h		this help message   -d <nível>	registrar saída de depuração em arquivo ~/.muttdebug0   -e <comando>	especifique um comando que será executado após a inicialização

-f <arquivo>	especifique qual caixa de mensagen será lida

-F <arquivo>	especifique um arquivo muttrc alternativo

-H <arquivo>	especifique um arquivo rascunho para leitura do cabeçalho e do corpo

-i <arquivo>	especifique um arquivo que o Mutt irá encluir no corpo

-m <tipo>	especifique um tipo caixa de mensagens padrão

-n		faz o Mutt não ler o sistema Muttrc

-p		retorna um menssagem  ('?' para uma lista):   (PGP/MIME)  (S/MIME)  (hora atual: %c)  (embutido PGP)  Pressione '%s' para trocar entre gravar ou não  aka ......:   nesta visão limitada  assinar como:   marcada "crypt_use_gpgme" ajustado mas não embutido com o suporte GPGME. %c:inválido modificar padrão %c: não é possível neste modo %d mantidas, %d apagadas. %d mantidas, %d movidas, %d apagadas. %d mensagens foram perdidas. Tente reabrir a caixa de mensagens. %d: número de mensagem iválido.
 %s "%s". %s <%s>. %s Você realmente quer usar a chave? %s [#%d] modificado. Atualizar codificação? %s [#%d] não existe mais! %s [%d de %d mensagens lidas] %s falhou na autenticação, tentando próximo método %s não existe. Devo criá-lo? %s tem permissões inseguras! %s é um caminho IMAP inválido %s é um caminho POP inválido %s não é um diretório. %s não é uma caixa de mensagens! %s não é uma caixa de correio. %s está atribuída %s não está atribuída %s não é um arquivo regular. %s não mais existe! %s... Saindo.
 %s: Tipo desconhecido. %s: o terminal não aceita cores %s: comando válido somente para índice, corpo, objetos do cabeçalho %s: tipo de caixa inválido %s: valor inválido %s: valor inválido (%s) %s: não existe tal atributo %s: não existe tal cor %s: não existe esta função %s: não existe tal função no mapa %s: não existe tal menu %s: não existe tal objeto %s: poucos argumentos %s: não foi possível anexar o arquivo %s: não foi possível anexar o arquivo.
 %s: comando desconhecido %s: comando de editor desconhecido (~? para ajuda)
 %s: método de ordenação desconhecido %s: tipo inválido %s: variável desconhecida %sgropo: faltando -rx ou -addr. %sgropo: aviso: IDN ruim '%s'.
 (Termine a mensagem com um . sozinho em uma linha)
 (continuar)
 (i)mbutido ('view-attachments' precisa estar associado a uma tecla!) (nenhuma caixa de mensagens) (r)ejeitar, (a)ceitar uma vez (r)ejeitar, (a)ceitar uma vez, aceitar (s)empre (tamanho %s bytes)  (use '%s' para ver esta parte) *** Início da nota (assinada por: %s) ***
 *** Fim da nota ***
 Assinatura *INVÁLIDA* de: ,  -- Anexos ---Anexo: %s ---Anexo: %s: %s ---Comando: %-20.20s Descrição: %s ---Comando: %-30.30s Anexo: %s -group: nenhum nome de grupo 1: AES128, 2: AES192, 3: AES256  1: DES, 2: Triple-DES  1: RC2-40, 2: RC2-64, 3: RC2-128  468 895 <DESCONHECIDO> <padrão> Algum requisito da política não foi satisfeito
 Occoreu um erro no sistema Autenticação APOP falhou. Cancelar Cancelar mensagem não modificada? Mensagem não modificada cancelada. Aceita a sequência construída Endereço:  Apelido adicionado. Apelidar como:  Apelidos Todos os protocolos disponíveis para conexão TLS/SSL disabilitadas Todas as chaves correspondentes estão expiradas, revogadas, ou desabilitadas. Todas as chaves correspondentes estão marcadas como expirada/revogada. Autenticação anônima falhou. Anexar Anexa um reenviador à sequência Anexa mensagens a %s? O argumento deve ser um número de mensagem. Anexar arquivo Anexando os arquivos escolhidos... Anexo filtrado. Anexo salvo. Anexos Autenticando (%s)... Autenticando (APOP)... Autenticando (CRAM-MD5)... Autenticando (GSSAPI)... Autenticando (SASL)... Autenticando (anônimo)... CRL disponível é muito antigo
 IDN "%s" Incorreto IDN %s inválido quando preparando o reenvio-de. IDN inválido em "%s": '%s' IDN inválido em %s: '%s'
 IDN Inválido: '%s' Formato de arquivo de histórico inválido (linha %d) Nome da caixa de mensagem inválida Expressão regular inválida: %s O fim da mensagem está sendo mostrado. Repetir mensagem para %s Repetir mensagem para:  Repetir mensagens para %s Repetir mensagens marcadas para:  Autenticação CRAM-MD5 falhou. CREATE falhou: %s Não é possível anexar à pasta: %s Não é possível anexar um diretório Não é possível criar %s. Não é possível criar %s: %s Não é possível criar o arquivo %s Não foi possível criar um filtro Impossível criar processo de filtro Não foi possível criar um arquivo temporário Não foi possível decodificar todos os anexos marcados.
Encapsular os demais através de MIME? Não foi possível decodificar todos os anexos marcados.
Encaminhar os demais através de MIME? Impossível decriptar mensagem encriptada! Impossível deletar anexos do servidor POP. Não é possível travar %s.
 Não foi encontrada nenhuma mensagem marcada. Não foi possível obter o type2.list do mixmaster! Não foi possível executar o PGP Não pude casar o nome, continuo? Não foi possível abrir /dev/null Impossível abrir subprocesso do OpenSSL! Não foi possível abrir o subprocesso do PGP! Não é possível abrir o arquivo de mensagens: %s Não foi possível abrir o arquivo temporário %s. Não é possível salvar mensagens na caixa de email POP Impossível assinar: Nenhuma chave especificada. Use Assinar Como. Não foi possível avaliar %s: %s Não foi possível verificar devido à chave perdida ou certificado
 Não é possível visualizar um diretório Impossível escrever cabeçalho no arquivo temporário! Não foi possível gravar a mensagem Impossível escrever mensagem no arquivo temporário! Não foi possível %s: Operação não permitida por ACL Não foi possível criar filtro de visualização Não é possível criar o filtro. Impossível deletar a pasta raiz Não é possível ativar escrita em uma caixa somente para leitura! %s recebido... Saindo.
 Sinal %d recebido... Saindo.
 Falha na verificação da máquina: %s Certificado não é X.509 Certificado salvo Erro (%s) ao verificar certificado Mudanças na pasta serão escritas na saída. Mudanças na pasta não serão escritas Char = %s, Octal = %o, Decimal = %d Conjunto de caracteres alterado para %s; %s. Diretório Mudar para:  Verificar chave   Verificando por novas mensagens... Escolha a família do algorítimo: 1: DES, 2: RC2, 3: AES, ou (c)lear?  Limpa marca Fechando conexão com %s... Fechando a conexão com o servidor POP... Conectando dados... Comando TOP não é suportado pelo servidor. Comando UIDL não é suportado pelo servidor. Comando USER não é suportado pelo servidor. Comando:  Confirmando mudanças... Compilando padrão de busca... Conectando a %s... Conectando com "%s"... Conexão perdida. Reconectar ao servidor POP? Conexão para %s fechada Content-Type alterado para %s. Content-Type é da forma base/sub Continuar? Converter para %s ao enviar? Copiar %s para a caixa de mensagens Copiando %d mensagens para %s... Copiando mensagem %d para %s... Copiando para %s... Copyright (C) 1996-2007 Michael R. Elkins <me@mutt.org>
Copyright (C) 1996-2002 Brandon Long <blong@fiction.net>
Copyright (C) 1997-2008 Thomas Roessler <roessler@does-not-exist.org>
Copyright (C) 1998-2005 Werner Koch <wk@isil.d.shuttle.de>
Copyright (C) 1999-2009 Brendan Cully <brendan@kublai.com>
Copyright (C) 1999-2002 Tommi Komulainen <Tommi.Komulainen@iki.fi>
Copyright (C) 2000-2002 Edmund Grimley Evans <edmundo@rano.org>
Copyright (C) 2006-2009 Rocco Rutte <pdmef@gmx.net>

Muitos outros não mencionados aqui contribuíram com códigos, correções,
e sugestões.
 Copyright (C) 1996-2009 Michael R. Elkins e outros.
Mutt vem ABSOLUTAMENTE SEM QUALQUER GARANTIA. Para detalhes, digite: 'mutt -vv'.
Mutt é software livre e você está convidado a redistribuí-lo sob certas condições. Digite 'mutt -vv' para detalhes.
 Não foi possível conectar a %s (%s). Não foi possível copiar a mensagem Não foi possível criar o arquivo temporário %s Não foi possível criar um arquivo temporário! Não foi possível decodificar a mensagem PGP. Não foi possível encontrar a função de ordenação! [relate este problema] Impossível achar a máquina "%s" Não foi possível descarregar mensagens para o disco Não foi possível incluir todas as mensagens solicitadas! Não foi possível negociar conexão TLS Não foi possível abrir %s Não foi possível reabrir a caixa de mensagens! Não foi possível enviar a mensagem. Não foi possível sincronizar a caixa %s! Não foi possível travar %s
 Criar %s? Somente caixas de correio IMAP suportam a operação criar Criar caixa de mensagem:  DEBUG não foi definido durante a compilação. Ignorado.
 Depurando no nível %d.
 Decodificar e copiar %s para a caixa de mensagens Decodificar e salvar %s na caixa de mensagens Decifrar e copiar %s para a caixa de mensagens Decifrar e gravar %s na caixa de mensagens Decriptando mensagem... Decodificação falhou. Decriptação falhou. Apagar Remover Remove um reenviador da sequência A remoção só é possível para caixar IMAP Deletar mensagens do servidor? Apagar mensagens que casem com:  Deleção de anexos de mensagens encriptadas não é suportado. Descrição Diretório [%s], Máscara de arquivos: %s ERRO: por favor relate este problema Editar mensagem encaminhada? Expressão vazia Encriptar Encriptar com:  Conexão codificada indisponível Entre a senha do PGP: Entre a frase secreta do S/MIME: Entre a keyID para %s:  Entre a keyID:  Digite as chaves (^G para abortar):  Erro alocando a conexão SASL Erro ao retornar mensagem! Erro ao retornar mensagens! Erro conectando ao servidor: %s Erro ao extrair dados da chave!
 Erro ao encontrar a chave de emissão: %s
 Erro em %s, linha %d: %s Erro na linha de comando: %s
 Erro na expressão: %s Erro inicializando dados de certificados do gnutls Erro ao inicializar terminal. Erro ao abrir caixa de mensagem Erro ao interpretar endereço! Erro processando dados do certificado Erro ao ler o arquivo apelido Erro ao executar "%s"! Erro salvando marcadores Erro salvando marcadores. Fechar mesmo assim? Erro ao examinar diretório. Erro ao buscar apelido de arquivo Erro ao enviar a mensagem, processo filho saiu com código %d (%s). Erro ao enviar mensagem, processo filho terminou com código %d
 Erro ao enviar mensagem. Erro no ajuste da força da segurança externa SASL Erro no ajuste do nome do usuário externo SASL Erro no ajuste das propriedades de segurança de SASL Erro ao falar com %s (%s) Erro ao tentar exibir arquivo Erro ao gravar a caixa! Erro. Preservando o arquivo temporário: %s Erro: %s não pode ser usado como reenviador final de uma sequência. Erro: '%s' não é um IDN válido. Erro: Corrente de certificação muito longa - parando aqui
 Erro: falha ao copiar dados
 Erro: decriptação/verificação falhou: %s
 Erro: multipart/signed não tem protocolo. Erro: nenhum socket TLS aberto Erro: score: número inválido Erro: impossível criar subprocesso do OpenSSL! Erro: valor '%s' é inválido para -d.
 Erro: falha na verificação: %s
 Avaliando a memória... Executando comando nas mensagens que casam... Sair Sair   Sair do Mutt sem salvar alterações? Sair do Mutt? Expirado    Falha de limpeza Apagando mensagens do servidor... Falhou ao interpretar remetente Falhou ao encontrar entropia suficiente em seu sistema Falha ao analisar mailto: link
 Falha ao verificar remetente Erro ao abrir o arquivo para interpretar os cabeçalhos. Erro ao abrir o arquivo para retirar cabeçalhos. Falha ao renomear arquivo. Erro fatal! Não foi posssível reabrir a caixa de mensagens! Obtendo chave PGP... Obtendo lista de mensagens... Obtendo cabeçalhos das mensagens... Obtendo mensagem... Máscara de arquivos:  Arquivo existe, (s)obrescreve, (a)nexa ou (c)ancela? O arquivo é um diretório, salvar lá? O arquivo é um diretório, salvar sob ele? [(y)sim, (n)não, (a)tudo] Arquivo no diretório:  Preenchendo armazenamento de entropia: %s...
 Filtrar através de:  Impressão digital:  Impressão digital: %s Primeiro, por favor marque uma mensagem a ser conectada aqui Responder para %s%s? Encaminhar encapsulado em MIME? Encaminhar como anexo? Encaminhar como anexos? Função não permitida no modo anexar-mensagem. Autenticação GSSAPI falhou. Obtendo lista de pastas... Assinatura válida de: Grupo Busca de cabeçalho sem nome do cabeçalho: %s Ajuda Ajuda para %s A ajuda está sendo mostrada. Eu não sei como imprimir isto! Eu não sei como imprimir anexos %s! Erro de I/O ID tem uma validade indefinida. ID está expirado/desabilitado/revogado. Este ID não é válido. Este ID é de baixa confiança. Cabeçalho de S/MIME ilegal Cabeçalho de criptografia ilegal Entrada mal formatada para o tipo %s em "%s" linha %d Incluir mensagem na resposta? Enviando mensagem citada... Inserir Insere um reenviador à sequência Excesso de número inteiro -- não foi possível reservar memória! Excesso de número inteiro -- não foi possível reservar memória. Erro interno. Informe <roessler@does-not-exist.org>. Inválido    URL POP inválida: %s
 URL do SMTP está inválida: %s Dia do mês inválido: %s Codificação inválida Número de índice inválido. Número de mensagem inválido. Mês inválido: %s Data relativa invalida: %s Resposta inválida do servidor Valor inválido para a opção %s: "%s" Executando PGP... Invocando S/MIME... Executando comando de autovisualização: %s Emitido por.:  Pular para mensagem:  Pular para:  O pulo não está implementado em diálogos. Key ID: 0x%s Tipo de Chave ..: %s, %lu bit %s
 Chave Usado .:  Tecla não associada. Tecla não associada. Pressione '%s' para ajuda. LOGIN desabilitado neste servidor. Limitar a mensagens que casem com:  Limitar: %s Limite de travas excedido, remover a trava para %s? Desconectado dos servidores IMAP Efetuando login... Login falhou. Procurando por chaves que casam com "%s"... Procurando %s... Impressão digital MD5 : %s Tipo MIME não definido. Não é possível visualizar o anexo. Laço de macro detectado. Msg Mensagem não enviada. Mensagem enviada. Caixa de correio verificada. Caixa de mensagens fechada Caixa de mensagens criada. Caixa de correio removida. A caixa de mensagens está corrompida! A caixa de mensagens está vazia. A caixa está marcada como não gravável. %s Esta caixa é somente para leitura. A caixa de mensagens não sofreu mudanças A caixa de mensagens deve ter um nome. Caixa de correio não removida. Caixa de mensagens renomeada. A caixa de mensagens foi corrompida! A caixa de mensagens foi modificada externamente. A caixa foi modificada externamente. As marcas podem estar erradas. Caixas [%d] Entrada de edição no mailcap requer %%s Entrada de composição no mailcap requer %%s Criar Apelido Marcando %d mensagens como removidas... Marcando as mensagens apagadas ... Máscara Mensagem repetida. A mensagem não pode ser enviada embutida. Reverter para uso de PGP/MIME? Mensagem contém:
 A mensagem não pôde ser impressa O arquivo de mensagens está vazio. Mensagem não repetida. Mensagem não modificada! Mensagem adiada. Mensagem impressa Mensgem gravada. Mensagens repetidas. As mensagens não puderam ser impressas Mensagens não repetidas. Mensagens impressas Faltam argumentos. Sequências do mixmaster são limitadas a %d elementos. O mixmaster não aceita cabeçalhos Cc ou Bcc. Mover mensagens lidas para %s? Movendo mensagens lidas para %s... Nome ......:  Nova Consulta Nome do novo arquivo:  Novo arquivo:  Novas mensagens em  Novas mensagens nesta caixa. Prox ProxPag Nenhum certificado (valido) foi encontrado para %s. Nenhum cabeçalho Message-ID disponível para conectar o tópico Nenhum autenticador disponível Nenhum parâmetro de fronteira encontrado! [relate este erro] Nenhuma entrada. Nenhum arquivo casa com a máscara Nenhum endereço "De" informado Nenhuma caixa de mensagem para recebimento definida. Nenhum padrão limitante está em efeito. Nenhuma linha na mensagem.
 Nenhuma caixa aberta. Nenhuma caixa com novas mensagens. Nenhuma caixa de mensagens.
 Nenhuma caixa com novas mensagens. Nenhuma entrada de composição no mailcap para %s, criando entrada vazia. Nenhuma entrada de edição no mailcap para %s Nenhum caminho de mailcap especificado Nenhuma lista de email encontrada! Nenhuma entrada no mailcap de acordo encontrada. Exibindo como texto. Nenhuma mensagem naquela pasta. Nenhuma mensagem casa com o critério Não há mais texto citado. Nenhuma discussão restante. Não há mais texto não-citado após o texto citado. Nenhuma mensagem nova no servidor POP. Nenhuma mensagem nova Nenhuma saída do OpenSSL... Nenhuma mensagem adiada. Nenhum comando de impressão foi definido. Nenhum destinatário está especificado! Nenhum destinatário foi especificado.
 Nenhum destinatário foi especificado. Nenhum assunto especificado. Sem assunto, cancelar envio? Sem assunto, cancelar? Sem assunto, cancelado. Não existe tal pasta Nenhuma entrada marcada. Nenhuma mensagem marcada está visível! Nenhuma mensagem marcada. Nenhum tópico conectado Nenhuma mensagem não removida. Nenhuma mensagem não lida Nenhuma mensagem visível. Nenhum Não disponível neste menu. Subexpressões insuficientes para modelo de spam Não encontrado. Não suportado Nada a fazer. OK Um ou mais partes desta mensagem não puderam ser exibidas Somente a deleção de anexos multiparte é suportada. Abrir caixa de correio Abrir caixa somente para leitura Abrir caixa para anexar mensagem de Acabou a memória! Saída do processo de entrega PGP (e)ncriptografar, as(s)inar, (a)ssinar como, am(b)os, %s formato ou ex(c)luir?  PGP (e)ncriptografar, as(s)inar, (a)ssinar como, am(b)os, %s formato ou ex(c)luir?  Chave do PGP %s. PGP já selecionado. Limpar e continuar?  Chaves PGP e S/MIME correspondentes Chaves PGP correspondentes Chaves do PGP que casam com "%s". Chaves do PGP que casam com <%s>. Mensagem PGP decodificada com sucesso. Senha do PGP esquecida. Assinatura PGP não pôde ser verificada. Assinatura PGP verificada com sucesso. PGP/M(i)ME O endereço do assinante verificado pela Associação da Chave Pública é:  Servidor POP não está definido. Registro de tempo inválido para o POP A mensagem pai não está disponível. A mensagem pai não está visível nesta visão limitada. Senha(s) esquecida(s). Senha para %s@%s:  Nome pessoal:  Cano Passar por cano ao comando:  Passar por cano a:  Por favor entre o key ID:  Por favor, defina a variável hostname para um valor adequado quando for
usar o mixmaster! Adiar esta mensagem? Mensagens Adiadas Comando de pré-conexão falhou. Preparando mensagem encaminhada... Pressione qualquer tecla para continuar... PagAnt Imprimir Imprimir anexo? Imprimir mensagem? Imprimir anexo(s) marcado(s)? Imprimir mensagens marcadas? Problema com a assinatura de: Remover %d mensagem apagada? Remover %d mensagens apagadas? Consulta Consulta '%s' Comando de consulta não definido. Consulta:  Sair Sair do Mutt? Lendo %s... Lendo novas mensagens (%d bytes)... Deseja mesmo remover a caixa "%s"? Editar mensagem adiada? A gravação só afeta os anexos de texto. Falha ao renomear: %s Somente caixas de correio IMAP suportam a operação renomear Renomear caixa de mensagens %s para:  Renomear para:  Reabrindo caixa de mensagens... Responder Responder para %s%s? Ordem-Rev (d)ata/r(e)metente/(r)eceb/a(s)sunto/(p)ara/(t)hread/de(s)ord/(t)am/pont(u)a/s(p)am?:  Procurar de trás para frente por:  Ordem inversa por (d)ata, (a)lfa, (t)amanho ou (n)ão ordenar?  Revogado    S/MIME d(e)codificar, a(s)sinar, codificar com(n), assin(a)r como, am(b)os, ou limpar(c)?  S/MIME já selecionado. Limpar e continuar?  Proprietário do certificado S/MIME não é igual ao remetente. Chaves do S/MIME correspondentes à "%s". Chaves S/MIME correspondentes Mensagens S/MIME sem dicas do conteúdo não são suportadas. Assinatura S/MIME não pôde ser verificada. Assinatura S/MIME verificada com sucesso. Autenticação SASL falhou Autenticação SASL falhou. Impressão digital SHA1: %s Autenticação SMTP requer SASL Servidor SMTP não suporta autenticação O envio de mensagem por SMTP falhou: %s Sessão SMTP falhou: leia o erro O envio de mensagem por SMTP falhou: incapaz de abrir %s Sessão SMTP falhou: escreva o erro Verificação do Certificado SSL (certificado %d de %d em fila) SSL desabilitado devido a falta de entropia SSL falhou: %s SSL está indisponível. Conexão SSL/TLS usando %s (%s/%s/%s) Salvar Salvar uma cópia desta mensagem? Salvar anexos em Fcc? Salvar em arquivo:  Gravar %s na caixa de mensagens Salvando mensagens alteradas... [%d /%d] Salvando... Procurando: %s... Busca Procurar por:  A busca chegou ao fim sem encontrar um resultado A busca chegou ao início sem encontrar um resultado Busca interrompida. A busca não está implementada neste menu. A pesquisa passou para o final. A pesquisa voltou ao início. Searching... Proteger conexão com TLS? Escolher Escolher   Escolha uma sequência de reenviadores. Seleciona o próximo elemento da sequência Seleciona o elemento anterior da sequência Selecionando %s... Enviar Enviando em segundo plano. Enviando mensagem... Número serial .: 0x%s
 Certificado do servidor expirou Certificado do servidor ainda não está validado O servidor fechou a conexão! Atribui marca Comando do shell:  Assinar Assinar como:  Assinar, Encriptar Ordem (d)ata/r(e)metente/(r)eceb/a(s)sunto/(p)ara/(t)hread/de(s)ord/(t)am/pont(u)a/s(p)am?:  Ordenar por (d)ata, (a)lfa, (t)amanho ou (n)ão ordenar?  Ordenando caixa... Subchave ....: 0x%s [%s] assinada, Máscara de arquivos: %s Assinando %s Assinando %s... Marcar mensagens que casem com:  Marque as mensagens que você quer anexar! Não é possível marcar. Aquela mensagem não está visível. O CRL não está disponível
 O anexo atual será convertido O anexo atual não será convertido. O índice de mensagens está incorreto. Tente reabrir a caixa de mensagem. A sequência de reenviadores já está vazia. Não há anexos. Não há mensagens. Não há anexos para mostrar! Este servidor IMAP é pré-histórico. Mutt não funciona com ele. Este certificado pertence a: Este certificado é válido Este certificado foi emitido por: Esta chave não pode ser usada: expirada/desabilitada/revogada. Tópico quebrado Thread não pode ser quebrada, mensagem não é parte da thread. A discussão contém mensagens não lidas. Separar discussões não está ativado. Tópicos conectados Limite de tempo excedido durante uma tentativa de trava com fcntl! Limite de tempo excedido durante uma tentativa trava com flock! Para entrar em contato com os desenvolvedores, envie um email para <mutt-dev@mutt.org>.
Para relatar um erro, por favor acesse http://bugs.mutt.org/.
 Para ver todas as mensagens, limite para "todas". Alternar exibição de sub-partes O início da mensagem está sendo mostrado. Confiável    Tentando extrair chaves PGP...
 Tentando extrair certificados S/MIME...
 Erro de tunel conectando a %s: %s Tunel para %s retornou o erro %d (%s) Não foi possível anexar %s! Não foi possível anexar! Não foi possível obter cabeçalhos da versão deste servidor IMAP. Não foi possível obter o certificado do servidor remoto Não foi possível deixar mensagens no servidor. Não foi possível travar a caixa de mensagens! Não foi possível abrir o arquivo temporário! Restaurar Restaurar mensagens que casem com:  Desconhecido Desconhecido    Content-Type %s desconhecido Perfil SASL desconhecido Cancelando assinatura de %s Cancelando assinatura de %s... Desmarcar mensagens que casem com:  Não verificado Enviando mensagem... Uso: set variável = yes|no Use 'toggle-write' para reabilitar a gravação! Usar keyID = "%s" para %s? Nome de usuário em %s:  Válido de: %s
 Válido Para ..: %s
 Verificado   Verificar assinatura de PGP? Verificando índices das mensagens... Ver Anexo AVISO! Você está prestes a sobrescrever %s, continuar? AVISO: NÃO é seguro que a chave pertença a pessoa indicada acima
 AVISO: entrada PKA não casa com endereço do assinante:  AVISO: Certificado do servidor foi revogado AVISO: Certificado do servidor expirou AVISO: Certificado do servidor ainda não é válido AVISO: Nome do servidor não corresponde ao certificado AVISO: Sinalizador do certificado do servidor não é CA AVISO: A chave NÃO PERTENCE à pessoa chamada como mostrado acima
 AVISO: Não dispomos de NENHUMA indicação de que a chave pertence à pessoa indicada acima
 Esperando pela trava fcntl... %d Esperando pela tentativa de flock... %d Agurdando pela resposta... Aviso: '%s' é um IDN inválido. Aviso: Pelo menos uma chave de certificação expirou
 Aviso: IDN '%s' inválido no apelido '%s'.
 Aviso: Não foi possível salvar o certificado Aviso: Uma das chaves foi revogada
 Aviso: Parte desta mensagem não foi assinada. Aviso: Certificado do servidor foi assinado usando um algoritmo inseguro Aviso: A chave usada na criação da assinatura expirou em:  Aviso: A assinatura expira em:  Aviso: Este apelido pode não funcionar. Consertá-lo? Aviso: a mensagem não contem o cabeçalho De: O que temos aqui é uma falha ao criar um anexo Erro de gravação! Caixa parcial salva em %s Erro de gravação! Gravar mensagem na caixa Gravando %s... Gravando mensagem em %s... Você já tem um apelido definido com aquele nome! O primeiro elemento da sequência já está selecionado. O último elemento da sequência já está selecionado. Você está na primeira entrada. Você está na primeira mensagem. Você está na primeira página Você está na primeira discussão. Você está na última entrada. Você está na última mensagem. Você está na última página. Você não pode mais descer. Você não pode mais subir Você não tem apelidos! Você não pode apagar o único anexo. Você só pode repetir partes message/rfc822 [%s =%s] Aceita? [-- Saída de %s a seguir %s --]
 [-- %s/%s não é aceito  [-- Anexo No.%d [-- Saída de erro da autovisualização de %s --]
 [-- Autovisualizar usando %s --]
 [-- INÍCIO DE MENSAGEM DO PGP --]

 [-- INÍCIO DE BLOCO DE CHAVE PÚBLICA DO PGP --]
 [-- INÍCIO DE MENSAGEM ASSINADA POR PGP --]

 [-- Início da informação da assinatura --]
 [-- Impossível executar %s. --]
 [-- FIM DE MENSAGEM PGP --]
 [-- FIM DE BLOCO DE CHAVE PÚBLICA DO PGP --]
 [-- FIM DE MENSAGEM ASSINADA POR PGP --]
 [-- Fim da saída do OpenSSL --]

 [-- Fim da saída do PGP --]

 [-- Fim dos dados encriptados com PGP/MIME --]
 [-- Fim dos dados assinados e encriptados com PGP/MIME --]
 [-- Fim dos dados codificados com S/MIME --]
 [-- Fim dos dados assinados S/MIME --]
 [-- Fim da informação da assinatura --]

 [-- Erro: Não foi possível exibir nenhuma parte de Multipart/Aternative! --]
 [-- Erro: Estrutura multipart/signed inconsistente! --]

 [-- Erro: Protocolo multipart/signed %s desconhecido! --]

 [-- Erro: não foi possível criar um subprocesso para o PGP! --]

 [-- Erro: não foi possível criar um arquivo temporário! --]
 [-- Erro: não foi possível encontrar o início da mensagem do PGP! --]

 [-- Erro: decriptação falhou: %s --]

 [-- Erro: message/external-body não tem nenhum parâmetro access-type --]
 [-- Erro: impossível criar o subprocesso do OpenSSL! --]
 [-- Erro: não foi possível criar o subprocesso do PGP! --]
 [-- Os dados a seguir estão encriptados com PGP/MIME --]

 [-- Os dados seguintes estão assinados e encriptados com PGP/MIME --]

 [-- Os dados a seguir estão encriptados com PGP/MIME --]
 [-- Os dados a seguir estão codificados com S/MIME --]

 [-- Os dados a seguir estão assinados com S/MIME --]
 [-- Os dados a seguir estão assinados com S/MIME --]

 [-- Os dados a seguir estão assinados --]

 [-- Este anexo %s/%s  [-- Este anexo %s/%s não está incluído, --]
 [-- Isto é um anexo  [-- Tipo: %s/%s, Codificação: %s, Tamanho: %s --]
 [-- Aviso: Não foi possível encontrar nenhuma assinatura. --]

 [-- Aviso: Não foi possível verificar %s de %s assinaturas. --]

 [-- e o tipo de acesso indicado %s não é suportado --]
 [-- e a fonte externa indicada --]
[-- expirou. --]
 [-- nome: %s --]
 [-- em %s --]
 [Não se pode mostrar o ID deste usuário (DN inválido)] [Não se pode mostrar o ID deste usuário (codificação inválida)] [Não se pode mostrar o ID deste usuário (codificação desconhecida)] [Desabilitado] [Expirado] [inválido] [Revogado] [data inválida] [impossível calcular] aka:  apelido: sem endereço especificação indefinida da chave secreta `%s'
 anexa os resultados da nova busca aos resultados atuais aplica a próxima função apenas às mensagens marcadas aplica a próxima função às mensagens marcadas anexa uma chave pública do PGP attach file(s) to this message anexa uma(s) mensagem(ns) à esta mensagem anexos: disposição inválida anexos: sem disposição bind: muitos argumentos quebra o tópico em dois não posso obter o nome comum do certificado não posso obter o assunto do certificado transforme a palavra toda para letras maiúsculas o dono do certificado não corresponde à máquina %s Certificação muda de diretório checar por PGP clássico verifica se há novas mensagens na caixa retira uma marca de estado de uma mensagem limpa e redesenha a tela abre/fecha todas as discussões abre/fecha a discussão atual color: poucos argumentos completa um endereço com uma pesquisa completa um nome de arquivo ou apelido compõe uma nova mensagem eletrônica compõe um novo anexo usando a entrada no mailcap converte a palavra para minúsculas converte a palavra para maiúsculas convertendo copia uma mensagem para um arquivo/caixa Não foi possível criar o arquivo temporário: %s impossível truncar pasta de mensagens temporária: %s Não foi possível criar a caixa temporária: %s cria uma nova caixa de correio (só para IMAP) cria um apelido a partir do remetente de uma mensagem criado:  circula entre as caixas de mensagem datn cores pré-definidas não suportadas apaga todos os caracteres na linha apaga todas as mensagens na sub-discussão apaga todas as mensagens na discussão apaga os caracteres a partir do cursor até o final da linha apaga os caracteres a partir do cursor até o final da palavra apagar mensagem apagar mensagem(ens) apaga mensagens que casem com um padrão apaga o caractere na frente do cursor apaga o caractere sob o cursor apaga a entrada atual apaga a caixa de correio atual (só para IMAP) apaga a palavra em frente ao cursor dfrsotuzcp mostra uma mensagem mostra o endereço completo do remetente mostra a mensagem e ativa/desativa poda de cabeçalhos mostra o nome do arquivo atualmente selecionado mostrar a chave-código para a chave impressa drac dt edita o tipo de anexo edita a descrição do anexo edita o código de transferência do anexo edita o anexo usando sua entrada no mailcap editar mensagem edita a lista de Cópias Escondidas (BCC) edita a lista de Cópias (CC) edita o campo Reply-To edita a lista de Destinatários (To) edita o arquivo a ser anexado edita o campo From edita a mensagem edita a mensagem e seus cabeçalhos edita a mensagem pura edita o assunto desta mensagem padrão vazio encriptação fim da execução condicional (noop) entra uma máscara de arquivos informe um arquivo no qual salvar uma cópia desta mensagem entra um comando do muttrc erro ao adicionar destinatário `%s': %s
 erro ao alocar dados no objeto: %s
 erro na criação do contexto gpgme: %s
 erro na criação do objeto de dados gpgme: %s
 erro ao habilitar protocolo CMS: %s
 erro ao codificar dados: %s
 erro na expressão erro no padrão em: %s erro ao ler dados do objeto: %s
 erro ao reverter objeto de dados: %s
 erro de configuração da assinatura PKA: %s
 erro ao definir chave secreta `%s': %s
 erro ao assinar dados: %s
 erro: operação %d desconhecida (relate este erro). esabfc esabfci esabmfc esabpfc eswabfc exec: sem argumentos executa um macro sai deste menu a extração suportou chaves públicas filtra o anexo através de um comando do shell sinalizar mensagem forçar recuperação das mensagens do servidor IMAP forçar a visualizaçãdo do anexo usando o mailcap erro de formatação encaminha uma mensagem com comentários obtém uma cópia temporária do anexo gpgme_new falhou: %s gpgme_op_keylist_next falhou: %s gpgme_op_keylist_start falhou: %s foi apagado --]
 imap_sync_mailbox: LIMPAR falhou campo de cabeçalho inválido executa um comando em um subshell pula para um número de índice pula para a mensagem pai na discussão pula para a sub-discussão anterior pula para a discussão anterior pula para o início da linha pula para o fim da mensagem pula para o final da linha pula para a próxima mensagem nova pula para a próxima mensagem nova ou não lida pula para a próxima sub-discussão pula para a próxima discussão pula para a próxima mensagem não lida pula para a mensagem nova anterior pula para a mensagem anterior ou não lida pula para a mensagem não lida anterior volta para o início da mensagem chaves correspondentes associar mensagens marcadas à mensagem atual endereço de uma série de mensagens listar caixas de mensagens com novas mensagens desconectar de todos os servidores IMAP macro: seqüência de teclas vazia macro: muitos argumentos envia uma chave pública do PGP entrada no mailcap para o tipo %s não foi encontrada. maildir_commit_message(): não foi possível marcar o horário no aquivo cria uma cópia decodificada (text/plain) cria uma cópia decodificada (text/plain) e apaga cria cópia desencriptada cria cópia desencriptada e apaga marcar mensagem(ens) como lida marca a sub-discussão atual como lida marca a discussão atual como lida faltando colchete: \"%s\" parêntese sem um corresponente: %s falta o nome do arquivo.
 faltam parâmetros faltando padrão: %s mono: poucos argumentos move a entrada para o fundo da tela move a entrada para o meio da tela move a entrada para o topo da tela move o cursor um caractere para a esquerda move o cursor um caractere para a direita mover o cursor para o início da palavra mover o cursor para o fim da palavra anda até o fim da página anda até a primeira entrada anda até a primeira mensagem anda até a última entrada anda até a última mensagem anda até o meio da página anda até a próxima entrada anda até a próxima página anda até a próxima mensagem não apagada anda até a entrada anterior anda até a página anterior anda até a mensagem não apagada anterior anda até o topo da página mensagem multiparte não tem um parâmetro de fronteiras! mutt_restore_default(%s): erro na expressão regular: %s
 não nenhum arquivo de certificado nenhuma caixa de mensagens nospam: nenhum padrão coincidente não convertendo seqüência de teclas nula operação nula estouro numérico sac abre uma pasta diferente abre uma pasta diferente somente para leitura abrir próxima caixa de correio com novo e-mail opções:
  -A <alias>	expande o dado apelido
  -a <file> [...] --	anexar arquivo(s) à mensagem
		a lista de arquivos deve ser terminada com a sequência "--"
  -b <address>	especifica um endereço de cópia de carbono oculta (CCO)
  -c <address>	especifica um endereço de cópia de carbono (CC)
  -D		imprime o valor de todas as variáveis para stdout poucos argumentos passa a mensagem/anexo para um comando do shell através de um cano prefixo é ilegal com reset imprime a entrada atual push: muitos argumentos executa uma busca por um endereço através de um programa externo põe a próxima tecla digitada entre aspas edita uma mensagem adiada re-envia uma mensagem para outro usuário renomeia a caixa de correio atual (só para IMAP) renomeia/move um arquivo anexado responde a uma mensagem responde a todos os destinatários responde à lista de email especificada obtém mensagens do servidor POP ra ras executa o ispell na mensagem salva as mudanças à caixa salva mudanças à caixa e sai salvar mensagem/anexo para uma caixa de mensagem/arquivo salva esta mensagem para ser enviada depois score: poucos argumentos score: muitos argumentos passa meia página desce uma linha Rolar para baixo na lista de histórico volta meia página sobe uma linha volta uma página no histórico procura de trás para a frente por uma expressão regular procura por uma expressão regular procura pelo próximo resultado procura pelo próximo resultado na direção oposta chave secreta `%s' não encontrada: %s
 escolhe um novo arquivo neste diretório seleciona a entrada atual envia a mensagem envia a mensagem através de uma sequência de reenviadores mixmaster atribui uma marca de estado em uma mensagem mostra anexos MIME mostra as opções do PGP mostra as opções S/MIME mostra o padrão limitante atualmente ativado mostra somente mensagens que casem com um padrão mostra o número e a data da versão do Mutt assinando pula para depois do texto citado ordena mensagens ordena mensagens em ordem reversa source: erro em %s source: erros em %s source: leitura abortada devido ao excesso de erros em %s source: muitos argumentos spam: nenhum padrão coincidente assina à caixa de correio atual (só para IMAP) sync: mbox modificada, mas nenhuma mensagem modificada! (relate este problema) marca mensagens que casem com um padrão marca a entrada atual marca a sub-discussão atual marca a discussão atual esta tela troca a marca 'importante' da mensagem troca a marca 'nova' de uma mensagem troca entre mostrar texto citado ou não troca a visualização de anexos/em linha nova troca ativa/desativa recodificação deste anexo troca entre mostrar cores nos padrões de busca ou não troca ver todas as caixas/só as inscritas (só para IMAP) troca entre reescrever a caixa ou não troca entre pesquisar em caixas ou em todos os arquivos troca entre apagar o arquivo após enviá-lo ou não poucos argumentos muitos argumentos troca o caractere sob o cursor com o anterior não foi possível determinar o diretório do usuário não foi possível determinar o nome do usuário Sem anexos: disposição inválida sem anexos: sem disposição restaura todas as mensagens na sub-discussão restaura todas as mensagens na discussão desapagar mensagem desapagar mensagem(ens) restaura mensagens que casem com um padrão restaura a entrada atual unkook: Não é possível excluir um %s de dentro de um %s. unhook: Não é possível fazer 'unhook *' de dentro de um hook. unhook: tipo de gancho desconhecido: %s erro desconhecido cancela assinatura da caixa atual (só para IMAP) desmarca mensagens que casem com um padrão atualiza a informação de codificação de um anexo usao: mutt [<opções>] [-z] [-f <arquivo> | -yZ]
       mutt [<opções>] [-x] [-Hi <arquivo>] [-s <assunto>] [-bc <addr>] [-a <arquivo> [...] --] <addr> [...]
       mutt [<opções>] [-x] [-s <subj>] [-bc <addr>] [-a <arquivo> [...] --] <addr> [...] < mensagem
       mutt [<opções>] -p
       mutt [<opções>] -A <alias> [...]
       mutt [<opções>] -Q <consulta> [...]
       mutt [<opções>] -D
       mutt -v[v]
 usa a mensagem atual como modelo para uma nova mensagem valor é ilegal com reset verifica uma chave pública do PGP ver anexo como texto vê anexo usando sua entrada no mailcap se necessário vê arquivo vê a identificação de usuário da chave retira a(s) senha(s) da memória grava a mensagem em uma pasta sim snt {interno} ~q		salva o arquivo e sai do editor

~r file		ler um arquivo dentro do editor

~t users	adiciona usuários no campo To: 

~u		retorna a linha anterior

~v		edita a mensagem com o editor $visual

~w file		grava a mesangem no arquivo

~x		cancela alterações e saia do editor

~?		esta mensagem

.		a entrada finaliza na mesma linha
 