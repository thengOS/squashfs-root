��          4      L       `   j  a      �  �  �  �  �     !                    Usage:
tasksel install <task>...
tasksel remove <task>...
tasksel [options]
	-t, --test          test mode; don't really do anything
	    --new-install   automatically install some tasks
	    --list-tasks    list tasks that would be displayed and exit
	    --task-packages list available packages in a task
	    --task-desc     returns the description of a task
 apt-get failed Project-Id-Version: tasksel
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-02 02:46+0000
PO-Revision-Date: 2016-01-02 11:25+0000
Last-Translator: Felipe Augusto van de Wiel (faw) <Unknown>
Language-Team: l10n portuguese <debian-l10n-portuguese@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:37+0000
X-Generator: Launchpad (build 18115)
Language: 
 Uso:
tasksel install <tarefa>...
tasksel remove <tarefa>...
tasksel [opções]
	-t, --test          modo de teste; não faz nada de verdade
	    --new-install   instala automaticamente algumas tarefas
	    --list-tasks    lista tarefas que seriam exibidas e finaliza
	    --task-packages lista pacotes disponíveis em uma tarefa
	    --task-desc     retorna a descrição de uma tarefa
 apt-get falhou 