��          �      ,      �     �     �     �     �  �   �  �   �  ~        �  /   �     �     �     �  ?        C     Q  
   c  *  n     �     �  !   �     �  �     �   �  �   7  
   �  6   �     �     	     '	  C   >	     �	     �	     �	        	                                                                  
    %s (%s) <small><b>Action:</b></small> <small><b>Vendor:</b></small> <small><b>_Details</b></small> An application is attempting to perform an action that requires privileges. Authentication as one of the users below is required to perform this action. An application is attempting to perform an action that requires privileges. Authentication as the super user is required to perform this action. An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Authenticate Authentication dialog was dismissed by the user Click to edit %s Click to open %s Select user... Your authentication attempt was unsuccessful. Please try again. _Authenticate _Password for %s: _Password: Project-Id-Version: policykit-gnome
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=policykit-gnome&component=general
POT-Creation-Date: 2015-12-04 00:05+0000
PO-Revision-Date: 2012-01-26 20:13+0000
Last-Translator: Rafael Neri <Unknown>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:41+0000
X-Generator: Launchpad (build 18115)
X-Poedit-Country: BRAZIL
X-Poedit-Language: Portuguese
 %s (%s) <small><b>Ação:</b></small> <small><b>Fornecedor:</b></small> <small><b>_Detalhes</b></small> Um aplicativo está tentando executar uma ação que requer privilégios. É necessária autenticação como um dos usuários abaixo para executar essa ação. Um aplicativo está tentando executar uma ação que requer privilégios. É necessária autenticação como superusuário para executar esta ação. Um aplicativo está tentando executar uma ação que requer privilégios. É necessária autenticação para executar esta ação. Autenticar Diálogo de autenticação foi rejeitado pelo usuário Clique para editar %s Clique para abrir %s Escolha um usuário... Sua tentativa de autenticação falhou. Por favor, tente novamente. _Autenticar _Senha para %s: _Senha: 