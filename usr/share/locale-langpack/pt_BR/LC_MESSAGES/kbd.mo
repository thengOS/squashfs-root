��    /     �	  �        p  V   q      �     �  6     5   ;  )   q  H  �  
   �  2   �     "     0  �   <          ,  E   F     �     �     �     �     �  P        i  *   �  0   �     �  &   �  !      9   A   T   {   #   �   !   �   2   !     I!  $   e!     �!     �!     �!     �!  &   �!     "  $   6"  #   ["  /   "  %   �"  ,   �"  $   #      '#  !   H#  &   j#  �  �#  H   C%     �%     �%  .   �%      �%     &     !&     :&     X&     x&     �&  g   �&  g   '     t'     �'     �'  !   �'  $   �'  <   (  	   C(     M(     e(     {(     �(     �(     �(  9   �(     )     )     4)     J)     d)     ~)     �)     �)     �)     �)     �)     *     &*     >*  #   ]*  7   �*  8   �*     �*     +     +     2+     K+     d+  .   }+     �+  G   �+  1   ,  <   8,  8   u,  !   �,     �,     �,     -  4   -  0   S-  +   �-      �-     �-     �-     .     $.     9.  5   P.     �.  K   �.  '   �.  &   /     :/     Y/     p/     �/     �/      �/  -   �/     0  (   '0     P0  !   \0  6   ~0  /   �0  )   �0  !   1     11     L1     j1     �1     �1  !   �1  =   �1     2     &2     =2  !   T2     v2  /   �2     �2  %   �2     3  $   3     A3     [3     v3     �3  .   �3     �3  3   �3     4  -   :4  ;   h4  (   �4  ,   �4  '   �4  %   "5  ,   H5  |   u5  (   �5     6  &   66  /   ]6     �6     �6     �6  &   �6  G   �6     @7     ^7  "   |7     �7  /  �7  �   �9  �   �:  �  �;  	  H>  .   R?  �  �?  :   [D     �D     �D  =   �D  :   �D  9   :E  B   tE  f   �E  ,   F     KF  !   kF     �F     �F  "   �F     �F     G     !G     9G     QG  #   iG  '   �G      �G     �G     �G  !   H     )H     =H     UH  (   gH     �H  
   �H     �H     �H     �H     �H     �H     �H  !   I      -I  "   NI  *   qI     �I  (   �I  -   �I  "   J  (   1J  /   ZJ     �J     �J     �J     �J  &   �J     K     K     5K     EK  4   bK     �K      �K      �K  h  �K  +   GN  #   sN  "   �N  4   �N     �N  ,   �N  0   +O  4   \O  "   �O     �O     �O     �O     �O  >   �O  ,   P     =P  �   EP  *   �P  (   Q  K   0Q  $   |Q  O   �Q     �Q    R     $S  $   7S     \S     cS     sS     {S     �S  !   �S     �S  $   �S     T  .   +T     ZT     oT     �T  
   �T  &   �T     �T     �T  *   �T     U  �   6U  G  �U     W  7   W  �  QW  p   Y  1   yY     �Y  9   �Y  E   �Y  .   EZ  �  tZ  
   /]  B   :]     }]  	   �]  �   �]  !   |^     �^  a   �^  #    _  #   D_  "   h_  #   �_  $   �_  S   �_      (`  8   I`  6   �`     �`  :   �`  3   a  <   Aa  h   ~a  ,   �a  '   b  1   <b  &   nb  -   �b  !   �b  3   �b     c     .c  <   Ic  9   �c  .   �c  6   �c  ?   &d  0   fd  <   �d  $   �d  2   �d  <   ,e  +   ie  �  �e  h   �g     �g     h  2   h  1   Nh     �h     �h     �h     �h     �h     i  z   1i  u   �i     "j  ,   0j  ,   ]j  -   �j  2   �j  a   �j     Mk     \k     uk  !   �k  "   �k  )   �k     �k  L   l     bl  )   l  (   �l  -   �l  +    m  "   ,m  /   Om  )   m  $   �m  ,   �m  :   �m     6n     Rn      rn  1   �n  H   �n  F   o     Uo     ro      �o      �o     �o     �o  :   p     >p  ^   Np  ;   �p  T   �p  :   >q  !   yq  #   �q  !   �q      �q  6   r  2   9r  -   lr  %   �r      �r  "   �r     s     s  !   9s  D   [s     �s  e   �s  *   #t  )   Nt      xt     �t     �t  &   �t     �t  .   u  <   ?u  )   |u  7   �u     �u  +   �u  B   v  =   \v  7   �v  &   �v  #   �v  %   w  !   Cw  !   ew     �w  9   �w  Z   �w  #   5x  #   Yx     }x  %   �x  #   �x  C   �x  )   &y  1   Py     �y  6   �y  '   �y     �y     z     ,z  2   Fz     yz  <   �z     �z  :   �z  M   *{  (   x{  <   �{  7   �{  +   |  (   B|  �   k|  *   �|  %   (}  +   N}  M   z}  %   �}     �}  *   
~  0   5~  r   f~  $   �~  ,   �~  )   +  3   U  �  �    !�  �   *�  �  	�  :  ņ  E    �    F�  F   c�     ��  $   ��  Q   ܎  ;   .�  <   j�  N   ��  �   ��  N   x�  &   ǐ  +   �  &   �  4   A�  $   v�     ��     ��     Б     �     �  4   �  1   S�  3   ��  $   ��  2   ޒ  (   �     :�     Q�     m�  8   ��     ��     ߓ     �     ��     �     �     �  "   1�  $   T�  $   y�  $   ��  (   Ô     �  N   	�  8   X�  1   ��  S   Õ  M   �     e�     ��  0   ��     Җ  /   �     �  $   �     =�     V�  ;   t�     ��  '   ��  '   ߗ  �  �  '   ǚ  7   �  .   '�  7   V�     ��  8   ��  6   ؛  2   �  +   B�  	   n�     x�     ��  	   ��  [   ��  2   ��     ,�  �   4�  H   ޝ  9   '�  \   a�  "   ��  ~   �  -   `�  G  ��     ֠  #   �     �     �     +�     3�  )   G�  '   q�  "   ��  0   ��  !   ��  a   �     q�     ��     ��     ¢  9   Ԣ     �     �  6   6�     m�  �   ��  �  ]�     �  ?   ��         �   X   v   �              �   �   .         �   �   �      /  �       �   �   &  �       m   t               0   u           <   �             �   +   l       =          �   ^   �   �   o   �   %  �       �                         �   �   �   E     y   D   }   .   �       �   $       �   �   9   �         �   b   _   �         �       B           p   r               N   �   Y     ,      g       �         #   �       �   h   �      �   6             �   2   �   M   �   �       #                           �           �   4   *  �      �   a   �      �   *          �   �   k       e   �       |   �   �   �   �   �   f       %      i       �   �           Q   �       �   �       G   �   �      x   �                 �   	               �   �       �   $  �       (  \      �                               �   �             �   �   �      �   �   �                            !   �   C       L       �   -   [   �      �         �       z       3   �     ]   
              �     �     �   �   �   H   �   �   �   )  w   5      /     �   �   �   ;   '  �   q   �       �       �   P   �   �      �   A   +  -      �   	     "  �       �   :   ,       �   S       �           �           ~   �   U   �   �      )   �   s   @           �   �   j         �   >       T   !  J   F       R   7   �       I   
  �   �   {               n   �   `   K               "   �       O   �   c   �   �       �         ?     (         1   �           W          �             �     d   &   �   V       �         �   8   '       Z    			    interpret character action codes to be from the
			    specified character set
 

Escaped scancodes e0 xx (hex)
 
Changed %d %s and %d %s.
 
Press any keys - Ctrl-D will terminate this program

 
Recognized modifier names and their column numbers:
 
The following synonyms are recognized:

 
usage: dumpkeys [options...]

valid options are:

	-h --help	    display this help text
	-i --short-info	    display information about keyboard driver
	-l --long-info	    display above and symbols known to loadkeys
	-n --numeric	    display keytable in hexadecimal notation
	-f --full-table	    don't use short-hand notations, one row per keycode
	-1 --separate-lines one line per (modifier,keycode) pair
	   --funcs-only	    display only the function key strings
	   --keys-only	    display only key bindings
	   --compose-only   display only compose key combinations
	-c --charset=     FAILED # not alt_is_meta: on keymap %d key %d is bound to %-15s for %s
 %s from %s
 %s version %s

Usage: %s [options]

Valid options are:

	-h --help            display this help text
	-V --version         display program version
	-n --next-available  display number of next unallocated VT
 %s: %s: Warning: line too long
 %s: 0: illegal VT number
 %s: Bad Unicode range corresponding to font position range 0x%x-0x%x
 %s: Bad call of readpsffont
 %s: Bad end of range (0x%lx)
 %s: Bad end of range (0x%x)
 %s: Bad input line: %s
 %s: Bad magic number on %s
 %s: Corresponding to a range of font positions, there should be a Unicode range
 %s: Error reading input font %s: Glyph number (0x%lx) past end of font
 %s: Glyph number (0x%x) larger than font length
 %s: Illegal vt number %s: Input file: bad input length (%d)
 %s: Input file: trailing garbage
 %s: Options --unicode and --ascii are mutually exclusive
 %s: Unicode range U+%x-U+%x not of the same length as font position range 0x%x-0x%x
 %s: Unsupported psf file mode (%d)
 %s: Unsupported psf version (%d)
 %s: VT 1 is the console and cannot be deallocated
 %s: Warning: line too long
 %s: addfunc called with bad func %d
 %s: addfunc: func_buf overflow
 %s: background will look funny
 %s: bad utf8
 %s: bug in do_loadtable
 %s: cannot deallocate or clear keymap
 %s: cannot find any keymaps?
 %s: could not deallocate console %d
 %s: could not deallocate keymap %d
 %s: could not return to original keyboard mode
 %s: could not switch to Unicode mode
 %s: deallocating all unused consoles failed
 %s: error reading keyboard mode: %m
 %s: error setting keyboard mode
 %s: font position 32 is nonblank
 %s: input font does not have an index
 %s: locks virtual consoles, saving your current session.
Usage: %s [options]
       Where [options] are any of:
-c or --current: lock only this virtual console, allowing user to
       switch to other virtual consoles.
-a or --all: lock all virtual consoles by preventing other users
       from switching virtual consoles.
-v or --version: Print the version number of vlock and exit.
-h or --help: Print this help message and exit.
 %s: not loading empty unimap
(if you insist: use option -f to override)
 %s: out of memory
 %s: out of memory?
 %s: plain map not allocated? very strange ...
 %s: psf file with unknown magic
 %s: short ucs2 unicode table
 %s: short unicode table
 %s: short utf8 unicode table
 %s: trailing junk (%s) ignored
 %s: unknown option
 %s: unknown utf8 error
 %s: warning: loading Unicode keymap on non-Unicode console
    (perhaps you want to do `kbd_mode -u'?)
 %s: warning: loading non-Unicode keymap on Unicode console
    (perhaps you want to do `kbd_mode -a'?)
 %s: wiped it
 %s: zero input character size?
 %s: zero input font length?
 '%s' is not a function key symbol (No change in compose definitions.)
 0 is an error; for 1-88 (0x01-0x58) scancode equals keycode
 ?UNKNOWN? Activation interrupted? Appended Unicode map
 Bad character height %d
 Bad character width %d
 Bad input file size
 Bad input line: %s
 Cannot check whether vt %d is free; use `%s -f' to force. Cannot find %s
 Cannot find %s font
 Cannot find a free vt Cannot find default font
 Cannot open %s read/write Cannot open /dev/port Cannot open font file %s
 Cannot read console map
 Cannot stat map file Cannot write font file Cannot write font file header Character count: %d
 Couldn't activate vt %d Couldn't deallocate console %d Couldn't find owner of current tty! Couldn't get a file descriptor referring to the console Couldn't get a file descriptor referring to the console
 Couldn't open %s
 Couldn't read VTNO:  Current default flags:   Current flags:           Current leds:            Error opening /dev/kbd.
 Error parsing symbolic map from `%s', line %d
 Error reading %s
 Error reading current flags setting. Maybe you are not on the console?
 Error reading current led setting from /dev/kbd.
 Error reading current led setting. Maybe stdin is not a VT?
 Error reading current setting. Maybe stdin is not a VT?
 Error reading map from file `%s'
 Error resetting ledmode
 Error writing map to file
 Error writing screendump
 Error: %s: Insufficient number of fields in line %u. Error: %s: Invalid value in field %u in line %u. Error: %s: Line %u has ended unexpectedly.
 Error: %s: Line %u is too long.
 Error: Not enough arguments.
 Error: Unrecognized action: %s
 Font height    : %d
 Font width     : %d
 Found nothing to save
 Hmm - a font from restorefont? Using the first half.
 Invalid number of lines
 It seems this kernel is older than 1.1.92
No Unicode mapping table loaded.
 KDGKBENT error at index %d in table %d
 KDGKBENT error at index 0 in table %d
 KDGKBSENT failed at index %d:  KIOCGLED unavailable?
 KIOCSLED unavailable?
 Keymap %d: Permission denied
 Loaded %d compose %s.
 Loading %d-char %dx%d (%d) font
 Loading %d-char %dx%d (%d) font from file %s
 Loading %d-char %dx%d font
 Loading %d-char %dx%d font from file %s
 Loading %s
 Loading Unicode mapping table...
 Loading binary direct-to-font screen map from file %s
 Loading binary unicode screen map from file %s
 Loading symbolic screen map from file %s
 Loading unicode map from file %s
 Meta key gives Esc prefix
 Meta key sets high order bit
 New default flags:     New flags:             New leds:              No final newline in combine file
 Old #scanlines: %d  New #scanlines: %d  Character height: %d
 Old default flags:     Old flags:             Old leds:              Old mode: %dx%d  New mode: %dx%d
 Only root can use the -u flag. Plain scancodes xx (hex) versus keycodes (dec)
 Please try again later.


 Read %d-char %dx%d font from file %s
 Reading font file %s
 Saved %d-char %dx%d font file on %s
 Saved screen map in `%s'
 Saved unicode map on `%s'
 Searching in %s
 Showing %d-char font

 Strange ... screen is both %dx%d and %dx%d ??
 Strange mode for Meta key?
 Symbols recognized by %s:
(numeric value, symbol)

 The %s is now locked by %s.
 The entire console display cannot be locked.
 The entire console display is now completely locked by %s.
 The keyboard is in Unicode (UTF-8) mode
 The keyboard is in mediumraw (keycode) mode
 The keyboard is in raw (scancode) mode
 The keyboard is in some unknown mode
 The keyboard is in the default (ASCII) mode
 This file contains 3 fonts: 8x8, 8x14 and 8x16. Please indicate
using an option -8 or -14 or -16 which one you want loaded.
 This tty (%s) is not a virtual console.
 Too many files to combine
 Try `%s --help' for more information.
 Typematic Rate set to %.1f cps (delay = %d ms)
 Unable to find command. Unable to open %s Unable to set new session Usage:
	%s [-C console] [-o map.orig]
 Usage:
	%s [-i infont] [-o outfont] [-it intable] [-ot outtable] [-nt]
 Usage:
	%s [-s] [-C console]
 Usage:
	%s infont [outtable]
 Usage:
	%s infont intable outfont
 Usage:
	%s infont outfont
 Usage:
	setleds [-v] [-L] [-D] [-F] [[+|-][ num | caps | scroll %s]]
Thus,
	setleds +caps -num
will set CapsLock, clear NumLock and leave ScrollLock unchanged.
The settings before and after the change (if any) are reported
when the -v option is given or when no change is requested.
Normally, setleds influences the vt flag settings
(and these are usually reflected in the leds).
With -L, setleds only sets the leds, and leaves the flags alone.
With -D, setleds sets both the flags and the default flags, so
that a subsequent reset will not change the flags.
 Usage:
	setmetamode [ metabit | meta | bit | escprefix | esc | prefix ]
Each vt has his own copy of this bit. Use
	setmetamode [arg] < /dev/ttyn
to change the settings of another vt.
The setting before and after the change are reported.
 Usage: %1$s [-C DEVICE] getmode [text|graphics]
   or: %1$s [-C DEVICE] gkbmode [raw|xlate|mediumraw|unicode]
   or: %1$s [-C DEVICE] gkbmeta [metabit|escprefix]
   or: %1$s [-C DEVICE] gkbled  [scrolllock|numlock|capslock]
 Usage: %s [OPTIONS] -- command

This utility help you to start a program on a new virtual terminal (VT).

Options:
  -c, --console=NUM   use the given VT number;
  -e, --exec          execute the command, without forking;
  -f, --force         force opening a VT without checking;
  -l, --login         make the command a login shell;
  -u, --user          figure out the owner of the current VT;
  -s, --switch        switch to the new VT;
  -w, --wait          wait for command to complete;
  -v, --verbose       print a message for each action;
  -V, --version       print program version and exit;
  -h, --help          output a brief help message.

 Usage: %s vga|FILE|-

If you use the FILE parameter, FILE should be exactly 3 lines of
comma-separated decimal values for RED, GREEN, and BLUE.

To seed a valid FILE:
   cat /sys/module/vt/parameters/default_{red,grn,blu} > FILE

and then edit the values in FILE.

 Usage: kbdrate [-V] [-s] [-r rate] [-d delay]
 Usage: setfont [write-options] [-<N>] [newfont..] [-m consolemap] [-u unicodemap]
  write-options (take place before file loading):
    -o  <filename>  Write current font to <filename>
    -O  <filename>  Write current font and unicode map to <filename>
    -om <filename>  Write current consolemap to <filename>
    -ou <filename>  Write current unicodemap to <filename>
If no newfont and no -[o|O|om|ou|m|u] option is given,
a default font is loaded:
    setfont         Load font "default[.gz]"
    setfont -<N>    Load font "default8x<N>[.gz]"
The -<N> option selects a font from a codepage that contains three fonts:
    setfont -{8|14|16} codepage.cp[.gz]   Load 8x<N> font from codepage.cp
Explicitly (with -m or -u) or implicitly (in the fontfile) given mappings
will be loaded and, in the case of consolemaps, activated.
    -h<N>      (no space) Override font height.
    -m <fn>    Load console screen map.
    -u <fn>    Load font unicode map.
    -m none    Suppress loading and activation of a screen map.
    -u none    Suppress loading of a unicode map.
    -v         Be verbose.
    -C <cons>  Indicate console device to be used.
    -V         Print version and exit.
Files are loaded from the current directory or %s/*/.
 Use Alt-function keys to switch to other virtual consoles. Using VT %s Warning: path too long: %s/%s
 When loading several fonts, all must be psf fonts - %s isn't
 When loading several fonts, all must have the same height
 When loading several fonts, all must have the same width
 You asked for font size %d, but only 8, 14, 16 are possible here.
 [ if you are trying this under X, it might not work
since the X server is also reading /dev/console ]
 adding map %d violates explicit keymaps line addkey called with bad index %d addkey called with bad keycode %d addkey called with bad table %d addmap called with bad index %d appendunicode: illegal unicode %u
 assuming iso-8859-1 %s
 assuming iso-8859-15 %s
 assuming iso-8859-2 %s
 assuming iso-8859-3 %s
 assuming iso-8859-4 %s
 bug: getfont called with count<256
 bug: getfont using GIO_FONT needs buf.
 cannot change translation table
 cannot open file %s
 cannot open include file %s caught signal %d, cleaning up...
 code outside bounds compose table overflow
 couldn't read %s
 couldn't read %s, and cannot ioctl dump
 deallocate keymap %d
 definition definitions dumpkeys version %s entries entry error executing  %s
 error reading scancode even number of arguments expected expected filename between quotes failed to bind key %d to value %d
 failed to bind string '%s' to function %s
 failed to clear string %s
 failed to get keycode for scancode 0x%x
 failed to restore original translation table
 failed to restore original unimap
 failed to set scancode %x to keycode %d
 for 1-%d (0x01-0x%02x) scancode equals keycode
 impossible error in do_constant impossible: not meta?
 includes are nested too deeply kb mode was %s
 kbd_mode: error reading keyboard mode
 key key bindings not changed
 keycode %3d %s
 keycode %d, table %d = %d%s
 keycode range supported by kernel:           1 - %d
 keys killkey called with bad index %d killkey called with bad table %d loadkeys version %s

Usage: loadkeys [option...] [mapfile...]

Valid options are:

  -a --ascii         force conversion to ASCII
  -b --bkeymap       output a binary keymap to stdout
  -c --clearcompose  clear kernel compose table
  -C --console=file
                     the console device to be used
  -d --default       load "%s"
  -h --help          display this help text
  -m --mktable       output a "defkeymap.c" to stdout
  -q --quiet         suppress all normal output
  -s --clearstrings  clear kernel string table
  -u --unicode       force conversion to Unicode
  -v --verbose       report the changes
 loadkeys: don't know how to compose for %s
 mapscrn: cannot open map file _%s_
 max nr of compose definitions: %d
 max number of actions bindable to a key:         %d
 new state:     nr of compose definitions in actual use: %d
 number of function keys supported by kernel: %d
 number of keymaps in actual use:                 %d
 of which %d dynamically allocated
 off old state:     on  press press any key (program terminates 10s after last keypress)...
 ranges of action codes supported by kernel:
 release resizecons:
call is:  resizecons COLSxROWS  or:  resizecons COLS ROWS
or: resizecons -lines ROWS, with ROWS one of 25, 28, 30, 34, 36, 40, 44, 50, 60
 resizecons: cannot find videomode file %s
 resizecons: cannot get I/O permissions.
 resizecons: don't forget to change TERM (maybe to con%dx%d or linux-%dx%d)
 resizecons: the command `%s' failed
 setfont: cannot both restore from character ROM and from file. Font unchanged.
 setfont: too many input files
 showkey version %s

usage: showkey [options...]

valid options are:

	-h --help	display this help text
	-a --ascii	display the decimal/octal/hex values of the keys
	-s --scancodes	display only the raw scan-codes
	-k --keycodes	display only the interpreted keycodes (default)
 stdin is not a tty strange... ct changed from %d to %d
 string string too long strings switching to %s
 syntax error in map file
 too many (%d) entries on one line too many compose definitions
 too many key definitions on one line unicode keysym out of range: %s unknown charset %s - ignoring charset request
 unknown keysym '%s'
 unrecognized argument: _%s_

 unrecognized user usage: %s
 usage: %s [-v] [-o map.orig] map-file
 usage: chvt N
 usage: getkeycodes
 usage: kbd_mode [-a|-u|-k|-s] [-C device]
 usage: screendump [n]
 usage: setkeycode scancode keycode ...
 (where scancode is either xx or e0xx, given in hexadecimal,
  and keycode is given in decimal)
 usage: showconsolefont -V|--version
       showconsolefont [-C tty] [-v] [-i]
(probably after loading a font with `setfont font')

Valid options are:
 -C tty   Device to read the font from. Default: current tty.
 -v       Be more verbose.
 -i       Don't print out the font table, just show
          ROWSxCOLSxCOUNT and exit.
 usage: totextmode
 vt %d is in use; command aborted; use `%s -f' to force. Project-Id-Version: kbd
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2012-11-16 00:52+0400
PO-Revision-Date: 2013-04-02 03:46+0000
Last-Translator: Neliton Pereira Jr. <nelitonpjr@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:34+0000
X-Generator: Launchpad (build 18115)
 			    Interprete os códigos de ação de caracteres como vindos do
			    conjunto de caracteres especificado
 

Os códigos de varredura e0 xx (hex) escaparam
 
Alterado %d %s e %d %s.
 
Pressione alguma tecla -- Ctrl+D encerra este programa

 
Nomes dos modificadores reconhecidos e os números de suas colunas:
 
Os seguintes sinônimos foram reconhecidos:

 
Modo de usar: dumpkeys [opções]

As opções disponíveis são:

	-h --help	    Exibe este manual
	-i --short-info	    Exibe informações sobre o driver do teclado
	-l --long-info	    Exibe essas informações e símbolos aptos a carregar mapas de teclado
	-n --numeric	    Exibe o mapa de teclado em notação hexadecimal
	-f --full-table	    don't use short-hand notations, one row per keycode
	-1 --separate-lines uma linha por par de modificador e código de tecla
	   --funcs-only	    Exibe somente as linhas com a descrição de teclas de função
	   --keys-only	    Exibe somente os vínculos de teclas
	   --compose-only   Exibe somente combinações de teclas compostas
	-c --charset=     FALHOU # not alt_is_meta: no mapa de teclado %d, a tecla %d corresponde a %-15s para %s
 %s de %s
 %s versão %s

Uso: %s [opções]

As opções válidas são:

	-h --help            mostra este texto de ajuda
	-V --version         mostra a versão do progama
	-n --next-available  mostra o número do próximo VT não alocado
 %s: %s: Aviso: linha muito longa
 %s: 0: número de VT inválido
 %s: Faixa de caracteres Unicode inválida corresponde à faixa de posição de fonte 0x%x a 0x%x
 %s: Sinal inválido do readpsffont
 %s: Fim da faixa inválido (0x%lx)
 %s: Fim de faixa inválido (0x%x)
 %s: Linha de entrada inválida: %s
 %s: Número mágico inválido em %s
 %s: Deveria haver uma faixa de Unicode correspondendo a uma de posições de fonte
 %s: Erro ao ler fonte de entrada %s: Número do glifo (0x%lx) ultrapassou o fim da fonte
 %s: Número do glifo (0x%x) excede a largura da fonte
 %s: Número vt inválido %s: Arquivo de entrada: tamanho de entrada inválido (%d)
 %s: Arquivo de entrada: acompanha dados inválidos
 %s: Opções --unicode e --ascii são mutuamente exclusivas
 %s: Faixa de caracteres Unicode U+%x a U+%x não coincide com a faixa de posição de fonte 0x%x a 0x%x
 %s: Modo de arquivo psf não suportado (%d)
 %s: Versão do psf não suportada (%d)
 %s: VT 1 é o console e não pode ser desalocado
 %s: Aviso: a linha está longa demais
 %s: addfunc chamado com a função errada %d
 %s: addfunc: estouro do func_buf
 %s: o plano de fundo terá uma aparência hilária
 %s: UTF-8 inválido
 %s: falha em do_loadtable
 %s: não é possível desalocar ou limpar o mapa de teclado
 %s: não foi possível encontrar nenhum mapa de teclado?
 %s: Não foi possível desalocar o console %d
 %s: não foi possível desalocar o mapa de teclado %d
 %s: não é possível retornar para o modo original do teclado
 %s: não é possível mudar para o modo Unicode
 %s: A desalocação de todos os consoles não usados falhou
 %s: erro ao ler modo do teclado: %m
 %s: erro durante a definição do modo do teclado
 %s: a posição de fonte 32 não corresponde a nenhum campo
 %s: a fonte de entrada não tem um índice
 %s: bloqueia os consoles virtuais, salvando sua sessão atual.
Uso: %s [opções]
     Onde [opções] podem ser:
-c ou --current: bloqueia somente este console virtual, permitindo ao usuário
       alternar para outros consoles virtuais.
-a ou --all: bloqueia todos os consoles virtuais impedindo outros usuários
       de alternar consoles virtuais.
-v ou --version: exibe o número da versão do vlock e fecha o aplicativo.
-h ou --help: exibe esta mensagem de ajuda e fecha o aplicativo.
 %s: impossível carregar unimap vazio
(para forçar o carregamento, use a opção -f para sobrescrever)
 %s: falta de memória
 %s: fora da memória?
 %s: mapa da área não alocado? Muito estranho...
 %s: Arquivo psf com número mágico desconhecido
 %s: tabela Unicode ucs2 curta
 %s: tabela Unicode curta
 %s: tabela Unicode UTF-8 curta
 %s: impurezas (%s) ignoradas
 %s: opção desconhecida
 %s: Erro de UTF-8 desconhecido
 %s: aviso: carregando um mapa de teclas Unicode em um console não-Unicode
    (talvez você queira fazer `kbd_mode -u'?)
 %s: aviso: carregando mapa de teclado não-Unicode em console Unicode
    (talvez você queira fazer `kbd_mode -a'?)
 %s: removido
 %s: tamanho zero para caractere de entrada?
 %s: comprimento zero para fonte de entrada?
 '%s' não é um símbolo de tecla de função (Nenhuma alteração nas definições compostas.)
 0 representa um erro; de 1 a 88 (0x01-0x58), o código de varredura é igual ao código de tecla
 ?DESCONHECIDO? Ativação interrompida? Mapa Unicode apenso
 Altura de caractere %d inválida
 Largura de caractere %d inválida
 Tamanho de arquivo de entrada inválido.
 Informação inválida: %s
 Não foi possível verificar se vt %d está livre; use `%s -f' para forçar. Não foi possível achar %s
 Não foi possível encontrar a fonte %s.
 Não foi possível encontrar um vt livre Não foi possível encontrar a fonte padrão
 Não foi possível abrir %s leitura/escrita Não foi possível abrir /dev/port Não foi possível abrir o arquivo de fonte %s
 Não foi possível ler o mapa do console
 Não foi possível copiar o arquivo. Não foi possível gravar o arquivo de fonte Não foi possível gravar o cabeçalho do arquivo de fonte Contagem de caracteres: %d
 Não foi possível ativar vt %d Impossível desalocar console %d Não foi possível encontrar o dono do tty atual! Não foi possível obter o descritor de arquivo que se refere ao console Não foi possível obter um descritor de arquivo referindo ao console
 Não foi possível abrir %s
 Não foi possível ler VTNO:  Sinalizações-padrão atuais:   Sinalizações atuais:           Leds atuais:            Erro ao abrir /dev/kbd.
 Erro ao interpretar o mapa de símbolos em `%s', linha %d
 Erro ao ler %s
 Erro ao ler as configurações atuais de sinalizações. Talvez você não esteja no console?
 Erro ao ler as configurações atuais de leds em /dev/kbd.
 Erro na leitura da configuração atual dos leds. Talvez a entrada não seja um VT?
 Erro lendo definição atual. Talvez stdin não é um VT?
 Erro ao ler mapa do arquivo `%s'
 Erro ao reajustar o modo dos leds.
 Erro ao gravar o mapa em arquivo
 Erro ao gravar descarga de tela
 Erro: %s: Número de campos insuficientes na linha %u. Erro: %s: Valor inválido no campo %u na linha %u. Erro: %s: Linha %u terminou inesperadamente.
 Erro: %s: Linha %u é muito extensa.
 Erro: Argumentos insuficientes.
 Erro: Ação não reconhecida: %s
 Tamanho da fonte    : %d
 Largura da fonte     : %d
 Nada foi encontrado para salvar.
 Hmm -- uma fonte do restorefont? Será utilizada a primeira metade.
 Número inválido de linhas
 Parece que este kernel é anterior à versão 1.1.92
Nenhuma tabela de mapeamento Unicode carregada.
 Erro do KDGKBENT no índice %d, tabela %d
 Erro do KDGKBENT no índice 0, tabela %d
 KDGKBSENT falhou no índice %d:  KIOCGLED indisponível?
 KIOCSLED indisponível?
 Mapa de teclado %d: Permissão negada
 Carregado %d constituido %s.
 Carregando a fonte %dx%d (%d) de caractere-%d
 Carregando a fonte %dx%d (%d) de caractere-%d do arquivo %s
 Carregando a fonte %dx%d de caractere-%d
 Carregando a fonte %dx%d de caractere-%d do arquivo %s
 Carregando %s
 Carregando tabela de mapeamento Unicode...
 Carregando mapa binário de exibição para-a-fonte do arquivo %s
 Carregando mapa binário de exibição Unicode do arquivo %s
 Carregando mapa de exibição simbólico do arquivo %s
 Carregando mapa Unicode do arquivo %s
 A metatecla resulta em prefixo Esc
 A metatecla ajusta bit de alta ordem
 Novas sinalizações-padrão:     Novas sinalizações:             Novos leds:              Não há uma nova linha final no arquivo de combinação
 Antigas #linhas de varredura: %d  Novas #linhas de varredura: %d  Altura do caractere: %d
 Sinalizações-padrão antigas:     Antigas sinalizações:             Leds antigos:              Modo antigo: %dx%d  Novo modo: %dx%d
 Apenas root pode usar a opção -u. Códigos de varredura planos xx (hex) para códigos de tecla (dec)
 Por favor, tente novamente mais tarde.


 Leia a fonte %dx%d de caractere-%d do arquivo %s
 Lendo o arquivo de fonte %s
 A fonte %dx%d de caractere-%d foi salva no arquivo %s
 O mapa de exibição foi salvo em `%s'
 Mapa Unicode salvo em `%s'
 Procurando em %s
 Mostrando fonte %d-char

 Estranho... exibição é tanto %dx%d como %dx%d?
 Modo estranho para metatecla?
 Símbolos reconhecidos por %s:
(valor numérico, símbolo)

 %s está travado por %s.
 A exibição do controle inteiro não pode ser bloqueada.
 A exibição do controle inteiro agora está completamente bloqueada por %s.
 O teclado está no modo Unicode (UTF-8)
 O teclado está em modo semi-puro (de código de varredura)
 O teclado está em modo puro (de código de varredura)
 O teclado está em algum modo desconhecido
 O teclado está no modo padrão (ASCII)
 Este arquivo contém 3 fontes: 8x8, 8x14 e 8x16. Por favor, indique
usando uma entre as opções -8, -14 e -16 qual delas você deseja carregar.
 Este tty (%s) não é um console virtual.
 Há arquivos demais para se combinar
 Tente `%s --help' para mais informações.
 Taxa de digitação ajustada em %.1f caracteres por segundo (atraso = %d ms)
 Não foi possível encontrar comando. Não foi possível abrir %s Não foi possível configurar nova sessão Modo de usar:
	%s [-C console] [-o mapa.origem]
 Modo de usar:
	%s [-i fonte-de-entrada] [-o fonte-de-saída] [-it tabela-de-entrada] [-ot tabela-de-saída] [-nt]
 Modo de usar:
	%s [-s] [-C console]
 Modo de usar:
	%s infont [tabela de saída]
 Modo de usar:
	%s infont intable outfont
 Modo de usar:
	%s fonte-de-entrada fonte-de-saída
 Modo de usar:
	setleds [-v] [-L] [-D] [-F] [[+|-][ num | caps | scroll %s]]
Desta forma,
	setleds +caps -num
ajustará o CapsLock, desabilitará o NumLock e não modificará o ScrollLock.
As configurações de antes e depois da mudança (se houver) são reportadas
quando a opção -v é inserida ou quando nenhuma mudança é solicitada.
Normalmente, o setleds influencia as configurações de sinais de VT
(e estas geralmente refletem-se nos leds).
Com -L, o setleds ajusta somente os leds, deixando as sinalizações à parte.
Com -D, o setleds ajusta tanto as sinalizações como as sinalizações-padrão,
de modo que um reajuste ulterior não as modifique.
 Modo de usar:
	setmetamode [ metabit | meta | bit | escprefix | esc | prefix ]
Cada VT tem sua própria cópia desse bit. Use
	setmetamode [arg] < /dev/ttyn
para alterar as configurações de outro VT.
As configurações anteriores e ulteriores serão reportadas.
 Uso: %1$s [-C DEVICE] getmode [text|graphics]
   ou: %1$s [-C DEVICE] gkbmode [raw|xlate|mediumraw|unicode]
   ou: %1$s [-C DEVICE] gkbmeta [metabit|escprefix]
   ou: %1$s [-C DEVICE] gkbled  [scrolllock|numlock|capslock]
 Uso: %s [OPÇÕES] -- comando

Este utilitário auxilia você a iniciar um programa em um novo
 terminal virtual (TV).

Opções:
  -c, --console=NUM   usa o número do TV dado;
  -e, --exec          executa o comando, sem bifurcação;
  -f, --force         força a abertura de um TV sem verificação;
  -l, --login         faz do comando um login do shell;
  -u, --user          descobre o dono do TV atual;
  -s, --switch        alterna para o novo TV;
  -w, --wait          espera que o comando termine;
  -v, --verbose       imprime uma mensagem para cada ação;
  -V, --version       imprime a versão do programa e sai;
  -h, --help          gera a saída de uma breve mensagem de ajuda.

 Uso: %s vga|ARQUIVO|-

Se você usar o parâmetro ARQUIVO, ele deve ter exatamente 3 linhas de valores decimais separados por vírgula para VERMELHO, VERDE e AZUL.

Para semear um arquivo válido:
   cat /sys/module/vt/parameters/default_{vermelho,verde,azul} > ARQUIVO

e então modifique os valores no ARQUIVO.

 Modo de usar: kbdrate [-V] [-s] [-r taxa de digitação] [-d atraso]
 Modo de usar: setfont [opções-de-gravação] [-<N>] [nova-fonte] [-m mapa-do-console] [-u mapa-Unicode]
  Opções de gravação (deve ser feita antes de carregar o arquivo):
    -o  <nome do arquivo>  Grava a fonte atual no arquivo <nome do arquivo>
    -O  <nome do arquivo>  Grava a fonte atual e o mapa Unicode no arquivo <nome do arquivo>
    -om <nome do arquivo>  Grava o mapa de console atual no arquivo <nome do arquivo>
    -ou <nome do arquivo>  Grava o mapa Unicode atual no arquivo <nome do arquivo>
Caso não seja inserida a opção nova-fonte ou qualquer -[o|O|om|ou|m|u],
uma fonte padrão será carregada:
    setfont         Carrega fonte "default[.gz]"
    setfont -<N>    Carrega fonte "default8x<N>[.gz]"
A opção -<N> seleciona uma fonte de uma página de códigos que contém três fontes:
    setfont -{8|14|16} codepage.cp[.gz]   Carrega a fonte 8x<N> de codepage.cp
Mapeamentos explícitos (com -m ou -u) ou implícitos (no arquivo de fonte) serão carregados e, no caso de serem mapas de console, ativados.
    -h<N>      (sem espaço) Sobrescreve a altura da fonte.
    -m <fn>    Carrega o mapa de exibição do console.
    -u <fn>    Carrega mapa de fonte Unicode.
    -m none    Impede o carregamento e a ativação de um mapa de exibição.
    -u none    Impede o carregamento de um mapa Unicode.
    -v         Realiza o comando em modo verbose.
    -C <cons>  Indica que dispositivo de console será usado.
    -V         Imprime a versão e encerra o aplicativo.
Os arquivos são carregados do diretório atual ou de %s/*/.
 Use Alt + teclas de função para mudar para outros consoles virtuais. Usando VT %s Aviso: caminho muito extenso: %s/%s
 Ao carregar várias fontes, todas devem estar no formato psf -- e %s não está.
 Ao carregar várias fontes, todas devem ter a mesma altura
 Ao carregar várias fontes, todas devem ter a mesma largura
 Você solicitou um tamanho de fonte %d, mas somente 8, 14 e 16 são válidos.
 [ Se você está tentando utilizar isto pelo X, talvez não funcione,
já que o servidor do X também está lendo /dev/console ]
 adicionando mapa %d viola linha da tabela de descrições de teclado explicito addkey chamado com um índice ruím %d addkey chamado com código de tecla ruim %d addkey chamado com uma tabela ruím %d adicionar mapeamento chamado com um índice ruím %d appendunicode: Unicode inválido %u
 Assumindo iso-8859-1, %s
 Assumindo iso-8859-15, %s
 Assumindo iso-8859-2, %s
 Assumindo iso-8859-3, %s
 Assumindo iso-8859-4, %s
 Erro: getfont respondeu com resultado menor que 256
 Erro: É preciso buf. para getfont usar GIO_FONT
 Não foi possível alterar a tabela de tradução.
 Não foi possível abrir arquivo %s
 não foi possível abrir arquivo para inclusão %s Capturou sinal %d, fazendo a limpeza...
 Código não-vinculado estouro da tabela composta
 Não foi possível ler %s
 Não foi possível ler %s e realizar ioctl de descarga.
 desalocar mapa de teclado %d
 definição definições dumpkeys, versão %s valores valor Erro ao executar  %s
 Erro ao ler o código de varredura mesmo número de argumentos esperado esperado nome do arquivo entre aspas falha ao ligar tecla %d ao valor %d
 falha ao ligar texto '%s' a função %s
 falha ao limpar a string %s
 Não foi possível obter um código de tecla para o código de varredura 0x%x
 A restauração da tabela de tradução original falhou
 A restauração do mapa Unicode original falhou.
 Não foi possível configurar o código de varredura %x para o código de tecla %d
 de 1 a %d (0x01-0x%02x), o código de varredura é igual ao código de tecla
 impossível erro em do_constant impossível: não é meta?
 includes estão aninhados com muita profundidade Modo kb era %s
 kbd_mode: Erro ao reconhecer o modo do teclado
 tecla atalhos de teclado não modificados
 Código de tecla %3d %s
 keycode %d, tabela %d = %d%s
 Faixa de códigos de caracteres aceita pelo kernel: 1 a %d
 teclas killkey chamado com um índice ruím %d killkey chamado com uma tabela ruím %d loadkeys versão %s

Uso: loadkeys [opção...] [arquivomapa...]

Opções válidas:

  -a --ascii            força a conversão para ASCII
  -b --bkeymap          gera mapa de chave binário na saída padrão
  -c --clearcompose     limpa a tabela de composição do kernel
  -C --console=arquivo  o dispositivo de console a ser usado
  -d --default          carrega "%s"
  -h --help             exibe este texto de ajuda
  -m --mktable          gera um "defkeymap.c" na saída padrão
  -q --quiet            suprime toda saída normal
  -s --clearstrings     limpa a tabela de strings do kernel
  -u --unicode          força a conversão para Unicode
  -v --verbose          relata as alterações
 loadkeys: não sei como compor para %s
 mapscrn: não foi possível abrir arquivo de mapa _%s_
 Número máximo de definições compostas: %d
 Podem ser vinculadas a uma tecla no máximo %d ações
 estado novo:     Número de definições compostas em uso no momento: %d
 Número de teclas de funções aceito pelo kernel: %d
 Número de mapas de teclado em uso no momento: %d
 dos quais %d estão alocados dinamicamente
 desligado estado antigo:     ligado  pressione Pressione qualquer tecla (o programa se encerrará 10s depois de pressionada uma tecla)...
 Faixas de códigos de ação aceitas pelo kernel:
 versão resizecons:
forma de utilização é:  resizecons COLSxROWS  ou:  resizecons COLS ROWS
ou: resizecons -lines ROWS, onde ROWS pode ser 25, 28, 30, 34, 36, 40, 44, 50, 60
 resizecons: não foi possível encontrar o arquivo de modo de vídeo %s
 resizecons: não foi possível obter permissões de E/S.
 resizecons: não se esqueça de alterar "TERM" (possivelmente para con%dx%d ou linux-%dx%d)
 resizecons: o comando `%s' falhou
 setfont: não foi possível restaurar a fonte nem da ROM de caracteres, nem do arquivo. A fonte permanece sem modificações.
 setfont: há arquivos de entrada em excesso.
 showkey, versão %s

Modo de usar: showkey [opções]

As seguintes opções são aplicáveis:

	-h --help	Exibe este manual
	-a --ascii	Exibe os valores decimais/octais/hexadecimais das teclas
	-s --scancodes	Exibe apenas os códigos de varredura puros
	-k --keycodes	Exibe apenas os códigos de tecla interpretados (padrão)
 stdin não é um tty Estranho... ct mudou de %d para %d
 string string muito extensa strings alternando para %s
 erro de sintaxe no arquivo de mapeamento
 muitas entradas (%d) em uma mesma linha excesso de definições compostas
 muitas definições de teclas em uma mesma linha unicode keysym fora do limite: %s Conjunto de caracteres %s desconhecido - a requisição de conjunto de caracteres será ignorada
 keysym '%s' desconhecido
 Argumento incógnito: _%s_

 usuário não reconhecido Modo de usar: %s
 Modo de usar: %s [-v] [-o mapa.origem] "arquivo de mapa"
 uso: chvt N
 Modo de usar: getkeycodes
 Modo de usar: kbd_mode [-a|-u|-k|-s] [-C dispositivo]
 Modo de usar: screendump [n]
 Modo de usar: setkeycode código-de-varredura código-de-tecla ...
 (em que o código de varredura é ou xx ou e0xx, inserido em notação hexadecimal,
  e o código de tecla é inserido em notação decimal)
 Modo de usar: showconsolefont -V|--version
       showconsolefont [-C tty] [-v] [-i]
(provavelmente após carregar uma fonte com o `setfont fonte')

As seguintes opções são aplicáveis:
 -C tty   Dispositivo do qual a fonte será lida. Padrão: tty atual.
 -v       Realiza o comando em modo verbose.
 -i       Não imprime a tabela de fonte, apenas a exibe.
          ROWSxCOLSxCOUNT e exit.
 uso: paramodotexto
 vt %d está em uso; comando abortado; use `%s -f' para forçar. 