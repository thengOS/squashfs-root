��    r     �  �  <        b   	     l  �   �  �   5      !  +   !     I!  
   W!     b!  	   n!  *   x!     �!     �!     �!     �!     "  %   "     @"     W"  $   m"  4   �"     �"     �"     �"  #   �"     #     .#     ;#     M#     d#     |#     �#     �#     �#     �#     �#     $     $     /$     C$     W$     e$     |$     �$     �$     �$     �$     �$  %   %  X   (%     �%     �%  :   �%  B   �%  +   (&  )   T&  ,   ~&     �&     �&  %   �&      '  #   /'     S'     r'     �'     �'     �'     �'  	   �'     �'     �'     (     0(     @(  #   R(  -   v(     �(     �(     �(     �(     �(     �(     �(     
)     )     0)     >)     C)  !   X)  $   z)     �)     �)     �)  
   �)     �)     �)     �)     �)     �)     *     	*     *     ;*     I*     N*  .   `*     �*     �*     �*  "   �*     �*  
   �*     �*     �*     +     +     +     *+     0+     C+     L+  
   ]+     h+     w+     �+  !   �+     �+     �+     �+     �+  
   
,     ,     ),  "   9,     \,     a,     v,     |,     �,     �,     �,     �,     �,     �,     �,     �,     �,     -     -  "   7-     Z-     n-     �-     �-     �-     �-  (   �-  '   �-     .     .     .     .     3.     K.     h.     u.     �.     �.     �.     �.  =   �.  5   �.     5/     P/     k/     |/     �/  !   �/  .   �/  8   �/     )0  &   D0     k0     �0     �0     �0     �0  -   �0  .   �0     '1     91  ,   I1  
   v1     �1     �1     �1     �1     �1     2     2     02     M2     f2  $   �2     �2     �2     �2     �2  +   �2  *   3  )   63  #   `3  "   �3     �3  J   �3     �3  !   4     (4     44     J4     d4     i4     w4     �4     �4     �4     �4  +   �4     �4  ,   5  $   95     ^5     u5     �5     �5     �5     �5     �5     �5     �5  !   6  $   -6  ,   R6  *   6     �6  %   �6  &   �6  "   7  !   67  '   X7  "   �7  %   �7  !   �7  "   �7  :   8  ?   I8  .   �8     �8     �8     �8  :   9  '   K9  *   s9     �9     �9     �9     �9  8   �9     	:     :  ,   :     B:  %   X:  D   ~:     �:  <   �:     ;  ;   /;  ,   k;     �;  7   �;  (   �;  F   �;     B<     W<  -   k<  +   �<  )   �<  *   �<  -   =  -   H=  +   v=     �=     �=  /   �=  F   �=     C>     J>     c>     �>  7   �>     �>     �>     ?     ?  "   7?     Z?     \?     i?     z?     �?     �?     �?     �?  +   �?     �?  ,   �?     (@  %   0@  '   V@     ~@     �@     �@     �@  #   �@     �@     �@     
A     A  -   A  I   ?A     �A     �A     �A  ,   �A  /   B  /   5B  .   eB     �B     �B     �B     �B     �B     �B     C     C      7C     XC     uC      �C     �C     �C  *   �C     D     D     ?D     TD     mD  "   uD  #   �D     �D  ,   �D     �D  	   �D     E  0   E  @   NE  A   �E  J   �E  �  F  k   �G  #   CH  �   gH  �   AI     )J  #   ?J     cJ  
   wJ     �J  
   �J  .   �J     �J     �J     �J  #   K     1K  '   GK     oK     �K  $   �K  6   �K     �K     L     L  %   5L     [L     jL     xL     �L     �L     �L     �L     �L     M     )M     AM     ZM     rM     �M     �M     �M     �M     �M     N  %   N  #   >N     bN     zN  )   �N  Y   �N     O     )O  @   >O  D   O  -   �O  '   �O  2   P  %   MP     sP  $   �P     �P  /   �P     Q  !   (Q     JQ  &   iQ     �Q     �Q  
   �Q     �Q     �Q     �Q     R     R  *   4R  -   _R     �R     �R     �R     �R     �R     �R     �R     S     S     1S     >S     AS  2   [S  )   �S     �S     �S     �S     �S     �S     �S     �S     T     "T     /T     3T  #   KT     oT     �T     �T  3   �T     �T     �T     �T  !   �T     U  
   0U     ;U     TU  	   hU     rU     yU     �U     �U  
   �U     �U     �U     �U     �U     V  *   V  '   GV     oV     �V  #   �V     �V     �V     �V      W     "W     *W     ?W     EW  "   YW     |W     �W     �W     �W     �W     �W     �W     �W     �W     
X  +   'X     SX      hX     �X     �X     �X     �X  :   �X  7   �X     )Y     0Y     CY     EY  "   bY  '   �Y     �Y      �Y     �Y  *   �Y     Z     Z  [   6Z  P   �Z  !   �Z  !   [     '[  %   E[  %   k[  >   �[  9   �[  Q   
\     \\  *   |\  "   �\  *   �\     �\  #   �\     ]  6   +]  6   b]     �]     �]  2   �]  	   �]  &   ^     /^     O^     h^     �^     �^     �^  &   �^     �^  '   _  /   <_     l_     {_     �_     �_  D   �_  B   �_  :   9`  +   t`  3   �`     �`  R   �`     -a  -   :a     ha     ta     �a     �a     �a  !   �a     �a     �a     b     /b  -   5b     cb  2   pb  *   �b     �b      �b     c     (c     Dc     cc     �c     �c     �c  ,   �c  4   �c  6   d  9   Cd  ,   }d  8   �d  7   �d  (   e  1   De  4   ve  %   �e  6   �e  (   f  (   1f  D   Zf  H   �f  /   �f  *   g  !   Cg      eg  =   �g  ?   �g  B   h     Gh     Uh     fh     hh  @   �h     �h     �h  0   �h     i  7   (i  W   `i  '   �i  H   �i     )j  G   @j  :   �j     �j  ;   �j  6   k  O   8k     �k     �k  *   �k  (   �k  &   
l  '   1l  *   Yl  A   �l  A   �l     m     "m  :   <m  O   wm     �m     �m     �m     n  I   *n     tn  !   �n  8   �n      �n  -   o     9o     ;o     Io     Zo     `o     vo     xo     �o  :   �o  #   �o  6   �o     0p  (   8p     ap     �p     �p  #   �p     �p  4   �p     q     1q     >q     Cq  2   Eq  O   xq  +   �q     �q     r  0   'r  L   Xr  Q   �r  P   �r     Hs     Qs     ns  "   ts      �s  #   �s  *   �s  !   t  *   )t  #   Tt     xt  4   �t     �t  )   �t  A   u     Vu  $   ru      �u     �u     �u  !   �u  '   �u  
   'v  8   2v     kv     ~v     �v  9   �v  ?   �v  D   w  G   _w     n             �       .  q   _  3  �   f   T  y           e  W      b   l  �           �          �          C   b      a          +   �   K     �     �   �   U   �       _   �   �   p      >  �   �   >   /   �   #      -  8      �   6  M  D   �       �   h      *     /  J   B   Q  ,    U  ]  c  �   k      ;  �   C  @      t   F   ?  4   �   �   \      �   .                 �   Y     ;     �   �   4  9  J  '  ~                           g      �   P  0           S   ,   `   -   L      �   p   �         �   �   �         w   �       H           �   �   )     &   d  [      �       �           �   �   �   X   D  R  �       �   2     !  S             v   �       L   f  �           \     T   �   �      r   �                       =       9   G  %           d       R       �          �   �   �   B    Z   %          m      �       5   A  �   #   l           `  M       �   N     �     $    !   �         k            c   �       o               �         �   1           �   Q       �          �   :      o          �   i  X  �          �   �   �     �       �       �   n          �   �   |   �           0  <       )                  :       g   F     &             $   �   z   �       j      7      �   j   �   �   A   ^   �   =        (              �   �                  	  �   �   �   i   x   1  O  �   ?   �   �   O   �   N      *           	       "   u   �   �   �   q     K      {   �   E        �   V   
      �   �   �   V      �   �       �   s      I  "  �       �   �   �   Z      �       �   m   6       �         8   �   �     �   
       }       ^  �       Y  7          �         �   �     @                �       3   2   I   �          �   h  [      ]   W   �   a   �               �   r      (  '       +      E   �   �   �       G   H      �      e   �       �   <  �   �   �   5  P         
 Try '%s --%s <%s|%s|%s|%s|%s|%s>'
  or '%s --%s <%s|%s|%s|%s|%s|%s>'
 for additional help text.
 
For more details see ps(1).
 
The default priority is +4. (snice +4 ...)
Priority numbers range from +20 (slowest) to -20 (fastest).
Negative priority numbers are restricted to administrative users.
 
The default signal is TERM. Use -l or -L to list available signals.
Particularly useful signals include HUP, INT, KILL, STOP, CONT, and 0.
Alternate signals may be specified in three ways: -SIGKILL -KILL -9
 
Usage:
 %s [options]
      --help     display this help and exit
    IDLE WHAT
   [ anon ]   [ stack ]  %2ludays  %s [new priority] [options] <expression>
  %s [options]
  %s [options] <pattern>
  %s [options] <pid> [...]
  %s [options] [delay [count]]
  %s [options] [tty]
  %s [options] [variable[=value] ...]
  %s [options] command
  %s [options] pid...
  %s [signal] [options] <expression>
  -v, --version  output version information and exit
  total %16ldK
  total %8ldK
 "%s" is an unknown key "%s" must be of the form name=value %-*s TTY       %13d disks 
 %13d partitions 
 %13lld idle cpu ticks
 %13lu %s active memory
 %13lu %s buffer memory
 %13lu %s free memory
 %13lu %s free swap
 %13lu %s inactive memory
 %13lu %s swap cache
 %13lu %s total memory
 %13lu %s total swap
 %13lu %s used memory
 %13lu %s used swap
 %13lu read sectors
 %13lu writes
 %13lu written sectors
 %13u boot time
 %13u interrupts
 %d column window is too narrow %s is deprecated, value not set %s killed (pid %lu)
 %s"%s" not found %s(%d): invalid syntax, continuing... %s:~3 %3u ~2total,~3 %3u ~2running,~3 %3u ~2sleeping,~3 %3u ~2stopped,~3 %3u ~2zombie~3
 * Applying %s ...
 -%c requires argument -L with H/-m/m but no PID/PGID/SID/SESS for NLWP to follow -L without -F makes no sense
Try `%s --help' for more information. -L/-T with H/m/-m and -o/-O/o/O is nonsense -S requires k, K, m or M (default is KiB) -T with H/-m/m but no PID for SPID to follow -d disallowed in "secure" mode -d requires positive argument -i makes no sense with -v, -f, and -n -v makes no sense with -i and -f AIX field descriptor processing bug Active / Total Caches (% used) Active / Total Objects (% used) Active / Total Size (% used) Active / Total Slabs (% used) CPU Time CPU Time, hundredths CPU Usage Cache Change delay from %.1f to Choose field group (1 - 4) Code Size (KiB) Command Name/Line Command disabled, 'A' mode required Command disabled, activate %s with '-' or '_' Control Groups Controlling Tty Cpu%-3d: Cpu(s): Cumulative time %s Data+Stack (KiB) Dirty Pages Count Effective User Id Effective User Name Every %.1fs:  FROM Failed '%s' open: %s Failed renice of PID %d to %d: %s Failed signal pid '%d' with '%d': %s Forest mode %s GiB Group Id Group Name High: Invalid maximum Invalid signal Invalid user Irix mode %s KiB Last Used Cpu (SMP) Locate next inactive, use "L" Locate string Low: Major Page Faults Maximum tasks = %d, change to (0 is unlimited) Mem: Memory Usage (RES) MiB Minimum / Average / Maximum Object Minor Page Faults Nice Value No colors to map! Number of Threads Off On Only 1 cpu detected Pages Parent Process pid Priority Process Group Id Process Id Process Status Real User Id Real User Name Rename window '%s' to (1-3 chars) Renice PID %d to value Saved User Id Saved User Name Send pid %d signal [%d/sigterm] Session Id Shared Memory (KiB) Show threads %s Signal %d (%s) caught by %s (%s).
 Size Sleeping in Function Swap: TTY could not be found Task Flags <sched.h> Tasks Thread Group Id Threads Total Total: Tty Process Grp Id USER Unacceptable floating point Unacceptable integer Unavailable in secure mode Unknown command - try 'h' for help Virtual Image (KiB) Which user (blank for all) Wrote configuration to '%s' a active all alternate System.map file must follow -n alternate System.map file must follow N another  argument missing b bad alignment code
 bad delay interval '%s' bad iterations argument '%s' bad pid '%s' bad sorting code buff bug: must reset the list first cache can not open tty can not set width for a macro (multi-column) format specifier can not use output modifiers with user-defined output cannot happen - problem #1 cannot happen - problem #2 cannot open "%s" cannot open file %s cannot stat %s cannot strdup() personality text
 column widths must be unsigned decimal numbers command exit with a non-zero status, press a key to exit conflicting format options conflicting process selections (U/p/u) could not find ppid
 could not find start_time
 cur delay must be positive integer dup2 failed embedded '-' among BSD options makes no sense embedded '-' among SysV options makes no sense empty format list empty sort list environment specified an unknown personality error: %s
 error: can not access /proc
 failed /proc/stat open: %s failed /proc/stat read failed memory allocate failed memory re-allocate failed number of cpus test failed openproc: %s failed pid maximum size test failed to parse argument failed to parse count argument failed to parse count argument: '%s' failed tty get fdopen fix bigness error
 forest view format or sort specification must follow -O format or sort specification must follow O format specification must follow --format format specification must follow -o format specification must follow o free from length environment PROCPS_FROMLEN must be between 8 and %d, ignoring
 garbage option general flags may not be repeated glob failed group ID out of range group name does not exist help illegal delay improper AIX field descriptor improper format list improper list improper sort list inact incompatible rcfile, you should delete '%s' internal error internal error: no PID or PPID for -j option internal error: no PRI for -c option invalid group name: %s invalid process group: %s invalid process id: %s invalid session id: %s invalid user name: %s killing pid %ld failed l list list member was not a TTY list of PRM groups must follow -R list of command names must follow -C list of effective groups must follow --group list of effective users must follow --user list of jobs must follow -J list of process IDs must follow --pid list of process IDs must follow --ppid list of process IDs must follow -p list of process IDs must follow p list of real groups must follow --Group list of real groups must follow -G list of real users must follow --User list of real users must follow -U list of session IDs must follow -s list of session leaders OR effective group IDs was invalid list of session leaders OR effective group names must follow -g list of terminals (pty, tty...) must follow -t list of ttys must follow --tty list of users must follow -u list of users must follow U list of zones (contexts, labels, whatever?) must follow -z long sort specification must follow 'k' long sort specification must follow --sort lost my CLS lost my PGID m malformed setting "%s" mapped: %ldK    writeable/private: %ldK    shared: %ldK
 merged misc modifier -y without format -l makes no sense multiple sort options must set personality to get -x option no matching criteria specified
Try `%s --help' for more information. no process selection criteria no variables specified
Try `%s --help' for more information. not a number: %s number of columns must follow --cols, --width, or --columns number of rows must follow --rows or --lines o obsolete W option not supported (you have a /dev/drum?) only one heading option may be specified only one pattern can be provided
Try `%s --help' for more information. oom_adjustment (2^X) oom_score (badness) option --cumulative does not take an argument option --deselect does not take an argument option --forest does not take an argument option --heading does not take an argument option --no-heading does not take an argument option -O can not follow other format options option -r is ignored as SunOS compatibility option A is reserved option C is reserved option O is neither first format nor sort order options -N and -q cannot coexist
Try `%s --help' for more information. output partition was not found
 permission denied on key '%s' pid limit (%d) exceeded pidfile not valid
Try `%s --help' for more information. please report this bug priority %lu out of range process ID list syntax error process ID out of range process selection options conflict r read sectors reading key "%s" reads   requested writes s scale cannot be negative sec second chance parse failed, not BSD or SysV seconds argument `%s' failed seconds argument `%s' is not positive number sectors separators should not be repeated: %s seriously crashing: goodbye cruel world setting key "%s" shared memory detach shared memory remove simple some sid thing(s) must follow --sid something at line %d
 something broke swpd t tell <procps@freelists.org> what you expected tell <procps@freelists.org> what you want (-L/-T, -m/m/H, and $PS_FORMAT) terminal setting retrieval the -r option is reserved the option is exclusive:  thread display conflicts with forest display thread flags conflict; can't use H with m or -m thread flags conflict; can't use both -L and -T thread flags conflict; can't use both m and -m threads too large delay value total unable to create IPC pipes unable to execute '%s' unable to fork process unable to open directory "%s" unicode handling error
 unicode handling error (malloc)
 unknown AIX field descriptor unknown gnu long option unknown page size (assume 4096)
 unknown signal name %s unknown sort specifier unknown user-defined format specifier "%s" unsupported SysV option unsupported option (BSD syntax) user ID out of range user name does not exist waitpid warning: $PS_FORMAT ignored. (%s)
 warning: screen width %d suboptimal way bad window entry #%d corrupt, please delete '%s' write error writes    writing to tty failed your %dx%d screen size is bogus. expect trouble
 your kernel does not support diskstat (2.5.70 or above required) your kernel does not support diskstat. (2.5.70 or above required) your kernel does not support slabinfo or your permissions are insufficient Project-Id-Version: procps
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-09-23 13:53+0200
PO-Revision-Date: 2013-10-15 02:41+0000
Last-Translator: gabriell nascimento <gabriellhrn@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:04+0000
X-Generator: Launchpad (build 18115)
 
 Tente '%s --%s <%s|%s|%s|%s|%s|%s>'
  ou '%s --%s <%s|%s|%s|%s|%s|%s>'
 para textos de ajuda adicionais.
 
Para maiores detalhes veja ps(1).
 
O número prioritário padrão é +4. (snice +4 ...)
O intervalo de números prioritários varia de +20 (devagar) até -20 (rápido).
Os números prioritários negativos são restritos aos usuários administrativos.
 
O sinal padrão é TERM. Utilize -l ou -L para listar os sinais disponíveis.
Sinais particularmente úteis incluem HUP, INT, KILL, STOP, CONT, e 0.
Sinais alternativos podem ser especificados de três maneiras: -SIGKILL -KILL -9
 
Uso:
 %s [opções]
      --help exibe esta ajuda e sai
    DESATIVAR O QUE
   [ anon ]   [ pilha ]  %2lu dias  %s [nova prioridade] [opções] <expressão>
  %s [opções]
  %s [opções] <padrão>
  %s [options] <pid> [...]
  %s [opções] [atraso [contador]]
  %s [opções] [tty]
  %s [opções] [variável[=valor] ...]
  %s [opções] comando
  %s [options] pid...
  %s [sinal] [opções] <expressão>
  -v, --version  imprime versão da informação e sai
  total %16ldK
  total %8ldK
 "%s" é uma chave desconhecida "%s" deve estar no formato nome=valor %-*s TTY       %13d discos 
 %13d partições 
 %13lld ciclos de CPU ociosos
 %13lu memória ativa %s
 %13lu diminuir a memória %s
 %13lu memória livre %s
 %13lu swap livre %s
 %13lu memória inativa %s
 %13lu cache de swap %s
 %13lu memória total %s
 %13lu total de swap %s
 %13lu memória usada %s
 %13lu swap utilizado %s
 %13lu setores lidos
 %13lu gravações
 %13lu setores gravados
 %13u tempo de inicialização
 %13u interrupções
 Janela da coluna %d é muito estreita %s é obsoleto, valor não definido %s encerrado (pid %lu)
 %s"%s" não encontrado %s(%d): sintaxe inválida, continuando... %s:~3 %3u ~2total,~3 %3u ~2executando,~3 %3u ~2dormindo,~3 %3u ~2parado,~3 %3u ~2zumbi~3
 * Aplicando %s ...
 -%c requer argumento -L com H/-m/m mas nenhum PID/PGID/SID/SESS para NLWP para seguir -L sem -F não faz sentido
Tente`%s --help' para mais informações. -L/-T com H/m/-m e -o/-O/o/O não faz sentido -S requer k, K, m ou M (padrão é KiB) -T com H/-m/m mas nenhum PID para SPID para seguir -d não é permitido no modo "seguro" -d requer um argumento positivo -i não faz sentido com -v, -f, e -n -v não faz sentido com -i e -f erro de processamento de descritor de campo AIX Caches Ativos / Total (% usado) Ativos / Objetos totais (% usado) Tamanho Ativo/ Total (% usado) Ativas / Total de chapas (% utilizada) Tempo da CPU Tempo de CPU, centésimos Uso da CPU Cache Atraso alterado de %.1f para Escolher grupo do campo (1 - 4) Tamanho do código (KiB) Nome do comando/linha Comando desabilitado, modo 'A' necessário Comando desabilitado, ative %s com '-' ou '_' Grupos de Controle TTY controlador Cpu%-3d: Cpu(s): Tempo acumulado %s Dados+Pilha (KiB) Contagem de páginas sujas Id efetivo do usuário Nome efetivo de usuário Cada %.1fs:  DE Falhou ao abrir '%s' : %s Falhou em alterar prioridade do PID %d para %d: %s Falhou ao sinalizar pid '%d' com '%d': %s Modo floresta %s GiB Id do grupo Nome do grupo Alta: Máximo inválido Sinal inválido Usuário inválido Modo Irix %s KiB Última CPU usada (SMP) Localizar próximo inativo, use "L" Localizar string Baixa: Falhas grandes de página Máximo de tarefas = %d, mude para (0 é ilimitado) Mem: Uso de memória (RES) MiB Objeto mínimo / médio / máximo Falhas menores de página Valor nice Nenhuma cor para mapear! Números de threads Desligado Ligado Apenas 1 cpu detectada Páginas Pid do processo pai Prioridade ID de grupo de processo Id do processo Statos do processo ID real de usuário Nome real de usuário Renomear janela '%s' para (1-3 caracteres) Alterar prioridade do PID %d para valor Nome salvo de usuário Nome salvo de usuário Enviar sinal ao pid %d [%d/sigterm] Id da sessão Memória compartilhada (KiB) Mostra as threads %s Sinal %d (%s) pego por %s (%s).
 Tamanho Dormindo em função Swap: TTY não encontrado Sinalizadores de tarefas <sched.h> Tarefas Id do grupo da thread Threads Total Total: ID de grupo de processos do TTY USUÁRIO Ponto flutuante inaceitável Inteiro inaceitável Indisponível no modo seguro Comando desconhecido - tente 'h' para ajuda Imagem virtual (KiB) Qual usuário (vazio para todos) Configuração gravada em '%s' t ativa todos arquivo alternativo de System.map deve ser precedido de -n arquivo System.map alternativo deve ser precedido por N outro  faltando argumento b código de alinhamento ruim
 intervalo de atraso inválido '%s' argumento de iterações inválido '%s' pid inválido '%s' código de ordenação inválido buffer erro: a lista deve ser reiniciada primeiro cache não foi possível abrir tty não é possível definir largura para especificador de formato de uma macro (multi-coluna) não é possível usar modificadores de saída com saída definida pelo usuário não pode acontecer - problema #1 não pode acontecer - problema #2 não foi possível abrir "%s" não foi possível abrir o arquivo %s não foi possível obter status de %s não é possível executar strdup() no texto de personalidade
 larguras de colunas devem ser números decimais sem sinal saída de comando com um status diferente de zero, pressionar uma tecla para sair Opções de formato em conflito seleções de processo conflitando (U/p/u) não foi possível definir o ppid
 não foi possível encontrar o start_time
 atual atraso deve ser um inteiro positivo dup2 falhou '-' incorporado entre as opções BSD não faz sentido não faz sentido incorporar '-' entre as opções SysV lista de formato vazia lista de ordenação vazia ambiente especificado e personalidade desconhecida erro: %s
 erro: não é possível acessar /proc
 falhou ao abrir /proc/stat : %s falhou ao ler /proc/stat falha ao alocar memória falha ao realocar memória teste de número de cpus falhou openproc falhou: %s teste de tamanho máximo de pid falhou falhou ao analisar argumento falha ao analisar argumento de contagem falha ao analisar o argumento de contagem: '%s' tty get falhou fdopen corrigir erros de grandeza
 visão de floresta formato ou especificação de ordenação devem ser precedidos de -O formato ou especificação de ordenação deve ser precedido por O especificação de formato deve ser precedido por --format especificações do formato devem seguir -o especificação de formato deve ser precedida por o livre ambiente a partir do comprimento PROCPS_FROMLEN deve ser entre 8 e %d. Ignorando.
 opção ruim sinalizadores gerais não podem ser repetidos glob falhou ID do grupo fora do intervalo Nome de grupo não existe ajuda atraso ilegal descritor de campo AIX impróprio lista de formato imprópria lista imprópria lista de ordenação imprópria inact rcfile incompatível, você deve remover '%s' erro interno erro interno: nenhum PID ou PPID para a opção -j erro interno: nenhum PRI para a opção -c nome de grupo inválido: %s grupo de processos inválido: %s ID de processo inválido: %s id de sessão inválido: %s nome de usuário inválido: %s encerramento do pid %ld falhou l lista membro da lista não era um TTY lista de grupos PRM deve ser precedida de -R lista de nomes de comando devem ser precedidos de -C lista efetiva de grupos deve ser precedida por --group lista de usuários efetivos deve ser precedida por --user lista de trabalhos deve ser precedida por -J lista de IDs de processos devem ser precedidas por --pid lista de IDs de processos deve ser precedida por --pidd IDs da lista de processo devem seguir -p lista de IDs de processo deve ser precedida por p lista de grupos reais deve ser precedida por --Group lista de grupos reais devem seguir -G lista de usuários reais deve ser precedida por --User lista de usuários reais devem seguir -U IDs da lista de seções devem seguir -s lista de líderes de seção OU IDs de grupos efetivos foi inválida lista de líderes de seção OU nomes de grupos efetivos devem seguir -g lista de terminais (pty, tty...) deve seguir -t lista de ttys deve ser precedida por --tty lista de usuários deve seguir -u lista de usuários deve seguir U lista de zonas (contexto, rótulos, o que for) deve seguir -z especificação de ordenação longa deve ser precedida por 'k' especificação longa de ordenação deve ser precedida por --sort perdi meu CLS Perdi minha PGID m configuração mal formada "%s" mapeado: %ldK    gravável/privado: %ldK    compartilhado: %ldK
 mesclado misc modificador -y sem o formato -l não faz sentido várias opções de ordenação deve definir a personalidade para utilizar a opção -x nenhum critério correspondente especificado
Tente `%s --help' para mais informações. sem critério de seleção de processos nenhuma variável especificada
Tente `%s --help' para mais informação. não é um número: %s número de colunas deve ser precedido por --cols, --width, ou --columns número de linhas deve ser precedido por --rows ou --lines o opção obsoleta W não suportada (você tem um /dev/drum?) apenas uma opção de cabeçalho pode ser especificada apenas um padrão pode ser fornecido
Tente `%s --help' para mais informações. oom_adjustment (2^X) oom_score (deficiência) opção --cumulative não requer argumento opção --deselect não requer argumento opção --forest não requer argumento opção --heading não requer argumento opção --no-heading não requer argumento opção -O não pode ser precedida por outras opções de formato a opção -r é ignorada para compatibilidade com o sistema SunOS opção A está reservada opção C está reservada a opção O não é de primeiro formato nem de ordenação opções -N e -q não podem coexistir
Tente `%s --help' para mais informação. saída partição não foi encontrada
 permissão negada na chave '%s' Limite de pid (%d) excedido arquivo de PID não é válido
Tente `%s --help' para mais informações. por favor relate este erro prioridade %lu fora do parâmetro erro de sintaxe na lista de identificadores de processos ID do processo fora do intervalo conflito em opções de seleção de processo r setores lidos lendo chave "%s" lê   escritas requisitadas s escala não pode ser negativa seg falha na segunda chance do analisador, não é BSD ou SysV falha no argumento de segundos `%s' argumento de segundos `%s' não é um número positivo setores separadores não podem ser repetidos: %s falha grave: adeus, mundo cruel chave de configuração "%s" destacar memória compartilhada remoção da memória compartilhada simples algumas coisas do sid devem ser precedidas por --sid algo na linha %d
 algo quebrou swpd t diga a <procps@freelists.org> o que você esperava informe <procps@freelists.org> o que você deseja (-L/-T, -m/m/H, e $PS_FORMAT) Recuperação da configuração do terminal A opção -r é reservada a opção é exclusiva:  thread exibe conflito com exibição de floresta conflito em sinalizadores de thread; não é possível usar ambH com m ou -m conflito em sinalizadores de thread; não é possível usar -L e-T ao mesmo tempo conflito em sinalizadores de thread; não é possível usar m e-m ao mesmo tempo tópicos valor de atraso muito grande total não foi possível criar pipes IPC não foi possível executar '%s' não foi possível dividir processo não foi possível abrir o diretório "%s" erro de manipulação do unicode
 erro de manipulação do unicode (malloc)
 descritor de campo AIX desconhecido opção gnu longa desconhecida o tamanho da página é desconhecido (assumir 4096)
 nome %s de sinal desconhecido especificador de ordenação desconhecido formato desconhecido de especificador "%s" definido pelo usuário opção SysV não suportada opção não suportada (sintaxe BSD) ID do usuário fora do intervalo nome do usuário não existe waitpid Aviso: $PS_FORMAT ignorado. (%s)
 aviso: largura da tela %d não é ideal muito ruim entrada de janela #%d corrompida, por favor, exclua '%s' erro de gravação grava    falha ao escrever no tty seu tamanho de tela %dx%d é falso. Espere por problemas
 seu kernel não suporta diskstat (2.5.70 ou superior requerido) seu kernel não tem suporte a diskstat (necessário 2.5.70 ou acima) seu kernel não suporta slabinfo ou suas permissões são insuficientes 