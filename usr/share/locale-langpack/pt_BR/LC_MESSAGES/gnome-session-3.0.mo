��    I      d  a   �      0     1  4  N     �  R   �  g   �  Z   W	  '   �	     �	     �	     �	     
  	   '
  1   1
  	   c
  	   m
  (   w
  )   �
     �
     �
     �
  #      '   $  "   L     o     �     �     �     �     �     �     �      �          	             !   /  '   Q  %   y  	   �     �  '   �     �  P   �     1     H     X     e  
   t  ;     3   �  Q   �     A     P  &   g     �      �  #   �      �  *   	      4      U  /   v     �  =   �  	         
               #  (   0     Y  �  i  #   5  �  Y     �  i     q   k  w   �  $   U     z     �     �      �  
   �  I   �  	   9     C  6   Q  4   �  +   �     �     �  /     5   <  %   r     �      �  
   �     �     �                '  &   .     U     f     z     �  #   �  9   �  A   �     4     =  *   F  	   q  Z   {     �     �             
      @   +  :   l  U   �     �       2   ,     _  1     %   �  !   �  9   �  9   3     m  ,   �     �  M   �  
        )     ;     M     T  1   b     �         )      +   I   E   A       #   D   =      "          ;          C   *           0      /              H            9   4      	      
              >   <   8       $           -                   B   ,          3       ?              :   &          5   2   7                @          6                           G   1      '   %   F   .   !       (     - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Add Startup Program Additional startup _programs: Allow logout Browse… Choose what applications to start when you log in Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Could not display help document Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don't prompt for user confirmation Edit Startup Program Enable debugging code Enabled Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Icon Ignoring any existing inhibitors Log out No description No name Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Select Command Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Applications Startup Applications Preferences The startup command cannot be empty The startup command is not valid This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-05-18 08:40+0000
PO-Revision-Date: 2015-07-10 03:30+0000
Last-Translator: Ricardo Barbosa <Unknown>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:40+0000
X-Generator: Launchpad (build 18115)
X-Project-Style: gnome
Language: pt_BR
  - gerenciador de sessões do GNOME %s [OPÇÃO...] COMANDO

Executa o COMANDO enquanto inibe algumas funcionalidades da sessão.

  -h, --help        Mostra esta ajuda
  --version         Mostra a versão do programa
  --app-id ID       O id do aplicativo a ser usado
                    quando inibir (opcional)
  --reason MOTIVO   O motivo para inibir (opcional)
  --inhibit ARG     Lista de elementos a serem inibidos, separados por dois
                    pontos:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Não executa o COMANDO e no lugar espera indefinidamente

Se nenhuma opção --inhibit for especificada, assume-se como ocioso (idle).
 %s requer um argumento
 Ocorreu um problema e o sistema não pôde ser recuperado.
Por favor encerre a sessão e tente novamente. Ocorreu um problema e o sistema não pôde ser recuperado. Todas as extensões foram desativadas por precaução. Ocorreu um problema e o sistema não pôde ser recuperado. Por favor, entre em contato com um administrador de sistemas Uma sessão com nome "%s" já existe AUTOSTART_DIR Adicionar programa inicial _Programas iniciais adicionais: Permitir encerramento de sessão Navegar… Escolha quais os aplicativos que irão iniciar quando sua sessão iniciar Co_mando: Com_entário: Não foi possível conectar ao gerenciador de sessões Não foi possível criar o soquete de escuta ICE: %s Não é possível exibir documento de ajuda Personalizar Sessão personalizada Desabilita checagem de aceleração de hardware Não carregar aplicativos especificados pelo usuário Não pedir configuração do usuário Editar programa inicial Habilitar código de depuração Habilitado Ocorreu falha ao executar %s
 GNOME GNOME experimental GNOME sobre Wayland Ícone Ignorar qualquer um inibidor existente Encerrar sessão Nenhuma descrição Sem nome Não respondendo Oh não! Alguma coisa está errada. Substituir os diretórios padrões de início automático Por gentileza selecione uma sessão personalizada a ser executada Desligar Programa Programa chamado com opções conflitantes Reiniciar Recusando a nova conexão do cliente porque a sessão está sendo desligada neste momento
 Lembrar dos aplicativos Reno_mear sessão SESSION_NAME Selecionar comando Sessão %d Não são permitidos a nomes de sessões conterem caracteres "/" Não são permitidos a nomes de sessões iniciarem com "." Não são permitidos a nomes de sessões iniciarem com "." ou conterem caracteres "/" Sessões para usar Mostrar alerta de extensão Mostrar o diálogo da "falha da baleia" para teste Aplicativos iniciais de sessão Preferências dos aplicativos iniciais de sessão O comando inicial não pode ser vazio O comando inicial não é válido Esta entrada permite a você selecionar uma sessão salva Este programa está bloqueando o encerramento da sessão. Essa sessão o leva ao GNOME Essa sessão o leva ao GNOME, usando Wayland Versão deste aplicativo _Automaticamente lembrar aplicativos em execução quando encerrar da sessão _Continuar _Encerrar sessão _Encerrar sessão _Nome: _Nova sessão _Lembrar dos aplicativos atualmente em execução _Remover sessão 