��          �   %   �      p     q     �  +   �  @   �  5        M  ,   _     �     �     �  $   �  	   �  2   �  $   2     W     Z     l  #   �  !   �     �  %   �               8     <     E     Y  �  h     �       -   3  >   a  8   �     �  ?   �     )     E     b  &   r     �  <   �  0   �     	     	  *   7	  $   b	  (   �	     �	  3   �	  +   �	     (
     A
  	   E
     O
     e
                    
                                                                                          	             <b>%s</b>
Hardware present: %s <b>%s</b>
Invalid Driver! <b>Currently Installed Windows Drivers:</b> <span size="larger" weight="bold">Select <i>inf</i> file:</span> Are you sure you want to remove the <b>%s</b> driver? Configure Network Could not find a network configuration tool. Driver is already installed. Error while installing. Install Driver Is the ndiswrapper module installed? Location: Module could not be loaded. Error was:

<i>%s</i>
 Ndiswrapper driver installation tool No No file selected. Not a valid driver .inf file. Please drag an '.inf' file instead. Root or sudo privileges required! Select inf File Unable to see if hardware is present. Windows Wireless Drivers Wireless Network Drivers Yes _Install _Install New Driver _Remove Driver Project-Id-Version: ndisgtk
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-10-02 17:50+0200
PO-Revision-Date: 2010-08-27 05:11+0000
Last-Translator: André Gondim <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:44+0000
X-Generator: Launchpad (build 18115)
 <b>%s</b>
Hardware presente: %s <b>%s</b>
Driver inválido! <b>Drivers Windows atualmente instalados:</b> <span size="larger" weight="bold">Selecione <i>inf</i>:</span> Você tem certeza que deseja remover o driver <b>%s</b>? Configurar Rede Não é possível achar a ferramente de configuração de rede. Driver já está instalado. Erro durante a instalação. Instalar Driver O módulo ndiswrapper está instalado? Localização: Não foi possível carregar Módulo. O erro era:

<i>%s</i>
 Ferramenta de instalação de driver Ndiswrapper Não Nenhum arquivo selecionado. Não é um arquivo de driver .inf válido. Por favor arraste um arquivo '.inf'. Requeridos privilégios de root ou sudo! Selecione o arquivo inf Impossível verificar se o hardware está presente. Drivers Windows para Placas de Rede Sem Fio Drivers de rede wireless Sim _Instalar _Instalar Novo Driver _Remover Driver 