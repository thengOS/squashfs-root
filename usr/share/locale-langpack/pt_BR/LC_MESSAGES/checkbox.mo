��    �       U  (      p5  �   q5  c   *6  	   �6     �6     �6  �   �6  �   37  �   �7  �   �8  !   �9     �9  	   �9     �9     �9     �9  "   �9     �9  #   :  E   ;:     �:  #   �:  !   �:  5   �:  I   ;  8   U;  $   �;  3   �;  &   �;  +   <     :<     T<  *   i<     �<  /   �<  ?   �<  3   =  2   M=  7   �=  /   �=  9   �=     ">  7   @>  .   x>  F   �>  D   �>  E   3?  C   y?  C   �?  =   @  <   ?@  8   |@  I   �@  A   �@  #   AA  
   eA     pA     |A     �A  F   �A     �A  R   B  a   eB  G   �B  L   C     \C     tC     �C     �C     �C  *   �C     �C     �C     D  	   D  "   D     >D     JD  (   WD  .   �D  /   �D  V   �D  <   6E  9   sE  7   �E  !   �E  )   F  &   1F  +   XF  )   �F  9   �F  Z   �F     CG  �   [G  M   �G  ?   +H  <   kH  >   �H  i   �H  k   QI  ]   �I     J  #   7J     [J     gJ  �   tJ  -    K     NK     jK  C   �K  +   �K     �K  ?   L     ML     VL  
   qL  "   |L     �L     �L     �L  3   �L      M     M     M  2   2M  6   eM  N   �M  M   �M  P   9N  Q   �N  	   �N  
   �N  #   �N  %   O     ;O     NO  J   SO     �O  )   �O     �O  r   �O     HP     UP  )   [P     �P  
   �P     �P     �P  �   �P     bQ     Q  3   �Q     �Q     �Q     �Q     	R     "R     4R     @R     ER  )   ZR     �R     �R  G   �R     �R     �R  :   S  f   CS  S   �S     �S     T     T  	   #T  $   -T     RT     fT  
   zT  %   �T     �T     �T     �T  	   �T     �T  	   �T     U  :   U  7   QU  :   �U     �U     �U     �U     �U     �U     V  (   #V     LV     cV     pV  "   ~V     �V     �V     �V     �V     �V     �V     �V     �V     W     W  
   %W     0W  /   =W  *   mW  )   �W  9   �W     �W     X     X  
   0X  [  ;X     �Z  �   �[  �  G\  �   �]  �   �^  �   `_  �   J`  �   #a  �   �a    �b    �c  �  �d  �   ef  �   g  �   �g  �   lh  (  ^i    �k  T  �m  K  �o  �   Gr  �    s  ?  �s  �  (u  K  w  ~  kx  o  �z  >  Z|  V  �}  h  �~  �   Y�  �   �  �   �  )  ��  m  
�  m  x�  o  �  q  V�  m  ȉ  n  6�  r  ��  q  �  �  ��  �  :�  �  В  �  b�  �  ��  �  ��  �  �  �  ��  U  F�  M  ��  .  �  \  �    v�  |  ��  �   �  �  �  �  }�  �  �  �  ��    C�    U�  �   e�  V  B�  \  ��  <  ��    3�  R  <�  Z  ��  �   �  �  ��  �  `�  `  P�  �   ��  �   k�  �   ]�    ��  ,  ��  �   ,�    �    ,�  ?  4�    t�  �   ��  �   N�  `  G�  �   ��  �  q�  �  �  �  ��  S  ��  �   �  �   ��  I  l�  �   ��  �  d�  4  V�  Q  ��  �  ��  U  g�  ,  ��  e  ��  H  P�  �   ��    x�    ��  E  ��  �   ��    ��    ��  �  ��  �   ��    ��  3  ��    ��    �  �  1�  j   ��  i   H�  i   ��  h   �  I  ��    ��  �  ��  �   ��    ��  �  ��  �   Z�  �   F  �   3 �   � �   �   �    � �   � �   G �   / N   @  j	 F  �
 9  � J  , �   w �   V �   5       �   , +  �      # �  ? �   f  � ]  D 5  � D  � �    �  �! �  r# f   %    {%    �% @   �%    �%    & 8   & P   R&    �& '   �& 1   �&    '    )'    ?'    V'    s' #   |' :   �' *   �'    ( (   ( -   B( 0   p(    �(    �( Z   �( .   )    E)    c)    �)    �)    �) /   �)    *    *    5*    I*    ^*    w*    �* '   �* *   �* 2    + 5   3+ )   i+    �+    �+    �+    �+    �+    ,    8, [   V,    �, 7   �, ?   �, $   >-    c- A   q- 9   �- J   �- 1   8. J   j. H   �. i   �. >   h/ J   �/ D   �/ (   70    `0 
   }0 
   �0 
   �0    �0 .   �0 3   �0 .   1 3   G1    {1    �1    �1    �1 L  �1    3    3    &3    -3    :3 #   L3 !   p3    �3    �3    �3    �3    �3    �3    �3    
4    4    .4    =4    C4    I4    \4 $   a4 
   �4    �4    �4    �4 @   �4 A   5 B   T5 B   �5 A   �5 B   6 A   _6 A   �6 D   �6 B   (7 E   k7    �7 *   �7 �   �7 0   �8 #   �8 K   �8     79 B   X9 �   �9    ,: 0   I: !   z: `   �:    �: 7   ; F   U; 2   �;    �; �   �; �   p< p   = �   v= m   > �   v> o   
? �   z? l   @ a   x@ N   �@ �   )A ;   �A 	   B    B A  ;B `   }C 9   �C 0   D o   ID �   �D �   �E }   F �   �F �   G Z   �G W   �G �   VH �   I :   �I *   &J B   QJ �   �J {   jK �   �K �   |L {   �L �   {M }   )N �   �N {   WO �   �O }   �P �   �P {   �Q �   +R    �R �   YS    T �   �T {   =U �   �U E   gV F   �V �   �V �  �W Q   HY O   �Y �   �Y U   �Z    9[    T[    c[ D   u[ 	   �[    �[    �[ 	   �[    �[    �[    �[    \    #\    9\ E   O\ 0   �\ +   �\ &   �\ 3   ]    M] @   l] F   �] ?   �] /   4^ @   d^ 5   �^ V   �^ H   2_ e   {_    �_    �_ �   `    �`    �` ?   �`    (a 0   0a    aa    oa    ua    }a 	   �a    �a    �a    �a    �a    �a    �a 5   �a    
b    b A   7b 6   yb    �b    �b    �b 
   �b     �b !   �b @   c    Lc �  Pc �   �d �   �e    *f 
   7f    Bf �   Nf �   �f �   �g �   �h $   li    �i 	   �i    �i    �i    �i 0   �i    �i ,   j A   <j    ~j /   �j =   �j :   �j V   :k >   �k -   �k <   �k (   ;l 8   dl    �l    �l .   �l    m .   m N   Nm C   �m P   �m 7   2n +   jn B   �n    �n ?   �n @   5o N   vo O   �o M   p N   cp G   �p E   �p ;   @q 8   |q K   �q E   r (   Gr    pr    �r '   �r (   �r W   �r #   :s ]   ^s f   �s h   #t \   �t )   �t #   u    7u    Ju    ^u 4   yu    �u    �u    �u    �u (   v    .v    ?v 7   Qv G   �v K   �v ~   w u   �w T   x X   gx 4   �x 6   �x /   ,y 7   \y 5   �y D   �y n   z    ~z �   �z R   { [   o{ O   �{ Z   | s   v| s   �| g   ^} ,   �} 9   �}    -~    =~ �   K~ B    4   W 1   � V   � 1   �    G� ?   `�    �� $   ��    Ҁ 0   ހ 	   �    �    -� P   I�    ��    ��    �� -   ԁ 9   � P   <� Z   �� Y   � [   B�    ��    �� "   �� #   ��    � 
   � s   (�    �� 8   ��    ܄ z   �    _�    p� (   u�    ��    ��    Ņ    ܅ �   � !   �� "   ֆ <   �� )   6�    `�    r�    ��    ��        ԇ    �� *   ��    %�    5� M   I�    ��    �� N   Ɉ g   � a   ��    �    ��    �    � -   &�    T� !   k�    �� ,   ��    ˊ !   �    �    �    -�    9�    G� L   a� C   �� I   �    <�    Y�    u�    ��    ��    �� 3   ��    �    	�    �    ,� 	   I� 	   S�    ]�    s�    ��    ��    ��    ��    ��    ύ    ލ    � :   �� )   5� +   _� ;   ��    ǎ    �    �    � �  � %  � �   � �  � �   �   Ö �   ؗ   �� �   �� �   I� )  A� R  k�   �� �   ӟ �   �� �   ��   _� ~  v� �  �� �  � �  /�   � �   � �  ʯ   N� a  d� �  ƴ �  �� �  4� �  ں �  c� �   �� �   ��    �� I  �� �  �� q  e� y  �� �  Q� w  �� �  N� �  �� �  X� �  �� �  �� �  �� �  "� �  �� �  P� �  �� �  ��    � r  �� p  � �  �� e  � I  ��   �� �  �� �  �� �  ?� �  � ?  �� l  � �   �� g  �� s  �� X  ]� #  �� t  �� `  O� �   �� �  �� J  �� u  �  �   J    �       � ^  � �   F    > +  _	 S  �
 g  � �   G      7 �   � �  � �  b *  ` �  � �    �   � e  � �   � ;  � �  - |  �  �  O" p  "$ j  �% �  �& b  �(   * E  %+ V  k, �  �- �   d/ 3  V0 A  �1 I  �2   5 ^  #6 a  �7 T  �8 N  9: �  �; v   m= r   �= x   W> s   �> �  D? b  �A   =D "  ]F X  �G �  �I 1  �K ;  M �   ?N �   O �   P 8  Q ]  ;R �   �S   XT   [U r  jV k  �W f  IY Y  �Z e  
\ �   p] �   d^ �   Y_ 2  I` R  |a �   �b W  �c "  e 8  7f _  pg =  �i �  l �  �m �  fo }  �p �  fr \  At �  �v {   yx -   �x 7   #y W   [y    �y    �y ;   �y @   z    Wz 7   pz :   �z &   �z !   
{ "   ,{ *   O{    z{ ,   �{ 7   �{ /   �{    | 7   1| )   i| /   �| (   �| 	   �| R   �| D   I} 3   �} 4   �} 1   �} 3   )~ .   ]~ D   �~ +   �~ /   �~ .   - *   \ .   � ,   � 4   � ?   � M   X� K   �� [   � ?   N� .   �� /   �� 0   � 0   � 2   O� 4   �� 3   �� m   �    Y� P   q� I    C   �    P� F   a� C   �� n   � 9   [� U   �� I   � �   5� @   �� ^   �� Q   ^� =   �� &   �    �    !�    2� &   C� 1   j� 7   �� 2   Ԉ 7   �    ?�    G�    Y� #   o� �  ��    /�    G�    W�    ^�    o� 8   �� =   ŋ    �    �    (�    :�    M�    k�    �    ��    ��        ӌ    ٌ    ߌ    � )   ��    "�    3�    R�    b� J   {� P   ƍ K   � Q   c� P   �� Q   � P   X� P   �� S   �� Q   N� T   ��    �� ?   � �   H� ?   � ,   %� b   R�    �� O   Ғ �   "� *   ؓ 9   � %   =� o   c� $   Ӕ L   �� 1   E� =   w�    �� �   �� �   U� �   
� �   ��    =� �   ��    z� �   �� }   �� `   1� H   �� �   ۛ A   ��    � *   �� _  %� f   �� @   � 5   -� n   c� �   ҟ �   �� �   `� �   � �   � Q   8� `   �� �   � �   ͤ 8   ƥ D   �� a   D� �   �� �   �� �   � �   �� �   :� �   �� �   v� �   � �   ī �   R� �   � �   �� �   h� �   � �   �� �   ,� �   � �   �� �   _� �   � J   �� \   �� �   Q� �  � e   �� j   $�   �� h   �� !   �    *� !   =� M   _�    ��    ��    ʹ    ׹    �    ��    �    �    6�    U� [   t� =   к =   � 1   L� @   ~� '   �� C   � d   +� e   �� ?   �� J   6� ?   �� T   �� d   � x   {�    ��    � �    �    �    � S   �    o� 8   {�    ��    �� 
   ��    �� 	   ��    ��    ��    	�    �    "�    '� @   ?�    �� %   �� A   �� <   ��    :�    ?�    E�    K� +   \� *   �� N   ��    �            @   ;  R    M   k  �   $   �     �  >       >  I  1   �   �   .   
        �   !  �  !      �            "      z  �       G     *      �     �           �            /   /      G  �  k   �  �  u    .  a  '          A  ;  �   _     f       �   �       e   �   �   2  �         �   �  0      N      9   �   �  �     �  �                 �  �  �   V       �   �        �    <  �      P             �  {  �      0       �  �                a       �          �   }  E   <   @  �  �      Y  �              �      _  p  q  	  �      �       �  {   
   z  �   y    �  d  �  C         �   ~  b   �       Y            �   T     �  }   �  2  9      o  �  �      &       3       �   8  �  I   C  �          B  �  �   t  �  �  �            P  �          s  g       �   6  �       �       �   n  \          p   �          l  �   �   ~  \      �   �  #      �   �  m  �           �   j  n   1  X     K    �   �  ,              �   O      �   �   �       H  6   �  q  7      �   5  �  �     �      V         �   J       *      �  �  �       5  Z         h          �  6      ]   �   �      
  ^  v  3  �   X  J      �   O       [   	          4      H   i    �  �  -       �   )      (  �      4             U   i  �   t          �      _            G   j           k     b  '      �  .    E  �   �  �          �   �      �  o  |  �  r   e      �  F   a  [  �  n  �      �      9  �   /  Q  ,   O  h   t  �   R       U      �       +  J      :        W    z   �        +  ^              g  �   ?  �  �  �  "   B  �         �  ~   d   �     :  ]       �  �     �   T      2   �  S    �  �  S  w  �      �  (  �  �           I    �      �  M        �           �   P    (   A       �   m      �           #   7   \               �  �   *   o   m   �   '   f  $  L    �     Q          C  T      �  V      �   �   S   �   �  [          :   Q  �       �   %       x       L  8  )   �   �  �       �    ]    �    �  %             `  q   �           �     L   #      �   ^  �     `      U      |   �   w       �  ?   �   �            �   u       E      ;   F      �   �   �  �       y  �   ,  �   d      �    {      �  �               �       �         s  %     �  i               �  �      �   =  �       �   X  -        v   �              -  �  1  �      �    �  �   &          W   =   l   =  c  �      �  <  y   �  &  �   c      �  �   �  �                   !   s      �   r      �  �  w  p      �   �      �    ?  D  )  >      �  �         �  �   �  �  j  8   7          �  �  �   H  �  A  �   Z   N   �  v               x  �               W  �   K  B       �       "  R  3  g  �   	   D  �   h  �   �   e  �        �       �          }  �      �      �  �  �       l  �   �   �          $     �   �   �         K               �      M      �        Z  b  �      0  �           �  �       +         D   x      �               �   �  �               Y  F  �   `       |  r  �   �          �  �               5   4  c   @  �   f  N         u   

Warning: Some tests could cause your system to freeze or become unresponsive. Please save all your work and close all other running applications before beginning the testing process.    Determine if we need to run tests specific to portable computers that may not apply to desktops.  Results   Run   Selection   Takes multiple pictures based on the resolutions supported by the camera and
 validates their size and that they are of a valid format.  Test that the system's wireless hardware can connect to a router using the
 802.11a protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11a protocol.  Test that the system's wireless hardware can connect to a router using the
 802.11b protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11b protocol.  Test that the system's wireless hardware can connect to a router using the
 802.11g protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11g protocol. %(key_name)s key has been pressed &No &Previous &Skip this test &Test &Yes 10 tests completed out of 30 (30%) Abort signal from abort(3) All required keys have been tested! Archives the piglit-summary directory into the piglit-results.tar.gz. Are you sure? Attach log from fwts wakealarm test Attach log from rendercheck tests Attaches a copy of /var/log/dmesg to the test results Attaches a dump of the udev database showing system hardware information. Attaches a list of the currently running kernel modules. Attaches a report of CPU information Attaches a report of installed codecs for Intel HDA Attaches a report of sysfs attributes. Attaches a tarball of gcov data if present. Attaches dmidecode output Attaches info on DMI Attaches information about disk partitions Attaches lshw output Attaches the FWTS results log to the submission Attaches the audio hardware data collection log to the results. Attaches the bootchart log for bootchart test runs. Attaches the bootchart png file for bootchart runs Attaches the contents of /proc/acpi/sleep if it exists. Attaches the contents of the /etc/modules file. Attaches the contents of the various modprobe conf files. Attaches the firmware version Attaches the graphics stress results to the submission. Attaches the installer debug log if it exists. Attaches the log from the 250 cycle Hibernate/Resume test if it exists Attaches the log from the 250 cycle Suspend/Resume test if it exists Attaches the log from the 30 cycle Hibernate/Resume test if it exists Attaches the log from the 30 cycle Suspend/Resume test if it exists Attaches the log from the single suspend/resume test to the results Attaches the log generated by cpu/scaling_test to the results Attaches the output of udev_resource, for debugging purposes Attaches the screenshot captured in graphics/screenshot. Attaches the screenshot captured in graphics/screenshot_fullscreen_video. Attaches very verbose lspci output (with central database Query). Attaches very verbose lspci output. Audio Test Audio tests Automated CD write test Automated DVD write test. Automated check of the suspend log to look for errors reported by fwts Automated optical read test. Automated test case to make sure that it's possible to download files through HTTP Automated test case to verify availability of some system on the network using ICMP ECHO packets. Automated test to store bluetooth device information in checkbox report Automated test to walk multiple network cards and test each one in sequence. Benchmark for each disk Benchmarks tests Bluetooth Test Bluetooth tests Bootchart information. Broken pipe: write to pipe with no readers Building report... CD write test. CPU Test CPU tests CPU utilization on an idle system. Camera Test Camera tests Check failed result from shell test case Check job is executed when dependency succeeds Check job is executed when requirements are met Check job result is set to "not required on this system" when requirements are not met Check job result is set to uninitiated when dependency fails Check logs for the stress poweroff (100 cycles) test case Check logs for the stress reboot (100 cycles) test case Check stats changes for each disk Check success result from shell test case Check that VESA drivers are not in use Check that hardware is able to run Unity 3D Check that hardware is able to run compiz Check the time needed to reconnect to a WIFI access point Check to see if CONFIG_NO_HZ is set in the kernel (this is just a simple regression check) Checkbox System Testing Checkbox did not finish completely.
Do you want to rerun the last test,
continue to the next test, or
restart from the beginning? Checks that a specified sources list file contains the requested repositories Checks the battery drain during idle.  Reports time until empty Checks the battery drain during suspend.  Reports time until Checks the battery drain while watching a movie.  Reports time Checks the length of time it takes to reconnect an existing wifi connection after a suspend/resume cycle. Checks the length of time it takes to reconnect an existing wired connection
 after a suspend/resume cycle. Checks the sleep times to ensure that a machine suspends and resumes within a given threshold Child stopped or terminated Choose tests to run on your system: Codec tests Collapse All Collect audio-related system information. This data can be used to simulate this computer's audio subsystem and perform more detailed tests under a controlled environment. Collect info on color depth and pixel format. Collect info on fresh rate. Collect info on graphic memory. Collect info on graphics modes (screen resolution and refresh rate) Combine with character above to expand node Command not found. Command received signal %(signal_name)s: %(signal_description)s Comments Common Document Types Test Components Configuration override parameters. Continue Continue if stopped DVD write test. Dependencies are missing so some jobs will not run. Deselect All Deselect all Detailed information... Detects and displays disks attached to the system. Detects and shows USB devices attached to this system. Determine whether the screen is detected as a multitouch device automatically. Determine whether the screen is detected as a non-touch device automatically. Determine whether the touchpad is detected as a multitouch device automatically. Determine whether the touchpad is detected as a singletouch device automatically. Disk Test Disk tests Disk utilization on an idle system. Do you really want to skip this test? Don't ask me again Done Dumps memory info to a file for comparison after suspend test has been run Email Email address must be in a proper format. Email: Ensure the current resolution meets or exceeds the recommended minimum resolution (800x600). See here for details: Enter text:
 Error Exchanging information with the server... Executing %(test_name)s Expand All ExpressCard Test ExpressCard tests Failed to contact server. Please try
again or upload the following file name:
%s

directly to the system database:
https://launchpad.net/+hwdb/+submit Failed to open file '%s': %s Failed to process form: %s Failed to upload to server,
please try again later. Fingerprint reader tests Firewire Test Firewire disk tests Floating point exception Floppy disk tests Floppy test Form Further information: Gathering information from your system... Graphics Test Graphics tests Hangup detected on controlling terminal or death of controlling process Hibernation tests Hotkey tests I will exit automatically once all keys have been pressed. If a key is not present in your keyboard, press the 'Skip' button below it to remove it from the test. If your keyboard lacks one or more keys, press its number to skip testing that key. Illegal Instruction In Progress Info Info Test Information not posted to Launchpad. Informational tests Input Devices tests Input Test Internet connection fully established Interrupt from keyboard Invalid memory reference Key test Keys Test Kill signal LED tests List USB devices Lists the device driver and version for all audio devices. Make sure that the RTC (Real-Time Clock) device exists. Maximum disk space used during a default installation test Media Card Test Media Card tests Memory Test Memory tests Miscellanea Test Miscellaneous tests Missing configuration file as argument.
 Mobile broadband tests Monitor Test Monitor tests Move a 3D window around the screen Ne&xt Ne_xt Network Information Networking Test Networking tests Next No Internet connection Not Resolved Not Started Not Supported Not Tested Not required One of debug, info, warning, error or critical. Open and close 4 3D windows multiple times Open and close a 3D window multiple times Open, suspend resume and close a 3D window multiple times Optical Drive tests Optical Test Optical read test. PASSWORD:  PURPOSE:
     Check that external line out connection works correctly
STEPS:
     1. Insert cable to speakers (with built-in amplifiers) on the line out port
     2. Open system sound preferences, 'Output' tab, select 'Line-out' on the connector list. Click the Test button
     3. On the system sound preferences, select 'Internal Audio' on the device list and click 'Test Speakers' to check left and right channel
VERIFICATION:
     1. Do you hear a sound in the speakers? The internal speakers should *not* be muted automatically
     2. Do you hear the sound coming out on the corresponding channel? PURPOSE:
    Block cap keys LED verification
STEPS:
    1. Press "Block Cap Keys" to activate/deactivate cap keys blocking
    2. Cap Keys LED should be switched on/off every time the key is pressed
VERIFICATION:
    Did the Cap Keys LED light as expected? PURPOSE:
    Camera LED verification
STEPS:
    1. Select Test to activate camera
    2. Camera LED should light for a few seconds
VERIFICATION:
    Did the camera LED light? PURPOSE:
    Check that external line in connection works correctly
STEPS:
    1. Use a cable to connect the line in port to an external line out source.
    2. Open system sound preferences, 'Input' tab, select 'Line-in' on the connector list. Click the Test button
    3. After a few seconds, your recording will be played back to you.
VERIFICATION:
    Did you hear your recording? PURPOSE:
    Check that the various audio channels are working properly
STEPS:
    1. Click the Test button
VERIFICATION:
    You should clearly hear a voice from the different audio channels PURPOSE:
    Check touchscreen drag & drop
STEPS:
    1. Double tap, hold, and drag an object on the desktop
    2. Drop the object in a different location
VERIFICATION:
    Does the object select and drag and drop? PURPOSE:
    Check touchscreen pinch gesture for zoom
STEPS:
    1. Place two fingers on the screen and pinch them together
    2. Place two fingers on the screen and move then apart
VERIFICATION:
    Does the screen zoom in and out? PURPOSE:
    Check touchscreen tap recognition
STEPS:
    1. Tap an object on the screen with finger. The cursor should jump to location tapped and object should highlight
VERIFICATION:
    Does tap recognition work? PURPOSE:
    Create jobs that use the CPU as much as possible for two hours. The test is considered passed if the system does not freeze. PURPOSE:
    HDD LED verification
STEPS:
    1. Select "Test" to write and read a temporary file for a few seconds
    2. HDD LED should light when writing to/reading from HDD
VERIFICATION:
    Did the HDD LED light? PURPOSE:
    HDMI audio interface verification
STEPS:
    1. Plug an external HDMI device with sound (Use only one HDMI/DisplayPort interface at a time for this test)
    2. Click the Test button
VERIFICATION:
    Did you hear the sound from the HDMI device? PURPOSE:
    Keep tester related information in the report
STEPS:
    1. Tester Information
    2. Please enter the following information in the comments field:
       a. Name
       b. Email Address
       c. Reason for this test run
VERIFICATION:
    Nothing to verify for this test PURPOSE:
    Numeric keypad LED verification
STEPS:
    1. Press "Block Num" key to toggle numeric keypad LED
    2. Click on the "Test" button to open a window to verify your typing
    3. Type using the numeric keypad both when the LED is on and off
VERIFICATION:
    1. Numeric keypad LED status should toggle everytime the "Block Num" key is pressed
    2. Numbers should only be entered in the keyboard verification window when the LED is on PURPOSE:
    Power LED verification
STEPS:
    1. Power LED should be on while device is switched on
VERIFICATION:
    Does the power LED light as expected? PURPOSE:
    Take a screengrab of the current screen (logged on Unity desktop)
STEPS:
    1. Take picture using USB webcam
VERIFICATION:
    Review attachment manually later PURPOSE:
    Take a screengrab of the current screen after suspend (logged on Unity desktop)
STEPS:
    1. Take picture using USB webcam
VERIFICATION:
    Review attachment manually later PURPOSE:
    Take a screengrab of the current screen during fullscreen video playback
STEPS:
    1. Start a fullscreen video playback
    2. Take picture using USB webcam after a few seconds
VERIFICATION:
    Review attachment manually later PURPOSE:
    Tests that the systems wireless hardware can connect to a router using
    no security and the 802.11b/g protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the B and G wireless bands
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use no security
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using
    no security and the 802.11n protocol.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the N wireless band
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use no security
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using WPA
    security and the 802.11b/g protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the B and G wireless bands
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use WPA2 and ensure the PSK matches that set in ROUTER_PSK
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using WPA
    security and the 802.11n protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the N wireless band
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use WPA2 and ensure the PSK matches that set in ROUTER_PSK
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    This test checks that the manual plugin works fine
STEPS:
    1. Add a comment
    2. Set the result as passed
VERIFICATION:
    Check that in the report the result is passed and the comment is displayed PURPOSE:
    This test cycles through the detected video modes
STEPS:
    1. Click "Test" to start cycling through the video modes
VERIFICATION:
    Did the screen appear to be working for each mode? PURPOSE:
    This test tests the basic 3D capabilities of your video card
STEPS:
    1. Click "Test" to execute an OpenGL demo. Press ESC at any time to close.
    2. Verify that the animation is not jerky or slow.
VERIFICATION:
    1. Did the 3d animation appear?
    2. Was the animation free from slowness/jerkiness? PURPOSE:
    This test will check that a DSL modem can be configured and connected.
STEPS:
    1. Connect the telephone line to the computer
    2. Click on the Network icon on the top panel.
    3. Select "Edit Connections"
    4. Select the "DSL" tab
    5. Click on "Add" button
    6. Configure the connection parameters properly
    7. Click "Test" to verify that it's possible to establish an HTTP connection
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will check that a USB audio device works correctly
STEPS:
    1. Connect a USB audio device to your system
    2. Click "Test", then speak into the microphone
    3. After a few seconds, your speech will be played back to you
VERIFICATION:
    Did you hear your speech played back through the USB headphones? PURPOSE:
    This test will check that bluetooth connection works correctly
STEPS:
    1. Enable bluetooth on any mobile device (PDA, smartphone, etc.)
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. In the device write the PIN code automatically chosen by the wizard
    6. The device should pair with the computer
    7. Right-click on the bluetooth icon and select browse files
    8. Authorize the computer to browse the files in the device if needed
    9. You should be able to browse the files
VERIFICATION:
    Did all the steps work? PURPOSE:
    This test will check that headphones connector works correctly
STEPS:
    1. Connect a pair of headphones to your audio device
    2. Click the Test button to play a sound to your audio device
VERIFICATION:
    Did you hear a sound through the headphones and did the sound play without any distortion, clicks or other strange noises from your headphones? PURPOSE:
    This test will check that internal speakers work correctly
STEPS:
    1. Make sure that no external speakers or headphones are connected
       If testing a desktop, external speakers are allowed
    2. Click the Test button to play a brief tone on your audio device
VERIFICATION:
    Did you hear a tone? PURPOSE:
    This test will check that recording sound using an external microphone works correctly
STEPS:
    1. Connect a microphone to your microphone port
    2. Click "Test", then speak into the external microphone
    3. After a few seconds, your speech will be played back to you
VERIFICATION:
    Did you hear your speech played back? PURPOSE:
    This test will check that recording sound using the onboard microphone works correctly
STEPS:
    1. Disconnect any external microphones that you have plugged in
    2. Click "Test", then speak into your internal microphone
    3. After a few seconds, your speech will be played back to you.
VERIFICATION:
    Did you hear your speech played back? PURPOSE:
    This test will check that the built-in camera works
STEPS:
    1. Click on Test to display a still image from the camera for ten seconds.
VERIFICATION:
    Did you see the image? PURPOSE:
    This test will check that the built-in camera works
STEPS:
    1. Click on Test to display a video capture from the camera for ten seconds.
VERIFICATION:
    Did you see the video capture? PURPOSE:
    This test will check that the display is correct after suspend and resume
STEPS:
    1. Check that your display does not show up visual artifacts after resuming.
VERIFICATION:
    Does the display work normally after resuming from suspend? PURPOSE:
    This test will check that the system can switch to a virtual terminal and back to X
STEPS:
    1. Click "Test" to switch to another virtual terminal and then back to X
VERIFICATION:
    Did your screen change temporarily to a text console and then switch back to your current session? PURPOSE:
    This test will check that the system correctly detects
    the removal of a CF card from the systems card reader.
STEPS:
    1. Click "Test" and remove the CF card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a MS card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MS card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a MSP card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MSP card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a SDXC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SDXC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a xD card from the systems card reader.
STEPS:
    1. Click "Test" and remove the xD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of an SD card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of an SDHC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SDHC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of the MMC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MMC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the insertion of
    a USB 3.0 storage device
STEPS:
    1. Click "Test" and insert a USB 3.0 storage device (pen-drive/HDD) in
       a USB 3.0 port. (Note: this test will time-out after 20 seconds.)
    2. Do not unplug the device after the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the insertion of
    a USB storage device
STEPS:
    1. Click "Test" and insert a USB storage device (pen-drive/HDD).
       (Note: this test will time-out after 20 seconds.)
    2. Do not unplug the device after the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a CF card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the CF card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a MS card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MS card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a MSP card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MSP card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a xD card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the xD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of an MMC card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MMC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of an SDHC card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the SDHC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal of
    a USB 3.0 storage device
STEPS:
    1. Click "Test" and remove the USB 3.0 device.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal of
    a USB storage device
STEPS:
    1. Click "Test" and remove the USB device.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that you can record and hear audio using a bluetooth audio device
STEPS:
    1. Enable the bluetooth headset
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. In the device write the PIN code automatically chosen by the wizard
    6. The device should pair with the computer
    7. Click "Test" to record for five seconds and reproduce in the bluetooth device
VERIFICATION:
    Did you hear the sound you recorded in the bluetooth PURPOSE:
    This test will check that you can transfer information through a bluetooth connection
STEPS:
    1. Make sure that you're able to browse the files in your mobile device
    2. Copy a file from the computer to the mobile device
    3. Copy a file from the mobile device to the computer
VERIFICATION:
    Were all files copied correctly? PURPOSE:
    This test will check that you can use a BlueTooth HID device
STEPS:
    1. Enable either a BT mouse or keyboard
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. For mice, perform actions such as moving the pointer, right and left button clicks and double clicks
    6. For keyboards, click the Test button to lauch a small tool. Enter some text into the tool and close it.
VERIFICATION:
    Did the device work as expected? PURPOSE:
    This test will check that you can use a USB HID device
STEPS:
    1. Enable either a USB mouse or keyboard
    2. For mice, perform actions such as moving the pointer, right and left button clicks and double clicks
    3. For keyboards, click the Test button to lauch a small tool. Type some text and close the tool.
VERIFICATION:
    Did the device work as expected? PURPOSE:
    This test will check that your system detects USB storage devices.
STEPS:
    1. Plug in one or more USB keys or hard drives.
    2. Click on "Test".
INFO:
    $output
VERIFICATION:
    Were the drives detected? PURPOSE:
    This test will check the system can detect the insertion of a FireWire HDD
STEPS:
    1. Click 'Test' to begin the test. This test will
       timeout and fail if the insertion has not been detected within 20 seconds.
    2. Plug a FireWire HDD into an available FireWire port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the insertion of an eSATA HDD
STEPS:
    1. Click 'Test' to begin the test. This test will
       timeout and fail if the insertion has not been detected within 20 seconds.
    2. Plug an eSATA HDD into an available eSATA port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the removal of a FireWire HDD
STEPS:
    1. Click 'Test' to begin the test. This test will timeout and fail if
       the removal has not been detected within 20 seconds.
    2. Remove the previously attached FireWire HDD from the FireWire port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the removal of an eSATA HDD
STEPS:
    1. Click 'Test' to begin the test. This test will timeout and fail if
       the removal has not been detected within 20 seconds.
    2. Remove the previously attached eSATA HDD from the eSATA port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check to make sure your system can successfully hibernate (if supported)
STEPS:
    1. Click on Test
    2. The system will hibernate and should wake itself within 5 minutes
    3. If your system does not wake itself after 5 minutes, please press the power button to wake the system manually
    4. If the system fails to resume from hibernate, please restart System Testing and mark this test as Failed
VERIFICATION:
    Did the system successfully hibernate and did it work properly after waking up? PURPOSE:
    This test will check your CD audio playback capabilities
STEPS:
    1. Insert an audio CD in your optical drive
    2. When prompted, launch the Music Player
    3. Locate the CD in the display of the Music Player
    4. Select the CD in the Music Player
    5. Click the Play button to listen to the music on the CD
    6. Stop playing after some time
    7. Right click on the CD icon and select "Eject Disc"
    8. The CD should be ejected
    9. Close the Music Player
VERIFICATION:
    Did all the steps work? PURPOSE:
    This test will check your DVD  playback capabilities
STEPS:
    1. Insert a DVD that contains any movie in your optical drive
    2. Click "Test" to play the DVD in Totem
VERIFICATION:
    Did the file play? PURPOSE:
    This test will check your USB 3.0 connection.
STEPS:
    1. Plug a USB 3.0 HDD or thumbdrive into a USB 3.0 port in the computer.
    2. An icon should appear on the Launcher.
    3. Click "Test" to begin the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check your USB connection.
STEPS:
    1. Connect a USB storage device to an external USB slot on this computer.
    2. An icon should appear on the Launcher.
    3. Confirm that the icon appears.
    4. Eject the device.
    5. Repeat with each external USB slot.
VERIFICATION:
    Do all USB slots work with the device? PURPOSE:
    This test will check your USB connection.
STEPS:
    1. Plug a USB HDD or thumbdrive into the computer.
    2. An icon should appear on the Launcher.
    3. Click "Test" to begin the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check your monitor power saving capabilities
STEPS:
    1. Click "Test" to try the power saving capabilities of your monitor
    2. Press any key or move the mouse to recover
VERIFICATION:
    Did the monitor go blank and turn on again? PURPOSE:
    This test will check your wired connection
STEPS:
    1. Click on the Network icon in the top panel
    2. Select a network below the "Wired network" section
    3. Click "Test" to verify that it's possible to establish a HTTP connection
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will check your wireless connection.
STEPS:
    1. Click on the Network icon in the panel.
    2. Select a network below the 'Wireless networks' section.
    3. Click "Test" to verify that it's possible to establish an HTTP connection.
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will cycle through the detected display modes
STEPS:
    1. Click "Test" and the display will cycle trough the display modes
VERIFICATION:
    Did your display look fine in the detected mode? PURPOSE:
    This test will send the image 'JPEG_Color_Image_Ubuntu.jpg' to a specified device
STEPS:
    1. Click "Test" and you will be prompted to enter the Bluetooth device name of a device that can accept file transfers (It may take a few moments after entering the name for the file to begin sending)
    2. Accept any prompts that appear on both devices
VERIFICATION:
    Was the data correctly transferred? PURPOSE:
    This test will send the image 'JPEG_Color_Image_Ubuntu.jpg' to a specified device
STEPS:
    1. Make sure Bluetooth is enabled by checking the Bluetooth indicator applet
    2. Click "Test" and you will be prompted to enter the Bluetooth device name of a device that can accept file transfers (It may take a few moments after entering the name for the file to begin sending)
    3. Accept any prompts that appear on both devices
VERIFICATION:
    Was the data correctly transferred? PURPOSE:
    This test will test display rotation
STEPS:
    1. Click "Test" to test display rotation. The display will be rotated every 4 seconds.
    2. Check if all rotations (normal right inverted left) took place without permanent screen corruption
VERIFICATION:
    Did the display rotation take place without without permanent screen corruption? PURPOSE:
    This test will test the brightness key
STEPS:
    1. Press the brightness buttons on the keyboard
VERIFICATION:
    Did the brightness change following to your key presses? PURPOSE:
    This test will test the brightness key after resuming from suspend
STEPS:
    1. Press the brightness buttons on the keyboard
VERIFICATION:
    Did the brightness change following to your key presses after resuming from suspend? PURPOSE:
    This test will test the default display
STEPS:
    1. Click "Test" to display a video test.
VERIFICATION:
    Do you see color bars and static? PURPOSE:
    This test will test the mute key of your keyboard
STEPS:
    1. Click test to open a window on which to test the mute key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Does the mute key work as expected? PURPOSE:
    This test will test the mute key of your keyboard after resuming from suspend
STEPS:
    1. Click test to open a window on which to test the mute key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Did the volume mute following your key presses? PURPOSE:
    This test will test the sleep key
STEPS:
    1. Press the sleep key on the keyboard
    2. Wake your system up by pressing the power button
VERIFICATION:
    Did the system go to sleep after pressing the sleep key? PURPOSE:
    This test will test the sleep key after resuming from suspend
STEPS:
    1. Press the sleep key on the keyboard
    2. Wake your system up by pressing the power button
VERIFICATION:
    Did the system go to sleep after pressing the sleep key after resuming from suspend? PURPOSE:
    This test will test the super key of your keyboard
STEPS:
    1. Click test to open a window on which to test the super key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Does the super key work as expected? PURPOSE:
    This test will test the super key of your keyboard after resuming from suspend
STEPS:
    1. Click test to open a window on which to test the super key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Does the super key work as expected after resuming from suspend? PURPOSE:
    This test will test the wireless key after resuming from suspend
STEPS:
    1. Press the wireless key on the keyboard
    2. Press the same key again
VERIFICATION:
    Did the wireless go off on the first press and on again on the second after resuming from suspend? PURPOSE:
    This test will test your keyboard
STEPS:
    1. Click on Test
    2. On the open text area, use your keyboard to type something
VERIFICATION:
    Is your keyboard working properly? PURPOSE:
    This test will test your pointing device
STEPS:
    1. Move the cursor using the pointing device or touch the screen.
    2. Perform some single/double/right click operations.
VERIFICATION:
    Did the pointing device work as expected? PURPOSE:
    This test will verify that the GUI is usable after manually changing resolution
STEPS:
    1. Open the Displays application
    2. Select a new resolution from the dropdown list
    3. Click on Apply
    4. Select the original resolution from the dropdown list
    5. Click on Apply
VERIFICATION:
    Did the resolution change as expected? PURPOSE:
    This test will verify the default display resolution
STEPS:
    1. This display is using the following resolution:
INFO:
    $output
VERIFICATION:
    Is this acceptable for your display? PURPOSE:
    To make sure that stressing the wifi hotkey does not cause applets to disappear from the panel or the system to lock up
STEPS:
    1. Log in to desktop
    2. Press wifi hotkey at a rate of 1 press per second and slowly increase the speed of the tap, until you are tapping as fast as possible
VERIFICATION:
    Verify the system is not frozen and the wifi and bluetooth applets are still visible and functional PURPOSE:
    Touchpad LED verification
STEPS:
    1. Click on the touchpad button or press key combination to enable/disable touchpad button
    2. Slide your finger on the touchpad
VERIFICATION:
    1. Touchpad LED status should toggle everytime the button is clicked or the key combination is pressed
    2. When the LED is on, the mouse pointer should move on touchpad usage
    3. When the LED is off, the mouse pointer should not move on touchpad usage PURPOSE:
    Touchpad LED verification after resuming from suspend
STEPS:
    1. Click on the touchpad button or press key combination to enable/disable touchpad button
    2. Slide your finger on the touchpad
VERIFICATION:
    1. Touchpad LED status should toggle everytime the button is clicked or the key combination is pressed
    2. When the LED is on, the mouse pointer should move on touchpad usage
    3. When the LED is off, the mouse pointer should not move on touchpad usage PURPOSE:
    Touchpad horizontal scroll verification
STEPS:
    1. Select "Test" when ready and place your cursor within the borders of the displayed test window.
    2. Verify that you can move the horizontal slider by moving your finger right and left in the lower part of the touchpad.
VERIFICATION:
    Could you scroll right and left? PURPOSE:
    Touchpad manual detection of multitouch.
STEPS:
    1. Look at the specifications for your system.
VERIFICATION:
    Is the touchpad supposed to be multitouch? PURPOSE:
    Touchpad user-verify
STEPS:
    1. Make sure that touchpad is enabled.
    2. Move cursor using the touchpad.
VERIFICATION:
    Did the cursor move? PURPOSE:
    Touchpad vertical scroll verification
STEPS:
    1. Select "Test" when ready and place your cursor within the borders of the displayed test window.
    2. Verify that you can move the vertical slider by moving your finger up and down in the right part of the touchpad.
VERIFICATION:
    Could you scroll up and down? PURPOSE:
    Touchscreen manual detection of multitouch.
STEPS:
    1. Look at the specifications for your system.
VERIFICATION:
    Is the screen supposed to be multitouch? PURPOSE:
    Validate Wireless (WLAN + Bluetooth) LED operated the same after resuming from suspend
STEPS:
    1. Make sure WLAN connection is established and Bluetooth is enabled.
    2. WLAN/Bluetooth LED should light
    3. Switch WLAN and Bluetooth off from a hardware switch (if present)
    4. Switch them back on
    5. Switch WLAN and Bluetooth off from the panel applet
    6. Switch them back on
VERIFICATION:
    Did the WLAN/Bluetooth LED light as expected after resuming from suspend? PURPOSE:
    Validate that WLAN LED shuts off when disabled after resuming from suspend
STEPS:
    1. Connect to AP
    2. Use Physical switch to disable WLAN
    3. Re-enable
    4. Use Network-Manager to disable WLAN
VERIFICATION:
    Did the LED turn off then WLAN is disabled after resuming from suspend? PURPOSE:
    Validate that the Bluetooth LED turns on and off when BT is enabled/disabled
STEPS:
    1. Switch bluetooth off from a hardware switch (if present)
    2. Switch bluetooth back on
    3. Switch bluetooth off from the panel applet
    4. Switch bluetooth back on
VERIFICATION:
    Did the bluetooth LED turn off and on twice? PURPOSE:
    Validate that the Bluetooth LED turns on and off when BT is enabled/disabled after resuming from suspend
STEPS:
    1. Switch bluetooth off from a hardware switch (if present)
    2. Switch bluetooth back on
    3. Switch bluetooth off from the panel applet
    4. Switch bluetooth back on
VERIFICATION:
    Did the bluetooth LED turn off and on twice after resuming from suspend? PURPOSE:
    Validate that the Caps Lock key operates the same before and after resuming from suspend
STEPS:
    1. Press "Block Cap Keys" to activate/deactivate cap keys blocking
    2. Cap Keys LED should be switched on/off every time the key is pressed
VERIFICATION:
    Did the Cap Keys LED light as expected after resuming from suspend? PURPOSE:
    Validate that the External Video hot key is working as expected
STEPS:
    1. Plug in an external monitor
    2. Press the display hot key to change the monitors configuration
VERIFICATION:
    Check that the video signal can be mirrored, extended, displayed on external or onboard only. PURPOSE:
    Validate that the External Video hot key is working as expected after resuming from suspend
STEPS:
    1. Plug in an external monitor
    2. Press the display hot key to change the monitors configuration
VERIFICATION:
    Check that the video signal can be mirrored, extended, displayed on external or onboard only, after resuming from suspend. PURPOSE:
    Validate that the HDD LED still operates as expected after resuming from suspend
STEPS:
    1. Select "Test" to write and read a temporary file for a few seconds
    2. HDD LED should blink when writing to/reading from HDD
VERIFICATION:
    Did the HDD LED still blink with HDD activity after resuming from suspend? PURPOSE:
    Validate that the battery LED indicated low power
STEPS:
    1. Let system run on battery for several hours
    2. Monitor battery LED carefully
VERIFICATION:
    Does the LED light orange when battery is low? PURPOSE:
    Validate that the battery LED indicated low power after resuming from suspend
STEPS:
    1. Let system run on battery for several hours
    2. Monitor battery LED carefully
VERIFICATION:
    Does the LED light orange when battery is low after resuming from suspend? PURPOSE:
    Validate that the battery LED properly displays charged status
STEPS:
    1. Let system run on battery for a short time
    2. Plug in AC
    3. Let system run on AC
VERIFICATION:
    Does the orange battery LED shut off when system is fully charged? PURPOSE:
    Validate that the battery LED properly displays charged status after resuming from suspend
STEPS:
    1. Let system run on battery for a short time
    2. Plug in AC
    3. Let system run on AC
VERIFICATION:
    Does the orange battery LED still shut off when system is fully charged after resuming from suspend? PURPOSE:
    Validate that the battery light shows charging status
STEPS:
    1. Let system run on battery for a while
    2. Plug in AC plug
VERIFICATION:
    Did the battery indicator LED turn orange? PURPOSE:
    Validate that the battery light shows charging status after resuming from suspend
STEPS:
    1. Let system run on battery for a while
    2. Plug in AC plug
VERIFICATION:
    Did the battery indicator LED still turn orange after resuming from suspend? PURPOSE:
    Validate that the camera LED still works as expected after resuming from suspend
STEPS:
    1. Select Test to activate camera
    2. Camera LED should light for a few seconds
VERIFICATION:
    Did the camera LED still turn on and off after resuming from suspend? PURPOSE:
    Validate that the numeric keypad LED operates the same before and after resuming from suspend
STEPS:
    1. Press "Block Num" key to toggle numeric keypad LED
    2. Click on the "Test" button to open a window to verify your typing
    3. Type using the numeric keypad both when the LED is on and off
VERIFICATION:
    1. Numeric keypad LED status should toggle everytime the "Block Num" key is pressed
    2. Numbers should only be entered in the keyboard verification window when the LED is on PURPOSE:
    Validate that the power LED operated the same after resuming from suspend
STEPS:
    1. Power LED should be on while device is switched on
VERIFICATION:
    Does the power LED remain on after resuming from suspend? PURPOSE:
    Verify touchpad hotkey toggles touchpad functionality on and off
STEPS:
    1. Verify the touchpad is functional
    2. Tap the touchpad toggle hotkey
    3. Tap the touchpad toggle hotkey again
VERIFICATION:
    Verify the touchpad has been disabled and re-enabled. PURPOSE:
    Verify touchpad hotkey toggles touchpad functionality on and off after resuming from suspend
STEPS:
    1. Verify the touchpad is functional
    2. Tap the touchpad toggle hotkey
    3. Tap the touchpad toggle hotkey again
VERIFICATION:
    Verify the touchpad has been disabled and re-enabled. PURPOSE:
    Wake up by USB keyboard
STEPS:
    1. Enable "Wake by USB KB/Mouse" item in BIOS
    2. Press "Test" to enter suspend (S3) mode
    3. Press any key of USB keyboard to wake system up
VERIFICATION:
    Did the system wake up from suspend mode when you pressed a keyboard key? PURPOSE:
    Wake up by USB mouse
STEPS:
    1. Enable "Wake by USB KB/Mouse" item in BIOS
    2. Press "Test" to enter suspend (S3) mode
    3. Press any button of USB mouse to wake system up
VERIFICATION:
    Did the system wake up from suspend mode when you pressed the mouse button? PURPOSE:
    Wireless (WLAN + Bluetooth) LED verification
STEPS:
    1. Make sure WLAN connection is established and Bluetooth is enabled.
    2. WLAN/Bluetooth LED should light
    3. Switch WLAN and Bluetooth off from a hardware switch (if present)
    4. Switch them back on
    5. Switch WLAN and Bluetooth off from the panel applet
    6. Switch them back on
VERIFICATION:
    Did the WLAN/Bluetooth LED light as expected? PURPOSE:
   This is an automated stress test that will force the system to hibernate/resume for 250 cycles PURPOSE:
   This is an automated stress test that will force the system to hibernate/resume for 30 cycles PURPOSE:
   This is an automated stress test that will force the system to suspend/resume for 250 cycles. PURPOSE:
   This is an automated stress test that will force the system to suspend/resume for 30 cycles. PURPOSE:
   This test will verify that a USB DSL or Mobile Broadband modem works
STEPS:
   1. Connect the USB cable to the computer
   2. Right click on the Network icon in the panel
   3. Select 'Edit Connections'
   4. Select the 'DSL' (for ADSL modem) or 'Mobile Broadband' (for 3G modem) tab
   5. Click on 'Add' button
   6. Configure the connection parameters properly
   7. Notify OSD should confirm that the connection has been established
   8. Select Test to verify that it's possible to establish an HTTP connection
VERIFICATION:
   Was the connection correctly established? PURPOSE:
   This test will verify that a fingerprint reader can be used to unlock a locked system.
STEPS:
   1. Click on the Session indicator (Cog icon on the Left side of the panel) .
   2. Select 'Lock screen'.
   3. Press any key or move the mouse.
   4. A window should appear that provides the ability to unlock either typing your password or using fingerprint authentication.
   5. Use the fingerprint reader to unlock.
   6. Your screen should be unlocked.
VERIFICATION:
   Did the authentication procedure work correctly? PURPOSE:
   This test will verify that a network printer is usable
STEPS:
   1. Make sure that a printer is available in your network
   2. Click on the Gear icon in the upper right corner and then click on Printers
   3. If the printer isn't already listed, click on Add
   4. The printer should be detected and proper configuration values  should be displayed
   5. Print a test page
VERIFICATION:
   Were you able to print a test page to the network printer? PURPOSE:
   This test will verify that the desktop clock displays the correct date and time
STEPS:
   1. Check the clock in the upper right corner of your desktop.
VERIFICATION:
   Is the clock displaying the correct date and time for your timezone? PURPOSE:
   This test will verify that the desktop clock synchronizes with the system clock.
STEPS:
   1. Click the "Test" button and verify the clock moves ahead by 1 hour.
   Note: It may take a minute or so for the clock to refresh
   2. Right click on the clock, then click on "Time & Date Settings..."
   3. Ensure that your clock application is set to manual.
   4. Change the time 1 hour back
   5. Close the window and reboot
VERIFICATION:
   Is your system clock displaying the correct date and time for your timezone? PURPOSE:
   This test will verify that you can reboot your system from the desktop menu
STEPS:
   1. Click the Gear icon in the upper right corner of the desktop and click on "Shut Down"
   2. Click the "Restart" button on the left side of the Shut Down dialog
   3. After logging back in, restart System Testing and it should resume here
VERIFICATION:
   Did your system restart and bring up the GUI login cleanly? PURPOSE:
   This test will verify your system's ability to play Ogg Vorbis audio files.
STEPS:
   1. Click Test to play an Ogg Vorbis file (.ogg)
   2. Please close the player to proceed.
VERIFICATION:
   Did the sample play correctly? PURPOSE:
   This test will verify your system's ability to play Wave Audio files.
STEPS:
   1. Select Test to play a Wave Audio format file (.wav)
   2. Please close the player to proceed.
VERIFICATION:
   Did the sample play correctly? PURPOSE:
 If Recovery is successful, you will see this test on restarting checkbox, not
 sniff4.
STEPS:
 1. Click Yes
VERIFICATION:
 None Necessary, this is a bogus test PURPOSE:
 Simulates a failure by rebooting the machine
STEPS:
 1. Click test to trigger a reboot
 2. Select "Continue" once logged back in and checkbox is restarted
VERIFICATION:
 You won't see the user-verify PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
  1. Cut
  2. Copy
  3. Paste
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
  1. Memory set
  2. Memory reset
  3. Memory last clear
  4. Memory clear
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
 1. Simple math functions (+,-,/,*)
 2. Nested math functions ((,))
 3. Fractional math
 4. Decimal math
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator.
VERIFICATION:
 Did it launch correctly? PURPOSE:
 This test checks that gedit works.
STEPS:
 1. Click the "Test" button to open gedit, and re-open the file you created previously.
 2. Edit then save the file, then close gedit.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test checks that gedit works.
STEPS:
 1. Click the "Test" button to open gedit.
 2. Enter some text and save the file (make a note of the file name you use), then close gedit.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the AOL Instant Messaging (AIM) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Facebook Chat service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Google Talk (gtalk) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Jabber service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Microsoft Network (MSN) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a IMAP account.
VERIFICATION:
 Were you able to receive and read e-mail correctly? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a POP3 account.
VERIFICATION:
 Were you able to receive and read e-mail correctly? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a SMTP account.
VERIFICATION:
 Were you able to send e-mail without errors? PURPOSE:
 This test will check that Firefox can play a Flash video. Note: this may
 require installing additional software to successfully complete.
STEPS:
 1. Select Test to launch Firefox and view a short flash video.
VERIFICATION:
 Did the video play correctly? PURPOSE:
 This test will check that Firefox can play a Quicktime (.mov) video file.
 Note: this may require installing additional software to successfully
 complete.
STEPS:
 1. Select Test to launch Firefox with a sample video.
VERIFICATION:
 Did the video play using a plugin? PURPOSE:
 This test will check that Firefox can render a basic web page.
STEPS:
 1. Select Test to launch Firefox and view the test web page.
VERIFICATION:
 Did the Ubuntu Test page load correctly? PURPOSE:
 This test will check that Firefox can run a java applet in a web page. Note:
 this may require installing additional software to complete successfully.
STEPS:
 1. Select Test to open Firefox with the Java test page, and follow the instructions there.
VERIFICATION:
 Did the applet display? PURPOSE:
 This test will check that Firefox can run flash applications. Note: this may
 require installing additional software to successfully complete.
STEPS:
 1. Select Test to launch Firefox and view a sample Flash test.
VERIFICATION:
 Did you see the text? PURPOSE:
 This test will check that Gnome Terminal works.
STEPS:
 1. Click the "Test" button to open Terminal.
 2. Type 'ls' and press enter. You should see a list of files and folder in your home directory.
 3. Close the terminal window.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test will check that the file browser can copy a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the file called Test File 1 and click Copy.
 3. Right click in the white space and click Paste.
 4. Right click on the file called Test File 1(copy) and click Rename.
 5. Enter the name Test File 2 in the name box and hit Enter.
 6. Close the File Browser.
VERIFICATION:
 Do you now have a file called Test File 2? PURPOSE:
 This test will check that the file browser can copy a folder
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the folder called Test Folder and click on Copy.
 3. Right Click on any white area in the window and click on Paste.
 4. Right click on the folder called Test Folder(copy) and click Rename.
 5. Enter the name Test Data in the name box and hit Enter.
 6. Close the File browser.
VERIFICATION:
 Do you now have a folder called Test Data? PURPOSE:
 This test will check that the file browser can create a new file.
STEPS:
 1. Click Select Test to open the File Browser.
 2. Right click in the white space and click Create Document -> Empty Document.
 3. Enter the name Test File 1 in the name box and hit Enter.
 4. Close the File browser.
VERIFICATION:
 Do you now have a file called Test File 1? PURPOSE:
 This test will check that the file browser can create a new folder.
STEPS:
 1. Click Test to open the File Browser.
 2. On the menu bar, click File -> Create Folder.
 3. In the name box for the new folder, enter the name Test Folder and hit Enter.
 4. Close the File browser.
VERIFICATION:
 Do you now have a new folder called Test Folder? PURPOSE:
 This test will check that the file browser can delete a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the file called Test File 1 and click on Move To Trash.
 3. Verify that Test File 1 has been removed.
 4. Close the File Browser.
VERIFICATION:
  Is Test File 1 now gone? PURPOSE:
 This test will check that the file browser can delete a folder.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the folder called Test Folder and click on Move To Trash.
 3. Verify that the folder was deleted.
 4. Close the file browser.
VERIFICATION:
 Has Test Folder been successfully deleted? PURPOSE:
 This test will check that the file browser can move a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Click and drag the file called Test File 2 onto the icon for the folder called Test Data.
 3. Release the button.
 4. Double click the icon for Test Data to open that folder up.
 5. Close the File Browser.
VERIFICATION:
 Was the file Test File 2 successfully moved into the Test Data folder? PURPOSE:
 This test will check that the update manager can find updates.
STEPS:
 1. Click Test to launch update-manager.
 2. Follow the prompts and if updates are found, install them.
 3. When Update Manager has finished, please close the app by clicking the Close button in the lower right corner.
VERIFICATION:
 Did Update manager find and install updates (Pass if no updates are found,
 but Fail if updates are found but not installed) PURPOSE:
 This test will verify that the file browser can move a folder.
STEPS:
 1. Click Test to open the File Browser.
 2. Click and drag the folder called Test Data onto the icon called Test Folder.
 3. Release the button.
 4. Double click the folder called Test Folder to open it up.
 5. Close the File Browser.
VERIFICATION:
 Was the folder called Test Data successfully moved into the folder called Test Folder? PURPOSE:
 To sniff things out
STEPS:
 1. Click Yes
VERIFICATION:
 None Necessary, this is a bogus test Panel Clock Verification tests Panel Reboot Verification tests Parses Xorg.0.Log and discovers the running X driver and version Peripheral tests Piglit tests Ping ubuntu.com and restart network interfaces 100 times Play back a sound on the default output and listen for it on the  default input. Please choose (%s):  Please press each key on your keyboard. Please type here and press Ctrl-D when finished:
 Pointing device tests. Power Management Test Power Management tests Press any key to continue... Previous Print version information and exit. Provides information about displays attached to the system Provides information about network devices Quit from keyboard Record mixer settings before suspending. Record the current network before suspending. Record the current resolution before suspending. Rendercheck tests Restart Returns the name, driver name and driver version of any touchpad discovered on the system. Run Cachebench Read / Modify / Write benchmark Run Cachebench Read benchmark Run Cachebench Write benchmark Run Compress 7ZIP benchmark Run Compress PBZIP2 benchmark Run Encode MP3 benchmark Run Firmware Test Suite (fwts) automated tests. Run GLmark2 benchmark Run GLmark2-ES2 benchmark Run GnuPG benchmark Run Himeno benchmark Run Lightsmark benchmark Run N-Queens benchmark Run Network Loopback benchmark Run Qgears2 OpenGL gearsfancy benchmark Run Qgears2 OpenGL image scaling benchmark Run Qgears2 XRender Extension gearsfancy benchmark Run Qgears2 XRender Extension image scaling benchmark Run Render-Bench XRender/Imlib2 benchmark Run Stream Add benchmark Run Stream Copy benchmark Run Stream Scale benchmark Run Stream Triad benchmark Run Unigine Heaven benchmark Run Unigine Santuary benchmark Run Unigine Tropics benchmark Run a tessellation test based on TessMark (OpenGL 4.0) Fullscreen 1920x1080 no antialiasing Run globs benchmark Run gtkperf to make sure that GTK based test cases work Run the graphics stress test. This test can take a few minutes. Run x264 H.264/AVC encoder benchmark Running %s... Runs a test that transfers 100 10MB files 3 times to a SDHC card. Runs a test that transfers 100 10MB files 3 times to usb. Runs all of the rendercheck test suites. This test can take a few minutes. Runs piglit tests for checking OpenGL 2.1 support Runs piglit tests for checking support for GLSL fragment shader operations Runs piglit tests for checking support for GLSL vertex shader operations Runs piglit tests for checking support for framebuffer object operations, depth buffer and stencil buffer Runs piglit tests for checking support for texture from pixmap Runs piglit tests for checking support for vertex buffer object operations Runs piglit_tests for checking support for stencil buffer operations Runs the piglit results summarizing tool SATA/IDE device information. SMART test Select All Select all Server Services checks Shorthand for --config=.*/jobs_info/blacklist. Shorthand for --config=.*/jobs_info/blacklist_file. Shorthand for --config=.*/jobs_info/whitelist. Shorthand for --config=.*/jobs_info/whitelist_file. Skip Smoke tests Sniff Sniffers Software Installation tests Some new hard drives include a feature that parks the drive heads after a short period of inactivity. This is a power-saving feature, but it can have a bad interaction with the operating system that results in the drive constantly parked then activated. This produces excess wear on the drive, potentially leading to early failures. Space when finished Start testing Status Stop process Stop typed at tty Stress poweroff system (100 cycles) Stress reboot system (100 cycles) Stress tests Submission details Submit results Submit to HEXR Successfully finished testing! Suspend Test Suspend tests System 
Testing System Daemon tests System Testing Tab 1 Tab 2 Termination signal Test Test ACPI Wakealarm (fwts wakealarm) Test Again Test and exercise memory. Test cancelled Test for clock jitter. Test if the atd daemon is running when the package is installed. Test if the cron daemon is running when the package is installed. Test if the cupsd daemon is running when the package is installed. Test if the getty daemon is running when the package is installed. Test if the init daemon is running when the package is installed. Test if the klogd daemon is running when the package is installed. Test if the nmbd daemon is running when the package is installed. Test if the smbd daemon is running when the package is installed. Test if the syslogd daemon is running when the package is installed. Test if the udevd daemon is running when the package is installed. Test if the winbindd daemon is running when the package is installed. Test interrupted Test offlining CPUs in a multicore system. Test that the /var/crash directory doesn't contain anything. Lists the files contained within if it does, or echoes the status of the directory (doesn't exist/is empty) Test that the X is not running in failsafe mode. Test that the X process is running. Test the CPU scaling capabilities using Firmware Test Suite (fwts cpufreq). Test the network after resuming. Test to check that a cloud image boots and works properly with KVM Test to check that virtualization is supported and the test system has at least a minimal amount of RAM to function as an OpenStack Compute Node Test to detect audio devices Test to detect the available network controllers Test to detect the optical drives Test to determine if this system is capable of running hardware accelerated KVM virtual machines Test to output the Xorg version Test to see if we can sync local clock to an NTP server Test to see that we have the same resolution after resuming as before. Test to verify that the Xen Hypervisor is running. Tested Tests oem-config using Xpresser, and then checks that the user has been created successfully. Cleans up the newly created user after the test has passed. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11b/g protocols after the system has been suspended. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11b/g protocols. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11n protocol after the system has been suspended. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11n protocol. Tests that the systems wireless hardware can connect to a router using no security and the 802.11b/g protocols after the system has been suspended. Tests that the systems wireless hardware can connect to a router using no security and the 802.11b/g protocols. Tests that the systems wireless hardware can connect to a router using no security and the 802.11n protocol after the system has been suspended. Tests that the systems wireless hardware can connect to a router using no security and the 802.11n protocol. Tests the performance of a systems wireless connection through the iperf tool, using UDP packets. Tests the performance of a systems wireless connection through the iperf tool. Tests to see that apt can access repositories and get updates (does not install updates). This is done to confirm that you could recover from an incomplete or broken update. Tests whether the system has a working Internet connection. TextLabel The file to write the log to. The following report has been generated for submission to the Launchpad hardware database:

  [[%s|View Report]]

You can submit this information about your system by providing the email address you use to sign in to Launchpad. If you do not have a Launchpad account, please register here:

  https://launchpad.net/+login The generated report seems to have validation errors,
so it might not be processed by Launchpad. There is another checkbox running. Please close it first. This Automated test attempts to detect a camera. This attaches screenshots from the suspend/cycle_resolutions_after_suspend_auto test to the results submission. This is a fully automated version of mediacard/sd-automated and assumes that the system under test has a memory card device plugged in prior to checkbox execution. It is intended for SRU automated testing. This is an automated Bluetooth file transfer test. It sends an image to the device specified by the BTDEVADDR environment variable. This is an automated Bluetooth test. It emulates browsing on a remote device specified by the BTDEVADDR environment variable. This is an automated Bluetooth test. It receives the given file from a remote host specified by the BTDEVADDR environment variable This is an automated test to gather some info on the current state of your network devices. If no devices are found, the test will exit with an error. This is an automated test which performs read/write operations on an attached FireWire HDD This is an automated test which performs read/write operations on an attached eSATA HDD This is an automated version of usb/storage-automated and assumes that the server has usb storage devices plugged in prior to checkbox execution. It is intended for servers and SRU automated testing. This is an automated version of usb3/storage-automated and assumes that the server has usb 3.0 storage devices plugged in prior to checkbox execution. It is intended for servers and SRU automated testing. This is the automated version of suspend/suspend_advanced. This test checks cpu topology for accuracy This test checks that CPU frequency governors are obeyed when set. This test checks that the wireless interface is working after suspending the system. It disconnects all interfaces and then connects to the wireless interface and checks that the connection is working as expected. This test checks the amount of memory which is reporting in meminfo against the size of the memory modules detected by DMI. This test disconnects all connections and then connects to the wireless interface. It then checks the connection to confirm it's working as expected. This test grabs the hardware address of the bluetooth adapter after suspend and compares it to the address grabbed before suspend. This test is automated and executes after the mediacard/cf-insert test is run. It tests reading and writing to the CF card. This test is automated and executes after the mediacard/cf-insert-after-suspend test is run. It tests reading and writing to the CF card after the system has been suspended. This test is automated and executes after the mediacard/mmc-insert test is run. It tests reading and writing to the MMC card. This test is automated and executes after the mediacard/mmc-insert-after-suspend test is run. It tests reading and writing to the MMC card after the system has been suspended. This test is automated and executes after the mediacard/ms-insert test is run. It tests reading and writing to the MS card. This test is automated and executes after the mediacard/ms-insert-after-suspend test is run. It tests reading and writing to the MS card after the system has been suspended. This test is automated and executes after the mediacard/msp-insert test is run. It tests reading and writing to the MSP card. This test is automated and executes after the mediacard/msp-insert-after-suspend test is run. It tests reading and writing to the MSP card after the system has been suspended. This test is automated and executes after the mediacard/sd-insert test is run. It tests reading and writing to the SD card. This test is automated and executes after the mediacard/sd-insert-after-suspend test is run. It tests reading and writing to the SD card after the system has been suspended. This test is automated and executes after the mediacard/sdhc-insert test is run. It tests reading and writing to the SDHC card. This test is automated and executes after the mediacard/sdhc-insert-after-suspend test is run. It tests reading and writing to the SDHC card after the system has been suspended. This test is automated and executes after the mediacard/sdxc-insert test is run. It tests reading and writing to the SDXC card. This test is automated and executes after the mediacard/sdxc-insert-after-suspend test is run. It tests reading and writing to the SDXC card after the system has been suspended. This test is automated and executes after the mediacard/xd-insert test is run. It tests reading and writing to the xD card. This test is automated and executes after the mediacard/xd-insert-after-suspend test is run. It tests reading and writing to the xD card after the system has been suspended. This test is automated and executes after the usb/insert test is run. This test is automated and executes after the usb3/insert test is run. This test will check to make sure supported video modes work after a suspend and resume. This is done automatically by taking screenshots and uploading them as an attachment. This test will verify that the volume levels are at an acceptable level on your local system.  The test will validate that the volume is greater than or equal to minvol and less than or equal to maxvol for all sources (inputs) and sinks (outputs) recognized by PulseAudio.  It will also validate that the active source and sink are not muted.  You should not manually adjust the  volume or mute before running this test. This will attach any logs from the power-management/poweroff test to the results. This will attach any logs from the power-management/reboot test to the results. This will check to make sure that your audio device works properly after a suspend and resume.  This may work fine with speakers and onboard microphone, however, it works best if used with a cable connecting the audio-out jack to the audio-in jack. This will run some basic connectivity tests against a BMC, verifying that IPMI works. Timer signal from alarm(2) Touchpad tests Touchscreen tests Try to enable a remote printer on the network and print a test page. Type Text UNKNOWN USB Test USB tests Unknown signal Untested Usage: checkbox [OPTIONS] User Applications User-defined signal 1 User-defined signal 2 Validate that the Vector Floating Point Unit is running on ARM device Verifies that DNS server is running and working. Verifies that Print/CUPs server is running. Verifies that Samba server is running. Verifies that Tomcat server is running and working. Verifies that sshd is running. Verifies that the LAMP stack is running (Apache, MySQL and PHP). Verify USB3 external storage performs at or above baseline performance Verify system storage performs at or above baseline performance Verify that all CPUs are online after resuming. Verify that all memory is available after resuming from suspend. Verify that all the CPUs are online before suspending Verify that an installation of checkbox-server on the network can be reached over SSH. Verify that mixer settings after suspend are the same as before suspend. Verify that storage devices, such as Fibre Channel and RAID can be detected and perform under stress. View results Virtualization tests Welcome to System Testing!

Checkbox provides tests to confirm that your system is working properly. Once you are finished running the tests, you can view a summary report for your system. Wireless Test Wireless networking tests Wireless scanning test. It scans and reports on discovered APs. Working You can also close me by pressing ESC or Ctrl+C. _Deselect All _Exit _Finish _No _Previous _Select All _Skip this test _Test _Test Again _Yes and capacity as well. attaches the contents of various sysctl config files. eSATA disk tests empty and capacity as well. https://help.ubuntu.com/community/Installation/SystemRequirements installs the installer bootchart tarball if it exists. no skip test test again tty input for background process tty output for background process until empty and capacity as well.  Requires MOVIE_VAR to be set. yes Project-Id-Version: checkbox
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-01-27 17:46+0000
PO-Revision-Date: 2014-04-07 16:38+0000
Last-Translator: Hriostat <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:35+0000
X-Generator: Launchpad (build 18115)
 

Aviso: Alguns testes podem fazer seu sistema congelar ou parar de responder. Favor salvar todo o seu trabalho e fechar todos os outros aplicativos em execução antes de começar o processo de teste.    Determina se é necessário executar testes específicos para computadores portáteis que não se aplicam a computadores de mesa.  Resultados   Executar   Seleção   Tira várias fotos baseadas nas resoluções suportadas pela câmera e
 valida o seus tamanhos e que elas são de um formato válido.  Testa se o hardware wireless do sistema pode conectar a um roteador usando o
 protocolo 802.11a.  Isso requer que você tenha um roteador pré-configurado para apenas
 responder às requisições pelo protocolo 802.11a.  Testa se o hardware wireless do sistema pode conectar a um roteador usando o
 protocolo 802.11b.  Isso requer que você tenha um roteador pré-configurado para apenas
 responder às requisições pelo protocolo 802.11b.  Testa se o hardware wireless do sistema pode conectar a um roteador usando o
 protocolo 802.11g.  Isso requer que você tenha um roteador pré-configurado para apenas
 responder às requisições pelo protocolo 802.11g. A tecla %(key_name)s foi pressionada &Não &Anterior &Ignorar este teste &Testar &Sim 10 testes foram concluídos do total de 30 (30%) Abortar sinal de abortar(3) Todas as teclas necessárias foram testadas! Arquiva o diretório de sumário piglit em piglit-results.tar.gz. Você tem certeza? Anexa o relatório do teste do despertador fwts Anexar registro dos testes de verificação de renderização Anexar uma cópia de /var/log/dmesg para testar resultados Anexar um dump do banco de dados udev, mostrando informações de hardware do sistema. Anexar uma lista dos módulos do kernel executados atualmente. Anexar um relatório com informações da CPU Anexar um relatório com os codecs instalados para Intel HDA Anexar um relatório de atributos sysfs. Anexar um tarball de dados gcov, se estiverem presentes. Anexar uma saída dmidecode Anexar informações em uma DMI Anexa informações sobre partições de disco Anexa a saída do lshw Anexa os resultados do  FWTS para a submissão Anexa o registro de coleções dos dados de hardware de áudio aos resultados. Anexar o registro do bootchart para execução de testes bootchart. Anexar o arquivo png do bootchart para execução de análises e visualizações Anexar o conteúdo de /proc/acpi/sleep se este existir. Anexar o conteúdo do arquivo /etc/modules. Anexar o conteúdo de vários arquivos de configuração modprobe. Anexa a versão do firmware Anexa os resutados do teste de estresse gráfico à submissão. Anexar o registro de depuração do instalador, se este existir. Anexa o relatório do teste de Hibernar/Retornar por 250 ciclos se ele existir Anexa o relatório do teste de Suspender/Retornar por 250 ciclos se ele existir Anexa o relatório do teste de Hibernar/Retornar por 30 ciclos se ele existir Anexa o relatório do teste de Suspender/retornar por 30 ciclos se ele existir Anexa o relatório do teste de suspensão/retorno único aos resultados Anexa o registro gerado por cpu/teste_de_escalonamento aos resultados Conecte a saída do udev_resource, para fins de depuração Anexa a captura de tela efetuada em graphics/screenshot. Anexa uma captura de tela efetuada em graphics/screenshot_fullscreen_video. Anexa detalhadamente a saída lspci (com a database central do Query) Anexar uma saída lspci muito detalhada. Teste de áudio Testes de áudio Teste automatizado de gravação de CD. Teste automatizado de gravação de DVD. Verificação automática do log de suspensão para procurar erros reportados pelo fwts Teste autatizado de unidade ótica. Teste automatizado para ter certeza se é possivel baixar arquivos através do protocolo HTTP Teste automatizado para verificar a disponibilidade de algum sistema na rede usando pacotes ICMP ECHO. Teste automatizado para armazenar as informações do dispositivo bluetooth em um relatório do Checkbox Teste automatizado para percorrer múltiplas placas de rede e testar cada uma em sequência. Avaliação de desempenho para cada disco Testes de avaliação de desempenho Teste do Bluetooth Testes de bluetooth Informação do Bootchart. Conexão interrompida:  escrita em pipe sem leitores Construindo relatório... Teste de gravação e CD. Teste de processador Testes de CPU Uso do processador em um sistema ocioso. Teste de câmera Testes da câmera Verifica o resultado da falha do caso de teste do shell Verifica se o trabalho é executado quando a dependência é satisfeita Verifica se o trabalho é executado quando há o cumprimento dos requisitos Verifica se o resultado de trabalho é definido como "não necessária neste sistema" quando os requisitos não são cumpridos Verifica se o resultado do trabalho está definido para não inicializado quando a dependência não está satisfeita Verificar relatórios para o caso de teste de estresse de desligamento  (100 ciclos) Verificar relatórios para o caso de teste de estresse de reinicialização (100 ciclos) Verificar mudança nas estatísticas para cada disco Verifica o resultado de sucesso do teste de shell case Verifique se os drivers VESA não estão em uso Verifique se o hardware é capaz de executar o Unity 3D Verifique se o hardware é capaz de executar o compiz Verifica o tempo necessário para reconectar ao ponto de acesso WIFI Verifique para ver se CONFIG_NO_HZ está configurado no kernel (isto é apenas um simples teste de regressão) Checkbox - testes do sistema O Checkbox não terminou completamente.
Deseja reexecutar o último teste,
continuar no próximo teste ou
voltar para o início? Verifica se uma lista de fontes específicada contém os repositórios necesários Verifica o descarregamento da bateria quando inativo. Relata o tempo até que ela se esgote Verifica o descarregamento da bateria durante a suspensão. Relata o tempo até Verifica o descarregamento da bateria quando estiver assistindo a um filme. Relata o tempo Verifica quanto tempo leva para reconectar uma conexão sem fio existente depois de um ciclo de suspensão/retorno. Verifica quanto tempo deva para reconectar uma conexão com fio existente depois
de um ciclo de suspensão/retorno. Verifica o tempo de suspensão para asseguar que a máquina suspenda e retorne dentro de um dado limite Processos filhos interrompidos ou terminados Selecione os testes para serem executados em seu sistema: Testes de codec Recolher tudo Coleta informações de sistema relacionados ao áudio. Esses dados podem ser utilizados para simular o sistema de áudio deste computador e executar testes mais detalhados sob um ambiente controlado. Coleta informações sobre profundidade de cor e formato de pixel. Coletar informações sobre a taxa de atualização. Coletar informações sobre a memória de vídeo. Coleta informações dos modos gráficos (resolução e taxa de atualização da tela) Combine com o caractere acima para expandir o nó Comando não localizado. O comando recebeu sinal %(signal_name)s: %(signal_description)s Comentários Tipos de testes de documentos comuns Componentes Configuração de sobreposição de parâmetros. Continuar Continuar se parado Teste de gravação de DVD. As dependências estão faltando, então algumas tarefas não serão executadas. Desmarcar todos Desmarcar todos Informação detalhada... Detecta e exibe discos conectados ao sistema. Detecta e mostra os dispositivos USB anexados ao sistema. Determina se a tela é detectada como um dispositivo multitoque automaticamente. Determina se a tela é detectada como um dispositivo sem recurso de toque
automaticamente. Determina se o touchpad foi detectado como um dispositivo de multi-toque
automaticamente. Determina se o touchpad foi detectado como um dispositivo de toque simples
automaticamente. Teste de disco Testes de disco Uso do disco em um sistema ocioso. Você realmente quer pular o teste? Não perguntar novamente Concluído Despeja informação da memória em um arquivo para comparação depois de que o teste de suspensão for executado. E-mail Endereço de e-mail precisa estar em formato apropriado. E-mail: Assegurar que a atual resolução é igual ou excede a resolução mínima recomendada (800x600). Veja aqui para detalhes: Digite o texto:
 Erro Trocando informações com o servidor... Executando %(test_name)s Expandir tudo Teste de Placa Express Testes de ExpressCards Falha ao entrar em contanto com o servidor. Por favor tente
novamente ou envie o seguinte nome de arquivo:
%s

diretamente para o banco de dados do sistema:
https://launchpad.net/+hwdb/+submit Falha ao abrir o arquivo '%s': %s Falha ao processar formulário: %s Falha ao enviar para o servidor,
tente novamente mais tarde. Testes de leitor de impressão de digital Teste de Firewire Testes de disco firewire Exceção de ponto flutuante Testes de disco flexível Testar o disquete Formulário Informações adicionais: Recolhendo informações de seu sistema... Teste de vídeo Testes de gráficos Travamento detectado no terminal de controle ou morte do processo de controle Testes de hibernação Testes de teclas de atalho Eu irei sair automaticamente quando todas as teclas tiverem sido pressionadas. Se uma tecla não estiver presente no seu teclado, pressione o botão "Pular" para removê-la do teste. Se falta em seu teclado uma ou mais teclas, pressione seu número para pular o teste dessa tecla. Instrução ilegal Em progresso Informação Teste de informações A informação não foi gravada no Launchpad. Testes de informação Testes de dispositivos de entrada Teste de entrada Conexão de Internet totalmente estabelecida Interromper a partir de teclado Referência de memória inválida Teste de teclas Teste de teclas Matar sinal Testes de LED Lista os dispositivos USB Lista o driver do dispositivo e a versão para todos dispositivos de áudio. Tenha certeza que o dispositivo RTC(relógio de tempo real) existe. Espaço máximo em disco utilizado durante uma instalação teste padrão Teste de cartão de memória Testes de cartão de mídia Teste de memória Testes de memória Teste sortido Testes diversos Faltando arquivo de configuração como argumento.
 Testes de banda larga móvel Teste de monitor Testes de monitor Move uma janela 3D pela tela Pró&ximo Pró_ximo Informações de rede Teste de rede Testes de rede Próximo Sem conexão de Internet Não resolvido Não Iniciado Não suportado Não testado Não requerido Um de depuração, informação, alerta, erro ou crítico. Abre e fecha 4 janelas 3D mútiplas vezes Abrir e fechar uma janela 3D diversas vezes Abra, suspenda, retorne e feche uma janela 3D várias vezes Testes de dispositivo ótico Teste óptico Teste de leitor óptico. SENHA:  PROPÓSITO:
     Verificar se a conexão do dispositivo de saída de linha funciona corretamente.
PASSOS:
     1. Conecte o cabo dos auto falantes (com amplificadores embutidos) na porta do dispositivo de saída de linha.
     2. Abra as preferências do sistema de som, guia "Saída", selecione "Dispositivo de saída" na lista do conector. Clique no botão "Teste".
     3. Nas preferências do sistema de som, selecione "Áudio interno" na lista do dispositivo e clique em "Testar auto falantes" para verificar os canais esquerdo e direito.
VERIFICAÇÃO:
     1. Você ouve algum som nos auto falantes? Os auto falantes internos NÃO devem ficar sem som automaticamente.
     2. Você ouve o som vindo dos canais correspondentes? PROPÓSITO:
    Verificação do LED do Caps Lock
PASSOS:
    1. Pressione a tecla "Caps Lock" para ativar e desativar letras maiúsculas
    2. O LED do Caps Lock deve ascender e apagar toda vez que a tecla for pressionada
VERIFICAÇÃO:
    A luz do LED do Caps Lock funcionou como esperado? PROPÓSITO:
    Verificação do LED da câmera
PASSOS:
    1. Selecione Teste para ativar a câmera
    2. O LED da câmera deve se ascender por alguns segundos
VERIFICAÇÃO:
    O LED da câmera se ascendeu? PROPÓSITO:
     Verificar se a conexão externa da conexão de entrada de linha funciona corretamente.
PASSOS:
     1. Use um cabo para conectar a porta do dispositivo de entrada de linha à fonte de som (microfone, tocador de mídia, etc).
     2. Abra as preferências do sistema de som, guia "Entrada", selecione "Dispositivo de entrada" na lista do conector. Clique no botão "Teste".
     3. Após alguns segundos, sua gravação será reproduzida.
VERIFICAÇÃO:
     1. Você ouviu sua gravação? PROPÓSITO:
    Verificar se os vários canais de áudio estão funcionando corretamente
PASSOS:
    1. Clique no botão Teste
VERIFICAÇÃO:
    Você deve ouvir claramente uma voz a partir dos diferentes canais de áudio PROPÓSITO:
    Verificar o recurso "arrastar e soltar" da tela sensível ao toque.
PASSOS:
    1. Dê um toque duplo, segure e arraste um objeto na área de trabalho.
    2. Solte o objeto em um local diferente.
VERIFICAÇÃO:
    O objeto foi selecionado, arrastado e solto? PROPÓSITO:
    Verificar o reconhecimento gesto de pinça para zoom
PASSOS:
    1. Coloque dois dedos na tela e junte-os
    2. Coloque dois dedos na tela e separe-os
VERIFICAÇÃO:
    A tela foi ampliada e reduzida? PROPÓSITO:
    Verificar o reconhecimento de toque no touchscreen
PASSOS:
    1. Toque em um objeto na tela com o dedo. O cursor deve pular para a localização do toque e o objeto deve ser selecionado
VERIFICAÇÃO:
    O reconhecimento de toque funcionou? PROPÓSITO:
    Criar tarefas que usem a CPU tanto quanto possível por duas horas. Este teste é considerado aprovado se o sistema não congelar. PROPÓSITO:
    Verificação do LED do HD
PASSOS:
    1. Selecione "Teste" para gravar e ler um arquivo temporário por alguns segundos
    2. O LED do HD deve ascender durante a gravação e leitura no HD
VERIFICAÇÃO:
    O LED do HD ascendeu? PROPÓSITO:
    Verificação da interface de áudio HDMI
PASSOS:
    1. Conecte um dispositivo externo HDMI com som (Utilize apenas uma interface HDMI / DisplayPort de cada vez para este teste)
    2. Clique no botão Testar
VERIFICAÇÃO:
    Você pode ouvir o som a partir do dispositivo HDMI? PROPÓSITO:
    Manter informações relacionadas ao testador no relatório
PASSOS:
    1. Informação do testador
    2. Por favor, insira as seguintes informações no campo de comentários:
       a. Nome
       b. Endereço de email
       c. Razão para a execução deste teste
VERIFICAÇÃO:
    Nada a verificara para este teste. PROPÓSITO:
    Verificação do LED do teclado numérico
PASSOS:
    1. Pressione a tela "Num Lock" para alternar o LED do teclado numérico
    2. Clique no botão "Teste" para abrir uma janela de verificação de digitação
    3. Digite utilizando tanto o teclado numérico com o LED aceso quanto apagado
VERIFICAÇÃO:
    1. O status do LED do teclado número deve alternar toda vez que a tecla "Num Lock" for pressionada
    2. Os números devem apenas aparecer na tela de verificação do teclado quando o LED estiver aceso PROPÓSITO:
    Verificação do LED indicador de "ligado"
PASSOS:
    1. O LED indicador de "ligado" deve estar acesso enquanto o dispositivo estiver ligado
VERIFICAÇÃO:
    O LED indicador de "ligado" está aceso como esperado? PROPÓSITO:
    Captura uma tela da sua sessão atual (logado no ambiente Unity)
PASSOS:
    1. Tire uma foto utilizando uma webcam USB
VERIFICAÇÃO:
    Revise manualmente a conexão posteriormente PROPÓSITO
    Fazer uma captura de tela da tela atual antes depois de uma suspensão (em uma sessão do Unity)
PASSOS:
    1. Tire uma foto utilizando uma webcam USB
VERIFICAÇÃO:
    Revise o anexo manualmente depois PROPÓSITO:
    Capture uma tela da sua sessão atual durante uma exibição de arquivo em tela cheia
PASSOS:
    1. Inicie um vídeo em tela cheia
    2. Tire uma foto utilizando a webcam USB após alguns segundos
VERIFICAÇÃO:
    Revise manualmente a conexão posteriormente PROPÓSITO:
    Testar se o hardware sem fio do sistema pode se conectar a um roteador sem
    qualquer segurança e usando os protocolos 802.11b/g.
PASSOS:
    1. Abra sua ferramenta de configuração de roteadores
    2. Altere as configurações para aceitar apenas conexões nas bandas sem fio B e G
    3. Assegure-se de que o SSID esteja configurado como ROUTER_SSID
    4. Altere as configurações de segurança para não utilizar segurança
    5. Clique no botão "Testar" para criar uma conexão com o roteador e testá-la
VERIFICAÇÃO:
    A verificação é automatizada, não altere o resultado selecionado automaticamente. PROPÓSITO:
    Testar se o dispositivo de rede sem fio do sistema pode se conectar a um
    roteador sem qualquer segurança e usando o protocolo 802.11n.
PASSOS:
    1. Abra sua ferramenta de configuração de roteadores
    2. Altere as configurações para aceitar apenas conexões na banda sem-fio N
    3. Certifique-se de que o SSID esteja configurado como ROUTER_SSID
    4. Altere as configurações de segurança não usar qualquer segurança
    5. Clique no botão "Testar" para criar uma conexão com o roteador e testar a conexão
VERIFICAÇÃO:
    A verificação é automatizada, não altere o resultado selecionado automaticamente. PROPÓSITO:
    Testar se o hardware sem-fio do sistema pode se conectar a um roteador usando segurança
    WPA e os protocolos 802.11b/g.
PASSOS:
    1. Abra sua ferramenta de configuração de roteadores
    2. Altere as configurações para aceitar apenas conexões nas bandas sem-fio B e G
    3. Tenha certeza que o SSID esteja configurado como ROUTER_SSID
    4. Altere as configurações de segurança para usar WPA2 e se assegure que o PSK combine com os configurados em ROUTER_PSK
    5. Clique no botão "Testar" para criar uma conexão com o roteador e testar a conexão
VERIFICAÇÃO:
    A verificação é automatizada, não altere o resultado selecionado automaticamente. PROPÓSITO:
    Verifica se o dispositivo de rede sem fio do sistema consegue se conectar a
    um roteador utilizando segurança WPA e os protocolos 802.11n.
PASSOS:
    1. Abra a ferramenta de configuração de seu roteador.
    2. Altere as definições para aceitar somente conexões na banda N.
    3. Assegure-se de que o SSID está configurado para ROUTER_SSID.
    4. Altere a configuração de segurança para utilizar WPA2 e assegure-se de
       que o PSK corresponde a ROUTER_PSK.
    5. Clique no botão "Teste" para criar uma conexão para o roteador e testar
       a conexão.
VERIFICAÇÃO:
    A verificação é automática, não altere o resultado selecionado automaticamente. PROPÓSITO:
    Este teste verifica se o plugin manual funciona corretamente
PASSOS:
    1. Adicione um comentário
    2. Marque o resultado como aprovado
VERIFICAÇÃO:
    Verifique se no relatório o resultado é "aprovado" e se o comentário é mostrado PROPÓSITO:
    Este teste faz um ciclo através dos modos de vídeo detectados
PASSO:
    1. Clique em "Testar" para iniciar o ciclo através dos modos de vídeo
VERIFICAÇÃO:
    A tela pareceu funcionar para cada modo? PROPÓSITO:
    Este teste irá verificar as capacidades 3D básicas da sua placa de vídeo
PASSOS:
    1. Clique em "Testar" para executar uma demonstração OpenGL. Pressione ESC a qualquer momento para fechar.
    2. Verifique se a animação não está irregular ou lenta.
VERIFICAÇÃO:
    1. A animação 3D apareceu?
    2. A animação estava livre de lentidão/irregularidades? PROPÓSITO:
    Este teste irá verificar se um modem DSL pode ser configurado e conectado.
PASSOS:
    1. Conecte a linha de telefone ao computador
    2. Clique no ícone da Rede no painel superior
    3. Escolha "Editar conexões"
    4. Escolha a aba "DSL"
    5. Clique no botão "Adicionar"
    6. Configure corretamente os parâmetros da conexão
    7. Clique em "Testar" para verificar se é possível estabelecer uma conexão HTTP
VERIFICAÇÃO:
    Foi exibido uma notificação e a conexão foi estabelecida corretamente? PROPÓSITO:
    Este teste verificará se um dispositivo de áudio USB funciona corretamente
PASSOS:
    1. Conecte um dispositivo de áudio USB ao sistema
    2. Clique em "Teste", então fale ao microfone
    3. Depois de alguns segundos, sua voz será reproduzida
VERIFICAÇÃO:
    Você ouviu sua voz ser reproduzida através do fone de ouvido USB? PROPÓSITO:
    Este teste irá verificar se a conexão bluetooth funciona corretamente
PASSOS:
    1. Habilite o bluetooth em algum dispositivo móvel (PDA, smartphone etc.)
    2. Clique no ícone bluetooth na barra de menu
    3. Selecione 'Configurar novo dispositivo'
    4. Procure o dispositivo na lista e selecione-o
    5. No dispositivo, digite o código PIN escolhido automaticamente pelo assistente
    6. O dispositivo deverá emparelhar com o computador
    7. Clique com o botão direito no ícone bluetooth e escolha explorar arquivos
    8. Se necessário, autorize o computador a explorar os arquivos no disposivito
    9. Você deverá ser capaz de explorar os arquivos
VERIFICAÇÃO:
    Todos os passos funcionaram? PROPÓSITO:
    Esse teste irá verificar se o conector do fone de ouvido funciona corretamente
PASSOS:
    1. Conecte um par de fones de ouvido ao seu dispositivo de áudio
    2. Clique no botão Testar para tocar um som no seu dispositivo de áudio
VERIFICAÇÃO:
    Você ouviu um som através dos fones de ouvido e o som tocou sem nenhuma distorção, estalido ou outros barulhos estranhos? PROPÓSITO:
    Esse teste irá verificar se os alto-falantes internos funcionam corretamente
PASSOS:
    1. Assegure-se de que nenhum alto-falante externo ou fone de ouvido está
       conectado. Se estiver testando um computador de mesa, alto-falantes
       externos são permitidos
    2. Clique no botão Testar para reproduzir um breve som no seu dispositivo
       de áudio
VERIFICAÇÃO:
    Você ouviu um som? PROPÓSITO:
    Este teste irá verificar se a gravação de som usando um microfone externo funciona corretamente
PASSOS:
    1. Conecte um microfone externo no conector de microfone
    2. Clique em "Testar" e em seguida fale no seu microfone externo
    3. Após alguns segundos, o que você falou será reproduzido para você ouvir
VERIFICAÇÃO:
    Você ouviu o que foi dito por você? PROPÓSITO:
    Este teste irá verificar se a gravação de som usando o microfone embutido funciona corretamente
PASSOS:
    1. Desconecte qualquer microfone externo que você tenha conectado
    2. Clique em "Testar" e em seguida fale no seu microfone interno
    3. Após alguns segundos, o que você falou será reproduzido para você ouvir.
VERIFICAÇÃO:
    Você ouviu o que foi dito por você? PROPÓSITO:
    Este teste verificará se a webcam está funcionando
PASSOS:
    1. Clique em teste para exibir uma imagem da webcam por 10 segundos.
VERIFICAÇÃO:
    Você conseguiu ver a imagem? PROPÓSITO:
    Este teste irá verificar se a câmera embutida funciona
PASSOS:
    1. Clique em Teste para exibir uma captura de vídeo da câmera por dez segundos.
VERIFICAÇÃO:
    Você viu a captura de vídeo? PROPÓSITO:
    Este teste vai verificar se a tela está correta após suspender e retornar
PASSOS:
    1. Verifique se a sua tela não apresenta anomalias visuais após retornar.
VERIFICAÇÃO:
    A tela funciona normalmente após retornar da suspensão? PROPÓSITO:
    Este teste irá verificar se o seu sistema pode alternar entre um terminal virtual e voltar ao X
PASSOS:
    1. Clique em "Testar" para alternar para outro terminal virtual e depois voltar ao X
VERIFICAÇÃO:
    A tela mudou temporariamente para o console de texto e então mudou de volta para sua sessão atual? FINALIDADE:
    Esse teste vai verificar se o sistema detecta corretamente
    a remoção de um cartão CF do leitor de cartões do sistema.
PASSOS:
    1. Clique em "Teste" e remova o cartão CF do leitor.
       (Nota: esse teste irá expirar após 20 segundos.)
VERIFICAÇÃO:
    A verificação desse teste é automatizada. Não altere o resultado
    selecionado automaticamente. PROPÓSITO:
    Este teste verificará se o sistema detecta corretamente
    a remoção de cartão MS do leitor de cartão.
PASSOS:
    1. Clique em "Teste"e remova o cartão MS do leitor.
       (Nota: este teste será finalizado após 20 segundos.)
VERIFICAÇÃO:
    A verificação do teste é automática. Não altere o 
    resultado selecionado automaticamente. PROPÓSITO:
    Este teste irá verificar se o sistema detecta corretamente a remoção
    de um cartão MSP do leitor de cartões.
PASSOS:
    1. Clique em "Teste" e remova o cartão MSP do leitor.
       (Nota: este teste tem tempo limite de 20 segundos)
VERIFICAÇÃO:
    A verificação deste teste é automática. Não altere o resultado selecionado
    automaticamente. PROPÓSITO:
    Este teste verificará se o sistema detecta corretamente
    a remoção de cartão SDXC do leitor de cartão do sistema.
PASSOS:
    1. Clique em "Teste" e remova o cartão SDXC  do leitor.
       (Nota: este este será finalizado após 20 segundos.)
VERIFICAÇÃO:
    A verificação deste teste é automática. Não altere o 
    resultado automaticamente selecionado. PROPÓSITO:
    Este teste irá verificar se o sistema detecta corretamente a remoção
    de um cartão xD do leitor de cartões.
PASSOS:
    1. Clique em "Teste" e remova o cartão xD do leitor.
       (Nota: este teste tem tempo limite de 20 segundos)
VERIFICAÇÃO:
    A verificação deste teste é automática. Não altere o resultado selecionado
    automaticamente. FINALIDADE:
    Esse teste vai verificar se o sistema detecta corretamente
    a remoção de um cartão SD do leitor de cartões do sistema.
PASSOS:
    1. Clique em "Teste" e remova o cartão SD do leitor.
       (Nota: esse teste irá expirar após 20 segundos.)
VERIFICAÇÃO:
    A verificação desse teste é automatizada. Não altere o resultado
    selecionado automaticamente. FINALIDADE:
    Esse teste vai verificar se o sistema detecta corretamente
    a remoção de um cartão SDHC do leitor de cartões do sistema.
PASSOS:
    1. Clique em "Teste" e remova o cartão SDHC do leitor.
       (Nota: esse teste irá expirar após 20 segundos.)
VERIFICAÇÃO:
    A verificação desse teste é automatizada. Não altere o resultado
    selecionado automaticamente. FINALIDADE:
    Esse teste vai verificar se o sistema detecta corretamente
    a remoção de um cartão MMC do leitor de cartões do sistema.
PASSOS:
    1. Clique em "Teste" e remova o cartão MMC do leitor.
       (Nota: esse teste irá expirar após 20 segundos.)
VERIFICAÇÃO:
    A verificação desse teste é automatizada. Não altere o resultado
    selecionado automaticamente. PROPÓSITO:
    Este teste irá verificar se o sistema detecta corretamente a conexão de
um dispositivo de armazenamento USB 3.0.
PASSOS:
    1. Clique em "Testar" e conecte um dispositivo de armazenamento USB 3.0
(pendrive ou HD) a uma porta USB 3.0. Nota: este teste irá expirar depois de
20 segundos.
    2. Não desconecte o dispositivo após o teste.
VERIFICAÇÃO:
    A verificação deste teste é automática. Não altere o resultado
selecionado automaticamente. PROPÓSITO:
    Este teste irá verificar se o sistema detecta corretamente a inserção de
    um dispositivo de armazenamento USB
PASSOS:
    1. Clique em "Teste" e insira um dispositivo de armazenamento USB (pen-drive/HD externo).
       (Nota: este teste irá expirar após 20 segundos.)
    2. Não desconecte o dispositivo após o teste.
VERIFICAÇÃO:
    A verificação deste teste é automatizada. Não altere o
    resultado selecionado automaticamente. FINALIDADE:
    Esse teste vai verificar se o sistema detecta corretamente
    a remoção de um cartão CF do leitor de cartões após o sistema ter sido suspenso.
PASSOS:
    1. Clique em "Teste" e remova o cartão CF do leitor.
       (Nota: esse teste irá expirar após 20 segundos.)
VERIFICAÇÃO:
    A verificação desse teste é automatizada. Não altere o resultado
    selecionado automaticamente. PROPÓSITO:
    Este teste irá verificar se o sistema detecta corretamente a remoção
    de um cartão MS do leitor de cartões após a suspensão do sistema.
PASSOS:
    1. Clique em "Teste" e remova o cartão MS do leitor.
       (Nota: este teste tem tempo limite de 20 segundos)
VERIFICAÇÃO:
    A verificação deste teste é automática. Não altere o resultado selecionado
    automaticamente. PROPÓSITO:
    Este teste irá verificar se o sistema detecta corretamente a remoção
    de um cartão MSP do leitor de cartões após a suspensão do sistema.
PASSOS:
    1. Clique em "Teste" e remova o cartão MSP do leitor.
       (Nota: este teste tem tempo limite de 20 segundos)
VERIFICAÇÃO:
    A verificação deste teste é automática. Não altere o resultado selecionado
    automaticamente. PROPÓSITO:
    Este teste irá verificar se o sistema detecta corretamente a remoção
    de um cartão xD do leitor de cartões após a suspensão do sistema.
PASSOS:
    1. Clique em "Teste" e remova o cartão xD do leitor.
       (Nota: este teste tem tempo limite de 20 segundos)
VERIFICAÇÃO:
    A verificação deste teste é automática. Não altere o resultado selecionado
    automaticamente. FINALIDADE:
    Este teste vai verificar se o sistema detecta corretamente a remoção
    de um cartão MMC do leitor de cartões após o sistema ter sido suspenso.
PASSOS:
    1. Clique em "Teste" e remova o cartão MMC do leitor.
       (Nota: esse teste irá expirar após 20 segundos.)
VERIFICAÇÃO:
    A verificação desse teste é automatizada. Não altere o resultado
    selecionado automaticamente. FINALIDADE:
    Esse teste vai verificar se o sistema detecta corretamente
    a remoção de um cartão SDHC do leitor de cartões após o sistema ter sido suspenso.
PASSOS:
    1. Clique em "Teste" e remova o cartão SDHC do leitor.
       (Nota: esse teste irá expirar após 20 segundos.)
VERIFICAÇÃO:
    A verificação desse teste é automatizada. Não altere o resultado
    selecionado automaticamente. PROPÓSITO:
    Este teste irá verificar se o sistema detecta corretamente a remoção de
um dispositivo de armazenamento USB 3.0.
PASSOS:
    1. Clique em "Testar" e remova o dispositivo de armazenamento USB 3.0.
Nota: este teste irá expirar depois de 20 segundos.
VERIFICAÇÃO:
    A verificação deste teste é automática. Não altere o resultado
selecionado automaticamente. PROPÓSITO:
    Este teste irá verificar se o sistema detecta corretamente a remoção de
    um dispositivo de armazenamento USB
PASSOS:
    1. Clique em "Teste" e remova o dispositivo USB.
       (Nota: este teste irá expirar após 20 segundos.)
VERIFICAÇÃO:
    A verificação deste teste é automatizada. Não altere o
    resultado selecionado automaticamente. PROPÓSITO:
    Este teste irá verificar se você consegue gravar e ouvir áudio usando um dispositivo de áudio bluetooth
PASSOS:
    1. Habilite o fone bluetooth
    2. Clique no ícone bluetooth na barra de menu
    3. Selecione 'Configurar novo dispositivo'
    4. Procure pelo dispositivo na lista e selecione-o
    5. No dispositivo, digite o código PIN escolhido automaticamente pelo assistente
    6. O dispositivo deverá emparelhar com o computador
    7. Clique em "Teste" para gravar por cinco segundos e reproduza a gravação no dispositivo bluetooth
VERIFICAÇÃO:
    Você ouviu o som gravado no bluetooth PROPÓSITO:
    Este teste irá verificar se você consegue transferir informação através de uma conexão bluetooth
PASSOS:
    1. Tenha certeza que você consegue explorar os arquivos no seu dispositivo móvel
    2. Copie um arquivo do computador para o dispositivo móvel
    3. Copie um arquivo do dispositivo móvel para o computador
VERIFICAÇÃO:
    Os arquivos foram todos copiados corretamente? PROPÓSITO:
    Este teste verificará se você pode usar um dispositivo BlueTooth HID
PASSOS:
    1. Habilite tanto o mouse quanto o teclado bluetooth
    2. Clique no ícone bluetooth na barra de menu
    3. Selecione 'Configurar um novo dispositivo'
    4. Localize o dispositivo na lista e o selecione
    5. Para mouse, faça algo como movimentar a seta, clique com o botão direito e esquerdo, duplo cliques
    6. Para teclados, clique no botão Teste para iniciar uma pequena ferramenta. Digite um texto qualquer na ferramenta e a feche.
VERIFICAÇÃO:
    O dispositivo funciona de acordo com o esperado? PROPÓSITO:
    Este teste irá verificar se você pode utilizar um dispositivo de interface
humana USB.
PASSOS:
    1. Ative um mouse ou teclado USB.
    2. Para teclados, clique no botão Testar para iniciar uma pequena
ferramenta. Digite um texto e feche a ferramenta.
VERIFICAÇÃO:
    O dispositivo funcionou como esperado? PROPÓSITO:
    Este teste irá verificar se o sistema detecta dispositivos de armazenamento USB.
PASSOS:
    1. Conecte um ou mais pendrives ou discos rígidos.
    2. Clique em "Teste".
INFO:
    $output
VERIFICAÇÃO:
    Os dispositivos foram detectados? PROPÓSITO:
    Este teste vai checar se o sistema consegue detectar a inserção de um HD externo FireWire
PASSOS:
    1. Clique em 'Testar' para iniciar o teste. Esse teste irá
       expirar e acusar falha se a inserção não for detectada em até 20 segundos.
    2. Conecte um HD externo FireWire em uma porta FireWire disponível.
VERIFICAÇÃO:
    A verificação deste teste é automatizada. Não altere o resultado
    automaticamente selecionado PROPÓSITO:
    Este teste verificará se o sistema consegue detectar a inserção de um HDD
    eSATA
PASSOS:
    1. Clique em 'Teste' para iniciar o teste. Este abortará e falhará se a 
       inserção não for detectada em 20 segundos.
    2. Conecte o HDD eSATA a uma porta eSATA disponível.
VERIFICAÇÃO:
    A verificação para este teste é automática. Não altere o resultado
    automático selecionado. FINALIDADE:
    Este teste irá checar se o sistema consegue detectar a remoção de um
    HD externo FireWire.
PASSOS:
    1. Clique em 'Testar' para iniciar o teste. Esse teste irá expirar e acusar falha se
        a remoção não for detectada em até 20 segundos.
    2. Remova o HD externo FireWire conectado à porta FireWire.
VERIFICAÇÃO:
    A verificação deste teste é automatizada. Não altere o resultado
    automaticamente selecionado PROPÓSITO:
    Este teste irá verificar se o sistema pode detectar a remoção de um HDD
    eSATA
PASSOS:
    1. Clique em 'Teste' para iniciar o teste.  Ele abortará e falhará se
       a remoção não for detectada em 20 segundos.
    2. Remova a unidade de disco rígido eSATA conectado anteriormente a partir
       da porta eSATA.
VERIFICAÇÃO:
    A verificação deste teste é automatizado. Não altere o resultado
    selecionado automaticamente PROPÓSITO:
    Este teste irá verificar se o seu sistema pode hibernar com sucesso (caso tenha suporte)
PASSOS:
    1. Clique em "Testar"
    2. O sistema irá hibernar e deverá acordar sozinho dentre 5 minutos
    3. Se o seu sistema não acordar sozinho após os 5 minutos, por favor aperte o botão Liga/desliga para acordá-lo manualmente
    4. Se o sistema não consegue retornar após a hibernação, por favor reinicie o Sistema de testes e marque este teste como Reprovado
VERIFICAÇÃO:
    O sistema hibernou com sucesso e funcionou adequadamente após acordar? PROPÓSITO:
    Este teste irá verificar a capacidade de reprodução de seu CD de áudio
PASSOS:
    1. Insira um CD de áudio em sua unidade ótica
    2. Quando requisitado, inicie o reprodutor de música
    3. Localize o CD na tela do reprodutor de música
    4. Selecione o CD no reprodutor de música
    5. Clique no botão Play para escutar a música no CD
    6. Pare a reprodução depois de algum tempo
    7. Clique com o botão direito do mouse no ícone do CD e selecione "Ejetar disco"
    8. O CD deverá ser ejetado
    9. Feche o reprodutor de música
VERIFICAÇÃO:
    Todos os passos funcionaram? PROPÓSITO
    Este teste verificará os recursos de reprodução do seu DVD
PASSOS:
    1. Insira um DVD que contenha qualquer filme em seu drive óptico
    2. Clique em "Teste" para reproduzir o DVD no Totem
VERIFICAÇÃO:
    O filme foi reproduzido? PROPÓSITO:
    Este teste irá verificar uma conexão USB 3.0.
PASSOS:
    1. Conecte um pendrive ou HD USB 3.0 a uma porta USB 3.0 do computador.
    2. Um ícone deverá aparecer no Lançador.
    3. Clique em "Testar" para iniciar o teste.
VERIFICAÇÃO:
    A verificação deste teste é automática. Não altere o resultado
selecionado automaticamente. PROPÓSITO:
    Este teste verificará sua conexão USB.
PASSOS:
    1. Conecte um dispositivo de armazenagem USB a um slot USB externo neste computador.
    2. Um ícone deve aparecer no Lançador.
    3. Confirme se o ícone aparece.
    4. Ejete o dispositivo.
    5. Repita com cada slot USB externo.
VERIFICAÇÃO:
    Todos os slots USB funcionam com o dispositivo? PROPÓSITO:
    Este teste irá verificar a conexão USB.
PASSOS:
    1. Conecte um disco rígido USB ou um pendrive no computador.
    2. Um ícone deve aparecer no lançador.
    3. Clique em "Teste" para iniciar o teste.
VERIFICAÇÃO:
    A verificação deste teste é automatizada. Não altere o
    resultado selecionado automaticamente. PROPÓSITO:
    Este teste irá verificar os recursos de economia de energia do seu monitor.
PASSOS:
    1. Clique em "Teste" para testar os recursos de economia de energia.
    2. Pressione qualquer tecla ou mova o mouse para retornar.
VERIFICAÇÃO:
    O monitor apagou e ligou novamente? PROPÓSITO:
    Este teste irá verificar sua conexão de rede com fio
PASSOS:
    1. Clique no ícone da Rede no painel superior
    2. Escolha uma rede abaixo da sessão "Redes com fio"
    3. Clique em "Testar" para verificar a possibilidade de se estabelecer uma conexão HTTP
VERIFICAÇÃO
    Foi exibido uma notificação e a conexão foi estabelecida corretamente? PROPÓSITO:
    Este teste verificará sua conexão sem fio.
PASSOS:
    1. Clique no ícone de Rede no painel.
    2. Selecione uma rede abaixo da seção 'Redes sem fio'.
    3. Clique em "Teste" para verificar se é possível estabelecer uma conexão HTTP.
VERIFICAÇÃO:
    Foi exibida uma notificação e a conexão foi corretamente estabelecida? PROPÓSITO:
    Este teste alternará entre os modos de exibição de tela detectados
PASSOS:
    1. Clique em "Teste" e a tela alternará entre os modos de exibição
VERIFICAÇÃO:
    Sua tela parece correta no modo detectado? PROPÓSITO:
    Este teste enviará a imagem 'JPEG_Color_Image_Ubuntu.jpg' para o dispositivo especificado
PASSOS:
1. Clique em "Teste" e você será solicitado a digitar o nome de um dispositivo Bluetooth de um aparelho que possa aceitar transferência de arquivos (pode levar alguns momentos depois de inserir o nome para que o arquivo comece a ser enviado)
    2. Aceite quaisquer confirmações que apareçam em ambos os dispositivos
VERIFICAÇÃO:
    Os dados foram corretamente transferidos? PROPÓSITO:
    Esse teste enviará a imagem "JPEG_Color_Image_Ubuntu.jpg" para um dispositivo específico
PASSOS:
    1. Tenha certeza que o Bluetooth está ativado checando o miniaplicativo indicador de Bluetooth
    2. Clique "Testar" e você será solicitado a informar o nome do dispositivo Bluetooth de um dispositivo que pode aceitar transferência de arquivos (Pode demorar um momento, após informar o nome, para o arquivo começar a ser enviado)
    3. Aceite qualquer solicitação que aparecer em ambos os dispositivos
VERIFICAÇÃO:
    O dado foi transferido corretamente? PROPÓSITO:
    Esse teste irá testar a rotação da tela
PASSOS:
    1. Clique "Testar" para testar a rotação da tela. A tela será rodada a cada 4 segundos.
    2. Verifique se todas as rotações (normal, direita, invertida, esquerda) aconteceram sem corrupção permanente da tela.
VERIFICAÇÃO:
    A rotação da tela aconteceu sem corrupção permanente da tela? PROPÓSITO:
    Este teste irá verificar a tecla de brilho
PASSO:
    1. Pressione os botões de brilho no teclado
VERIFICAÇÃO:
    O brilho alterou após o pressionamento das teclas? PROPÓSITO:
    Este teste irá verificar as tecla de brilho depois de retornar da suspensão
PASSOS:
    1. Pressione os botões de brilho no teclado
VERIFICAÇÃO:
    O brilho foi alterado de acordo com o pressionamento das teclas depois de retornar da suspensão? PROPÓSITO:
    Este teste irá verificar a tela padrão
PASSO:
    1. Clique em "Testar" para exibir um teste de vídeo.
VERIFICAÇÃO:
    Você viu a barra de cores e estática? PROPÓSITO:
    Este teste irá verificar a tecla mudo do seu teclado
PASSOS:
    1. Clique em Testar para abrir uma janela para testar a "sem som".
    2. Se a tecla funcionar, o teste será aprovado e a janela se fechará.
VERIFICAÇÃO:
    A tecla "sem som" funcionou como o esperado? PROPÓSITO:
    Este teste irá verificar a tecla de "sem som" no seu teclado depois de retornar da suspensão
PASSOS:
    1. Clique Testar para abrir uma janela na qual testar a tecla de "sem som".
    2. Se a tecla funcionar, o teste será marcado como aprovado.
VERIFICAÇÃO:
    O sistema ficou sem som de acordo com os pressionamentos da tecla? PROPÓSITO:
    Este teste irá verificar a tecla dormir
PASSOS:
    1. Pressione no teclado a tecla dormir
    2. Acorde seu sistema pressionando o botão Liga/desliga
VERIFICAÇÃO:
    O sistema dormiu após ter sido pressionado a tecla dormir? PROPÓSITO:
    Este teste irá testar a tecla "dormir" após voltar da suspensão.
PASSOS:
    1. Pressione a tecla "dormir" no teclado
    2. Acorde o sistema apertando o botão de energia
VERIFICAÇÃO:
    O sistema adormeceu após apertar a tecla "dormir" após voltar da suspensão? FINALIDADE:
    Esse teste vai verificar a tecla super do seu teclado
PASSOS:
    1. Clique em 'teste' para abrir uma janela onde a tecla super será verificada.
    2. Se a tecla funcionar, o teste será bem-sucedido e a janela irá fechar.
VERIFICAÇÃO:
    A tecla super funcionou como esperado? PROPÓSITO:
    Este teste irá verificar a tecla super do seu teclado após retornar de uma
suspensão.
PASSOS:
    1. Clique em Testar para abrir a janela onde testar a tecla super.
    2. Se a tecla funcionar, o teste será marcado como aprovado.
VERIFICAÇÃO:
    A tecla super funciona como esperado depois de retornar da suspensão? PROPÓSITO:
    Este teste irá verificar a tecla de rede sem fio depois de retornar de uma
suspensão.
PASSOS:
    1. Pressione a tecla de rede sem fio no teclado.
    2. Pressione a mesma tecla novamente
VERIFICAÇÃO:
    A rede sem fio foi desligada no primeiro pressionamento e ligada novamente
no segundo pressionamento depois de retornar da suspensão? PROPÓSITO:
    Este teste irá testar o seu teclado
PASSOS:
    1. Clique em Testar
    2. Na caixa de texto, use o seu teclado para digitar algo
VERIFICAÇÃO:
    O seu teclado está funcionando corretamente? PROPÓSITO:
    Este teste irá verificar seu dispositivo apontador
PASSOS:
    1. Mova o cursor usando o dispositivo apontador ou toque na tela.
    2. Faça algumas operações com cliques simples/duplo/direito.
VERIFICAÇÃO:
    O dispositivo apontador funcionou como o esperado? PROPÓSITO:
     Este teste irá verificar se a GUI é  utilizável depois de ter a resolução alterada manualmente
PASSOS:
    1. Abra o aplicativo de Telas
    2. Selecione uma nova resolução da lista suspensa
    3. Clique em Aplicar
    4. Selecione a resolução original na lista suspensa
    5. Clique em Aplicar
VERIFICAÇÃO:
    A resolução foi alterada como esperado? PROPÓSITO:
    Este teste irá verificar a resolução padrão da tela
PASSOS:
    1. Esta tela está usando a seguinte resolução:
INFORMAÇÃO:
    $output
VERIFICAÇÃO:
    É aceitável para a sua tela? PROPÓSITO:
    Verificar se estresse do atalho do do dispositivo de rede sem fio não causa o desaparecimento de miniaplicativos do painel ou o travamento do sistema
PASSOS:
    2. Pressione o atalho do wifi a uma taxa de 1 vez por segundo e lentamente aumente a velocidade de pressionamento, até o mais rápido que puder
VERIFICAÇÃO:
    Verifique se o sistema não está travado e se os miniaplicativos de bluetooth e de rede sem fio continuam visíveis e funcionais PROPÓSITO:
    Verificação do LED do Touchpad
PASSOS:
    1. Clique no botão touchpad ou pressione a combinação de teclas para habilitar/desabilitar o touchpad
    2. Mova seu dedo sobre o touchpad
VERIFICAÇÃO:
    1. O LED do Touchpad deve se alternar toda vez que o botão ou a combinação de teclas forem pressionados
    2. Quando o LED está asceso, a seta do mouse deve se mover ao utilizar o touchpad
    3. Quando o LED está apagado, a seta do mouse não deve se mover ao utilizar o touchpad PROPÓSITO:
    Verificação do LED do touchpad depois de retornar da suspensão
PASSOS:
    1. Clique no botão do touchpad ou pressione a combinação de teclas para ativar/desativar o botão do touchpad
    2. Deslize seu dedo sobre o touchpad
VERIFICAÇÃO:
    1. O LED de status do touchpad deve alternar toda vez que o botão é clicado ou pressionado
    2. Quando o LED estiver aceso, o ponteiro do mouse deverá se mover com o uso do touchpad
    3. Quando o LED estiver apagado, o ponteiro do mouse não deverá se mover com o uso do touchpad PROPÓSITO:
    Verificação do rolamento horizontal.
PASSOS:
    1. Selecione "Testar" quando estiver pronto e posicione o cursor entre as
bordas da janela de teste.
    2. Verifique se você consegue mover a barra deslizante horizontal movendo
o seu dedo para a direita e para a esquerda na parte inferior do touchpad.
VERIFICAÇÃO:
    Você conseguiu rolar para a direita e para a esquerda? PROPÓSITO:
    Detecção manual de multitoque do touchpad.
PASSOS:
    1. Verifique as especificações do seu sistema.
VERIFICAÇÃO:
    O touchpad deveria ter o recurso multitoque? PROPÓSITO:
    Verificação do touchpad pelo usuário.
PASSOS:
    1. Certifique-se de que o touchpad está ativado.
    2. Mova o cursou usando o touchpad.
VERIFICAÇÃO:
    O cursor se moveu? Verificação do rolamento vertical.
PASSOS:
    1. Selecione "Testar" quando estiver pronto e posicione o cursor entre as
bordas da janela de teste.
    2. Verifique se você consegue mover a barra deslizante vertical movendo
o seu dedo para cima e para baixo na lateral direita do touchpad.
VERIFICAÇÃO:
    Você conseguiu rolar para cima e para baixo? PROPÓSITO:
    Detecção manual de multitoque em tela sensível ao toque.
PASSOS:
    1. Verifique as especificações para o seu sistema.
    2. Solte o objeto em um local diferente.
VERIFICAÇÃO:
    A tela deveria ser sensível ao toque? PROPÓSITO:
    Verificar se o LED de rede sem fio (WLAN + Bluetooth) funciona depois de retornar da suspensão
PASSOS:
    1. Certifique-se de que a conexão WLAN etá estabelecida e que o Bluetooth está ativado.
    2. O LED de WLAN/Bluetooth deve acender
    3. Desligue a WLAN e o Bluetooth a partir do interruptor físico (se presente)
    4. Torne a ligá-los
    5. Desligue a WLAN e o Bluetooth a partir do miniaplicativo do painel
    6. Torne a ligá-los
VERIFICAÇÃO:
    A luz do LED de WLAN/Bluetooth acendeu como esperado depois de retornar da suspensão? PROPÓSITO:
    Verificar se o LED da rede sem fio apaga quando ela é desligada após
    retornar de uma suspensão
PASSOS:
    1. Conecte-se a um ponto de acesso
    2. Use o interruptor físico para desativar a rede sem fio
    3. Reative-a
    4. Use o gerenciador de redes para desativar a rede sem fio
VERIFICAÇÃO:
    O LED da rede sem fio apagou e a rede foi desconectada depois de retornar
    da suspensão? PROPÓSITO:
    Valida se o LED do Bluetooth se desliga e liga quando o BT é desabilitado/habilitado
PASSOS:
    1. Desative o bluetooth através de um botão físico (caso exista)
    2. Ative novamente o bluetooth
    3. Desative o bluetooth através do aplicativo do painel
    4. Ative novamente o bluetooth
VERIFICAÇÃO:
    O LED do bluetooth apagou e ascendeu duas vezes? PROPÓSITO:
    Verificar se o LED do Bluetooth acende e apaga quando o Bluetooth é ativado/
    desativado após retornar de uma suspensão
PASSOS:
    1. Desative o Bluetooth a partir do interruptor físico (se presente)
    2. Ative o Bluetooth novamente
    3. Desative o Bluetooth a partir do miniaplicativo do painel
    4. Ative o Bluetooth novamente
VERIFICAÇÃO:
    O LED do Bluetooth apagou e acendeu por duas vezes depois de retornar da
    suspensão? PROPÓSITO:
    Verificar se a tecla Caps Lock funciona da mesma forma antes e depois de uma suspensão
PASSOS:
    1. Pressione a tecla Caps Lock para ativar/desativar as maiúsculas
    2. O LED do Caps Lock deverá acender e apagar toda vez que a tecla é pressionada
VERIFICAÇÃO:
    O LED do Caps Lock acendeu como esperado depois de retornar de uma suspensão? PROPÓSITO:
    Verificar se o atalho para o Vídeo Externo está funcionando como esperado.
PASSOS:
    1.Conecte um monitor externo.
    2.Pressione a tecla de atalho de visualização para alterar a configuração dos monitores.
VERIFICAÇÃO:
    Veja se o sinal de vídeo pode ser espelhado, estendido e visualizado somente no monitor externo ou no padrão. PROPÓSITO:
    Verificar se a tecla de atalho de monitor externo está funcionando como
esperado depois de retornar de uma suspensão.
PASSOS:
    1. Conecte um monitor externo.
    2. Pressione a tecla de atalho do monitor para alterar a configuração do
monitor.
VERIFICAÇÃO:
    Verifique se o sinal de vídeo pode ser espelhado, estendido, exibido apenas
no monitor externo ou no padrão após retornar de uma suspensão. PROPÓSITO:
    Verificar se o LED do HD continua operando como esperado após retornar de uma suspensão
PASSOS:
    1. Selecione "Testar" para gravar e ler um arquivo temporário por alguns segundos
    2. O LED do HD deve piscar quando grava ou lê do HD
VERIFICAÇÃO:
    O LED do HD ainda pisca com atividade do HD após retornar de uma suspensão? PROPÓSITO:
    Validar o LED indicador de bateria baixa.
PASSOS:
    1.Deixe o sistema funcionando utilizando apenas a bateria por várias horas.
    2.Monitore o LED da carga da bateria. 
VERIFICAÇÃO:
    A luz laranja do LED acende quando a bateria está com carga baixa ? PROPÓSITO:
    Verificar se a luz da bateria indica baixa energia após retornar de uma suspensão
PASSOS:
    1. Deixe o sistema funcionar na bateria por várias horas
    2. Monitore o LED de bateria cuidadosamente
VERIFICAÇÃO:
    O LED acendeu em laranja quando a bateria estava baixa após retornar de uma suspensão? PROPÓSITO:
    Valida se o LED de bateria exibe corretamente o status de carregado
PASSOS:
    1. Deixe o sistema funcionar com bateria por um pequeno período
    2. Conecte  o adaptador AC
    3. Deixe o sistema funcionar com a energia elétrica
VERIFICAÇÃO:
    O LED deixou de ficar laranja quando a bateria ficou totalmente carregada? PROPÓSITO:
    Verificar se a luz da bateria mostra o status de carregada após retornar de uma suspensão
PASSOS:
    1. Deixe o sistema funcionar com bateria por um período
    2. Conecte o adaptador de energia
    3. Deixe o sistema funcionar com o adaptador de energia
VERIFICAÇÃO:
    O LED laranja de bateria continuou apagado quando o sistema está completamente carregado após retornar de uma suspensão? PROPÓSITO:
    Valida se a luz de bateria exibe o status de "carregando"
PASSOS:
    1. Deixe o sistema funcionar com a bateria por um momento
    2. Conecte o adaptador de energia
VERIFICAÇÃO:
    O LED indicado de bateria ficou laranja? PROPÓSITO:
    Verificar se a luz da bateria mostra o status de carga após retornar de uma suspensão
PASSOS:
    1. Deixe o sistema funcionar com a bateria por um período
    2. Conecte o adaptador de energia
VERIFICAÇÃO:
    O LED indicador de bateria se manteve laranja após retornar da suspensão? PROPÓSITO:
    Verificar se o LED da câmera ainda funciona como o esperado depois de
    retornar de uma suspensão
PASSOS:
    1. Selecione Testar para ativar a câmera
    2. O LED da câmera deve acender por alguns segundos
VERIFICAÇÃO:
    O LED da câmera ainda acende e apaga depois de voltar de uma suspensão? PROPÓSITO:
    Verificar se LED do teclado numérico opera do mesmo modo antes e após retornar de uma suspensão
PASSOS:
    1. Pressione a tecla "Num Lock" para alternar o  LED do teclado numérico
    2. Clique no botão "Testar" para abrir uma janela e verificar sua digitação
    3. Digite, usando o teclado numérico, tanto quando o LED estiver aceso quanto apagado
VERIFICAÇÃO:
    1. O LED do teclado numérico deve alternar sempre que a tecla "Num Lock" é pressionada
    2. Números devem aparecer na janela de verificação do teclado apenas quando o LED estiver aceso PROPÓSITO:
    Verificar se o LED de energia opera da mesma forma após retornar de uma suspensão
PASSOS:
    1. O LED de energia deve estar aceso enquanto o dispositivo é ligado
VERIFICAÇÃO:
    O LED de energia se manteve aceso após retornar de uma suspensão? PROPÓSITO:
    Verifica se a tecla de atalho do touchpad habilita e desabilita sua funcionalidade
PASSOS:
    1. Verifica se o touchpad está funcional
    2. Aperte a tecla de atalho para alternar o touchpad
    3. Aperte novamente a tecla de atalho para alternar o touchpad
VERIFICAÇÃO:
    Verifique se o touchpad foi desabilitado e reabilitado PROPÓSITO:
    Verificar se a tecla de atalho do touchpad ativa e desativa o touchpad
depois de retornar de uma suspensão.
PASSOS:
    1. Verifique se o touchpad está ativo.
    2. Pressione a tecla de atalho do touchpad.
    3. Pressione a tecla de atalho do touchpad novamente.
VERIFICAÇÃO:
    Verifique se o touchpad foi desativado e reativado. PROPÓSITO:
    Acordar pelo teclado USB
PASSOS:
    1. Ative o item "Acordar por teclado/mouse USB" na BIOS
    2. Aperte "Testar" para entrar no modo de suspensão (S3)
    3. Aperte qualquer tecla do teclado USB  para acordar o sistema
VERIFICAÇÃO:
    O sistema acordou do modo de suspensão quando você apertou uma tecla do teclado? PROPÓSITO:
    Acordar pelo mouse USB
PASSOS:
    1. Ativar o item "Acordar por teclado/mouse USB" na BIOS
    2. Aperte "Testar" para entrar no modo de suspensão (S3)
    3. Aperte qualquer botão do mouse USB para acordar o sistema
VERIFICAÇÃO:
    O sistema acordou do modo de suspensão quando você apertou o botão do mouse? PROPÓSITO:
    Verificação do LED Wireless (WLAN + Bluetooth)
PASSOS:
    1. Assegure-se de que a conexão WLAN está estabelecida e o Bluetooth está ativado.
    2. O LED WLAN/Bluetooth deve se ascender
    3. Desative o WLAN e Bluetooth através de um botão físico (caso exista)
    4. Ative-os novamente
    5. Desative o WLAN e Bluetooth através do aplicativo do painel
    6. Ative-os novamente
VERIFICAÇÃO:
    A luz do LED WLAN/Bluetooth LED se ascendeu como esperado? PROPÓSITO:
   Este é um teste de estresse automatizado que irá forçar o sistema a hibernar/retornar por 250 ciclos FINALIDADE:
   Este é um teste de stress automatizado que vai forçar o sistema a hibernar/retornar por 30 ciclos PROPÓSITO:
   Este é um teste de estresse automatizado que irá forçar o sistema a suspender/retornar por 250 ciclos. FINALIDADE:
   Este é um teste automatizado de estresse que forçará o sistema a suspender/retornar por 30 vezes. PROPÓSITO:
    Este teste irá verificar se os modens DSL USB ou de banda larga móvel
funcionam.
PASSOS:
    1. Conecte o cabo USB ao computador.
    2. Clique com o botão direito do mouse no ícone Rede no painel.
    3. Selecione "Editar conexões".
    4. Selecione a aba "DSL" (para modem ADSL) ou "Banda larga móvel" (para
modem 3G).
    5. Clique no botão "Adicionar".
    6. Configure os parâmetros de conexão apropriadamente.
    7. Uma notificação deve confirmar que a conexão foi estabelecida.
    8. Selecione "Teste" para verificar se é possível estabelecer uma conexão
HTTP.
VERIFICAÇÃO:
    A conexão foi estabelecida corretamente? PROPÓSITO:
   Este teste verificará se o leitor de digitais pode ser usado para desbloquear o sistema.
PASSOS:
   1. Clique no indicador de sessão (ícone de engrenagem no canto esquerdo do painel).
   2. Selecione 'Bloquear tela'.
   3. Pressione qualquer tecla ou movimente o mouse.
   4. Uma janela deve aparecer para permitir o desbloqueio da tela tanto digitando a sua senha quanto utilizando autenticação por impressão digital.
   5. Utilize o leitor de digitais para desbloquear.
   6. A sua tela deverá ser desbloqueada.
VERIFICAÇÃO:
   O procedimento de autenticação funcionou corretamente? PROPÓSITO:
   Este teste verificará se uma impressora de rede é utilizável
PASSOS:
   1. Certifique-se de que uma impressora está disponível na sua rede
   2. Clique no botão de engrenagem no canto superior direito e depois clique em Impressoras
   3. Se a impressora não estiver listada, clique em Adicionar
   4. A impressora deve ser detectada e os valores de configuração adequados devem ser exibidos
   5. Imprima uma página de teste
VERIFICAÇÃO:
   Você foi capaz de imprimir uma página de teste para a impressora de rede? FINALIDADE:
   Este teste vai verificar se o relógio da área de trabalho mostra a hora e a data corretas.
PASSOS:
   1. Verifique o relógio no canto superior direito da sua área de trabalho.
VERIFICAÇÃO:
   O relógio está mostrando a hora e a data corretas para o seu fuso-horário? PROPÓSITO:
   Este teste irá verificar se o relógio da área de trabalho está sincronizado com o relógio do sistema.
PASSOS:
   1. Clique em "Teste" e verifique se o relógio avança 1 hora.
   Nota: Pode demorar um minuto ou mais para o relógio atualizar
   2. Botão direito do mouse sobre o relógio, depois clique em "Configurações de data & hora..."
   3. Garanta que seu aplicativo de relógio esteja definido para manual.
   4. Altere o tempo de volta em 1 hora
   5. Feche a janela e reincie
VERIFICAÇÃO:
   O relógio do sistema exibe a data e hora corretas para seu fuso horário? PROPÓSITO
   Este teste verificará se você consegue reiniciar seu sistema a partir do menu da área de trabalho
PASSOS:
   1. Clique no ícone de engrenagem no canto superior direito da área de trabalho e clique em "Desligar"
   2. Clique no botão "Reiniciar" no lado esquerdo da caixa de diálogo Desligar.
   3. Depois de iniciar a sessão, reinicie o Teste do sistema e ele deverá continuar daqui.
VERIFICAÇÃO:
Seu sistema reiniciou e exibiu a interface gráfica de autenticação claramente? PROPÓSITO:
   Este teste irá verificar se o seu sistema é capaz de reproduzir arquivos de áudio em formato Ogg Vorbis
PASSOS:
   1. Clique em Teste para reproduzir um arquivo Ogg Vorbis (.ogg)
   2. Por favor feche o reprodutor para prosseguir
VERIFICAÇÃO:
   A amostra foi reproduzida corretamente? PROPÓSITO:
   Este teste irá verificar se o seu sistema é capaz de reproduzir arquivos de áudio em formato Wave
PASSOS:
   1. Clique em Teste para reproduzir um arquivo em formato de Áudio Wave (.wav)
   2. Por favor feche o reprodutor para prosseguir.
VERIFICAÇÃO:
   A amostra foi reproduzida corretamente? PROPÓSITO:
    Se a recuperação foi bem-sucedida, você verá este teste quando reiniciar
o checkbox, e não o sniff4.
PASSOS:
   1. Clique em Sim.
VERIFICAÇÃO:
    Desnecessária, este é um falso teste. PROPÓSITO:
 Simula uma falha reiniciando a máquina
PASSOS:
 1. Clique em teste para acionar a reinicialização
 2. Selecione "Continuar" quando reiniciar a sessão e o checkbox reabrir
VERIFICAÇÃO:
 Você não irá ver a verificação de usuário PROPÓSITO:
 Este teste irá verificar se gcalctool (Calculadora) funciona.
PASSOS:
 Clique no botão "Testar" para abrir a calculadora e realize:
  1. Recortar
  2. Copiar
  3. Colar
VERIFICAÇÃO:
 As funções foram realizadas como esperado? PROPÓSITO:
 Este teste irá verificar se gcalctool (Calculadora) funciona.
PASSOS:
 Clique no botão "Testar" para abrir a calculadora e realize:
  1. Ajuste da memória
  2. Zerar a memória
  3. Última limpeza da memória
  4. Limpeza da memória
VERIFICAÇÃO
 As funções foram realizadas como o esperado? PROPÓSITO:
 Este teste irá verificar se gcalctool (Calculadora) funciona).
PASSOS:
 Clique no botão "Testar" para abrir a calculadora e realize:
 1. Funções matemáticas simples (+,-,/,*)
 2. Funções matemáticas aninhadas ((,))
 3. Matemática fracionária
 4. Matemática decimal
VERIFICAÇÃO:
 As funções foram realizadas como esperado? PROPÓSITO:
 Este teste irá verificar se gcalctool (Calculadora) funciona.
PASSO:
 Clique no botão "Testar" para abrir a calculadora.
VERIFICAÇÃO:
 A calculadora foi aberta corretamente? PROPÓSITO:
 Este teste verifica se o gedit funciona.
PASSOS:
 1. Clique no botão "Teste" para abrir o gedit, e reabra o arquivo que você criou anteriormente.
 2. Edite e salve o arquivo, depois feche o gedit .
VERIFICAÇÃO:
 Isto funcionou como esperado? PROPÓSITO:
 Este teste irá verificar se gedit funciona.
PASSOS:
 1. Clique no botão "Testar" para abrir o gedit.
 2. Digite algum texto e salve o arquivo (anote o nome do arquivo que você usou), então feche o gedit.
VERIFICAÇÃO:
 Isso foi realizado como esperado? PROPÓSITO:
Este teste verificará se o cliente de mensagens Empathy funciona.
PASSOS:
1. Selecione Teste para iniciar o Empathy.
2. Configure-o para conectar ao serviço de mensagens instantâneas AOL (AIM).
3. Uma vez completado o teste, por favor saia do Empathy para continuar aqui.
VERIFICAÇÃO:
Você foi capaz de conectar corretamente e enviar/receber mensagens? PROPÓSITO:
 Este teste verificará se o cliente de mensagens Empathy funciona.
PASSOS:
 1. Selecione Teste para iniciar o Empathy.
 2. Configure-o para conectar ao serviço de bate-papo do Facebook.
 3. Uma vez completado o teste, por favor saia do Empathy para continuar aqui.
VERIFICAÇÃO:
 Você foi capaz de conectar corretamente e enviar/receber mensagens? PROPÓSITO:
 Este teste verificará se o cliente de mensagens Empathy funciona.
PASSOS:
 1. Selecione Teste para iniciar o Empathy.
 2. Configure-o para conectar ao serviço Google Talk (gtalk).
 3. Uma vez completado o teste, por favor saia do Empathy para continuar aqui.
VERIFICAÇÃO:
 Você foi capaz de conectar corretamente e enviar/receber mensagens? PROPÓSITO:
 Este teste verificará se o cliente de mensagens Empathy funciona.
PASSOS:
 1. Selecione Teste para iniciar o Empathy.
 2. Configure-o para conectar ao serviço Jabber.
 3. Uma vez completado o teste, por favor saia do Empathy para continuar aqui.
VERIFICAÇÃO:
 Você foi capaz de conectar corretamente e enviar/receber mensagens? PROPÓSITO:
Este teste verificará se o cliente de mensagens Empathy funciona.
PASSOS:
1. Selecione Teste para iniciar o Empathy.
2. Configure-o para conectar ao serviço Microsoft Network (MSN).
3. Uma vez completado o teste, por favor saia do Empathy para continuar aqui.
VERIFICAÇÃO:
Você foi capaz de conectar corretamente e enviar/receber mensagens? PROPÓSITO:
 Este teste irá verificar se o Evolution funciona.
PASSOS:
 1. Clique no botão "Testar" para abrir o Evolution.
 2. Configure-o para conectar a uma conta IMAP.
VERIFICAÇÃO:
 Você foi capaz de receber e ler e-mail corretamente? PROPÓSITO:
 Este teste irá verificar se o Evolution funciona.
PASSOS:
 1. Clique no botão "Testar" para abrir o Evolution.
 2. Configure-o para conectar a uma conta POP3.
VERIFICAÇÃO: 
 Você foi capaz de receber e ler e-mail corretamente? PROPÓSITO:
 Este teste irá verificar se o Evolution funciona.
PASSOS:
 1. Clique no botão "Testar" para abrir o Evolution.
 2. Configure-o para conectar a uma conta SMTP.
VERIFICAÇÃO:
 Você foi capaz de enviar e-mail sem nenhum erro? PROPÓSITO:
 Este teste irá verificar se o Firefox pode reproduzir um vídeo Flash. Nota: isto pode
 exigir a instalação de software adicional para completar com sucesso.
PASSOS:
 1. Selecione Teste para iniciar Firefox e ver um vídeo flash curto.
VERIFICAÇÃO:
 O vídeo foi reproduzido corretamente? PROPÓSITO:
 Este teste irá verificar se o Firefox consegue reproduzir um arquivo de vídeo Quicktime (.mov).
 Nota: isto pode exigir a instalação de software adicional para completar com
 sucesso.
PASSOS:
 1. Selecione Teste para executar o Firefox com um vídeo de exemplo.
VERIFICAÇÃO:
 O vídeo foi reproduzido usando um plug-in? PROPÓSITO:
 Este teste verificará se o Firefox pode renderizar uma página web básica.
PASSOS:
 1. Selecione Teste para iniciar Firefox e ver a página web de teste.
VERIFICAÇÃO:
 A página de teste do Ubuntu carregou corretamente? PROPÓSITO:
 Este teste irá verificar se o Firefox pode executar um miniaplicativo java em uma página web. Nota:
 isto pode exigir a instalação de software adicional para concluir com sucesso.
PASSOS:
 1. Selecione Teste para abrir o Firefox com a página de teste Java, e siga as instruções.
VERIFICAÇÃO:
 O miniaplicativo é exibido? PROPÓSITO:
 Este teste irá verificar se o Firefox pode executar aplicativos flash. Nota: isto pode
 exigir a instalação de software adicional para completar com sucesso.
PASSOS:
 1. Selecione Teste para iniciar Firefox e ver um exemplo de teste Flash.
VERIFICAÇÃO:
 Você viu o texto? PROPÓSITO:
 Este teste verificará se o terminal do Gnome funciona.
PASSOS:
 1. Clique no botão "Teste" para abrir o Terminal.
 2. Digite 'ls' e pressione enter. Você deverá ver uma lista de arquivos e pastas da sua pasta pessoal.
 3. Feche a janela do terminal.
VERIFICAÇÃO:
 Isto funcionou como esperado? PROPÓSITO:
 Este teste irá verificar se o navegador de arquivos pode copiar um arquivo.
PASSOS:
 1. Clique em Teste para abrir o navegador de arquivos.
 2. Clique com o botão direito sobre o arquivo chamado Arquivo de teste 1 e clique em Copiar.
 3. Clique com o botão direito no espaço em branco e clique em Colar.
 4. Clique com o botão direito sobre o arquivo chamado Arquivo de teste 1(cópia) e clique em Renomear.
 5. Digite o nome de Arquivo de teste 2 na caixa de nome e pressione Enter.
 6. Feche o navegador de arquivos.
VERIFICAÇÃO:
 Você agora tem um arquivo chamado Arquivo de teste 2? PROPÓSITO:
 Este teste verificará se o navegador de arquivos pode copiar uma pasta
PASSOS:
 1. Clique em Teste para abrir o Navegador de arquivos.
 2. Clique com o botão direito na pasta chamada Pasta de teste e clique em Copiar.
 3. Clique com o botão direito em qualquer área branca na janela e clique em Colar.
 4. Clique com o botão direito na pasta chamada Pasta de teste(cópia) e clique em Renomear.
 5. Digite o nome Dados de teste na caixa de nome e aperte Enter.
 6. Feche o navegador de arquivos.
VERIFICAÇÃO:
 Você tem uma pasta chamada Dados de teste? PROPÓSITO:
 Este teste irá verificar se o navegador de arquivos pode criar um novo arquivo.
PASSOS:
 1. Clique em Selecionar teste para abrir o navegador de arquivos.
 2. Clique com o botão direito no espaço em branco e clique em Criar documento -> Documento vazio.
 3. Digite o nome Arquivo de teste 1 na caixa de nome e pressione Enter.
 4. Feche o navegador de arquivos.
VERIFICAÇÃO:
 Você agora tem um arquivo chamado Arquivo de teste 1? PROPÓSITO:
 Este teste irá verificar se o gerenciador de arquivos pode criar uma nova pasta.
PASSOS:
 1. Clique em Teste para abrir o gerenciador de arquivos.
 2. Na barra de menu, clique em Arquivo -> Criar pasta.
 3. Na caixa de nome para a nova pasta, digite o nome Pasta de teste e pressione Enter.
 4. Feche o gerenciador de arquivos.
VERIFICAÇÃO:
 Você tem uma nova pasta chamada Pasta de teste? PROPÓSITO:
 Este teste verificará se o navegador de arquivos pode excluir um arquivo.
PASSOS:
 1. Clique em Teste para abrir o navegador de arquivos.
 2. Clique com o botão direito no arquivo chamado Arquivo de teste 1 e clique em Mover para lixeira.
 3. Verifique se o Arquivo de teste 1 foi removido.
 4. Feche o navegador de arquivos.
VERIFICAÇÃO:
 O Arquivo de teste 1 se foi? PROPÓSITO:
 Este teste verificará se o navegador de arquivos pode excluir uma pasta.
PASSOS:
 1. Clique em Teste para abrir o navegador de arquivos.
 2. Clique com o botão direito na pasta chamada Pasta de teste e clique em Mover para lixeira.
 3. Verifique se a pasta foi excluída.
 4. Feche o navegador de arquivos.
VERIFICAÇÃO:
 A Pasta de teste foi excluída com sucesso? PROPÓSITO:
 Este teste irá verificar se o navegador de arquivos pode mover um arquivo.
PASSOS:
 1. Clique em Teste para abrir o navegador de arquivos.
 2. Clique e arraste o arquivo chamado Arquivo de teste 2 para o ícone da pasta chamada Dados de teste.
 3. Solte o botão.
 4. Dê um clique duplo no ícone de Dados de teste para abrir a pasta.
 5. Feche o navegador de arquivos.
VERIFICAÇÃO:
 O Arquivo de teste 2 foi movido com sucesso a pasta de os Dados de teste? PROPÓSITO:
 Este teste verificará se o o gerenciador de atualizações pode encontrar atualizações.
PASSOS:
 1. Clique em Teste para lançar o gerenciador de atualizações.
 2. Siga as instruções que aparecerem e se encontrar atualizações, instale-as.
 3. Quando o Gerenciador de atualizações finalizar, por favor feche o aplicativo clicando no botão Fechar no canto inferior direito.
VERIFICAÇÃO:
 O Gerenciador de Atualizações encontrou e instalou atualizações? (Passa se não foram encontradas atualizações,
mas Falha se atualizações foram encontradas, mas não foram instaladas) PROPÓSITO:
 Este teste vai verificar se o navegador de arquivos pode mover uma pasta.
PASSOS:
 1. Clique em Testar para abrir o navegador de arquivos.
 2. Clique e arraste a pasta chamada Dados de teste para o ícone chamado Pasta de teste.
 3. Solte o botão.
 4. Clique duas vezes na pasta chamada Pasta de teste para abri-la.
 5. Feche o navegador de arquivos.
VERIFICAÇÃO:
 A pasta chamada os Dados de teste foi movida com sucesso para a pasta chamada Pasta de teste? PROPÓSITO:
    Para descobrir as coisas.
PASSOS:
    1. Clique em Sim.
VERIFICAÇÃO:
    Nenhuma. Este é um falso teste. Testes de verificação do painel de relógio Testes de vereificação de reinicialização do painel Analisa o Xorg.0.Log e descobre o driver do X que está sendo executado e a sua versão Testes de periférico Testes de piglit Chama ubuntu.com e reinicia as interfaces de rede 100 vezes Reproduz um som da saída padrão e o escuta na entrada padrão. Por favor escolha (%s):  Por favor, pressione cada uma das tecla no seu teclado. Por favor digite aqui e pressione Ctrl-D quando terminar:
 Testes de dispositivos de apontamento. Teste de gerenciamento de energia Testes de gerenciamento de energia Pressione qualquer tecla para continuar... Anterior Imprimir informações sobre versão e sair. Fornece informações sobre telas conectadas ao sistema Fornece informação sobre dispositivos de rede Sair a partir de teclado Grava as configurações do mixador antes de suspender. Registre a rede atual antes de suspender. Registre a resolução atual antes de suspender Teste de verificação de renderização Reiniciar Retorna o nome, driver e sua versão para qualquer touchpad encontrado no sistema. Executa a avaliação de desempenho Cachebench Read / Modify / Write Executa a avaliação de desempenho Cachebench Read Executa a avaliação de desempenho Cachebench Write Executa a avaliação de desempenho Compress 7ZIP Executa a avaliação de desempenho Compress PBZIP2 Executa a avaliação de desempenho Encode MP3 Executar testes automatizados da Suíte de testes de firmware (fwts) Executa a avaliação de desempenho GLmark2 Executa a avaliação de desempenho GLmark2-ES2 Executa a avaliação de desempenho para GnuPG Executa a avaliação de desempenho Himeno Executa a avaliação de desempenho Lightsmark Executa a avaliação de desempenho N-Queens Executa a avaliação de desempenho Network Loopback Executa a a avaliação de desempenho Qgears2 OpenGL gearsfancy Executa a avaliação de desempenho Qgears2 OpenGL de escalonamento de imagem Executa a extensão gearsfancy da avaliação de desempenho Qgears2 XRender Executa a extensão de escalonamento de imagem da avaliação de desempenho Qgears2 XRender Executa a avaliação de desempenho Render-Bench XRender/Imlib2 Executa a avaliação de desempenho Stream Add Executa a avaliação de desempenho Stream Copy Executa a avaliação de desempenho Stream Scale Executa a avaliação de desempenho Stream Triad Executa a avaliação de desempenho Unigine Heaven Executa a avaliação de desempenho Unigine Santuary Executa a avaliação de desempenho Unigine Tropics Executa um teste de tesselação baseado em TessMark (OpenGL 4.0) em tela cheia 1920x1080 sem anti-serrilhado Executar teste de globs Execute gtkperf para ter certeza que os casos de teste baseados no GTK funcionam Executa teste de estresse gráfico. Este teste pode levar alguns minutos. Executa a avaliação de desempenho para codificador x264 H.264/AVC Executando %s... Executa um teste que transfere 100 arquivos de 10MB a um cartão SDHC. Executa um teste que transfere 100 arquivos de 10MB 3 vezes ao USB. Executa todos os conjuntos de testes de verificação de renderização. Este teste
pode levar alguns minutos. Executa testes piglit para verificar suporte a OpenGL 2.1 Executa testes piglit para verificar suporte a operações de shader fragmentado GLSL Executa testes piglit para verificar suporte a operações de shader GLSL Executa testes piglit para verificar suporte a operações de objetos de buffer de frames, buffer de profundidade e buffer de estêncil. Executa testes piglit para verificar suporte a textura de pixmap Executa testes piglit para verificar suporte a operações com objetos de buffer de vértices. Executa testes piglit para verificar suporte a operações de buffer de estêncil Executa a ferramenta de sumarização de resultados do piglit Informações de dispositivo SATA/IDE. Teste SMART Selecionar todos Selecionar todos Verificação de serviços do servidor Abreviatura para --config=.*/jobs_info/blacklist. Abreviatura para  --config=.*/jobs_info/blacklist_file. Abreviatura para  --config=.*/jobs_info/whitelist. Abreviatura para  --config=.*/jobs_info/whitelist_file. Ignorar Testes de fumaça Procurar por Sniffers Testes de instalação de programas Algumas novas unidades de disco rígido incluem uma funcionalidade que recolhe a cabeça da unidade após um curto período de inatividade. Este é um recurso de economia de energia, mas que pode ter uma má interação com o sistema operacional, resultando na unidade sendo constantemente desativada e, em seguida, ativada. Isto produz o desgaste excessivo na unidade, potencialmente levando a falhas precoces. Espaço quando terminar Iniciar o teste Estado Parar o processo Digitação encerrada no tty Teste de estresse de desligamento do sistema (100 vezes) Teste de estresse de reinicialização do sistema (100 vezes) Testes de pressão Detalhes de envio Enviar resultados Submeter para HEXR Teste concluído com sucesso! Teste de suspensão Testes de suspensão Sistema 
Testando Testes do daemon do sistema Teste do sistema Aba 1 Aba 2 Sinal de término Testar Testa despertador ACPI (despertador fwts) Testar novamente Testar e exercitar a memória. Teste cancelado Teste para clock jitter. Testar se o atd daemon está sendo executado quando o pacote é instalado. Testar se o cron daemon está sendo executado quando o pacote estiver instalado. Testa se o daemon cupsd está sendo executado quando o pacote é instalado. Testar se o getty daemon está sendo executado quando o pacote estiver instalado. Testar se o init daemon está sendo executado quando o pacote estiver instalado. Testar se o klogd daemon está sendo executado quando o pacote estiver instalado. Testar se o nmbd daemon está sendo executado quando o pacote estiver instalado. Testar se o smbd daemon está sendo executado quando o pacote estiver instalado. Testar se o syslogd daemon está sendo executado quando o pacote estiver instalado. Testar se o udevd daemon está sendo executado quando o pacote estiver instalado. Testar se o winbindd daemon está sendo executado quando o pacote estiver instalado. Teste interrompido Teste desconexão de CPUs em um sistema de núcleos múltiplos. Testa que o diretório /var/crash não contém nada. Lista os arquivos contidos em caso afirmativo, ou ecoa o status do diretório (não existe/está vazio) Testar se o X não está sendo executado no modo de segurança. Testar se o processo X está sendo executado Testar as capacidades de escalonamento da CPU usando a Suíte de Testes de Firmware (fwts cpufreq) Teste a rede após retornar. Teste para avaliar se a imagem na nuvem inicia e funciona adequadamente com KVM Teste para verificar que a virtualização é suportada e o sistema de teste tem pelo menos uma quantidade mínima de memória RAM para funcionar como um computador de nó OpenStack Teste para detectar dispositivos de áudio Teste para detectar os controladores de rede disponíveis Teste para detectar unidades ópticas Teste para determinar se este sistema é capaz de carregar máquinas virtuais KVM com aceleração por hardware Teste para mostrar a versão do Xorg Teste para verificar se podemos sincronizar o relógio local ao servidor NTP Veja se a resolução permanece a mesma de antes. Teste para verificar se o Xen Hypervisor está em execução. Testado Testa oem-config utilizando Xpresser, e então verifica se o usuário foi criado com sucesso. Exclui o novo usuário criado após o teste ser aprovado. Testa se o hardware de rede sem-fio do sistema é capaz de se conectar a um roteador, utilizando segurança WPA e os protocolos 802.11b/g, depois que o sistema tenha sido suspenso. Este teste irá verificar se o dispositivo sem fio pode se conectar a um roteador 802.11b/g usando o protocolo de segurança WPA. Testa se o hardware de rede sem-fio do sistema é capaz de se conectar a um roteador, utilizando segurança WPA e o protocolo 802.11n, depois que o sistema tenha sido suspenso. Este teste irá verificar se o dispositivo sem fio pode se conectar a um roteador 802.11n usando o protocolo de segurança WPA. Testa se o hardware de rede sem-fio do sistema é capaz de se conectar a um roteador, sem utilizar segurança e através dos protocolos 802.11b/g, depois que o sistema tenha sido suspenso. Este teste irá verificar se o dispositivo sem fio pode se conectar a um roteador 802.11b/g sem nenhum protocolo de segurança. Testa se o hardware de rede sem-fio do sistema é capaz de se conectar a um roteador, sem utilizar segurança e através do protocolo 802.11n, depois que o sistema tenha sido suspenso. Este teste irá verificar se o dispositivo sem fio pode se conectar a um roteador 802.11n sem nenhum protocolo de segurança. Testa o desempenho de uma conexão sem fio através da ferramenta iperf, utilizando pacotes UDP. Testa o desempenho de uma conexão sem fio através da ferramenta iperf. Testa se o apt pode acessar repositórios e obter atualizações (não instala as atualizações). Isto é feito para confirmar se o sistema pode recuperar-se de uma atualização incompleta ou quebrada. Testa se o sistema tem uma conexão de internet em funcionamento. Rótulo de texto O arquivo em que será gravado o registro. O relatório a seguir foi gerado para apresentação ao banco de dados de hardware Launchpad:

  [[%s|Ver relatório]]

Você pode enviar esta informação sobre seu sistema, fornecendo o endereço de e-mail que você usa para entrar no Launchpad. Se você não tiver uma conta no Launchpad, por favor registre-se aqui:

  https://launchpad.net/+login O relatório gerado parece ter erros de validação,
por isso não pode ser processado pelo Launchpad. Há outro Checkbox em execução. Por favor finalize-o primeiro. Este teste automatizado tentará detectar uma câmera Isto anexa imagens da tela do teste suspend/cycle_resolutions_after_suspend_auto à submissão dos resultados. Esta é uma versão totalmente automatizada de cartão de mídia/sd-automatizado e pressupõe que o sistema em teste possui um cartão de memória conectado antes da execução do checkbox. É destinado ao teste de SRU automatizado. Este é um teste automático de transferência de arquivos por Bluetooth. Ele envia uma imagem para o dispositivo especificado pela variável de ambiente BTDEVADDR. Esse é um teste automático de Bluetooth. Ele emula a navegação em um dispositivo remoto especificado pela variável de ambiente BTDEVADDR. Esse é um teste automático de Bluetooth. Ele recebe um dado arquivo de uma máquina remota especificada pela variável de ambiente BTDEVADDR. Este é um teste automatizado para recolher algumas informações sobre o estado atual de seus dispositivos de rede. Se nenhum dispositivo for encontrado, o teste vai reportar um erro. Este é um teste automatizado que realiza leitura/gravação em um disco FireWire Este é um teste automatizado que realiza leitura/gravação em um disco rígido eSATA conectado Esta é uma versão automatizada do usb/storage-automated e pressupõe que o servidor tem dispositivos de armazenamento USB conectados antes da execução do Checkbox. Ela é destinada a servidores e testes automatizados SRU. Esta é uma versão automatizada do USB3/armazenamento-automatizado e pressupõe que o servidor possua dispositivos de armazenamento USB 3.0 conectados antes da execução do checkbox. Ele é direcionado a servidores e ao teste automatizado de SRU. Esta á versão automática de suspend/suspend_advanced. Este teste verifica a topologia do processador para melhor precisão Este teste irá checar se os controladores de frequência da CPU são obedecidos quando definido. Este teste verifica se a interface sem fio está funcionando depois da suspensão do sistema. Ele desconecta todas as interfaces e então conecta à interface sem fio e verifica se a conexão está funcionando como o esperado. Este teste verifica a quantidade de memória que é relatada no meminfo contra o tamanho dos módulos de memória detectados por DMI. Este teste desconecta todas as conexões e conecta à interface sem-fio. Então ele verifica a conexão para confirmar que está funcionando como o esperado. Este teste coleta o endereço de hardware do adaptador bluetooth depois de suspender e compara com o endereço coletado antes da suspensão. Este teste é automatizado e é executado após o teste de detecção do cartão cf. Ele testa a leitura e a escrita do cartão CF. Este teste é automatizado e é executado após o teste de detecção do cartão pós-suspensão . Ele testa a leitura e a escrita para o cartão CF após o sistema ter sido suspenso. Este teste é automático e é executado após a finalização do teste de inserção de cartão. São testadas leitura e gravação no cartão MMC. Este teste é automatizado e é executado após o teste de detecção do cartão pós-suspensão . Ele testa a leitura e a escrita para o cartão MMC após o sistema ter sido suspenso. Este teste é automático e é executado depois do teste de inserção do cartão MS. Serão testados a leitura e a gravação no cartão MS. Este teste é automático e é executado depois do teste de inserção de cartão MS após o sistema ser suspenso. Serão testadas a leitura e a gravação do cartão MS após o sistema ser suspenso. Este teste é automatizado e é executado após o teste de insersão de cartão MSP. Ele testa a leitura e a gravação no cartão MSp. Este teste é automatizado e é executado após o teste de insersão de cartão MSP após uma suspensão. Ele testa a leitura e a gravação no cartão MSP depois que o sistema tenha sido suspenso. Este teste é automatizado e é executado após o teste de detecção do cartão sd. Ele testa a leitura e a escrita do cartão SD. Este teste é automatizado e é executado após o teste de detecção do cartão pós-suspensão . Ele testa a leitura e a escrita para o cartão SD após o sistema ter sido suspenso. Este teste é automatizado e é executado após o teste de detecção do cartão sdhc. Ele testa a leitura e a escrita do cartão SDHC. Este teste é automatizado e é executado após o teste de detecção do cartão pós-suspensão . Ele testa a leitura e a escrita para o cartão SDHC após o sistema ter sido suspenso. Este teste é automatizado e é iniciado depois que os testes de cartão de mídia/inserção de sdxc são executados. Ele testa a leitura e gravação em cartões SDXC. Este teste é automático e é executado depois do teste de inserção de um cartão SDXC após o sistema ser suspenso. Serão testadas a leitura e a gravação do cartão SDXC após o sistema ser suspenso. Este teste é automatizado e é executado após o teste de insersão de cartão xD. Ele testa a leitura e a gravação no cartão xD. Este teste é automatizado e é executado após o teste de insersão de cartão xD após uma suspensão. Ele testa a leitura e a gravação no cartão xD depois que o sistema tenha sido suspenso. Este teste é automatizado e é executado após o teste de inserção usb. Este teste é automatizado e executado depois que o teste de inserção da usb3 é acionado. Este teste irá verificar se os modos de vídeo suportados funcionam depois de suspender e continuar. Isto é feito automaticamente, fazendo capturas de tela e as enviando como um anexo. Este teste irá verificar se os níveis de volume estão em um nível aceitável no seu sistema local. O teste irá verificar se o volume é maior ou igual ao volume mínimo e menor ou igual ao volume máximo para todas as entradas e saídas reconhecidas pelo PulseAudio. Irá verificar também se a entrada e a saídas não estão sem som. Você não deve ajustar manualmente o volume ou deixar sem som antes de executar este teste. Isto anexará quaisquer relatórios do teste de gerenciamento de energia/desligamento aos resultados. Isto anexará quaisquer relatórios do teste de gerenciamento de energia/reinicialização aos resultados. Isto irá verificar se o seu dispositivo de áudio funciona corretamente ao retornar de uma suspensão. Isto pode funcionar bem com auto falantes e microfones integrados, entretanto, funciona melhor se usado com um cabo conectando a saída de áudio à entrada de áudio. Isto vai executar alguns testes de conectividade básicos contra um BMC, verificando se a IPMI funciona. Sinal de cronômetro do alarme(2) Testes de touchpad Testes de tela sensível ao toque Tente habilitar uma impressora remota na rede e imprima uma página de teste. Digite um texto DESCONHECIDO Teste da USB Testes de USB Sinal desconhecido Não testado Uso: checkbox [OPÇÕES] Aplicativos do usuário Sinal definido pelo usuário 1 Sinal definido pelo usuário 2 Verificar se a unidade de ponto flutuante vetorial está sendo executada no dispositivo ARM Verifica se o servidor DNS está em execução e funcionando. Verifica se o servidor de impressão/CUPs está em execução Verifica se o servidor Samba está em execução. Verifica se o servidor Tomcat está em execução e funcionando. Verifica se o sshd está em execução. Verifica se a pilha LAMP está em execução (Apache, MySQL e PHP). Verifica se o armazenamento externo USB3 funciona dentro da referência de desempenho ou abaixo dela Verifica a performance do sistema de armazenamento e realiza em ou acima de referência de desempenho Verifica que todas as CPUs estão conectadas depois de retomar. Verifique se toda memória está disponível após retornar da suspensão. Verifica que todas as CPUs estão conectadas antes de suspender Verifica se a instalação do servidor-checkbox na rede pode atingir mais de um SSH. Verifica que as configurações do mixador depois de suspender são as mesmas de antes de suspender. Verifica os dispositivos de armazenamento, tais como Fibre Channel e RAID podem ser detectados e trabalhar sob pressão. Ver os resultados Testes de virtualização Bem-vindo ao Sistema de testes!

O Checkbox fornece testes para confirmar que seu sistema está funcionando corretamente. Ao terminar a execução dos testes, você verá um relatório resumido do sistema. Teste de rede sem-fio Testes de rede sem fio Teste de varredura de redes sem fio. Detecta e relata pontos de acesso descobertos. Processando Você também pode me fechar pressionando ESC ou Ctrl+C. _Desmarcar tudo Sa_ir _Finalizar _Não Anterio_r _Selecionar todos _Pular este teste _Teste _Testar novamente _Sim e a capacidade também. anexar o conteúdo de vários arquivos de configuração sysctl. Teste do disco eSATA o esgotamento e a capacidade também. https://help.ubuntu.com/community/Installation/SystemRequirements instalar o tarball instalador do bootchart, se este existir. não pular teste testar novamente entrada tty para processos em segundo plano saída tty para processos em segundo plano até o esgotamento e a capacidade também. Requer que MOVIE_VAR seja definida. sim 