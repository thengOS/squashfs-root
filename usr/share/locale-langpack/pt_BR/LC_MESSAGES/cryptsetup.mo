��    �      �    L      �     �  �   �  �   �  <   ;     x     �     �     �     �     �  7   �     !  .   6     e  P   �     �  &   �  /   �  4   -  %   b  "   �  D   �  A   �  7   2  $   j  /   �  ,   �  !   �       &   ,  "   S  A   v     �  #   �     �  #     &   0  -   W  )   �     �  !   �  B   �  '   +     S  !   k  $   �     �     �     �  %   �  F   #  2   j     �     �  *   �     �  &        3     M     i  *   �  )   �     �     �  ,     2   4     g  #   ~     �     �     �  !   �  #     1   4  8   f     �  (   �     �  #   �  ,   "     O  *   i     �     �  '   �  *   �       !   <  ,   ^     �  ?   �  4   �  (      1   7      i      }       �   4   �      �   &   !  .   -!  %   \!  !   �!     �!  0   �!  8   �!  @   $"     e"     ~"  D   �"  #   �"     #     #  '   -#  (   U#     ~#  )   �#     �#     �#  (   �#     $$  )   D$  '   n$  0   �$  ,   �$  /   �$  9   $%  9   ^%  "   �%  .   �%  /   �%  6   &  )   Q&     {&  E   �&  6   �&  :   '  (   S'  &   |'     �'  @   �'     �'     (  '   /(  (   W(  >   �(     �(     �(     �(  )   �(  3   ))  '   ])  '   �)     �)     �)     �)  8   �)     #*     7*  "   N*  /   q*  6   �*  �   �*  6   c+  >   �+     �+     �+  &   ,  M   6,  5   �,  6   �,  2   �,  +   $-  6   P-     �-  +   �-  3   �-     �-     .  (   /.     X.     v.  *   �.  +   �.  .   �.     /      0/     Q/     k/     �/  &   �/     �/  !   �/     0  N   $0     s0  /   y0  O   �0     �0     1  ,   /1     \1     b1  1   |1     �1     �1     �1  (   �1     2  1   #2  �  U2     4  �   .4  �   5  V   �5      ,6     M6     [6     y6  $   �6     �6  ?   �6      7  :    7     [7  g   u7     �7  9   �7  =   8  J   Z8  5   �8  4   �8  g   9  b   x9  E   �9  ?   !:  D   a:  ?   �:  ?   �:  3   &;  7   Z;  0   �;  Y   �;  +   <  M   I<  )   �<  K   �<  8   =  E   F=  J   �=  *   �=  4   >  U   7>  =   �>  )   �>  7   �>  <   -?     j?     �?  (   �?  *   �?  n   �?  >   l@     �@     �@  2   �@  !   A  6   0A  !   gA  #   �A  !   �A  6   �A  9   B     @B      PB  5   qB  H   �B      �B  &   C  !   8C     ZC  "   tC      �C  "   �C  A   �C  L   D  "   jD  )   �D     �D  %   �D  D   �D  !   >E  B   `E  !   �E  "   �E  *   �E  D   F  !   XF  ,   zF  <   �F     �F  C   �F  &   ;G  3   bG  ?   �G     �G     �G  1   H  >   @H  &   H  .   �H  :   �H  2   I  #   CI     gI  1   �I  9   �I  O   �I     <J     TJ  E   mJ  *   �J     �J     �J  .   K  0   BK     sK  *   �K     �K  *   �K  B   �K  +   AL  2   mL  0   �L  D   �L  <   M  @   SM  F   �M  >   �M  +   N  >   FN  5   �N  <   �N  -   �N  &   &O  N   MO  9   �O  ;   �O  0   P  ,   CP  #   pP  G   �P     �P     �P  6   Q  A   KQ  N   �Q     �Q     �Q      	R  4   *R  M   _R  9   �R  1   �R  "   S     <S     VS  B   ^S  #   �S     �S  )   �S  E   T  K   ST  �   �T  >   DU  N   �U     �U  #   �U  )   V  _   9V  I   �V  A   �V  <   %W  9   bW  :   �W     �W  .   �W  -   $X  *   RX     }X  ?   �X      �X  (   �X  -   Y  .   JY  1   yY     �Y     �Y     �Y     Z  .   !Z  .   PZ  %   Z  2   �Z  #   �Z  e   �Z     b[  =   h[  a   �[  7   \     @\  4   \\     �\     �\  B   �\      ]      ]     %]  1   C]  #   u]  :   �]             U   �           )   �       �      �   z       �   �   Z   �   ^   �       �   �   �              +       u   g   W   �   �   �   �   �   K           %   �      M   "              �   q   �       �       Y   �   �   �       4   }   �   �   V       y       �   $           =   k   ?   @   �   �   H   �      �       >   e      �   N   a   �                         �              l   �   �       �   J   �      8          �   T   \   �   .   �       �   [   /   {       �   �   p   C   �      �   b   9   `   �      �   �   �   A       I   v   �      ]   x                       #      �   5   1   �   �          o               �             �   2   �   w   S   6      i   �                 �       B   c       Q   R      �   '   h   �   j   F   &           �       ~   �   O   �   :   �   f   d           r       L   �   �   _   (       G   s   �   ,   �   �      �   
   -       �   3   �   |       �   n   �          �                                  E   �   �   �   �   X   P      ;   7      *              !       m   �   �   �   �      <   t                   �   	   0   D    
<action> is one of:
 
<name> is the device to create under %s
<device> is the encrypted device
<key slot> is the LUKS key slot number to modify
<key file> optional key file for the new key for luksAddKey action
 
Default compiled-in device cipher parameters:
	loop-AES: %s, Key %d bits
	plain: %s, Key: %d bits, Password hashing: %s
	LUKS1: %s, Key: %d bits, LUKS header hashing: %s, RNG: %s
 
WARNING: real device header has different UUID than backup! %s: requires %s as arguments <device> <device> <key slot> <device> [<key file>] <device> [<new key file>] <name> Align payload at <n> sector boundaries - for luksFormat All key slots full.
 Allow discards (aka TRIM) requests for device. Argument <action> missing. Attaching loopback device failed (loop device with autoclear flag is required).
 BITS Backup LUKS device header and keyslots Backup file doesn't contain valid LUKS header.
 Can't do passphrase verification on non-tty inputs.
 Can't format LOOPAES without device.
 Can't format LUKS without device.
 Cannot add key slot, all slots disabled and no volume key provided.
 Cannot create LUKS header: header digest failed (using hash %s).
 Cannot create LUKS header: reading random salt failed.
 Cannot find a free loopback device.
 Cannot format device %s which is still in use.
 Cannot format device %s, permission denied.
 Cannot get info about device %s.
 Cannot get process priority.
 Cannot initialize crypto RNG backend.
 Cannot initialize crypto backend.
 Cannot initialize device-mapper. Is dm_mod kernel module loaded?
 Cannot open device %s.
 Cannot open header backup file %s.
 Cannot read device %s.
 Cannot read header backup file %s.
 Cannot read requested amount of data.
 Cannot retrieve volume key for plain device.
 Cannot seek to requested keyfile offset.
 Cannot unlock memory.
 Cannot use %s as on-disk header.
 Cannot use device %s which is in use (already mapped or mounted).
 Cannot use offset with terminal input.
 Cannot wipe device %s.
 Cannot wipe header on device %s.
 Cannot write header backup file %s.
 Command failed with code %i Command successful.
 Create a readonly mapping DM-UUID for device %s was truncated.
 Data offset or key size differs on device and backup, restore failed.
 Detected not yet supported GPG encrypted keyfile.
 Device %s %s%s Device %s already exists.
 Device %s doesn't exist or access denied.
 Device %s has zero size.
 Device %s is not a valid LUKS device.
 Device %s is not active.
 Device %s is still in use.
 Device %s is too small.
 Device or file with separated LUKS header. Device type is not properly initialised.
 Display brief usage Do not ask for confirmation Do you really want to change UUID of device? Dump volume (master) key instead of keyslots info. Enter any passphrase:  Enter new passphrase for key slot:  Enter passphrase for %s:  Enter passphrase:  Error %d reading from RNG: %s
 Error during resuming device %s.
 Error during suspending device %s.
 Error during update of LUKS header on device %s.
 Error re-reading LUKS header after update on device %s.
 Error reading keyfile %s.
 Error reading passphrase from terminal.
 Error reading passphrase.
 FIPS checksum verification failed.
 Failed to access temporary keystore device.
 Failed to open key file.
 Failed to open temporary keystore device.
 Failed to stat key file.
 Failed to swap new key slot.
 Fatal error during RNG initialisation.
 File with LUKS header and keyslots backup. Generating key (%d%% done).
 Hash algorithm %s not supported.
 Header detected but device %s is too small.
 Help options: How many sectors of the encrypted data to skip at the beginning How often the input of the passphrase can be retried Incompatible loop-AES keyfile detected.
 Incorrect volume key specified for plain device.
 Invalid device %s.
 Invalid key size.
 Invalid plain crypt parameters.
 Kernel doesn't support loop-AES compatible mapping.
 Key %d not active. Can't wipe.
 Key processing error (using hash %s).
 Key size in XTS mode must be 256 or 512 bits.
 Key size must be a multiple of 8 bits Key slot %d active, purge first.
 Key slot %d changed.
 Key slot %d is full, please select another one.
 Key slot %d is invalid, please select between 0 and %d.
 Key slot %d is invalid, please select keyslot between 0 and %d.
 Key slot %d is invalid.
 Key slot %d is not used.
 Key slot %d material includes too few stripes. Header manipulation?
 Key slot %d selected for deletion.
 Key slot %d unlocked.
 Key slot is invalid. Keyslot %i: bogus partition signature.
 Keyslot %i: offset repaired (%u -> %u).
 Keyslot %i: salt wiped.
 Keyslot %i: stripes repaired (%u -> %u).
 LUKS keyslot %u is invalid.
 Limits the read from keyfile Limits the read from newly added keyfile Maximum keyfile size exceeded.
 Negative number for option not permitted. No key available with this passphrase.
 No known cipher specification pattern detected.
 No known problems detected for LUKS header.
 Non standard key size, manual repair required.
 Non standard keyslots alignment, manual repair required.
 Not compatible PBKDF2 options (using hash algorithm %s).
 Number of bytes to skip in keyfile Number of bytes to skip in newly added keyfile Only one of --use-[u]random options is allowed. Option --align-payload is allowed only for luksFormat. Option --header-backup-file is required.
 Option --key-file is required.
 Option --key-file takes precedence over specified key file argument.
 Option --use-[u]random is allowed only for luksFormat. Option --uuid is allowed only for luksFormat and luksUUID. Out of memory while reading passphrase.
 PBKDF2 iteration time for LUKS (in ms) Passphrases do not match.
 Please use gpg --decrypt <KEYFILE> | cryptsetup --keyfile=- ...
 Print package version Read the key from a file. Read the volume (master) key from file. Really try to repair LUKS device header? Reduced data offset is allowed only for detached LUKS header.
 Repair failed. Repairing keyslots.
 Replaced with key slot %d.
 Requested LUKS hash %s is not supported.
 Requested offset is beyond real size of device %s.
 Restore LUKS device header and keyslots Resume is not supported for device %s.
 Resume suspended LUKS device. Running in FIPS mode.
 SECTORS Share device with another non-overlapping crypt segment. Show debug messages Show this help message Shows more detailed error messages Slot number for new key (default is first free) Suspend LUKS device and wipe key (all IOs are frozen). System is out of entropy while generating volume key.
Please move mouse or type some text in another window to gather some random events.
 The cipher used to encrypt the disk (see /proc/crypto) The hash used to create the encryption key from the passphrase The size of the device The size of the encryption key The start offset in the backend device This is the last keyslot. Device will become unusable after purging this key. This operation is not supported for %s crypt device.
 This operation is not supported for this device type.
 This operation is supported only for LUKS device.
 This will overwrite data on %s irrevocably. Timeout for interactive passphrase prompt (in seconds) UUID for device to use. UUID is not supported for this crypt type.
 Underlying device for crypt device %s disappeared.
 Unknown RNG quality requested.
 Unknown action. Unknown crypt device type %s requested.
 Unsupported LUKS version %d.
 Unsupported VERITY block size.
 Use /dev/random for generating volume key. Use /dev/urandom for generating volume key. Verifies the passphrase by asking for it twice Verify passphrase:  Volume %s is already suspended.
 Volume %s is not active.
 Volume %s is not suspended.
 Volume key buffer too small.
 Volume key does not match the volume.
 Writing LUKS header to disk.
 Wrong LUKS UUID format provided.
 add key to LUKS device already contains LUKS header. Replacing header will destroy existing keyslots. bytes changes supplied key or key file of LUKS device does not contain LUKS header. Replacing header can destroy data on that device. dump LUKS partition information formats a LUKS device memory allocation error in action_luksFormat msecs print UUID of LUKS device removes supplied key or key file from LUKS device resize active device secs show device status tests <device> for LUKS partition header try to repair on-disk metadata wipes key with number <key slot> from LUKS device Project-Id-Version: cryptsetup
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-08-10 16:19+0200
PO-Revision-Date: 2014-02-16 23:19+0000
Last-Translator: Juliano Fischer Naves <julianofischer@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:25+0000
X-Generator: Launchpad (build 18115)
 
<action> é um de:
 
<nome> é o dispositivo criado sob %s
<dispositivo> é um dispositivo criptografado
<porta> é o número da porta LUKS a ser modificada
<arquivo chave> arquivo chave opcional para a nova chave na ação luksAddKey
 
Parâmetros padrão compilados de cifragem do dispositivo:
	loop-AES: %s, Chave %d bits
	plano: %s, Chave: %d bits, Hashing de senha: %s
	LUKS1: %s, Chave: %d bits, Hashing de cabeçalho LUKS: %s, RNG: %s
 
ATENÇÃO: cabeçalho do dispositivo real tem UUID diferente da cópia de segurança! %s: precisa de %s como argumento <dispositivo> <dispositivo> <espaço chave> <device> [<arquivo chave>] <dispositivo> [<novo arquivo chave>] <nome> Alinhar a carga útil em <n> sectores limites - para luksFormat Todas as portas estão cheias.
 Permitir descartes (aka TRIM) requisitados do dispositivo. Falta argumento <action>. Falha ao anexar o dispositivo de loopback (dispositivo de loop com flag de autoclear é obrigatório).
 BITS Backup do cabeçalho LUKS e espaço-chave do dispositivo. O arquivo de backup não contém um cabeçalho LUKS válido.
 Não é possível verificar frase secreta em entradas que não sejam tty.
 Não foi possível formatar LOOPAES sem dispositivo.
 Não é possível formatar LUKS sem um disposivito.
 Não é possível adicionar portas, todos as vagas desabilitadas e não foi fornecido chave de volume.
 Não é possível criar cabeçalho LUKS: sumário do cabeçalho falhou (usando encadeiamento %s).
 Não foi possível criar cabeçalho LUKS: leitura aleatória falhou.
 Não foi possível localizar um dispositivo de loopback livre.
 Não é possível formatar o dispositivo %s que ainda está em uso.
 Não foi possível formatar dispositivo %s, permissão negada.
 Não foi possível obter informações sobre o dispositivo %s.
 Não foi possível obter a prioridade do processo.
 Não foi possivel inicializar o backend do crypto RNG.
 Não é possível inicializar mecanismo crypto.
 Não é possível inicializar device-mapper. O módulo do kernel dm_mod está carregado?
 Não foi possível abrir o dispositivo %s.
 Não foi possível abrir o cabeçalho do arquivo de cópia de segurança %s.
 Não foi possível ler o dispositivo %s.
 Não foi possível ler o cabeçalho do arquivo de cópia de segurança %s.
 Não é possível ler a quantidade de dados solicitada.
 Não é possível resgatar chave de volume para dispositivo simples.
 Não é possível procurar pelo deslocamento do arquivo-chave solicitado.
 Não é possível desbloquear a memória.
 Não é possível usar %s como cabeçalho em-disco.
 Não é possível usar o dispositivo %s, pois está em uso (já mapeado ou montado).
 Não é possível usar deslocamento com entrada de terminal.
 Não é possível limpar dispositivo %s.
 Não é possível limpar cabeçalho no dispositivo %s.
 Não é possível escrever cabeçalho de arquivo backup %s.
 Falha no comando com código %i Comando excutado com sucesso.
 Criar um mapeamento para somente leitura DM-UUID para dispositivo %s foi truncado.
 Intervalo de dados ou tamanho de chave difere entre dispositivo e cópia de segurança, restauração falhou.
 Ainda não foi detectado suporte a chaves criptografadas GPG.
 Dispositivo %s %s%s Dispositivo %s já existe.
 Dispositivo %s não existe ou acesso é restrito.
 Dispositivo %s tem tamanho zero.
 O dispositivo %s não é um dispositivo LUKS válido.
 Dispositivo %s não está ativo.
 Dispositivo %s ainda está em uso.
 Dispositivo %s é muito pequeno.
 Dispositivo ou arquivo com o cabeçalho LUKS separado. Tipo de dispositivo não está devidamente inicializado.
 Exibe uso breve Não perguntar por confirmação Você realmente deseja alterar o UUID do dispositivo? Despejar chave (principal) de volume em vez de informação de keyslots. Informe qualquer frase secreta:  Digite nova frase secreta para porta:  Informe a frase secreta para %s:  Informe a frase secreta:  Erro %d lendo a partir de RNG: %s
 Erro ao resumir dispositivo %s.
 Erro ao suspender dispositivo %s.
 Erro durante atualização do cabeçalho LUKS no dispositivo %s.
 Erro na releitura do cabeçalho LUKS após atualização no dispositivo %s.
 Erro ao ler arquivo de chaves %s.
 Erro ao ler a frase secreta do terminal.
 Erro ao ler frase secreta.
 Falha na soma de verificação FIPS.
 Falha ao acessar dispositivo de armazenamento de chave temporária.
 Falha ao abrir arquivo de chave.
 Falha ao abrir dispositivo de armazenamento de chave temporária.
 Falhou no stat do arquivo chave.
 Falhou ao trocar novo slot chave.
 Erro fatal durante a inicialização RNG.
 Arquivo com cópia de segurança de cabeçalho LUKS e espaço-chave. Gerando chave (%d%% concluído).
 O algoritmo %s para hash não é suportado.
 Cabeçalho detectado mas o dispositivo %s é muito pequeno.
 Opções de ajuda: Quantos setores de dados encriptados a serem preservados ao início Quantas vezes a frase pode ser tentada Loop incompatível de arquivo-chave AES detectado.
 Chave de volume especificada incorreta para dispositivo plano.
 Dispositivo %s inválido.
 Tamanho da chave inválido.
 Parâmetros do plano de criptografia inválidos.
 O kernel não suporta um mapeamento compatível com loop-AES.
 Chave %d inativa. Impossível limpar.
 Erro ao processar a chave (usando o hash %s).
 Tamanho da chave no modo XTS deve ser de 256 ou 512 bits.
 O tamanho da chave deve ser um múltiplo de 8 bits Porta %d ativa, descarte primeiro.
 Slot chave %d alterado.
 Porta %d está cheia, por favor selecione outra.
 Porta %d é inválida, por favor selecione entre 0 e %d.
 Espaço chave %d é inválido, por favor selecione espaço-chave entre 0 e %d.
 Porta %d é inválida.
 Porta %d não é usada.
 Porta %d material inclui poucas faixas. Manipulação de cabeçalho?
 Slot chave %d selecionado para exclusão.
 Porta %d desbloqueada.
 Local da chave é inválido. Fechadura %i: assinatura de partição falsa.
 Fechadura %i: deslocamento reparado (%u -> %u).
 Fechadura %i: limpa.
 Fechadura %i: listra reparada (%u -> %u).
 keyslot LUKS %u é inválido.
 Limita a leitura a partir do arquivo chave Limita a leitura a partir do arquivo chave adicionado recentemente Tamanho máximo do arquivo-chave excedido.
 Número negativo para a opção não é permitida. Chave não disponível para esta frase secreta.
 Nenhuma especificação padrão de criptograma conhecido detectado.
 Nenhum problema conhecido foi detectado no cabeçalho LUKS.
 Chave de tamanho fora do padrão, reparação manual requerida.
 Alinhamento de fechaduras fora do padrão, necessário reparo manual.
 Opções PBKDF2 incompatíveis (usando algoritmo de hash %s).
 Número de bytes a ignorar no arquivo-chave Número de bytes a ignorar no arquivo-chave recém-adicionado. Apenas uma das opções --use-[u]random é permitida. Opção --align-payload é permitida apenas para luksFormat. Opção --header-backup-file é necessária.
 A opção --key-file é obrigatória.
 A opção --key-file tem precedência sobre o arquivo de chaves especificado.
 Opção --use-[u]random só é permitida para luksFormat. Opção --uuid só é permitida para luksFormat e luksUUID. Falta de memória enquanto lia a frase secreta.
 PBKDF2 tempo de iteração para LUKS (em ms) As frases secretas não coincidem.
 Por favor utilize gpg --decrypt <KEYFILE> | cryptsetup --keyfile=- ...
 Visualizar versão do pacote Ler a chave de um arquivo. Lê a chave (master) do volume a partir de um arquivo. Deseja realmente tentar reparar o cabeçalho de dispositivo LUKS? Offset de dados reduzido é permitido apenas para cabeçalho LUKS desanexado.
 Falha na reparação. Reparando fechaduras.
 Substituído com slot chave %d.
 Encadeiamento LUKS %s solicidado não é suportado.
 Solicitação de deslocamento está além do tamanho real do dispositivo %s.
 Restaura cabeçalhos LUKS e espaço-chave do dispositivo. Resumir não é suportado para o dispositivo %s.
 Retoma dispositivos LUKS suspenso. Executando no modo FIPS.
 SETORES Compartilha o dispositivo com outro segmento crypt não sobreposto Mostrar as mensagens de depuração Mostra esta mensagem de ajuda Mostrar mensagens de erro mais detalhadas Nero da posição para uma nova chave (o padrão é a primeira livre) Suspenda dispositivo LUKS e limpe a chave (todos as ESs estão congeladas). Sistema está fora de entropia ao gerar chave de volume.
Por favor, mova o mouse ou digitar algum texto em uma outra janela para reunir alguns eventos aleatórios.
 A cifragem usada para criptografar o disco (veja /proc/crypto) O hash utilizado para criar a chave de encriptação a partir da frase secreta O tamanho do dispositivo O tamanho da chave de encriptação O início do deslocamento num dispositivo Esse é o último espaço-chave. O dispositivo será inutilizado após a remoção dessa chave. Esta operação não é suportada para o dispositivo de criptografia %s.
 Esta operação não é suportada para este tipo de dispositivo.
 Esta operação é suportada somente por dispositivos LUKS.
 Isto irá sobrescrever os arquivos em %s definitivamente. Tempo de espera para prompt de frase secreta (em segundos) UUID para usar o dispositivo. UUID não suportado para este tipo de cripta.
 O dispositivo %s para encriptar desapareceu.
 Qualidade desconhecida do RNG solicitado.
 Ação desconhecida. Tipo de dispositivo encriptado %s requisitado é desconhecido.
 Versão LUKS %d não suportada.
 Tamanho de bloco VERITY não suportado.
 Use /dev/random para gerar a chave do volume. Use /dev/urandom para gerar a chave do volume. Verifica a frase secreta perguntando-a duas vezes Verificar frase secreta:  Volume %s já está suspenso.
 Volume %s não está ativo.
 Volume %s não está suspenso.
 Acumulador de chaves de volume muito pequeno.
 Chaves de volume não correspondem ao volume.
 Gravando o cabeçalho LUKS no disco.
 O formato de LUKS UUIS fornecido está incorreto.
 adicionar chave ao dispositivo LUKS já contém um cabeçalho LUKS. Substituindo o cabeçalho irá destruir os espaço-chaves existentes. bytes altera a chave fornecida ou arquivo chave do dispositivo LUKS não contém um cabeçalho LUKS. Substituindo o cabeçalho pode destruir os dados no dispositivo. realiza um dump da informação dobre a partição LUKS formata um dispositivo LUKS falha na alocação de memória em action_luksFormat milisegundos exibe UUID do dispositivo LUKS remove a chave fornecida ou o arquivo de chave do dispositivo LUKS redimensionar dispositivo ativo segs mostrar status do dispositivo testa <device> para cabeçalho de partição LUKS tente reparar os metadados no disco lipa chave com número <espaço chave> do dispositivo LUKS 