��          |      �             !     5  -   E  '   s  3   �  ,   �     �  �        �     �     �  �       �     �  1   �  "   #  G   F  -   �     �  �   �     �     �     �                            
      	                         Current folder name New folder name Note that existing content will not be moved. There was an error updating the folders Update common folders names to match current locale Update standard folders to current language? User folders update You have logged in in a new language. You can automatically update the names of some standard folders in your home folder to match this language. The update would change the following folders: _Don't ask me this again _Keep Old Names _Update Names Project-Id-Version: xdg-user-dirs-gtk
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-04 03:00+0000
PO-Revision-Date: 2015-12-04 23:44+0000
Last-Translator: Fábio Nogueira <fnogueira@protonmail.com>
Language-Team: Brazilian Portuguese <gnome-l10n-br@listas.cipsga.org.br>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:22+0000
X-Generator: Launchpad (build 18115)
 Nome de pasta atual Novo nome da pasta Note que o conteúdo existente não será movido. Houve um erro ao atualizar a pasta Atualiza o nome de pastas comuns para se adequar à localização atual Atualizar pastas padrão para o idioma atual? Atualizar pastas de usuário Você iniciou uma sessão num novo idioma. Você deseja atualizar automaticamente o nome de algumas pastas padrão em sua pasta pessoal para se adequarem a esse idioma. A atualização alteraria as seguintes pastas: _Não me perguntar novamente _Manter nomes antigos At_ualizar nomes 