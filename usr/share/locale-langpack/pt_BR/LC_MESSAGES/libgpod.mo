��    �      �  �   �      �  *   �      �     �  	     *     0   8     i     q     �     �     �  /   �     �  ,   �  A        \      z  %   �  "   �  $   �  H   	      R  J   s  .   �  "   �       '   -     U  $   r  $   �  (   �  O   �  I   5  #        �  !   �     �  #   �  !   !  *   C  +   n  #   �  *   �     �               8     V  5   u  .   �     �  	   �       #        5  5   N  /   �     �     �  U   �  I   1  	   {     �     �     �     �     �     �     �  
   �     �     �       -   &     T     d     t     �     �     �     �     �  
   �     �     �     �     �          "     8  #   T  4   x  3   �  +   �  9     9   G     �     �  =   �  D   �  F   !  "   h     �     �     �  .   �  D   �  E   )     o     x  .   �     �     �     �     �     �               *     =     P     `     o     ~     �     �     �     �     �     �  *   �          #     4     E     V  b   e  X   �  .   !       P      q      �   9   �      �   :   �   �   !  N   "  ?   a"  1  �"     �#     �#     �#     $     $  8   $  (   S$  C   |$  �   �$  Q   J%     �%     �%  
   �%     �%     �%  	   �%  
   �%     �%     �%  .   �%  (   #&  Y   L&  N   �&  ,   �&  T   "'  =   w'  5   �'  5   �'  H   !(  \   j(  R   �(  ^   )  D   y)  D   �)     *  0   "*  �  S*  ,   ,  !   ;,     ],  
   l,  2   w,  S   �,  	   �,     -     -     ,-     0-  :   <-  *   w-  @   �-  a   �-  +   E.  2   q.  =   �.  (   �.  .   /  F   :/  (   �/  V   �/  @   0  %   B0     h0  ,   �0     �0  $   �0  '   �0  2   1  X   O1  R   �1  -   �1     )2  $   C2     h2     �2     �2  -   �2  /   �2  $   3  ,   >3     k3     �3     �3  *   �3  +   �3  F   4  :   _4  #   �4     �4     �4  .   �4     5  C   +5  9   o5  	   �5  (   �5  �   �5  ]   e6  	   �6     �6     �6     �6     7     7     7     +7  
   87     C7  #   W7  !   {7  5   �7     �7     �7     �7     
8     8     #8     28     >8     J8     Z8     g8     u8     �8     �8     �8      �8  +   �8  Q   9  ?   q9  2   �9  >   �9  >   #:     b:  '   |:  Z   �:  Q   �:  `   Q;  +   �;     �;     �;     <  7   <  P   N<  D   �<     �<     �<  5   =     9=     Q=     i=     �=  
   �=     �=     �=     �=     �=     >     '>     9>     N>     a>     v>     �>     �>     �>     �>  4   �>     ?     ?     '?     =?     N?  �   \?  a   �?  5   D@  $   z@     �@     �@  J   �@     A  c   /A  1  �A  p   �B  [   6C  �  �C     E     /E     FE     UE  	   eE  F   oE  ?   �E  V   �E  �   MF  h   �F     DG     IG  
   PG     [G     jG  	   zG  
   �G     �G     �G  4   �G  '   �G  v   �G  q   qH  /   �H  i   I  E   }I  G   �I  G   J  X   SJ  f   �J  [   K  x   oK  N   �K  L   7L  &   �L  G   �L     *       H   +       �       u      6      �   Q           1       �       �   w   �   -      �   !   l   �       t   ^       �                      �   9   i          b   D   �   �   V   �      [   �   ,       s   Z   ~   C          �   �   G       �   P                  �       L   e   �       �   &   R       �   �       d       |   S   �      j   �   �   U       5   �   �   $       M      ;      �       �   K   �       W      %   �      �   E           a   �       `   �   =      '               �       �   �   �   3          T   �       �   x       #       ?   �      "   �       (   �   Y   q   )       �   \                     �   .   �       /   	           B   z       f   }           4   {       �          
   k       X   8   g             �   o   >       :      �          �   �       m   �       O   v   n      �   ]      �   �   �   A   y   p       0       _          �   @   2   c   �           �   r   <          h       J      I   7   N      �   F    %s: Unexpected length %d for genius_cuid!
 '%s' could not be accessed (%s). <No members>
 <Unnamed> Artwork support not compiled into libgpod. Cannot remove Photo Library playlist. Aborting.
 Classic Classic (Black) Classic (Silver) Color Color U2 Control directory not found: '%s' (or similar). Could not access file '%s'. Could not access file '%s'. Photo not added. Could not find corresponding track (dbid: %s) for artwork entry.
 Could not find on iPod: '%s'
 Could not open '%s' for writing. Couldn't find an iPod database on %s. Couldn't read xml sysinfo from %s
 Couldn't write SysInfoExtended to %s Destination file '%s' does not appear to be on the iPod mounted at '%s'. Device directory does not exist. Encountered unknown MHOD type (%d) while parsing the iTunesDB. Ignoring.

 Error adding photo (%s) to photo database: %s
 Error compressing iTunesCDB file!
 Error creating '%s' (mkdir)
 Error initialising iPod, unknown error
 Error initialising iPod: %s
 Error opening '%s' for reading (%s). Error opening '%s' for writing (%s). Error reading iPod photo database (%s).
 Error reading iPod photo database (%s).
Will attempt to create a new database.
 Error reading iPod photo database, will attempt to create a new database
 Error reading iPod photo database.
 Error removing '%s' (%s). Error renaming '%s' to '%s' (%s). Error when closing '%s' (%s). Error while reading from '%s' (%s). Error while writing to '%s' (%s). Error writing list of albums (mhsd type 4) Error writing list of artists (mhsd type 8) Error writing list of tracks (hths) Error writing list of tracks (mhsd type 1) Error writing mhsd type 10 Error writing mhsd type 6 Error writing mhsd type 9 Error writing mhsd5 playlists Error writing playlists (hphs) Error writing special podcast playlists (mhsd type 3) Error writing standard playlists (mhsd type 2) Error: '%s' is not a directory
 Grayscale Grayscale U2 Header is too small for iTunesCDB!
 Illegal filename: '%s'.
 Illegal seek to offset %ld (length %ld) in file '%s'. Insufficient number of command line arguments.
 Invalid Itdb_Track ID '%d' not found.
 Length of smart playlist rule field (%d) not as expected. Trying to continue anyhow.
 Library compiled without gdk-pixbuf support. Picture support is disabled. Master-PL Mini (1st Gen.) Mini (2nd Gen.) Mini (Blue) Mini (Gold) Mini (Green) Mini (Pink) Mini (Silver) Mobile (1) Mobile Phones Mountpoint not set. Mountpoint not set.
 Music directory not found: '%s' (or similar). Nano (1st Gen.) Nano (2nd Gen.) Nano (Black) Nano (Blue) Nano (Green) Nano (Orange) Nano (Pink) Nano (Purple) Nano (Red) Nano (Silver) Nano (White) Nano (Yellow) Nano Video (3rd Gen.) Nano Video (4th Gen.) Nano touch (6th Gen.) Nano with camera (5th Gen.) No 'F..' directories found in '%s'. Not a OTG playlist file: '%s' (missing mhpo header). Not a Play Counts file: '%s' (missing mhdp header). Not a iTunesDB: '%s' (missing mhdb header). Number of MHODs in mhip at %ld inconsistent in file '%s'. Number of MHODs in mhyp at %ld inconsistent in file '%s'. OTG Playlist OTG Playlist %d OTG playlist file '%s': reference to non-existent track (%d). OTG playlist file ('%s'): entry length smaller than expected (%d<4). OTG playlist file ('%s'): header length smaller than expected (%d<20). Path not found: '%s' (or similar). Path not found: '%s'. Photo Photo Library Photos directory not found: '%s' (or similar). Play Counts file ('%s'): entry length smaller than expected (%d<12). Play Counts file ('%s'): header length smaller than expected (%d<96). Playlist Podcasts Problem creating iPod directory or file: '%s'. Regular (1st Gen.) Regular (2nd Gen.) Regular (3rd Gen.) Regular (4th Gen.) Shuffle Shuffle (1st Gen.) Shuffle (2nd Gen.) Shuffle (3rd Gen.) Shuffle (4th Gen.) Shuffle (Black) Shuffle (Blue) Shuffle (Gold) Shuffle (Green) Shuffle (Orange) Shuffle (Pink) Shuffle (Purple) Shuffle (Red) Shuffle (Silver) Shuffle (Stainless) Specified album '%s' not found. Aborting.
 Touch Touch (2nd Gen.) Touch (3rd Gen.) Touch (4th Gen.) Touch (Silver) Unable to retrieve thumbnail (appears to be on iPod, but no image info available): filename: '%s'
 Unexpected error in itdb_photodb_add_photo_internal() while adding photo, please report. Unexpected image type in mhni: %d, offset: %d
 Unexpected mhod string type: %d
 Unexpected mhsd index: %d
 Unknown Unknown action (0x%x) in smart playlist will be ignored.
 Unknown command '%s'
 Unknown smart rule action at %ld: %x. Trying to continue.
 Usage to add photos:
  %s add <mountpoint> <albumname> [<filename(s)>]
  <albumname> should be set to 'NULL' to add photos to the master photo album
  (Photo Library) only. If you don't specify any filenames an empty album will
  be created.
 Usage to dump all photos to <output_dir>:
  %s dump <mountpoint> <output_dir>
 Usage to list all photos IDs to stdout:
  %s list <mountpoint>
 Usage to remove photo IDs from Photo Library:
  %s remove <mountpoint> <albumname> [<ID(s)>]
  <albumname> should be set to 'NULL' to remove photos from the iPod
  altogether. If you don't specify any IDs, the photoalbum will be removed
  instead.
  WARNING: IDs may change when writing the PhotoDB file.
 Video (1st Gen.) Video (2nd Gen.) Video (Black) Video (White) Video U2 Warning: could not find photo with ID <%d>. Skipping...
 Wrong number of command line arguments.
 You need to specify the iPod model used before photos can be added. Your iPod does not seem to support photos. Maybe you need to specify the correct iPod model number? It is currently set to 'x%s' (%s/%s). header length of '%s' smaller than expected (%d < %d) at offset %ld in file '%s'. iPad iPhone iPhone (1) iPhone (Black) iPhone (White) iPhone 3G iPhone 3GS iPhone 4 iPod iTunes directory not found: '%s' (or similar). iTunesCDB '%s' could not be decompressed iTunesDB '%s' corrupt: Could not find playlists (no mhsd type 2 or type 3 sections found) iTunesDB '%s' corrupt: Could not find tracklist (no mhsd type 1 section found) iTunesDB '%s' corrupt: mhsd expected at %ld. iTunesDB ('%s'): header length of mhsd hunk smaller than expected (%d<32). Aborting. iTunesDB corrupt: hunk length 0 for hunk at %ld in file '%s'. iTunesDB corrupt: no MHOD at offset %ld in file '%s'. iTunesDB corrupt: no SLst at offset %ld in file '%s'. iTunesDB corrupt: no section '%s' found in section '%s' starting at %ld. iTunesDB corrupt: number of mhip sections inconsistent in mhyp starting at %ld in file '%s'. iTunesDB corrupt: number of tracks (mhit hunks) inconsistent. Trying to continue.
 iTunesDB possibly corrupt: number of playlists (mhyp hunks) inconsistent. Trying to continue.
 iTunesStats file ('%s'): entry length smaller than expected (%d<18). iTunesStats file ('%s'): entry length smaller than expected (%d<32). usage: %s <device> [timezone]
 usage: %s <device|uuid|bus device> <mountpoint>
 Project-Id-Version: libgpod
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-04-13 08:40+0000
PO-Revision-Date: 2011-12-23 21:01+0000
Last-Translator: gabriell nascimento <gabriellhrn@gmail.com>
Language-Team: Portuguese (Brazil) <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 %s: Tamanho inesperado %d para genius_cuid!
 '%s' não pode ser acessado (%s). <Sem membros>
 <Sem nome> Suporte a ilustrações não compilado em libgpod. Não foi possível remover a lista de reprodução Biblioteca de Fotos. Abortando.
 Clássico Clássico (Preto) Clássico (Prata) Cor Colorido U2 Diretório de controle não encontrado: '%s' (ou similar). Não foi possível acessar o arquivo '%s'. Não foi possível acessar o arquivo '%s'. Foto não adicionada. Não foi possível encontrar a trilha correspondente (dbid: %s) para o registro da ilustração.
 Não foi possível encontrar no iPod: '%s'
 Não é possível abrir arquivo '%s' para escrita. Não foi possível encontrar um banco de dados do iPod em %s. Não é possível ler xml sysinfo de %s
 Não é possível gravar SysInfoExtended em %s O arquivo de destino '%s' não aparenta estar no iPod montado em '%s'. O diretório do dispositivo não existe. Encontrado um tipo desconhecido do tipo MHOD (%d) ao analisar o iTunesDB. Ignorando.

 Error adicionando fotos (%s) para o banco de dados de fotos: %s
 Erro ao comprimir arquivo iTunesCDB!
 Erro ao criar '%s' (mkdir)
 Erro ao inicializar iPod, erro desconhecido
 Erro ao inicializar iPod: %s
 Erro abrindo '%s' para leitura (%s). Erro abrindo '%s' para gravação (%s). Erro ao ler banco de dados de fotos do iPod (%s).
 Erro lendo banco de dados de fotos do iPod (%s).
Tentando criar um novo banco de dados.
 Erro lendo banco de dados de fotos do iPod, tentará criar um novo banco de dados
 Erro ao ler banco de dados de fotos do iPod.
 Erro removendo '%s' (%s). Erro renomeando '%s' para '%s' (%s). Erro ao fechar '%s' (%s). Erro ao ler de '%s' (%s). Erro ao gravar para '%s' (%s). Erro ao gravar lista de álbuns (mhsd tipo 4) Erro ao gravar lista dos artistas (mhsd tipo 8) Erro o gravar lista de faixas (hths) Erro ao gravar lista de faixas (mhsd tipo 1) Erro ao escrever mhsd tipo 10 Erro ao gravar mhsd tipo 6 Erro ao escrever mhsd tipo 9 Erro ao gravar lista de reprodução mhsd5 Erro ao gravar lista de reprodução (hphs) Erro ao gravar lista de reprodução de podcast especial (mhsd tipo 3) Erro ao gravar lista de reprodução padrão (mhsd tipo 2) Erro: \"%s\" não é um diretório
 Tons de cinza Tons de cinza U2 Cabeçalho é pequeno desmais para iTunesCDB!
 Nome de arquivo ilegal: '%s'.
 Busca ilegal para o deslocamento %ld (tamanho %ld) no arquivo '%s'. Número insuficiente de argumentos de linha de comandos.
 Inválido ltdb_Track ID '%d' não foi encontrado.
 Comprimento do campo inteligênte da regra da lista de reprodução (%d) não é como esperado. Tentando continuar de qualquer maneira.
 A biblioteca foi compilada sem suporte ao gdk-pixbuf. O suporte a imagens está desabilitado. LR-Mestre Mini (1ª Geração) Mini (2ª Geração) Mini (Azul) Mini (Dourado) Mini (Verde) Mini (Rosa) Mini (Prata) Móvel (1) Telefones celulares Ponto de montagem não configurado. Ponto de montagem não definido.
 Diretório de música não achado: '%s' (ou similar). Nano (1ª Geração) Nano (2ª Geração) Nano (Preto) Nano (Azul) Nano (Verde) Nano (Laranja) Nano (Rosa) Nano (Roxo) Nano (Vermelho) Nano (Prata) Nano (Branco) Nano (Amarelo) Vídeo Nano (3ª Geração) Vídeo Nano (4ª Geração) Nano touch (6th Gen.) Nano com câmera (5ª Geração) Nenhum diretório 'F..' encontrado em '%s'. Não é um arquivo de lista de reprodução OTG: '%s' (faltando cabeçalho mhpo). Não é um arquivo de contagem: '%s' (faltando cabe;alho mhdp). Não é iTunesDB: '%s' (faltando cabeçalho mhdb). Número de MHODs no mhip em %ld inconsistente no arquivo '%s'. Número de MHODs no mhyp em %ld inconsistente no arquivo '%s'. Lista de Reprodução OTG Arquivo de lista de reprodução OTG %d Arquivo de lista de reprodução OTG '%s': referência para uma faixa não existente (%d). Arquivo de lista de reprodução OTG ('%s'): entrada menor que o esperado (%d<4). Arquivo de lista de reprodução OTG ('%s'): tamanho do cabeçalho menor que o esperado (%d<20). Caminho não encontrado: '%s' (ou similar). Caminho não encontrado: '%s'. Foto Biblioteca de fotos Diretório de fotos não encontrado: '%s' (ou similar). Arquivo de contagem ('%s'): comprimento da entrada menor que o esperado (%d<12). Arquivo de contagem ('%s'): cabeçalho maior que o esperado (%d<96). Lista de Reprodução Podcasts Problema ao criar diretório ou arquivo '%s' no iPod. Regular (1ª Geração) Regular (2ª Geração) Regular (3ª Geração) Regular (4ª Geração) Aleatório Embaralhado (1ª Geração) Embaralhado (2ª Geração) Embaralhado (3ª Geração) Embaralhado (4ª Geração) Aleatório (Preto) Aleatório (Azul) Aleatório (Dourado) Aleatório (Verde) Aleatório (Laranja) Aleatório (Rosa) Aleatório (Roxo) Aleatório (Vermelho) Aleatório (Prata) Embaralhado (Puro) Álbum especificado '%s' não encontado. Abortando.
 Touch Toque (2ª Geração) Toque (5ª Geração) Touch (4th Gen.) Toque (Prata) Não é possível recuperar miniatura (parece estar no iPod, mas nenhuma informação de imagem disponível): nome do arquivo: '% s'
 Erro inesperado em itdb_photodb_add_photo_internal() ao adicionar foto, por favor reporte o erro. Tipo de imagem em mhni inesperada: %d, compensar: %d
 Tipo de string mhod inesperado : %d
 Índice mhsd inesperado: %d
 Desconhecido Ação desconhecida (0x%x) na lista de reprodução ativa será ignorada.
 Comando desconhecido '%s'
 Opção da regra da lista de reprodução inteligente desconhecida em %ld: %x. Tentando continuar.
 Uso para adicionar fotos:
%s add <ponto de montagem> <nome do álbum> [<nome do(s) arquivo(s)>]
<nome do álbum> deve estar definido como 'NULL' para adicionar fotos apenas ao
álbum de fotos principal (Biblioteca de Fotos). Se você não especificar nenhum nome
de arquivo, um álbum vazio será criado.
 Uso para depositar todas as fotos em <diretório de saída>:
%s dump <ponto de montagem> <diretório de saída>
 Uso para listar todas as identificações de fotos na saída padrão:
%s list <mountpoint>
 Uso para remover identificações de fotos da biblioteca de fotos:
%s remove <ponto de montagem> <nome do álbum> [<identificação(ões)>]
<nome do álbum> deve ser definido como 'NULL' para remover as fotos do
iPod. Se você não especificar nenhuma identificação, o álbum de fotos será removido.
ATENÇÃO: Identificações podem mudar ao escrever informações no arquivo PhotoDB.
 Vídeo (1ª Geração) Vídeo (2ª Geração) Vídeo (Preto) Vídeo (Branco) Vídeo U2 Aviso: Não foi possível encontar a foto com ID <%d>. Descartando...
 Número de argumentos especificado na linha de comando errado.
 Você deve especificar o modelo do iPod usado antes das fotos poderem ser adicionadas. Seu iPod parece não suportar fotos. Talvez você tenha que especificar o número de modelo correto do iPod? Atualmente ele é 'x%s' (%s/%s). comprimento do cabeçalho de '%s' é menor que o esperado (%d < %d) no deslocamento %ld no arquivo '%s'. iPad iPhone iPhone (1) iPhone (Preto) iPhone (Branco) iPhone 3G iPhone 3GS iPhone 4 iPod Diretório do iTunes não achado: '%s' (ou similar). iTunesCDB '%s' não pode ser comprimido iTunesDB '%s' corrompido: Não foi possível encontrar as listas de reprodução (não foi encontrado mhsd tipo2 ou 3) iTunesDB '%s' corrompido: Não foi possivel encontrar a lista de faixas (não foi encontrada sessão mhsd tipo 1) iTunesDB '%s' corrompido: mhsd esperado em %ld. iTunesDB ('%s'): comprimento do pedaço de cabeçalho do mhsd é menor que o esperado (%d<32). Abortando. iTunesDB corrompido: comprimento 0 do pedaço em %ld no arquivo '%s'. iTunesDB corrompido: faltando MHOD no deslocamento %ld no arquivo '%s'. iTunesDB corrompido: faltando SLst no deslocamento %ld no arquivo '%s'. ItunesDB corrompido: faltando sessão '%s' encontrado na sessão '%s' começando em %ld. iTunesDB corrompido: número de sessões mhip inconsistente em mhyp começando em %ld no arquivo '%s'. iTunesDB corrompido: número de faixais (pedaços mhit) inconsistente. Tentando continuar.
 iTunesDB possivelmente corrompido: número de listas de reprodução (pedaços mhyp) inconsistente. Tentando continuar.
 Arquivo iTunesStats  ('%s'): tamanho da entrada menor queo o esperado (%d<18). Arquivo iTunesStats ('%s'): tamanho de entrada menor que o esperado (%d<32). uso: %s <dispositivo> [fuso horário]
 Uso: %s <dispositivo|uuid|conexão do dispositivo> <ponto de montagem>
 