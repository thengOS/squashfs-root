��    3     �	  �  L      �  9   �  J   �  '   6  (   ^  $   �  .   �  5   �  ?     6   Q  :   �  :   �     �  )        >  '   \  5   �  -   �  9   �  8   "  )   [  &   �  "   �  "   �     �            .   (  "   W     z  .   �  1   �  #   �       I     /   _  &   �     �     �  	   �     �  &   �        ,   3   *   `   -   �      �   $   �   !   �       !     9!  +   S!     !     �!  )   �!  C   �!  >   �!  "   9"     \"  0   d"  1   �"  2   �"  $   �"     #     8#     O#     n#     v#     |#  !   �#  !   �#  !   �#  !   �#  '   $  
   3$  
   >$  
   I$  
   T$  
   _$  
   j$  
   u$  	   �$  =   �$     �$  4   �$     	%     &%     ,%     ;%     I%  !   a%  %   �%  �   �%     +&     8&     S&     Z&     v&     |&  	   �&     �&  	   �&     �&     �&     �&     �&     '     4'     N'     e'     �'  
   �'     �'     �'     �'     �'     �'  !   �'     (     )(     /(     1(     G(  $   K(     p(     r(     �(     �(     �(     �(     �(     �(     �(     �(  %   �(  "   )     @)     N)     W)     n)     �)  +   �)  &   �)  +   �)     *     *  )    *  6   J*  "   �*     �*  	   �*  
   �*     �*  	   �*     �*     +      !+     B+  -   ^+  &   �+     �+     �+     �+     �+  	   ,  
   ,     ,  
   3,     >,     X,  (   d,     �,     �,     �,     �,  ,   �,     �,  O   -  #   V-  $   z-     �-  5   �-  -   �-  6   #.     Z.     q.  &   �.  6   �.     �.  '   /  )   )/  /   S/  1   �/     �/  )   �/  +   �/  1   *0  4   \0  )   �0     �0     �0  !   �0     1  )   "1     L1     e1  	  m1     �:  �   �:     r;     �;     �;     �;     �;     �;  !   �;  %   �;  %   <  !   7<  =   Y<  #   �<  !   �<     �<     �<     �<  +   =     A=  -   O=  2   }=     �=     �=     �=  !   �=     >     0>     E>     S>  D   i>  $   �>  $   �>     �>     ?     ,?     I?     d?  4   v?  %   �?     �?     �?  /   �?     -@     6@     D@     Y@     f@     u@     �@     �@     �@     �@     �@     �@     �@     A     .A     @A  	   PA     ZA     bA     wA     �A  
   �A      �A     �A     �A     �A      B  +   B     EB     dB  #   uB     �B     �B  ,   �B     �B     C     C      =C  ?   ^C  J   �C     �C     �C     D     !D      &D     GD     eD     yD     �D     �D     �D     �D     �D  +   �D  !   #E     EE     ]E  �  zE  E   8G  J   ~G  2   �G  /   �G      ,H  '   MH  2   uH  7   �H  2   �H  2   I  2   FI     yI  )   �I     �I  '   �I  K    J  0   LJ  C   }J  E   �J  /   K  &   7K  "   ^K  "   �K  "   �K  !   �K     �K  6   �K  %   ,L     RL  *   [L  6   �L  (   �L     �L  Y   �L  A   MM  %   �M  (   �M     �M  	   �M     �M  ,   �M     ,N  ,   @N  *   mN  -   �N     �N  %   �N  !   O     .O     IO  6   iO     �O     �O  ,   �O  P   �O  K   -P  1   yP     �P  E   �P  B   �P  I   <Q  7   �Q  '   �Q  &   �Q  )   R     7R     ?R     FR  $   MR  !   rR      �R     �R  '   �R     �R     S     S     S     S     'S     0S     9S  E   AS  	   �S  ?   �S  #   �S     �S     �S     	T     T     ,T  .   IT  �   xT     U     U     0U     =U     YU     ^U  	   gU     qU  	   U      �U     �U  #   �U     �U     
V     'V     FV     aV     �V     �V  
   �V  $   �V     �V     �V  "   �V  (   W     0W     LW     RW     TW     eW  &   iW     �W     �W  $   �W     �W     �W     �W     X     X  	   X     )X  ;   0X  3   lX     �X     �X  "   �X  (   �X     	Y  .   Y  0   @Y  2   qY     �Y     �Y  1   �Y  ?   �Y  +   %Z     QZ     ZZ     mZ  "   Z     �Z  "   �Z  &   �Z  %   �Z     [  6   ;[  -   r[     �[  -   �[  -   �[     \     \     "\     /\     L\      Y\     z\  )   �\     �\     �\     �\     �\  /   �\     ]  Q   5]  %   �]  *   �]  &   �]  ?   �]  5   ?^  D   u^     �^     �^  *   �^  V   _  .   r_  ;   �_  I   �_  H   '`  7   p`  0   �`  I   �`  7   #a  J   [a  J   �a  L   �a     >b     Vb  &   qb  %   �b  5   �b     �b     c  "
  c     :m  �   Sm  !   On  %   qn     �n     �n     �n     �n  &   �n  &   �n  &   
o  "   1o  :   To  2   �o  "   �o     �o     �o  %   p  2   *p     ]p  8   ip  8   �p     �p      �p  .   q  .   Dq  *   sq  -   �q  #   �q  (   �q  ]   r  :   wr  ?   �r  7   �r  4   *s  (   _s  (   �s     �s  >   �s  +   t     3t     Ot  ,   dt     �t     �t  "   �t     �t     �t     �t  #   u  !   8u     Zu     xu     u     �u  "   �u  (   �u     �u     �u  
   v  
   v     v     6v     Pv     `v  &   pv     �v  $   �v     �v     �v  /   �v     +w     Kw  8   ^w     �w     �w  2   �w  +   �w     &x  '   3x  $   [x  N   �x  I   �x     y  "   3y     Vy     \y  !   ey  !   �y     �y     �y     �y     �y     z     z      .z  +   Oz  %   {z     �z  $   �z        J      '   �   m   �       �   �         )  7           H      �   �       �   �   �               s       *       2              �   �      �   �     �   	   �   �       �   p   �   S   �   a       �      F                       =       �   b       o   �   &  5   #  �   Z       y   }   �   �       "   B   �         �       ,   �   g   '      �   �   �   %     �               !  1          �   �       �       �   �   ,  w   �             4   �   �   u       �   �   �   �         G   �   �   !       9   f   �             2     �   $  &   .   �   /       �   l   )       �   ?          �       �         �   \   v         >   �       �   n             |              X   %   �   �           �   x       �   �   3  "  �   {           -   �      e   �   W       I   U       �   
  c   �   �           �     �   �   �       �             R   z   D        �   �   �   A              �   �       �   i   �           �   �   �   $   �   �   /          *  �   �   C   V   �   �              �          �       �   �       �   �   �   �   �       �   (     K   L   M   N      P   Q   3   �   +                              �     �   �      (      �             <   j          �   
   :   T   [   8   �   �       �          1     ;       .                    �        q   r       @   �      �       �   ^      �   O   �     0  �           �   +      �   d       k   �              �   h          #           �     6           Y     �   	  �   0   _   �   E           ~       �   ]       �   t   �   `         �   �   -  �    
Some of these may not be available on selected hardware
 === PAUSE ===                                                             PAUSE command ignored (no hw support)
          please, try the plug plugin %s
      -d,--disconnect     disconnect
      -e,--exclusive      exclusive connection
      -i,--input          list input (readable) ports
      -l,--list           list current connections of each port
      -o,--output         list output (writable) ports
      -r,--real #         convert real-time-stamp on queue
      -t,--tick #         convert tick-time-stamp on queue
      -x, --removeall
      sender, receiver = client:port pair
    aconnect -i|-o [-options]
    aconnect [-options] sender receiver
   -d,--dest addr : write to given addr (client:port)
   -i, --info : print certain received events
   -p,--port # : specify TCP port (digit or service name)
   -s,--source addr : read from given addr (client:port)
   -v, --verbose : print verbose messages
   Clemens Ladisch <clemens@ladisch.de>   Copyright (C) 1999 Takashi Iwai
   Jaroslav Kysela <perex@perex.cz>   Subdevice #%i: %s
   Subdevices: %i/%i
   Tim Janik   client mode: aseqnet [-options] server_host
   server mode: aseqnet [-options]
  !clip    * Connection/disconnection between two ports
  * List connected ports (no subscription action)
  * Remove all exported connections
  [%s %s, %s]  can't play WAVE-files with sample %d bits in %d bytes wide (%d channels)  can't play WAVE-files with sample %d bits wide %s is not a mono stream (%d channels)
 %s!!! (at least %.3f ms long)
 %s: %s
 (default) (unplugged) **** List of %s Hardware Devices ****
 + -        Change volume -f cd (16 bit little endian, 44100, stereo)
 -f cdr (16 bit big endian, 44100, stereo)
 -f dat (16 bit little endian, 48000, stereo)
 0-9        Set volume to 0%-90% ; '        Toggle left/right capture < >        Toggle left/right mute Aborted by signal %s...
 Access type not available Access type not available for playback: %s
 All Authors: B          Balance left and right volumes Broken configuration for playback: no configurations available: %s
 Broken configuration for this PCM: no configurations available Buffer size range from %lu to %lu
 CAPTURE Can't recovery from suspend, prepare failed: %s
 Can't recovery from underrun, prepare failed: %s
 Can't use period equal to buffer size (%lu == %lu) Cannot create process ID file %s: %s Cannot open WAV file %s
 Cannot open file "%s". Cannot open mixer device '%s'. Capture Card: Center Channel %2d: Control event : %5d
 Channel %2d: Note Off event: %5d
 Channel %2d: Note On event : %5d
 Channel %2d: Pitchbender   : %5d
 Channel %d doesn't match with hw_parmas Channel 10 Channel 11 Channel 12 Channel 13 Channel 14 Channel 15 Channel 16 Channel 9 Channel numbers don't match between hw_params and channel map Channels %i Channels count (%i) not available for playbacks: %s
 Channels count non available Chip: Connected From Connecting To Connection failed (%s)
 Connection is already subscribed
 Copyright (C) 1999-2000 Takashi Iwai
 Debugging options:
  -g, --no-color          toggle using of colors
  -a, --abstraction=NAME  mixer abstraction level: none/basic Device name: Disconnection failed (%s)
 Done.
 End        Set volume to 0% Error Esc     Exit Esc: Exit F1 ? H  Help F1:  Help F2 /    System information F2:  System information F3      Show playback controls F4      Show capture controls F5      Show all controls F6 S    Select sound card F6:  Select sound card Failed. Restarting stream.  Front Front Left Front Right HW Params of device "%s":
 Help Invalid WAV file %s
 Invalid number of periods %d
 Invalid parameter for -s option.
 Invalid test type %s
 Item: L L       Redraw screen LFE Left    Move to the previous control M M          Toggle mute Max peak (%li samples): 0x%08x  Mono No enough memory
 No subscription is found
 Not a WAV file: %s
 O Off On Page Up/Dn Change volume in big steps Period size range from %lu to %lu
 Periods = %u
 Playback Playback device is %s
 Playback open error: %d,%s
 Playing Playing Creative Labs Channel file '%s'...
 Press F6 to select another sound card. Q W E      Increase left/both/right volumes R Rate %d Hz,  Rate %iHz not available for playback: %s
 Rate doesn't match (requested %iHz, get %iHz, err %d)
 Rate set to %iHz (requested %iHz)
 Rear Rear Left Rear Right Recognized sample formats are: Recording Requested buffer time %u us
 Requested period time %u us
 Right   Move to the next control Sample format non available Sample format not available for playback: %s
 Sample rate doesn't match (%d) for %s
 Select File Setting of hwparams failed: %s
 Setting of swparams failed: %s
 Side Side Left Side Right Sine wave rate is %.4fHz
 Sound Card Space      Toggle capture Sparc Audio Sparc Audio doesn't support %s format... Status(DRAINING):
 Status(R/W):
 Status:
 Stereo Stream parameters are %iHz, %s, %i channels
 Suspended. Trying resume.  Suspicious buffer position (%li total): avail = %li, delay = %li, buffer = %li
 Tab     Toggle view mode (F3/F4/F5) The available format shortcuts are:
 The sound device was unplugged. This sound device does not have any capture controls. This sound device does not have any controls. This sound device does not have any playback controls. Time per period = %lf
 Transfer failed: %s
 Try `%s --help' for more information.
 Unable to determine current swparams for playback: %s
 Unable to install hw params: Unable to parse channel map string: %s
 Unable to set avail min for playback: %s
 Unable to set buffer size %lu for playback: %s
 Unable to set buffer time %u us for playback: %s
 Unable to set channel map: %s
 Unable to set hw params for playback: %s
 Unable to set nperiods %u for playback: %s
 Unable to set period time %u us for playback: %s
 Unable to set start threshold mode for playback: %s
 Unable to set sw params for playback: %s
 Undefined channel %d
 Unknown option '%c'
 Unsupported WAV format %d for %s
 Unsupported bit size %d.
 Unsupported sample format bits %d for %s
 Up/Down    Change volume Usage:
 Usage: %s [OPTION]... [FILE]...

-h, --help              help
    --version           print current version
-l, --list-devices      list all soundcards and digital audio devices
-L, --list-pcms         list device names
-D, --device=NAME       select PCM by name
-q, --quiet             quiet mode
-t, --file-type TYPE    file type (voc, wav, raw or au)
-c, --channels=#        channels
-f, --format=FORMAT     sample format (case insensitive)
-r, --rate=#            sample rate
-d, --duration=#        interrupt after # seconds
-M, --mmap              mmap stream
-N, --nonblock          nonblocking mode
-F, --period-time=#     distance between interrupts is # microseconds
-B, --buffer-time=#     buffer duration is # microseconds
    --period-size=#     distance between interrupts is # frames
    --buffer-size=#     buffer duration is # frames
-A, --avail-min=#       min available space for wakeup is # microseconds
-R, --start-delay=#     delay for automatic PCM start is # microseconds 
                        (relative to buffer size if <= 0)
-T, --stop-delay=#      delay for automatic PCM stop is # microseconds from xrun
-v, --verbose           show PCM structure and setup (accumulative)
-V, --vumeter=TYPE      enable VU meter (TYPE: mono or stereo)
-I, --separate-channels one file for each channel
-i, --interactive       allow interactive operation from stdin
-m, --chmap=ch1,ch2,..  Give the channel map to override or follow
    --disable-resample  disable automatic rate resample
    --disable-channels  disable automatic channel conversions
    --disable-format    disable automatic format conversions
    --disable-softvol   disable software volume control (softvol)
    --test-position     test ring buffer position
    --test-coef=#       test coefficient for ring buffer position (default 8)
                        expression for validation is: coef * (buffer_size / 2)
    --test-nowait       do not wait for ring buffer - eats whole CPU
    --max-file-time=#   start another output file when the old file has recorded
                        for this many seconds
    --process-id-file   write the process ID here
    --use-strftime      apply the strftime facility to the output file name
    --dump-hw-params    dump hw_params of the device
    --fatal-errors      treat all errors as fatal
 Usage: alsamixer [options] Useful options:
  -h, --help              this help
  -c, --card=NUMBER       sound card number or id
  -D, --device=NAME       mixer device name
  -V, --view=MODE         starting view mode: playback/capture/all Using 16 octaves of pink noise
 Using max buffer size %lu
 VOC View: WAV file(s)
 WAVE Warning: format is changed to %s
 Warning: format is changed to MU_LAW
 Warning: format is changed to S16_BE
 Warning: format is changed to U8
 Warning: rate is not accurate (requested = %iHz, got = %iHz)
 Warning: unable to get channel map
 Wave doesn't support %s format... Woofer Write error: %d,%s
 You need to specify %d files Z X C      Decrease left/both/right volumes accepted[%d]
 aconnect - ALSA sequencer connection manager
 aseqnet - network client/server on ALSA sequencer
 audio open error: %s bad speed value %i buffer to small, could not use
 can't allocate buffer for silence can't get address %s
 can't get client id
 can't malloc
 can't open sequencer
 can't play WAVE-file format 0x%04x which is not PCM or FLOAT encoded can't play WAVE-files with %d tracks can't play loops; %s isn't seekable
 can't play packed .voc files can't set client info
 cannot enumerate sound cards cannot load mixer controls cannot open mixer capture stream format change? attempting recover...
 card %i: %s [%s], device %i: %s [%s]
 client %d: '%s' [type=%s]
 closing files..
 command should be named either arecord or aplay dB gain: disconnected
 enter device name... fatal %s: %s info error: %s invalid card index: %s
 invalid destination address %s
 invalid sender address %s
 invalid source address %s
 kernel malloc error mute no soundcards found... nonblock setting error: %s not enough memory ok.. connected
 options:
 overrun pause push error: %s pause release error: %s raw data read error read error (called from line %i) read error: %s read/write error, state = %s readv error: %s sequencer opened: %d:%d
 service '%s' is not found in /etc/services
 snd_pcm_mmap_begin problem: %s status error: %s stdin O_NONBLOCK flag setup failed
 suspend: prepare error: %s too many connections!
 try `alsamixer --help' for more information
 unable to install sw params: underrun unknown abstraction level: %s
 unknown blocktype %d. terminate. unknown length of 'fmt ' chunk (read %u, should be %u at least) unknown length of extensible 'fmt ' chunk (read %u, should be %u at least) unknown option: %c
 unrecognized file format %s usage:
 user value %i for channels is invalid voc_pcm_flush - silence error voc_pcm_flush error was set buffer_size = %lu
 was set period_size = %lu
 write error write error: %s writev error: %s wrong extended format '%s' wrong format tag in extensible 'fmt ' chunk xrun(DRAINING): prepare error: %s xrun: prepare error: %s xrun_recovery failed: %d,%s
 Project-Id-Version: alsa-utils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-10-27 17:34+0100
PO-Revision-Date: 2013-10-11 00:52+0000
Last-Translator: gabriell nascimento <gabriellhrn@gmail.com>
Language-Team: Portuguese (Brazil) <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:56+0000
X-Generator: Launchpad (build 18115)
 
Alguns destes podem não estar disponíveis no hardware selecionado
 === PAUSE ===                                                             Comando PAUSE ignorado (sem suporte de hardware)
          por favor, tente conectar o plugin %s
      -d,--disconnect desconecta
      -e,--exclusive conexão exclusiva
      -i,--input lista portas de entrada (leitura)
      -l,--list lista as conexões atuais de cada porta
      -o,--output lista portas de saída (escrita)
      -r,--real # converte real-time-stamp na fila
      -t,--tick # converte tick-time-stamp na fila
      -x, --removeall
      origem, destino = par cliente:porta
    aconnect -i|-o [-opções]
    aconnect [-opções] origem destino
   -d,--dest endereço : escreve para o endereço fornecido (cliente:porta)
   -i, --info : imprime certos eventos recebidos
   -p,--port # : especifica porta TCP (dígito ou nome do serviço)
   -s,--source endereço : lê do endereço fornecido (cliente:porta)
   -v, --verbose : imprime mensagens detalhadas
   Clemens Ladisch <clemens@ladisch.de>   Copyright (C) 1999 Takashi Iwai
   Jaroslav Kysela <perex@perex.cz>   Dispositivo secundário #%i: %s
   Dispositivo secundário: %i/%i
   Tim Janik   modo cliente: aseqnet [-opções] endereco_servidor
   modo servidor: aseqnet [-opções]
  !clip    * Conexão/desconexão entre duas portas
  * Lista portas conectadas (sem ação de assinatura)
  * Remove todas as conexões exportadas
  [%s %s, %s]  não foi possível reproduzir arquivos wave com taxa de %d bits por %d bytes (%d canais)  não foi possível reproduzir arquivos waves com taxa de %d bits %s não é um fluxo mono (%d canais)
 %s!!! (pelo menos %.3f ms de duração)
 %s: %s
 (padrão) (desconectado) **** Lista de Dispositivos %s Hardware ****
 + - Altera o volume -f cd (16 bit little endian, 44100, stereo)
 -f cdr (16 bit big endian, 44100, stereo)
 -f dat (16 bit little endian, 48000, stereo)
 0-9 Ajusta o volume para 0%-90% ; '  Alterna captura esquerda/direita < > Alterna mudo esquerda/direita Cancelado por sinal %s...
 Tipo de acesso não disponível Tipo de acesso não disponível para reprodução: %s
 Todos Autores: B Equilibra os volumes da esquerda e direita Configuração de reprodução quebrada: nenhuma configuração disponível: %s
 Configuração danificada para este PCM: nenhuma configuração disponível Tamanho do buffer tem um intervalo de %lu à %lu
 CAPTURA Não foi possível recuperar da suspenção; preparação falhou: %s
 Não foi possível recuperar do esvaziamento; erro no preparo: %s
 Não é possivel usar um período igual ao tamanho do buffer (%lu == %lu) Não é possível criar o arquivo do processo ID %s: %s Não é possível abrir arquivo WAV %s
 Não é possível abrir arquivo  "%s". Não pode abrir o dispositivo mixer '%s'. Captura Placa: Centro Canal %2d: Controle de evento : %5d
 Canal %2d: Evento Note Off : %5d
 Canal %2d: Evento Note On : %5d
 Canal %2d: Pitchbender: %5d
 Canal %d não corresponde com hw_params Canal 10 Canal 11 Canal 12 Canal 13 Canal 14 Canal 15 Canal 16 Canal 9 Números de canais não correspondem entre hw_params e mapa de canais Canais %i Contagem de canais (%i) não disponível para reprodução: %s
 Contagem de canais não disponível Chipe: Conectado De Conectando A Conexão falhou (%s)
 Conexão já está inscrita
 Direitos de cópia (C) 1999-2000 Takashi Iwai
 Debugging options:
  -g, --no-color         alterna uso de cores
  -a, --abstraction=NAME  nível de abstração do mixer: nenhum/básico Nome do dispositivo: Desconexão falhou (%s)
 Concluído.
 End ajusta o volume para 0% Erro Esc Sair Esc: Sair F1 ? H  Ajuda F1: Ajuda F2 /    Informações do sistema F2: Informações do sistema F3 Mostra controles de reprodução F4 Mostra controles de captura F5 Mostra todos os controles F6 S    Seleciona placa de som F6: Seleciona placa de som Falhou. Reiniciando o stream.  Frontal Da Esquerda Da Direita Parâmetros HW do dispositivo "%s":
 Ajuda Arquivo WAV inválido %s
 Número inválido de períodos %d
 Parâmetro inválido para a opção -s.
 Tipo de teste inválido %s
 Item: E L Redesenha tela LFE Esquerda move para o controle anterior M M Alterna mudo Pico máximo (%li amostras): 0x%08x  Mono Memória insuficiente
 Nenhuma assinatura encontrada
 Não é um arquivo WAV: %s
 O Desligado Ligado Página para Cima / Baixo altera o volume em passos grandes Tamanho do período tem um intervalo de %lu à %lu
 Períodos = %u
 Reprodução Dispositivo de reprodução é %s
 Erro de abertura de reprodução: %d,%s
 Tocando Tocando arquivo Creative Labs Channel '%s'...
 Pressione F6 para selecionar outra placa de som. Q W E Aumenta os volumes da esquerda/ambos/direita D Taxa %d Hz,  Taxa %iHz não disponível para reprodução: %s
 Taxas não compatíveis (requisitada %iHz, dada %iHz, erro %d)
 Taxa alterada para %iHz (requisitada %iHz)
 Traseira Esquerda Posterior Direita Posterior Formatos modelo reconhecidos são: Gravando Requisitado tempo de buffer de %u
 Requisitado o período de tempo de %u
 Direita move para o próximo controle Formato modelo não disponível Formato modelo não disponível para reprodução: %s
 Taxa de amostragem não combina (%d) para %s
 Selecionar arquivo Ajuste de parâmetros de hardware falhou: %s
 Ajuste de parâmetros de software falhou: %s
 Lado Lado Esquerdo Lado Direito Taxa da onda sena é %.4fHz
 Placa de som Barra de espaço alterna captura Áudio Sparc Sparc Áudio não suporta o formato %s... Status(ESVAZIANDO):
 Status(R/W):
 Estado:
 Estéreo Parâmetros do stream são %iHz, %s, %i canais
 Suspenso. Tentando continuar.  Posição suspeita de buffer (%li total): avail = %li, delay = %li, buffer = %li
 Tab Alterna modo de visão (F3/F4/F5) Os formatos de atalhos disponíveis são:
 O dispositivo de som foi desconectado. Este dispositivo de som não possui nenhum controle de captura. Este dispositivo de som não possui  nenhum controle. Este dispositivo de som não possui nenhum controle de reprodução. Tempo por peíodo = %lf
 Falha na transferência: %s
 Tente `%s --help' para mais informação.
 Não foi possível determinar os parâmetros de software atuais para reprodução: %s
 Não foi possível instalar parâmetros de hw: Não foi possível analisar o mapa de canais da string: %s
 Não foi possível ajustar os minutos disponíveis para reprodução: %s
 Não foi possível ajustar o tamanho%lu do buffer para reprodução: %s
 Incapaz de sincronizar o buffer %u para reproduzir: %s
 Não foi possível configurar mapa de canal: %s
 Não foi possível ajustar parâmetros de hardware para reprodução: %s
 Impossível configurar nperiods %u para reproduzir: %s
 Impossível de configurar o período de tempo de %u para reprodução: %s
 Não foi possível ajustar o início do modo limite para reprodução: %s
 Não foi possível ajustar os parâmetros de software para reprodução: %s
 Canal não definido %d
 Opção desconhecida '%c'
 Formato WAV %d não suportado para %s
 Bit de tamanho %d não é suportado.
 Formato de bits de amostra %d não suportado para %s
 Cima / Baixo altera o volume Uso:
 Uso: %s [OPÇÃO]... [ARQUIVO]...

-h, --help              ajuda
    --version           imprime a versão atual
-l, --list-devices      lista todas as placas de som e dispositivos de áudio digital
-L, --list-pcms         lista nomes de dispositivos
-D, --device=NOME       seleciona PCM por nome
-q, --quiet             modo silencioso
-t, --file-type TIPO    tipo de arquivo (voc, wav, raw or au)
-c, --channels=#        canais
-f, --format=FORMATO     formato das amostras (não diferencia maiúsculas de minúsculas)
-r, --rate=#            taxa de amostragem
-d, --duration=#        interrompe após # segundos
-M, --mmap              fluxo mmap
-N, --nonblock          modo não bloqueante
-F, --period-time=#     distância entre interrupções é # microssegundos
-B, --buffer-time=#     duração do buffer é # microssegundos
    --period-size=#     distância entre interrupções é # frames
    --buffer-size=#     duração do buffer é # quadros
-A, --avail-min=#       espaço mínimo disponível para despertar é # microssegundos
-R, --start-delay=#     atraso para início do PCM automático é # microssegundos
                        (relativo ao tamanho do buffer se <= 0)
-T, --stop-delay=#      atraso para interromper PCM automático é # microssegundos a partir de xrun
-v, --verbose           mostra estrutura PCM e configuração (acumulativo)
-V, --vumeter=TIPO      habilita medidor VU (TIPO: mono ou stereo)
-I, --separate-channels um arquivo para cada canal
-i, --interactive       permite operação interativa do stdin
-m, --chmap=ch1,ch2,..  Dar ao mapa do canal para substituir ou seguir
    --disable-resample  desativa reamostragem automática
    --disable-channels  desativa conversão automática de canais
    --disable-format    desativa conversão automática de formato
    --disable-softvol   desativa controle de volume por software (softvol)
    --test-position     testa posição do buffer anel
    --test-coef=#       testa coeficiente para posição do buffer anel (padrão 8)
                        expressão para validação é: coef * (tamanho_do_buffer / 2)
    --test-nowait       não espera por buffer anel - devora toda a CPU
    --max-file-time=#   inicia outro arquivo de saída quando o arquivo antigo foi gravado
                        por esta quantidade de segundos
    --process-id-file   escreve o ID do processo aqui
    --use-strftime      aplica a facilidade strftime para o nome do arquivo de saída
    --dump-hw-params    descarrega hw_params do dispositivo 
    --fatal-errors      trata todos os erros como fatais
 Uso: alsamixer [opção] Opções úteis:
  -h, --help              esta ajuda
  -c, --card=NUMBER       número da placa de som ou identificação
  -D, --device=NAME       nome do dispositivo mixer
  -V, --view=MODE         modo inicial de visão: reprodução/captura/todos Usando 16 oitavas de ruído rosa
 Usando tamanho máximo de buffer %lu
 VOC Visualizar: Arquivo(s) WAV
 WAVE Aviso: o formato foi alterado para %s
 Aviso: formato modificado para MU_LAW
 Aviso: formato modificado para S16_BE
 Aviso: formato modificado para U8
 Aviso: taxa não é exata (solicitada = %iHz, got = %iHz)
 Alerta: Não foi possível obter o mapa de canais
 Wave não suporta o formato %s ... Woofer Erro de escrita: %d,%s
 Você precisa especificar %d arquivos Z X C Diminui os volumes da esquerta/ambos/direita aceito[%d]
 aconnect - gerenciador de conexão do sequenciador ALSA
 aseqnet - cliente/servidor de rede no sequenciador ALSA
 erro ao abrir áudio: %s valor de velocidade %i incorreto buffer muito pequeno, não foi possível usar
 não é possível alocar buffer para silêncio não foi possível encontrar endereço %s
 não foi possível recuperar o id do cliente
 não foi possível executar malloc
 não foi possível abrir o sequenciador
 não se pode tocar o formato de arquivo WAVE 0x%04x porque não é um PCM ou FLOAT codificado não foi possível reproduzir arquivos waves com %d faixas não é possível reproduzir loops; %s não pode ser procurado
 Não foi possível reproduzir arquivos .voc empacotados não foi possível definir informações do cliente
 não é possível enumerar placas de som não pode carregar os controles do mixer não pode abir o mixer capturar mudança no formato do stream? tentando recuperar...
 placa %i: %s [%s], dispositivo %i: %s [%s]
 cliente %d: '%s' [type=%s]
 fechando arquivos..
 comando deveria ser chamado arecord ou aplay ganho em dB: desconectado
 entre com o nome do dispositivo... fatal %s: %s erro de informação: %s indice inválido da placa: %s
 endereço de destindo inválido %s
 endereço de origem inválido %s
 endereço-fonte %s inválido
 kernel erro no malloc mudo nenhuma placa de som encontrada... erro de configuração de não bloco: %s memória insuficiente ok.. conectado
 opções:
 sobrecarga erro ao empurrar pause: %s erro ao lançar pause: %s dados originais erro de leitura erro de leitura (invocado da linha %i) erro de leitura: %s erro de leitura/escrita, estado = %s erro readv: %s sequenciador aberto: %d:%d
 serviço '%s' não encontrado em /etc/services
 snd_pcm_mmap_begin problema: %s erro de status: %s stdin O_NONBLOCK sinalização de configuração falhou
 suspend: erro de preparo: %s muitas conexões!
 Tente  `alsamixer --help' para mais informações
 não foi possível instalar parâmetros sw: esvaziamento Nível de abstração desconhecido: %s
 blocktype %d desconhecido. terminar. comprimento desconhecido do pedaço 'fmt ' (leu %u, deveria ser pelo menos %u) Tamanho desconhecido do 'fmt' extensivo (leia %u, ou também %u no final) opção desconhecida: %c
 formato de arquivo %s desconhecido uso:
 usuário valor %i para canais é inválido voc_pcm_flush - erro de silêncio erro no voc_pcm_flush buffer_size definido = %lu
 period_size definido = %lu
 erro de escrita erro de escrita: %s erro de writev: %s formato estendido '%s' incorreto formato desconhecido na tag extensiva 'fmt' xrun(ESVAZIANDO): erro de preparo: %s xrun: erro de preparo: %s recuperação do xrun falhou: %d,%s
 