��    F      L  a   |         /     �   1  &     %   <  #   b  %   �  
   �     �     �  	   �     �     �     �       C        S     c  
   i  
   t       
   �  *   �     �  	   �  	   �     �     �     	     	     1	     :	     N	     W	     c	  �   y	  �   
  �   �
     (     =     O     ^     q     w     }     �  f   �  `     d   w     �     �  
   �     �     �          -     ?     M  	   g     q     }     �     �     �     �     �     �     �     �     �  �  �  :   �    �  &   �  %     #   ;  %   _  
   �  	   �     �     �     �     �     �     �  H   �     9     H     N  
   Z     e     t  (   �     �  	   �  	   �     �     �     �  "        0     9     P     h     u  �   �  �   4  �   �     z     �  "   �     �     �     �     �       n   +  h   �  l        p     v     {     �     �     �      �     �       	   1     ;     G  
   W     b     r     �     �     �     �     �  
   �     D   5   4       =         +      	                  %      
   &   A       0       #   !   2                     @   /   -   $      .          "              :      F         ?                1                            C             7       <   3   >   '                     6          B      9          (   )   E         *   8               ,       ;    * You should restart nabi to apply above option <span size="x-large" weight="bold">Can't load tray icons</span>

There are some errors on loading tray icons.
Nabi will use default builtin icons and the theme will be changed to default value.
Please change the theme settings. <span weight="bold">Connected</span>:  <span weight="bold">Encoding</span>:  <span weight="bold">Locale</span>:  <span weight="bold">XIM name</span>:  About Nabi Advanced Automatic reordering BackSpace Choseong Commit by word English keyboard Hangul Hangul input method: Nabi - You can input hangul using this program Hangul keyboard Hanja Hanja Font Hanja Lock Hanja Options Hanja keys Ignore fontset information from the client Input mode scope:  Jongseong Jungseong Keyboard Keypress Statistics Nabi Preferences Nabi keypress statistics Nabi: %s Nabi: error message Off keys Orientation Preedit string font:  Press any key which you want to use as candidate key. The key you pressed is displayed below.
If you want to use it, click "Ok", or click "Cancel" Press any key which you want to use as off key. The key you pressed is displayed below.
If you want to use it, click "Ok" or click "Cancel" Press any key which you want to use as trigger key. The key you pressed is displayed below.
If you want to use it, click "Ok" or click "Cancel" Select candidate key Select hanja font Select off key Select trigger key Shift Space Start in hangul mode The orientation of the tray. This key is already registered for <span weight="bold">candidate key</span>
Please  select another key This key is already registered for <span weight="bold">off key</span>
Please  select another key This key is already registered for <span weight="bold">trigger key</span>
Please  select another key Total Tray Tray icons Trigger keys Use dynamic event flow Use simplified chinese Use system keymap Use tray icon XIM Server is not running XIM name: _Hanja Lock _Hide palette _Reset _Show palette hangul(hanja) hanja hanja(hangul) per application per context per desktop per toplevel Project-Id-Version: nabi
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-01-26 17:31+0900
PO-Revision-Date: 2011-07-03 19:05+0000
Last-Translator: Igor Martins <Unknown>
Language-Team: Portuguese (Brazil) <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:12+0000
X-Generator: Launchpad (build 18115)
 * Você deve reiniciar o nabi para aplicar a opção acima <span size="x-large" weight="bold">Não foi possível carregar os ícones da bandeja</span>

Houve alguns erros ao carregar os ícones da bandeja.
Nabi utilizará ícones-padrão embutidos e o tema será alterado para o valor padrão.
Por favor, altere as configurações do tema. <span weight="bold">Conectado</span>:  <span weight="bold">Encoding</span>:  <span weight="bold">Locale</span>:  <span weight="bold">XIM name</span>:  Sobre Nabi Avançado Reordenação automática Voltar Choseong Submeter por palavra Teclado Inglês Hangul Método input Hangul - você pode entrar com hangul usando este programa Teclado Hangul Hanja Fonte Hanja Hanja Lock Opções Hanja Chaves Hanja Ignorar informações fontset do cliente Escopo do modo de entrada:  Jongseong Jungseong Teclado Estatísticas de Acesso Preferências Nabi Estatística do uso de teclas Nabi Nabi: %s Nabi: mensagem de erro Teclas de desativação Orientação Pré-editar fonte da linha:  Pressione qualquer tecla que deseja usar como tecla candidata. A tecla que você pressionar é mostrada abaxo.
Se você usá-la, clique "OK" ou clique "Cancelar" Pressione qualquer tecla que deseja usar como tecla de disparo. A tecla que você pressionar é mostrada abaxo.
Se você usá-la, clique "OK" ou clique "Cancelar" Pressione qualquer tecla que deseja usar como tecla de disparo. A tecla que você pressionar é mostrada abaxo.
Se você usá-la, clique "OK" ou clique "Cancelar" Selecione a tecla candidata Selecione fonte hanja Selecione a tecla de desativação Selecione a tecla de disparo Shift Espaço Iniciar em modo hangul A orientação da bandeja Esta chave já está registrada para <span weight="bold">candidate key</span>
Por favor, selecione outra chave Esta chave já está registrada para <span weight="bold">off key</span>
Por favor, selecione outra chave Esta chave já está registrada para <span weight="bold">trigger key</span>
Por favor, selecione outra chave Total Tray Bandeja de ícones Teclas de disparo Use evento com fluxo dinâmico Usar chinês simplificado Usar o mapa de teclas do sistema Usar o ícone da bandeja XIM Server não está rodando Nome XIM: _Hanja Lock _Ocultar paleta _Restaurar Mo_strar paleta hangul(hanja) hanja hanja(hangul) por aplicação por contexto por desktop por nível 