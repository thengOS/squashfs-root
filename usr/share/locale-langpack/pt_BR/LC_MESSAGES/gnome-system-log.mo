��    M      �  g   �      �     �     �     �     �     �     �     �     �  #         $  "   >     a     m  E   u     �  -   �     �  )     -   1     _  #   k     �     �  	   �     �     �     �  
   �      	     	     ?	     Q	     b	     n	     w	  5   	     �	     �	  !   �	     �	  
   
     
      %
  (   F
  k   o
  =   �
  �     �   �  <   <  
   y  5   �  &   �  �   �  �   �  9   �      �  "        9  3   A  �   u     B     J     S     \     a     i     p     w     }     �     �     �     �     �     �  	   �  Y  �  $   =     b     y     �     �     �     �     �  /   �  &     )   3     ]     k  ^   s     �  1   �     "  7   *  7   b     �  %   �     �     �     �     �       /        M  $   [  $   �     �  #   �     �  	   �     �  O         P  !   U      w     �     �     �     �  (   �  h     J   �  �   �  �   a  K        [  B   j  %   �  �   �  �   �  I   �  +     &   /     V  :   _  �   �     n     v     ~  
   �  	   �     �     �     �     �     �     �      �     �  [    
   `#     k#            %      *      "      3   J   >      ,       5   =           #       ?              '   F                (   +      8   0      
          G   M                        H   )          <   9   @      E   K   :   6   !           &              2      B   7                 1         ;                     L      .   	          I       -      $   A       /   4       D   C        A system log viewer for GNOME. About System Log Add new filter Auto Scroll Background: Can't read from "%s" Close Copy Could not open the following files: Could not parse arguments Could not register the application Edit filter Effect: Error while uncompressing the GZipped log. The file might be corrupt. Filter name is empty! Filter name may not contain the ':' character Filters Find next occurrence of the search string Find previous occurrence of the search string Foreground: Height of the main window in pixels Help Hide Highlight Impossible to open the file %s List of saved filters List of saved regexp filters Loading... Log file to open up on startup Log files to open up on startup Manage Filters... No matches found Normal Size Open Log Open... Please specify either foreground or background color! Quit Regular expression is empty! Regular expression is invalid: %s Search in "%s" Select All Show Matches Only Show the version of the program. Size of the font used to display the log Specifies a list of log files to open up at startup. A default list is created by reading /etc/syslog.conf. Specifies the height of the log viewer main window in pixels. Specifies the log file displayed at startup. The default is either /var/adm/messages or /var/log/messages, depending on your operating system. Specifies the size of the fixed-width font used to display the log in the main tree view. The default is taken from the default terminal font size. Specifies the width of the log viewer main window in pixels. System Log The file is not a regular file or is not a text file. There was an error displaying help: %s This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version. This version of System Log does not support GZipped logs. View or monitor system log files Width of the main window in pixels Wrapped You don't have enough permissions to read the file. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA Zoom In Zoom Out [URI...] _Add _Cancel _Close _Name: _Open _Properties _Regular Expression: _Remove logs;debug;error; today translator-credits updated yesterday Project-Id-Version: gnome-utils
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-system-log&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:33+0000
PO-Revision-Date: 2014-05-29 03:11+0000
Last-Translator: Enrico Nicoletto <liverig@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:16+0000
X-Generator: Launchpad (build 18115)
X-Poedit-Country: Brazil
Language: pt_BR
X-Poedit-Language: Portuguese
 Um visualizador de log para o GNOME. Sobre o Log do sistema Adicionar novo filtro Rolagem automática Plano de fundo: Não é possível ler "%s" Fechar Copiar Não foi possível abrir os seguintes arquivos: Não foi possível analisar argumentos Não foi possível registrar o aplicativo Editar filtro Efeito: Erro ao descompactar um arquivo de log compactado com o GZip. O arquivo pode estar corrompido. O nome do filtro está vazio! O nome do filtro não pode conter o caractere ":" Filtros Localizar próxima ocorrência da expressão pesquisada Localizar ocorrência anterior da expressão pesquisada Primeiro plano: Altura da janela principal, em pixels Ajuda Oculto Destaque Impossível abrir o arquivo %s Lista de filtros salvos Lista de filtros de expresões regulares salvos Carregando... Arquivo de log para abrir ao iniciar Arquivo de log para abrir ao iniciar Gerenciar filtros... Não foram encontradas ocorrências Tamanho normal Abrir log Abrir... Por favor especifique tanto a cor do primeiro plano quanto a do plano de fundo! Sair A expressão regular está vazia! Expressão regular inválida: %s Pesquisar em "%s" Selecionar tudo Mostrar somente ocorrências Mostra a versão do programa. Tamanho da fonte usada para exibir o log Especifica uma lista de logs para abrir ao iniciar. Uma lista padrão é criada ao ler /etc/syslog.conf. Especifica a altura da janela principal do visualizador de log, em pixels. Especifica o arquivo de log exibido ao iniciar. O padrão é ou /var/adm/messages ou /var/log/messages, dependendo do seu sistema operacional. Especifica o tamanho da fonte de largura fixa usada para exibir o log na principal visão em árvore. O padrão é definido a partir do tamanho padrão da fonte de terminal. Especifica a largura da janela principal do visualizador de log, em pixels. Log do sistema Este arquivo não é um arquivo comum ou não é um arquivo texto. Ocorreu um erro ao exibir a ajuda: %s Este programa é distribuído na esperança de que seja útil, mas SEM NENHUMA GARANTIA; sem mesmo implicar garantias de COMERCIABILIDADE ou ADAPTAÇÃO A UM PROPÓSITO PARTICULAR.  Veja a Licença Pública Geral GNU (GPL) para mais detalhes. Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos da Licença Pública Geral GNU como publicada pela Free Software Foundation (FSF); na versão 2 da Licença, ou (na sua opinião) qualquer versão. Esta versão do sistema de logs não suporta logs compactados com o GZip. Ver ou monitorar arquivos de log do sistema Largura da janela principal, em pixels Quebrado Você não tem permissões suficientes para ler o arquivo. Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para a Fundação do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA. Ampliar Reduzir [URI...] _Adicionar _Cancelar _Fechar _Nome: _Abrir _Propriedades Expressão _regular: _Remover registros;logs;depuração;erro; hoje Alexandre Hautequest <hquest@fesppr.br>
Ariel Bressan da Silva <ariel@conectiva.com.br>
Evandro Fernandes Giovanini <evandrofg@ig.com.br>
Francisco Petrúcio Cavalcante Junior <fpcj@impa.br>
Gustavo Maciel Dias Vieira <gustavo@sagui.org>
Welther José O. Esteves <weltherjoe@yahoo.com.br>
Goedson Teixeira Paixão <goedson@debian.org>
Guilherme de S. Pastore <gpastore@colband.com.br>
Luiz Fernando S. Armesto <luiz.armesto@gmail.com>
Leonardo Ferreira Fontenelle <leonardof@gnome.org>
Og Maciel <ogmaciel@gnome.org>
Hugo Doria <hugodoria@gmail.com>
Djavan Fagundes <dnoway@gmail.com>
Krix Apolinário <krixapolinario@gmail.com>
Antonio Fernandes C. Neto <fernandesn@gnome.org>
Rodrigo Padula de Oliveira <contato@rodrigopadula.com>
André Gondim <In Memoriam>
Enrico Nicoletto <liverig@gmail.com>

Launchpad Contributions:
  Andre Mello Larangeira https://launchpad.net/~andremlarangeira
  Antonio Fernandes C. Neto https://launchpad.net/~fernandesn
  Enrico Nicoletto https://launchpad.net/~liverig
  João Marcus P. Gomes https://launchpad.net/~joaompg
  Tiago Hillebrandt https://launchpad.net/~tiagohillebrandt atualizado ontem 