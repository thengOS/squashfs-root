��    2      �  C   <      H  �   I  �   �  �   �     �      �  >   �  F   6  <   }  =   �  ;   �  >   4  
  s  �   ~	  �   T
  �     ;   �  =   �  9     �   R  &   $  5   K  =   �  9   �  X   �  .   R  =   �  E   �  A       G     ]  I   z  H   �  I     H   W  I   �  H   �  H   3  J   |  H   �  >     !  O  D   q  X   �  Y     +  i  ,  �  +  �  *  �  I     �  c  �       �  �   �  "   �   (   !  E   6!  N   |!  G   �!  @   "  G   T"  F   �"  /  �"  �   $  �   %  �   �%  =   u&  G   �&  E   �&  �   A'  1   8(  F   j(  E   �(  G   �(  o   ?)  1   �)  F   �)  F   (*  G   o*  D  �*     �+  I   ,  I   d,  H   �,  I   �,  G   A-  G   �-  F   �-  H   .  E   a.  D   �.    �.  O   	2  g   Y2  h   �2  \  *3  g  �4  g  �5  f  W7  G   �8               %   -      	          #          1             0   )       
                                      (       /   "      +          $                       ,               *          2       &      .      '                    !    Acceptance level
The level of character sequence acceptance defined in WTT 2.0.
0 accepts any key sequence.  2 accepts only orthographic ones.
1 is somewhere between. Cham input method simulating Cham keyboard.
Cham characters are encoded in logical order in memory and in files.
But, you can type Cham text in visual order with this input method.
Backspace and Delete also work in the manner of visual order. Character set to limit candidates.
Value must be a symbol representing a charater set, or nil.
If the value is not nil, a candidate containing a character not belonging
to the specified character set is ignored. Commit
Commit the preedit text Convert
Convert the preedit text Delete char
Delete the following character in the preedit text Delete char backward
Delete the previous character in the preedit text Extend segment
Extend the current segment length to the tail First candidate
Spot the first candidate in the current group First segment
Move to the first segment in the preedit text Fist character
Move to the first character in the preedit text Flag to control the action of Backspace key (delete or undo).
If this variable is 0 (the default), Backspace key deletes the previous
character (e.g. "q u a i s BS" => "quá").
If the value is 1, Backspace key undoes the previous key
(e.g. "q u a i s BS" => "quai"). Flag to control tone mark position in equivocal cases.
If this variable is 0 (the default), put tone mark on the first vowel
in such equivocal cases as "oa", "oe", "uy".
Otherwise, put tone mark on the last vowel. Global variable and command definitions.
This is actually not an input method, but provides documents,
default values of global variables, and default key-bindings of
global commands. Input method for Unicode BMP characters using hexadigits.
Type C-u followed by four hexadecimal numbers [0-9A-Fa-f]
of a Unicode character code.
 Last candidate
Spot the last candidate in the current group Last character
Move to the last character in the preedit text Last segment
Move to the last segment in the preedit text Maxmum number of candidates in a candidate group.
Value must be an integer.
If the value is not positive, number of candidates in a group is decided
by how candiates are grouped in an input method source file. Next candidate
Spot the next candidate Next candidate group
Move to the next candidate group Next character
Move to the next character in the preedit text Next segment
Move to the next segment in the preedit text Preedit prompt
Prompt string shown in the preedit area while typing hexadecimal numbers. Previous candidate
Spot the previous candidate Previous candidate group
Move to the previous candidate group Previous character
Move to the previous character in the preedit text Previous segment
Move to the previous segment in the preedit text Reload input method
Reload the input method (and configulation if any) and freshly start it.
Note that the length of key-sequence bound for this command must be 1.
This is one of special commands reserved by the m17n library, and
should not be used in a map of an input method. Revert
Revert the conversion Select the 10th candidate
Select the tenth candidate in the current group Select the 1st candidate
Select the first candidate in the current group Select the 2nd candidate
Select the second candidate in the current group Select the 3rd candidate
Select the third candidate in the current group Select the 4th candidate
Select the fourth candidate in the current group Select the 5th candidate
Select the fifth candidate in the current group Select the 6th candidate
Select the sixth candidate in the current group Select the 7th candidate
Select the seventh candidate in the current group Select the 9th candidate
Select the ninth candidate in the current group Shrink segment
Shrink the current segment length from the tail Simulating Azerty keyboard on English keyboard.

     &1  é2  "3  '4  (5  -6  è7  _8  ç9  à0  )°  =_  ²~
      aA  zZ  eE  rR  tT  yY  uU  iI  oO  pP  ^¨  $£
       qQ  sS  dD  fF  gG  hH  jJ  kK  lL  mM  ù%  *|
        wW  xX  cC  vV  bB  nN  ,?  ;.  :/  !§

'[' and '{' are used as a dead key to type a character with the
circumflex and diaeresis respectively (e.g. '[' 'e' -> "ê").

'Alt-2' and 'Alt-7' are used as a dead key to type a character
with tilde and grave respectively (e.g. 'Alt-2' 'n' -> "ñ").

'Ctrl-Alt-2' and 'Ctrl-Alt-7' can be used as 'Alt-2' and 'Alt-7'
respectively.

Azerty keyboard has one more key at the bottom left corner for
inputting "<" and ">".  As a normal English keyboard doesn't
have such a key left, type '<' and '>' twice for "<" and ">"
respectively. Start Unicode
Start typing hexadecimal numbers of Unicode character. Surrounding text vs. preedit
If 1, try to use surrounding text.  Otherwise, use preedit. Surrounding text vs. preedit.
If 1, try to use surrounding text.  Otherwise, use preedit. Vietnames input method using the TCVN6064 sequence.
Typing Backslash ('\') toggles the normal mode and English mode.
The following variables are customizable:
  tone-mark-on-last: control tone mark position in equivocal cases
  backspace-is-undo: control the action of Backspace key (delete or undo) Vietnames input method using the TELEX key sequence.
Typing Backslash ('\') toggles the normal mode and English mode.
The following variables are customizable:
  tone-mark-on-last: control tone mark position in equivocal cases
  backspace-is-undo: control the action of Backspace key (delete or undo) Vietnames input method using the VIQR key sequence.
Typing Backslash ('\') toggles the normal mode and English mode.
The following variables are customizable:
  tone-mark-on-last: control tone mark position in equivocal cases
  backspace-is-undo: control the action of Backspace key (delete or undo) Vietnames input method using the VNI key sequence.
Typing Backslash ('\') toggles the normal mode and English mode.
The following variables are customizable:
  tone-mark-on-last: control tone mark position in equivocal cases
  backspace-is-undo: control the action of Backspace key (delete or undo) select the 8th candidate
Select the eighth candidate in the current group Project-Id-Version: m17n-db
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-12-12 23:29+0900
PO-Revision-Date: 2011-09-20 23:12+0000
Last-Translator: André Gondim <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:34+0000
X-Generator: Launchpad (build 18115)
 nível de aceitação
O nível de aceitação sequência de caracteres definidos no WTT 2.0.
0 aceita qualquer sequência de teclas. 2 aceita apenas as ortográficas.
Um está em algum lugar entre eles. Cham método de entrada de teclado simulando Cham.
Cham caracteres são codificados em ordem lógica na memória e nos arquivos.
Mas, você pode digitar o texto Cham em ordem visual com este método de entrada.
Retrocesso e Delete também o trabalho da maneira de ordem visual. Conjunto de caracteres para limitar candidatos.
Valor deve ser um símbolo que representa um conjunto caractere, ou nulo.
Se o valor não é nulo, um candidato contendo caracteres não pertencentes
para o conjunto de caracteres especificado é ignorado. Enviar
Enviar o texto pré-editado Converter
Converter o texto pré-editado Excluir caractere
Excluir o carácter seguinte no texto pré-edição Excluir caractere anterior
Excluir o caractere anterior do texto pré-edição Estender segmento
Estender o comprimento do segmento atual para a cauda Primeiro candidato
Localizar o primeiro candidato no grupo atual Primeiro segmento
Mover para o primeiro segmento do texto pré-edição Primeiro caráter
Mudar para o primeiro caráter no texto pré-editado Bandeira para controlar a ação dos principais Retrocessos (apagar ou desfazer).
Se esta variável for 0 (o padrão), exclui Retrocesso chave anterior
personagem (por exemplo, "q u a i s BS" => "quá").
Se o valor for 1, tecla Retrocesso desfaz a chave anterior
(por exemplo, "q u a i s BS" => "quai"). Sinal para controlar a posição tom marca em casos duvidosos.
Se esta variável for 0 (o padrão), coloque marca o tom na primeira vogal
em tais casos duvidosos como "oa", "oe", "uy".
Caso contrário, coloca marca de tom na última vogal. Variáveis globais e definição dos comandos.
Na verdade, isto não é um método de entrada, mas fornece documentos,
valores padrões das variáveis globais, e o padrão de atalhos dos
comandos globais. Método de entrada para caracteres Unicode usando hexadigits BMP.
C-u tipo seguido por quatro números hexadecimal [0-9A-Fa-f]
de um código de caracteres Unicode.
 Último candidato
Localizar o ultimo candidato no grupo atual Último caractere
Mover para o último caractere no texto pré-edição Último segmento
Mover para o último segmento do texto pré-edição Número máximo de candidatos em um grupo de candidatos.
O valor deve ser inteiro.
Se o valor não for positivo, o número de candidato em um grupo é decidido
pela forma que os candidatos são agrupados em um arquivo fonte de entrada de método. Próximo candidato
Localizar o próximo candidato Próximo grupo de candidatos
Mudar para o próximo grupo de candidatos Próximo caráter
Mudar para o próximo carater no texto pré-editado Próximo segmento
Mover para o próximo segmento do texto pré-edição Pré-edição rápida
Sequência de prompt mostrado na área de pré-edição ao digitar números hexadecimais. Candidato anterior
Localizar o candidato anterior Grupo de candidatos anterior
Mudar para o grupo de candidator anterior Caráter anterior
Mudar para o caráter anterior no texto pré-editado Segmento anterior
Mover para o segmento anterior do texto pré-edição Método de entrada de recarga
Atualizar o método de entrada (e configuração se houver) e recém-iniciá-lo.
Note-se que o comprimento da sequência de teclas-ligadas para este comando deve ser 1.
Este é um dos comandos especiais reservados pela biblioteca m17n, e
não deve ser usado em um mapa de um método de entrada. Reverter
Reverte a conversão Selecionar o 10º candidato
Selecionar o décimo candidato no grupo atual Selecionar o 1º candidato
Selecionar o primeiro candidato no grupo atual Selecionar o 2º candidato
Selecionar o segundo candidato no grupo atual Selecionar o 3º candidato
Selecionar o terceiro candidato no grupo atual Selecionar o 4º candidato
Selecionar o quarto candidato no grupo atual Selecionar o 5º candidato
Selecionar o quinto candidato no grupo atual Selecionar o 6º candidato
Selecionar o sexto candidato no grupo atual Selecionar o 7º candidato
Selecionar o sétimo candidato no grupo atual Selecionar o 9º candidato
Selecionar o nono candidato no grupo atual Encolher segmentos
Diminuir o comprimento do segmento atual da cauda Simulando um teclado Azerty em um teclado americano.

     &1 é2 "3 '4 (5 -6 è7 _8 ç9 à0 )° =_ ²~
      aA zZ eE rR tT yY uU iI oO pP ^¨ $£
       qQ sS dD fF gG hH jJ kK lL mM ù% *|
        wW xX cC vV bB nN ,? ;. :/ !

"[" e "{" são usados como teclas mortas para digitar um caráter com
acento circunflexo e trema respectivamente (ex: '[' 'e' -> "ê").

"Alt-2" e "Alt-7" são usados como teclas mortas parar digitar um caráter
com til e grave respectivamente (ex: 'Alt-2' 'n' -> "ñ").

"Ctrl-Alt-2" e "Ctrl-Alt-7" podem ser usados como "Alt-2" e "Alt-7"
respectivamente.

O teclado Azerty tem mais uma tecla no canto inferior esquerdo para
inserir "<" e ">". Como um teclado inglês comum não tem tal tecla à esquerda, digite "<" e ">" duas vezes para "<" e ">" respectivamente. Iniciar Unicode
Comece a digitar números em hexadecimal de caracteres Unicode. Texto ao redor verso pré-edição
Se 1, tente usar texto ao redor. Caso contrário, use pré-edição. Texto ao redor verso pré-edição.
Se 1, tente usar texto ao redor. Caso contrário, use pré-edição. Vietnames método de entrada usando a sequência de TCVN6064.
Digitando barra invertida ('\') alterna o modo normal eo modo de Inglês.
As seguintes variáveis ​​são personalizáveis:
mark-tom-sobre-passado: controle de posição da marca de tom em casos duvidosos
backspace-is-undo: controlar a ação da tecla Retrocesso (apagar ou desfazer) Vietnames método de entrada usando a sequência de teclas TELEX.
Digitando barra invertida ('\') alterna o modo normal eo modo de Inglês.
As seguintes variáveis ​​são personalizáveis:
marcar-tom-sobre-passado: controle de posição da marca de tom em casos duvidosos
retrocesso-é-desfazer: controlar a ação da tecla Retrocesso (apagar ou desfazer) Vietnames método de entrada usando a sequência de teclas VIQR.
Digitando barra invertida ('\') alterna o modo normal e o modo de Inglês.
As seguintes variáveis ​​são personalizáveis:
marcar-tom-sobre-passado: controle de posição da marca de tom em casos duvidosos
retrocesso-é-desfazer: controlar a ação da tecla Retrocesso (apagar ou desfazer) Vietnames método de entrada usando a sequência de teclas VNI.
Digitando barra invertida ('\') alterna o modo normal e o modo de Inglês.
As seguintes variáveis ​​são personalizáveis:
marcar-tom-sobre-passado: controle de posição da marca de tom em casos duvidosos
retrocesso-é-desfazer: controlar a ação da tecla Retrocesso (apagar ou desfazer) Selecionar o 8º candidato
Selecionar o oitavo candidato no grupo atual 