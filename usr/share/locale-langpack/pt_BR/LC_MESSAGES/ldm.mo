��          �      \      �     �     �                     '  &   6     ]     f     w     �  (   �  ,   �     �  	          !   &     H     U  �  ]           "     4     D     L     g  (   |     �     �     �     �  .   �  '   !  %   I  	   o     y  '   �     �  
   �                                  
                     	                                            Automatic login in %d seconds Change _Language Change _Session Default Failsafe xterm Login as Guest No response from server, restarting... Password Select _Host ... Select _Language ... Select _Session ... Select the host for your session to use: Select the language for your session to use: Select your session manager: Shut_down Username Verifying password.  Please wait. _Preferences _Reboot Project-Id-Version: ldm
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-11-24 08:57+0200
PO-Revision-Date: 2010-12-15 03:30+0000
Last-Translator: André Gondim <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:26+0000
X-Generator: Launchpad (build 18115)
 Login automático em %d segundos Trocar _Linguagem Trocar _Sessão Padrão Xterm Seguro Contra Falhas Login como Visitante Sem resposta do servidor, reiniciando... Senha Selecionar _Host ... Selecionar _Linguagem ... Selecionar _Sessão ... Selecione a máquina para usar na sua sessão: Selecione a linguagem para sua sessão: Selecione seu gerenciador de sessão: _Desligar Usuário Verificando a senha. Por favor aguarde. _Preferências _Reiniciar 