��          T      �       �      �   0   �            $  &   ;      b  �  �     C  ;   c  )   �     �  +   �  6                                           Command to start service Include microseconds in timestamps in debug logs Include timestamps in debug logs SSSD Services to start Set the verbosity of the debug logging Write debug messages to logfiles Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: sssd-devel@lists.fedorahosted.org
POT-Creation-Date: 2016-04-13 16:31+0200
PO-Revision-Date: 2015-12-05 08:57+0000
Last-Translator: Marco Aurélio Krause <Unknown>
Language-Team: Portuguese (Brazil)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:33+0000
X-Generator: Launchpad (build 18115)
Language: pt-BR
 Comando para iniciar o serviço Incluir microssegundos em timestamps em logs de depuração Incluir timestamps em logs de depuração Serviços SSSD para iniciar Definir a verbosidade do log de depuração Escrever mensagens de depuração para arquivos de log 