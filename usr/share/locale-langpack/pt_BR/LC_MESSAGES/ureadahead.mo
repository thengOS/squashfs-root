��          �   %   �      P     Q     k     �     �      �     �     �          $     ?     Z     y     �  1  �     �     �     �  "     4   :     o      v     �  6   �  (   �  1     �  D             '   ?  #   g  (   �      �     �     �  $   	  $   -	  '   R	     z	     �	  �  �	     I     b  ,   v  8   �  2   �  	     %     "   ?  I   b  4   �  ?   �                                             	                       
                                                         %s: illegal argument: %s
 Error mapping into memory Error retrieving chunk extents Error retrieving file stat Error retrieving page cache info Error unmapping from memory Error while reading Error while tracing Failed to set CPU priority Failed to set I/O priority File vanished or error reading Ignored far too long path Ignored relative path PATH should be the location of a mounted filesystem for which files should be read.  If not given, the root filesystem is assumed.

If PATH is not given, and no readahead information exists for the root filesystem (or it is old), tracing is performed instead to generate the information for the next boot. Pack data error Pack too old Read required files in advance Unable to determine pack file name Unable to obtain rotationalness for device %u:%u: %s [PATH] detach and run in the background dump the current pack file how to sort the pack file when dumping [default: path] ignore existing pack and force retracing maximum time to trace [default: until terminated] Project-Id-Version: ureadahead
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-29 15:05+0000
PO-Revision-Date: 2010-03-15 16:44+0000
Last-Translator: Tiago Hillebrandt <tiagohillebrandt@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:47+0000
X-Generator: Launchpad (build 18115)
 %s: argumento ilegal: %s
 Erro de mapeamento na memória Erro ao recuperar extensões do pedaço Erro ao recuperar estado do arquivo Erro ao recuperar informações de cache Erro de esvaziamento da memória Erro durante a leitura Erro durante o rastreamento Falha ao definir a prioridade da CPU Falha ao definir a prioridade de E/S Arquivo desaparecido ou erro de leitura Caminho muito longo ignorado Caminho relativo ignorado CAMINHO deve ser a localização de um sistema de arquivos montado do qual os arquivos devem ser lidos. Se não fornecido, assume-se o sistema de arquivos raiz.

Se  não é fornecido o CAMINHO, e não existe nenhuma informação de leitura de cabeçalho para o sistema de arquivos raiz (ou ela é muito antiga), é realizado então um rastreamento para gerar a informação para a próxima inicialização. Erro nos dados do pacote Pacote muito antigo Ler os arquivos necessários antecipadamente Não é possível determinar o nome do arquivo do pacote Incapaz de obter rotação do dispositivo %u:%u %s [CAMINHO] desanexar e executar em segundo plano despejar o arquivo de pacote atual como classificar o arquivo de pacote quando despejando [padrão: caminho] ignorar pacote existente e forçar novo rastreamento tempo máximo para rastrear [padrão: até que seja finalizado] 