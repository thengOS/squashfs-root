��    C      4  Y   L      �     �     �     �  G   �  >   :  "   y  (   �     �  (   �  6        C  -   b  (   �     �     �  +   �  $     %   A  2   g  (   �  "   �  %   �  .   	  -   ;	     i	  0   �	  >   �	  2   �	  -   &
     T
  @   d
  "   �
  B   �
  
     ;     :   R     �     �  (   �  4   �       B   '  *   j     �  �   �     1  &   B     i  �   {  :   3  &   n  $  �  K   �  -        4  %   6     \      z     �     �     �     �     �     �     �     �  �  �     �  0   �     �  a   �  L   C  *   �  3   �  )   �  0     ?   J  !   �  .   �  $   �  "      %   #  0   I  0   z  0   �  5   �  /     .   B  *   q  8   �  .   �       /   "  N   R  2   �  .   �       ^     *   x  Q   �  	   �  <   �  =   <     z     �  ;   �  E   �     +  K   H  ,   �     �  �   �     v  )   �     �  �   �  J   �  (   �  6  %  Y   \  3   �     �  (   �          3     S     s     u     w     y     {     }          ,   6   )                  8          5   %   .          =         3   @      &      	      4   /   A       0                    B   7   #   *       -   :   >      "              <              ?       9       !   $          
   ;               C                   +       2          1      '             (                   	%s		File: %s

 
-- Type space to continue -- 
 
Commands are:

 
[SPACE] R)epl A)ccept I)nsert U)ncap S)tem Q)uit e(X)it or ? for help
   -1		check only first field in lines (delimiter = tabulator)
   -D		show available dictionaries
   -G		print only correct words or lines
   -H		HTML input file format
   -L		print lines with misspelled words
   -P password	set password for encrypted dictionaries
   -a		Ispell's pipe interface
   -d d[,d2,...]	use d (d2 etc.) dictionaries
   -h, --help	display this help and exit
   -i enc	input encoding
   -l		print misspelled words
   -m 		analyze the words of the input text
   -n		nroff/troff input file format
   -p dict	set dict custom dictionary
   -r		warn of the potential mistakes (rare words)
   -s 		stem the words of the input text
   -t		TeX/LaTeX input file format
   -v, --version	print version number
   -vv		print Ispell compatible version number
 0-n	Replace with one of the suggested words.
 ?	Show this help screen.
 A	Accept the word for the rest of this session.
 AVAILABLE DICTIONARIES (path is not mandatory for -d option):
 Are you sure you want to throw away your changes?  Bug reports: http://hunspell.sourceforge.net
 Can't open %s.
 Can't open affix or dictionary files for dictionary named "%s".
 Cannot update personal dictionary. Check spelling of each FILE. Without FILE, check standard input.

 FORBIDDEN! Hunspell has been compiled without Ncurses user interface.
 I	Accept the word, and put it in your private dictionary.
 LOADED DICTIONARY:
%s
%s
 Line %d: %s ->  Model word (a similar dictionary word):  Model word must be in the dictionary. Press any key! New word (stem):  Q	Quit immediately. Asks for confirmation. Leaves file unchanged.
 R	Replace the misspelled word completely.
 Replace with:  S	Ask a stem and a model word and store them in the private dictionary.
	The stem will be accepted also with the affixes of the model word.
 SEARCH PATH:
%s
 Space	Accept the word this time only.
 Spelling mistake? This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE,
to the extent permitted by law.
 U	Accept and add lowercase version to private dictionary.
 Usage: hunspell [OPTION]... [FILE]...
 Whenever a word is found that is not in the dictionary
it is printed on the first line of the screen.  If the dictionary
contains any similar words, they are listed with a number
next to each one.  You have the option of replacing the word
completely, or choosing one of the suggested words.
 X	Write the rest of this file, ignoring misspellings, and start next file.
 ^Z	Suspend program. Restart with fg command.
 a error - %s exceeds dictionary limit.
 error - iconv_open: %s -> %s
 error - iconv_open: UTF-8 -> %s
 error - missing HOME variable
 i q r s u x y Project-Id-Version: hunspell
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-06-02 13:54+0200
PO-Revision-Date: 2011-12-14 12:19+0000
Last-Translator: Tiago Hillebrandt <tiagohillebrandt@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
 	%s		Arquivo: %s

 
-- Tecle a barra de espaço para continuar -- 
 
Os comandos são:

 
[SPACE] R)substituir A)aceitar I)inserir U)remover S)devivação Q)sair (X)sair ou ? para ajuda
   -1		verifica apenas o primeiro campo nas linhas (delimitador = tabulador)
   -D		 exibe os dicionários disponíveis
   -G		mostra apenas as palavras ou linhas corretas
   -H		formato de arquivo de entrada HTML
   -L		imprime as linhas com palavras incorretas
   -P password	definir a senha para dicionários criptografados
   -a		 interface pipe do Ispell.
   -d d[,d2,...]	usar d (d2 etc.) dicionários
   -h, --help	exibe esta ajuda e sai
   -i enc	codificação de entrada
   -l		imprime as palavras incorretas
   -m 		analisar as palavras do texto de entrada
   -n		formato de arquivo de entrada nroff/troff
   -p dict	 ajustar dicionário customizado dict
   -r		avisar sobre potenciais erros (palavras raras)
   -s 		derivar as palavras do testo de entrada
   -t		formato de arquivo de entrada TeX/LaTeX
   -v, --version	mostra número da versão
   -vv		mostra número de versão compatível com Ispell
 0-n	Substitui por uma das palavras sugeridas.
 ?	Exibir esta tela de ajuda.
 A	Aceita a palavra até o resto desta sessão.
 DICIONÁRIOS DISPONÍVEIS (o caminho não é obrigatório para a opção -d):
 Tem certeza de que quer descartar suas mudanças?  Reportar bug: http://hunspell.sourceforge.net
 Não posso abrir %s.
 Não foi possível abrir o afixo ou arquivos do dicionário  para o dicionário chamado "%s".
 Impossível atualizar dicionário pessoal. Verificar a ortografia de cada ARQUIVO. Sem ARQUIVO, verificar entrada padrão.

 PROIBIDO! Hunspell foi compilado sem a interface de usuário Ncurses.
 I	Aceita a palavra e a coloca em seu dicionário particular.
 DICIONÁRIO CARREGADO:
%s
%s
 Linha %d: %s ->  Modelo de palavra (um dicionário de palavras semelhante):  A palavra-modelo deve estar no dicionário. Pressione qualquer tecla! Nova palavra (derivação):  Q	Sai imediatamente. Pede por confirmação. Deixa o arquivo sem mudança.
 R	Substitui completamente a palavra errada.
 Substituir por:  S	Pergunta uma derivação  e uma modelo de palavra e os guarda no dicionário particular.
	A derivação será aceita também com as adições da palavra modelo.
 PESQUISAR CAMINHO:
%s
 Space	Aceita a palavra somente esta vez.
 Erro ortográfico? Isto é um software livre; veja as condições de copia no fonte. Isto NÃO é

garantia; not even for em mesmo implicar garantia de MERCANTIBILIDADE ou APTIDÃO PARA UMA FINALIDADE PARTICULAR, 

	
à extensão permitiu pela lei.
 U	Aceita e adiciona uma versão em minúsculas ao dicionário particular.
 Uso: hunspell [OPÇÃO]... [ARQUIVO]...
 Quando é encontrada uma palavra que não consta do dicionário
é impresso na primeira linha da tela. Se o dicionário contém
quaisquer palavras semelhantes, são listados com um número
ao lado de cada uma. Você tem a opção de substituir a palavra
por completo, ou escolhendo uma das palavras sugeridas.
 X	Escreve o resto deste arquivo, ignorando erros de grafia, e inicia o próximo arquivo.
 ^Z	Suspende o programa. Reinicia com o comando fg.
 um erro - %s excede limite do dicionário.
 erro - iconv_open: %s -> %s
 erro - iconv_open: UTF-8 -> %s
 Erro - faltando variável HOME
 i s r s u x y 