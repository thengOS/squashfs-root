��    @        Y         �  *   �  �  �     H  6   M  .   �  �   �     9  �   H     �  ;   
	     F	     N	  
   W	     b	     h	     �	     �	     �	     �	     �	     �	     �	  !   �	     
     
     0
     7
     <
     P
     a
     n
  O   �
     �
     �
     �
  	   �
                 	   (  	   2  	   <     F     S     Y     b     p     x     �     �     �     �  #   �     �               9  !   T     v     �     �     �     �  7    4   ;  �  p     M  6   R  .   �  �   �     P  �   _     &  ?   C     �     �     �     �  "   �     �  	   �     �     �     �       &     (   8     a     z     �     �     �     �     �     �  S   �     S     Z     i  	   q     {     �     �  	   �  
   �  
   �     �     �  
   �     �     �     �       	     6        P     V     h     q     x     �     �     �  
   �     �  :  �  )           /   4                       ?   $   :                      ,                 2         #   ;              <   6   1         %   -   9   @           	      (         +                      8       3      )          5      "                                     *   &   >      .           
       !   0   7       '   =       A matching game played with Mahjongg tiles A solitaire version of the classic Eastern tile game. Tiles are stacked on the board at the start of the game. The goal is to remove all the tiles in as little time as possible. Select two matching tiles and they will disappear from the board, but you can only select a tile if there is an empty space to its left or right at the same level. Be careful: tiles that look alike may actually be slightly different. Date Disassemble a pile of tiles by removing matching pairs Do you want to start a new game with this map? Each puzzle has at least one solution.  You can undo your moves and try and find the solution, restart this game, or start a new one. GNOME Mahjongg GNOME Mahjongg features a variety of starting layouts, some easy and some difficult. If you get stuck, you can ask for a hint, but this adds a large time penalty. Height of the window in pixels If you continue playing the next game will use the new map. Layout: Mahjongg Main game: Maps: Match tiles and clear the board Moves Left: New Game OK Pause the game Paused Preferences Print release version and exit Receive a hint for your next move Redo your last move There are no more moves. Tiles: Time Undo your last move Unpause the game Use _new map Width of the window in pixels You can also try to reshuffle the game, but this does not guarantee a solution. _About _Background color: _Close _Contents _Continue playing _Help _Layout: _Mahjongg _New Game _New game _Preferences _Quit _Restart _Restart Game _Scores _Shuffle _Theme: _Undo game;strategy;puzzle;board; mahjongg map nameCloud mahjongg map nameConfounding Cross mahjongg map nameDifficult mahjongg map nameEasy mahjongg map nameFour Bridges mahjongg map nameOverpass mahjongg map namePyramid's Walls mahjongg map nameRed Dragon mahjongg map nameThe Ziggurat mahjongg map nameTic-Tac-Toe translator-credits true if the window is maximized Project-Id-Version: gnome-mahjongg
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-mahjongg&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:17+0000
PO-Revision-Date: 2015-12-04 15:34+0000
Last-Translator: Felipe Braga <fbobraga@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:21+0000
X-Generator: Launchpad (build 18115)
X-Project-Style: gnome
Language: pt_BR
 Um jogo de memória jogado com as pedras do Mahjongg Uma versão do jogo de paciência do clássico jogo oriental de pedras. As pedras são empilhadas em um tabuleiro no começo do jogo. O objetivo é remover todas as pedras no menor espaço de tempo possível. Selecione duas pedras correspondentes e elas vão desaparecer do tabuleiro, mas você pode selecionar apenas uma pedra, se houver um espaço vazio à sua esquerda ou direita no mesmo nível. Tenha cuidado: pedras parecidas podem, na verdade, ser levemente diferentes. Data Desmonte uma pilha de pedras removendo os pares iguais Você quer iniciar um novo jogo com este mapa? Cada enigma tem pelo menos uma solução. Você pode desfazer suas jogadas e tentar encontrar a solução, reiniciar este jogo ou iniciar um novo jogo. GNOME Mahjongg GNOME Mahjongg possui uma variedade de layouts de início, alguns fáceis e alguns difíceis. Se você ficar sem saída, você pode pedir uma dica, mas isso adicionar uma grande penalidade de tempo. A altura da janela em pixels Se você continuar jogando, o próximo jogo usará o novo mapa. Layout: Mahjongg Jogo principal: Mapas: Combine pedras e limpe o tabuleiro Jogadas restantes: Novo jogo OK Pausa o jogo Pausado Preferências Imprime a versão de lançamento e sai Recebe uma dica para sua próxima jogada Refaz sua última jogada Não há mais jogadas válidas. Pedras: Temp Desfaz sua última jogada Continua o jogo Usar _novo mapa A largura da janela em pixels Você também pode tentar reembaralhar o jogo, mas isto não garante uma solução. _Sobre Cor de _fundo: _Fechar S_umário _Continuar jogando Aj_uda _Layout: _Mahjongg _Novo jogo _Novo jogo _Preferências Sai_r _Reiniciar _Reiniciar jogo _Pontuações _Embaralhar _Tema: Desfa_zer jogo;game;estratégia;quebra-cabeça;enigma;tabuleiro; Nuvem Travessia Confusa Difícil Fácil Quatro Pontes Passagem elevada Muros da Pirâmide Dragão Vermelho O Ziggurat Jogo da Velha Sandro Nunes Henrique <sandro@conectiva.com.br>
Cândida Nunes da Silva <candida@zaz.com.br>
Gustavo Noronha Silva <kov@debian.org>
Alexandre Folle de Menezes <afmenez@terra.com.br>
Maurício de Lemos Rodrigues Collares Neto <mauricioc@myrealbox.com>
Welther José O. Esteves <weltherjoe@yahoo.com.br>
Raphael Higino <In memoriam>
Og Maciel <ogmaciel@ubuntu.com>
Andre Noel <andrenoel@ubuntu.com>
Vladimir Melo <vmelo@gnome.org>
Daniel S. Koda <danielskoda@gmail.com>
Felipe Borges <felipe10borges@gmail.com>
Sérgio Cipolla <secipolla@gmail.com>
Rodrigo Padula de Oliveira <contato@rodrigopadula.com>
Rafael Ferreira <rafael.f.f1@gmail.com>

Launchpad Contributions:
  Enrico Nicoletto https://launchpad.net/~liverig
  Felipe Braga https://launchpad.net/~fbobraga-gmail
  Rafael Fontenelle https://launchpad.net/~rffontenelle verdadeiro se a janela estiver maximizada 