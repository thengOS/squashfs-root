��          �      �       0  %   1     W     s     �     �  "   �  1   �  3     .   J  7   y  0   �  -   �  �    .   �     �          '     D  '   _  E   �  G   �  C     P   Y  I   �  F   �                                         	             
    Check if the package system is locked Get current global keyboard Get current global proxy Set current global keyboard Set current global proxy Set current global proxy exception System policy prevents querying keyboard settings System policy prevents querying package system lock System policy prevents querying proxy settings System policy prevents setting global keyboard settings System policy prevents setting no_proxy settings System policy prevents setting proxy settings Project-Id-Version: ubuntu-system-service
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-11-04 00:35+0000
PO-Revision-Date: 2012-09-20 02:04+0000
Last-Translator: Rafael Neri <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:35+0000
X-Generator: Launchpad (build 18115)
 Verique se o sistema de pacote está bloqueado Obter teclado global atual Obter proxy global atual Definir teclado global atual Definir proxy global atual Definir excessão global de proxy atual Política do sistema impede a consulta às configurações de teclado Política do sistema impede a consulta ao bloqueio do sistema de pacote Política do sistema impede a consulta às configurações de proxy Política do sistema impede a definição das configurações globais de teclado Política do sistema impede a definição das configurações de no_proxy Política do sistema impede a definição das configurações de proxy 