��    a      $  �   ,      8     9  )   I     s     �     �     �     �     �     	  '   	  (   F	     o	     �	  0   �	     �	     �	     �	     
     5
     L
  8   b
  6   �
  $   �
  %   �
  %        C  	   Z  +   d  !   �  6   �     �  (   	  J   2  ,   }     �  #   �     �     �  (     )   ;  W   e  O   �  /     7   =  #   u     �  )   �  %   �  8     -   @  '   n  &   �  "   �  5   �  -     4   D  3   y  %   �  ,   �  "         #  A   ?  !   �     �     �  
   �     �  	   �  L   �  !   *     L     [     i     �  I   �  1   �          1     H     _  *   x     �     �     �  
   �     �     �  3   �     
                    #     (     G     M  �  T       4   4  ,   i  .   �  %   �  '   �          /     M  2   _  .   �      �     �  :   �     6     U     a     {     �     �  9   �  7     (   C  )   l  )   �     �     �  <   �  *   $  D   O  $   �  *   �  N   �  4   3     h  0   �     �     �  3   �  0   "  [   S  [   �  4     9   @  1   z  &   �  ;   �  8     B   H  @   �  +   �  .   �  '   '  J   O  :   �  4   �  A   
   5   L   >   �   2   �   '   �   W   !  "   t!     �!     �!  
   �!     �!     �!  M   �!  %   '"     M"     \"     j"     �"  Y   �"  2   �"  !   -#     O#     f#     }#  *   �#     �#     �#     �#  
   �#     �#     �#  3   �#     -$     2$     7$  
   @$     K$  *   Q$     |$     �$     U   :       Z              R          -                 )               +   %   C   D   @         B   H       J       T   [      6   >   7   4   	   `   ;   O   <       .   /   5               9           3       &             E         A   L   I   _      Y   a      2       \           V              '   ^   1       W   M       P          $   "   #   F       ,   
             =   Q   X   G                 *   8       ]       S           0          N       (   ?      K         !           			Call graph

 		     Call graph (explanation follows)

 	%d basic-block count record
 	%d basic-block count records
 	%d call-graph record
 	%d call-graph records
 	%d histogram record
 	%d histogram records
 


flat profile:
 

Top %d Lines:

     Line      Count

 
%9lu   Total number of line executions
 
Each sample counts as %g %s.
 
Execution Summary:

 
granularity: each sample hit covers %ld byte(s)  <cycle %d as a whole> [%d]
  <cycle %d>  for %.2f%% of %.2f %s

  for %.2f%% of %.2f seconds

  no time accumulated

  no time propagated

 %6.6s %5.5s %7.7s %11.11s %7.7s %7.7s     <spontaneous>
 %6.6s %5.5s %7.7s %7.7s %7.7s %7.7s     <spontaneous>
 %9.2f   Average executions per line
 %9.2f   Percent of the file executed
 %9ld   Executable lines in this file
 %9ld   Lines executed
 %c%c/call %s: %s: found bad tag %d (file corrupted?)
 %s: %s: not in executable format
 %s: %s: unexpected EOF after reading %u of %u samples
 %s: %s: unexpected end of file
 %s: -c not supported on architecture %s
 %s: Only one of --function-ordering and --file-ordering may be specified.
 %s: address size has unexpected value of %u
 %s: can't do -c
 %s: can't find .text section in %s
 %s: could not locate `%s'
 %s: could not open %s.
 %s: debugging not supported; -d ignored
 %s: different scales in histogram records %s: dimension abbreviation changed between histogram records
%s: from '%c'
%s: to '%c'
 %s: dimension unit changed between histogram records
%s: from '%s'
%s: to '%s'
 %s: don't know how to deal with file format %d
 %s: file '%s' does not appear to be in gmon.out format
 %s: file `%s' has bad magic cookie
 %s: file `%s' has no symbols
 %s: file `%s' has unsupported version %d
 %s: file too short to be a gmon file
 %s: found a symbol that covers several histogram records %s: gmon.out file is missing call-graph data
 %s: gmon.out file is missing histogram
 %s: incompatible with first gmon file
 %s: overlapping histogram records
 %s: profiling rate incompatible with first gmon file
 %s: ran out room for %lu bytes of text space
 %s: somebody miscounted: ltab.len=%d instead of %ld
 %s: sorry, file format `prof' is not yet supported
 %s: unable to parse mapping file %s.
 %s: unexpected EOF after reading %d/%d bins
 %s: unknown demangling style `%s'
 %s: unknown file format %s
 %s: warning: ignoring basic-block exec counts (use -l or --line)
 %s:%d: (%s:0x%lx) %lu executions
 %time *** File %s:
 <cycle %d> <indirect child> <unknown> Based on BSD gprof, copyright 1983 Regents of the University of California.
 File `%s' (version %d) contains:
 Flat profile:
 GNU gprof %s
 Index by function name

 Report bugs to %s
 This program is free software.  This program has absolutely no warranty.
 [cg_tally] arc from %s to %s traversed %lu times
 [find_call] %s: 0x%lx to 0x%lx
 [find_call] 0x%lx: bsr [find_call] 0x%lx: jal [find_call] 0x%lx: jalr
 [find_call] 0x%lx: jsr%s <indirect_child>
 called calls children cumulative descendants index index %% time    self  children    called     name
 name parents self self   time time is in ticks, not seconds
 total total  Project-Id-Version: gprof 2.12.1
Report-Msgid-Bugs-To: bug-binutils@gnu.org
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2016-05-30 16:27+0000
Last-Translator: Alexandre Folle de Menezes <Unknown>
Language-Team: Brazilian Portuguese <ldp-br@bazar.conectiva.com.br>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:03+0000
X-Generator: Launchpad (build 18115)
 			Gráfico de chamadas

 		     Gráfico de chamadas (explicação adiante)

 	%d registro de contagem de blocos básicos
 	%d registros de contagens de blocos básicos
 	%d registro de gráfico de chamadas
 	%d registros de gráficos de chamadas
 	%d registro de histograma
 	%d registros de histogramas
 


perfil plano:
 

%d Linhas Principais:

     Linha     Contador

 
%9lu   Número total de execuções de linha
 
Cada amostra conta como %g %s.
 
Resumo da Execução:

 
granularidade: cada elemento de amostra cobre %ld byte(s)  <ciclo %d como um todo> [%d]
  <ciclo %d>  para %.2f%% de %.2f %s

  para %.2f%% de %.2f segundos

  não há tempo acumulado

  nenhum tempo propagado

 %6.6s %5.5s %7.7s %11.11s %7.7s %7.7s     <espontâneos>
 %6.6s %5.5s %7.7s %11.11s %7.7s %7.7s   <espontâneos>
 %9.2f   Média de execuções por linha
 %9.2f   Percentagem executada do arquivo
 %9ld   Linhas executáveis neste arquivo
 %9ld   Linhas executadas
 %c%c/chamada %s: %s: marca %d inválida encontrada (arquivo corrompido?)
 %s: %s: não está no formato executável
 %s: %s: final de arquivo inesperado depois de ler %u de %u amostras
 %s: %s: final de arquivo inesperado
 %s: -c não tem suporte na arquitetura %s
 %s: Apenas um de --function-ordering e --file-ordering pode ser especificado.
 %s: endereço tem tamanho de valor inesperado de %u
 %s: impossível fazer -c
 %s: impossível encontrar a seção .text em %s
 %s: impossível encontrar `%s'
 %s: impossível abrir %s.
 %s: não há suporte para depuração; -d ignorado
 %s diferentes escalas no registro de histogramas %s: dimensões abreviadas diferentes entre histograma e registro
%s: de '%c'
%s: para '%c'
 %s: unidade de dimensões diferentes entre histograma e registro
%s: de '%s'
%s: para '%s'
 %s: não sei como lidar com o arquivo de formato %d
 %s: o arquivo '%s' não parece estar no formato gmon.out
 %s: o arquivo `%s' tem um magic cookie inválido
 %s: o arquivo `%s' não tem símbolos
 %s: o arquivo `%s' tem a versão %d, que não é suportada
 %s: o arquivo é muito pequeno para ser um arquivo gmon
 %s: encontrado um símbolo que abrange vários registos histograma %s: faltam os dados do gráfico de chamadas do arquivo gmon.out
 %s: falta o histograma do arquivo gmon.out
 %s: incompatível com o primeiro arquivo gmon
 %s sopreponto registros de histogramas
 %s: taxa de análises de perfil incompatível com o primeiro arquivo gmon
 %s: terminou o espaço para %lu bytes de espaço de texto
 %s: alguém contou mal: ltab.len=%d em lugar de %ld
 %s: perdão, o formato de arquivo `prof' ainda não é suportado
 %s: impossível analisar o arquivo de mapeamento %s.
 %s: final de arquivo inesperado depois de ler %d/%d binários
 %s: estilo de desembaralhamento desconhecido `%s'
 %s: formato de arquivo %s desconhecido
 %s: aviso: ignorando os contadores de execução de blocos básicos (use -l ou --line)
 %s:%d: (%s:0x%lx) %lu execuções
 %time *** Arquivo %s:
 <ciclo %d> <filho indireto> <desconhecido> Basado no BSD gprof, copyright 1983 Regents of the University of California.
 O arquivo `%s' (versão %d) contém:
 Perfil plano:
 GNU gprof %s
 Índice por nome de função

 Reportar bugs para %s
 Este programa é software livre.  Este programa não tem absolutamente nenhuma garantia.
 [cg_tally] arco de %s até %s percorido %lu vezes
 [find_call] %s: 0x%lx até 0x%lx
 [find_call] 0x%lx: bsr [find_call] 0x%lx: jal [find_call] 0x%lx: jalr
 [find_call] 0x%lx: jsr%s <filho_indireto>
 chamado chamadas filhos cumulativo descendentes índice ind   %% tempo  si_mesmo filhos    chamado    nome
 nome pais si mesmo si mesmo   tempo o tempo está em tiques, não em segundos
 total total  