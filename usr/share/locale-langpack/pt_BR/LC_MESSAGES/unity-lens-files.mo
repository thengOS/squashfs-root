��    C      4  Y   L      �     �  
   �     �     �     �     �     �     �     �     �     �     �          
       	     	   &     0     >     D     M     ]     m     |     �     �     �     �     �     �     �     �     �     �     �     �  	   �     �                               %  	   +     5     :     H     O     X  	   o     y     �  <   �     �  �   �     �     �     �     �     �     �     	     	  	   	  	   	  �  !	     �
     �
     �
     �
          
                         *     0     7  	   >     H  
   Q  	   \  	   f     p  	   w     �     �     �     �     �     �     �     �     �                    $     *     ;     K     a     m     t     y     �     �     �     �     �     �     �     �     �     �     �     �       J        b  �   j     W     j          �     �     �     �     �     �     �     @   8          %      (   C       "   B   .       :           4   #       /          9          <   )                   1      $   ?       =         5   +         A   '          ;   2   0   ,   -       &                                *                             3      
   7                 >   	          6       !       %s, %u items 1 hour ago 100MB 100kB 10MB 1GB 1MB 1kB >1GB A month ago April Audio August Contents December Documents Downloads Earlier today Email February Files & Folders Filesystem type Five hours ago Folders Format Four hours ago Friday Home Images Invalid Month January July June Last 30 days Last 7 days Last modified Last year March May Monday November October Open Other Past hour Path Presentations Recent Saturday Search Files & Folders September Show in Folder Size Sorry, there are no files or folders that match your search. Sunday This is an Ubuntu search plugin that enables local files to be searched and displayed in the Dash underneath the Files & Folders header. If you do not wish to search this content source, you can disable this search plugin. Three hours ago Three weeks ago Thursday Total capacity %s Tuesday Two hours ago Type Videos Wednesday Yesterday Project-Id-Version: unity-lens-files
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-17 18:41+0000
PO-Revision-Date: 2016-03-22 20:26+0000
Last-Translator: Paulo Guzmán <white.hat@msn.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:05+0000
X-Generator: Launchpad (build 18115)
 %s, %u items Uma hora atrás 100MB 100kB 10MB 1GB 1MB 1kB >1GB Um mês atrás Abril Áudio Agosto Conteúdo Dezembro Documentos Downloads Hoje cedo E-mail Fevereiro Arquivos e pastas Tipo do sistema de arquivos Cinco horas atrás Pastas Formato Quatro horas atrás Sexta-feira Pasta pessoal Imagens Mês inválido Janeiro Julho Junho Últimos 30 dias Últimos 7 dias Última modificação Ano passado Março Maio Segunda-feira Novembro Outubro Abrir Outro Última hora Caminho Apresentações Recentes Sábado Pesquisar arquivos e pastas Setembro Exibir na pasta Tamanho Desculpe, não existem arquivos ou pastas correspondentes à sua pesquisa. Domingo Este é um plugin de busca do Ubuntu que possibilita a pesquisa em arquivos locais e os exibe no Painel logo abaixo do título Arquivos e Pastas. Se você não deseja pesquisar esse tipo de conteúdo, você pode desabilitar esse plugin. Três horas atrás Três semanas atrás Quinta-feira Capacidade total %s Terça-feira Duas horas atrás Tipo Vídeos Quarta-feira Ontem 