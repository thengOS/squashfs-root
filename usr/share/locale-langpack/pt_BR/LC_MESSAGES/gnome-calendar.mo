��    x      �  �   �      (
     )
     I
     O
     U
     k
     q
     z
     |
     
     �
     �
     �
     �
     �
  *   �
     �
               !     <     M     _     n     �     �     �  	   �     �     �     �     �  $   �  )        :     >     K     \     s     x     �     �  �   �     Q     _     l  �   x     W     ^     {     �     �     �     �     �     �     �  	   �                          $     '     7      <  &   ]     �     �     �     �     �     �     �     �     �            '   &     N     U     [     a     x  >   �     �     �     �  )   �       �   "     �     �     
          5     A     a     f     m  
   s     ~     �     �     �     �     �     �          /     J     d     ~     �     �     �     �               1  h  D  #   �     �     �     �     �     �                    (  	   .     8     I     ^  0   r  
   �     �     �     �     �     �          '     7     O     ^     v     ~     �     �     �  $   �  )   �     �     �               5     ;     I     \  �   d     *     ;  	   I  �   S     :     A     ]     k     �  
   �     �     �     �     �     �     �            	        &     -     ;     A  (   _  (   �     �     �     �     �     �     �                     @  2   P     �     �     �     �     �  >   �               !  /   1     a  �   j     F     X     t     �     �  %   �     �     �     �     �     �     	                  )      /      6      ?      O      [      g      w   	   �      �      �      �      �      �   |  �                    C   .       p   a   #   i   E      "   2   9   8                 &   +       *   n                       t      N   P      V   K   
          ]   G   m                      A   o   5   \   U              -   l   w   s              7      x       ?           g   Z           /   1   >   (      r         M       W              '   _   ^      e       [           B   Y      @           %       k      X   :   L   Q   F   b   )   c   J   v       O       H   $       <       f   0   ;       6   `   !   S               	   ,   j   R           =   q      3      T      D       d                 4   u   I   h    %s (this calendar is read-only) %s AM %s PM - Calendar management 00:00 00:00 PM : AM Access, and manage calendar Account Add Add Calendar Add Eve_nt… Add Event… Add new events to this calendar by default All day Another event deleted Calendar Calendar <b>%s</b> removed Calendar Address Calendar Settings Calendar files Calendar for GNOME Calendar management Calendar name Calendar;Event;Reminder; Calendars Cancel Click to set up Color Connect Copyright © %d The Calendar authors Copyright © %d–%d The Calendar authors Day Delete event Display calendar Display version number Done Edit Calendar Edit Details… Ends Enter the address of the calendar that you want to add. If the calendar belongs to one of your online accounts, you can add it through the <a href="GOA">online account settings</a>. Event deleted From File… From Web… GNOME Calendar is a simple and beautiful calendar application designed to perfectly fit the GNOME desktop. By reusing the components which the GNOME desktop is build on, Calendar nicely integrates with the GNOME ecosystem. Google List of the disabled sources Location Manage your calendars Microsoft Exchange Midnight Month New Event from %s to %s New Event on %s New Local Calendar… No events No results found Noon Notes Off On Online Accounts Open Open calendar on the passed date Open calendar showing the passed event Open online account settings Other event Other %d events Other events Overview PM Password Remove Calendar Save Search for events Select a calendar file Settings Sources disabled last time Calendar ran Starts Title Today Try a different search Type of the active view Type of the active window view, default value is: monthly view Undo Unnamed Calendar Unnamed event Use the entry above to search for events. User We aim to find the perfect balance between nicely crafted features and user-centred usability. No excess, no lacks. You'll feel comfortable using Calendar, like you've been using it for ages! Window maximized Window maximized state Window position Window position (x and y). Window size Window size (width and height). Year _About _Quit _Search… _Synchronize event date format%B %d ownCloud shortcut windowClose window shortcut windowGeneral shortcut windowGo back shortcut windowGo forward shortcut windowMonth view shortcut windowNavigation shortcut windowNew event shortcut windowNext view shortcut windowPrevious view shortcut windowSearch shortcut windowShortcuts shortcut windowShow help shortcut windowShow today shortcut windowView shortcut windowYear view translator-credits Project-Id-Version: gnome-calendar master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-calendar&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-05-18 08:25+0000
PO-Revision-Date: 2016-05-25 20:01+0000
Last-Translator: Antonio Fernandes C. Neto <Unknown>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 19:34+0000
X-Generator: Launchpad (build 18115)
X-Project-Style: gnome
Language: pt_BR
 %s (esta agenda é somente-leitura) %s AM %s PM - Gerenciamento de agenda 00:00 00:00 PM : AM Acesse e gerencie sua agenda Conta Adicionar Adicionar agenda Adicionar eve_nto… Adicionar evento… Adiciona novos eventos a este agenda por padrão O dia todo Outro evento excluído Agenda Agenda <b>%s</b> removida Endereço da agenda Configurações da agenda Arquivos de agenda Agenda do GNOME Gerenciamento de agenda Nome da agenda Agenda;Evento;Lembrete; Agendas Cancelar Clique para configurar Cor Conectar Copyright © %d Os autores do Agenda Copyright © %d–%d Os autores do Agenda Dia Excluir evento Exibir agenda Exibir número da versão Feito Editar agenda Editar detalhes… Termina Digite o endereço da agenda que você deseja adicionar. Se a agenda pertence a uma de suas contas on-line, você pode adicioná-la através das <a href="GOA">configurações de contas on-line</a>. Evento excluído De arquivo… Da Web… O GNOME Agenda é um aplicativo de agenda simples e bonito projetado para se encaixar perfeitamente ao ambiente GNOME. Por reutilizar os componentes com o qual GNOME é construído, Agenda se integra muito bem à esse ecossistema. Google Lista de fontes desativadas Localização Gerenciar suas agendas Microsoft Exchange Meia-noite Mês Novo evento de %s até %s Novo evento em %s Nova agenda local… Nenhum evento Nenhum resultado localizado Tarde Notas Desligado Ligado Contas online Abrir Abrir agenda na data anterior Abrir agenda mostrando o evento anterior Abre as configurações de contas online Outro evento Outros %d eventos Outros eventos Visão geral PM Senha Remover agenda Salvar Pesquisa por eventos Selecionar um arquivo de agenda Configurações Fontes desativadas na última execução do Agenda Inicia Título Hoje Tente uma pesquisa diferente Tipo da visão ativa Tipo da visão da janela ativa, sendo o padrão: visão mensal Desfazer Agenda sem nome Evento sem nome Use a entrada acima para pesquisar por eventos. Usuário Nosso objetivo é alcançar o balanço perfeito entre funcionalidades bem construídas e usabilidade focada no usuário. Sem excessos, sem faltas. Você se sentirá confortável usando Agenda, como se usasse-o por anos! Janela maximizada Estado de janela maximizada Posição da janela Posição da janela (x e y). Tamanho da janela Tamanho da janela (largura e altura). Ano _Sobre Sai_r Pes_quisar… _Sincronizar %d de %B ownCloud Fechar janela Geral Voltar Avançar Visão por mês Navegação Novo evento Próxima visão Visão anterior Pesquisar Atalhos Mostrar ajuda Mostrar hoje Visão Visão por ano Rafael Ferreira <rafael.f.f1@gmail.com>, 2012
Enrico Nicoletto <liverig@gmail.com>, 2014, 2015, 2016
Gabriel Speckhahn <gabspeck@gmail.com>, 2015.
Fábio Nogueira <fnogueira@gnome.org>, 2016.

Launchpad Contributions:
  Antonio Fernandes C. Neto https://launchpad.net/~fernandesn
  Enrico Nicoletto https://launchpad.net/~liverig
  Fábio Nogueira https://launchpad.net/~fnogueira 