��          �   %   �      0  '  1  A   Y  @   �  D   �  =   !  :   _  l   �          !  ,   ;     h  %   �  ,   �  -   �        &   (     O     o  t   �  �     �   �  �   7	  0   �	  �  	
  8  �  =   �  G   ;  K   �  >   �  <     d   K     �     �  4   �       %   :  4   `  .   �      �  &   �            -  {   N  �   �  =     �   �  3   h                          
                                                                      	                               		[CDEF:vname=rpn-expression]
		[VDEF:vdefname=rpn-expression]
		[PRINT:vdefname:format]
		[GPRINT:vdefname:format]
		[COMMENT:text]
		[SHIFT:vname:offset]
		[TEXTALIGN:{left|right|justified|center}]
		[TICK:vname#rrggbb[aa][:[fraction][:legend]]]
		[HRULE:value#rrggbb[aa][:legend]]
		[VRULE:value#rrggbb[aa][:legend]]
		[LINE[width]:vname[#rrggbb[aa][:[legend][:STACK]]]]
		[AREA:vname[#rrggbb[aa][:[legend][:STACK]]]]
		[PRINT:vname:CF:format] (deprecated)
		[GPRINT:vname:CF:format] (deprecated)
		[STACK:vname[#rrggbb[aa][:legend]]] (deprecated)
  * cd - changes the current directory

	rrdtool cd new directory
  * ls - lists all *.rrd files in current directory

	rrdtool ls
  * mkdir - creates a new directory

	rrdtool mkdir newdirectoryname
  * pwd - returns the current working directory

	rrdtool pwd
  * quit - closing a session in remote mode

	rrdtool quit
  * resize - alter the length of one of the RRAs in an RRD

	rrdtool resize filename rranum GROW|SHRINK rows
 %s: illegal option -- %c
 %s: invalid option -- %c
 %s: option `%c%s' doesn't allow an argument
 %s: option `%s' is ambiguous
 %s: option `%s' requires an argument
 %s: option `--%s' doesn't allow an argument
 %s: option `-W %s' doesn't allow an argument
 %s: option `-W %s' is ambiguous
 %s: option requires an argument -- %c
 %s: unrecognized option `%c%s'
 %s: unrecognized option `--%s'
 * graph - generate a graph from one or several RRD

	rrdtool graph filename [-s|--start seconds] [-e|--end seconds]
 * graphv - generate a graph from one or several RRD
           with meta data printed before the graph

	rrdtool graphv filename [-s|--start seconds] [-e|--end seconds]
 * restore - restore an RRD file from its XML form

	rrdtool restore [--range-check|-r] [--force-overwrite|-f] filename.xml filename.rrd
 RRDtool is distributed under the Terms of the GNU General
Public License Version 2. (www.gnu.org/copyleft/gpl.html)

For more information read the RRD manpages
 Valid remote commands: quit, ls, cd, mkdir, pwd
 Project-Id-Version: rrdtool
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-04-06 16:14+0000
PO-Revision-Date: 2013-04-06 02:00+0000
Last-Translator: Neliton Pereira Jr. <nelitonpjr@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:05+0000
X-Generator: Launchpad (build 18115)
 		[CDEF:vname=rpn-expression]
		[VDEF:vdefname=rpn-expression]
		[PRINT:vdefname:formato]
		[GPRINT:vdefname:formato]
		[COMMENT:texto]
		[SHIFT:vname:deslocamento]
		[TEXTALIGN:{esquerdo|direito|justificado|centro}]
		[TICK:vname#rrggbb[aa][:[fração][:legenda]]]
		[HRULE:value#rrggbb[aa][:legenda]]
		[VRULE:value#rrggbb[aa][:legenda]]
		[LINE[width]:vname[#rrggbb[aa][:[legenda][:STACK]]]]
		[AREA:vname[#rrggbb[aa][:[legenda][:STACK]]]]
		[PRINT:vname:CF:format] (obsoleto)
		[GPRINT:vname:CF:format] (obsoleto)
		[STACK:vname[#rrggbb[aa][:legenda]]] (obsoleto)
  * cd - muda o diretório atual

	rrdtool cd novo diretório
  * ls - lista todos os arquivos *.rrd do diretório atual

	rrdtool ls
  * mkdir - cria um novo diretório

	rrdtool mkdir nome_do_novo_diretório
  * pwd - retorna o diretório de trabalho atual

	rrdtool pwd
  * quit - encerra uma sessão em modo remoto

	rrdtool quit
  * resize - altera o tamanho de um dos RRAs no RRD

	rrdtool resize arquivo rranum GROW|SHRINK rows
 %s: opção ilegal -- %c
 %s: opção inválida -- %c
 %s: opção `%c%s' não permite o uso de argumentos
 %s: opção `%s' é ambígua
 %s: opção `%s' requer um argumento
 %s: opção `--%s' não permite o uso de argumentos
 %s: opção `-W %s' não permite um argumento
 %s: opção `-W %s' é ambígua
 %s: opção requer um argumento -- %c
 %s: opção desconhecida `%c%s'
 %s: opção desconhecida `--%s'
 * graph - gera um gráfico a partir de um ou vários RRD

	rrdtool graph arquivo [-s|--start segundos] [-e|--end segundos]
 * graphv - gera um gráfico a partir de um ou vários RRD
           com um meta dado impresso antes do gráfico

	rrdtool graphv arquivo [-s|--start segundos] [-e|--end segundos]
 restore - restaura um arquivo RRD a partir de um arquivo XML
 RRDtool é distribuído sob os termos da GNU General
Public License versão 2. (Copyleft www.gnu.org / gpl.html)

Para mais informações leia a página principal de RRD
 Comandos remoto válidos: quit, ls, cd, mkdir, pwd
 