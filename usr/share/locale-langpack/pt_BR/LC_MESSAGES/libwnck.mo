��    �      T  7  �      h     i     n  	   {     �     �     �     �     �  "   �     �     �     �     �          '     :     P     b     q     �  N   �  g   �  G   C  :   �  E   �  A     *   N  *   y  )   �  (   �  7   �  s   /  '   �  ?   �  <        H  ,   Y  $   �     �     �     �  /   �  8   !  \   Z  b   �  d     ]     d   �  f   B  b   �  m     `   z     �  "   �  !     /   8     h     {     �     �  
   �  >   �  G   �  5   3     i     �  `   �  G   �  	   A     K     Y      w  #   �  #   �     �  5   �  !   1   $   S   '   x   '   �   9   �   $   !  )   '!  5   Q!      �!     �!     �!  	   �!     �!     �!  <   "  <   D"  :   �"     �"     �"     �"     #      #     6#     ;#  )   B#  ,   l#  	   �#     �#     �#     �#  #   �#     $  %   $  (   @$  (   i$  +   �$     �$  )   �$     �$  �   %     �%     �%     &     &     )&  *   :&  -   e&  -   �&  0   �&     �&     '  (   '  *   E'     p'  
   �'     �'     �'  &   �'     �'  !   �'     (     #(     7(     G(  "   S(     v(      �(     �(     �(     �(     �(     �(  L   )  8   T)     �)     �)     �)     �)     �)     �)  E   �)     8*     E*     T*  :   b*     �*     �*     �*     �*  <   �*  .   +     H+  	   L+     V+     X+     u+  
   |+     �+     �+     �+     �+     �+     �+     �+     �+     �+      ,     ,     ,     ,     *,  
   0,  
   ;,  
   F,     Q,     Z,     p,  	   �,     �,     �,     �,  	   �,     �,     �,     �,     �,     �,     
-     -     -     -      -     &-  
   --     8-     F-     T-     c-     i-     p-     }-     �-     �-     �-  
   �-     �-     �-  
   �-     �-     �-     �-     .     .     !.  �  0.     �/     �/  	   �/     �/     �/     0     0     0  0   "0  !   S0     u0     �0     �0     �0     �0     �0     �0     1     1     41  j   ;1  �   �1  _   '2  ?   �2  X   �2  k    3  )   �3  )   �3  (   �3  1   	4  @   ;4  t   |4  *   �4  Q   5  O   n5     �5  ,   �5  1   �5     /6  .   I6     x6  +   �6  4   �6  U   �6  X   ?7  \   �7  R   �7  b   H8  f   �8  k   9  t   ~9  a   �9     U:      u:  $   �:  2   �:     �:     ;     ;     ;     3;  B   @;  O   �;  =   �;     <     0<  p   I<  _   �<  
   =     %=  "   6=  4   Y=  %   �=  ,   �=     �=  ?   �=  '   8>  9   `>  *   �>  1   �>  D   �>     <?  9   T?  =   �?  "   �?     �?      @  
   #@     .@     ?@  P   R@  P   �@  S   �@  %   HA  ,   nA  (   �A  -   �A  '   �A     B     B  -   'B  ?   UB  	   �B     �B     �B  $   �B  )   �B     C  4   8C  0   mC  2   �C  >   �C     D  1   D     KD  �   cD     NE     `E     xE     �E     �E  ;   �E  7   �E  9   &F  E   `F     �F  "   �F     �F  '   G     -G     HG  $   TG     yG  0   ~G  &   �G  3   �G     
H      H     7H     HH  "   \H     H      �H  
   �H     �H     �H     �H     �H  w   I  T   �I  0   �I     J     J     )J     EJ     XJ  ]   lJ     �J     �J     �J  Q   K      dK  #   �K     �K     �K  <   �K  6   L     >L  	   BL     LL  '   NL     vL     ~L     �L  !   �L     �L     �L     �L     �L     �L     M     M     <M     CM     UM     hM     zM  
   �M     �M     �M  	   �M     �M     �M  
   �M     �M     N  	   )N  
   3N     >N     DN     UN     nN     uN     �N     �N     �N     �N     �N  	   �N  $   �N     �N     �N     O     O     O     &O     4O  
   RO     ]O     sO  	   �O     �O     �O  	   �O  
   �O     �O     �O     �O     
P     P     �       �   v   |   Q   �   �   	   �   t       �   �           �          �               S   �       �   O           �   �          �       ,   �   �   �      �          <       (           %   `   i   2   �   a   >   g      �           �       /                �       �   �   k   V   A       �   �       �   �   n   "   5                 [      K   H       �   9   �   Z   m      y       q       �   �   �   @   I               f          R           L   �       �   +   $   N   �   =   �   �   �   c   o          &   �       z       �   }   �   U   F   #       )   �   �   �       �               �      �   h           �   7   .       G       u       �       �           �           �       �   �       �          -       Y       �   �   �   �   4   �   �      l   T   �   b   ?   p   �   �          �   r   �   �   �      �   �   '   �   �   �      {   ]      :              
   �       M   �   �       �   �   �   8          �   s   �   �       W   �   �   3   X   �   �   \   !   *       B       P   �                   j   �   �   w                     �   J   �          �   6   �   �   �       �          _   ^   D   ~   0   e   ;   �      �   1   �   E           �   C      d   �       �   x   �    "%s" %1$s%2$s%3$s %d ("%s") %d: %s
 %lu (%s) %lu: %s
 ,  <name unset> <no EWMH-compliant window manager> <no viewport> <unset> Action not allowed
 Activate the window Activate the workspace Active Window: %s
 Active Workspace: %s
 Alias of --window Always On _Top Bottom Neighbor: %s
 CLASS Cannot change the workspace layout on the screen: the layout is already owned
 Cannot interact with application having its group leader with XID %lu: the application cannot be found
 Cannot interact with class group "%s": the class group cannot be found
 Cannot interact with screen %d: the screen does not exist
 Cannot interact with window with XID %lu: the window cannot be found
 Cannot interact with workspace %d: the workspace cannot be found
 Change the X coordinate of the window to X Change the Y coordinate of the window to Y Change the height of the window to HEIGHT Change the name of the workspace to NAME Change the number of workspaces of the screen to NUMBER Change the type of the window to TYPE (valid values: normal, desktop, dock, dialog, toolbar, menu, utility, splash) Change the width of the window to WIDTH Change the workspace layout of the screen to use NUMBER columns Change the workspace layout of the screen to use NUMBER rows Class Group: %s
 Class resource of the class group to examine Click this to switch to workspace %s Click to start dragging "%s" Click to switch to "%s" Close the window Conflicting options are present: --%s and --%s
 Conflicting options are present: --%s or --%s, and --%s
 Conflicting options are present: a window should be interacted with, but --%s has been used
 Conflicting options are present: an application should be interacted with, but --%s has been used
 Conflicting options are present: class group "%s" should be interacted with, but --%s has been used
 Conflicting options are present: screen %d should be interacted with, but --%s has been used
 Conflicting options are present: windows of an application should be listed, but --%s has been used
 Conflicting options are present: windows of class group "%s" should be listed, but --%s has been used
 Conflicting options are present: windows of workspace %d should be listed, but --%s has been used
 Conflicting options are present: windows or workspaces of screen %d should be listed, but --%s has been used
 Conflicting options are present: workspace %d should be interacted with, but --%s has been used
 Current workspace: "%s" Error while parsing arguments: %s
 Geometry (width, height): %d, %d
 Geometry (x, y, width, height): %d, %d, %d, %d
 Group Leader: %lu
 Group Name: %s
 HEIGHT Icon Name: %s
 Icons: %s
 Invalid argument "%d" for --%s: the argument must be positive
 Invalid argument "%d" for --%s: the argument must be strictly positive
 Invalid argument "%s" for --%s, valid values are: %s
 Invalid value "%s" for --%s Left Neighbor: %s
 List windows of the application/class group/workspace/screen (output format: "XID: Window Name") List workspaces of the screen (output format: "Number: Workspace Name") Ma_ximize Ma_ximize All Make the window always on top Make the window appear in pagers Make the window appear in tasklists Make the window below other windows Make the window fullscreen Make the window have a fixed position in the viewport Make the window not always on top Make the window not appear in pagers Make the window not appear in tasklists Make the window not below other windows Make the window not have a fixed position in the viewport Make the window quit fullscreen mode Make the window visible on all workspaces Make the window visible on the current workspace only Maximize horizontally the window Maximize the window Maximize vertically the window Mi_nimize Mi_nimize All Minimize the window Move the viewport of the current workspace to X coordinate X Move the viewport of the current workspace to Y coordinate Y Move the window to workspace NUMBER (first workspace is 0) Move to Another _Workspace Move to Workspace R_ight Move to Workspace _Down Move to Workspace _Left Move to Workspace _Up NAME NUMBER NUMBER of the screen to examine or modify NUMBER of the workspace to examine or modify Name: %s
 No Windows Open Number of Windows: %d
 Number of Workspaces: %d
 On Screen: %d (Window Manager: %s)
 On Workspace: %s
 Options to list windows or workspaces Options to modify properties of a screen Options to modify properties of a window Options to modify properties of a workspace PID: %s
 Position in Layout (row, column): %d, %d
 Possible Actions: %s
 Print or modify the properties of a screen/workspace/window, or interact with it, following the EWMH specification.
For information about this specification, see:
	http://freedesktop.org/wiki/Specifications/wm-spec Resource Class: %s
 Right Neighbor: %s
 Screen Number: %d
 Session ID: %s
 Shade the window Show options to list windows or workspaces Show options to modify properties of a screen Show options to modify properties of a window Show options to modify properties of a workspace Show the desktop Showing the desktop: %s
 Start moving the window via the keyboard Start resizing the window via the keyboard Startup ID: %s
 State: %s
 Stop showing the desktop TYPE Tool to switch between visible windows Tool to switch between windows Tool to switch between workspaces Top Neighbor: %s
 Transient for: %lu
 Un_minimize All Unma_ximize Unmaximize horizontally the window Unmaximize the window Unmaximize vertically the window Unmi_nimize Unminimize the window Unshade the window Untitled application Untitled window Viewport cannot be moved: the current workspace does not contain a viewport
 Viewport cannot be moved: there is no current workspace
 Viewport position (x, y): %s
 WIDTH Window List Window Manager: %s
 Window Selector Window Type: %s
 Window cannot be moved to workspace %d: the workspace does not exist
 Workspace %d Workspace %s%d Workspace 1_0 Workspace Layout (rows, columns, orientation): %d, %d, %s
 Workspace Name: %s
 Workspace Number: %d
 Workspace Switcher X X window ID of the group leader of an application to examine X window ID of the window to examine or modify XID XID: %lu
 Y _Always on Visible Workspace _Close _Close All _Move _Only on This Workspace _Resize _Unmaximize All above all workspaces below change fullscreen mode change workspace close desktop dialog window dock or panel false fullscreen make above make below maximize maximize horizontally maximize vertically maximized maximized horizontally maximized vertically minimize minimized move needs attention no action possible normal normal window pin pinned resize set shade shaded skip pager skip tasklist splash screen startupIDnone stick sticky tearoff menu tearoff toolbar true unmake above unmake below unmaximize unmaximize horizontally unmaximize vertically unminimize unpin unshade unstick utility window windownone workspacenone Project-Id-Version: libwnck 2.22
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 23:36+0000
PO-Revision-Date: 2011-01-16 18:58+0000
Last-Translator: André Gondim <Unknown>
Language-Team: Portuguese/Brazil <gnome-l10n-br@listas.cipsga.org.br>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:08+0000
X-Generator: Launchpad (build 18115)
 "%s" %1$s%2$s%3$s %d ("%s") %d: %s
 %lu (%s) %lu: %s
 ,  <nome indefinido> <nenhum gerenciador de janelas EWMH-compatível> <nenhuma porta de visualização> <não definido> Ação não permitida
 Ativar a janela Ativar o espaço de trabalho Janela atual: %s
 Espaço de trabalho atual: %s
 Apelido de --window Sempre no _topo Vizinho inferior: %s
 CLASSE Não é possível modificar a disposição do espaço de trabalho na tela: a disposição já possui dono
 Não foi possível interagir com o aplicativo que possui um grupo principal com XID %lu: o aplicativo não pôde ser localizado
 Não foi possível interagir com o grupo classe "%s": o grupo classe não pôde ser localizado
 Não foi possível interagir com a tela %d: a tela não existe
 Não foi possível interagir com janela com XID %lu: a janela não pôde ser localizada
 Não foi possível interagir com o espaço de trabalho %d: o espaço de trabalho não pôde ser localizado
 Modificar a coordenada X da janela para X Modificar a coordenada Y da janela para Y Modificar a altura da janela para ALTURA Modificar o nome do espaço de trabalho para NOME Modificar o número de espaços de trabalho da tela para NÚMERO Alterar o tipo da janela para TIPO (valores válidos: normal, desktop, dock, dialog, toolbar, menu, utility, splash) Modificar a largura da janela para LARGURA Modificar a disposição do espaço de trabalho da tela para usar NÚMERO colunas Modificar a disposição do espaço de trabalho da tela para usar NÚMERO filas Grupo classe: %s
 Classe fonte da classe grupo a ser examinada Clique aqui para ir para o espaço de trabalho %s Clique para arrastar "%s" Clique para ir para o espaço de trabalho "%s" Fechar a janela Opções em conflito presente: --%s e --%s
 Opções em conflito presente: --%s ou --%s, e --%s
 Opções em conflito presente: uma janela deveria ser interagida, mas --%s foi usado
 Opções em conflito presente: um aplicativo deveria ser interagida, mas --%s foi usado
 Opções em conflito presente: grupo classe "%s" deveria ser interagida, mas --%s foi usado
 Opções em conflito presente: tela %d deveria ser interagida, mas --%s foi usado
 Opções em conflito presente: janelas de um aplicativo deveriam ser listadas, mas --%s foi usado
 Opções em conflito presente: janelas do grupo classe "%s" deveriam ser listadas, mas --%s foi usado
 Opções em conflito presente: janelas do espaço de trabalho %d deveriam ser listadas, mas --%s foi usado
 Opções em conflito presente: janelas ou espaços de trabalho da tela %d deveriam ser listados, mas --%s foi usado
 Opções em conflito presente: espaço de trabalho %d deveria ser interagido, mas --%s foi usado
 Espaço de trabalho atual: "%s" Erro ao analizar argumentos: %s
 Geometria (largura, altura): %d, %d
 Geometria (x, y, largura, altura): %d, %d, %d, %d
 Grupo principal: %lu
 Nome do grupo: %s
 ALTURA Nome minimizado: %s
 Ícones: %s
 Argumento "%d" inválido para --%s: o argumento deve ser positivo
 Argumento "%d" inválido para --%s: o argumento deve ser estritamente positivo
 Argumento"%s" inválido para --%s, valores válidos são: %s
 Valor "%s" inválido para --%s Vizinho à esquerda: %s
 Listar janelas do grupo/espaço de trabalho/tela do aplicativo/classe (formato de saída: "XID: Nome da Janela") Listar espaços de trabalho da tela (formato de saída: "Número: Nome do Espaço de Trabalho") Ma_ximizar Ma_ximizar todas Posicionar a janela sempre em cima Mostrar a janela em seletores de espaço de trabalho Mostrar a janela em listas de tarefas Posicionar a janela abaixo de outras janelas Usar o modo tela cheia Posicionar a janela em um local fixo na porta de visualização Não posicionar a janela sempre em cima Não mostrar a janela em seletores de espaço de trabalho Não mostrar a janela em listas de tarefas Não posicionar a janela abaixo de outras janelas Não posicionar a janela em um local fixo na porta de visualização Sair do modo tela cheia Tornar a janela visível em todos os espaços de trabalho Tornar a janela visível somente no espaço de trabalho atual Maximizar horizontalmente a janela Maximizar a janela Maximizar verticalmente a janela Mi_nimizar Mi_nimizar todas Minimizar a janela Mover a porta de visualização do espaço de trabalho atual para X coordenada X Mover a porta de visualização do espaço de trabalho atual para Y coordenada Y Mover a janela para espaço de trabalho NÚMERO (primeiro espaço de trabalho é 0) Mover para _outro espaço de trabalho Mover para o espaço de trabalho à _direita Mover para o espaço de trabalho a_baixo Mover para o espaço de trabalho à _esquerda Mover para o espaço de trabalho a_cima NOME NÚMERO NÚMERO da tela a ser examinada ou modificada NÚMERO de espaços de trabalho a ser examinados ou modificados Nome: %s
 Nenhuma Janela Aberta Número de janelas: %d
 Número de espaços de trabalho: %d
 Na tela: %d (Gerenciador de Janelas: %s)
 No espaço de trabalho: %s
 Opções para listar janelas ou espaços de trabalho Opções para modificar propriedades de uma tela Opções para modificar propriedades de uma janela Opções para modificar propriedades de um espaço de trabalho PID: %s
 Posição na disposição (fila, coluna): %d, %d
 Ações possíveis: %s
 Imprima ou modifique as propriedades de uma tela/espaço de trabalho/janela, ou interaja com ela, seguindo a especificação EWMH.
Para informação sobre esta especificação, veja:
	http://freedesktop.org/wiki/Specifications/wm-spec Classe fonte: %s
 Vizinho à direita: %s
 Número da tela: %d
 ID de sessão: %s
 Sombrear a janela Mostra opções para listar janelas ou espaços de trabalho Mostra opções para modificar propriedades de uma tela Mostra opções para modificar propriedades de uma janela Mostra opções para modificar propriedades de um espaço de trabalho Mostrar a área de trabalho Mostrando a área de trabalho: %s
 Mover a janela usando o teclado Redimensionar a janela usando o teclado ID de inicialização: %s
 Estado: %s
 Parar de mostrar a área de trabalho TIPO Ferramenta para alternar entre janelas visíveis Ferramenta para alternar entre janelas Ferramenta para alternar entre espaços de trabalho Vizinho superior: %s
 Temporário para: %lu
 Restaura_r todas Desfazer ma_ximizar Restaurar horizontalmente a janela Restaurar a janela Restaurar verticalmente a janela Restaura_r Restaurar a janela Não sombrear a janela Aplicativo sem título Janela sem título Não é possível mover a porta de visualização: o espaço de trabalho atual não possui uma porta de visualização
 Não é possível mover a porta de visualização: nenhum espaço de trabalho atual
 Posição da porta de visualização (x, y): %s
 LARGURA Lista de Janelas Gerenciador de janelas: %s
 Seletor de Janelas Tipo de janela: %s
 Janela não pode ser movida para o espaço de trabalho %d: o espaço de trabalho não existe
 Espaço de Trabalho %d Espaço de Trabalho %s%d Espaço de Trabalho 1_0 Disposição dos espaços de trabalho (filas, colunas, orientação): %d, %d, %s
 Nome do espaço de trabalho: %s
 Número do espaço de trabalho: %d
 Seletor de Espaço de Trabalho X ID de X Window do grupo líder do aplicativo a ser examinada ID de X Window da janela a ser examinada ou modificada XID XID: %lu
 Y _Sempre no espaço de trabalho visível _Fechar _Fechar todas _Mover _Apenas neste espaço de trabalho _Redimensionar Desfaze_r maximizar todas acima todos espaços de trabalho abaixo mudar modo de tela cheia mudar de espaço de trabalho fechar área de trabalho janela de diálogo encaixe ou painel falso tela cheia posicionar acima posicionar abaixo maximizar maximizar horizontalmente maximizar verticalmente maximizado maximizado horizontalmente maximizado verticalmente minimizar minimizado mover requer atenção nenhuma ação possível normal janela normal fixar fixado redimensionar definido sombrear sombreado pular seletor de espaço de trabalho pular lista de tarefas janela de apresentação nenhum aderir aderente destacar menu destacar barra de ferramentas verdadeiro não posicionar acima não posicionar abaixo restaurar restaurar horizontalmente restaurar verticalmente restaurar não fixar não sombrear não aderir janela de utilidades nenhuma nenhum 