��    l      |  �   �      0	  I   1	  J   {	  F   �	  A   
     O
     T
     Y
     ^
     c
     h
     m
     r
     v
  $   z
     �
     �
  
   �
     �
     �
     �
     �
          
  K     �   f     (     8     N     k  
   �     �     �  !   �  -   �     �       6     "   H     k     q     y     �     �     �  ,   �     �     �            #   #     G     M  4   i     �     �     �     �     �     �     �  2        9  #   N     r     �  '   �  "   �  '   �  "     #   &     J  "   i  '   �     �     �     �     �     �  5     0   B  1   s  /   �  5   �       "   )     L     e     �     �     �     �     �     �     �     �               (     5     B     I  
   O     Z     l  /   ~  Z   �     	      T   2  U   �  O   �  K   -     y     ~     �     �     �     �     �     �     �  3   �  
   �     �     �     �       /     &   G     n     v  K   �  �   �     �     �     �     �     
  
        #  !   (  4   J  !        �  E   �  '   �           '     0     9     L     k  6   �     �     �  
   �     �  -   �       %   %  ;   K  	   �     �     �     �     �  4   �     	  =        T  +   r     �  	   �  &   �  "   �  &     "   7  !   Z     |  "   �  *   �     �                 !   +  A   M  5   �  6   �  6   �  <   3      p   *   �      �   #   �      �   #   !     ?!     _!     f!     w!  
   !     �!     �!     �!     �!     �!  	   �!     �!     �!     �!     "  R   $"  Z   w"  �  �"     _   V      ^          N   W   /          #   5   !   M   a   ?   .   e   7       b               `   1   I       4   j      K       "      <   3                      2   H               (   R             h   
       [       c   8   \   g             	      f         -   '   6       E   ]   S              T       &          k       :       =             C   i       J           %   0      B       F           U   *   D       +   G      X       d   A   Q   L       ,               P         Z          9          $      ;                Y   l   >   )      @   O    "name" and "link" elements are required inside '%s' on line %d, column %d "name" and "link" elements are required inside <sub> on line %d, column %d "title", "name" and "link" elements are required at line %d, column %d "type" element is required inside <keyword> on line %d, column %d 100% 125% 150% 175% 200% 300% 400% 50% 75% A developers' help browser for GNOME Back Book Book Shelf Book: Books disabled Cannot uncompress book '%s': %s Developer's Help program Devhelp Devhelp Website Devhelp integrates with other applications such as Glade, Anjuta, or Geany. Devhelp is an API documentation browser. It provides an easy way to navigate through libraries, search by function, struct, or macro. It provides a tabbed interface and allows to print results. Devhelp support Devhelp — Assistant Display the version and exit Documentation Browser Empty Page Enabled Enum Error opening the requested link. Expected '%s', got '%s' at line %d, column %d Font for fixed width text Font for text Font for text with fixed width, such as code examples. Font for text with variable width. Fonts Forward Function Group by language Height of assistant window Height of main window Invalid namespace '%s' at line %d, column %d KEYWORD Keyword Language: %s Language: Undefined List of books disabled by the user. Macro Main window maximized state Makes F2 bring up Devhelp for the word at the cursor New _Tab New _Window Opens a new Devhelp window Page Preferences Quit any running Devhelp S_maller Text Search and display any hit in the assistant window Search for a keyword Selected tab: "content" or "search" Show API Documentation Struct The X position of the assistant window. The X position of the main window. The Y position of the assistant window. The Y position of the main window. The height of the assistant window. The height of the main window. The width of the assistant window. The width of the index and search pane. The width of the main window. Title Type Use system fonts Use the system default fonts. Whether books should be grouped by language in the UI Whether the assistant window should be maximized Whether the assistant window should be maximized. Whether the main window should start maximized. Which of the tabs is selected: "content" or "search". Width of the assistant window Width of the index and search pane Width of the main window X position of assistant window X position of main window Y position of assistant window Y position of main window _About _About Devhelp _Close _Find _Fixed width: _Group by language _Larger Text _Normal Size _Preferences _Print _Quit _Side pane _Use system fonts _Variable width:  documentation;information;manual;developer;api; https://wiki.gnome.org/Apps/Devhelp?action=AttachFile&#x26;do=view&#x26;target=devhelp.png translator-credits Project-Id-Version: devhelp
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=devhelp&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-16 12:08+0000
PO-Revision-Date: 2015-11-10 05:02+0000
Last-Translator: Enrico Nicoletto <liverig@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:10+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 os elementos "name" e "link" são necessários dentro de "%s" na linha %d, coluna %d os elementos "name" e "link" são necessários dentro de <sub> na linha %d, coluna %d os elementos "title", "name", e "link" são necessários na linha %d, coluna %d o elemento "type" é necessário dentro de <keyword> na linha %d, coluna %d 100% 125% 150% 175% 200% 300% 400% 50% 75% Um navegador de ajuda para desenvolvedores do GNOME Retroceder Livro Estante de livros Livro: Livros desabilitados Não é possível descompactar o livro "%s": %s Programa de ajuda para desenvolvedores Devhelp Site do Devhelp Devhelp se integra com outros aplicativos, tais como Glade, Anjuta e Geany. Devhelp é um navegador de documentação de API. Ele fornece uma forma fácil de navegar pelas bibliotecas, pesquisar por funções, structs ou macros. Ele fornece uma interface com abas e permite imprimir resultados. Suporte do Devhelp Devhelp — Assistente Exibe a versão e sai Navegador de documentações Página vazia Habilitado Enum Erro ao abrir o link requisitado. Esperava "%s", encontrei "%s" na linha %d, coluna %d Fonte para texto com largura fixa Fonte para texto Fonte para texto com largura fixa, assim como os códigos de exemplo. Fonte para texto com largura variável. Fontes Avançar Função Agrupar por idioma Altura da janela do assistente Altura da janela principal Espaço de nomes "%s" inválido na linha %d, coluna %d PALAVRA Palavra-chave Idioma: %s Idioma: Indefinido Lista dos livros desabilitados pelo usuário. Macro Estado maximizado da janela principal Faz a tecla F2 exibir o Devhelp para a palavra sob o cursor Nova _aba Nova _janela Abre uma nova janela de Devhelp Página Preferências Sair de qualquer Devhelp que estiver sendo executado Texto _menor Pesquisar e exibir qualquer resultado na janela do assistente Procura por uma palavra-chave Aba selecionada: "conteúdo" ou "pesquisar" Mostrar documentação da API Estrutura A posição X da janela do assistente. A posição X da janela principal. A posição Y da janela do assistente. A posição Y da janela principal. A altura da janela do assistente. A altura da janela principal. A largura da janela do assistente. A largura do painel de índice e pesquisa. A largura da janela principal. Título Tipo Usar fontes do sistema Usa as fontes padrão do sistema. Os livros devem ser agrupados por idioma na interface do usuário Se a janela do assistente deve ser maximizada ou não Se a janela do assistente deve ser maximizada ou não. Se a janela principal deve iniciar maximizada ou não. Qual das abas está selecionada: "conteúdo" ou "pesquisar". Largura da janela do assistente Largura do painel de índice e de pesquisa Largura da janela principal Posição X da janela do assistente Posição X da janela principal Posição Y da janela do assistente Posição Y da janela principal S_obre S_obre o Devhelp _Fechar _Localizar Largura _fixa: A_grupar por idioma Texto m_aior Texto _normal _Preferências Im_primir _Sair Painel _lateral _Usar fontes do sistema Largura _variável:  documentação;documentações;informações;manual;desenvolvedor;programador;api; https://wiki.gnome.org/Apps/Devhelp?action=AttachFile&#x26;do=view&#x26;target=devhelp.png Alexandre Folle de Menezes <afmenez@terra.com.br>
Raphael Higino <In memoriam>
Luiz Armesto <luiz.armesto@gmail.com>
Washington Lins <washington-lins@uol.com.br>
Og Maciel <ogmaciel@gnome.org>
Taylon Silmer <taylonsilva@gmail.com>
Leonardo Ferreira Fontenelle <leonardof@gnome.org>
Jonh Wendell <wendell@bani.com.br>
Rafael Ferreira <rafael.f.f1@gmail.com>

Launchpad Contributions:
  André Gondim https://launchpad.net/~andregondim
  Enrico Nicoletto https://launchpad.net/~liverig
  Jonh Wendell https://launchpad.net/~wendell
  Leonardo Ferreira Fontenelle https://launchpad.net/~leonardof
  Luiz Armesto https://launchpad.net/~luiz-armesto
  Mario A. C. Silva (Exp4nsion) https://launchpad.net/~marioancelmo
  Mateus Zenaide https://launchpad.net/~mateuszenaide
  Og Maciel https://launchpad.net/~ogmaciel
  Rafael Fontenelle https://launchpad.net/~rffontenelle
  Raphael Higino https://launchpad.net/~raphaelh
  Taylon https://launchpad.net/~taylonsilva-gmail
  Washington Lins https://launchpad.net/~washington-lins 