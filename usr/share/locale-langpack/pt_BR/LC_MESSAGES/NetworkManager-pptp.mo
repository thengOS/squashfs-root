��    I      d  a   �      0     1     G     \     r     ~     �     �     �     �     �  n   �     ]     y     �  +   �  J   �  E   +  �   q  r   �  k   n	     �	     �	  5   �	  "   &
  <   I
     �
  #   �
     �
  )   �
     �
  <      3   =      q     �     �     �      �      �     �  
        "     @     P  �   g            ;   3  -   o  	   �  (   �  u   �  h   F     �  `   �     '     5  %   Q     w  
   �  D   �  	   �  
   �  6   �  6   ,  -   c     �     �  �   �  6   E  ;   |  &   �     �  4  �     4     K     b  
   x     �     �     �     �     �     �  �   �  "   �  &   �  2   �  1     X   4  S   �  �   �  }   �  |   �     {     �  7   �  5   �  W   �  -   U  +   �     �  7   �     �  R   �  @   F  #   �     �  
   �     �      �  +   �  "         C  &   P     w     �  �   �     l     p  B   �  2   �       0   	  u   :  i   �       \   5     �  #   �  '   �  '   �       H   &  	   o     y  I   �  I   �  9         T   %   k   u   �   ?   !  D   G!  )   �!  )   �!     *          %                          E      '   4   +       0          :   ;   1          &      I                         F      $   B   (   9      7   @   G       8   C   
      ?   2       3                   ,   )   5   H             .       =       #       A   /             "                  6              D   <   !   >         	   -               128-bit (most secure) 40-bit (less secure) <b>Authentication</b> <b>Echo</b> <b>General</b> <b>Misc</b> <b>Optional</b> <b>Security and Compression</b> Ad_vanced... All Available (Default) Allow MPPE to use stateful mode. Stateless mode is still attempted first.
config: mppe-stateful (when checked) Allow _BSD data compression Allow _Deflate data compression Allow st_ateful encryption Allow the following authentication methods: Allow/disable BSD-Compress compression.
config: nobsdcomp (when unchecked) Allow/disable Deflate compression.
config: nodeflate (when unchecked) Allow/disable Van Jacobson style TCP/IP header compression in both the transmit and the receive directions.
config: novj (when unchecked) Allow/disable authentication methods.
config: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap Append the domain name <domain> to the local host name for authentication purposes.
config: domain <domain> Authenticate VPN CHAP Compatible with Microsoft and other PPTP VPN servers. Could not find pptp client binary. Could not find secrets (connection invalid, no vpn setting). Could not find the pppd binary. D-Bus name to use for this instance Default Don't quit when VPN connection terminates EAP Enable custom index for ppp<n> device name.
config: unit <n> Enable verbose debug logging (may expose passwords) Invalid or missing PPTP gateway. MSCHAP MSCHAPv2 Missing VPN gateway. Missing or invalid VPN password. Missing or invalid VPN username. Missing required option '%s'. NT Domain: No VPN configuration options. No VPN secrets! No cached credentials. Note: MPPE encryption is only available with MSCHAP authentication methods. To enable this checkbox, select one or more of the MSCHAP authentication methods: MSCHAP or MSCHAPv2. PAP PPTP Advanced Options PPTP server IP or name.
config: the first parameter of pptp Password passed to PPTP when prompted for it. Password: Point-to-Point Tunneling Protocol (PPTP) Require the use of MPPE, with 40/128-bit encryption or all.
config: require-mppe, require-mppe-128 or require-mppe-40 Send LCP echo-requests to find out whether peer is alive.
config: lcp-echo-failure and lcp-echo-interval Send PPP _echo packets Set the name used for authenticating the local system to the peer to <name>.
config: user <name> Show password Use TCP _header compression Use _Point-to-Point encryption (MPPE) Use custom _unit number: User name: You need to authenticate to access the Virtual Private Network '%s'. _Gateway: _Security: couldn't convert PPTP VPN gateway IP address '%s' (%d) couldn't look up PPTP VPN gateway IP address '%s' (%d) invalid boolean property '%s' (not yes or no) invalid gateway '%s' invalid integer property '%s' nm-pptp-service provides integrated PPTP VPN capability (compatible with Microsoft and other implementations) to NetworkManager. no usable addresses returned for PPTP VPN gateway '%s' no usable addresses returned for PPTP VPN gateway '%s' (%d) property '%s' invalid or not supported unhandled property '%s' type %s Project-Id-Version: NetworkManager-pptp
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=NetworkManager&keywords=I18N+L10N&component=VPN: pptp
POT-Creation-Date: 2016-04-14 07:51+0000
PO-Revision-Date: 2012-10-04 20:22+0000
Last-Translator: Tiago Hillebrandt <tiagohillebrandt@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:03+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 128 bits (mais seguro) 40 bits (menos seguro) <b>Autenticação</b> <b>Eco</b> <b>Geral</b> <b>Diverso</b> <b>Opcional</b> <b>Segurança e compressão</b> A_vançado... Todas disponíveis (padrão) Permite que MPPE use o modo em estado de conexão. O modo sem estado ainda é a primeira tentativa.
config: mppe-stateful (quando marcado) Permitir compressão de dados _BSD Permitir compressão de dados _Deflate Permitir criptogr_afia com monitoração de estado Permitir os seguintes métodos de autenticação: Permitir/desabilitar compressão com BSD-Compress.
config: nobsdcomp (quando desmarcado) Permitir/desabilitar compressão com Deflate.
config: nodeflate (quando desmarcado) Permitir/desabilitar estilo de compressão de cabeçalho TCP/IP do Van Jacobson em tanto a transmissão quanto na recepção.
config: novj (quando desmarcado) Permitir/desabilitar métodos de autenticação.
config: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap Anexar o nome de domínio <domínio> ao nome da máquina local para propósitos de autenticação.
config: domain <domínio> Autenticar VPN CHAP Compatível com servidores Microsoft e outros PPTP VPN. Não é possível encontrar binário do cliente pptp. Não é possível encontrar segredos (conexão inválida ou sem configuração de vpn). Não é possível encontrar binário do pppd. Nome D-Bus a ser usado para essa instância Padrão Não sair até que a conexão com a VPN tenha terminado EAP Habilita índice personalizado para o nome de dispositivo ppp<n>.
config: unit <n> Habilitar registro de depuração detalhado (pode expôr senhas) Gateway PPTP inválido ou faltando. MSCHAP MSCHAP v.2 Faltando gateway VPN. Senha VPN inválida ou faltando. Nome de usuário VPN inválido ou faltando. Faltando opção "%s" necessária. Domínio NT: Sem opções de configuração de VPN. Nenhum segredo de VPN! Sem credenciais no cache. Nota: A criptografia MPPE está disponível apenas com métodos de autenticação MSCHAP. Para ativar esta opção, selecione um ou mais dos métodos de autenticação MSCHAP: MSCHAP ou MSCHAPv2. PAP Opções avançadas de PPTP IP ou nome do servidor PPTP.
config: o primeiro parâmetro de pptp Senha passada para PPTP quando lhe for solicitado. Senha: Protocolo de Encapsulamento Ponto a Ponto (PPTP) REquer o usao de MPPE, com criptografia 40/128-bit ou tudo.
config: require-mppe, require-mppe-128 or require-mppe-40 Enviar echo-requests LCP para encontrar se o par está vivo.
config: lcp-echo-failure e lcp-echo-interval Enviar pacotes PPP de _eco Definir o nome usado para autenticar o sistema local ao par como <nome>.
config: user <nome> Mostrar senha Usar compressão de cabeçal_ho TCP Usar criptografia _ponto a ponto (MPPE) Usar número de _unidade personalizado: Nome de usuário: Você precisa autenticar-se para acessar a Rede Particular Virtual "%s". _Gateway: _Segurança: não foi possível converter o endereço IP do gateway PPTP VPN "%s" (%d) não foi possível consultar o endereço IP do gateway PPTP VPN "%s" (%d) propriedade do booleano "%s" inválido (não sim ou não) gateway "%s" inválido propriedade do inteiro "%s" inválido nm-pptp-service provê integração PPTP VPN (compatível com Microsoft e outras implementações) ao NetworkManager. nenhum endereço usável retornado para o gateway PPTP VPN "%s" nenhum endereço usável retornado para o gateway PPTP VPN "%s" (%d) propriedade "%s" inválida ou sem suporte propriedade "%s" não-manipulada, tipo %s 