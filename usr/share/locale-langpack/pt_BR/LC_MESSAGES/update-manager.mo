��    s      �  �   L      �	  1  �	     �
       &        >  ,   F     s     �     �  �   �  �   M            	        (  F   0  *   w  :   �     �     �            ,   :     g     s  &   �  &   �  !   �     �     �       O   4  .   �     �     �     �     �     �  �     A   �     �  K     "   c      �     �     �     �     �          "  %   B  G   h     �  2  �  O   �     B     S     _     k  #   �  "   �     �  8   �  '     )   C     m     �     �     �  2   �  0   �     "     B     V  (   l  �   �  I   B  ;   �  "   �  �   �  ,   �  Q   �  �         �  A     ,   H     u     �     �     �     �  Y   �  O   ,  4   |     �  A   �  
   �       �     V   �  D   �  @   3  P   t  A   �  L     "   T     w  	   �     �     �     �     �  
   �     �  F  �    (!     �"     �"  *   �"  
   �"  2   #     5#     I#     U#  �   i#  �   $  #   �$     �$     %     %  Q   +%  9   }%  R   �%  !   
&     ,&  (   :&  +   c&  9   �&     �&     �&  +   �&  4   '     R'     o'  #   v'     �'  W   �'  @   (     R(  -   [(     �(  #   �(     �(  �   �(  R   �)  -   �)  ^   "*  2   �*  0   �*  $   �*  $   
+  $   /+     T+     l+     �+  /   �+  _   �+     0,  |  B,  V   �-     .     3.     E.  %   X.  +   ~.  1   �.      �.  G   �.  )   E/  .   o/     �/     �/     �/  '   �/  H   0  B   ]0  &   �0     �0     �0  D   �0  �   41  d   2  P   y2  "   �2  �   �2  1   �3  F   �3  �   04  &   -5  P   T5  ?   �5     �5  !   6     %6     46     E6  m   ^6  `   �6  Q   -7     7  U   �7     �7     �7  �   �7  b   �8  D   �8  D   ;9  j   �9  U   �9  ]   A:  /   �:     �:  
   �:     �:      ;     ;     -;     A;     S;        +                  $   W   7          k   '                @   g   K      9   R           3      6       D   C               <   .   f      L   l       	   Q   X       e          s      Z   ?   N   P   &   F          4           a   B   ]   [               (                 "   p   ,   c   b   1   U   j   5       /   A   :          #   G      E      d       
   _          S      h   -      n              J       m   \   M       `               2       )       >   T       0      i   *   %   o   ;   V   8             r   !   ^       =       I   q                            H   Y      O    
A normal upgrade can not be calculated, please run:
  sudo apt-get dist-upgrade


This can be caused by:
 * A previous upgrade which didn't complete
 * Problems with some of the installed software
 * Unofficial software packages not provided by Ubuntu
 * Normal changes of a pre-release version of Ubuntu %(size).0f kB %(size).0f kB %.1f MB %i obsolete entries in the status file %s base %s needs to be marked as manually installed. %s will be downloaded. .deb package A file on disk An unresolvable problem occurred while calculating the upgrade.

Please report this bug against the 'update-manager' package and include the following error message:
 An unresolvable problem occurred while initializing the package information.

Please report this bug against the 'update-manager' package and include the following error message:
 Building Updates List Cancel Changelog Changes Changes for %s versions:
Installed version: %s
Available version: %s

 Check if a new Ubuntu release is available Check if upgrading to the latest devel release is possible Checking for updates… Connecting... Copy Link to Clipboard Could not calculate the upgrade Could not initialize the package information Description Details of updates Directory that contains the data files Do not check for updates when starting Do not focus on map when starting Download Downloading changelog Downloading list of changes... Failed to download the list of changes. 
Please check your Internet connection. However, %s %s is now available (you have %s). Install Install All Available Updates Install Now Install missing package. Installing updates… It is impossible to install or remove any software. Please use the package manager "Synaptic" or run "sudo apt-get install -f" in a terminal to fix this issue at first. It’s safer to connect the computer to AC power before updating. No longer downloadable: No network connection detected, you can not download changelog information. No software updates are available. Not all updates can be installed Not enough free disk space Obsolete dpkg status entries Obsolete entries in dpkg status Open Link in Browser Other updates Package %s should be installed. Please wait, this can take some time. Remove lilo since grub is also installed.(See bug #314004 for details.) Restart _Later Run a partial upgrade, to install as many updates as possible.

    This can be caused by:
     * A previous upgrade which didn't complete
     * Problems with some of the installed software
     * Unofficial software packages not provided by Ubuntu
     * Normal changes of a pre-release version of Ubuntu Run with --show-unsupported, --show-supported or --show-all to see more details Security updates Select _All Settings… Show all packages in a list Show all packages with their status Show and install available updates Show debug messages Show description of the package instead of the changelog Show supported packages on this machine Show unsupported packages on this machine Show version and exit Software Updater Software Updates Software index is broken Software updates are no longer provided for %s %s. Some software couldn’t be checked for updates. Support status summary of '%s': Supported until %s: Technical description Test upgrade with a sandbox aufs overlay The changelog does not contain any relevant changes.

Please use http://launchpad.net/ubuntu/+source/%s/%s/+changelog
until the changes become available or try again later. The computer also needs to restart to finish installing previous updates. The computer needs to restart to finish installing updates. The computer will need to restart. The list of changes is not available yet.

Please use http://launchpad.net/ubuntu/+source/%s/%s/+changelog
until the changes become available or try again later. The software on this computer is up to date. The update has already been downloaded. The updates have already been downloaded. The upgrade needs a total of %s free space on disk '%s'. Please free at least an additional %s of disk space on '%s'. Empty your trash and remove temporary packages of former installations using 'sudo apt-get clean'. There are no updates to install. This update does not come from a source that supports changelogs. To stay secure, you should upgrade to %s %s. Unimplemented method: %s Unknown download size. Unsupported Unsupported:  Update is complete Updated software has been issued since %s %s was released. Do you want to install it now? Updated software is available for this computer. Do you want to install it now? Updated software is available from a previous check. Updates Upgrade using the latest proposed version of the release upgrader Upgrade… Version %s: 
 When upgrading, if kdelibs4-dev is installed, kdelibs5-dev needs to be installed. See bugs.launchpad.net, bug #279621 for details. You are connected via roaming and may be charged for the data consumed by this update. You have %(num)s packages (%(percent).1f%%) supported until %(time)s You have %(num)s packages (%(percent).1f%%) that are unsupported You have %(num)s packages (%(percent).1f%%) that can not/no-longer be downloaded You may not be able to check for updates or download new updates. You may want to wait until you’re not using a mobile broadband connection. You stopped the check for updates. _Check Again _Continue _Deselect All _Partial Upgrade _Remind Me Later _Restart Now… _Try Again updates Project-Id-Version: update-manager
Report-Msgid-Bugs-To: sebastian.heinlein@web.de
POT-Creation-Date: 2016-04-12 03:58+0000
PO-Revision-Date: 2016-04-12 12:12+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Português Brasileiro <ubuntu-l10n-ptbr@lists.ubuntu.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 17:19+0000
X-Generator: Launchpad (build 18115)
X-Poedit-Country: BRAZIL
Language: 
X-Poedit-Language: Portuguese
 
Não foi possível calcular a atualização de distribuição normal. Por favor, execute:
  sudo apt-get dist-upgrade


Isto pode ter sido causado por:
 * Uma atualização anterior que não foi concluída
 * Problemas com algum dos programas instalados
 * Pacotes de programas não-oficiais, não fornecidos pelo Ubuntu
 * Modificações normais de algum pré-lançamento do Ubuntu %(size).0f kB %(size).0f kB %.1f MB %i entradas obsoletas no arquivo de status base do %s %s precisa ser marcado como instalado manualmente. %s serão baixados. pacote .deb Um arquivo no disco Um problema não resolvível ocorreu enquanto era calculada a atualização.

Por favor, relate este bug sobre o pacote 'update-manager' e inclua a seguinte mensagem de erro:
 Um problema impossível de se resolver ocorreu enquanto inicializava as informações do pacote.

Por favor relate este erro do pacote 'update-manager' e inclua a seguinte mensagem de erro:
 Construindo lista de atualizações Cancelar Registro de alterações Alterações Mudanças para as versões de %s:
Versão instalada: %s
Versão disponível: %s

 Verificar se uma nova versão do Ubuntu está disponível Verificar se é possível atualizar para a versão de desenvolvimento mais recente Verificando por atualizações... Conectando... Copiar link para área de transferência Não foi possível calcular a atualização Não foi possível inicializar as informações do pacote Descrição Detalhes das atualizações Diretório que contém os arquivos de dados Não verificar por atualizações na inicialização Não focar o mapa ao iniciar Baixar Baixando o registro de alterações Baixando lista de mudanças... Falha ao baixar a lista de alterações. 
Por favor verifique sua conexão à Internet. Entretanto, o %s %s está disponível agora (você possui o %s). Instalar Instalar todas as atualizações disponíveis Instalar agora Instalar pacote que está faltando. Instalando atualizações... Não é possível instalar ou remover nenhum programa. Por favor, utilize o Gerenciador de Pacotes "Synaptic" ou execute "sudo apt-get install -f" em um terminal para resolver esse problema primeiro. É recomendável conectar o computador à uma fonte de energia antes de atualizar. Não está mais disponível para ser baixado: Não foi detectada uma conexão de rede, você não pode baixar as informações do changelog. Não há atualizações de programas disponíveis. Nem todas as atualizações podem ser instaladas Não há espaço suficiente no disco Entradas de status do dpkg obsoletas Entradas obsoletas no status do dpkg Abrir link no navegador Outras atualizações Pacote %s deve ser instalado. Por favor, espere. Isto pode levar algum tempo. Remover o lilo, uma vez que o grub também está instalado. (Veja o bug #314004 para detalhes.) Reiniciar _depois Execute uma atualização parcial, para instalar as atualizações disponíveis.

    Isto pode ter sido causado por:
     * Uma atualização anterior que não foi concluída
     * Problemas com algum dos programas instalados
     * Pacotes de programas não-oficiais, que não foram fornecidos pelo Ubuntu
     * Modificações normais de uma versão pré-lançamento do Ubuntu Executar com --show-unsupported, --show-supported ou --show-all para ver mais detalhes Atualizações de segurança Selecion_ar Todos Configurações... Mostrar todos os pacotes em uma lista Mostrar todos os pacotes com o seus estados Mostrar e instalar as atualizações disponíveis Mostrar mensagens de depuração Mostrar a descrição dos pacotes ao invés do registro de alterações Mostrar pacotes suportados nessa máquina Mostrar pacotes não suportados nessa máquina Mostrar a versão e sair Atualizador de programas Atualizações de programas O índice de programas está danificado As atualizações de programas não serão mais fornecidas para o %s %s. Alguns programas não puderam ter suas atualizações verificadas. Sumário do estado de suporte de '%s': Suportado até %s: Descrição técnica Testar atualização com uma sobreposição de aufs na área segura. O registro de alterações não contém quaisquer alterações relevantes.

Por favor, use http://launchpad.net/ubuntu/+source/%s/%s/+changelog
até que as alterações se tornem disponíveis ou tente novamente, mais tarde. O computador ainda precisa ser reiniciado para concluir a instalação de atualizações anteriores. O computador precisa reiniciar para finalizar a instalação das atualizações. O computador precisará reiniciar. A lista de modificações ainda não está disponível.

Por favor utilize http://launchpad.net/ubuntu/+source/%s/%s/+changelog
até que as mudanças estejam disponíveis ou tente novamente mais tarde. Os programas neste computador estão atualizados. A atualização já foi baixada. As atualizações já foram baixadas. A atualização necessita de pelo menos %s de espaço livre no disco '%s'. Por favor, libere pelo menos um espaço adicional de %s no disco '%s'. Esvazie sua lixeira e remova pacotes temporários de instalações anteriores usando 'sudo apt-get clean'. Não há atualizações para instalar. Esta atualização não veio de uma fonte que suporta registros de alterações. Para continuar protegido, você deverá atualizar para o %s %s. Método não implementado: %s Tamanho de download desconhecido. Não suportado Não suportado:  Atualização concluída Atualizações de programas foram disponibilizadas desde que o %s %s foi lançado. Deseja instalá-las agora? Atualizações de programas estão disponíveis para este computador. Deseja instalá-las agora? Programas atualizados estão disponíveis a partir de uma verificação anterior. Atualizações Atualizar usando a versão sugerida mais recente do atualizador de versão do sistema Atualizar... Versão %s 
 Ao atualizar, se kdelibs4-dev estiver instalado, kdelibs5-dev precisa ser instalado. Veja bugs.launchpad.net, bug # 279621 para obter mais detalhes. Você está conectado via roaming e poderá ser cobrado pelo consumo de dados desta atualização. Você tem %(num)s pacotes (%(percent).1f%%) suportados até %(time)s Você tem %(num)s pacotes (%(percent).1f%%) que não são suportados Você tem %(num)s pacotes (%(percent).1f%%) que não podem ou não estão disponíveis para serem baixados Você pode não ser capaz de verificar atualizações ou baixar novas atualizações. Você pode querer esperar até não estar mais utilizando uma conexão de banda larga móvel. Você parou a verificação por atualizações. Verifi_car novamente _Continuar _Desmarcar todos Atualização _parcial Lemb_re-me mais tarde _Reiniciar agora... _Tentar novamente atualizações 