��    �     ,  �  <,      �:     �:     ;  
   ;     (;  *   7;      b;  %   �;  %   �;  U   �;     %<     8<  #   S<  *   w<  A   �<  D   �<  �   )=  H  >  $   \?  3   �?     �?  -   �?  �   @  &   �@  h   �@      GA  ~   hA  E   �A  0   -B  b   ^B  .   �B  C   �B     4C     HC     [C     kC     {C     �C     �C     �C  $  �C     �D     �D  #   E  0   8E  '   iE  2   �E     �E     �E  *   �E  *   F  3   EF     yF  '   �F  -   �F     �F  "   �F     !G  "   >G     aG     �G  ,   �G  3   �G      H     H  &   -H     TH  N   lH     �H     �H  #   �H  .   I     <I  	   EI     OI     _I  ,   pI     �I     �I  -   �I  .   �I  (   #J     LJ  '   SJ     {J     �J     �J     �J     �J     �J     �J     K     0K     AK     UK     hK     yK     �K     �K     �K     �K     �K     �K      L  .    L     OL     ^L     qL     �L     �L     �L     �L     �L      M     -M     MM     hM     wM     �M     �M     �M     �M  *   �M  /   N     ?N  '   UN     }N     �N     �N  +   �N     �N     O  s   (O  +   �O  +   �O  (   �O     P      7P  !   XP  -   zP     �P  +   �P  8   �P  0   !Q  .   RQ     �Q  '   �Q  :   �Q  )   �Q  L   "R  5   oR     �R     �R     �R     �R     S  -   -S     [S  +   wS  1   �S     �S     �S     T  "   +T     NT  7   kT  -   �T  9   �T     U  )   'U     QU     lU  ;   �U     �U  %   �U     �U      V  #   4V     XV     vV     �V  '   �V     �V  N   �V  =   >W  )   |W     �W  )   �W     �W  &   �W     $X  "   5X  !   XX     zX  0   �X     �X     �X     �X  	   Y     Y      Y  	   (Y     2Y     8Y     FY     cY     �Y     �Y  5   �Y  "   �Y     �Y  *   �Y  D   'Z     lZ  $   �Z     �Z  ,   �Z     �Z     �Z  =   [  :   M[     �[  B   �[     �[  
   �[     �[     �[     \     (\     E\      c\     �\     �\  :   �\  *   �\  ;   "]  *   ^]  +   �]     �]     �]     �]     �]     �]  0   �]     ^     :^     Q^  $   Z^     ^     �^  '   �^  )   �^      _  .   (_  3   W_  +   �_  2   �_     �_  C   `  L   G`  .   �`  +   �`  "   �`  :   a  &   Ma     ta     {a     �a     �a     �a     �a  �   �a  5   �b  O   c     Sc     rc  ;   �c     �c  &   �c     d  $   d     Bd  [   Kd  +   �d  2   �d  '   e  '   .e     Ve      ve     �e  .   �e      �e     f     f  #   :f  #   ^f     �f  (   �f     �f  ;   �f  9    g  (   Zg  (   �g  1   �g  ,   �g  "   h  %   .h  #   Th     xh  #   �h  (   �h     �h  +   i     0i     7i  %   Qi     wi  1   �i     �i     �i     �i  >   �i     &j  ,   ;j  =   hj  /   �j  "   �j  0   �j     *k     ?k     Ck     Wk  O   ek     �k  '   �k  2   �k  '   +l  2   Sl     �l     �l     �l     �l     �l     m     
m     m     %m  #   .m     Rm  /   mm  +   �m     �m     �m     �m     n      +n  .   Ln  "   {n     �n     �n     �n     �n  #   o  !   2o  =   To     �o     �o     �o     �o  3   �o  
   /p     :p     Yp  >   xp  1   �p     �p     q  "   q     Aq     Tq     nq    �q  ;   �r  9   �r     s  $   (s  _   Ms  $   �s  $   �s     �s  %   �s  2   $t     Wt     tt     {t     �t  !   �t      �t     �t  "   �t     u  (   u     >u  "   Ju  %   mu  &   �u     �u     �u  D   �u     v  	   *v  4   4v     iv  :   �v     �v     �v  '   �v     �v     w  "   (w  "   Kw     nw  7   �w  (   �w     �w     �w     	x     x  
   x     #x     +x  -   @x  "   nx     �x  /   �x     �x  /   �x  "   /y  #   Ry  #   vy     �y     �y  +   �y  +   �y  =   )z  3   gz  V   �z     �z     �z  %   {     '{     ;{     M{  6   i{  6   �{  	   �{     �{     �{  D   |  :   \|  �   �|  0   (}  &   Y}  v   �}  Y   �}  +   Q~  =   }~  g   �~  2   #  �   V     �     �  
   �     �     �     /�     J�     h�     ��     ��     ��     ��  /   Ԁ     �     $�     9�  !   I�  L   k�     ��     ́  �   �  %   ��     ��     ւ     �  3   ��  H   2�  $   {�  B   ��  2   �  '   �  (   >�  $   g�  *   ��  >   ��  %   ��  O   �  S   l�     ��     ޅ     ��     �  ;   +�  n   g�  5   ֆ     �     %�     -�     A�     Y�     q�     �     ��     ��     ��     ه     ��     �     '�     9�  7   S�     ��     ��     ��  #   ��  &   Ј     ��  +   
�     6�  	   B�     L�     ]�  
   m�  	   x�     ��     ��     ��  !   ��     ŉ     ߉  !   �  3   �     A�  #   Z�  )   ~�  "   ��  "   ˊ      �     �     �     +�     9�     I�  0   \�  0   ��  /   ��  8   �     '�  
   3�     >�  )   S�     }�     ��     ��     ��     ˌ     �     �     �     6�     O�     V�  !   g�     ��     ��     ��     ��     ʍ     �     ��  "   	�  6   ,�     c�  &   {�  %   ��     Ȏ  $   ��      �     &�     /�     7�     C�     T�  4   k�     ��     ��     ͏     �  
   �  %   ��     �     %�     (�  	   ,�     6�     I�     U�  #   m�     ��     ��     ͐     ڐ     �  D   �     F�     e�     n�     ��  
   ��     ��     ��  *   ��  -   �  %   �     8�     X�     h�     t�  (   |�  (   ��     Β  !   �  (   	�  /   2�  "   b�     ��     ��  (   ��     �  (   ��     '�     >�     Z�  %   s�  %   ��     ��      ܔ     ��     �     ,�     E�  0   c�     ��     ��  -   ĕ     �      �     /�  "   H�  #   k�  (   ��     ��  %   Ж  *   ��     !�     ?�  "   Y�     |�     ��     ��     Η  *   �  %   �  )   7�  .   a�     ��  "   ��     ̘  %   �  '   
�     2�     M�     g�     ��     ��     ��     ҙ     �     �      #�     D�     ]�     w�     z�     ~�     ��  �  ��     b�     ��     ��     ��  <   ��  +   �  /   �  .   E�  c   t�     ؝     �  !   �  6   .�  M   e�  F   ��    ��  >  �  5   G�  E   }�     á  5   �  �   �  =   Ǣ  y   �  -   �  �   ��  L   F�  8   ��  j   ̤  0   7�  V   h�     ��     ӥ     �     ��  !   �  "   /�     R�     p�  #  w�     ��     ��  &   ��  (   �  "   �  E   3�  !   y�     ��  =   ��  .   �  6   !�  1   X�  ,   ��  9   ��     �  -   �  &   2�  0   Y�  8   ��  "   ê  /   �  5   �     L�     c�  0   ~�     ��  h   ʫ     3�     J�  .   c�  C   ��  
   ֬  	   �     �     ��  -   �     A�     V�  +   k�  ,   ��  (   ĭ     ��     ��     �     #�     >�     T�     o�  +   ��  +   ��     �     ��     �     *�     ?�     N�     i�     ��     ��  3   ��     �     �     ��  F   �     \�     k�  ,   ~�     ��     ��     Ұ     ��     �  "   %�      H�     i�     ��     ��     ��  &   ñ     �      �  :   �  9   R�  '   ��  *   ��  $   ߲     �      �  2   =�     p�     ��  v   ��  F   �  (   \�  -   ��     ��  &   ʹ  '   ��  5   �     R�  .   f�  8   ��  <   ε  *   �     6�  +   J�  H   v�  %   ��  e   �  j   K�     ��     ӷ     �     	�     "�  <   >�     {�  2   ��  ?   ɸ  !   	�     +�     G�  "   b�     ��  A   ��  <   �  D   "�     g�  .   ��  (   ��  *   ݺ  <   �     E�  2   Z�     ��  #   ��  '   ʻ     �     �     /�  +   C�     o�  \   ��  K   �  -   7�      e�  .   ��     ��  (   Ƚ     �  "   �  #   %�     I�  I   c�     ��     ��     ޾  	   �     ��     
�     �     �     #�  2   6�  )   i�     ��     ��  F   ��  2   ��     ,�  :   A�  M   |�  /   ��  )   ��     $�  8   5�     n�  !   v�  D   ��  C   ��     !�  X   ?�     ��  	   ��     ��     ��  !   ��  '   ��  '   �  )   6�  +   `�      ��  @   ��  ?   ��  @   .�  6   o�  @   ��     ��  	   ��     ��     �     �  A   #�     e�     ��  	   ��  ,   ��  $   ��  %   ��  +   �  =   K�  *   ��  E   ��  A   ��  6   <�  <   s�  &   ��  e   ��  p   =�  B   ��  7   ��  -   )�  R   W�  2   ��     ��     ��     �  #   �     @�     I�    Y�  @   v�  q   ��  !   )�     K�  J   f�     ��  $   ��     ��  -   	�  	   7�  f   A�  /   ��  2   ��  +   �  +   7�     c�     ��  %   ��  .   ��  #   ��     �      1�  &   R�  !   y�     ��  (   ��     ��  3   ��  H   +�  ,   t�  *   ��  1   ��  )   ��     (�  #   D�  !   h�     ��  $   ��  -   ��  !   ��  2   �     Q�     Z�  "   x�     ��  6   ��     ��      �     �  V   �  "   v�  >   ��  I   ��  J   "�  (   m�  3   ��     ��     ��  #   ��     �  V   %�  !   |�  .   ��  :   ��  .   �  :   7�     r�  !   ��     ��  %   ��     ��     �     �     �     '�  "   0�     S�  8   n�  ,   ��     ��  "   ��  ,   �     ;�  '   S�  <   {�  6   ��  +   ��  .   �     J�  (   j�  5   ��  '   ��  \   ��     N�     `�  '   ��     ��  @   ��  
   �  "   �  -   1�  H   _�  7   ��     ��     ��  #   �     ;�     T�     r�    ��  S   ��  ;   ��     :�  .   S�  {   ��  .   ��  .   -�     \�  .   d�  7   ��     ��     ��  "   ��     �  )   �  !   H�     j�  +   r�     ��  /   ��     ��  6   ��  4   �  2   S�  	   ��     ��  R   ��     �  	   �  A   �     Y�  Y   n�     ��     ��  '   ��  !   �     #�  (   5�  &   ^�  *   ��  K   ��  !   ��     �     ;�     U�     [�  	   d�     n�     |�  .   ��  .   ��  (   ��  C   �  )   a�  ;   ��  0   ��  ,   ��  ,   %�  )   R�  +   |�  G   ��  9   ��  L   *�  :   w�  j   ��     �     %�  9   .�     h�     ~�  #   ��  J   ��  J    �     K�     T�  !   l�  =   ��  8   ��  �   �  2   ��  -   ��  �   ��  h   ��  >   ��  T   +�  l   ��  3   ��  �   !�     ��     ��  	   ��     ��      �     ,�     H�  %   f�  #   ��     ��     ��  #   ��  2   ��     %�     C�     V�  #   e�  J   ��     ��      ��  �   �  &   ��  '   ��     !�     0�  9   O�  [   ��  #   ��  C   	�  <   M�  4   ��  &   ��  0   ��  ,   �  C   D�  )   ��  f   ��  g   �  !   ��  "   ��      ��     ��  P    �  h   Q�  ?   ��      ��     �     $�  $   7�      \�     }�     ��  $   ��      ��  *   ��  ,   �  !   L�  #   n�     ��  $   ��  N   ��     �      �     /�  ,   D�  *   q�     ��  4   ��     ��  	   ��     ��     �  
   /�  
   :�     E�     a�     m�  #   z�     ��     ��  ,   ��  3   ��      *�  +   K�  5   w�      ��  #   ��  -   ��      �     '�     ?�     X�     h�  0   {�  .   ��  6   ��  <   �     O�     [�     o�  ?   ��  %   ��     ��     ��     �     !�     =�     W�     t�     ��     ��     ��  !   ��     ��     ��     �     �     4�  "   S�     v�  #   ��  ?   ��     ��  4   
�  /   ?�     o�  /   ��  #   ��     ��     ��     ��        %     J   ;     �      �  %   �     �     �  3   �     #    ,    1 	   7    A    ]    i "   �     �    �    �     �     e   '    �    �    �    �    �    �    � 0    /   8 ,   h )   �    �    �    � :   � >   #    b !   t +   � 2   � &   � #       @ =   U $   � )   �    �    �        , 0   I    z (   �    �    �    � +    :   2    m "   � &   �    � (   �         4 $   U 8   z !   � +   � /   	 -   1	 *   _	 *   �	 $   �	 $   �	 %   �	    %
 ;   A
 -   }
 :   �
 <   �
    # 0   6    g *   y 5   �    �    � '   	    1    Q "   q "   �    � #   � (   �        0    H    K    O    T            Y  �  �  ,       �  q                 h   �      �        d  ]   G  q  b  �    ;  H  )      C   X          R              �     <  ?  7     �  $      N  l   D  �   1  3     8  w  -   y   '  �   D   �            {   2  �   �   ^  C  h  9  j   �  �  f  �   �       �   s  2  �  �  �  �        @          �       �   �   �  "     {  �   �  2   '                   k  �             6      �          !  ~  �  c      �   �  �  |   =   a  Z        7   
  �  O  r  �   �   c   �       �   �    #   B  �     �           �   �  V   �   4  E           �       m          u  �   �                    �   �   
      ]  V  �   !  �        �      �   �       x  [         <      �         �   i   �  L      �      �   `  0   n    �  +   �      3  	  �  �        �  B   �      ^  O   M      �    )      �  �        �      y  �      �   B  f         !   A   }  �  s  �   �   �  �  \          F  �  5  �    �  �  �      �  o  �       v  H       }  �     �       g  �   P       >  K  �    z      �  J   �  �  �              �          �  �   p       k   �   �   �  m   �   �          �       U  �      �      |  �       �   �  �         {    �     &   �   }   p  u  �  �      �   �     7  �  �       �           �   8      �  9  �  �  ~       �   �  �          �   *      k              �      �   �      �   �       3  �     t  �  j  �   �       <   1  _  e      �          �       �      �               w       �         �          �  [  �  T      g      �   *  �       ]      [   0      R   �  �   S     (       a  �  �  i     *       �  �      g  :   �   �  �  +              x              �        �   �      �  |  �  $        �       �  D  .   9     .  n   #      �  �     #  �  \     V  �   �  M   &  �    �   &  :  >  b  �   �  4       �      �  5       ,          �  r  '  �  �      �     �  l  �  �       �          �      �   >         �   �      �   �   S  w  m  �  �  �  U   �      �  �      +    .  ;            
   J  E    K  �      P  �  c  �  b       �   )   �   ;         �  �                   �  6      F   �  �      `   Z       t       �      �   ,  N   Q      S         u   h      �      r   W     j  L      �  �   �   �     �  �  �  T  �  �  �   =  o                      �  �          �  �  M  �  %       (  �          o      �       L  �       �  �  N  f  �  X      �  �  �   �  �  e  5  �   �                      �   _       0  �   �      v   �    \      I          �  �  �       Y    �       �           �  �     ^   �  �  �  Q   �  �  �   8   �         �       �  �      �      s   �   n  /       �      	  �    �  �   /  p  �            �      ~  F        	   P      e   ?   �      �     l    �  �   �   �      U          G        1   _  d   �  �   4  O      �          �      "      I   �     (  z      y     �  �   :         �      -      �   �              �         �   �      @   v  �   I      �  �  �   �       �   6      x  $     �  �  �  �      �  W  �   �  @  �   -  �  R  �      C  ?  �      `  /  �   �  E  =      �  �          Y   %      Z  �   �  z   �   �   �  �   �  X   A                    i  �   �       H  "  t       a          �  �  T   �   J      A  �  d  %      W     Q  K       �     �   �          �      �                 G       �  �  �          q               �  �       	%Q (@i #%i, mod time %IM)
 	<@f metadata>
 	Using %s
 	Using %s, %s
 
	while trying to add journal to device %s 
	while trying to create journal 
	while trying to create journal file 
	while trying to open journal on %s
 

%s: UNEXPECTED INCONSISTENCY; RUN fsck MANUALLY.
	(i.e., without -a or -p options)
 
  Inode table at  
  Reserved GDT blocks at  
%s: %s: error reading bitmaps: %s
 
%s: ***** FILE SYSTEM WAS MODIFIED *****
 
%s: ********** WARNING: Filesystem still has errors **********

 
*** journal has been re-created - filesystem is now ext3 again ***
 
Bad extended option(s) specified: %s

Extended options are separated by commas, and may take an argument which
	is set off by an equals ('=') sign.

Valid extended options are:
	superblock=<superblock number>
	blocksize=<blocksize>
 
Emergency help:
 -p                   Automatic repair (no questions)
 -n                   Make no changes to the filesystem
 -y                   Assume "yes" to all questions
 -c                   Check for bad blocks and add them to the badblock list
 -f                   Force checking even if filesystem is marked clean
 
Filesystem too small for a journal
 
If the @b is really bad, the @f can not be fixed.
 
Interrupt caught, cleaning up
 
Invalid non-numeric argument to -%c ("%s")

 
Journal block size:       %u
Journal length:           %u
Journal first block:      %u
Journal sequence:         0x%08x
Journal start:            %u
Journal number of users:  %u
 
Journal size too big for filesystem.
 
Running additional passes to resolve @bs claimed by more than one @i...
Pass 1B: Rescanning for @m @bs
 
Sparse superblock flag set.  %s 
The bad @b @i has probably been corrupted.  You probably
should stop now and run e2fsck -c to scan for bad blocks
in the @f.
 
The device apparently does not exist; did you specify it correctly?
 
The filesystem already has sparse superblocks.
 
The requested journal size is %d blocks; it must be
between 1024 and 10240000 blocks.  Aborting.
 
Warning, had trouble writing out superblocks. 
Warning: RAID stripe-width %u not an even multiple of stride %u.

   %s superblock at    Block bitmap at    Free blocks:    Free inodes:   (check after next mount)  (check deferred; on battery)  (check in %ld mounts)  (y/n)  -v                   Be verbose
 -b superblock        Use alternative superblock
 -B blocksize         Force blocksize when looking for superblock
 -j external_journal  Set location of the external journal
 -l bad_blocks_file   Add to badblocks list
 -L bad_blocks_file   Set badblocks list
  Done.
  Group descriptor at   contains a file system with errors  has been mounted %u times without being checked  has gone %u days without being checked  primary superblock features different from backup  was not cleanly unmounted # Extent dump:
 %d-byte blocks too big for system (max %d) %s %s: status is %x, should never happen.
 %s @o @i %i (uid=%Iu, gid=%Ig, mode=%Im, size=%Is)
 %s has unsupported feature(s): %s is apparently in use by the system;  %s is entire device, not just one partition!
 %s is mounted;  %s is not a block special device.
 %s is not a journal device.
 %s: %s filename nblocks blocksize
 %s: %s trying backup blocks...
 %s: ***** REBOOT LINUX *****
 %s: Error %d while executing fsck.%s for %s
 %s: The -n and -w options are mutually exclusive.

 %s: e2fsck canceled.
 %s: journal too short
 %s: no valid journal superblock found
 %s: recovering journal
 %s: skipping bad line in /etc/fstab: bind mount with nonzero fsck pass number
 %s: too many arguments
 %s: too many devices
 %s: wait: No more child process?!?
 %s: won't do journal recovery while read-only
 %s? no

 %s? yes

 %u block group
 %u block groups
 %u blocks per group, %u fragments per group
 %u inodes per group
 %u inodes scanned.
 '.' @d @e in @d @i %i is not NULL terminated
 '..' @d @e in @d @i %i is not NULL terminated
 '..' in %Q (%i) is %P (%j), @s %q (%d).
 (NONE) (There are %N @is containing @m @bs.)

 (no prompt) , Group descriptors at  , Inode bitmap at  , check forced.
 --waiting-- (pass %d)
 -O may only be specified once -o may only be specified once /@l is not a @d (ino=%i)
 /@l not found.   <Reserved inode 10> <Reserved inode 9> <The NULL inode> <The bad blocks inode> <The boot loader inode> <The group descriptor inode> <The journal inode> <The undelete directory inode> <n> <y> = is incompatible with - and +
 @A %N contiguous @b(s) in @b @g %g for %s: %m
 @A @a @b %b.   @A @b @B (%N): %m
 @A @b buffer for relocating %s
 @A @d @b array: %m
 @A @i @B (%N): %m
 @A @i @B (@i_dup_map): %m
 @A icount link information: %m
 @A icount structure: %m
 @A new @d @b for @i %i (%s): %m
 @A refcount structure (%N): %m
 @D @i %i has zero dtime.   @E @L to '.'   @E @L to @d %P (%Di).
 @E @L to the @r.
 @E has @D/unused @i %Di.   @E has @n @i #: %Di.
 @E has a @z name.
 @E has a non-unique filename.
Rename to %s @E has an incorrect filetype (was %Dt, @s %N).
 @E has filetype set.
 @E has illegal characters in its name.
 @E has rec_len of %Dr, @s %N.
 @E is duplicate '.' @e.
 @E is duplicate '..' @e.
 @E points to @i (%Di) located in a bad @b.
 @I @i %i in @o @i list.
 @I @o @i %i in @S.
 @S @b_size = %b, fragsize = %c.
This version of e2fsck does not support fragment sizes different
from the @b size.
 @S @bs_per_group = %b, should have been %c
 @S first_data_@b = %b, should have been %c
 @S hint for external superblock @s %X.   @a @b %b has h_@bs > 1.   @a @b %b is corrupt (@n name).   @a @b %b is corrupt (@n value).   @a @b %b is corrupt (allocation collision).   @a @b @F @n (%If).
 @a in @i %i has a namelen (%N) which is @n
 @a in @i %i has a value @b (%N) which is @n (must be 0)
 @a in @i %i has a value offset (%N) which is @n
 @a in @i %i has a value size (%N) which is @n
 @b @B differences:  @b @B for @g %g is not in @g.  (@b %b)
 @f contains large files, but lacks LARGE_FILE flag in @S.
 @f did not have a UUID; generating one.

 @f does not have resize_@i enabled, but s_reserved_gdt_@bs
is %N; @s zero.   @f has feature flag(s) set, but is a revision 0 @f.   @g %g's @b @B (%b) is bad.   @g %g's @b @B at %b @C.
 @g %g's @i @B (%b) is bad.   @g %g's @i @B at %b @C.
 @g %g's @i table at %b @C.
 @h %i has a tree depth (%N) which is too big
 @h %i has an @n root node.
 @h %i has an unsupported hash version (%N)
 @h %i uses an incompatible htree root node flag.
 @i %i (%Q) has @n mode (%Im).
 @i %i (%Q) is an @I @b @v.
 @i %i (%Q) is an @I FIFO.
 @i %i (%Q) is an @I character @v.
 @i %i (%Q) is an @I socket.
 @i %i has @cion flag set on @f without @cion support.   @i %i has INDEX_FL flag set but is not a @d.
 @i %i has INDEX_FL flag set on @f without htree support.
 @i %i has a bad @a @b %b.   @i %i has a extra size (%IS) which is @n
 @i %i has illegal @b(s).   @i %i has imagic flag set.   @i %i is a %It but it looks like it is really a directory.
 @i %i is a @z @d.   @i %i is in use, but has dtime set.   @i %i is too big.   @i %i ref count is %Il, @s %N.   @i %i was part of the @o @i list.   @i %i, i_@bs is %Ib, @s %N.   @i %i, i_size is %Is, @s %N.   @i @B differences:  @i @B for @g %g is not in @g.  (@b %b)
 @i count in @S is %i, @s %j.
 @i table for @g %g is not in @g.  (@b %b)
WARNING: SEVERE DATA LOSS POSSIBLE.
 @is that were part of a corrupted orphan linked list found.   @j @i is not in use, but contains data.   @j is not regular file.   @j version not supported by this e2fsck.
 @m @b(s) in @i %i: @m @bs already reassigned or cloned.

 @n @h %d (%q).   @n @i number for '.' in @d @i %i.
 @p @h %d (%q): bad @b number %b.
 @p @h %d: root node is @n
 @r has dtime set (probably due to old mke2fs).   @r is not a @d.   @r is not a @d; aborting.
 @r not allocated.   @u @i %i
 @u @z @i %i.   ABORTED ALLOCATED Abort Aborting....
 Adding dirhash hint to @f.

 Adding journal to device %s:  Aerror allocating Allocate BLKFLSBUF ioctl not supported!  Can't flush buffers.
 Backing up @j @i @b information.

 Backup Bad @b %b used as bad @b @i indirect @b.   Bad @b @i has an indirect @b (%b) that conflicts with
@f metadata.   Bad @b @i has illegal @b(s).   Bad block %u out of range; ignored.
 Bad blocks: %u Bad or non-existent /@l.  Cannot reconnect.
 Bbitmap Begin pass %d (max = %lu)
 Block %b in the primary @g descriptors is on the bad @b list
 Block %d in primary superblock/group descriptor area bad.
 Block size=%u (log=%u)
 Blocks %u through %u must be good in order to build a filesystem.
 CLEARED CONTINUING CREATED Can not continue. Can't find external @j
 Cannot continue, aborting.

 Cannot proceed without a @r.
 Cconflicts with some other fs @b Checking all file systems.
 Checking blocks %lu to %lu
 Checking for bad blocks (non-destructive read-write test)
 Checking for bad blocks (read-only test):  Checking for bad blocks in non-destructive read-write mode
 Checking for bad blocks in read-only mode
 Checking for bad blocks in read-write mode
 Clear Clear @j Clear HTree index Clear inode Clearing Clearing filesystem feature '%s' not supported.
 Clone multiply-claimed blocks Connect to /lost+found Continue Corruption found in @S.  (%s = %N).
 Could not expand /@l: %m
 Could not reconnect %i: %m
 Could this be a zero-length partition?
 Couldn't allocate block buffer (size=%d)
 Couldn't allocate header buffer
 Couldn't allocate memory for filesystem types
 Couldn't allocate memory to parse journal options!
 Couldn't allocate memory to parse options!
 Couldn't allocate path variable in chattr_dir_proc Couldn't clone file: %m
 Couldn't determine device size; you must specify
the size manually
 Couldn't determine device size; you must specify
the size of the filesystem
 Couldn't find journal superblock magic numbers Couldn't find valid filesystem superblock.
 Couldn't fix parent of @i %i: %m

 Couldn't fix parent of @i %i: Couldn't find parent @d @e

 Couldn't parse date/time specifier: %s Create Creating journal (%d blocks):  Creating journal inode:  Creating journal on device %s:  Ddeleted Delete file Device size reported to be zero.  Invalid partition specified, or
	partition table wasn't reread after running fdisk, due to
	a modified partition being busy and in use.  You may need to reboot
	to re-read your partition table.
 Directories count wrong for @g #%g (%i, counted=%j).
 Disk write-protected; use the -n option to do a read-only
check of the device.
 Do you really want to continue Duplicate @E found.   Duplicate @e '%Dn' found.
	Marking %p (%i) to be rebuilt.

 Duplicate or bad @b in use!
 E2FSCK_JBD_DEBUG "%s" not an integer

 E@e '%Dn' in %p (%i) ERROR: Couldn't open /dev/null (%s)
 EXPANDED Either all or none of the filesystem types passed to -t must be prefixed
with 'no' or '!'.
 Empty directory block %u (#%d) in inode %u
 Error adjusting refcount for @a @b %b (@i %i): %m
 Error copying in replacement @b @B: %m
 Error copying in replacement @i @B: %m
 Error creating /@l @d (%s): %m
 Error creating root @d (%s): %m
 Error deallocating @i %i: %m
 Error determining size of the physical @v: %m
 Error iterating over @d @bs: %m
 Error moving @j: %m

 Error reading @a @b %b (%m).   Error reading @a @b %b for @i %i.   Error reading @d @b %b (@i %i): %m
 Error reading @i %i: %m
 Error reading block %lu (%s) while %s.   Error reading block %lu (%s).   Error storing @d @b information (@i=%i, @b=%b, num=%N): %m
 Error storing @i count information (@i=%i, count=%N): %m
 Error validating file descriptor %d: %s
 Error while adjusting @i count on @i %i
 Error while iterating over @bs in @i %i (%s): %m
 Error while iterating over @bs in @i %i: %m
 Error while scanning @is (%i): %m
 Error while scanning inodes (%i): %m
 Error while trying to find /@l: %m
 Error writing @a @b %b (%m).   Error writing @d @b %b (@i %i): %m
 Error writing block %lu (%s) while %s.   Error writing block %lu (%s).   Error: ext2fs library version out of date!
 Expand Extending the inode table External @j does not support this @f
 External @j has bad @S
 External @j has multiple @f users (unsupported).
 FILE DELETED FIXED Ffor @i %i (%Q) is Filesystem features not supported with revision 0 filesystems
 Filesystem label=%s
 Filesystem larger than apparent device size. Filesystem mounted or opened exclusively by another program?
 Filesystem's UUID not found on journal device.
 Finished with %s (exit status %d)
 First @e '%Dn' (@i=%Di) in @d @i %i (%p) @s '.'
 First data block=%u
 Fix Flags of %s set as  Force rewrite Found @n V2 @j @S fields (from V1 @j).
Clearing fields beyond the V1 @j @S...

 Fragment size=%u (log=%u)
 Free @bs count wrong (%b, counted=%c).
 Free @bs count wrong for @g #%g (%b, counted=%c).
 Free @is count wrong (%i, counted=%j).
 Free @is count wrong for @g #%g (%i, counted=%j).
 From block %lu to %lu
 Get a newer version of e2fsck! Group %lu: (Blocks  Group descriptors look bad... HTREE INDEX CLEARED IGNORED INODE CLEARED Ignore error Iillegal Illegal number for blocks per group Illegal number of blocks!
 Internal error: couldn't find dir_info for %i.
 Internal error: fudging end of bitmap (%N)
 Invalid EA version.
 Invalid RAID stride: %s
 Invalid RAID stripe-width: %s
 Invalid UUID format
 Invalid blocksize parameter: %s
 Invalid completion information file descriptor Invalid filesystem option set: %s
 Invalid mount option set: %s
 Invalid resize parameter: %s
 Invalid stride length Invalid stride parameter: %s
 Invalid stripe-width parameter: %s
 Invalid superblock parameter: %s
 Journal dev blocksize (%d) smaller than minimum blocksize %d
 Journal removed
 Journal size:              Journal superblock not found!
 Journal users:            %s
 Journals not supported with revision 0 filesystems
 Lis a link MULTIPLY-CLAIMED BLOCKS CLONED Maximum filesystem blocks=%lu
 Maximum of one test_pattern may be specified in read-only mode Memory used: %d, elapsed time: %6.3f/%6.3f/%6.3f
 Missing '.' in @d @i %i.
 Missing '..' in @d @i %i.
 Moving @j from /%s to hidden @i.

 Moving inode table Must use '-v', =, - or +
 No room in @l @d.   Note: if several inode or block bitmap blocks or part
of the inode table require relocation, you may wish to try
running e2fsck with the '-b %S' option first.  The problem
may lie only with the primary block group descriptors, and
the backup block group descriptors may be OK.

 On-line resizing not supported with revision 0 filesystems
 Only one of the options -p/-a, -n or -y may be specified. Optimizing directories:  Out of memory erasing sectors %d-%d
 PROGRAMMING ERROR: @f (#%N) @B endpoints (%b, %c) don't match calculated @B endpoints (%i, %j)
 Padding at end of @b @B is not set.  Padding at end of @i @B is not set.  Pass 1 Pass 1: Checking @is, @bs, and sizes
 Pass 1C: Scanning directories for @is with @m @bs
 Pass 1D: Reconciling @m @bs
 Pass 2 Pass 2: Checking @d structure
 Pass 3 Pass 3: Checking @d connectivity
 Pass 3A: Optimizing directories
 Pass 4 Pass 4: Checking reference counts
 Pass 5 Pass 5: Checking @g summary information
 Peak memory Please run 'e2fsck -f %s' first.

 Please run e2fsck on the filesystem.
 Possibly non-existent or swap device?
 Primary Proceed anyway? (y,n)  Programming error?  @b #%b claimed for no reason in process_bad_@b.
 RECONNECTED RELOCATED Random test_pattern is not allowed in read-only mode Reading and comparing:  Recovery flag not set in backup @S, so running @j anyway.
 Recreate Relocate Relocating @g %g's %s from %b to %c...
 Relocating @g %g's %s to %c...
 Relocating blocks Reserved @i %i (%Q) has @n mode.   Resize @i (re)creation failed: %m. Resize @i not valid.   Resize_@i not enabled, but the resize @i is non-zero.   Restarting e2fsck from the beginning...
 Run @j anyway Running command: %s
 SALVAGED SPLIT SUPPRESSED Salvage Scanning inode table Second @e '%Dn' (@i=%Di) in @d @i %i @s '..'
 Setting current mount count to %d
 Setting error behavior to %d
 Setting filesystem feature '%s' not supported.
 Setting filetype for @E to %N.
 Setting interval between checks to %lu seconds
 Setting maximal mount count to %d
 Setting reserved blocks gid to %lu
 Setting reserved blocks uid to %lu
 Setting stride size to %d
 Setting stripe width to %d
 Setting time filesystem last checked to %s
 Should never happen: resize inode corrupt!
 Sparse superblocks not supported with revision 0 filesystems
 Special (@v/socket/fifo) @i %i has non-zero size.   Special (@v/socket/fifo/symlink) file (@i %i) has immutable
or append-only flag set.   Split Ssuper@b Superblock backups stored on blocks:  Superblock invalid, Suppress messages Symlink %Q (@i #%i) is @n.
 Syntax error in e2fsck config file (%s, line #%d)
	%s
 Syntax error in mke2fs config file (%s, line #%d)
	%s
 TRUNCATED Testing with pattern 0x Testing with random pattern:  The -c and the -l/-L options may not be both used at the same time.
 The -t option is not supported on this version of e2fsck.
 The @f size (according to the @S) is %b @bs
The physical size of the @v is %c @bs
Either the @S or the partition table is likely to be corrupt!
 The Hurd does not support the filetype feature.
 The filesystem already has a journal.
 The filesystem revision is apparently too high for this version of e2fsck.
(Or the filesystem superblock is corrupt)

 The needs_recovery flag is set.  Please run e2fsck before clearing
the has_journal flag.
 The primary @S (%b) is on the bad @b list.
 The resize maximum must be greater than the filesystem size.
 The resize_inode and meta_bg features are not compatible.
They can not be both enabled simultaneously.
 This doesn't bode well, but we'll try to go on...
 This filesystem will be automatically checked every %d mounts or
%g days, whichever comes first.  Use tune2fs -c or -i to override.
 Too many illegal @bs in @i %i.
 Truncate Truncating UNLINKED Unable to resolve '%s' Unconnected @d @i %i (%p)
 Unexpected @b in @h %d (%q).
 Unhandled error code (0x%x)!
 Unknown extended option: %s
 Unknown pass?!? Unlink Updating inode references Usage: %s [-F] [-I inode_buffer_blocks] device
 Usage: %s [-RVadlv] [files...]
 Usage: %s [-r] [-t]
 Usage: %s disk
 Usage: e2label device [newlabel]
 Usage: fsck [-AMNPRTV] [ -C [ fd ] ] [-t fstype] [fs-options] [filesys ...]
 Usage: mklost+found
 Version of %s set as %lu
 WARNING: PROGRAMMING BUG IN E2FSCK!
	OR SOME BONEHEAD (YOU) IS CHECKING A MOUNTED (LIVE) FILESYSTEM.
@i_link_info[%i] is %N, @i.i_links_count is %Il.  They @s the same!
 WARNING: bad format on line %d of %s
 WARNING: couldn't open %s: %s
 WILL RECREATE Warning!  %s is mounted.
 Warning... %s for device %s exited with signal %d.
 Warning: %d-byte blocks too big for system (max %d), forced to continue
 Warning: Group %g's @S (%b) is bad.
 Warning: Group %g's copy of the @g descriptors has a bad @b (%b).
 Warning: blocksize %d not usable on most systems.
 Warning: could not erase sector %d: %s
 Warning: could not read @b %b of %s: %m
 Warning: could not read block 0: %s
 Warning: could not write @b %b for %s: %m
 Warning: illegal block %u found in bad block inode.  Cleared.
 Warning: label too long, truncating.
 Warning: skipping journal recovery because doing a read-only filesystem check.
 Warning: the backup superblock/group descriptors at block %u contain
	bad blocks.

 Weird value (%ld) in do_read
 While reading flags on %s While reading version on %s Writing inode tables:  Writing superblocks and filesystem accounting information:  You can remove this @b from the bad @b list and hope
that the @b is really OK.  But there are no guarantees.

 You must have %s access to the filesystem or be root
 Zeroing journal device:  aborted aextended attribute bad error behavior - %s bad gid/group name - %s bad inode map bad interval - %s bad mounts count - %s bad num inodes - %s bad reserved block ratio - %s bad reserved blocks count - %s bad revision level - %s bad uid/user name - %s bad version - %s
 badblocks forced anyway.
 badblocks forced anyway.  Hope /etc/mtab is incorrect.
 bblock block bitmap block device blocks per group count out of range blocks per group must be multiple of 8 blocks to be moved can't allocate memory for test_pattern - %s cancelled!
 ccompress character device check aborted.
 ddirectory directory directory inode map done
 done

 done                            
 during ext2fs_sync_device during seek during test data write, block %lu e2fsck_read_bitmaps: illegal bitmap block(s) for %s e2label: cannot open %s
 e2label: cannot seek to superblock
 e2label: cannot seek to superblock again
 e2label: error reading superblock
 e2label: error writing superblock
 e2label: not an ext2 filesystem
 eentry elapsed time: %6.3f
 empty dir map empty dirblocks ext attr block map ext2fs_new_@b: %m while trying to create /@l @d
 ext2fs_new_@i: %m while trying to create /@l @d
 ext2fs_new_dir_@b: %m while creating new @d @b
 ext2fs_write_dir_@b: %m while writing the @d @b for /@l
 ffilesystem filesystem fsck: %s: not found
 fsck: cannot check %s: fsck.%s not found
 getting next inode from scan ggroup hHTREE @d @i i_blocks_hi @F %N, @s zero.
 i_dir_acl @F %Id, @s zero.
 i_faddr @F %IF, @s zero.
 i_file_acl @F %If, @s zero.
 i_frag @F %N, @s zero.
 i_fsize @F %N, @s zero.
 iinode imagic inode map in malloc for bad_blocks_filename in-use block map in-use inode map inode bitmap inode done bitmap inode in bad block map inode loop detection bitmap inode table inodes (%llu) must be less than %u internal error: couldn't lookup EA inode record for %u invalid block size - %s invalid inode ratio %s (min %d/max %d) invalid inode size %d (min %d/max %d) invalid inode size - %s invalid reserved blocks percent - %s it's not safe to run badblocks!
 jjournal journal llost+found meta-data blocks mke2fs forced anyway.
 mke2fs forced anyway.  Hope /etc/mtab is incorrect.
 mmultiply-claimed multiply claimed block map multiply claimed inode map nN named pipe need terminal for interactive repairs ninvalid no no
 oorphaned opening inode scan pproblem in reading directory block reading indirect blocks of inode %u reading inode and block bitmaps reading journal superblock
 regular file regular file inode map reserved blocks reserved online resize blocks not supported on non-sparse filesystem returned from clone_file_block rroot @i size of inode=%d
 socket sshould be symbolic link time: %5.2f/%5.2f/%5.2f
 too many inodes (%llu), raise inode ratio? too many inodes (%llu), specify < 2^32 inodes unable to set superblock flags on %s
 unknown file type with mode 0%o unknown os - %s uunattached vdevice while adding filesystem to journal on %s while adding to in-memory bad block list while allocating buffers while allocating zeroizing buffer while beginning bad block list iteration while calling ext2fs_block_iterate for inode %d while checking ext3 journal for %s while clearing journal inode while creating /lost+found while creating in-memory bad blocks list while creating root dir while determining whether %s is mounted. while doing inode scan while expanding /lost+found while getting next inode while getting stat information for %s while initializing journal superblock while looking up /lost+found while marking bad blocks as used while opening %s while opening %s for flushing while opening inode scan while printing bad block list while processing list of bad blocks from program while reading bitmaps while reading flags on %s while reading in list of bad blocks from file while reading journal inode while reading journal superblock while reading root inode while reading the bad blocks inode while recovering ext3 journal of %s while reserving blocks for online resize while resetting context while retrying to read bitmaps for %s while sanity checking the bad blocks inode while setting bad block inode while setting flags on %s while setting root inode ownership while setting up superblock while setting version on %s while starting inode scan while trying popen '%s' while trying to allocate filesystem tables while trying to determine device size while trying to determine filesystem size while trying to determine hardware sector size while trying to flush %s while trying to initialize program while trying to open %s while trying to open external journal while trying to open journal device %s
 while trying to re-open %s while trying to resize %s while trying to run '%s' while trying to stat %s while updating bad block inode while writing block bitmap while writing inode bitmap while writing inode table while writing journal inode while writing journal superblock while writing superblock will not make a %s here!
 yY yes yes
 zzero-length Project-Id-Version: e2fsprogs
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-05-17 20:31-0400
PO-Revision-Date: 2011-06-25 23:52+0000
Last-Translator: Waldir Leoncio <Unknown>
Language-Team: Portuguese (Brazil) <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 16:07+0000
X-Generator: Launchpad (build 18115)
 	%Q (@i #%i, modo tempo %IM)
 	<@f metadata>
 	Usando %s
 	Usando %s, %s
 
	durante tentativa de acrescentar diário ao dispositivo %s 
	durante tentativa de criação de diário 
	durante tentativa de criar arquivo de diário 
	durante tentativa de abrir um diário em %s
 

%s: INCONSISTÊNCIA INESPERADA; EXECUTE O fsck MANUALMENTE.
	(ou seja, sem as opções -a ou -p)
 
  tabela de inode em  
  Blocos GDT reservados em  
%s: %s: erro ao ler bitmaps: %s
 
%s: ***** O SISTEMA DE ARQUIVOS FOI MODIFICADO *****
 
%s: ********** ATENÇÃO: O sistema de arquivos ainda tem erros **********

 
*** diário foi recriado - sistema de arquivos é ext3 novamente ***
 
Opção(ões) estendida(s) mal especificada(s): %s

Opções estendidas são separadas por vírgula e podem levar um argumento
	iniciado por um sinal de igual ('=').

Opções estendidas válidas são:
	superblock=<número do superbloco>
	blocksize=<tamanho do bloco>
 
Ajuda de emergência:
 -p Reparo automático (sem perguntas)
 -n Não fazer alterações no sistema de arquivos
 -y Responder todas as questões como "sim"
 -c Procurar por defeitos no bloco e adicioná-los na lista de blocos defeituosos
 -f Forçar checagem mesmo que o sistema de arquivos esteja marcado como limpo
 
Sistema de arquivos muito pequeno para usar journal
 
Se o @b estiver em estado crítico, o @f não poderá ser reparado.
 
Interrupção pega, limpando
 
Argumento não-numérico inválido para -%c ("%s")

 
Tamanho do bloco do diário: %u
Tamanho do diário: %u
Primeiro bloco do diário: %u
Sequência do jornal: 0x%08x
Início do diário: %u
Número de usuários do diário: %u
 
Tamanho de journal muito grande para o sistema de arquivos.
 
Executando passos adicionais para resolver @bs reclamado por mais de um @i...
Passo 1B: Procurando novamente por @m @bs
 
Bandeira de superblocos escassos ativada. %s 
O @i @b inválido provavelmente foi corrompido. Talvez você 
devesse parar no momento e executar e2fsck -c para procurar por blocos
inválidos no @f.
 
Aparentemente o dispositivo não existe; você o especificou corretamente?
 
O sistema de arquivos já possui superblocos escassos.
 
O tamanho do journal solicitado é de %d blocos; ele deve estar
entre 1024 e 10240000 blocos. Abortando.
 
Aviso, problema durante escrita de superblocos. 
Aviso: largura %u da segmentação RAID não é um múltiplo par do passo largo %u.

   %s superbloco em    Bitmap de bloco em    Blocos livres:    inodes livres:   (verificar na próxima montagem)  (verificar deferidos; na bateria)  (verificar na montagem %ld )  (y/n)  -v Verbalizar
 -b superblock Usar superbloco alternativo
 -B blocksize Forçar tamanho do bloco ao procurar por superbloco
 -j external_journal Definir localização do diário externo
 -l bad_blocks_file Adicionar à lista de blocos ruins
 -L bad_blocks_file Definir lista de blocos ruins
  Terminado.
  Descritor de grupo em   contem um sistema de arquivo com erro  foi montado %u vezes sem ser verificado  passou %u dias sem ser verificado  recursos primários do superbloco diferentes da cópia de segurança  não foi desmontado corretamente # Despejo de extensão:
 blocos de %d-bytes grandes demais para o sistema (máximo %d) %s %s: status é %x, nunca deveria acontecer.
 %s @o @i %i (uid=%Iu, gid=%Ig, modo=%Im, tamanho=%Is)
 %s tem a(s) característica(s) não suportada(s): %s está aparentemente em uso pelo sistema;  %s é o dispositivo inteiro, não apenas uma partição!
 %s está montado;  %s não é um dispositivo especial de bloco.
 %s não é um dispositivo de diário.
 %s: %s nome do arquivo nblocos tamanho do bloco
 %s: %s experimentando blocos de cópia de segurança...
 %s: ***** REINICIAR O LINUX *****
 %s: Erro %d enquanto executava fsck.%s para %s
 %s: As opções -n e -w são mutuamente exclusivas.

 %s: e2fsck cancelado.
 %s: 'journal' muito curto
 %s: nenhum super-bloco 'journal' foi encontrado
 %s: recuperando 'journal'
 %s: pulando linha ruim em /etc/fstab: montagem de ligação com número de passo fsck diferente de zero
 %s: muitos argumentos
 %s: dispositivos demais
 %s: espere: Não há mais processos filhos?!?
 %s: não fará recuperação do 'journal' enquanto somente leitura
 %s? não

 %s? sim

 %u grupo de bloco
 %u grupos de blocos
 %u blocos por grupo, %u fragmentos por grupo
 %u inodes por grupo
 %u inodes varridos.
 '.' @d @e em @d @i %i não termina em NULL
 '..' @d @e em @d @i %i não termina em NULL
 '..' no %Q (%i) é %P (%j), @s %q (%d).
 (NENHUM) (Há %N @is contendo @m @bs.)

 (sem prompt) , Descritores de grupo em  , Bitmap de inode em  , verificação forçada.
 --aguardando-- (passo %d)
 -O só pode ser especificado uma única vez -o só pode ser especificado uma única vez /@l não é um @d (ino=%i)
 /@l não encontrado.   <Inode 10 reservado> <Inode  9 reservado> <O Inode NULO> <O inode dos blocos ruins> <O inode do carregador de boot> <O inode do descritor de grupo> <O inode journal> <O inode de restauração (undelete) de diretório> <n> <y> = é incompatível com - e +
 erro alocando %N bloco(s) não fragmentado(s) no @b @g %g para %s: %m
 @A @a @b %b.   @A @b @B (%N): %m
 erro alocando block buffer para realocar %s
 @A @d @b vetor: %m
 @A @i @B (%N): %m
 @A @i @B (@i_dup_map): %m
 erro alocando icount link: %m
 @A estrutura icount: %m
 @A novo @d @b para @i %i (%s): %m
 @Uma estrutura refcount(%N): %m
 @d @i %i possui dtime zero.   @E @L para '.'   @E @L para @d %P (%Di).
 @E @L para o @r.
 @E possui @D/não utilizados @i %Di.   @E tem @n @i #: %Di.
 @E possui um nome @z.
 @E tem um nome de arquivo não-exclusivo.
Renomear para %s @E possui um tipo de arquivo incorreto (era %Dt, @s %N).
 @E possui um tipo de arquivo definido.
 @E contém caracteres ilegais nesse nome.
 @E possui um rec_len de %Dr, @s %N.
 @E é um '.' duplicado @e.
 @E é um '..' duplicado @e.
 @E aponta para @i (%Di) localizado em um @b ruim.
 @I @i %i em @o @i lista.
 @I @o @i %i em @S.
 @S @b_size = %b, fragsize = %c.
Essa versão do e2fsck não suporta fragmentos de tamanho de bloco 
diferentes de @b.
 English:   	 Copy text   	
@S @bs_per_group = %b, should have been %c
 @S first_data_@b = %b, deve ter sido %c
 @S sugestão para superbloco externo @s %X.   @a @b %b tem h_@bs > 1.   @a @b %b está corrompido (@n nome).   @a @b %b está corrompido (@n valor).   @a @b %b está corrompida (conflito de alocação).   @a @b @F @n (%If).
 @a em @i %i possui um namelen (%N) igual a @n
 @a em @i %i tem um valor @b (%N) que é @n (deve ser 0)
 @a em @i %i possui um deslocamente de valor (%N) igual a @n
 @a em @i %i tem um tamanho (%N) que é @n
 Diferenças @b @B:  @b @B para @g %g não está em @g. (@b %b)
 @f contém arquivos grandes, mas não possui bandeira LARGE_FILE em @S.
 @f não possui um UUID; gerando um.

 Sistema de arquivos não tem resize_@i habilitado, mas possui s_reserved_gdt_@bs
%N; deve ser zero.   sistema de arquivos tem recurso (s) de emblemas definidos, mas é uma revisão do sistema de arquivos 0.   @g %g's @b @B (%b) is bad.   @g %g's @b @B at %b @C.
 @g %g's @i @B (%b) is bad.   @g %g's @i @B at %b @C.
 @g %g's @i table at %b @C.
 @h %i possui uma profundidade de árvore (%N) grande demais
 @h %i tem um nó raiz @n.
 @h %i tem uma versão do hash não suportado (%N)
 @h %i usa uma bandeira htree de nódulo do root incompatível.
 @i %i (%Q) possui modo @n (%Im).
 @i %i (%Q) é um @I @b @v.
 @i %i (%Q) é um @I FIFO.
 @i %i (%Q) é um caractere @I @v.
 @i %i (%Q) é um soquete @I.
 @i %i está com flag @cion setada no @f sem ter suporte a @cion   @i %i  tem conjunto sinalizador INDEX_FL mas não é um @d.
 @i %i tem conjunto sinalizador INDEX_FL em @f sem suporte ao htree.
 @i %i foi uma má @a @b %b.   @i %i tem um tamanho extra (%IS) o qual é @n
 @i %i possui @b(s) não-permitidos(s).   @i %i possui uma flag imagic atribuída.   @i %i é um %It, mas na verdade aparenta ser um diretório.
 @i %i é um @z @d.   @i %i está em uso, mas possui dtime atribuído.   @i %i é muito grande.   @i %i contador ref é %Il, @s %N.   @i %i foi uma parte de o @o @i lista.   @i %i, i_@bs is %Ib, @s %N.   @i %i, i_size is %Is, @s %N.   Diferenças @i @B:  @i @B para @g %g não está em @g. (@b %b)
 @i contar em @S é %i, @s %j.
 @i tabela para @g %g não está em @g. (@b %b)
AVISO: POSSÍVELMENTE DADOS SERÃO PERDIDOS.
 @is que foram parte de uma lista de links órfãos corrompida encontrada.   @j @i não está em uso, mas contém dados.   @j não é um arquivo regular.   Versão do @j não suportada por este e2fsck.
 @m @b(s) em @i %i: @m @bs já foi redesignado ou clonado.

 @n @h %d (%q).   @n @i número de '.' em @d @i %i.
 @p @h %d (%q): @b ruim número %b.
 @p @h %d: nó raiz é @n
 @r possui dtime atribuído (provavelmente devido a um mke2fs anterior).   @r não é um @d.   @r não é um @d; anulando.
 @r não alocado.   @u @i %i
 @u @z @i %i.   ABORTADO ALOCADO Abortar Interrompendo....
 Adicionando dirhash hint ao sistema de arquivos.

 Acrescentando diário ao dispositivo %s:  Atribuição de um erro Alocar BLKFLSBUF ioctl não suportado! Impossível limpar área de memória.
 Realizando backup das informações de @j @i @b.

 Cópia de segurança @B %b inválido usado como @i @b inválido desalinha @b.   @b @i ruim tem um @b (%b) indireto que entra em conflito com
a metadata @f.   @I @b inválido possui @b(s) não-permitidos.   Bloco ruim %u fora de alcance: ignorado.
 Blocos ruins: %u /@l ruim ou inexistente.  Não é possível reconectar.
 Bbitmap Iniciar passo %d (máximo = %lu)
 O bloco %b no @g descritores primários está na lista de @b ruins.
 Bloco %d ruim na área do descritor primário de superbloco/grupo.
 Tamanho do bloco=%u (log=%u)
 Blocos %u a %u devem estar bons para permitir a construção de um sistema de arquivos.
 LIMPO CONTINUAR CRIADO Não pode continuar. Não pode encontrar o @j externo
 Não é possível continuar, abortar.

 Não é possível continuar sem um @r.
 Conflito com outro sistema de arquivos @b Verificando todos os sistemas de arquivos.
 Verificando blocos %lu até %lu
 Procurando blocos ruins (teste leitura-escrita não destrutiva)
 Verificando por blocos defeituosos (teste em modo de leitura):  Procurando blocos ruins em modo leitura-escrita não destrutiva
 Verificando por blocos defeituosos em modo de leitura
 Verificando por blocos defeituosos em modo de leitura e escrita
 Limpar Limpar @j Limpar árvore HTree Limpar inode Limpando Remoção de recurso de sistema de arquivos '%s' não suportada.
 Bloquear clone multiplo-claimed Conectar à /lost+found Continuar Deterioração encontrada em @S. (%s = %N).
 Não foi possível expandir /@l: %m
 Não foi possível reconectar %i: %m
 Será esta uma partição de tamanho zero?
 Impossível alocar a área de memória do bloco (tamanho=%d)
 Não se pôde alocar buffer de cabeçalho
 Não foi possível alocar memória para tipos de sistema de arquivos
 Não se pôde alocar memória para analisar opções de diário!
 Não se pôde alocar memória para analisar opções!
 Não se pôde alocar variável de caminho em chattr_dir_proc Não foi possível clonar arquivo: %m
 Não foi possível determinar o tamanho do dispositivo; você deve especificar
o tamanho manualmente
 Não foi possível determinar o tamanho do dispositivo; você deve especificar
o tamanho do sistema de arquivos
 Não se pôde encontrar números mágidos do superbloco de diário Não se pôde encontrar superbloco de sistema válido.
 Não se pôde consertar origem de @i %i: %m

 Não se pôde consertar origem de @i %i: Não se pôde encontrar origem de @d @e

 Não se pôde analisar especificador data/hora: %s Criar Criando diário (%d blocos):  Criando inode de diário:  Criando diário no dispositivo %s:  Ddeleted Excluir arquivo Tamanho do dispositivo relatado como zero. Partição especificada inválida, ou
	a tabela de partições não foi relida após a execução do fdisk, devido a
	uma partição modificada estar ocupada e em uso. Talvez seja necessário reiniciar
	para reler sua tabela de partições.
 Diretórios não puderam contar para @g #%g (%i, contou-se=%j).
 Disco protegido contra escrita; use a opção -n para fazer a verificação
no disco somente no modo de leitura.
 Você realmente deseja continuar? @E duplicada encontrada.   Encontrado @e '%Dn' duplicado.
	Marcando %p (%i) para ser reconstruído.

 @b duplicado ou ruim em uso!
 E2FSCK_JBD_DEBUG "%s" não inteiro

 E@e '%Dn' em %p (%i) ERRO: Não é possível abrir /dev/null (%s)
 EXPANDIDO Todos ou nenhum dos tipos de sistema de arquivos passados ao -t devem ser prefixados
com 'no' ou '!'.
 Bloco de diretório %u (#%d) vazio no inode %u
 Erro ajustando refcount para @a @b %b (@i %i): %m
 Erro ao copiar em substituição @b @B: %m
 Erro ao copiar em substituição @i @B: %m
 Erro ao criar /@l @d (%s): %m
 Erro ao criar @d raiz (%s): %m
 Erro ao remover alocação @i %i: %m
 Erro determinando o tamanho físico do @v: %m
 Erro ao iterar acima de @d @bs: %m
 Movendo erro @j: %m

 Erro de leitura @a @b %b (%m).   Erro de leitura @a @b %b para @i %i.   Erro ao ler @d @b %b (@i %i): %m
 Erro lendo @i %i: %m
 Erro lendo bloco %lu (%s) enquanto %s.   Erro lendo bloco %lu (%s).   Erro ao armazenar @d @b (@i=%i, @b=%b, num=%N): %m
 Erro ao armazenar informações de contagem de @i (@i=%i, count=%N): %m
 Erro ao validar descritor de arquivo %d: %s
 Erro ao ajustar quantidade de @i no @i %i
 Erro enquanto interagia em @bs em @i %i (%s): %m
 Erro ao interagir sobre @bs no @i %i: %m
 Erro buscando @is (%i): %m
 Erro ao examinar nódulos (%i): %m
 Erro ao tentar encontrar /@l: %m
 Erro gravando @a @b %b (%m).   Erro ao gravar @d @b %b (@i %i): %m
 Erro escrevendo bloco %lu (%s) enquanto %s.   Erro escrevendo bloco %lu (%s).   Erro: versão da biblioteca ext2fs desatualizada!
 Expandir Estendendo a tabela de inodes O @j externo não suporta este @f
 O @j externo tem um @S ruim
 @j externo tem vários usuários @f (não suportado).
 ARQUIVO REMOVIDO (CORRIGIDO) Ffor @i %i (%Q) é Recursos do sistema de arquivos não suportada por sistemas de arquivos de revisão 0
 Rótulo do sistema de arquivos=%s
 Sistema de arquivos maior que aparente tamanho do dispositivo. Sistema de arquivos montado ou aberto exclusivamente por outro programa?
 UUID do sistema de arquivos não encontrado em um dispositivo de diário.
 Finalizado com %s (estado de saída %d)
 Primeiro @e '%Dn' (@i=%Di) em @d @i %i (%p) @s '.'
 Primeiro bloco de dados=%u
 Corrigir Sinalização de %s definidas como  Forçar reescrita Encontrados campos @n V2 @j @S (de V1 @j).
Limpando campos posteriores a V1 @j @S...

 Tamanho do fragmento=%u (log=%u)
 Contagem @bs livre errada (%b, contou-se=%c).
 Contagem @bs livre errada para @g #%g (%b, contou-se=%c).
 Contagem @is livre errada (%i, contou-se=%j).
 Contagem @is livre errada para @g #%g (%i, contou-se=%j).
 A partir do bloco %lu ao %lu
 Obter uma nova versão do e2fsck! Grupo %lu: (Blocos  Descritores de grupo parecem ruins... ÍNDICE HTREE LIBERADO IGNORADO INODE LIMPADO Ignorar erro Iillegal Número de blocos por grupo ilegal Número ilegal de blocos!
 Erro interno: não foi encontrado dir_info for para %i.
 Erro interno: final ambíguo do bitmap (%N)
 Versão EA inválida.
 Passo largo de RAID inválido: %s
 Largura da segmentação RAID inválida: %s
 Formato UUID inválido
 Parâmetro de superbloco inválido: %s
 Descritor de arquivo de completude de informação inválido Opção definida do sistema de arquivos inválida: %s
 Opção definida de montagem inválida: %s
 Parâmetro de redimensionamento inválido: %s
 comprimento de stride inválido Parâmetro de passo largo inválido: %s
 Parâmetro de largura da segmentação inválido: %s
 Parâmetro de superbloco inválido: %s
 Tamanho de bloco de desenvolvimento do diário (%d) menor que o tamanho de bloco mínimo %d
 Diário removido
 Tamaho do Journal:              Superbloco de diário não encontrado!
 Usuários do diário: %s
 Diários não suportados por sistemas de arquivos de revisão 0
 É um link Multiplo-Claimed clonado bloqueado Máximo de blocos de sistema de arquivos=%lu
 Máximo de um test_pattern pode ser especificado no modo somente leitura Memória usada: %d, tempo decorrido: %6.3f/%6.3f/%6.3f
 Perdendo '.' em @d @i %i.
 Faltando '..' in @d @i %i.
 Movendo @j de /%s para oculto @i.

 Movendo tabela de inodes Deve-se usar '-v', =, - or +
 Sem espaço em @l @d.   Nota: se diversos blocos inode, blocos bitmap ou uma parte
da tabela de inode requerer realocação, você pode tentar
executar o e2fsck com a ooção ' -b %S'. O problema
pode estar no grupo de descritores do bloco primário, e
o bloco de backup do grupo de descritores pode estar OK.

 Redimensionamento on-line não é suportado em sistemas de arquivos com revisão 0
 Apenas as opções -p/-a, -n or -y podem ser especificadas. Otimizando diretórios:  Acabou a memória ao remover os setores %d-%d
 ERRO DE PROGRAMAÇÃO: @f (#%N) pontos de término @B (%b, %c) não batem com os @B pontos de término calculados (%i, %j)
 Espaçamento ao final de @b @B não definido.  Espaçamento ao final de @i @B não definido.  Passo 1 Passo 1: Verificando @is, @bs, e os tamanhos.
 Passo 1C: Procurando por @is com @m @bs em diretórios
 Passo 1D: Reconciliando @m @bs
 Passo 2 Passo 2: Verificando estrutura @d
 Passo 3 Passo 3: Checando conectividade com o @d
 Passo 3A: Otimizando diretórios
 Passo 4 Passo 4: Checando contagens de referência
 Passo 5 Passo 5: Procurando informações de resumo @g
 Pico de memória Por favor, execute 'e2fsck -f %s' em primeiro lugar.

 Por favor, execute 2fsck no seu sistema de arquivo.
 Dispositivo de swap possivelmente não existente?
 Primária Continuar mesmo assim? (y,n)  erro de programação? @b #%b reivindicado por nenhuma razão em process_bad_ @ b
 RECONECTADO REALOCADO test_pattern aleatório não é permitido no modo somente leitura Lendo e comparando:  Flag de restauração não configurada no @S de backup, executando @j de qualquer forma.
 Recriar Realocar Realocando @g %g's %s de %b para %c...
 Realocando @g %g's %s para %c...
 Realocando blocos O @i reservado %i (%Q) tem um modo @n.   Redimensionar @i (re)criar falhou: %m. Redimensionamento do @i não é válido.   Resize_@i não está habilitado, mas o resize inode é diferente de zero.   Reiniciando e2fsck do início...
 Execute @j de qualquer forma Executando o comando: %s
 SALVO DIVIDIDO SUPRIMIDO Recuperação Varrendo tabela de inodes Segundo @e '%Dn' (@i=%Di) em @d @i %i @s '..'
 Definindo contagem de montagens atual como %d
 Definindo comportamento de erro como %d
 Definição de recurso de sistema de arquivos '%s' não suportada.
 Definindo tipo de arquivo de @E como %N.
 Definindo intervalo entre verificações como %lu segundos
 Definindo contagem máxima de montagens como %d
 Definindo gid de blocos reservados como %lu
 Definindo uid de blocos reservados para %lu
 Definindo tamanho do passo largo como %d
 Definindo largura da segmentação como %d
 Definindo hora da última verificação do sistema de arquivos como %s
 Nunca deveria acontecer: redimensionar inode corrompido!
 Superblocos esparsos não suportados por sistemas de arquivos de revisão 0
 Special (@v/socket/fifo) @i %i possui tamanho não nulo.   Arquivo especial (@v/socket/fifo/symlink) (@i %i) é immutável
ou está com flag de append-only setada.   Dividir Ssuper@b Cópias de segurança de superblocos gravadas em blocos:  Superbloco inválido, Suprimir mensagens Link simbólico %Q (@i #%i) é @n.
 Erro de sintaxe no arquivo de configuração do e2fsck (%s, line #%d)
	%s
 Erro de sintaxe no arquivo de configuração do mke2fs (%s, line #%d)
	%s
 TRUNCADO Testando com padrão 0x Testando com padrão aleatório:  As opções -c e -l/-L não podem ser usadas ao mesmo tempo.
 A opção -t não é suportada nesta versão do e2fsck.
 O tamanho de @f (segundo o @S) é %b @bs
O tamanho físico do @v é de %c @bs
Ou o @S ou a tabela de partição aparentam estar corrompidos!\n
 O 'Hurd' não suporta a função tipo de arquivo.
 O sistema de arquivos já possui um diário.
 A revisão do sistema de arquivos é muito superior para esta versão do e2fsck.
(Ou o superbloco do sistema de arquivos está corrompido)

 A bandeira needs_recovery está ativada. Favor executar e2fsck antes de remover
a bandeira has_journal.
 O @S primário (%b) está incluso na lista de @bs inválidos.
 O redimensionamento máximo deve ser maior do que o tamanho do sistema de arquivos.
 Os recursos resize_inode e meta_bg não são compatíveis.
Eles não podem ser simultaneamente habilitados.
 Não é um bom sinal, mas tentaremos prosseguir...
 Este sistema de arquivos será automaticamente verificado quando completar %d montagens ou
%g dias, o que ocorrer primeiro. Use tune2fs -c ou -i para fazer alterações.
 Muitos @bs ilegais em @i %i.
 Truncado Truncando SEM LIGAÇÃO Não foi possível resolver '%s' @d @i %i (%p) desconectado
 @b inesperado em @h %d (%q).
 Código de erro não tratado (0x%x)!
 Opção estendida desconhecida: %s
 Passo desconhecido?!? Excluir Atualizando referências dos inodes Uso: %s [-F] [-l inode_buffer_blocks] dispositivo
 Uso: %s [-RVadlv] [files...]
 Uso: %s [-r] [-t]
 Uso: %s disco
 Uso: e2label device [novo rótulo]
 Uso: fsck [-AMNPRTV] [ -C [ fd ] ] [-t fstype] [fs-options] [filesys ...]
 Uso: mklost+found
 Versão do %s definido como %lu
 ATENÇÃO: ERRO DE PROGRAMAÇÃO NO E2FSCK!
	OU ALGUM CABEÇA DURA (VOCÊ) ESTÁ VERIFICANDO UM SISTEMA DE ARQUIVOS MONTADO (LIVE).
@i_link_info[%i] is %N, @i.i_links_count is %Il.  Eles @s os mesmos!
 AVISO: formato ruim na linha %d de %s
 AVISO: não foi possível abrir %s: %s
 SERÁ RECRIADO Atenção!  %s está montado.
 Atenção... %s para dispositivo %s saindo com sinal %d.
 Aviso: blocos de %d-bytes grandes demais para o sistema (máximo %d), forçado a continuar
 Aviso: Grupo %g's @S (%b) é ruim.
 Aviso: Grupo %g's a cópia do @g foi uma descrição ruim @b (%b).
 Aviso: tamanho de bloco %d inútil na maioria dos sistemas.
 Atenção: não foi possível apagar o setor %d: %s
 Aviso: não podia ler @b %b do %s: %m
 Atenção: não foi possível ler o bloco 0: %s
 Aviso: não podia escrever @b %b for %s: %m
 Aviso: bloco ilegal %u encontrado em inode de bloco ruim. Limpado.
 Aviso: rótulo grande demais, truncando.
 Aviso: pulando recuperação de diário por estar verificando um sistema de arquivos somente leitura.
 Aviso: a cópia de segurança dos descritores de superblocos/grupo no bloco %u contém
	blocos ruins.

 Valor esquisito (%ld) em do_read
 Durante leitura de bandeiras em %s Durante leitura de versão em %s Gravando tabelas inode:  Escrevendo superblocos e informações de contabilidade de sistema de arquivos:  Você pode remover este @b da lista de @b ruins e esperamosque o @b esteja bom. Mas não há garantia.

 Você precisa ter acesso %s ao sistema de arquivos ou ser root
 Zerando dispositivo de diário:  abortado atributo aextended comportamento de erro inválido - %s nome de gid/grupo inválido - %s mapa de inode inválido intervalo intervalo - %s contagem de montagens inválida - %s número de inodes inválido - %s razão de blocos reservados inválido - %s contagem de blocos reservados inválida - %s nível de revisão inválido - %s nome de uid/usuário inválido - %s versão ruim - %s
 blocos ruins forçados mesmo assim.
 blocos ruins forçados mesmo assim. Espera-se que /etc/mtab esteja incorreto.
 bblock bloco 'bitmap' dispositivo de bloco contagem de blocos por grupo fora de alcance blocos por grupo devem ser múltiplos de 8 blocos a serem movidos não se pôde alocar memória para test_pattern - %s cancelado!
 ccompress Dispositivo de caractere verificação anulada.
 ddirectory diretório mapa de inode de diretório concluído
 concluído

 pronto                            
 durante ext2fs_sync_device durante a busca durante escrita de dados de teste, bloco %lu e2fsck_read_bitmaps: blocos de bitmap ilegais em %s e2label: não se pôde abrir %s
 e2label: não se pôde procurar superbloco
 e2label: não se pôde procurar superbloco novamente
 e2label: erro ao ler superbloco
 e2label: erro ao gravar superbloco
 e2label: não é um sistema de arquivos ext2
 eentry tempo decorrido: %6.3f
 mapa de diretório vazio dirblocks vazio ext attr block map ext2fs_new_@b: %m enquanto tentava criar /@l @d
 ext2fs_new_@i: %m durante criação de /@l @d
 ext2fs_new_dir_@b: %m durante criação de novo @d @b
 ext2fs_write_dir_@b: %m durante tentativa de @d @b para /@l
 ffilesystem sistema de arquivos fsck: %s: não encontrado
 fsck: não foi possível verificar %s: fsck.%s não encontrado
 obtendo próximo inode para verredura ggroup hHTREE @d @i i_blocks_hi @F %N, @s zero.
 i_dir_acl @F %Id, @s zero.
 i_faddr @F %IF, @s zero.
 i_file_acl @F %If, @s zero.
 i_frag @F %N, @s zero.
 i_fsize @F %N, @s zero.
 iinode mapa de inode 'imagic' em malloc por bad_blocks_filename mapa de bloco em uso mapa de inode em uso inode 'bitmap' feito bitmap do inode inode em um mapa de bloco ruim detecção de loop no bitmap inode tabela de inode inodes (%llu) deve ser menos que %u erro interno: nao pode encontrar o registro de inode EA para %u tamanho do bloco inválido - %s proporção inválida do inode  %s (mín %d/máx %d) tamanho inválido do inode %d (mín %d/máx %d) tamanho inválido do inode - %s porcentagem inválida de blocos reservados - %s Não é seguro rodar blocos ruins!
 jjournal diário llost+found blocos de meta-dados mke2fs forçado de qualquer maneira.
 mke2fs forçado mesmo assim. Espera-se que o /etc/mtab/ esteja incorreto.
 mmultiply-claimed crescimento exigiu mapa de bloco multiplicar mapa de inode requisitado nN pipe nomeado é necessário um terminal para reparos interativos ninvalid não não
 oorphaned iniciando verrendo de inode pproblem em lendo bloco de diretório lendo blocos indiretos do inode %u lendo bitmaps de inodes e blocos lendo super-bloco 'journal'
 arquivo regular mapa de inode de arquivo regular blocos reservados Blocos de redimensionamento on-line reservados não suportados por sistemas de arquivos não-esparsos retornou de clone_file_block rroot @i tamanho do inode=%d
 soquete sdeveria ser link simbólico tempo: %5.2f/%5.2f/%5.2f
 inodes demais (%llu), aumentar razão de inodes? inodes demais (%llu), especifique < 2^32 inodes indisponível definir flag superbloco em %s
 tipo de arquivo desconhecido com modo 0%o os desconhecido - %s uunattached vdevice enquanto acrescentava sistema de arquivos ao diário em %s durante acréscimo à lista de blocos ruins dentro da memória ao alocar buffers durante alocação buffer zerante durante iteração da lista de blocos ruins durante chamada ext2fs_block_iterate para inode %d enquanto verifica ext3 journal para %s durante limpeza de inode de diário ao criar /lost+found durante criação de lista de blocos ruins dentro da memória durante criação de diretório root enquanto determinava se %s está montado. durante varredura de inode ao expandir /lost+found obtendo próximo inode ao obter estatísticas de %s durante inicialização do superbloco do diário durante procura por /lost+found ao marcar blocos defeituosos como usados ao abrir %s abrindo %s para limpeza abrindo verificação de inode durante impressão de lista de blocos ruins durante processamento de lista de blocos ruins do programa durante leitura de bitmaps durante leitura de bandeiras em %s lendo lista de blocos ruins do arquivo enquanto ler inode journal durante leitura de superbloco de diário durante leitura de inode root lendo os inodes dos blocos ruins enquanto recupera ext3 journal de %s durante reserva de blocos para redimensionamento on-line durante redefinição do contexto durante nova tentativa de ler bitmaps em %s checando a sensatez dos inodes dos blocos ruins durante configuração de inode de bloco ruim durante definição de sinalização em %s durante definição de posse do inode root durante configuração de superbloco durante definição da versão em %s durante início da varredura de inode tentando abrir (popen) '%s' durante tentativa de alocar tabelas de sistemas de arquivos ao tentar determinar o tamanho do dispositivo enquanto tenta determinar o tamanho do sistema de arquivos durante tentativa de determinar tamanho de setor de hardware tentando limpar %s durante tentativa de inicialização do programa tentando abrir %s durante tentativa de abrir diário externo durante tentativa de abrir dispositivo de diário %s
 tentando reabrir %s ao tentar redimensionar %s durante tentativa de execução de '%s' durante tentativa de iniciar %s atualizando inode de bloco ruim durante escrita do bitmap de bloco durante escrita do bitmap de inode ao gravar tabela inode durante escrita de inode de diário durante escrita de superbloco de diário ao gravar superbloco não fará um %s aqui!
 yY sim sim
 ztamanho-zero 