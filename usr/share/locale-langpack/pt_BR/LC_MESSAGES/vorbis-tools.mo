��    �     �  �  |       *  (   *     **     H*      d*     �*  $   �*     �*     �*     �*     �*     +  $  +  F   D.  F   �.  B   �.  ;   /  7   Q/  1   �/  >   �/  @   �/  *   ;0  ?   f0  �   �0  F   b1  L   �1  �   �1  ,   y2  J   �2  &  �2  N   4  �   g4  F   c5  <   �5  7   �5  i   6  H   �6  F   �6  1   7  >   K7  ?   �7  l  �7  <   79    t9  z   �:  -   ;  b  9;  �   �<  1   3=  +   e=  d  �=  c   �>  �  Z?  ,   EA  �   rA  �   �A    �B  �  �D  �  xF  �  H  J  �I     #K  j  9K     �L  4   �L     �L     
M  ,   $M     QM  %   oM  ,   �M  -   �M      �M  &   N     8N     XN     xN     ~N     �N  $   �N     �N  Q  �N     P     3P     IP     YP     oP  ,   vP  !   �P  Z   �P  7    Q  *   XQ  -   �Q  S   �Q  <   R  S   BR  +   �R  Q   �R  !   S     6S  4   NS  *   �S  ,   �S     �S     �S     	T     T     )T     =T  $   PT  W   uT     �T     �T  	   �T  A   U  9   EU     U  B   �U  7   �U     V     (V     ;V  0   IV     zV     �V     �V  1   �V  7   �V     #W     ,W  	   9W  &   CW     jW  /   �W  #   �W  %   �W     �W  .   X  #   IX     mX  /   �X  @   �X     �X     Y     9Y     WY  '   uY  %   �Y     �Y     �Y     �Y     �Y     Z     Z     Z  !   3Z     UZ  "   sZ  ?   �Z  ?   �Z  5   [  G   L[     �[  (   �[  '   �[  (   \  :   -\  ?   h\  ;   �\  I   �\  !   .]     P]     j]     �]  2   �]  &   �]  %   �]  *   #^  &   N^  '   u^     �^  1   �^  :   �^  1   *_  M   \_  ?   �_  1   �_  2   `  '   O`  2   w`  >   �`  )   �`  0   a  2   Da  -   wa     �a  4   �a  &   �a  .   b  \   Gb  X   �b     �b     c  #   +c     Oc  [   ^c  @   �c  6   �c  R   2d  >   �d  1   �d     �d  B   e      We  !   xe  "   �e     �e      �e  *   �e  $   )f  +   Nf     zf     �f     �f  L   �f     g  &   &g  >   Mg  4   �g  ,   �g  v   �g     eh     vh      }h     �h  3   �h  2   �h  +   "i     Ni  "   ni  2   �i  .   �i  ,   �i  %    j  6   Fj  /   }j  '   �j  4   �j     
k     k     k     ,k     ?k  ,   Qk  +   ~k     �k  "   �k     �k     l  �  l  �   �n  4   io  6   �o     �o     �o     p     p  ,   -p  -   Zp  '   �p  &   �p  8   �p     q     /q     Gq     eq  :   �q  %   �q     �q     �q  +   �q     )r     :r  >   @r     r     �r     �r  (   �r     �r  ;   s  ;   Cs  :   s  )   �s  8  �s     u     "u     9u  0   Xu     �u  0   �u     �u     �u  *   �u  +   v     :v     Sv  #   mv  ]   �v  �   �v     �w  ,   �w  2   �w  %   x     *x  +   8x  5   dx  N   �x     �x     y     y     $y  $   <y      ay     �y     �y     �y     �y     �y     �y     �y     z  #   z  $   Cz  -   hz     �z     �z     �z     �z     �z     �z  ;   {  %   H{     n{  '   �{  %   �{  +   �{  '   �{  >   %|     d|     v|     ~|     �|     �|     �|     �|  (   �|  ^   }  q   a}  <   �}  '   ~  L   8~     �~  ;   �~  ,   �~     �~          
     &     4  "   Q     t     �  �   �  L   +�  =   x�  )   ��  �   ��  G   ��  !   �     �     �     )�     6�     F�     Y�  <   s�  '   ��  9   ؂  H   �  7   [�  2   ��  L   ƃ  0   �  2   D�  P   w�  K   Ȅ  L   �  O   a�  &   ��  ,   ؅  0   �  1   6�  "   h�     ��  ?   ��  >   �  ?   $�  U   d�  O   ��  I   
�  0   T�  W   ��  C   ݈  5   !�  6   W�  C   ��  E   ҉  .   �  8   G�  �   ��  D   �  I   R�  =   ��  6   ڋ  ;   �  )   M�  L   w�  N   Č  K   �  L   _�  ?   ��  �   �  .   {�  �   ��  &   9�  *   `�  F   ��  #   ҏ  1   ��     (�  _   D�  '   ��  C   ̐  -   �  -   >�  &   l�  -   ��  P   ��  ?   �  i   R�  <   ��  0   ��  (   *�     S�     s�  7   ��  "   ��  3   ޓ  -   �  &   @�  V   g�  *   ��  ,   �  W   �  /   n�  &   ��  /   ŕ  .   ��  ,   $�  1   Q�  o   ��  
   �     ��     �     �     �     1�     8�     >�     B�     _�     t�     y�     �  F   ��     ؗ     �     ��     �     $�     *�     B�     _�     p�     �     ��     ��     ��     И  M   ֘      $�  Q  E�  �  ��  4   P�     ��     ��  '   ��     �  #   �     �  $   �  +   >�     j�     ��  m  ��  N   �  ^   _�  J   ��  2   	�  =   <�  4   z�  :   ��  Y   �  $   D�  E   i�  �   ��  X   j�  X   ä  �   �  +   ǥ  O   �  2  C�  K   v�    §  O   ܨ  ;   ,�  I   h�  w   ��  M   *�  @   x�  8   ��  =   �  L   0�  �  }�  C   
�  .  N�  �   }�  ,   �  ~  .�  �   ��  1   Y�  (   ��  r  ��  h   '�  �  ��  /   x�  �   ��  �   )�  T  ��  �  �  �  ʺ    n�  R  �     ҿ  �  ��     q�  2   ��     ��     ��  -   ��     #�  %   A�  -   g�  .   ��      ��  &   ��  $   �      1�     R�  	   g�     q�  2   ��     ��  t  ��     7�     S�     k�     }�     ��  K   ��  /   ��  r   �  F   ��  .   ��  4   �  W   6�  R   ��  y   ��  6   [�  c   ��  %   ��     �  P   8�  7   ��  0   ��     ��  "   �  	   1�     ;�     P�     h�  /   ��  �   ��     9�     P�     o�  J   �  =   ��  $   �  X   -�  K   ��     ��  #   ��     �  >   "�     a�      m�  !   ��  5   ��  9   ��      �     -�     =�  -   O�  "   }�  F   ��  /   ��  4   �  ,   L�  F   y�  /   ��  +   ��  E   �  ^   b�  0   ��  )   ��  ,   �  0   I�  ?   z�  8   ��     ��     �     0�     N�     V�     b�  (   j�  )   ��     ��  %   ��  L   �  J   P�  A   ��  V   ��  /   4�  :   d�  =   ��  <   ��  B   �  F   ]�  C   ��  Y   ��  1   B�      t�     ��  !   ��  D   ��  *   �  ,   G�  /   t�  +   ��  *   ��     ��  =   �  A   Y�  N   ��  X   ��  V   C�  @   ��  @   ��  6   �  2   S�  A   ��  ,   ��  3   ��  5   )�  8   _�     ��  3   ��  ,   ��  ;   �  m   O�  a   ��  !   �     A�  ;   W�     ��  q   ��  E   �  =   ^�  [   ��  O   ��  5   H�  $   ~�  G   ��  *   ��  *   �  ,   A�  &   n�  %   ��  4   ��  )   ��  0   �  "   K�     n�     ��  X   ��     ��  4   �  G   K�  =   ��  5   ��  }   �     ��     ��  #   ��  *   ��  G   ��  D   :�  <   �  "   ��  )   ��  E   	�  @   O�  :   ��  ,   ��  G   ��  9   @�  1   z�  >   ��     ��     ��      �     �     3�  ;   J�  @   ��  "   ��  %   ��      �     1�    =�  �   U�  D   F�  F   ��  %   ��     ��     �     #�  9   >�  4   x�  0   ��  )   ��  F   �  "   O�  )   r�  "   ��  #   ��  A   ��  /   %�     U�     k�  6   w�     ��     ��  H   ��     �  +   2�     ^�  1   ~�     ��  I   ��  F   �  ?   T�  0   ��  X  ��     �     #�     :�  0   Y�     ��  @   ��     ��     ��  .   �  B   <�  &   �     ��  '   ��  �   ��  �   q�     
�  +   %�  1   Q�  "   ��     ��  +   ��  1   ��  e   �     x�     ��     ��     ��  F   ��  "   �     @�     X�     p�     ��     ��  "   ��     ��     ��  %   �  '   2�  6   Z�     ��     ��     ��     ��     ��     ��  D   �  *   Z�      ��  5   ��  .   ��  6   �  ,   B�  H   o�     ��     ��     ��     ��     ��     �     �  .   =�  o   l�  y   ��  F   V�  +   ��  Z   ��     $�  G   2�  =   z�     ��     ��  )   ��     ��  &   �  $   2�  '   W�     �  �   ��  R   A  A   �  1   �  �    f   � 4   O    �    �    �    �    �    � J   � /   ; 8   k N   � >   � 5   2 G   h /   � <   � P    _   n `   � c   / /   � 8   � =   � <   : *   w    � B   � M   � D   L [   � X   � P   F	 3   �	 Z   �	 R   &
 =   y
 >   �
 Q   �
 O   H A   � @   � �    A   � e   � S   _ D   � C   � 6   < f   s A   � ]    C   z M   � �    6   � �   � %   } 0   � G   � .    *   K    v k   � 3     =   4 2   r 2   � *   � 2    P   6 S   � �   � D   b ?   � (   �        / K   E #   � @   � 0   � /   ' h   W 4   � 8   � Y   . 2   � 5   � >   � G   0 >   x ;   � t   �    h    y    �    �    �    �    �    � &   �    �             B   .    q    �    �    �    � &   � &   � !   	    +    ;    J &   Q &   x    � I   � #   � f          k   q   +  �   k      V          b          �  �   �   �   �               �  U  �  �         o         '           �  �  �  a          �  �   X  �     �  �   �   !   K  �   {   1   �   ]   r  e  �   j      �   �  u  �  �      9   �  �       �          �      �  |  �   �   �  �  �  �     i  �  �               �   �   3   �   4   �   !  P         ^   �    Q   �  �  �       �          �   �       �      �           O  M              �   2       I          5  �  �   �     �   �  �   v      8     A  {    �      ;             7       �  [  �   �  �                 �   Y          ~  	  �  /   '  �   �   h  z   �   c          )   y   �      x   �              �   �  �   �  �      a   �  �   s      m   �  �     T   �  "   9      �         K   /  -       n        E  �   �   �   N  2            L               �   v       �  �  �   �            �     Z   g  ?  �  �          %  �      �           �   �  0  �   �  �  �   C   �       �   �            �  :       %   �      �     �      M  �      �   �      j   �          �      Q  \   �  �  �   �     �  �  �   �     �   �      �  �           �       �  �       &   �   S   $       ^  �               �  �       �   �      �   �   _   �  �       	   �  m  *  �   z      <   6     �   G     �  =   O   &  L      �                 B   J   8   F   g   �   �  W  :   �   �  x        �  �  �   �  �  |   �      ~   U   
  �   f  �   �       �   �  C  +   Y   #   �  �   s       �      ?     �    .   �  w       R          w         �       �   f   �   D   4  [   �  t  �   �   �                ,  �      �   �   X   @  �      �  H      �          d   �  p   �       �   �      �           �   }   *                   �   n      �          >   7  ;  �       �   �      `       �         �   �   q                     �                �   .     �  �   ,   H   "  �  �  �  �       �          t       �           A   I   N               �       �      r   �         �               �          �   �  F  �  �   �  �   �  y  (  (           �  �      �  b   p      �  E   �       �   �       �  �       G  �   V      �  )      1  �   0   S  _        ]         P      �         D  B          5       \  T      e   J  i   �   =  �  u   �       
   h   �  <      W   #  3  Z  �  $  6  �  �  `  �       �       �      �   d        �       �   @   �   �   >  c   -  l      �          �      �  l       �    �       �      �  o       �  }  R    	-V Output version information and exit
 	Average bitrate: %.1f kb/s

 	Elapsed time: %dm %04.1fs
 	Encoding [%2dm%.2ds so far] %c  	Rate:         %.4f
 	[%5.1f%%] [%2dm%.2ds remaining] %c  
 
	File length:  %dm %04.1fs
 

Done encoding file "%s"
 

Done encoding.
 
Audio Device:   %s                       If multiple input files are given, then multiple
                      instances of the previous eight arguments will be used,
                      in the order they are given. If fewer titles are
                      specified than files, OggEnc will print a warning, and
                      reuse the final one for the remaining files. If fewer
                      track numbers are given, the remaining files will be
                      unnumbered. If fewer lyrics are given, the remaining
                      files will not have lyrics added. For the others, the
                      final tag will be reused for all others without warning
                      (so you can specify a date once, for example, and have
                      it used for all the files)

   --audio-buffer n        Use an output audio buffer of 'n' kilobytes
   -@ file, --list file    Read playlist of files and URLs from "file"
   -K n, --end n           End at 'n' seconds (or hh:mm:ss format)
   -R, --raw               Read and write comments in UTF-8
   -R, --remote            Use remote control interface
   -V, --version           Display ogg123 version
   -V, --version           Output version information and exit
   -Z, --random            Play files randomly until interrupted
   -a, --append            Append comments
   -b n, --buffer n        Use an input buffer of 'n' kilobytes
   -c file, --commentfile file
                          When listing, write comments to the specified file.
                          When editing, read comments from the specified file.
   -d dev, --device dev    Use output device "dev". Available devices:
   -e, --escapes           Use \n-style escapes to allow multiline comments.
   -f file, --file file    Set the output filename for a file device
                          previously specified with --device.
   -h, --help              Display this help
   -k n, --skip n          Skip the first 'n' seconds (or hh:mm:ss format)
   -l s, --delay s         Set termination timeout in milliseconds. ogg123
                          will skip to the next song on SIGINT (Ctrl-C),
                          and will terminate if two SIGINTs are received
                          within the specified timeout 's'. (default 500)
   -l, --list              List the comments (default if no options are given)
   -o k:v, --device-option k:v
                          Pass special option 'k' with value 'v' to the
                          device previously specified with --device. See
                          the ogg123 man page for available device options.
   -p n, --prebuffer n     Load n%% of the input buffer before playing
   -q, --quiet             Don't display anything (no title)
   -r, --repeat            Repeat playlist indefinitely
   -t "name=value", --tag "name=value"
                          Specify a comment tag on the commandline
   -v, --verbose           Display progress and other status information
   -w, --write             Write comments, replacing the existing ones
   -x n, --nth n           Play every 'n'th block
   -y n, --ntimes n        Repeat every played block 'n' times
   -z, --shuffle           Shuffle list of files before playing
  --advanced-encode-option option=value
                      Sets an advanced encoder option to the given value.
                      The valid options (and their values) are documented
                      in the man page supplied with this program. They are
                      for advanced users only, and should be used with
                      caution.
  --bits, -b       Bit depth for output (8 and 16 supported)
  --discard-comments   Prevents comments in FLAC and Ogg FLAC files from
                      being copied to the output Ogg Vorbis file.
 --ignorelength       Ignore the datalength in Wave headers. This allows
                      support for files > 4GB and STDIN data streams. 

  --endianness, -e Output endianness for 16-bit output; 0 for
                  little endian (default), 1 for big endian.
  --help,  -h      Produce this help message.
  --managed            Enable the bitrate management engine. This will allow
                      much greater control over the precise bitrate(s) used,
                      but encoding will be much slower. Don't use it unless
                      you have a strong need for detailed control over
                      bitrate, such as for streaming.
  --output, -o     Output to given filename. May only be used
                  if there is only one input file, except in
                  raw mode.
  --quiet, -Q      Quiet mode. No console output.
  --raw, -R        Raw (headerless) output.
  --resample n         Resample input data to sampling rate n (Hz)
 --downmix            Downmix stereo to mono. Only allowed on stereo
                      input.
 -s, --serial         Specify a serial number for the stream. If encoding
                      multiple files, this will be incremented for each
                      stream after the first.
  --sign, -s       Sign for output PCM; 0 for unsigned, 1 for
                  signed (default 1).
  --utf8               Tells oggenc that the command line parameters date, title,
                      album, artist, genre, and comment are already in UTF-8.
                      On Windows, this switch applies to file names too.
 -c, --comment=c      Add the given string as an extra comment. This may be
                      used multiple times. The argument should be in the
                      format "tag=value".
 -d, --date           Date for track (usually date of performance)
  --version, -V    Print out version number.
  -L, --lyrics         Include lyrics from given file (.srt or .lrc format)
 -Y, --lyrics-language  Sets the language for the lyrics
  -N, --tracknum       Track number for this track
 -t, --title          Title for this track
 -l, --album          Name of album
 -a, --artist         Name of artist
 -G, --genre          Genre of track
  -X, --name-remove=s  Remove the specified characters from parameters to the
                      -n format string. Useful to ensure legal filenames.
 -P, --name-replace=s Replace characters removed by --name-remove with the
                      characters specified. If this string is shorter than the
                      --name-remove list or is not specified, the extra
                      characters are just removed.
                      Default settings for the above two arguments are platform
                      specific.
  -b, --bitrate        Choose a nominal bitrate to encode at. Attempt
                      to encode at a bitrate averaging this. Takes an
                      argument in kbps. By default, this produces a VBR
                      encoding, equivalent to using -q or --quality.
                      See the --managed option to use a managed bitrate
                      targetting the selected bitrate.
  -k, --skeleton       Adds an Ogg Skeleton bitstream
 -r, --raw            Raw mode. Input files are read directly as PCM data
 -B, --raw-bits=n     Set bits/sample for raw input; default is 16
 -C, --raw-chan=n     Set number of channels for raw input; default is 2
 -R, --raw-rate=n     Set samples/sec for raw input; default is 44100
 --raw-endianness     1 for bigendian, 0 for little (defaults to 0)
  -m, --min-bitrate    Specify a minimum bitrate (in kbps). Useful for
                      encoding for a fixed-size channel. Using this will
                      automatically enable managed bitrate mode (see
                      --managed).
 -M, --max-bitrate    Specify a maximum bitrate in kbps. Useful for
                      streaming applications. Using this will automatically
                      enable managed bitrate mode (see --managed).
  -q, --quality        Specify quality, between -1 (very low) and 10 (very
                      high), instead of specifying a particular bitrate.
                      This is the normal mode of operation.
                      Fractional qualities (e.g. 2.75) are permitted
                      The default quality level is 3.
  Input Buffer %5.1f%%  Naming:
 -o, --output=fn      Write file to fn (only valid in single-file mode)
 -n, --names=string   Produce filenames as this string, with %%a, %%t, %%l,
                      %%n, %%d replaced by artist, title, album, track number,
                      and date, respectively (see below for specifying these).
                      %%%% gives a literal %%.
  Output Buffer %5.1f%%  by the Xiph.Org Foundation (http://www.xiph.org/)

 %s: illegal option -- %c
 %s: invalid option -- %c
 %s: option `%c%s' doesn't allow an argument
 %s: option `%s' is ambiguous
 %s: option `%s' requires an argument
 %s: option `--%s' doesn't allow an argument
 %s: option `-W %s' doesn't allow an argument
 %s: option `-W %s' is ambiguous
 %s: option requires an argument -- %c
 %s: unrecognized option `%c%s'
 %s: unrecognized option `--%s'
 %sEOS %sPaused %sPrebuf to %.1f%% '%s' is not valid UTF-8, cannot add
 (NULL) (c) 2003-2005 Michael Smith <msmith@xiph.org>

Usage: ogginfo [flags] file1.ogg [file2.ogx ... fileN.ogv]
Flags supported:
	-h Show this help message
	-q Make less verbose. Once will remove detailed informative
	   messages, two will remove warnings
	-v Make more verbose. This may enable more detailed checks
	   for some stream types.
 (min %d kbps, max %d kbps) (min %d kbps, no max) (no min or max) (no min, max %d kbps) (none) --- Cannot open playlist file %s.  Skipped.
 --- Cannot play every 0th chunk!
 --- Cannot play every chunk 0 times.
--- To do a test decode, use the null output driver.
 --- Driver %s specified in configuration file invalid.
 --- Hole in the stream; probably harmless
 --- Prebuffer value invalid. Range is 0-100.
 255 channels should be enough for anyone. (Sorry, but Vorbis doesn't support more)
 === Cannot specify output file without specifying a driver.
 === Could not load default driver and no driver specified in config file. Exiting.
 === Driver %s is not a file output driver.
 === Error "%s" while parsing config option from command line.
=== Option was: %s
 === Incorrect option format: %s.
 === No such device %s.
 === Option conflict: End time is before start time.
 === Parse error: %s on line %d of %s (%s)
 === Vorbis library reported a stream error.
 AIFF/AIFC file reader Aspect ratio undefined
 Author:   %s Available codecs:  Available options:
 Avg bitrate: %5.1f BOS not set on first page of stream
 BUG: Got zero samples from resampler: your file will be truncated. Please report this.
 Bad comment: "%s"
 Bad type in options list Bad value Big endian 24 bit PCM data is not currently supported, aborting.
 Bitrate hints: upper=%ld nominal=%ld lower=%ld window=%ld Bitstream error, continuing
 Can't produce a file starting and ending between sample positions  Can't produce a file starting between sample positions  Cannot open %s.
 Cannot read header Category: %s
 Changed lowpass frequency from %f kHz to %f kHz
 Channels: %d
 Character encoding: %s
 Colourspace unspecified
 Colourspace: Rec. ITU-R BT.470-6 System M (NTSC)
 Colourspace: Rec. ITU-R BT.470-6 Systems B and G (PAL)
 Comment: Comments: %s Copyright Corrupt or missing data, continuing... Corrupt secondary header. Could not find a processor for stream, bailing
 Could not skip %f seconds of audio. Could not skip to %f in audio stream. Couldn't close output file
 Couldn't convert comment to UTF-8, cannot add
 Couldn't create directory "%s": %s
 Couldn't flush output stream
 Couldn't get enough memory for input buffering. Couldn't get enough memory to register new stream serial number. Couldn't initialise resampler
 Couldn't open %s for reading
 Couldn't open %s for writing
 Couldn't parse cutpoint "%s"
 Couldn't un-escape comment, cannot add
 Couldn't write packet to output file
 Cutpoint not found
 Decode options
 Decoding "%s" to "%s"
 Default Description Done. Downmixing stereo to mono
 EOF before end of Vorbis headers. EOF before recognised stream. ERROR - line %u: Syntax error: %s
 ERROR - line %u: end time must not be less than start time: %s
 ERROR: %s requires an output filename to be specified with -f.
 ERROR: An output file cannot be given for %s device.
 ERROR: Can only specify one input file if output filename is specified
 ERROR: Cannot open device %s.
 ERROR: Cannot open file %s for writing.
 ERROR: Cannot open input file "%s": %s
 ERROR: Cannot open output file "%s": %s
 ERROR: Could not allocate memory in malloc_buffer_stats()
 ERROR: Could not allocate memory in malloc_data_source_stats()
 ERROR: Could not allocate memory in malloc_decoder_stats()
 ERROR: Could not create required subdirectories for output filename "%s"
 ERROR: Could not set signal mask. ERROR: Decoding failure.
 ERROR: Device %s failure.
 ERROR: Device not available.
 ERROR: Failed to load %s - can't determine format
 ERROR: Failed to open input as Vorbis
 ERROR: Failed to open input file: %s
 ERROR: Failed to open lyrics file %s (%s)
 ERROR: Failed to open output file: %s
 ERROR: Failed to write Wave header: %s
 ERROR: File %s already exists.
 ERROR: Input file "%s" is not a supported format
 ERROR: Input filename is the same as output filename "%s"
 ERROR: Multiple files specified when using stdin
 ERROR: Multiple input files with specified output filename: suggest using -n
 ERROR: No Ogg data found in file "%s".
Input probably not Ogg.
 ERROR: No input files specified. Use -h for help
 ERROR: No input files specified. Use -h for help.
 ERROR: No lyrics filename to load from
 ERROR: Out of memory in create_playlist_member().
 ERROR: Out of memory in decoder_buffered_metadata_callback().
 ERROR: Out of memory in malloc_action().
 ERROR: Out of memory in new_audio_reopen_arg().
 ERROR: Out of memory in new_status_message_arg().
 ERROR: Out of memory in playlist_to_array().
 ERROR: Out of memory.
 ERROR: This error should never happen (%d).  Panic!
 ERROR: Unable to create input buffer.
 ERROR: Unsupported option value to %s device.
 ERROR: Wav file is unsupported subformat (must be 8,16, or 24 bit PCM
or floating point PCM
 ERROR: Wav file is unsupported type (must be standard PCM
 or type 3 floating point PCM
 ERROR: buffer write failed.
 Editing options
 Enabling bitrate management engine
 Encoded by: %s Encoding %s%s%s to 
         %s%s%s 
at approximate bitrate %d kbps (VBR encoding enabled)
 Encoding %s%s%s to 
         %s%s%s 
at average bitrate %d kbps  Encoding %s%s%s to 
         %s%s%s 
at quality %2.2f
 Encoding %s%s%s to 
         %s%s%s 
at quality level %2.2f using constrained VBR  Encoding %s%s%s to 
         %s%s%s 
using bitrate management  Error checking for existence of directory %s: %s
 Error in header: not vorbis?
 Error opening %s using the %s module.  The file may be corrupted.
 Error opening comment file '%s'
 Error opening comment file '%s'.
 Error opening input file "%s": %s
 Error opening input file '%s'.
 Error opening output file '%s'.
 Error reading first page of Ogg bitstream. Error reading initial header packet. Error removing erroneous temporary file %s
 Error removing old file %s
 Error renaming %s to %s
 Error unknown. Error writing stream to output. Output stream may be corrupted or truncated. Error writing to file: %s
 Error: Could not create audio buffer.
 Error: Out of memory in decoder_buffered_metadata_callback().
 Error: Out of memory in new_print_statistics_arg().
 Error: path segment "%s" is not a directory
 Examples:
  vorbiscomment -a in.ogg -c comments.txt
  vorbiscomment -a in.ogg -t "ARTIST=Some Guy" -t "TITLE=A Title"
 FLAC file reader FLAC,  Failed encoding Kate EOS packet
 Failed encoding Kate header
 Failed encoding karaoke motion - continuing anyway
 Failed encoding karaoke style - continuing anyway
 Failed encoding lyrics - continuing anyway
 Failed to convert to UTF-8: %s
 Failed to open file as Vorbis: %s
 Failed to set advanced rate management parameters
 Failed to set bitrate min/max in quality mode
 Failed to write comments to output file: %s
 Failed writing data to output stream
 Failed writing fisbone header packet to output stream
 Failed writing fishead packet to output stream
 Failed writing header to output stream
 Failed writing skeleton eos packet to output stream
 File: File: %s Frame aspect %f:1
 Frame aspect 16:9
 Frame aspect 4:3
 Frame offset/size invalid: height incorrect
 Frame offset/size invalid: width incorrect
 Framerate %d/%d (%.02f fps)
 Granulepos rate %d/%d (%.02f gps)
 Header packet corrupt
 Height: %d
 INPUT FILES:
 OggEnc input files must currently be 24, 16, or 8 bit PCM Wave, AIFF, or AIFF/C
 files, 32 bit IEEE floating point Wave, and optionally FLAC or Ogg FLAC. Files
  may be mono or stereo (or more channels) and any sample rate.
 Alternatively, the --raw option may be used to use a raw PCM data file, which
 must be 16 bit stereo little-endian PCM ('headerless Wave'), unless additional
 parameters for raw mode are specified.
 You can specify taking the file from stdin by using - as the input filename.
 In this mode, output is to stdout unless an output filename is specified
 with -o
 Lyrics files may be in SubRip (.srt) or LRC (.lrc) format

 If no output file is specified, vorbiscomment will modify the input file. This
is handled via temporary file, such that the input file is not modified if any
errors are encountered during processing.
 Input buffer size smaller than minimum size of %dkB. Input filename may not be the same as output filename
 Input is not an Ogg bitstream. Input not ogg.
 Input options
 Input truncated or empty. Internal error parsing command line options
 Internal error parsing command line options.
 Internal error parsing command options
 Internal error: Unrecognised argument
 Internal error: attempt to read unsupported bitdepth %d
 Internal stream parsing error
 Invalid zero framerate
 Invalid zero granulepos rate
 Invalid/corrupted comments Kate headers parsed for stream %d, information follows...
 Kate stream %d:
	Total data length: % Key not found Language: %s
 List or edit comments in Ogg Vorbis files.
 Listing options
 Live: Logical bitstreams with changing parameters are not supported
 Logical stream %d ended
 Lower bitrate not set
 Lower bitrate: %f kb/s
 Memory allocation error in stats_init()
 Miscellaneous options
 Mode initialisation failed: invalid parameters for bitrate
 Mode initialisation failed: invalid parameters for quality
 Mode number %d does not (any longer) exist in this version Multiplexed bitstreams are not supported
 NOTE: Raw mode (--raw, -R) will read and write comments in UTF-8 rather than
converting to the user's character set, which is useful in scripts. However,
this is not sufficient for general round-tripping of comments in all cases,
since comments can contain newlines. To handle that, use escaping (-e,
--escape).
 Name Negative granulepos (% Negative or zero granulepos (% New logical stream (#%d, serial: %08x): type %s
 No category set
 No input files specified. "ogginfo -h" for help
 No key No language set
 No module could be found to read from %s.
 No value for advanced encoder option found
 Nominal bitrate not set
 Nominal bitrate: %f kb/s
 Nominal quality setting (0-63): %d
 Note: Stream %d has serial number %d, which is legal but may cause problems with some tools.
 OPTIONS:
 General:
 -Q, --quiet          Produce no output to stderr
 -h, --help           Print this help text
 -V, --version        Print the version number
 Ogg FLAC file reader Ogg Speex stream: %d channel, %d Hz, %s mode Ogg Speex stream: %d channel, %d Hz, %s mode (VBR) Ogg Vorbis stream: %d channel, %ld Hz Ogg Vorbis.

 Ogg bitstream does not contain Vorbis data. Ogg bitstream does not contain a supported data-type. Ogg muxing constraints violated, new stream before EOS of all previous streams Opening with %s module: %s
 Out of memory
 Output options
 Page error, continuing
 Page found for stream after EOS flag Pixel aspect ratio %d:%d (%f:1)
 Pixel format 4:2:0
 Pixel format 4:2:2
 Pixel format 4:4:4
 Pixel format invalid
 Playing: %s Playlist options
 Processing failed
 Processing file "%s"...

 Processing: Cutting at %lf seconds
 Processing: Cutting at %lld samples
 Quality option "%s" not recognised, ignoring
 RAW file reader Rate: %ld

 ReplayGain (Album): ReplayGain (Track): ReplayGain Peak (Album): ReplayGain Peak (Track): Requesting a minimum or maximum bitrate requires --managed
 Resampling input from %d Hz to %d Hz
 Scaling input to %f
 Set optional hard quality restrictions
 Setting advanced encoder option "%s"
 Setting advanced encoder option "%s" to %s
 Skipping chunk of type "%s", length %d
 Specify "." as the second output file to suppress this error.
 Speex version: %s Speex,  Success Supported options:
 System error Target bitrate: %d kbps
 Text directionality: %s
 The file format of %s is not supported.
 The file was encoded with a newer version of Speex.
 You need to upgrade in order to play it.
 The file was encoded with an older version of Speex.
You would need to downgrade the version in order to play it. Theora headers parsed for stream %d, information follows...
 Theora stream %d:
	Total data length: % This version of libvorbisenc cannot set advanced rate management parameters
 Time: %s To avoid creating an output file, specify "." as its name.
 Total image: %d by %d, crop offset (%d, %d)
 Track number: Type Unknown character encoding
 Unknown error Unknown text directionality
 Unrecognised advanced option "%s"
 Upper bitrate not set
 Upper bitrate: %f kb/s
 Usage: 
  vorbiscomment [-Vh]
  vorbiscomment [-lRe] inputfile
  vorbiscomment <-a|-w> [-Re] [-c file] [-t tag] inputfile [outputfile]
 Usage: ogg123 [options] file ...
Play Ogg audio files and network streams.

 Usage: oggdec [options] file1.ogg [file2.ogg ... fileN.ogg]

 Usage: oggenc [options] inputfile [...]

 Usage: ogginfo [flags] file1.ogg [file2.ogx ... fileN.ogv]

ogginfo is a tool for printing information about Ogg files
and for diagnosing problems with them.
Full help shown with "ogginfo -h".
 Usage: vcut infile.ogg outfile1.ogg outfile2.ogg [cutpoint | +cuttime]
 User comments section follows...
 Vendor: %s
 Vendor: %s (%s)
 Version: %d
 Version: %d.%d
 Version: %d.%d.%d
 Vorbis format: Version %d Vorbis headers parsed for stream %d, information follows...
 Vorbis stream %d:
	Total data length: % WARNING - line %d: failed to get UTF-8 glyph from string
 WARNING - line %d: failed to process enhanced LRC tag (%*.*s) - ignored
 WARNING - line %d: lyrics times must not be decreasing
 WARNING - line %u: missing data - truncated file?
 WARNING - line %u: non consecutive ids: %s - pretending not to have noticed
 WARNING - line %u: text is too long - truncated
 WARNING: Can't downmix except from stereo to mono
 WARNING: Comment %d in stream %d has invalid format, does not contain '=': "%s"
 WARNING: Could not decode Kate header packet %d - invalid Kate stream (%d)
 WARNING: Could not decode Theora header packet - invalid Theora stream (%d)
 WARNING: Could not decode Vorbis header packet %d - invalid Vorbis stream (%d)
 WARNING: Could not read directory %s.
 WARNING: Couldn't parse scaling factor "%s"
 WARNING: Couldn't read endianness argument "%s"
 WARNING: Couldn't read resampling frequency "%s"
 WARNING: EOS not set on stream %d
 WARNING: Expected frame % WARNING: Failure in UTF-8 decoder. This should not be possible
 WARNING: Hole in data (%d bytes) found at approximate offset % WARNING: Ignoring illegal escape character '%c' in name format
 WARNING: Illegal UTF-8 sequence in comment %d (stream %d): invalid sequence "%s": %s
 WARNING: Illegal UTF-8 sequence in comment %d (stream %d): length marker wrong
 WARNING: Illegal UTF-8 sequence in comment %d (stream %d): too few bytes
 WARNING: Illegal comment used ("%s"), ignoring.
 WARNING: Insufficient lyrics languages specified, defaulting to final lyrics language.
 WARNING: Insufficient titles specified, defaulting to final title.
 WARNING: Invalid bits/sample specified, assuming 16.
 WARNING: Invalid channel count specified, assuming 2.
 WARNING: Invalid comment fieldname in comment %d (stream %d): "%s"
 WARNING: Invalid header page in stream %d, contains multiple packets
 WARNING: Invalid header page, no packet found
 WARNING: Invalid sample rate specified, assuming 44100.
 WARNING: Kate stream %d does not have headers correctly framed. Terminal header page contains additional packets or has non-zero granulepos
 WARNING: Kate support not compiled in; lyrics will not be included.
 WARNING: Multiple name format filter replacements specified, using final
 WARNING: Multiple name format filters specified, using final
 WARNING: Multiple name formats specified, using final
 WARNING: Multiple output files specified, suggest using -n
 WARNING: No filename, defaulting to "%s"
 WARNING: Raw bits/sample specified for non-raw data. Assuming input is raw.
 WARNING: Raw channel count specified for non-raw data. Assuming input is raw.
 WARNING: Raw endianness specified for non-raw data. Assuming input is raw.
 WARNING: Raw sample rate specified for non-raw data. Assuming input is raw.
 WARNING: Resample rate specified as %d Hz. Did you mean %d Hz?
 WARNING: Theora stream %d does not have headers correctly framed. Terminal header page contains additional packets or has non-zero granulepos
 WARNING: Unknown option specified, ignoring->
 WARNING: Vorbis stream %d does not have headers correctly framed. Terminal header page contains additional packets or has non-zero granulepos
 WARNING: discontinuity in stream (%d)
 WARNING: failed to add Kate karaoke style
 WARNING: failed to allocate memory - enhanced LRC tag will be ignored
 WARNING: found EOS before cutpoint
 WARNING: granulepos in stream %d decreases from % WARNING: hole in data (%d)
 WARNING: illegally placed page(s) for logical stream %d
This indicates a corrupt Ogg file: %s.
 WARNING: input file ended unexpectedly
 WARNING: language can not be longer than 15 characters; truncated.
 WARNING: maximum bitrate "%s" not recognised
 WARNING: minimum bitrate "%s" not recognised
 WARNING: no language specified for %s
 WARNING: nominal bitrate "%s" not recognised
 WARNING: packet %d does not seem to be a Kate header - invalid Kate stream (%d)
 WARNING: quality setting too high, setting to maximum quality.
 WARNING: sequence number gap in stream %d. Got page %ld when expecting page %ld. Indicates missing data.
 WARNING: stream start flag found in mid-stream on stream %d
 WARNING: stream start flag not set on stream %d
 WARNING: subtitle %s is not valid UTF-8
 WARNING: unexpected granulepos  WAV file reader Warning from playlist %s: Could not read directory %s.
 Warning: AIFF-C header truncated.
 Warning: Can't handle compressed AIFF-C (%c%c%c%c)
 Warning: Corrupted SSND chunk in AIFF header
 Warning: Could not read directory %s.
 Warning: INVALID format chunk in wav header.
 Trying to read anyway (may not work)...
 Warning: No SSND chunk found in AIFF file
 Warning: No common chunk found in AIFF file
 Warning: OggEnc does not support this type of AIFF/AIFC file
 Must be 8 or 16 bit PCM.
 Warning: Truncated common chunk in AIFF header
 Warning: Unexpected EOF in AIFF chunk
 Warning: Unexpected EOF in reading AIFF header
 Warning: Unexpected EOF in reading WAV header
 Warning: Unexpected EOF reading AIFF header
 Warning: Unrecognised format chunk in WAV header
 Warning: WAV 'block alignment' value is incorrect, ignoring.
The software that created this file is incorrect.
 Width: %d
 bad comment: "%s"
 bool char default output device double float int left to right, top to bottom no action specified
 none of %s ogg123 from %s %s ogg123 from %s %s
 by the Xiph.Org Foundation (http://www.xiph.org/)

 oggdec from %s %s
 oggenc from %s %s oggenc from %s %s
 ogginfo from %s %s
 other repeat playlist forever right to left, top to bottom shuffle playlist standard input standard output string top to bottom, left to right top to bottom, right to left utf-8 vorbiscomment from %s %s
 by the Xiph.Org Foundation (http://www.xiph.org/)

 vorbiscomment from vorbis-tools  vorbiscomment handles comments in the format "name=value", one per line. By
default, comments are written to stdout when listing, and read from stdin when
editing. Alternatively, a file can be specified with the -c option, or tags
can be given on the commandline with -t "name=value". Use of either -c or -t
disables reading from stdin.
 Project-Id-Version: vorbis-tools
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-03-26 03:08-0400
PO-Revision-Date: 2012-03-03 03:39+0000
Last-Translator: Renato Krupa <renatokrupa@gmail.com>
Language-Team: Portuguese (Brazil) <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:21+0000
X-Generator: Launchpad (build 18115)
 	-V Informações sobre a versão de saída e sair.
 	Taxa média: %.1f kb/s

 	Tempo decorrido: %dm %04.1fs
 	Codificando [%2dm%.2ds até agora] %c  	Taxa: %.4f
 	[%5.1f%%] [%2dm%.2ds restante] %c  
 
	Duração do arquivo: %dm %04.1fs
 

Codificação concluída no arquivo "%s"
 

Codificação concluída.
 
Dispositivo de Áudio: %s                       Se vários arquivos de entrada são dadas, em seguida, várias
                      instâncias dos últimos oito argumentos serão utilizadas,
                      na ordem em que são dadas. Se os títulos são menos
                      específicos que os arquivos, OggEnc irá imprimir um aviso, e
                      reutilizar o final para os arquivos restantes. Se menos
                      números de faixas são dadas, os arquivos restantes serão
                      sem número. Se menos letras são dadas, o restante dos
                      arquivos não terão letras adicionadas. Para os outros, a 
                      tag final será reutilizado para todos os outros sem avisar
                      (assim você pode especificar uma data, uma vez, por exemplo, e tê-lo 
                      usado para todos os arquivos)

   --audio-buffer n        Usa um buffer de saída de áudio de 'n' quilobytes
   -@ arquivo, --list arquivo    Lê a lista de reprodução dos arquivos e URLs de  "arquivo"
   -K n, --end n           Finaliza aos 'n' segundos (ou formato hh:mm:ss)
   -R, --raw  Ler e escrever comentários em UTF-8
   -R, --remote            Usa a interface do controle remoto
   -V, --version           Exibe a versão do ogg123
   -V, --version  Informação da versão de saída e sair
   -Z, --random            Reproduz os arquivos aleatoriamente até que seja interrompido
   -a, --append  Anexar comentários
   -b n, --buffer n        Usa um buffer de entrada de 'n' quilobytes
   -c file, --commentfile file
                          Ao listar, escreva comentários para especificar o arquivo.
                          Ao editar, leia os comentários do arquivo.
   -d dev, --device dev    Usa o dispositivo de saída "dev". Dispositivos disponíveis:
   -e, --escapes Use \n-style para escapar para permitir comentários de várias linhas.
   -f arquivo, --file arquivo    Define o nome de saída do arquivo para um dispositivo de arquivo
                          especificado anteriormente usando o --device.
   -h, --help              Exibe esta ajuda
   -k n, --skip n          Pula os primeiros 'n' segundos (ou formato hh:mm:ss)
   -l s, --delay s         Ajusta o tempo de fim em milissegundos. O ogg123
                          saltará para a próxima canção em SIGINT (Ctrl-C),
                          e finalizará caso dois SIGINTs sejam recebidos
                          com o tempo limite 's' especificado. (padrão 500)
   -l, --list Listar os comentários (padrão se não forem dadas opções)
   -o k:v, --device-option k:v
                          Passa a opção especial 'k' com valor 'v' ao 
                          dispositivo previamente especificado com o --device. Veja
                          a página man do ogg123 para opções de dispositivos disponíveis.
   -p n, --prebuffer n     carrega n%% do buffer de entrada antes da execução
   -q, --quiet             Não exibe nada (nenhum título)
   -r, --repeat            Repete a lista de reprodução indefinidamente
   -t "nome=valor", --tag "nome=valor"
                          Especifique uma tag de comentário na linha de comando
   -v, --verbose           Exibe o progresso e outras informações de status
   -w, --write Escrever comentários, substituindo os existentes
   -x n, --nth n           Reproduz cada 'n'ésimo bloco
   -y n, --ntimes n        Repete cada bloco tocado 'n' vezes
   -z, --shuffle           Embaralha a lista de arquivos antes de reproduzir
  --advanced-encode-option opção=valor
                      Define uma opção avançada do codificador para o valor dado.
                      As opções válidas (e seus valores) estão documentadas
                      na página do manual fornecido com este programa. Eles são
                      somente para usuários avançados, e deve ser usado com
                      cuidado.
  --bits, -b Profundidade de bits para a saída (8 e 16 suportados)
  --discard-comments Evita que comentários nos arquivos FLAC e Ogg FLAC
                      sejam copiados para a saída do arquivo Ogg Vorbis.
 --ignorelenght Ignora o comprimento dos dados no cabeçalho Wave. Isso permite
                      suporte para arquivos > 4GB e fluxo de dados STDIN. 

  --endianness,-e Saída endianess para saída de 16-bit; 0 para
                   pequeno endian (padrão), 1 para grande endian.
  --help,-h Produzir esta mensagem de ajuda.
  --managed Ativa o mecanismo de gerenciamento de taxa de bits. Isso irá permitir
                      controle muito maior sobre a taxa de bits utilizada,
                      mas a codificação será muito mais lenta. Não use-o, a menos
                      que você tem uma forte necessidade de controle detalhado sobre
                      taxa de bits, como, streaming.
  --output, -o Saída para nome de arquivo dado. Só podem ser utilizados
                   se houver apenas um arquivo de entrada, exceto em
                   modo raw.
  --quiet,-Q Modo silencioso. Sem saída console.
  ---raw,-R Saída raw (sem cabeçalho).
  --resample n Reamostragem de dados de entrada para taxa de amostragem n (Hz)
 --downmix Downmix estéreo para mono. Permitido somente em entrada
                      estéreo.
 -s, --serial Define um número de série para o fluxo. Se codificar
                      múltiplos arquivos, este será incrementado para cada
                      fluxo após a primeira.
  --sign,-s Sinal para saída PCM; 0 para não assinado, 1 para
                   assinado (padrão 1).
  --utf8 Diz ao oggenc que a linha de parâmetros data, título,
                      álbum, artista, gênero, e comentários já estão em UTF-8.
                      No Windows, essa opção se aplica a nomes de arquivo também.
 -c, --comment=c Adiciona a string dada como um comentário extra. Isso pode ser
                      usado muitas vezes. O argumento deve estar no
                      formato "tag=valor".
 -d, --date Data para a faixa (geralmente data de desempenho)
  --version, -V    Mostra o número da versão.
  -L, --lyrics Inclui letras de determinado arquivo (formato .srt ou .lrc)
 -Y, --lyrics-language Define o idioma para as letras
  -N, --tracknum Número de faixa
 -t, --title Título da faixa
 -l, --album Nome do álbum
 -a, --artist Nome do artista
 -G, --genre Gênero da faixa
  -X, --name-remove=s Remove os caracteres especificados a partir de parâmetros para o
                      formato de string -n. Útil para assegurar nomes legais.
 -P, --name-replace=s Substitui caracteres removidos por --name-remove com os
                      caracteres especificados. Se essa seqüência é mais curta que a
                      lista --name-remove ou não está especificada, os caracteres
                      extras serão removidos.
                      As configurações padrão para os dois argumentos acima são de plataforma
                      específica.
  -b, --bitrate Escolha uma taxa de bits nominal para codificar. Tentativa
                      para codificar a uma taxa de bits média. Recebe um
                      argumento em kbps. Por padrão, isto produz uma
                      codificação VBR, equivalente a usar -q ou --quality.
                      Ver opção --managed para usar gerenciador de taxa de bits
                      visando a taxa de bits selecionada.
  -k, --skeleton Adiciona um fluxo de bits Ogg Skeleton
 -r, --raw Modo bruto. Arquivos de entrada são lidos diretamente como dados PCM
 -B, --raw-bits=n Define bits/amostra para entrada bruta; padrão é 16
 -C, --raw-chan=n Define número de canais para entrada bruta; padrão é 2
 -R, --raw-rate=n Define amostras/seg para entrada bruta; padrão é 44100
 --raw-endianness 1 para muito, 0 para pouco (padrão é 0)
  -m, --min-bitrate Especifica uma taxa de bits mínima (em kbps). Útil para
                      codificação de um canal de tamanho fixo. Usando isso irá
                      ativar automaticamente o modo de gerenciamento de taxa de bits (ver
                      --managed).
 -M, --max-bitrate Especifica uma taxa de bits máxima (em kbps). Útil para
                      aplicações de streaming. Usando isso irá ativar
                      automaticamente o modo de gerenciamento de taxa de bits (ver --managed).
  -q, --quality Define a qualidade, entre -1 (muito baixa) e 10 (muito
                      alta), em vez de especificar uma taxa de bits especial.
                      Esse é o modo normal de operação.
                      Qualidades fracionárias (ex 2.75) são permitidas
                      O nível de qualidade padrão é 3.
  Buffer de entrada %5.1f%%  Nomeando:
 -o, --output=fn Gravar arquivo para fn (válido somente no modo arquivo único)
 -n, --names=string Produz nomes de arquivo com a string, %%a, %%t, %%l,
                      %%n, %%d substituído por artista, título, álbum, número da faixa,
                      e data, respectivamente (ver abaixo para especificar estes).
                      %%%% dá uma literal %%.
  Buffer de saída %5.1f%%  pela Fundação Xiph.Org (http://www.xiph.org/)

 %s: opção ilegal -- %c
 %s: opção inválida -- %c
 %s: opção `%c%s' não permite um argumento
 %s: opção `%s' é ambígua
 %s: opção `%s' requer um argumento
 %s: opção `--%s' não permite um argumento
 %s: opção `-W %s' não permite um argumento
 %s: opção `-W %s' é ambígua
 %s: opção requer um argumento -- %c
 %s: opção não reconhecida `%c%s'
 %s: opção desconhecida `--%s'
 %sEOS (fim do fluxo) %sPausado %sPré-buf para %.1f%% '%s' não é UTF-8 válido, impossível adicionar
 (NULL) (c) 2003-2005 Michael Smith <msmith@xiph.org>

Uso: ogginfo [indicadores] arquivo1.ogg [arquivo2.ogx ... arquivoN.ogv]
Indicadores suportados:
	-h Exibe essa mensagem de ajuda
	-q Menos detalhes. Irá remover informações detalhadas
	 de mensagens, irá remover dois avisos
	-v Mais detalhes. Isso pode permitir verificação mais detalhada
	 para alguns tipos de fluxo.
 (min %d kbps, máx %d kbps) (min %d kbps, sem máx) (sem min ou máx) (sem min, máx %d kbps) (none) --- Não foi possível abrir arquivo da lista de reprodução %s. Pulando.
 --- Não é possível reproduzir cada bloco 0.
 --- Não é possível reproduzir cada bloco 0 vezes.
--- Para testar decodificação use o driver de saída nula.
 --- Driver %s especificado no arquivo de configuração é inválido.
 --- Buraco no fluxo; provavelmente inofensivo
 --- Valor de pré-buffer inválido. Escala é 0-100
 255 canais devem ser suficiente para todos. (Desculpe, mas o Vorbis não suporta mais)
 === Não é possível especificar um arquivo de saída sem especificar um driver.
 === Não foi possível carregar o driver padrão e nenhum outro está especificado no arquivo de configuração. Saindo.
 === Driver %s não é um driver de saída de arquivo.
 === Erro "%s" quando analisando opção de configuração da linha de comando.
=== Opção foi: %s
 === Formato de opção incorreto: %s
 === Nenhum dispositivo %s.
 === Conflito de opções: Tempo de fim está situado antes do tempo de início.
 === Erro de interpretação: %s na linha %d de %s (%s)
 === Biblioteca vorbis relatou um erro de fluxo.
 Leitor de arquivo AIFF/AIFC Aspecto de proporção indefinida
 Autor: %s Codecs disponíveis  Opções disponíveis:
 Taxa de bits média: %5.1f BOS não definido na primeira página do fluxo
 ERRO: Foram obtidas zero amostras através do re-amostrador: seu arquivo ficará truncado. Por favor, avise-nos sobre este problema.
 Mal comentário: "%s"
 Tipo ruim em lista de opções Valor incorreto Dados de PCM de 24 bit big endian não é atualmente suportado; abortando
 Taxa de bit: superior=%ld nominal=%ld inferior=%ld janela=%ld Erro no fluxo de bits; prosseguindo
 Não pode produzir um arquivo começando e terminando entre as posições de amostragem  Não pode produzir um arquivo começando entre as posições de amostragem  Não é possível abrir %s.
 Não é possível ler o cabeçalho. Categoria: %s
 Modificou frequência de passagem baixa de %f kHz para %f kHz
 Canais: %d
 Codificação de caracteres: %s
 Espaço de cor não especificado
 Espaço de cor: Rec. ITU-R BT.470-6 Sistema M (NTSC)
 Espaço de cor: Rec. ITU-R BT.470-6 Sistemas B e G (PAL)
 Comentário: Comentário: %s Direitos autorais Dados corrompidos ou faltando, continuando... Cabeçalho secundário corrompido. Não foi possível encontrar um processador para o fluxo; abandonando
 Não foi possível pular %f segundos de áudio. Não foi possível pular para %f no fluxo de áudio. Não foi possível fechar arquivo de saída
 Não foi possível converter para UTF-8; não foi possível adicionar
 Não foi possível criar o diretório "%s": %s
 Não foi possível liberar fluxo de saída
 Não foi possível obter memória suficiente para o buffer de entrada Não foi possível obter memória suficiente para registrar o novo número de série do fluxo. Não foi possível inicializar o re-amostrador.
 Não foi possível abrir %s para leitura
 Não foi possível abrir %s para gravação
 Não foi possível analisar ponto de corte "%s"
 Não foi possível cancelar o comentário, não pode adicionar
 Não foi possível gravar pacote para arquivo de saída
 Ponto de corte não encontrado
 Opções de decodificação
 Decodificando "%s" para "%s"
 Padrão Descrição Pronto. Reduzindo a mixagem de stereo para mono
 EOF antes do final de cabeçalhos Vorbis. EOF antes de fluxo reconhecido. ERRO - linha %u: Erro de sintaxe: %s
 ERRO - linha %u: tempo final não deve ser inferior ao tempo de início: %s
 ERRO: %s requer o nome de um arquivo de saída a ser especificado com -f.
 ERRO: Um arquivo de saída não pode ser dada por%s dispositivo.
 ERRO: Só pode especificar um arquivo de entrada se arquivo de saída é especificado
 ERRO: Não foi possível abrir dispositivo %s.
 ERRO: Não foi possível abrir o arquivo %s para escrita.
 ERRO: Não foi possível abrir o arquivo de entrada "%s": %s
 ERRO: Não foi possível abrir o arquivo de saída "%s": %s
 ERRO: Não foi possível alocar memória em malloc_buffer_stats()
 ERRO: Não é possível alocar memória em malloc_data_source_stats()
 ERRO: Não foi possível alocar memória em malloc_decoder_stats()
 ERRO: Não foi possível criar sub-diretórios solicitados para o arquivo de saída "%s"
 ERRO: Não foi possível setar dinal de máscara. ERRO: Falha na decodificação.
 ERRO: Falha no dispositivo %s.
 ERRO: Dispositivo indisponível.
 ERRO: Falha ao carregar %s - não é possível determinar o formato
 ERROR: Falha ao abrir entrada como Vorbis
 ERRO: Falha ao abrir arquivo de entrada: %s
 ERRO: Falha ao abrir arquivo de letras %s (%s)
 ERRO: Falha ao abrir arquivo de saída: %s
 ERRO: Falha ao gravar cabeçalho Wave: %s
 ERRO: O arquivo %s já existe.
 ERRO: O arquivo de entrada "%s" não é um formato suportado
 ERRO: O arquivo de entrada e de saída possuem o mesmo nome "%s"
 ERRO: Múltiplos arquivos especificados quando usando stdin (entrada padrão)
 ERRO: Múltiplos arquivos de entrada com o nome de arquivo especificado: sugiro usar -n
 ERRO: Nenhum dado Ogg encontrado no arquivo "%s".
Entrada provávelmente não é Ogg.
 ERRO: Nenhum arquivo de entrada especificado. Use -h para ajuda
 ERRO: Sem arquivos de entrada especificados. Use -h para ajuda.
 ERRO: Sem letras com nome do arquivo para carregar de
 ERROR: Out of memory in create_playlist_member().
 ERRO: Falta de memória em decoder_buffered_metadata_callback().
 Erro: Falta de memória em malloc_action().
 ERRO: falta de memória em new_audio_reopen_arg().
 ERRO: Falta de memória em new_status_message_arg().
 ERRO: Fora da memória na lista de playlist_to_array().
 ERRO: Falta de memória.
 ERRO: Este erro não deve acontecer (%d). Pânico!
 ERROR: Impossível criar buffer de entrada.
 ERRO: Valor da opção não suportado para dispositivo %s.
 ERRO: Arquivo wav não é um sub-formato suportado (deve ser 8, 16, ou 24 bit PCM)
ou PCM de ponto flutuante
 Erro: Arquivo wav não é um tipo suportado (deve ser um PCM
 padrão ou PCM de tipo flutuante 3
 ERRO: falha ao gravar no buffer.
 Opções de edição
 Habilitando sistema de administração para a taxa de bits
 Codificado por: %s Codificando %s%s%s para 
         %s%s%s 
aprocimadamente na taxa de bits %d kbps (codificação VBR habilitada)
 Codificando %s%s%s para 
         %s%s%s 
at average bitrate %d kbps  Codificando %s%s%s para 
         %s%s%s 
na qualidade %2.2f
 Codificando %s%s%s para 
         %s%s%s
no nível de qualidade %2.2f usando VBR confinado  Codificando %s%s%s para 
         %s%s%s 
usando gerenciamento de taxa de bits  Erro ao checar pela existência do diretório %s: %s
 Erro no cabeçalho: não é vorbis?
 Erro ao abrir %s usando o módulo %s. O arquivo pode estar corrompido.
 Erro ao abrir arquivo de comentário '%s'
 Erro ao abrir arquivo de comentário '%s'
 Erro ao abrir o arquivo de entrada "%s": %s
 Erro ao abrir arquivo de entrada '%s'
 Erro ao abrir arquivo de saída '%s'
 Erro ao ler a primeira página do fluxo de bits Ogg. Erro ao ler pacote de cabeçalho inicial. Erro ao remover arquivo temporário errôneo %s
 Erro ao remover arquivo antigo %s
 Erro ao renomear %s para %s
 Erro desconhecido. Erro ao escrever o fluxo de saída. O fluxo de saída pode estar corrompido ou truncado. Erro ao gravar no arquivo: %s
 Erro: Não foi possível criar um buffer de áudio.
 Erro: Memória insuficiente para decoder_buffered_metadata_callback().
 Erro: Memória insuficiente para new_print_statistics_arg().
 Erro: segmento de caminho "%s" não é um diretório
 Exemplos:
  vorbiscomment -a in.ogg -c comentarios.txt
  vorbiscomment -a in.ogg -t "ARTIST=Algum Cara" -t "TITLE=Um Titulo"
 Leitor de arquivo FLAC FLAC,  Falha ao codificar pacote Kate EOS
 Falha na codificação do cabeçalho Kate
 Falha ao codificar movimento karaoke - continuando de qualquer maneira
 Falha ao codificar estilo karaoke - continuando de qualquer maneira
 Falha ao codificar letras - continuando de qualquer maneira
 Falha ao converter para UTF-8: %s
 Falha ao abrir o arquivo como Vorbis: %s
 Falhou ao configurar gerenciamento de parâmetros de ratio avançado
 Falha na configuração de bitrate min/max no modo de qualidade
 Falha ao gravar comentários para o arquivo de saída: %s
 Falhou ao escrever dados no fluxo de saída
 Falha ao escrever no cabeçalho do pacote fisbone para fluxo de saída
 Falha ao escrever no pacote fishead para fluxo de saída
 Falhou ao escrever cabeçalho no fluxo de saída
 Falha ao escrever no pacote skeleton eos para fluxo de saída
 Arquivo: Arquivo: %s Aspecto da estrutura %f:1
 Aspecto do quadro 16:9
 Aspecto de quadro 4:3
 Deslocamento/tamanho de quadro inválido: altura incorreta
 Deslocamento/tamanho de quadro inválido: comprimento incorreto
 Taxa de quadros %d/%d (%.02f fps)
 Taxa de granulepos %d/%d (%.02f gps)
 Cabeçalho do pacote corrompido
 Altura: %d
 ARQUIVOS DE ENTRADA:
 OggEnc arquivos de entrada devem ser atualmente 24, 16, ou 8 bit PCM Wave, AIFF, ou AIFF/C
 arquivos, 32 bit IEEE ponto flutuante Wave, e opcionalmente FLAC ou Ogg FLAC. Arquivos
  podem ser mono ou estéreo (ou mais canais) e qualquer taxa de amostragem.
 Alternativamente, a opção --raw pode ser usada para utilizar dados brutos de arquivo PCM, que
 deve ser de PCM little-endian 16 bits estéreo ('sem cabeçalho Wave'), adicional a menos
 que parâmetros para o modo bruto são especificados.
 Você pode especificar tomando o arquivo stdin usando - como o nome do arquivo de entrada.
 Neste modo, saída é a saída padrão, a menos que um nome de arquivo de saída é especificado
 com -o
 Arquivos de letras podem estar no formato SubRip (.srt) ou LRC (.lrc)

 Se o arquivo e saída não for especificado, vorbiscomment irá modificar o arquivo de
entrada. Isso é tratado por uma arquivo temporário, de modo que o arquivo de entrada
não é modificado se qualquer erro acontecer durante o processo.
 Tamanho do buffer de entrada é menor que o tamanho mínimo de %dkB. Nome do arquivo de entrada não pode ser o mesmo do arquivo de saída
 Entrada não é um fluxo de bits Ogg. Entrada não é ogg.
 Opções de entrada
 Entrada truncada ou vazia. Erro interno ao analisar as opções de linha de comando
 Erro interno ao analisar opções de linha comando.
 Erro interno ao analisar as opções de comando
 Erro interno: argumento não reconhecido
 Erro interno: tentativa de ler profundidade de bits %d não suportada
 Erro de análise de fluxo interno
 Zero não é uma taxa de quadros válida
 Taxa zero de granulepos inválida
 Comentários inválidos/corrompidos Cabeçalhos Kate analisados pelo fluxo %d, informação segue...
 Kate stream %d:
	Comprimento total dos dados: % Chave não encontrada Idioma: %s
 Listar ou editar comentários em arquivos Ogg Vorbis.
 Opções de listagem
 Ao vivo: Bitstreams lógicos com alteração de parâmetros não são suportados
 Fluxo lógico %d finalizou
 Taxa de bits inferior não foi configurada
 Taxa de bits inferior: %f kb/s
 Erro no reservamento de memória em stats_init()
 Opções diversas
 Modo de inicialização falhou: parâmetros inválidos para taxa de bits
 Modo de inicialização falhou: parâmetros inválidos para qualidade
 Modo número %d não (por mais tempo) não existe nesta versão Taxa de bits multiplexados não são suportadas
 NOTA: Modo bruto (--raw, -R) vai ler e escrever comentários em UTF-8 ao invés de
converter para o conjunto de caracteres do usuário, que é útil em scripts. Contudo,
isso não é suficiente para comentários gerais em todos os casos,
pois os comentários podem conter novas linhas. Para lidar com isso, use a opção escape (-e,
--escape).
 Nome Granulepos negativo (% Negativo ou zero granulepos (% Novo fluxo lógico (#%d, serial: %08x): tipo %s
 Nenhuma categoria definida
 Nenhum arquivo de entrada especificado. "ogginfo -h" para ajuda
 Nenhuma tecla Nenhum idioma definido
 Nenhum módulo foi encontrado para ler de %s.
 Nenhum valor para a opção de codificação avançada encontrado
 Taxa de bits nominal não configurada
 Taxa de bits nominal: %f kb/s
 Ajuste de qualidade nominal (0-63): %d
 Observação: O fluxo %d possui o número serial %d, o que é permitido mas poderá causar algum problema com algumas ferramentas.
 OPÇÕES:
 Geral:
 -Q, --quiet Não produz nenhuma saída para stderr
 -h, --help Imprime esse texto de ajuda
 -V, --version Imprime número de versão
 Leitor de arquivo Ogg FLAC Ogg Speex fluxo: %d canal, %d Hz, %s o modo Ogg Speex fluxo: %d canal, %d Hz, %s o modo (VBR) Fluxo Ogg Vorbis: %d canal, %ld Hz Ogg Vorbis.

 Taxa de bits Ogg não contém dados Vorbis. Fluxo de bits ogg não contém um tipo suportado. Restrições de mixagem de Oggs violados; novo fluxo antes de EOS (fim de fluxo) de todos os prévios Abrindo com o módulo %s: %s
 Sem memória
 Opções de saída
 Erro na página, continuando
 Página encontrada para o fluxo após o sinalizador EOS (fim do fluxo) Proporção do pixel %d:%d (%f:1)
 Formato de pixel 4:2:0
 Formato de pixel 4:2:2
 Formato de pixel 4:4:4
 Formato de pixel inválido
 Reproduzindo: %s Opções da lista de reprodução
 Processamento falhou
 Processando arquivo "%s"...

 Processando: Cortando a %lf segundos
 Processando: Cortando em %lld amostras
 Opção de qualidade "%s" não reconhecida; ignorando
 Leitor de arquivo RAW Taxa: %ld

 Repetir (Álbum): Repetir (Faixa): Repetir ganho de pico (álbum): Repetir ganho de pico (faixa): A solicitação de taxas de bit máxima ou mínima requer --managed
 Re-amostrando entrada de %d Hz para %d Hz
 Redimensionando entrada para %f
 Ajustar restrições opcionais de qualidade estritas
 Definir opção avançada do codificador "%s"
 Mudando opção avançada do codificador "%s" para %s
 Pulando blocos do tipo "%s", comprimento %d
 Especifique "." como segundo arquivo de saída para suprimir este erro.
 Versão do Speex: %s Speex,  Sucesso Opções suportadas:
 Erro de sistema Taxa alvo de bits: %d kbps
 Direcionalidade de texto: %s
 O formato de arquivo de %s não é suportado.
 O arquivo foi encodado com a mais nova versão do Speex.
  Você precisa atualizar em oredem para executá-lo.
 O arquivo foi encodado com uma versão mais antiga do Speex.
Você deve desatualizar a versão em ordem para executá-lo. Cabeçalhos do Theora analisados para fluxo %d, Segue informação...
 Theora fluxo %d:
	Tamanho total de dados: % Esta versão do libvorbisenc não suporta gerenciamento de parâmetros de ratio avançado
 Duração: %s Para evitar a criação de um arquivo de saída, defina "." como nome.
 Imagem total: %d por %d, deslocamento para recortar (%d, %d)
 Número da faixa: Tipo Codificação de caracteres desconhecida
 Erro desconhecido Direcionalidade de texto desconhecida
 Opção avançada "%s" desconhecida
 Taxa de bits superior não configurada
 Taxa de bits superior: %f kb/s
 Uso: 
  vorbiscomment [-Vh]
  vorbiscomment [-lRe] arquivo de entrada
  vorbiscomment <-a|-w> [-Re] [-c arquivo] [-t tag] arquivo de entrada [arquivo de saída]
 Uso: ogg123 [opções] arquivo ...
Toca arquivos de áudio Ogg e fluxos de rede.

 Uso: oggdec [options] arquivo1.ogg [file2.ogg ... arquivoN.ogg]

 Uso: oggenc [opções] arquivo de entrada [...]

 Uso: ogginfo [indicadores] arquivo1.ogg [arquivo2.ogx ... arquivoN.ogv]

ogginfo é uma ferramenta para imprimir informações sobre arquivos Ogg
e para diagnosticar problemas neles.
Exibir ajuda completa com "ogginfo -h".
 Uso: vcut arquivodeentrada.ogg arquivodesaida1.ogg arquivodesaida2.ogg [pontodecorte | +tempodecorte]
 A seção de comentários do usuário em seguida...
 Vendedor: %s
 Vendedor: %s (%s)
 Versão: %d
 Versão: %d.%d
 Versão: %d.%d.%d
 Formato vorbis: Versão %d Cabeçalhos vorbis analisados para o fluxo %d; informação em seguida...
 Fluxo do vorbis %d:
	Tamanho total dos dados: % AVISO - linha %d: falhou ao obter glifo UTF-8 de string
 AVISO - linha %d: falha ao processar rótulo LRC melhorado (%*.*s) - ignorado
 AVISO - linha %d: tempos de letras não devem ser decrescente
 AVISO - linha %u: faltando dados - arquivo truncado?
 AVISO - linha %u: ids não consecutivos: %s - fingindo não ter notado
 AVISO - linha %u: texto muito longo - truncado
 Atenção: Não pode sub-mixar exceto de estéreo para mono
 AVISO: Comentário %d no fluxo %d tem formato inválido, não contém '=': "%s"
 AVISO: Não foi possível decodificar pacote do cabeçalho Kate %d - fluxo Kate inválido (%d)
 AVISO: Não foi possível decodificar pacote de cabeçalho Theora - fluxo Theora inválido (%d)
 AVISO: Não foi possível decodificar pacote de cabeçalho Vorbis %d - fluxo Vorbis inválido (%d)
 AVISO: Não foi possível ler o diretório %s.
 AVISO: Não foi possível analisar fator de escala "%s"
 AVISO: Não foi possível ler fator endian do argumento "%s"
 AVISO: Não foi possível ler a frequência de amostra "%s"
 AVISO: EOS não definido em operação %d
 AVISO: Quadro esperado % AVISO: Falha no decodificador UTF-8. Isto não deve ser possível
 AVISO: Buraco nos dados (%d bytes) encontrado compensado em aproximadamente % AVISO: Ignorando caractere de escape ilegal '%c' no formato de nome
 AVISO: Sequência UTF-8 ilegal no comentário %d (fluxo %d): sequência inválida "%s": %s
 AVISO: Sequência UTF-8 ilegal no comentário %d (fluxo %d): marcador de tamanho errado
 AVISO: Sequência UTF-8 ilegal no comentário %d (fluxo %d): muito poucos bytes
 AVISO: Comentário ilegal usado ("%s"), ignorando.
 AVISO: Idiomas de letras especificados insuficientes, padronizando idioma de letra final.
 AVISO: Títulos especificados insuficientes; padronizando para o último título.
 AVISO: Bits/amostras especificados inválidos; assumindo 16.
 AVISO: Contagem inválida de canal especificado; assumindo 2.
 AVISO: Comentário inválido em nome do campo no comentário %d (fluxo %d): "%s"
 AVISO: Cabeçalho de página inválido no fluxo %d, contém múltiplos pacotes
 AVISO: Cabeçalho de página inválido, nenhum pacote encontrado
 AVISO: Taxa de amostra especificada inválida; assumindo 44100.
 AVISO: Fluxo Kate %d não tem cabeçalhos corretamente enquadrados. Página de cabeçalho Terminal contém pacotes adicionais ou tem não-zeros granulepos
 AVISO: Suporte Kate não compilado; letras não serão inclusas.
 AVISO: Múltiplos nomes de substituição de filtros de formatação especificados; usando o último
 AVISO: Múltiplos nomes de filtros de formatação especificados; usando o último
 AVISO: Múltiplos formatos de nomes especificados; usando o último
 AVISO: Múltiplos arquivos de saída especificados; sugiro usar -n
 AVISO: Nenhum nome de arquivo, padronizando para "%s"
 AVISO: Bits/amostras cru especificados para dados não cru. Assumindo que dados de entrada são crus.
 AVISO: Contagem crua de canal especificado para dados não crus.
 AVISO: Finalização crua especificada para dados não-crus. Considerando entrada como crua.
 AVISO: Taxa de amostra "crua" especificada para dados não "crus".
 AVISO: Taxa de reamostragem especificada como %d Hz. Você quis dizer %d Hz?
 AVISO: Fluxo Theora %d não tem cabeçalhos corretamente enquadrados. Página de cabeçalho terminal contém pacotes adicionais ou tem não-zero granulepos
 AVISO: Opção especificada desconhecida; ignorando->
 AVISO: Fluxo Vorbis %d não tem cabeçalhos corretamente enquadrados. Página de cabeçalho terminal contém pacotes adicionais ou tem não-zero granulepos
 AVISO: descontinuidade no fluxo (%d)
 AVISO: falha ao adicionar o estilo Kate karaoke
 AVISO: falha ao alocar memória - rótulo LRC melhorado será ignorado
 AVISO: encontrado EOS antes do ponto de corte
 AVISO: granulepos no fluxo %d diminui de % AVISO: buraco nos dados (%d)
 AVISO: página(s) ilegalmente colocada(s) para fluxo lógico %d
Isso indica um arquivo Ogg corrompido: %s.
 AVISO: arquivo de entrada terminou inesperadamente
 AVISO: idioma não pode ser maior de 15 caracteres; cortado.
 AVISO: taxa de bits máxima "%s" não reconhecida
 AVISO: taxa de bits mínima "%s" não reconhecida
 AVISO: nenhum idioma especificado para %s
 AVISO: Taxa de bits nominal "%s" não reconhecida
 AVISO: pacote %d parece não ser um cabeçalho Kate - fluxo Kate inválido (%d)
 AVISO: configuração de qualidade muita alta; ajustando para a qualidade máxima.
 AVISO: lacuna na seqüência numérica de fluxo %d. Tem página %ld quando esperava página %ld. Indica que os dados estão faltando.
 AVISO: indicador de início de fluxo encontrado no meio do fluxo %d
 AVISO: indicador de início de fluxo não definido no fluxo %d
 AVISO: legenda %s não é válida UTF-8
 AVISO: granulepos inesperados  Leitor de arquivo WAV Aviso da lista de reprodução %s: Não foi possível ler o diretório %s.
 Aviso: Cabeçalho IAFF-C truncado.
 Aviso: Não é possível lidar com AIFF-C comprimido (%c%c%c%c)
 Aviso: Bloco SSND corrompido em cabeçalho AIFF
 Aviso: Não foi possível ler o diretório %s.
 Aviso: Formato de bloco INVÁLIDO no cabeçalho WAV.
 Tentando ler mesmo assim (pode não funcionar)...
 Aviso: Nenhum bloco SSND encontrado em arquivo AIFF
 Aviso: Nenhum bloco em comum encontrado em arquivo AIFF
 Aviso: OggEnc não suporta este tipo de arquivo AIFF/AIFC
 Deve ser PCM de 8 ou 16 bits.
 Aviso: Bloco em comum truncado no cabeçalho AIFF
 Aviso: EOF (fim de arquivo) inesperado em bloco AIFF
 Aviso: EOF (fim de arquivo) inesperado ao ler cabeçalho AIFF
 Aviso: EOF (fim de arquivo) não esperado na leitura do cabeçalho WAV
 Aviso: EOF (fim de arquivo) inesperado ao ler cabeçalho AIFF
 Aviso: Formato de bloco não reconhecido no cabeçalho WAV
 Aviso: O valor WAV 'block alignment' está incorreto, ignorando.
O software que criou este arquivo está incorreto.
 Comprimento: %d
 mal comentário: "%s"
 booleano char dispositivo de saída padrão double float int esquerda para direita, cima para baixo nenhuma ação especificada
 nenhum de %s ogg123 de %s %s ogg123 de %s %s
 pela Fundação Xiph.Org (http://www.xiph.org/)

 oggdec de %s %s
 oggenc de %s %s oggenc de %s %s
 ogginfo de %s %s
 outro sempre repetir a lista de reprodução direita para esquerda, cima para baixo aleatorizar lista de reprodução entrada padrão saída padrão string cima para baixo, esquerda para direita cima para baixo, direita para esquerda utf-8 vorbiscomment de %s %s
 pela Xiph.Org Foundation (http://www.xiph.org/)

 comentário vorbis de vorbis-tools  vorbiscomment lida com os comentários no formato "nome=valor", um por linha. Por
padrão, comentários são escritos para stdout ao listar, e ao editar lê em stdin.
Como alternativa, um arquivo pode ser especificado com a opção -c, ou as tags
podem ser obtidas pela linha de comando -t "nome=valor". Use tanto -c ou -t
para desativar a leitura de stdin.
 