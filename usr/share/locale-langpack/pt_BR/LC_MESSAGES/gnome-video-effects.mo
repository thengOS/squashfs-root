��    E      D  a   l      �     �  @   	  /   J  &   z  )   �  -   �     �     �          &     =     I      P     q  -   v  =   �  +   �       
   &     1  B   6     y     �  )   �     �  
   �     �  !   �      �  	   	     "	     /	  -   7	     e	     k	     r	  
   �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	  
   �	  #   
     ,
     2
     9
  !   A
  
   c
  )   n
  $   �
  (   �
  *   �
  -     $   ?  +   d  4   �  (   �     �     �                    (     .     O  L   l  A   �  .   �  1   *  6   \     �     �     �     �     �     �      �       <     K   W  2   �     �     �        A     #   H     l  8   u     �  
   �     �  !   �  '   �  	             -  $   5     Z     a     i     z     �     �     �  	   �     �     �     �     �  
   �       )        9     ?     H     Q     l  /   |  *   �  /   �  4     1   <  4   n  :   �  ;   �  "        =     C     [     d     m     |        7                     B                 6   1      =   >       E               8       $              ;   %   -       ?   *             D       )   0       '              &      !      <   	   :   +   3                 A      /              C   
   #                          2                (   @   4       5             "       9   ,         .        A triangle Kaleidoscope Add a loopback alpha blending effector with rotating and scaling Add age to video input using scratches and dust Add more saturation to the video input Add some hallucination to the video input Add the ripple mark effect on the video input Bulge Bulges the center of the video Cartoon Cartoonify video input Che Guevara Chrome Detect radioactivity and show it Dice Dices the video input into many small squares Display video input like good old low resolution computer way Dissolves moving objects in the video input Distort the video input Distortion Edge Extracts edges in the video input through using the Sobel operator Fake heat camera toning Flip Flip the image, as if looking at a mirror Heat Historical Hulk Invert and slightly shade to blue Invert colors of the video input Invertion Kaleidoscope Kung-Fu Makes a square out of the center of the video Mauve Mirror Mirrors the video Noir/Blanc Optical Illusion Pinch Pinches the center of the video Quark Radioactive Ripple Saturation Sepia Sepia toning Shagadelic Show what was happening in the past Sobel Square Stretch Stretches the center of the video Time delay Traditional black-white optical animation Transform motions into Kung-Fu style Transform video input into a mauve color Transform video input into a metallic look Transform video input into a waveform monitor Transform video input into grayscale Transform video input into realtime goo'ing Transform video input into typical Che Guevara style Transform yourself into the amazing Hulk Twirl Twirl the center of the video Vertigo Warp Waveform X-Ray Project-Id-Version: cheese
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-video-effects&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-10-29 03:54+0000
PO-Revision-Date: 2015-11-10 03:41+0000
Last-Translator: Enrico Nicoletto <liverig@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:02+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 Um caleidoscópio triangular Adiciona um efetuador de mistura alfa de auto-retorno com rotação e escala Adicionar idade para a entrada de vídeo usando raspagem e poeira Adiciona mais saturação na entrada de vídeo Adiciona alguma alucinação na entrada de vídeo Adiciona um efeito de ondulação na entrada de vídeo Inchar Incha o centro do vídeo Cartoon Cartoonificar entrada de vídeo Che Guevara Cromo Detecta radioatividade e exibe-a Despedaçar Despedaçar a entrada de vídeo em muitos quadrados pequenos Exibe a entrada de vídeo como um bom computador velho de baixa resolução Dissolve objetos em movimento na entrada de vídeo Distorcer a entrada de vídeo Distorção Borda Extrai as beirada da entrada de vídeo através do operador Sobel Tom de aquecimento falso da câmera Inverter Inverte a imagem como se estivesse olhando em um espelho Aquecimento Histórico Hulk Inverte e dá um leve tom de azul Inverter cores em uma entrada de vídeo Inversão Caleidoscópio Kung-Fu Tira um quadrado do centro do vídeo Lilás Espelho Espelha o vídeo Preto e branco Ilusão de ótica Espremer Espreme o centro do vídeo Dissolver Radioatividade Ondulação Saturação Sépia Tom sépia Psicodélico Exibe o que estava acontecendo no passado Sobel Quadrado Espichar Espicha o centro do vídeo Atraso de tempo Animação de ótica tradicional preto-e-branco Transforma os movimentos no estilo Kung-Fu Transformar entrada de vídeo em uma cor lilás Transformar entrada de vídeo em um visual metálico Transformar entrada de vídeo em ondas do monitor Transformar entrada de vídeo em uma escala de cinza Transforma a entrada de vídeo em uma viagem em tempo real Transformar entrada de vídeo no estilo típico Che Guevara Transforme-se em no incrível Hulk Girar Gira o centro do vídeo Vertigem Deformar Formas de onda Raio-X 