��          �      \      �  $   �  )   �        6   9     p  /   �  (   �     �  +   �     $     B  $   ^  -   �  +   �  7   �       &   -  /   T     �  �  �  -   j  3   �  "   �  ?   �     /  5   B  *   x     �  2   �     �  %     %   6  D   \  6   �  W   �     0	  1   P	  8   �	     �	                                  	                      
                                           Attempt to set invalid NRC map '%c'. Attempt to set invalid wide NRC map '%c'. Could not open console.
 Could not parse the geometry spec passed to --geometry Duplicate (%s/%s)! Error (%s) converting data for child, dropping. Error compiling regular expression "%s". Error creating signal pipe. Error reading PTY size, using defaults: %s. Error reading from child: %s. Error setting PTY size: %s. Got unexpected (key?) sequence `%s'. No handler for control sequence `%s' defined. Unable to convert characters from %s to %s. Unable to send data to child, invalid charset convertor Unknown pixel mode %d.
 Unrecognized identified coding system. _vte_conv_open() failed setting word characters can not run %s Project-Id-Version: 2.0
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=vte&component=general
POT-Creation-Date: 2007-10-06 10:12+0000
PO-Revision-Date: 2016-02-08 00:36+0000
Last-Translator: Daniel <Unknown>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:07+0000
X-Generator: Launchpad (build 18115)
 Tentativa de definir mapa NRC inválido "%c". Tentativa de definir mapa NRC largo inválido "%c". Não foi possível abrir console.
 Não foi possível analisar a geometria do argumento --geometry Duplicata (%s/%s)! Erro (%s) ao converter dados para filho, descartando. Erro compilando a expressão regular "%s". Erro ao criar o pipe de sinais. Erro ao ler o tamanho do PTY, usando padrões: %s. Erro ao ler do filho: %s. Erro ao definir o tamanho do PTY: %s. Seqüência (chave?) "%s" inesperada. Nenhum manipulador foi definido para a seqüência de controle "%s". Não foi possível converter caracteres de %s para %s. Não foi possível enviar dados para o filho, conversor inválido do mapa de caracteres Modo de pixel %d desconhecido.
 Código identificado de sistema não reconhecido. _vte_conv_open() falhou ao definir caracteres de palavra não foi possível executar %s 