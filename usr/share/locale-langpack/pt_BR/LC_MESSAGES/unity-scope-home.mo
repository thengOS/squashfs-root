��    b      ,  �   <      H  "   I  �   l     �     	  p   	  	   �	     �	     �	     �	  
   �	     �	  	   �	     �	     �	     �	     �	  :   �	  	   
     '
     4
     D
     L
     T
     Z
     c
     h
     p
     u
  	   z
  -   �
  #   �
     �
     �
     �
  	          	         *  	   0  	   :     D     I     O     l     s     �     �     �     �     �     �     �     �     �  	   �     �     �     �       	   "     ,     9     G     U  '   j     �     �  1   �     �  9   �  9     7   K     �     �     �     �     �  
   �     �     �     �     �  	   �     �     �  	   �     �     �     �     �          
       
        &     4     =  �  B  "     �   )     �     �  p   �  	   Y     c     j     q  
   }     �  
   �     �     �     �     �  @   �  	             #     5     <     C  	   K     U  
   [     f     n  
   |  ?   �  -   �     �       '        9     @     P     \     e     r  	        �  8   �     �     �     �     �     �     �               !     ;     O     X  
   d  
   o     z     �     �     �     �     �     �  3   
     >     F  8   O     �  8   �  :   �  ;        ?     D     I     Q     `     h     u     {          �     �     �     �     �     �     �     
          *     7     E     W     n     �     �                     C          ;          <   7   Q       0   9       @   M      "   B   /             A      ]         &       K   ^   O           G   H   #   4   Z                 T   =      a   6       :             [   -   E   
                     	   F   V       2              J   Y   b           P       *          _            !   (           5       $   '      X      U      >   S             .   \   R      `   I   ,       )      ?   1   D   8   3   %   W   N   L                  +    1KB;100KB;1MB;10MB;100MB;1GB;>1GB; Accessories;Education;Games;Graphics;Internet;Fonts;Office;Media;Customisation;Accessibility;Developer;Science & Engineering;Dash plugins;System Albums Applications Blues;Classical;Country;Disco;Funk;Rock;Metal;Hip-hop;House;New-wave;R&B;Punk;Jazz;Pop;Reggae;Soul;Techno;Other; Bookmarks Books Boxes Calendar Categories Code Community Dash plugins Date Decade Documentation Documents;Folders;Images;Audio;Videos;Presentations;Other; Downloads Encyclopedia Files & Folders Folders Friends Genre Graphics Help History Home Info Installed Last 7 days;Last 30 days;Last 6 months;Older; Last 7 days;Last 30 days;Last year; Last modified Local Local apps;Software center Locations More suggestions Most used Music My photos My videos News Notes Old;60s;70s;80s;90s;00s;10s; Online Online photos People Photos Popular online Radio Recent Recent apps Recently played Recently used Recipes Reference Results Scholar Search applications Search files & folders Search in Search music Search photos Search videos Search your computer Search your computer and online sources Size Songs Sorry, there is nothing that matches your search. Sources There are no photos currently available on this computer. There are no videos currently available on this computer. There is no music currently available on this computer. Top Type Upcoming Updates Videos Vocabulary Weather Web books; boxes; calendar; code; files; graphics; help; info; music; news; notes; photos; recipes; reference; videos;video; weather; web; Project-Id-Version: unity-scope-home
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-12 20:00+0000
PO-Revision-Date: 2016-03-22 20:27+0000
Last-Translator: Fábio Nogueira <fnogueira@protonmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:22+0000
X-Generator: Launchpad (build 18115)
 1KB;100KB;1MB;10MB;100MB;1GB;>1GB; Acessórios;Educação;Jogos;Gráficos;Internet;Fontes;Escritório;Mídia;Personalização;Acessibilidade;Desenvolvimento;Ciência e Engenharia;Plugins do Painel;Sistema; Álbuns Aplicativos Blues;Clássica;Country;Disco;Funk;Rock;Metal;Hip-hop;House;New-wave;R&B;Punk;Jazz;Pop;Reggae;Soul;Techno;Outro; Favoritos Livros Caixas Calendário Categorias Código Comunidade Plug-ins do painel Data Década Documentação Documentos;Pastas;Imagens;Áudio;Vídeos;Apresentações;Outros; Downloads Enciclopédia Arquivos e pastas Pastas Amigos Gênero Gráficos Ajuda Histórico Início Informações Instalados Últimos 7 dias;Últimos 30 dias;Últimos 6 meses;Mais antigas; Últimos 7 dias;Últimos 30 dias;Último ano; Última modificação Local Aplicativos locais;Central de programas Locais Mais sugestões Mais usadas Músicas Minhas fotos Meus vídeos Notícias Notas Antigas;Anos 60;Anos 70;Anos 80;Anos 90;Anos 00;Anos 10; On-line Fotos on-line Pessoas Fotos Popular on-line Rádio Recentes Aplicativos recentes Reproduzidas recentemente Usados recentemente Receitas Referência Resultados Acadêmico Pesquisar aplicativos Pesquisar arquivos e pastas Pesquisar em Pesquisar músicas Pesquisar fotos Pesquisar vídeos Pesquisar no seu computador Pesquisar em seu computador e em fontes da Internet Tamanho Músicas Desculpe, não há nada que corresponda à sua pesquisa. Fontes Não há fotos disponíveis neste computador atualmente. Não há vídeos disponíveis neste computador atualmente. Não há músicas disponíveis neste computador atualmente. Topo Tipo Por vir Atualizações Vídeos Vocabulário Clima Web books;livros; boxes;caixas; calendar;agenda;calendário; code;código; files;arquivos; graphics;gráficos; help;ajuda; info;informações; music;músicas; news;notícias; notes;notas; photos;fotos; recipes;receitas; reference;referência; videos;video;vídeos;vídeo; weather;clima; web; 