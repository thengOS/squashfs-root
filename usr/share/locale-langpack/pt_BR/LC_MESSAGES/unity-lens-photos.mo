��    1      �  C   ,      8     9  b   H     �  	   �     �     �  
   �     �     �     �     �     �                    &  	   /     9     ?     M     [     b     w     ~     �     �     �     �     �     �  -   �  +        ;     D     S  2   X     �     �  �   �  �   �  �   d  �   F	     *
     /
  	   4
     >
     F
  	   N
  �  X
       n   #     �  
   �     �     �  
   �     �     �     �     �     �     �                    $     1     =     K     \     b          �     �     �     �     �     �     �  2   
  0   =     n     w     �  >   �     �     �  �   �  �   �  �   �  �   �  
   �     �  	   �     �     �  	   �         #                     &   -      %                    	          /              '       $          "                                    !   0       ,         +      1   .      
   *         (              )                           %s x %s pixels <b>This photo is missing.</b>
You can open Shotwell to retrieve it or remove it from your library. Album By %s, %s Camera Date Dimensions Email Facebook Flickr Friends Photos Last 30 days Last 6 months Last 7 days License Location My Photos Older Online Photos Open Shotwell Photos Photos search plugin Picasa Print Recent Search Facebook Search Flickr Search Picasa Search Shotwell Search your Picasa photos Search your and your friends' Facebook photos Search your and your friends' Flickr photos Shotwell Show in Folder Size Sorry, there are no photos that match your search. Tags This Computer This is an Ubuntu search plugin that enables information from Facebook to be searched and displayed in the Dash underneath the Photos header. If you do not wish to search this content source, you can disable this search plugin. This is an Ubuntu search plugin that enables information from Flickr to be searched and displayed in the Dash underneath the Photos header. If you do not wish to search this content source, you can disable this search plugin. This is an Ubuntu search plugin that enables information from Picasa to be searched and displayed in the Dash underneath the Photos header. If you do not wish to search this content source, you can disable this search plugin. This is an Ubuntu search plugin that enables information from Shotwell to be searched and displayed in the Dash underneath the Photos header. If you do not wish to search this content source, you can disable this search plugin. View With facebook; flickr; picasa; shotwell; Project-Id-Version: unity-lens-photos
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-18 15:04+0000
PO-Revision-Date: 2014-04-07 16:41+0000
Last-Translator: Renan Araujo Rischiotto <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:19+0000
X-Generator: Launchpad (build 18115)
 %s x %s pixels <b>Essa foto está perdida.</b>
Você pode abrir o Shotwell para recuperá-la ou removê-la da sua biblioteca. Álbum Por %s, %s Câmera Data Dimensões E-mail Facebook Flickr Fotos de amigos Últimos 30 dias Últimos 6 meses Últimos 7 dias Licença Local Minhas fotos Mais antigo Fotos on-line Abrir o Shotwell Fotos Plug-in de pesquisa de fotos Picasa Imprimir Recente Pesquisar no Facebook Pesquisar no Flickr Pesquisar no Picasa Pesquisar no Shotwell Pesquisar suas fotos no Picasa Pesquisar suas fotos e dos seus amigos no Facebook Pesquisar suas fotos e dos seus amigos no Flickr Shotwell Mostrar na pasta Tamanho Desculpe, não existem fotos que correspondam à sua pesquisa. Tags Esse computador Este é um plug-in de busca do Ubuntu que permite que informações do Facebook sejam pesquisadas e exibidas no Painel debaixo do cabeçalho Fotos. Se você não deseja pesquisar essa fonte de conteúdo, você pode desativar este plug-in de busca. Este é um plug-in de busca do Ubuntu que permite que informações do Flickr sejam pesquisadas e exibidas no Painel debaixo do cabeçalho Fotos. Se você não deseja pesquisar essa fonte de conteúdo, você pode desativar este plug-in de busca. Este é um plug-in de busca do Ubuntu que permite que informações do Picasa sejam pesquisadas e exibidas no Painel debaixo do cabeçalho Fotos. Se você não deseja pesquisar essa fonte de conteúdo, você pode desativar este plug-in de busca. Este é um plug-in de busca do Ubuntu que permite que informações do Shotwell sejam pesquisadas e exibidas no Painel debaixo do cabeçalho Fotos. Se você não deseja pesquisar essa fonte de conteúdo, você pode desativar este plug-in de busca. Visualizar Com facebook; flickr; picasa; shotwell; 