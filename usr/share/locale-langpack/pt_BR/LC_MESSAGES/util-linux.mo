��    �       �  :      `M  	   aM  #   kM     �M  �   �M     ,N     GN  (   `N     �N     �N  3   �N     �N     �N     �N     O     $O  �   =O  
   �O  �  �O     �Q     �Q     �Q      �Q     R  ^   R     vR  $   �R  '   �R  %   �R      S  :   S  )   GS     qS  1   �S  9   �S  $   �S     !T     ;T     QT     YT     bT  
   gT     rT     T     �T     �T     �T     �T     �T  )   �T  	   U  &   U     @U     LU     XU     qU     �U     �U     �U  /   �U      �U     V     6V     NV     hV     �V     �V  4   �V     �V     �V  9   �V     1W     9W     WW     wW  /   �W  #   �W  %   �W     X     -X  !   CX  D   eX     �X     �X     �X     Y     "Y     ;Y     OY  !   mY     �Y     �Y  "   �Y     �Y  !   	Z     +Z     DZ     ^Z     rZ  .   �Z     �Z  >   �Z     	[  O   [     j[     �[     �[     �[     �[     �[     �[     \     1\     L\     h\     u\  (   �\     �\     �\     �\  (   ]      C]  A   d]  (   �]  L   �]     ^     /^     A^     Y^  '   y^     �^     �^     �^  "   �^     _     8_     U_  !   m_  )   �_     �_     �_     �_     �_     `  
   `     "`     )`  	   1`     ;`     J`     R`     j`  $   �`  6   �`  !   �`  1   a  !   :a  1   \a  !   �a  9   �a  %   �a  6   b  '   Gb  %   ob     �b  W   �b     c     c     $c     ;c     Uc     jc     }c     �c     �c     �c  *   �c     �c  &   d  l  Bd  3   �h     �h     �h     �h     i     i  
   i     )i  ,   7i     di     hi     �i     �i  	   �i     �i  +   �i  -   �i     j  	   j     j     j     2j     9j     Bj     Gj     Yj     bj     nj     ~j     �j     �j     �j  $   �j     �j     �j     k  "   k     5k     Jk     ak     tk     �k  O   �k  E   �k     &l  $   Bl     gl  	   �l     �l     �l  
   �l     �l     �l     �l     �l  
   m     m     m     !m     >m     Km     Rm     ^m     ym     �m     �m  .   �m     �m     �m  Z    n     [n     an     en     �n     �n     �n  '   �n     �n     �n  
   �n     o     o     ,o     <o  +   \o     �o  4   �o     �o      �o  "   p     3p     <p      Bp     cp     sp     {p     �p     �p     �p     �p  
   �p  
   �p     �p     �p     q     q     q     )q     6q     Hq     Yq     kq     �q     �q  
   �q  
   �q  F   �q     r  0   r     Kr  
   dr     or  /   ~r     �r  .   �r  ,   �r     &s     Cs     Xs  @   js     �s  '   �s  ?   �s     )t     ;t  )   Ut     t     �t     �t     �t     �t     �t  0   �t  5   u     Pu     bu     pu  	   vu  
   �u     �u     �u     �u     �u  
   �u     �u     �u      v     v     -v     ?v  *   Kv  "   vv     �v     �v     �v  &   �v     �v     �v     w     'w     5w     :w  A   Cw     �w     �w     �w  	   �w     �w     �w  %   �w     �w     x  !   ,x     Nx  |   Zx  T   �x     ,y     Ay  7   Iy     �y  3   �y  C   �y     z     z     .z     ;z     Gz     ez     yz     �z     �z     �z     �z     �z     �z  	   �z  
   �z     �z     	{  
   {     %{     D{     L{     R{     `{     q{  
   �{     �{     �{     �{     �{     �{  #   �{     |     	|     |     )|  ,   .|  
   [|  9   f|  	   �|     �|      �|     �|     �|  #   }  1   '}     Y}     `}     m}     �}  "   �}     �}     �}     �}     �}     �}     �}  	   �}      ~     ~     ~  
   !~  
   ,~     7~  
   ?~     J~     R~     Z~     s~     �~     �~     �~     �~     �~     �~     �~  B   �~     8     N     ^  
   e     p     x  	   �  	   �     �     �  
   �     �  
   �     �  
   �  	   �  	   �     	�     !�     3�     :�  I   O�  [   ��  �   ��  �   ��  ?   '�     g�     ��  <   ��  X   Ղ  -   .�  &   \�     ��     ��  )   ��  '   ��  $   ރ  (   �  &   ,�  !   S�  .   u�     ��     ��     ӄ  =   �     )�  -   F�  '   t�     ��     ��     ͅ  
   �     �     ��     �     "�  2   )�     \�  )   |�     ��     ��  A   ʆ     �     �  
   '�     2�     >�     M�     b�     r�     ��     ��  	   ��     ��  p   ��     -�  +   H�  2   t�  %   ��     ͈  Y   �  (   F�  
   o�  *   z�  %   ��     ˉ  $   щ  7   ��  
   .�  	   9�  ,   C�  /   p�     ��     ��     Ċ  )   �  !   �  !   .�     P�     o�     |�     ��  '   ��     ׋      �  2   �     @�     N�     `�     w�     ��     ��  %   ��     ߌ     �     ��     	�     '�     ;�     L�     [�     {�     ��  &   ��  &   ̍     �     �     �  '   )�     Q�  2   U�  &   ��     ��     Î     Վ     ێ     �     �     �     �  "   2�     U�     j�     {�     ��     ��     ��  %   ׏     ��     �     ,�  %   G�     m�     |�     ��     ��     ��     ��     ��     Ɛ     א     �     ��     �      1�     R�  %   m�     ��     ��     ��  	   ő     ϑ     �     �     �     2�     D�  	   I�     S�     `�  2   u�     ��     ��     ��     ɒ     ے     �  	   �  5   ��     3�     @�  -   F�     t�     ��     ��     ��     ғ     �     ��  (   �  !   A�  -   c�     ��     ��     ��     ̔     �     �     �     .�  !   C�     e�  *   ��  #   ��  &   ҕ  !   ��     �     4�     T�     h�     ��     ��     ��  !   Ζ     �     �     )�  ,   1�  ,   ^�     ��     ��     ��     ɗ     ڗ     ��     �     �     %�     1�  
   Q�     \�     q�     v�  1   ��     ��     Ҙ  !   ��     �     �     ;�     W�     l�      ��     ��     ��     ��     Й  
   ԙ  
   ߙ  !   �  (   �  (   5�     ^�     z�  5   ��  
   Κ     ٚ  #   �     �      �     9�  
   R�     ]�     i�     ��     ��     ��     ϛ  1   �     �  2   .�  >   a�  "   ��  !   Ü     �     ��     	�  )   �  %   C�  (   i�     ��     ��     ��  (   ��     ܝ     �     �     �     
�     �     �     +�     3�     8�     >�     D�     P�     k�     ��     ��      Ğ     �  (   �     �     (�     C�     T�     e�     ��     ��  T  ��     �  1   �     :�     H�     O�  %   V�     |�     �     ��  
   ��     ��     ա     ޡ     �     ��     �     �     =�  C   R�  +   ��     ¢     Ȣ     ڢ     �     �      �     3�     E�     K�     P�     _�     n�     �     ��     ��     ��     ԣ     ڣ     ޣ  
   �     ��  
   
�     �     &�     7�     <�     M�     d�      y�     ��     ��     Ӥ     פ     ܤ     �  "   �     �     &�     4�  
   T�     _�     p�     |�     ��     ��      ˥  ,   �     �     �     &�     +�     ?�     X�     f�     u�     ��     ��     ��     ��     Ǧ     ͦ     �     ��     �  $   �     C�  !   X�     z�     ��  	   ��     ��     ��     ٧     ��     �  !   &�  8   H�  '   ��     ��     è     ֨     �     �     
�     �  	   .�     8�     Q�     f�     u�  %   y�  "   ��  $   ©      �  8   �     A�     Z�     u�     ��     ��  %   Ī     �     �     #�     +�     >�     Q�     h�     {�  *   ��     ��     ɫ     ث     �  �  �     ծ     �  
   ��     �  	   �     �     (�     .�     6�     K�     Z�  6   t�  *   ��  )   ֯  :    �     ;�     W�     d�     j�     {�     ��     ��     ��  �  ��  	   ��  #   ��     ��  �   ز     ��     ��  4   Ƴ     ��     �  7   �     P�     h�     u�     ��      ��  �   Ǵ     a�  '  m�     ��     ��  !   ȷ  0   �     �  i   "�     ��  +   ��  -   θ  "   ��  
   �  E   *�  &   p�     ��  ?   ��  >   ��  +   4�  !   `�     ��     ��     ��     ��  
   ��     ��     ̺     �      �     �     '�     3�  4   9�     n�  +   {�     ��  	   ��     ��  $   ܻ     �  #   �     A�  ;   U�  ,   ��      ��     ߼     ��     �     6�     K�  H   _�     ��     ��  C   ؽ     �      $�  %   E�  #   k�  K   ��  1   ۾  1   �  &   ?�  &   f�  9   ��  `   ǿ  :   (�  8   c�  -   ��  !   ��  !   ��     �  #   &�  '   J�  *   r�     ��  )   ��      ��  &   ��     $�      ?�     `�     {�  /   ��     ��  I   ��     $�  d   5�  #   ��     ��  &   ��  $   ��     $�  -   8�  &   f�     ��     ��  #   ��     ��     ��  *   �      D�     e�      ��  %   ��  '   ��  Q   ��  7   F�  ]   ~�     ��     ��  +   �  +   -�  7   Y�  +   ��  +   ��  $   ��  3   �  (   B�  *   k�  #   ��  '   ��  '   ��  &   
�  #   1�     U�     [�     o�     ��  	   ��     ��  	   ��     ��     ��     ��      ��  #   ��  @   "�  #   c�  4   ��  #   ��  7   ��  "   �  K   ;�  &   ��  7   ��  (   ��  %   �     5�  i   J�     ��  !   ��     ��  !   ��     �     3�     Q�     ]�     t�     ��  :   ��  ,   ��  )   �  �  :�  <   ��     ,�     0�     C�     R�     i�  
   p�     {�  >   ��     ��  %   ��     ��     ��  	   �     �  7   "�  -   Z�     ��  	   ��     ��     ��     ��  	   ��     ��     ��     ��     ��     �     #�     +�     4�     L�  )   ]�  &   ��  +   ��     ��  *   ��     �     %�     >�     R�     j�  `   r�  T   ��  (   (�  .   Q�  $   ��  	   ��     ��     ��  
   ��     ��     ��     ��     �  
   "�     -�     :�     B�     \�     m�     y�     ��     ��  !   ��     ��  C   ��     ,�     ?�  ^   H�     ��     ��     ��  "   ��  	   ��     ��  3   �     C�     I�     O�     [�     y�     ��  &   ��  A   ��  #   �  O   +�  ,   {�  (   ��  (   ��     ��     �  6   �     F�     V�     ^�     o�     s�     ��  	   ��  
   ��  
   ��     ��  (   ��     �     �     �     '�     7�     M�     d�     y�     ��     ��  
   ��     ��  U   ��     -�  5   G�     }�  	   ��     ��  B   ��     ��  ;   �  6   N�     ��     ��     ��  T   ��     ,�  4   F�  I   {�     ��     ��  4   ��     *�     2�  !   E�     g�     u�     ��  3   ��  ;   ��     �     $�     7�  	   =�  
   G�     R�     b�     o�     ��  
   ��     ��     ��     ��     ��     �     �  +   !�  !   M�     o�     ��     ��  :   ��     ��     ��  *   �     .�     >�     C�  Z   L�     ��     ��     ��  
   ��  $   ��     ��  /   �     B�  !   Z�  ;   |�     ��  �   ��  k   u�     ��     ��  [   �     d�  >   {�  D   ��     ��     �     %�     9�  !   J�     l�     ��     ��     ��     ��     ��     ��     ��     ��  
   �     �     '�     8�  $   D�     i�     q�     w�     ��     ��     ��     ��     ��     ��     ��     ��  6   �     J�     Q�     b�     s�  1   x�  	   ��  9   ��  
   ��     ��  1   �     A�  $   W�  2   |�  C   ��     ��     ��     �     %�  )   ;�     e�     ��     ��     ��     ��     ��  	   ��     ��     ��     ��  
   ��  
   ��     ��  
   ��     ��     �  #   �     0�  !   F�     h�     {�     ��     ��  
   ��  !   ��  R   ��  !   '�     I�     Z�     ^�     j�     r�  	   ��     ��     ��     ��  
   ��     ��  
   ��     ��  
   ��  	   ��  	   �     �     &�     9�     @�  Z   U�  y   ��  �   *�  �   ��  N   k�     ��     ��  @   ��  `   /�  8   ��  -   ��     ��     ��  8    �  4   9�  ,   n�  7   ��  2   ��  0   �  5   7�  %   m�     ��  $   ��  N   ��  1   %�  9   W�  8   ��  '   ��      ��  &   �     :�     H�     U�  !   n�  	   ��  5   ��  !   ��  0   ��     #�     9�  [   Q�     ��     ��     ��     ��     ��     ��     �     !�      1�     R�  	   b�     l�  �   |�      	�  /   *�  8   Z�  #   ��  )   ��  _   ��  3   A�     u�  "   ��  )   ��     ��  5   ��  A   �  
   O�  	   Z�  9   d�  9   ��     ��     ��     
�  4   )�  #   ^�  <   ��  7   ��     ��     �  (   +�  2   T�     ��  '   ��  =   ��     �     �     4�     I�     b�     r�  )   ��     ��     ��     ��  %   ��  $   �     +�     H�  '   c�  !   ��     ��  +   ��  +   ��     $  #   =     a  =        �  >   �  0       4    L    e    k !   w .   �    � '   � 5   
 &   @ '   g -   � 2   �     � (    -   : +   h    � 0   � ;   � %       C    `    e    q    �    �    � %   �    � /   � &    .   @ (   o E   �    �    �    �     $   #    H *   f    �    �    � 	   �    �    � 6   �    (    - 	   @    J    g    { 	   � C   �    �    � 0   �    '    F    X     x    � #   �     � .   � ,   )	 1   V	    �	     �	    �	 $   �	    �	    �	    
    2
 "   G
 $   j
 +   �
 (   �
 (   �
 %       3 &   N    u    �    �     � #   � +    )   - !   W 
   y :   � :   �     � "       >    Y )   w    �    �    �    � *   �        .    I    R :   ^ ,   �    � -   � '    '   3 *   [    �    � '   �    �             )    - 
   ; &   F 2   m 2   �     � !   � =    
   T    _ +   l    �    � '   �    �     +       A )   ]    �    � F   �    � D    Q   T $   � #   �    �         7   + 0   c <   �    �    �    � +   �     
   3    >    P    X 	   ^    h    �    �    �    �    �    � %   � .   � $   ! +   F 	   r +   |    � #   �    �    �            9 �  R    � d   �    L    Z    a 3   h    � #   � &   �    � ,   �    *    7    D    [    { /   �    � M   � ;       X    ^ 	   q (   {    � %   �    �    �    �    �        $    :    R    d    �    �    �    �    �    �    �    �            + "   B    e    {     �     �    �    �    �    � ,   �    +    < %   K    q        �    �    �    � !   � 7       L    R    Y    _ *   s    �    �    �    �    �    	          /  &   5     \     s  #   z  /   �     �      �     !    "!    )! %   6! +   \! %   �! #   �!    �! 0   �! @   " ,   _" -   �"    �" !   �" ,   �"    )#    /#    E# 	   V#     `#    �#    �#    �# 0   �# 8   �# /   $ <   I$ O   �$ '   �$ !   �$ 0    %     Q% 2   r% 7   �% $   �%    &    "&    /&    G&    _&    {&    �& 0   �&    �&    �&    ' -   ' l  H'    �*    �*    �*    �*    �*    +    +    $+    ,+    G+    V+ =   r+ /   �+ .   �+ 0   ,     @,    a,    p,    w, '   �,    �,    �,    �,        �  �      �  =       �   M  R  k  �   �   �    �  �  s     �  �      �  �  	   �  �          }  �  �  �   �          o  8  �  |  H   -    A  P   ~  �  J  9      �  R  �   �         2                  �  �   �      ]      E  �  �  �  >  �   &  �   F  �   D   �  >      t   +  �      9  d  p  W      +  �      5      �   j    �        �  /  �  �          �     �   �        �      �   �  �      �  {  �  �   �   �  �   �    C  �  �  �   T   K  D  �   �      X  "          �  �  �      h           #  u         �   �  U      8      L      b  �  �  �  �  �   �       �   �      �   `             n  �  �   �  �  y     �  �   �        �       �  �       O            f          �   �          �          �  �   0       w  E  ]          �  �  �  �  �  P  x    �  �  /  �  �      �           q      �   �          N      �       [   U     #  �  �       �  �   s  n      
  �   j   �            E  L              �  A   )     -  H  S  1   �  �           @   k    �  �    �          `  $  �        �  g  �   �   	  Z   �      �  �      ^        {  �  �         =  H      �   �   �   �  �   Q  [  *  �   �  g  r   �    *  Z  '      �  '            �          �  �  {  �   �  2  �    �   e      ?  �                  ,          |       �  �  N      D  0  �  �   k  C  ]   _       �   {   ~      �   W  �   �      �               �  �  W   I  �   5  K   }   L  �  Y  M  w       �   %  9         �                  ^   ~  %       @      d  W  X  �      �  ?        �  ;  �  �          n       6    %  #       �  �  �  5          �  [  "  a      p  z      <  �  �        �  I  D  i       �  �      <  X    i  _      �   �      �  7  �  �  h  �  �  �    =      T      �       �  �      J    3                       �   *  �      �      �   �  (  -          �  K         x   �  �        �  h  B      Z  �             �            �      }  `   �      �  l      f        �  G      `  0  �             C  �  o   �          N  r  U  �  �   �             7   ?   ;  :   *   �  �  A  �  f   �  �  B  �         u      �    �        �      R       8  q   �  2         �       z   �    �   �  q         �    .   o  �   �      1      �           .          �      G  !  $  &  E   �  �  m          C   	    �   �   \  �   R          �  N       �   '                    7  	  c  Q  �    u  6                
  �          �  �                      +  �   �      �   /   �  �       i      F   �   �   j  �      =  �  �  )  �  �   �         :      .  V  ,  ^     �  9   8   �       6                    v   Y  @  �      a  �  P  �      (      v      �   �      a    <      m      �  �  �                    �   �      b   1  �   b  �  �  }  l      5        �  �         \        s  �  A               �  �    �  �       �      �  ;   �  �  J     �  �   Y     m   w  #  �  �     w  �               <  O   I   e  �   o  �   �   d      �  �   �  b  �   �            �  G         )         �  �   �          |  F  T  a            �  �  4  -   t      �  r  �                "  �   r  :      �      O  X   �      �  S   k   �      �  &              y           $  �  x  �  0  �   �  Y  H        �    �  i  �   u   
   S  3  �  v  �  ,      �                  $      t    �  .  �   �  �      
  �   c  �  �   4  l            g           �   �      �      �      �   d     �   �   �  z      �   �   \           �               �  �  �    �          _  �      �  )  �      �      t  �      ;  �          �  >      �        �           �  B      /  �   �  �  2  S  z  p  �   �  �  �  �   �           3  %      [  U   �  !   _  c      �      �    @      �  �          ^  M   �     �  n  F  6  �      q      (        �  �   V  P          �     �  �    �  �  :        7      V     !  �      ]  �     M  L   ~       �  x  T  �  �  �       �   �  �  �   G      �   y      �  �  v  �   �  >   �          f      �  �         e  ?  !      3   �   �   I      \  �      �      "   �      �  O  �           �      �         �   �   V      c   Q  s   1  4   �   (   �  �  �      |                  �  �   �        p   �  �   l   g  �  �  �  �  �   �   �      e   y          Q       '      &   J   4      �  B   �  +   �   �      m      �      j                      K      �  h  Z  ,   �   �           	UTC: %s
 	tv.tv_sec = %ld, tv.tv_usec = %ld
 	tz.tz_minuteswest = %d
 
%6d regular files
%6d directories
%6d character device files
%6d block device files
%6d links
%6d symbolic links
------
%6d files
 
%6ld inodes used (%ld%%)
 
*** %s: directory ***

 
******** %s: Not a text file ********

 
***Back***

 
...Skipping  
Available columns (for --show, --raw or --pairs):
 
Available columns:
 
Functions:
 
Known <ldisc> names:
 
Login incorrect
 
Message Queue msqid=%d
 
Most commands optionally preceded by integer argument k.  Defaults in brackets.
Star (*) indicates argument becomes new default.
 
Options:
 
Options:
 -c, --check               check bad blocks before creating the swap area
 -f, --force               allow swap size area be larger than device
 -p, --pagesize SIZE       specify page size in bytes
 -L, --label LABEL         specify label
 -v, --swapversion NUM     specify swap-space version number
 -U, --uuid UUID           specify the uuid to use
 -V, --version             output version information and exit
 -h, --help                display this help and exit

 
Pattern not found
 
Script done on %s 
Semaphore Array semid=%d
 
Shared memory Segment shmid=%d
 
Usage:
 
Usage:
 %1$s -V
 %1$s --report [devices]
 %1$s [-v|-q] commands devices

Available commands:
 
Usage:
 %s [options]
 
Usage:
 %s [options] device [size]
         (%s partition table detected).          (compiled without libblkid).    Overflow
   b          Toggle bootable flag of the current partition   d          Delete the current partition   h          Print this screen   n          Create new partition from free space   q          Quit program without writing partition table  %-25s get size in 512-byte sectors
  %s [options] <filename>
  %s [options] [file]
  Remove  badsect  ecc  removable "%s" line %d $TERM is not defined. %6.2f%% (%+ld bytes)	%s
 %6ld zones used (%ld%%)
 %ld blocks
 %ld inodes
 %s %d %s %s: status is %x, should never happen. %s cache: %s does not have interrupt functions.  %s failed.
 %s from %s
 %s has messages disabled %s has messages disabled on %s %s is a mountpoint
 %s is clean, no check.
 %s is mounted.	  %s is mounted; will not make a filesystem here! %s is not a block special device %s is not a mountpoint
 %s is not a serial line %s is not logged in on %s %s requires an argument %s status is %d %s succeeded.
 %s takes no non-option arguments.  You supplied %d.
 %s using IRQ %d
 %s using polling
 %s-sided, %d tracks, %d sec/track. Total capacity %d kB.
 %s: OK
 %s: Unrecognized architecture %s: adding partition #%d failed %s: assuming RTC uses UTC ...
 %s: bad blocks before data-area: cannot make fs %s: bad directory: '.' isn't first
 %s: bad directory: '..' isn't second
 %s: bad directory: size < 32 %s: can't exec %s: %m %s: can't open file of bad blocks %s: cannot add inotify watch (limit of inotify watches was reached). %s: cannot add inotify watch. %s: cannot read badblocks file %s: cannot read inotify events %s: change directory failed %s: device is misaligned %s: dup problem: %m %s: error adding partition %d %s: error adding partitions %d-%d %s: exceeded limit of symlinks %s: failed to read link %s: failed to read partition table %s: failed to seek to swap UUID %s: failed to seek to swap label  %s: failed to write UUID %s: failed to write label %s: get size failed %s: input overrun %s: insecure permissions %04o, %04o suggested. %s: is not a directory %s: last_page 0x%08llx is larger than actual size of swapspace %s: lseek failed %s: nonexistent device ("nofail" fstab option may be used to skip this device)
 %s: not a block device %s: not a directory %s: not a valid swap partition %s: not enough good blocks %s: not found %s: not open for read/write %s: number of blocks too small %s: partition #%d added
 %s: partition #%d removed
 %s: read swap header failed %s: read: %m %s: reinitializing the swap. %s: seek failed during testing of blocks %s: seek failed in check_blocks %s: seek failed in write_block %s: seek failed in write_tables %s: skipping - it appears to have holes. %s: skipping nonexistent device
 %s: software suspend data detected. Rewriting the swap signature. %s: swap format pagesize does not match. %s: swap format pagesize does not match. (Use --fixpgsz to reinitialize it.) %s: swapoff failed %s: swapon failed %s: too many bad blocks %s: unable to clear boot sector %s: unable to obtain selinux file label %s: unable to probe device %s: unable to write inode map %s: unable to write inodes %s: unable to write signature page %s: unable to write super-block %s: unable to write zone map %s: unknown device name %s: unsupported swap version '%s' %s: warning: don't erase bootbits sectors %s: write failed in write_block %s: write signature failed (EOF) (Next file:  (Next file: %s) (waiting)  , busy , error , on-line , out of paper , ready -- line already flushed ------ Message Queues --------
 ------ Message Queues PIDs --------
 ------ Message Queues Send/Recv/Change Times --------
 ------ Semaphore Arrays --------
 ------ Semaphore Arrays Creators/Owners --------
 ------ Semaphore Limits --------
 ------ Semaphore Operation/Change Times --------
 ------ Semaphore Status --------
 ------ Shared Memory Attach/Detach/Change Times --------
 ------ Shared Memory Limits --------
 ------ Shared Memory Segment Creators/Owners --------
 ------ Shared Memory Segments --------
 ------ Shared Memory Status --------
 -------      ------- ----------------------------
FILE SYSTEM HAS BEEN CHANGED
----------------------------
 --More-- --date argument too long --waiting-- (pass %d)
 ...Skipping back to file  ...Skipping to file  ...got clock tick
 ...skipping
 ...skipping backward
 ...skipping forward
 ...synchronization failed
 /dev/%s: cannot open as standard input: %m /dev/%s: not a character device : !command not allowed in rflag mode.
 <space>                 Display next k lines of text [current screen size]
z                       Display next k lines of text [current screen size]*
<return>                Display next k lines of text [1]*
d or ctrl-D             Scroll k lines [current scroll size, initially 11]*
q or Q or <interrupt>   Exit from more
s                       Skip forward k lines of text [1]
f                       Skip forward k screenfuls of text [1]
b or ctrl-B             Skip backwards k screenfuls of text [1]
'                       Go to place where previous search started
=                       Display current line number
/<regular expression>   Search for kth occurrence of regular expression [1]
n                       Search for kth occurrence of last r.e [1]
!<cmd> or :!<cmd>       Execute <cmd> in a subshell
v                       Start up /usr/bin/vi at current line
ctrl-L                  Redraw screen
:n                      Go to kth next file [1]
:p                      Go to kth previous file [1]
:f                      Display current file name and line number
.                       Repeat previous command
 AIEEE: block "compressed" to > 2*blocklength (%ld)
 AIX AIX bootable AST SmartSleep Alternate cylinders Amoeba Amoeba BBT Architecture: Assuming hardware clock is kept in %s time.
 BBT BLKGETSIZE ioctl failed on %s BSD/OS BSDI fs BSDI swap BeOS fs Block %d in file `%s' is marked not in use. Block has been used before. Now in file `%s'. BlockSize: %d
 BogoMIPS: Boot Boot Wizard hidden BootIt Bootable CP/M CP/M / CTOS / ... CPU MHz: CPU family: CPU op-mode(s): CPU(s): CRC: %x
 Calling settimeofday:
 Caps Lock on Changing finger information for %s.
 Changing shell for %s.
 Checking all file systems.
 Clear Clock not changed - testing only.
 Command      Meaning Command (m for help):  Compaq diagnostics Core(s) per socket: Correct Could not open file with the clock adjustment parameters in it (%s) for writing Could not update file with the clock adjustment parameters (%s) in it Couldn't initialize PAM: %s Create new partition from free space Current system time: %ld = %s
 Cylinders DIALUP AT %s BY %s DOS R/O DOS access DOS secondary DRDOS/sec (FAT-12) DRDOS/sec (FAT-16 < 32M) DRDOS/sec (FAT-16) Darwin UFS Darwin boot Delete Delete the current partition Dell Utility Device Device: %s
 Directory data: %zd bytes
 DiskSecure Multi-Boot Do you really want to continue Double Down Arrow   Move cursor to the next partition EFI (FAT-12/16/32) EZ-Drive Either all or none of the filesystem types passed to -t must be prefixed
with 'no' or '!'. Empty End Everything: %zd kilobytes
 Expert command (m for help):  Extended Extra sectors per cylinder FAILED LOGIN SESSION FROM %s FOR %s, %s FAT12 FAT16 FAT16 <32M FATAL: %s is not a terminal FATAL: bad tty FSname: <%-6s>
 Failed to set personality to %s Filesystem on %s is dirty, needs checking.
 Filesystem state=%d
 Finger information *NOT* changed.  Try again later.
 Finger information changed.
 Finger information not changed.
 Finished with %s (exit status %d)
 First %s Flags Forcing filesystem check on %s.
 Formatting ...  FreeBSD GNU HURD or SysV GPT Generated random UUID: %s
 Generated time UUID: %s
 Geometry Golden Bow HFS / HFS+ HPFS/NTFS/exFAT Hardware clock is on %s time
 Heads Help Hidden FAT12 Hidden FAT16 Hidden FAT16 <32M Hidden HPFS/NTFS Hidden NTFS WinRE Hidden W95 FAT16 (LBA) Hidden W95 FAT32 Hidden W95 FAT32 (LBA) Hint: %s

 Home Phone Hw clock time : %4d/%.2d/%.2d %.2d:%.2d:%.2d = %ld seconds since 1969
 Hypervisor vendor: I failed to get permission because I didn't try. IBM Thinkpad hibernation Id  Name

 Including: %s
 Inode %d marked unused, but used for file '%s'
 Inode %lu mode not cleared. Inode %lu not used, marked used in the bitmap. Inode %lu used, marked unused in the bitmap. Inode end: %d, Data end: %d
 Input line too long. Interleave factor Internal error: trying to write bad block
Write request ignored
 Invalid operation %d
 Invalid user name "%s" in %s:%d. Abort. Invalid values in hardware clock: %4d/%.2d/%.2d %.2d:%.2d:%.2d
 Is /proc mounted? Issuing date command: %s
 Kernel is assuming an epoch value of %lu
 LANstep LOGIN ON %s BY %s LOGIN ON %s BY %s FROM %s LPGETIRQ error LPGETSTATUS error Label was truncated. Last calibration done at %ld seconds after 1969
 Last drift adjustment done at %ld seconds after 1969
 Last login: %.*s  Line too long Linux Linux LVM Linux RAID Linux extended Linux native Linux plaintext Linux raid autodetect Linux swap Linux swap / Solaris Linux/PA-RISC boot List of UUIDs:
 Logging in with home = "/".
 Login incorrect

 Mark in use Message from %s@%s (as %s) on %s at %s ... Message from %s@%s on %s at %s ... Message queue id: %d
 Minix / old Linux Model: Must be superuser to set system clock. NEC DOS NTFS volume set NULL user name in %s:%d. Abort. NUMA node(s): Name NeXTSTEP Needed adjustment is less than one second, so not setting clock.
 NetBSD New New beginning of data New shell No --date option specified. No next file No previous command to substitute for No previous file No remembered search string No usable clock interface found.
 Non-FS data Not adjusting drift factor because last calibration time is zero,
so history is bad and calibration startover is necessary.
 Not adjusting drift factor because the Hardware Clock previously contained garbage.
 Not enough arguments Not set Not setting system clock because running in test mode.
 Not superuser. Not updating adjtime file because of testing mode.
 Note: All of the commands can be entered with either upper or lower Novell Netware 286 Novell Netware 386 Num Lock off Num Lock on Number of alternate cylinders Number of cylinders Number of heads Number of physical cylinders Number of sectors OPUS OS/2 Boot Manager Office Office Phone Old Minix OnTrack DM OnTrack DM6 Aux1 OnTrack DM6 Aux3 OnTrackDM6 Only 1k blocks/zones supported OpenBSD PC/IX PPC PReP Boot Partition number PartitionMagic recovery Password:  Pattern not found Physical cylinders Plan 9 Priam Edisk Print help screen Probably you need root privileges.
 QNX4.x QNX4.x 2nd part QNX4.x 3rd part Quit Quit program without writing partition table RE error:  RO    RA   SSZ   BSZ   StartSec            Size   Device
 ROM image ROM image map ROM image write failed (%zd %zd) ROOT LOGIN ON %s ROOT LOGIN ON %s FROM %s Read error: bad block in file '%s'
 Read error: unable to seek to block in file '%s'
 Read:  Remove block Rotation speed (rpm) Ruffian BCD clock
 SCHED_%s min/max priority	: %d/%d
 SCHED_%s not supported?
 SFS SGI bsd SGI efs SGI lvol SGI raw SGI rlvol SGI secrepl SGI sysv SGI trkrepl SGI volhdr SGI volume SGI xfs SGI xfslog SGI xlv SGI xvm Script done, file is %s
 Script started on %s Script started, file is %s
 Scroll Lock on Sectors Sectors/track Semaphore id: %d
 Set Set i_nlinks to count Setting Hardware Clock to %.2d:%.2d:%.2d = %ld seconds since 1969
 Shared memory id: %d
 Shell changed.
 Single Socket(s): Solaris Solaris boot SpeedStor Stepping: SunOS alt sectors SunOS cachefs SunOS home SunOS reserved SunOS root SunOS stand SunOS swap SunOS usr SunOS var Super block: %zd bytes
 Switching on %s.
 Syrinx TIOCSCTTY failed: %m The Hardware Clock does not contain a valid time, so we cannot adjust it. The Hardware Clock does not contain a valid time, so we cannot set the System Time from it. The Hardware Clock registers contain values that are either invalid (e.g. 50th day of month) or beyond the range we can handle (e.g. Year 2095). The date command issued by %s returned something other than an integer where the converted time value was expected.
The command was:
  %s
The response was:
 %s
 The directory '%s' contains a bad inode number for file '%.*s'. The file `%s' has mode %05o
 Thread(s) per core: Time read from Hardware Clock: %4d/%.2d/%.2d %02d:%02d:%02d
 To set the epoch value, you must use the 'epoch' option to tell to what value to set it. Toggle bootable flag of the current partition Try `%s --help' for more information.
 Type UTC Unable to allocate buffer for inode count Unable to allocate buffer for inode map Unable to allocate buffer for inodes Unable to allocate buffer for zone count Unable to allocate buffer for zone map Unable to connect to audit system Unable to get the epoch value from the kernel. Unable to read inode map Unable to read inodes Unable to read zone map Unable to run 'date' program in /bin/sh shell. popen() failed Unable to set system clock.
 Unable to set the epoch value in the kernel.
 Unable to set up swap-space: unreadable Unable to write inode map Unable to write inodes Unable to write zone map Unassigned Unknown Unknown command: %s Unknown user context Unmark Up Arrow     Move cursor to the previous partition Usage: %s [options] [file ...]
 Usage: %s [options] device [block-count]
 Using UTC time.
 Using local time.
 Using user-specified page size %d, instead of the system value %d VMware VMFS VMware VMKCORE Vendor ID: Venix 80286 Verifying ...  Virtualization type: Virtualization: Volume: <%-6s>
 W95 Ext'd (LBA) W95 FAT16 (LBA) W95 FAT32 W95 FAT32 (LBA) WARNING: device numbers truncated to %u bits.  This almost certainly means
that some device files will be wrong. Waiting for clock tick...
 Waiting in loop for time from %s to change
 Warning... %s for device %s exited with signal %d. Warning: Firstzone != Norm_firstzone
 Warning: inode count too big.
 Warning: unrecognized third line in adjtime file
(Expected: `UTC' or `LOCAL' or nothing.) Weird values in do_check: probably bugs
 Whole disk Would have written the following to %s:
%s Would you like to edit %s now [y/n]?  Write Write error: bad block in file '%s'
 Write partition table to disk (this might destroy data) XENIX root XENIX usr You are using shadow groups on this system.
 You are using shadow passwords on this system.
 You have mail.
 You have new mail.
 Zone %lu: in use, counted=%d
 Zone %lu: marked in use, no file uses it. Zone %lu: not in use, counted=%d
 Zone nr < FIRSTZONE in file `%s'. Zone nr >= ZONES in file `%s'. Zonesize=%d
 [Not a file] line %d [Press 'h' for instructions.] [Press space to continue, 'q' to quit.] [Use q or Q to quit] action must be taken immediately alarm %ld, sys_time %ld, rtc_time %ld, seconds %u
 alarm: on  %s all mount options allocated queues = %d
 allocated semaphores = %d
 already removed id already removed key atomic %s failed for 1000 iterations! att_time=%-26.24s
 attached bad arguments bad conversion character %%%s bad filename length bad inode offset bad inode size bad magic number in super-block bad response length bad root offset (%lu) bad s_imap_blocks field in super-block bad s_zmap_blocks field in super-block bad speed: %s bad timeout value: %s bad v2 inode size badblock number input error on line %d
 big block size smaller than physical sector size of %s blocks argument too large, max is %llu bogus mode: %s (%o) booted from MILO
 bytes bytes/sector can't fork
 can't open temporary file can't read %s cannot access file %s cannot check %s: fsck.%s not found cannot close file %s cannot daemonize cannot determine size of %s cannot find the device for %s cannot fork cannot get size of %s cannot get terminal attributes for %s cannot obtain the list of tasks cannot open %s cannot set line discipline cannot set terminal attributes for %s cannot stat %s cannot write %s cgid change change_time=%-26.24s
 changed check aborted.
 chown failed: %s clockport adjusted to 0x%x
 connect could not get device size could not read directory %s couldn't compute selinux context couldn't exec shell script couldn't find matching filesystem: %s cpid cpuset_alloc failed cramfs endianness is %s
 crc error create message queue failed create semaphore failed create share memory failed critical conditions ctime = %-26.24s
 cuid cylinders cylinderskew data block too large date string %s equates to %ld seconds since 1969.
 dest det_time=%-26.24s
 detached device identifier device name device type different directory inode has zero offset and non-zero size: %s divisor '%s' done
 empty long option after -l or --long argument end of the partition in sectors error closing %s error writing . entry error writing .. entry error writing inode error writing root inode error writing superblock error: %s: probing initialization failed error: Nowhere to set up swap on? error: swap area needs to be at least %ld KiB error: uname failed excessively long line arg exec failed
 expected a number, but got '%s' failed failed to add line to output failed to allocate memory: %m failed to execute %s failed to get pid %d's attributes failed to get pid %d's policy failed to initialize blkid filter for '%s' failed to initialize libmount cache failed to initialize libmount iterator failed to initialize output table failed to parse UUID: %s failed to parse number of lines failed to parse pid failed to parse priority failed to read mtab failed to read symlink: %s failed to read timing file %s failed to read typescript file %s failed to set pid %d's policy fifo has non-zero size: %s file %s file inode has zero offset and non-zero size file inode has zero size and non-zero offset file length too short filename length is zero filesystem UUID filesystem label filesystem too big.  Exiting. filesystem type first argument flush buffers fork failed fork() failed, try again later
 from %.*s
 fsname name too long full funky TOY!
 get 32-bit sector count (deprecated, use --getsz) get alignment offset in bytes get blocksize get discard zeroes support status get filesystem readahead get logical block (sector) size get max sectors per request get minimum I/O size get optimal I/O size get physical block (sector) size get read-only get readahead get size in bytes gid group name headswitch ignore -U (UUIDs are unsupported) ignoring given class data for idle class ignoring given class data for none class illegal day value: use 1-%d illegal month value: use 1-12 incomplete write to "%s" (written %zd, expected %zd)
 interleave internal error internal error, contact the author. invalid block-count invalid divisor argument invalid file data offset invalid id invalid key invalid length value specified invalid number of inodes invalid offset value specified invalid option ioctl error on %s ioctl failed: unable to determine device size: %s ioctl(%s) was successful.
 ioctl() to %s to turn off update interrupts failed ioctl() to %s to turn on update interrupts failed unexpectedly ioctl(RTC_EPOCH_READ) to %s failed ioctl(RTC_EPOCH_SET) to %s failed ioprio_get failed ioprio_set failed kernel messages kernel not configured for message queues
 kernel not configured for semaphores
 kernel not configured for shared memory
 key klogctl error klogctl failed label is too long. Truncating it to '%s' last-changed last-op lchown failed: %s little local locked logical sector size login:  lpid lrpid lspid mail system max number of arrays = %d
 max ops per semop call = %d
 max queues system wide = %d
 max semaphores per array = %d
 max semaphores system wide = %d
 messages messages generated internally by syslogd minimum I/O size missing optstring argument mkdir failed: %s mknod failed: %s mode=%#o	access_perms=%#o
 mode=%#o, access_perms=%#o
 mount table full mount: %s does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
 msqid must be root to scan for matching filesystems: %s namelen=%zd

 nattch ncount need terminal for interactive repairs no no filename specified no input file specified no label,  no length argument specified no shell no uuid
 non-block (%ld) bytes non-size (%ld vs %ld) bytes none not a block device or file: %s not enough arguments not enough space allocated for ROM image (%lld allocated, %zu used) not enough space, need at least %llu blocks nsems number of sectors on %.*s
 only root can use "--%s" option openpty failed operation %d, incoming num = %d
 otime = %-26.24s
 owner para partition UUID partition name partition number past first line permission denied permission denied for id permission denied for key perms pid poll() failed process ID rcv_time=%-26.24s
 read count read error on %s read-only device recv removable device reread partition table resource(s) deleted
 response from date command = %s
 root inode is not directory root inode isn't a directory rpm same saved second argument sector count: %d, sector size: %d
 sectors/cylinder sectors/track security/authorization messages seek error seek error on %s seek failed seek failed in bad_zone seek failed in check_blocks seek failed in write_block seek failed in write_super_block select() to %s to wait for clock tick failed semid semnum send send_time=%-26.24s
 set filesystem readahead set read-only set read-write set readahead setgid failed setgid() failed settimeofday() failed setuid() failed shmid size error in symlink: %s size of the device socket socket has non-zero size: %s special file has non-zero offset: %s speed %d unsupported start of the partition in sectors state of the device status succeeded superblock magic not found superblock size (%d) too small symbolic link has zero offset symbolic link has zero size symlink failed: %s terminfo database cannot be found the -l option can be used with one device only -- ignore timings file %s: %lu: unexpected format too many alternate speeds too many arguments too many devices too many inodes - max is 512 total track-to-track seek tracks/cylinder trackskew trouble reading terminfo tty path %s too long ttyname failed uid unable to alloc buffer for superblock unable to alloc new libblkid probe unable to create new selinux context unable to erase bootbits sectors unable to get I/O port access:  the iopl(3) call failed. unable to matchpathcon() unable to read super block unable to relabel %s to %s unable to resolve '%s' unable to rewind swap-device unable to test CRC: old cramfs format unable to write super-block unexpected end of file on %s unknown unknown action: %s unknown column: %s unknown direction '%s' unknown level '%s' unknown option -%s unknown shell after -s or --shell argument unknown user %s unshare failed unsupported command unsupported filesystem features usage: %s [-h] [-v] [-b blksize] [-e edition] [-N endian] [-i file] [-n name] dirname outfile
 -h         print this help
 -v         be verbose
 -E         make all warnings errors (non-zero exit status)
 -b blksize use this blocksize, must equal page size
 -e edition set edition number (part of fsid)
 -N endian  set cramfs endianness (big|little|host), default host
 -i file    insert a file image into the filesystem (requires >= 2.4.0)
 -n name    set name of cramfs filesystem
 -p         pad by %d bytes for boot code
 -s         sort directory entries (old option, ignored)
 -z         make explicit holes (requires >= 2.3.39)
 dirname    root of the filesystem to be compressed
 outfile    output file
 used arrays = %d
 used headers = %d
 used-bytes user ID user name utime failed: %s value version volume name too long waitpid failed warning: %s is misaligned warning: file sizes truncated to %luMB (minus 1 byte). warning: files were skipped due to errors. warning: truncating swap area to %llu KiB we have read epoch %ld from %s with RTC_EPOCH_READ ioctl.
 where the device is mounted who are you? write write failed: %s write to stdout failed yes zcount zero file count Project-Id-Version: util-linux 2.11b
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-11-02 11:43+0100
PO-Revision-Date: 2013-10-11 00:57+0000
Last-Translator: Neliton Pereira Jr. <nelitonpjr@gmail.com>
Language-Team: Brazilian Portuguese <ldp-br@bazar.conectiva.com.br>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 16:06+0000
X-Generator: Launchpad (build 18115)
 	UTC: %s
 	tv.tv_sec = %ld, tv.tv_usec = %ld
 	tz.tz_minuteswest = %d
 
%6d arquivos regulares
%6d diretórios
%6d arquivos de dispositivos de caracteres
%6d arquivos de dispositivos de blocos
%6d vínculos
%6d vínculos simbólicos
------
%6d arquivos
 
%6ld inodes usados (%ld%%)
 
*** %s: diretório ***

 
******** %s: Não é um arquivo de texto ********

 
***Voltar***

 
...Pulando  
Colunas disponíveis (para --show, --raw ou --pairs):
 
Colunas disponíveis:
 
Funções:
 
Nomes de <ldisc> conhecidos:
 
Login incorreto
 
msqid da fila de mensagens: %d
 
A maioria dos comandos opcionalmente são precedidos por um argumento inteiro k.
O padrão é colchetes. Asterisco (*) indica o argumento novo padrão.
 
Opções:
 
Opções:
 -c, --check               verifica blocos defeituosos antes de criar área swap
 -f, --force               permite tamanho da área de swap maior que dispositivo
 -p, --pagesize SIZE       especifica o tamanho da página em bytes
 -L, --label LABEL         especifica rótulo
 -v, --swapversion NUM     especifica o número da versão do espaço de swap
 -U, --uuid UUID           especifica a uuid a ser utilizada
 -V, --version             gera saída de informações da versão e sai
 -h, --help                exibe esta ajuda e sai

 
Padrão não encontrado
 
Script concluído em %s 
semid de vetor de semáforos=%d
 
Memória compartilhada: shmid de segmento = %d
 
Uso:
 
Uso:
 %1$s -V
 %1$s --report [dispositivos]
 %1$s [-v|-q] comandos dispositivos

Comandos disponíveis:
 
Uso:
 %s [opções]
 
Uso:
 %s [opções] dispositivo [tamanho]
         (tabela de partição %s detectada).          (compilado sem libblkid).    Estouro
   b          Alterna a opção de inicialização da partição atual   d          Exclui a partição atual   h          Mostra esta tela   n          Cria uma nova partição a partir do espaço livre   q          Sai do programa sem gravar a tabela de partição  %-25s obter tamanho em setor de 512-bytes
  %s [opções] <nome do arquivo>
  %s [opções] [arquivo]
  Remover  badsect  ecc  removable "%s" linha %d $TERM não está definido. %6.2f%% (%+ld bytes)	%s
 %6ld zonas usadas (%ld%%)
 %ld blocos
 %ld inodes
 %s %d %s %s: o status é %x, não deveria acontecer nunca. cache de %s: %s não possui funções de interrupção.  %s falhou.
 %s de %s
 %s tem mensagens desabilitadas %s tem mensagens desabilitadas em %s %s é um ponto de montagem
 %s está limpo; sem verificação.
 %s está montado.	  %s está montado; não criará um sistema de arquivos aqui! %s não é um dispositivo de blocos especial %s não é um ponto de montagem
 %s não é uma linha serial %s não está conectado em %s %s necessita de um argumento o status de %s é %d %s obteve sucesso.
 %s não aceita argumentos que não sejam de opção. Você forneceu %d.
 %s usando IRQ %d
 %s usando consulta (polling)
 %s face, %d trilhas, %d setores/trilha. Capacidade total de %d KB.
 %s: OK
 %s: Arquitetura não reconhecida %s: falha ao adicionar partição #%d %s: presumindo que RTC usa UTC ...
 %s: blocos defeituosos antes da área de dados: não foi possível fazer fs %s: diretório inválido: '.' não é o primeiro
 %s: diretório inválido: '..' não é o segundo
 %s: diretório inválido: tamanho < 32 %s: não foi possível executar %s: %m %s: não foi possivel abrir arquivo de blocos defeituosos %s: não foi possível acompanhar inotify (limite de acompanhamentos de inotify foi alcançado). %s: não foi possível adicionar monitoramento de inotify. %s: não foi possível ler arquivo de blocos defeituosos %s: não foi possível ler eventos de inotify %s: mudança de diretório falhou %s: dispositivo está desalinhado %s: problema de dup: %m %s: erro ao adicionar partição %d %s: erro ao adicionar partições %d-%d %s: excedido o limite de links simbólicos %s falha ao ler link %s: falhou ao ler a tabela de partições %s: falha ao buscar UUID de swap %s: falha ao procurar rótulo do swap  %s: falha ao gravar o UUID %s: falhou ao escrever o rótulo %s: falha ao obter tamanho %s: entrada excedida %s: permissões inseguras %04o, sugere-se %04o. %s: não é um diretório %s: last_page 0x%08llx é maior do que o tamanho atual do espaço de swap %s: lseek falhou %s: dispositivo inexistente (a opção "nofail" do fstab pode ser usada para ignorar o dispositivo)
 %s: não é um dispositivo de bloco %s: não é um diretório %s: não é partição de swap válida %s: não há suficientes blocos bons %s: não encontrado %s: não está aberto para leitura/gravação %s: número de blocos é muito pequeno %s: partição #%d adicionado
 %s: partição #%d removida
 %s: falha ao ler cabeçalho de swap %s: leitura: %m %s: reinicializando a swap. %s: falha na busca durante teste de blocos %s: busca falhou em check_blocks %s: busca falhou em write_block %s: busca falhou em write_tables %s: pulando - ele parece ter buracos. %s: pulando dispositivo não existente
 %s: dados de suspensão de software detectado. Reescrevendo a assinatura da swap. %s: tamanho de página do formato de swap não confere. %s: tamanho de página do formato de swap não confere. (Use --fixpgsz para reinicializá-lo) %s: swapoff falhou %s: swapon falhou %s: número excessivo de blocos defeituosos %s: não foi possível limpar setor de boot %s: não foi possível obter rótulo de arquivo selinux %s: não foi possível detectar dispositivo %s: não foi possível gravar mapa de inode %s: não foi possível gravar inodes %s: não foi possível gravar página de assintaura %s: não foi possível gravar superbloco %s: não foi possível gravar mapa de zona %s nome de dispositivo desconhecido %s: versão '%s' de swap não suportada %s: aviso: não apague setores bootbits %s: falha de gravação em write_block %s: assinatura de gravação falhou (EOF) (Próximo arquivo:  (Próximo arquivo: %s) (esperando)  , ocupado , erro , on-line , sem papel , pronto -- linha já descarregada ----- Filas de mensagens ------
 --- PIDs de filas de mensagens ---
 Horários de envio/recepção/alteração de filas de mensagens
 ------ Arrays de semáforos ------
 ----- Criadores/donos de arrays de semáforos -----
 ------ Limites de semáforo ------
 -- Horários de alteração/operação de semáforo --
 ----- Status de semáforo -------
 Horários de anexação/destacamento/alteração da memória compartilhada
 - Limites da memória compartilhada -
 Criadores/donos de segmentos de memória compartilhada
 - Segmentos da memória compartilhada -
 - Status da memória compartilhada -
 -------      ------- ----------------------------------
O SISTEMA DE ARQUIVOS FOI ALTERADO
----------------------------------
 --Mais-- argumento para --date muito longo --esperando-- (pass %d)
 ...Pulando de volta para arquivo  ...Pulando para arquivo  ... tique do relógio obtido
 ...pulando
 ...pulando para trás
 ...pulando para frente
 ... sincronização falhou
 /dev/%s: não foi possível abrir como entrada padrão: %m /dev/%s não é um dispositivo de caracteres : !comando não permitido no modo rflag.
 <espaço>                Exibe as próximas k linhas de texto [espaço da tela]
z                       Exibe as próximas k linhas de texto [espaço da tela]*
<enter>                 Exibe as próximas l linhas de texto [1]*
d ou ctrl-D             Desce k linhas [tamanho de scroll atual, inicia em 11]*
q ou Q ou <interrupção> Sai do more
s                       Pula k linhas de texto [1]
f                       Pula k telas de texto [1]
b ou ctrl-B             Pula para trás k telas de texto [1]
'                       Vai para o lugar onde a pesquisa anterior se iniciou
=                       Exibe o número de linha atual
/<expressão regular>    Pesquisa pela k-ésima ocorrência da expressão [1]
n                       Pesquisa pela k-ésima ocorrência da última exp. [1]
!<cmd> ou :!<cmd>       Executa <cmd> em um subshell
v                       Inicia o /usr/bin/vi na linha atual
ctrl-L                  Redesenha a tela
:n                      Vai para o k-ésima próximo arquivo [1]
:p                      Vai para o k-ésima arquivo anterior [1]
:f                      Exibe o número da linha e o nome do arquivo atual
.                       Repete o comando anterior
 AIEEE, bloco "compressed" para > 2 * tamanho do bloco (%ld)
 AIX AIX inicializável AST SmartSleep Cilindros Alternativos Amoeba Amoeba BBT Arquitetura: Pressupondo que o relógio de hardware é mantido na hora %s.
 BBT houve falha do BLKGETSIZE ioctl em %s BSD/OS sist. arq. BSDI BSDI swap sist. arq. BeOS Bloco %d no arquivo "%s" está marcado como não usado. O bloco já foi usado. Agora no arquivo "%s". TamBloco: %d
 BogoMIPS: Inicializar Assist. Inici. escondido BootIt Iniciali. CP/M CP/M / CTOS / ... CPU MHz: Família da CPU: Modo(s) operacional da CPU: CPU(s): CRC: %x
 Chamando settimeofday:
 Caps Lock ligado Alterando informações de finger de %s.
 Alterando o shell para o usuário %s.
 Verificando todos os sistemas de arquivos.
 Limpar Relógio não alterado - apenas testando.
 Comando      Significado Comando (m para ajuda):  Diagnóstico Compaq Núcleo(s) por soquete: Correto Não foi possível abrir o arquivo com os parâmetros de ajuste do relógio (%s) para gravação Não foi possível atualizar o arquivo com os parâmetros de ajuste do relógio (%s) Não foi possível inicializar o PAM: %s Cria nova partição a partir do espaço livre horário atual do sistema: %ld = %s
 Cilindros DISCAGEM EM %s POR %s DOS R/O Acesso DOS DOS secundário DRDOS/sec (FAT12) DRDOS/sec (FAT16<32M) DRDOS/sec (FAT16) Darwin UFS inic. Darwin Excluir Exclui a partição atual Utilitário Dell Dispositivo Dispositivo: %s
 Dados do diretório: %zd bytes
 Multi-Boot DiskSecure Você realmente deseja continuar? Dupla Seta p/ baixo
             Move o cursor para a próxima partição EFI (FAT-12/16/32) EZ-Drive Todos ou nenhum dos tipos de sistema de arquivo passados por -t devem
ter prefixo 'no' ou '!'. Vazia Fim Tudo: %zd kilobytes
 Comando avançado (m para ajuda):  Estendida Setores extras por cilindro SESSÃO DE LOGIN FALHOU: A PARTIR DE %s PARA %s, %s FAT12 FAT16 FAT16 < 32M FATAL: %s não é um terminal FATAL: tty inválido FSname: <%-6s>
 Falha ao definir a personalidade de %s O sistema de arquivos em %s está sujo: precisa de verificação
 Estado do sistema de arquivos = %d
 As informações de finger *NÃO* foram alteradas. Tente novamente mais tarde.
 As informações de finger foram alteradas.
 Informações de finger não alteradas.
 Finalizado com %s (status de saída %d)
 Primeiro %s Opções Forçando verificação do sistema de arquivos em %s.
 Formatando ...  FreeBSD GNU HURD ou SysV GPT Gerado um UUID aleatório: %s
 Gerado um UUID de tempo: %s
 Geometria Golden Bow HFS / HFS+ HPFS/NTFS/exFAT O relógio de hardware está na hora %s
 Cabeças Ajuda FAT12 Escondida FAT16 Escondida FAT16 Escondida < 32M HPFS ou NTFS Escondida WinRE de NTFS oculto FAT16 W95 Esc. (LBA) FAT32 W95 Escondida FAT32 W95 Esc. (LBA) Dica: %s

 Telefone residencial Hora do relógio de hardware: %4d/%.2d/%.2d %.2d:%.2d:%.2d = %ld segundos desde 1969
 Fabricante do hypervisor: Eu não conseguir a permissão porque eu não tentei. Hibernação IBM Thinkpad Nome Id

 Incluindo: %s
 Inode %d marcado como não utilizado, mas usado pelo arquivo "%s"
 Modo Inode %lu não foi limpo. Inode %lu não-utilizado, marcado como utilizado no bitmap. Inode %lu utilizado, marcado como utilizado no bitmap. Inode fim: %d, Dados fim: %d
 Linha de entrada muito longa. Fator de "Interleave" Erro interno: tentando gravar bloco defeituoso
Solicitação de gravação ignorada
 Operação inválida: %d
 Nome de usuário inválido "%s" em %s:%d. Abortando. Valores inválidos no relógio de hardware: %4d/%.2d/%.2d %.2d:%.2d:%.2d
 /proc está montado? Emitindo comando date: %s
 O kernel está pressupondo um valor de epoch de %lu
 LANstep LOGIN EM %s POR %s LOGIN EM %s POR %s A PARTIR DE %s erro LPGETIRQ erro de LPGETSTATUS Rótulo foi truncado. Última calibração feita %ld segundos após 1969
 Último ajuste de variação feito %ld segundos após 1969
 Último login: %.*s  Linha longa demais Linux Linux LVM Linux RAID Estendida Linux Linux nativa Linux texto simples Linux RAID auto-detecção Linux swap Linux swap / Solaris inic. Linux/PA-RISC Lista de UUIDs:
 Fazendo login com home = "/".
 Login incorreto

 Marca em uso Mensagem de %s@%s (como %s) em %s em %s ... Mensagem de %s@%s em %s em %s ... ID de fila de mensagens: %d
 Linux antigo/Minix Modelo: Deve ser superusuário para definir o relógio do sistema. DOS NEC Conjunto de volumes NTFS nome de usuário NULO em %s:%d. Abortando. Nó(s) de NUMA: Nome NeXTSTEP O ajuste necessário é menor do que um segundo, portanto o relógio não será ajustado.
 NetBSD Nova Novo início dos dados Novo shell Nenhuma opção --date especificada. Nenhum próximo arquivo Não há comando anterior para ser substituído Nenhum arquivo anterior Nenhum texto de pesquisa guardado Não foi encontrada uma interface de relógio utilizável.
 Dados Não-FS O fator de variação não está sendo ajustado porque a data de sua última caliração é zero,
então o histórico é inválido e é necessário reiniciar a calibração.
 O fator de variação não está sendo ajustado porque o relógio de hardware continha lixo anteriormente.
 Argumentos insuficientes Não definido O relógio do sistema não está sendo ajustado porque estava executando em modo de teste.
 Não é superusuário. O arquivo adjtime não está sendo atualizado: modo de teste.
 Nota: Todos os comandos podem ser digitados em letras maiúsculas ou Novell Netware 286 Novell Netware 386 Num lock desativada Num lock ativada Número de cilindros alternativos Número de cilindros Número de cabeças Número de cilindros físicos Número de setores OPUS Gerenc. inici. OS/2 Escritório Telefone comercial Minix antigo DM OnTrack DM6 OnTrack Aux1 DM6 OnTrack Aux3 DM6 OnTrack Há suporte apenas a 1K blocos/zonas OpenBSD PC/IX Boot PReP PPC Número da partição PartitionMagic recuperação Senha:  Padrão não encontrado Cilindros físicos Plan 9 Edisk Priam Mostra tela de ajuda Provavelmente são necessários privilégios de root.
 QNX4.x QNX4.x 2ª parte QNX4.x 3ª parte Sair Sai do programa sem gravar a tabela de partição Erro RE:  RO    RA   SSZ   BSZ   StartSec            Size   Device
 Imagem ROM mapa de imagem de ROM Houve falha na gravação de imagem ROM (%zd %zd) LOGIN COMO ROOT EM %s LOGIN COMO ROOT EM %s A PARTIR DE %s Erro de leitura: bloco defeituoso no arquivo '%s'
 Erro de leitura: não foi possível procurar bloco no arquivo '%s'
 Ler:  Remover bloco Velocidade de rotação (RPM) Relógio BCD Ruffian
 prioridade mín/máx de SCHED_%s	: %d/%d
 não há suporte a SCHED_%s?
 SFS bsd SGI efs SGI lvol SGI raw SGI rlvol SGI secrepl SGI sysv SGI trkrepl SGI volhdr SGI volume SGI xfs SGI SGI xfslog SGI xlv SGI xvm Script concluído, o arquivo é %s
 Script iniciado em %s Script iniciado, o arquivo é %s
 Scroll Lock ligado Setores Setores/trilha ID do semáforo: %d
 Configurar Configurar i_nlinks para contagem Configurando o relógio de hardware para %.2d:%.2d:%.2d = %ld segundos desde 1969
 ID de memória compartilhada: %d
 Shell alterado.
 Uma Soquete(s): Solaris inicialização do Solaris SpeedStor Step: setores alt. SunOS cachefs SunOS home SunOS reservado SunOS root SunOS stand SunOS swap SunOS usr SunOS var SunOS Superbloco: %zd bytes
 Alternando em %s.
 Syrinx TIOCSCTTY falhou: %m O relógio do hardware não contém um horário válido, por isto não podemos ajustá-lo. O relógio do hardware não contém um horário válido, então não podemos definir o Horário do Sistema a partir dele. O registro do relógio do hardware contém valores que são inválidos (p. ex., 50º dia do mês) ou além do intervalo que podemos manipular (p. ex., ano 2095) O comando date emitido por %s retornou um valor diferente de um inteiro onde o valor de hora convertido era esperado.
O comando foi:
  %s
A resposta foi:
  %s
 O diretório '%s' contém um número de inode inválido para o arquivo '%.*s'. O arquivo "%s" tem modo %05o
 Thread(s) per núcleo Hora lida do relógio de Hardware: %4d/%.2d/%.2d %02d:%02d:%02d
 Para definir o valor da época, você deve usar a opção 'epoch' para dizer qual valor definir. Alterna a opção de inicialização da partição atual Utilize `%s --help' para mais informações.
 Tipo UTC Não foi possível alocar buffer para contagem de inodes Não foi possível alocar buffer para mapa de inodes Não foi possível alocar buffer para inodes Não foi possível alocar buffer para contagem de zonas Não foi possível alocar buffer para mapa de zona Não foi possível conectar ao sistema de áudio Não foi possível obter o valor da época do kernel. Não foi possível ler mapa de inodes Não foi possível ler inodes Não foi possível ler mapa de zonas Não foi possível executar o programa "date" no shell /bin/sh: popen() falhou Não é possível ajustar o relógio do sistema.
 Não é possível configurar o valor de epoch no kernel.
 Não foi possível configurar espaço de swap: ilegível Não foi possível gravar mapa de inode Não foi possível gravar inodes Não foi possível gravar mapa de zona Não assinada Desconhecido Comando desconhecido: %s Contexto de usuário desconhecido Desmarcar Seta p/ cima Move o cursor para a partição anterior Uso: %s [opções] [arquivo ...]
 Uso: %s [opções] dispositivo [contagem-bloco]
 Usando horário UTC.
 Usando horário local.
 Usando tamanho de paginação informado pelo usuário de %d no lugar do valor de sistema %d VMware VMFS VMware VMKCORE ID de fornecedor: Venix 80286 Verificando ...  Tipo de virtualização: Virtualização: Volume: <%-6s>
 Win95 (LBA) Partição Extendida FAT16 W95 (LBA) FAT32 W95 W95 FAT32 (LBA) AVISO: números de dispositivo truncados em %u bits. Isto quase certamente
significa que alguns arquivos de dispositivo ficarão incorretos. Aguardando tique do relógio...
 Aguardando em loop até que a hora de %s mude.
 Aviso... %s para o dispositivo %s encerrou com sinal %d. Aviso: Firstzone != Norm_firstzone
 Aviso: contagem de inodes grande demais.
 Aviso: terceira linha não reconhecida no arquivo adjtime
(Esperado: `UTC' ou `LOCAL' ou nada). Valores estranhos em do_check: provavelmente erros
 Disco inteiro Teria gravado o seguinte em %s:
%s Você gostaria de editar %s agora [s/n]?  Gravar Erro de gravação: bloco defeituoso no arquivo "%s"
 Grava tabela de partição no disco (isto poderá destruir dados) root XENIX usr XENIX Você está usando grupos sombra (shadow) neste sistema.
 Você está usando senhas sombra (shadow) neste sistema.
 Você tem mensagens.
 Você tem novas mensagens.
 Zona %lu: em uso, contados=%d
 Zona %lu: marcada como em uso, nenhum arquivo a usa. Zona %lu: não em uso, contados=%d
 Número de zona < FIRSTZONE (primeira zona) no arquivo `%s'. Número de zona maior ou igual a ZONES no arquivo "%s". Tamanho da zona = %d
 [Não é um arquivo] linha %d [Pressione "h" para obter instruções.] [Pressione espaço para continuar, "q" para sair.] [Use q ou Q para sair] ação deve ser realizada imediatamente alarme %ld, horário_sist %ld, horário_rtc %ld, segundos %u
 alarme: ligado %s todas as opções de montagem filas alocadas = %d
 semáforos alocados: %d
 ID já removido chave já removida %s atômico falhou para 1000 iterações! att_time=%-26.24s
 anexado argumentos inválidos falha na conversão de caractere %%%s tamanho de nome de arquivo inválido posição de inode inválida tamanho de inode inválido número mágico inválido no superbloco comprimento de resposta inválido posição raiz inválida (%lu) campo s_imap_blocks inválido no superbloco campo s_zmap_blocks inválido no superbloco velocidade inválida: %s valor de tempo limite inválido: %s tamanho de inode v2 inválido erro de entrada de número de blocos defeituosos na linha %d
 grande tamanho de bloco menor do que o tamanho do setor físico de %s argumento de bloco muito grande, máximo é %llu modo inválido: %s (%o) inicialização do MILO
 bytes bytes/setor não foi possível realizar fork
 não foi possível abrir o arquivo temporário não foi possível ler %s não foi possível acessar o arquivo %s não foi possível checar %s: fsck.%s não encontrado não foi possível fechar o arquivo %s não foi possível executar como daemon não foi possível determinar o tamanho de %s não foi possível localizar o dispositivo para %s não foi possível realizar fork não foi possível obter o tamanho de %s não foi possível obter os atributos para %s não foi possível obter a lista de tarefas não foi possível abrir %s não foi possível definir a disciplina da linha não foi possível definir os atributos do terminal para %s não foi possível obter status de %s não foi possível gravar %s cgid alteração change_time=%-26.24s
 alterado verificação abortada.
 chown falhou: %s porta do relógio ajustada para 0x%x
 conectar não foi possível obter tamanho do dispositivo não foi possível ler o diretório %s não foi possível computar o contexto selinux Não foi possível executar shell script não foi possível encontrar sistemas de arquivos correspondentes: %s cpid cpuset_alloc falhou significatividade cramfs é %s
 erro de crc criação de fila de mensagem falhou criação de semáforo falhou criação de memória compartilhada falhou condições críticas ctime = %-26.24s
 cuid cilindros cylinderskew bloco de dados muito grande String de date %s equivale a %ld segundos desde 1969.
 dest det_time=%-26.24s
 destacado identificador do dispositivo nome do dispositivo tipo de dispositivo diferente inode de diretório possui posição zero com tamanho não zero: %s divisor "%s" concluído
 opção longa vazia após argumento -l ou --long final da partição em setores erro ao fechar %s erro de gravação de entrada . erro de gravação de entrada .. erro de gravação de inode erro de gravação de inode de root erro de gravação de superbloco erro: %s: inicialização da detecção falhou erro: Não há lugar onde configurar o swap? erro: a área de swap deve ter pelo menos %ld KiB erro: uname falhou linha de argumentos longa demais exec falhou
 esperava um número, mas obteve "%s" falhou falha ao adicionar para saída falhou ao alocar memória: %m falha ao executar %s falha ao obter atributos do pid %d falha ao obter a política do pid %d falha ao inicializar filtro blkid para '%s' falha ao inicializar o cache do libmount falha ao inicializar o iterador libmount falha ao inicializar tabela de saída falha ao analisar UUID: %s falhou ao analisar o número de linhas falha ao analisar o pid falha ao analisar prioridade falha ao ler mtab falha ao ler link simbólico: %s falha ao ler do arquivo de tempo %s falha ao ler o arquivo de script-gravado %s falha ao configurar a política do pid %d fifo possui tamanho não zero: %s arquivo %s inode de arquivo possui posição zero e tamanho não zero inode de arquivo possui tamanho zero e posição não zero tamanho de arquivo muito pequeno tamanho de nome de arquivo é zero UUID do sistema de arquivo Rótulo do sistema de arquivo sistema de arquivos muito grande. Saindo. tipo do sistema de arquivos primeiro argumento descarrega buffers o fork falhou fork() falhou, tente novamente mais tarde
 a partir de %.*s
 nome de fsname muito longo completo funky TOY!
 obter contagem de setor de 32-bits (obsoleto, use --getsz) obter o deslocamento de alinhamento em bytes obter tamanho do bloco obter o status do suporte a descarte de zeros obtém readahead de sistema de arquivos obtém tamanho de bloco lógico (setor) obtém máximo de setores por requisição obtém tamanho de E/S mínimo obtém tamanho de E/S ótimo obtém tamanho de bloco físico (setor) obtém somente leitura obtém readahead obtém tamanho em bytes gid nome do grupo headswitch ignore -U (UUIDs não são suportados) ignorando dados de classe dados para classe "idle" ignorando dados de classe dados para classe "none" valor de dia inválido: use 1-%d valor de mês inválido: use 1-12 escriva incompleta para "%s" (escreveu %zd, esperava-se %zd)
 interleave erro interno erro interno, entre em contato com o autor. contagem de bloco inválida argumento divisor inválido posição de dados de arquivo inválida id inválido chave inválida valor inválido de comprimento especificado número inválido de inodes valor inválido de posição especificado opção inválida erro de ioctl em %s ioctl falhou: não foi possível determinar tamanho do dispositivo: %s ioctl(%s) obteve sucesso.
 ioctl() para %s para desligar interrupções de atualização falhou ioctl() para %s para ligar interrupções de atualização falhou inesperadamente ioctl(RTC_EPOCH_READ) para %s falhou ioctl(RTC_EPOCH_SET) para %s falhou falha no ioprio_get falha no ioprio_set mensagens do kernel o kernel não está configurado para fila de mensagens
 o kernel não está configurado para semáforos
 o kernel não está configurado para memória compartilhada
 chave erro klogctl klogctl falhou rótulo é muito longo. Truncando-o em '%s' última-alteração última-op lchown falhou: %s pequeno local bloqueado tamanho do setor lógico login:  lpid lrpid lspid sistema de e-mail número máximo de vetores: %d
 máximo de ops por chamada semop: %d
 número máximo geral de filas no sistema: %d
 máximo de semáforos por vetor: %d
 máximo geral de semáforos do sistema: %d
 mensagens mensagens geradas internamente pelo syslogd tamanho mínimo I/O faltando o argumento texto-opções mkdir falhou: %s mknod falhou: %s mode=%#o	access_perms=%#o
 modo=%#o, access_perms=%#o
 tabela de montagem cheia mount: %s não contém rótulos SELinux.
       Você acabou de montar um sistema de arquivos que tem suporte a rótulos,
       mas que não contém rótulos, em uma máquina com SELinux. É bem possível
       que aplicativos confinados venham a gerar mensagens AVC e não tenham
       permissão para acessar este sistema de arquivos. Para mais detalhes,
       veja restorecon(8) e mount(8).
 msqid você precisa ser permissão administrativa para varrer por sistemas de arquivos correspondentes: %s namelen=%zd

 nattch ncount é necessário um terminal para reparos interativos não nenhum nome de arquivo especificado nenhum arquivo de entrada especificado nenhum rótulo,  nenhum argumento de comprimento especificado nenhum shell nenhum uuid
 não bloco (%ld) bytes não tamanho (%ld vs %ld) bytes nenhum não é um dispositivo de blocos ou arquivo: %s argumentos não suficientes não há espaço suficiente alocado para imagem ROM (%lld alocado, %zu usado) espaço insuficiente, é necessário pelo menos %llu blocos nsems número de setores em: %.*s
 apenas o root pode usar a opção "--%s" openpty falhou operação %d, número recebido = %d
 otime = %-26.24s
 proprietário para UUID da partição nome da partição número da partição além da primeira linha permissão negada permissão negada para o ID permissão negada para tecla perms pid poll() falhou ID do processo rcv_time=%-26.24s
 quantidade de leitura erro de leitura em %s dispositivo somente-leitura recebimento dispositivo removível lê novamente tabela de partição recurso(s) excluído
 resposta do comando date = %s
 inode raiz não é um diretório inode raiz não é um diretório RPM igual salvo segundo argumento contagem de setor: %d, tamanho de setor: %d
 setores/cilindro setores/trilha mensagens de segurança/autorização erro de busca erro de busca em %s busca falhou busca falhou em bad_zone busca falhou em check_blocks busca falhou em write_block busca falhou em write_super_block select() para %s para aguardar tique do relógio falhou semid semnum envio send_time=%-26.24s
 configura readahead de sistema de arquivos configura somente leitura configura leitura e gravação configura readahead setgid falhou setgid() falhou settimeofday() falhou setuid() falhou shmid erro de tamanho no link simbólico: %s tamanho do dispositivo socket socket possui tamanho não zero: %s arquivo especial possui posição não zero: %s sem suporte à velocidade %d início da partição em setores estado do dispositivo status bem-sucedido mágica de superbloco não encontrada tamanho do superbloco (%d) é muito pequeno link simbólico possui posição zero link simbólico possui tamanho zero link simbólico falhou: %s banco de dados terminfo não pode ser encontrado a opção -l pode ser usada com apenas um dispositivo -- ignorar arquivo de tempo %s: %lu: formato inesperado número excessivo de velocidades alternativas número excessivo de argumentos número excessivo de dispositivos número excessivo de inodes - máximo é 512 total busca trilha a trilha trilhas/cilindro trackskew problemas na leitura de terminfo caminho tty %s muito longo ttyname falhou uid não foi possível alocar buffer para superbloco não foi possível alocar nova examinação com libblkid não foi possível gravar novo contexto selinux não foi possível apagar setores de bits de inicialização não foi possível obter o endereço da porta de E/S: falha na chamada iopl(3). não foi possível fazer matchpathcon() não foi possível ler superbloco não foi possível alterar rótulo de %s para %s não foi possível resolver "%s" não foi possível rebobinar o dispositivo de swap não foi possível testar CRC: formato de cramfs antigo não foi possível gravar superbloco fim de arquivo inesperado em %s desconhecida ação desconhecida: %s coluna desconhecida: %s direção desconhecida '%s' nível '%s' desconhecido opção -%s desconhecida shell desconhecido após argumento -s ou --shell usuário %s inválido unshare falhou comando não suportado Sem suporte a recursos do sistema de arquivos uso: %s [-h] [-v] [-b tam-blk] [-e edição] [-N endian] [-i arquivo] [-n nome] nome-dir arq-saída
 -h         exibe esta ajuda
 -v         mensagens detalhadas
 -E         torna todos os avisos erros (status de saída não zero)
 -b tam-blk usa este tamanho de bloco; deve ser igual em tamanho de páginas
 -e edição  define número de edição (parte de fsid)
 -N endian  define nível de endian do cramfs (big|little|host), padrão é host
 -i arquivo insere uma imagem de arquivo no sistema de arquivos
            (requer >= 2.4.0)
 -n nome    define o nome do sistema de arquivos cramfs
 -p         preenche em %d bytes para código de inicialização
 -s         ordena registros do diretório (opção antiga, ignorada)
 -z         torna buracos explícitos (requer >= 2.3.39)
 nome-dir   raiz do sistema de arquivos a ser comprimido
 arq-saída  arquivo de saída
 arrays usados: %d
 cabeçalhos usados: %d
 bytes usados ID do usuário nome de usuário utime falhou: %s valor versão nome de volume muito longo waitpid falhou aviso: %s está desalinhado aviso: tamanho de arquivos truncados em %luMB (menos 1 byte). aviso: arquivos foram ignorados devido a erros. aviso: truncando a área de swap para %llu KiB lemos epoch %ld de %s com ioctl RTC_EPOCH_READ.
 onde o dispositivo está montado quem é você? gravar gravação falhou: %s gravação para a saída padrão falhou sim zcount contagem de arquivo zero 