��    	     d  �  �       �+  ~   �+  X    ,  H   y,     �,     �,     �,  &   �,  &   -  %   D-  $   j-  =   �-  '   �-     �-     .     .  �   +.  :   �.     �.     �.     �.     /     /  
   %/     0/     9/     F/  	   M/     W/  $   p/     �/     �/     �/     �/     �/  &   0  <   90     v0     �0     �0      �0  	   �0  2   �0  2   1     A1      I1  #   j1     �1     �1     �1  O   �1     2     12     =2     E2     N2     U2     g2     {2     �2     �2     �2     �2  "   �2     3  D   3  �   Z3  7   4  C   T4  3   �4     �4     �4     �4  f   5  (   �5      �5     �5  6   �5      6     <6     O6     f6     u6     |6     �6     �6     �6     �6     �6  	   �6     �6     �6     7     7     7     $7     47     :7     C7     c7     }7  #   �7  #   �7  +   �7     8     8     8     %8  
   28     =8     S8     e8     �8     �8     �8     �8     �8     �8     �8     �8      9     9  #   79     [9     y9     �9     �9     �9     �9     �9     �9  	   �9      :     :     1:     A:     E:     Y:     l:     t:     �:     �:     �:     �:     �:     �:     �:  	   ;     ;     2;     I;     b;     t;  
   �;      �;  !   �;     �;     �;  
   <     !<  :   5<  $   p<     �<     �<     �<     �<  $   �<     �<     �<     =     !=     7=  +   L=  "   x=     �=  1   �=     �=     >     &>     D>  �   [>     =?     \?      {?      �?     �?     �?     �?     �?  
   @     @     -@     J@     Z@  8   r@     �@     �@     �@     �@     �@     A     A     *A     0A     =A  	   IA  5   SA  %   �A  %   �A      �A     �A     B  $    B     EB     TB     bB     oB     }B  (   �B     �B     �B     �B     �B  �   C     �C  4   �C     �C     �C     �C     �C     D     D     1D  %   OD     uD     �D     �D     �D     �D     �D     E  $   'E     LE     PE     XE  �   pE  �   XF  �   EG     6H     FH     TH  )   jH     �H  (   �H     �H     �H     �H     �H  	   I  	   $I     .I     II     aI     gI     }I     �I     �I  /   �I  "   �I     J  ,   J     LJ     YJ      nJ     �J     �J     �J     �J     �J  
   �J  	   �J     �J     �J     �J     �J     K  �   $K  M   �K     �K     L     M     (M     @M     VM     kM     �M  �   �M     'N     AN     ^N     vN     �N     �N  v   �N     O     .O     3O     LO     dO     lO  
   tO     O     �O     �O  "   �O     �O  !   �O      �O  -   P     =P  �   ZP  "   ,Q     OQ     lQ  �   �Q     R     R  $   $R  
   IR  )   TR     ~R     �R     �R     �R     �R     �R     �R     �R     �R     �R     �R     S     S  H   +S  (   tS     �S     �S     �S     �S     �S     �S     �S     �S     �S     T  	   T     "T     )T  	   7T  
   AT     LT     XT     rT     yT     T  �   �T     U  7   U     QU     bU     qU     �U  !   �U     �U     �U     �U     �U     V     V     /V     MV     ]V  4   lV  $   �V     �V  "   �V     �V     W     W     W     ?W  
   FW     QW  ,   kW  +   �W     �W     �W     �W     X     X     $X     AX     JX     ZX     kX  (   �X     �X     �X     �X     �X     Y     Y     4Y     KY     [Y     xY     �Y     �Y  !   �Y  '   �Y  *   Z  -   -Z     [Z     zZ     �Z     �Z     �Z     �Z     �Z     �Z     �Z      [  -   [  9   @[  @   z[  r   �[     .\  A   E\  �   �\     =]     W]     d]     �]     �]     �]     �]  <   �]     �]  "   ^     $^     (^     =^  *   W^     �^     �^     �^     �^     �^     
_  !   _  N   1_     �_     �_  
   `     `     3`     G`     ``     y`     �`     �`     �`     �`     �`     	a     !a  `   )a  %   �a  ,   �a     �a     �a     �a  ,   b  	   >b     Hb  /   Xb  	   �b     �b     �b  -   �b  1   �b  "   &c  (   Ic     rc  �   �c  �   Ld  }   e  4   �e     �e     �e     �e     �e     f     f     !f  5   :f     pf     yf     f     �f  -   �f     �f     �f     �f     �f     �f     g     g     'g  ,   /g     \g  �  og  s   i  k   vi  Y   �i     <j     Yj     ij  $   rj  9   �j  /   �j  ,   k  U   .k  .   �k     �k  (   �k  *   �k  �   l  >   �l  	   �l  
   �l  	   �l  "   �l  $    m     Em     cm     xm     �m     �m     �m  9   �m     n  "   n  *   An  ,   ln     �n  0   �n  M   �n     -o     Eo     \o  )   io  
   �o  :   �o  =   �o     p  0   p  -   Op     }p     �p     �p  S   �p     q      q     -q     3q     9q     Bq     Zq     tq     �q     �q  !   �q  !   �q  '   �q     %r  h   .r  �   �r  V   ts  T   �s  P    t  $   qt  %   �t  (   �t  z   �t  ;   `u  )   �u  1   �u  ]   �u  #   Vv  #   zv  #   �v  #   �v     �v     �v     �v     w  "    w     Cw     \w     hw     tw     {w     �w     �w  	   �w     �w     �w     �w  &   �w  +   x     :x  ,   Sx  2   �x  D   �x     �x  	   y     y     y     .y     7y     Ly  %   ey  !   �y  	   �y     �y     �y     �y     �y  %   �y     !z  -   2z  #   `z  9   �z  )   �z  $   �z     {     {  
   ${  !   /{     Q{     a{     y{     �{     �{     �{     �{     �{     �{     |  "   |     2|     9|     E|     X|     g|     y|     �|  	   �|     �|     �|     �|     �|     }     *}  $   6}  )   [}  "   �}  '   �}     �}     �}  9   �}  )   0~     Z~     a~     h~     |~  1   �~      �~     �~     �~          .  9   J  $   �     �  3   �     �     �      /�     P�  �   g�  $   Q�  #   v�  '   ��  &        �  !   �     #�     9�  
   T�     _�     p�     ��     ��  F   ��     ��     �  "   .�  (   Q�  #   z�     ��     ��  
   ��     ă     ̃     ڃ  8   �  *   $�  &   O�  #   v�      ��     ��  /   ˄     ��     �     �  
   +�     6�  H   S�     ��     ��     ��  "   ԅ  �   ��     ��  A   ��     ��     ��     �     �     7�      G�  !   h�  (   ��     ��  *   ��  "   �     �  #   .�  ,   R�     �  4   ��     ˈ     ψ     ׈  �   �  �   �  �   ��     ؋     �     ��  2   �     F�  #   f�  
   ��     ��     ��     ��     ό     ֌  &   �     �     -�  "   5�     X�  #   f�  !   ��  6   ��  -   �     �  =   1�     o�     |�  /   ��     ��     Ԏ     ݎ     ��     ��     �  
   *�     5�     =�     F�     K�  "   Z�  �   }�  c   $�  !   ��    ��     ��  $   ّ     ��     �  2   7�  '   j�  �   ��      8�  (   Y�     ��     ��     ��     Γ  �   ѓ     U�     b�     h�     ��     ��     ��     ��     ��     є     �  +   �     �      �  "   =�  1   `�     ��  �   ��  -   ��  6   Ӗ     
�  �   )�     ֗     �  )   �  	   �     !�     A�     H�     U�  	   a�     k�     ��     ��     ��     ��     Ø     Ϙ     �     ��  D   �  2   R�     ��  	   ��  "   ��     ��     ��     �     �     �     ��     �  
   *�     5�     >�     L�     X�     e�     s�  	   ��  	   ��     ��  �   ��     >�  D   K�     ��     ��     ��  '   ӛ  ,   ��     (�     6�  #   V�     z�     ��     ��  %   ��     ל     �  D   
�  2   O�  %   ��  "   ��     ˝     ڝ     �  '   �  
   �     &�     6�  <   N�  ?   ��  %   ˞     �     �     '�     =�  .   O�     ~�     ��     ��  #   ��  9   ʟ  (   �  ,   -�     Z�     k�     ��     ��     ��     ڠ     �     �     !�     A�  &   X�  @   �  1   ��  ?   �      2�     S�     d�     ��     ��     ��  *   ��     ۢ     �     �  &   �  <   /�  C   l�  z   ��     +�  R   9�  �   ��     J�     h�  %   �     ��     ��     ʥ     ֥  A   �     -�  /   H�     x�     |�  &   ��  6   ��     ��  $   �  %   2�  *   X�  "   ��     ��  @   ��  q   �     b�  �   v�     ,�  )   <�  	   f�     p�     ��     ��     ̩     �  +   �  '   -�     U�     s�  
   ��  y   ��  /   �  8   B�     {�     ��     ��  2   ��     �     �  6   �  
   S�     ^�  '   z�  6   ��  2   ٬  #   �  )   0�     Z�  �   t�  �   M�  g   3�  A   ��     ݯ     ��     	�     �     �     -�  #   2�  U   V�  
   ��     ��     ��     Ӱ  +   �     �     '�     7�     ?�  	   C�  	   M�     W�     j�  /   p�     ��         �  �        i  -  �   q       �   �          [  �  �     [       )   �   �  7   �   (   �  �      Q   �   �      �  �           �  +   `   n      �  �      �  �     k   �  m   0      l       �  �  �  �   �          �  <   �  4             �   �   �     �  s  �          p          /               �  p  n   �       j  +      �   �     �  a   C   $   H  �   �           �   x      T      �   �          �   �   =   �  b  �          e  �   �  �  �                  i   �          |           }   V           w  �    �  �               �  r  �   �  �   �  W      �   �  w   |  l  �   �  �  1        d       �   #       �  g     �   �               �  �  �  g            �   �      9       �   �   �   �      t         8   0   �                 X      �   �             �       �               �           )  �  ^   �   "    3   �  �  �   �          %  �  A   M  �  ~  �  e   B  ;   �  �   �  �  4               	      �        �  o   *   �    �           �                    _      m  �     �  ~   b       L  �          �  �  �   �      R            T   �   �   �   \      %   �  �   N       �  A  �      �  @      ]   v     (  Z      O  �   h  �   J   O      �  �      �     �   �      �   U   �   =  r   E  �      {  �     &  .     D   {   z   �   �       >  �   �  G   �     �   �   �      �     E   �  ^  _   �          7  H   �  f       �   �         G  c  u      �   �   �   Z       	  	       ,           f   �   �      Q  �   �      ?   �   q  #  �  C            �       $  3  �   �  ]      �   �   �              �   y   �   '     �  �  �   �           
  �              B          x           �   `                 �  "   �  �  �  �  F    F   �  9  �              *  �      @       �    8  ?  �   M   �  \      �        �     s   �  �  <  D      P  �       V  d    �       &       �  S    z  �      �      �       �     �      �   t   W              o  P   �  �   �   ;      }    �  �   �       K  �   J  �   u   �       �   I  �  �         �       :   ,  -           �      L   �              �  5  �       R   �   /  �   �  y  �      �  �          Y  1   �       Y   �      �       �   �  �       �  �   �   k  S   �  �    �   �  �               �       �     K       �  �  �  �   �       !      �   �  �   �       �  �   v      6   >       �      h           
   �  �  �  �  �   5               U                    �   6  2     N  a  .         �   �    �  '       I   2        X  �       �   �                 !      �   :      �       �  j       c    

A good replacement for %s
is placing values for the user.name and
user.email settings into your personal
~/.gitconfig file.
 
* Untracked file clipped here by %s.
* To see the entire file, use an external editor.
 
This is due to a known issue with the
Tcl binary distributed by Cygwin. %s ... %*i of %*i %s (%3i%%) %s Repository %s of %s '%s' is not an acceptable branch name. '%s' is not an acceptable remote name. (Blue denotes repository-local tools) * Binary file (not showing content). * Untracked file is %d bytes.
* Showing only first %d bytes.
 A branch is required for 'Merged Into'. Abort Merge... Abort completed.  Ready. Abort failed. Abort merge?

Aborting the current merge will cause *ALL* uncommitted changes to be lost.

Continue with aborting the current merge? Aborted checkout of '%s' (file level merging is required). Aborting About %s Add Add New Remote Add New Tool Command Add Remote Add Tool Add globally Add... Adding %s Adding resolution for %s Always (Do not perform merge checks) Amend Last Commit Amended Commit Message: Amended Initial Commit Message: Amended Merge Commit Message: Annotation complete. Annotation process is already running. Any unstaged changes will be permanently lost by the revert. Apply/Reverse Hunk Apply/Reverse Line Arbitrary Location: Are you sure you want to run %s? Arguments Ask the user for additional arguments (sets $ARGS) Ask the user to select a revision (sets $REVISION) Author: Blame Copy Only On Changed Files Blame History Context Radius (days) Blame Parent Commit Branch Branch '%s' already exists. Branch '%s' already exists.

It cannot fast-forward to %s.
A merge is required. Branch '%s' does not exist. Branch Name Branch: Branches Browse Browse %s's Files Browse Branch Files Browse Branch Files... Browse Current Branch's Files Busy Calling commit-msg hook... Calling pre-commit hook... Calling prepare-commit-msg hook... Cancel Cannot abort while amending.

You must finish amending this commit.
 Cannot amend while merging.

You are currently in the middle of a merge that has not been fully completed.  You cannot amend the prior commit unless you first abort the current merge activity.
 Cannot determine HEAD.  See console output for details. Cannot fetch branches and objects.  See console output for details. Cannot fetch tags.  See console output for details. Cannot find HEAD commit: Cannot find git in PATH. Cannot find parent commit: Cannot merge while amending.

You must finish amending this commit before starting any type of merge.
 Cannot move to top of working directory: Cannot parse Git version string: Cannot resolve %s as a commit. Cannot resolve deletion or link conflicts using a tool Cannot use bare repository: Cannot write icon: Cannot write shortcut: Case-Sensitive Change Change Font Checked out '%s'. Checkout Checkout After Creation Checkout Branch Checkout... Choose %s Clone Clone Existing Repository Clone Type: Clone failed. Clone... Cloning from %s Close Command: Commit %s appears to be corrupt Commit Message Text Width Commit Message: Commit declined by commit-msg hook. Commit declined by pre-commit hook. Commit declined by prepare-commit-msg hook. Commit failed. Commit: Commit@@noun Commit@@verb Committer: Committing changes... Compress Database Compressing the object database Conflict file does not exist Continue Copied Or Moved Here By: Copy Copy All Copy Commit Copy To Clipboard Copying objects Could not add tool:
%s Could not start ssh-keygen:

%s Could not start the merge tool:

%s Couldn't find git gui in PATH Couldn't find gitk in PATH Counting objects Create Create Branch Create Desktop Icon Create New Branch Create New Repository Create... Created commit %s: %s Creating working directory Current Branch: Cut Database Statistics Decrease Font Size Default Default File Contents Encoding Delete Delete Branch Delete Branch Remotely Delete Branch... Delete Local Branch Delete Only If Delete Only If Merged Into Delete... Deleting branches from %s Destination Repository Detach From Local Branch Diff/Console Font Directory %s already exists. Directory: Disk space used by loose objects Disk space used by packed objects Displaying only %s of %s files. Do Full Copy Detection Do Nothing Do Nothing Else Now Do not know how to initialize repository at location '%s'. Don't show the command output window Done Edit Email Address Encoding Error loading commit data for amend: Error loading diff: Error loading file: Error retrieving versions:
%s Error: Command Failed Explore Working Copy Failed to add remote '%s' of location '%s'. Failed to completely save options: Failed to configure origin Failed to configure simplified git-pull for '%s'. Failed to create repository %s: Failed to delete branches:
%s Failed to open repository %s: Failed to rename '%s'. Failed to set current branch.

This working directory is only partially switched.  We successfully updated your files, but failed to update an internal Git file.

This should not have occurred.  %s will now close and give up. Failed to stage selected hunk. Failed to stage selected line. Failed to unstage selected hunk. Failed to unstage selected line. Failed to update '%s'. Fast Forward Only Fetch Immediately Fetch Tracking Branch Fetch from Fetching %s from %s Fetching new changes from %s Fetching the %s File %s already exists. File %s seems to have unresolved conflicts, still stage? File Browser File Viewer File level merge required. File type changed, not staged File type changed, staged File: Find Text... Find: Font Example Font Family Font Size Force overwrite existing branch (may discard changes) Force resolution to the base version? Force resolution to the other branch? Force resolution to this branch? Found a public key in: %s From Repository Full Copy (Slower, Redundant Backup) Further Action Garbage files Generate Key Generating... Generation failed. Generation succeeded, but no keys found. Git Gui Git Repository Git Repository (subproject) Git directory not found: Git version cannot be determined.

%s claims it is version '%s'.

%s requires at least Git 1.5.0 or later.

Assume '%s' is version 1.5.0?
 Global (All Repositories) Hardlinks are unavailable.  Falling back to copying. Help In File: Include tags Increase Font Size Index Error Initial Commit Message: Initial file checkout failed. Initialize Remote Repository and Push Initializing... Invalid GIT_COMMITTER_IDENT: Invalid date from Git: %s Invalid font specified in %s: Invalid global encoding '%s' Invalid repo encoding '%s' Invalid revision: %s Invalid spell checking configuration KiB LOCAL:
 LOCAL: deleted
REMOTE:
 Last scanned state does not match repository state.

Another Git program has modified this repository since the last scan.  A rescan must be performed before a merge can be performed.

The rescan will be automatically started now.
 Last scanned state does not match repository state.

Another Git program has modified this repository since the last scan.  A rescan must be performed before another commit can be created.

The rescan will be automatically started now.
 Last scanned state does not match repository state.

Another Git program has modified this repository since the last scan.  A rescan must be performed before the current branch can be changed.

The rescan will be automatically started now.
 Linking objects Loading %s... Loading annotation... Loading copy/move tracking annotations... Loading diff of %s... Loading original location annotations... Local Branch Local Branches Local Merge... Location %s already exists. Location: Main Font Match Tracking Branch Name Match Tracking Branches Merge Merge Commit Message: Merge Into %s Merge Verbosity Merge completed successfully. Merge failed.  Conflict resolution is required. Merge strategy '%s' not supported. Merge tool failed. Merge tool is already running, terminate it? Merged Into: Merging %s and %s... Minimum Letters To Blame Copy On Mirroring to %s Missing Modified, not staged Name: New Branch Name Template New Commit New Name: New... Next No No Suggestions No changes to commit. No changes to commit.

No files were modified by this commit and it was not a merge commit.

A rescan will be automatically started now.
 No changes to commit.

You must stage at least 1 file before you can commit.
 No default branch obtained. No differences detected.

%s has no changes.

The modification date of this file was updated by another application, but the content within the file was not changed.

A rescan will be automatically started to find other files which may have the same state. No keys found. No repository selected. No revision selected. No working directory Not a GUI merge tool: '%s' Not a Git repository: %s Note that the diff shows only conflicting changes.

%s will be overwritten.

This operation can be undone only by restarting the merge. Nothing to clone from %s. Number of Diff Context Lines Number of loose objects Number of packed objects Number of packs OK One or more of the merge tests failed because you have not fetched the necessary commits.  Try fetching from %s first. Online Documentation Open Open Existing Repository Open Recent Repository: Open... Options Options... Original File: Originally By: Other Packed objects waiting for pruning Paste Please select a branch to rename. Please select a tracking branch. Please select one or more branches to delete. Please supply a branch name. Please supply a commit message.

A good commit message has the following format:

- First line: Describe in one sentence what you did.
- Second line: Blank
- Remaining lines: Describe why this change is good.
 Please supply a name for the tool. Please supply a remote name. Portions staged for commit Possible environment issues exist.

The following environment variables are probably
going to be ignored by any Git subprocess run
by %s:

 Preferences Prev Prune Tracking Branches During Fetch Prune from Pruning tracking branches deleted from %s Push Push Branches Push to Push... Pushing %s %s to %s Pushing changes to %s Quit REMOTE:
 REMOTE: deleted
LOCAL:
 Reading %s... Ready to commit. Ready. Recent Repositories Recovering deleted branches is difficult.

Delete the selected branches? Recovering lost commits may not be easy. Redo Refresh Refreshing file status... Remote Remote Details Remote: Remove Remove Remote Remove Tool Remove Tool Commands Remove... Rename Rename Branch Rename... Repository Repository: Requires merge resolution Rescan Reset Reset '%s'? Reset changes?

Resetting the changes will cause *ALL* uncommitted changes to be lost.

Continue with resetting the current changes? Reset... Resetting '%s' to '%s' will lose the following commits: Restore Defaults Revert Changes Revert To Base Revert changes in file %s? Revert changes in these %i files? Reverting %s Reverting dictionary to %s. Reverting selected files Revision Revision Expression: Revision To Merge Revision expression is empty. Run Command: %s Run Merge Tool Run only if a diff is selected ($FILENAME not empty) Running %s requires a selected file. Running merge tool... Running thorough copy detection... Running: %s Save Scanning %s... Scanning for modified files ... Select Select All Setting up the %s (at %s) Shared (Fastest, Not Recommended, No Backup) Shared only available for local repository. Show Diffstat After Merge Show History Context Show Less Context Show More Context Show SSH Key Show a dialog before running Sign Off Source Branches Source Location: Spell Checker Failed Spell checker silently failed on startup Spell checking is unavailable Spelling Dictionary: Stage Changed Stage Changed Files To Commit Stage Hunk For Commit Stage Line For Commit Stage Lines For Commit Stage To Commit Staged Changes (Will Commit) Staged for commit Staged for commit, missing Staged for removal Staged for removal, still present Staging area (index) is already locked. Standard (Fast, Semi-Redundant, Hardlinks) Standard only available for local repository. Start git gui In The Submodule Starting Revision Starting gitk... please wait... Starting... Staying on branch '%s'. Success Summarize Merge Commits System (%s) Tag Target Directory: The 'master' branch has not been initialized. The following branches are not completely merged into %s: The following branches are not completely merged into %s:

 - %s There is nothing to amend.

You are about to create the initial commit.  There is no commit before this to amend.
 This Detached Checkout This is example text.
If you like this text, it can be your font. This repository currently has approximately %i loose objects.

To maintain optimal performance it is strongly recommended that you compress the database.

Compress the database now? Tool '%s' already exists. Tool Details Tool completed successfully: %s Tool failed: %s Tool: %s Tools Tracking Branch Tracking branch %s is not a branch in the remote repository. Transfer Options Trust File Modification Timestamps URL Unable to cleanup %s Unable to copy object: %s Unable to copy objects/info/alternates: %s Unable to display %s Unable to display parent Unable to hardlink object: %s Unable to obtain your identity: Unable to unlock the index. Undo Unexpected EOF from spell checker Unknown file state %s detected.

File %s cannot be committed by this program.
 Unlock Index Unmerged files cannot be committed.

File %s has merge conflicts.  You must resolve them and stage the file before committing.
 Unmodified Unrecognized spell checker Unstage From Commit Unstage Hunk From Commit Unstage Line From Commit Unstage Lines From Commit Unstaged Changes Unstaging %s from commit Unsupported merge tool '%s' Unsupported spell checker Untracked, not staged Update Existing Branch: Updated Updating the Git index failed.  A rescan will be automatically started to resynchronize git-gui. Updating working directory to '%s'... Use '/' separators to create a submenu tree: Use Local Version Use Merge Tool Use Remote Version Use thin pack (for slow network connections) User Name Verify Database Verifying the object database with fsck-objects Visualize Visualize %s's History Visualize All Branch History Visualize All Branch History In The Submodule Visualize Current Branch History In The Submodule Visualize Current Branch's History Visualize These Changes In The Submodule Working... please wait... You are in the middle of a change.

File %s is modified.

You should complete the current commit before starting a merge.  Doing so will help you abort a failed merge, should the need arise.
 You are in the middle of a conflicted merge.

File %s has merge conflicts.

You must resolve them, stage the file, and commit to complete the current merge.  Only then can you begin another merge.
 You are no longer on a local branch.

If you wanted to be on a branch, create one now starting from 'This Detached Checkout'. You must correct the above errors before committing. Your OpenSSH Public Key Your key is in: %s [Up To Parent] buckets commit-tree failed: error fatal: Cannot resolve %s fatal: cannot stat path %s: No such file or directory fetch %s files files checked out files reset git-gui - a graphical user interface for Git. git-gui: fatal error lines annotated objects pt. push %s remote prune %s update-ref failed: warning warning: Tcl does not support encoding '%s'. write-tree failed: Project-Id-Version: git-gui
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-01-26 15:47-0800
PO-Revision-Date: 2013-09-14 01:14+0000
Last-Translator: Ricardo Ichizo <n1ghtcr4wler@gmail.com>
Language-Team: Brazilian Portuguese <>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:28+0000
X-Generator: Launchpad (build 18115)
 

Uma boa alternativa para %s
é colocar os valores para o nome de usuário e e-mail
no seu arquivo "~/.gitconfig"
 
* O arquivo não rastreado foi cortado aqui por %s.
* Para ver o arquivo completo, use um editor externo.
 
Isto se deve a um problema conhecido com os binários da Tcl 
distribuídos com o Cygwin %s ... %*i de %*i %s (%3i%%) Repositório %s %s de %s "%s" não é um nome de ramo válido "%s" não é um nome válido para um repositório remoto. (Azul indica ferramentas do repositório local) * Arquivo binário (conteúdo não exibido). * O arquivo não rastreado possui %d bytes.
* Exibindo apenas os primeiros %d bytes.
 É preciso indicar um ramo para "Mesclado em". Abortar mesclagem... Operação abortada com sucesso. Pronto. A tentativa de abortar a operação falhou Abortar mesclagem?

Abortar a mesclagem atual implicará na perda de *TODAS* as mudanças não salvas.

Abortar a mesclagem atual? Checkout de "%s" abortado (é preciso mesclar alguns arquivos) Abortando Sobre o %s Adicionar Adicionar novo repositório remoto Adicionar novo comando de ferramenta Adicionar repositório remoto Adicionar ferramenta Adicionar globalmente Adicionar... Adicionando %s Adicionando resolução para %s Forçar exclusão (não verificar se o ramo foi mesclado) Corrigir última revisão Descrição da revisão corrigida: Descrição da revisão inicial corrigida: Descrição da revisão de mescla corrigida: Anotação completa. O processo de anotação já está em execução Qualquer alteração não marcada será permanentemente perdida na reversão. Aplicar/reverter trecho Aplicar/reverter linha Outro local: Você tem certeza que deseja executar %s? Argumentos Solicitar argumentos adicionais (define a variável $ARGS) Solicitar a seleção de uma revisão (a variável $REVISION) Autor: Detectar cópias somente em arquivos modificados Extensão do contexto de detecção (em dias) Anotar revisão anterior Ramo O ramo "%s" já existe. O ramo "%s" já existe.

Não é possível avançá-lo para %s.
É preciso mesclar. O ramo "%s" não existe. Nome do ramo Ramo: Ramos Explorar Explorar arquivos de %s Explorar arquivos do ramo Explorar arquivos do ramo... Explorar arquivos do ramo atual Ocupado Executando script "commit-msg"... Executando script "pre-commit"... Executando hook "prepare-commit-msg"... Cancelar Não é possível abortar durante uma correção.

Você precisa finalizar a correção desta revisão.
 Não é possível corrigir durante uma mesclagem.

Você está em meio a uma operação de mesclagem que não foi completada. Não é possível corrigir a revisão anterior a menos que você aborte a mescla atual antes.
 Não foi possível determinar a etiqueta HEAD. Veja a saída do console para detalhes. Não foi possível receber ramos ou objetos. Veja a saída do console para detalhes. Não foi possível receber as etiquetas. Veja a saída do console para detalhes. Impossível encontrar revisão HEAD: Impossível encontrar o git no "PATH" Impossível encontrar revisão anterior: Não é possível mesclar durante uma correção.

Você deve concluir a correção antes de começar qualquer mesclagem.
 Impossível mover para o início do diretório de trabalho: Impossível interpretar a versão do git: Não foi possível resolver %s como uma revisão. Impossível resolver conflitos envolvendo exclusão ou links de arquivos com esta ferramenta. Impossível usar repositório puro: Não foi possível gravar o ícone: Não foi possível gravar o atalho: Sensível a maiúsculas/minúsculas Alterar Mudar fonte Checkout de "%s" concluído. Checkout Efetuar checkout após a criação Efetuar checkout do ramo Checkout... Escolher %s Clonar Clonar repositório existente Tipo de clonagem: A clonagem falhou. Clonar... Clonando de %s Fechar Comando: A revisão %s parece estar corrompida. Largura do texto da descrição da revisão Descrição da revisão: Revisão bloqueada pelo script "commit-msg". A revisão foi bloqueada pelo script "pre-commit". O script "prepare-commit-msg" negou a criação de uma nova revisão A revisão falhou. Revisão: Revisão Salvar revisão Revisor: Salvando revisão... Compactar banco de dados Compactando banco de dados de objetos O arquivo conflitante não existe Continuar Copiado ou movido para cá por: Copiar Copiar todos Copiar revisão Copiar para a área de transferência Copiando objetos Não foi possível adicionar a ferramenta:
%s Impossível iniciar ssh-keygen:

%s Não foi possível iniciar a ferramenta de mesclagem:

%s Impossível encontrar o "git gui" no PATH Impossível encontrar o gitk no PATH Contando objetos Criar Criar ramo Criar ícone na área de trabalho Criar novo ramo Criar novo repositório Criar... Revisão %s criada: %s Criando diretório de trabalho. Ramo atual: Recortar Estatísticas do banco de dados Reduzir tamanho da fonte Padrão Codificação padrão dos arquivos Apagar Apagar ramo Apagar ramo remoto Apagar ramo... Apagar ramo local Apagar somente se Apagar somente se mesclado em Apagar... Apagar ramos de %s Repositório de destino Separar do ramo local Fonte para o diff/console O diretório %s já existe. Diretório: Espaço ocupado pelos objetos soltos Espaço ocupado pelos objetos compactados Exibindo apenas %s de %s arquivos. Executar detecção completa de cópias Não fazer nada Não fazer nada agora Não sabe como inicializar o repositório remoto em "%s". Não exibir a janela de saída do comando Pronto Editar Endereço de e-mail Codificação Erro ao carregar dados da revisão para corrigir: Erro ao carregar as diferenças: Erro ao carregar o arquivo: Erro ao obter as versões:
%s Erro: o comando falhou Explorar cópia de trabalho Erro ao adicionar repositório remoto "%s" do local "%s": Houve um erro ao salvar as opções: Erro ao configurar origem Erro ao configurar git-pull simplificado para "%s". Erro ao criar repositório %s: Erro ao apagar ramos:
%s Erro ao abrir o repositório %s: Erro ao renomear "%s". Erro ao definir o ramo atual.

Este diretório de trabalho está incompleto. Foi possível atualizar seus arquivos, mas houve uma falha ao atualizar os arquivos internos do Git.

Isto não deveria ter acontecido, %s terminará agora. Erro ao marcar o trecho selecionado. Erro ao marcar a linha selecionada. Erro ao desmarcar o trecho selecionado. Erro ao desmarcar a linha selecionada. Erro ao atualizar "%s". Somente se for um avanço rápido Receber imediatamente Obter ramo de rastreamento Receber de Obtendo %s de %s Recebendo novas mudanças de %s Recebendo o %s O arquivo %s já existe. O arquivo %s parece ter conflitos não resolvidos. Marcar mesmo assim? Navegador de arquivos Visualizador de arquivos Mesclagem de arquivos necessária. Tipo do arquivo modificado, não marcado Tipo do arquivo modificado, marcado Arquivo: Procurar texto... Encontrar: Exemplo Tipo da fonte Tamanho da fonte Sobrescrever ramos existentes (pode descartar mudanças) Forçar a resolução para a versão base? Forçar resolução para o outro ramo? Forçar resolução para este ramo? Chave pública encontrada em: %s Do repositório Cópia completa (mais lenta, backup redundante) Ações adicionais Arquivos de lixo Gerar chave Gerando... A geração da chave falhou. A geração da chave foi bem-sucedida, mas nenhuma chave foi encontrada. Git Gui Repositório Git Repositório Git (sub-projeto) Diretório do Git não encontrado: Não foi possível determinar a versão do git:

%s afirmar que sua versão é "%s".

%s exige o Git 1.5.0 ou posterior.

Assumir que '%s' é a versão 1.5.0?
 Global (todos os repositórios) Não foi possível criar hardlinks, usando cópias convencionais. Ajuda No arquivo: Incluir etiquetas Aumentar tamanho da fonte Erro no índice Descrição da revisão inicial: Erro ao efetuar checkout inicial. Inicializar repositório remoto e enviar Iniciando... Variável "GIT_COMMITTER_IDENT" inválida: Data inválida recebida do Git: %s Fonte inválida indicada em %s: Codificação global inválida "%s" Codificação do repositório inválida "%s" Revisão inválida: %s Configuração do verificador ortográfico inválida KiB Local:
 Local: apagado
Remoto:
 O último estado lido não confere com o estado atual.

Outro programa do Git modificou o repositório desde a última leitura. Uma atualização deve ser executada antes de efetuar uma mesclagem.

A atualização começará automaticamente agora.
 O último estado lido não confere com o estado atual.

Outro programa do Git modificou o repositório desde a última leitura. Uma atualização deve ser executada antes de criar outra revisão.

A atualização começará automaticamente agora.
 O último estado lido não confere com o estado atual.

Outro programa do Git modificou o repositório desde a última leitura. Uma atualização deve ser executada antes de alterar o ramo atual.

A atualização começará automaticamente agora.
 Ligando objetos Carregando %s... Carregando anotações... Carregando anotações de cópia/movimentação... Carregando diferenças de %s... Carregando anotações originais... Ramo local Ramos locais Mesclar localmente... O local %s já existe. Local: Fonte principal Coincidir nome do ramo de rastreamento Coincidir ramos de rastreamento Mesclar Descrição da revisão de mescla: Mesclar em %s Nível de detalhamento da mesclagem Mesclagem completada com sucesso. A mesclagem falhou. É necessário resolver conflitos. Estratégia de mesclagem "%s" não suportada. Ferramenta de mesclagem falhou. A ferramenta de mesclagem já está em execução. Finalizar? Mesclado em: Mesclando %s e %s... Número mínimo de letras para detectar cópias Duplicando para %s Faltando Modificado, não marcado Nome: Modelo de nome para novos ramos Nova revisão Novo nome: Novo... Próximo Não Sem sugestões Não há alterações para salvar. Não há alterações para salvar.

Nenhum arquivo foi modificado e esta não é uma revisão de mesclagem.

Uma atualização será executada automaticamente agora.
 Não há mudanças para salvar.

Você deve marcar ao menos um arquivo antes de salvar a revisão.
 O ramo padrão não foi recebido. Nenhuma diferença foi detectada.

%s não possui mudanças.

A data de modificação deste arquivo foi atualizada por outro aplicativo, mas o conteúdo do arquivo não foi alterado.

Uma atualização ser executada para encontrar outros arquivos que possam ter o mesmo estado. Nenhuma chave encontrada Nenhum repositório foi selecionado. Nenhuma revisão selecionada. Sem diretório de trabalho Não é uma ferramenta de mesclagem gráfica: "%s" Este não é um repositório do Git: %s Note que o diff mostra apenas as mudanças conflitantes.

%s será sobrescrito.

Caso necessário, será preciso reiniciar a mesclagem para desfazer esta operação. Não há nada para clonar em %s. Número de linhas para o diff contextual Número de objetos soltos Número de objetos compactados Número de pacotes OK Um ou mais testes de mesclagem falharam porque você não possui as revisões necessárias. Tente receber revisões de %s primeiro. Ajuda online Abrir Abrir repositório existente Abrir repositório recente: Abrir... Opções Opções... Arquivo original: Originalmente por: Outro Objetos compactados aguardando eliminação Colar Selecione um ramo para renomear. Selecione um ramo de rastreamento. Por favor selecione um ou mais ramos para apagar. Indique um nome para o ramo. Por favor, indique uma descrição para a revisão.

Uma boa descrição tem o seguinte formato:

- Primeira linha: descreve, em uma única frase, o que você fez.
- Segunda linha: em branco.
- Demais linhas: Descreve detalhadamente a revisão.
 Por favor, indique um nome para a ferramenta. Por favor, indique um nome para o repositório remoto. Trechos marcados para revisão Possíveis problemas com as variáveis de ambiente.

As seguintes variáveis de ambiente provavelmente serão
ignoradas por qualquer sub-processo do Git executado por
%s:

 Preferências Anterior Eliminar ramos de rastreamento ao receber Limpar de Limpando ramos excluídos de %s Enviar Enviar ramos Enviar para Enviar... Enviando %s %s para %s Enviando mudanças para %s Sair Remoto:
 Remoto: apagado
Local:
 Lendo %s... Pronto para salvar a revisão. Pronto. Repositórios recentes Recuperar ramos apagados é difícil.

Apagar os ramos selecionados? Recuperar revisões perdidas pode não ser fácil. Refazer Atualizar Atualizando estado dos arquivos... Remoto Detalhes do repositório remoto Remoto: Excluir Excluir Excluir ferramenta Excluir comando de ferramenta Remover... Renomear Renomear ramo Renomear... Repositório Repositório: Requer resolução de conflitos Atualizar Redefinir Redefinir "%s"? Descartar as mudanças?

Ao fazê-lo, *TODAS* as alterações não salvas serão perdidas.

Continuar e descartar as mudanças atuais? Redefinir... Redefinir "%s" para "%s" provocará a perda das seguintes revisões: Restaurar padrões Reverter mudanças Reverter para a versão-base Reverter as alterações no arquivo %s? Reverter as alterações nestes %i arquivos? Revertendo %s Revertendo dicionário para %s. Revertendo os arquivos selecionados Revisão Expressão de revisão: Revisão para mesclar A expressão de revisão está vazia. Executar comando: %s Executar ferramenta de mescla Executar apenas se houver um diff selecionado ($FILENAME não-vazio) É preciso selecionar um arquivo para executar %s. Executando ferramenta de mesclagem... Executando detecção de cópia... Executando: %s Salvar Atualizando %s... Procurando por arquivos modificados ... Selecionar Selecionar tudo Configurando %s (em %s) Compartilhada (A mais rápida, não recomendada, sem backup) Clonagens parciais só são possíveis em repositórios locais. Exibir estatísticas após mesclagens Mostrar contexto do histórico Mostrar menos contexto Mostrar mais contexto Mostrar chave SSH Exibir uma caixa de diálogo antes de executar Assinar embaixo Ramos de origem Origem: A verificação ortográfica falhou O verificador ortográfico falhou sem relatar nenhum erro Verificação ortográfica indisponível Dicionário para o verificador ortográfico: Marcar alterados Marcar arquivos modificados Marcar trecho para revisão Marcar linha para revisão Marcar linhas para revisão Marcar para revisão Mudanças marcadas Marcado para uma nova revisão Marcado para revisão, faltando Marcado para remoção Marcado para remoção, ainda presente A área de marcação (staging area, index) já está bloqueada. Padrão (rápida, semi-redundante, com hardlinks) Clonagens padrões só são possíveis em repositórios locais. Iniciar "git gui" no sub-módulo Revisão inicial Iniciando gitk... Aguarde... Inciando... Permanecendo no ramo "%s". Sucesso Exibir sumário das revisões de mesclagem Sistema (%s) Etiqueta Diretório de destino: O ramo "master" não foi inicializado. Os ramos seguintes não foram completamente mesclados em %s: Os seguintes ramos não estão inteiramente mesclados em %s:

 - %s Não há nada para corrigir.

Você está prestes a criar uma revisão inicial. Não há revisão anterior para corrigir.
 Este checkout Este é um texto de exemplo.
Se você gostar deste texto, esta pode ser sua fonte. Este repositório possui aproximadamente %i objetos soltos.

Para manter o desempenho ótimo é altamente recomendado que você compacte o banco de dados.

Compactar o banco de dados agora? A ferramenta "%s" já existe. Detalhes da ferramenta Execução completada com sucesso: %s Ferramenta falhou: %s Ferramenta: %s Ferramentas Ramo de rastreamento O ramo de rastreamento %s não é um ramo do repositório remoto. Opções de transferência Confiar nas datas de modificação dos arquivos URL Não foi possível limpar %s Não foi possível copiar o objeto: %s Erro ao copiar objetos ou informações adicionais: %s Impossível exibir %s Impossível exibir revisão anterior Não foi possível ligar o objeto: %s Não foi possível obter a sua identidade: Impossível desbloquear o índice. Desfazer Final de arquivo inesperado recebido do verificador ortográfico Estado desconhecido detectado para o arquivo %s.

Este programa não pode salvar uma revisão para o arquivo %s.
 Desbloquear índice Não é possível salvar revisões para arquivos não mesclados.

O arquivo %s possui conflitos de mesclagem. Você deve resolvê-los e marcar o arquivo antes de salvar a revisão.
 Não modificado Verificador ortográfico não reconhecido Desmarcar Desmarcar trecho para revisão Desmarcar linha para revisão Desmarcar linhas para revisão Mudanças não marcadas Desmarcando %s para revisão Ferramenta de mesclagem não suportada "%s" Verificador ortográfico não suportado Não monitorado, não marcado Atualizar ramo existente: Atualizado A atualização do índice do Git falhou. Uma atualização será executada automaticamente para ressincronizar o Git GUI Atualizando diretório de trabalho para "%s"... Use o separador "/" para criar uma árvore de sub-menus: Usar versão local Usar ferramenta de mesclagem Usar versão remota Usar compactação minimalista (para redes lentas) Nome do usuário Verificar banco de dados Verificando banco de dados de objetos com fsck-objects Visualizar Visualizar histórico de %s Visualizar histórico de todos os ramos Visualizar histórico de todos os camos no sub-módulo Visualizar histórico do ramo atual no sub-módulo Visualizar histórico do ramo atual Visualizar estas mudanças no sub-módulo Trabalhando... aguarde... Você está em meio a uma mudança.

O arquivo %s foi modificado.

Você deve completar e salvar a revisão atual antes de começar uma mesclagem. Ao fazê-lo, você poderá abortar a mesclagem caso haja algum erro.
 Há uma mesclagem com conflitos em progresso.

O arquivo %s possui conflitos de mesclagem.

Você deve resolvê-los, marcar o arquivo e salvar a revisão para completar a mesclagem atual. Só então você poderá começar outra.
 Você não está mais em um ramo local

Se você deseja um ramo, crie um agora a partir deste checkout. Você precisa corrigir os erros acima antes de salvar a revisão. Sua chave pública OpenSSH Sua chave em: %s [Subir] buckets commit-tree falhou: Erro Erro fatal: impossível resolver %s erro fatal: impossível executar "stat" em  %s: Arquivo ou diretório não encontrado receber %s arquivos arquivos retirados arquivos redefindos git-gui - uma interface gráfica para o Git git-gui: erro fatal linhas anotadas objetos pt. enviar %s Limpar %s update-ref falhou: aviso aviso: O Tcl não suporta a codificação "%s". write-tree falhou: 