��    �        �   
      �     �     �     �     �  #   �               2  k   A     �  <   �     �     �        	             )     =     U  
   m  H   x     �     �     �  #   �          +     9     O     X     d  (   w  (   �  @   �     
  D   )     n     �     �     �     �     �     �     �  
   �  
   �     	       .   &  0   U     �     �  	   �  M   �     �               /     H     V  %   l  (   �  3   �  
   �  @   �  C   ;          �     �     �  9   �       )     ^   E  0   �     �  .   �       %         F     `  &   w     �     �     �     �  ?   �     8     @  (   T     }     �     �     �     �     �  �     9   �  1     2   =     p     �     �  -   �     �  &   �  @   #  '   d     �  '   �  "   �     �            &   #  O   J  �   �  �   @     �  �   �  @   y  D   �  D   �  <   D  .   �  -   �  4   �          #     4  /   J     z     �  '   �     �     �  >   �      :      [      v      �      �       �      �      �      �      �      �      �      �      �      �      !     !     !  �   $!     �!     �!     �!  �  �!     �#     �#     $     $  3   $     O$     i$     �$  k   �$     %  J   %  	   W%     a%  
   h%     s%     �%     �%  %   �%     �%  
   �%  H   &     Q&     i&     q&  0   &  )   �&     �&     �&     '     '     '  6   >'  6   u'  N   �'  (   �'  W   $(     |(     �(  
   �(     �(     �(     �(     �(     )  
   )     ()     7)     M)  7   c)  8   �)     �)     �)     �)  a   �)     _*     w*      �*     �*     �*      �*  )   �*  '   %+  <   M+     �+  Q   �+  P   �+  &   <,     c,     �,  "   �,  @   �,     �,  <   -  u   C-  9   �-     �-  <   .     >.  A   Y.  0   �.     �.  )   �.     /     /  #   1/  #   U/  Q   y/     �/     �/  7   �/     "0     ;0  %   [0     �0      �0     �0  �   �0  V   �1  A   2  B   Q2  "   �2     �2  %   �2  :   �2  &   53  <   \3  e   �3  2   �3     24  8   R4  '   �4     �4     �4     �4  (   �4  l    5  �   �5  �   G6  "   �6  �   7  I   �7  I   �7  L   '8  @   t8  6   �8  3   �8  ?    9     `9     x9  &   �9  #   �9     �9     �9  6   :     C:     X:  N   x:  )   �:  #   �:     ;     ";  &   +;  &   R;     y;  
   �;     �;  	   �;     �;     �;     �;     �;     �;     �;     �;     �;  �   �;     �<     �<  h  �<     _          o   y   B   t   |   c   �       T   {   (   �   W   �   ,   l             r       �   4   S   �   �       ^   P          �      �   #   �       =   	       �           R   E             N   `   d   U   '   \       }   u   ;      >       [   h          q      �                      ~   1       A   �               Z   2   )       e       Y       �          �   �   �   "         �      g   k   H       ?      L       $   C   i       x      j   D      p       �   �   8   .       M   5           I   &                  �                  z   �       G   �   F   �   Q   v   @   
       V       9   �   b           /          �       K   w              �   s       J   �                         X           �   ]   f   �      �       3           a      7   O   �   �      -      *       m       �   0              6   :   <   !       %       n   +    %s is unknown command!
 (Not implemented) ... <b>Font and Style</b> <b>Global input method settings</b> <b>Keyboard Layout</b> <b>Keyboard Shortcuts</b> <b>Startup</b> <big><b>IBus</b></big>
<small>The intelligent input bus</small>
Homepage: http://code.google.com/p/ibus



 About Add the selected input method into the enabled input methods Advanced Always Author: %s
 Auto hide Bottom left corner Bottom right corner Can't connect to IBus.
 Candidates orientation: Commands:
 Copyright (c) 2007-2010 Peng Huang
Copyright (c) 2007-2010 Red Hat, Inc. Create registry cache Custom Custom font Custom font name for language panel DConf preserve name prefixes Description:
 Disable shortcut keys Disable: Do not show Embed Preedit Text Embed Preedit Text in Application Window Embed preedit text in application window Embed the preedit text of input method in the application window Enable input method by default Enable input method by default when the application gets input focus Enable or disable: Enable shortcut keys Enable: Engines order Exit ibus-daemon General Get global engine failed.
 Hide automatically Horizontal IBus Panel IBus Preferences IBus Update IBus daemon could not be started in %d seconds IBus is an intelligent input bus for Linux/Unix. Input Method Kbd Key code: Keyboard Input Methods (IBus Daemon) is not running. Do you wish to start it? Keyboard layout: %s
 Keyboard shortcuts Language panel position Language panel position: Language: %s
 List engine name only List of Asian languages on ibus-setup List of European languages on ibus-setup List of system keyboard layout groups on ibus-setup Modifiers: Move down the selected input method in the enabled input methods Move up the selected input method in the enabled input methods list Next engine shortcut keys Next input method: No engine is set.
 Orientation of lookup table Orientation of lookup table. 0 = Horizontal, 1 = Vertical Other Please press a key (or a key combination) Please press a key (or a key combination).
The dialog will be closed when the key is released. Popup delay milliseconds for IME switcher window Preferences Prefixes of DConf keys to stop name conversion Preload engines Preload engines during ibus starts up Prev engine shortcut keys Previous input method: Print the D-Bus address of ibus-daemon Quit RGBA value of XKB icon Read the registry cache FILE. Read the system registry cache. Remove the selected input method from the enabled input methods Restart Restart ibus-daemon Saved engines order in input method list Saved version number Select an input method Select keyboard shortcut for %s Set IBus Preferences Set global engine failed.
 Set or get engine Set popup delay milliseconds to show IME switcher window. The default is 400. 0 = Show the window immediately. 0 &lt; Delay milliseconds. 0 &gt; Do not show the window and switch prev/next engines. Set the behavior of ibus how to show or hide language bar Set the orientation of candidates in lookup table Share the same input method among all applications Show all input methods Show available engines Show icon on system tray Show information of the selected input method Show input method name Show input method name on language bar Show input method's name on language bar when check the checkbox Show only input methods for your region Show property panel: Show setup of the selected input method Show the content of registry cache Show this information Show version Start ibus on login Super+space is now the default hotkey. The behavior of property panel. 0 = Do not show, 1 = Auto hide, 2 = Always show The group list is used not to show all the system keyboard layouts by default. The list item will be appended at the end of gconf key. e.g. .../xkblayoutconfig/item1 The position of the language panel. 0 = Top left corner, 1 = Top right corner, 2 = Bottom left corner, 3 = Bottom right corner, 4 = Custom The registry cache is invalid.
 The saved version number will be used to check the difference between the version of the previous installed ibus and one of the current ibus. The shortcut keys for switching to next input method in the list The shortcut keys for switching to previous input method in the list The shortcut keys for switching to the next input method in the list The shortcut keys for switching to the previous input method The shortcut keys for turning input method off The shortcut keys for turning input method on The shortcut keys for turning input method on or off Top left corner Top right corner Trigger shortcut keys Trigger shortcut keys for gtk_accelerator_parse Usage: %s COMMAND [OPTION...]

 Use custom font Use custom font name for language panel Use custom font: Use global input method Use shortcut with shift to switch to the previous input method Use system keyboard (XKB) layout Use system keyboard layout Use xmodmap Vertical Write the registry cache FILE. Write the system registry cache. _About _Add _Apply _Cancel _Close _Delete _Down _OK _Preferences _Remove _Up default:LTR ibus-setup shows the languages only in input method list when you run ibus-setup on one of the languages. Other languages are hidden under an extended button. language: %s
 switching input methods translator-credits Project-Id-Version: IBus
Report-Msgid-Bugs-To: https://github.com/ibus/ibus/issues
POT-Creation-Date: 2015-06-20 22:35+0000
PO-Revision-Date: 2016-01-20 00:03+0000
Last-Translator: jonataszv <Unknown>
Language-Team: Portuguese (Brazil) <trans-pt_br@lists.fedoraproject.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:36+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 %s é um comando desconhecido!
 (Não implementado) ... <b>Fonte e Estilo</b> <b>Configurações de Método de Entrada Global</b> <b>Desenho do Teclado</b> <b>Atalhos do Teclado</b> <b>Inicialização</b> <big><b>IBus</b></big>
<small>The intelligent input bus</small>
Homepage: http://code.google.com/p/ibus



 Sobre Adicione o método de entrada selecionado nos métodos de entrada ativados Avançado Sempre Autor: %s
 Ocultar Automaticamente Canto esquerdo inferior Canto direito superior Não foi possível conectar ao IBus.
 Orientação dos candidatos: Comandos:
 Copyright (c) 2007-2010 Peng Huang
Copyright (c) 2007-2010 Red Hat, Inc. Criar cache de registro Padrão Fonte padrão Nome da fonte padrão para o painel de linguagem Prefixo de nome de preservação do DConf Descrição:
 Desabilitar teclas de atalho Desabilitar: Não mostrar Embutir Texto de Pré-Edição Embutir Texto de Pré-edição na Janela do Aplicativo Embutir texto de pré-edição na janela do aplicativo Embutir o texto de pré-edição do método de entrada na janela do aplicativo Habilitar método de entrada por padrão Habilitar método de entrada por padrão quando o aplicativo obtiver o foco de entradas Habilitar ou desabilitar: Habilitar teclas de atalho Habilitar: Ordem dos Motores Sair do ibus-daemon Geral Obter motor global falhour.
 Ocultar automaticamente Horizontal Painel do IBus Preferências do IBus Atualização do IBus O daemon do IBus não pôde ser iniciado em %d segundos IBus é um bus de entrada inteligente para o Linux/Unix. Método de Entrada Kbd Código de tecla: Métodos de entrada de teclado (IBus Daemon) não está sendo executado. Você deseja iniciá-lo? Desenho do teclado: %s
 Atalhos do teclado Posição do Painel de Linguagem Posição do painel de idiomas: Linguagem: %s
 Somente o Nome do motor da lista Lista de Idiomas Aisáticos no ibus-setup Lista de idiomas Europeus no ibus-setup Lista do layout do grupo de teclado do sistema em ibus-setup Modificadores: Mova o método de entrada selecionado para baixo nos métodos de entrada ativados Mova para cima o método de entrada selecionado na lista dos métodos de entrada Próximo mecanismo de teclas de atalho Próximo método de entrada: Nenhum motor configurado.
 Orientação da tabela de pesquisa Orientação da Tabela de Pesquisa. 0 = Horizontal, 1 = Vertical Outros Por favor pressione uma tecla (ou uma combinação de tecla) Por favor pressione uma tecla (ou uma combinação de tecla).
O diálogo será encerrado quando a tecla for liberada. Popup atrasou milisegundos para a janela de alterador IME Preferências Prefixos das chaves do DConf para parar a conversão do nome Mecanismos de carregamento Mecanismos de pré-carregamento durante a inicialização do ibus Visualização do mecanismo das teclas de atalho Método de entrada anterior: Imprimir o endereço D-Bus do ibus-daemon Sair Valor RGBA de ícone XKB Ler o cache de registro de ARQUIVO. Ler o cache de registro do sistema. Remova o método de entrada selecionado a partir dos métodos de entrada ativados Reinicie Reiniciar ibus-daemon Ordem dos motores salvas na lista de método de entrada Número da versão salvo Selecione um método de entrada Selecione o atalho do teclado para %s Definir Preferências do IBus Configurar motor global falhou.
 Definir ou obter engine Configuração do popup atrasa milisegundos par exibir a janela do alterador IME. O padrão é 400.0 = Exibe a janela imediatamente. 0 &lt; Atrasa milisegundos. 0 &gt; Não exibe a janela e altera motores anterior/próximo. Configure o comportamento do ibus para como demonstrar ou ocultar a barra de linguagem Configure a orientação dos candidatos na tabela de observação Compartilhar o mesmo método de entrada entre todos os aplicativos Exibe todos os métodos de entrada Mostrar engines disponíveis Mostra o ícone na bandeja do sistema Apresente a informação do método de entrada selecionado Apresenta o nome do método de entrada Apresenta o nome do método de entrada na barra de linguagem Apresente o nome do método de entrada na barra de linguagem quando selecionando a caixa de seleção Exibe somente métodos de entrada para sua região Mostrar painel de propriedades: Mostrar configuração do método de entrada selecionado Mostrar conteúdo de cachê do registro Mostrar essa informação Mostrar versão Inicie o ibus na conexão Super+espaço é agora o atalho padrão. Comportamento do painel de propriedades. 0 = Não mostrar, 1 =  Ocultar automaticamente,  2 = Sempre mostrar A lista de grupo é usada para não exibir todos os layouts de teclados de sistema por padrão. O ítem da lista será adicionado ao final da tecla gconf. ex.: .../xkblayoutconfig/item1 A posição do painel de linguagem. 0 = Canto esquerdo superior, 1 = Canto direito superior, 2 = canto esquerdo inferior, 3 = canto direito inferior, 4 = Padrão O cache de registro é inválido.
 O número da versão salvo será usado para verificar a diferença entre a versão do ibus instalado anteriormente e um do ibus atual. Teclas de atalho para alterar para o próximo método de entrada na lista Teclas de atalho para alterar para o método de entrada anterior na lista As teclas de atalho para alteração ao próximo método de entrada na lista Teclas de atalho para alteração ao método de entrada anterior As teclas de atalho para desligar o método de entrada As teclas de atalho para ligar o método de entrada As teclas de atalho para ligar ou desligar o método de entrada Canto esquerdo superior Canto direito superior Realiza o trigger nas teclas de atalho Desencadeia chaves de atalho para o Uso: %s COMMAND [OPTION...]

 Usa a fonte padrão Usa o nome da fonte padrão para o painel de linguagem Usa a fonte padrão: Use o método de entrada global Utilize o atalho com o alterador para mudar para o método de entrada anterior Usa o desenho do teclado do sistema (XKB) Usa o desenho do teclado do sistema Usar xmodmap Vertical Escrever cache de registro do ARQUIVO. Escrever cache de registro do sistema. _Sobre _Adicionar _Aplicar _Cancelar _Fechar E_xcluir A_baixo _OK _Preferências _Remover A_cima default:LTR ibuts-setup exibe os idiomas somente na lista de método de entrada quando você executa ibus-setup em um dos idiomas. Outros idiomas estão escondidos sob um botão estendido. idioma: %s
 mudando métodos de entrada créditos-tradutor

Launchpad Contributions:
  4nd0r3ss https://launchpad.net/~sabinbr
  Anderson Lima https://launchpad.net/~mano-queixo
  André Gondim https://launchpad.net/~andregondim
  Celio Alves https://launchpad.net/~celio.alves
  Cissô https://launchpad.net/~jcicero
  Fábio Nogueira https://launchpad.net/~fnogueira
  Gilberto https://launchpad.net/~profgilbertob
  Glaucia Cintra https://launchpad.net/~gcintra
  Heder Dorneles Soares https://launchpad.net/~heder-to
  Hriostat https://launchpad.net/~hriostat
  Isaque Alves https://launchpad.net/~isaquealves
  Juliano Fischer Naves https://launchpad.net/~julianofischer
  Rafael Braga https://launchpad.net/~rafaelbraga
  Rafael Neri https://launchpad.net/~rafepel
  Renan Araujo Rischiotto https://launchpad.net/~renanrischiotto1
  Renato Krupa https://launchpad.net/~renatokrupa
  Salomão Carneiro de Brito https://launchpad.net/~salomaocar
  Tiago Hillebrandt https://launchpad.net/~tiagohillebrandt
  Vinicius Almeida https://launchpad.net/~vinicius-algo
  gabriell nascimento https://launchpad.net/~gabriellhrn
  jonataszv https://launchpad.net/~jonatas-zv 