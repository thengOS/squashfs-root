��    *      l  ;   �      �     �     �     �     �     �     �     �     �     �  	   �     �     �     �     �     �                               *     0     9     =     C     H     L     Q     U     [  	   b     l     q     ~     �     �  2   �     �     �  �   �  �   �  �  �     u     y     }     �     �     �     �     �     �  	   �     �     �     �     �     �     �     �     �     �     �     �     	     
	     	  
   	     "	     &	     +	     /	     6	  	   =	     G	     L	     _	     {	     �	  6   �	     �	     �	  �   �	  �   �
     *   !                        	              
               $         '   )                                (                       &                                    "                 #   %              00s 10s 60s 70s 80s 90s Albums Banshee Blues Classical Country Decade Disco Funk Genre Hip-hop House Jazz Metal More suggestions Music New-wave Old Other Play Pop Punk R&B Radio Reggae Rhythmbox Rock Search music Search music collection Show in Folder Songs Sorry, there is no music that matches your search. Soul Techno This is an Ubuntu search plugin that enables information from Banshee to be searched and displayed in the Dash underneath the Music header. If you do not wish to search this content source, you can disable this search plugin. This is an Ubuntu search plugin that enables information from Rhythmbox to be searched and displayed in the Dash underneath the Music header. If you do not wish to search this content source, you can disable this search plugin. Project-Id-Version: unity-lens-music
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-15 08:23+0000
PO-Revision-Date: 2014-01-17 02:02+0000
Last-Translator: Evertton de Lima <e.everttonlima@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:06+0000
X-Generator: Launchpad (build 18115)
 00s 10s 60s 70s 80s 90s Álbuns Banshee Blues Clássica Country Década Disco Funk Gênero Hip-hop House Jazz Metal Mais sugestões Música New-wave Antigo Outro Reproduzir Pop Punk R&B Rádio Reggae Rhythmbox Rock Pesquisar músicas Pesquisar coleção musical Mostrar na pasta Músicas Desculpe, nenhuma música corresponde à sua pesquisa. Soul Techno Este é um plugin de busca do Ubuntu que permite que informações do Banshee sejam buscadas e exibidas no Painel abaixo do título Música. Se você não deseja realizar buscas usando essa fonte de conteúdo, você pode desabilitar esse plugin. Este é um plugin de busca do Ubuntu que permite que informações do Rhythmbox sejam buscadas e exibidas no Painel abaixo do título Música. Se você não deseja realizar buscas usando essa fonte de conteúdo, você pode desabilitar esse plugin. 