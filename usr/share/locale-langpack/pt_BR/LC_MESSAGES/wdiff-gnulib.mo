��             +         �     �  ,   �  ,     ,   ?  '   l  -   �      �  (   �  (        5     U     u     �     �     �  $   �               /     @     I  #   h     �     �     �     �     �     �     �            �  +     	  ,   (  2   U  ,   �  (   �  -   �  %   	  )   2	  )   \	      �	      �	     �	  )   �	     
     -
  ,   I
     v
     �
     �
     �
  #   �
  #         '     H  "   P     s     �     �     �     �     �                                                             	                                          
                                         %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid preceding regular expression Invalid range end Invalid regular expression Memory exhausted No match No previous regular expression Premature end of regular expression Regular expression too big Success Trailing backslash Unknown system error Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ memory exhausted Project-Id-Version: gnulib 3.0.0.6062.a6b16
Report-Msgid-Bugs-To: bug-gnulib@gnu.org
POT-Creation-Date: 2014-04-14 22:15+0200
PO-Revision-Date: 2016-02-07 18:42+0000
Last-Translator: Rafael Ferreira <Unknown>
Language-Team: Brazilian Portuguese <ldpbr-translation@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:40+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 %s: opção inválida -- "%c"
 %s: a opção "%c%s" não admite argumentos
 %s: a opção "%s" está ambígua; possibilidades: %s: a opção "--%s" não admite argumentos
 %s: a opção "--%s" exige um argumento
 %s: a opção "-W %s" não admite argumentos
 %s: a opção "-W %s" está ambígua
 %s: a opção "-W %s" exige um argumento
 %s: a opção exige um argumento -- "%c"
 %s: opção desconhecida "%c%s"
 %s: opção desconhecida "--%s"
 Retrorreferência inválida Nome inválido de categoria de caracteres Caractere de colagem inválido Conteúdo de \{\} inválido A expressão regular precedente é inválida Fim de intervalo inválido Expressão regular inválida Memória esgotada Nenhuma ocorrência do padrão Nenhuma expressão regular anterior Fim prematuro de expressão regular expressão regular grande demais Sucesso Barra invertida excedente ao final Erro desconhecido de sistema ( ou \( sem correspondente ) ou \) sem correspondente [ ou [^ sem correspondente \{ sem correspondente memória esgotada 