��    C      4  Y   L      �     �  3  �  C   �  "   <  3   _     �  D   �     �  $   �       B   <       L   �  X   �     ?	  7   G	  &   	  W   �	     �	     
     
  	   +
     5
     P
     o
  "   �
     �
  ,   �
     �
  +   �
  *        6     F      ]  (   ~     �  "   �     �  +   �  .     2   F     y  3   �  4   �     �  #        %  �   6  ]   �  �        �  Y   �          3     6  &   <     c     z  &   �  0   �  +   �          ,  +   F  +   r  +   �  �  �     �  r  �  4     "   E  >   h     �  D   �     �  +        <  H   [     �  Q   �  k        }  <   �  0   �  p   �     d     m     �  	   �     �     �     �  -   �       /   #     S  /   Y  2   �     �     �      �  )        0  !   5     W  6   r  :   �  ;   �        9   3  9   m     �  '   �     �  �   �  a   �  �   �     �  d   �          /     2  4   8     m     �  *   �  7   �  8         9  !   V  2   x  2   �  2   �               7   "           2   A      4          <                '   0   #   -   1                 C          ?      :             $   )      =   &   	              %           >         3   8       /      
   !              9               B      @   ;         6   +                        5   ,      (         .       *    
Terminated Jobs:
 
Version: %s (%s) %s %s %s

Usage: bat [-s] [-c config_file] [-d debug_level] [config_file]
       -c <file>   set configuration file to file
       -dnn        set debug level to nn
       -s          no signals
       -t          test - read configuration and exit
       -?          print this message.

  JobId  Level    Files      Bytes   Status   Finished        Name 
 %6d  %-6s %8s %10s  %-7s  %-8s %s
 %s item is required in %s resource, but not found.
 *None* ===================================================================
 Already connected"%s".
 At main prompt waiting for input ... At prompt waiting for input ... Attempt to define second %s resource named "%s" is not permitted.
 Authentication error : %s Authorization problem with Director at "%s:%d": Remote server requires TLS.
 Authorization problem: Remote server at "%s:%d" did not advertise required TLS support.
 Bacula  Bad response from File daemon to Hello command: ERR=%s
 Bad response to Hello command: ERR=%s
 Bad response to Hello command: ERR=%s
The Director at "%s:%d" is probably not running.
 Cancel Command completed ... Command failed. Connected Connecting to Client %s:%d Connecting to Director %s:%d

 Console: name=%s
 ConsoleFont: name=%s font face=%s
 Created Cryptography library initialization failed.
 Diffs Director at "%s:%d" rejected Hello command
 Director authorization problem at "%s:%d"
 Director daemon Director disconnected. Director rejected Hello command
 Director: name=%s address=%s DIRport=%d
 Error Error : BNET_HARDEOF or BNET_ERROR Error : Connection closed. Error sending Hello to File daemon. ERR=%s
 Error sending Hello to Storage daemon. ERR=%s
 Error, currentitem is not a Client or a Storage..
 Failed ASSERT: %s
 Failed to initialize TLS context for Console "%s".
 Failed to initialize TLS context for Director "%s".
 File daemon File daemon rejected Hello command
 Initializing ... Invalid refresh interval defined in %s
This value must be greater or equal to 1 second and less or equal to 10 minutes (read value: %d).
 Neither "TLS CA Certificate" or "TLS CA Certificate Dir" are defined for Console "%s" in %s.
 Neither "TLS CA Certificate" or "TLS CA Certificate Dir" are defined for Director "%s" in %s. At least one CA certificate store is required.
 No %s resource defined
 No Director resource defined in %s
Without that I don't how to speak to the Director :-(
 No record for %d %s
 OK Other Please correct configuration file: %s
 Processing command ... Storage daemon Storage daemon rejected Hello command
 TLS negotiation failed with Director at "%s:%d"
 TLS required but not configured in Bacula.
 Too many items in %s resource
 Unknown resource type %d
 Unknown resource type %d in dump_resource.
 Unknown resource type %d in free_resource.
 Unknown resource type %d in save_resource.
 Project-Id-Version: bacula
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-07-29 18:18+0200
PO-Revision-Date: 2015-01-08 15:07+0000
Last-Translator: Fábio Nogueira <fnogueira@protonmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:25+0000
X-Generator: Launchpad (build 18115)
 
Trabalhos concluídos:
 
Versão: %s (%s) %s %s %s

Use: bat [-s] [-c arquivo_de_configuração] [-d nível_de_depuração] [arquivo_de_configuração]
       -c <arquivo>   define configurações ao arquivo
       -dnn        define o nível de depuração para nn
       -s          sem sinais
       -t          test - Lê as configurações e sai
       -?          Imprime esta mensagem.

  JobId Nível Arquivos Bytes Estado Terminado Nome 
 %6d  %-6s %8s %10s  %-7s  %-8s %s
 %s ítem é requerido no recurso %s, mas não foi encontrado.
 *Nenhum* ===================================================================
 "%s" já está conectado.
 No prompt principal a espera de entrada ... Prompt aguardando por entrada. Tentativa de definir um segundo recuros %s chamado "%s" não permitida.
 Erro de autenticação: %s Problema de autorização com Director em "%s:%d": O servidor remoto requer TLS.
 Problema de autorização: o servidor remoto em "%s:%d" não informou a exigência de utilização de TLS.
 Bacula  Resposta errada do daemon File para o comando Hello: ERR=%s
 Resposta inválida para o comando Hello: ERR=%s
 Resposta defeituosa para o comando Hello: ERRO=%s
O Director em "%s:%d" provavelmente não está em execução.
 Cancelar Comando completado ... Falha no comando Conectado Conectando-se ao cliente %s:%d Conectando ao Diretor %s:%d

 Console: nome=%s
 Fonte do Console: Nome=%s Estilo de Fonte=%s
 Criado Falha ao iniciar a biblioteca de criptografia.
 Diffs O Director em "%s:%d" rejeitou o comando Hello
 Problema de autorização com Director em "%s:%d"
 Monitor de Diretor Diretor desconectado. Diretor rejeitou commando Hello
 Diretor: nome=%s endereço=%s DIRport=%d
 Erro Erro : BNET_HARDEOF ou BNET_ERROR Erro : Conexão cancelada. Erro ao enviar Hello para o daemon de Arquivo. ERR=%s
 Erro enviando um Hello ao daemon de Armazenamento. ERR=%s
 Erro, item atual não é um Cliente ou um Armazenamento...
 ASSERT falhou: %s
 Falha ao inicializar o contexto TLS para o Console "%s".
 Falha ao inicializar o contexto TLS para o Diretor "%s".
 Daemon Arquivo O daemon File rejeitou o comando Hello
 Inicializando ... Intervalo de atualização inválido definido em %s
Este valor deve ser maior ou igual a 1 segundo e inferior ou igual a 10 minutos (valor lido: %d).
 Nem "TLS CA Certificate" nem "TLS CA Certificate Dir" foram definidos para o Console "%s" em %s.
 Nem o "TLS CA Certificate" nem "TLS CA Certificate Dir" foram definidos para o Diretor "%s" em %s. Pelo menos um armazenamento de certificado CA certificate é requerido.
 Nenhum recurso %s definido
 Não há recurso de Diretor definido em %s
Sem isso eu não sei como me comunicar com o Diretor :-(
 Sem registro para %d %s
 OK Outro Por favor, corrigir o arquivo de configuração: %s
 Processando comando ... Daemon de armazenamento O deamon Storage rejeitou o comando Hello
 A negociação do TLS falhou com o Director em "%s:%d"
 O TLS é requirido, mas não foi configurado no Bacula.
 Muitos ítens no recurso %s
 Tipo de recurso %d desconhecido.
 Tipo de recurso %d desconhecido em dump_resource.
 Tipo de recurso %d desconhecido em free_resource.
 Tipo de recurso %d desconhecido em save_resource.
 