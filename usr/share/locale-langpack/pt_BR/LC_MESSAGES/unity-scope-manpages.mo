��    	      d      �       �      �      �      �      �   4        H  �   \     D  �  R          )     /      L  J   m     �  �   �     �        	                                      Manpages Open Search Manpages Search for Manpages Sorry, there are no Manpages that match your search. Technical Documents This is an Ubuntu search plugin that enables information from local manpages to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. manpages;man; Project-Id-Version: unity-scope-manpages
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-24 16:55+0000
PO-Revision-Date: 2014-07-23 18:20+0000
Last-Translator: Victor Teodoro <victorjst@hotmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Páginas do manual Abrir Pesquisar páginas do manual Pesquisar por páginas do manual Desculpe, não existem páginas do manual que correspondam a sua pesquisa. Documentos técnicos Este é um plugin de pesquisa do Ubuntu que permite que informações das páginas do manual local possam ser pesquisadas e exibidas pelo Dash. Se você não deseja pesquisar esse tipo de conteúdo, você pode desativar esse plugin de pesquisa. páginas do manual;manual; 