��          �            h     i     ~     �  %   �     �     �     �     �       	   %  H   /     x     |     �  �  �     6     V     _  3   d     �  $   �  $   �  &   �  &        3  _   C     �  
   �  	   �     	                                                              
             Bad format parameter Cancel No Not enough resources to create thread Ok Ouch!  Got SIGABRT, dying..
 Ouch!  Got SIGQUIT, dying..
 Ouch!  Got SIGSEGV, dying..
 Ouch!  Got SIGTERM, dying..
 TOP LEVEL Unable to load filename: the string %ls has no multibyte representation. Yes no_key yes_key Project-Id-Version: cwidget
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-22 00:06+0000
PO-Revision-Date: 2012-08-27 14:07+0000
Last-Translator: Adriano Steffler <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:26+0000
X-Generator: Launchpad (build 18115)
 Formato do parâmetro inválido Cancelar Não Não há recursos suficientes para criar uma thread Ok Ai! Recebi um SIGABRT, encerrando..
 Ai! Recebi um SIGQUIT, encerrando..
 Ops! Recebi um SIGSEGV, encerrando...
 Ops! Recebi um SIGTERM, encerrando...
 NÍVEL SUPERIOR Não foi possível carregar o nome do arquivo: a string %ls não tem representação multibyte. Sim chave_não chave_sim 