��    7      �  I   �      �     �     �     �     �  )   
     4  (   @     i          �  "   �  E   �       %   +     Q     Z     b  )   �     �  *   �     �  /   �     #  	   6  8   @  "   y     �     �     �     �     �     �       D   9  `   ~     �     �  )   	     A	  *   S	  #   ~	  	   �	     �	  D   �	  D   
     R
  !   q
  ,   �
  B   �
  
          '         >  +   _    �     �     �  %   �  +   �  (   (     Q  7   `     �      �     �     �  7   	     A  .   R     �     �  #   �  -   �     �  %     
   )      4     U     i  ?   p  '   �     �     �            +   :      f  #   �  :   �  d   �  !   K  #   m  2   �     �  /   �  1        6     H  ?   a  F   �      �  &   	  =   0  ?   n     �     �  -   �  )   �  *            -              0                	      1              4      %       '      *      6                 !   &          #         7   
   "       (      5   /   +              )   2                           ,   $         .            3                         (current) Unix password: Authentication failed. Can't set PAM_TTY=%s Cannot get username Causes the screensaver to exit gracefully Checking… Command to invoke from the logout button Don't become a daemon Enable debugging code Enter new Unix password: Error while changing NIS password. If the screensaver is active then deactivate it (un-blank the screen) Incorrect password. Launch screensaver and locker program Log _Out MESSAGE Message to show in the dialog No longer permitted to access the system. No password supplied Not permitted to gain access at this time. Not used Password has been already used. Choose another. Password unchanged Password: Query the length of time the screensaver has been active Query the state of the screensaver Retype new Unix password: S_witch User… Screensaver Show debugging output Show the logout button Show the switch user button Sorry, passwords do not match Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Time has expired. Turn the screensaver on (blank the screen) Unable to establish service %s: %s
 Username: Version of this application You are required to change your password immediately (password aged) You are required to change your password immediately (root enforced) You have the Caps Lock key on. You must choose a longer password You must wait longer to change your password Your account has expired; please contact your system administrator _Password: _Unlock failed to register with the message bus not connected to the message bus screensaver already running in this session Project-Id-Version: gnome-screensaver
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-04 14:27+0000
PO-Revision-Date: 2016-01-05 00:35+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
X-Poedit-Country: Brazil
X-Poedit-Language: Portuguese
 Senha UNIX (atual): Falha na autenticação. Não foi possível definir PAM_TTY=%s Não foi possível obter o nome de usuário Faz a proteção de tela sair suavemente Verificando… Comando invocado pelo botão de encerramento de sessão Não se tornar um daemon Habilitar depuração de código Digite a nova senha UNIX: Erro ao alterar a senha NIS. Se a proteção de tela estiver ativa então desative-a Senha incorreta. Executa o protetor de tela e bloqueia programa _Encerrar sessão MENSAGEM Mensagem a ser mostrada no diálogo Não é mais permitido ter acesso ao sistema. Senha não fornecida Dessa vez o acesso não é permitido. Não usado Senha já em uso. Escolha outra. Senha não alterada Senha: Consultar quanto tempo a proteção de tela está em execução Consulta o estado da proteção de tela Redigite a nova senha UNIX: _Trocar de usuário… Protetor de tela Mostrar saída de depuração Mostrar o botão de encerramento de sessão Mostrar o botão trocar usuário Desculpe, suas senhas não conferem Faz o processo da proteção bloquear a tela imediatamente A proteção de tela está ativa por %d segundo.
 A proteção de tela está ativa por %d segundos.
 A proteção de tela está ativa
 A proteção de tela está inativa
 A proteção de tela não está ativa atualmente.
 Tempo expirado. Ativa a proteção de tela (deixa a tela vazia) Não foi possível estabelecer o serviço %s: %s
 Nome de usuário: Versão deste aplicativo Você precisa alterar sua senha imediatamente (a senha expirou) Você precisa alterar sua senha imediatamente (determinação do root) A tecla Caps Lock está ativada. Você precisa escolher uma senha maior Você precisa aguardar um pouco mais para alterar a sua senha Sua conta expirou; por favor contate o administrador do sistema _Senha: _Desbloquear falha ao registrar no barramento de mensagens não conectado ao barramento de mensagens proteção de tela já ativa nesta sessão 