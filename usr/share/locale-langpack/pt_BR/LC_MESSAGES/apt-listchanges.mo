��    !      $  /   ,      �  $   �       /   *  g   Z     �     �  *   �       M        j     �  (   �     �     �     �     �     �        *   ,  �   W  ;   �       0   2  <   c  r   �  2        F     b  "   x     �     �  #   �  �  �     �	     �	  7   �	  }   6
  	   �
     �
  H   �
       g   3     �     �  *   �     �     �          (     :  1   R  (   �  �   �  C   C     �  C   �  :   �  �   "  7   �  "   �       *   (  "   S  '   v  *   �     !                                                                   	                                
                                                       %s: Version %s has already been seen %s: will be newly installed --since=<version> expects a only path to a .deb <big><b>Changelogs</b></big>

The following changes are found in the packages you are about to install: Aborting Changes for %s Confirmation failed, don't save seen state Continue Installation? Could not run apt-changelog (%s), unable to retrieve changelog for package %s Do you want to continue? [Y/n]  Done Ignoring `%s' (seems to be a directory!) Informational notes List the changes Mailing %s: %s News for %s Reading changelogs Reading changelogs. Please wait. The %s frontend is deprecated, using pager The gtk frontend needs a working python-gtk2 and python-glade2.
Those imports can not be found. Falling back to pager.
The error is: %s
 The mail frontend needs a installed 'sendmail', using pager Unknown frontend: %s
 Unknown option %s for --which.  Allowed are: %s. Usage: apt-listchanges [options] {--apt | filename.deb ...}
 Wrong or missing VERSION from apt pipeline
(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version set to 2?)
 You can abort the installation if you select 'no'. apt-listchanges: Changelogs apt-listchanges: News apt-listchanges: changelogs for %s apt-listchanges: news for %s database %s failed to load.
 didn't find any valid .deb archives Project-Id-Version: apt-listchanges-2.24
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-17 14:04+0000
PO-Revision-Date: 2016-02-18 04:47+0000
Last-Translator: Felipe Augusto van de Wiel (faw) <Unknown>
Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:57+0000
X-Generator: Launchpad (build 18115)
 %s: Versão %s já foi vista %s: será instalado como novo --since=<version> esperado apenas o caminho para o .deb <big><b>Registros de Mudanças</b></big>

As seguintes mudanças foram encontradas nos pacotes que você está para instalar: Abortando Mudanças para %s Confirmação falhou, não salve o estado de visualizados ("seen state") Continuar a instalação? Não foi possível executar apt-changelog (%s), incapaz de obter o registro de operações do pacote %s Você deseja continuar? [Y/n]  Feito Ignorando '%s' (parece ser um diretório!) Notas informativas Lista de mudanças Enviando e-mail %s: %s Notícias para %s Lendo logs de mudanças Lendo registros de mudanças. Por favor, aguarde. A interface %s é obsoleta, usando pager A interface gtk precisa do python-gtk2 e python-glade2 funcionando.
Estas extensões não puderam ser encontradas. Optando pelo pager.
O erro é: %s
 A interface e-mail precisa de um 'sendmail' instalado, usando pager Interface desconhecida: %s
 Opção desconhecida %s para --which. Opções permitidas são: %s. Uso: apt-listchanges [opções] {--apt | arquivo.deb ...}
 VERSÃO ("VERSION") incorreta ou faltando a partir do "pipeline" apt
(o Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version está como 2?)
 Você pode abortar a instalação se selecionar 'não'. apt-listchanges: Logs de mudanças apt-listchanges: Notícias apt-listchanges: logs de mudanças para %s apt-listchanges: notícias para %s falha ao carregar o banco de dados %s.
 não encontrou nenhum arquivo .deb válido 