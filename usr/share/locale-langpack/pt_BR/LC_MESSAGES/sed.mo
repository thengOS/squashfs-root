��    \      �     �      �  �   �  ,   �  5   	  N   7	  7   �	  \   �	  _   
  `   {
  u   �
  i   R  b   �  V     Y   v  ~   �  �   O  D   �  %   $     J     a  5   {  B   �     �  e     w   w     �          #     ?  $   W     |     �     �     �     �     �     �     �  #   	     -     H     P     Y     l     ~     �     �  H   �     �       !   1     S     h  (   }     �     �  #   �     �       $   5     Z     y  #   �  B   �  2   �     -      A     b     �     �  *   �  *   �          (     8  #   F  #   j  &   �     �     �  ,   �          )  -   >     l     {     �     �     �     �     �     �      L    )   `  <   �  c   �  =   +  `   i  _   �  d   *  �   �  s     c   �  \   �  f   S  �   �  �   I  F   �  *   >   "   i      �   6   �   G   �      )!  a   D!  o   �!     "  &   ."  "   U"  <   x"  %   �"  "   �"     �"     #     (#     4#     F#  $   V#     {#  #   �#      �#     �#     �#     �#     �#     $     $     2$  I   D$     �$     �$  .   �$  !   �$  "   %  I   8%  $   �%  $   �%  $   �%  #   �%  1   &  ;   G&  '   �&  )   �&  5   �&  `   '  1   l'     �'  9   �'  (   �'  '   (     =(  ,   P(  +   }(  %   �(  )   �(     �(  *   )  *   A)  9   l)     �)  $   �)  A   �)     '*     B*  5   \*     �*     �*     �*  /   �*     +     +  1   2+  =   d+           C          	   5             !   <      J   +                 R   #   
   3   L   S           E   M       *   B                     F      :   X      2   I                 H   .                 >                  ?       %          $   Q   9               T   4               1       [                -             V       @              G   =       ,   U   \   O   7   N      )   &           P   (   ;   A   Z       W   '   /   0       K   D      8   Y   "   6          
If no -e, --expression, -f, or --file option is given, then the first
non-option argument is taken as the sed script to interpret.  All
remaining arguments are names of input files; if no input files are
specified, then the standard input is read.

       --help     display this help and exit
       --version  output version information and exit
   --follow-symlinks
                 follow symlinks when processing in place
   --posix
                 disable all GNU extensions.
   -R, --regexp-perl
                 use Perl 5's regular expressions syntax in the script.
   -b, --binary
                 open files in binary mode (CR+LFs are not processed specially)
   -e script, --expression=script
                 add the script to the commands to be executed
   -f script-file, --file=script-file
                 add the contents of script-file to the commands to be executed
   -i[SUFFIX], --in-place[=SUFFIX]
                 edit files in place (makes backup if SUFFIX supplied)
   -l N, --line-length=N
                 specify the desired line-wrap length for the `l' command
   -n, --quiet, --silent
                 suppress automatic printing of pattern space
   -r, --regexp-extended
                 use extended regular expressions in the script.
   -s, --separate
                 consider files as separate rather than as a single continuous
                 long stream.
   -u, --unbuffered
                 load minimal amounts of data from the input files and flush
                 the output buffers more often
   -z, --null-data
                 separate lines by NUL characters
 %s: -e expression #%lu, char %lu: %s
 %s: can't read %s: %s
 %s: file %s line %lu: %s
 %s: warning: failed to get security context of %s: %s %s: warning: failed to set default file creation context to %s: %s : doesn't want any addresses E-mail bug reports to: <%s>.
Be sure to include the word ``%s'' somewhere in the ``Subject:'' field.
 GNU sed home page: <http://www.gnu.org/software/sed/>.
General help using GNU software: <http://www.gnu.org/gethelp/>.
 Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid preceding regular expression Invalid range end Invalid regular expression Jay Fenlason Ken Pizzini Memory exhausted No match No previous regular expression Paolo Bonzini Premature end of regular expression Regular expression too big Success Tom Lord Trailing backslash Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ Usage: %s [OPTION]... {script-only-if-no-other-script} [input-file]...

 `e' command not supported `}' doesn't want any addresses can't find label for jump to `%s' cannot remove %s: %s cannot rename %s: %s cannot specify modifiers on empty regexp cannot stat %s: %s command only uses one address comments don't accept any addresses couldn't attach to %s: %s couldn't edit %s: is a terminal couldn't edit %s: not a regular file couldn't follow symlink %s: %s couldn't open file %s: %s couldn't open temporary file %s: %s couldn't write %d item to %s: %s couldn't write %d items to %s: %s delimiter character is not a single-byte character error in subprocess expected \ after `a', `c' or `i' expected newer version of sed extra characters after command incomplete command invalid reference \%d on `s' command's RHS invalid usage of +N or ~N as first address invalid usage of line address 0 missing command multiple `!'s multiple `g' options to `s' command multiple `p' options to `s' command multiple number options to `s' command no input files no previous regular expression number option to `s' command may not be zero option `e' not supported read error on %s: %s strings for `y' command are different lengths unexpected `,' unexpected `}' unknown command: `%c' unknown option to `s' unmatched `{' unterminated `s' command unterminated `y' command unterminated address regex Project-Id-Version: sed 4.2.1
Report-Msgid-Bugs-To: bug-gnu-utils@gnu.org
POT-Creation-Date: 2012-12-22 14:36+0100
PO-Revision-Date: 2013-09-11 20:45+0000
Last-Translator: Vinicius Almeida <vinicius.algo@gmail.com>
Language-Team: Brazilian Portuguese <ldp-br@bazar.conectiva.com.br>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 17:17+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 
Se nenhuma opção -e, --expression, -f, ou --file é dada, o primeiro
argumento que não seja uma opção é considerado como o script sed a ser
interpretado. Todos os argumentos restantes são considerados como
nomes de arquivos de entrada. Caso nenhum arquivo de entrada seja
especificado, então a entrada padrão será lida.

       --help     mostra esta ajuda e sai
       --version  mostra informações sobre a versão e sai
   --follow-symlinks
                 segue links simbólicos ao editar o próprio arquivo original
   --posix
                 desativa todas as extensões GNU.
   -R, --regexp-perl
                 usar sintaxe de expressões regulares do Perl 5 no script.
   -b, --binary
                 abre os arquivos em modo binário (CR+LFs não são especiais)
   -e script, --expression=script
                 adiciona o script aos comandos a serem executados
   -f script-file, --file=script-file
                 adiciona o conteúdo do arquivo-script aos comandos
                 a serem executados
   -i[SUFIXO], --in-place[=SUFIXO]
                 editar arquivos no local (faça backup se SUFIXO for fornecido)
   -l N, --line-length=N
                 determina comprimento da quebra de linha para comando `l'
   -n, --quiet, --silent
                 suprime a impressão automática do buffer padrão
   -r, --regexp-extended
                 usar sintaxe moderna de expressões regulares (sem escapes).
   -s, --separate
                 considera arquivos como entidades separadas, e não como um
                 longo e único fluxo de dados.
   -u, --unbuffered
                 carrega uma quantidade mínima de dados dos arquivos de entrada
                 e descarrega os buffers de saída com mais freqüência
   -z, --null-data
                 separa linhas por caracteres NULOS
 %s: -e expressão #%lu, caractere %lu: %s
 %s: não foi possível ler %s: %s
 %s: arquivo %s linha %lu: %s
 %s: aviso: falha ao obter o security context de %s: %s %s: aviso: falha ao definir o default file creation context para %s: %s `:' não recebe endereços Envie relatórios de erros (em inglês) para: %s.
Inclua a palavra ``%s'' no campo ``Assunto:''.
 Site do GNU sed: <http://www.gnu.org/software/sed/>.
Ajuda sobre softwares GNU: <http://www.gnu.org/gethelp/>.
 Retrovisor \n inválido Nome inválido de classe de caracteres Caractere de ordenação inválido Conteúdo inválido no \{\} (permitidos números e vírgula) Expressão regular anterior inválida Fim de intervalo (range) inválido Expressão regular inválida Jay Fenlason Ken Pizzini Falta de memória Nada encontrado Não há expressão regular anterior Paolo Bonzini Fim prematuro da expressão regular Expressão regular grande demais Sucesso Tom Lord Escape \ no final ( ou \( não terminado ) or \) inesperado [ ou [^ não terminado \{ não terminado Uso: %s [OPÇÃO]... {script-apenas-se-for-único} [arquivo-entrada]...

 comando `e' não suportado `}' não recebe endereços Não foi possível encontrar a marcação `%s' Não foi possível remover %s: %s não foi possível renomear %s: %s não é permitido especificar modificadores numa expressão regular vazia não foi possível fazer stat %s: %s Este comando usa apenas um endereço Comentários não aceitam endereços não foi possível anexar ao %s: %s não foi possível editar %s, pois é um terminal não foi possível editar %s, pois não é um arquivo comum Não foi possível seguir o link %s: %s não foi possível abrir o arquivo %s: %s Não foi possível abrir o arquivo temporário %s: %s não foi possível escrever %d item para %s: %s não foi possível escrever %d items para %s: %s O delimitador deve ser um caractere normal, ASCII erro no subprocesso Deve haver um escape \ depois dos comandos `a', `c' e `i' Esperada uma versão mais recente do sed Há caracteres sobrando após o comando comando incompleto referência inválida \%d no comando RHS `s' Não use +N ou ~N como o primeiro endereço Uso incorreto do endereço de linha 0 Falta especificar um comando ao endereço Exclamações `!' múltiplas opções `g' múltiplas para o comando `s' opções `p' múltiplas para o comando `s' opções numéricas múltiplas para o comando `s' (s///n) nenhum arquivo de entrada não há expressão regular anterior A opção numérica para o comando `s' não pode ser zero (s///0) opção `e' não suportada erro de leitura em %s: %s os textos para o comando `y' têm tamanhos diferentes Vírgula `,' inesperada `}' inesperada comando desconhecido: `%c' Opção desconhecida para o comando `s' (s///?) `{' não terminada comando `s' não terminado Comando `y' inacabado (y/// - faltou delimitador) A expressão regular do endereço está inacabada (falta a /) 