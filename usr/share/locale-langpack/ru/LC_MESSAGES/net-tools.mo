��    �     �  k        �&  P   �&  9   '  J   T'  A   �'  H   �'  +   *(     V(     s(  6   �(  G   �(  B   )     K)     e)  :   �)  :   �)  :   �)  9   2*  G   l*  A   �*  O   �*  B   F+  8   �+  P   �+  J   ,  5   ^,  :   �,  @   �,  Q   -  E   b-  G   �-  9   �-  B   *.  5   m.  0   �.  F   �.  7   /  5   S/  K   �/  ,   �/  $   0  (   '0  $   P0  $   u0  L   �0  B   �0  ?   *1  D   j1  2   �1  B   �1  1   %2  L   W2  K   �2      �2  *   3  4   <3  N   q3  !   �3  G   �3  F   *4  K   q4  A   �4  K   �4  O   K5  U   �5      �5  K   6  &   ^6  *   �6  0   �6  5   �6  +   7  *   C7  ,   n7  @   �7  P  �7  #   -9  9   Q9  .   �9     �9  C   �9  N   :  +   d:     �:      �:  =   �:  %   ;  7   1;  #   i;     �;  ;   �;     �;  )   �;      "<      C<  1   d<  ?   �<     �<  ?   �<  "   3=     V=     k=  P   }=  Q   �=  P    >  P   q>  
   �>  	   �>  
   �>     �>     �>     �>     �>     �>     ?     $?  	   +?     5?     Q?     f?  
   r?     }?  ,   �?  ,   �?  ;   �?  "   3@     V@     p@  !   �@     �@     �@     �@  /   �@  -   A  -   JA     xA     �A     �A     �A  1   �A  7   B     =B     UB  /   iB  /   �B     �B  4   �B     C     7C  %   UC     {C  *   �C  +   �C  8   �C     &D  #   ;D     _D     }D     �D     �D  "   �D     �D     �D     E     !E     ?E     ]E     }E     �E     �E     �E     �E  K   F      MF  7   nF  $   �F  E   �F     G     +G  B   ?G     �G  $   �G     �G  5   �G     H     )H     BH  1   WH     �H     �H     �H     �H     �H  +   �H     I  :   ;I     vI     �I     �I     �I     �I  G   �I  }   0J     �J     �J     �J     �J     �J     �J     K     )K     >K     OK  
   ]K     hK  	   uK     K  %   �K     �K  b   �K     %L     BL     ZL     vL     �L  Q   �L  /   �L  #   %M     IM     WM     [M  ,   iM  
   �M     �M  
   �M     �M  	   �M  	   �M  
   �M     �M     �M     �M  	   N     N     N  M   -N  C   {N  O   �N  N   O  1   ^O     �O  /   �O     �O     �O     �O  "   �O     P  	   P  	    P     *P     /P  ,   KP  -   xP     �P     �P     �P     �P     �P  "   �P     Q     Q     Q  +   4Q  ,   `Q     �Q  #   �Q     �Q     �Q     �Q  O   �Q  N   7R     �R     �R     �R     �R     �R     �R     S     $S     >S     YS     qS     �S  (   �S     �S     �S     �S  	   �S     �S     �S     T     T  -   T  '   MT  "   uT  W   �T  y   �T     jU  $   U  &   �U  $   �U  %   �U  "   V     9V  
   YV  2   dV     �V     �V  %   �V     �V     �V     �V  $   W     )W  <   =W  7   zW     �W     �W     �W     �W  	   X  	   X     !X     (X     1X     :X     IX     eX     X     �X     �X  	   �X  <   �X  #   �X  #   Y     ?Y     KY     PY     XY     _Y     cY     kY     �Y  W   �Y  @   �Y  F   7Z  $   ~Z  W   �Z  7   �Z  +   3[  D   _[  H   �[  N   �[     <\     T\  *   f\  .   �\  /   �\  -   �\  /   ]  1   N]  S   �]  .   �]     ^  &   ^  (   A^  
   j^     u^  
   �^  	   �^     �^     �^     �^  ,   �^  &   _  !   6_      X_     y_      �_  3   �_  '   �_  )   `  /   <`  #   l`     �`     �`     �`     �`  0   �`     /a     Ja     Za     va  @   a     �a     �a     �a     �a     �a  
   b     $b     1b     Gb     [b  <   ob  7   �b  ,   �b  &   c     8c     Sc     lc     �c     �c  )   �c     �c     �c     �c     �c     d     $d  !   Bd     dd     |d  "   �d     �d     �d     �d  ,   �d     #e     <e     Ye     qe  +   �e  (   �e     �e     �e     f     f     .f     Df     Xf     nf     �f  )   �f     �f     �f  N   �f  !   5g  "   Wg     zg     �g     �g     �g     �g  �  h  >   �i  %   �i  �   j  A   �j  H   �j  +   %k     Qk     nk  6   �k  G   �k  B   l     Fl     `l  n   |l  d   �l  e   Pm  T   �m  ]   n  b   in  u   �n  c   Bo  P   �o  a   �o  �   Yp  <   �p  J   q  Y   bq  c   �q  b    r  e   �r  T   �r  f   >s  U   �s  8   �s  �   4t  S   �t  C   u  `   Xu  ?   �u  $   �u  '   v  #   Fv  #   jv  w   �v  j   w  C   qw  g   �w  F   x  D   dx  E   �x  L   �x  K   <y      �y  *   �y  4   �y  q   	z  !   {z  F   �z  X   �z  J   ={     �{  s   |  [   ||  v   �|  2   O}  v   �}  (   �}  >   "~  G   a~  ?   �~  &   �~  ;     "   L  @   o    �  3   ��  I   �  >   ;�     z�  �   ��  N   �  ]   k�  G   Ƀ  Q   �  �   c�  I   �  g   8�  G   ��  I   �  @   2�     s�  '   ��  5   ��  5   �  2   �  <   J�     ��  F   ��  "   �     �     "�  a   4�  Q   ��  f   �     O�  
   ω  	   ډ  
   �  
   �     ��     �     �  /   *�     Z�     r�     {�     ��     ��     Ҋ     ފ     ��  Q   �  W   `�  r   ��  G   +�  =   s�  -   ��  >   ߌ  :   �  $   Y�  )   ~�  �   ��  �   :�  �   Ύ     V�  .   r�  ,   ��  *   Ώ  `   ��  O   Z�     ��     ɐ  /   �  /   �  :   H�  p   ��  4   ��  8   )�  M   b�  /   ��  `   ��  V   A�  s   ��  D   �  N   Q�  :   ��  4   ۔     �  '   0�  F   X�  )   ��  6   ɕ  8    �  5   9�  ?   o�  1   ��  (   �  6   
�  :   A�  :   |�  ,   ��  K   �  T   0�  �   ��  G   �  �   g�  6   ��  !   -�     O�  !   Ϛ  F   �  <   8�  r   u�  1   �  6   �  )   Q�  ]   {�  !   ٜ  6   ��  '   2�  8   Z�  +   ��  +   ��  D   �  �   0�  +   ��  )   �  /   �  +   A�     m�  �   z�  5  &�  
   \�     g�     ��     ��     ��     ��  #   ϡ     �     �     '�  
   B�     M�  	   Z�     d�  W   k�  "   â  Y   �  >   @�  &   �  0   ��  #   ף  #   ��  L   �  [   l�  J   Ȥ     �     !�     %�  i   C�  
   ��     ��  
   ��     ɥ  	   ѥ  	   ۥ  
   �  .   �     �     .�  	   4�     >�     L�  1   k�  ,   ��  8   ʦ  7   �  -   ;�  .   i�  S   ��     �     �     ��  =   �     C�  	   L�  	   V�     `�  2   e�  ^   ��  _   ��  !   W�  !   y�  
   ��     ��     ��  D   ��     ��  &   �  (   )�  >   R�  M   ��     ߪ  U   �  &   I�     p�     u�  H   ��  N   ˫  )   �  :   D�     �     ��  <   ��  6   Ь  G   �  7   O�  8   ��  /   ��  >   �  ;   /�  =   k�     ��     ��     ��  	   ��     Ȯ  (   ͮ     ��     ��  `   �  Y   v�  4   Я  +   �  B   1�  /   t�  [   ��  C    �  A   D�  >   ��  i   ű  I   /�  
   y�  v   ��  .   ��  3   *�  k   ^�     ʳ     γ     ҳ  9   ۳     �  2   -�  c   `�  "   Ĵ  %   �  #   �      1�  	   R�  	   \�     f�     m�     v�     �  <   ��  @   ˵  4   �     A�  /   J�  	   z�  1   ��  ,   ��  \   �     @�     L�     Q�     Y�     `�     d�  G   {�  0   ÷  �   ��  i   ��  �   �  ;   ��  l   Ϲ  N   <�  I   ��  M   պ  �   #�  2   ��     ׻     �  \   �  o   a�  p   Ѽ  n   B�  p   ��  r   "�  �   ��  d   L�     ��  ?   ɿ  ]   	�     g�     }�  *   ��     ��  *   ��  ,   �  *   0�  H   [�  @   ��  7   ��  @   �  4   ^�  7   ��  b   ��  \   .�  O   ��  C   ��  D   �  >   d�  I   ��  9   ��  ;   '�  W   c�  2   ��     ��  8   ��     7�  f   J�     ��     ��     ��     ��  *   �     ;�     R�     r�     ��     ��  �   ��  s   @�  ^   ��  9   �  *   M�     x�  E   ��     ��  "   ��  Q   �  1   j�     ��     ��     ��     ��  3   ��  A   �  *   S�  :   ~�  H   ��  +   �  "   .�  "   Q�  ]   t�  !   ��  0   ��  "   %�  7   H�  l   ��  R   ��  )   @�  0   j�  !   ��  .   ��  .   ��  ,   �  .   H�  0   w�     ��  0   ��     ��     �  [   �  L   {�  M   ��  H   �  H   _�  H   ��  I   ��  '   ;�     "  �        �   �   �   �   r          M    �             -  �  �   �  �   �   �       k           s  7  �   �  �      j  �       �  �           �  P  |  V     1       Z       V  J  �       ;  :  q          K        �  �  G   �   �   (    �  Z     &       #           	  �   h   �  �   �                 �       �          R   �   �     �  )  �   �   �  y  L   �      z   �  T   �       �  �        ]   �   ]  �  0  �          �   x   �      �          p      :   �  �      �   2   =   �   �  �   �      .       �       |   .  �   �  o  @   3  !   <   �   \  _  I   4  �   �           �   �  �        �   A   <           2      5   /      +   '   O   �  �  �   e   �   ?           �   w              �   i           �   *              ^   0       {   M   �      �      �      *      �  �   �       �       �   �   �   �  �       �  �       �  �  �  �   �  {  5  Y   �     �   l   c         `   g   �   �  �         (   C  3       �              �   1  f                       ,  �  b        �   B  �  S   �      �   �  +  �       c          �  J       �       �   �   N         �   ;             �   �           �      �           m      �   �  f      �  �       4       �   �       �   �       h      �  �     )   l          v  Q              C       �  p   �      �           �  ?   ~   �   6  �   �   �   	               y   d       �  x  t      �   �   �     �   i         �      �       U   �   �   n    �        6   B       �  �  E       �       �  �   �   [   �     ^  �         �     �  �      H   �   d     �   �       z      �  g          s   A  �   /          �    �   #          u  R      [       �   @  W   K           b   �      �   \   $  F   8   �  t   �       o                   �  9  X  %   �  X   q   �   _   ,     �       �  v           �   O  �  �   P   >   T  Q  k  �   E  m   D   �   �       G      !  �       &      �  L      N   -   F      �      w   j   �  �   �  �  W          �   Y  "   �           }   7           r  �  %        �           D      a       �  �   I  $       �  a          9   `        u      e  =  ~  }      �              �   �   n   U      8  �      �      H  
  '  S  �   �  �   �  �     �       
          �      �       �           >  �   �  �  �   �    
Proto Recv-Q Send-Q Local Address           Foreign Address         State       
Proto RefCnt Flags       Type       State         I-Node 
Unless you are using bind or NIS for host lookups you can change the DNS
                               [mod] [dyn] [reinstate] [[dev] If]
                               [netmask N] [mss Mss] [window W] [irtt I]
                   [nibble NN] [trigger NN]
              compressed:%lu
           %s addr:%s            [ [i|o]seq ] [ [i|o]key KEY ] [ [i|o]csum ]
           [ mode { ipip | gre | sit } ] [ remote ADDR ] [ local ADDR ]
           [ ttl TTL ] [ tos TOS ] [ nopmtudisc ] [ dev PHYS_DEV ]
           collisions:%lu            inet6 addr: %s/%d         --numeric-hosts          don't resolve host names
         --numeric-ports          don't resolve port names
         --numeric-users          don't resolve user names
         -A, -p, --protocol       specify protocol family
         -C, --cache              display routing cache instead of FIB

         -D, --use-device         read <hwaddr> from given device
         -F, --fib                display Forwarding Information Base (default)
         -M, --masquerade         display masqueraded connections

         -N, --symbolic           resolve hardware names
         -a                       display (all) hosts in alternative (BSD) style
         -a, --all, --listening   display all sockets (default: connected)
         -c, --continuous         continuous listing

         -d, --delete             delete a specified entry
         -e, --extend             display other/more information
         -f, --file               read new entries from file or from /etc/ethers

         -g, --groups             display multicast group memberships
         -i, --device             specify network interface (e.g. eth0)
         -i, --interfaces         display interface table
         -l, --listening          display listening server sockets
         -n, --numeric            don't resolve names
         -o, --timers             display timers
         -p, --programs           display PID/Program name for sockets
         -r, --route              display routing table
         -s, --set                set a new ARP entry
         -s, --statistics         display networking statistics (like SNMP)
         -v, --verbose            be verbose
        ADDR := { IP_ADDRESS | any }
        KEY  := { DOTTED_QUAD | NUMBER }
        TOS  := { NUMBER | inherit }
        TTL  := { 1..255 | inherit }
        domainname [-v] {nisdomain|-F file}   set NIS domainname (from file)
        hostname -V|--version|-h|--help       print info and exit

        hostname [-v]                         display hostname

        hostname [-v] [-d|-f|-s|-a|-i|-y|-n]  display formatted name
        inet6_route [-FC] flush      NOT supported
        inet6_route [-vF] add Target [gw Gw] [metric M] [[dev] If]
        inet_route [-FC] flush      NOT supported
        inet_route [-vF] add {-host|-net} Target[/prefix] [gw Gw] [metric M]
        inet_route [-vF] add {-host|-net} Target[/prefix] [metric M] reject
        iptunnel -V | --version

        netstat [-vnNcaeol] [<Socket> ...]
        netstat { [-veenNac] -i | [-cnNe] -M | -s }

        nodename [-v] {nodename|-F file}      set DECnet node name (from file)
        plipconfig -V | --version
        rarp -V                               display program version.

        rarp -d <hostname>                    delete entry from cache.
        rarp -f                               add entries from /etc/ethers.
        rarp [<HW>] -s <hostname> <hwaddr>    add entry to cache.
        route [-v] [-FC] {add|del|flush} ...  Modify routing table for AF.

        route {-V|--version}                  Display version/author and exit.

        route {-h|--help} [<AF>]              Detailed usage syntax for specified AF.
      - no statistics available -     -F, --file            read hostname or NIS domainname from given file

     -a, --alias           alias names
     -d, --domain          DNS domain name
     -f, --fqdn, --long    long host name (FQDN)
     -i, --ip-address      addresses for the hostname
     -n, --node            DECnet node name
     -s, --short           short host name
     -y, --yp, --nis       NIS/YP domainname
     dnsdomainname=hostname -d, {yp,nis,}domainname=hostname -y

    This command can read or set the hostname or the NIS domainname. You can
   also read the DNS domain or the FQDN (fully qualified domain name).
   Unless you are using bind or NIS for host lookups you can change the
   FQDN (Fully Qualified Domain Name) and the DNS domain name (which is
   part of the FQDN) in the /etc/hosts file.
   <AF>=Address family. Default: %s
   <AF>=Use '-6|-4' or '-A <af>' or '--<af>'; default: %s
   <AF>=Use '-A <af>' or '--<af>'; default: %s
   <HW>=Hardware Type.
   <HW>=Use '-H <hw>' to specify hardware address type. Default: %s
   <Socket>={-t|--tcp} {-u|--udp} {-w|--raw} {-x|--unix} --ax25 --ipx --netrom
   Checksum in received packet is required.
   Checksum output packets.
   Drop packets out of sequence.
   List of possible address families (which support routing):
   List of possible address families:
   List of possible hardware types (which support ARP):
   List of possible hardware types:
   Sequence packets on output.
   [[-]broadcast [<address>]]  [[-]pointopoint [<address>]]
   [[-]dynamic]
   [[-]trailers]  [[-]arp]  [[-]allmulti]
   [add <address>[/<prefixlen>]]
   [del <address>[/<prefixlen>]]
   [hw <HW> <address>]  [metric <NN>]  [mtu <NN>]
   [mem_start <NN>]  [io_addr <NN>]  [irq <NN>]  [media <type>]
   [multicast]  [[-]promisc]
   [netmask <address>]  [dstaddr <address>]  [tunnel <address>]
   [outfill <NN>] [keepalive <NN>]
   [txqueuelen <NN>]
   [up|down] ...

   arp [-v]          [-i <if>] -d  <host> [pub]               <-Delete ARP entry
   arp [-v]   [<HW>] [-i <if>] -Ds <host> <if> [netmask <nm>] pub          <-''-

   arp [-v]   [<HW>] [-i <if>] -s  <host> <hwaddr> [temp]            <-Add entry
   arp [-vnD] [<HW>] [-i <if>] -f  [<filename>]            <-Add entry from file
  Bcast:%s   Mask:%s
  P-t-P:%s   Path
  Scope:  Timer  User  User       Inode       interface %s
  on %s  users %d %s	nibble %lu  trigger %lu
 %s (%s) -- no entry
 %s (%s) at  %s started %s: %s/ip  remote %s  local %s  %s: ERROR while getting interface flags: %s
 %s: ERROR while testing interface flags: %s
 %s: You can't change the DNS domain name with this command
 %s: address family not supported!
 %s: bad hardware address
 %s: can't open `%s'
 %s: hardware type not supported!
 %s: illegal option mix.
 %s: invalid %s address.
 %s: name too long
 %s: you must be root to change the domain name
 %s: you must be root to change the host name
 %s: you must be root to change the node name
 %u DSACKs received %u ICMP messages failed %u ICMP messages received %u ICMP messages sent %u ICMP packets dropped because socket was locked %u ICMP packets dropped because they were out-of-window %u SYN cookies received %u SYN cookies sent %u TCP sockets finished time wait in fast timer %u TCP sockets finished time wait in slow timer %u active connections openings %u active connections rejected because of time stamp %u bad segments received. %u connection resets received %u connections aborted due to timeout %u connections established %u connections reset due to unexpected SYN %u connections reset due to unexpected data %u delayed acks further delayed because of locked socket %u delayed acks sent %u dropped because of missing route %u failed connection attempts %u fast retransmits %u forwarded %u fragments created %u fragments dropped after timeout %u fragments failed %u fragments received ok %u incoming packets delivered %u incoming packets discarded %u input ICMP message failed. %u invalid SYN cookies received %u other TCP timeouts %u outgoing packets dropped %u packet headers predicted %u packet reassembles failed %u packet receive errors %u packets dropped from out-of-order queue because of socket buffer overrun %u packets dropped from prequeue %u packets header predicted and directly queued to user %u packets pruned from receive queue %u packets pruned from receive queue because of socket buffer overrun %u packets reassembled ok %u packets received %u packets rejects in established connections because of timestamp %u packets sent %u packets to unknown port received. %u passive connection openings %u passive connections rejected because of time stamp %u predicted acknowledgments %u reassemblies required %u requests sent out %u resets received for embryonic SYN_RECV sockets %u resets sent %u retransmits lost %u segments received %u segments retransmited %u segments send out %u time wait sockets recycled by time stamp %u timeouts after SACK recovery %u times recovered from packet loss due to fast retransmit %u total packets received %u with invalid addresses %u with invalid headers %u with unknown protocol (Cisco)-HDLC (No info could be read for "-p": geteuid()=%d but you should be root.)
 (Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
 (auto) (incomplete) (only servers) (servers and established) (w/o servers) 16/4 Mbps Token Ring 16/4 Mbps Token Ring (New) 6-bit Serial Line IP <from_interface> <incomplete>  AMPR AX.25 AMPR NET/ROM AMPR ROSE ARCnet AX.25 not configured in this system.
 Active AX.25 sockets
 Active IPX sockets
Proto Recv-Q Send-Q Local Address              Foreign Address            State Active Internet connections  Active NET/ROM sockets
 Active UNIX domain sockets  Active X.25 sockets
 Adaptive Serial Line IP Address                  HWtype  HWaddress           Flags Mask            Iface
 Address deletion not supported on this system.
 Address family `%s' not supported.
 Appletalk DDP Ash Bad address.
 Broadcast tunnel requires a source address.
 CCITT X.25 CLOSE CLOSE_WAIT CLOSING CONN SENT CONNECTED CONNECTING Cannot create socket DARPA Internet DGRAM DISC SENT DISCONNECTING Default TTL is %u Dest         Source          Device  LCI  State        Vr/Vs  Send-Q  Recv-Q
 Dest       Source     Device  State        Vr/Vs    Send-Q  Recv-Q
 Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
 Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
 Destination  Mnemonic  Quality  Neighbour  Iface
 Device not found Don't know how to set addresses for family %d.
 ESTAB ESTABLISHED Econet Entries: %d	Skipped: %d	Found: %d
 Ethernet FIN_WAIT1 FIN_WAIT2 FREE Failed to get type of [%s]
 Flushing `inet' routing table not supported
 Flushing `inet6' routing table not supported
 Forwarding is %s Generic EUI-64 Global HIPPI HWaddr %s   Hardware type `%s' not supported.
 Host ICMP input histogram: ICMP output histogram: INET (IPv4) not configured in this system.
 INET6 (IPv6) not configured in this system.
 IPIP Tunnel IPX not configured in this system.
 IPv4 Group Memberships
 IPv6 IPv6-in-IPv4 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR    TX-OK TX-ERR TX-DRP TX-OVR Flg
 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR   TX-OK TX-ERR TX-DRP TX-OVR Flg
 Interface       RefCnt Group
 Interface %s not initialized
 Interrupt:%d  IrLAP Kernel AX.25 routing table
 Kernel IP routing cache
 Kernel IP routing table
 Kernel IPX routing table
 Kernel IPv6 routing table
 Kernel Interface table
 Kernel NET/ROM routing table
 Kernel ROSE routing table
 Keys are not allowed with ipip and sit.
 LAPB LAST_ACK LISTEN LISTENING Link Local Loopback Media:%s Memory:%lx-%lx  Modifying `inet' routing cache not supported
 NET/ROM not configured in this system.
 NET/ROM: this needs to be written
 Neighbour                                   HW Address        Iface    Flags Ref State
 Neighbour                                   HW Address        Iface    Flags Ref State            Stale(sec) Delete(sec)
 No ARP entry for %s
 No routing for address family `%s'.
 No support for ECONET on this system.
 No support for INET on this system.
 No support for INET6 on this system.
 No usable address families found.
 Node address must be ten digits Novell IPX Please don't supply more than one address family.
 Point-to-Point Protocol Problem reading data from %s
 Quick ack mode was activated %u times RAW RDM RECOVERY ROSE not configured in this system.
 RTO algorithm is %s RX: Packets    Bytes        Errors CsumErrs OutOfSeq Mcasts
 Ran %u times out of system memory during packet sending Resolving `%s' ...
 Result: h_addr_list=`%s'
 Result: h_aliases=`%s'
 Result: h_name=`%s'
 SABM SENT SEQPACKET STREAM SYN_RECV SYN_SENT Serial Line IP Setting domainname to `%s'
 Setting hostname to `%s'
 Setting nodename to `%s'
 Site Sorry, use pppd!
 TIME_WAIT TX: Packets    Bytes        Errors DeadLoop NoRoute  NoBufs
 This kernel does not support RARP.
 Too much address family arguments.
 UNIX Domain UNK. UNKNOWN UNSPEC UP  Unknown Unknown address family `%s'.
 Unknown media type.
 Usage:
  arp [-vn]  [<HW>] [-i <if>] [-a] [<hostname>]             <-Display ARP cache
 Usage:
  ifconfig [-a] [-v] [-s] <interface> [[<AF>] <address>]
 Usage: hostname [-v] {hostname|-F file}      set hostname (from file)
 Usage: inet6_route [-vF] del Target
 Usage: inet_route [-vF] del {-host|-net} Target[/prefix] [gw Gw] [metric M] [[dev] If]
 Usage: iptunnel { add | change | del | show } [ NAME ]
 Usage: plipconfig [-a] [-i] [-v] interface
 Usage: rarp -a                               list entries in cache.
 Usage: route [-nNvee] [-FC] [<AF>]           List kernel routing tables
 User       Dest       Source     Device  State        Vr/Vs    Send-Q  Recv-Q
 VJ 6-bit Serial Line IP VJ Serial Line IP WARNING: at least one error occured. (%d)
 Warning: Interface %s still in ALLMULTI mode.
 Warning: Interface %s still in BROADCAST mode.
 Warning: Interface %s still in DYNAMIC mode.
 Warning: Interface %s still in MULTICAST mode.
 Warning: Interface %s still in POINTOPOINT mode.
 Warning: Interface %s still in promisc mode... maybe other application is running?
 Warning: cannot open %s (%s). Limited output.
 Where: NAME := STRING
 Wrong format of /proc/net/dev. Sorry.
 You cannot start PPP with this program.
 [NO FLAGS] [NO FLAGS]  [NONE SET] [UNKNOWN] address mask replies: %u address mask request: %u address mask requests: %u arp: %s: hardware type without ARP support.
 arp: %s: kernel only supports 'inet'.
 arp: %s: unknown address family.
 arp: %s: unknown hardware type.
 arp: -N not yet supported.
 arp: cannot open etherfile %s !
 arp: cannot set entry on line %u of etherfile %s !
 arp: cant get HW-Address for `%s': %s.
 arp: device `%s' has HW address %s `%s'.
 arp: format error on line %u of etherfile %s !
 arp: in %d entries no match found.
 arp: invalid hardware address
 arp: need hardware address
 arp: need host name
 arp: protocol type mismatch.
 cannot determine tunnel mode (ipip, gre or sit)
 cannot open /proc/net/snmp compressed:%lu  destination unreachable: %u disabled domain name (which is part of the FQDN) in the /etc/hosts file.
 echo replies: %u echo request: %u echo requests: %u enabled error parsing /proc/net/snmp family %d  generic X.25 getdomainname()=`%s'
 gethostname()=`%s'
 getnodename()=`%s'
 hw address type `%s' has no handler to set address. failed.
 ifconfig: Cannot set address for this protocol family.
 ifconfig: `--help' gives usage information.
 ifconfig: option `%s' not recognised.
 ip: argument is wrong: %s
 keepalive (%2.2f/%ld/%d) missing interface information netmask %s  netrom usage
 netstat: unsupported address family %d !
 no RARP entry for %s.
 off (0.00/%ld/%d) on %s
 on (%2.2f/%ld/%d) on%d (%2.2f/%ld/%d) problem reading data from %s
 rarp: %s: unknown hardware type.
 rarp: %s: unknown host
 rarp: cannot open file %s:%s.
 rarp: cannot set entry from %s:%u
 rarp: format error at %s:%u
 redirect: %u redirects: %u route: %s: cannot use a NETWORK as gateway!
 route: Invalid MSS/MTU.
 route: Invalid initial rtt.
 route: Invalid window.
 route: bogus netmask %s
 route: netmask doesn't match route address
 rresolve: unsupport address family %d !
 source quench: %u source quenches: %u time exceeded: %u timeout in transit: %u timestamp replies: %u timestamp reply: %u timestamp request: %u timestamp requests: %u timewait (%2.2f/%ld/%d) ttl != 0 and noptmudisc are incompatible
 unkn-%d (%2.2f/%ld/%d) unknown usage: netstat [-veenNcCF] [<Af>] -r         netstat {-V|--version|-h|--help}
 warning, got bogus igmp line %d.
 warning, got bogus igmp6 line %d.
 warning, got bogus raw line.
 warning, got bogus tcp line.
 warning, got bogus udp line.
 warning, got bogus unix line.
 wrong parameters: %u Project-Id-Version: net-tools
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-06-30 12:28+0900
PO-Revision-Date: 2009-09-29 17:39+0000
Last-Translator: wiz <aenor.realm@gmail.com>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:13+0000
X-Generator: Launchpad (build 18115)
 
Proto Recv-Q Send-Q Local Address Foreign Address State       
Proto RefCnt Flags Type State I-Node 
Если вы не используете bind или NIS для поиска компьютеров, вы можете изменить DNS
                               [mod] [dyn] [reinstate] [[dev] If]
                               [netmask N] [mss Mss] [window W] [irtt I]
                   [nibble NN] [trigger NN]
              сжато:%lu
           %s addr:%s            [ [i|o]seq ] [ [i|o]key KEY ] [ [i|o]csum ]
           [ mode { ipip | gre | sit } ] [ remote ADDR ] [ local ADDR ]
           [ ttl TTL ] [ tos TOS ] [ nopmtudisc ] [ dev PHYS_DEV ]
           collisions:%lu            inet6 addr: %s/%d         --numeric-hosts не преобразовывать адреса в имена компьютеров
         --numeric-ports не преобразовывать номера портов в имена
         --numeric-users не преобразовывать в имена пользователей
         -A, -p, --protocol указание семейства протоколов
         -C, --cache отобразить кэш маршрутизации вместо FIB

         -D, --use-device прочитать <hwaddr> из заданного устройства
         -F, -fib отобразить информацию форвардинга базы (по умолчанию)
         -M, --masquerade отобразить замаскированные соединения

         -N, --symbolic преобразовать имена устройств
         -a показать (все) хосты в альтернативном (BSD) стиле
         -a, --all, --listening отобразить все сокеты (по умолчанию - в статусе connected)
         -c, --continuous непрерывный вывод

         -d, --delete удалить определенную запись
         -e, --extend отображать другую/больше информации
         -f, --file считать новые записи из файла или из /etc/ethers

         -g, --groups отобразить членства в мультикаст группах
         -i, --device указание сетевого интерфейса (например, eth0)
         -i, --interfaces отобразить таблицу интерфейсов
         -l, --listening отображать прослушиваемые сокеты сервера
         -n, --numeric не преобразовывать адреса в имена
         -o, -timers отобразить таймеры
         -p, --programs отображать номер процесса программы/имя программы для сокетов
         -r, --route отобразить таблицу маршрутизации
         -s, --set установить новую запись ARP
         -s, --statistics отобразить сетевую статистику (как SNMP)
         -v, --verbose более детальный вывод
        ADDR := { IP_ADDRESS | any }
        KEY := { DOTTED_QUAD | NUMBER }
        TOS := { NUMBER | inherit }
        TTL := { 1..255 | inherit }
        domainname [-v] {NIS_домен|-F файл} установить NIS доменное имя (из файла)
        hostname -V|--version|-h|--help вывести на экран информацию и выйти

        hostname [-v] показать имя компьютера

        hostname [-v] [-d|-f|-s|-a|-i|-y|-n] показать отформатированное имя
        inet6_route [-FC] сброс НЕ поддерживается
        inet6_route [-vF] add Цель [gw Gw] [metric M] [[dev] If]
        inet_route [-FC] сброс НЕ поддерживается
        inet_route [-vF] add {-host|-net} Target[/prefix] [gw Gw] [metric M]
        inet_route [-vF] add {-host|-net} Target[/prefix] [metric M] reject
        iptunnel -V | --version

        netstat [-vnNcaeol] [<Socket> ...]
        netstat { [-veenNac] -i | [-cnNe] -M | -s }

        nodename [-v] {имя_узла|-F файл} установить DECnet имя узла (из файла)
        plipconfig -V | --version
        rarp -V отобразить версию программы.

        rarp -d <имя_компьютера> удалить запись из кэша.
        rarp -f добавить записи из файла /etc/ethers.
        rarp [<HW>] -s <имя_компьютера> <физический адрес> добавить запись в кэш.
        route [-v] [-FC] {add|del|flush} ... Изменить таблицу маршрутизации для AF.

        route {-V|--version} Отобразить версию/автора и выйти.

        route {-h|--help} [<AF>] Детальное описание использование указанной AF.
      - статистика недоступна -     -F, --file прочитать имя компьютера или NIS имя из указанного файла

     -a, --alias другие имена
     -d, --domain доменное имя компьютера
     -f, --fqdn, --long полное имя компьютера (FQDN)
     -i, --ip-address IP-адрес для компьютера
     -n, --node имя узла DECnet
     -s, --short имя компьютера вкратце
     -y, --yp, --nis NIS/YP имя
     dnsdomainname=hostname -d, {yp,nis,}domainname=hostname -y

    Данная команда может прочитать или установить имя компьютера или NIS-имя. Вы также можете
   прочитать DNS или FQDN (Fully Qualified Domain Name).
   Если вы не используете bind или NIS для поиска компьютеров, вы можете изменить
   FQDN (Fully Qualified Domain Name) и DNS доменное имя (которое
   является частью FQDN) в файле /etc/hosts.
   <AF>=Address family. По умолчанию: %s
   <AF>=Use '-6|-4' or '-A <af>' or '--<af>'; по умолчанию: %s
   <AF>=Use '-A <af>' or '--<af>'; по умолчанию: %s
   <HW>=Hardware Type.
   <HW>=Используйте '-H <hw>' для указания типа аппаратного адреса. По умолчанию: %s
   <Socket>={-t|--tcp} {-u|--udp} {-w|--raw} {-x|--unix} --ax25 --ipx --netrom
   В полученном пакете требуется контрольная сумма.
   Контрольная сумма выходящих пакетов.
   Выбрасывать пакеты из последовательности.
   Список возможный адресных семейств (которые поддерживают маршрутизацию):
   Список возможных адресных диапазонов:
   Список всех возможных типов HW (которые поддерживают ARP)
   Список возможных типов оборудования:
   Последовательность пакетов на выходе.
   [[-]broadcast [<адрес>]] [[-]pointopoint [<адрес>]]
   [[-]dynamic]
   [[-]trailers] [[-]arp] [[-]allmulti]
   [add <адрес>[/<длина префикса>]]
   [del <адрес>[/<длина префикса>]]
   [hw <HW> <адрес>] [metric <NN>] [mtu <NN>]
   [mem_start <NN>] [io_addr <NN>] [irq <NN>] [media <type>]
   [multicast] [[-]promisc]
   [netmask <адрес>] [dstaddr <адрес>] [tunnel <адрес>]
   [outfill <NN>] [keepalive <NN>]
   [txqueuelen <NN>]
   [up|down] ...

   arp [-v]          [-i <if>] -d  <host> [pub]               <-Удалить элемент ARP
   arp [-v]   [<HW>] [-i <if>] -Ds <host> <if> [netmask <nm>] pub          <-''-

   arp [-v]   [<HW>] [-i <if>] -s  <host> <hwaddr> [temp]            <-Добавить элемент
   arp [-vnD] [<HW>] [-i <интерфейс>] -f [<имя_файла>] <- Добавить запись в arp из файла
  Bcast:%s   Mask:%s
  P-t-P:%s   Путь
  Scope:  Таймер  Пользователь  пользовательский узел       интерфейс %s
  на %s  пользователи %d %s	ibble %lu trigger %lu
 %s (%s) -- нет записи
 %s (%s) в  %s запущен %s: %s/ip remote %s local %s  %s: ОШИБКА при получении флагов интерфейса: %s
 %s: ОШИБКА при тестировании флагов интерфейса: %s
 %s: Вы не можете изменить DNS доменное имя используя эту команду.
 %s: диапазон адресов не поддерживается!
 %s: неправильный физический адрес
 %s: невозможно открыть `%s'
 %s: данный HW тип не поддерживается!
 %s: указаны несовместимые опции.
 %s: неверный %s адрес.
 %s: слишком длинное имя
 %s: для изменения доменного имени у вас должны быть права суперпользователя (root)
 %s: для изменения имени компьютера у вас должны быть права суперпользователя (root)
 %s: для изменения имени узла у вас должны быть права суперпользователя (root)
 получено DSACKs: %u неудачные сообщения ICMP: %u ICMP сообщений получено: %u послано сообщений ICMP: %u Отброшенные ICMP пакеты по причине закрытого сокета: %u ICMP пакеты, отброшенные по причине out-of-window: %u принято SYN cookies: %u послано SYN cookies: %u %u TCP sockets finished time wait in fast timer %u TCP sockets finished time wait in slow timer открытия активных соединений: %u активных подключений отвергнуто по причине штампа времени: %u плохих сегментов получено: %u получено сбросов соединений: %u разорванных соединений из-за тайм-аутов: %u соединений установлено: %u %u соединения сброшены из-за неожиданного значения SYN %u соединения сброшены из-за неожиданных данных %u задержал подтверждение приема из-за заблокированного сокета задержанных подтверждений послано: %u сброшено по причине отсутствия маршрута: %u неудачные попытки соединения: %u быстрых повторов передачи: %u %u перенаправлено фрагментов создано: %u фрагментов сброшено после тайм-аута: %u фрагментов неудачно: %u фрагментов получено удачно: %u входящих пакетов доставлено: %u %u входящих пакетов отклонено неудачных входящих ICMP сообщений: %u получено неверных SYN cookies: %u других TCP тайм-аутов: %u исходящих пакетов сброшено: %u ожидаемых заголовков пакетов: %u пакетов пересобрано неудачно: %u ошибок приема пакетов: %u %u packets dropped from out-of-order queue because of socket buffer overrun брошено пакетов из предварительной очереди: %u ожидаемых заголовков пакетов, непосредственно стоявших в очереди к пользователю: %u пакеты, вырезанные из очереди приема: %u пакеты, вырезанные из очереди приема по причине переполнения буфера сокета: %u пакетов пересобрано удачно: %u пакетов принято: %u %u пакеты отброшены в установленных соединениях из-за временной метки пакетов послано: %u принято пакетов на неизвестный порт: %u открытия пассивных соединений: %u пассивных подключений отвергнуто по причине штампа времени: %u ожидаемые подтверждения: %u требуется повторных сборок: %u запросов отправлено: %u получено сбросов для эмбриональных SYN_RECV сокетов: %u сбросов послано: %u повторных передач потеряно: %u сегментов получено: %u повторно передано сегментов: %u отправлено сегментов: %u %u time wait sockets recycled by time stamp тайм-аутов после восстановления SACK: %u востановлений потерянных пакетов посредством быстрого повтора передачи: %u всего пакетов принято %u %u с неверными адресами %u с неверными заголовками %u с неверным протоколом (Cisco)-HDLC (Информация для "-p": geteuid()=%d не может быть прочитана, но вам нужны права суперпользователя (root).)
 (Не все процессы были идентифицированы, информация о процессах без владельца
 не будет отображена, вам нужны права суперпользователя (root), чтобы увидеть всю информацию.)
 (авто) (не завершено) (only servers) (servers and established) (w/o servers) 16/4 Мб/c Token Ring 16/4 Мб/c Token Ring (Новый) 6-бит Serial Line IP <из интерфейса> <не завершено>  AMPR AX.25 AMPR NET/ROM AMPR ROSE ARCnet Протокол AX.25 в этой системе не сконфигурирован.
 Активный сокет AX.25
 Активные сокеты IPX
Proto Recv-Q Send-Q Local Address Foreign Address State Активные соединения с интернетом  Активные сокеты NET/ROM
 Активные сокеты домена UNIX  Активные сокеты X.25
 Адаптивный Serial Line IP Адрес HW-тип HW-адрес Флаги Маска Интерфейс
 Удаление адреса не поддерживается этой системой.
 Семейство адресов `%s' не поддерживается.
 Appletalk DDP Ash Неверный адрес.
 Для широковещательного тунеля требуется исходный адрес.
 CCITT X.25 CLOSE CLOSE_WAIT CLOSING CONN SENT CONNECTED CONNECTING Невозможно создать сокет DARPA Internet DGRAM DISC SENT DISCONNECTING TTL по умолчанию %u Dest Source Device LCI State Vr/Vs Send-Q Recv-Q
 Dest Source Device State Vr/Vs Send-Q Recv-
 Destination Gateway Genmask Flags MSS Window irtt Iface
 Destination Gateway Genmask Flags Metric Ref Use Iface
 Destination Mnemonic Quality Neighbour Iface
 Устройство не обнаружено Не знаю как установить адрес для семейства %d.
 ESTAB ESTABLISHED Econet Записей: %d	Пропущено: %d	Найдено: %d
 Ethernet FIN_WAIT1 FIN_WAIT2 FREE Невозможно получить тип [%s]
 Сброс таблицы маршрутизации `inet' не поддерживается
 Сброс таблицы маршрутизации `inet6' не поддерживается
 Перенаправление %s Универсальный EUI-64 Общий HIPPI HWaddr %s   Аппаратный тип `%s' не поддерживается.
 Host Гистограмма входа ICMP Гистограмма выхода ICMP INET (IPv4) не настроен в этой системе.
 INET6 (IPv6) в этой системе не сконфигурирован.
 Туннель IPIP Протокол IPX в этой системе не сконфигурирован.
 Членство в группе IPv4
 IPv6 IPv6-in-IPv4 Iface MTU Met RX-OK RX-ERR RX-DRP RX-OVR TX-OK TX-ERR TX-DRP TX-OVR Flg
 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR   TX-OK TX-ERR TX-DRP TX-OVR Flg
 Группа интерфейса RefCnt
 Интерфейс %s не инициализирован
 Interrupt:%d  IrLAP Таблица маршрутизации AX.25 в ядре
 Таблица маршрутизации ядра IP
 Таблица маршутизации ядра протокола IP
 Таблица маршрутизации ядра IPX
 Таблица маршрутизация ядра IPv6
 Таблица интерфейсов ядра
 Таблица маршрутизации NET/ROM в ядре
 Таблица маршрутизации ROSE в ядре
 Ключи недоступны вместе с ipip и sit.
 LAPB LAST_ACK LISTEN LISTENING Link Локальная петля (Loopback) Media:%s Память:%lx-%lx  Изменение кэша маршрутизации `inet' не поддерживается
 Протокол NET/ROM в этой системе не сконфигурирован.
 NET/ROM: это требуется написать
 Neighbour HW Address Iface Flags Ref State
 Neighbour HW Address Iface Flags Ref State Stale(sec) Delete(sec)
 ARP запись для %s не найдена
 Нет маршрутизации для адресного пространства `%s'.
 Нет поддержки ECONET на данной системе.
 INET не поддерживается этой системой
 Нет поддержки INET6 на этой системе.
 Не найдено семейств адресов, которые можно использовать.
 Адрес узла должен содержать десять цифр Novell IPX Пожалуйста не используйте более одного адресного пространства.
 Протокол PPP (Point-to-Point Protocol) Проблема чтения данных из %s
 Редим быстрого подтверждения приема был активирован %u раз RAW RDM RECOVERY ROSE не настроен на этой системе.
 алгоритм RTO %s RX: Packets Bytes Errors CsumErrs OutOfSeq Mcasts
 Управлений из памяти системы во время посыла пакета: %u Определяется `%s' ...
 Результат: h_addr_list=`%s'
 Результат: h_aliases=`%s'
 Результат: h_name=`%s'
 SABM SENT SEQPACKET STREAM SYN_RECV SYN_SENT Serial Line IP Устанавливается доменное имя `%s'
 Устанавливается имя компьютера '%s'
 Устанавливается имя узла '%s'
 Сайт Извините, используйте pppd!
 TIME_WAIT TX: Packets Bytes Errors DeadLoop NoRoute NoBufs
 Ядро не поддерживает RARP
 Слишком много аргументов адресного пространства.
 UNIX Domain UNK. UNKNOWN UNSPEC UP  Неизвестный Неизвестное адресное пространство `%s'.
 Неизвестный тип носителя.
 Использование:
  arp [-vn] [<HW>] [-i <интерфейс>] [-a] [<имя_компьютера>] <- Отобразить кэш arp
 Использование:
  ifconfig [-a] [-v] [-s] <имя интерфейса [[<AF>] <адрес>]
 Использование: hostname [-v] {имя_компьютера|-F файл} установить новое имя компьютера (из файла)
 Использование: inet6_route [-vF] del Цель
 Использование: inet_route [-vF] del {-host|-net} Target[/prefix] [gw Gw] [metric M] [[dev] If]
 Использование: iptunnel { add | change | del | show } [ ИМЯ ]
 Использование: plipconfig [-a] [-i] [-v] интерфейс
 Использование: rarp -a список записей в кэше.
 Использование: route [-nNvee] [-FC] [<AF>] Отобразить таблицу маршрутизации ядра
 User Dest Source Device State Vr/Vs Send-Q Recv-Q
 VJ 6-бит Serial Line IP VJ Serial Line IP ПРЕДУПРЕЖДЕНИЕ: произошла минимум одна ошибка. (%d)
 Предупреждение: Интерфейс %s все еще находится в режиме ALLMULTI.
 Предупреждение: Интерфейс %s все еще находится в режиме BROADCAST.
 Предупреждение: Интерфейс %s все еще находится в режиме DYNAMIC.
 Предупреждение: Интерфейс %s все еще находится в режиме MULTICAST.
 Предупреждение: Интерфейс %s все еще находится в режиме POINTOPOINT.
 Предупреждение: Интерфейс %s все еще находится в режиме PROMISC... возможно запущена еще одна программа?
 Внимание: невозможно открыть %s (%s). Ограниченный вывод.
 Где: NAME := STRING
 Неверный формат /proc/net/dev. Извините.
 Вы не можете запустить PPP вместе с этой программой.
 [НЕТ ФЛАГОВ] [НЕТ ФЛАГОВ]  [НИЧЕГО НЕ УСТАНОВЛЕНО] [НЕИЗВЕСТНЫЙ] ответов маски адреса: %u запросов маски адреса: %u запросы масок адреса: %u arp: %s: тип оборудования без поддержки ARP.
 arp: %s: ядро поддерживает только 'inet'.
 arp: %s: неизвестный тип адресов.
 arp: %s: неизвестный тип оборудования
 arp: -N пока не поддерживается.
 arp: невозможно открыть дамп %s !
 arp: невозможно установить параметр в строке %u, дамп %s !
 arp: невозможно получить физический адрес для `%s': %s.
 arp: устройство `%s' с физическим адресом %s `%s'.
 arp: ошибка формата в строке %u, дамп %s !
 arp: в %d записях совпадения не найдено.
 arp: неправильный физический адрес
 arp: необходимо указать физический адрес
 arp: необходимо указать имя узла
 arp: несовпадение типа протокола.
 невозможно определить режим туннеля (ipip, gre or sit)
 Невозможно открыть /proc/net/snmp сжато:%lu  пункт назначения недоступен: %u отключено доменное имя (которое является частью FQDN) в файле /etc/hosts.
 эхо-ответы: %u эхо-запросов: %u эхо-запросы: %u включено ошибка анализа /proc/net/snmp семейство %d  универсальный X.25 getdomainname()=`%s'
 gethostname()=`%s'
 getnodename()=`%s'
 тип физического адреса `%s' не имеет обработчика для установки адреса. неудача.
 ifconfig: Не могу установить адрес для этого семейства протоколов.
 ifconfig: `--help' показывает информацию по использованию.
 ifconfig: опция `%s' не распознается.
 ip: аргумент ошибочен: %s
 keepalive (%2.2f/%ld/%d) Отсутствует информация об интерфейсе сетевая маска %s  использование netrom
 netstat: неподдерживаемое семейство адресов %d !
 не найдена запись RARP для %s.
 off (0.00/%ld/%d) на %s
 on (%2.2f/%ld/%d) on%d (%2.2f/%ld/%d) проблема чтения данных из %s
 rarp: %s: неизвестный тип оборудования
 rarp: %s: неизвестный хост
 rarp: невозможно открыть файл %s:%s.
 rarp: невозможно установить запись из %s:%u
 rarp: ошибка формата в %s:%u
 перенаправлений: %u перенаправления: %u route: %s: нельзя использовать СЕТЬ как маршрутизатор!
 route: неверные MSS/MTU.
 route: неверный начальный rtt.
 route: неверное окно.
 route: фальшивая сетевая маска %s
 route: сетевая маска не совпадает с маршрутизируемым адресом
 rresolve: неподдерживаемое семейство адресов %d !
 источник подавления: %u подавления от источника: %u время превышено: %u потери при прохождении: %u ответы отметок времени: %u ответ временной метки: %u запрос временной метки: %u запросы отметок времени: %u timewait (%2.2f/%ld/%d) ttl != 0 и noptmudisc несовместимы
 unkn-%d (%2.2f/%ld/%d) неизвестный Использование: netstat [-veenNcCF] [<Af>] -r netstat {-V|--version|-h|--help}
 внимание, получена поддельная линия igmp %d.
 внимание, получена поддельная линия igmp6 %d.
 внимание, получена поддельная линия raw.
 внимание, получена поддельная линия tcp.
 внимание, получена поддельная линия udp.
 внимание, получена поддельная линия unix.
 неверные параметры: %u 