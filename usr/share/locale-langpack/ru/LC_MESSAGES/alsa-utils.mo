��    +     t	  �  �        9     J   K  '   �  (   �  $   �  .     5   ;  ?   q  6   �  :   �  :   #     ^  )   t     �  '   �  5   �  -     9   H  8   �  )   �  &   �  "     "   /     R     g     |  .   �  "   �     �  .   �  1     #   D     h  I   u  /   �  &   �          5  	   =     G  &   S     z  ,   �  *   �  -   �        !   9      [      t   +   �      �      �   )   �   C   �   >   5!  "   t!     �!  0   �!  1   �!  2   "  $   5"     Z"     s"     �"     �"     �"     �"  !   �"  !   �"  !   #  !   $#  '   F#  
   n#  
   y#  
   �#  
   �#  
   �#  
   �#  
   �#  	   �#     �#  4   �#     $     #$     )$     8$     F$  !   ^$  %   �$  �   �$     (%     5%     P%     W%     s%     y%  	   �%     �%  	   �%     �%     �%     �%     �%     &     1&     K&     b&     ~&  
   �&     �&     �&     �&     �&  !   �&     �&     '     '     '     )'  $   -'     R'     T'     k'     �'     �'     �'     �'     �'     �'     �'  %   �'  "   �'     "(     0(     9(     P(     l(  +   t(  &   �(  +   �(     �(     �(  )   )  6   ,)  "   c)     �)  	   �)  
   �)     �)  	   �)     �)     �)      *     $*  -   @*  &   n*     �*     �*     �*     �*  	   �*  
   �*     �*  
   +      +     :+  (   F+     o+     �+     �+     �+  ,   �+     �+  O   �+  #   8,  $   \,     �,  5   �,  -   �,  6   -     <-     S-  &   h-  6   �-     �-  )   �-  /   .  1   =.  )   o.  +   �.  1   �.  4   �.  )   ,/     V/     l/  !   �/     �/  )   �/     �/      0     0  �   #0     �0     1     31     71     =1     J1  !   O1  %   q1  %   �1  !   �1  =   �1  !   2     ?2     F2     Z2  +   w2     �2  -   �2  2   �2     3     '3     :3  !   Z3     |3     �3     �3     �3  D   �3  $   4  $   54     Z4     w4     �4     �4     �4  4   �4  %   5     35     N5  /   _5     �5     �5     �5     �5     �5     �5     6     6     86     ?6     L6     Q6     h6     �6     �6  	   �6     �6     �6     �6     �6  
   �6      �6     7     (7     E7     U7  +   n7     �7     �7  #   �7     �7     	8  ,    8     M8     j8     s8      �8  ?   �8  J   �8     >9     R9     n9     v9      {9     �9     �9     �9     �9     :     :      :     1:  +   L:  !   x:     �:     �:  �  �:  z   k<  c   �<  s   J=  U   �=  ,   >  @   A>  H   �>  \   �>  c   (?  �   �?  `   @     o@  S   �@      �@  F   �@  ]   AA  F   �A  ^   �A  a   EB  I   �B  &   �B  "   C  "   ;C  '   ^C  $   �C     �C  @   �C  3   �C     ,D  T   5D  a   �D  O   �D     <E  �   IE  w   F  F   �F  1   �F     G     G     %G  -   AG  ;   oG  U   �G  X   H  V   ZH  <   �H  M   �H  (   <I  ,   eI  T   �I     �I     �I  Q   �I  �   NJ  x   �J  ,   JK     wK  �   �K  ~   
L  y   �L  P   M  5   TM  0   �M  I   �M     N     N     N  0   5N  E   fN  C   �N  G   �N  7   8O     pO     ~O     �O     �O     �O     �O     �O     �O     �O  h   �O  8   XP     �P     �P     �P  %   �P  /   �P  %   #Q  �   IQ  &   BR  #   iR     �R  1   �R     �R     �R     �R     �R     S  8   #S  +   \S  \   �S  L   �S  D   2T  1   wT  *   �T  -   �T  
   U     U     )U     GU  0   VU  0   �U  8   �U  &   �U     V     (V  '   -V     UV  2   fV     �V  #   �V  G   �V     W  &   W  %   7W     ]W     tW  	   vW     �W  O   �W  .   �W     X     X  :   ;X  Y   vX     �X  <   �X  N   ,Y  q   {Y     �Y     �Y  U   Z  u   aZ  L   �Z     $[     +[     C[  Z   ][     �[  ?   �[  5   \  2   ;\  :   n\  T   �\  Q   �\     P]  4   ^]  4   �]     �]     �]     �]  B   ^     P^  (   l^     �^  <   �^  "   �^     _     _     &_  @   3_  E   t_  �   �_  ;   N`  R   �`  A   �`  P   a  i   pa  x   �a  #   Sb  &   wb  [   �b  f   �b  @   ac  v   �c  v   d  y   �d  j   
e  `   ue  o   �e  �   Ff  d   �f     1g  '   Pg  @   xg  ;   �g  \   �g  ;   Rh     �h  2   �h    �h  C   �i  P   @j     �j     �j     �j     �j  6   �j  :   �j  (   &k  6   Ok  g   �k  5   �k     $l  !   +l  1   Ml  q   l     �l  M   m  R   Sm  A   �m  5   �m  U   n  R   tn  -   �n  P   �n  ;   Fo  7   �o  �   �o  ^   Hp  U   �p  L   �p  N   Jq  @   �q  Y   �q  /   4r  p   dr  9   �r  "   s     2s  E   Qs     �s     �s  7   �s  %   �s  -   t  4   Lt  6   �t  2   �t     �t  4   �t     )u  B   Eu  ,   �u  %   �u     �u     �u     v  H   v  <   [v     �v     �v  ?   �v     
w  9   (w     bw  '   yw  5   �w  )   �w     x  <   !x  ?   ^x  0   �x  b   �x  ?   2y     ry  =   �y  J   �y  �   
z  �   �z  &   ?{  1   f{     �{     �{  9   �{  )   |     2|  .   M|  .   ||     �|     �|     �|  A   �|  \   =}  ?   �}  2   �}  "   ~     �           n   	     D           !            "  �     }      |       �   �   �           �   /   �     '   �   �       �   :          �   �   �   $      *  $   (   y   �          �       �       �     �   B              �         7      �   T               �   �         m      �   d   �       ,   �           �       �   b   �       a             *   �      �   I       �   F   �   �   �       ~   �   �   �   '  �   <              �   @                        Z   �          �         �   A   �   (  �   u       >           �           W   �         Q           4   �   z   �   g   �   �   �   �   �          =   i   )      l   3   �      �      �       o   r              w   q           
   9   �   �   +  s     x   �             �                   p     �       �   �   �   �   �       )           ?       �   �   h     �     �      �                �   J   K   L   M   N   O   P   +   ^   �         #   -   j   �   C   �   t   �   �      	   �             0   !     %       6   G   �     f   E   �   �         e   "           �   �   �   _   �   `   8   #  �   c   �   �           X       �           �          &  
  �   �   �      v      �      �   �   {   �          �       �       H                        \   �   �   �       ]           �   �        k       �   U   R   �       �      S         �   �           Y               V      �   �   1   �          �   �   �   �           �   �   �   �   �       �   .   �       &      %  [   �       2       �       ;   �   �   �   5        
Some of these may not be available on selected hardware
 === PAUSE ===                                                             PAUSE command ignored (no hw support)
          please, try the plug plugin %s
      -d,--disconnect     disconnect
      -e,--exclusive      exclusive connection
      -i,--input          list input (readable) ports
      -l,--list           list current connections of each port
      -o,--output         list output (writable) ports
      -r,--real #         convert real-time-stamp on queue
      -t,--tick #         convert tick-time-stamp on queue
      -x, --removeall
      sender, receiver = client:port pair
    aconnect -i|-o [-options]
    aconnect [-options] sender receiver
   -d,--dest addr : write to given addr (client:port)
   -i, --info : print certain received events
   -p,--port # : specify TCP port (digit or service name)
   -s,--source addr : read from given addr (client:port)
   -v, --verbose : print verbose messages
   Clemens Ladisch <clemens@ladisch.de>   Copyright (C) 1999 Takashi Iwai
   Jaroslav Kysela <perex@perex.cz>   Subdevice #%i: %s
   Subdevices: %i/%i
   Tim Janik   client mode: aseqnet [-options] server_host
   server mode: aseqnet [-options]
  !clip    * Connection/disconnection between two ports
  * List connected ports (no subscription action)
  * Remove all exported connections
  [%s %s, %s]  can't play WAVE-files with sample %d bits in %d bytes wide (%d channels)  can't play WAVE-files with sample %d bits wide %s is not a mono stream (%d channels)
 %s!!! (at least %.3f ms long)
 %s: %s
 (default) (unplugged) **** List of %s Hardware Devices ****
 + -        Change volume -f cd (16 bit little endian, 44100, stereo)
 -f cdr (16 bit big endian, 44100, stereo)
 -f dat (16 bit little endian, 48000, stereo)
 0-9        Set volume to 0%-90% < >        Toggle left/right mute Aborted by signal %s...
 Access type not available Access type not available for playback: %s
 All Authors: B          Balance left and right volumes Broken configuration for playback: no configurations available: %s
 Broken configuration for this PCM: no configurations available Buffer size range from %lu to %lu
 CAPTURE Can't recovery from suspend, prepare failed: %s
 Can't recovery from underrun, prepare failed: %s
 Can't use period equal to buffer size (%lu == %lu) Cannot create process ID file %s: %s Cannot open WAV file %s
 Cannot open file "%s". Cannot open mixer device '%s'. Capture Card: Center Channel %2d: Control event : %5d
 Channel %2d: Note Off event: %5d
 Channel %2d: Note On event : %5d
 Channel %2d: Pitchbender   : %5d
 Channel %d doesn't match with hw_parmas Channel 10 Channel 11 Channel 12 Channel 13 Channel 14 Channel 15 Channel 16 Channel 9 Channels %i Channels count (%i) not available for playbacks: %s
 Channels count non available Chip: Connected From Connecting To Connection failed (%s)
 Connection is already subscribed
 Copyright (C) 1999-2000 Takashi Iwai
 Debugging options:
  -g, --no-color          toggle using of colors
  -a, --abstraction=NAME  mixer abstraction level: none/basic Device name: Disconnection failed (%s)
 Done.
 End        Set volume to 0% Error Esc     Exit Esc: Exit F1 ? H  Help F1:  Help F2 /    System information F2:  System information F3      Show playback controls F4      Show capture controls F5      Show all controls F6 S    Select sound card F6:  Select sound card Failed. Restarting stream.  Front Front Left Front Right Help Invalid WAV file %s
 Invalid number of periods %d
 Invalid parameter for -s option.
 Invalid test type %s
 Item: L L       Redraw screen LFE Left    Move to the previous control M M          Toggle mute Max peak (%li samples): 0x%08x  Mono No enough memory
 No subscription is found
 Not a WAV file: %s
 O Off On Page Up/Dn Change volume in big steps Period size range from %lu to %lu
 Periods = %u
 Playback Playback device is %s
 Playback open error: %d,%s
 Playing Playing Creative Labs Channel file '%s'...
 Press F6 to select another sound card. Q W E      Increase left/both/right volumes R Rate %d Hz,  Rate %iHz not available for playback: %s
 Rate doesn't match (requested %iHz, get %iHz, err %d)
 Rate set to %iHz (requested %iHz)
 Rear Rear Left Rear Right Recognized sample formats are: Recording Requested buffer time %u us
 Requested period time %u us
 Right   Move to the next control Sample format non available Sample format not available for playback: %s
 Sample rate doesn't match (%d) for %s
 Select File Setting of hwparams failed: %s
 Setting of swparams failed: %s
 Side Side Left Side Right Sine wave rate is %.4fHz
 Sound Card Space      Toggle capture Sparc Audio Sparc Audio doesn't support %s format... Status(DRAINING):
 Status(R/W):
 Status:
 Stereo Stream parameters are %iHz, %s, %i channels
 Suspended. Trying resume.  Suspicious buffer position (%li total): avail = %li, delay = %li, buffer = %li
 Tab     Toggle view mode (F3/F4/F5) The available format shortcuts are:
 The sound device was unplugged. This sound device does not have any capture controls. This sound device does not have any controls. This sound device does not have any playback controls. Time per period = %lf
 Transfer failed: %s
 Try `%s --help' for more information.
 Unable to determine current swparams for playback: %s
 Unable to install hw params: Unable to set avail min for playback: %s
 Unable to set buffer size %lu for playback: %s
 Unable to set buffer time %u us for playback: %s
 Unable to set hw params for playback: %s
 Unable to set nperiods %u for playback: %s
 Unable to set period time %u us for playback: %s
 Unable to set start threshold mode for playback: %s
 Unable to set sw params for playback: %s
 Undefined channel %d
 Unknown option '%c'
 Unsupported WAV format %d for %s
 Unsupported bit size %d.
 Unsupported sample format bits %d for %s
 Up/Down    Change volume Usage:
 Usage: alsamixer [options] Useful options:
  -h, --help              this help
  -c, --card=NUMBER       sound card number or id
  -D, --device=NAME       mixer device name
  -V, --view=MODE         starting view mode: playback/capture/all Using 16 octaves of pink noise
 Using max buffer size %lu
 VOC View: WAV file(s)
 WAVE Warning: format is changed to %s
 Warning: format is changed to MU_LAW
 Warning: format is changed to S16_BE
 Warning: format is changed to U8
 Warning: rate is not accurate (requested = %iHz, got = %iHz)
 Wave doesn't support %s format... Woofer Write error: %d,%s
 You need to specify %d files Z X C      Decrease left/both/right volumes accepted[%d]
 aconnect - ALSA sequencer connection manager
 aseqnet - network client/server on ALSA sequencer
 audio open error: %s bad speed value %i buffer to small, could not use
 can't allocate buffer for silence can't get address %s
 can't get client id
 can't malloc
 can't open sequencer
 can't play WAVE-file format 0x%04x which is not PCM or FLOAT encoded can't play WAVE-files with %d tracks can't play loops; %s isn't seekable
 can't play packed .voc files can't set client info
 cannot enumerate sound cards cannot load mixer controls cannot open mixer capture stream format change? attempting recover...
 card %i: %s [%s], device %i: %s [%s]
 client %d: '%s' [type=%s]
 closing files..
 command should be named either arecord or aplay dB gain: disconnected
 enter device name... info error: %s invalid card index: %s
 invalid destination address %s
 invalid sender address %s
 invalid source address %s
 kernel malloc error mute no soundcards found... nonblock setting error: %s not enough memory ok.. connected
 options:
 overrun pause push error: %s pause release error: %s raw data read error read error (called from line %i) read error: %s read/write error, state = %s readv error: %s sequencer opened: %d:%d
 service '%s' is not found in /etc/services
 snd_pcm_mmap_begin problem: %s status error: %s stdin O_NONBLOCK flag setup failed
 suspend: prepare error: %s too many connections!
 try `alsamixer --help' for more information
 unable to install sw params: underrun unknown abstraction level: %s
 unknown blocktype %d. terminate. unknown length of 'fmt ' chunk (read %u, should be %u at least) unknown length of extensible 'fmt ' chunk (read %u, should be %u at least) unknown option: %c
 unrecognized file format %s usage:
 user value %i for channels is invalid voc_pcm_flush - silence error voc_pcm_flush error was set buffer_size = %lu
 was set period_size = %lu
 write error write error: %s writev error: %s wrong extended format '%s' wrong format tag in extensible 'fmt ' chunk xrun(DRAINING): prepare error: %s xrun: prepare error: %s xrun_recovery failed: %d,%s
 Project-Id-Version: alsa-utils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-10-27 17:34+0100
PO-Revision-Date: 2016-05-21 13:14+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:56+0000
X-Generator: Launchpad (build 18115)
 
Некоторые из них могут быть недоступны на выбранном оборудовании
 === ПРИОСТАНОВЛЕНИЕ ===                                                             Пропущена команда ПРИОСТАНОВЛЕНИЯ (нет аппаратной поддержки)
          пожалуйста, попробуйте плагин-заглушку %s
      -d,--disconnect отключиться
      -e,--exclusive монопольное соединение
      -i,--input список (читаемых) портов ввода
      -l,--list список текущих подключений каждого порта
      -o,--output список портов вывода (с возможностью записи)
      -r,--real N преобразовать отметку-в-реальном-масштабе-времени в очереди
      -t, --tick N преобразовать время-метку-такта в очереди
      -x, --removeall
      отправитель, получатель = пара клиент:порт
    aconnect -i|-o [-опции]
    aconnect [-опции] отправитель получатель
   d,--dest addr: запись в заданного аддреса (клиент:порта)
   -i, --info : вывести полученные сообщения
   -p,--port # : укажите TCP-порт (номер или название службы)
   s,--source addr: чтение от заданного аддреса (клиент:порта)
   -v, --verbose : выводить подробные сообщения
   Clemens Ladisch <clemens@ladisch.de>   Copyright (C) 1999 Takashi Iwai
   Jaroslav Kysela <perex@perex.cz>   Подустройство №%i: %s
   Подустройства: %i/%i
   Tim Janik   режим клиента: aseqnet [-опции] сервер
   режим сервера: aseqnet [-опции]
  !clip    * Подключение/отключение между двумя портами
  * Список подключённых портов (не требуется подписка)
  * Удалить все экспортированные соединения
  [%s %s, %s]  невозможно воспроизведение WAVE-файлов с частотой оцифровки %d бит, через пропускную полосу в %d байт (%d каналов)  невозможно воспроизведение WAVE-файлов с частотой оцифровки %d бит поток %s не моно (количество каналов: %d)
 %s!!! (не менее %.3f мс длинной)
 %s: %s
 (по умолчанию) (не подключено) **** Список %s устройств ****
 Плюс/Минус - Изменение громкости -f cd (16 бит с младшим значащим битом, 44100, стерео)
 -f cdr (16 бит со старшим значащим битом, 44100, стерео)
 -f dat (16 бит с младшим значащим битом, 48000, стерео)
 0-9 - Изменение громкости от 0% до 90% < > - Отключение звука в Правом/Левом канале Прервано сигналом %s...
 Отсутствует тип доступа Указанный тип не доступен для проигрывания: %s
 Все Авторы: B - Выровнять баланс в левом и правом каналах. Неверная конфигурация для произведения: нет доступных конфигураций: %s
 Неправильная конфигурация для этого PCM: нет ни одной конфигурации Размер буфера от %lu до %lu
 ЗАХВАТ Невозможно восстановиться после режима ожидания, подготовка прервана:%s
 Востановление после после недобора невозможно, ошибка подготовки: %s
 Невозможно использовать промежуток, равный размеру буфера (%lu == %lu) Невозможно создать ID процесса для файла %s: %s Не возможно открыть WAV файл %s
 Не удается открыть файл "%s" Не удается открыть устройство-микшер '%s' Захват Карта: Центральный Канал %2d: Номер события : %5d
 Канал %2d: Выключить запись событий : %5d
 Канал %2d: Включить запись событий : %5d
 Канал %2d: Регулятор модуляции звука : %5d
 Канал %d не соответствует hw_parmas Канал 10 Канал 11 Канал 12 Канал 13 Канал 14 Канал 15 Канал 16 Канал 9 Каналы %i Количество каналов (%i) недоступно для воспроизведения: %s
 Недоступно количество каналов Чип: Подключено от Подключение к Сбой подключения (%s)
 Соединение уже подписано
 Copyright (C) 1999-2000 Takashi Iwai
 Параметры отладки:
  -g, --no-color - переключение режима использования цветов
  -a, --abstraction=ИМЯ - режим рассеивания микшера: отсутствует/базовый Название устройства: Сбой отключения (%s)
 Готово.
 End - Снижение громкости до 0% Ошибка Esc - Выйти Esc: Выход F1/?/H - Помощь F1: Помощь F2 - Информация об оборудовании. F2: Системная информация F3 - отобразить кнопки управления воспроизведением F4 - отобразить кнопки управления захватом F5 - отобразить все управляющие кнопки F6/S - Выбрать звуковую карту F6: Выбор звуковой карты Сбой. Перезапуск потока.  Фронт Передний левый Передний правый Справка Неверный формат WAV файла %s
 Неверное число периодов %d
 Неверный параметр для опции -s.
 Неверный тип теста %s
 Элемент: ЛВ L - Перерисовать экран сабвуфер "Влево" - Предыдущая дорожка M M - Отключение звука Максимальное значение (%li сэмплов): 0x%08x  Моно Недостаточно памяти
 Не найдено подписок
 Не WAV файл: %s
 O Выкл. Вкл. Page Up/Dn - Скачкообразное изменение громкости Размер периода от %lu до %lu
 Периоды = %u
 Воспроизведение Устройство для проигрывания - %s
 Ошибка открытия устройства воспроизведения: %d,%s
 Воспроизведение Проигрываю файл Creative Labs Channel '%s'...
 Нажмите F6 для выбора другой звуковой карты Q/W/E - увеличение громкости в левом/центральном/правом каналах. ПР Частота %d Гц,  Частота %iГц недоступна для воспроизведения: %s
 Частота не соответствует (запрошено %iГц, получено %iГц, ошибка %d)
 Установлена частота в %iГц (запрошено %iГц)
 Тыл Задний левый Задний правый Поддерживаются следующие форматы дискретизации: Запись Запрошено время предзагрузки %u мс
 Запрошено время периода %u мс
 "Вправо" - Следующая дорожка Остсутствует частота оцифровки Формат сэмпла не доступен для произведения: %s
 Частота дискретизации не совпадает (%d) для %s
 Обзор... Ошибка при установке hwparams: %s
 Ошибка при установке swparams: %s
 Сторона Боковой левый Боковой правый Частота синусоидальной волны - %.4fГц
 Звуковая карта Пробел - Начать захват Sparc Audio Sparc Audio не поддерживает формат %s... Статус(ИСТОЩЕНИЕ):
 Статус(Ч/З):
 Статус:
 Стерео Параметры потока - %iГц, %s, %i каналов
 Приостановленно. Пытаюсь продолжить.  Подозрительное положение в буфере (%li всего): доступно = %li, задержка = %li, буфер = %li
 Tab - Смена режима просмотра (F3/F4/F5) Доступны следующие сокращения для форматов:
 Звуковое устройство было отключено Отсутствует возможность настройки захвата. Отсутствует возможность настройки звукового устройства. Это звуковое устройство не имеет никаких клавиш воспроизведения. Время в периоде = %lf
 Передача прервана: %s
 Попробуйте `%s --help' для дополнительной информации.
 Невозможно определить текущие swparams для проигрывания: %s
 Невозможно установить параметры hw: Невозможно установить доступный минимум для воспроизведения: %s
 Невозможно установить величину буфера %lu для воспроизведения: %s
 Невозможно установить время предзагрузки %u мс для проигрывания %s
 Невозможно установить параметры hw для воспроизведения: %s
 Не удалось установить nperiods %u  для воспроизведения: %s
 Невозможно установить время периода %u мс для проигрывания %s
 Невозможно установить начальное пороговое значение для проигрывания: %s
 Невозможно установить параметры sw для проигрывания: %s
 Канал %d не задан
 Неизвестная опция '%c'
 Не поддерживаемый WAV формат %d для %s
 Неподдерживаемый размер бита %d.
 Неподдерживаемое количество бит (%d) на сэмпл для %s
 Вверх/Вниз - Изменение громкости Использование:
 Использование: alsamixer [опции] Полезные опции:
  -h, --help эта страница
  -c. --card=ЧИСЛО номер звуковой карты или id
  -D, --device=ИМЯ имя устройства mixer
  -V, --view=РЕЖИМ запуск режима просмотра: playback/capture/all Используется 16 октав "розового" шума
 Используется максимальный размер буфера %lu
 VOC Вид: WAV файл(ы)
 WAVE Внимание: формат изменён на %s
 Внимание: формат изменён на MU_LAW
 формат изменён на S16_BE
 Внимание: формат изменен на U8
 Внимание: неточный поток (запрошено = %iГц, получено = %iГц)
 Wave не поддерживает формат %s... Бас Ошибка записи: %d,%s
 Необходимо указать %d файлы Z/X/C - уменьшение громкости в левом/центральном/правом каналах. принято[%d]
 aconnect - ALSA менеджер подключений сиквенсера
 aseqnet - сетевой клиент/сервер на сиквенсере ALSA
 ошибка открытия аудио-устройства: %s неверное значение скорости %i невозможно использовать буфер, он слишком мал
 невозможно зарезервировать буфер для тишины не могу получить адрес %s
 невозможно получить идентификатор клиента
 невозможно распределить память
 невозможно открыть сиквенсер
 невозможно воспроизвести WAVE-файл формата 0x%04x, кодировка которого ни PCM, ни FLOAT невозможно воспроизведение WAVE-файлов с %d дорожками Невозможно проиграть петлю; %s не разыскиваемо
 невозможно проиграть упакованный .voc файл невозможно установить информацию клиента
 не могу перечислить звуковые карты Не удаётся загрузить панель управления микшером Не удаётся открыть микшер смена формата захватываемого потока? Пытаюсь восстановить...
 карта %i: %s [%s], устройство %i: %s [%s]
 клиент %d: '%s' [тип=%s]
 закрываю файлы..
 команда должна называться arecord или aplay Усиление дБ: отключён
 введите название устройства... ошибка информации: %s неверный индекс карты: %s
 неверный адрес получателя %s
 неверный адрес отправителя %s
 неверный адрес источника %s
 ядро ошибка распределения памяти отключить звук не найдено ни одной звуковой карты... ошибка параметра nonblock: %s недостаточно памяти ок.. подключён
 опции:
 перебор ошибка перехода в режим приостановки: %s ошибка снятия приостановления: %s Сырые данные ошибка чтения ошибка чтения (вызвана из строки %i) ошибка чтения: %s ошибка чтения/записи, статус = %s ошибка readv: %s сиквенсер открыт: %d:%d
 сервис '%s' не найден в /etc/services
 неполадка snd_pcm_mmap_begin: %s ошибка статуса: %s Ошибка установки флага stdin O_NONBLOCK
 приостановка: ошибка подготовки: %s слишком много соединений!
 попробуйте `alsamixer --help' для дополнительной информации.
 Невозможно установить параметры sw недобор неверный уровень рассеянности: %s
 неизвестный тип блока %d. останавливаюсь. продолжительность отрезка 'fmt' неизвестна (прочтено %u, минимум должно быть прочитано %u) неизвестная длина расширяемой части 'fmt ' (прочитано %u, а должно быть как минимум %u) неизвестная опция: %c
 неизвестный формат файла %s использование:
 пользователь значение %i для каналов не верно voc_pcm_flush - ошибка тишины ошибка voc_pcm_flush был установлен buffer_size = %lu
 был установлен period_size = %lu
 ошибка записи ошибка записи: %s ошибка writev: %s неправильный расширенный формат '%s' неправильный формат метки в расширяемой части 'fmt ' xrun(ИСТОЩЕНИЕ): ошибка подготовки: %s xrun: ошибка при подготовке: %s ошибка xrun_recovery: %d,%s
 