��          �      L      �     �  �   �  -   �  -   �  >   �     >  0   C  ;   t     �     �  "   �     �           :     H     d          �  �  �  B   ^  �  �  ]   >  R   �  >   �     .	  N   3	  o   �	  9   �	  %   ,
  K   R
  @   �
  F   �
  #   &  )   J  -   t  0   �  :   �                                
                          	                                          Convert key to lower case Copyright (C) %s Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 Create simple DB database from textual input. Do not print messages while building database INPUT-FILE OUTPUT-FILE
-o OUTPUT-FILE INPUT-FILE
-u INPUT-FILE NAME Print content of database file, one entry a line Report bugs using the `glibcbug' script to <bugs@gnu.org>.
 Write output to file NAME Written by %s.
 cannot open database file `%s': %s cannot open input file `%s' cannot open output file `%s': %s duplicate key problems while reading `%s' while reading database: %s while writing database file: %s wrong number of arguments Project-Id-Version: libnss-db
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2000-09-10 16:37+0200
PO-Revision-Date: 2010-03-23 15:11+0000
Last-Translator: Eugene Sysmanov <xghpro@gmail.com>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:03+0000
X-Generator: Launchpad (build 18115)
 Преобразовать ключ в нижний регистр Copyright (C) %s Free Software Foundation, Inc.
Это свободная программа; подробности об условиях распространения смотрите
в исходном тексте. Мы НЕ предоставляем гарантий; даже гарантий
КОММЕРЧЕСКОЙ ЦЕННОСТИ или ПРИГОДНОСТИ ДЛЯ КОНКРЕТНОЙ ЦЕЛИ.
 Создание простой DB базы данных из текстового ввода Не печатать сообщения при сборке базы данных INPUT-FILE OUTPUT-FILE
-o OUTPUT-FILE INPUT-FILE
-u INPUT-FILE NAME Печатать содержимое базы данных построчно Сообщите нам об ошибках при помощи скрипта `glibcbug' на <bugs@gnu.org>.
 Записать поток вывода в файл NAME Автор программы -- %s.
 невозможно открыть файл базы данных `%s': %s невозможно открыть входной файл `%s' невозможно открыть выходной файл `%s': %s Дуплицировать ключ проблемы при чтении `%s' при чтении базы данных: %s при записи в файл данных: %s Неверное количество аргументов 