��          4      L       `   j  a      �  �  �  R  �  B                       Usage:
tasksel install <task>...
tasksel remove <task>...
tasksel [options]
	-t, --test          test mode; don't really do anything
	    --new-install   automatically install some tasks
	    --list-tasks    list tasks that would be displayed and exit
	    --task-packages list available packages in a task
	    --task-desc     returns the description of a task
 apt-get failed Project-Id-Version: tasksel_po_ru
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-02 02:46+0000
PO-Revision-Date: 2015-12-22 01:19+0000
Last-Translator: Yuri Kozlov <kozlov.y@gmail.com>
Language-Team: Russian <debian-l10n-russian@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:37+0000
X-Generator: Launchpad (build 18115)
Language: ru
10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Использование:
tasksel install <задание>...
tasksel remove <задание>...
tasksel [опции]
	-t --test           тестовый режим; ничего не устанавливается
	    --new-install   автоматическая установка некоторых заданий
	    --list-tasks    показать список возможных заданий и выйти
	    --task-packages показать список доступных пакетов в задании
	    --task-desc     показать описание задания
 программа apt-get завершилась неудачно 