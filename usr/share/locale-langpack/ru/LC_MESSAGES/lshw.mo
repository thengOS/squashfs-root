��         �  o  <      �  (   �  )   "  (   L  (   u  7   �  A   �  @     .   Y  9   �  &   �  Z   �  ,   D  -   q     �     �     �     �     �     �     �     �     �            	                  #     +     2     ;     C     J     ]     o     t     {     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �  	   �  	   �               #     :     F     \     o     �  
   �     �     �     �     �  %   �          #     (     ,     B     F  
   `     k     x     �     �     �     �     �     �     �     �               5      M     n     �     �     �     �     �     �  
   �     �     �     �            
   %     0     ?     K     \     c     s     w     �     �     �     �     �     �     �     �     �            
             (     5     H     [     r  
   �  $   �     �  	   �     �     �     �     �     �     �                 !      *      3   	   :      D      P      `      l      ~      �      �      �      �      �      �      �      	!     !     #!     /!     5!  	   E!     O!     U!     d!  	   y!     �!     �!     �!     �!     �!     �!     �!     �!     �!     "     -"     9"     I"     Q"     d"     r"     v"     �"     �"     �"     �"     �"     �"     �"     �"     
#     "#     :#     G#     N#     a#     f#     #     �#     �#     �#     �#     �#     �#     �#     �#     $     $     !$     /$     K$     Y$     h$     w$     $     �$     �$     �$     �$     �$     �$     �$  \   �$  4   Z%     �%     �%     �%  
   �%     �%     �%     �%     �%     �%     �%     �%     �%     &     &      &     2&     A&     M&     m&     q&     �&     �&     �&     �&     �&     �&     �&     �&     �&      '     '     '     "'     ('  	   D'     N'  
   \'     g'     n'     s'     x'     �'     �'     �'  #   �'     �'     �'     �'  �  �'  ?   �)  T   �)  P   8*  ?   �*  R   �*  c   +  a   �+  M   �+  l   0,  8   �,  y   �,  A   P-  L   �-  "   �-  (   .     +.     ?.     F.     M.     c.     h.     n.     |.     �.     �.     �.     �.     �.     �.     �.     �.     �.  "   �.  &   �.     /     /     %/     +/     0/     K/     R/     Y/     _/     d/     m/     s/     z/     �/     �/     �/     �/     �/     �/     �/  ,   �/     0  +   !0     M0  *   `0     �0     �0      �0     �0     �0     1      *1  /   K1  X   {1  %   �1     �1     �1  !   2     %2  <   )2     f2     {2  %   �2     �2  5   �2     
3  0   3     @3     O3     S3  5   r3  -   �3     �3     �3  '   4     ;4     V4     q4     w4     �4     �4  .   �4  
   �4     �4  
   �4  5   �4     /5  )   D5     n5     �5     �5  '   �5     �5     �5     6  %   
6     06     >6     C6     O6  !   [6     }6     �6     �6  -   �6     �6     �6  
   �6     7     7  '   .7  %   V7     |7     �7  $   �7  A   �7     �7     �7     8     $8  #   )8     M8     Z8  '   v8     �8     �8  	   �8  	   �8     �8  #   �8     9     %9     E9  '   a9  F   �9  #   �9  #   �9     :     %:  R   *:  P   }:     �:  /   �:  
   ;  !   #;     E;     N;  	   f;     p;     v;  ,   �;  	   �;  *   �;     �;     <  *   <     8<  )   E<  )   o<  ;   �<  '   �<  /   �<     -=  #   9=     ]=  &   j=     �=     �=  '   �=     �=  +   �=     >  )   >     >>     Z>  !   i>  :   �>  .   �>  :   �>     0?     @?  G   G?     �?     �?     �?  H   �?     �?     @  %   !@  +   G@     s@     �@     �@     �@  2   �@     �@  F   A     WA     wA  *   �A     �A  ?   �A      B     7B     WB     lB  -   �B  
   �B     �B  �   �B  �   �C     PD  +   \D     �D  
   �D     �D     �D     �D     �D  
   �D     �D     E     E     3E     BE     [E     sE     �E  5   �E     �E  "   �E     �E  /   F  +   4F     `F     dF     �F  8   �F     �F  1   �F     G     G     0G     ?G  6   PG     �G     �G     �G     �G     �G     �G  $   �G  /   H     6H     CH  I   WH     �H     �H     �H     �   w   �   �   �   3   �       �   �           �         �   �           �   $       %     o     8   g   �   �              q   V   P           E   �      S   *       �   I   A   /      �   +       �   �   _       l   �          �       �   �           �   �   �   �   <       Q   �   �   	   U   �   �   �   #       �   �       N           5   �   �       �   �   �     "      �   f   �   v       �   t         C   �      Y   �   n   �           9   2          �   �       �   �   �   u       �   �   �                   0   �   1       �      �       (   !   �   �   �   �               r   �   ]   �   �   :       �   ,        i   �   `   �   c   k   �   �   {         �         �   �       �   M   x   F          y       D   �       �   W              �   e   �   a   �      O   �   �         �   �   �          �   &   B   �   �       �   -   �           �       �   T   �              4   )   m   }   �      �   H   �       �   �                 �   K      	  R               �   �   �   �   �              X       Z                  @                  �   J          �      \   >   
       �   �          ;       h       �   �   �   �   ?   ~          �   =   L   �       7          z   p          ^         �   G   �       6   
            s   .          �           �   b   �       �   �   |   �           [   d   �      �   �     �       j   �   �   �              �       �       �   '    	-C CLASS        same as '-class CLASS'
 	-X              use graphical interface
 	-businfo        output bus information
 	-c CLASS        same as '-class CLASS'
 	-class CLASS    only show a certain class of hardware
 	-disable TEST   disable a test (like pci, isapnp, cpuid, etc. )
 	-enable TEST    enable a test (like pci, isapnp, cpuid, etc. )
 	-html           output hardware tree as HTML
 	-numeric        output numeric IDs (for PCI, USB, etc.)
 	-quiet          don't display status
 	-sanitize       sanitize output (remove sensitive information like serial numbers, etc.)
 	-version        print program version (%s)
 	-xml            output hardware tree as XML
 
format can be
 
options can be
        %s -version
  3DRAM  CDRAM  Chip  DDR  DDR2  DDR2 FB-DIMM  DIMM  DIP  DISABLED  DRAM  EDRAM  EEPROM  EPROM  FB-DIMM  FEPROM  FLASH  Memory Controller  Proprietary Card  RAM  RDRAM  RIMM  ROM  Row of chips  SDRAM  SGRAM  SIMM  SIP  SODIMM  SRAM  SRIMM  TSOP  VRAM  ZIP %lx(size=%ld) (nil) (none) (unknown) 100Mbit/s 100Mbit/s (full duplex) 10Mbit/s 10Mbit/s (full duplex) 16TB+ files 1Gbit/s (full duplex) 3.5" 2.88MB floppy 3.5" 720KB floppy 32-bit processes 4GB+ files 5.25" 1.2MB floppy 5.25" 360KB floppy 64-bit processes 64bit filesystem <!-- WARNING: not running as root --> <!-- generated by lshw- ACPI AGP AT (Hayes) compatible AUI Advanced Power Management All In One Asynchronous Audio device Audio streaming Auto-negotiation BIOS BIOS EEPROM can be upgraded BIOS shadowing BNC Bidirectional Bluetooth wireless interface Bluetooth wireless radio Booting from ATAPI ZIP Booting from CD-ROM/DVD Booting from IEEE1394 (Firewire) Booting from LS-120 Booting from PCMCIA Burst Bus info CMOS CPU CPU virtualization (Vanderpool) Cache DRAM Cache Memory Class Communication device Connectivitiy Switch Control device Data cache Daughter Board Description Desktop Computer Device Docking Station EDO EISA add-on card EISA bus ESCD EXT3 volume EXT4 volume Encrypted volume Ethernet Ethernet networking Expansion Chassis Extended Attributes External FDDI Fast-paged Flash Memory Floppy (UFI) Generic USB device Hand Held Computer Human interface device HyperThreading I/O Module IEEE 1284.4 compatible bidirectional IEEE1394 IP6tunnel IPtunnel IRDA ISA add-on card ISA bus Instruction cache Interconnect Board Internal Keyboard L1 cache L2 cache Laptop Lead Acid Lithium Ion Lithium Polymer Logical CPU Logical interface Low Profile Desktop Computer Lunch Box Computer MCA add-on card MCA bus MIDI Mass storage device Media Independant Interface Memory Module Mini Tower Computer Modem Motherboard Mouse NEC 9800 floppy NEC PC-98 NVRAM Nickel Cadmium Nickel Metal Hydride Non-burst Non-volatile Notebook NuBus OBEX networking Other PC-98/C20 add-on card PC-98/C24 add-on card PC-98/Card slot add-on card PC-98/E add-on card PC-98/Local bus add-on card PCI Express PCI add-on card PCI bus PCMCIA add-on card PCMCIA/PCCard PPP Physical interface Pipeline burst Pizza Box Computer Plug-and-Play Portable Computer Print Screen key Printer Processor Module Processor/IO Module  Processor/Memory Module Proprietary add-on card RAID Chassis RAMBUS Rack Mount Chassis SCSI SFF-8020i, MMC-2 (ATAPI) SLIP Selectable boot path Server Blade Smart battery Smart card reader Space-saving Computer Sub Chassis Sub Notebook Synchronous System System Management Module System Memory System board or motherboard System memory Toshiba floppy Tower Computer USB hub USB legacy emulation Unidirectional Unified cache Unknown VESA video extensions Varies With Memory Address Video Video Memory WARNING: output may be incomplete or inaccurate, you should run this program as super-user.
 WARNING: you should run this program as super-user.
 Window DRAM Wireless interface Wireless-LAN Write-back Write-trough X25 Zinc Air [empty] address bus info cache capabilities clock configuration configuration of  configuration: description directories with 65000+ subdirs dma empty memory bank false i8042 keyboard controller initialized volume irq logical name loopback make outbound connections memory needs recovery optical fibre physical id product range receive inbound connections resources resources of  resources: serial size slot the latest version is %s
 this device has been disabled
 true twisted pair usage: %s [-format] [-options ...]
 vendor version width Project-Id-Version: lshw
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-10-08 14:02+0200
PO-Revision-Date: 2015-01-21 09:55+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:08+0000
X-Generator: Launchpad (build 18115)
Language: ru_RU
 	-C КЛАСС        то же, что и '-class КЛАСС'
 	-X              использовать графический интерфейс
 	-businfo        вывод информации о системной шине
 	-c КЛАСС        то же, что и '-class КЛАСС'
 	-class КЛАСС    только заданный класс устройств
 	-disable ТЕСТ   отключить тест (например, pci, isapnp, cpuid и т. п.)
 	-enable ТЕСТ    включить тест (например, pci, isapnp, cpuid и т. п.)
 	-html           вывод дерева устройств в виде HTML
 	-numeric        выводить цифровые идентификаторы (для PCI, USB и т.п.)
 	-quiet          не показывать статус
 	-sanitize       не выводить секретную информацию (серийные номера и т.п.)
 	-version        вывод версии программы (%s)
 	-xml            вывод дерева устройств в виде XML
 
возможный формат:
 
возможные параметры:
        %s -version
  3DRAM  CDRAM  Микросхема  DDR  DDR2  DDR2 FB-DIMM  DIMM  DIP  ВЫКЛЮЧЕНО  DRAM  EDRAM  EEPROM  EPROM  FB-DIMM  FEPROM  FLASH  Контроллер памяти  Проприетарная карта  RAM  RDRAM  RIMM  ROM  Ряд микросхем  SDRAM  SGRAM  SIMM  SIP  SO-DIMM  SRAM  SRIMM  TSOP  VRAM  ZIP %lx(размер=%ld) (nil) (отсутствует) (неизвестно) 100Мбит/с 100Мбит/с (полный дуплекс) 10Мбит/с 10Мбит/с (полный дуплекс) 16ТБ+ файлы 1Гбит/с (полный дуплекс) Дискета 3.5" 2.88MB Дискета 3.5" 720KB 32-битные процессы 4 ГБ+ файлы Дискета 5.25" 1.2MB Дискета 5.25" 360KB 64-битные процессы 64-битная файловая система <!-- ВНИМАНИЕ: программа запущена не от имени root --> <!-- сгенерировано lshw- ACPI AGP AT (Hayes) совместимый AUI Расширенное управление питанием Всё-в-одном Асинхронная Звуковое устройство Звуковой поток Автоматическое согласование BIOS BIOS EEPROM может быть обновлён BIOS shadowing BNC Двунаправленный Беспроводной интерфейс Bluetooth Беспроводная связь Bluetooth Загрузка с ATAPI ZIP Загрузка с CD-ROM/DVD Загрузка с IEEE1394 (Firewire) Загрузка с LS-120 Загрузка с PCMCIA Burst Сведения о шине CMOS ЦПУ Виртуализация ЦПУ (Vanderpool) Cache DRAM Кэш-память Класс Коммуникационное устройство Коммутатор Устройство управления Кэш данных Мезонинная плата Описание Настольный компьютер Устройство Док-станция EDO Плата расширения  EISA Шина EISA ESCD Том EXT3 Том EXT4 Зашифрованный том Ethernet Сеть Ethernet Шасси расширения Дополнительные атрибуты Внешний FDDI Fast-paged Флэш-память Флоппи (UFI) Типовой USB-накопитель Карманный компьютер HID HyperThreading Модуль ввода/вывода IEEE 1284.4 совместимый двунаправленный IEEE1394 Туннель IP6 IP-туннель IRDA Плата расширения ISA Шина ISA Кэш инструкций Соединительная плата Внутренний Клавиатура Кэш L1 Кэш L2 Лэптоп Свинцово-кислотный Литий-ионный Литий-полимерный Логический ЦПУ Логический интерфейс Низкопрофильный настольный компьютер Кейсовый компьютер Плата расширения MCA Шина MCA MIDI Устройство класса устройств хранения данных Независящий от среды передачи интерфейс (MII) Модуль памяти Компьютер в корпусе Mini Tower Модем Материнская плата Мышь Дискета NEC 9800 NEC PC-98 NVRAM Никель-кадмиевый Никель-металл-гидридный Non-burst Энергонезависимая (NVRAM) Ноутбук NuBus Сетевые возможности OBEX Другой Плата расширения PC-98/C20 Плата расширения PC-98/C24 Плата расширения PC-98/слота карты Плата расширения PC-98/E Плата расширения PC-98/Local bus PCI Express Плата расширения PCI Шина PCI Карта расширения PCMCIA PCMCIA/PCCard PPP Физический интерфейс Pipeline burst Тонкий компьютер (Pizza Box) Plug-and-Play Портативный компьютер Клавиша Print Screen Принтер Модуль процессора Модуль ввода/вывода процессора  Модуль процессора/памяти Проприетарная плата расширения RAID-шасси RAMBUS Корпус с возможностью монтажа в стойку SCSI SFF-8020i, MMC-2 (ATAPI) SLIP Выбираемое загрузочное местоположение Блэйд-сервер Smart battery Считыватель Smart-карт Эргономичный компьютер Субшасси Субноутбук Синхронная Система Модуль управления системой Системная память Системная плата или материнская плата Системная память Дискета Toshiba Компьютер в корпусе Tower USB-концентратор Эмуляция устаревших устройств с USB Однонаправленный Объединённый кэш Неизвестно VESA video extensions Зависит от адреса памяти Видео Видеопамять ПРЕДУПРЕЖДЕНИЕ: выходная информация может быть неполной или неточной. Следует запустить эту программу от имени суперпользователя.
 ПРЕДУПРЕЖДЕНИЕ: эту программу нужно запускать от имени суперпользователя.
 Window DRAM Беспроводной интерфейс Беспроводная ЛВС Write-back Write-trough X25 Цинк-воздушный [пусто] адрес сведения о шине кэш возможности частота конфигурация кофигурация  конфигурация: описание каталоги с 65000+ подкаталогами DMA пустой банк памяти ложь Контроллер клавиатуры i8042 инициализированный том IRQ логическое имя loopback создание исходящих соединений память необходимо восстановление оптоволокно физический ID продукт диапазон создание входящих соединений ресурсы ресурсы  ресурсы: серийный № размер слот последняя версия: %s
 это устройство отключено
 истина витая пара использование: %s [-формат] [-параметры ...]
 производитель версия разрядность 