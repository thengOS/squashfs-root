��    -      �  =   �      �     �  "   �  @        _     v     |  
   �  4   �  .   �  9   �  +   -     Y     `     v     �     �     �  	   �     �     �     �            @   '     h     t  �   y          $     )     1  	   6     @     H     \     t     �  &   �     �     �     �  $     /   ,     \  r  e      �	  ?   �	  W   9
  G   �
     �
  
   �
     �
  X     U   e  k   �  j   '     �  !   �  >   �  <      .   =  
   l     w  '   �  "   �  )   �     �  2     i   >  $   �     �  )  �  9        >     G     Z  3   ]     �  '   �  4   �     �  !     P   ;     �  :   �  ;   �  G     k   `  	   �           
                                                     %                     )          *   "       !          -      ,      &                $         	   '      (       #             +                       Byobu Configuration Menu  file exists, but is not a symlink <Tab>/<Alt-Tab> between elements | <Enter> selects | <Esc> exits Add to default windows Apply Archive Byobu Help Byobu currently does not launch at login (toggle on) Byobu currently launches at login (toggle off) Byobu will be launched automatically next time you login. Byobu will not be used next time you login. Cancel Change Byobu's colors Change escape sequence Change escape sequence: Change keybinding set Choose Command:  Create new window(s): Create new windows ERROR: Invalid selection Error: Escape key: ctrl- Extract the archive in your home directory on the target system. File exists Help If you are using the default set of keybindings, press\n<F5> to activate these changes.\n\nOtherwise, exit this screen session and start a new one. Manage default windows Menu Message Okay Presets:  Profile Remove file? [y/N]  Run "byobu" to activate Select a color:  Select a screen profile:  Select window(s) to create by default: Title:  Toggle status notifications Toggle status notifications: Which profile would you like to use? Which set of keybindings would you like to use? Windows: Project-Id-Version: ru
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-11-23 20:48-0600
PO-Revision-Date: 2012-01-14 08:13+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:40+0000
X-Generator: Launchpad (build 18115)
Language: 
  Меню настроек Byobu  файл существует, но это не симлинк <Tab>/<Alt-Tab> между элементами | <Enter> выбор | <Esc> выход Добавить к окнам значения по умолчанию Применить Архив Справка Byobu Byobu не запускается при входе в систему (включить) Byobu запускается при входе в систему (выключить) Byobu будет автоматически запущен при вашем следующем входе. Byobu не будет использоваться при следующем входе в систему. Отмена Изменить цвета Byobu Изменение Escape-последовательности Изменить Escape-последовательность Изменить привязки клавиш Выбор Команда:  Создать новое окно(а): Создать новые окна ОШИБКА: неверный выбор Ошибка: Escape-последовательность: ctrl- Распакуйте архив в ваш домашний раздел в целевой системе. Файл уже существует Помощь Если Вы хотите использовать стандартный набор сочетаний клавиш, нажмите \n<F5> для активации изменений.\n\nВ противном случае, выйдите из сессии и запустите еще одну. Управление окнами по умолчанию Меню Сообщение OK Предварительная настройка:  Профиль Удалить файл? (да/нет)  Запустите "byobu" для активации Выберите цвет:  Выберите профиль:  Выберите окно(на) для создания по умолчанию: Заголовок:  Изменить уведомления состояния Изменить уведомления состояний: Какой профайл вы желаете использовать? Какой набор привязок к клавишам хотели бы Вы использовать? Окна: 