��            )   �      �     �     �     �     �     �     �     	          5     I     _     k     x     �     �     �  /   �     �     �               #  !   0  #   R  "   v     �  
   �  
   �  �  �     X     v     �  J   �  +   �  .   "  ?   Q  =   �  &   �  0   �     '  %   >  &   d  7   �  6   �     �  [   	     u	     �	  !   �	     �	  (   �	  @    
  :   A
  9   |
     �
      �
     �
                                                                	   
                                                                       (replace text) <b>Input/Output</b> <b>Options</b> Allow type with more freedom Capture _mouse event Capture mouse event Choose file to export Choose file to import Choose input method Choose output charset Delete _all Enable Macro Enable _macro Enable _spell check Enable spell check IBus-Unikey Setup If enable, you can decrease mistake when typing Input method: Options Output charset:  Replace with Run Setup... Run setup utility for IBus-Unikey Use oà, _uý (instead of òa, úy) Use oà, uý (instead of òa, úy) _Edit macro _Export... _Import... Project-Id-Version: ibus-unikey
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-06-11 17:18+0700
PO-Revision-Date: 2012-04-19 07:38+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:44+0000
X-Generator: Launchpad (build 18115)
 (заменить текст) <b>Ввод/Вывод</b> <b>Параметры</b> Позвольте вводить текст более свободной Захватить событие _мыши Захватывать событие мыши Выберите файл для экспортирования Выберите файл для импортирования Выберите метод ввода Выберите кодировку вывода Удалить _все Использовать макрос Использовать _макрос Включить _проверку орфографии Включить проверку орфографии Настройка IBus-Unikey Включение уменьшает количество ошибок при наборе Метод ввода: Параметры Кодировка вывода:  Заменить на Запустить настройку... Запустить утилиту настройки IBus-Unikey Использовать oà, _uý (вместо òa, úy) Использовать oà, uý (вместо òa, úy) _Изменить макрос _Экспортировать… _Импортировать… 