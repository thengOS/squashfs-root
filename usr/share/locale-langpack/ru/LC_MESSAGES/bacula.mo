��          �   %   �      0     1  "   D  3   g  D   �  B   �     #     =  %   D     j     �  ,   �     �      �  (   �     
  "        3     N     c     f  &   l  &   �     �  �  �  &   �     �  S   �  D   G  e   �  .   �     !  4   .  .   c     �  a   �     	  +   	  7   B	     z	  ,   �	  3   �	  !   �	     

     
  O   
  4   j
  4   �
                                                                         
                                  	                    
Terminated Jobs:
 %6d  %-6s %8s %10s  %-7s  %-8s %s
 %s item is required in %s resource, but not found.
 ===================================================================
 Attempt to define second %s resource named "%s" is not permitted.
 Authentication error : %s Cancel Client: name=%s address=%s FDport=%d
 Connecting to Client %s:%d Created Cryptography library initialization failed.
 Diffs Director rejected Hello command
 Director: name=%s address=%s DIRport=%d
 Error Error : BNET_HARDEOF or BNET_ERROR Error : Connection closed. No record for %d %s
 OK Other Please correct configuration file: %s
 Storage: name=%s address=%s SDport=%d
 Unknown resource type %d
 Project-Id-Version: bacula
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-07-29 18:18+0200
PO-Revision-Date: 2013-03-07 06:17+0000
Last-Translator: Markelov Anton <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:24+0000
X-Generator: Launchpad (build 18115)
X-Poedit-Country: RUSSIAN FEDERATION
X-Poedit-Language: Russian
 
Прерванные Задания:
 %6d %-6s %8s %10s %-7s %-8s %s
 %s элемент требуется в %s ресурсе, но не найден.
 ===================================================================
 Попытка дать ресурсу %s второе имя "%s" не была разрешена.
 Ошибка аутентификации : %s Отмена Клиент: имя=%s адрес=%s FDпорт=%d
 Соединение с клиентом %s:%d Создано Ошибка инициализации криптографической библиотеки.
 Изменения Director отверг команду Hello
 Director: имя=%s адрес=%s порт Director=%d
 Ошибка Ошибка: BNET_HARDEOF или BNET_ERROR Ошибка : соединение закрыто. Нет записи для %d %s
 OK Прочее Пожалуйста, исправьте файл конфигурации: %s
 Данные: имя=%s адрес=%s SDпорт=%d
 Неизвестный тип источника %d
 