��          �            h  /   i  /   �  >   �  H     I   Q  H   �  I   �  H   .  I   w  H   �  H   
  J   S  H   �  I   �  �  1  c   �  c   /  v   �  �   
  }   �  z   1	  z   �	  |   '
  �   �
  x   %  z   �  |     |   �  |                     	                           
                                   Meant for writing Esperanto-letters adding h's. Meant for writing Esperanto-letters adding q's. Meant for writing Esperanto-letters adding x's (the X-system). Meant for writing Esperanto-letters using Zamenhof's fundamental system. Select the 10th candidate
Select the tenth candidate in the current group Select the 1st candidate
Select the first candidate in the current group Select the 2nd candidate
Select the second candidate in the current group Select the 3rd candidate
Select the third candidate in the current group Select the 4th candidate
Select the fourth candidate in the current group Select the 5th candidate
Select the fifth candidate in the current group Select the 6th candidate
Select the sixth candidate in the current group Select the 7th candidate
Select the seventh candidate in the current group Select the 9th candidate
Select the ninth candidate in the current group select the 8th candidate
Select the eighth candidate in the current group Project-Id-Version: m17n-db
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-12-12 23:29+0900
PO-Revision-Date: 2014-07-11 11:08+0000
Last-Translator: ☠Jay ZDLin☠ <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:34+0000
X-Generator: Launchpad (build 18115)
 Предназначено для письма на Эсперанто с добавлением h. Предназначено для письма на Эсперанто с добавлением q. Предназначено для письма на Эсперанто с добавлением x (X-система). Предназначено для письма на Эсперанто с использованием фундаментальной системы Заменгофа. Выбрать 10-го кандидата
Выбрать десятого кандидата из текущей группы Выбрать 1-го кандидата
Выбрать первого кандидата из текущей группы Выбрать 2-го кандидата
Выбрать второго кандидата из текущей группы Выбрать 3-го кандидата
Выбрать третьего кандидата из текущей группы Выбрать 4-го кандидата
Выбрать четвёртого кандидата из текущей группы Выбрать 5-го кандидата
Выбрать пятого кандидата из текущей группы Выбрать 6-го кандидата
Выбрать шестого кандидата из текущей группы Выбрать 7-го кандидата
Выбрать седьмого кандидата из текущей группы Выбрать 9-го кандидата
Выбрать девятого кандидата из текущей группы Выбрать 8-го кандидата
Выбрать восьмого кандидата из текущей группы 