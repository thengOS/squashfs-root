��    F      L  a   |                      1  G   B  >   �  "   �  (   �       (   3  6   \     �  -   �  (   �     	     "  +   @  $   l  %   �  2   �  (   �  "   	  %   6	  .   \	  -   �	     �	  0   �	  >   
  2   C
  -   v
     �
     �
  @   �
             "   6  B   Y  
   �  ;   �  :   �          8  (   H  4   q     �  B   �  *   �     &  �   5     �  &   �     �  �     :   �  &   �  $  &  K   K  -   �     �  %   �     �           ,     K     M     O     Q     S     U     W  �  Y       B        Z  �   m  �   �  9   z  X   �  5     Y   C  c   �  3     H   5  >   ~  ,   �  W   �  N   B  C   �  U   �  b   +  Q   �  :   �  7     G   S  M   �  #   �  O     o   ]  @   �  F     ?   U  '   �  }   �  ;   ;  =   w  A   �  �   �     �  ~   �  U   ,  -   �     �  U   �  z     &   �  s   �  B   4      w   �   �      q!  .   �!  ,   �!  �  �!  x   �#  M   3$  �  �$  �   &  o   �&     
'  >   '  $   L'  '   q'  A   �'     �'     �'     �'     �'     �'     �'     �'                0       ;       @             =       *      >   B   -      D   9      &   F   !   /       $                     1                 5   3      "   %   #                 E      6             .   
             C       7          	   +                               :       <   '          8             2   ,   4   (      A       ?              )    	%s		File: %s

 
-- Type space to continue -- 
 
Commands are:

 
[SPACE] R)epl A)ccept I)nsert U)ncap S)tem Q)uit e(X)it or ? for help
   -1		check only first field in lines (delimiter = tabulator)
   -D		show available dictionaries
   -G		print only correct words or lines
   -H		HTML input file format
   -L		print lines with misspelled words
   -P password	set password for encrypted dictionaries
   -a		Ispell's pipe interface
   -d d[,d2,...]	use d (d2 etc.) dictionaries
   -h, --help	display this help and exit
   -i enc	input encoding
   -l		print misspelled words
   -m 		analyze the words of the input text
   -n		nroff/troff input file format
   -p dict	set dict custom dictionary
   -r		warn of the potential mistakes (rare words)
   -s 		stem the words of the input text
   -t		TeX/LaTeX input file format
   -v, --version	print version number
   -vv		print Ispell compatible version number
 0-n	Replace with one of the suggested words.
 ?	Show this help screen.
 A	Accept the word for the rest of this session.
 AVAILABLE DICTIONARIES (path is not mandatory for -d option):
 Are you sure you want to throw away your changes?  Bug reports: http://hunspell.sourceforge.net
 Can't create tempfile Can't open %s.
 Can't open affix or dictionary files for dictionary named "%s".
 Can't open inputfile Can't open outputfile Cannot update personal dictionary. Check spelling of each FILE. Without FILE, check standard input.

 FORBIDDEN! Hunspell has been compiled without Ncurses user interface.
 I	Accept the word, and put it in your private dictionary.
 LOADED DICTIONARY:
%s
%s
 Line %d: %s ->  Model word (a similar dictionary word):  Model word must be in the dictionary. Press any key! New word (stem):  Q	Quit immediately. Asks for confirmation. Leaves file unchanged.
 R	Replace the misspelled word completely.
 Replace with:  S	Ask a stem and a model word and store them in the private dictionary.
	The stem will be accepted also with the affixes of the model word.
 SEARCH PATH:
%s
 Space	Accept the word this time only.
 Spelling mistake? This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE,
to the extent permitted by law.
 U	Accept and add lowercase version to private dictionary.
 Usage: hunspell [OPTION]... [FILE]...
 Whenever a word is found that is not in the dictionary
it is printed on the first line of the screen.  If the dictionary
contains any similar words, they are listed with a number
next to each one.  You have the option of replacing the word
completely, or choosing one of the suggested words.
 X	Write the rest of this file, ignoring misspellings, and start next file.
 ^Z	Suspend program. Restart with fg command.
 a error - %s exceeds dictionary limit.
 error - iconv_open: %s -> %s
 error - iconv_open: UTF-8 -> %s
 error - missing HOME variable
 i q r s u x y Project-Id-Version: hunspell
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-06-02 13:54+0200
PO-Revision-Date: 2014-09-10 15:11+0000
Last-Translator: Pavel Frolov <pavel.frolov.94@gmail.com>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
 	%s		Файл: %s

 
-- Нажмите пробел для продолжения -- 
 
Команды:

 
[ПРОБЕЛ] З)аменить П)ринять Д)обавить Н)ижРег О)снова В(ы)ход К)онец ?) Справка
   -1		проверять только первое поле строки (разделитель = знак табуляции)
   -D		показать доступные словари
   -G		показать только правильные слова или строки
   -H		входной файл в формате HTML
   -L		показать строки с орфографическими ошибками
   -P password	установить пароль для зашифрованных словарей
   -a		Канальный интерфейс Ispell
   -d d[,d2,...]	использовать d (d2 и др.) словари
   -h, --help	вывести этот текст и выйти
   -i имя	кодировка текста
   -l		показать слова с орфографическими ошибками
   -m 		анализировать слова во входном тексте
   -n		входной файл в формате nroff или troff
   -p словарь	использовать словарь пользователя
   -r		предупреждать о возможных ошибках (редких словах)
   -s 		выделить основы из слов входного текста
   -t		входной файл в формате TeX/LaTeX
   -v, --version	вывести номер версии
   -vv		вывести номер версии в формате Ispell
 0-n	Заменить слово на одно из предложенных.
 ?	Показать справку.
 П	Пропустить слово до завершения проверки.
 ДОСТУПНЫЕ СЛОВАРИ (при использовании -d путь можно опустить):
 Вы хотите отказаться от изменений?  Сообщить об ошибках: http://hunspell.sourceforge.net
 Невозможно создать временный файл Не удалось открыть %s.
 Невозможно открыть дополнительные или словарные файлы словаря «%s».
 Невозможно открыть входной файл Невозможно открыть выходной файл Не удалось обновить личный словарь. Проверить правописание каждого ФАЙЛА. Если ФАЙЛ не указан, проверить стандартный ввод.

 ЗАПРЕЩЕНО! Hunspell была скомпилирована без поддержки диалогового интерфейса Ncurses.
 Д	Пропустить и занести слово в личный словарь.
 ЗАГРУЖЕННЫЙ СЛОВАРЬ:
%s
%s
 Строка %d: %s ->  Образец (слово из словаря с такими же формами):  Слово-образец должно содержаться в словаре! Нажмите любую клавишу. Новое слово (основа):  Ы	Выход с запросом подтверждения. Файл остается без изменений.
 З	Заменить ошибочное слово целиком.
 Заменить на:  О	Запросить основу и слово-образец и сохранить их в личном словаре.
	Основа будет добавлена со всеми формами слова-образца.
 ПУТЬ ПОИСКА:
%s
 Пробел	Пропустить слово.
 Орфографическая ошибка? Это свободно распространяемое программное обеспечение; условия копирования есть в исходных текстах.
Здесь НЕТ никаких гарантий; ни гарантий ТОВАРНЫХ СВОЙСТВ, ни гарантий ПРИГОДНОСТИ ДЛЯ
ПРИМЕНЕНИЯ ПО НАЗНАЧЕНИЮ, во всех установленных законом случаях.
 Н	Пропустить и добавить слово в нижнем регистре в личный словарь.
 Использование: hunspell [ПАРАМЕТРЫ]... [ФАЙЛЫ]...
 Если найдено слово, отсутствующее в словаре, оно появится
в первой строке экрана. Если в словаре есть похожие слова,
будет показан их пронумерованный список. Можно заменить
слово целиком или выбрать одно из предложенных.
 К	Записать оставшуюся часть файла без проверки, и перейти к следующему.
 ^Z	Приостановить программу. Для возврата к ней используйте fg.
 п ошибка - %s превышен лимит словаря.
 ошибка - iconv_open: %s -> %s
 ошибка - iconv_open: UTF-8 -> %s
 ошибка - отсутствует переменная  HOME
 д ы з о н к д 