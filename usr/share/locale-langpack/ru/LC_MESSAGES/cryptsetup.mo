��    �      4    L      h     i  �     <   >     {     �     �  !   �     �     �            0   %  7   V     �     �     �     �  &   �  "   �  %      $   F  /   k  ,   �     �  !   �       "   !  A   D     �     �  #   �  &   �        #        <  &   U  )   |  )   �     �     �  !   �     !  $   :  .   _     �     �     �  %   �  F   �  2   F     y     �  *   �     �     �            )   7  ,   a     �  .   �     �  ,   �  6     5   Q     �     �  #   �     �     �  "         6      W     x  !   �  1   �  8   �       (   3     \  #   w  ,   �     �  *   �  '     *   5  %   `     �  !   �  �   �     t  ?   �  4   �     �          %     8  &   X  .     %   �  !   �     �  0     8   =  @   v     �     �  #   �        #   %      I   0   f      �      �      �   )   �   '   �   '   !  /   9!  9   i!  "   �!  /   �!  6   �!  <   -"  )   j"     �"  |   �"  =   1#  6   o#  :   �#  (   �#  &   
$     1$  @   L$     �$     �$  (   �$  )   �$  0   %  3   A%  0   u%  '   �%  '   �%     �%     &     &     )&  "   @&  /   c&  �   �&  6   '  >   U'  %   �'     �'     �'  &   �'  M   (  5   e(  6   �(  2   �(  +   )  6   1)     h)     �)     �)     �)     �)  *   �)  +   *     E*  .   _*     �*      �*     �*     �*     �*  &   +  R   ?+  G   �+     �+     �+  N   ,  O   ^,     �,     �,     �,  ,   -     0-     6-     P-     e-     j-  (   }-  1   �-  �  �-  )   u/  {  �/  �   1  ;   �1     �1  ,   2  3   .2  ,   b2  9   �2     �2  1   �2  ^   3  h   c3  /   �3  ;   �3  $   84     ]4  z   d4  I   �4  a   )5  Y   �5  u   �5  q   [6  H   �6  Y   7  K   p7  f   �7  y   #8  ;   �8  =   �8  b   9  Z   z9  A   �9  f   :  E   ~:  ]   �:  c   ";  p   �;  ?   �;  ?   7<  T   w<  R   �<  d   =  d   �=  X   �=  2   B>  V   u>  <   �>  �   	?  v   �?     G@  5   d@  d   �@  @   �@  -   @A  @   nA  ;   �A  S   �A  ^   ?B  4   �B  r   �B  6   FC  X   }C  m   �C  R   DD  R   �D  (   �D  M   E  9   aE  '   �E  N   �E  C   F  A   VF     �F  U   �F  X   G  ~   eG  ;   �G  M    H  5   nH  B   �H  ~   �H  >   fI  l   �I  T   J  e   gJ  :   �J  9   K  N   BK  g  �K     �L  k   M  e   �M  &   �M  *   N  *   8N  H   cN  Q   �N  e   �N  R   dO  N   �O  '   P  ^   .P  r   �P  �    Q  7   �Q  <   �Q  =   �Q  9   8R  O   rR  H   �R  ^   S  V   jS     �S     �S  ^   �S  P   -T  K   ~T  n   �T  p   9U  Y   �U  V   V  M   [V  Z   �V  6   W  0   ;W  �   lW  f   dX  M   �X  P   Y  ]   jY  9   �Y  3   Z  ^   6Z  *   �Z  &   �Z  x   �Z  I   `[  q   �[  {   \  U   �\  d   �\  ]   S]  -   �]     �]  <   �]  *   -^  \   X^  n   �^    $_  e   >`  w   �`  <   a  !   Ya  ,   {a  H   �a  �   �a  u   �b  m   c  h   vc  t   �c  e   Td  5   �d  (   �d  7   e  A   Qe  9   �e  S   �e  T   !f  .   vf  o   �f  .   g  9   Dg  %   ~g  7   �g  >   �g  E   h  �   ah  �   -i  4   �i  6    j  �   7j  �   �j  J   �k  T   �k  0   %l  T   Vl     �l  3   �l  E   �l     .m  8   5m  d   nm  \   �m         �       G       q   N   t       �   �              g   �       �   �          @   ;   5   [   9   2   C       +         {           X       u   =   E      O   �   �   �   �   d   �   ?   �   D   �           &   �   4   '   L   <          P                         �   -      #       �   !   �   �          	   �   J   w   7   x           �   
       �      "   �   �       h      �   �      \       A   �   �   y   $   �   0   �      �           �   �   r   �   T   �   S   �   �   b              `   n   �              ,   a          �              U      }       l   �   c              *   i   f   o       %   R           �       _   �   e       V   |          >       (       �       �   �   Z   �       6   �   8               F   p   ^           �   1   3   �                   :      �   �           �   �   �   s   v   Y   H   �       j   �   �       �   I   �       z           B   .      Q   m   K      /   k   �   �   )   ]               M   �   �      �       �   �   ~         �             W   �   �    
<action> is one of:
 
<name> is the device to create under %s
<device> is the encrypted device
<key slot> is the LUKS key slot number to modify
<key file> optional key file for the new key for luksAddKey action
 
WARNING: real device header has different UUID than backup! %s: requires %s as arguments <device> <device> <key slot> <device> [--type <type>] [<name>] <device> [<key file>] <device> [<new key file>] <name> Activated keyslot %i.
 Activation is not supported for %d sector size.
 Align payload at <n> sector boundaries - for luksFormat All key slots full.
 Argument <action> missing. Argument required. BITS Backup LUKS device header and keyslots Cannot check password quality: %s
 Cannot create header backup file %s.
 Cannot find a free loopback device.
 Cannot format device %s which is still in use.
 Cannot format device %s, permission denied.
 Cannot get device size.
 Cannot get info about device %s.
 Cannot get process priority.
 Cannot initialize crypto backend.
 Cannot initialize device-mapper. Is dm_mod kernel module loaded?
 Cannot open device %s
 Cannot open device %s.
 Cannot open header backup file %s.
 Cannot read %d bytes from keyfile %s.
 Cannot read device %s.
 Cannot read header backup file %s.
 Cannot read keyfile %s.
 Cannot read requested amount of data.
 Cannot restore LUKS header on device %s.
 Cannot seek to requested keyfile offset.
 Cannot unlock memory.
 Cannot wipe device %s.
 Cannot wipe header on device %s.
 Cannot write device %s.
 Cannot write header backup file %s.
 Cannot write to device %s, permission denied.
 Command failed with code %i Command successful.
 Create a readonly mapping DM-UUID for device %s was truncated.
 Data offset or key size differs on device and backup, restore failed.
 Detected not yet supported GPG encrypted keyfile.
 Device %s %s%s Device %s already exists.
 Device %s doesn't exist or access denied.
 Device %s has zero size.
 Device %s is not active.
 Device %s is still in use.
 Device %s is too small.
 Device type is not properly initialised.
 Disable password quality check (if enabled). Display brief usage Do not activate device, just check passphrase. Do not ask for confirmation Do you really want to change UUID of device? Dump operation is not supported for this device type.
 Ensure you have algif_skcipher kernel module loaded.
 Enter any existing passphrase:  Enter any passphrase:  Enter new passphrase for key slot:  Enter new passphrase:  Enter passphrase for %s:  Enter passphrase for key slot %u:  Enter passphrase to be changed:  Enter passphrase to be deleted:  Enter passphrase:  Error during resuming device %s.
 Error during update of LUKS header on device %s.
 Error re-reading LUKS header after update on device %s.
 Error reading keyfile %s.
 Error reading passphrase from terminal.
 Error reading passphrase.
 FIPS checksum verification failed.
 Failed to access temporary keystore device.
 Failed to open key file.
 Failed to open temporary keystore device.
 Fatal error during RNG initialisation.
 File with LUKS header and keyslots backup. Function not available in FIPS mode.
 Generating key (%d%% done).
 Hash algorithm %s not supported.
 Header dump with volume key is sensitive information
which allows access to encrypted partition without passphrase.
This dump should be always stored encrypted on safe place. Help options: How many sectors of the encrypted data to skip at the beginning How often the input of the passphrase can be retried Interrupted by a signal.
 Invalid device %s.
 Invalid key size.
 Key %d not active. Can't wipe.
 Key processing error (using hash %s).
 Key size in XTS mode must be 256 or 512 bits.
 Key size must be a multiple of 8 bits Key slot %d active, purge first.
 Key slot %d changed.
 Key slot %d is full, please select another one.
 Key slot %d is invalid, please select between 0 and %d.
 Key slot %d is invalid, please select keyslot between 0 and %d.
 Key slot %d is invalid.
 Key slot %d is not used.
 Key slot %d selected for deletion.
 Key slot %d unlocked.
 LUKS header on device %s restored.
 Limits the read from keyfile Maximum TCRYPT passphrase length (%d) exceeded.
 Maximum keyfile size exceeded.
 MiB N/A Negative number for option not permitted. New LUKS header for device %s created.
 No key available with this passphrase.
 Non standard key size, manual repair required.
 Not compatible PBKDF2 options (using hash algorithm %s).
 Number of bytes to skip in keyfile Only one of --use-[u]random options is allowed. Option --align-payload is allowed only for luksFormat. Option --allow-discards is allowed only for open operation.
 Option --header-backup-file is required.
 Option --key-file is required.
 Option --key-size is allowed only for luksFormat, open and benchmark.
To limit read from keyfile use --keyfile-size=(bytes). Option --new must be used together with --reduce-device-size. Option --use-[u]random is allowed only for luksFormat. Option --uuid is allowed only for luksFormat and luksUUID. Out of memory while reading passphrase.
 PBKDF2 iteration time for LUKS (in ms) Passphrases do not match.
 Please use gpg --decrypt <KEYFILE> | cryptsetup --keyfile=- ...
 Print package version Read the key from a file. Really try to repair LUKS device header? Requested LUKS hash %s is not supported.
 Requested header backup file %s already exists.
 Requested offset is beyond real size of device %s.
 Required kernel crypto interface not available.
 Restore LUKS device header and keyslots Resume is not supported for device %s.
 Running in FIPS mode.
 SECTORS Show debug messages Show this help message Shows more detailed error messages Slot number for new key (default is first free) System is out of entropy while generating volume key.
Please move mouse or type some text in another window to gather some random events.
 The cipher used to encrypt the disk (see /proc/crypto) The hash used to create the encryption key from the passphrase The number of blocks in the data file The size of the device The size of the encryption key The start offset in the backend device This is the last keyslot. Device will become unusable after purging this key. This operation is not supported for %s crypt device.
 This operation is not supported for this device type.
 This operation is supported only for LUKS device.
 This will overwrite data on %s irrevocably. Timeout for interactive passphrase prompt (in seconds) UUID for device to use. Unknown action. Unsupported LUKS version %d.
 Unsupported VERITY block size.
 Unsupported VERITY version %d.
 Use /dev/random for generating volume key. Use /dev/urandom for generating volume key. VERITY header corrupted.
 Verifies the passphrase by asking for it twice Verify passphrase:  Volume %s is already suspended.
 Volume %s is not active.
 Volume %s is not suspended.
 Volume key buffer too small.
 Volume key does not match the volume.
 WARNING: Kernel cannot activate device if data block size exceeds page size (%u).
 WARNING: this is experimental code, it can completely break your data.
 Writing LUKS header to disk.
 add key to LUKS device already contains LUKS header. Replacing header will destroy existing keyslots. does not contain LUKS header. Replacing header can destroy data on that device. dump LUKS partition information dump TCRYPT device information formats a LUKS device memory allocation error in action_luksFormat msecs print UUID of LUKS device resize active device secs show device status tests <device> for LUKS partition header wipes key with number <key slot> from LUKS device Project-Id-Version: cryptsetup
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-08-10 16:19+0200
PO-Revision-Date: 2016-06-02 04:24+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:25+0000
X-Generator: Launchpad (build 18115)
 
<действие> может быть:
 
<имя> - имя устройства для создания под %s
<устройство> - зашифрованное устройство
<слот ключа> - номер слота ключа LUKS для изменения
<файл ключа> - опциональный файл ключа ключ для нового ключа для действия luksAddKey
 
ВНИМАНИЕ: заголовок реального устройства и резервная копия содержат разные UUID! %s требует %s в качестве аргумента <устройство> <устройство> <слот ключа> <устройство> [--type <тип>] [<имя>] <устройство> [<файл-ключ>] <устройство> [<новый файл ключа>] <имя> Активирован слот ключей %i.
 Активация не поддерживается для размера сектора %d.
 Выравнивать нагрузку по границам <n> секторов — для luksFormat Все ключевые слоты полны.
 Аргумент <действие> отсутствует. Требуется аргумент. БИТ Сделать резервную копию заголовка и ключевых слотов устройства LUKS Не удалось проверить качество пароля: %s
 Не удалось создать файл резервной копии заголовка %s.
 Не удалось найти свободное петлевое устройство.
 Нельзя отформатировать устройство %s, которое ещё используется.
 Невозможно отформатировать устройство %s, отказано в доступе.
 Не удалось получить размер устройства.
 Невозможно получить информацию об устройстве %s.
 Невозможно получить приоритет процесса.
 Невозможно инициализировать внутренний интерфейс crypto.
 Невозможно инициализировать device-mapper. Загружен ли модуль ядра dm_mod?
 Не удалось открыть устройство %s
 Невозможно открыть устройство %s.
 Невозможно открыть файл резервной копии заголовка %s.
 Не удалось прочитать %d байт из ключевого файла %s.
 Невозможно прочитать устройство %s.
 Невозможно прочитать файл резервной копии заголовка %s.
 Не удалось прочитать ключевой файл %s.
 Не удалось считать запрошенное количество данных.
 Не удалось восстановить заголовок LUKS на устройстве %s.
 Не удалось перейти к запрошенному смещению в ключевом файле.
 Не удалось разблокировать память.
 Невозможно очистить устройство %s.
 Не удалось стереть заголовок на устройстве %s.
 Не удалось выполнить запись на устройство %s.
 Невозможно записать файл резервной копии заголовка %s.
 Запись на устройство %s невозможна, отказано в доступе.
 Выполнение команды завершилось с кодом ошибки %i Команда выполнена успешно.
 Создать назначение в режиме "Только для чтения" DM-UUID для устройства %s был усечён.
 Смещение данных или размер ключа не совпадают на устройстве и в резервной копии, восстановление не удалось.
 Обнаружен ещё не поддерживаемый зашифрованный GPG ключевой файл.
 Устройство %s %s%s Устройство %s уже существует.
 Устройство %s не существует или доступ к нему запрещён.
 Устройство %s имеет нулевой размер.
 Устройство %s не активно.
 Устройство %s всё ещё используется.
 Устройство %s слишком маленькое.
 Тип устройства некорректно инициализирован.
 Отключить проверку качества пароля (если включена). Показать краткие инструкции Не активировать устройство, просто проверить парольную фразу. Не запрашивать подтверждения Вы действительно хотите изменить UUID устройства? Создание дампа не поддерживается для этого типа устройств.
 Убедитесь, что загружен модуль ядра algif_skcipher.
 Введите любую существующую парольную фразу:  Введите любой пароль:  Введите новый пароль для ключевого слота:  Введите новую парольную фразу:  Введите пароль для %s:  Введите парольную фразу для слота ключа %u:  Введите изменяемую парольную фразу:  Введите удаляемую парольную фразу:  Введите пароль:  Ошибка при возобновлении работы устройства %s.
 Ошибка обновления заголовка LUKS на устройстве %s.
 Ошибка перечитывания заголовка LUKS после обновления на устройстве %s.
 Ошибка при чтении файла ключа %s.
 Ошибка чтения кодовой фразы из терминала.
 Ошибка чтения кодовой фразы.
 Сбой проверки контрольной суммы FIPS.
 Не удалось получить доступ к устройству временного хранения ключей.
 Не удалось открыть ключевой файл.
 Не удалось открыть устройство временного хранения ключей.
 Критическая ошибка во время инициализации RNG.
 Файл с резервной копией заголовка и ключевых слотов LUKS. Функция недоступна в режиме FIPS.
 Генерация ключа (%d%% завершено).
 Алгоритм хэширования %s не поддерживается.
 Дамп заголовка с ключом тома является секретной информацией,
обеспечивающей доступ к зашифрованному разделу без парольной фразы.
Этот дамп следует всегда хранить зашифрованным в надёжном месте. Опции справки: Сколько секторов зашифрованных данных пропустить сначала Как часто можно повторять попытку ввода ключевой фразы Прервано по сигналу.
 Неверное устройство %s.
 Неверный размер ключа.
 Ключ %d не активен. Невозможно очистить.
 Ошибка обработки ключа (используется хэш %s).
 Размер ключа в режиме XTS должен быть 256 или 512-разрядный.
 Размер ключа должен быть кратным 8-ми битному Ключевой слот %d активен, сначала очистите.
 Слот ключа %d изменён.
 Ключевой слот %d полон, пожалуйста, выберите другой.
 Неправильный ключевой слот %d, пожалуйста, выберите между 0 и %d.
 Неверный ключевой слот %d, пожалуйста, выберите ключевой слот между 0 и %d.
 Неправильный ключевой слот %d.
 Ключевой слот %d не используется.
 Слот ключа %d выбран для удаления.
 Ключевой слот %d разблокирован.
 Заголовок LUKS на устройстве %s восстановлен.
 Ограничивает чтение из ключевого файла Максимальная длина кодовой фразы TCRYPT (%d) превышена.
 Максимальный размер ключевого файла превышен.
 МиБ Н/Д Нельзя использовать для опции отрицательные числа. Создан новый заголовок LUKS для устройства %s.
 Нет доступных ключей для данного пароля.
 Нестандартный размер ключа, требуется исправление вручную.
 Несовместимые параметры PBKDF2 (использование хеш-алгоритма %s).
 Количество пропускаемых байтов в ключевом файле Можно использовать лишь одну из опций --use-[u]random. Опция --align-payload разрешена только для luksFormat. Опция --allow-discards разрешена только для действия open.
 Необходима опция --header-backup-file.
 Требуется параметр --key-file.
 Опция --key-size разрешена только для luksFormat, open и benchmark.
Чтобы ограничить чтение из ключевого файла, используйте --keyfile-size=(количество байтов). Опция --new должна использоваться совместно с --reduce-device-size. Опция --use-[u]random разрешена только для luksFormat. Опция --uuid разрешена только для luksFormat и luksUUID. Недостаточно памяти для считывания кодовой фразы.
 Время итерации PBKDF2 для LUKS (мсек) Кодовые фразы не совпадают.
 Используйте gpg --decrypt <КЛЮЧЕВОЙ_ФАЙЛ> | cryptsetup --keyfile=- ...
 Показать версию пакета Читать ключ из файла. Действительно попробовать восстановить заголовок устройства LUKS? Запрошенный хэш LUKS %s не поддерживается.
 Запрошенный файл резервной копии заголовка %s уже существует.
 Запрошенное смещение за пределами реального размера устройства %s.
 Запрошенный криптоинтерфейс ядра недоступен.
 Восстановить заголовок и ключевые слоты устройства LUKS Возобновление не поддерживается для устройства %s.
 Выполняется в режиме FIPS.
 СЕКТОРОВ Показывать отладочные сообщения Показать это сообщение Показывает детализированные сообщения об ошибках Номер слота для нового ключа (по-умолчанию первый свободный) В системе закончились данные энтропии при генерации ключа тома.
Подвигайте мышь или наберите любой текст в другом окне, чтобы собрать случайные события.
 Шифр, используемый для шифрования диска (смотри /proc/crypto) Хеш, исползуемый для создания ключа шифрования из ключевой фразы Количество блоков в файле данных Размер устройства Размер ключа шифрования Начальное смещение в бэкенд-устройстве Это последний слот ключа. Устройство станет нечитаемым после освобождения этого ключа. Данная операция не поддерживается для устройства шифрования %s.
 Это действие не поддерживается для данного типа устройств.
 Данная операция поддерживается только для устройств LUKS.
 Данные на %s будут перезаписаны без возможности восстановления. Таймаут интерактивного запроса ключевой фразы (секунд) UUID используемого устройства. Неизвестное действие. Неподдерживаемая версия LUKS %d.
 Неподдерживаемый размер блока VERITY.
 Неподдерживаемая версия VERITY %d.
 Использовать /dev/random для генерации ключа тома. Использовать /dev/urandom для генерации ключа тома. Заголовок VERITY повреждён.
 Проверяет правильность ключевой фразы, запрашивая ее дважды Проверить кодовую фразу:  Том %s уже в режим приостановки.
 Раздел %s не активен.
 Том %s не в режим приостановки.
 Буфер ключей раздела слишком мал.
 Ключ раздела не совпадает с разделом.
 ПРЕДУПРЕЖДЕНИЕ: Ядро не может активировать устройство, если размер блока данных превышает размер страницы (%u).
 Предупреждение: это экспериментальный код, он может полностью повредить ваши данные.
 Запись заголовка LUKS на диск.
 добавить ключ к устройству LUKS уже содержит заголовок LUKS. Перемещение заголовка уничтожит существующие ключевые слоты. не содержит заголовка LUKS. Перемещение заголовка может уничтожить данные на этом устройстве. выгрузить в дамп информацию о разделе LUKS выгрузить в дамп информацию об устройстве TCRYPT форматирует устройство LUKS Ошибка 'memory allocation error' при выполнении action_luksFormat мсек напечатать UUID устройства LUKS изменить размер активного устройства сек показать состояние устройства проверить <устройство> на наличие заголовка раздела LUKS стирает ключ с номером <слот ключа> с устройства LUKS 