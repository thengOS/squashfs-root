��    7      �  I   �      �  K   �  �   �  8  �  0   �  9   �  6   2     i     p     �     �     �  9   �     	     	     %	  -   6	  $   d	  5   �	  '   �	  $   �	  $   
     1
     O
     h
  J   
     �
     �
  H   �
     7     R     k     �     �     �     �  1   �       (        ;     U     b          �     �  0   �     �  (        ?  �   W  "   �          '  7   A  
   y  �  �  K       j  �  ~  b   <  f   �  2        9  6   H  7        �     �     �  3   _     �  #   �  [   �  ]   -  v   �  M     M   P  >   �     �  *   �  (   &  �   O  8   �       L   (  &   u  #   �  c   �  C   $     h  U   |  4   �  G        O  [   l  "   �     �  8   �  B   6  ?   y  N   �  x     (   �  Z   �  6      m  <   U   �!  '    "  7   ("  S   `"     �"               	   3       6                .               
   )   &                 !      %   0      $                 5   +                -   (       /   2             *       7                 ,      "       1   4   #             '                             
Copyright (C) 1990, 92, 93, 94, 96, 97, 99 Free Software Foundation, Inc.
 
If a long option shows an argument as mandatory, then it is mandatory
for the equivalent short option also.  Similarly for optional arguments.
 
Listings:
  -l, --list[=FORMAT]        list one or all known charsets and aliases
  -k, --known=PAIRS          restrict charsets according to known PAIRS list
  -h, --header[=[LN/]NAME]   write table NAME on stdout using LN, then exit
  -F, --freeze-tables        write out a C module holding all tables
  -T, --find-subsets         report all charsets being subset of others
  -C, --copyright            display Copyright and copying conditions
      --help                 display this help and exit
      --version              output version information and exit
 
Report bugs to <recode-bugs@iro.umontreal.ca>.
 
Usage: %s [OPTION]... [ [CHARSET] | REQUEST [FILE]... ]
   -p, --sequence=pipe     same as -i (on this system)
  done
  failed: %s in step `%s..%s'
 %s failed: %s in step `%s..%s' %s in step `%s..%s' %s to %s %sConversion table generated mechanically by Free `%s' %s %sfor sequence %s.%s *mere copy* Ambiguous output Cannot complete table from set of known pairs Cannot invert given one-to-one table Cannot list `%s', no names available for this charset Charset %s already exists and is not %s Charset `%s' is unknown or ambiguous Codes %3d and %3d both recode to %3d Dec  Oct Hex   UCS2  Mne  %s
 Format `%s' is ambiguous Format `%s' is unknown Free `recode' converts files between various character sets and surfaces.
 Internal recoding bug Invalid input LN is some language, it may be `c', `perl' or `po'; `c' is the default.
 Language `%s' is ambiguous Language `%s' is unknown Misuse of recoding library No character recodes to %3d No error No table to print Non canonical input Pair no. %d: <%3d, %3d> conflicts with <%3d, %3d> Recoding %s... Recoding is too complex for a mere table Request `%s' is erroneous Request: %s
 Required argument is missing Sequence `%s' is ambiguous Sequence `%s' is unknown Step initialisation failed Step initialisation failed (unprocessed options) Symbol `%s' is unknown Syntax is deprecated, please prefer `%s' System detected problem This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 Try `%s %s' for more information.
 Untranslatable input Virtual memory exhausted! Written by Franc,ois Pinard <pinard@iro.umontreal.ca>.
 reversible Project-Id-Version: recode
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2001-01-02 22:35+0100
PO-Revision-Date: 2010-05-05 09:09+0000
Last-Translator: HeartsinAtlantis <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:04+0000
X-Generator: Launchpad (build 18115)
 
Copyright (C) 1990, 92, 93, 94, 96, 97, 99 Free Software Foundation, Inc.
 
Если длинная опция имеет обязательный аргумент, тогда он обязателен и для
эквивалентной короткой опции. То же самое и для необязательных аргументов.
 
Вывод:
  -l, --list[=ФОРМАТ]        вывести список всех или некоторых известных наборов символов и их псевдонимов
  -k, --known=ПАРЫ           ограничить набор символов, основываясь на списке ПАР
  -h, --header[=[ЯЗ/]ИМЯ]    вывести таблицу ИМЯ на stdout в формате ЯЗ, затем выйти
  -F, --freeze-tables        вывести модуль C, содержащий все таблицы
  -T, --find-subsets         вывести все наборы символов, являющиеся подмножеством других
  -C, --copyright            вывести сведения об авторских правах
      --help                 вывести этот текст и выйти
      --version              вывести номер версии и выйти
 
Присылайте сообщения об ошибках на <recode-bugs@iro.umontreal.ca>.
 
Использование: %s [ОПЦИИ]... [ [КОДИРОВКА] | ЗАПРОС [ФАЙЛ]... ]
   -p, --sequence=pipe same as -i (on this system)
  готово
  не получилось: %s на шаге `%s..%s'
 %s не получилось: %s на шаге `%s..%s' %s на шаге `%s..%s' из %s в %s %sТаблица преобразования сгенерирована механически с помощью  Free `%s' %s %sдля последовательности %s.%s *просто копия* Неоднозначный ввод Не могу заполнить таблицу из набора известных пар Не могу инвертировать данную таблицу один к одному Не могу вывести список `%s', нет ни одного имени для этой кодировки Кодировка %s уже существует и не является %s Кодировка `%s' не известна или двусмысленна Оба кода, %3d и %3d, преобразуются в %3d Dec  Oct Hex   UCS2  Mne  %s
 Формат `%s' двусмысленен Неизвестный формат `%s' Свободная программа `recode' преобразует файлы из одной кодировки в другую.
 Внутренняя ошибка кодировщика Неверный ввод ЯЗ это язык: `c', `perl' или `po'; по умолчанию `c'.
 Язык `%s' двусмысленен Язык `%s' не известен Неправильное использование библиотеки перекодировки Ни один символ не перекодируется в %3d Нет ошибки Нет таблицы, которую можно было бы распечатать Ввод не в канонической форме Пара номер %d: <%3d, %3d> конфликтует с <%3d, %3d> Перекодирую %s... Перекодировка слишком сложна для простой таблицы Запрос `%s' ошибочен Запрос: %s
 Пропущен необходимый аргумент Последовательность `%s' двусмысленна Последовательность `%s' не известна Не получилось выполнить шаг инициализации Не получилось выполнить шаг инициализации (необработанные опции) Неизвестный символ `%s' Устаревший синтаксис. Пожалуйста, используйте `%s' Системой обнаружена проблема Это свободная программа; подробности об условиях распространения смотрите
в исходном тексте.  Мы НЕ предоставляем гарантий; даже гарантий
КОММЕРЧЕСКОЙ ЦЕННОСТИ или ПРИГОДНОСТИ ДЛЯ КОНКРЕТНОЙ ЦЕЛИ.
 Для дополнительных сведений используйте `%s %s'.
 Непреобразуемый ввод Кончилась виртуальная память! Автор Франсуа Пинар (Franc,ois Pinard) <pinard@iro.umontreal.ca>.
 обратимая 