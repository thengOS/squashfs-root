��    O      �  k         �  $   �  &   �  )         /  #   P  -   t  *   �  +   �  =   �  )   7  ;   a  <   �  @   �      	  :   <	     w	     �	      �	     �	  $   �	     

     '
  (   F
     o
     �
     �
     �
     �
     �
  '      &   (  "   O  $   r     �  $   �     �      �  #     )   ;  ,   e  0   �  "   �     �  /         0     P     n      �  !   �      �  #   �          (  ?   @  4   �  -   �  4   �  4     $   M  &   r  ,   �     �  "   �       ;     *   V     �     �  %   �     �  &   �  �      �  �   �  5   r     �     �     �  z  �  >   b  L   �  H   �  4   7  @   l  L   �  8   �  M   3  \   �  K   �  \   *  \   �  �   �  G   q  O   �  .   	  :   8  @   s  3   �  ^   �  ,   G  E   t  c   �  )     &   H  6   o  7   �      �  '   �  O   '   O   w   :   �   9   !  <   <!  .   y!      �!  8   �!  D   "  J   G"  M   �"  Q   �"  C   2#  F   v#  g   �#  H   %$  >   n$  &   �$  >   �$  c   %  2   w%  >   �%  =   �%  3   '&  �   [&  h   '  K   x'  l   �'  f   1(  L   �(  B   �(  Z   ()  K   �)  6   �)  +   *  k   2*  ?   �*  6   �*  &   +  C   <+  O   �+  H   �+  *  ,    D2  c  b3  8   �4  )   �4  R   )5     |5     
                 1      M                                        !       H   5       0       %          /   ?             '   	   $   K   *          N   F   4                 I          +      7   2          >   9   D   E   J   L   O   B   "                          A       ,   @   )      :           #              &   <   .       C   ;       (       8   =   6   -       3                 G    %s: %s doesn't exist, skipping call
 %s: %s is encrypted on real device %s
 %s: CD-ROM auto-eject command failed: %s
 %s: CD-ROM eject command failed
 %s: CD-ROM eject command succeeded
 %s: CD-ROM load from slot command failed: %s
 %s: CD-ROM select disc command failed: %s
 %s: CD-ROM select speed command failed: %s
 %s: CD-ROM select speed command not supported by this kernel
 %s: CD-ROM tray close command failed: %s
 %s: CD-ROM tray close command not supported by this kernel
 %s: CD-ROM tray toggle command not supported by this kernel
 %s: Could not restore original CD-ROM speed (was %d, is now %d)
 %s: FindDevice called too often
 %s: IDE/ATAPI CD-ROM changer not supported by this kernel
 %s: SCSI eject failed
 %s: SCSI eject succeeded
 %s: `%s' can be mounted at `%s'
 %s: `%s' is a link to `%s'
 %s: `%s' is a multipartition device
 %s: `%s' is mounted at `%s'
 %s: `%s' is not a mount point
 %s: `%s' is not a multipartition device
 %s: `%s' is not mounted
 %s: closing tray
 %s: could not allocate memory
 %s: default device: `%s'
 %s: device is `%s'
 %s: device name is `%s'
 %s: disabling auto-eject mode for `%s'
 %s: enabling auto-eject mode for `%s'
 %s: error while allocating string
 %s: error while finding CD-ROM name
 %s: error while reading speed
 %s: exiting due to -n/--noop option
 %s: expanded name is `%s'
 %s: floppy eject command failed
 %s: floppy eject command succeeded
 %s: invalid argument to --auto/-a option
 %s: invalid argument to --cdspeed/-x option
 %s: invalid argument to --changerslot/-c option
 %s: invalid argument to -i option
 %s: listing CD-ROM speed
 %s: maximum symbolic link depth exceeded: `%s'
 %s: restored original speed %d
 %s: saving original speed %d
 %s: selecting CD-ROM disc #%d
 %s: setting CD-ROM speed to %dX
 %s: setting CD-ROM speed to auto
 %s: tape offline command failed
 %s: tape offline command succeeded
 %s: toggling tray
 %s: too many arguments
 %s: tried to use `%s' as device name but it is no block device
 %s: trying to eject `%s' using CD-ROM eject command
 %s: trying to eject `%s' using SCSI commands
 %s: trying to eject `%s' using floppy eject command
 %s: trying to eject `%s' using tape offline command
 %s: unable to eject, last error: %s
 %s: unable to exec umount of `%s': %s
 %s: unable to find or open device for: `%s'
 %s: unable to fork: %s
 %s: unable to open /etc/fstab: %s
 %s: unable to open `%s'
 %s: unable to read the speed from /proc/sys/dev/cdrom/info
 %s: unmount of `%s' did not exit normally
 %s: unmount of `%s' failed
 %s: unmounting `%s'
 %s: unmounting device `%s' from `%s'
 %s: using default device `%s'
 %s: using device name `%s' for ioctls
 Eject version %s by Jeff Tranter (tranter@pobox.com)
Usage:
  eject -h				-- display command usage and exit
  eject -V				-- display program version and exit
  eject [-vnrsfqpm] [<name>]		-- eject device
  eject [-vn] -d			-- display default device
  eject [-vn] -a on|off|1|0 [<name>]	-- turn auto-eject feature on or off
  eject [-vn] -c <slot> [<name>]	-- switch discs on a CD-ROM changer
  eject [-vn] -t [<name>]		-- close tray
  eject [-vn] -T [<name>]		-- toggle tray
  eject [-vn] -i on|off|1|0 [<name>]	-- toggle manual eject protection on/off
  eject [-vn] -x <speed> [<name>]	-- set CD-ROM max speed
  eject [-vn] -X [<name>]		-- list CD-ROM available speeds
Options:
  -v	-- enable verbose output
  -n	-- don't eject, just show device found
  -r	-- eject CD-ROM
  -s	-- eject SCSI device
  -f	-- eject floppy
  -q	-- eject tape
  -p	-- use /proc/mounts instead of /etc/mtab
  -m	-- do not unmount device even if it is mounted
 Long options:
  -h --help   -v --verbose      -d --default
  -a --auto   -c --changerslot  -t --trayclose  -x --cdspeed
  -r --cdrom  -s --scsi         -f --floppy     -X --listspeed     -q --tape
  -n --noop   -V --version
  -p --proc   -m --no-unmount   -T --traytoggle
 Parameter <name> can be a device file or a mount point.
If omitted, name defaults to `%s'.
By default tries -r, -s, -f, and -q in order until success.
 eject version %s by Jeff Tranter (tranter@pobox.com)
 unable to open %s: %s
 usage: volname [<device-file>]
 volname Project-Id-Version: eject
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-02-20 00:35+0100
PO-Revision-Date: 2014-01-02 12:26+0000
Last-Translator: Sly_tom_cat <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:13+0000
X-Generator: Launchpad (build 18115)
 %s: %s не существует, пропуск вызова
 %s: %s зашифровано на реальном устройстве %s
 %s: сбой команды автоизвлечения лотка: %s
 %s: сбой при извлечении лотка
 %s: извлечение лотка прошло успешно
 %s: сбой команды загрузки диска из слота: %s
 %s: сбой команды выбора диска: %s
 %s: сбой команды выбора скорости привода: %s
 %s: выбор скорости привода не поддерживается ядром
 %s: сбой команды закрытия лотка привода: %s
 %s: закрытие лотка привода не поддерживается ядром
 %s: закрытие лотка привода не поддерживается ядром
 %s: Невозможно восстановить исходную скорость CD-ROM (первоначально %d, сейчас %d)
 %s: слишком много вызовов функции FindDevice
 %s: IDE/ATAPI-чейнджер CD не поддерживается ядром
 %s: сбой при извлечении SCSI
 %s: извлечение SCSI прошло успешно
 %s: `%s' может быть примонтирован в `%s'
 %s: `%s' является ссылкой на `%s'
 %s: `%s' является устройством с несколькими разделами
 %s: `%s' примонтирован в `%s'
 %s: `%s' не является точкой монтирования
 %s: `%s' не является устройством с несколькими разделами
 %s: `%s' не примонтирован
 %s: лоток закрывается
 %s: не удаётся выделить память
 %s: устройство по умолчанию: `%s'
 %s: устройство - `%s'
 %s: имя устройства - `%s'
 %s: отключается режим автоизвлечения для `%s'
 %s: включается режима автоизвлечения для `%s'
 %s: ошибка при назначении строки
 %s: не удалось найти имя привода
 %s: сбой чтения значения скорости
 %s: выход из-за опции -n/--noop
 %s: полное имя -  `%s'
 %s: сбой при извлечении дискеты
 %s: извлечение дискеты прошло успешно
 %s: недопустимый аргумент для опции --auto/-a
 %s: недопустимый аргумент для опции --cdspeed/-x
 %s: недопустимый аргумент для опции --changerslot/-c
 %s: неправильный аргумент для опции -i
 %s: выводится список скоростей привода
 %s: превышена максимальная глубина символьных ссылок: `%s'
 %s: восстановленная исходная скорость %d
 %s: сохранение исходной скорости %d
 %s: выбирается диск #%d
 %s: установка скорости привода в %dX
 %s: установка автоматического выбора скорости привода
 %s: сбой при остановке ленты
 %s: остановка ленты прошла успешно
 %s: переключается состояние лотка
 %s: слишком много аргументов
 %s: попытка использования `%s' в качестве имени устройства, которое не является блочным устройством
 %s: попытка извлечь `%s' с помощью команды извлечения лотка
 %s: попытка извлечь `%s' с помощью команд SCSI
 %s: попытка извлечь `%s' с помощью команды извлечения дискеты
 %s: попытка извлечь `%s' с помощью команды остановки ленты
 %s: не удаётся извлечь, последняя ошибка: %s
 %s: не удаётся выполнить umount для `%s': %s
 %s: не удаётся найти или открыть устройство для `%s'
 %s: не удаётся создать дочерний процесс: %s
 %s: не удаётся открыть /etc/fstab: %s
 %s: не удаётся открыть`%s'
 %s: не удаётся прочитать значение скорости из /proc/sys/dev/cdrom/info
 %s: некорректное отмонтирование `%s'
 %s: сбой при отмонтировании `%s'
 %s: отмонтирование `%s'
 %s: устройство `%s' отмонтируется от `%s'
 %s: используется устройство по умолчанию `%s'
 %s: использует имя устройства `%s' для ioctls
 Eject version %s автор Jeff Tranter (tranter@pobox.com)
Использование:
  eject -h				-- показать помощь и выйти
  eject -V				-- показать версию программы и выйти
  eject [-vnrsfqpm] [<имя>]		-- извлечь устройство
  eject [-vn] -d			-- показать устройство по умолчанию
  eject [-vn] -a on|off|1|0 [<имя>]	-- включить\выключить авто-извлечение
  eject [-vn] -c <слот> [<имя>]	-- сменить диски на CD-чейнджере
  eject [-vn] -t [<имя>]		-- закрыть лоток
  eject [-vn] -T [<имя>]		-- переключить лоток
  eject [-vn] -i on|off|1|0 [<имя>]	-- включить\выключить защиту от ручного извлечения
  eject [-vn] -x <скорость> [<имя>]	-- установить максимальную скорость CD-ROM
  eject [-vn] -X [<имя>]		-- список доступных скоростей CD-ROM
Параметры:
  -v	-- включить расширенный вывод
  -n	-- не извлекать, только показать найденное устройство
  -r	-- извлечь CD-ROM
  -s	-- извлечь устройство SCSI
  -f	-- извлечь дискету
  -q	-- извлечь кассету
  -p	-- использовать /proc/mounts вместо /etc/mtab
  -m	-- не размонтировать устройство, даже если оно примонтировано
 Длинные опции:
  -h --help   -v --verbose      -d --default
  -a --auto   -c --changerslot  -t --trayclose  -x --cdspeed
  -r --cdrom  -s --scsi         -f --floppy     -X --listspeed     -q --tape
  -n --noop   -V --version
  -p --proc   -m --no-unmount   -T --traytoggle
 Параметр <имя> может представлять файл устройства или точку монтирования.
Если он не указан, будет использоваться `%s'.
По умолчанию используются опции -r, -s, -f и -q (до первого успешного выполнения).
 eject версии %s, Jeff Tranter (tranter@pobox.com)
 не удаётся открыть%s: %s
 Использование: метка_тома [<файл_устройства>]
 метка_тома 