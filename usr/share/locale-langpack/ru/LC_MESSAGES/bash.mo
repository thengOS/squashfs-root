��    �     �    �      `!  *   a!     �!  $   �!  
   �!     �!     �!     �!     "     "     9"     P"  	   f"     p"     �"     �"     �"     �"     �"     �"      #     )#     0#     D#     \#  /   x#  ;   �#  $   �#  :   	$     D$     [$  (   r$  "   �$     �$     �$     �$  3   %      B%  &   c%  &   �%  /   �%  /   �%     &     '&  .   =&     l&     �&     �&     �&     �&     �&  "   	'     ,'     @'     Q'     o'  /   �'     �'     �'     �'  -   �'     "(     8(     U(     f(     �(     �(     �(     �(     �(  !   �(     )  )   6)     `)     {)     �)     �)     �)     �)      �)  !   *     2*      E*     f*     }*     �*     �*     �*     �*     �*     �*     +     ,+     F+     _+     z+     �+     �+  &   �+     �+     ,     !,     7,  &   F,  3   m,     �,     �,     �,     �,     �,     -     -  9   )-  #   c-     �-     �-     �-     �-  H  �-     1     1     "1     21     ?1  	   K1  	   U1     _1     s1  1   |1  1   �1  ?  �1      5     %5     .5     >5  
   F5     Q5     j5     �5     �5     �5     �5     �5     �5  	   �5     �5     6  	    6     *6  N   16     �6     �6     �6  �  �6  S   �9     �9     �9  D   :     M:  	   ]:     g:     o:     �:     �:     �:  *   �:  
   �:     �:     	;     ";  O  ;;  B   �<  B   �<  E   =     W=     h=     {=     �=     �=  X   �=     >  *   #>     N>     \>     j>     y>     �>     �>     �>     �>  %   �>  $   ?  '   2?     Z?     n?     �?     �?  !   �?     �?     �?     @     (@  '   ?@  0   g@  .   �@     �@  9   �@      A     )A     ;A  $   [A     �A     �A     �A     �A  &   �A  '   �A  9   B     EB  3   ^B     �B  =   �B  -   �B     C  '   =C  &   eC  *   �C  *   �C  )   �C  )   D  %   6D  %   \D      �D  1   �D  #   �D  1   �D  &   +E  5   RE     �E     �E  !   �E  !   �E     �E  1   F  #   EF  $   iF     �F  #   �F  '   �F     �F  .   �F     G     =G     SG     iG     wG     �G  ,   �G  %   �G  ,   H  %   /H     UH     bH     wH  #   �H     �H     �H     �H  '   �H  .   I  ,   DI  &   qI  0   �I  (   �I     �I  ?   J     CJ     TJ     jJ  8   xJ  V   �J  &   K  '   /K     WK     wK     �K  (   �K     �K     �K     �K     �K  "   L     5L     GL  	   ML     WL     pL  
   xL  +   �L  9   �L  ;   �L  $   %M     JM     hM     �M     �M     �M     �M      �M     �M     N     (N  H   9N     �N     �N     �N     �N  "   �N  +   �N     O     9O  4   FO  
   {O  D   �O  ?   �O  ,   P     8P  !   OP  "   qP     �P  	   �P     �P  /   �P  )   �P  3   Q     QQ  &   kQ  2   �Q  5   �Q  ,   �Q  
   (R  1   3R  I   eR  4   �R  (   �R  ,   S  ,   :S  0   gS  )   �S     �S  "   �S     �S     T  &   T  =   ?T     }T     �T  '   �T     �T     �T  ,   U  )   8U     bU  $   �U     �U     �U     �U     �U     �U     �U  %   V  .   9V  -   hV  7   �V  6   �V  2   W  1   8W  *   jW  ,   �W  ,   �W  ;   �W  #   +X     OX     WX     mX  *   �X  "   �X     �X  6   �X  	   +Y     5Y  -   EY  -   sY     �Y  '   �Y  '   �Y     Z     Z  O    \     p\  $   �\  
   �\  %   �\     �\  A   �\  )   2]  @   \]  6   �]  &   �]     �]  %   ^  (   9^  C   b^  /   �^     �^  A   �^  K   6_  /   �_     �_  O   �_  /   	`  7   9`  ^   q`  �   �`  O   ]a  �   �a  B   /b  =   rb  r   �b  A   #c  3   ec  W   �c  H   �c  z   :d  C   �d  U   �d  G   Oe  |   �e  |   f  $   �f  %   �f  ^   �f  D   ;g  (   �g  1   �g  1   �g  @   h  $   Nh  O   sh  (   �h  "   �h  X   i  &   hi  P   �i  $   �i  *   j     0j  ?   Oj  %   �j  ;   �j  !   �j  .   k     Bk  :   Uk  ,   �k  -   �k  0   �k  I   l  D   fl  W   �l  2   m  4   6m  '   km  .   �m  )   �m  ?   �m  D   ,n  <   qn  +   �n  7   �n  '   o     :o  >   Po  .   �o  "   �o  "   �o     p  5   p  -   Rp  *   �p  4   �p  2   �p     q  8   )q  0   bq  9   �q  :   �q  D   r  J   Mr     �r  O   �r  \   s  -   ^s  U   �s  '   �s      
t     +t  +   Dt     pt  e   �t  Y   �t     Gu  *   Iu     tu      �u  �  �u     T{  '   p{  .   �{     �{     �{     |     |  3   -|     a|  1   v|  1   �|  1  �|     �     �     0�     I�     \�  K   t�     ��     ��  -    �  ,   .�  5   [�     ��     ��     Ń  -   �  !   �     4�  +   I�  t   u�  7   �  
   "�  #   -�  �  Q�  �   ډ     d�  :   {�    ��     ֋     �     �  %   �  7   A�  1   y�     ��  >   ƌ     �  -   �  4   D�  8   y�  c  ��  �   �  w   ��  �   ,�  %   ��  '   ב  #   ��  #   #�  ;   G�  �   ��  d   ;�  _   ��  /    �  /   0�     `�  #   z�  .   ��     ͔  !   �  G   �  G   P�  K   ��  =   �  1   "�  2   T�  0   ��  @   ��  g   ��  9   a�  2   ��     Η  %   �  >   �  H   J�  K   ��  )   ߘ  9   	�     C�  V   T�  K   ��  =   ��  4   5�  "   j�  #   ��     ��  X   ͚  M   &�  q   t�     �  k   ��  K   k�  q   ��  ]   )�  3   ��  T   ��  A   �  P   R�  R   ��  L   ��  N   C�  M   ��  M   ��  ;   .�  [   j�  U   Ơ  �   �  g   ��  q   �  )   w�  D   ��  R   �  R   9�  %   ��  1   ��  =   �  E   "�     h�  E   u�  ;   ��     ��  �   �  *   ��  &   ��  2   ݥ     �  ?   &�  "   f�  A   ��  J   ˦  Z   �  P   q�     §  #   ק  %   ��  A   !�     c�  g   l�  5   Ԩ  P   
�  ]   [�  Y   ��  N   �  V   b�  X   ��  )   �  o   <�  $   ��  %   ѫ     ��  y   �  t   ��  (   �  7   .�     f�  @   ��     ǭ  (   �  >   
�  #   I�  4   m�  2   ��  ;   ծ  &   �     8�     E�  <   W�     ��  !   ��  D   ¯  9   �  ;   A�  Z   }�  9   ذ  6   �     I�     ^�  J   s�  $   ��  A   �     %�  *   @�  )   k�  �   ��  "   (�  G   K�  "   ��  (   ��  e   ߳  f   E�  �   ��  J   /�  Q   z�     ̵  D   �  ?   1�  ,   q�  0   ��  !   ϶  "   �  #   �  	   8�  !   B�  \   d�  )   ��  Y   �  4   E�  T   z�  2   ϸ  5   �  ,   8�     e�  1   z�  I   ��  k   ��  F   b�  J   ��  J   ��  N   ?�  Y   ��  #   �  2   �  (   ?�  !   h�  H   ��  t   Ӽ  ?   H�  1   ��  W   ��  +   �  >   >�  e   }�  i   �  D   M�  O   ��  $   �  
   �  .   �  .   A�     p�     }�  3   ��  T   ��  Q   &�  f   x�  c   ��  `   C�  ^   ��  K   �  M   O�  M   ��  q   ��  B   ]�     ��  2   ��  F   ��  a   1�  9   ��  4   ��  z   �     }�  $   ��  p   ��  p   &�     ��  '   ��  H   ��  <   &�     �   �   �        �   +  $  5  �             1      �   t      �       h          [   �       c   )   �       e  �      @   n   �   i          %   H      G  )      q  �   :   *  V   �  (   C   �   �   �   g  s           �   _  Y          j       �   N  �   |     �       7   �   �   1   �       P     l   k        �           L       =      O                   #  �       x  "  9  	       �   �   @  .   �       �   �  �   2           �          �   ?   J   :  Q  W  
      M      �      ^      5       i  H   _   U  �   a  7  D      �       �   ?  �   e       R   �  �   �       �   ~      S  �   d       �  ]          �          �      �       �   �  �   M       �       �   n      �   !    �         U   �   �   �   "   �   Q               w       �   �    V  �   �   �       r  �   T  G         F   �   \  2  �   �   ]           }       �      Z      A        �   �     d            }     �   0  �       s      �   f       E   !   =       �   �   4      A   &   ^   �  �   �       �   B          
   z  '           #   C             �   c    q   D   �  �           �           B       F  �   h       O  b   �   3   R  o     �              y   �   K   �        �         a   8  �          T   ;   v           �       /       S   E                   �   �   X  �  *       >             9   �       �       �       �           +   \       �   w        3  %      �       �   4   k         �   <   �   `   �   �   6  [  -          u  t     �       0   p            �       $      �           o  (  �   6             Z   g   �  �   �      �   8   J  I  �             L         <  f      �   {      	      r       m      �   y  �   x          �   �   &  �   {   -    `  v                j  '      .  P   �   /  �   �      �   W   |   �               �                        p   �   �   �           b  ~   �       �                       Y   �      �   N       �   �       ,   >   X   ,  �   z   �   ;  K         �   u   I     l        �       �   �   m      �    timed out waiting for input: auto-logout
 	-%s or -o option
 
malloc: %s:%d: assertion botched
   (wd: %s)  (core dumped)  line  $%s: cannot assign in this way %c%c: invalid option %d: invalid file descriptor: %s %s can be invoked via  %s has null exportstr %s is %s
 %s is a function
 %s is a shell builtin
 %s is a shell keyword
 %s is aliased to `%s'
 %s is hashed (%s)
 %s is not bound to any keys.
 %s out of range %s%s%s: %s (error token is "%s") %s: %s %s: %s out of range %s: %s: bad interpreter %s: %s: cannot open as FILE %s: %s: invalid value for trace file descriptor %s: %s: must use subscript when assigning associative array %s: %s:%d: cannot allocate %lu bytes %s: %s:%d: cannot allocate %lu bytes (%lu bytes allocated) %s: ambiguous job spec %s: ambiguous redirect %s: arguments must be process or job IDs %s: bad network path specification %s: bad substitution %s: binary operator expected %s: cannot allocate %lu bytes %s: cannot allocate %lu bytes (%lu bytes allocated) %s: cannot assign fd to variable %s: cannot assign list to array member %s: cannot assign to non-numeric index %s: cannot convert associative to indexed array %s: cannot convert indexed to associative array %s: cannot create: %s %s: cannot delete: %s %s: cannot destroy array variables in this way %s: cannot execute binary file %s: cannot execute: %s %s: cannot get limit: %s %s: cannot modify limit: %s %s: cannot open temp file: %s %s: cannot open: %s %s: cannot overwrite existing file %s: cannot read: %s %s: cannot unset %s: cannot unset: readonly %s %s: command not found %s: error retrieving current directory: %s: %s
 %s: expression error
 %s: file is too large %s: file not found %s: first non-whitespace character is not `"' %s: hash table empty
 %s: history expansion failed %s: host unknown %s: illegal option -- %c
 %s: inlib failed %s: integer expression expected %s: invalid action name %s: invalid argument %s: invalid array origin %s: invalid associative array key %s: invalid callback quantum %s: invalid file descriptor specification %s: invalid limit argument %s: invalid line count %s: invalid option %s: invalid option name %s: invalid service %s: invalid shell option name %s: invalid signal specification %s: invalid timeout specification %s: is a directory %s: job %d already in background %s: job has terminated %s: line %d:  %s: missing colon separator %s: no completion specification %s: no job control %s: no such job %s: not a function %s: not a regular file %s: not a shell builtin %s: not an array variable %s: not an indexed array %s: not dynamically loaded %s: not found %s: numeric argument required %s: option requires an argument %s: option requires an argument -- %c
 %s: parameter null or not set %s: readonly function %s: readonly variable %s: restricted %s: restricted: cannot redirect output %s: restricted: cannot specify `/' in command names %s: substring expression < 0 %s: unary operator expected %s: unbound variable %s: usage:  (( expression )) (core dumped)  (wd now: %s)
 /dev/(tcp|udp)/host/port not supported without networking /tmp must be a valid directory name : <no current directory> ABORT instruction Aborting... Adds a directory to the top of the directory stack, or rotates
    the stack, making the new top of the stack the current working
    directory.  With no arguments, exchanges the top two directories.
    
    Options:
      -n	Suppresses the normal change of directory when adding
    	directories to the stack, so only the stack is manipulated.
    
    Arguments:
      +N	Rotates the stack so that the Nth directory (counting
    	from the left of the list shown by `dirs', starting with
    	zero) is at the top.
    
      -N	Rotates the stack so that the Nth directory (counting
    	from the right of the list shown by `dirs', starting with
    	zero) is at the top.
    
      dir	Adds DIR to the directory stack at the top, making it the
    	new current working directory.
    
    The `dirs' builtin displays the directory stack. Alarm clock BPT trace/trap Bad system call Bogus signal Broken pipe Bus error CPU limit Child death or stop Continue Copyright (C) 2012 Free Software Foundation, Inc. Copyright (C) 2013 Free Software Foundation, Inc. Display the list of currently remembered directories.  Directories
    find their way onto the list with the `pushd' command; you can get
    back up through the list with the `popd' command.
    
    Options:
      -c	clear the directory stack by deleting all of the elements
      -l	do not print tilde-prefixed versions of directories relative
    	to your home directory
      -p	print the directory stack with one entry per line
      -v	print the directory stack with one entry per line prefixed
    	with its position in the stack
    
    Arguments:
      +N	Displays the Nth entry counting from the left of the list shown by
    	dirs when invoked without options, starting with zero.
    
      -N	Displays the Nth entry counting from the right of the list shown by
	dirs when invoked without options, starting with zero. Done Done(%d) EMT instruction Exit %d File limit Floating point exception GNU bash, version %s (%s)
 GNU bash, version %s-(%s)
 GNU long options:
 HFT input data pending HOME not set Hangup I have no name! I/O ready Illegal instruction Information request Interrupt Killed License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
 OLDPWD not set Quit Record lock Removes entries from the directory stack.  With no arguments, removes
    the top directory from the stack, and changes to the new top directory.
    
    Options:
      -n	Suppresses the normal change of directory when removing
    	directories from the stack, so only the stack is manipulated.
    
    Arguments:
      +N	Removes the Nth entry counting from the left of the list
    	shown by `dirs', starting with zero.  For example: `popd +0'
    	removes the first directory, `popd +1' the second.
    
      -N	Removes the Nth entry counting from the right of the list
    	shown by `dirs', starting with zero.  For example: `popd -0'
    	removes the last directory, `popd -1' the next to last.
    
    The `dirs' builtin displays the directory stack. Returns the context of the current subroutine call.
    
    Without EXPR, returns  Running Segmentation fault Shell commands matching keyword ` Shell commands matching keywords ` Shell options:
 Signal %d Stopped Stopped (signal) Stopped (tty input) Stopped (tty output) Stopped(%s) TIMEFORMAT: `%c': invalid format character Terminated The mail in %s has been read
 There are running jobs.
 There are stopped jobs.
 These shell commands are defined internally.  Type `help' to see this list.
Type `help name' to find out more about the function `name'.
Use `info bash' to find out more about the shell in general.
Use `man -k' or `info' to find out more about commands not in this list.

A star (*) next to a name means that the command is disabled.

 This is free software; you are free to change and redistribute it. Type `%s -c "help set"' for more information about shell options.
 Type `%s -c help' for more information about shell builtin commands.
 Unknown Signal # Unknown Signal #%d Unknown error Unknown status Urgent IO condition Usage:	%s [GNU long option] [option] ...
	%s [GNU long option] [option] script-file ...
 Use "%s" to leave the shell.
 Use the `bashbug' command to report bugs.
 User signal 1 User signal 2 Window changed You have mail in $_ You have new mail in $_ [[ expression ]] `%c': bad command `%c': invalid format character `%c': invalid symbolic mode character `%c': invalid symbolic mode operator `%c': invalid time format specification `%s': cannot unbind `%s': invalid alias name `%s': invalid keymap name `%s': missing format character `%s': not a pid or valid job spec `%s': not a valid identifier `%s': unknown function name `)' expected `)' expected, found %s `:' expected for conditional expression add_process: pid %5ld (%s) marked as still alive add_process: process %5ld (%s) in the_pipeline alias [-p] [name[=value] ... ] all_local_variables: no function context at current scope argument argument expected array variable support required attempted assignment to non-variable bad array subscript bad command type bad connector bad jump bad substitution: no closing "`" in %s bad substitution: no closing `%s' in %s bash_execute_unix_command: cannot find keymap for command bug: bad expassign token can only `return' from a function or sourced script can only be used in a function cannot allocate new file descriptor for bash input from fd %d cannot create temp file for here-document: %s cannot duplicate fd %d to fd %d cannot duplicate named pipe %s as fd %d cannot find %s in shared object %s: %s cannot make child for command substitution cannot make child for process substitution cannot make pipe for command substitution cannot make pipe for process substitution cannot open named pipe %s for reading cannot open named pipe %s for writing cannot open shared object %s: %s cannot redirect standard input from /dev/null: %s cannot reset nodelay mode for fd %d cannot set and unset shell options simultaneously cannot set terminal process group (%d) cannot simultaneously unset a function and a variable cannot suspend cannot suspend a login shell cannot use `-f' to make functions cannot use more than one of -anrw child setpgid (%ld to %ld) command_substitute: cannot duplicate pipe as fd 1 completion: function `%s' not found conditional binary operator expected continue [n] could not find /tmp, please create! cprintf: `%c': invalid format character current deleting stopped job %d with process group %ld describe_pid: %ld: no such pid directory stack empty directory stack index division by 0 dynamic loading not available empty array variable name enable [-a] [-dnps] [-f filename] [name ...] error getting terminal attributes: %s error importing function definition for `%s' error setting terminal attributes: %s expected `)' exponent less than 0 expression expected expression recursion level exceeded false file descriptor out of range filename argument required forked pid %d appears in running job %d free: called with already freed block argument free: called with unallocated block argument free: start and end chunk sizes differ free: underflow detected; mh_nbytes out of range getcwd: cannot access parent directories hashing disabled here-document at line %d delimited by end-of-file (wanted `%s') history position history specification hits	command
 identifier expected after pre-increment or pre-decrement if COMMANDS; then COMMANDS; [ elif COMMANDS; then COMMANDS; ]... [ else COMMANDS; ] fi initialize_job_control: getpgrp failed initialize_job_control: line discipline initialize_job_control: setpgid invalid arithmetic base invalid base invalid character %d in exportstr for %s invalid hex number invalid number invalid octal number invalid signal number job %d started without job control last command: %s
 limit line %d:  line editing not enabled logout
 loop count make_here_document: bad instruction type %d make_local_variable: no function context at current scope make_redirection: redirection instruction `%d' out of range malloc: block on free list clobbered malloc: failed assertion: %s
 migrate process to another CPU missing `)' missing `]' missing hex digit for \x missing unicode digit for \%c network operations not supported no `=' in exportstr for %s no closing `%c' in %s no command found no help topics match `%s'.  Try `help help' or `man -k %s' or `info %s'. no job control no job control in this shell no match: %s no other directory no other options allowed with `-x' not currently executing completion function not login shell: use `exit' octal number only meaningful in a `for', `while', or `until' loop pipe error pop_scope: head of shell_variables not a temporary environment scope pop_var_context: head of shell_variables not a function context pop_var_context: no global_variables context power failure imminent print_command: bad connector `%d' progcomp_insert: %s: NULL COMPSPEC programming error pwd [-LP] read error: %d: %s realloc: called with unallocated block argument realloc: start and end chunk sizes differ realloc: underflow detected; mh_nbytes out of range recursion stack underflow redirection error: cannot duplicate fd register_alloc: %p already in table as allocated?
 register_alloc: alloc table is full with FIND_ALLOC?
 register_free: %p already in table as free?
 restricted run_pending_traps: bad value in trap_list[%d]: %p run_pending_traps: signal handler is SIG_DFL, resending %d (%s) to myself save_bash_input: buffer already exists for new fd %d setlocale: %s: cannot change locale (%s) setlocale: %s: cannot change locale (%s): %s setlocale: LC_ALL: cannot change locale (%s) setlocale: LC_ALL: cannot change locale (%s): %s shell level (%d) too high, resetting to 1 shift count sigprocmask: %d: invalid operation start_pipeline: pgrp pipe syntax error syntax error in conditional expression syntax error in conditional expression: unexpected token `%s' syntax error in expression syntax error near `%s' syntax error near unexpected token `%s' syntax error: `((%s))' syntax error: `;' unexpected syntax error: arithmetic expression required syntax error: invalid arithmetic operator syntax error: operand expected syntax error: unexpected end of file system crash imminent times too many arguments trap_handler: bad signal %d true unalias [-a] name [name ...] unexpected EOF while looking for `]]' unexpected EOF while looking for matching `%c' unexpected EOF while looking for matching `)' unexpected argument `%s' to conditional binary operator unexpected argument `%s' to conditional unary operator unexpected argument to conditional binary operator unexpected argument to conditional unary operator unexpected token %d in conditional command unexpected token `%c' in conditional command unexpected token `%s' in conditional command unexpected token `%s', conditional binary operator expected unexpected token `%s', expected `)' unknown unknown command error value too great for base wait: pid %ld is not a child of this shell wait_for: No record of process %ld wait_for_job: job %d is stopped waitchld: turning on WNOHANG to avoid indefinite block warning:  warning: %s: %s warning: -C option may not work as you expect warning: -F option may not work as you expect write error: %s xtrace fd (%d) != fileno xtrace fp (%d) xtrace_set: %d: invalid file descriptor xtrace_set: NULL file pointer Project-Id-Version: GNU bash 3.1-release
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-02-11 11:19-0500
PO-Revision-Date: 2015-03-02 11:46+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 15:59+0000
X-Generator: Launchpad (build 18115)
 тайм-аут ожидания ввода: завершение сеанса
 	-%s или опция -o
 
malloc: %s:%d: assertion botched
   (wd: %s)  (сделан дамп памяти)  строка  $%s: не могу присвоить таким способом %c%c: недопустимая опция %d: недопустимый дескриптор файла: %s %s может быть вызван с помощью  %s имеет нулевой exportstr %s является %s
 %s является функцией
 %s встроена в оболочку
 %s является ключевым словом оболочки
 %s является алиасом для `%s'
 %s хэширована (%s)
 %s не привязана не к одной из клавиш.
 %s выходит за пределы допустимых значений %s%s%s: %s (ошибочная метка "%s") %s: %s %s: %s выходит за пределы допустимых значений %s: %s: плохой интерпретатор %s: %s: Невозможно открыть как FILE %s: %s: неверное значение указателя файла трассировки %s: %s: необходимо использовать индексы при назначении ассоциативного массива %s: %s:%d: невозможно зарезервировать %lu байтов %s: %s:%d: невозможно зарезервировать %lu байтов (%lu байтов зарезервировано) %s: неоднозначное определение задачи %s: неоднозначное перенаправление %s: аргументы должны быть идентификаторами процесса или задачи %s: плохое определение сетевого пути %s: неправильная подстановка %s: ожидается использование бинарного оператора %s: невозможно зарезервировать %lu байтов %s: невозможно зарезервировать %lu байтов (%lu байтов зарезервировано) %s: невозможно присвоить fd переменной %s: невозможно занести список в элемент массива %s; не могу назначить не числовой индекс %s: не удалось преобразовать ассоциативный массив в индексированный %s: не удалось преобразовать индексированный массив в ассоциативный %s: не могу создать: %s %s: не могу удалить:  %s %s: не могу удалить переменную-массив таким способом %s: не удалось запустить двоичный файл %s: не могу запустить: %s %s: не могу получить лимит: %s %s: не могу изменить лимит: %s %s: не могу открыть временный файл: %s %s: не могу открыть: %s %s: не могу переписать уже существующий файл %s: не могу прочитать: %s %s: не могу сбросить %s: не могу сбросить: доступно только для чтения %s %s: команда не найдена %s: ошибка получения текущей директории: %s: %s
 %s: ошибка выражения
 %s: слишком большой файл %s: файл не найден %s: первый непробельный символ не `"' %s: хэш-таблица пуста
 %s: не удалось расширение истории %s: хост неизвестен %s: недопустимая опция -- %c
 %s: сбой inlib %s: ожидается числовое выражение %s: неверное имя действия %s: недопустимый аргумент %s: неверное начало массива %s: неверный ключ ассоциативного массива %s: неверное обращение к части массива %s: недопустимое описание файлового дескриптора %s: неверный аргумент лимита %s: неверное количество строк %s: неправильная опция %s: недопустимое имя опции %s: недопустимый сервис %s: недопустимое имя опции оболочки %s: недопустимая спецификация сигнала %s: неверное определение таймаута %s: является директорией %s: задача %d уже работает в фоне %s: задача завершилась %s: строка %d:  %s: пропущен разделитель двоеточие %s: нет примера завершения %s: нет такой задачи %s: нет такой задачи %s: не функция %s: не является обычным файлом %s: не встроена в оболочку %s: не переменная-массив %s: не индексированный массив %s: загружено не динамически %s: не найден %s: требуется числовой аргумент %s: опция требует аргумента %s: опции требуется аргумент -- %c
 %s: параметр null или не установлен %s: доступная только на чтение функция %s: доступная только на чтение переменная %s: ограничено %s: ограничение: не могу перенаправить вывод %s: ограничение: нельзя указывать `/' в именах команд %s: выражение подстроки < 0 %s: ожидается использование унарного оператора %s: свободное значение %s: использование:  (( выражение )) (подготовлен дамп ядра)  (wd сейчас: %s)
 /dev/(tcp|udp)/host/port не поддерживается без сетевой подсистемы /tmp должна быть действительным именем директории : <нет текущего каталога> инструкция ABORT Завершаю работу... Добавление директории на вершину списка каталогов, или обращение
    списка, делая новой вершиной списка текущую рабочую
    директорию. Без аргументов, обмен двух верхних каталогов.
    
    Опции:
      -n 	 Запрещает нормальное изменение каталога при добавлении
    	, каталогов в список, так что возможно только манипулирование списком.
    
    Аргументы:
      +N 	 Поворачивает список так, что N-й каталог (считая
    	 с левой части списка, показанного с помощью `dirs ', начиная с
    	 нуля) становится на самый верх.
    
      -N 	 Поворачивает список так, что N-й каталог (считая
    	, справа от списка, показанного с помощью `dirs ', начиная с
    	 нуля) становится на самый верх.
    
      dir 	 Добавляет DIR в список директорий, в верхнюю часть, что делает его
    	 новым текущим рабочим каталогом.
    
    `dirs ' выводит список каталогов. Сигнал таймера BPT трассировка/захват Неверный системный вызов Фиктивный сигнал Нарушенный канал Ошибка шины Лимит ЦП Потомок убит или остановлен Продолжить Copyright (C) 2012 Free Software Foundation, Inc. Copyright (C) 2013 Free Software Foundation, Inc. Отображает список текущих запомненных директорий.  Директории
    можно занести в список, используя команду `pushd'; Вы можете забрать
    директорию из списка, используя команду `popd'.
    
    Параметры:
      -c	очистить стек директорий, удалив все его элементы
      -l	не печатать начинающиеся с тильды директории относительно
    	Вашей домашней директории
      -p	вывести стек директорий по одной в строке
      -v	вывести стек директорий по одной в строке, ставя перед ней
    	ее позицию в стеке
    
    Аргументы:
      +N	Отобразить N-тую запись, считая слева в списке
    	директорий, показываемом при вызове без параметров, начиная с 0.
    
      -N	Отобразить N-тую запись, считая справа в списке
	директорий, показываемом при вызове без параметров, начиная с 0. Готово Выполнено(%d) инструкция EMT Выход из %d Лимит файлов Исключение в операции с плавающей точкой GNU bash, версия %s (%s)
 GNU bash, версия %s-(%s)
 Длинные опции в стеле GNU:
 Ожидается ввод данных HFT переменная HOME не установлена Отключение У меня нет имени! Ввод-вывод готов недопустимая инструкция Запрос информации Прерывание Принудительно завершён Лицензия GPLv3+: GNU GPL версии 3 или более поздней <http://gnu.org/licenses/gpl.html>
 переменная OLDPWD не установлена Выход Блокировать запись Удаление записей из списка каталогов. Без аргументов, удаляется 
    каталог с вершины списка и изменятеся новая директория на вершине.
    
    Опции:
      -n 	 Запрещает нормальное изменение каталога при удалении
    	, каталогов из списка, так что возможно только манипулирование списком.
    
       +N 	 Удаляет N-ю запись, считая слева от списка,
    	 показанного с помощью 'dirs ', начиная с нуля. Например: `popd +0'
    	 удаляет первый каталог, `popd +1' второй.
    
      -N 	 Удаляет N-ю запись, считая справа от списка
    	 показанного с помощью `dirs ', начиная с нуля. Например: `popd -0 '
    	 удаляет последний каталог, `popd -1 ' предпоследний.
    
    `dirs ' выводит список каталогов. Возвращает контекст текущего вызова подпрограммы.
    
    Без EXPR, возвращает  Выполняется Нарушение прав доступа к памяти Команды оболочки, соответствующие ключевому слову ` Команды оболочки, соответствующие ключевым словам ` Команды оболочки, соответствующие ключевым словам ` Опции оболочки:
 Сигнал %d Остановлено Остановлено (сигнал) Остановлено (ввод с терминала) Останов (вывод с терминала) Остановлено(%s) TIMEFORMAT: `%c': неверный символ формата Прервано Почта в %s была прочитана
 Список выполняемых заданий.
 Есть приостановленные задачи.
 Эти команды включены в состав оболочки. Напишите `help'  чтобы увидеть этот список.
Напишите `help name' чтобы больше узнать о функции `name'.
Используйте `info bash' чтобы больше узнать об оболочке в целом.
Используйте `man -k' или `info' чтобы больше узнать о командах, не включенных в этот список.

Звездочка (*) возле имени означает, что команда отключена.

 Это свободное программное обеспечение; его можно свободно изменять и распространять. Напишите `%s -c "help set"' для получения информации об опциях оболочки.
 Напишите `%s -c ' для получения информации о встроенных командах оболочки.
 Неизвестный сигнал # Неизвестный сигнал #%d Неизвестная ошибка Неизвестный статус Состояние срочного ввода-вывода Использование:
%s [длинные опции а-ля `GNU'] [опции] ...
	%s [длинные опции а-ля `GNU'] [опции] файл_со_скриптом...
 Используйте "%s", чтобы завершиться работу с  оболочкой.
 Используйте команду `bashbug' для сообщений об ошибках.
 Пользовательский сигнал 1 Пользовательский сигнал 2 Окно изменено У вас есть почта в $_ У вас есть новая почта в $_ [[ выражение ]] `%c': плохая команда %c': недопустимый символ форматирования `%c': неверный символ символьного режима `%c': неверный оператор символьного режима `%c': неверно указан формат времени `%s': не могу освободить (unbind) `%s': неверное имя псевдонима `%s': неверное имя раскладки `%s': пропущен символ форматирования `%s': не идентификатор  процесса или правильное имя задачи `%s': неправильный идентификатор `%s': неизвестное имя функции ожидается `)' ожидался `)', найден %s ожидается `:'  в условном выражении добавлен процесс: pid %5ld (%s) помечен живым добавлен процесс: процесс %5ld (%s) в очереди alias [-p] [name[=значение] ... ] all_local_variables: no function context at current scope аргумент предполагается что будет использован аргумент требуется поддержка переменных-массивов попытка присвоения не-переменной неправильный индекс массива плохой тип команды плохой соединитель плохой переход некорректная подстановка: нет закрывающей "`" в %s плохая подстановка: нет закрывающей `%s' в %s bash_execute_unix_command: не могу найти в раскладке соответствие команде bug: bad expassign token `return'  может использоваться только в функции или в "sourced script" может быть использована только в функции не могу выделить новый файловый дескриптор для ввода bash из fd %d невозможно создать временный файл для документа: %s не могу дублировать fd %d в fd %d не могу дублировать именованый канал %s как fd %d не могу найти %s в общих объектах %s: %s не могу создать потомка для подмены команды не могу создать потомка для подмены процесса не могу создать канал для подмены команды не могу создать канал для подмены процесса не могу открыть именной канал %s для чтения не могу открыть именной канал %s для записи не могу открыть общий объект %s: %s не могу перенаправить стандартный ввод из /dev/null: %s невозможно сбросить режим без задержки для fd %d не могу одновременно определять и убирать определения опций оболочки невозможно установить группу терминального процесса (%d) не могу одновременно убрать определения функции и переменной невозможно остановить не могу приостановить оболочку входа не могу использовать `-f' для создания функций не могу использовать более одной опции из -anrw дочерний setpgid (%ld к %ld) command_substitute: cannot duplicate pipe as fd 1 дополнение: функция `%s' не найдена ожидается условный бинарный оператор continue [n] не могу найти /tmp, пожалуйста создайте! cprintf: `%c': неверный символ формата текущий удаляю остановленную задачу %d, имеющую идентификатор группы процессо %ld describe_pid: %ld: нет такого pid стек директорий пуст стековый индекс директории деление на 0 динамическая загрузка не доступна пустое имя массива включить [-a] [-dnps] [-f имя файла] [имя ...] ошибка при чтении атрибутов терминала: %s ошибка при импортировании определения функции`%s' ошибка при установке атрибутов терминала: %s ожидался `)' экспонента меньше 0 ожидалось выражение превышен предел рекурсии выражения ложь файловый дескриптор за пределами допустимого диапазона требуется аргумент имя файла порожденный pid %d замечен в рабочей области %d пусто: вызван с уже освобождённым аргументом блока free: запрошен с не освобождённым аргументом блока free: начальная и конечная части различаются free: обнаружено исчезновение; mh_nbytes за пределами getcwd: невозможен доступ к родительским каталогам хэширование отключено документ неожиданно заканчивается на строке %d (ожидалось `%s') положение в истории определение истории показов	команды
 идентификатор ожидается после прединкремента или преддекремента if КОМАНДЫ; then КОМАНДЫ; [ elif КОМАНДЫ; then КОМАНДЫ; ]... [ else КОМАНДЫ; ] fi initialize_job_control: сбой getpgrp initialize_job_control: линейный порядок initialize_job_control: setpgid неверное арифметическое основание неверная база invalid character %d in exportstr for %s неверное шестнадцатеричное число недопустимое число неверное восьмеричное число недопустимый номер сигнала задача %d запущена бесконтрольно последняя команда: %s
 предел строка %d:  редактирование строки запрещено выход
 количество циклов make_here_document: неверный тип инструкции: %d make_local_variable: no function context at current scope make_redirection: redirection instruction `%d' out of range malloc: нарушена блокировка списка свободных блоков malloc: ошибка проверки условия: %s
 перенос процесса на другой ЦП пропущен `)' пропущен `]' в \x отсутствует шестнадцатиричная цифра пропущена цифра в \%c сетевые операции не поддерживаются no `=' in exportstr for %s нет закрывающего `%c' в %s не нашел такую команду нет разделов справки, соответствующих `%s'. Попробуйте `help help' или `man -k %s' или `info %s'. %s: нет такой задачи в этой оболочке нет управления задачей нет совпадения с: %s нет другой директории не разрешено использование других опций совместно с `-x' не исполняющаяся в настоящее время завершающая функция оболочка не является запущенной после входа в систему: используйте `exit' число в восьмеричной системе исчисления имеет смысл только в циклах `for', `while', или `until' ошибка конвейера pop_scope: head of shell_variables not a temporary environment scope pop_var_context: head of shell_variables not a function context pop_var_context: no global_variables context угроза отключения питания print_command: bad connector `%d' progcomp_insert: %s: NULL COMPSPEC программная ошибка pwd [-LP] ошибка чтения: %d: %s realloc: запрошен с не освобождённым аргументом блока realloc: start and end chunk sizes differ realloc: обнаружено исчезновение; mh_nbytes за пределами переполнение стека рекурсии ошибка перенаправления: не могу дублировать fd register_alloc: %p already in table as allocated?
 register_alloc: alloc table is full with FIND_ALLOC?
 register_free: %p already in table as free?
 ограничено run_pending_traps: bad value in trap_list[%d]: %p run_pending_traps: signal handler is SIG_DFL, resending %d (%s) to myself save_bash_input: для файлового дескриптора %d буфер уже существует setlocale: %s: невозможно изменить локаль (%s) setlocale: %s: невозможно изменить локаль (%s): %s setlocale: LC_ALL: невозможно изменить локаль (%s) setlocale: LC_ALL: невозможно изменить локаль (%s): %s уровень оболочки (%d) слишком велик, сбрасываю до 1 количество сдвигов sigprocmask: %d: неверная операция запуск конвеера: pgrp pipe ошибка синтаксиса ошибка синтаксиса в условном выражении ошибка синтаксиса в условном выражении: неожиданная лексема `%s' синтаксическая ошибка в выражение ошибка синтаксиса около `%s' ошибка синтаксиса около неожиданной лексемы `%s' ошибка синтаксиса: `((%s))' ошибка синтаксиса: `;' не ожидается ошибка синтаксиса: требуется арифметическое выражение Ошибка синтаксиса: неправильный арифметический оператор ошибка синтаксиса: ожидается операнд ошибка синтаксиса: неожиданный конец файла угроза сбоя системы раз(а) слишком много аргументов trap_handler: неверный сигнал %d истина unalias [-a] имя [имя...] неожиданный EOF при поиске `]]' неожиданный EOF при поиске соответствующего `%c' неожиданный EOF при поиске соответствующей `)' неожиданный аргумент `%s'  условного бинарного оператора неожиданный аргумент условного унарного оператора `%s' неожиданный аргумент условного бинарного оператора неожиданный аргумент условного унарного оператора неожиданная лексема %d в условной команде неожиданная лексема `%c' в условной команде неожиданная лексема `%s' в условной команде неожиданный лексема `%s', ожидается условный бинарный оператор неожиданная лексема `%s', ожидалась `)' неизвестный неизвестная ошибка команды значение слишком велико для основания wait: pid %ld не является дочерним процессом этой оболочки wait_for: Нет записи для процесса %ld wait_for_job: задача %d остановлена waitchld: включение WNOHANG для предотвращения неопределенной блокировки внимание:  предупреждение: %s: %s предупреждение: опция -C может не работать так как вы ожидаете предупреждение: опция -F может не работать так как вы ожидаете ошибка записи: %s xtrace fd (%d) != fileno xtrace fp (%d) xtrace_set: %d: недопустимый дескриптор файла xtrace_set: указатель на файл равен NULL 