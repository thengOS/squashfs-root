��          �      ,      �     �     �     �     �  �   �  �   �  ~        �  /   �     �     �     �  ?        C     Q  
   c  �  n     Y  '   a  -   �  -   �  k  �  $  Q    v	  "   {
  V   �
  0   �
  .   &  ,   U  t   �  #   �  *        F        	                                                                  
    %s (%s) <small><b>Action:</b></small> <small><b>Vendor:</b></small> <small><b>_Details</b></small> An application is attempting to perform an action that requires privileges. Authentication as one of the users below is required to perform this action. An application is attempting to perform an action that requires privileges. Authentication as the super user is required to perform this action. An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Authenticate Authentication dialog was dismissed by the user Click to edit %s Click to open %s Select user... Your authentication attempt was unsuccessful. Please try again. _Authenticate _Password for %s: _Password: Project-Id-Version: policykit-gnome.HEAD.ru
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=policykit-gnome&component=general
POT-Creation-Date: 2015-12-04 00:05+0000
PO-Revision-Date: 2015-12-04 21:25+0000
Last-Translator: Yuri Myasoedov <Unknown>
Language-Team: Russian <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:41+0000
X-Generator: Launchpad (build 18115)
 %s (%s) <small><b>Действие:</b></small> <small><b>Разработчик:</b></small> <small><b>_Подробности</b></small> Приложение пытается выполнить действие, которое требует дополнительных привилегий. Для выполнения этого действия необходима аутентификация под именем одного из перечисленных ниже пользователей. Приложение пытается выполнить действие, которое требует дополнительных привилегий. Для выполнения этого действия необходима аутентификация пользователя root. Приложение пытается выполнить действие, которое требует дополнительных привилегий. Для выполнения этого действия требуется аутентификация. Аутентифицировать Диалог авторизации был отклонен пользователем Щёлкните, чтобы изменить %s Щёлкните, чтобы открыть %s Выберите пользователя... Попытка авторизации не удалась. Пожалуйста, попробуйте ещё раз. _Аутентифицировать _Пароль пользователя %s: _Пароль: 