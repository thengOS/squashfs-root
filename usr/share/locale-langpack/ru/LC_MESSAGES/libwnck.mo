��    �      T  7  �      h     i     n  	   {     �     �     �     �     �  "   �     �     �     �     �          '     :     P     b     q     �  N   �  g   �  G   C  :   �  E   �  A     *   N  *   y  )   �  (   �  7   �  s   /  '   �  ?   �  <        H  ,   Y  $   �     �     �     �  /   �  8   !  \   Z  b   �  d     ]     d   �  f   B  b   �  m     `   z     �  "   �  !     /   8     h     {     �     �  
   �  >   �  G   �  5   3     i     �  `   �  G   �  	   A     K     Y      w  #   �  #   �     �  5   �  !   1   $   S   '   x   '   �   9   �   $   !  )   '!  5   Q!      �!     �!     �!  	   �!     �!     �!  <   "  <   D"  :   �"     �"     �"     �"     #      #     6#     ;#  )   B#  ,   l#  	   �#     �#     �#     �#  #   �#     $  %   $  (   @$  (   i$  +   �$     �$  )   �$     �$  �   %     �%     �%     &     &     )&  *   :&  -   e&  -   �&  0   �&     �&     '  (   '  *   E'     p'  
   �'     �'     �'  &   �'     �'  !   �'     (     #(     7(     G(  "   S(     v(      �(     �(     �(     �(     �(     �(  L   )  8   T)     �)     �)     �)     �)     �)     �)  E   �)     8*     E*     T*  :   b*     �*     �*     �*     �*  <   �*  .   +     H+  	   L+     V+     X+     u+  
   |+     �+     �+     �+     �+     �+     �+     �+     �+     �+      ,     ,     ,     ,     *,  
   0,  
   ;,  
   F,     Q,     Z,     p,  	   �,     �,     �,     �,  	   �,     �,     �,     �,     �,     �,     
-     -     -     -      -     &-  
   --     8-     F-     T-     c-     i-     p-     }-     �-     �-     �-  
   �-     �-     �-  
   �-     �-     �-     �-     .     .     !.  �  0.     �/     �/     �/     �/     �/     0     
0  (   0  W   60  *   �0  !   �0  $   �0  !    1  2   "1     U1  /   t1     �1     �1     �1  
   �1  �   2  �   �2  ^   V3  b   �3  }   4  �   �4  X   5  T   p5  >   �5  =   6  U   B6  �   �6  >   (7  a   g7  [   �7     %8  ^   58  Y   �8  K   �8  C   :9     ~9  I   �9  V   �9  �   7:  �   �:     a;  {   �;  �   ]<  �   	=  �   �=  �   j>  �   2?  0   �?  O   �?  4   @@  F   u@  *   �@  "   �@     
A     A     0A  �   BA  �   �A  f   uB  <   �B     C  �   4C  �   �C  .   �D  5   �D  7   �D  Q   (E  2   zE  3   �E  D   �E  W   &F  >   ~F  M   �F  .   G  >   :G  h   yG  F   �G  G   )H  X   qH  8   �H  -   I  4   1I     fI     xI     �I  �   �I  �   DJ  �   �J  C   vK  C   �K  ?   �K  A   >L  ?   �L     �L  
   �L  C   �L  R   M     iM      M     �M  '   �M  6   �M  #   N  \   <N  J   �N  F   �N  Y   +O     �O  h   �O  (   �O  $   P     EQ     dQ     �Q  ,   �Q  +   �Q  m   �Q  [   eR  W   �R  j   S  (   �S  7   �S  S   �S  ^   9T  .   �T     �T  ;   �T     U  Y   "U  H   |U  [   �U     !V  #   >V     bV  5   V  K   �V  4   W  G   6W     ~W     �W  1   �W  )   �W     X  �   ,X  ~   �X  <   OY     �Y     �Y     �Y  #   �Y     �Y  �   Z     �Z     �Z     �Z  t   �Z  1   b[  +   �[  2   �[     �[  �   �[  �   �\     ]  	   ]     ]  ;   ]     []     k]     �]  5   �]     �]  2   �]     "^      )^     J^  6   Q^  *   �^     �^     �^     �^  5   �^     (_     /_     G_     a_  -   {_  /   �_  +   �_  -   `  /   3`  +   c`     �`     �`     �`     �`  4   �`     a     ,a     Da     Ya     la     �a  "   �a  "   �a  H   �a  +   0b  !   \b     ~b     �b     �b  #   �b  @   �b     c  *   c  *   Ic  4   tc  B   �c  >   �c     +d     @d  (   Sd     |d  '   �d     �d     �d     �       �   v   |   Q   �   �   	   �   t       �   �           �          �               S   �       �   O           �   �          �       ,   �   �   �      �          <       (           %   `   i   2   �   a   >   g      �           �       /                �       �   �   k   V   A       �   �       �   �   n   "   5                 [      K   H       �   9   �   Z   m      y       q       �   �   �   @   I               f          R           L   �       �   +   $   N   �   =   �   �   �   c   o          &   �       z       �   }   �   U   F   #       )   �   �   �       �               �      �   h           �   7   .       G       u       �       �           �           �       �   �       �          -       Y       �   �   �   �   4   �   �      l   T   �   b   ?   p   �   �          �   r   �   �   �      �   �   '   �   �   �      {   ]      :              
   �       M   �   �       �   �   �   8          �   s   �   �       W   �   �   3   X   �   �   \   !   *       B       P   �                   j   �   �   w                     �   J   �          �   6   �   �   �       �          _   ^   D   ~   0   e   ;   �      �   1   �   E           �   C      d   �       �   x   �    "%s" %1$s%2$s%3$s %d ("%s") %d: %s
 %lu (%s) %lu: %s
 ,  <name unset> <no EWMH-compliant window manager> <no viewport> <unset> Action not allowed
 Activate the window Activate the workspace Active Window: %s
 Active Workspace: %s
 Alias of --window Always On _Top Bottom Neighbor: %s
 CLASS Cannot change the workspace layout on the screen: the layout is already owned
 Cannot interact with application having its group leader with XID %lu: the application cannot be found
 Cannot interact with class group "%s": the class group cannot be found
 Cannot interact with screen %d: the screen does not exist
 Cannot interact with window with XID %lu: the window cannot be found
 Cannot interact with workspace %d: the workspace cannot be found
 Change the X coordinate of the window to X Change the Y coordinate of the window to Y Change the height of the window to HEIGHT Change the name of the workspace to NAME Change the number of workspaces of the screen to NUMBER Change the type of the window to TYPE (valid values: normal, desktop, dock, dialog, toolbar, menu, utility, splash) Change the width of the window to WIDTH Change the workspace layout of the screen to use NUMBER columns Change the workspace layout of the screen to use NUMBER rows Class Group: %s
 Class resource of the class group to examine Click this to switch to workspace %s Click to start dragging "%s" Click to switch to "%s" Close the window Conflicting options are present: --%s and --%s
 Conflicting options are present: --%s or --%s, and --%s
 Conflicting options are present: a window should be interacted with, but --%s has been used
 Conflicting options are present: an application should be interacted with, but --%s has been used
 Conflicting options are present: class group "%s" should be interacted with, but --%s has been used
 Conflicting options are present: screen %d should be interacted with, but --%s has been used
 Conflicting options are present: windows of an application should be listed, but --%s has been used
 Conflicting options are present: windows of class group "%s" should be listed, but --%s has been used
 Conflicting options are present: windows of workspace %d should be listed, but --%s has been used
 Conflicting options are present: windows or workspaces of screen %d should be listed, but --%s has been used
 Conflicting options are present: workspace %d should be interacted with, but --%s has been used
 Current workspace: "%s" Error while parsing arguments: %s
 Geometry (width, height): %d, %d
 Geometry (x, y, width, height): %d, %d, %d, %d
 Group Leader: %lu
 Group Name: %s
 HEIGHT Icon Name: %s
 Icons: %s
 Invalid argument "%d" for --%s: the argument must be positive
 Invalid argument "%d" for --%s: the argument must be strictly positive
 Invalid argument "%s" for --%s, valid values are: %s
 Invalid value "%s" for --%s Left Neighbor: %s
 List windows of the application/class group/workspace/screen (output format: "XID: Window Name") List workspaces of the screen (output format: "Number: Workspace Name") Ma_ximize Ma_ximize All Make the window always on top Make the window appear in pagers Make the window appear in tasklists Make the window below other windows Make the window fullscreen Make the window have a fixed position in the viewport Make the window not always on top Make the window not appear in pagers Make the window not appear in tasklists Make the window not below other windows Make the window not have a fixed position in the viewport Make the window quit fullscreen mode Make the window visible on all workspaces Make the window visible on the current workspace only Maximize horizontally the window Maximize the window Maximize vertically the window Mi_nimize Mi_nimize All Minimize the window Move the viewport of the current workspace to X coordinate X Move the viewport of the current workspace to Y coordinate Y Move the window to workspace NUMBER (first workspace is 0) Move to Another _Workspace Move to Workspace R_ight Move to Workspace _Down Move to Workspace _Left Move to Workspace _Up NAME NUMBER NUMBER of the screen to examine or modify NUMBER of the workspace to examine or modify Name: %s
 No Windows Open Number of Windows: %d
 Number of Workspaces: %d
 On Screen: %d (Window Manager: %s)
 On Workspace: %s
 Options to list windows or workspaces Options to modify properties of a screen Options to modify properties of a window Options to modify properties of a workspace PID: %s
 Position in Layout (row, column): %d, %d
 Possible Actions: %s
 Print or modify the properties of a screen/workspace/window, or interact with it, following the EWMH specification.
For information about this specification, see:
	http://freedesktop.org/wiki/Specifications/wm-spec Resource Class: %s
 Right Neighbor: %s
 Screen Number: %d
 Session ID: %s
 Shade the window Show options to list windows or workspaces Show options to modify properties of a screen Show options to modify properties of a window Show options to modify properties of a workspace Show the desktop Showing the desktop: %s
 Start moving the window via the keyboard Start resizing the window via the keyboard Startup ID: %s
 State: %s
 Stop showing the desktop TYPE Tool to switch between visible windows Tool to switch between windows Tool to switch between workspaces Top Neighbor: %s
 Transient for: %lu
 Un_minimize All Unma_ximize Unmaximize horizontally the window Unmaximize the window Unmaximize vertically the window Unmi_nimize Unminimize the window Unshade the window Untitled application Untitled window Viewport cannot be moved: the current workspace does not contain a viewport
 Viewport cannot be moved: there is no current workspace
 Viewport position (x, y): %s
 WIDTH Window List Window Manager: %s
 Window Selector Window Type: %s
 Window cannot be moved to workspace %d: the workspace does not exist
 Workspace %d Workspace %s%d Workspace 1_0 Workspace Layout (rows, columns, orientation): %d, %d, %s
 Workspace Name: %s
 Workspace Number: %d
 Workspace Switcher X X window ID of the group leader of an application to examine X window ID of the window to examine or modify XID XID: %lu
 Y _Always on Visible Workspace _Close _Close All _Move _Only on This Workspace _Resize _Unmaximize All above all workspaces below change fullscreen mode change workspace close desktop dialog window dock or panel false fullscreen make above make below maximize maximize horizontally maximize vertically maximized maximized horizontally maximized vertically minimize minimized move needs attention no action possible normal normal window pin pinned resize set shade shaded skip pager skip tasklist splash screen startupIDnone stick sticky tearoff menu tearoff toolbar true unmake above unmake below unmaximize unmaximize horizontally unmaximize vertically unminimize unpin unshade unstick utility window windownone workspacenone Project-Id-Version: libwnck trunk
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 23:36+0000
PO-Revision-Date: 2011-01-02 22:36+0000
Last-Translator: Nickolay V. Shmyrev <nshmyrev@yandex.ru>
Language-Team: Russian <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:08+0000
X-Generator: Launchpad (build 18115)
 «%s» %1$s%2$s%3$s %d («%s») %d: %s
 %lu (%s) %lu: %s
 ,  ‹имя не установлено› ‹менеджер окон не соответствует стандарту EWMN› ‹нет видимой области› ‹не установлено› Действие запрещено
 Активировать окно Активировать рабочее место Активное окно: %s
 Активное рабочее место: %s
 то же, что и --window Всегда _наверху Сосед снизу: %s
 КЛАСС Не удалось сменить расположение рабочих мест на экране: расположение уже используется
 Не удалось обратиться к приложению с идентификатором XID главного окна %lu: приложение не найдено
 Не удалось обратиться к классу «%s»: класс не найден
 Не удалось обратиться к экрану %d: экран не существует
 Не удалось обратиться к окну с идентификатором XID %lu: окно не найдено
 Не удалось обратиться к рабочему месту %d: рабочее место не существует
 Сделать горизонтальную координату окна равной X Сделать вертикальную координату окна равной Y Сделать высоту окна равной ВЫСОТЕ Сменить имя рабочего места на ИМЯ Изменить число рабочих мест на экране на ЧИСЛО Сменить тип окна на ТИП (возможные значения: normal, desktop, dock, dialog, toolbar, menu, utility, splash) Сделать ширину окна равной ШИРИНЕ Расположить рабочие места на экране в ЧИСЛО столбцов Расположить рабочие места на экране в ЧИСЛО строк Класс: %s
 Ресурс класса для того класса, который надо изучить Щёлкните, чтобы переключиться на рабочее место %s Щёлкните, чтобы начать перетаскивать «%s» Щёлкните, чтобы переключиться на «%s» Закрыть окно Заданы несовместимые параметры: --%s и --%s
 Заданы несовместимые параметры: --%s или --%s, и --%s
 Заданы несовместимые параметры: должно быть указано окно, но использовано --%s
 Заданы несовместимые параметры: должно быть указано приложение, но использовано --%s
 Заданы несовместимые параметры: указан класс «%s», но использовано --%s
 Заданы несовместимые параметры: указан экран %d, но использовано --%s
 Заданы несовместимые параметры: должны быть перечислены окна приложения, но использовано --%s
 Заданы несовместимые параметры: должны быть перечислены окна класса «%s», но использовано --%s
 Заданы несовместимые параметры: должны быть перечислены окна рабочего места %d, но использовано --%s
 Заданы несовместимые параметры: должны быть перечислены окна или рабочие места экрана %d, но использовано --%s
 Заданы несовместимые параметры: указано рабочее место %d, но использовано --%s
 Текущее рабочее место: «%s» Произошла ошибка при разборе аргументов: %s
 Размеры (ширина, высота): %d, %d
 Геометрия (x, y, ширина, высота): %d, %d, %d, %d
 Главное окно группы: %lu
 Название группы: %s
 ВЫСОТА Имя значка: %s
 Значки: %s
 Недопустимый аргумент «%d» для параметра %s: аргумент должен быть положительным
 Недопустимый аргумент «%d» для параметра --%s: аргумент должен быть строго положительным
 Недопустимый аргумент «%s» для %s, допустимые значения: %s
 Недопустимое значение «%s» для --%s Левый сосед: %s
 Перечислить окна приложения/класса/рабочего места/экрана (формат вывода: «XID: Название окна») Перечислить рабочие места на текущем экране (формат вывода: «Номер: Название рабочего места») Развернуть на весь _экран Развернуть все на весь _экран Показывать окно поверх других Показать окно на переключателе рабочих мест Показать окно в списке окон Показывать окно под другими Перевести окно в полноэкранный режим Зафиксировать положение окна в видимой области Отменить показ окна поверх других Скрыть окно на переключателе рабочих мест Скрыть окно в списке окон Отменить показ окна поверх других Отменить фиксированное положение окна в видимой области Вывести окно из полноэкранного режима Показывать окно на всех рабочих местах Показывать окно только на текущем рабочем месте Развернуть окно горизонтально Развернуть на весь экран Развернуть окно вертикально _Свернуть _Свернуть все Свернуть окно Переместить видимую область текущего рабочего места к горизонтальной координате X Переместить видимую область текущего рабочего места к вертикальной координате Y Переместить окно на рабочее место с номером ЧИСЛО (первое рабочее место имеет номер 0) Переместить на другое _рабочее место _Переместить на рабочее место справа Переместить на рабочее место _ниже Переместить на рабочее место с_лева Переместить на рабочее место _выше ИМЯ НОМЕР НОМЕР экрана для изучения или правки НОМЕР рабочего места для изучения или правки Название: %s
 Нет открытых окон Число окон: %d
 Число рабочих мест: %d
 На экране: %d (менеджер окон: %s)
 На рабочем месте: %s
 Возможности по перечислению окон или рабочих мест Возможности по изменению свойств экрана Возможности по изменению свойств окна Возможности по изменению свойств рабочего места PID: %s
 Положение в раскладке рабочих мест (строка, столбец): %d, %d
 Возможные действия: %s
 Вывести или изменить свойства экрана/рабочего места/окна, следуя спецификации EWMH.
Подробнее о спецификации можно узнать по адресу:
	http://freedesktop.org/wiki/Specifications/wm-spec Ресурс класса: %s
 Правый сосед: %s
 Номер экрана: %d
 Идентификатор сеанса: %s
 Свернуть окно в полоску Показать возможности по перечислению окон или рабочих мест Показать возможности по изменению свойств экрана Показать возможности по изменению свойств окна Показать возможности по изменению свойств рабочего места Показать рабочий стол Отображение рабочего стола: %s
 Начать перемещение окна с помощью клавиатуры Начать изменение размера окна с помощью клавиатуры Идентификатор запуска: %s
 Состояние: %s
 Прекратить показ рабочего стола ТИП Средство для переключения между видимыми окнами Средство для переключения между окнами Средство для переключения между рабочими местами Сосед сверху: %s
 Расположено над: %lu
 _Развернуть все Восстановить _прежний размер Восстановить горизонтальный размер окна Восстановить прежний размер Восстановить вертикальный размер окна _Развернуть Развернуть окно Развернуть окно из полоски Безымянное приложение Безымянное окно Не удалось переместить видимую область: текущее рабочее место не имеет видимых областей
 Не удалось переместить видимую область: нет текущего рабочего места
 Положение видимой области (x, y): %s
 ШИРИНА Список окон Менеджер окон: %s
 Переключатель окон Тип окна: %s
 Не удалось переместить окно на рабочее место %d: рабочее место не существует
 Рабочее место %d Рабочее место %s%d Рабочее место 1_0 Расположение рабочих мест (строк, столбцов, направление): %d, %d, %s
 Название рабочего места: %s
 Номер рабочего места: %d
 Переключатель рабочих мест X Идентификатор главного окна приложения, которое надо изучить, в рамках X-сервера Идентификатор окна, которое надо изучить или изменить, в рамках X-сервера XID XID: %lu
 Y _Всегда на видимом рабочем месте _Закрыть _Закрыть все Пере_местить _Только на этом рабочем месте _Изменить размер Восстановить размер у вс_ех над все рабочие места под изменить полноэкранный режим изменить рабочее место закрыть рабочий стол окно диалога плавающий элемент или панель нет полный экран поместить над поместить под развернуть на весь экран развернуть горизонтально развернуть вертикально развёрнуто на весь экран развёрнуто горизонтально развёрнуто вертикально свернуть свёрнуто переместить требует внимания невозможны никакие действия обычное обычное окно прикрепить приколото изменить размер установлено свернуть в полоску свёрнуто в полоску пропущено в переключателе рабочих мест пропущено в списке окон экран приветствия не задан приклеить приклеено отсоединяемое меню отсоединяемая панель инструментов да отменить положение над отменить положение под восстановить прежний размер восстановить горизонтальный размер восстановить вертикальный размер развернуть открепить развернуть из полоски отклеить вспомогательное окно не задано не задано 