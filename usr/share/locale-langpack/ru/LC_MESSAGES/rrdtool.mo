��          �   %   �      0  '  1  A   Y  @   �  D   �  =   !  :   _  l   �          !  ,   ;     h  %   �  ,   �  -   �        &   (     O     o  t   �  �     �   �  �   7	  0   �	  �  	
  d  �  w     r   �  q   �  Q   e  H   �  �      4   �  4     Q   F  W   �  K   �  O   <  Y   �  Z   �  N   A  3   �  3   �  �   �  1  �  �     B  �  i   B                          
                                                                      	                               		[CDEF:vname=rpn-expression]
		[VDEF:vdefname=rpn-expression]
		[PRINT:vdefname:format]
		[GPRINT:vdefname:format]
		[COMMENT:text]
		[SHIFT:vname:offset]
		[TEXTALIGN:{left|right|justified|center}]
		[TICK:vname#rrggbb[aa][:[fraction][:legend]]]
		[HRULE:value#rrggbb[aa][:legend]]
		[VRULE:value#rrggbb[aa][:legend]]
		[LINE[width]:vname[#rrggbb[aa][:[legend][:STACK]]]]
		[AREA:vname[#rrggbb[aa][:[legend][:STACK]]]]
		[PRINT:vname:CF:format] (deprecated)
		[GPRINT:vname:CF:format] (deprecated)
		[STACK:vname[#rrggbb[aa][:legend]]] (deprecated)
  * cd - changes the current directory

	rrdtool cd new directory
  * ls - lists all *.rrd files in current directory

	rrdtool ls
  * mkdir - creates a new directory

	rrdtool mkdir newdirectoryname
  * pwd - returns the current working directory

	rrdtool pwd
  * quit - closing a session in remote mode

	rrdtool quit
  * resize - alter the length of one of the RRAs in an RRD

	rrdtool resize filename rranum GROW|SHRINK rows
 %s: illegal option -- %c
 %s: invalid option -- %c
 %s: option `%c%s' doesn't allow an argument
 %s: option `%s' is ambiguous
 %s: option `%s' requires an argument
 %s: option `--%s' doesn't allow an argument
 %s: option `-W %s' doesn't allow an argument
 %s: option `-W %s' is ambiguous
 %s: option requires an argument -- %c
 %s: unrecognized option `%c%s'
 %s: unrecognized option `--%s'
 * graph - generate a graph from one or several RRD

	rrdtool graph filename [-s|--start seconds] [-e|--end seconds]
 * graphv - generate a graph from one or several RRD
           with meta data printed before the graph

	rrdtool graphv filename [-s|--start seconds] [-e|--end seconds]
 * restore - restore an RRD file from its XML form

	rrdtool restore [--range-check|-r] [--force-overwrite|-f] filename.xml filename.rrd
 RRDtool is distributed under the Terms of the GNU General
Public License Version 2. (www.gnu.org/copyleft/gpl.html)

For more information read the RRD manpages
 Valid remote commands: quit, ls, cd, mkdir, pwd
 Project-Id-Version: rrdtool
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-04-06 16:14+0000
PO-Revision-Date: 2014-07-10 06:59+0000
Last-Translator: ☠Jay ZDLin☠ <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:05+0000
X-Generator: Launchpad (build 18115)
 		[CDEF:vname=rpn-expression]
		[VDEF:vdefname=rpn-expression]
		[PRINT:vdefname:format]
		[GPRINT:vdefname:format]
		[COMMENT:text]
		[SHIFT:vname:offset]
		[TEXTALIGN:{left|right|justified|center}]
		[TICK:vname#rrggbb[aa][:[fraction][:legend]]]
		[HRULE:value#rrggbb[aa][:legend]]
		[VRULE:value#rrggbb[aa][:legend]]
		[LINE[width]:vname[#rrggbb[aa][:[legend][:STACK]]]]
		[AREA:vname[#rrggbb[aa][:[legend][:STACK]]]]
		[PRINT:vname:CF:format] (не рекомендуется)
		[GPRINT:vname:CF:format] (не рекомедуется)
		[STACK:vname[#rrggbb[aa][:legend]]] (не рекомендуется)
  * cd - сменить текущую директорию

	rrdtool cd имя_следующей_директории
  * ls - вывести имена всех файлов *.rrd в текущей директории

	rrdtool ls
  * mkdir - создать новую директорию

	rrdtool mkdir имя_новой_директории
  * pwd - выводит имя текущей директории

	rrdtool pwd
  * quit - закрыть удаленную сессию

	rrdtool quit
  * resize - изменяет размер одного из архивов (RRAs)
	в файле циркулятора (RRD)

	rrdtool resize имя_файла номер_архива GROW|SHRINK число_строк
 %s: недопустимый параметр -- %c
 %s: недопустимый параметр -- %c
 %s: параметр `%c%s' используется без аргументов
 %s: параметру `%s' задано неопределенное значение
 %s: параметр `%s' требует указания значения
 %s: парметр `--%s' используется без аргументов
 %s: параметр `-W %s' не допускает указания аргумента
 %s: параметру `-W %s' задано неопределенное значение
 %s: параметр требует указания аргумента -- %c
 %s: неизвестный параметр `%c%s'
 %s: неизвестный параметр `--%s'
 * graph - сгенерировать график на основе одного
	 или нескольких циркуляторов (RRD)

	rrdtool graph имя_файла_графика [-s|--start seconds] [-e|--end seconds]
 * graphv - сгенерировать график на основе одного
	 или нескольких циркуляторов (RRD)
	 с предварительной печатью метаданных

	rrdtool graphv имя_файла_графика [-s|--start seconds] [-e|--end seconds]
 * restore - восстановить файл циркулятора (RRD) из фала формата XML

	 rrdtool restore [--range-check|-r] [--force-overwrite|-f] имя_файла.xml имя_файла_циркулятора.rrd
 RRDtool распространяется под Стандартной Общественной
Лицензией GNU GPL Version 2. (www.gnu.org/copyleft/gpl.html)

Для получения более подробной информации
обратитесь к страницам руководства man rrdtool
 Допустимые команды для удаленного доступа:
quit, ls, cd, mkdir, pwd
 