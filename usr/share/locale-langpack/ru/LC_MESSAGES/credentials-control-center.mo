��          �   %   �      P      Q     r     �  #   �  =   �  @   �     5     B     O     \     l  '   �     �  #   �  1   �  2        A  F   P  $   �  "   �  [   �  :   ;  X   v  S   �  '   #  �  K  J      /   K     {  (   �  u   �  v   6	  #   �	  %   �	  %   �	  *   
  ?   H
  U   �
     �
  ?   �
  |   1     �  *   .  �   Y  T   �  O   M  �   �  g   Z  �   �  �   a  c   �                                                                                                    	               
           - Edit Web credentials settings Add account… All applications Another instance is already running Are you sure that you wish to remove this Ubuntu Web Account? Control whether this application integrates with Online Accounts Edit Options Grant access Legal notice Online Accounts Online Accounts preferences Online account credentials and settings Options Output version information and exit Please authorize Ubuntu to access your %s account Please authorize Ubuntu to access your %s account: Remove Account Run '%s --help' to see a full list of available command line options.
 Select to configure a new %s account Show accounts that integrate with: The Web Account which manages the integration of %s with your applications will be removed. The following applications integrate with your %s account: There are currently no account providers available which integrate with this application There are currently no applications installed which integrate with your %s account. Your online %s account is not affected. Project-Id-Version: gnome-control-center-signon
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-01 12:56+0000
PO-Revision-Date: 2013-09-28 06:22+0000
Last-Translator: Roman Mindlin <mindline@mail.ru>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:14+0000
X-Generator: Launchpad (build 18115)
  - Изменить параметры сетевых реквизитов Добавить учетную запись... Все приложения Запущена другая копия Вы действительно хотите удалить эту сетевую учётную запись Ubuntu? Управление интеграцией приложения с сетевыми учетными записями Изменить параметры Предоставить доступ Правовая информация Сетевые учётные записи Параметры сетевых учётных записей Реквизиты сетевых учётных записей и параметры Параметры Показать сведения о версии и выйти Пожалуйста авторизуйтесь в Ubuntu для доступа к вашей учетной записи %s Пожалуйста, авторизуйтесь в Ubuntu для доступа к вашей учетной записи %s : Удалить учётную запись Выполните '%s --help' для просмотра полного списка доступных параметров командной строки.
 Выберите для настройки новой %s учётной записи Показать учётные записи интегрированные с: Сетевая учётная запись, которая позволяет настроить интеграцию %s с вашими приложениями будет удалена. Следующие приложения отнесены к вашей учётной записи %s : Отсутствуют поставщики учетных записей, подходящих для интеграции с этим приложением В настоящее время нет приложений относящихся к вашей учётной записи %s . Изменения не коснутся вашей сетевой учётной записи %s . 