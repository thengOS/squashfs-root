��    !      $  /   ,      �  $   �       /   *  g   Z     �     �  *   �       M        j     �  (   �     �     �     �     �     �        *   ,  �   W  ;   �       0   2  <   c  r   �  2        F     b  "   x     �     �  #   �  �  �  ,   �	  0   �	  N   
  �   h
  '        3  r   L  (   �  �   �  )   �     �  F   �  <        A  "   a     �  .   �  Y   �  J   "  �   m  o   =  2   �  c   �  e   D  �   �  I   W  0   �     �  3   �  %   &  A   L  :   �     !                                                                   	                                
                                                       %s: Version %s has already been seen %s: will be newly installed --since=<version> expects a only path to a .deb <big><b>Changelogs</b></big>

The following changes are found in the packages you are about to install: Aborting Changes for %s Confirmation failed, don't save seen state Continue Installation? Could not run apt-changelog (%s), unable to retrieve changelog for package %s Do you want to continue? [Y/n]  Done Ignoring `%s' (seems to be a directory!) Informational notes List the changes Mailing %s: %s News for %s Reading changelogs Reading changelogs. Please wait. The %s frontend is deprecated, using pager The gtk frontend needs a working python-gtk2 and python-glade2.
Those imports can not be found. Falling back to pager.
The error is: %s
 The mail frontend needs a installed 'sendmail', using pager Unknown frontend: %s
 Unknown option %s for --which.  Allowed are: %s. Usage: apt-listchanges [options] {--apt | filename.deb ...}
 Wrong or missing VERSION from apt pipeline
(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version set to 2?)
 You can abort the installation if you select 'no'. apt-listchanges: Changelogs apt-listchanges: News apt-listchanges: changelogs for %s apt-listchanges: news for %s database %s failed to load.
 didn't find any valid .deb archives Project-Id-Version: apt-listchanges NEW
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-17 14:04+0000
PO-Revision-Date: 2016-02-17 22:07+0000
Last-Translator: Sergey Alyoshin <alyoshin.s@gmail.com>
Language-Team: Russian <debian-l10n-russian@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:57+0000
X-Generator: Launchpad (build 18115)
Language: ru
 %s: Version %s уже встречалась %s: будет установлен заново --since=<версия> ожидает только путь к файлу .deb <big><b>Список изменений</b></big>

В устанавливаемых пакетах были найдены следующие изменения: Аварийное завершение Изменения в %s Не удалось подтвердить, состояние обработки записано не будет Продолжить установку? Не удалось запустить  apt-changelog (%s), не удаётся получить журнал изменений для пакета %s Хотите продолжить? [Y/n]  Выполнено Игнорируется `%s' (кажется, это каталог!) Замечания справочного характера Список изменений Отправка почты %s: %s Новости о %s Чтение журнала изменений Чтение журнала изменений. Пожалуйста, подождите. Просмотрщик %s исключён, используется pager Для просмотрщика GTK требуется рабочий python-gtk2 и python-glade2.
Эти библиотеки не найдены. Будет использован pager.
Ошибка: %s
 Для просмотрщика mail нужно установить 'sendmail', используется pager Неизвестный просмотрщик: %s
 Неизвестное значение %s для --which. Возможные значения: %s. Использование: apt-listchanges [параметры] {--apt | имя_файла.deb ...}
 Неправильное или отсутствует VERSION в выводе apt
(значение Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version равно 2?)
 Вы можете прервать установку, выбрав 'no'. apt-listchanges: журнал изменений apt-listchanges: новости apt-listchanges: журнал изменений %s apt-listchanges: новости о %s не удалось загрузить базу данных %s.
 допустимые .deb архивы не найдены 