��            )   �      �     �     �  #   �  #   �  #     #   1  #   U     y  "   �  &   �  $   �               +     A     X  $   p  &   �     �  -   �     	  ^   )     �  "   �  .   �     �  N         O  	  l  @   v  #   �  ?   �  E   	  F   a	  E   �	  F   �	  %   5
  M   [
  @   �
  <   �
     '  C   4  8   x  '   �  -   �  W     m   _  L   �  m     4   �  �   �     {  @   �     �  "   J  �   m  1                                                                                                           	             
                     Certificate has no private key Connection is closed Could not create TLS connection: %s Could not parse DER certificate: %s Could not parse DER private key: %s Could not parse PEM certificate: %s Could not parse PEM private key: %s Error performing TLS close: %s Error performing TLS handshake: %s Error reading data from TLS socket: %s Error writing data to TLS socket: %s Module No certificate data provided Operation would block PKCS#11 Module Pointer PKCS#11 Slot Identifier Peer failed to perform TLS handshake Peer requested illegal TLS rehandshake Proxy resolver internal error. Server did not return a valid TLS certificate Server required TLS certificate Several PIN attempts have been incorrect, and the token will be locked after further failures. Slot ID TLS connection closed unexpectedly TLS connection peer did not send a certificate The PIN entered is incorrect. This is the last chance to enter the PIN correctly before the token is locked. Unacceptable TLS certificate Project-Id-Version: glib-networking master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=glib&keywords=I18N+L10N&component=network
POT-Creation-Date: 2016-05-19 08:00+0000
PO-Revision-Date: 2013-04-16 20:36+0000
Last-Translator: Dmitriy S. Seregin <Unknown>
Language-Team: русский <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:01+0000
X-Generator: Launchpad (build 18115)
Language: ru
 У сертификата нет секретного ключа Соединение закрыто Не удалось создать соединение TLS: %s Не удалось обработать сертификат DER: %s Не удалось обработать личный ключ DER: %s Не удалось обработать сертификат PER: %s Не удалось обработать личный ключ PEM: %s Ошибка закрытия TLS: %s Ошибка выполнения квитирования связи TLS: %s Ошибка чтения данных из сокета TLS: %s Ошибка записи данных в сокет TLS: %s Модуль Данные сертификата не предоставлены Действие будет заблокировано Указатель модуля PKCS#11 Идентификатор слота PKCS#11 Узлу не удалось квитировать выполнение связи TLS Узел запросил недопустимое повторное квитирование связи TLS Внутренняя ошибка распознавателя прокси. Сертификат TLS, возвращённый сервером, не является подлинным Сервер требует сертификат TLS PIN был несколько раз введён неправильно, токен будет заблокирован после последующих неудачных попыток. ID слота Соединение TLS неожиданно закрылось Узел, с которым производится TLS-соединение, не предоставил сертификат Введён неверный PIN. Это — последняя возможность ввести корректный PIN перед тем, как токен будет заблокирован. Недопустимый сертификат TLS 