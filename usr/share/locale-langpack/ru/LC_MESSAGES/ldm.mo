��          �      \      �     �     �                     '  &   6     ]     f     w     �  (   �  ,   �     �  	          !   &     H     U  �  ]  G         h          �  %   �     �  @   �     4     A     Z     u  I   �  I   �  /   &     V     l  K   �     �     �                                  
                     	                                            Automatic login in %d seconds Change _Language Change _Session Default Failsafe xterm Login as Guest No response from server, restarting... Password Select _Host ... Select _Language ... Select _Session ... Select the host for your session to use: Select the language for your session to use: Select your session manager: Shut_down Username Verifying password.  Please wait. _Preferences _Reboot Project-Id-Version: ldm 2.2.1
Report-Msgid-Bugs-To: sbalneav@ltsp.org
POT-Creation-Date: 2013-11-24 08:57+0200
PO-Revision-Date: 2011-08-18 21:46+0000
Last-Translator: Yuri Kozlov <yuray@komyakino.ru>
Language-Team: Russian <debian-l10n-russian@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:26+0000
X-Generator: Launchpad (build 18115)
Language: ru
 Автоматический вход в течении %d секунд Смена _Языка Смена _Сеанса По умолчанию Безопасный сеанс xterm Войти как Гость Нет ответа от сервера, перезапуск... Пароль Выбор _узла ... Выбор _Языка ... Выбор _Сеанса ... Выберите узел для используемого сеанса: Выберите язык для используемого сеанса: Выберите менеджер сеанса: _Выключение Имя пользователя Выполняется проверка пароля, подождите... П_араметры _Перезагрузка 