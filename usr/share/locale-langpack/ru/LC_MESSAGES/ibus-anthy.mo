��            )   �      �     �     �     �  	   �     �     �     �                      $     2     D     M     R  	   [     e  	   k     u  )   }     �     �     �  2   �     �  
                   0  �  C     �  6        G     [     w     �     �  #   �     �     �  2         3     S     d     m     ~     �     �     �  F   �          "  ,   B  K   o     �  '   �     �     	  �   '	                                                          
                                                              	                 <b>Behavior</b> <b>Initial Setting</b> <b>Other</b> Add words Co_ntrol Command Configure Anthy Configure dictionaries De_fault Dic Edit Shortcut Edit dictionaries Hiragana Kana Katakana Key Code: Latin Modifier: Notice! Please press a key (or a key combination) Romaji Shortcut Switch input mode The dialog will be closed when the key is released Thumb shift Wide Latin _Input Mode: _Typing Method: translator_credits Project-Id-Version: ibus-anthy
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-08-21 18:57+0900
PO-Revision-Date: 2010-10-20 11:36+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:38+0000
X-Generator: Launchpad (build 18115)
 <b>Поведение</b> <b>Первоначальная настройка</b> <b>Прочее</b> Добавить слова Уп_равление Команда Настройка Anthy Настройка словарей По умолчанию Словарь Изменить комбинацию клавиш Изменить словари Хирагана Кана Катакана Код клавиши: Латынь Модификатор: Внимание! Нажмите клавишу (или сочетание клавиш) Ромадзи Сочетание клавиш Переключить режим ввода При отпускании клавиши, диалог закроется Thumb shift Расширенная латиница _Режим ввода: _Метод печатания: Launchpad Contributions:
  Mikhail https://launchpad.net/~stalker2011x
  Stanislav Hanzhin https://launchpad.net/~hanzhin-stas
  Yuri Efremov https://launchpad.net/~yur.arh 