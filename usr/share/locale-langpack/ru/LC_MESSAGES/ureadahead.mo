��          �   %   �      P     Q     k     �     �      �     �     �          $     ?     Z     y     �  1  �     �     �     �  "     4   :     o      v     �  6   �  (   �  1     �  D  *   �  :     H   B      �  K   �  L   �      E	  0   f	  8   �	  K   �	  ;   
  7   X
  4   �
  M  �
  1     .   E  ?   t  Q   �  m     
   t  K     =   �  u   	  p     u   �                                             	                       
                                                         %s: illegal argument: %s
 Error mapping into memory Error retrieving chunk extents Error retrieving file stat Error retrieving page cache info Error unmapping from memory Error while reading Error while tracing Failed to set CPU priority Failed to set I/O priority File vanished or error reading Ignored far too long path Ignored relative path PATH should be the location of a mounted filesystem for which files should be read.  If not given, the root filesystem is assumed.

If PATH is not given, and no readahead information exists for the root filesystem (or it is old), tracing is performed instead to generate the information for the next boot. Pack data error Pack too old Read required files in advance Unable to determine pack file name Unable to obtain rotationalness for device %u:%u: %s [PATH] detach and run in the background dump the current pack file how to sort the pack file when dumping [default: path] ignore existing pack and force retracing maximum time to trace [default: until terminated] Project-Id-Version: ureadahead
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-29 15:05+0000
PO-Revision-Date: 2010-03-19 08:05+0000
Last-Translator: Dark Raven <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:47+0000
X-Generator: Launchpad (build 18115)
 %s: неверный аргумент: %s
 Ошибка при отображении в память Ошибка получения сведений о фрагментах Ошибка функции stat Ошибка получения сведений о кэше страниц Ошибка освобождения отображения в память Ошибка при чтении Ошибка при профилировании Не удалось задать приоритет ЦП Не удалось задать приоритет ввода-вывода Файл исчез или ошибка при чтении Слишком длинный путь пропущен Относительный путь пропущен ПУТЬ должен указывать на смонтированную файловую систему из которой следует считывать файлы. Если не задан,  используется корневая файловая система.

Если ПУТЬ не задан, и отсутствуют результаты readahead для корневой ФС (или они устарели), то выполняется профилирование при следующей загрузке для получения этих результатов. Ошибка в файле результатов Файл результатов устарел Заранее считывает требуемые файлы Не удалось определить имя файла результатов Невозможно получить скорость вращения для устройства %u:%u: %s [ПУТЬ] отсоединить и запустить в фоновом режиме вывести текущий файл результатов сортировка файла результатов при выводе [по умолчанию: path (путь)] не использовать файл результатов и повторить профилирование максимальное время профилирования [по умолчанию: до завершения] 