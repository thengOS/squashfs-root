��    s      �  �   L      �	  ]   �	     
  �   &
    �
          (  #   H  .   l  2   �  )   �  %   �       #   4  �   X  '   3  )   [  8   �     �     �     �  =     ,   I  )   v     �  !   �     �     �       =   %  �   c       "   '  '   J  ?   r  G   �  G   �  ?   B  P   �  H   �        3   =  -   q  "   �     �  1   �  6        K  *   ^     �     �     �     �     �  '     &   )  &   P  7   w     �  $   �     �  1   �  B   .     q     �      �     �  B   �  :   /  A   j     �     �  /   �           :  '   [  @   �  .   �  <   �  8   0     i  '   �  ?   �     �  +     -   ;  -   i     �  �  �     �  (   �  J   �  ?   :  7   z  =   �  Y   �  )   J  6   t  %   �  %   �  &   �  ,        K  �  `  �   *#     �#     �#  2   �#  -    $     N$     j$     �$  0   �$  !   �$  (   �$  �  #%  �   �&     �'  �  �'  �  |)  8   5+  ?   n+  C   �+  b   �+  f   U,  s   �,  W   0-  ;   �-  R   �-  	  .  ]   !/  f   /  �   �/  L   p0  #   �0  4   �0  =   1  T   T1  S   �1  @   �1  >   >2     }2  U   �2  c   �2  �   F3  �   �3  !   �4  <   �4  R   5  T   r5  \   �5  \   $6  T   �6  g   �6  _   >7  H   �7  �   �7  }   w8  @   �8  J   69  �   �9  �   :  "   �:  O   �:     ;  -   .;  "   \;  +   ;  7   �;  P   �;  E   4<  E   z<  z   �<  ,   ;=  Q   h=     �=  x   �=  V   G>  "   �>  (   �>  ,   �>  (   ?  �   @?  {   �?  v   O@  1   �@  /   �@  a   (A  %   �A  )   �A  o   �A  k   JB  s   �B  �   *C  i   �C  =   D  r   UD  �   �D  ;   RE  Z   �E  X   �E  m   BF  3   �F  �  �F  ,   �H  O   I  �   ^I  �   �I  e   ~J  k   �J  �   PK  U   �K  m   UL  g   �L  W   +M  m   �M  m   �M     _N    yN  �   �U     9V  E   SV  n   �V  k   W  =   tW  C   �W  M   �W  M   DX  B   �X  H   �X     ;          `       W   <   :       ^                      .       *      (      X       6      i       G           K   7      9   S      R   P   b   %   H       T   #   0       k          5       F   @       O       ,   +   s       U      J   N           o   )   ?   &   C   Q   A              	                  Y      _      2       B      '           4              \       D      h   3   V   E      8       >          
       e   1   g   ]   Z   r   -   =   c   I       /       "       l   d   n          !       $      L   p       a           M               f   j         q   m      [       %s failed with return code 15, shadow not enabled, password aging cannot be set. Continuing.
 %s: %s %s: Please enter a username matching the regular expression configured
via the NAME_REGEX[_SYSTEM] configuration variable.  Use the `--force-badname'
option to relax this check or reconfigure NAME_REGEX.
 %s: To avoid problems, the username should consist only of
letters, digits, underscores, periods, at signs and dashes, and not start with
a dash (as defined by IEEE Std 1003.1-2001). For compatibility with Samba
machine accounts $ is also supported at the end of the username
 Adding group `%s' (GID %d) ...
 Adding new group `%s' (%d) ...
 Adding new group `%s' (GID %d) ...
 Adding new user `%s' (%d) with group `%s' ...
 Adding new user `%s' (UID %d) with group `%s' ...
 Adding new user `%s' to extra groups ...
 Adding system user `%s' (UID %d) ...
 Adding user `%s' ...
 Adding user `%s' to group `%s' ...
 Adds a user or group to the system.
  
Copyright (C) 1997, 1998, 1999 Guy Maor <maor@debian.org>
Copyright (C) 1995 Ian Murdock <imurdock@gnu.ai.mit.edu>,
                   Ted Hajek <tedhajek@boombox.micro.umn.edu>

 Allowing use of questionable username.
 Backing up files to be removed to %s ...
 Cannot deal with %s.
It is not a dir, file, or symlink.
 Cannot handle special file %s
 Caught a SIG%s.
 Copying files from `%s' ...
 Copyright (C) 2000 Roland Bauerschmidt <roland@copyleft.de>

 Could not find program named `%s' in $PATH.
 Couldn't create home directory `%s': %s.
 Couldn't parse `%s', line %d.
 Creating home directory `%s' ...
 Done.
 Enter a group name to remove:  Enter a user name to remove:  If you really want this, call deluser with parameter --force
 In order to use the --remove-home, --remove-all-files, and --backup features,
you need to install the `perl-modules' package. To accomplish that, run
apt-get install perl-modules.
 Internal error Is the information correct? [Y/n]  Looking for files to backup/remove ...
 No GID is available in the range %d-%d (FIRST_GID - LAST_GID).
 No GID is available in the range %d-%d (FIRST_SYS_GID - LAST_SYS_GID).
 No UID is available in the range %d-%d (FIRST_SYS_UID - LAST_SYS_UID).
 No UID is available in the range %d-%d (FIRST_UID - LAST_UID).
 No UID/GID pair is available in the range %d-%d (FIRST_SYS_UID - LAST_SYS_UID).
 No UID/GID pair is available in the range %d-%d (FIRST_UID - LAST_UID).
 No options allowed after names.
 Not backing up/removing `%s', it is a mount point.
 Not backing up/removing `%s', it matches %s.
 Not creating home directory `%s'.
 Only one or two names allowed.
 Only root may add a user or group to the system.
 Only root may remove a user or group from the system.
 Permission denied
 Removes users and groups from the system.
 Removing crontab ...
 Removing directory `%s' ...
 Removing files ...
 Removing group `%s' ...
 Removing user `%s' ...
 Removing user `%s' from group `%s' ...
 Selecting GID from range %d to %d ...
 Selecting UID from range %d to %d ...
 Setting quota for user `%s' to values of user `%s' ...
 Setting up encryption ...
 Specify only one name in this mode.
 Stopped: %s
 Stopping now without having performed any action
 The --group, --ingroup, and --gid options are mutually exclusive.
 The GID %d does not exist.
 The GID %d is already in use.
 The GID `%s' is already in use.
 The UID %d is already in use.
 The group `%s' already exists and is not a system group. Exiting.
 The group `%s' already exists as a system group. Exiting.
 The group `%s' already exists, but has a different GID. Exiting.
 The group `%s' already exists.
 The group `%s' does not exist.
 The group `%s' is not a system group. Exiting.
 The group `%s' is not empty!
 The group `%s' was not created.
 The home dir must be an absolute path.
 The home directory `%s' already exists.  Not copying from `%s'.
 The system user `%s' already exists. Exiting.
 The user `%s' already exists with a different UID. Exiting.
 The user `%s' already exists, and is not a system user.
 The user `%s' already exists.
 The user `%s' already exists. Exiting.
 The user `%s' does not exist, but --system was given. Exiting.
 The user `%s' does not exist.
 The user `%s' is already a member of `%s'.
 The user `%s' is not a member of group `%s'.
 The user `%s' is not a system user. Exiting.
 The user `%s' was not created.
 This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License, /usr/share/common-licenses/GPL, for more details.
 Try again? [y/N]  Unknown variable `%s' at `%s', line %d.
 Usually this is never required as it may render the whole system unusable
 WARNING: You are just about to delete the root account (uid 0)
 Warning: The home dir %s you specified already exists.
 Warning: The home dir %s you specified can't be accessed: %s
 Warning: The home directory `%s' does not belong to the user you are currently creating.
 Warning: group `%s' has no more members.
 You may not remove the user from their primary group.
 `%s' does not exist. Using defaults.
 `%s' exited from signal %d. Exiting.
 `%s' returned error code %d. Exiting.
 `%s' still has `%s' as their primary group!
 adduser version %s

 deluser USER
  remove a normal user from the system
  example: deluser mike

  --remove-home             remove the users home directory and mail spool
  --remove-all-files        remove all files owned by user
  --backup                  backup files before removing.
  --backup-to <DIR>         target directory for the backups.
                            Default is the current directory.
  --system                  only remove if system user

delgroup GROUP
deluser --group GROUP
  remove a group from the system
  example: deluser --group students

  --system                  only remove if system group
  --only-if-empty           only remove if no members left

deluser USER GROUP
  remove the user from a group
  example: deluser mike students

general options:
  --quiet | -q      don't give process information to stdout
  --help | -h       usage message
  --version | -v    version number and copyright
  --conf | -c FILE  use FILE as configuration file

 deluser is based on adduser by Guy Maor <maor@debian.org>, Ian Murdock
<imurdock@gnu.ai.mit.edu> and Ted Hajek <tedhajek@boombox.micro.umn.edu>

 deluser version %s

 fork for `find' failed: %s
 fork for `mount' to parse mount points failed: %s
 getgrnam `%s' failed. This shouldn't happen.
 invalid argument to option
 invalid combination of options
 passwd file busy, try again
 pipe of command `mount' could not be closed: %s
 unexpected failure, nothing done
 unexpected failure, passwd file missing
 Project-Id-Version: adduser 3.112+nmu2
Report-Msgid-Bugs-To: adduser-devel@lists.alioth.debian.org
POT-Creation-Date: 2015-07-02 22:08+0200
PO-Revision-Date: 2013-10-14 06:33+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <debian-l10n-russian@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:56+0000
X-Generator: Launchpad (build 18115)
Language: ru
 %s не выполнен (вернул код ошибки 15), теневые пароли не включены, устаревание пароля не может быть задано. Продолжение работы.
 %s: %s %s: Пожалуйста, введите имя пользователя соответствующее регулярному 
выражению, настроенному через переменную NAME_REGEX[_SYSTEM].  
Используйте параметр `--force-badname', чтобы нивелировать  
эту проверку или повторно настроить NAME_REGEX.
 %s: Чтобы не было проблем, имя пользователя должно состоять только из
букв, цифр, подчёркиваний, точек, тире, знака @ и не начинаться
с тире (так определено в IEEE Std 1003.1-2001). Для совместимости с Samba
также можно указывать $ в конце имени пользователя
 Добавляется группа «%s» (GID %d) ...
 Добавляется новая группа «%s» (%d) ...
 Добавляется новая группа «%s» (GID %d) ...
 Добавляется новый пользователь «%s» (%d) в группу «%s» ...
 Добавляется новый пользователь «%s» (UID %d) в группу «%s» ...
 Добавляется новый пользователь «%s» в дополнительные группы ...
 Добавляется системный пользователь «%s» (UID %d) ...
 Добавляется пользователь «%s» ...
 Добавляется пользователь «%s» в группу «%s» ...
 Добавляет пользователя или группу в систему.
..
Copyright (C) 1997, 1998, 1999 Guy Maor <maor@debian.org>
Copyright (C) 1995 Ian Murdock <imurdock@gnu.ai.mit.edu>,
                   Ted Hajek <tedhajek@boombox.micro.umn.edu>

 Разрешить использование не везде корректных имён.
 Создание резервных копий файлов перед удалением из %s ...
 Непонятно что делать с %s.
Это не каталог, не файл и не символическая ссылка.
 Не удалось обработать специальный файл %s
 Получен сигнал SIG%s.
 Копирование файлов из «%s» ...
 Copyright (C) 2000 Roland Bauerschmidt <roland@copyleft.de>

 Невозможно найти программу с именем «%s» в $PATH.
 Не удалось создать домашний каталог «%s»: «%s».
 Не удалось разобрать «%s», строка %d.
 Создаётся домашний каталог «%s» ...
 Готово.
 Введите имя группы, которую вы хотите удалить:  Введите имя пользователя, которого вы хотите удалить:  Если вы действительно хотите это сделать, запустите deluser с параметром --force
 Чтобы использовать параметры --remove-home, --remove-all-files и --backup
вам требуется установить пакет perl-modules. Для этого выполните:
apt-get install perl-modules
 Внутренняя ошибка Данная информация корректна? [Y/n]  Идёт поиск файлов для сохранения/удаления ...
 Нет свободного GID в диапазоне %d-%d (FIRST_GID - LAST_GID).
 Нет свободного GID в диапазоне %d-%d (FIRST_SYS_GID - LAST_SYS_GID).
 Нет свободного UID в диапазоне %d-%d (FIRST_SYS_UID - LAST_SYS_UID).
 Нет свободного UID в диапазоне %d-%d (FIRST_UID - LAST_UID).
 Нет свободной UID/GID пары в диапазоне %d-%d (FIRST_SYS_UID - LAST_SYS_UID).
 Нет свободной UID/GID пары в диапазоне %d-%d (FIRST_UID - LAST_UID).
 Нельзя указывать параметры после имён.
 Нельзя выполнить резервное копирование/удаление «%s», это точка монтирования.
 Нельзя выполнить резервное копирование/удаление «%s», совпадает с %s.
 Не создаётся домашний каталог «%s».
 Можно указать только одно или два имени.
 Только суперпользователь может добавить пользователя или группу в систему.
 Только суперпользователь может удалить пользователя или группу из системы.
 Недостаточно прав
 Удаление пользователей и групп из системы.
 Удаляется crontab ...
 Удаляется каталог «%s» ...
 Удаляются файлы ...
 Удаляется группа «%s» ...
 Удаляется пользователь «%s» ...
 Удаляется пользователь «%s» из группы «%s» ...
 Выбирается GID из диапазона от %d по %d ...
 Выбирается UID из диапазона от %d по %d ...
 Настраиваются квоты для пользователя «%s» как у пользователя «%s»...
 Настройка шифрования ...
 Только одно имя можно указать в этом режиме.
 Останов: %s
 Сейчас процедура завершается без выполнения каких-либо действий
 Параметры --group, --ingroup и --gid взаимно исключаемые.
 GID %d не существует.
 GID %d уже используется.
 GID «%s» уже используется.
 UID %d уже используется.
 Группа `%s' уже существует и не является системной. Завершение работы приложения
 Группа «%s» уже существует и является системной. Завершение работы.
 Группа «%s» уже существует, но имеет другой GID. Завершение работы.
 Группа «%s» уже существует.
 Группа «%s» не существует.
 Группа «%s» не является системной. Завершение работы.
 Группа «%s» не пуста!
 Группа «%s» не создана.
 Домашний каталог должен задаваться в виде абсолютного пути.
 Домашний каталог «%s» уже существует. Не копируется из «%s».
 Системный пользователь «%s» уже существует. Завершение работы.
 Пользователь «%s» уже существует, но имеет другой UID. Завершение работы.
 Пользователь «%s» уже существует и не является системным.
 Пользователь «%s» уже существует.
 Пользователь `%s' уже существует. Завершение работы приложения
 Пользователь «%s» не существует, но задан параметр --system. Завершение работы.
 Пользователь «%s» не существует.
 Пользователь «%s» уже является членом группы «%s».
 Пользователь «%s» не является членом группы «%s».
 Пользователь «%s» не является системным. Завершение работы.
 Пользователь «%s» не создан.
 This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License, /usr/share/common-licenses/GPL, for more details.
 Попробовать ещё раз? [y/N]  Неизвестная переменная «%s» в «%s», строка %d.
 Обычно, это никогда не требовалось, так как может сделать всю систему нерабочей
 ПРЕДУПРЕЖДЕНИЕ: Вы пытаетесь удалить учётную запись суперпользователя (uid 0)
 Внимание: указанный домашний каталог %s уже существует.
 Внимание: к указанному домашнему каталогу %s нет доступа: %s
 Внимание: данный домашний каталог `%s' не принадлежит пользователю, которого вы сейчас создаете
 Предупреждение: в группе `%s' больше никого нет.
 Вы не можете удалить учётную запись из её первичной группы.
 «%s» не существует. Используются настройки по умолчанию.
 «%s» завершился по сигналу %d. Завершение работы.
 Работа «%s» завершилась с кодом ошибки %d. Завершение работы.
 У пользователя«%s» в качестве первичной указана группа «%s»!
 adduser версия %s

 deluser ПОЛЬЗОВАТЕЛЬ
  удаляет обычного учётную запись пользователя из системы
  пример: deluser mike

  --remove-home             удалить домашний каталог пользователя
                            и почтовый ящик
  --remove-all-files        удалить все файлы принадлежащие пользователю
  --backup                  сделать резервные копии файлов перед удалением.
  --backup-to <КАТ>         каталог для резервных копий файлов.
                            По умолчанию используется текущий каталог.
  --system                  удалить только если учётная запись системная

delgroup ГРУППА
deluser --group ГРУППА
  удаляет группу из системы
  пример: deluser --group students

  --system                  удалить только если группа системная
  --only-if-empty           удалить, только если в ней нет пользователей

deluser ПОЛЬЗОВАТЕЛЬ ГРУППА
  удаляет пользователя из группы
  пример: deluser mike students

общие параметры:
  --quiet | -q              не выводить информацию при работе в stdout
  --help | -h               показать справку об использовании
  --version | -v            показать версию и авторские права
  --conf | -c ФАЙЛ          использовать ФАЙЛ в качестве конфигурационного

 deluser основана на adduser, написанной Guy Maor <maor@debian.org>, 
Ian Murdock <imurdock@gnu.ai.mit.edu> и
Ted Hajek <tedhajek@boombox.micro.umn.edu>

 deluser версия %s

 ошибка при вызове fork для команды find: %s
 ошибка при вызове fork «mount» для разбора точек монтирования: %s
 ошибка при выполнении getgrnam «%s». Этого не должно случиться.
 недопустимое значение параметра
 недопустимая комбинация параметров
 файл passwd заблокирован, попробуйте ещё раз
 невозможно закрыть канал от команды mount: %s
 неожиданный сбой, ничего не сделано
 неожиданный сбой, отсутствует файл passwd
 