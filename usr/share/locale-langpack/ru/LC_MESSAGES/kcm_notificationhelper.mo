��          �      ,      �  :   �     �  5   �               8  )   H     r     �     �     �     �     �               )  �  H  :   �  I   1  W   {     �  4   �       J   &     q  :   �  D   �  +     :   <  3   w  9   �  .   �  v                                                   	             
                 (C) 2009-2010 Jonathan Thomas, (C) 2009-2014 Harald Sitter Application crashes Configure the behavior of Kubuntu Notification Helper Harald Sitter Incomplete language support Jonathan Thomas Kubuntu Notification Helper Configuration Notification type: Popup notifications only Proprietary Driver availability Required reboots Restricted codec availability Show notifications for: Tray icons only Upgrade information Use both popups and tray icons Project-Id-Version: kubuntu-notification-helper
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-04-01 17:07+0000
PO-Revision-Date: 2015-03-27 12:18+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:48+0000
X-Generator: Launchpad (build 18115)
 (C) 2009-2010 Jonathan Thomas, (C) 2009-2014 Harald Sitter Аварийное завершение работы приложений Настройка поведения помощника уведомлений Kubuntu Harald Sitter Неполная языковая поддержка Jonathan Thomas Конфигурация помощника уведомлений Kubuntu Тип уведомления: Только всплывающие уведомления Доступность проприетарных драйверов Требуемые перезагрузки Доступны дополнительные кодеки Показывать уведомления для: Только значки системного лотка Информация об обновлении Использовать всплывающие уведомления и значки системного лотка 