��           U  (      `5  
   a5  ;   l5  ~   �5     '6     96     W6  +   l6     �6  %   �6     �6  o   �6     a7     r7  )   �7  �   �7     g8     |8     �8     �8     �8  %   �8     9     9  #   39     W9    t9     v:  !   �:     �:     �:  A   �:  &   3;  "   Z;  y   };     �;  1   <     F<  <  a<  T  �=  �   �>  *   �?     �?  (   �?  (   @  A   9@  2   {@  8   �@  /   �@  -   A  ;   EA  ;   �A  +   �A  �   �A  <   �B     �B     �B  -   �B  '   ,C  =   TC  >   �C  <   �C  l   D  A   {D  !   �D  K   �D  O   +E     {E     �E     �E     �E     �E      �E  _   �E  w   ]F  f   �F  ?   <G  F   |G  �   �G  �  KH  (  �I  8   K  :   LK  �   �K  E   CL  �   �L  B  5M  �   xN  �   �N  �   �O  Z  LP  �   �Q  _   eR     �R  I   �R  �   S  o   �S  �   CT  M  �T  �   ;V  P   �V  �   W    �W  �   Y  F   �Y  H   �Y  +   1Z  K   ]Z  �   �Z  �   �[  �   F\  -   �\  8   ]  �   I]  �   �]  R   x^  �   �^  _   ^_  6   �_  7   �_  m   -`      �`     �`     �`  	   �`  �   �`  $   �a  S   �a  H   �a  (   Eb  I   nb     �b  J   �b  8   c  �  Wc  A   e  -   Me  .   {e  %   �e     �e     �e     �e     �e  
   f  A   f  .   Xf  )   �f  3   �f  P   �f  ]   6g     �g  c   �g     h     �h     �h     �h     �h     �h     �h     �h     i     i     (i     9i     Ii     Ui  1   ci  `   �i     �i  #   j  �   /j     k  �   1k     l  '   /l  f   Wl     �l     �l     �l  6   �l  4   -m     bm  t   nm     �m     �m  +   n     4n     In     bn     �n     �n     �n     +o  )   Go  C  qo  �   �p  !   �q  D   �q     r     r  J   9r  J   �r     �r     �r  ,   s     4s  B   =s  E   �s     �s     �s  �   �s  �   }t  �   u  �   �u     �v  ;   �v  $   �v  .    w  -   /w  ;   ]w     �w  2   �w  6   �w  8   !x     Zx  	   vx  #   �x  #   �x  C   �x     y     #y  `   0y     �y  3   z     Ez  (   Uz  '   ~z  K   �z     �z  M   {  '   `{  '   �{  9   �{  ,   �{  !   |  ?   9|  �   y|  Q   	}  �   [}  F   �}     F~  +   c~     �~     �~     �~  +   �~          "     6     V      h     �     �     �  '   �     �  Q   �     U�  /   m�  -   ��  (   ˀ  0   �  2   %�     X�  D   t�  2   ��     �  �   �     ��  %   ��  F   ς  G   �      ^�  6   �  :   ��  $   �  >   �     U�  '   [�  %   ��  y   ��  4   #�  /   X�  �   ��  +   N�  !   z�     ��     ��  ,   Ɇ  "   ��     �  �   6�  .  /�  F  ^�  �   ��  [   ��  �   ݋  �   ��  [  �  �   m�  f   c�  7   ʏ  E   �  �   H�  w  �  �  ��  �  !�  �   ��    P�  �   b�  w   D�    ��    ̙  �   �  �   ��  #   ^�  '   ��  �   ��  �  Y�  �   �  �   ��  *   j�  )   ��  #   ��     �  !   �     %�     8�  ~   W�  B   ֡     �     0�  *   ?�  U   j�  <   ��  ,   ��     *�  6   E�  4   |�     ��  1   ѣ  )   �  V   -�  A   ��     Ƥ  R   Ҥ     %�  &   <�  7   c�  +   ��  4   ǥ  F   ��     C�  �   Z�  &   S�     z�  q   ��  8   �     =�     W�  !   j�     ��  $   ��  $   ˨     �  #   �     ,�  �   F�     �  %   %�  �   K�  ]  �  F   @�  U   ��  �   ݬ     z�  4   ��  z   ��  $   <�  )   a�  1   ��  A   ��     ��  $   �     ?�  �   T�     �     ��  o   �     ��  0   ��  2   Ѱ  p   �  �   u�     B�     \�  �   v�  �   [�  Z   ܳ     7�     R�     g�  !   ��  /   ��     ش     ��     �     �     <�     V�     f�  &   }�  #   ��     ȵ  Q   �  @   7�  d   x�     ݶ     �     �  .   �  F   N�  5   ��     ˷  !   ߷  ;   �     =�  (   O�  :   x�  A   ��  !   ��  @   �  0   X�     ��     ��  J   ��  T   �  b   W�  <   ��  (   ��      �     ?�     T�     s�  0   ��  D   ��       �  *   !�     L�  .   T�     ��  D   ��      �  $   �     ,�  7   H�  <   ��  ?   ��      ��     �  +   =�     i�  3   ��  0   ��  7   �  5   �  6   U�     ��     ��     ƿ  8   ۿ  d   �  R   y�  5   ��  I   �  ?   L�     ��  <   ��  #   ��  %   �  �   *�  �   ��  E   ��  #   ��  "   ��  a  �  '   ��  &   ��     ��  ,   ��  J   �  ?   g�  �   ��  �   /�  �   ��  q   C�  T   ��  J   
�  �   U�  q    �  �   ��  N   n�  d   ��  D   "�  8   g�  @   ��  c   ��  i   E�  2   ��  �   ��  q   ��  �   !�  b   �  �   o�  "   7�  0   Z�  1   ��     ��  _   ��  4   8�  1   m�  D   ��  $   ��  
   	�     �     2�  (   R�     {�  %   ��  ?   ��  B   ��     <�     I�     [�  G   q�     ��  "   ��  L   ��     F�     f�      ��     ��  C   ��     �     "�  K   0�  �   |�  8   $�  j   ]�  _   ��  C  (�     l�  /   ��  G   ��  4   ��  m   3�  >   ��  3   ��  #   �  �  8�  2   ��  m    �     n�  I   t�  	   ��     ��  _   ��  7   4�  *   l�  :   ��  .   ��  7   �  @   9�  3   z�  B   ��  0   ��  C   "�     f�     �  (   ��  $   ��     ��  #   �  #   +�  
   O�  "   Z�     }�  !   ��     ��     ��  =   ��     �     -�     G�  J   ^�     ��  �   ��     ��  
   ��     ��     ��  :   ��     (�  ?   H�  )   ��  @   ��  ^   ��     R�     `�  �   h�  �   �     ��  _   �  +   k�     ��  F   ��  B   ��  	   8�  �   B�  =   ��     �  �    �  i   ��     G�  $   X�  
   }�  "   ��  9   ��     ��     �     	�  j   "�  �  ��     8�     J�     h�  +   }�     ��  %   ��     ��  �   �     ��     ��  )   ��  �   ��     ��     ��     �     �     3�  ;   O�     ��     ��  #   ��     ��  R  ��     O�  !   j�     ��     ��  A   ��  &   �  "   3�  �   V�     ��  1   �     5�  �  P�    E�  �   c�  ;   a�  (   ��  E   ��  K   �  `   X�  J   ��  U   �  X   Z�  H   ��  ^   ��  Y   [�  `   ��  :  �  P   Q  9   �  D   �  M   ! 5   o _   � n    c   t �   � l   ^ 3   � ~   � O   ~    �    �    �    	     3   / t   c �   � �   q n   � �   e �   �   � &  �
 s   � m   k ,  � i    S  p �  � �   m   Y �   [   X �  e �   �    � x   � >  " �   a !  / �  Q �   � �   �  �  O! �  �" �   �$ w   �% H   �% +   B& K   n& �  �& &  �( N  �) -   �* 8   &+   _+ �   d, �   D-   �- �   �. 6   �/ 7   �/ �   �/ ]   �0 -   �0 %   1    <1   V1 $   c2 �   �2 �   Y3 L   �3 �   @4     �4 |   �4 L   p5 �  �5 v   �9 6   �9 N   2: 8   �:    �:    �:    �:     ;    ; q   +; b   �; K    < W   L< �   �< �   .=    �=   �=    ?    �? '   �? #   �? %   �?    @    %@ %   @@     f@ -   �@    �@    �@    �@    A Y   A �   vA '   JB K   rB �  �B 6   uD �  �D $   IF O   nF �   �F )   lG 7   �G &   �G t   �G f   jH    �H   �H    �I Q   J ]   XJ 8   �J 2   �J i   "K 3   �K    �K �   �K <   �L M   �L f  8M �  �O 5   \Q e   �Q    �Q G   R �   XR u   �R D   OS G   �S w   �S 
   TT �   _T v   �T .   WU !   �U   �U 6  �V �   �W �  �X    mZ t   �Z <   [ i   ?[ 9   �[ J   �[ 1   .\ ]   `\ i   �\ n   (] B   �]    �] C   �] M   8^    �^ '   _    ._ �   G_ �   ,` a   a -   �a W   �a g   b g   ob 6   �b k   c ?   zc ?   �c l   �c N   gd N   �d �   e �   �e �   f 2  'g f   Zh )   �h V   �h 4   Bi     wi %   �i I   �i /   j    8j M   Tj #   �j +   �j :   �j    -k +   ?k ;   kk /   �k �   �k H   jl z   �l d   .m E   �m f   �m d   @n X   �n �   �n W   �o F   �o �   (p '   q F   :q �   �q z   r =   �r T   �r c   !s A   �s z   �s 
   Bt G   Mt X   �t �   �t X   �u I   �u `  Dv L   �w N   �w    Ax $   _x x   �x B   �x =   @y 4  ~y ?  �z 
  �| r  �~ �   q� *  �   .� f  2� �  �� �   1� Y   ׇ h   1� �  �� �   � �  ʌ �  �� G  �� �  ֑ K  Ó �   � �  �� �  b� �  �� /  �� M   �� g   	� �  q� �  � "  ~� �  �� S   -� I   �� D   ˦ <   � K   M� )   �� %   ç �   � �   � ,   �    � M   #� �   q� Z   � a   y� >   ۫ w   � l   �� C   �� b   C� k   �� �   � t   ��    6� �   M� B   � B   $� j   g� G   Ұ d   � �   � /   (� �  X� 3   
� /   >� +  n� �   �� 5   A� E   w� K   �� 9   	� ]   C� r   �� .   � J   C� 9   �� �  ȸ <   N� [   ��   � ?  � �   0� �   � �   ��     �� l   ��   /� O   3� `   �� m   �� s   R� J   �� B   � +   T� )  ��    ��    �� �   �� 2   �� J   �� g   8� �   �� �  b�    /�    L� �  i� %  D� �   j� =   � 5   W� 4   �� =   �� I    � 3   J� R   ~�    �� 7   �� E   &�    l� ,   �� >   �� >   �� B   5� �   x� �   �� �   �� #   N� :   r� "   �� \   �� �   -� i   �� 2   :� 9   m� r   �� #   � j   >� a   �� �   � A   �� j   �� e   D� ;   �� &   �� u   � �   �� �   *� z   � H   |� V   �� '   � >   D� 5   �� F   �� ]    � :   ^� L   ��    �� o   �� :   h� x   �� R   � U   o� 1   �� c   �� o   [� h   �� O   4� @   �� K   �� E   � z   W� z   �� �   M� �   �� ~   W� I   �� D    �     e� y   �� �    � �   �� L   �� �   �� j   w� ,   �� l   � S   |� G   �� 5  � 5  N� �   �� <   0� D   m� �  �� V   \� N   �� -   � D   0� �   u� R   � �   T� �   4� �   � �   � �   �� �   ��   � �   <� h  '� u   �� �   � k   �� Q   5� c   �� �   �� �   �� e   o� �  �� �   �  �  2 �   � F  � 7    F   O W   � *   � �    W   � ^   A   � U   � $   � B   	 4   `	 L   �	 *   �	 9   
 x   G
 v   �
    7    O     l    � L    I   Z �   � 2   O 9   � b   � _    �    ?   B     � �   � ]  N ^   � �    �   � �  �    A 5   ^ �   � Y    �   r w   X P   � :   ! 
  \ H   g �   �    � �   �    D    Y �   y b    M   j X   � Q     a   c  [   �  b   !! v   �! K   �! x   G" 0   �" ;   �" 6   -# ;   d# 4   �# H   �# <   $    [$ 8   k$ -   �$ H   �$    % $   .% i   S% 4   �% -   �% /    & ~   P&     �& �  �& 6   �(    �( 0   �(    ) Z   %) 6   �) i   �) 2   !* d   T* �   �*    _+    {+ P  �+ �  �, /   �. �   �. =   T/ ,   �/ v   �/ �   60    �0   1 �   2 ,   �2 C  �2 �   4 2   �4 7   .5 
   f5 +   q5 �   �5 $   c6    �  �   �      �               b   &  �   L      �      �       	  �   �  T   $  s          �  �   �   K      u  �      v   a  �  b  u   	      X   ~        "   6   n  �       �  �  �   �   >        [   Z  �  2  �      �  x  ,   z     D  A   �     \  4  U         0   k  �   �  #  �  %   5       �  +   *         H   3     X  x   �  '  s  8       a      �  �      2   �   F  I             |         R  �   �   �      .       Y   @  �   
      �       �   �   �   (  2  P  �   �  �   �          �   f        Q  ^        �  Q  i  �     �  �   k  S   =   5  s       ,  L           .  O       !  �   �     j   B  P  ,      �  \      C  7  �   ?   �  �  �   _      1  q            9       �   N   �         p  d  �   S  f  F       �  �   m  �  c  ;  &          ]  �  �  1   !  �  �                  �   +  �      0  .      <    �  ^  t   �   Z  �  �   �     E     �      �   �  W  A      i   �      N  }            z  �   �  �  �      3      /      �             t         +       H      �  y   �  �   `  z      �   �   ~       �   �   %  M          (  �       (   �  �   �  �  e      /           `   n      �  �   �     �  w     �             r  �   E  ]          �  �      O  ~  e           E       G   �     q       l      :  �        J   T           b    h       �       V  �     �  �          �   �   -  1  �   �   �   �  �           �  �   *  !   �       �  �           o    }   �      �        �      =  '                                      �           �  �                 N  �           n   M     �  C          �  �   U      i  �  I  8      �   �       �      �      �  �         �         �      �  K   |       7                �   �  r       �   �   J  j  �   :       X  �       A  '   �   R   ?      -   {      �      d           �   ;          $   x      u  �            �   <                       �   �         �  �               7       W   _          w            �  `      V   �     9  
  3         "  c      %  ?  
     C   �              f   l  :  <  �  �      �       �   #      _               �   Z              k           �  �   �   �   d            �  �  	   )  �   �          B  \       O  {  �    =  �   @   {   �       *    �   �   �   �   �   [  �   ]   ;   r      o      �   Y  �   �   �  m  �  P       �  �  D  g  �         �  �  �  4    G  6  �  �          0  �  �                 R      l              &   |  g  K  �  m       �  v  e  �  )  �  �   �   �       �  o      "  �       L         W  �   �  v      �      �      �   �  U  -      �   �    �  �  �   6       9      �  )   �  �   �     �      �  J  a   �       >   5      [  y  h  }  M  �  p  �       �                 �   �   8               �   V  j  y  t  �        Y  h  q  ^     �          �   �  H  �   �   /  �  c   w   �   �  �  F  �              �  S  g   �  �               Q   $      T      �   I  �              �  �  �  �       �  @  �   #          G  �  �   4       B             �      �      �      >  �            D     p   �        
.. note:: 
SUMMARY: {0} upgrades attempted, {1} succeeded, {2} failed 
To commit to master branch, run update and then commit.
You can also pass --local to commit to continue working disconnected.         bzr alias         bzr alias --remove ll         bzr alias ll         bzr alias ll="log --line -r-10..-1"         bzr check --repo bar         bzr check --tree --branch foo         bzr check baz         bzr checkout --lightweight repo/trunk trunk-checkout
        cd trunk-checkout
        (add files here)         bzr diff         bzr diff --old xxx         bzr diff --old xxx --new yyy NEWS         bzr diff --prefix old/:new/
        
    Show the differences using a custom diff program with options::
    
        bzr diff --using /usr/bin/diff --diff-options -wu         bzr diff -c2         bzr diff -r-2..         bzr diff -r1         bzr diff -r1..3         bzr diff -r1..3 xxx         bzr diff -r<chosen_parent>..X         bzr diff NEWS         bzr diff xxx/NEWS         bzr ignore "!special.class"         bzr ignore "*.class"         bzr ignore "RE:(?!debian/).*"
    
    Ignore everything except the "local" toplevel directory,
    but always ignore autosave files ending in ~, even under local/::
    
        bzr ignore "*"
        bzr ignore "!./local"
        bzr ignore "!!*~"         bzr ignore "RE:^#"         bzr ignore "RE:lib/.*\.o"         bzr ignore "lib/**/*.o"         bzr ignore ./Makefile         bzr init-repo --no-trees repo
        bzr init repo/trunk         bzr merge -r 81..82 ../bzr.dev         bzr merge -r 82 ../bzr.dev         bzr merge ../feature1a
        bzr merge ../feature1b --force
        bzr commit -m 'revision with three parents'         bzr merge /tmp/merge         bzr whoami "Frank Chu <fchu@example.com>"         bzr whoami --email     -l N        display a maximum of N revisions
    -n N        display N levels of revisions (0 for all, 1 for collapsed)
    -v          display a status summary (delta) for each revision
    -p          display a diff (patch) for each revision
    --show-ids  display revision-ids (and file-ids), not just revnos     -r-1                show just the tip
    -r-10..             show the last 10 mainline revisions
    -rsubmit:..         show what's new on this branch
    -rancestor:path..   show changes since the common ancestor of this
                        branch and the one at location path
    -rdate:yesterday..  show changes since yesterday     -rX      display revision X
    -rX..    display revision X and later
    -r..Y    display up to and including revision Y
    -rX..Y   display from X to Y inclusive     ...but do not ignore "special.class"::     Check everything at 'baz'::     Check only the repository at 'bar'::     Check the tree and branch at 'foo'::     Difference between revision 3 and revision 1 for branch xxx::     Difference between revision 3 and revision 1::     Difference between the working tree and revision 1::     Ignore .class files in all directories...::     Ignore .o files under the lib directory::     Ignore everything but the "debian" toplevel directory::     Ignore files whose name begins with the "#" character::     Make a lightweight checkout elsewhere::     Note that in the case of a merge, the -c option shows the changes
    compared to the left hand parent. To see the changes against
    another parent, use::     Same as 'bzr diff' but prefix paths with old/ and new/::     Set an alias for 'll'::     Set the current user::     Show just the differences for file NEWS::     Show the alias specified for 'll'::     Show the differences between two branches for file NEWS::     Show the differences from branch xxx to this working tree:     Show the differences in working tree xxx for file NEWS::     The changes between the current revision and the previous revision
    (equivalent to -c-1 and -r-2..-1)     The changes introduced by revision 2 (equivalent to -r1..2)::     To remove an alias for 'll'::     To see the changes introduced by revision X::
    
        bzr diff -cX     [ALIASES]
    tip = log -r-1
    top = log -l10 --line
    show = log -v -p     bzr commit foo -x foo/bar     bzr info     bzr info -v     bzr info -vv     bzr ls --ignored     bzr mv SOURCE... DESTINATION     cd ~/project
    bzr init
    bzr add .
    bzr status
    bzr commit -m "imported project"   * ``bzr log -r2 -p guide.txt`` will display an error message as there
    was no file called guide.txt in revision 2.   * ``bzr log -r2 -p tutorial.txt`` will show the changes made to
    the original file in revision 2.   * ``bzr log guide.txt`` will log the file added in revision 1   * ``bzr log tutorial.txt`` will log the new file added in revision 3   * revision 1: add tutorial.txt
  * revision 2: modify tutorial.txt
  * revision 3: rename tutorial.txt to guide.txt; add tutorial.txt   =================       =========================
  Supported formats       Autodetected by extension
  =================       =========================
     dir                         (none)
     tar                          .tar
     tbz2                    .tar.bz2, .tbz2
     tgz                      .tar.gz, .tgz
     zip                          .zip
  =================       =========================   A common mistake is to forget to add a new file or directory before
  running the commit command. The --strict option checks for unknown
  files and aborts the commit if any are found. More advanced pre-commit
  checks can be implemented by defining hooks. See ``bzr help hooks``
  for details.   A selective commit after a merge is not yet supported.   Display information on the format and related locations:   Display the above together with extended format information and
  basic statistics (like the number of files in the working tree and
  number of revisions in the branch and repository):   Display the above together with number of committers to the branch:   Filenames are interpreted within their historical context. To log a
  deleted file, specify a revision range so that the file existed at
  the end or start of the range.   GUI tools and IDEs are often better at exploring history than command
  line tools: you may prefer qlog or viz from qbzr or bzr-gtk, the
  bzr-explorer shell, or the Loggerhead web interface.  See the Plugin
  Guide <http://doc.bazaar.canonical.com/plugins/en/> and
  <http://wiki.bazaar.canonical.com/IDEIntegration>.     Historical context is also important when interpreting pathnames of
  renamed files/directories. Consider the following example:   If parameters are given and the first one is not a branch, the log
  will be filtered to show only those revisions that changed the
  nominated files or directories.   If selected files are specified, only changes to those files are
  committed.  If a directory is specified then the directory and
  everything within it is committed.   If the author of the change is not the same person as the committer,
  you can specify the author's name using the --author option. The
  name should be in the same format as a committer-id, e.g.
  "John Doe <jdoe@example.com>". If there is more than one author of
  the change you can specify the option multiple times, once for each
  author.   If you accidentially commit the wrong changes or make a spelling
  mistake in the commit message say, you can use the uncommit command
  to undo it. See ``bzr help uncommit`` for details.   If you are interested in looking deeper into a particular merge X,
  use ``bzr log -n0 -rX``.   In this case:   N File created
  D File deleted
  K File kind changed
  M File modified   Note that the default number of levels to display is a function of the
  log format. If the -n option is not used, the standard log formats show
  just the top level (mainline).   See ``bzr help revisionspec`` for details on how to specify X and Y.
  Some common examples are given below::   Status summaries are shown using status flags like A, M, etc. To see
  the changes explained using words like ``added`` and ``modified``
  instead, use the -vv option.   The --match option can be used for finding revisions that match a
  regular expression in a commit message, committer, author or bug.
  Specifying the option several times will match any of the supplied
  expressions. --match-author, --match-bugs, --match-committer and
  --match-message can be used to only match a specific field.   The -r option can be used to specify what revision or range of revisions
  to filter against. The various forms are shown below::   The following options can be used to control what information is
  displayed::   The log format controls how information about each revision is
  displayed. The standard log formats are called ``long``, ``short``
  and ``line``. The default is long. See ``bzr help log-formats``
  for more details on log formats.   To display revisions from oldest to newest, use the --forward option.
  In most cases, using this option will have little impact on the total
  time taken to produce a log, though --forward does not incrementally
  display revisions like --reverse does when it can.   When excludes are given, they take precedence over selected files.
  For example, to commit only changes within foo, but not changes
  within foo/bar::   You may find it useful to add the aliases below to ``bazaar.conf``::   [ALIASES]
  commit = commit --strict
  log10 = log --short -r -10..-1
   [DEFAULT]
  email=John Doe <jdoe@isp.com>   [myprojects]
  scheme=ftp
  host=host.com
  user=joe
  password=secret
     ``bzr log -v`` on a branch with lots of history is currently
  very slow. A fix for this issue is currently under development.
  With or without that fix, it is recommended that a revision range
  be given when using the -v option.   ``bzr tip`` will then show the latest revision while ``bzr top``
  will show the last 10 mainline revisions. To see the details of a
  particular revision X,  ``bzr show -rX``.   bzr add            make files or directories versioned
  bzr ignore         ignore a file or pattern
  bzr mv             move or rename a versioned file   bzr branch ftp://host.com/path/to/my/branch   bzr branch ftp://joe:secret@host.com/path/to/my/branch   bzr help init      more help on e.g. init command
  bzr help commands  list all commands
  bzr help topics    list all help topics
   bzr log                       log the current branch
  bzr log foo.py                log a file in its branch
  bzr log http://server/branch  log a branch on a server   bzr log            show history of changes
  bzr check          validate storage   bzr merge          pull in changes from another branch
  bzr commit         save some or all changes
  bzr send           send changes via email   bzr status         summarize changes in working copy
  bzr diff           show detailed diffs   bzr+ssh://remote@shell.example.com/~/myproject/trunk   http://bzruser:BadPass@bzr.example.com:8080/bzr/trunk "bzr diff -p1" is equivalent to "bzr diff --prefix old/:new/", and
produces patches suitable for "patch -p1". %d tag updated. %d tags updated. %s
Commit refused. %s is now not stacked
 (default) * Ignore patterns starting with "#" act as comments in the ignore file.
  To ignore patterns that begin with that character, use the "RE:" prefix. *** {0} content-filter: {1} => {2!r} , the header appears corrupt, try passing -r -1 to set the state to the last commit - not supported for merge directives that use more than one output file. --after cannot be specified with --auto. --benchmark is no longer supported from bzr 2.2; use bzr-usertest instead --dry-run requires --auto. --dry-run will show which files would be added, but not actually
add them. --exclude-common-ancestry requires -r with two revisions --file-ids-from will try to use the file ids from the supplied path.
It looks up ids trying to find a matching parent directory with the
same filename, and then by pure path. This option is rarely needed
but can be useful when adding the same logical file into two
branches that will be merged later (without showing the two different
adds as a conflict). It is also useful when merging another project
into a subdirectory of this one. --prefix expects two values separated by a colon (eg "old/:new/") --remember requires a branch to be specified. --tree and --revision can not be used together --uncommitted requires a working tree :Aliases:   :Checks: :Custom authors: :Description:
  %s

 :Examples: :Examples:
    Create a shared repository holding just branches:: :Examples:
    Ignore the top level Makefile:: :Examples:
    Show the current aliases:: :Examples:
    Show the email of the current user:: :Examples:
    Shows the difference in the working tree versus the last commit:: :Exit values:
    1 - changed
    2 - unrepresentable changes
    3 - error
    0 - no change :From:     plugin "%s"
 :Notes: 
    
* Ignore patterns containing shell wildcards must be quoted from
  the shell on Unix. :On Unix:   ~/.bazaar/bazaar.conf
:On Windows: C:\\Documents and Settings\\username\\Application Data\\bazaar\\2.0\\bazaar.conf :Options:%s :Ordering control: :Other filtering: :Output control: :Path filtering: :Purpose: %s
 :Revision filtering: :See also: %s :Selective commits: :Things to note: :Tips & tricks: :Usage:
%s
 :Usage:   %s
 A typical config file might look something like:: A warning will be printed when nested trees are encountered,
unless they are explicitly ignored. Add file "%(path)s"? Add specified files or directories. Adding a file whose parent directory is not versioned will
implicitly add the parent, and so on up to the root. This means
you should never need to explicitly add a directory, they'll just
get added when you add a file in the directory. Adding file contents Additionally for directories, symlinks and files with a changed
executable bit, Bazaar indicates their type using a trailing
character: '/', '@' or '*' respectively. These decorations can be
disabled using the '--no-classify' option. All hidden commands Alternatively, to list just the files:: Any files matching patterns in the ignore list will not be added
unless they are explicitly mentioned. Apply %d change(s)? Apply binary changes? Apply change? Apply content filters to display the convenience form. Apply content filters to export the convenient form. Apply phase At the same time it is run it may recompress data resulting in
a potential saving in disk space or performance gain. Authors Automatically guess renames. Avoid making changes when guessing renames. Backing up inventory Backup Inventory created Backup changed files (default). Backup inventory created. Basic commands Basic commands:
  bzr init           makes this directory a versioned branch
  bzr branch         make a copy of another branch Basic help for all commands Basic information on shared repositories. Bazaar also supports a global ignore file ~/.bazaar/ignore. On Windows
the global ignore file can be found in the application data directory as
C:\Documents and Settings\<user>\Application Data\Bazaar\2.0\ignore.
Global ignores are not touched by this command. The global ignore file
can be edited directly using an editor. Before merges are committed, the pending merge tip revisions are
shown. To see all pending merge revisions, use the -v option.
To skip the display of pending merge information altogether, use
the no-pending option or specify a file/directory. Bind new branch to from location. Branch %(base)s is missing revision %(text_revision)s of %(file_id)s Branch format Branch to bind checkout to. Branch to pull into, rather than the one containing the working directory. Branch to push from, rather than the one containing the working directory. Branch/tree to compare from. Branch/tree to compare to. Branched %d revision. Branched %d revisions. Branches Branches and working trees will also report any missing revisions. Branches in the repository will default to not having a working tree. Bug tracker settings Building tree By default branch will fail if the target directory exists, but does not already have a control directory.  This flag will allow branch to proceed. By default push will fail if the target directory exists, but does not already have a control directory.  This flag will allow push to proceed. By default, the entire tree is committed and the person doing the
commit is assumed to be the author. These defaults can be overridden
as explained below. By default, this command only works on branches that have not diverged.
Branches are considered diverged if the destination branch's most recent 
commit is one that has not been merged (directly or indirectly) into the 
parent. Calculating hashes Cannot debug memory on win32 without ctypes or win32process Cannot set both --verbose and --null Cannot use -r with merge directives or bundles Change "%(path)s" from %(this)s to %(other)s? Change target of "%(path)s" from "%(this)s" to "%(other)s"? Changes shelved with id "%d". Check the branch related to the current directory. Check the repository related to the current directory. Check the working tree related to the current directory. Checking unused inventories Checkouts Commit changes into a new revision. Commit even if nothing has changed. Commit refused because there are unknown files in the working tree. Committed revision %d. Committing%s Conflicts detected in working tree.  Use "bzr conflicts" to list, "bzr resolve FILE" to resolve. Convert the current branch into a checkout of the supplied branch.
If no branch is supplied, rebind to the last bound location. Convert the current checkout into a regular branch. Copying content Copying repository content as tarball... Could not determine branch to refer to. Could not move %(from_path)s%(operator)s %(to_path)s%(_has_extra)s%(extra)s Could not parse --commit-time:  Could not rename %(from_path)s%(operator)s %(to_path)s%(_has_extra)s%(extra)s Create a branch without a working tree. Create a branch without a working-tree. Create a new branch that is a copy of an existing branch. Create a new checkout of an existing branch. Create a new versioned directory. Create a shared repository for branches to share storage space. Create a stacked branch referring to the source branch. The new branch will depend on the availability of the source branch for all operations. Create a stacked branch that references the public location of the parent branch. Create a stacked branch that refers to another branch for the commit history. Only the work not present in the referenced branch is included in the branch created. Create the path leading up to the branch if it does not already exist. Created a {0} (format: {1})
 Created new stacked branch referring to %s. Creating new repository Cycle in graph %(graph)r Delete file "%(path)s"? Delete from bzr but leave the working copy. Deleted changes with id "%d". Deleting backup.bzr Deleting old repository content Deletion Strategy Description of the new revision. Determining hash hits Diff format Diff format to use. Diff3 is not installed on this machine. Display email address only. Display only the revisions that are not part of both ancestries (require -rX..Y). Display status summary. Display the default ignore rules that bzr uses. Do not consider changes made to a given path. Do not mark object type using indicator. Do not report commits with more than one parent. Do not use a shared repository, even if available. Don't backup changed files. Don't populate the working tree, even for protocols that support it. Don't recursively add the contents of directories. Don't show pending merges. Empty commit message specified. Please specify a commit message with either --message or --file or leave a blank message with --message "". Environment Variables Environment variable names and values Export current or past revision to a destination directory or archive. Export the working tree contents rather than that of the last revision. Failed to GetProcessMemoryInfo() Failed to rename %(from_path)s to %(to_path)s: %(why)s Failed to verify GPG signature data with error "%(error)s" File %(filename)s is not conflicted. File id {%(file_id)s} already exists in inventory as %(entry)s Files Files cannot be moved between branches. Fixing last revision info {0}  => {1} For more information on upgrades, see the Bazaar Upgrade Guide,
http://doc.bazaar.canonical.com/latest/en/upgrade-guide/. For more information see http://help.launchpad.net/
 Format 0.9 does not permit bundle with no patch Format may be an "exporter" name, such as tar, tgz, tbz2.  If none is
given, try to find the format with the extension. If no extension
is found exports to a directory (equivalent to --format=dir). Found %d dependent branches - upgrading ... Get file contents from this tree. Global Options HPSS calls: {0} ({1} vfs) {2} Hard-link working tree files where possible. How many lines of context to show. How to fix diverged branches If BRANCH_LOCATION is omitted, checkout will reconstitute a working tree for
the branch found in '.'. This is useful if you have removed the working tree
or if it was never created - i.e. if you pushed the branch to its current
location using SFTP. If OLDNAME does not exist on the filesystem but is versioned and
NEWNAME does exist on the filesystem but is not versioned, mv
assumes that the file has been manually moved and only updates
its internal inventory to reflect that change.
The same is valid when moving many SOURCE files to a DESTINATION. If a .bzrignore file does not exist, the ignore command
will create one and add the specified files or patterns to the newly
created file. The ignore command will also automatically add the 
.bzrignore file to be versioned. Creating a .bzrignore file without
the use of the ignore command will require an explicit add command. If a second branch is provided, cross-branch reconciliation is
also attempted, which will check that data like the tree root
id which was not present in very early bzr versions is represented
correctly in both branches. If branch is omitted then the branch containing the current working
directory will be used. If branches have diverged, you can use 'bzr merge' to integrate the changes
from one into the other.  Once one branch has merged, the other should
be able to pull it again. If branches have diverged, you can use 'bzr push --overwrite' to replace
the other branch completely, discarding its unmerged changes. If no arguments are given, all changes for the current tree are listed.
If files are given, only the changes in those files are listed.
Remote and multiple branches can be compared by using the --old and
--new options. If not provided, the default for both is derived from
the first argument, if any, or the current tree if no arguments are
given. If no arguments are specified, the status of the entire working
directory is shown.  Otherwise, only the status of the specified
files or directories is reported.  If a directory is given, status
is reported for everything inside that directory. If no restrictions are specified, all Bazaar data that is found at the given
location will be checked. If no revision is nominated, the last revision is used. If no revision is specified this exports the last committed revision. If root is supplied, it will be used as the root directory inside
container formats (tar, zip, etc). If it is not supplied it will default
to the exported filename. The root option has no effect for 'dir' format. If the --no-trees option is given then the branches in the repository
will not have working trees by default.  They will still exist as 
directories on disk, but they will not have separate copies of the 
files at a certain revision.  This can be useful for repositories that
store branches which are interacted with through checkouts or remote
branches, such as on a server. If the TO_LOCATION is omitted, the last component of the BRANCH_LOCATION will
be used.  In other words, "checkout ../foo/bar" will attempt to create ./bar.
If the BRANCH_LOCATION has no / or path separator embedded, the TO_LOCATION
is derived from the BRANCH_LOCATION by stripping a leading scheme or drive
identifier, if any. For example, "checkout lp:foo-bar" will attempt to
create ./foo-bar. If the TO_LOCATION is omitted, the last component of the FROM_LOCATION will
be used.  In other words, "branch ../foo/bar" will attempt to create ./bar.
If the FROM_LOCATION has no / or path separator embedded, the TO_LOCATION
is derived from the FROM_LOCATION by stripping a leading scheme or drive
identifier, if any. For example, "branch lp:foo-bar" will attempt to
create ./foo-bar. If the last argument is a versioned directory, all the other names
are moved into it.  Otherwise, there must be exactly two arguments
and the file is changed to a new name. If there are any uncommitted changes in the tree, they will be carried
across and remain as uncommitted changes after the update.  To discard
these changes, use 'bzr revert'.  The uncommitted changes may conflict
with the changes brought in by the change in basis revision. If there is a repository in a parent directory of the location, then
the history of the branch will be stored in the repository.  Otherwise
init creates a standalone branch which carries its own history
in the .bzr directory. If there is already a branch at the location but it has no working tree,
the tree can be populated with 'bzr checkout'. If there is no default location set, the first pull will set it (use
--no-remember to avoid setting it). After that, you can omit the
location to use the default.  To change the default, use --remember. The
value will only be saved if the remote location can be accessed. If there is no default push location set, the first push will set it (use
--no-remember to avoid setting it).  After that, you can omit the
location to use the default.  To change the default, use --remember. The
value will only be saved if the remote location can be accessed. If you want to ensure you have the different changes in the other branch,
do a merge (see bzr help merge) from the other branch, and commit that.
After that you will be able to do a push without '--overwrite'. If you want to replace your local changes and just want your branch to
match the remote one, use pull --overwrite. This will work even if the two
branches have diverged. Ignore specified files or patterns. Ignoring files outside view. View is %s In non-recursive mode, all the named items are added, regardless
of whether they were previously ignored.  A warning is given if
any of the named files are already versioned. In recursive mode (the default), files are treated the same way
but the behaviour for directories is different.  Directories that
are already versioned do not give a warning.  All directories,
whether already versioned or not, are searched for files or
subdirectories that are neither versioned or ignored, and these
are added.  This search proceeds recursively into versioned
directories.  If no names are given '.' is assumed. In recursive mode, files larger than the configuration option 
add.maximum_file_size will be skipped. Named items are never skipped due
to file size. In verbose mode, statistical information is included with each report.
To see extended statistic information, use a verbosity level of 2 or
higher by specifying the verbose option multiple times, e.g. -vv. Information on configuration and log files Information on configuring authentication Information on end-of-line handling Information on what a branch is Information on what a checkout is Instead of using:: Integration with Launchpad.net Invalid bug %s. Must be in the form of 'tracker:id'. See "bzr help bugs" for more information on this feature.
Commit refused. Invalid ignore pattern found. %s Invalid ignore patterns found. %s Inventory regenerated. It sure does!
 Limit the output to the first N revisions. List all the ignored files and the ignore pattern that caused the file to
be ignored. List entries of a particular kind: file, directory, symlink. List files deleted in the working tree.
     List files in a tree.
     List ignored files and the patterns that matched them. List the branches available at the current location. Lookup file ids from this tree. Mail client "%s" does not support specifying body Make a directory into a versioned branch. Manually set a commit time using commit date format, e.g. '2009-10-10 08:00:00 +0100'. Mark a bug as being fixed by this revision (see "bzr help bugs"). Message: %s Move only the bzr identifier of the file, because the file has already been moved. Move or rename a file. Moving repository to repository.backup Moving the root directory is not supported at this time NAME_PATTERN should not be an absolute path Name of the root directory inside the exported file. Never change revnos or the existing log.  Append revisions to it only. New branch bound to %s New branches created under the repository directory will store their
revisions in the repository, not in the branch directory.  For branches
with shared history, this reduces the amount of storage needed and 
speeds up the creation of new branches. No bundle was found in "%(filename)s". No changes are shelved. No changes to commit. Please 'bzr add' the files you want to commit, or use --unchanged to force an empty commit. No error if existing, make parent directories as needed. No help for this command. No matching files. No new revisions or tags to push. No new revisions to push. No pull location known or specified. No push location known or specified. No revisions to submit. No submit branch known or specified No such send format '%s'. No tracker specified for bug %s. Use the form 'tracker:id' or specify a default bug tracker using the `bugtracker` option.
See "bzr help bugs" for more information on this feature. Commit refused. No working tree to remove No working tree, ignoring --show-base Note that --short or -S gives status flags for each item, similar
to Subversion's status command. To get output similar to svn -q,
use bzr status -SV. Note that when using the -r argument with a range of revisions, the
differences are computed between the two specified revisions.  That
is, the command does not show the changes introduced by the first 
revision in the range.  This differs from the interpretation of 
revision ranges used by "bzr log" which includes the first revision
in the range. Note: Export of tree with non-ASCII filenames to zip is not supported. Note: Take care to redirect standard output when using this command on a
binary file. Note: The location can be specified either in the form of a branch,
or in the form of a path to a file containing a merge directive generated
with bzr send. Nothing to merge. Number of levels to display - 0 for all, 1 for flat. Once converted into a checkout, commits must succeed on the master branch
before they will be applied to the local branch. Only ASCII permitted in option names Only one path may be specified to --auto. Only remove files that have never been committed. Only remove the commits from the local branch when in a checkout. Only show versioned files. Options that control how Bazaar runs Overwrite tags only. Pack-based format used in 1.x series. Introduced in 0.92. Interoperates with bzr repositories before 0.92 but cannot be read by bzr < 0.92.  PageFaultCount    %8d PagefileUsage     %8d KiB Parent directory of %s does not exist.
You may supply --create-prefix to create all leading parent directories. Parent of "%s" does not exist. Pass these options to the external diff program. Path unknown at end or start of revision range: %s Patterns prefixed with '!!' act as regular ignore patterns, but have
precedence over the '!' exception patterns. Patterns prefixed with '!' are exceptions to ignore patterns and take
precedence over regular ignores.  Such exceptions are used to specify
files that should be versioned which would otherwise be ignored. PeakPagefileUsage %8d KiB PeakWorking       %8d KiB Perform a lightweight checkout.  Lightweight checkouts depend on access to the branch for every operation.  Normal checkouts can perform common operations like diff and status without such access, and also support local commits. Perform a local commit in a bound branch.  Local commits are not pushed to the master branch until a normal commit is performed. Perform a local pull in a bound branch.  Local pulls are not applied to the master branch. Perform a three-way merge. Print ignored files. Print just the version number. Print or set the branch nickname. Print paths relative to the root of the branch. Print unknown files. Print versioned files. PrivateUsage      %8d KiB Profile data written to "%s". Pushed up to revision %d. Reading indexes Reading inventory data Recipe for importing a tree of files:: Reconcile bzr metadata in a branch. Recurse into subdirectories. Recursively scan for branches rather than just looking in the specified location. Refuse to commit if there are unknown files in the working tree. Refuse to push if there are uncommitted changes in the working tree, --no-strict disables the check. Related commands:: Remove files or directories. Remove the alias. Remove the backup.bzr directory if successful. Remove the working tree even if it has uncommitted or shelved changes. Remove the working tree from a given branch/checkout. Removing backup ... Rename "%(this)s" => "%(other)s"? Repository %r does not support access to raw revision texts Repository format Rerun update after fixing the conflicts. Revision numbers are always relative to the source branch. Revision numbers only make sense for single revisions, not ranges Run "bzr check" for more details. See ``bzr help patterns`` for details on the syntax of patterns. See bzr help %s for more details and examples.

 Selected changes destroyed. Selected changes: Set files in the working tree back to the contents of a previous revision. Set modification time of files to that of the last revision in which it was changed. Set prefixes added to old and new filenames, as two values separated by a colon. (eg "old/:new/"). Set the author's name, if it's different from the committer. Set the branch of a checkout and update. Set/unset and display aliases. Shelve %d change(s)? Shelve adding file "%(path)s"? Shelve binary changes? Shelve changing "%s" from %(other)s to %(this)s? Shelve changing target of "%(path)s" from "%(other)s" to "%(this)s"? Shelve removing file "%(path)s"? Shelve renaming "%(other)s" => "%(this)s"? Shelve? Show changes made in each revision as a patch. Show current revision number. Show differences in the working tree, between revisions or branches. Show digital signature validity. Show files changed in each revision. Show from oldest to newest. Show historical log for a branch or subset of a branch. Show information about a working tree, branch or repository. Show just the specified revision. See also "help revisionspec". Show list of renamed files.
     Show logs of pulled revisions. Show merged revisions like --levels 0 does. Show or set bzr user id. Show revisions whose authors match this expression. Show revisions whose bugs match this expression. Show revisions whose committer matches this expression. Show revisions whose message matches this expression. Show revisions whose properties match this expression. Show revno of working tree. Show the tree root directory. Show version of bzr. Show what would be done, but don't actually do anything. Since a lightweight checkout is little more than a working tree
this will refuse to run against one. Some smart servers or protocols *may* put the working tree in place in
the future. Specify a format for this branch. See "help formats". Specify a format for this repository. See "bzr help formats" for details. Switch the checkout in the current directory to the new branch. Switched to branch: %s Tags can only be placed on a single revision, not on a range Take commit message from this file. Target directory "%s" already exists. The --verbose option will display the revisions pulled using the log_format
configuration option. You can use a different format by overriding it with
-Olog_format=<other_format>. The --verbose option will display the revisions pushed using the log_format
configuration option. You can use a different format by overriding it with
-Olog_format=<other_format>. The branch *MUST* be on a listable system such as local disk or sftp. The branch {0} has no revision {1}. The file deletion mode to be used. The filtering, ordering and information shown for each revision can
be controlled as explained below. By default, all revisions are
shown sorted (topologically) so that newer revisions appear before
older ones and descendants always appear before ancestors. If displayed,
merged revisions are shown indented under the revision in which they
were merged. The levels argument must be an integer. The limit argument must be an integer. The path name in the old tree. The repository {0} contains no revision {1}. The root is the nearest enclosing directory with a .bzr control
directory. The synonyms 'clone' and 'get' for this command are deprecated. The target branch will not have its working tree populated because this
is both expensive, and is not supported on remote file systems. The tree does not appear to be corrupt. You probably want "bzr revert" instead. Use "--force" if you are sure you want to reset the working tree. The working tree and branch checks will only give output if a problem is
detected. The output fields of the repository check are: The working tree for %(basedir)s has changed since the last commit, but weave merge requires that it be unchanged Therefore simply saying 'bzr add' will version all files that
are currently unknown. This branch format cannot be set to append-revisions-only.  Try --default. This can correct data mismatches that may have been caused by
previous ghost operations or bzr upgrades. You should only
need to run this command if 'bzr check' or a bzr developer
advises you to run it. This command checks various invariants about branch and repository storage
to detect data corruption or bzr bugs. This command only works on branches that have not diverged.  Branches are
considered diverged if the destination branch's most recent commit is one
that has not been merged (directly or indirectly) by the source branch. This command will print the names of all the branches at the current
location. This command will show all known locations and formats associated to the
tree, branch or repository. This is a checkout. The branch (%s) needs to be upgraded separately. This is equal to the number of revisions on this branch. This is equivalent to creating the directory and then adding it. This reports on versioned and unknown files, reporting them
grouped by state.  Possible states are: To compare the working directory to a specific revision, pass a
single revision to the revision argument. To re-create the working tree, use "bzr checkout". To remove patterns from the ignore list, edit the .bzrignore file.
After adding, editing or deleting that file either indirectly by
using this command or directly by using an editor, be sure to commit
it. To retrieve the branch as of a particular revision, supply the --revision
parameter, as in "branch foo/bar -r 5". To retrieve the branch as of a particular revision, supply the --revision
parameter, as in "checkout foo/bar -r 5". Note that this will be immediately
out of date [so you cannot commit] but it may be useful (i.e. to examine old
code.) To see ignored files use 'bzr ignored'.  For details on the
changes to file texts, use 'bzr diff'. To see which files have changed in a specific revision, or between
two revisions, pass a revision range to the revision argument.
This will produce the same results as calling 'bzr diff --summarize'. Tree is up to date at revision %d. Tree is up to date at revision {0} of branch {1} Turn this branch into a mirror of another branch. Type of file to export to. Unable to connect to current master branch %(target)s: %(error)s To switch anyway, use --force. Unable to handle bundle version %(version)s: %(msg)s Unable to import library "%(library)s": %(error)s Unknown key {0} signed {1} commit Unknown key {0} signed {1} commits Unrecognized bug %s. Commit refused. Unstacking Unsupported export format: %s Update a mirror of this branch. Update a working tree to a new revision. Updated to revision %d. Updated to revision {0} of branch {1} Upgrade a repository, branch or working tree to a newer format. Upgrade to a specific format.  See "bzr help formats" for details. Upgrading %s Upgrading bzrdirs Upgrading {0} {1} ... Use bzr resolve when you have fixed a problem.  See also bzr conflicts. Use short status indicators. Use this command to compare files. Use this to create an empty branch, or before importing an
existing project. Using Bazaar with Launchpad.net Using changes with id "%d". Using saved parent location: %s
 Using saved push location: %s Using saved {0} location "{1}" to determine what changes to submit. Using shared repository: %s
 Using {0} {1} Validate working tree structure, branch consistency and repository history. Warning: the following files are version controlled and match your ignore pattern:
%s
These files will continue to be version controlled unless you 'bzr remove' them.
 What names to list as authors - first, all or committer. When committing to a foreign version control system do not push data that can not be natively represented. When no message is supplied, show the diff along with the status summary in the message editor. When the default format has changed after a major new release of
Bazaar, you may be informed during certain operations that you
should upgrade. Upgrading to a newer format may improve performance
or make new features available. It may however limit interoperability
with older repositories or with older versions of Bazaar. WorkingSize       %8d KiB WorkingSize {0:>7}KiB	PeakWorking {1:>7}KiB	{2} Write the contents of a file as of a given revision to standard output. You can only supply one of revision_id or --revision You can use this to visit an older revision, or to update a working tree
that is out of date from its branch. You cannot remove the working tree from a lightweight checkout You cannot remove the working tree of a remote path You cannot specify a NULL revision. You cannot update just a single file or directory, because each Bazaar
working tree has just a single basis revision.  If you want to restore a
file that has been removed locally, use 'bzr revert' instead of 'bzr
update'.  If you want to restore a file to its state in a previous
revision, use 'bzr revert' with a '-r' option, or use 'bzr cat' to write
out the old content of that file to a new location. You must supply either --revision or a revision_id Your local commits will now show as pending merges with 'bzr status', and can be committed with 'bzr commit'. added added
    Versioned in the working copy but not in the previous revision. added %s
 adding file branch has no revision %s
bzr update --revision only works for a revision in the branch history bzr %s --revision takes exactly one revision identifier bzr %s --revision takes one or two values. bzr %s doesn't accept two revisions in different branches. bzr alias --remove expects an alias to remove. bzr cat --revision takes exactly one revision specifier bzr diff --revision takes exactly one or two revision specifiers bzr send takes at most two one revision identifiers bzr status --revision takes exactly one or two revision specifiers bzr update --revision takes exactly one revision bzr update can only update a whole tree, not a file or subdirectory bzr: ERROR (ignored): %s can not move root of branch cannot specify both --from-root and PATH cannot upgrade from bzrdir format %s checked branch {0} format {1} command {0!r} needs one or more {1} command {0!r} requires argument {1} deleted %s extra argument to command {0}: {1} failed to clean-up {0}: {1} failed to reset the tree state{0} finished found error:%s ignore requires at least one NAME_PATTERN or --default-rules. ignored {0} matching "{1}"
 invalid kind %r specified invalid kind specified kind changed
    File kind has been changed (e.g. from file to directory). listening on port: %s log is bzr's default tool for exploring the history of a branch.
The branch to use is taken from the first parameter. If no parameters
are given, the branch containing the working directory is logged.
Here are some simple examples:: merge base is revision %s
 missing %s missing file argument modified modified
    Text has changed since the previous revision. not a valid revision-number: %r please specify a commit message with either --message or --file please specify either --message or --file python-gpgme is not installed, it is needed to verify signatures removed
    Versioned in the previous revision but removed or deleted
    in the working copy. removing file renamed renamed
    Path of this file changed from the previous revision;
    the text may also have changed.  This includes files whose
    parent directory was renamed. repeated file texts
    This is the total number of repeated texts seen
    in the checked revisions.  Texts can be repeated when their file
    entries are modified, but the file contents are not.  It does not
    indicate a problem. repository converted revisions
    This is just the number of revisions checked.  It doesn't
    indicate a problem. skipping {0} (larger than {1} of {2} bytes) starting upgrade of %s subunit not available. subunit needs to be installed to use --subunit. to mv multiple files the destination must be a versioned directory unchanged unique file texts
    This is the total number of unique file contents
    seen in the checked revisions.  It does not indicate a problem. unknown
    Not versioned and not matching an ignore pattern. unknown command "%s" unreferenced ancestors
    Texts that are ancestors of other texts, but
    are not properly referenced by the revision ancestry.  This is a
    subtle problem that Bazaar can work around. versionedfiles
    This is just the number of versionedfiles checked.  It
    doesn't indicate a problem. you simply use:: {0!r} is not present in revision {1} {0} => {1} {0} and {1} are mutually exclusive {0} commit with unknown key {0} commits with unknown keys {0} is now stacked on {1}
 Project-Id-Version: bzr
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-07-27 12:45+0200
PO-Revision-Date: 2011-12-07 12:58+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 17:42+0000
X-Generator: Launchpad (build 18115)
 
.. замечание:: 
СВОДКА: произведено {0} обновлений, {1} успешно, {2} неуспешно 
Чтобы зафиксировать изменения в основную ветку, выполните сначала update (обновление), затем - commit. 
Также Вы можете задать команде commit параметр --local, чтобы зафиксировать изменения локально и продолжить работу без соединения с сервером.         bzr alias         bzr alias --remove ll         bzr alias ll         bzr alias ll="log --line -r-10..-1"         bzr check --repo bar         bzr check --tree --branch foo         bzr check baz         bzr checkout --lightweight repo/trunk trunk-checkout
        cd trunk-checkout
        (здесь добавляйте файлы)         bzr diff         bzr diff --old xxx         bzr diff --old xxx --new yyy NEWS         bzr diff --prefix old/:new/
        
    Показать изменения, используя пользовательскую программу diff с опциями::
    
        bzr diff --using /usr/bin/diff --diff-options -wu         bzr diff -c2         bzr diff -r-2..         bzr diff -r1         bzr diff -r1..3         bzr diff -r1..3 xxx         bzr diff -r<выбранный_родитель>..X         bzr diff NEWS         bzr diff xxx/NEWS         bzr ignore "!special.class"         bzr ignore "*.class"         bzr ignore "RE:(?!debian/).*"
    
    Игнорировать всё, кроме верхней директории "local",
    но всегда игнорировать файлы, заканчивающиеся на ~, даже в local/::
    
        bzr ignore "*"
        bzr ignore "!./local"
        bzr ignore "!!*~"         bzr ignore "RE:^#"         bzr ignore "RE:lib/.*\.o"         bzr ignore "lib/**/*.o"         bzr ignore ./Makefile         bzr init-repo --no-trees repo
        bzr init repo/trunk         bzr merge -r 81..82 ../bzr.dev         bzr merge -r 82 ../bzr.dev         bzr merge ../feature1a
        bzr merge ../feature1b --force
        bzr commit -m 'ревизия с тремя родителями'         bzr merge /tmp/merge         bzr whoami "Frank Chu <fchu@example.com>"         bzr whoami --email     -l N        выводить не больше N ревизий
    -n N        выводить N уровней ревизий (0 для всех, 1 для сжатого)
    -v          выводить статусные сообщения (delta) для каждой ревизии
    -p          выводить различия diff для каждой ревизии
    --show-ids  выводить id ревизий (и id файлов), не только номера ревизий     -r-1                показать только верхушку
    -r-10..             показать последние 10 ревизий основной линии
    -rsubmit:..         показать, что нового в этой ветви
    -rancestor:путь..   показать изменения, начиная с общего предка
                        этой ветви и указанной в пути
    -rdate:yesterday..  показать изменения со вчерашнего дня     -rX      вывести ревизию X
    -rX..    вывести ревизию X и более поздние
    -r..Y    вывести до и включая ревизию Y
    -rX..Y   вывести от X до Y включительно     ...но не игнорировать "special.class"::     Проверить всё в 'baz'::     Проверить только репозиторий в 'bar'::     Проверить рабочее дерево и ветвь в 'foo'::     Различия между ревизией 3 и ревизией 1 для ветви xxx::     Различия между ревизией 3 и ревизией 1::     Различия между рабочим деревом и ревизией 1::     Игнорировать файлы .class во всех директориях...::     Игнорировать файлы .o в директории lib::     Игнорировать всё, кроме верхней директории "debian"::     Игнорировать файлы, начинающиеся с символа "#"::     Создать легковесную рабочую копию где-нибудь ещё::     Заметьте, что в случае слияния, опция -c выведет только изменения
    относительно левого родителя.  Чтобы увидеть изменения относительно
    правого родителя, используйте::     Тоже, что и 'bzr diff', но начать пути с old/ и new/::     Установить псевдоним для 'll'::     Установить текущего пользователя::     Показать изменения только для файла NEWS::     Показать псевдоним для 'll'::     Показать различия в файле NEWS между двумя ветвями::     Показать изменения рабочего дерева относительно ветви xxx:     Показать изменения в рабочем дереве xxx для файла NEWS::     Изменения между текущей ревизией и предыдущей
    (эквивалент -c-1 и -r-2..-1)     Изменения, представленные в ревизии 2 (эквивалентно -r1..2)::     Удалить псевдоним для 'll'::     Чтобы увидеть изменения, представленные в ревизии X::
    
        bzr diff -cX     [ALIASES]
    tip = log -r-1
    top = log -l10 --line
    show = log -v -p     bzr commit foo -x foo/bar     bzr info     bzr info -v     bzr info -vv     bzr ls --ignored     bzr mv ИСТОЧНИК... НАЗНАЧЕНИЕ     cd ~/project
    bzr init
    bzr add .
    bzr status
    bzr commit -m "проект импортирован"   * «bzr log -r2 -p guide.txt» выведет сообщение об ошибке, так как
    в ревизии 2 нет файла guide.txt.   * «bzr log -r2 -p tutorial.txt» выведет изменения оригинального
    файла в ревизии 2.   * «bzr log guide.txt» выведет журнал файла, добавленного в ревизии 1   * «bzr log tutorial.txt» выведет журнал нового файла, добавленного
    в ревизии 3   * ревизия 1: добавлен tutorial.txt
  * ревизия 2: изменён tutorial.txt
  * ревизия 3: переименован tutorial.txt в guide.txt; добавлен tutorial.txt   ======================       =============================
  Поддерживаемые форматы       Автоопределение по расширению
  ======================       =============================
      dir                                (нет)
      tar                                .tar
      tbz2                          .tar.bz2, .tbz2
      tgz                            .tar.gz, .tgz
      zip                                .zip
  ======================       =============================   Распространённой ошибкой является забыть добавить новый файл перед
  запуском команды commit. Опция --strict прерывает закрепление, если
  присутствуют неизвестные файлы. Более продвинутые проверки могут
  быть реализованы определением обработчиков. Подробности можно найти,
  выполнив команду ``bzr help hooks''.   Выборочные закрепления после слияния пока не поддерживаются.   Вывести информацию о формате и связанных местоположениях:   Вывести то же, что и выше, а также расширенную информацию о формате
  и базовую статистику (как число файлов в рабочем дереве и число
  ревизий в ветви и репозитории):   Вывести то же, что и выше, а также число участников ветви:   Имена файлов интерпретируются в историческом контексте. Чтобы вывести
  журнал для удалённого файла, укажите такой интервал ревизий, что файл
  существует от начала до конца интервала.   Чаще лучше просматривать историю с помощью графических утилит и IDE,
  например qlog, viz из qbzr или bzr-gtk, оболочку bzr-explorer, или
  веб-интерфейс Loggerhead. Смотрите руководство по модулям
  <http://doc.bazaar.canonical.com/plugins/en/> и
  <http://wiki.bazaar.canonical.com/IDEIntegration>.     Исторический контекст также важен при интерпретации путей
  переименованных файлов и директорий. Рассмотрим следующий пример:   Если заданы параметры и первый не является ветвью, будут отображаться
  только те ревизии, которые изменяли указанные файлы или директории.   Если указаны файлы, закрепить только изменения в этих файлах.
  Если указана директория, то закрепляется директория и всё её
  содержимое.   Если автор изменения не тот же самый человек, что и закрепляющий,
  вы можете указать имя автора, используя опцию --author. Имя должно
  быть в том же формате, что и committer-id, например
  "John Doe <jdoe@example.com>". Если в изменении участвовал более,
  чем один автор, вы можете указать эту опцию несколько раз.   Если вы случайно закрепили неверные изменения или допустили орфо-
  графическую ошибку в сообщении, вы можете использовать команду
  uncommit, чтобы отменить последнее закрепление. Подробности смотрите
  в «bzr help uncommit».   Если вы хотите подробно рассмотреть определённое слияние X,
  используйте «bzr log -n0 -rX».   В этом случае:   N Файл создан
  D Файл удалён
  K Тип файла изменился
  M Файл изменён   Число уровней отображения по умолчанию зависит от формата журнала.
  Если используется не опция -n, стандартные форматы журнала показывают
  верхний уровень (основную линию).   Чтобы узнать, как указывать X и Y, смотрите «bzr help revisionspec».
  Ниже приведеные некоторые распространённые примеры::   Описания статусов отображаются с помощью флагов A, M, и т.д. Чтобы
  увидеть изменения, описанные словами вроде «добавлены» и «изменены»,
  используйте опцию -vv.   Опцию --match можно использовать для нахождения ревизий, удовлетворяющих
  регулярному выражению в сообщении, закрепляющем, авторе или ошибке.
  Опцию можно указать несколько раз и в этом случае будут выведены только
  ревизии, удовлетворяющие всем критериям. Для поиска по определённым
  полям можно использовать опции --match-author, --match-bugs,
  --match-committer и --match-message.   Опцию -r можно использовать для указания ревизии или интервала ревизий
  для фильтрации. Различные формы показаны ниже::   Следующие опции могут использоваться для управления отображением
  информации::   Формат журнала определяет, как отображается информация о каждой
  ревизии.  Стандартные форматы журнала называются «long», «short»
  и «line». По умолчание используется long. Подробности о форматах
  смотрите в «bzr help log-formats».   Чтобы вывести ревизии от старой до новой, используйте опцию --forward.
  В большинстве случаев, при использовании этой опции на создание журнала
  расходуется мало времени, хотя --forward не выводит ревизии по порядку,
  как это делает --reverse.   Когда даны исключения, они имеют приоритет над выбранными файлами.
  Например, чтобы закрепить изменения внутри foo, но не изменения
  в foo/bar::   Возможно, вы захотите добавить следующие псевдонимы в «bazaar.conf»::   [ALIASES]
  commit = commit --strict
  log10 = log --short -r -10..-1
   [DEFAULT]
  email=John Doe <jdoe@isp.com>   [myprojects]
  scheme=ftp
  host=host.com
  user=joe
  password=secret
     «bzr log -v» на ветви с большой историей на данный момент работает
  очень медленно. Сейчас ведётся работа над исправлением этого
  недостатка. Независимо от того, будет это исправлено или нет,
  рекомендуется задавать интервал ревизий с помощью опции -v.   Тогда «bzr tip» выведет последнюю ревизию, а «bzr top» — последние
  10 ревизий основной линии. Чтобы просмотреть журнал определённой
  ревизии X, используйте «bzr show -rX».   bzr add            добавить файлы или директории к системе контроля версий
  bzr ignore         игнорировать файл или шаблон
  bzr mv             переместить или переименовать файл в системе контроля версий   bzr branch ftp://host.com/path/to/my/branch   bzr branch ftp://joe:secret@host.com/path/to/my/branch   bzr help init      подробные сведения например по команде init
  bzr help commands  вывести список всех команд
  bzr help topics    вывести список всех разделов справки
   bzr log                       журнал текущей ветви
  bzr log foo.py                журнал файла в его ветви
  bzr log http://server/branch  журнал ветви на сервере   bzr log            показать историю изменений
  bzr check          проверить целостность   bzr merge          принять изменения из другой ветви
  bzr commit         сохранить некоторые или все изменения
  bzr send           переслать изменения по электронной почте   bzr status         вывести краткие изменения в рабочей копии
  bzr diff           вывести подробные различия   bzr+ssh://remote@shell.example.com/~/myproject/trunk   http://bzruser:BadPass@bzr.example.com:8080/bzr/trunk «bzr diff -p1» эквивалентно «bzr diff -prefix old/:new/», и
создаёт патчи, подходящие для «patch -p1». %d тег обновлён. %d тега обновлено. %d тегов обновлено. %s
Закрепление отклонено. %s теперь не на стеке
 (по умолчанию) * Шаблоны игнорирования, начинающиеся с "#", являются комментариями.
  Чтобы добавить шаблон, начинающийся с этого символа, используйте
  префикс "RE:". *** {0} content-filter: {1} => {2!r} , заголовок повреждён, попробуйте использовать аргумент -r -1, чтобы сбросить состояние до предыдущего закрепления - не поддерживается для директив merge, которые используют более, чем один файл вывода. --after не может использоваться вместе с --auto. --benchmark не поддерживается начиная с версии bzr 2.2; вместо этого используйте bzr-usertest --dry-run требует --auto. --dry-run покажет, какие файлы будут добавлены, но не будет их
добавлять. --exclude-common-ancestry требует -r с двумя ревизиями --file-ids-from попытается использовать идентификаторы файлов из
предоставленного пути. С этой опцией просматриваются идентификаторы
в попытке найти соответствующую родительскую директорию с тем же
именем, а затем полный путь. Эта опция редко необходима, но может
быть полезна при добавлении одного и того же логического файла к
двум различным ветвям, которые затем будут совмещены (при этом
добавления в обе ветви не будут рассматриваться как конфликты).
Это также полезно при помещении другого проекта, как поддиректории
данного. --prefix ожидает два значения, разделённые двоеточием (напр. «old/:new/») --remember требует указания ветви. --tree и --revision не могут использоваться вместе --uncommitted требует рабочее дерево :Псевдонимы:   :Проверки: :Выбор автора: :Описание:
  %s

 :Примеры: :Примеры:
    Создать общий репозиторий, хранящий только ветви:: :Примеры:
    Игнорировать файл Makefile на верхнем уровне:: :Примеры:
    Показать текущие псевдонимы:: :Примеры:
    Показать email текущего пользователя:: :Примеры:
    Показывает отличия рабочего дерева от последнего закрепления:: :Значения выхода:
    1 - изменено
    2 - непредставимые изменения
    3 - ошибка
    0 - нет изменений :Из:     модуль «%s»
 :Замечания: 
    
* Шаблоны игнорирования, содержащие специальные символы оболочки,
  должны быть заключены в кавычки при указании в командной строке. :В Unix:   ~/.bazaar/bazaar.conf
:В Windows: C:\\Documents and Settings\\username\\Application Data\\bazaar\\2.0\\bazaar.conf :Опции:%s :Управление порядком: :Другая фильтрация: :Управление выводом: :Фильтрация пути: :Назначение: %s
 :Фильтрация ревизий: :Смотрите также: %s :Выборочные закрепления: :На заметку: :Советы и трюки: :Синтаксис:
%s
 :Синтаксис:   %s
 Типичный файл конфигурации может выглядеть как:: Предупреждение будет напечатано для каждого встреченного вложенного
дерева, если только оно не игнорируется явно. Добавить файл «%(path)s»? Добавить указанные файлы или директории. Добавление файла, родительская директория которого не имеет версии
неявно добавляет родителя и так далее до корня. Это означает, что
вам никогда не нужно явно добавлять директорию, они добавляются
автоматически, когда вы добавляете файлы. Добавление содержимого файлй Кроме этого, для директорий, символьных ссылок и файлов, бит
выполнения которых изменился, Bazaar показывает их тип, добавляя
в конец имени символы: '/', '@' или '*' соответственно. Эти
декорации можно убрать, используя опцию '--no-classify'. Все скрытые команды Альтернативно, чтобы вывести только файлы:: Любые файлы, удовлетворяющие шаблонам в списке ignore не будут
добавлены, если их не указать явно. Применить %d изменений? Применить двоичные изменения? Применить изменение? Применить фильтры содержимого для отображения в удобной форме. Применить контент-фильтры для экспорта в удобной форме. Применить фазу В то же время, при запуске возможно пережатие данных, потенциально
уменьшающее размер занимаемого пространства и повышающее
производительность. Авторы Автоматически предполагать переименования. Избегать изменений при угадывании переименований. Создание резервного инвентаря Резервный инвентарь создан Создать резервную копию измененных файлов (по умолчанию). Резервный инвентарь создан. Базовые команды Базовые команды:
  bzr init           превратить директорию в ветвь системы версий
  bzr branch         создать копию другой ветви Базовая справка по всем командам Базовая информация об общих репозиториях. Bazaar также поддерживает глобальный игнорирования по пути ~/.bazaar/ignore.
На Windows глобальный файл игнорирования может быть найден в директории
данных приложений. Ищите пути вроде
C:\Documents and Settings\<user>\Application Data\Bazaar\2.0\ignore.
Эта команда не изменяет глобальный файл игнорирования. Он может быть
отредактирован вручную с помощью текстового редактора. Перед закреплением слияний, отображаются необработанные ревизии.
Чтобы увидеть все ревизии слияния, используйте опцию -v.
Чтобы полностью пропустить отображение информации о слиянии,
используйте опцию no-pending либо укажите файл или директорию. Связать новую ветвь с FROM_LOCATION В ветви %(base)s отсутствует ревизия %(text_revision)s файла %(file_id)s Формат ветви Ветвь, с которой связать рабочую копию. Принимать изменения в эту ветвь, а не в содержащую рабочую директорию. Отправлять из этой ветви, а не из содержащей рабочую директорию. Ветвь или дерево, которое сравнивать. Ветвь или дерево, с которым сравнивать. Ответвлена %d ревизия. Ответвлены %d ревизии. Ответвлены %d ревизий. Ветви Ветви и рабочие деревья также включают в отчёт отсутствующие ревизии. Ветви в репозитории по умолчанию не будут иметь рабочего дерева. Установки трекера ошибок Построение дерева По умолчанию branch завершится неудачей, если директория назначения существует, но не содержит управляющих метаданных. Этот флаг позволит branch продолжаться. По умолчанию, push закончится неудачей, если директория назначения существует, но не содержит управляющих метаданных. Этот флаг позволит отправке изменений продолжаться. По умолчанию, закрепляется всё дерево и закрепляющий считается автором.
Эти умолчания могут быть перезаписаны, как объяснено ниже. По умолчанию, эта команда работает только на ветвях, которые не являются
отклонениями. Ветвь считается отклонением, если последнее закрепление в
ветви назначения не было совмещено (явно или неявно) с родителем. Вычисление хешей Невозможно производить отладку памяти на win32 без ctypes или win32process Нельзя установить оба --verbose и --null Нельзя использовать -r с директивами слияния или пакетами Изменить «%(path)s» с %(this)s на %(other)s? Изменить цель «%(path)s» с «%(this)s» на «%(other)s»? Изменения отложены с id «%d». Проверить ветвь, относящуюся к текущей директории. Проверить репозиторий, относящийся к текущей директории. Проверить рабочее дерево, относящееся к текущей директории. Проверка неиспользуемых инвентарей Рабочие копии Закрепить изменения в новой ревизии. Закрепить, даже если ничего не изменилось. Закрепление отклонено, поскольку в рабочем дереве неизвестные файлы. Закреплена ревизия %d. Закрепление%s В рабочем дереве обнаружены конфликты. Выполните «bzr conflicts», чтобы вывести список конфликтов и «bzr resolve ФАЙЛ» для исправления. Преобразовать текущую ветвь в рабочую копию указанной ветви.
Если ветвь не указаны, перепривязать последнее расположение привязки. Преобразовать текущую рабочую копию в обычную ветвь. Копирование содержимого Копирование содержимого репозитория в тарбол... Не удалось определить ветвь, на которую нужно ссылаться. Не удалось переместить %(from_path)s%(operator)s %(to_path)s%(_has_extra)s%(extra)s Не удалось разобрать --commit-time:  Не удалось переименовать %(from_path)s%(operator)s %(to_path)s%(_has_extra)s%(extra)s Создать ветвь без рабочего дерева. Создать ветвь без рабочего дерева. Создать новую ветвь, являющуюся копией существующей ветви. Создать новую рабочую копию текущей ветви. Создать новую директорию в системе версий. Создать общий репозиторий для ветвей, которые разделяют общее пространство. Создать стековую ветвь, ссылающуюся на исходную ветвь. Все операции над новой ветвью будут зависеть от доступности старой ветви. Создать стековую ветвь, которая ссылается на публичное местоположение родительской ветви. Создать стековую ветвь, которая ссылается на другую ветвь для хранения истории фиксаций. В созданную ветвь помещается только работа, не существующая в исходной ветви. Создать путь, ведущий к ветви, если он ещё не существует. Создана {0} в формате {1}
 Создана новая стековая ветвь, ссылающаяся на %s. Создание нового репозитория Цикл в графе %(graph)r Удалить файл «%(path)s»? Удалить из bzr, но оставить рабочую копию. Удалены изменения с id «%d». Удаление backup.bzr Удаление старого содержимого репозитория Стратегия удаления Описание новой ревизии. Определение столкновений хешей Формат diff Используемый формат diff. Diff3 не установлен на этой машине. Показать только адрес email. Показать только ревизии, которые не являются частью обоих предков (требует -rX..Y). Отобразить краткое описание состояния. Вывести правила игнорирования, которые bzr использует по умолчанию. Не рассматривать изменения, сделанные в заданном пути. Не помечать типы объектов суффиксами. Не сообщать о закреплениях с более, чем одним родителем. Не использовать общий репозиторий, даже если доступен. Не создавать резервные копии изменённых файлов. Не создавать рабочее дерево, даже для протоколов, которые поддерживают это. Не добавлять содержимое директорий рекурсивно. Не показывать необработанные слияния. Указано пустое сообщение о закреплении. Пожалуйста, укажите сообщение опцией --message либо --file или оставьте пустым, указав --message "". Переменные окружения Имена и значения переменных окружения Экспортировать текущую или прошлую ревизию в директорию назначения
или архив. Экспортировать содержимое рабочего дерева а не последней ревизии. Не удалось выполнить GetProcessMemoryInfo() Не удалось переименовать %(from_path)s в %(to_path)s: %(why)s Не удалось верифицировать подпись GPG с ошибкой «%(error)s» Файл %(filename)s не содержит конфликтов. Идентификатор файла {%(file_id)s} уже присутствует в инвентаре как %(entry)s Файлы Файлы нельзя перемещать между ветвями. Исправление информации последней ревизии {0} => {1} Более подробную информацию по обновлению смотрите на Bazaar Upgrade Guide 
http://doc.bazaar.canonical.com/latest/en/upgrade-guide/. Больше информации можно найти на http://help.launchpad.net/
 Формат 0.9 не разрешает пакеты без патчей Формат может быть именем экспортируемого формата, как tar, tgz, tbz2.
Если не указан, формат определяется по расширению. Если расширение
отсутствует, экспортирует в директорию (эквивалентно --format=dir). Найдено %d зависящих ветвей - обновление ... Получить содержимое файла из этого дерева. Общие настройки Вызовы HPSS: {0} ({1} vfs) {2} Использовать жёсткие ссылки в рабочем дереве, когда это возможно. Сколько строк контекста показывать. Как исправлять отклонения ветвей Если аргумент BRANCH_LOCATION опущен, checkout воссоздаст рабочее дерево
для ветви, найденной в '.'. Это полезно, если вы удалили рабочее дерево,
или если вы никогда не создавали его. Если СТАРОЕИМЯ не существует в файловой системе, но есть в системе
версий, а НОВОЕИМЯ существует, но отсутствует в системе версий, mv
предполагает, что файл был перемещён вручную и только обновляет
свои внутренние структуры, чтобы отразить это изменение.
Тоже верно и при перемещении многих ИСТОЧНИКов в НАЗНАЧЕНИЕ. Если файл .bzrignore не существует, команда ignore создаст его
и добавит указанные файлы или шаблоны к только что созданному
файлу. Команда ignore также автоматически добавит файл .bzrignore
к системе версий. Создание файла .bzrignore без использования
команды ignore потребует явного добавления командой add. Если предоставлена вторая ветвь, производится попытка согласования
между ветвями, в ходе которой проверяется, что данные, не
присутствовавшие в ранних версиях bzr, корректно представлены
в обоих ветвях. Если ветвь опущена, используется ветвь, содержащая текущую рабочую
директорию. Если ветвь является отклонением, можно использовать 'bzr merge' для
возвращения изменений.  После совмещения ветвей, можно снова принимать
изменения из одной в другую. Если ветвь была отклонена, вы можете использовать 'bzr push --overwrite',
чтобы полностью заменить другую ветвь, убирая все её несовмещённые
изменения. Если не дано аргументов, будут выведены все изменения текущего дерева.
Если даны файлы, выводятся изменения только в этих файлах. Удалённые
или множественные ветви можно сравнить, используя опции --old и --new.
Если не указаны, значение по умолчанию для обоих извлекается из первого
аргумента или из текущего дерева, если не дано аргументов. Если не указано аргументов, выводится состояние всей рабочей директории.
Иначе, выводится только статус указанных файлов или директорий.
Если указана директория, то выводится состояние всех файлов внутри
этой директории. Если не указаны ограничения, все данные Bazaar, 
найденные в указанном месте, будут проверены. Если ревизия не указана, используется последняя. Если ревизия не указана, экспортирует последнюю ревизию. Если указан root, он будет использоваться как корневая директория
внутри форматов-контейнеров (tar, zip, и т.д.). Если не указан, по
умолчанию используется имя файла для эскпорта. Опция root не имеет
эффекта для формата 'dir'. Если дана опция --no-trees, то по умолчанию ветви в репозитории не
будут иметь рабочих деревьев.  Они по прежнему будут существовать,
как директории на диске, но не будут иметь различных копий файлов
определённой ревизии.  Это может быть полезно для репозиториев,
хранящих ветви, работа с которыми производится через рабочие копии
или через удалённые ветви, например на сервере. Если TO_LOCATION опущено, будет использована последняя компонента
BRANCH_LOCATION. Иными словами, «checkout ../foo/bar» попытается
создать ./bar. Если BRANCH_LOCATION не содержит разделителя «/»,
то TO_LOCATION определется из BRANCH_LOCATION удалением префикса.
Например «checkout lp:foo-bar» попытается создать ./foo-bar. Если TO_LOCATION опущено, будет использована последняя компонента
FROM_LOCATION. Иными словами, «branch ../foo/bar» попытается создать ./bar.
Если FROM_LOCATION не содержит разделителя «/», то TO_LOCATION определется
из FROM_LOCATION удалением префикса. Например «branch lp:foo-bar»
попытается создать ./foo-bar. Если последний аргумент является директорией под контролем Bazaar,
все остальные имена перемещаются в неё.  Иначе, должно быть указано
ровно два аргумента, и файл переименовывается. При наличии незафиксированных изменений в дереве, они будут перенесены 
и останутся как неподтвержденные после обновления. 
Чтобы отменить эти изменения используйте 'bzr revert'. Незафиксированные изменения
могут конфликтовать с изменениями, проведенными при обновлении. Если в родительской директории находится репозиторий, история ветви
будет сохранена в репозитории. Иначе, init создаёт одиночную ветвь,
которая несёт историю с собой в директории .bzr. Если ветвь уже существует, но рабочее дерево отсутствует, то оно
создаётся с помощью 'bzr checkout'. Если местоположение по умолчанию не установлено, первый pull установит
его (используйте --no-remember, чтобы избежать этой установки).  После
этого, вы можете опускать местоположение, чтобы использовать значение
по умолчанию.  Чтобы изменить значение по умолчанию, используйте опцию
--remember.  Значение будет сохранено, только если доступ к удалённому
местоположению будет успешен. Если местоположение отправки по умолчанию не установлено, первый push
установит её (используйте --no-remember, чтобы не устанавливать).
После этого вы можете опускать местоположение, чтобы использовать
значение по умолчанию.  Чтобы изменить значение по умолчанию,
используйте опцию --remember.  Значение будет сохранено, только если
доступ к удалённой локации будет успешен. Если вы хотите убедиться, что у вас иные изменения, чем в другой ветви,
сделайте слияние (см. bzr help merge) с другой ветвью, и закрепите эти
изменения. После этого вы сможете делать push без необходимости указывать
опцию '--overwrite'. Если вы хотите затереть ваши локальные изменения, чтобы ветвь совпадала
с удалённой, используйте pull --overwrite.  Это будет работать, даже если
ветвь является отклонением. Игнорировать указанные файлы или шаблоны. Игнорируются файлы вне окна просмотра. Окно просмотра: %s В нерекурсивном режиме, все указанные элементы добавляются, вне
зависимости от того, игнорировались ли они до этого.  Выводится
предупреждение, если какие-либо из указанных элементов уже были
под контролем Bazaar. В рекурсивном режиме (по умолчанию), файлы обрабатываются точно
также, но для директорий применяется другой алгоритм.  Для
директорий, которые уже имели версию, предупреждение не выводится.
Во всех директориях, независимо от того, находятся они под контролем
системы контроля версий или нет, производится поиск файлов, которые
не имеют версии и не игнорируются, и эти файлы добавляются.
Этот поиск входит рекурсивно в директории, имеющие версию.
Если не указано ни одного имени, предполагается '.'. В рекурсивном режиме, файлы больше, чем опция конфигурации
add.maximum_file_size, будут пропущены. Явно названные элементы
никогда не будут пропущены по этому критерию. В подробном режиме в каждый отчёт включается статистическая информация.
Чтобы увидеть расширенную статистическую информацию, используйте уровень
подробностей 2 или выше, указывая опцию -v несколько раз, например -vv. Информация по файлам конфигурации и журналам Информация по настройке аутентификации Информация по обработке конца строки Информация о том, что такое ветвь Информация о том, что такое рабочая копия Вместо использования:: Интеграция с Launchpad.net Некорректная ошибка %s. Должна быть в формате 'tracker:id'.  Больше информации об этом можно узнать, выполнив «bzr help bugs».
Закрепление отклонено. Найден некорректный шаблон игнорирования. %s Найдены некорректные шаблоны игнорирования. %s Найдены некорректные шаблоны игнорирования. %s Инвентарь восстановлен. Да, он крут!
 Ограничить вывод лишь первыми N ревизиями. Вывести все игнорируемые файлы и шаблоны игнорирования, из-за которых эти
файлы игнорируются. Выводить файлы определённого типа: file, directory, symlink. Вывести список файлов, удалённых в рабочем дереве.
     Вывести список файлов в дереве.
     Вывести игнорируемые файлы и шаблоны, которым они удовлетворяют. Вывести список ветвей, доступных в текущем местоположении. Искать идентификаторы в этом дереве. Почтовый клиент «%s» не поддерживает определение тела Преобразовать директорию в ветвь системы контроля версий. Вручную установить время закрепления, используя формат даты закрепления, напр. '2009-10-10 08:00:00 +0100'. Отметить ошибку, как исправленную в этой ревизии (см. «bzr help bugs»). Сообщение: %s Переместить только bzr-идентификатор файла, поскольку сам файл уже был перемещён. Переместить или переименовать файл. Перемещение репозитория в repository.backup Перемещение в корневую директорию пока не поддерживается NAME_PATTERN не должен быть абсолютным путём Имя корневой директории внутри экспортируемого файла. Никогда не изменять номера ревизий в существующем журнале. Только добавлять к нему ревизии. Новая ветвь, связанная с %s Новые ветви, создаваемые в директории репозитория, будут хранить
информацию о ревизиях в репозитории, а не в директории ветви.
Для ветвей с общей историей это уменьшает размер необходимого
пространства и ускоряет создание новых ветвей. В файле «%(filename)s» нет пакета. Нет отложенных изменений. Нет изменений для закрепления. Добавьте файлы, которые вы хотите закрепить, командой 'bzr add', или используйте --unchanged, чтобы принудительно сделать пустое закрепление. Не выводить ошибку, если существует, создавать родительские директории при необходимости. Нет справки для этой команды. Нет файлов, удовлетворяющих критерию. Нет новых ревизий или тегов для отправки. Нет новых ревизий для отправки. Местоположение для приёма неизвестно и не указано. Местоположение для отсылки изменений неизвестно и не указано. Нет ревизий для отправки. Ветвь для отсылки неизвестна и не задана Нет такого формата отправки '%s'. Для ошибки %s не указан трекер. Используйте форму 'tracker:id' или укажите систему трекинга ошибок по умолчанию, используя опцию `bugtracker`.
Больше информации об этом можно узнать, выполнив «bzr help bugs». Закрепление отклонено. Нет рабочего дерева для удаления Нет рабочего дерева. Игнорируется параметр --show-base Заметьте, что опция --short или -S даёт вывод статусных флагов,
подобный команде Subversion status. Чтобы получить вывод, похожий
на svn -q, используйте bzr status -SV. Заметьте, что при использовании аргумента -r с интервалом ревизий,
различия вычисляются между указанными ревизиями.  То есть, команда
не показывает изменения, представленные в первой ревизии интервала.
Это отличается от интерпретации интервалов ревизий командой «bzr log»,
которая включает первую ревизию интервала. Замечание: Экспорт в zip деревьев с именами файлов, содержащими не-ASCII
символы, не поддерживается. Замечание: Не забывайте перенаправлять стандартный вывод при использовании
этой команды на двоичном файле. Замечание: Местоположение можно указывать как в форме ветви, так и в
форме пути к файлу, содержащему директиву слияния, созданному bzr send. Нечего совмещать. Число уровней для вывода — 0 для всех, 1 для плоского вывода. После преобразования в рабочую копию, закрепления должны производиться
над основной ветвью, прежде чем они могут быть применены к локальной. В именах опций разрешены только символы ASCII Только один путь может быть указан, как аргумент --auto. Исключать только файлы, которые никогда не были закреплены. Удалить закрепления из локальной ветви только в рабочей копии. Показывать только файлы, имеющие версию. Настройки, управляющие запуском Bazaar Переписать только теги. Пакетный формат, использовавшийся в версиях 1.х. Представлен начиная с версии 0.92. Взаимодействует с хранилищами версий bzr 0.92 и ниже, но не читается в версиях bzr < 0.92.  PageFaultCount    %8d PagefileUsage     %8d КиБ Родительская директория для %s не существует.
Вы может предоставить --create-prefix, чтобы создать все родительские директории. Родитель «%s» не существует. Передать эти опции внешней программе diff. Неизвестный путь в начале или конце интервала ревизий: %s Шаблоны, начинающиеся с '!!' ведут себя как обычные шаблоны, но имеют
приоритет над шаблонами исключений '!'. Шаблоны, начинающиеся с '!', являются исключениями к шаблонам игнорирования
и имеют приоритет над обычными шаблонами. Такие исключения используются для
указания файлов, которые должны попасть под контроль системы версий, но
по умолчанию игнорируются. PeakPagefileUsage %8d КиБ PeakWorking       %8d КиБ Получить легковесную рабочую копию. Каждая операция над легковесной рабочей копией зависит от доступности ветви. Обычные рабочие копии позволяют производить стандартные операции, как diff и status, без такого доступа, а также поддерживают локальные закрепления. Совершить локальное закрепление в связанную ветвь. Локальные закрепления не выгружаются в основную ветвь до тех пор, пока не совершено нормальное закрепление. Сделать локальный приём в связанную ветвь. Локальный приём не накладывается на основную ветвь. Выполнить трехстороннее слияние. Выводить игнорируемые файлы. Вывести только номер версии. Вывести или установить имя ветви. Выводить пути относительно корня ветви. Выводить неизвестные файлы. Выводить файлы под контролем системы версий. PrivateUsage      %8d КиБ Данные профиля записаны в «%s». Отправлено до ревизии %d включительно. Чтение индексов Чтение данных инвентаря Рецепт для импорта дерева файлов:: Согласовать метаданные bzr в ветви. Рекурсивно входить в поддиректории. Рекурсивно сканировать ветви, а не смотреть в указанном местоположении. Отказываться закреплять, если в рабочем дереве есть неизвестные файлы. Отказывать в отсылке изменений, если в рабочем дереве есть незакреплённые изменения, --no-strict запрещает проверку. Связанные команды:: Исключить файлы или директории. Удалить псевдоним. Удалить директорию backup.bzr при успешном завершении. Удалить рабочее дерево даже, если оно содержит незакреплённые или отложенные изменения. Удалить рабочее дерево из данной ветви или рабочей копии. Удаление резервной копии ... Переименовать «%(this)s» => «%(other)s»? Репозиторий %r не поддерживает прямой доступ к текстам ревизий Формат репозитория Снова запустите обновление после исправления конфликтов. Номера ревизий всегда относительны к исходной ветке. Номер ревизии имеет смысл только для одиночных ревизий, а не для диапазонов. Для подробностей запустите «bzr check». Смотрите подробности о синтаксисе шаблонов в «bzr help patterns». Смотрите bzr help %s, чтобы получить подробности и примеры.

 Выбранные изменения уничтожены. Выбранные изменения: Вернуть файлы в рабочем дереве к содержанию предыдущей ревизии. Установить время изменения файлов на время последней ревизии, в которой они были изменены. Установить префиксы, добавляемые к старым и новым именам файлов, как значения, разделённые двоеточием (напр. «old/:new/»). Установить имя автора, если оно отличается от имени закрепляющего. Выбрать ветвь рабочей копии и обновить. Установить, удалить или отобразить псевдонимы. Отложить %d изменений? Отложить добавление файла «%(path)s»? Отложить двоичные изменения? Отложить изменение «%s» из %(other)s в %(this)s? Отложить изменение цели «%(path)s» из «%(other)s» в «%(this)s»? Отложить удаление файла «%(path)s»? Отложить переименование «%(other)s» => «%(this)s»? Отложить? Показать изменения, сделанные в каждой ревизии, в виде патча. Показать номер текущей ревизии. Показать изменения в рабочем дереве, между ревизиями или ветвями. Показать действительность цифровой подписи. Показывать файлы, изменённые в каждой ревизии. Выводить от старых к новым. Показать журнал истории ветви или подмножества ветви. Показать информацию о рабочем дереве, ветви или репозитории. Показать только указанную ревизию. См. также «help revisionspec». Показать список переименованных файлов.
     Показать журналы принятых ревизий. Показывать слияния, как это делает --levels 0. Показать или задать ID пользователя bzr. Показывать ревизии, авторы которых удовлетворяют этому выражению. Показывать ревизии, ошибки которых удовлетворяют этому выражению. Показывать ревизии, закрепитель которых удовлетворяет этому выражению. Показывать ревизии, комментарий которых удовлетворяет этому выражению. Показывать ревизии, свойства которых удовлетворяют этому выражению. Показать номер ревизии рабочего дерева. Показать корневую директорию дерева. Вывести версию bzr. Показать, что могло бы быть сделано, но реально ничего не делается. Поскольку легковесная рабочая копия, это нечто большее, чем рабочее
дерево, эту команду нельзя выполнить внутри неё. Некоторые умные серверы или протоколы *может быть* будут размещать
рабочее дерево в месте назначения в будущем. Указать формат данной ветви. См. «help formats». Указать формат для этого репозитория. Описание форматов см. в «bzr help formats». Заменить рабочую копию в текущей директории новой ветвью. Переключение на ветвь: %s Тегом можно пометить только отдельную ревизию, не интервал Взять сообщение о закреплении из этого файла. Целевая директория «%s» уже существует. Опция --verbose отобразит ревизии, загруженные с использованием опции
конфигурации log_format. Вы можете использовать другой формат, перегрузив
её опцией -Olog_format=<другой_формат>. Опция --verbose отобразит ревизии, выгруженные с использованием опции
конфигурации log_format. Вы можете использовать другой формат, перегрузив
её опцией -Olog_format=<другой_формат>. Ветвь *ДОЛЖНА* быть на системе, поддерживающей список файлов, такой как
локальный диск или SFTP. В ветви {0} отсутствует ревизия {1}. Используемый способ удаления файлов. Фильтрация, сортировка и отображение информации для каждой ревизии
могут управляться, как объяснено ниже.  По умолчанию, выводятся все
ревизии отсортированные так, что более новые ревизии находятся выше
более старых и потомки находятся выше предков.  Если ревизия слияния
отображаются, то они отображаются с отступом, относительно ревизии,
в которой происходило слияние. Аргументы пределов должны быть целыми числами. Аргумент предела должен быть целым числом. Имя пути в старом дереве. Репозиторий {0} не содержит ревизии {1}. Корень это ближайшая родительская директория с управляющей
директорией .bzr. Синонимы 'clone' и 'get' для этой команды устарели. Целевая ветвь не будет иметь рабочего дерева, поскольку это накладно
и не поддерживается удалёнными файловыми системами. Дерево не повреждено. Возможно, вы хотели выполнить «bzr revert». Используйте «--force», если вы уверены, что хотите использовать reset. Проверки рабочего дерева и ветви выведут что-либо только в случае
обнаружения проблемы. Поля вывода проверки репозитория следующие: Рабочее дерево для %(basedir)s изменилось с последнего закрепления, но merge требует, чтобы оно не содержало изменений Следовательно, просто вызов 'bzr add' добавит к системе контроля
версий все файлы, которые ей неизвестны. Формат этой ветви не может быть установлен в append-revisions-only. Попробуйте --default. Может исправить несовпадения данных, вызванные обновлениями bzr.
Эту команду стоит запускать только, если 'bzr check' или разработчик
bzr предложат вам сделать это. Эта команда проверяет различные инварианты о хранилище ветки и репозитория 
для обнаружения повреждения данных или ошибок в bzr. Эта команда работает только на ветвях, которые не являются
отклонениями. Ветвь считается отклонением, если последнее
закрепление в ветви назначения не было совмещено (явно или
неявно) с родителем. Эта команда выведет имена всех ветвей в текущем местоположении. Эта команда выведет все известные местоположения и форматы, связанные
с деревом, ветвью или репозиторием. Это рабочая копия. Ветвь (%s) необходимо обновлять отдельно. Это эквивалентно числу ревизий в этой ветви. Это эквивалентно созданию директории и её добавлению. Эта команда сообщает о файлах с версиями и неизвестных файлах,
группируя их по состоянию.  Возможные состояния: Чтобы сравнить рабочую копию с определённой ревизией, укажите
ревизию в соответствующем аргументе. Чтобы воссоздать рабочее дерево, используйте «bzr checkout». Чтобы удалить шаблоны из списка игнорируемых файлов, отредактируйте
файл .bzrignore. После добавления, редактирования или удаления этого
файла, как неявно с помощью этой команды, так и явно при помощи
редактора, не забывайте закреплять его. Чтобы получить определённую ревизию ветви, передайте её в параметре
--revision, напр. «branch foo/bar -r 5». Чтобы получить определённую ревизию ветви, добавьте параметр --revision,
например «checkout foo/bar -r 5». Заметьте, что такое дерево будет сразу
просрочено [так что в не сможете закрепить изменения], но это может быть
полезно, к примеру, для просмотра старого кода. Чтобы просмотреть игнорируемые файлы, используйте 'bzr ignored'.
Изменения текста файлов можно увидеть, используя 'bzr diff'. Чтобы увидеть, какие файлы изменились в определённой ревизии, или
между двумя ревизиями, укажите в аргументе ревизии интервал.
Это даёт такой же результат, как вызов 'bzr diff --summarize'. Дерево в актуальной ревизии %d. Дерево в актуальной ревизии {0} ветви {1} Преобразовать эту ветвь в зеркало другой ветви. Тип файла для экспорта. Не удалось соединиться с текущей основной ветвью %(target)s: %(error)s Для принудительного переключения используйте --force. Не удалось обработать версию пакета %(version)s: %(msg)s Не удалось импортировать библиотеку «%(library)s»: %(error)s Неизвестным ключом {0} подписано {1} закрепление Неизвестным ключом {0} подписаны {1} закрепления Неизвестным ключом {0} подписаны {1} закреплений Ошибка %s не распознана. Закрепление отклонено. Извлечение из стека Неподдерживаемый формат экспорта: %s Обновить зеркало этой ветви. Обновить рабочее дерево до новой ревизии. Обновлено до ревизии %d. Обновлено до ревизии {0} ветви {1} Обновить репозиторий, ветвь или рабочее дерево до нового формата. Обновить до определённого формата. Подробности см. в «bzr help formats». Обновление %s Обновление bzrdirs Обновление {0} {1} ... Используйте bzr resolve после устранения проблемы. Смотрите также bzr conflicts. Использовать короткие статусные пометки. Использовать эту команду для сравнения. Используйте эту команду для создания пустой ветви или
перед импортом существующего проекта. Использование Bazaar с Launchpad.net Используются изменения с id «%d». Используется сохранённое местоположение родителя: %s
 Используется сохранённое местоположение отсылки: %s Используется сохранённое {0} местоположение «{1}» для определения изменений, которые необходимо отправить. Используется общий репозиторий: %s
 Используется {0} {1} Проверить и подтвердить структуру рабочего дерева, целостность веток и историю репозитория. Предупреждение: следующие файлы под контролем системы версий и удовлетворяют шаблонам игнорирования:
%s
Эти файлы будут продолжать под контролем системы версий, пока вы не выполните 'bzr remove'.
 Какие имена выводить, как авторов — first, all или committer. При закреплении в иной системе контроля версий, не выгружать данные, которые нельзя в ней представить. Если не указано сообщение, показывать изменения  формате diff вместе с обзором состояния в редакторе сообщения. Если после выпуска нового релиза Bazaar изменился  формат по-умолчанию , 
во время определенных операций Вы можете быть извещены о том, что Вам необходимо сделать обновление. 
Обновление до нового формата может улучшить производительность или сделать доступными новые опции. 
В то же время, это может ограничить взаимодействие со старыми репозиториями 
или старыми версиями Bazaar. WorkingSize       %8d КиБ WorkingSize {0:>7}КиБ	PeakWorking {1:>7}КиБ	{2} Вывести содержимое файла из определённой ревизии на стандартный вывод. Вы можете указать только одно из revision_id или --revision Вы можете использовать это чтобы открыть просмотреть старые ревизии или 
обновить рабочее дерево, устаревшее для свой ветки. Вы не можете удалить рабочее дерево из легковесной рабочей копии Вы не можете удалить внешнее рабочее дерево Вы не можете указать ревизию NULL. Нельзя обновить только отдельный файл или директорию, поскольку каждое
рабочее дерево Bazaar имеет только одну базовую ревизию.  Если вы хотите
восстановить файл, который был удалён на локальной машине, используйте
'bzr revert', а не 'bzr update'.  Если вы хотите восстановить файл до
состояния в предыдущей ревизии, используйте 'bzr revert' с опцией '-r',
или используйте 'bzr cat', чтобы записать старое содержимое этого файла
в новое местоположение. Вы должны предоставить --revision или revision_id Ваши локальные закрепления теперь будут отображаться в 'bzr status' как незавершённые слияния, и их можно закрепить с помощью 'bzr commit'. добавлены добавлены
    Имеют версию в рабочей копии, но отсутствуют в предыдущей
    ревизии. добавлен %s
 добавление файла в ветви нет ревизии %s
bzr update --revision работает только для ревизий в истории ветви bzr %s --revision принимает ровно один идентификатор ревизии bzr %s --revision принимает одно или два значения. bzr %s не принимает две ревизии в различных ветвях. bzr alias --remove требуется псевдоним для удаления. bzr cat --revision принимает ровно один спецификатор ревизии bzr diff --revision принимает ровно ожин или два аргумента bzr send принимает не более двух идентификаторов ревизии bzr status --revision принимает ровно один или два идентификатора ревизии bzr update --revision принимает ровно одну ревизию bzr update может обновить только всё дерево, не файл или поддиректорию bzr: ОШИБКА (игнорирована): %s нельзя переместить корень ветви нельзя указать оба --from-root и PATH нельзя обновить из формата bzrdir %s проверена ветвь {0} формата {1} команде {0!r} требуется один или больше {1} команде {0!r} требуется аргумент {1} удалён %s лишний аргумент к команде {0}: {1} не удалось очистить {0}: {1} не удалось сбросить состояние дерева {0} завершено обнаружена ошибка:%s Для ignore требуется как минимум одно из NAME_PATTERN или --default-rules. пропуск {0} из-за условия «{1}»
 указан неизвестный вид %r указан неверный вид файла изменили вид
    Вид файла изменился (например, файл стал директорией). слушаю на порту: %s log — это утилита bzr по умолчанию для просмотра истории ветви.
Используемая ветвь определяется первым параметром. Если не задано
параметров, выводится журнал ветви, содержащей рабочюю директорию.
Вот несколько простых примеров:: основание слияния - ревизия %s
 отсутствует %s отсутствует аргумент файл изменены изменены
    Текст изменился с предыдущей ревизии. некорректный номер ревизии: %r укажите сообщение о закреплении, используя --message либо --file укажите либо --message, либо --file python-gpgme не устонвлен, он нужен для верификации подписей исключены
    Имеют версию в предыдущей ревизии но исключены или удалены
    в рабочей копии. удаление файла переименованы переименованы
    Путь к этим файлам изменился с предыдущей ревизии;
    текст также мог измениться.  Это состояние включает
    файлы, родительская директория которых была переименована. повторяемые тексты файлов
    Это общее число повторяемых текстов, встреченных
    в проверенных ревизиях.  Тексты могут повторяться, если записи
    файлов изменились, но их содержимое осталось прежним.  Оно не
    указывает на проблему. репозиторий преобразован ревизии
    Это просто число проверенных ревизий.  Оно не указывает
    на проблему. пропуск {0} (более, чем {1} из {2} байт) начинается обновление %s subunit не доступен. Нужно установить subunit, чтобы использовать --subunit. чтобы переместить несколько файлов, назначение должно быть директорией под контролем системы версий без изменений уникальные тексты файлов
    Это общее число уникального содержимого файлов, встреченного
    в проверенных ревизиях.  Оно не указывает на проблему. неизвестны
    Не имеют версии и не подходят под шаблон игнориируемых файлов. неизвестная команда «%s» предки без ссылок
    Тексты, являющиеся предками других текстов, но не имеющие
    корректных ссылок в истории ревизии.  Это небольшая
    проблема, с которой Bazaar может разобраться. версифицированные файлы
    Это просто число проверенных файлов под контролем системы
    версий.  Оно не указывает на проблему. можно просто использовать:: {0!r} не представлен в ревизии {1} {0} => {1} {0} и {1} взаимоисключающи {0} закрепление с неизвестным ключом {0} закрепления с неизвестным ключом {0} закреплений с неизвестным ключом {0}теперь на стеке {1}
 