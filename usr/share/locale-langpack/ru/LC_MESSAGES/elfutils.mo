��    T      �  q   \            !     :  	   Y     c     t     �     �     �     �     �  	   �     �     	     !     ;  	   R     \     r     w     �  1   �  	   �     �     �     	  !   0	     R	     k	     �	     �	     �	     �	     �	     
  !   "
  -   D
     r
     �
     �
     �
     �
     �
     �
          .  $   A     f     �     �     �     �     �          &     7     S     f     �     �     �     �     �     �     �       !        8  "   P     s     �     �     �     �     �  '   �               /     =     Z     w     �     �    �  )   �  K   �     0  (   @  ,   i  2   �  ,   �     �     �       $     :   D  )     C   �  2   �        3   4     h  1   q  7   �  d   �  	   @  5   J  3   �  C   �  L   �  <   E  6   �  =   �  4   �  ;   ,  0   h  <   �  D   �  I     X   e  L   �  ?     '   K  %   s  '   �  2   �  6   �  :   +  +   f  F   �  @   �  8     @   S  A   �  I   �  =      E   ^  '   �  &   �  *   �  J     B   i     �  %   �  B   �     4  '   P  !   x     �  Q   �  4     >   ;  %   z     �  0   �  "   �          (  ;   D  #   �  )   �  #   �  ;   �  >   .   0   m      �   #   �                 J   I       ?       H   :   	   '   >                   -                  &   +       *       C   S   Q   N                 B       %   L   F         
   5   $   =       @   3      #              M   0              9              P   6   4   G      7      /           A   2         .                        E   T   "         K   1      !   (       ;      O   ,   D   R   )                        <   8        '%s' is not a DSO or PIE At least one input file needed Commands: Control options: Delete files from archive. Display content of archive. Extract files from archive. FILE FILE... FILE1 FILE2 I/O error Invalid number of parameters.
 Kernel with all modules LZMA decompression failed Move files in archive. No errors No symbol table found PATH Print files in archive. Quick append files to archive. Replace existing or insert new file into archive. [ADDR...] address out of range archive name required bzip2 decompression failed cannot change mode of output file cannot create hash table cannot create new file cannot create output file cannot find kernel modules cannot find kernel or modules cannot find symbol '%s' cannot get ELF header cannot get ELF header '%s': %s cannot get ELF header of '%s': %s cannot get content of section %zu in '%s': %s cannot get data for symbol %zu
 cannot get symbol in '%s': %s cannot open %.*s cannot open %s cannot open '%s' cannot open archive '%s' cannot open archive '%s': %s cannot open input file cannot read %s: %s cannot read ELF header of %s(%s): %s cannot read ELF header: %s
 cannot read archive `%s': %s cannot read content of %s: %s cannot read data from file cannot rename output file cannot write data to file debug information too big duplicate symbol error during output of data failed to write %s file has no program header gzip decompression failed invalid DWARF invalid ELF file invalid ELF header size: %hd
 invalid access invalid command invalid file invalid parameter invalid program header size: %hd
 invalid reference value invalid section type for operation invalid version memory exhausted more than one '-m' parameter no entries found no error not implemented only one option of -G and -r is allowed out of memory the archive '%s' is too large unknown error unknown object file type %d
 unknown object file version
 unknown option `-%c %s' unknown type unknown version Project-Id-Version: elfutils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-11 08:48+0100
PO-Revision-Date: 2014-08-11 12:45+0000
Last-Translator: ☠Jay ZDLin☠ <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 18:10+0000
X-Generator: Launchpad (build 18115)
 '%s' не является DSO или PIE Требуется как минимум оодин входной файл Команды: Параметры управления: Удалить файлы из архива. Показать содержимое архива Извлечь файлы из архива. ФАЙЛ ФАЙЛ… ФАЙЛ1 ФАЙЛ2 Ошибка ввода/вывода Некорректный номер параметров.
 Ядро со всеми модулями Не удалось разархивировать архив  LZMA Переместить файлы в архиве. Ошибок нет Не найдена таблица символов ПУТЬ Распечатать файлы в архиве Срочно добавить файлы в архив. Заменить существующий или добавить новый файл в архив. [ADDR...] адрес за пределами диапазона требуется задать имя архива Не удалось разархивировать архив bzip2 невозможно сменить режим конечного файла не удаётся создать таблицу хешей не удаётся создать новый файл невозможно создать выходной файл не удалось найти модули ядра не удалось найти ядро или модули не удаётся найти символ '%s' невозможно получить ELF заголовок не удаётся получить заголовок ELF '%s': %s не удаётся получить заголовок ELF из '%s': %s не удаётся получить содержимое секции %zu из '%s': %s не удаётся получить данные для символа %zu
 не удаётся получить символ из '%s': %s не удаётся открыть %.*s не удаётся открыть %s не удаётся открыть '%s' не удаётся открыть архив '%s' не удаётся открыть архив '%s': %s не удаётся открыть входной файл не удаётся прочесть %s: %s не удаётся прочесть заголовок ELF %s(%s): %s не удаётся прочесть заголовок ELF: %s
 не удаётся прочесть архив `%s': %s не удаётся прочесть содержимое %s: %s не удаётся прочесть данные из файла невозможно переименовать конечный файл не удаётся записать данные в файл отладочная информация слишком велика повторяющийся символ ошибка вывода данных не удалось записать в %s в файле отсутствует заголовок программы Не удалось разархивировать gzip-архив неправильный DWARF недопустимый ELF файл некорректный размер заголовка ELF: %hd
 ошибка доступа недопустимая команда недопустимый файл неверный ключ некорректный размер заголовка программы: %hd
 неверное ссылочное значение неверный тип раздела для операции недопустимая версия память исчерпана более чем один параметр '-m' не найдено записей нет ошибок не реализовано разрешены только параметры -G и -r закончилась память архив '%s' слишком велик неизвестная ошибка неизвестный тип файла объекта %d
 неизвестная версия файла объекта
 неизвестный параметр `-%c %s' неизвестный тип неизвестная версия 