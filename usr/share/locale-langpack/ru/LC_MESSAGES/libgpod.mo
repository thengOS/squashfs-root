��    m      �  �   �      @	  *   A	     l	     t	     �	     �	     �	     �	  A   �	     
       
      A
     b
  '   
     �
     �
  !   �
                =  	   ]     g     t  5   �     �     �     �     �     �                    *     8     L     a     q     �     �     �     �     �     �  
   �     �     �     �               /     E  3   a     �     �     �     �     �  D   �  E   !     g     p     y     �     �     �     �     �     �     �               )     8     G     W     h     w     �     �     �     �     �     �     �     �  b     .   f      �     �     �     �     �                     -  C   6     z       
   �     �     �  	   �  
   �     �     �  5   �  D     D   M  �  �  l   +     �     �      �     �     �  F   �  `   @  /   �  =   �  ?     +   O  R   {  1   �  (      <   )  8   f  N   �  6   �     %     A  2   `  a   �  !   �            8     Y     s     �     �  !   �  #   �  4     @   <      }      �     �     �     �     �          /     K     a          �  $   �  &   �  &   �  2     �   K  /   �  2   �  #   0     T     Z  �   k  |   �  +   l     �  2   �  2   �  2     2   B     u  #   }  #   �  !   �  #   �           "      ;      T      m      �      �      �       �   +   �      (!  !   .!     P!  !   p!     �!  �   �!  S   x"  :   �"  -   #     5#  -   J#  !   x#  !   �#     �#     �#     �#  n   �#     f$     k$  
   r$     }$     �$  	   �$  
   �$     �$     �$  h   �$  g   3%  g   �%         >   S       5   ?       D   g      X              d   A          U      G   (             K      W             k             <      b       P   C           m   V       H       \         B   	   ^          4   '       7   `       O      0   1   
   &   f           Q      E   I       !   i   #       $      e   Z   9   *              ;   F          %      +   j       @       N   =           T   c   Y   )   3   ,   .       l          ]   -   J          2   /   R                  h          L      6       a   :   _      M      "               [   8            Artwork support not compiled into libgpod. Classic Classic (Black) Classic (Silver) Color Color U2 Could not access file '%s'. Could not find corresponding track (dbid: %s) for artwork entry.
 Could not find on iPod: '%s'
 Could not open '%s' for writing. Device directory does not exist. Error creating '%s' (mkdir)
 Error initialising iPod, unknown error
 Error initialising iPod: %s
 Error removing '%s' (%s). Error renaming '%s' to '%s' (%s). Error when closing '%s' (%s). Error writing playlists (hphs) Error: '%s' is not a directory
 Grayscale Grayscale U2 Illegal filename: '%s'.
 Illegal seek to offset %ld (length %ld) in file '%s'. Invalid Mini (1st Gen.) Mini (2nd Gen.) Mini (Blue) Mini (Gold) Mini (Green) Mini (Pink) Mini (Silver) Mobile Phones Mountpoint not set. Mountpoint not set.
 Nano (1st Gen.) Nano (2nd Gen.) Nano (Black) Nano (Blue) Nano (Green) Nano (Orange) Nano (Pink) Nano (Purple) Nano (Red) Nano (Silver) Nano (White) Nano (Yellow) Nano Video (3rd Gen.) Nano Video (4th Gen.) Nano touch (6th Gen.) Nano with camera (5th Gen.) Not a Play Counts file: '%s' (missing mhdp header). OTG Playlist OTG Playlist %d Path not found: '%s'. Photo Photo Library Play Counts file ('%s'): entry length smaller than expected (%d<12). Play Counts file ('%s'): header length smaller than expected (%d<96). Playlist Podcasts Regular (1st Gen.) Regular (2nd Gen.) Regular (3rd Gen.) Regular (4th Gen.) Shuffle Shuffle (1st Gen.) Shuffle (2nd Gen.) Shuffle (3rd Gen.) Shuffle (4th Gen.) Shuffle (Black) Shuffle (Blue) Shuffle (Gold) Shuffle (Green) Shuffle (Orange) Shuffle (Pink) Shuffle (Purple) Shuffle (Red) Shuffle (Silver) Shuffle (Stainless) Touch Touch (2nd Gen.) Touch (3rd Gen.) Touch (4th Gen.) Touch (Silver) Unable to retrieve thumbnail (appears to be on iPod, but no image info available): filename: '%s'
 Unexpected image type in mhni: %d, offset: %d
 Unexpected mhod string type: %d
 Unexpected mhsd index: %d
 Unknown Unknown command '%s'
 Video (1st Gen.) Video (2nd Gen.) Video (Black) Video (White) Video U2 You need to specify the iPod model used before photos can be added. iPad iPhone iPhone (1) iPhone (Black) iPhone (White) iPhone 3G iPhone 3GS iPhone 4 iPod iTunesDB corrupt: no MHOD at offset %ld in file '%s'. iTunesStats file ('%s'): entry length smaller than expected (%d<18). iTunesStats file ('%s'): entry length smaller than expected (%d<32). Project-Id-Version: libgpod
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-04-13 08:40+0000
PO-Revision-Date: 2013-04-12 08:12+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 Библиотека libgpod была скомпилирована без поддержки графики. Classic Classic (Чёрный) Classic (Серебристый) Цветное Цветное U2 Невозможно получить доступ к файлу '%s'. Не удалось найти дорожку (dbid: %s) данного изображения.
 Не удалось найти на iPod: '%s'
 Невозможно открыть "%s" для записи. Директория устройства не доступна Ошибка создания '%s' (mkdir)
 Ошибка инициализации iPod, неизвестная ошибка
 Ошибка инициализации iPod: %s
 Ошибка удаления '%s' (%s). Ошибка переименования '%s' в '%s' (%s). Ошибка во время закрытия '%s' (%s). Ошибка записи списков воспроизведения (hphs) Ошибка: '%s' не является папкой
 Оттенки серого Оттенки серого U2 Недопустимое имя файла: '%s'.
 Неверный переход к позиции %ld (при длине %ld) в файле '%s'. Неверное значение Mini (1-ое поколение) Mini (2-ое поколение) Мини (голубой) Мини (золотой) Мини (зеленый) Мини (розовый) Мини (серебрянный) Мобильные телефоны Место подключения не задано. Точка монтирования не установлена
 Nano (1-ое поколение) Nano (2-ое поколение) Nano (черный) Nano (Голубой) Nano (Зелёный) Nano (Оранжевый) Nano (Розовый) Nano (Фиолетовый) Nano (Красный) Nano (серебристый) Nano (белый) Nano (Жёлтый) Nano Video (3-е поколение) Nano Video (4-ое поколение) Nano touch (6-ое поколение) Nano с камерой (5-ое поколение) Неверный файл счетчика воспроизведений: '%s' (отсутствует заголовок mhdp). Список воспроизведения OTG Список воспроизведения OTG %d Адрес не найден: '%s'. Photo Фототека Файл счетчика воспроизведений ('%s'): размер записи слишком короткий (%d<12). Файл счетчика воспроизведений ('%s'): заголовок слишком короткий (%d<96). Список воспроизведения Подкасты Стандартный (1-ое поколение) Стандартный (2-ое поколение) Стандартный (3-ое поколение) Стандартный (4-ое поколение) Shuffle Shuffle (1-ое поколение) Shuffle (2-ое поколение) Shuffle (3-е поколение) Shuffle (4-ое поколение) Shuffle (Чёрный) Shuffle (Голубой) Shuffle (Золотой) Shuffle (Зелёный) Shuffle (Оранжевый) Shuffle (Розовый) Shuffle (Фиолетовый) Shuffle (Красный) Shuffle (Серебристый) Shuffle (Нержавеющая сталь) Touch Touch (2-ое поколение) Touch (3-е поколение) Touch (4-ое поколение) Touch (Серебристый) Невозможно принять миниатюры (они располагаются на iPod, но сведения о изображениях недоступны): имя файла: '%s'
 Непредвиденный тип образа в mhni: %d, смещение: %d
 Непредвиденный тип строки mhod: %d
 Неизвестный mhsd индекс: %d
 Неизвестно Неизвестная команда «%s»
 Video (1-ое поколение) Video (2-ое поколение) Видео (черный) Видео (белый) Video U2 Необходимо указать модель iPod, перед добавлением фотографий. iPad iPhone iPhone (1) iPhone (Чёрный) iPhone (Белый) iPhone 3G iPhone 3GS iPhone 4 iPod Неполадка с iTunesDB: отсутствует MHOD в смещении %ld, в файле '%s'. Файл iTunesStats ('%s'): длинна записи меньше, чем ожидалось (%d<18). Файл iTunesStats ('%s'): длинна записи меньше, чем ожидалось (%d<32). 