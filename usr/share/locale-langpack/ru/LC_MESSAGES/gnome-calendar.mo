��    x      �  �   �      (
     )
     I
     O
     U
     k
     q
     z
     |
     
     �
     �
     �
     �
     �
  *   �
     �
               !     <     M     _     n     �     �     �  	   �     �     �     �     �  $   �  )        :     >     K     \     s     x     �     �  �   �     Q     _     l  �   x     W     ^     {     �     �     �     �     �     �     �  	   �                          $     '     7      <  &   ]     �     �     �     �     �     �     �     �     �            '   &     N     U     [     a     x  >   �     �     �     �  )   �       �   "     �     �     
          5     A     a     f     m  
   s     ~     �     �     �     �     �     �          /     J     d     ~     �     �     �     �               1  �  D  R   �          %  -   +     Y     _     h     j  9   r     �     �  #   �  #   �     !  c   >     �  -   �     �  )   �       %   =     c  F   �  )   �     �  9        F     Y  (   f     �     �  \   �  a   
     l     u  #   �  (   �     �  -   �  4   !  
   V  J  a     �     �     �  �  �     �  8   �     4   +   Q      }      �   
   �   +   �   %   �   7   �      4!      J!     k!     z!     �!     �!  *   �!     �!  B   �!  H   0"  N   y"  \   �"     %#  
   A#     L#     T#  !   a#     �#     �#  ,   �#     �#     �#     p$     }$     �$  A   �$  ,   �$  �   %     �%  '   �%  #   �%  [   �%     F&  �  _&     7(  9   U(  !   �(  *   �(     �(  5   �(     ()     /)     H)     \)  !   k)     �)     �)     �)     �)  
   �)  
   �)     �)     �)     *     '*     A*  
   ]*     h*     }*     �*     �*     �*  %  �*                   C   .       p   a   #   i   E      "   2   9   8                 &   +       *   n                       t      N   P      V   K   
          ]   G   m                      A   o   5   \   U              -   l   w   s              7      x       ?           g   Z           /   1   >   (      r         M       W              '   _   ^      e       [           B   Y      @           %       k      X   :   L   Q   F   b   )   c   J   v       O       H   $       <       f   0   ;       6   `   !   S               	   ,   j   R           =   q      3      T      D       d                 4   u   I   h    %s (this calendar is read-only) %s AM %s PM - Calendar management 00:00 00:00 PM : AM Access, and manage calendar Account Add Add Calendar Add Eve_nt… Add Event… Add new events to this calendar by default All day Another event deleted Calendar Calendar <b>%s</b> removed Calendar Address Calendar Settings Calendar files Calendar for GNOME Calendar management Calendar name Calendar;Event;Reminder; Calendars Cancel Click to set up Color Connect Copyright © %d The Calendar authors Copyright © %d–%d The Calendar authors Day Delete event Display calendar Display version number Done Edit Calendar Edit Details… Ends Enter the address of the calendar that you want to add. If the calendar belongs to one of your online accounts, you can add it through the <a href="GOA">online account settings</a>. Event deleted From File… From Web… GNOME Calendar is a simple and beautiful calendar application designed to perfectly fit the GNOME desktop. By reusing the components which the GNOME desktop is build on, Calendar nicely integrates with the GNOME ecosystem. Google List of the disabled sources Location Manage your calendars Microsoft Exchange Midnight Month New Event from %s to %s New Event on %s New Local Calendar… No events No results found Noon Notes Off On Online Accounts Open Open calendar on the passed date Open calendar showing the passed event Open online account settings Other event Other %d events Other events Overview PM Password Remove Calendar Save Search for events Select a calendar file Settings Sources disabled last time Calendar ran Starts Title Today Try a different search Type of the active view Type of the active window view, default value is: monthly view Undo Unnamed Calendar Unnamed event Use the entry above to search for events. User We aim to find the perfect balance between nicely crafted features and user-centred usability. No excess, no lacks. You'll feel comfortable using Calendar, like you've been using it for ages! Window maximized Window maximized state Window position Window position (x and y). Window size Window size (width and height). Year _About _Quit _Search… _Synchronize event date format%B %d ownCloud shortcut windowClose window shortcut windowGeneral shortcut windowGo back shortcut windowGo forward shortcut windowMonth view shortcut windowNavigation shortcut windowNew event shortcut windowNext view shortcut windowPrevious view shortcut windowSearch shortcut windowShortcuts shortcut windowShow help shortcut windowShow today shortcut windowView shortcut windowYear view translator-credits Project-Id-Version: gnome-calendar master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-calendar&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-05-18 08:25+0000
PO-Revision-Date: 2016-05-25 20:01+0000
Last-Translator: Stas Solovey <whats_up@tut.by>
Language-Team: Русский <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 19:34+0000
X-Generator: Launchpad (build 18115)
Language: ru
 %s (данный календарь в режиме «только чтение») %s AM %s PM — управление календарем 00:00 00:00 PM : д. п. Доступ и управление календарём Учётная запись Добавить Добавить календарь _Добавить событие… Новое событие… Добавлять новые события в этот календарь по умолчанию Весь день Ещё одно событие удалено Календарь Календарь <b>%s</b> удалён Адрес календаря Параметры календаря Файлы календаря Календарь для среды рабочего стола GNOME Управление календарем Имя календаря Календарь;Событие;Напоминание; Календари Отмена Нажмите для установки Цвет Подключить Авторские права © %d авторы приложения «Календарь» Авторские права © %d–%d авторы приложения «Календарь» День Удалить событие Показать календарь Показать номер версии Выполнено Редактировать календарь Редактировать подробности… Конец Введите адрес календаря, который нужно добавить. Если календарь относится к одной из сетевых учётных записей, его можно добавить через <a href="GOA">параметры сетевых учётных записей</a>. Событие удалено Из файла… Из интернета… Календарь GNOME представляет собой простое и красивое приложение календаря, разработанное, чтобы полностью соответствовать рабочему столу GNOME. За счет использования тех же компонентов из которых состоит рабочий стол GNOME, календарь красиво интегрируется с экосистемой среды GNOME. Google Список отключённых источников Местоположение Управление календарями Microsoft Exchange Полночь Месяц Создать событие с %s по %s Создать событие на %s Создать локальный календарь… Нет событий Ничего не найдено Полдень Примечания Выключено Включено Сетевые учётные записи Открыть Открыть календарь на прошедшей дате Открыть календарь на прошедшем событии Открыть параметры сетевых учётных записей Другое %d событие Другие %d события Другие %d событий Другие события Обзор п. п. Пароль Удалить календарь Сохранить Поиск событий Выберите файл календаря Параметры Источники, отключённые при последнем запуске приложения «Календарь» Начало Заголовок Сегодня Попробуйте другой поисковый запрос Текущий режим просмотра Текущий режим просмотра, по умолчанию используется месячный просмотр Вернуть Безымянный календарь Безымянное событие Используйте строку ввода выше для поиска событий. Пользователь Мы стремимся найти идеальный баланс между хорошо созданными функциями и ориентированным на пользователя удобством использования. Нет излишеств, нет недостатков. Вы будете чувствовать себя комфортно, используя календарь, будто вы используете его уже годы! Окно развёрнуто Окно имеет максимальный размер Расположение окна Расположение окна (x ; y). Размер окна Размер окна (ширина и высота). Год _О приложении _Завершить _Найти… _Синхронизировать %d %b. ownCloud Закрыть окно Основное Назад Далее Просмотр месяца Навигация Новое событие Следующий вид Предыдущий вид Поиск Комбинации Показать справку Показать сегодня Вид Просмотр года Юрий Мясоедов <ymyasoedov@yandex.ru>, 2012-2015.
Станислав Соловей <whats_up@tut.by>, 2014-2016.

Launchpad Contributions:
  Aleksey Kabanov https://launchpad.net/~ak099
  Eugene Marshal https://launchpad.net/~lowrider
  Stas Solovey https://launchpad.net/~whats-up 