��    /      �  C                A   /     q     �  &   �     �     �                :     K  �   b     �                2     O     e     s     �  "   �     �     �     �  +   �  ,   "     O  !   f     �     �     �     �  �   �     �  U   �     	     7	     N	    ]	     u
     ~
     �
  	   �
     �
     �
  
   �
  �  �
  =   �  �   �  -   V  /   �  9   �  #   �       8   +  2   d  )   �  9   �    �  '     b   6  @   �  >   �  (        B  &   a  $   �  <   �  $   �  +     $   ;  |   `  u   �  8   S  p   �  N   �     L     Y  "   x  �  �  6   d  �   �  r   Z  ;   �     	  _  "     �     �     �     �  r   �  W   K     �     	           "          #       *         
      /          !                                                            -                     &   ,                      +         )                $       %      '   .   (                    Resume normal boot (Use arrows/PageUp/PageDown keys to scroll and TAB key to select) === Detailed disk usage === === Detailed memory usage === === Detailed network configuration === === General information === === LVM state === === Software RAID state === === System database (APT) === CPU information: Check all file systems Continuing will remount your / filesystem in read/write mode and mount any other filesystem defined in /etc/fstab.
Do you wish to continue? Database is consistent: Drop to root shell prompt Enable networking Finished, please press ENTER IP and DNS configured IP configured Network connectivity: No LVM detected (vgscan) No software RAID detected (mdstat) Physical Volumes: Read-only mode Read/Write mode Recovery Menu (filesystem state: read-only) Recovery Menu (filesystem state: read/write) Repair broken packages Revert to old snapshot and reboot Run in failsafe graphic mode Snapshot System mode: System summary The option you selected requires your filesystem to be in read-only mode. Unfortunately another option you selected earlier, made you exit this mode.
The easiest way of getting back in read-only mode is to reboot your system. Try to make free space Trying to find packages you don't need (apt-get autoremove), please review carefully. Unknown (must be run as root) Update grub bootloader Volume Groups: You are now going to exit the recovery mode and continue the boot sequence. Please note that some graphic drivers require a full graphical boot and so will fail when resuming from recovery.
If that's the case, simply reboot from the login screen and then perform a standard boot. no (BAD) none not ok (BAD) ok (good) unknown (must be run as root) unknown (read-only filesystem) yes (good) Project-Id-Version: friendly-recovery
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2012-03-08 14:38-0500
PO-Revision-Date: 2012-03-17 05:29+0000
Last-Translator: Илья Калитко <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:23+0000
X-Generator: Launchpad (build 18115)
    Продолжить нормальную загрузку (Используйте кнопки со стрелками или PageUp/PageDown для прокрутки и TAB для выбора) === Использование диска === === Использование памяти === === Сведения о настройках сети === === Общие сведения === === Статус LVM === === Состояние программного RAID === === База данных системы (APT) === Сведения о процессоре: Проверить все файловые системы Сейчас ваша корневая файловая система будет открыта для записи, а также будут смонтированы все файловые системы, указанные в файле /etc/fstab.
Продолжить? База данных содержит: Перейти в командный интерпретатор суперпользователя Задействовать сетевые возможности Закончено, пожалуйста, нажмите ENTER IP-адрес и DNS  назначены IP-адрес назначен Сетевые возможности: LVM не обнаружен (vgscan) Программный RAID не обнаружен (mdstat) Физические разделы: Режим только для чтения Режим чтения/записи Меню восстановления (состояние файловой системы: только для чтения) Меню восстановления (состояние файловой системы: чтение/запись) Исправить повреждённые пакеты Восстановить из предыдущего снимка и выполнить перезагрузку Запустить в безопасном графическом режиме Снимок Системный режим: Сведения о системе Для выполнения этой команды файловая система должна находиться в режиме только для чтения, 
однако выбранные вами команды разрешили запись на файловую систему.
Самый простой способ вернуться к режиму только для чтения -- перезагрузить вашу систему. Попробовать освободить место Поиск ненужных для данной системы пакетов (apt-get autoremove); внимательно проверьте их список перед удалением. Неизвестно (должно быть запущено с правами суперпользователя) Обновить начальный загрузчик grub Группы томов: Вы собираетесь выйти из меню восстановления и продолжить загрузку. Обратите внимание, что некоторым видеодрайверам требуется загрузка в графическом режиме, и после выхода из режима восстановления у них могут возникнуть неполадки.
В этом случае перезагрузите компьютер, используя меню экрана входа, и загрузитесь обычным образом. нет (ПЛОХО) отсутствует не ок (ПЛОХО) ок (хорошо) неизвестно (должно быть запущено с правами суперпользователя) неизвестно (файловая система только для чтения) да (хорошо) 