��            )   �      �  c   �  �     a   �     �       �    -     )   9     c     q     �  -   �     �     �       &   	  O   0  -   �     �     �  #   �  "   �  %     "   9  %   \     �     �  5   �     �  �  �  �   �
  �     �   �  ,   X     �  s  �  Z     <   n  )   �  ]   �  !   3  [   U  1   �  3   �       �   &  �   �  [   I  #   �  %   �  C   �  ;   3  >   o  ;   �  >   �  >   )     h  d   �     �                                                      	                         
                                                                --debug              Print debugging information
      --quiet              Silent operation
       --no-tld             Don't check string for TLD specific rules
                             Only for --idna-to-ascii and --idna-to-unicode
   -h, --help               Print help and exit
  -V, --version            Print version and exit
 Cannot allocate memory Charset `%s'.
 Command line interface to the internationalized domain name library.

All strings are expected to be encoded in the preferred charset used
by your locale.  Use `--debug' to find out what this charset is.  You
can override the charset used by setting environment variable CHARSET.

To process a string that starts with `-', for example `-foo', use `--'
to signal the end of parameters, as in `idn --quiet -a -- -foo'.

Mandatory arguments to long options are mandatory for short options too.
 Conflicting bidirectional properties in input Input already contain ACE prefix (`xn--') Invalid input Malformed bidirectional string Missing input Output would exceed the buffer space provided String preparation failed String size limit exceeded Success Try `%s --help' for more information.
 Type each input string on a line by itself, terminated by a newline character.
 Unicode normalization failed (internal error) Unknown error Unknown profile Usage: %s [OPTION]... [STRINGS]...
 could not convert from %s to UTF-8 could not convert from UCS-4 to UTF-8 could not convert from UTF-8 to %s could not convert from UTF-8 to UCS-4 could not do NFKC normalization input error only one of -s, -e, -d, -a, -u or -n can be specified stringprep_profile: %s Project-Id-Version: libidn
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-08-01 15:20+0200
PO-Revision-Date: 2014-02-05 11:21+0000
Last-Translator: ☠Jay ZDLin☠ <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:00+0000
X-Generator: Launchpad (build 18115)
       --debug              Вывести отладочную информацию
      --quiet              Тихий режим
       --no-tld             Не проверять строку по TLD правилам
                             Только для --idna-to-ascii и --idna-to-unicode
   -h, --help               Вывести справку и выйти
  -V, --version            вывести версию и выйти
 Ошибка выделения памяти Кодировка `%s'.
 Интерфейс командной строки для библиотеки доменных имен

Для всех строк должна использоваться кодировка ващей локали.
Используйте флаг `--debug' для того, чтобы узнать какая это кодировка.
Вы можете явно задать кодировку используя переменную окружения CHARSET.

Для обработки строк начинающихся с символа `-' (например, `-foo') используйте `--'
для объявления конца списка параметров, как в `idn --quiet -a -- -foo'.

Обязательные параметры длинных настроек являются обязательными и для коротких настроек.
 Конфликтующие двунаправленные свойства во входе Вход уже содержит префикс ACE (`xn--') Недопустимое значение Некорректно сформированая двунаправленная строка Неправильный вход Выходные данные превысят выделяемый объем буфера Сбой при подготовке строки Превышен лимит длины строки Успешно Попробуйте указать `%s --help', чтобы получить дополнительную информацию.
 Печатать каждую входную строку отдельно, заканчивая символом перехода на новую строку.
 Сбой при нормализации юникода (внутренняя ошибка) Неизвестная ошибка Неизвестный профиль Используйте: %s [ПАРАМЕТР]... [СТРОКА]...
 не могу конвертировать из %s в UTF-8 не могу конвертировать из UCS-4 в UTF-8 не могу конвертировать из UTF-8 в %s не могу конвертировать из UTF-8 в UCS-4 не могу выполнить NFKC нормализацию ошибка ввода Только один из параметров -s, -e, -d, -a, -u или -n можно задать stringprep_profile: %s 