��    K      t  e   �      `  $   a     �     �  	   �     �  
   �     �  #   �  #        *     0     7      <     ]  !   o     �    �     �  <   �     �  
   	     	     (	     4	     :	  "   C	     f	     �	     �	     �	     �	     �	     �	     �	  S   �	  2   I
  [   |
  /   �
            
        %     1  $   @     e  #   v     �     �     �     �     �  �   �     �     �     �     �  	   �     �     �     �  	   �          
       
        (     .     6     >     M     a     h     u     �  �  �  ?   0  6   p     �     �     �  &   �  $     %   ;  ,   a     �     �     �  .   �  )   �  Q        ^  �  o  )   5  k   _     �     �               5     >  B   R  <   �  
   �     �     �  >     S   J  	   �     �  �   �  o   �  �     _   �     O     ]     y     �      �  8   �  !     3   .  
   b  *   m  :   �  ]   �  )   1  �  [     �     �          )     9     O     ]     m     y     �     �     �     �     �     �     �  2     3   8     l     z  �  �  R         B   C                !   )   ;   D          I      	       F   9   K              *      %      +                    #   (   2   6               "         ?   .             1   G       @      0              &      H   E   -   =                    /   J   >   5            '                   8                     A       :   <       3   4       
             $   ,   7    %u × %u, %u mine %u × %u, %u mines <b>%d</b> mine <b>%d</b> mines A_ppearance Big board Big game Board size Change _Difficulty Clear explosive mines off the board Clear hidden mines from a minefield Close Custom Date Do you want to start a new game? Enable animations Enable automatic placing of flags GNOME Mines GNOME Mines is a puzzle game where you search for hidden mines. Flag the spaces with mines as quickly as possible to make the board a safer place. You win the game when you've flagged every mine on the board. Be careful not to trigger one, or the game is over! Height of the window in pixels If you start a new game, your current progress will be lost. Keep Current Game Main game: Medium board Medium game Mines New Game Number of columns in a custom game Number of rows in a custom game Paused Percent _mines Play _Again Print release version and exit Resizing and SVG support: Score: Select Theme Set to true to automatically flag squares as mined when enough squares are revealed Set to true to be able to mark squares as unknown. Set to true to enable warning icons when too many flags are placed next to a numbered tile. Size of the board (0-2 = small-large, 3=custom) Size: Small board Small game St_art Over Start New Game The number of mines in a custom game The theme to use The title of the tile theme to use. Time Use _animations Use the unknown flag Warning about too many flags Width of the window in pixels You can select the size of the field you want to play on at the start of the game. If you get stuck, you can ask for a hint: there's a time penalty, but that's better than hitting a mine! _About _Best Times _Cancel _Close _Contents _Height _Help _Mines _New Game _OK _Pause _Play Again _Play Game _Quit _Resume _Scores _Show Warnings _Use Question Flags _Width minesweeper; translator-credits true if the window is maximized Project-Id-Version: gnome-games trunk
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-mines&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-11 17:13+0000
PO-Revision-Date: 2016-06-02 14:25+0000
Last-Translator: Yuri Myasoedov <ymyasoedov@yandex.ru>
Language-Team: русский <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 19:21+0000
X-Generator: Launchpad (build 18115)
Language: ru
 %u × %u, %u мина %u × %u, %u мины %u × %u, %u мин <b>%d</b> мина <b>%d</b> мины <b>%d</b> мин _Внешний вид Большое поле Большая игра: Размер игрового поля Изменить _сложность Очистить поле от мин Поиск мин на минном поле Закрыть другая Дата Хотите начать новую игру? Использовать анимацию Включить автоматическую расстановку флагов Сапёр GNOME Сапёр GNOME — это игра, в которой нужно найти скрытые мины. Установите флажки на все поля, где находятся мины, затратив минимальное количество времени. Вы побеждаете, если все мины на поле отмечены флажками. Не нажимайте на мины, иначе игра закончится! Высота окна в пикселах Если начать новую игру, текущие результаты будут потеряны. Продолжить игру Основная игра: Среднее поле Средняя игра: Мины Новая игра Число столбцов в настраиваемой игре Число строк в настраиваемой игре Пауза Процент _мин _Сыграть ещё Показать версию программы и выйти Изменение размера и поддержка изображений SVG: Счёт: Выбор темы Установите, чтобы квадраты с минами автоматически помечались, когда вокруг открыто достаточное количество квадратов. Выберите для возможности помечать квадраты как неизвестные. Выберите, чтобы включить значки предупреждения при размещении рядом с нумерованным полем слишком большого количества флагов. Размер доски (0-2 = маленькая-большая, 3=настраиваемая) Размер: Маленькое поле Маленькая игра: Начать сначала Начать новую игру Число мин в настраиваемой игре Используемая тема Название используемой темы. Время Использовать _анимацию Использовать флаг «Неизвестно» Предупреждать о слишком большом количестве флагов Ширина окна в пикселах В начале игры вы можете выбрать размер игрового поля. Если вы попали в затруднительное положение, вы можете получить подсказку, за получение подсказки начисляется штрафное время, но это лучше, чем нарваться на мину! _О приложении _Лучшее время _Отменить _Закрыть _Содержание _Высота _Справка _Сапёр _Новая игра _OK П_ауза _Играть снова _Играть _Закончить _Продолжить _Результаты _Показывать предупреждения _Использовать флаги-вопросы _Ширина сапёр;мины; Валек Филиппов aka frob
Дмитрий Мастрюков
Вячеслав Диконов
Михаил Яхонтов
Леонид Кантер

Launchpad Contributions:
  Aleksey Kabanov https://launchpad.net/~ak099
  Vassili Platonov https://launchpad.net/~vassilip
  Yuri Myasoedov https://launchpad.net/~ymyasoedov
  ☠Jay ZDLin☠ https://launchpad.net/~black-buddha666 истинно, если окно имеет максимальный размер 