��    "      ,  /   <      �     �     
       
        #  .   :     i  	   |     �     �     �  !   �     �     �     �     �          .  ,   E     r     �     �     �     �  $   �     �     �       �   '     �     �     �     �  �  �  &   t     �     �     �  (   �  �   
     �     �  $   �     �  ?   	  G   F	     �	     �	     �	  *   �	  -   
  +   2
  d   ^
  (   �
     �
  I   �
  5   ?     u  ;   �     �  "   �  @     F  B     �     �     �     �                                                                                 	             
         !                                             "          Add input method Addon Advance Appearance Available Input Method Cannot load currently used user interface info Clear font setting Configure Current Input Method Default Default keyboard layout Didn't install related component. Empty Global Config Input Method Input Method Configuration Input Method Default Input method settings: Keyboard layout to use when no input window: Keyboard layout: Language No configuration option for %s. Only Show Current Language Other Please press the new key combination Search Addon Search Input Method Show Advance Option The first input method will be inactive state. Usually you need to put <b>Keyboard</b> or <b>Keyboard - <i>layout name</i></b> in the first place. Unknown Unspecified _Cancel _OK Project-Id-Version: fcitx-configtool
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-10-18 03:07-0400
PO-Revision-Date: 2016-06-02 13:59+0000
Last-Translator: Sandro <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:00+0000
X-Generator: Launchpad (build 18115)
 Добавить метод ввода Дополнения Дополнительно Внешний вид Доступный метод ввода Не удалось загрузить информацию об используемом пользовательском интерфейсе Сбросить шрифт Настроить Текущий метод ввода По умолчанию Раскладка клавиатуры по умолчанию Зависимый компонент не был установлен. Отсутствует Общие настройки Метод ввода Настройка метода ввода Метод ввода по умолчанию Настройки метода ввода: Если нет окна ввода, использовать следующую раскладку: Раскладка клавиатуры: Язык Отсутсвует параметр конфигурации для %s. Показать только текущий язык Другие Нажмите новую комбинацию клавиш Поиск дополнения Поиск метода ввода Показать дополнительные параметры Первичный метод ввода будет в незадействованном состоянии. Обычно, необходимо указывать первым пунктом в списке - <b>Клавиатура</b> или <b>Клавиатура - <i>наименование раскладки</i></b>. Неизвестно Не задано О_тменить _ОК 