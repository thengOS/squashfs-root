��    0      �  C         (     )     9  
   K     V     \     p     x     �     �     �     �  !   �     �     �     �     �     �  "        )     /     8     E     T     `  1   �     �     �     �  
   �     �  	   �     �     �          %     5     E     T     g     �     �  ?   �     �     �     �  p   �     T  �  b  '   	  +   ;	     g	     x	  .   �	     �	  8   �	     
  *   
  
   D
     O
  N   i
     �
     �
     �
  
   �
  .   �
  O   !     q     �  )   �  /   �  &   �  I      x   j     �     �     �  !        -     :     U  8   o  #   �  (   �  .   �  &   $  .   K  A   z     �     �  m   �  
   H     S  
   f  �   q  0   J     /   +              -         )   %                                   *                        $                            0   ,      "      .   &         '             #             (          	                   !                 
    3d-ish (button) 3d-ish (gradient) Animations Arrow Cell Indicator Size Classic Colorize Scrollbar Contrast Disable focus drawing Dot Edge Thickness Enable Animations on Progressbars Flat Full Glossy Gummy Handlebox Marks If set, button corners are rounded Inset Inverted Inverted Dot Inverted Slash Mark Type 1 Mark Type for Scrollbar Buttons Mark Type for Scrollbar Handles, Handleboxes, etc Menubar Style None Nothing Paned Dots Radius Rectangle Relief Style Requires style Glossy or Gummy Rounded Buttons Scrollbar Color Scrollbar Marks Scrollbar Type Scrollbutton Marks Sets the Color of Scrollbars Shadow Shaped Size of check- and radiobuttons inside treeviews. (Bug #351764) Slash Some Style This option allows to disable the focus drawing. The primary purpose is to create screenshots for documentation. Toolbar Style Project-Id-Version: gtk-engines trunk
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 22:41+0000
PO-Revision-Date: 2009-03-17 03:11+0000
Last-Translator: Alexandre Prokoudine <alexandre.prokoudine@gmail.com>
Language-Team: Russian <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:46+0000
X-Generator: Launchpad (build 18115)
 трёхмерность (кнопка) трёхмерность (градиент) Анимации Стрелки Размер индикатора ячейки Классическая Раскрашивать полосы прокрутки Контраст Выключить рамку фокуса Точки Толщина углов Включить анимацию в индикаторах прогресса Простой Полностью Глянец Смола Стиль отделяемой области Если установлено, углы кнопок закругляются Вкладка Инвертированные Инвертированные точки Инвертированные черточки Отметка первого типа Тип отметок для кнопок полосы прокрутки Тип отметки для бегунков полос прокрутки, панелей отделения и т.д. Стиль меню Нет Ничего Разделённые точки Радиус Прямоугольник Стиль рельефа Требует стиля Глянец или Смола Скруглённые кнопки Цвет полосы прокрутки Отметки полосы прокрутки Тип полосы прокрутки Отметки кнопки прокрутки Устанавливает цвет полос прокрутки Тень Выделенные Размер отметок и кнопок выбора в деревьях и списках. (Bug #351764) Черта Некоторые Стиль Этот параметр выключает отрисовку рамки фокуса. Он полезен, например, для сохранения снимков экрана для документации. Стиль панели инструментов 