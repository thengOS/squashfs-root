��    9      �  O   �      �     �               $     7  '   H  ;   p  �   �     V  !   c     �  +   �     �     �     �     �     �  %        .  4   I     ~     �     �      �     �     �     
          &     2     A  "   J      m     �     �     �     �      �     �     �     	     	  (   	     A	     M	     Z	  	   c	     m	     t	     |	     �	     �	     �	     �	     �	     �	  �  �	  .   �  !   �  5   �  /        A  f   a  _   �  q  (  !   �  =   �     �  _        a     p     �  ]   �  <   �  u   3  B   �  r   �  4   _  <   �  %   �  E   �  +   =  1   i  
   �  6   �     �  !   �  %     C   A  S   �  #   �  $   �     "     >     K  &   c     �  )   �     �  :   �          )     E     Y     m     �  ;   �     �     �     �       =     �  ]     .                3            ,                              6                7   9          	             -       %             +   !                      $         (       &           #   '          8   /   
   4          "       1      )            2       *   0       5            "%s" is not a valid folder %d day %d days %d item %d items %d month %d months %d year %d years A graphical tool to analyze disk usage. A list of URIs for partitions to be excluded from scanning. A simple application which can scan either specific folders (local or remote) or volumes and give a graphical representation including each directory size or percentage. Active Chart Apparent sizes are shown instead. Baobab Check folder sizes and available disk space Close Computer Contents Could not analyze disk usage. Could not analyze volume. Could not detect occupied disk sizes. Could not scan folder "%s" Could not scan some of the folders contained in "%s" Devices and locations Disk Usage Analyzer Excluded partitions URIs Failed to move file to the trash Failed to open file Failed to show help Folder Go to _parent folder Home folder Mo_ve to Trash Modified Print version information and exit Recursively analyze mount points Rings Chart Scan Folder… Select Folder Size The GdkWindowState of the window The initial size of the window Today Treemap Chart Unknown Which type of chart should be displayed. Window size Window state Zoom _in Zoom _out _About _Cancel _Copy Path to Clipboard _Help _Open _Open Folder _Quit storage;space;cleanup; translator-credits Project-Id-Version: gnome-utils trunk
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=baobab&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 14:31+0000
PO-Revision-Date: 2015-07-20 13:54+0000
Last-Translator: Yuri Myasoedov <ymyasoedov@yandex.ru>
Language-Team: русский <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 17:41+0000
X-Generator: Launchpad (build 18115)
Language: ru
10<=4 && (n%100<10 || n%100>=20) ? 1: 2);
 «%s» — недопустимая папка %d день %d дня %d дней %d объект %d объекта %d объектов %d месяц %d месяца %d месяцев %d год %d года %d лет Графическое средство для анализа использования дисков. Список разделов (URI), которые не будут сканироваться. Простое приложение, которое может сканировать отдельные папки (локальные или удалённые) или тома и выводить графическое представление, включающее размеры каждого каталога или процентное соотношение. Текущая диаграмма Показаны предполагаемые размеры. Baobab Изучение размера папок и свободного места на дисках Закрыть Компьютер Содержимое Не удалось проанализировать использование дисков. Не удалось проанализировать том. Не удалось определить размер используемого пространства диска. Не удалось просканировать папку «%s» Не удалось просканировать некоторые папки, содержащиеся в «%s» Устройства и местоположения Анализатор использования дисков Исключённые разделы Не удалось переместить файл в корзину Не удалось открыть файл Не удалось открыть справку Папка Перейти к _родительской папке Домашняя папка У_далить в корзину Последнее изменение Показать информацию о версии и выйти Рекурсивно анализировать точки монтирования Круговая диаграмма Сканировать папку… Выберите папку Размер GdkWindowState окна Исходный размер окна Сегодня Древовидная диаграмма Неизвестно Какой тип диаграммы показывать. Размер окна Состояние окна У_величить У_меньшить О _приложении О_тмена Скопировать путь в _буфер обмена _Справка _Открыть Откр_ыть папку _Завершить накопитель;пространство;очистка; Валёк Филлипов
Сергей Панов
Дмитрий Мастрюков
Андрей Носенко
Леонид Кантер
Василий Фаронов <qvvx@yandex.ru>
Станислав Соловей <whats_up@tut.by>, 2012

Launchpad Contributions:
  Aleksey Kabanov https://launchpad.net/~ak099
  Eugene Marshal https://launchpad.net/~lowrider
  Nikita Putko https://launchpad.net/~ktototam98
  Yuri Myasoedov https://launchpad.net/~ymyasoedov 