��          �      �       H  ,   I  �   v       .   .     ]     x      �  �   �     ;  7   Z     �  /   �  ~   �  �  a  7     W  F  
   �  !   �     �     �  G   �  �   D  Y   D	  �   �	  B   $
  _   g
    �
                
                                   	              @info<link url='%1'>Previous Reports</link> @infoKubuntu can collect anonymous information that helps developers improve it. All information collected is covered by our <link url='%1'>privacy policy</link>. @info:creditAuthor @info:creditCopyright 2013-2014 Harald Sitter @info:creditHarald Sitter @titleDiagnostics EMAIL OF TRANSLATORSYour emails Error reports include information about what a program was doing when it failed. You always have the choice to send or cancel an error report. NAME OF TRANSLATORSYour names Send a report automatically if a problem prevents login Send error reports to Canonical Send occasional system information to Canonical This includes things like how many programs are running, how much disk space the computer has, and what devices are connected. Project-Id-Version: kde-config-whoopsie
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-03-27 00:49+0000
PO-Revision-Date: 2015-04-01 08:22+0000
Last-Translator: Roman Mindlin <mindline@mail.ru>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:31+0000
X-Generator: Launchpad (build 18115)
 <link url='%1'>Предыдущие отчёты</link> Kubuntu может собирать анонимную информацию, которая помогает разработчикам улучшать систему. Правила сбора и обработки информации описаны в нашей <link url='%1'>политике конфиденциальности</link>. Автор Copyright 2013-2014 Harald Sitter Харалд Ситтер Диагностика ,,mindline@mail.ru,ilya.tretyakov85@gmail.com,matvei.vialkov@yandex.ru, Отчёты содержат в себе информацию о работе программы в момент возникновения ошибки. Вы всегда можете отменить отправку отчётов об ошибках.  ,Launchpad Contributions:,Roman Mindlin,mcilya,Матвей Вялков,☠Jay ZDLin☠ Отправлять отчёт автоматически, если ошбика не позволяет выполнить вход Отправлять отчёты об ошибках в Canonical Отправить дополнительные сведения о системе в Canonical Дополнительные сведения включают в себя количество запущенных программ, объём дискового пространства, а также информацию о подключённых устройствах. 