��    [      �     �      �  �   �  ,   �  5   �  N   '	  7   v	  \   �	  _   
  `   k
  u   �
  b   B  V   �  Y   �  ~   V  �   �  D   e  %   �     �     �  5     B   7     z  e   �  w   �     u     �     �     �  $   �               /     <     H     Y     b     �  #   �     �     �     �     �     �               (  H   5     ~     �  !   �     �     �  (        ,     ?  #   ]     �     �  $   �     �     �  #     B   =  2   �     �      �     �          %  *   8  *   c     �     �     �  #   �  #   �  &        ;     J  ,   i     �     �  -   �     �               &     <     J     c     |    �  �  �  B   p  O   �  �     S   �  �   �  �   }       �   �  �   -  o   �  �   2  �   �  �   �   [   !  5   �!  3   "  %   E"  h   k"  �   �"  O   d#  �   �#  �   o$  6   2%  B   i%  8   �%  5   �%  ]   &  >   y&  @   �&     �&     '     '     2'  G   P'     �'  [   �'  E   (     H(     W(  =   `(  %   �(  )   �(  )   �(      )  �   9)  4   �)  O   *  J   V*  *   �*  6   �*  t   +  :   x+  D   �+  b   �+  3   [,  K   �,  W   �,  Q   3-  3   �-  F   �-  �    .  c   �.  &   -/  3   T/  9   �/  5   �/     �/  E   0  n   ^0  O   �0  %   1  '   C1  I   k1  I   �1  V   �1  0   V2  G   �2  i   �2  0   93      j3  I   �3  -   �3  -   4  +   14  4   ]4  !   �4  -   �4  -   �4  S   5           B          	   4                 ;      I   *                 Q   "   
   2   K   R           D   L       )   A                     E      9   W      1   H                 G   -   =                                 >       $          #   P   8               S   3               0       Z                ,             U       ?              F   <       +   T   [   N   6   M      (   %          O   '   :   @   Y       V   &   .   /       J   C      7   X   !   5          
If no -e, --expression, -f, or --file option is given, then the first
non-option argument is taken as the sed script to interpret.  All
remaining arguments are names of input files; if no input files are
specified, then the standard input is read.

       --help     display this help and exit
       --version  output version information and exit
   --follow-symlinks
                 follow symlinks when processing in place
   --posix
                 disable all GNU extensions.
   -R, --regexp-perl
                 use Perl 5's regular expressions syntax in the script.
   -b, --binary
                 open files in binary mode (CR+LFs are not processed specially)
   -e script, --expression=script
                 add the script to the commands to be executed
   -f script-file, --file=script-file
                 add the contents of script-file to the commands to be executed
   -l N, --line-length=N
                 specify the desired line-wrap length for the `l' command
   -n, --quiet, --silent
                 suppress automatic printing of pattern space
   -r, --regexp-extended
                 use extended regular expressions in the script.
   -s, --separate
                 consider files as separate rather than as a single continuous
                 long stream.
   -u, --unbuffered
                 load minimal amounts of data from the input files and flush
                 the output buffers more often
   -z, --null-data
                 separate lines by NUL characters
 %s: -e expression #%lu, char %lu: %s
 %s: can't read %s: %s
 %s: file %s line %lu: %s
 %s: warning: failed to get security context of %s: %s %s: warning: failed to set default file creation context to %s: %s : doesn't want any addresses E-mail bug reports to: <%s>.
Be sure to include the word ``%s'' somewhere in the ``Subject:'' field.
 GNU sed home page: <http://www.gnu.org/software/sed/>.
General help using GNU software: <http://www.gnu.org/gethelp/>.
 Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid preceding regular expression Invalid range end Invalid regular expression Jay Fenlason Ken Pizzini Memory exhausted No match No previous regular expression Paolo Bonzini Premature end of regular expression Regular expression too big Success Tom Lord Trailing backslash Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ Usage: %s [OPTION]... {script-only-if-no-other-script} [input-file]...

 `e' command not supported `}' doesn't want any addresses can't find label for jump to `%s' cannot remove %s: %s cannot rename %s: %s cannot specify modifiers on empty regexp cannot stat %s: %s command only uses one address comments don't accept any addresses couldn't attach to %s: %s couldn't edit %s: is a terminal couldn't edit %s: not a regular file couldn't follow symlink %s: %s couldn't open file %s: %s couldn't open temporary file %s: %s couldn't write %d item to %s: %s couldn't write %d items to %s: %s delimiter character is not a single-byte character error in subprocess expected \ after `a', `c' or `i' expected newer version of sed extra characters after command incomplete command invalid reference \%d on `s' command's RHS invalid usage of +N or ~N as first address invalid usage of line address 0 missing command multiple `!'s multiple `g' options to `s' command multiple `p' options to `s' command multiple number options to `s' command no input files no previous regular expression number option to `s' command may not be zero option `e' not supported read error on %s: %s strings for `y' command are different lengths unexpected `,' unexpected `}' unknown command: `%c' unknown option to `s' unmatched `{' unterminated `s' command unterminated `y' command unterminated address regex Project-Id-Version: sed 4.2.0
Report-Msgid-Bugs-To: bug-gnu-utils@gnu.org
POT-Creation-Date: 2012-12-22 14:36+0100
PO-Revision-Date: 2015-03-12 11:15+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <gnu@mx.ru>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 17:17+0000
X-Generator: Launchpad (build 18115)
Language: ru
 
Если опция -e, --expression, -f, или --file не указана, тогда первый
неопциональный аргумент берется как скрипт sed для интерпретации. Все
оставшиеся аргументы являются именами входных файлов; если входные
файлы не указаны, тогда читается стантартный ввод.

       --help     вывод этой справки и выход
       --version  вывод информации о версии и выход
   --follow-symlinks
                 переходить по символьным ссылкам при обработке на месте
   --posix
                 отключение всех расширений GNU.
   -R, --regexp-perl
                 использование в скрипте синтаксиса регулярных выражений Perl 5.
   -b, --binary
                 открывать файлы в бинарном режиме (CR+LF не обрабатываются)
   -e script, --expression=script
                 добавление скрипта в исполняемые команды
   -f script-file, --file=script-file
                 добавление содержимого файла-скрипта в исполняемые команды
   -l N, --line-length=N
                 указание желаемой длины переносимой строки для команды `l'
   -n, --quiet, --silent
                 не выводить автоматически промежутки
   -r, --regexp-extended
                 использование в скрипте расширенных регулярных выражений.
   -s, --separate
                 допущение, что файлы разделены, а не в виде одного
                 длинного непрерывного потока.
   -u, --unbuffered
                 загрузка минимального объема данных из входных файлов
                 и более частый сброс на диск выходных буферов
   -z, --null-data
                 разделять строки символами NULL
 %s: -e выражение #%lu, символ %lu: %s
 %s: невозможно прочитать %s: %s
 %s: файл %s строка %lu: %s
 %s: предупреждение: ошибка получения контекста защиты %s: %s %s: предупреждение: ошибка установки контекста создания файла по умолчанию %s: %s `:' не допускает указания каких-либо адресов Отчеты об ошибках отправляйте по адресу: <%s>.
Убедитесь, что включили где-либо в поле ``Тема:'' слово ``%s''.
 Домашняя страница GNU sed: <http://www.gnu.org/software/sed/>.
Основная помощь по использованию программ GNU: <http://www.gnu.org/gethelp/>.
 Недопустимая обратная ссылка Недопустимое имя для класса символа Недопустимый символ сравнения Недопустимое содержимое в \{\} Недопустимое предшествующее регулярное выражение Недопустимое окончание диапазона Недопустимое регулярное выражение Jay Fenlason Ken Pizzini Память исчерпана Нет соотвествия Нет предыдущего регулярного выражения Paolo Bonzini Преждевременное окончание регулярного выражения Регулярное выражение слишком большое Успешно Tom Lord Завершающая обратная косая черта Непарный символ ( or \( Непарный символ ) или \) Непарный символ [ или [^ Непарный символ \{ Использование: %s [ОПЦИЯ]... {только-скрипт-если-нет-другого-скрипта} [входной-файл]...

 команда `e' не поддерживается `}' не допускает указания каких-либо адресов невозможно найти метку для перехода к `%s' невозможно удалить %s: %s невозможно переименовать %s: %s невозможно указать модификаторы в пустом регулярном выражении невозможно выполнить stat для %s: %s команда использует только один адрес комментарии не допускают указания каких-либо адресов невозможно прикрепить к %s: %s невозможно редактировать %s: это терминал невозможно редактировать %s: это не обычный файл невозможно перейти по символьной ссылке %s: %s невозможно открыть файл %s: %s невозможно открыть временный файл %s: %s не удалось записать %d элемент в %s: %s не удалось записать %d элемента в %s: %s не удалось записать %d элементов в %s: %s символ-разделитель не является однобайтовым символом ошибка в подпроцессе ожидалась \ после `a', `c' или `i' ожидалась более новая версия sed лишние символы после команды неполная команда недопустимая ссылка \%d на RHS команды `s' использование +N или ~N в качестве первого адреса недопустимо недопустимое использование строки адреса 0 отсутствует команда несколько символов `!' несколько модификаторов `g' с командой `s' несколько модификаторов `p' с командой `s' несколько числовых модификаторов с командой `s' отсутствуют входные файлы нет предыдущего регулярного выражения числовой модификатор для команды `s' не может быть нулевым опция `e' не поддерживается ошибка чтения %s: %s строки для команды `y' имеют разную длину непредвиденный символ `,' непредвиденный символ `}' неизвестная команда: `%c' неизвестный модификатор к `s' непарный символ `{' незавершенная команда `s' незавершенная команда `y' незавершенное адресное регулярное выражение 