��    	      d      �       �      �   )   �   2        F  ,   T  #   �  "   �  0   �  �  �  
   �  ^   �  t   ]  "   �  r   �  C   h  P   �  �   �                                          	    Archive Could not parse command-line options: %s
 Expects URIs or filenames to be passed as options
 Files to send No mail client installed, not sending files
 Output version information and exit Run from build directory (ignored) Use XID as parent to the send dialogue (ignored) Project-Id-Version: ru
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=nautilus-sendto&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 23:48+0000
PO-Revision-Date: 2015-12-04 21:59+0000
Last-Translator: Dmitriy S. Seregin <Unknown>
Language-Team: Russian <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:34+0000
X-Generator: Launchpad (build 18115)
Language: ru
 Архив Не удалось разобрать параметры командной строки: %s
 Ожидается, что URI или имена файлов будут переданы как параметры
 Файлы для отправки Не установлен почтовый клиент, файлы не могут быть отправлены
 Показать информацию о версии и выйти Запустить из каталога сборки (игнорируется) Использовать XID как родителя по отношению к диалогу отправки (игнорируется) 