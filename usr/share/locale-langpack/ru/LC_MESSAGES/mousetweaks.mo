��            )         �  "   �     �  &   �          #     A     Y     f     k      ~     �     �  
   �     �  
   �     �       %        D     T     n     �  e   �     �          5  	   D  !   N  /   p     �  �  �  `   �     �  7     1   G  3   y  )   �     �     �  ;   	  E   D	  3   �	  /   �	     �	  "   	
     ,
  O   F
     �
  4   �
     �
  G   �
     @     _  �     2   9  K   l     �     �  @   �  7   ,     d                                                   
                     	                                                                - GNOME mouse accessibility daemon Button Style Button style of the click-type window. Click-type window geometry Click-type window orientation Click-type window style Double Click Drag Enable dwell click Enable simulated secondary click Failed to Display Help Hide the click-type window Horizontal Hover Click Icons only Ignore small pointer movements Orientation Orientation of the click-type window. Secondary Click Set the active dwell mode Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Start mousetweaks as a daemon Start mousetweaks in login mode Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Project-Id-Version: ru
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=mousetweaks&component=general
POT-Creation-Date: 2015-12-03 23:45+0000
PO-Revision-Date: 2012-03-29 06:38+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: Russian <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:24+0000
X-Generator: Launchpad (build 18115)
X-Poedit-Language: Russian
 — Служба дополнительных возможностей мыши среды GNOME Стиль кнопок Стиль кнопок окна типа щелчка. Геометрия окна типа щелчка Ориентация окна типа щелчка Стиль окна типа щелчка Двойной щелчок Перетащить Включить щелчок при удерживании Включить поддержку вторичного щелчка Не удалось показать справку Скрыть окно типов щелчков Горизонтально Щелчок по таймауту Только значки Игнорировать слабые перемещения указателя Положение Ориентация окна типа щелчка. Второй щелчок Установить активный режим удерживания Выключить mousetweaks Одинарный щелчок Размер и положение окна типа щелчка.Используется стандартный формат геометрической строки X Window System. Запускать mousetweaks как службу Запустить mousetweaks в режиме входа в систему Текст и иконки Только текст Задержка до щелчка при удерживании Задержка до вторичного щелчка Вертикально 