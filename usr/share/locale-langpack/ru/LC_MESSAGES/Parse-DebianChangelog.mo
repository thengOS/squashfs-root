��    &      L  5   |      P  ,   Q  '   ~  (   �  	   �  z   �     T     g     �     �     �     �     �          +     B  !   Y     {  $   �     �  -   �  "     "   &  #   I     m  &   �     �     �  (   �          '     F     \  +   o     �  .   �  ,   �  A   	  �  K  T   �	  N   O
  '   �
  &   �
  �   �
  *   �  ?   �  H   �  E   D  G   �  M   �  2      6   S  J   �  2   �  :     ?   C  >   �  ;   �  �   �  P   �  E   �  K   (  L   t  E   �  @     &   H  <   o  -   �  >   �  :     .   T  h   �  )   �  R     Q   i  �   �                                         
   #                 $                                           &                              	                            %   !         "    'since' option specifies most recent version 'until' option specifies oldest version Copyright (C) 2005 by Frank Lichtenheld
 FATAL: %s This is free software; see the GNU General Public Licence version 2 or later for copying conditions. There is NO warranty. WARN: %s(l%s): %s
 WARN: %s(l%s): %s
LINE: %s
 bad key-value after `;': `%s' badly formatted heading line badly formatted trailer line badly formatted urgency value can't close file %s: %s can't load IO::String: %s can't lock file %s: %s can't open file %s: %s changelog format %s not supported couldn't parse date %s fatal error occured while parsing %s field %s has blank lines >%s< field %s has newline then non whitespace >%s< field %s has trailing newline >%s< found blank line where expected %s found change data where expected %s found eof where expected %s found start of entry where expected %s found trailer where expected %s ignored option -L more than one file specified (%s and %s) no changelog file specified output format %s not supported repeated key-value %s too many arguments unknown key-value key %s - copying to XS-%s unrecognised line you can only specify one of 'from' and 'since' you can only specify one of 'to' and 'until' you can't combine 'count' or 'offset' with any other range option Project-Id-Version: libparse-debianchangelog-perl
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-08-25 20:38+0000
PO-Revision-Date: 2010-03-14 11:41+0000
Last-Translator: Yuri Tkachenko <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 опция 'since' опеределяет самую последнюю версию опция 'until' опеределяет самую старую версию Copyright (C) 2005 - Frank Lichtenheld
 Критическая ошибка %s Это - бесплатное программное обеспечение; см. версию 2 Лицензии ГНУ. Нет никакой гарантии. Предупреждение: %s(l%s): %s
 Предупреждение: %s(l%s): %s 
В строке: %s
 неверная пара ключ-значение после `;': `%s' неправильный формат строки заголовка неправильный формат строки завершения неправильный формат значения критичности не удалось закрыть файл %s: %s невозможно загрузить IO::String: %s файл используется другой программой %s: %s не удалось открыть файл %s: %s формат лога %s не поддерживается не удалось выполнить анализ даты %s неустранимая ошибка при анализе %s поле %s содержит пустые строки >%s< поле %s содержит символ новой строки, после которого идёт непробельный символ >%s< поле %s завершается символом новой строки >%s< найдена пустая строка где ожидалось %s найдены данные изменений где ожидалось %s найден символ конца файла где ожидалось %s найдено начало записи где ожидалось %s найдено завершение где ожидалось %s опция -L игнорирована указано более одного файла (%s и %s) не указан файл изменений формат вывода %s не поддерживается повторение пары ключ-значение %s слишком много аргументов неизвестный ключ %s в паре ключ-значение - копируется в XS-%s нераспознанная строка Вы можете определить либо 'от' либо 'с тех пор' Вы можете только определить либо 'к' либо 'до' нельзя использовать 'count' или 'offset' одновременно с любыми другими параметрами диапазона 