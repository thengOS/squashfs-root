��          �      �       H  (   I  '   r     �     �     �  K   �  +     %   3  %   Y  %     %   �  b   �     .    ;  5   N  4   �     �     �  &   �  }     -   �  A   �  A   �  /   >  7   n  +  �     �                          	                     
                  A cover is open on the printer “%s”. A door is open on the printer “%s”. Paused Printers Printing Problem The printer “%s” can’t be used, because required software is missing. The printer “%s” is currently off-line. The printer “%s” is low on paper. The printer “%s” is low on toner. The printer “%s” is out of paper. The printer “%s” is out of toner. You have %d job queued to print on this printer. You have %d jobs queued to print on this printer. _Settings… Project-Id-Version: indicator-printers
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-04 13:07+0000
PO-Revision-Date: 2012-10-04 15:09+0000
Last-Translator: Auduf <5097@mail.ru>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 19:14+0000
X-Generator: Launchpad (build 18115)
 Крышка принтера  «%s» открыта. Открыта дверца принтера «%s». Приостановлено Принтеры Неполадки при печати Принтер «%s» нельзя использовать, так как отсутствует необходимое ПО Принтер “%s” недоступен. В принтере «%s» недостаточно бумаги. В принтере "%s" заканчивается краска. В принтере «%s» нет бумаги. В принтере «%s» кончился тонер. У вас %d задание в очереди на печать для этого принтера. У вас %d задания в очереди на печать для этого принтера. У вас %d заданий в очереди на печать для этого принтера. _Параметры… 