��   �   0   �  �   �     �     �  �      L  �   M  7    �  W  -      F   N     �     �  7   �  �   �  �   �  �   *  �     �  �  H   j     �  E   3  �   y      >   &  9   e  �   �  �   5  �   �  �   <  �   
  �   �  l   \     �     �     �          5     U     o     �     �  z   �     8      R      l   .   ~   6   �      �      �      !     !  !   &!  !   H!  '   j!     �!     �!     �!  *   �!  /   "  %   L"     r"  /   �"  ,   �"     �"  4   �"     ,#     H#     f#     ~#     �#      �#      �#  h   �#  <   _$     �$  :   �$  $   �$     %  2   *%     ]%  $   z%  /   �%  I   �%     &  3   -&  =   a&  d   �&      '  O   %'  .   u'  /   �'     �'  A   �'  )   1(     [(     d(  8   }(     �(     �(  (   �(  I   )  !   Y)  '   {)  '   �)  9   �)     *      *  0   #*     T*  <   Y*  -   �*  @   �*  /   +  7   5+  D   m+  &   �+  '   �+     ,  %   	,     /,     G,  
   U,  
   `,  
   k,  
   v,  
   �,  	   �,  	   �,  	   �,  	   �,  	   �,  	   �,  	   �,  "   �,  *   �,      -  A   4-  Q   v-  *   �-  @   �-  !   4.     V.  	  Z.    d0  �  ~1  �  r6  I   I9  �   �9  -   <:     j:  m   �:  
  �:  �   �;  �  �<    v>  �  �?  �   oB  �   C  �   �C    ZD  �  kE  [   G  L   wG  F  �G  �   I  �   �I  q  �J    <L  -  TM  �   �N     mO  $   �O  9   �O  I   �O  :   4P  +   oP  *   �P  /   �P  0   �P  
  'Q     2R  A   QR     �R  6   �R  S   �R  ,   9S  +   fS     �S  $   �S  @   �S  F   �S  A   DT  0   �T  7   �T  C   �T  M   3U  V   �U  E   �U  /   V  R   NV  `   �V  (   W  n   +W  <   �W  :   �W  &   X  6   9X  5   pX  H   �X  6   �X  �   &Y  �   �Y  !   KZ  v   mZ  u   �Z  6   Z[  Z   �[  2   �[  S   \  v   s\  �   �\  !   x]  Z   �]  g   �]  �   ]^  J   _  �   P_  ]   �_  W   B`  .   �`  h   �`  H   2a     {a  2   �a  `   �a  +   "b  *   Nb  K   yb  �   �b  C   cc  `   �c  f   d  }   od  G   �d     5e  o   <e     �e  _   �e  s   f  |   �f  K   g  r   Pg  �   �g  O   Mh  L   �h     �h  l   �h  ,   ci  #   �i     �i     �i     �i     �i     j     ,j     Cj     Zj     qj     �j     �j     �j  D   �j  b   k  +   uk  �   �k  �   2l  s   �l  T   Mm  >   �m     �m     ^   =       K   I       |   .           �          �   1              U   u      +          _              5   8       q       #   d       9   `   (       3   c          ]       -   [   �   y   a          J   "   ~   �   j            Z       %           s      0                          x          $   e       A          E      k           ,   }       R       O   B   �       )   ?       @   
       �       L   t   f   G             �       <              !   {   n   l   >   V   Q   2      Y   \       m   M   '           F          D   o      r   ;      p      X   w   v   7   z       	      N         �   i   4           b   *   H   W       6   g      �   �       T   &       h               P   /   :       �      C   S         �m     �m  �       0  �m  2          ����'n  0               ����\n  �          �����n  K               ���� 
  --delta[=OPTS]      Delta filter; valid OPTS (valid values; default):
                        dist=NUM   distance between bytes being subtracted
                                   from each other (1-256; 1) 
  --lzma1[=OPTS]      LZMA1 or LZMA2; OPTS is a comma-separated list of zero or
  --lzma2[=OPTS]      more of the following options (valid values; default):
                        preset=PRE reset options to a preset (0-9[e])
                        dict=NUM   dictionary size (4KiB - 1536MiB; 8MiB)
                        lc=NUM     number of literal context bits (0-4; 3)
                        lp=NUM     number of literal position bits (0-4; 0)
                        pb=NUM     number of position bits (0-4; 2)
                        mode=MODE  compression mode (fast, normal; normal)
                        nice=NUM   nice length of a match (2-273; 64)
                        mf=NAME    match finder (hc3, hc4, bt2, bt3, bt4; bt4)
                        depth=NUM  maximum search depth; 0=automatic (default) 
  --x86[=OPTS]        x86 BCJ filter (32-bit and 64-bit)
  --powerpc[=OPTS]    PowerPC BCJ filter (big endian only)
  --ia64[=OPTS]       IA-64 (Itanium) BCJ filter
  --arm[=OPTS]        ARM BCJ filter (little endian only)
  --armthumb[=OPTS]   ARM-Thumb BCJ filter (little endian only)
  --sparc[=OPTS]      SPARC BCJ filter
                      Valid OPTS for all BCJ filters:
                        start=NUM  start offset for conversions (default=0) 
 Basic file format and compression options:
 
 Custom filter chain for compression (alternative for using presets): 
 Operation modifiers:
 
 Other options:
 
With no FILE, or when FILE is -, read standard input.
       --block-size=SIZE
                      when compressing to the .xz format, start a new block
                      after every SIZE bytes of input; 0=disabled (default)       --info-memory   display the total amount of RAM and the currently active
                      memory usage limits, and exit       --memlimit-compress=LIMIT
      --memlimit-decompress=LIMIT
  -M, --memlimit=LIMIT
                      set memory usage limit for compression, decompression,
                      or both; LIMIT is in bytes, % of RAM, or 0 for defaults       --no-adjust     if compression settings exceed the memory usage limit,
                      give an error instead of adjusting the settings downwards       --no-sparse     do not create sparse files when decompressing
  -S, --suffix=.SUF   use the suffix `.SUF' on compressed files
      --files[=FILE]  read filenames to process from FILE; if FILE is
                      omitted, filenames are read from the standard input;
                      filenames must be terminated with the newline character
      --files0[=FILE] like --files but use the null character as terminator       --robot         use machine-parsable messages (useful for scripts)       --single-stream decompress only the first stream, and silently
                      ignore possible remaining input data       CheckVal %*s Header  Flags        CompSize    MemUsage  Filters   -0 ... -9           compression preset; default is 6; take compressor *and*
                      decompressor memory usage into account before using 7-9!   -F, --format=FMT    file format to encode or decode; possible values are
                      `auto' (default), `xz', `lzma', and `raw'
  -C, --check=CHECK   integrity check type: `none' (use with caution),
                      `crc32', `crc64' (default), or `sha256'   -Q, --no-warn       make warnings not affect the exit status   -V, --version       display the version number and exit   -e, --extreme       try to improve compression ratio by using more CPU time;
                      does not affect decompressor memory requirements   -h, --help          display the short help (lists only the basic options)
  -H, --long-help     display this long help and exit   -h, --help          display this short help and exit
  -H, --long-help     display the long help (lists also the advanced options)   -k, --keep          keep (don't delete) input files
  -f, --force         force overwrite of output file and (de)compress links
  -c, --stdout        write to standard output and don't delete input files   -q, --quiet         suppress warnings; specify twice to suppress errors too
  -v, --verbose       be verbose; specify twice for even more verbose   -z, --compress      force compression
  -d, --decompress    force decompression
  -t, --test          test compressed file integrity
  -l, --list          list information about .xz files   Blocks:
    Stream     Block      CompOffset    UncompOffset       TotalSize      UncompSize  Ratio  Check   Blocks:             %s
   Check:              %s
   Compressed size:    %s
   Memory needed:      %s MiB
   Minimum XZ Utils version: %s
   Number of files:    %s
   Ratio:              %s
   Sizes in headers:   %s
   Stream padding:     %s
   Streams:
    Stream    Blocks      CompOffset    UncompOffset        CompSize      UncompSize  Ratio  Check      Padding   Streams:            %s
   Uncompressed size:  %s
  Operation mode:
 %s MiB of memory is required. The limit is %s. %s MiB of memory is required. The limiter is disabled. %s file
 %s files
 %s home page: <%s>
 %s:  %s: Cannot remove: %s %s: Cannot set the file group: %s %s: Cannot set the file owner: %s %s: Cannot set the file permissions: %s %s: Closing the file failed: %s %s: Error reading filenames: %s %s: Error seeking the file: %s %s: File already has `%s' suffix, skipping %s: File has setuid or setgid bit set, skipping %s: File has sticky bit set, skipping %s: File is empty %s: File seems to have been moved, not removing %s: Filename has an unknown suffix, skipping %s: Filter chain: %s
 %s: Input file has more than one hard link, skipping %s: Invalid filename suffix %s: Invalid multiplier suffix %s: Invalid option name %s: Invalid option value %s: Is a directory, skipping %s: Is a symbolic link, skipping %s: Not a regular file, skipping %s: Null character found when reading filenames; maybe you meant to use `--files0' instead of `--files'? %s: Options must be `name=value' pairs separated with commas %s: Read error: %s %s: Seeking failed when trying to create a sparse file: %s %s: Too small to be a valid .xz file %s: Unexpected end of file %s: Unexpected end of input when reading filenames %s: Unknown file format type %s: Unsupported integrity check type %s: Value is not a non-negative decimal integer %s: With --format=raw, --suffix=.SUF is required unless writing to stdout %s: Write error: %s --list does not support reading from standard input --list works only on .xz files (--format=xz or --format=auto) Adjusted LZMA%c dictionary size from %s MiB to %s MiB to not exceed the memory usage limit of %s MiB Cannot establish signal handlers Cannot read data from standard input when reading filenames from standard input Compressed data cannot be read from a terminal Compressed data cannot be written to a terminal Compressed data is corrupt Compression and decompression with --robot are not supported yet. Decompression will need %s MiB of memory. Disabled Empty filename, skipping Error restoring the O_APPEND flag to standard output: %s File format not recognized Internal error (bug) LZMA1 cannot be used with the .xz format Mandatory arguments to long options are mandatory for short options too.
 Maximum number of filters is four Memory usage limit for compression:     Memory usage limit for decompression:   Memory usage limit is too low for the given filter setup. Memory usage limit reached No No integrity check; not verifying file integrity None Only one file can be specified with `--files' or `--files0'. Report bugs to <%s> (in English or Finnish).
 Strms  Blocks   Compressed Uncompressed  Ratio  Check   Filename The .lzma format supports only the LZMA1 filter The environment variable %s contains too many arguments The exact options of the presets may vary between software versions. The sum of lc and lp must not exceed 4 Total amount of physical memory (RAM):  Totals: Try `%s --help' for more information. Unexpected end of input Unknown error Unknown-11 Unknown-12 Unknown-13 Unknown-14 Unknown-15 Unknown-2 Unknown-3 Unknown-5 Unknown-6 Unknown-7 Unknown-8 Unknown-9 Unsupported LZMA1/LZMA2 preset: %s Unsupported filter chain or filter options Unsupported options Unsupported type of integrity check; not verifying file integrity Usage: %s [OPTION]... [FILE]...
Compress or decompress FILEs in the .xz format.

 Using a preset in raw mode is discouraged. Valid suffixes are `KiB' (2^10), `MiB' (2^20), and `GiB' (2^30). Writing to standard output failed Yes Project-Id-Version: xz-utils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-04 03:00+0000
PO-Revision-Date: 2013-03-10 19:03+0000
Last-Translator: Sly_tom_cat <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 18:40+0000
X-Generator: Launchpad (build 18115)
 
  --delta[=ОПЦИИ]     Дельта фильтр; допустимые ОПЦИИ:
                        dist=РАССТОЯНИЕ   Расстояние между байтами, вычитаемыми
                                   друг из друга (1-256; 1) 
  --lzma1[=OPTS]      LZMA1 или LZMA2; OPTS это перечень указываемых через запятую нулевых или
  --lzma2[=OPTS]      нескольких параметров (действующих значений, по умолчанию):
                        preset=PRE восстановить параметры профиля (0-9[e])
                        dict=NUM   размер словаря (4 КБ - 1536 МБ; 8 МБ)
                        lc=NUM     количество буквенных контекстных бит (0-4; 3)
                        lp=NUM     количество буквенных позиционных бит  (0-4; 0)
                        pb=NUM     количество позиционных бит (0-4; 2)
                        mode=MODE  режим сжатия (быстрый, нормальный, нормальный)
                        nice=NUM   подходящая длинна соответствия (2-273; 64)
                        mf=NAME    поиск соответствий (hc3, hc4, bt2, bt3, bt4; bt4)
                        depth=NUM  максимальная глубина поиска; 0=автоматическая (по умолчанию) 
  --x86[=OPTS]        x86 BCJ фильтр (32-разрядные и 64-разрядные)
  --powerpc[=OPTS]    PowerPC BCJ фильтр (только обратный порядок байтов)
  --ia64[=OPTS]       IA-64 (Itanium) BCJ фильтр
  --arm[=OPTS]        ARM BCJ фильтр (только прямой порядок байтов)
  --armthumb[=OPTS]   ARM-Thumb BCJ фильтр (только прямой порядок байтов)
  --sparc[=OPTS]      SPARC BCJ фильтр
                      Действующие OPTS для всех фильтров BCJ:
                        start=NUM  выполнить смещение для преобразований (по умолчанию=0) 
 Основной формат файлов и опций сжатия:
 
 Пользовательская цепочка фильтров для сжатия (альтернатива используемым предустановкам): 
 Модификаторы операции:
 
 Другие опции:
 
Если ФАЙЛ не задан или задан как -, читает стандартный ввод.
       --block-size=РАЗМЕР
                      при сжатии в формате .xz, начинать новый блок,
                      после каждых РАЗМЕР байт на входе; 0=выключить (по умолчанию)       --info-memory   показать общее количество ОЗУ, действующие
                      ограничения использования памяти и выйти       --memlimit-compress=LIMIT
      --memlimit-decompress=LIMIT
  -M, --memlimit=LIMIT
                      назначает ограничение доступной памяти для сжатия, извлечения,
                      или обеих операций; LIMIT указывается в байтах, % от ОЗУ или 0 для использования настроек по умолчанию       --no-adjust     если параметры сжатия превышают ограничение доступной памяти,
                      выдают ошибку вместо выравнивания до указанных параметров       --no-sparse     не создавать частичные файлы при извлечении
  -S, --suffix=.SUF   использовать суффикс `.SUF' для сжатых файлов
      --files[=FILE]  считывать названия файлов для обработки из FILE; если файл FILE
                      пропущен, названия файлов считываются из стандартного ввода;
                      названия файлов должны быть завершены символом перевода строки
      --files0[=FILE] like --files но используется символ пробела в качестве завершителя       --robot         использовать машинно-анализируемые сообщения (полезно для сценариев)       --single-stream извлекает только первый поток и скрыто 
                      пропускает возможные оставшиеся вводные данные       ПроверкаЗнч %*s Заголовок  Флаги        СжатыйРазмер    ИспПамяти  Фильтры   -0 ... -9           профиль сжатия; по умолчанию 6; для использования режимов сжатия *и*
                      извлечения 7-9, убедитесь в достаточном количестве памяти!   -F, --format=FMT    формат файла для кодирования или декодирования; возможные значения
                      `auto' (по умолчанию), `xz', `lzma' и `raw'
  -C, --check=CHECK   тип проверки целостности: `none' (используйте внимательно),
                      `crc32', `crc64' (по умолчанию) или `sha256'   -Q, --no-warn       ошибки не влияют на статус завершения   -V, --version       показать номер версии и выйти   -e, --extreme       возможность увеличения сжатия за счёт использования длительного процессорного времени;
                      не приводит к изменению требований объёма памяти для извлечения   -h, --help          показать короткую справку (список основных опций)
  -H, --long-help     показать эту подробную справку и выйти   -h, --help          показать эту короткую справку и выйти
  -H, --long-help     показать подробную справку (отображает также дополнительные опции)   -k, --keep сохранить (не удалять) входные файлы
  -f, --force принудительно перезаписать поверх выходных файлов и сжатых/распакованных ссылок
  -c, --stdout вывод в стандартный поток выводы и не удалять входные файлы   -q, --quiet         подавлять предупреждения; укажите дважды для подавления ошибок
  -v, --verbose       подробный вывод; укажите дважды для ещё более подробного вывода   -z, --compress      принудительное сжатие
  -d, --decompress    принудительное извлечение
  -t, --test          проверить целостность сжатого файла
  -l, --list          показывает список о файлах .xz   Блоки:
    Поток     Блок      СмещениеСжатого    СмещениеИзвлечённого       ОбщийРазмер      РазмерИзвлечённого  Коэффициент  Проверка   Блоки:             %s
   Проверка:              %s
   Размер в сжатом состоянии:    %s
   Необходимое количество памяти:      %s МБ
   Минимальная версия XZ утилит: %s
   Количество файлов:    %s
   Коэффициент:              %s
   Размеры в заголовках:   %s
   Выравнивание потока:     %s
   Потоки:
    Поток    Блоки      СмещениеСжатия    СмещениеИзвлечения        РазмерСжатого      РазмерИзвлечённого  Коэффициент  Проверка      Выравнивание   Потоки:            %s
   Размер в извлечённом состоянии:  %s
  Режим работы:
 %s MiB памяти требуется. Лимит %s. Требуется %s МБ памяти. Ограничитель отключён. %s файл
 %s файлы
 %s файлов
 Домашняя страница %s: <%s>
 %s:  %s: Не могу удалить: %s %s: Не могу выставить группу файлу: %s %s: Не могу выставить владельца файла: %s %s: Не могу выставить права на файл: %s %s: Ошибка закрытия файла: %s %s: Ошибка чтения имён файлов: %s %s: Ошибка позиционирования в файле: %s %s: Файл уже имеет расширение `%s' , пропущено %s: У файла выставлен бит setuid или setgid, пропускаем %s: У файла выставлен sticky bit, пропускаем %s: Файл не содержит данных %s: Файл кажется был перемещён, нечего удалять %s: Имя файла имеет неизвестное расширение, пропущено %s: Цепочка фильтров: %s
 %s: Входной файл имеет более одной жёсткой ссылки, пропускаем %s: Неправильное расширение файла %s: Неверное окончание множителя %s: Неверное имя опции %s: Невозможное значение опции %s: Это директория, пропускаем %s: Это символическая ссылка, пропускаем %s: Не обычный файл, пропускаем %s: В именах файлов найден нулевой символ; возможно нужно использовать `--files0' вместо `--files'? %s: Опции должны идти парами `название=значение' и разделяться запятыми. %s: Ошибка чтения: %s %s: Сбой позиционирования при попытке создать разреженный файл: %s %s: Имеет незначительный размер, чтобы быть совместимым файлом .xz %s: Непредвиденный конец файла %s: Неожиданный конец ввода при чтении имён файлов %s: неизвестный формат файла %s: Неподдерживаемый тип проверки целостности %s: Значение не является неотрицательным целым десятичным числом %s: --format=raw и --suffix=.SUF требуются только, если вывод идёт в стандартный вывод (stdout) %s: Ошибка записи: %s --list не поддерживает чтение из стандартного ввода --list используется только для файлов .xz (--format=xz или --format=auto) Изменен размера словаря LZMA%c с %s MiB до %s MiB чтобы не превысить лимит используемой памяти в %s MiB Не могу установить обработчики сигналов Не могу прочитать данные со стандартного ввода при чтении имён файлов оттуда же. Сжатые данные не могут быть прочитаны из терминала Сжатые данные не могут быть записаны в терминал Сжатые данные повреждены Сжатие и распаковка с флагом --robot пока не поддерживается. Для извлечения необходимо %s МиБ памяти. Отключено Пустое имя файла, пропущено Сбой установки флага O_APPEND для стандартного вывода: %s Формат файла не опознан Внутренняя ошибка (баг) LZMA1 не может быть использован в формате .xz Обязательные аргументы для длинных названий опций обязательны так же и для коротких.
 Максимальное число фильтров - четыре Количество памяти допустимое для операций сжатия:     Количество памяти допустимое для операций извлечения:   Лимит используемой памяти слишком мал для данной настройки фильтра. Достигнут предел использования памяти Нет Без проверки на целостность; не проверяем целостность файла. Нет Только один файл может быть указан с `--files' или `--files0'. Отчёты об ошибках присылайте на <%s> (на английском или финском).
 Потоки  Блоки   Сжатые Извлечённые  Коэффициент  Проверка   Имя файла Формат .lzma поддерживает только LZMA1 фильтр Переменная среды %s содержит избыточное количество аргументов Точное число предустановок может различаться в разных версиями программы. Общее количество lc и lp не должно превышать 4 Общее количество физической памяти (ОЗУ):  Всего: Попробуйте `%s --help' для получения более подробного описания. Неожиданный конец ввода Неизвестная ошибка Неизвестно-11 Неизвестно-12 Неизвестно-13 Неизвестно-14 Неизвестно-15 Неизвестно-2 Неизвестно-3 Неизвестно-5 Неизвестно-6 Неизвестно-7 Неизвестно-8 Неизвестно-9 Не поддерживаемая LZMA1/LZMA2 настройка: %s Не поддерживаемая цепочка фильтров или опций фильтра Неподдерживаемые опции Неподдерживаемый тип проверки на целостность; не проверяем целостность файла. Использование: %s [OPTION]... [FILE]...
Сжатие или распаковка файлов перечисленных в FILE в формате .xz.

 Использовать предустановки в "чистом" режиме не рекомендуется. Правильные окончания `KiB' (2^10), `MiB' (2^20), и `GiB' (2^30). Ошибка записи в стандартный вывод Да PRIu32 PRIu64 The selected match finder requires at least nice=% Value of the option `%s' must be in the range [%, %] Выбранный искатель совпадений требует, по меньшей мере, приоритет равный % Значение опции `%s' может быть в пределах [%, %] 