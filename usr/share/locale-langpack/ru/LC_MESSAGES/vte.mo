��          �      \      �  $   �  )   �        6   9     p  /   �  (   �     �  +   �     $     B  $   ^  -   �  +   �  7   �       &   -  /   T     �  �  �  f   U  u   �  3   2  �   f  !   �  �     m   �  X   	  �   _	  L   �	  Q   4
  j   �
  m   �
  d   _  �   �  5   k  =   �  w   �  ,   W                                  	                      
                                           Attempt to set invalid NRC map '%c'. Attempt to set invalid wide NRC map '%c'. Could not open console.
 Could not parse the geometry spec passed to --geometry Duplicate (%s/%s)! Error (%s) converting data for child, dropping. Error compiling regular expression "%s". Error creating signal pipe. Error reading PTY size, using defaults: %s. Error reading from child: %s. Error setting PTY size: %s. Got unexpected (key?) sequence `%s'. No handler for control sequence `%s' defined. Unable to convert characters from %s to %s. Unable to send data to child, invalid charset convertor Unknown pixel mode %d.
 Unrecognized identified coding system. _vte_conv_open() failed setting word characters can not run %s Project-Id-Version: ru
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=vte&component=general
POT-Creation-Date: 2007-10-06 10:12+0000
PO-Revision-Date: 2016-02-08 00:37+0000
Last-Translator: Yuri Myasoedov <Unknown>
Language-Team: <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:07+0000
X-Generator: Launchpad (build 18115)
 Произошла попытка установить недопустимую карту NRC «%c». Произошла попытка установить недопустимую широкую карту NRC «%c». Не удалось открыть консоль.
 Не удалось разобрать спецификацию геометрии, переданную параметру --geometry Дублирование (%s/%s)! Произошла ошибка (%s) при преобразовании данных для потомка, отбрасывается. Произошла ошибка при компиляции регулярного выражения «%s». Произошла ошибка при создании канала сообщений. Произошла ошибка при чтении размера PTY, используются исходные значения: %s. Произошла ошибка при чтении из потомка: %s. Произошла ошибка при установке размера PTY: %s. Получена непредусмотренная (ключ?) последовательность "%s". Отсутствует обработчик управляющей последовательности "%s". Не удалось преобразовать символы из набора %s в набор %s. Не удаётся отправить данные дочернему процессу, неверный преобразователь кодовой таблицы Режим пикселей %d неизвестен.
 Неизвестная система кодирования. Произошёл сбой в функции _vte_conv_open() при установке символов в слове невозможно запустить "%s" 