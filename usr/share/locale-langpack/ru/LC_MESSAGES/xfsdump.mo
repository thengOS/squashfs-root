��    y      �  �   �      8
  
   9
     D
     Y
     o
     �
  9   �
  9   �
  4        B  $   a     �  O   �     �     �  A   �     7  )   V  3   �  ,   �  2   �  3     )   H  /   r  /   �  9   �  @        M  "   j  7   �  1   �  <   �  >   4  7   s     �  A   �  R   �  !   F  1   h  /   �  0   �     �       E   (  W   n  '   �  #   �  	          '   .     V  
   u  '   �     �  2   �  8   �     )  &   F  2   m     �     �     �     �  /   �          /     E     [  2   i  !   �  =   �  ;   �  )   8     b     ~  (   �  7   �     �        0   %  (   V       3   �  *   �  [   �  &   K  )   r  &   �  @   �  ?     5   D  "   z  K   �  (   �          +  /   E     u  $   �     �  
   �     �     �  /   �  .   +  !   Z  1   |  )   �     �     �  *     /   :  P   j  K   �  K     <   S  >   �  ?   �          "     A  �  [        (     )   D  '   n  D   �  u   �  u   Q   W   �   >   !  o   ^!  '   �!  y   �!     p"     u"  �   y"  >   #  H   Y#  Q   �#  K   �#  Q   @$  R   �$  <   �$  u   "%  q   �%  �   
&  �   �&  =   '  A   R'  |   �'  O   (  p   a(  j   �(  U   =)  
   �)  �   �)  �   G*  M   +  u   U+  �   �+  �   M,  (   �,  +   �,  �   &-  �   �-  s   s.  ?   �.     '/  9   8/  j   r/  S   �/     10  T   Q0  3   �0  g   �0  n   B1  5   �1  5   �1  W   2     u2  4   �2  -   �2  9   �2  S   (3  )   |3  -   �3  .   �3     4  [   #4  ,   4  i   �4  �   5  W   �5  &   �5  .   6  y   J6  �   �6     m7  K   �7  c   �7  [   98  .   �8  �   �8  l   H9  �   �9  [   j:  [   �:  q   ";  q   �;  �   <  i   �<  ]   �<  �   T=  ]   �=  3   @>  >   t>  >   �>  B   �>  *   5?  K   `?     �?     �?  3   �?  Q   @  t   e@  @   �@  |   A  g   �A  H    B  4   IB  x   ~B  ~   �B  �   vC  �   9D  �   �D  �   �E  �   _F  �   	G  /   �G  ?   �G  V   �G         (   %   T   2   U   >           1                     I      .   J       @   *          R   i   &           s   k   '   e   M   b      :   D   6       E   Y   f      <                 c   7   B   x                  a      -   !   n           )   l           W       [                        4   =   g   q                  #      +   N   u   `   G                  _   ]   ^   3   \       O   v   d   S          Q   o   p       r      j       A         ;          L   Z         9       m   ?          V   P   X   t       w           h   
   5   /         y       0       F      C   K   	          $   H   "   8   ,     (default)  (timeout in %u sec)  (timeout in %u sec)
 (timeout in %d sec) -%c argument missing
 -%c argument must be a positive number (MB): ignoring %u
 -%c argument must be a positive number (Mb): ignoring %u
 -%c argument must be between %u and %u: ignoring %u
 -%c argument not a valid uuid
 Can't open %s for mount information
 FSF tape command failed
 I/O metrics: %u by %s%s %sring; %lld/%lld (%.0lf%%) records streamed; %.0lfB/s
 KB MB Minimal rmt cannot be used without specifying blocksize. Use -%c
 Overwrite command line option
 RMTCLOSE( %d ) returns %d: errno=%d (%s)
 RMTIOCTL( %d, %d, 0x%x ) returns %d: errno=%d (%s)
 RMTOPEN( %s, %d ) returns %d: errno=%d (%s)
 RMTREAD( %d, 0x%x, %u ) returns %d: errno=%d (%s)
 RMTWRITE( %d, 0x%x, %u ) returns %d: errno=%d (%s)
 This tape was erased earlier by xfsdump.
 WARNING: could not stat dirent %s ino %llu: %s
 WARNING: unable to open directory ino %llu: %s
 WARNING: unable to process dirent %s: ino %llu too large
 WARNING: unable to read dirents (%d) for directory ino %llu: %s
 abnormal dialog termination
 advancing tape to next media file
 assuming media is corrupt or contains non-xfsdump data
 attempt to access/open device %s failed: %d (%s)
 attempt to access/open remote tape drive %s failed: %d (%s)
 attempt to get status of remote tape drive %s failed: %d (%s)
 attempt to get status of tape drive %s failed: %d (%s)
 audio bad media file header at BOT indicates foreign or corrupted tape
 cannot create %s thread for stream %u: too many child threads (max allowed is %d)
 cannot determine tape block size
 cannot determine tape block size after two tries
 cannot specify source files and stdin together
 cannot specify source files and stdout together
 change media dialog could not create %s: %s
 could not forward space %d tape blocks: rval == %d, errno == %d (%s)
 could not forward space one tape block beyond read error: rval == %d, errno == %d (%s)
 could not fstat stdin (fd %d): %s (%d)
 could not read from drive: %s (%d)
 drive %u  dump label dialog encountered EOD : assuming blank media
 encountered EOD : end of data
 end dialog error removing temp DMF attr on %s: %s
 examining new media
 failed to get bulkstat information for inode %llu
 failed to get valid bulkstat information for inode %llu
 file mark missing from tape
 file mark missing from tape (hit EOD)
 giving up attempt to determining tape record size
 hangup
 hostname length is zero
 keyboard interrupt
 keyboard quit
 malloc of stream context failed (%d bytes): %s
 may be an EFS dump at BOT
 media change aborted
 media change declined media changed media file header version (%d) invalid: advancing
 no destination file(s) specified
 no media strategy available for selected dump destination(s)
 no media strategy available for selected restore source(s)
 no more data can be written to this tape
 no session label specified
 no source file(s) specified
 not allowed to pin down I/O buffer ring
 not enough physical memory to pin down I/O buffer ring
 pinned  please change media in drive %u
 please enter a value between 1 and %d inclusive  please enter label for this dump session preparing drive
 read_record encountered EOD : assuming blank media
 read_record encountered EOD : end of data
 recommended media file size of %llu Mb less than estimated file header size %llu Mb for %s
 record %lld corrupt: bad magic number
 record %lld corrupt: bad record checksum
 record %lld corrupt: dump id mismatch
 record %lld corrupt: incorrect record offset in header (0x%llx)
 record %lld corrupt: incorrect record padding offset in header
 record %lld corrupt: incorrect record size in header
 record %lld corrupt: null dump id
 record %lld corrupt: record offset in header not a multiple of record size
 resynchronized at record %lld offset %u
 session label entered: " session label left blank
 syssgi( SGI_FS_BULKSTAT ) on fsroot failed: %s
 tape is write protected
 tape media error on write operation
 tape record checksum error
 terminate
 timeout
 too many -%c arguments
 too many -%c arguments: "-%c %s" already given
 unable to allocate memory for I/O buffer ring
 unable to determine hostname: %s
 unable to determine uuid of fs mounted at %s: %s
 unable to locate next mark in media file
 unable to set block size to %d
 unable to stat %s: %s
 unable to write file mark at eod: %s (%d)
 unexpected EIO error attempting to read record
 unexpected error attempting to read record %lld: read returns %d, errno %d (%s)
 unexpected error attempting to read record: read returns %d, errno %d (%s)
 unexpected error attempting to read record: read returns %d, errno %s (%s)
 unexpected tape error: errno %d nread %d blksz %d recsz %d 
 unexpectedly encountered EOD at BOT: assuming corrupted media
 unexpectedly encountered a file mark: assuming corrupted media
 using %s strategy
 valid record %lld but no mark
 writing file mark at EOD
 Project-Id-Version: xfsdump
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-08 05:09+0000
PO-Revision-Date: 2012-08-24 14:05+0000
Last-Translator: Beatrix Kiddo <mashksharn@gmail.com>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:22+0000
X-Generator: Launchpad (build 18115)
  (по умолчанию)  (время ожидания %u сек)  (время ожидания %u сек)
 (время ожидания %d сек) аргумент -%c неверный или отсутствует
 аргумент -%c должен быть положительным числом (MB): %u игнорируется
 аргумент -%c должен быть положительным числом (Mb): %u игнорируется
 -%c  аргумент должен быть между %u и %u: игнорируя %u
 аргумент -%c  не является верным uuid
 невозможно открыть %s для получения информации монтирования
 Сбой команды ленты FSF
 метрики ввода/вывода: %u от %s%s %sring; %lld/%lld (%.0lf%%) текущих записей; %.0lfB/s
 Кб Mб Минимальное значение rmt не может быть задано без указания размера блока. Используйте -%c
 Перекрыть опцию командной строки
 RMTCLOSE( %d ) возвращает %d: номер ошибки=%d (%s)
 MTIOCTL( %d, %d, 0x%x ) возвращает %d: номер ошибки=%d (%s)
 RMTOPEN( %s, %d ) возвращает %d: номер ошибки=%d (%s)
 RMTREAD( %d, 0x%x, %u ) возвращает %d: номер ошибки=%d (%s)
 RMTWRITE( %d, 0x%x, %u ) возвращает %d: номер ошибки=%d (%s)
 xfsdump уже затирал эту ленту ранее.
 Предупреждение: невозможно выполнить stat %s для дескриптора %llu: %s
 Предупреждение: невозможно открыть дескриптор каталога %llu: %s
 Предупреждение: невозможно обработать %s: дескриптор %llu слишком велик
 Предупреждение: невозможно считать поля (%d) для дескриптора каталога %llu: %s
 аварийное диалоговое завершение
 протяжка ленты до следующего файла
 вставленный носитель поврежден или содержит данные не утилиты xfsdump
 ошибка попытки доступа к устройству %s: %d (%s)
 ошибка при попытке доступа к удаленному приводу ленты %s: %d (%s)
 ошибка получения статуса удаленного привода ленты %s: %d (%s)
 ошибка получения статуса привода ленты %s: %d (%s)
 аудио плохой заголовочный файл носителя на BOT показывает неподдерживаемую или разрушенную ленту
 Нет возможности создать нить %s для потока данных %u: слишком много дочерних нитей (предельно допустимо %d)
 невозможно определить размер блока ленты
 невозможно определить размер блока на ленте после двух попыток
 нет возможности задать файл-источник и стандартный ввод одновременно
 нет возможности задать файл-приемник и стандартный вывод одновременно
 диалог смены носителя невозможно создать %s: %s
 невозможно переместить блоки ленты %d: значение ошибки == %d, номер ошибки == %d (%s)
 невозможно переместить блок на ленте без возникновения ошибки: значение ошибки == %d, номер ошибки == %d (%s)
 нет возможности выполнить fstat для стандартного ввода (fd %d): %s (%d)
 невозможно читать из привода: %s (%d)
 привод %u  диалог определения имени дампа сдвинутая метка конца носителя: возможно пустой носитель
 сдвинутая метка конца носителя: конец данных
 завершить диалог ошибка удаления временного DFM атрибута на %s: %s
 проверяется новый носитель
 невозможно получить информацию bulkstat для дескриптора %llu
 ошибка получения верной информации bulkstat для дескриптора %llu
 метка файла на ленте неверна
 неверная метка файла с ленты
 прекращение попыток определения размера ленты
 прерывание
 длина имени хоста 0 символов
 прерывание с клавиатуры
 выход по сигналу от клавиатуры
 сбой malloc для контекста потока данных (%d bytes): %s
 возможно сброс EFS на BOT
 смена носителя отменена
 смена носителя завершена носитель заменён неверный заголовочный файл носителя (%d): протяжка
 не задан целевой файл(ы)
 недоступна стратегия носителя для выбранных целей дампа
 недоступна стратегия носителя для выбранных источников восстановления
 больше невозможно добавить данные на эту ленту
 не задано имя сессии
 не задан исходный файл(ы)
 невозможно завершить организацию кольцевого буфера ввода/вывода
 недостаточно физической памяти для завершения организации кольцевого буфера ввода/вывода
 зафиксировано  пожалуйста, смените носитель в приводе %u
 Пожалуйста, введите значение между 1 и %d  включительно  пожалуйста, введите название для этой дамп сессии подготавливается привод
 read_record показывает сдвиг метки конца носителя: возможно пустой носитель
 read_record показывает сдвиг метки конца носителя: конец данных
 рекомендуемый размер медиа-файла %llu Мб меньше чем ожидаемый размер заголовочного файла %llu Mb для %s
 запись %lld повреждена: неверное контрольное число
 запись %lld повреждена: неверная контрольная сумма
 запись %lld повреждена: несоответствующий идентификатор дампа
 запись %lld повреждена: некорректное смещение в заголовке (0x%llx)
 запись %lld повреждена: некорректное дополнительное смещение в заголовке
 запись %lld повреждена: неверный размер записи в заголовке
 запись %lld повреждена: нулевой идентификатор дампа
 запись %lld повреждена: положение указателя выходит за пределы размера записи
 повторно синхронизирована запись %lld по смещению %u
 введенное название сессии: " название сессии оставлено пустым
 сбой syssgi( SGI_FS_BULKSTAT ) для корня ФС: %s
 для ленты включена защита от записи
 ошибка записи на ленту
 ошибка проверки контрольной суммы ленты
 завершение
 время ожидания
 слишком много -%c аргументов
 слишком много -%c аргументов "-%c %s" уже заданы.
 невозможно выделить память для кольцевого буфера ввода/вывода
 невозможно определить имя хоста: %s
 определение uuid файловой системы, смонтированной на %s, невозможно: %s
 невозможно адресовать следующую метку в файле носителя
 невозможно установить размер блока в %d
 невозможно выполнить stat %s: %s
 невозможно записать метку файла в зоне метки конца носителя: %s (%d)
 непредусмотренная ошибка ввода/вывода при попытке прочитать запись
 непредусмотренная ошибка при попытке прочитать запись%lld: операция чтения возвращает %d, номер ошибки %d (%s)
 непредусмотренная ошибка при попытке прочитать запись: операция чтения возвращает %d, номер ошибки %d (%s)
 непредусмотренная ошибка при попытке прочитать запись: операция чтения вернула %d, номер ошибки %s (%s)
 непредусмотренная ошибка ленты: номер ошибки %d число попыток %d размер блока %d размер записи %d 
 непредусмотренная ошибка сдвига метки конца носителя на BOT: возможно поврежденный носитель
 непредусмотренный сдвиг метки файла: возможно поврежденный носитель
 используется стратегия %s
 запись %lld исправна, но нет отметки
 запись метки файла в зоне метки конца носителя
 