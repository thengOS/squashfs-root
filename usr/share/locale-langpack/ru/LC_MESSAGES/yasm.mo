��    T      �  q   \         �   !     �     �     �     �  &     "   ,     O  #   m     �     �     �     �  "   �     	     6	  /   O	     	     �	     �	     �	  .   �	     �	      �	     
     %
     2
     K
     h
     p
     u
     ~
     �
     �
     �
     �
     �
     �
     �
     �
  !   �
               ,  %   L     r     x          �     �     �     �  4   �     �     �     �  !     '   4  +   \  2   �  &   �  (   �  !     '   -     U     d     v     �     �     �     �      �  $   �  	     K     ?   a  >   �  =   �  @     #   _  6   �  (   �  !   �  �    �   �     j     |     �     �  +   �  ;   �  >   )  G   h  %   �  $   �     �  4     =   K  1   �  3   �  q   �     a     x     �  >   �  J   �  *   0  K   [     �     �  @   �  )        =     L     U     g  D   t     �     �     �     �  8        K     X  L   x     �     �  >   �  Q   >     �     �     �     �     �     �     �  �   �     �     �  O   �     .  ]   �  �     j   �  �      �   �  `     �   q  *   �  ,   "  (   O  
   x     �  9   �  $   �  M   �  R   ?      �   �   �   �   S!  �   "  �   �"  �   x#  9   0$  |   j$  L   �$  >   4%        R   G   /          9      5      B   L          +   O      
                1   "   0   <      !   7   .       H   $       P   ;   3      I             :      J       )             D   F           A          >       &       4      '       (   N           	   ?                                @   S      =                   E   K   -   M      Q      6   2      ,          *   T       %   C   8   #                      
Files are asm sources to be assembled.

Sample invocation:
   yasm -f elf -o object.o source.asm

Report bugs to bug-yasm@tortall.net
 ".%s" without ".if" ".endif" without ".if" ".endm" without ".macro" ".endr" without ".rept" %s: `%s' is not a valid %s for %s `%s' %s: could not initialize BitVector %s: could not load default %s %s: could not load standard modules %s: unrecognized %s `%s' Available %s for %s `%s':
 Available yasm %s:
 Could not open input file INTERNAL ERROR at %s, line %u: %s
 LEB128 requires constant values No input files specified Unsupported floating-point arithmetic operation arch architecture architectures command line too long! could not determine start in bc_tobytes_incbin could not open file `%s' data values can't have registers debug debug format enables/disables warning error when reading from file error:  file filename format inhibits warning messages list list format list formats listfile load plugin module macro macro[=value] negative value in unsigned LEB128 object format object formats option `-%c' needs an argument! overflow in floating point expression param parser parsers path plugin prefix preproc preprocess only (writes output to stdout by default) preprocessor preprocessors redirect error messages to file redirect error messages to stdout select architecture (list with -a help) select debugging format (list with -g help) select error/warning message style (`gnu' or `vc') select list format (list with -L help) select object format (list with -f help) select parser (list with -p help) select preprocessor (list with -r help) show help text show license text show version text style suffix symbol "%s" is already defined undefine a macro unexpected EOF in ".macro" block usage: yasm [option]* file
Options:
 warning:  warning: can open only one input file, only the last file will be processed warning: can output to only one error file, last specified used warning: can output to only one list file, last specified used warning: can output to only one map file, last specified used warning: can output to only one object file, last specified used warning: could not load plugin `%s' warning: object format `%s' does not support map files warning: unrecognized message style `%s' warning: unrecognized option `%s' Project-Id-Version: yasm
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-08-10 22:26-0700
PO-Revision-Date: 2014-02-14 21:06+0000
Last-Translator: Oleg Baykov <krimdono@yandex.ru>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 
Файлы исходников asm для сборки.

Пример вызова:
   yasm -f elf -o object.o source.asm

Отчёт об ошибках отправлять bug-yasm@tortall.net
 "%s" без ".if" ".endif" без ".if" ".endm" без ".macro" ".endr" без ".rept" %s: `%s' неверный %s для %s `%s' %s: нельзя инициализировать BitVector %s: нельзя загрузить по умолчанию %s %s: нельзя загрузить стандартные модули %s: не распознано %s `%s' Доступно %s для %s `%s':
 Доступен yasm %s:
 Не могу открыть входной файл ВНУТРЕННЯЯ ОШИБКА в %s, строка %u: %s
 LEB128 должно быть константой Не определены входные файлы Неподдерживаемая арифметическая операция с плавающей точкой архитектура архитектура архитектуры слишком длинная командная строка! не удается определить начало в bc_tobytes_incbin не могу открыть файл `%s' значения данных не могут иметь регистров отладка формат отладки включить/выключить предупреждения ошибка чтения из файла ошибка:  файл имя файла формат запрещать предупреждающие сообщения список формат списка форматы списка файл списка загрузить подключаемый модуль макрос макрос[=значение] отрицательное значение в беззнаковом LEB128 формат объекта форматы объекта параметру `--%c' требуется аргумент! переполнение в выражении с плавающей точкой параметр парсер парсеры путь плагин префикс препроцессор только предварительна обработка (по умолчанию запись вывода на стандартное устройство вывода) препроцессор препроцессоры перенаправлять сообщения об ошибках в файл перенаправлять сообщения об ошибках в стандартное устройство вывода выберите архитектуру (-a - посмотреть полный список) выберите формат отладки (чтобы просмотреть список используйте параметр -g) выбрать стиль сообщений ошибка/предупреждение (`gnu' или `vc') выберите формат списка (чтобы просмотреть список используйте параметр -L) выберите формат объекта (чтобы просмотреть список используйте параметр -r) выберите парсер (поможет получить список параметр -p) выберите препроцессор (чтобы просмотреть список используйте параметр -r) показать текст справки показать текст лицензии показать текст версии стиль суффикс идентификатор "%s" уже определён макрос не определён непредвиденный конец файла EOF в блоке ".macro" использование: yasm [параметр]* файл
Параметры:
 предупреждение:  внимание: можно открыть только один входной файл, только последний файл будет обработан предупреждение: вывод ошибок возможен только в один файл, будет использован последний указанный предупреждение: вывод списка возможен только в один файл, будет использован последний указанный предупреждение: вывод карты символов возможен только в один файл, будет использован последний указанный предупреждение: вывод возможен только в один объектный файл, будет использован последний указанный внимание: не загружен плагин `%s' предупреждение: объектный формат `%s' не поддерживает карты символов внимание: не распознан стиль сообщения `%s' внимание: неизвестный параметр `%s' 