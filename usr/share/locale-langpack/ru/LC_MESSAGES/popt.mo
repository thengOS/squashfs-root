��             +         �     �     �     �  "   �               '     +     0     9  (   >     g     m     t     �     �     �     �     �     �     �               7  /   H     x     �  )   �     �     �     �  �       �     �  T   �  ;   �     4  /   :     j     n     s     |  J   �     �     �  &   �  %         &     B     F  \   \  l   �  F   &	  :   m	  1   �	  !   �	  ^   �	  Z   [
  &   �
  S   �
     1  #   N  '   r                                                                 	                 
                                                              ARG DOUBLE Display brief usage message Display option defaults in message FLOAT Help options: INT LONG LONGLONG NONE Options implemented via popt alias/exec: SHORT STRING Show this help message Terminate options Usage: VAL [OPTION...] aliases nested too deeply config file failed sanity test error in parameter quoting invalid numeric value memory allocation failed missing argument mutually exclusive logical operations requested number too large or too small opt->arg should not be NULL option type (%u) not implemented in popt
 unknown errno unknown error unknown option Project-Id-Version: popt 1.14
Report-Msgid-Bugs-To: <popt-devel@rpm5.org>
POT-Creation-Date: 2010-02-17 13:35-0500
PO-Revision-Date: 2013-03-12 05:02+0000
Last-Translator: Ivan Ysh <Unknown>
Language-Team: Russian <gnu@mx.ru>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:04+0000
X-Generator: Launchpad (build 18115)
 ARG DOUBLE Показать краткую инструкцию по использованию Показать параметры по умолчанию FLOAT Параметры вывода справки: INT LONG LONGLONG NONE Параметры, реализованные через popt alias/exec: SHORT STRING Показать эту справку Завершает параметры Использование: VAL [ПАРАМЕТР...] превышен уровень допустимой рекурсии подстановок Тест очистки конфигурационного файла закончился с ошибкой ошибка помещения параметров в кавычки неправильное числовое значение не удалось выделить память пропущен аргумент запрошены взаимно исключающие логические операции числовое значение за пределами предусмотренного opt->arg не может быть NULL обработка параметра (%u) в popt не предусмотрена
 неизвестный errno неизвестная ошибка неизвестный параметр 