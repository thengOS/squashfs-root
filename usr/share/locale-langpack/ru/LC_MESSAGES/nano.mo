��    �       W        x%     y%     �%     �%     �%     �%  (   �%     &     -&  =   H&  D   �&  	   �&     �&     �&     �&     '  	   '     '     -'     C'     W'     q'  (   �'  $   �'     �'     �'     �'     �'     �'     (     (     +(     <(     M(     \(     n(     w(  
   �(     �(     �(  
   �(  	   �(     �(     �(  �   �(     �)     �)  #   �)     �)     *     *     (*     H*     g*  &   l*  	   �*     �*     �*  	   �*     �*     �*  
   �*     �*  -  	+  �  7,     �-  )   �-  $   .     1.     K.     e.     l.  	   �.  3   �.  2   �.  ,   �.  8   &/  "   _/     �/     �/  	   �/     �/  .   �/  �   �/     �0  )   �0     �0     1  +   &1      R1     s1  "   �1     �1  	   �1  3   �1     2  $   '2     L2  !   [2  -   }2  @   �2  +   �2  0   3  -   I3     w3     3     �3  3   �3  2   �3  
   4  
   4     $4  
   )4     44  .   ;4  %   j4  &   �4  z   �4  z   25  (   �5  "   �5     �5  $   6     56  '   Q6  '   y6     �6     �6     �6     �6     7     !7     77     Q7     c7     7  
   �7     �7      �7     �7     �7     �7     8     8     38     J8     `8     8  0   �8     �8     �8      �8     9     09     L9  /  \9     �:     �:     �:     �:  @   �:  9   ;  :   O;  �  �;     ==     T=     l=     =     �=  )   �=  9   �=     >     >     &>  
   A>  
   L>  &   W>  (   ~>     �>     �>  
   �>  )   �>     �>  	   �>     ?  
   ?    ?  
   %@     0@     F@     W@     p@  7   �@     �@     �@     �@  8   A     ?A     OA     iA     |A     �A     �A     �A     �A      �A      B     /B     OB     oB     �B  #   �B  %   �B     �B     C  	   *C     4C  %   9C  G   _C  �   �C  �   ]D     E     .E     :E  �  RE  '   �F  #   'G  (   KG  "   tG  <   �G  ;   �G  8   H  5   IH     H     �H  &   �H     �H  #   I     %I     -I     KI  #   cI     �I  )   �I  	   �I  	   �I  &   �I  (   J  
   +J  �  6J     �K  	   �K  
   �K  +   	L     5L     HL     YL     pL     �L     �L     �L     �L     �L  *   �L  >   M  
   CM     NM  	   WM  	   aM     kM  	   yM  	   �M  
   �M     �M     �M  
   �M  !   �M     �M     �M  (   �M     N     /N     IN     gN     uN     �N  !   �N     �N      �N  &   �N     O     7O     SO     aO     iO  $   �O  	   �O  	   �O     �O  	   �O  	   �O  
   �O  "   �O     P     P  �   8P  d   �P  �   @Q  T   �Q  �   (R  T   �R  	   S     S  !   'S      IS  %   jS  )   �S     �S     �S     �S     �S     T  #   T  3   8T     lT     sT     �T  (   �T     �T     �T  0   �T  #   U  "   4U     WU  #   gU     �U  %   �U     �U  !   �U     �U  =   V  $   QV     vV  	   �V  %   �V  1   �V  /   �V     W  �  W     �X     �X  +   �X  '   �X     "Y  #   :Y  2   ^Y     �Y     �Y     �Y  (   �Y     �Y  �  Z     �[     �[  !   �[     \  
   \     \  "   8\     [\  !   j\  %   �\     �\     �\  ,   �\     �\     ]  �  8]     �^  ,   �^     &_  >   B_  
   �_     �_  	   �_     �_     �_  &   �_     �_  )   �_     `     9`     V`  %   u`     �`  %   �`  .   �`  
   a  .   a     Aa     Sa     Xa     pa     ~a     �a  	   �a     �a     �a  0   �a     b     "b  &   =b      db     �b     �b     �b     �b     �b  J   �b     !c     *c     7c  %   Jc  
   pc  �  {c  	   +e     5e     Me     le     �e     �e     �e     �e     �e     �e     �e     �e  <   f  
   Cf  	   Nf     Xf     of     xf     �f     �f     �f  
   �f     �f    �f  "   �h  *   �h  $   i  !   3i  8   Ui  L   �i     �i  ,   �i  M   j  �   mj     �j     k  #   k     5k     Ik     ]k     mk  B   �k  3   �k  6   l  !   8l  C   Zl  6   �l     �l     �l     �l     �l     m     .m     Gm     _m     sm     �m     �m     �m     �m     �m     �m     �m     �m     
n     "n     )n  �  0n     �o  >   �o  9   p     Fp  %   fp     �p  N   �p  H   �p  
   ;q  @   Fq     �q     �q  *   �q  
   �q  C   �q  ?   8r     xr      �r  "  �r    �t  B   �w  I   (x  ?   rx  9   �x  ?   �x     ,y  .   9y     hy  i   yy  }   �y  y   az     �z  J   [{  D   �{  0   �{     |     0|  ?   ?|    |  '   �}  R   �}  *   ~  5   ,~  R   b~  >   �~  E   �~  L   :  ?   �     �  h   �  3   E�  P   y�  B   ʀ  Q   �  o   _�  �   ρ  l   p�  R   ݂  Y   0�     ��     ��  ?   ��  L   �  d   9�      ��     ��     ބ     �     ��  :   �  3   I�  ]   }�  �   ۅ  �   ��  Y   n�  2   ȇ  &   ��  =   "�  V   `�  J   ��  ?   �  I   B�  4   ��  E   ��  '   �  n   /�  -   ��  7   ̊  +   �  6   0�  
   g�     r�     ��  ;   ��  D   ΋  (   �  4   <�  (   q�  -   ��  +   Ȍ  *   �  D   �      d�  v   ��      ��  !   �  I   ?�  @   ��  >   ʎ  !   	�  p  +�  0   ��  
   ͑     ؑ  /   �  �   �  y   ˒  \   E�  �  ��  ?   U�  =   ��  %   ӗ  :   ��  )   4�  B   ^�  g   ��  	   	�     �  <   &�     c�     x�  4   ��  E        �     �     %�  B   8�     {�     ��  &   ��     ��  �  њ     ��  %   ̜  #   �  '   �  %   >�  Y   d�  '   ��  )   �  -   �  ]   >�  "   ��  )   ��  1   �  H   �  $   d�  *   ��  &   ��  ,   ۟  ;   �  )   D�  A   n�  /   ��  0   �  A   �  C   S�  P   ��  7   �  #    �     D�     ^�  ^   k�  �   ʢ  >  T�  q  ��     �     �  ,   +�  �  X�  >   >�  D   }�  8   ©  U   ��  �   Q�  �   ڪ  �   p�  j   �  F   q�  I   ��  B   �  1   E�  F   w�     ��  ,   ѭ  $   ��  V   #�  ;   z�  \   ��     �     *�  F   ?�  V   ��     ݯ  8  �  !   '�     I�     Z�  M   p�  2   ��  6   �  Q   (�  8   z�  '   ��  ;   ۴  <   �     T�     c�  S   �  �   ӵ     {�     ��     ��     ��     Ҷ     �      �     �     (�     /�     6�  ?   L�  &   ��  (   ��  {   ܷ  2   X�  D   ��  0   и     �  2   �      F�  8   g�     ��  1   ��  _   �  F   P�  5   ��  !   ͺ     �  @   �  A   F�     ��     ��     ��     ֻ     �     ��  +   �  #   8�  i   \�  �  Ƽ    ��  �  ׿    ��  �  ��    ��     ��     ��  :   ��  V   �  J   d�  L   ��  %   ��  ,   "�     O�  K   \�     ��  *   ��  �   ��     p�  0   ��     ��  J   ��  +   �     8�  }   N�  U   ��  S   "�  #   v�  4   ��     ��  J   ��  V   -�  >   ��  1   ��  w   ��  Q   m�     ��     ��  N   ��  Y   L�  [   ��  
   �  o  �     }�     ��  F   ��  c   ��  2   \�  P   ��  _   ��     @�  !   ]�  5   �  K   ��  (   �  �  *�  A   %�  >   g�  9   ��     ��     ��  9   �  ;   L�     ��  G   ��  X   ��     F�  8   Y�  L   ��  8   ��  l   �  �  ��  (   /�  O   X�  4   ��  s   ��     Q�     c�     s�     ��      ��  =   ��  "   ��  -   �  )   J�  )   t�  .   ��  @   ��  :   �  ;   I�  _   ��     ��  ^    �  *   _�     ��  4   ��     ��     ��  ,   ��     "�  %   <�  (   b�  \   ��  >   ��  I   '�  N   q�  h   ��  h   )�     ��     ��     ��  9   ��  �   �  
   ��     ��  '   ��  ;   ��     �  �  1�     �  1   �  6   D�  c   {�  1   ��  0   �     B�     G�  �   N�     ��  %   ��      �  N   1�     ��  !   ��  2   ��  !   ��     �     +�     I�     e�  (   �     ��     �       Y  �         <  �         v   J   S  �      �  �          �     �  �   �  �  �         g  g   �   G  X   �   �  �       !  >   -   t   �                      l       �   w  �           �      |      #      �   &  �   �   V  �   �       �   �   �           `            U  )   B  �       �   �   �  f    �   )  �      �      �       �  �   �     �  
  �   k  �   �  �  �   
       D   P   �   �      �    y   �  �   '       �   �   k         L  I          <   s               �               �   *   J  �      �   +  m  x   �  ?  H  (   3       �  �   r                   �   H        �   �  	   �   W  �   n     I   _   �  �   /          4  &      w   2   ^  \  A      .       �   �  A           �   �       K  �           �      �   �   �  s  �  M              Z  �           d  ]  �  O      z   O   �       �   �       �   h       �   �       :   �         �          �  �   �     i             �   o          �     �       �    �   �   �      �   �   #   �  �       N   9       �   �   �       �   �   y  p   $  �              �  i   �      v              �   Q  �       �  �   �   j      '  G   �   �   �  >  n   a      "       ^      0       %   5   �   �  u         ,      �   z                    �       �             L      0  7   �           �         6   =  R   X  �  3  �   �      .  �                   �   C   N  [   F  }   "  �       �      m   c  �      �  �   �       �  �   Y   �  u      �   d   �   b       x     q  5  -  f   �               p      �               ;  �  2            4   �       j   �      T     �   @       �   P     8       {  {   �   �   �   }  V   �   @  Z   ]      T  �   \   Q   �   �           �  �       *      1  �       �      �   �   �     W   �      7  �   ?   �   �  !   o   9     R          �  �   E  c   �   S       8  e  F         B       �   r    ~  1   ,   =   q   e                 �  �   �  E      ;   �    �   �   D     t      :  �           �  6  �  K           +       �      �   �       U   _      M    h  	  �           �  (  �       b  $           %  �  �  a   |   l  �           `          �       �         C      /   �      �         �  �         �          ~     [   
 Compiled options: 
Buffer not written to %s: %s
 
Buffer not written: %s
 
Buffer written to %s
 
Press Enter to continue
 
Press Enter to continue starting nano.
  (to replace)  (to replace) in selection  Email: nano@nano-editor.org	Web: http://www.nano-editor.org/  The following function keys are available in Browser Search mode:

  [Backup]  [Backwards]  [Case Sensitive]  [DOS Format]  [Mac Format]  [Regexp] "%.*s%s" not found "%s" is a device file "%s" is a directory "%s" is not a normal file "%s" not found "start=" requires a corresponding "end=" %sWords: %lu  Lines: %ld  Chars: %lu (dir) (more) (parent dir) +LINE,COLUMN --backupdir=<dir> --fill=<#cols> --operatingdir=<dir> --quotestr=<str> --speller=<prog> --syntax=<str> --tabsize=<#cols> -C <dir> -Q <str> -T <#cols> -Y <str> -o <dir> -r <#cols> -s <prog> Aa All Also, pressing Esc twice and then typing a three-digit decimal number from 000 to 255 will enter the character with the corresponding value.  The following keystrokes are available in the main editor window.  Alternative keys are shown in parentheses:

 Append Append Selection to File Argument '%s' has an unterminated " At first message At last message Auto indent Auto save on exit, don't prompt Automatically indent new lines Back Background color "%s" cannot be bright Backspace Backup File Backup files Backwards Bad quote string %s: %s Bad regex "%s": %s Beg of Par Brought to you by: Browser Go To Directory Help Text

 Enter the name of the directory you would like to browse to.

 If tab completion has not been disabled, you can use the Tab key to (attempt to) automatically complete the directory name.

 The following function keys are available in Browser Go To Directory mode:

 Browser Search Command Help Text

 Enter the words or characters you would like to search for, and then press Enter.  If there is a match for the text you entered, the screen will be updated to the location of the nearest match for the search string.

 The previous search string will be shown in brackets after the search prompt.  Hitting Enter without entering any text will perform the previous search.

 Can now UnJustify! Can't go outside of %s in restricted mode Can't insert file from outside of %s Can't move up a directory Can't write outside of %s Cancel Cancel the current function Cancelled Cannot add a color command without a syntax command Cannot add a header regex without a syntax command Cannot add a linter without a syntax command Cannot add a magic string regex without a syntax command Cannot map name "%s" to a function Cannot map name "%s" to a menu Cannot unset option "%s" Case Sens Close Close the current file buffer / Exit from nano Color "%s" not understood.
Valid colors are "green", "red", "blue",
"white", "yellow", "cyan", "magenta" and
"black", with the optional prefix "bright"
for foreground colors. Color syntax highlighting Command "%s" not allowed in included file Command "%s" not understood Command to execute [from %s]  Command to execute in new buffer [from %s]  Constant cursor position display Constantly show cursor position Conversion of typed tabs to spaces Convert typed tabs to spaces Copy Text Copy the current line and store it in the cutbuffer Could not create pipe Could not find syntax "%s" to extend Could not fork Could not get size of pipe buffer Couldn't determine hostname for lock file: %s Couldn't determine my identity for lock file (getpwuid() failed) Couldn't reopen stdin from keyboard, sorry
 Count the number of words, lines, and characters Creating misspelled word list, please wait... Cur Pos Cut Text Cut from cursor to end of line Cut from the cursor position to the end of the file Cut the current line and store it in the cutbuffer Cut to end CutTillEnd DIR: DOS Format Delete Delete the character to the left of the cursor Delete the character under the cursor Detect word boundaries more accurately Detected a legacy nano history file (%s) which I moved
to the preferred location (%s)
(see the nano FAQ about this change) Detected a legacy nano history file (%s) which I tried to move
to the preferred location (%s) but encountered an error: %s Directory for saving unique backup files Display the position of the cursor Display this help text Do not read the file (only write it) Do quick statusbar blanking Don't add newlines to the ends of files Don't convert files from DOS/Mac format Don't hard-wrap long lines Don't look at nanorc files Don't show the two help lines Edit a replacement Enable alternate speller Enable smart home key Enable soft line wrapping Enable suspension Enable the use of the mouse End End of Par Enter Enter line number, column number Error deleting lock file %s: %s Error expanding %s: %s Error in %s on line %lu:  Error invoking "%s" Error invoking "sort -f" Error invoking "spell" Error invoking "uniq" Error opening lock file %s: %s Error reading %s: %s Error reading lock file %s: Not enough data read Error writing %s: %s Error writing %s: %s
 Error writing backup file %s: %s Error writing lock file %s: %s Error writing temp file: %s Execute Command Execute Command Help Text

 This mode allows you to insert the output of a command run by the shell into the current buffer (or a new buffer in multiple file buffer mode).  If you need another blank buffer, do not enter any command.

 The following function keys are available in Execute Command mode:

 Execute external command Exit Exit from nano Exit from the file browser Failed to write backup file, continue saving? (Say N if unsure)  Fatal error: no keys mapped for function "%s".  Exiting.
 File %s is being edited (by %s with %s, PID %d); continue? File Browser Help Text

 The file browser is used to visually browse the directory structure to select a file for reading or writing.  You may use the arrow keys or Page Up/Down to browse through the files, and S or Enter to choose the selected file or enter the selected directory.  To move up one level, select the directory called ".." at the top of the file list.

 The following function keys are available in the file browser:

 File Name to Append to File Name to Prepend to File Name to Write File exists, OVERWRITE ?  File to insert [from %s]  File to insert into new buffer [from %s]  File was modified since you opened it, continue saving ?  File: Finished Finished checking spelling First File First Line Fix Backspace/Delete confusion problem Fix numeric keypad key confusion problem For ncurses: Forward FullJstify Function '%s' does not exist in menu '%s' Get Help Go To Dir Go To Directory Go To Line Go To Line Help Text

 Enter the line number that you wish to go to and hit Enter.  If there are fewer lines of text than the number you entered, you will be brought to the last line of the file.

 The following function keys are available in Go To Line mode:

 Go To Text Go back one character Go back one word Go forward one character Go forward one word Go just beyond end of paragraph; then of next paragraph Go one screenful down Go one screenful up Go to beginning of current line Go to beginning of paragraph; then of previous paragraph Go to directory Go to end of current line Go to file browser Go to line and column number Go to next line Go to next linter msg Go to previous line Go to previous linter msg Go to the first file in the list Go to the first line of the file Go to the last file in the list Go to the last line of the file Go to the matching bracket Go to the next file in the list Go to the previous file in the list Got 0 parsable lines from command: %s Hard wrapping of overlong lines Help is not available Help mode Home I can't find my home directory!  Wah! If needed, use nano with the -I option to adjust your nanorc settings.
 If you have selected text with the mark and then search to replace, only matches in the selected text will be replaced.

 The following function keys are available in Search mode:

 If you need another blank buffer, do not enter any filename, or type in a nonexistent filename at the prompt and press Enter.

 The following function keys are available in Insert File mode:

 In Selection:   Indent Text Indent the current line Insert File Help Text

 Type in the name of a file to be inserted into the current file buffer at the current cursor location.

 If you have compiled nano with multiple file buffer support, and enable multiple file buffers with the -F or --multibuffer command line flags, the Meta-F toggle, or a nanorc file, inserting a file will cause it to be loaded into a separate buffer (use Meta-< and > to switch between file buffers).   Insert a newline at the cursor position Insert a tab at the cursor position Insert another file into the current one Insert the next keystroke verbatim Internal error: can't match line %d.  Please save your work. Internal error: cannot set up redo.  Please save your work. Internal error: line is missing.  Please save your work. Internal error: unknown type.  Please save your work. Invalid line or column number Invoke the linter, if available Invoke the spell checker, if available Invoking linter, please wait Invoking spell checker, please wait Justify Justify the current paragraph Justify the entire file Key invalid in non-multibuffer mode Key name is too short Key name must begin with "^", "M", or "F" Last File Last Line Log & read location of cursor position Log & read search/replace string history Mac Format Main nano help text

 The nano editor is designed to emulate the functionality and ease-of-use of the UW Pico text editor.  There are four main sections of the editor.  The top line shows the program version, the current filename being edited, and whether or not the file has been modified.  Next is the main editor window showing the file being edited.  The status line is the third line from the bottom and shows important messages.   Mark Set Mark Text Mark Unset Mark text starting from the cursor position Missing color name Missing key name Missing linter command Missing magic string name Missing option Missing regex string Missing syntax name Modified Mouse support Must specify a function to bind the key to Must specify a menu (or "all") in which to bind/unbind the key New Buffer New File Next File Next Line Next Lint Msg Next Page Next Word NextHstory Nn No No Replace No conversion from DOS/Mac format No current search pattern No file name No linter defined for this type of file! No matching bracket No more open file buffers Non-blank characters required Not a bracket Nothing in undo buffer! Nothing to re-do! Option		GNU long option		Meaning
 Option		Meaning
 Option "%s" requires an argument Option is not a valid multibyte string Path '%s' is not a directory Path '%s' is not accessible Path '%s': %s Prepend Prepend Selection to File Preserve XON (^Q) and XOFF (^S) keys Prev File Prev Line Prev Lint Msg Prev Page Prev Word PrevHstory Print version information and exit Quoting string Read %lu line Read %lu lines Read %lu line (Converted from DOS and Mac format - Warning: No write permission) Read %lu lines (Converted from DOS and Mac format - Warning: No write permission) Read %lu line (Converted from DOS and Mac format) Read %lu lines (Converted from DOS and Mac format) Read %lu line (Converted from DOS format - Warning: No write permission) Read %lu lines (Converted from DOS format - Warning: No write permission) Read %lu line (Converted from DOS format) Read %lu lines (Converted from DOS format) Read %lu line (Converted from Mac format - Warning: No write permission) Read %lu lines (Converted from Mac format - Warning: No write permission) Read %lu line (Converted from Mac format) Read %lu lines (Converted from Mac format) Read File Reading File Reading file into separate buffer Reading from stdin, ^C to abort
 Recall the next search/replace string Recall the previous search/replace string Received SIGHUP or SIGTERM
 Redid action (%s) Redo Redo the last undone operation Refresh Refresh (redraw) the current screen Regex strings must begin and end with a " character Regexp Repeat the last search Replace Replace a string or a regular expression Replace this instance? Replace with Replaced %lu occurrence Replaced %lu occurrences Requested fill size "%s" is invalid Requested tab size "%s" is invalid Restricted mode Reverse the direction of the search Save Save a file by default in Unix format Save backups of existing files Save file under DIFFERENT NAME ?  Save file without prompting Save modified buffer (ANSWERING "No" WILL DESTROY CHANGES) ?  Save modified buffer before linting? Scroll Down Scroll Up Scroll by line instead of half-screen Scroll down one line without scrolling the cursor Scroll up one line without scrolling the cursor Search Search Command Help Text

 Enter the words or characters you would like to search for, and then press Enter.  If there is a match for the text you entered, the screen will be updated to the location of the nearest match for the search string.

 The previous search string will be shown in brackets after the search prompt.  Hitting Enter without entering any text will perform the previous search.   Search Wrapped Search for a string Search for a string or a regular expression Set hard-wrapping point at column #cols Set operating directory Set width of a tab to #cols columns Silently ignore startup issues like rc file errors Smart home key Smooth scrolling Soft wrapping of overlong lines Sorry, keystroke "%s" may not be rebound Special thanks to: Spell Check Help Text

 The spell checker checks the spelling of all text in the current file.  When an unknown word is encountered, it is highlighted and a replacement can be edited.  It will then prompt to replace every instance of the given misspelled word in the current file, or, if you have selected text with the mark, in the selected text.

 The following function keys are available in Spell Check mode:

 Spell checking failed: %s Spell checking failed: %s: %s Start at line LINE, column COLUMN Suspend Suspension Switch to the next file buffer Switch to the previous file buffer Switched to %s Syntax "%s" has no color commands Syntax definition to use for coloring Tab Thank you for using nano! The "default" syntax must take no extensions The "none" syntax is reserved The Free Software Foundation The bottom two lines show the most commonly used shortcuts in the editor.

 The notation for shortcuts is as follows: Control-key sequences are notated with a caret (^) symbol and can be entered either by using the Control (Ctrl) key or pressing the Escape (Esc) key twice.  Escape-key sequences are notated with the Meta (M-) symbol and can be entered using either the Esc, Alt, or Meta key depending on your keyboard setup.   The nano text editor This function is disabled in restricted mode This is the only occurrence This message is for unopened file %s, open it in a new buffer? To Bracket To Files To Linter To Spell Toggle appending Toggle backing up of the original file Toggle prepending Toggle the case sensitivity of the search Toggle the use of DOS format Toggle the use of Mac format Toggle the use of a new buffer Toggle the use of regular expressions Too many backup files? Two single-column characters required Type '%s -h' for a list of available options.
 Uncut Text Uncut from the cutbuffer into the current line Undid action (%s) Undo Undo the last operation Unicode Input Unindent Text Unindent the current line Unjustify Unknown Command Unknown option "%s" Usage: nano [OPTIONS] [[+LINE,COLUMN] FILE]...

 Use "fg" to return to nano.
 Use (vim-style) lock files Use bold instead of reverse video text Use of one more line for editing Use one more line for editing Verbatim Verbatim Input View View mode (read-only) Warning: Modifying a file which is not locked, check directory permission? Where Is WhereIs Next Whitespace display Window size is too small for nano...
 Word Count Write File Help Text

 Type the name that you wish to save the current file as and press Enter to save the file.

 If you have selected text with the mark, you will be prompted to save only the selected portion to a separate file.  To reduce the chance of overwriting the current file with just a portion of it, the current filename is not the default in this mode.

 The following function keys are available in Write File mode:

 Write Out Write Selection to File Write the current file to disk Wrote %lu line Wrote %lu lines XOFF ignored, mumble mumble XON ignored, mumble mumble Yes Yy and anyone else we forgot... disabled enable/disable enabled line %ld/%ld (%d%%), col %lu/%lu (%d%%), char %lu/%lu (%d%%) line break line join nano is out of memory! text add text cut text delete text insert text replace text uncut version Project-Id-Version: nano 2.3.3pre2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-25 16:41-0500
PO-Revision-Date: 2016-05-21 13:13+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: Russian <gnu@mx.ru>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 17:13+0000
X-Generator: Launchpad (build 18115)
Language: ru
 
 Параметры сборки: 
Буфер не записан в %s: %s
 
Буфер не записан: %s
 
Буфер записан в %s
 
Нажмите ВВОД для продолжения.
 
Нажмите ВВОД для продолжения запуска nano.
  (что менять)  (что менять) в выделеном  Эл. почта: nano@nano-editor.org	Сайт: http://www.nano-editor.org/  Следующие функциональные клавиши доступны в режиме Поиск просмотрщика:

  [Копия]  [Назад]  [С учётом регистра]  [формат DOS]  [формат Mac]  [РегВыр] "%.*s %s " не найден Файл «%s» является файлом устройства Файл «%s» является каталогом "%s" не является обычным файлом Файл «%s» не найден «start=» требует соответствующего «end=» %s Слов: %lu Строк: %ld Символов: %lu (каталог) (ещё) (вверх) +СТРОКА,СТОЛБЕЦ --backupdir=<дир> --fill=<#столбцы> --operatingdir=<дир> --quotestr=<стр> --speller=<программа> --syntax=<стр> --tabsize=<#чис> -C <дир> -Q <стр> -T <#чис> -Y <стр> -o <дир> -r <#столбцы> -s <программа> AaВв Все Также, нажатие Esc дважды и дальнейший ввод трёхзначного числа от 000 до 255 введёт соответствующий символ.
Следущие комбинации доступны в главном окне редактирования. Альтернативные комбинации показаны в скобках:

 Доп. в начало Добавить выделенное в конец файла Аргумент «%s» имеет незакрытую " Первое сообщение Последнее сообщение Автоотступы Автозапись при выходе, без лишних вопросов Автоматический отступ на новых строках Назад Цвет фона «%s» не может быть светлым Возврат каретки Резерв. копия Делать резервные копии Назад Недопустимая строка цитирования %s: %s Плохое регулярное выражение «%s»: %s Нач. Пар-фа Предоставлен вам: Справка по режиму перехода к катологам

 Введите имя каталога, к которому вы хотите перейти.

 Если tab-дополнение не было отключено, Вы можете использовать клавишу TAB для [попытки] автоматического дополнения имени директории.

 Следующие функциональные клавиши доступны в режиме перехода к каталогу:

 Текст помощи команды поиска

 Введите слова или символы, которые вы собираетесь искать, затем нажмите Ввод. Если для введеного Вами найдется совпадение, экран переместится в положение поблизости от найденного совпадения.

 Предыдущая строка поиска будет показана в скобках после приглашения 'Поиск:'. Нажатие Ввод без редактирования текста продолжит предыдущий поиск.

 Следующие функциональные клавиши доступны в режиме поиска:

 Сейчас можно отменить выравнивание! Не могу выйти за %s в ограниченном режиме Не удаётся вставить файл снаружи %s Не удаётся переместить каталог Не удаётся записать за пределами %s Отмена Отменить текущую функцию Отменено Не удаётся добавить команду цвета без команды синтаксиса Не удаётся добавить рег. выражение заголовка без команды синтаксиса Не удаётся добавить анализатор синтаксиса без команды синтаксиса Не удаётся добавить магическое рег. выражение без команды синтаксиса Не удалось сопоставить имя «%s» к функции Не удалось сопоставить имя «%s» к меню Не удалось снять опцию «%s» Уч.регистр Закрыть Закрыть текущий буфер / Выйти из nano Цвет «%s» не совсем понятен.
Разрешены цвета - «green», «red», «blue», 
«white», «yellow», «cyan», «magenta» и 
«black», с необязательным префиксом «bright»
для цвета фона. Подсветка синтаксиса Команда «%s» не разрешена во включенном файле Непонятная команда «%s» Команда для выполнения [из %s]  Комманда для выполнения в новом буфере [из %s]  Постоянное отображение положения Постоянно показывать позицию курсора Преобразование ввода табуляций в пробелы Преобразовать табуляции в пробелы Копировать Копировать текущую строку и сохранить ее в буфере обмена Не удалось создать конвейер Не удалось сопоставить имя «%s» к расширению Не удалось создать дочерний процесс Не удалось получить размер буфера конвейера Не удалось определить имя компьютера для файла блокировки: %s Не удалось получить мой идентификатор пользователя для файла блокировки (ошибка getpwuid()) Не удалось повторно открыть стандартный ввод с клавиатуры
 Подсчитать количество слов, строк и символов Идёт создание списка ошибочных слов. Подождите... ТекПозиц Вырезать Вырезаь от курсора до конца строки Вырезать с текущей позиции до конца файла Вырезать текущую строку и сохранить её в буфере обмена Вырезать до конца ВырезатьДоКонца Каталог: Формат DOS Удалить Удалить символ слева от курсора Удалить символ под курсором Использовать более точное определение границ слов Обнаружен старый файл истории nano (%s), который я переместил
на новое место (%s)
(смотрите в nano FAQ об этом изменении) Обнаружен старый файл истории nano (%s), который я попытался переместить
на новое место (%s), но произошла ошибка: %s Каталог для хранения уникальных резервных копий Показать положение курсора Показать эту справку Не читать файл (только писать его) Использовать быструю очистку строки состояния Не добавлять пустые строки в конце файла Не преобразовывать из DOS/Mac формата Не делать жесткий перенос длинных строк Не использовать на файлы nanorc Не показывать две строки помощи внизу Редактировать замену Использовать альтернативную программу проверки орфографии Включить умную кнопку home Включить мягкий перенос строк Разрешить приостановку Разрешить использование мыши Конец Конец Пар-фа Ввод Введите номер строки, номер ряда Ошибка удаления файла блокировки %s: %s Ошибка расширения %s: %s Ошибка в позиции %s строки %lu:  Ошибка выполнения «%s» Ошибка выполнения «sort -f» Ошибка выполнения «spell» Ошибка выполнения «uniq» Ошибка открытия файла блокировки %s: %s Ошибка чтения %s: %s Ошибка чтения файла блокировки %s: Недостаточно данных прочитано Ошибка записи %s: %s Ошибка записи %s: %s
 Ошибка записи файла резервной копии %s: %s Ошибка записи файла блокировки %s: %s Ошибка записи во временный файл: %s Выполнить команду Текст помощи режима Внешняя Команда

 Это меню позволяет вам вставить вывод команды, выполненной в командном интерпретаторе, в текущий файловый буфер (или в новый буфер в мультибуферном режиме).  Если вам необходим другой пустой буфер, не вводите никакой команды вообще.

 Следующие функциональные клавиши доступны в режиме Внешняя Команда:

 Выполнить внешнюю команду Выход Выход из nano Выйти из просмотра файлов Не удалось записать файл резервной копии, продолжить сохранение? (Ответьте Н - если не уверены)  Критическая ошибка: для функции «%s» не назначены клавиши.  Выхожу.
 В файл %s вносятся изменения (%s с %s, PID %d); продолжить? Текст помощи просмотрщика файлов

 Просмотрщик файлов используется для визуального просмотра содержимого каталога, для выбора файла из этой каталога для операций ввода-вывода. Пользуйтесь клавишами со стрелками или PageUp/PageDown для перехода по содержимому каталога, и клавишами S или ВВОД для выбора нужного файла или входа в выбранный каталог. Для перемещения вверх на одну директорию, выберите директорию, названную «..» в самом верху списка файлов.

 Следующие функциональные клавиши доступны в просмотрщике файлов:

 Имя файла для добавления (в начало) Имя файла для добавления (в конец) Имя файла для записи Файл существует, ПЕРЕЗАПИСАТЬ ?  Файл для вставки [от %s]  Файл для вставки в новый буфер [от %s]  Файл был изменён после открытия. Продолжить сохранение?  Файл: Завершено Проверка правописания завершена ПервыйФайл ПервСтрока Исправить проблему Backspace/Delete Исправлять проблему малой клавиатуры Для ncurses: Вперёд Выровнять Функция '%s' не представлена в меню '%s' Помощь Перейти к Перейти к директории К строке Текст помощи режима Перейти к строке

 Введите номер строки к которой вы желаете перейти и нажмите Ввод. Если в файле число строк меньше чем введённое число, то окажетесь на последней строке файла.

 Следующие функциональные клавиши доступны в режиме Перейти к строке:

 К строке Назад на один символ Назад на одно слово Вперёд на один символ Вперёд на одно слово В конец текущего абзаца; потом следующего абзаца Перейти на экран вниз Перейти на экран вверх На начало текущей строки На начало текущего абзаца; потом следующего абзаца Перейти к каталогу В конец текущей строки Перейти в файловый браузер Перейти на указанный номер строки и ряд На следующую строку На следующее сообщение На предыдущую строку На предыдущее сообщение Перейти к первому файлу в списке На первую строку файла Перейти к последнему файлу в списке На последнюю строку файла На соответствующую скобку Перейти к следующему файлу в списке Перейти к предыдущему файлу в списке Получено 0 распознанных строк от комманды: %s Жесткий перенос длинных строк Справка недоступна Режим справки Начало Не могу найти собственную домашнюю директорию! Йой! Используйте nano с опцией -I, если требуется подстроить параметры вашего nanorc.
 Если Вы выделили текст и выполнили поиск с заменой, то только совпадения в выделенном фрагменте будут заменены.

 Следующие функциональные клавиши доступны в режиме поиска:

 Если вам нужен еще один пустой буфер, просто не вводите имя файла или наберите несуществующее имя в строке приглашения и нажмите Enter.

 Следующие функциональные клавиши доступны в режиме вставки файла:

 В выделеном:   Отступ Увеличить отступ строки Справка по режиму вставки файла

 Наберите имя файла для вставки в текущий файловый буфер в текущую позицию курсора.

 Если Ваш nano собран с поддержкой нескольких файловых буферов, и это свойство включено опциями -F или --multibuffer, комбинацией клавиш Meta-F или при помощи файла nanorc, то вставка файла приведет к загрузке этого файла в отдельный буфер (используйте Meta-< и > для переключения между файловыми буферами).   Вставить строку в позиции курсора Вставить табуляцию в позиции курсора Вставить другой файл в текущий Вставить следующую комбинацию клавиш как есть Внутренняя ошибка: не удаётся сопоставить строку %d. Сохраните свою работу. Внутренняя ошибка: ошибка настройки повторения изменения. Сохраните свою работу. Внутренняя ошибка: отсутствует строка. Пожалуйста, сохраните проделанную работу. Внутренняя ошибка: неизвестный тип. Сохраните свою работу Неправильный номер строки или столбца Проверить синтаксис кода, если доступно Проверить орфографию, если доступно Запуск проверки, подождите Запуск проверки орфографии, подождите Выровнять Выровнять текущий абзац Выровнять весь файл Недопустимая клавиша в немногобуферном режиме Название ключа слишком короткое Привязки клавиш должны начинаться с «^», «M» или «F» ПоследнФайл ПослСтрока Сохранять и загружать позицию курсора Сохранять и читать историю поиска/замены строк Формат Mac Текст помощи nano

 Редактор nano разработан для эмуляции функциональности и простоты использования оригинального редактора UW Pico. Редактор разбит на 4 основные части: верхняя строка содержит версию программы, текущее имя файла, который редактируется, и были ли внесены изменения в текущий файл. Вторая часть - это главное окно редактирования, в котором отображен редактируемый файл. Строка состояния - 3 строка снизу - показывает разные важные сообщения.   Метка установлена Отметить Метка снята Отметить текст от текущей позиции курсора Отсутствует название цвета Отсутствует название клавиши Отсутствует команда анализатора синтаксиса Отсутствует магическая строка Параметр отсутствует Не указано регулярное выражение Отсутствует название синтаксиса Изменён Поддержка мыши Укажите функцию, которой назначается клавиша Вы должны указать имя меню (или «все») на которое нужно назначить/убрать назначение клавиши Новый буфер Новый файл Следующий файл След Стр. След. сообщ. пров. СледCтр След. слово СледИстор NnНн Нет Не заменять Без преобразования из формата DOS/Mac Нет активного поиска Отсутствует имя файла Для данного типа файла не настроена программа проверки синтаксиса! Нет соответствующей скобки Нет больше открытых файловых буферов Требуется непустой символ Не скобка Буфер отмены действий пуст! Нечего повторять! Опция		Длинная форма		Значение
 Опция		Значение
 Опция «%s» требует аргумент Опция не является допустимой многобайтовой строкой Расположение '%s' не является каталогом Расположение «%s»  недоступно Расположение '%s': %s Доп. в конец Добавить выделенное в начало файла Зарезервировать кнопки XON (^Q) и XOFF (^S) Предыдущий файл Пред Cтр. Пред. сообщ. пров. ПредCтр Пред. слово ПредИстор Показать версию и выйти Строка цитирования Прочитана %lu строка Прочитано %lu строки Прочитано %lu строк Прочитана %lu строка (преобразовано из формата Mac и DOS; предупреждение: нет доступа на запись) Прочитано %lu строки (преобразовано из формата Mac и DOS; предупреждение: нет доступа на запись) Прочитано %lu строк (преобразовано из формата Mac и DOS; предупреждение: нет доступа на запись) Прочитана %lu строка (преобразовано из формата Mac и DOS) Прочитано %lu строки (преобразовано из формата Mac и DOS) Прочитано %lu строк (преобразовано из формата Mac и DOS) Прочитана %lu строка (преобразовано из формата DOS; предупреждение: нет доступа на запись) Прочитано %lu строки (преобразовано из формата DOS; предупреждение: нет доступа на запись) Прочитано %lu строк (преобразовано из формата DOS; предупреждение: нет доступа на запись) Прочитана %lu строка (преобразовано из формата DOS) Прочитано %lu строки (преобразовано из формата DOS) Прочитано %lu строк (преобразовано из формата DOS) Прочитана %lu строка (преобразовано из формата Mac; предупреждение: нет доступа на запись) Прочитано %lu строки (преобразовано из формата Mac; предупреждение: нет доступа на запись) Прочитано %lu строк (преобразовано из формата Mac; предупреждение: нет доступа на запись) Прочитана %lu строка (конвертирована из формата Mac) Прочитано %lu строки (конвертировано из формата Mac) Прочитано %lu строк (конвертировано из формата Mac) ЧитФайл Чтение файла Чтение файла в отдельном буфере Чтение из стандартного ввода. ^C для прерывания
 Сбросить следующую строку поиска/замены Сбросить предыдущую строку поиска/замены Получен SIGHUP или SIGTERM
 Повторённое действие (%s) Повтор Повторить последнее отменённое действие Обновить Обновить текущий экран Строки регулярных выражений должны начинаться и заканчиваться кавычками (") Рег.выраж. Повторить последний поиск Замена Заменить текст или регулярное выражение Заменить это вхождение? Заменить на Заменено %lu совпадение Заменено %lu совпадения Заменено %lu совпадений Запрошенный размер заполнения «%s» не подходит Запрошенный размер табуляции «%s» не подходит Ограниченный режим Изменить направление поиска Сохранить Сохранить файл в формате Unix по умолчанию Сохранять резервные копии существующих файлов Сохранить файл под другим именем?  Сохранить файл без запроса Сохранить изменённый буфер? (ИНАЧЕ ВСЕ ИЗМЕНЕНИЯ БУДУТ ПОТЕРЯНЫ)  Сохранить измененный буфер перед проверкой? Прокрутить вниз Прокрутить вверх Построчная прокрутка вместо полу-экранной Прокрутить одну строку вниз, не перемещая курсор Прокрутить одну строку вверх, не перемещая курсор Поиск Справка по командам поиска

 Введите слова или символы, которые Вы собираетесь искать, затем нажмите Enter. Если для введеного Вами найдется совпадение, экран переместится к ближайшему совпадению.

 Предыдущая строка поиска будет показана в скобках после приглашения 'Поиск:'. Нажатие Enter без редактирования текста продолжит предыдущий поиск.   Поиск завёрнут Искать текст Искать текст или регулярное выражение Установить точку жесткого переноса строки на #столбцы Установить рабочий каталог Установить ширину табуляции в #чис столбцов Молча игнорировать ошибки запуска, например rc-файла Умная кнопка home Плавная прокрутка Мягкий перенос длинных строк Привязка «%s» не может быть переназначена Особая благодарность: Текст помощи проверки правописания

 Программа проверки правописания проверяет орфографию всего текста текущего файла. Если найдено неизвестное слово, оно подсвечивается и появляется редактируемая замена этому слову. Затем будет появляться приглашение для замены каждого вхождения данного ошибочно написанного слова в текущем файле.

 Следующие функциональные клавиши доступны в режиме проверки правописания:

 Проверка правописания не удалась: %s Ошибка проверки правописания: %s: %s Начать с указаной строки и ряда Приостановка Приостановка Переключить на следующий буфер Переключить на предыдущий буфер Переключено в %s Синтаксис «%s» не имеет цветовых команд Использовать описание синтаксиса для подсветки Табуляция Благодарим за использвание nano! Синтаксис "default" не может иметь расширений Синтаксис «none» зарезервирован The Free Software Foundation (фонд свободного программного обеспечения) Две строки внизу показывают наиболее часто используемые комбинации клавиш.

 Система обозначений комбинаций клавиш следующая: Комбинации с Control обозначены символом (^) и вводятся при помощи нажатой кнопки (Ctrl) или двойном нажатии Escape (Esc); Комбинации с Esc обозначены символом Meta (M) и могут быть введены при помощи кнопок Esc, Alt или Meta, в зависимости от используемой клавиатуры.   Текстовый редактор nano Эта функция отключена в режиме ограничения Это единственное совпадение Это сообщение о не открытом файле %s, открыть его в новом буфере? На скобку К файлам Пров. синтак. Словарь Добавлять в конец Делать резервные копии оригинала Добавлять в начало Искать с учётом регистра Использовать формат DOS Использовать формат Mac Использовать новый буфер Использовать регулярные выражения Слишком много резервных файлов? Требуется два одинарных символа Введите «%s -h» для получения списка доступных опций.
 Отмен. вырезку Вставить содержимое буфера обмена в текущую строку Отменённое действие (%s) Отмена Отменить последнее действие Юникод код ОтмОтступа Уменьшить отступ строки Отмен. растяж. Неизвестная команда Неизвестная опция «%s» Использование: nano [ОПЦИИ] [[+СТРОКА,СТОЛБЕЦ] ФАЙЛ]...

 Используйте «fg» для возврата в nano
 Использовать файлы блокировки (vim-стиль) Использовать жирный шрифт вместо обычного Использование дополнительной строки для редактирования Использование дополнительной строки для редактирования Подробный Буквальный ввод Вид Режим просмотра (только чтение) Предупреждение: Изменяя не заблокированный файл, проверять доступ к каталогу? Поиск Найти далее Отображение пробелов Размер окна слишком мал для Nano...
 Счётчик слов Справка по режиму записи файла

 Наберите имя под которым Вы хотите сохранить текущий файл и нажмите Enter.

 Если Вы отметили текст при помощи Ctrl-^, Вам предложат записать только выделенную часть в отдельный файл. Чтобы понизить шансы перезаписи текущего файла частью этого же файла, текущее имя файла не будет именем по умолчанию в этом режиме.

 Следующие клавиши доступны в режиме записи файла:

 Записать Записать выделенное в файл Записать текущий файл на диск Записано %lu строка Записано %lu строки Записано %lu строк XOFF проигнорирован, мр-бр-бр XON проигнорирован, мр-бр-бр Да YyДд Дмитрию Рязанцеву за помощь в переводе и всем остальным, кого мы забыли упомянуть... отключено разрешить/запретить включено строка %ld/%ld(%d%%), ряд %lu/%lu (%d%%), символ %lu/%lu (%d%%) новая строка объединение строк Недостаточно памяти для nano! добавление текста обрезка текста удаление текста вставка текста замена текста отмена обрезки текста версия 