��    Z      �     �      �     �     �     �  �  �  �   �	  �  s
     2      ?     `     g     m     r          �     �  2   �     �     �            
   *     5  
   A     L  -   f  %   �     �  I   �  %         F  .   N  #   }     �     �  (   �     �  �        �      �    �     �                #  �   /            	   7     A     M     a     w     �     �    �     �     �     �  #   �       O   &      v  (   �  "   �     �  :   �     %     *  
   /  H   :  4   �  ,   �     �  #      s  $     �     �     �     �     �  ,   �  %   (  v  N  �   �    X     o     t     �  
  �  �  �'  (   S)  *   |)  &   �)  _  �)  \  .,  �  �-     @0  6   U0     �0     �0     �0     �0  #   �0  5   �0     1  ?   -1     m1  5   �1  "   �1     �1  !   �1     2  
   )2  6   42  r   k2  X   �2  -   73  �   e3  J   �3     :4  v   I4  =   �4  E   �4     D5  E   d5  A   �5    �5  '   �6  8   7  �  P7     ;     #;  %   7;     ];  �  i;  4   �<  2   1=  	   d=     n=  +   z=  /   �=  (   �=  %   �=  7   %>  �  ]>     �?      �?      @  C   ,@     p@  }   �@  =   A  `   CA  T   �A     �A  f   B     mB     rB  
   wB  ~   �B  �   C  y   �C  /   �C  :   .D  J  iD  ,   �F  :   �F  $   G  $   AG     fG  Z   kG  5   �G  &  �G  �   #J  =  �J     8R  ,   FR  ,   sR  �  �R     R   <   G      +                  /       $   Q          0   (   @           &                        9            V      %   *       	          ;   P   !   M          :   
   H   3       F   I   1      7   K   8   Z   ?              =           L   2   5       X   C   A      S       #   >           J         B                            W              .       )   -                                     '       ,      6   T   D   U   E   4   N      Y   "   O                   Disk usage:   Memory usage:   Network usage: ### This is a yaml representation of the configuration.
### Any line starting with a '# will be ignored.
###
### A sample configuration looks like:
### name: container1
### profiles:
### - default
### config:
###   volatile.eth0.hwaddr: 00:16:3e:e9:f8:7f
### devices:
###   homedir:
###     path: /extra
###     source: /home/user
###     type: disk
### ephemeral: false
###
### Note that the name is shown but cannot be changed ### This is a yaml representation of the image properties.
### Any line starting with a '# will be ignored.
###
### Each property is represented by a single line:
### An example would be:
###  description: My custom image ### This is a yaml representation of the profile.
### Any line starting with a '# will be ignored.
###
### A profile consists of a set of configuration items followed by a set of
### devices.
###
### An example would look like:
### name: onenic
### config:
###   raw.lxc: lxc.aa_profile=unconfined
### devices:
###   eth0:
###     nictype: bridged
###     parent: lxdbr0
###     type: nic
###
### Note that the name is shown but cannot be changed %s (%d more) '/' not allowed in snapshot name (none) ALIAS ARCH ARCHITECTURE Accept certificate Admin password for %s:  Aliases: An environment variable of the form HOME=/home/foo Architecture: %s Auto update: %s Available commands: Bytes received Bytes sent COMMON NAME CREATED AT Can't read from stdin: %s Can't unset key '%s', it's not currently set. Cannot provide container name to list Certificate fingerprint: %x Changes state of one or more containers to %s.

lxc %s <name> [<name>...] Client certificate stored at server:  Columns Config key/value to apply to the new container Connection refused; is LXD running? Container name is mandatory Container name is: %s Container published with fingerprint: %s Copy aliases from source Copy containers within or in between lxd instances.

lxc copy [remote:]<source container> [remote:]<destination container> [--ephemeral|e] Copying the image: %s Could not create server cert dir Create a read-only snapshot of a container.

lxc snapshot [remote:]<source> <snapshot name> [--stateful]

Creates a snapshot of the container (optionally with the container's memory
state). When --stateful is used, LXD attempts to checkpoint the container's
running state, including process memory state, TCP connections, etc. so that it
can be restored (via lxc restore) at a later time (although some things, e.g.
TCP connections after the TCP timeout window has expired, may not be restored
successfully).

Example:
lxc snapshot u1 snap0 Created: %s Creating %s Creating the container DESCRIPTION Delete containers or container snapshots.

lxc delete [remote:]<container>[/<snapshot>] [remote:][<container>[/<snapshot>]...]

Destroy containers or snapshots with any attached data (configuration, snapshots, ...). Device %s added to %s Device %s removed from %s EPHEMERAL EXPIRY DATE Enables debug mode. Enables verbose mode. Environment: Ephemeral container Event type to listen for Execute the specified command in a container.

lxc exec [remote:]container [--mode=auto|interactive|non-interactive] [--env EDITOR=/usr/bin/vim]... <command>

Mode defaults to non-interactive, interactive mode is selected if both stdin AND stdout are terminals (stderr is ignored). Expires: %s Expires: never FINGERPRINT Fast mode (same as --columns=nsacPt Fingerprint: %s Fingers the LXD instance to check if it is up and working.

lxc finger <remote> Force the container to shutdown. Force the removal of stopped containers. Force using the local unix socket. Format Generating a client certificate. This may take a minute... IPV4 IPV6 ISSUE DATE If this is your first time using LXD, you should also run: sudo lxd init Ignore aliases when determining what command to run. Ignore the container state (only for start). Image copied successfully! Image imported with fingerprint: %s Initialize a container from a particular image.

lxc init [remote:]<image> [remote:][<name>] [--ephemeral|-e] [--profile|-p <profile>...] [--config|-c <key=value>...]

Initializes a container using the specified image and name.

Not specifying -p will result in the default profile.
Specifying "-p" with no argument will result in no profile.

Example:
lxc init ubuntu u1 Invalid URL scheme "%s" in "%s" Invalid configuration key Invalid source %s Invalid target %s Ips: Keep the image up to date after initial copy LXD socket not found; is LXD running? Launch a container from a particular image.

lxc launch [remote:]<image> [remote:][<name>] [--ephemeral|-e] [--profile|-p <profile>...] [--config|-c <key=value>...]

Launches a container using the specified image and name.

Not specifying -p will result in the default profile.
Specifying "-p" with no argument will result in no profile.

Example:
lxc launch ubuntu:16.04 u1 List information on LXD servers and containers.

For a container:
 lxc info [<remote>:]container [--show-log]

For a server:
 lxc info [<remote>:] Lists the available resources.

lxc list [resource] [filters] [--format table|json] [-c columns] [--fast]

The filters are:
* A single keyword like "web" which will list any container with a name starting by "web".
* A regular expression on the container name. (e.g. .*web.*01$)
* A key/value pair referring to a configuration item. For those, the namespace can be abreviated to the smallest unambiguous identifier:
 * "user.blah=abc" will list all containers with the "blah" user property set to "abc".
 * "u.blah=abc" will do the same
 * "security.privileged=1" will list all privileged containers
 * "s.privileged=1" will do the same
* A regular expression matching a configuration item or its value. (e.g. volatile.eth0.hwaddr=00:16:3e:.*)

Columns for table format are:
* 4 - IPv4 address
* 6 - IPv6 address
* a - architecture
* c - creation date
* n - name
* p - pid of container init process
* P - profiles
* s - state
* S - number of snapshots
* t - type (persistent or ephemeral)

Default column layout: ns46tS
Fast column layout: nsacPt Log: Make image public Make the image public Manage configuration profiles.

lxc profile list [filters]                     List available profiles.
lxc profile show <profile>                     Show details of a profile.
lxc profile create <profile>                   Create a profile.
lxc profile copy <profile> <remote>            Copy the profile to the specified remote.
lxc profile get <profile> <key>                Get profile configuration.
lxc profile set <profile> <key> <value>        Set profile configuration.
lxc profile delete <profile>                   Delete a profile.
lxc profile edit <profile>
    Edit profile, either by launching external editor or reading STDIN.
    Example: lxc profile edit <profile> # launch editor
             cat profile.yml | lxc profile edit <profile> # read from profile.yml
lxc profile apply <container> <profiles>
    Apply a comma-separated list of profiles to a container, in order.
    All profiles passed in this call (and only those) will be applied
    to the specified container.
    Example: lxc profile apply foo default,bar # Apply default and bar
             lxc profile apply foo default # Only default is active
             lxc profile apply '' # no profiles are applied anymore
             lxc profile apply bar,default # Apply default second now

Devices:
lxc profile device list <profile>                                   List devices in the given profile.
lxc profile device show <profile>                                   Show full device details in the given profile.
lxc profile device remove <profile> <name>                          Remove a device from a profile.
lxc profile device get <[remote:]profile> <name> <key>              Get a device property.
lxc profile device set <[remote:]profile> <name> <key> <value>      Set a device property.
lxc profile device unset <[remote:]profile> <name> <key>            Unset a device property.
lxc profile device add <profile name> <device name> <device type> [key=value]...
    Add a profile device, such as a disk or a nic, to the containers
    using the specified profile. Project-Id-Version: lxd
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-05-11 18:51-0400
PO-Revision-Date: 2016-06-04 23:41+0000
Last-Translator: Данил Тютюник <den.tyutyunik@ya.ru>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:34+0000
X-Generator: Launchpad (build 18115)
   Использование диска:   Использование памяти:   Использование сети: ### Этот YAML предоставляет конфигурацию.
### Любая строка, начинающаяся с '#' будет игнорироваться.
###
### Пример конфигурации выглядит следующим образом:
### name: container1
### profiles:
### - default
### config:
### volatile.eth0.hwaddr: 00:16:3e:e9:f8:7f
### devices:
### homedir:
### path: /extra
### source: /home/user
### type: disk
### ephemeral: false
###
### Обратите внимание, что имя отображается, но не может быть изменено ### Этот YAML представляет изображения свойств.
### Любая строка, начинающаяся с '#' будет игнорироваться.
###
### Каждое свойство представлено одной строкой:
### Примером может быть:
### description: My custom image ### Этот YAML представляет профиль.
### Любая строка, начинающаяся с '#' будет игнорироваться.
###
### Профиль состоит из набора элементов конфигурации, за которым следует набор
### устройств.
###
### Пример может выглядеть следующим образом:
### name: onenic
### config:
### raw.lxc: lxc.aa_profile=unconfined
### devices:
### eth0:
### nictype: bridged
### parent: lxdbr0
### type: nic
###
### Обратите внимание, что имя отображается, но не может быть изменено %s (%d больше) '/' не разрешено в имени снимка (отсутствует) ALIAS ARCH ARCHITECTURE Принять сертификат Пароль администратора для %s:  Псевдонимы: Переменная окружения вида HOME=/home/foo Архитектура: %s Автоматическое обновление: %s Доступные команды: Получено байт Байтов отправлено COMMON NAME CREATED AT Невозможно прочитать из stdin: %s Может не заданы ключи '%s', это в настоящее время не установлено. Не удалось предоставить имя контейнера в список Отпечаток сертификата: %x Изменения состояния одного или нескольких контейнеров %s.

lxc %s <имя> [<имя> ...] сертификат клиента хранится на сервере:  Столбцы Конфигурация ключ/значение, чтобы применить к новому контейнеру В соединении отказано; LXD запущен? Имя контейнера является обязательным Имя контейнера: %s Контейнер публикуется с отпечатком: %s Копировать псевдонимы из источника Копирование контейнеров внутри или в промежутках между lxc экземплярами.

lxc copy [remote:]<источник контейнера> [remote:]<контейнер назначения> [--ephemeral|e] Копирование образа: %s Не удалось создать сервер CERT DIR Создать снимок контейнера только для чтения.

lxc snapshot [remote:]<источник> <имя снимка> [--stateful]

Создает снимок контейнера (необязательно с памятью контейнера).
Когда --stateful используется, LXD пытается создать контрольную точку контейнера
в рабочем состоянии, в том числе состояние памяти процесса, TCP-соединений и т.д., так что
все может быть восстановлено (с помощью LXC restore) в более позднее время (хотя некоторые вещи, например,
TCP-соединения после того, как окно TCP-тайм-аут истек, не может быть восстановлено
успешно).

Пример:
lxc snapshot u1 snap0 Создано: %s Создание %s Создание контейнера DESCRIPTION Удалить контейнеры или снимки контейнеров.

lxc delete [remote:]<контейнер>[/<снимок контейнера>] [remote:][<контейнер>[/<снимок контейнера>]...]

Уничтожить контейнеры или снимки с любыми прикрепленными данными (конфигурация, снимки, ...). Устройство %s добавляется к %s Устройство %s удаляется из %s EPHEMERAL EXPIRY DATE Включает режим отладки. Включить подробный режим. Переменные окружения: Эфемерный контейнер Тип события для прослушивания Выполнить указанную команду в контейнере.

 lxc exec [remote:]container [--mode=auto|interactive|non-interactive] [--env EDITOR=/usr/bin/vim]... <command>

Режим по умолчанию неинтерактивный; интерактивный режим выбран, если оба STDIN и STDOUT терминалы (STDERR игнорируется). Истекает: %s Истикает: никогда FINGERPRINT Быстрый режим (такой же, как --columns=nsacPt Отпечаток: %s Экземпляр LXD, чтобы проверить, если он запущен и работает.

lxc finger <remote> Принудительно закрыть контейнер. Принудительное удаление остановленных контейнеров. Принудительно с помощью локального unix-сокета. Формат Создание сертификата клиента. Это может занять минуту... IPv4 IPv6 ISSUE DATE Если это ваш первый раз использования LXC, вы должны запустить: sudo lxd init Игнорировать псевдонимы при определении того, что команда для запуска. Не обращайте внимания на хранилище контейнера (только для начала). Образ скопирован успешно! Образ, импортирован с снимком: %s Инициализировать контейнер от конкретного изображения.

 lxc init [remote:]<образ> [remote:][<имя>] [--ephemeral|-e] [--profile|-p <профиль>...] [--config|-c <ключ=значение>...]

Инициализирует контейнер, используя указанные образ и имя.

Не указав '-p' приведет профиль по умолчанию.
Указание '-p' без аргументов приведет к отсутствию профиля.

Пример:
 lxc init ubuntu u1 Неверная схема URL "%s" в "%s" Недопустимый ключ конфигурации Неверный источник %s Недопустимая цель %s IPs: Держите образ до даты после первоначальной копии LXD сокет не найден; LXD запущен? Запуск контейнера из конкретного образа.

 lxc launch [remote:]<образ> [remote:][<имя>] [--ephemeral|-e] [--profile|-p <profile>...] [--config|-c <ключ=значение>...]

Запускает контейнер, используя указанные образ и имя.

Не указав '-p' приведет профиль по умолчанию.
Указание '-p' без аргументов приведет к отсутствию профиля.

Пример:
lxc launch ubuntu:16.04 u1 Разместить информацию на серверах LXD и контейнеров.

Для контейнера:
 lxc info [<remote>:]container [--show-log]

Для сервера:
 lxc info [<remote>:] Список доступных ресурсов.

 lxc list [resource] [filters] [--format table|json] [-c columns] [--fast]

Фильтры:
* Одно ключевое слово, как "web", который будет перечислять любой контейнер с именем, начинающимся на "web".
* Регулярное выражение на имя контейнера. (например .*web.*01$)
* Пара ключ/значение со ссылкой на элемент конфигурации. Для тех, пространство имен может быть abreviated наименьшему однозначного идентификатора:
 * "user.blah=abc" будут перечислены все контейнеры с "blah", свойство пользователя установлено в положение "ABC".
 * "u.blah=abc" будет делать то же самое
 * "security.privileged=1" будут перечислены все привилегированные контейнеры
 * "s.privileged=1" будет делать то же самое
* Регулярное выражение на соответствие элемента конфигурации или его значения. (Например, volatile.eth0.hwaddr=00:16:3e:.*)

Колонны для формата таблицы являются:
* 4 - IPv4-адрес
* 6 - IPv6-адрес
* a - архитектура
* c - дата создания
* n - имя
* p - PID процесса инициализации контейнера
* P - профили
* s - состояние
* S - количество снимков
* t - тип (постоянный или эфемерным)

По умолчанию макет колонки: ns46tS
Быстрый макет колонки: nsacPt Журнал: Сделать публичный образ Сделать публичный образ Управление профилями конфигурации.

lxc profile list [filters]Список доступных профилей.
lxc profile show <профиль> Показать детали профиля.
lxc profile create <профиль> Создать профиль.
lxc profile copy <профиль> <указанный> Скопировать профиль в указанный удаленный.
lxc profile get <профиль> <ключ> Получить конфигурацию профиля.
lxc profile set <профиль> <ключ> <значение> Установить конфигурации профиля.
lxc profile delete <профиль> Удалить профиль.
lxc profile edit <профиль>
    Редактировать профиль, либо путем запуска внешнего редактора или чтения STDIN.
    Пример: lxc profile edit <профиль> # запуск редактора
             cat profile.yml | lxc profile edit <профиль> # читать из profile.yml
lxc profile apply <контейнер> <profiles>
    Применить разделенный запятыми список профилей в контейнер, в порядок.
    Все профили прошедшие в этом вызове (и только те) будут применяться
    в указанном контейнере.
    Пример: lxc profile apply foo default,bar # Примернить по умолчанию и бар
             lxc profile apply foo default # Только по умолчанию активен
             lxc profile apply '' # Ни одного профиля не применяются больше
             lxc profile apply bar,default # Применить по умолчанию второй в настоящее время

Devices:
lxc profile device list <профиль> Список устройств в данном профиле.
lxc profile device show <профиль> Показать полные сведения об устройстве в данном профиле.
lxc profile device remove <профиль> <имя> Удалить устройство из профиля.
lxc profile device get <[remote:]профиль> <имя> <ключ> Получить свойство устройства.
lxc profile device set <[remote:]профиль> <имя> <ключ> <значение> Установить свойство устройства.
lxc profile device unset <[remote:]профиль> <имя> <ключ> Отключить свойство устройства.
lxc profile device add <профиль> <имя устройства> <тип устройства> [ключ=значение]...
    Добавить профиль устройств, такие как диск или сетевой адаптер, к контейнерам
    с использованием указанного профиля. 