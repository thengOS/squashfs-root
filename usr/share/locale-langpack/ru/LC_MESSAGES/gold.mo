��    8      �  O   �      �     �     �     �  "     )   3  "   ]     �     �  ?   �  2   �     $  &   D  (   k  2   �     �     �     �          !     -     =     P  /   a  *   �  *   �  �   �     �     �     �     �     �     	      	     ?	  (   Q	     z	  !   �	  !   �	     �	     �	     
      5
     V
     t
     �
  $   �
  %   �
     �
  $     .   :     i     �  %   �  #   �     �  �  �  :   �     �  &     V   *  Y   �  C   �  :     )   Z  �   �  r     =   �  D   �  U     _   [  <   �  "   �  3        O     V     b  *   r  $   �  {   �  U   >  U   �  �  �  "   �  $   �  *   �  +   
  5   6  @   l  4   �     �  :   �  E   1  #   w  #   �  !   �     �         "   !     D     d  !   �  &   �  '   �     �  A     V   Q  B   �  .   �  =     Z   X     �                      .   #               /                       ,      4   +             0             -   
          2   (              5       '                    *       !   "      $   &   3                    1   7              6      %   )                        	   8              
Memory map

 %s failed: %s %s: archive libraries: %u
 %s: bad archive symbol table names %s: bad extended name entry at header %zu %s: bad extended name index at %zu %s: can not read directory: %s %s: fatal error:  %s: invalid option value (expected a floating point number): %s %s: invalid option value (expected an integer): %s %s: loaded archive members: %u
 %s: member at %zu is not an ELF object %s: no archive symbol table (run ranlib) %s: removing unused section from '%s' in file '%s' %s: short archive header at %zu %s: stat failed: %s %s: total archive members: %u
 ** PLT ** eh_frame ** eh_frame_hdr ** merge constants ** merge strings --build-id argument '%s' not a valid hex number DT_NEEDED value out of range: %lld >= %lld DT_SONAME value out of range: %lld >= %lld This program is free software; you may redistribute it under the terms of
the GNU General Public License version 3 or (at your option) a later version.
This program has absolutely no warranty.
 cannot find %s cannot find %s%s cannot open %s: %s cannot open %s: %s: command line changed could not reopen file %s missing SHT_SYMTAB_SHNDX section munmap failed: %s not compressing section data: zlib error output is not an ELF file. pthead_mutextattr_init failed: %s pthread_cond_broadcast failed: %s pthread_cond_destroy failed: %s pthread_cond_init failed: %s pthread_cond_signal failed: %s pthread_mutex_destroy failed: %s pthread_mutex_init failed: %s pthread_mutex_lock failed: %s pthread_mutex_unlock failed: %s pthread_mutexattr_destroy failed: %s pthread_mutextattr_settype failed: %s pthread_once failed: %s script or expression reference to %s undefined symbol '%s' referenced in expression unknown PHDR type (try integer) unknown constant %s unrecognized --build-id argument '%s' unsupported reloc %u in object file while closing %s: %s Project-Id-Version: binutils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2016-06-02 15:43+0000
Last-Translator: Данил Тютюник <den.tyutyunik@ya.ru>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:03+0000
X-Generator: Launchpad (build 18115)
 
Данные о распределении памяти

 %s не удалось: %s %s: архив библиотек: %u
 %s: плохо заархивированы имена таблицы символов %s:  плохо расширяемое имя элемента в заголовке  %zu %s:  плохо расширяемое имя индекса и %zu %s: невозможно открыть каталог: %s %s: критическая ошибка:  %s: недопустимое значение параметра (должно быть число с плавающей точкой): %s %s: недопустимое значение параметра (должно быть целое число): %s %s: загруженные элементы архива: %u
 %s:  элемент  %zu не является объектом  ELF %s:  нет архива таблицы символов (запустить  ranlib) %s: удаление неиспользуемого раздела из '%s' в файле '%s' %s: короткий заголовок архива и  %zu %s: сбой вызова stat: %s %s: всего элементов архива: %u
 ** PLT ** eh_frame ** eh_frame_hdr ** объединить константы ** объединить строки --build-id аргумент '%s' не является допустимым шестнадцатеричным числом значение DT_NEEDED находится вне диапазона: %lld >= %lld значение DT_SONAME находится вне диапазона: %lld >= %lld Данная программа является свободным программным обеспечением;  вы можете распространять её в соответствии
с условиями GNU General Public License Версия 3 или (на ваш выбор) более поздней версии.
Эта программа не имеет абсолютно никаких гарантий.
 невозможно найти %s невозможно найти %s%s невозможно открыть %s: %s невозможно открыть %s: %s: изменения в командной строке невозможно повторно открыть файл %s отсутствует раздел SHT_SYMTAB_SHNDX сбой munmap: %s не сжатый блок данных: ошибка zlib выходные данные не являются ELF-файлом. сбой pthead_mutextattr_init: %s сбой pthread_cond_broadcast: %s сбой pthread_cond_destroy: %s сбой pthread_cond_init: %s сбой pthread_cond_signal: %s сбой pthread_mutex_destroy: %s сбой pthread_mutex_init: %s сбой pthread_mutex_lock: %s сбой pthread_mutex_unlock: %s сбой pthread_mutexattr_destroy: %s сбой pthread_mutextattr_settype: %s сбой pthread_once: %s скрипт или выражение ссылается на %s ссылка на неопределенный символ '%s' в выражении неизвестный тип PHDR (ожидается целое) неизвестная постоянная %s нераспознанный --build-id аргумент '%s' не поддерживается перенесение %u в объектный файл при закрытии %s: %s 