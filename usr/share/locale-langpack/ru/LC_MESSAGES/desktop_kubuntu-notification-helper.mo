��          �      <      �  $   �  +   �  F     4   I  L   ~  F   �  4     "   G     j     |     �     �     �     �     �       0   #  �  T  :     X   =  w   �  W     �   f  �   �  P   �  :   �  /   	  %   =	  )   c	  '   �	  #   �	  +   �	  +   
  '   1
  f   Y
     
                                   	                                                             CommentA system restart is required CommentAdditional drivers can be installed CommentAn application has crashed on your system (now or in the past) CommentControl the notifications for system helpers CommentExtra packages can be installed to enhance application functionality CommentExtra packages can be installed to enhance system localization CommentSoftware upgrade notifications are available CommentSystem Notification Helper NameApport Crash NameDriver Enhancement NameLocalization Enhancement NameNotification Helper NameOther Notifications NameReboot Required NameRestricted Install NameUpgrade Hook X-KDE-KeywordsNotify,Alerts,Notification,popups Project-Id-Version: kubuntu-notification-helper
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-04-01 17:07+0000
PO-Revision-Date: 2014-02-21 07:09+0000
Last-Translator: ☠Jay ZDLin☠ <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:48+0000
X-Generator: Launchpad (build 18115)
 Требуется перезагрузка системы Могут быть установлены дополнительные драйверы Приложение неожиданно завершилось с ошибкой (сейчас или недавно) Управление уведомлениями системных помощников Для повышения функциональности можно установить дополнительные пакеты. Для улучшения локализации системы могут быть установлены дополнительные пакеты Доступны оповещения об обновлении программ Помощник системных уведомлений Неисправимая ошибка в Apport Улучшение драйверов Улучшение локализации Помощник уведомлений Прочие уведомления Требуется перезагрузка Ограниченная установка Обновить подключение Сообщения,Предупреждения,Уведомления,Всплывающие окна 