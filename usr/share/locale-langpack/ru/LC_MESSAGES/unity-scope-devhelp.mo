��          \      �       �      �      �      �   ;   �      !  �   5       �  ,     �     �     �  �     /   �  ]  �     .                                       Devhelp Search Devhelp Show Sorry, there are no Devhelp results that match your search. Technical Documents This is an Ubuntu search plugin that enables information from Devhelp to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. devhelp;dev;help;doc; Project-Id-Version: unity-scope-devhelp
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-28 09:22+0000
PO-Revision-Date: 2014-04-11 09:12+0000
Last-Translator: ☠Jay ZDLin☠ <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Devhelp Искать в Devhelp Показать К сожалению, результаты, соответствующие вашим параметрам поиска, отсутствуют в Devhelp. Техническая документация Это поисковый модуль Ubuntu, позволяющий искать и отображать информацию из Devhelp в главном меню под заголовком Код. Если вы не хотите осуществлять поиск в данном ресурсе, отключите данный модуль. devhelp;dev;help;doc; 