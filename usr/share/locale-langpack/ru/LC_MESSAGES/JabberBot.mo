��          �   %   �      p  <   q  9   �  @   �  C   )     m  N   s     �  Z   �  ?   .  X   n  #   �     �  )   �  .   !  t   P  E   �  ?     4   K     �     �     �  $   �  #   �  $   �  [     *   y  /   �  �  �  P   j	  j   �	  X   &
  �   
       c     '   �  y   �  m   "  �   �  B        ^  N   v  B   �  �     s   �  y   D  _   �  "     +   A     m  N   �  ]   �  ^   .  �   �  /     T   A                                                           
                                                     	           %(command)s - %(description)s

Usage: %(command)s %(params)s A serious error occured while processing your request:
%s An internal error has occured, please contact the administrator. Credentials check failed, you may be unable to see all information. Error Following detailed information on page "%(pagename)s" is available::

%(data)s Full-text search Hello there! I'm a MoinMoin Notification Bot. Available commands:

%(internal)s
%(xmlrpc)s Here's the page "%(pagename)s" that you've requested:

%(data)s Last author: %(author)s
Last modification: %(modification)s
Current version: %(version)s Please specify the search criteria. Search text Submit this form to perform a wiki search That's the list of pages accesible to you:

%s The "help" command prints a short, helpful message about a given topic or function.

Usage: help [topic_or_function] The "ping" command returns a "pong" message as soon as it's received. This command may take a while to complete, please be patient... This command requires a client supporting Data Forms Title search Unknown command "%s"  Wiki search You are not allowed to use this bot! You must set a (long) secret string You must set a (long) secret string! You've specified a wrong parameter list. The call should look like:

%(command)s %(params)s Your request has failed. The reason is:
%s searchform - perform a wiki search using a form Project-Id-Version: moin
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-07-17 03:14+0200
PO-Revision-Date: 2011-09-16 10:19+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:11+0000
X-Generator: Launchpad (build 18115)
 %(command)s - %(description)s
Использование: %(command)s %(params)s Возникла серьезная проблема во время обработки запроса:
%s Внутренняя ошибка, обратитесь к администратору. Невозможно проверить полномочия, возможно вы не сможете увидеть все сведения. Ошибка На странице "%(pagename)s" доступны следующие сведения:
%(data)s Полнотекстовой поиск Привет! Я робот-уведомитель MoinMoin. Доступные команды: 

%(internal)s
%(xmlrpc)s Здесь представлена запрошенная вами страница "%(pagename)s":
%(data)s Последний автор: %(author)s
Последнее изменение: %(modification)s
Текущая версия: %(version)s Пожалуйста, укажите критерии поиска Искать текст Подтвердите эту форму для начала wiki-поиска Список страниц, доступных для Вас:

%s Команда "help" показывает краткое сообщение по выбранной теме или функции.

Использование: help [тема_или_функция] Команда "ping" возвращает сообщение "pong" сразу, как получает ответ Эта команда требует времени для выполнения, наберитесь терпения... Эта команда требует поддержки Data Forms в Вашем клиенте Поиск по заголовку Неизвестная команда "%s"  Вики поиск У Вас нет прав на использование этого бота! Нужно ввести (желательно длинную) секретную строку Нужно ввести (желательно длинную) секретную строку! Указан неверный список параметров. Вызов должен быть вида:

%(command)s %(params)s Ошибка запроса. Причина:
%s searchform - предоставляет wiki-поиск с помощью формы 