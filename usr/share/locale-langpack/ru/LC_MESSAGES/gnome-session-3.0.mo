��    I      d  a   �      0     1  4  N     �  R   �  g   �  Z   W	  '   �	     �	     �	     �	     
  	   '
  1   1
  	   c
  	   m
  (   w
  )   �
     �
     �
     �
  #      '   $  "   L     o     �     �     �     �     �     �     �      �          	             !   /  '   Q  %   y  	   �     �  '   �     �  P   �     1     H     X     e  
   t  ;     3   �  Q   �     A     P  &   g     �      �  #   �      �  *   	      4      U  /   v     �  =   �  	         
               #  (   0     Y  �  i  *     �  3  '     �   ,  �   �  �   �  =   H  %   �  #   �  ]   �  4   .     c  a   q     �     �  Q   �  I   H  D   �     �     �  \     Z   d  P   �  #     ,   4     a  *   r     �  #   �     �     �  U   �     =     [     s     �  /   �  Q   �  1        O     b  X   u     �  y   �  +   c  &   �     �     �     �  Q      V   T   �   �   #   1!  K   U!  ]   �!  F   �!  6   F"  C   }"  6   �"  ^   �"  O   W#  A   �#  [   �#  ,   E$  �   r$     �$     	%     (%     G%     P%  `   k%     �%         )      +   I   E   A       #   D   =      "          ;          C   *           0      /              H            9   4      	      
              >   <   8       $           -                   B   ,          3       ?              :   &          5   2   7                @          6                           G   1      '   %   F   .   !       (     - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Add Startup Program Additional startup _programs: Allow logout Browse… Choose what applications to start when you log in Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Could not display help document Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don't prompt for user confirmation Edit Startup Program Enable debugging code Enabled Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Icon Ignoring any existing inhibitors Log out No description No name Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Select Command Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Applications Startup Applications Preferences The startup command cannot be empty The startup command is not valid This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: ru
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-05-18 08:40+0000
PO-Revision-Date: 2015-07-10 03:28+0000
Last-Translator: Stas Solovey <whats_up@tut.by>
Language-Team: Русский <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:39+0000
X-Generator: Launchpad (build 18115)
Language: ru
  — менеджер сеансов GNOME %s [ПАРАМЕТР…] КОМАНДА

Выполнить КОМАНДУ с ограничением некоторой функциональности сеанса.

  -h, --help         Показать эту справку
  --version          Показать версию программы
  --app-id ID        Идентификатор используемой программы
                     при ограничении (необязательно)
  --reason ПРИЧИНА   Причина для ограничения (необязательно)
  --inhibit АРГУМЕНТ Список отключаемых функций, разделённых запятыми:
                     logout, switch-user, suspend, idle, automount
  --inhibit-only     Не запускать КОМАНДУ и включить вечное ожидание

При использовании параметра --inhibit включается ожидание.
 %s требуется аргумент
 Произошла ошибка, и системе не удалось восстановиться.
Нужно завершить сеанс и войти снова. Произошла ошибка, и системе не удалось восстановиться. Все расширения были отключены в целях безопасности. Произошла ошибка, и системе не удалось восстановиться. Обратитесь к системному администратору Сеанс с именем «%s» уже существует КАТАЛОГ_АВТОЗАПУСКА Добавить программу Дополнительные _программы, запускаемые при старте: Разрешить завершение сеанса Обзор… Выберите приложения, запускаемые при входе в систему _Команда: Описание: Не удалось соединиться с менеджером сеансов Не удалось создать сокет, слушающий ICE: %s Не удалось показать документ справки Другой сеанс Другой сеанс Отключить проверку наличия аппаратного ускорения Не загружать указанные пользователем приложения Не требовать подтверждения от пользователя Изменить программу Включить отладочный код Включено Не удалось запустить %s
 GNOME Фиктивный сеанс GNOME GNOME на Wayland Значок Игнорирование любых существующих препятствий Завершить сеанс Нет описания Нет имени Не отвечает О, нет! Что-то пошло не так. Заместить стандартные каталоги автозапуска Выберите сеанс для запуска Выключить Программа Программа вызвана с конфликтующими параметрами Перезагрузить Новые клиентские подключения отклоняются, т. к. сеанс завершается
 Сохраненное приложение П_ереименовать сеанс НАЗВАНИЕ_СЕАНСА Выберите команду Сеанс %d Имена сеансов не могут содержать символы «/» Имена сеансов не могут начинаться с символа «.» Имена сеансов не могут начинаться с символа «.» или содержать символы «/» Использовать сеанс Показывать предупреждения от расширений Показать для отладки диалог с сообщением об ошибке Автоматически запускаемые приложения Настройки запуска приложений Команда запуска не может быть пустой Некорректная команда запуска Эта запись позволяет вам выбрать сохранённый сеанс Эта программа блокирует завершение сеанса. Этот сеанс позволяет вам войти в GNOME Этот сеанс позволяет вам войти в GNOME с помощью Wayland Версия этого приложения _Автоматически запоминать запущенные приложения при выходе из сеанса _Продолжить _Завершить сеанс _Завершить сеанс _Имя: _Создать сеанс _Запомнить запущенные в настоящий момент приложения _Удалить сеанс 