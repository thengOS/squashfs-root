��            )         �     �  �  �  �  �  !      !   B  /   d  =   �  2   �  $        *  %   C  .   i  +   �  *   �  .   �     	     9	     U	     s	     �	  &   �	     �	     �	  @   �	  3   /
  R  c
     �     �     �       �        �  �  �  �  �  F   G  F   �  `   �  �   6  Q   �  J   	  :   T  �   �  M     K   `  M   �  _   �  6   Z  3   �  3   �  9   �  $   3  =   X  &   �     �  �   �  �   r    �  0   �  "   0  2   S  /   �                                                                                     	                                      
                    %s %s
   -n, --name=name         get the named extended attribute value
  -d, --dump              get all extended attribute values
  -e, --encoding=...      encode values (as 'text', 'hex' or 'base64')
      --match=pattern     only get attributes with names matching pattern
      --only-values       print the bare values only
  -h, --no-dereference    do not dereference symbolic links
      --absolute-names    don't strip leading '/' in pathnames
  -R, --recursive         recurse into subdirectories
  -L, --logical           logical walk, follow symbolic links
  -P  --physical          physical walk, do not follow symbolic links
      --version           print version and exit
      --help              this help text
   -n, --name=name         set the value of the named extended attribute
  -x, --remove=name       remove the named extended attribute
  -v, --value=value       use value as the attribute value
  -h, --no-dereference    do not dereference symbolic links
      --restore=file      restore extended attributes
      --version           print version and exit
      --help              this help text
 %s %s -- get extended attributes
 %s %s -- set extended attributes
 %s: %s: No filename found in line %d, aborting
 %s: No filename found in line %d of standard input, aborting
 %s: Removing leading '/' from absolute path names
 %s: invalid regular expression "%s"
 -V only allowed with -s
 A filename to operate on is required
 At least one of -s, -g, -r, or -l is required
 Attribute "%s" had a %d byte value for %s:
 Attribute "%s" has a %d byte value for %s
 Attribute "%s" set to a %d byte value for %s:
 Could not get "%s" for %s
 Could not list "%s" for %s
 Could not remove "%s" for %s
 Could not set "%s" for %s
 No such attribute Only one of -s, -g, -r, or -l allowed
 Unrecognized option: %c
 Usage: %s %s
 Usage: %s %s
       %s %s
Try `%s --help' for more information.
 Usage: %s %s
Try `%s --help' for more information.
 Usage: %s [-LRSq] -s attrname [-V attrvalue] pathname  # set value
       %s [-LRSq] -g attrname pathname                 # get value
       %s [-LRSq] -r attrname pathname                 # remove attr
       %s [-LRq]  -l pathname                          # list attrs 
      -s reads a value from stdin and -g writes a value to stdout
 getting attribute %s of %s listing attributes of %s setting attribute %s for %s setting attributes for %s Project-Id-Version: attr
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-03 13:57+0000
PO-Revision-Date: 2013-10-14 06:35+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:59+0000
X-Generator: Launchpad (build 18115)
        %s %s
   -n, --name=имя получить расширенный атрибут с указанным именем
  -d, --dump вывести значения всех расширенных атрибутов
  -e, --encoding=... кодировка значений ('text', 'hex' или 'base64')
      --match=шаблон выводить только атрибуты с соответствующими именами
      --only-values печать только значений
  -h, --no-dereference не менять название символических ссылок
      --absolute-names не удалять ведущий '/' из путей
  -R, --recursive входить в подкаталоги
  -L, --logical логический обход, следовать по симв. ссылкам
  -P --physical физический обход, не следовать по симв. ссылкам
      --version вывести версию и выйти
      --help этот текст
   -n, --name=name         задать значение названного расширенного атрибута
  -x, --remove=name       удалить названный расширенный атрибут
  -v, --value=value       использовать значение как значения атрибута
  -h, --no-dereference    не разыменовывать символические ссылки
      --restore=file      восстановить расширенные атрибуты
      --version           показать версию и выйти
      --help              этот справочный текст
 %s %s -- получение расширенных атрибутов
 %s %s -- установка расширенных атрибутов
 %s: %s: Имя файла не найдено в строке %d, отмена операции
 %s: Имя файла не найдено в строке %d стандартного ввода, отмена операции
 %s: Удаление начальных '/' из абсолютных путей
 %s: неправильное регулярное выражение "%s"
 -V можно использовать только с -s
 Необходимо указать имя файла, над которым будет производится операция
 Требуется указать хотя бы одно из -s, -g, -r, -l
 Атрибут "%s" имел %d -байтное значение для %s
 Атрибут "%s" имеет %d-байтное значение для  %s
 Атрибут "%s" установлен до %d -байтного значения для %s:
 Невозможно получить  "%s" для %s
 Невозможно вывести "%s" для %s
 Невозможно удалить "%s" для %s
 Невозможно установить "%s" для %s
 Нет такого атрибута Только одно из -s, -g, -r, -l разрешено
 Неизвестная опция: %c
 Применение: %s %s
 Применение: %s %s
       %s %s
Попробуйте `%s --help' для получения дополнительной информации.
 Применение: %s %s
Попробуйте `%s --help' для получения дополнительной информации.
 Применение: %s [-LRSq] -s имя_атрибута [-V значение_атрибута] путь  # установка значения
       %s [-LRSq] -g имя_атрибута путь  # получить значения
       %s [-LRSq] -r имя_атрибута путь   # удалить атрибут
       %s [-LRq]  -l путь # получить список атрибутов 
       -s читает значение из stdin и -g записывает значение в stdout
 получение атрибутов %s из %s список атрибутов %s установка атрибутов %s для %s установка атрибутов для %s 