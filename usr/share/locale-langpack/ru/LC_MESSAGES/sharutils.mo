��    ;      �  O   �        "   	  -   ,     Z     l     �     �     �     �  	   �     �     �  	   �     �     �  !        (     <     N  %   `     �     �     �     �     �  @   �  2   9     l     �     �     �  )   �     �     �                     1     =     P     V     g     w     �     �     �     �     �     �     �     �     �     �     
	     	     (	     -	     @	     U	  �  Y	  ?     W   H     �  5   �     �       .   0     _     r     �     �     �     �  8   �  N     /   U      �     �  A   �       5   !  .   W     �  "   �  z   �  o   B  G   �     �          5  \   Q  6   �  9   �          &  8   3     l  $   �     �     �  '   �  %        ,     9     Y     `     r  *   �  ,   �  *   �  
     /     &   L     s     �  (   �      �     �     ;      4       :            3   &   9                                        +   	      1      %                  -   $   2   )          "              /   8   #                                           0            *   ,                 6      '   !             (   
   5   7   .    %s is probably not a shell archive %s looks like raw C code, not a shell archive %s: No `end' line %s: Not a regular file %s: Short file %s: Write error %s: illegal option -- %c
 (binary) (bzipped) (compressed) (empty) (gzipped) (text) Cannot access %s Cannot get current directory name Cannot open file %s Created %d files
 End of %s part %d End of part %d, continue with part %d File %s is complete File %s is continued in part %d Found no shell commands in %s MD5 check failed No input files Note: not verifying md5sums.  Consider installing GNU coreutils. PLEASE avoid -X shars on Usenet or public networks Please unpack part 1 first! SKIPPING %s Saving %s (%s) Starting file %s
 Too many directories for mkdir generation Unknown system error You have unpacked the last part all archive cannot access %s chmod of %s continuing file %s empty exit immediately explain choices extraction aborted help memory exhausted no none overwrite %s overwrite all files overwrite no files overwrite this file quit restore of %s failed skip this file standard input text uudecoding file %s x - extracting %s %s yes Project-Id-Version: sharutils 4.6.3-pre4
Report-Msgid-Bugs-To: bug-gnu-utils@gnu.org
POT-Creation-Date: 2015-05-30 08:36-0700
PO-Revision-Date: 2014-01-07 01:11+0000
Last-Translator: Pavel Maryanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:17+0000
X-Generator: Launchpad (build 18115)
Language: ru
 Вероятно, %s не является shell-архивом %s похож на необработанный код C, а не на shell-архив %s: Нет строки `end' %s: Не является обычным файлом %s: Короткий файл %s: Ошибка записи %s: недопустимая опция -- %c
 (двоичный) (сжатый bzip) (сжатый) (пустой) (сжатый gzip) (текстовый) Невозможно получить доступ к %s Невозможно получить имя текущего каталога Невозможно открыть файл %s Создано файлов: %d
 Конец %s части %d Конец части %d, продолжение в части %d Файл %s завершён Файл %s продолжается в части %d В %s не найдены shell-команды Сбой проверки MD5 Нет входных файлов Замечание: md5-суммы не проверяются. Как насчёт установки coreutils от GNU? ПОЖАЛУЙСТА, избегайте режима -X в Usenet или общедоступных сетях Распакуйте сначала часть 1, пожалуйста! ПРОПУСКАЕТСЯ %s Сохраняется %s (%s) Начало файла %s
 Слишком много каталогов для создания с помощью mkdir Неизвестная системная ошибка Вы распаковали последнюю часть все архива невозможно получить доступ к %s Выполняется chmod %s продолжение файла %s пустой выйти немедленно объяснение вариантов извлечение прервано помощь Память исчерпана нет ни одного перезаписать %s перезаписать все файлы не перезаписывать файлы перезаписать этот файл выход не удалось восстановить %s пропустить этот файл стандартный ввод текстовый декодируется uue-файл %s x - извлекается %s %s да 