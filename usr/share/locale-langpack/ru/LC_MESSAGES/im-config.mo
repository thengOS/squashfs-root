��    A      $  Y   ,      �  �   �  t        �  �   �  n   U  "  �  e   �	  X   M
  O   �
     �
     
  J     �   j  Q   
  B   \  K   �  ;   �     '     F     e  x   �  �  �  �   �  9     �   W  ?   �  {  !  D   �  X   �     ;     C  -   _  :   �  N   �      {   5  �   �  L   M  }   �  <     s   U  $   �  0   �  (     %   H  %   n  )   �  ,   �  '   �  &     3   :  %   n     �      �     �  6   �     �       �    F   �  >   *  :   i     �     �  �  �  �   {   v   B!     �!    �!  �   �"  �  �#     7'  m   �'  �   %(  +   �(  9   �(  �   )  �   �)  }   �*  Z   8+  q   �+  P   ,  9   V,  7   �,  5   �,  �   �,  ?  �-  �   �/  I   �0  �   1  ]   2  �  c2  b   6  s   e6     �6  9   �6  L   *7  >   w7  U   �7  .  8  �   ;;  �   �;  v   �<  �   $=  ?   >  �   N>  M   �>  @   @?  8   �?  W   �?  5   @  9   H@  R   �@  E   �@  A   A  X   ]A  O   �A     B  W   B     oB  >   vB     �B  +   �B  �  �B  s   �E  h   HF  j   �F  Y   G  1   vG            $   3          "      	   (   9   ?   @   4       8   0       /   -                      #               
         +      ;            <      *            %   &   A   >   )                       7       1                        =                              ,           .   5   '   6         :               !   2    
The $IM_CONFIG_XINPUTRC_TYPE is modified by im-config.

Restart the X session to activate the new $IM_CONFIG_XINPUTRC_TYPE.
$IM_CONFIG_RTFM $IM_CONFIG_ID
(c) Osamu Aoki <osamu@debian.org>, GPL-2+
See im-config(8), /usr/share/doc/im-config/README.Debian.gz. $IM_CONFIG_MSG
$IM_CONFIG_MSGA $IM_CONFIG_MSG
$IM_CONFIG_MSGA
  Available input methods:$IM_CONFIG_AVAIL
Unless you really need them all, please make sure to install only one input method tool. $IM_CONFIG_MSG
Automatic configuration selects: $IM_CONFIG_AUTOMATIC
$IM_CONFIG_AUTOMATIC_LONG
$IM_CONFIG_RTFM $IM_CONFIG_MSG
In order to enter non-ASCII native characters, you must install one set of input method tools:
 * ibus and its assocoated packages (recommended)
   * multilingual support
   * GUI configuration
 * fcitx and its assocoated packages
   * multilingual support with focus on Chinese
   * GUI configuration
 * uim and its assocoated packages
   * multilingual support
   * manual configuration with the Scheme code
   * text terminal support even under non-X environments
 * any set of packages which depend on im-config
$IM_CONFIG_MSGA $IM_CONFIG_MSG
Manual configuration selects: $IM_CONFIG_ACTIVE
$IM_CONFIG_ACTIVE_LONG
$IM_CONFIG_RTFM $IM_CONFIG_RTFM
See im-config(8) and /usr/share/doc/im-config/README.Debian.gz for more. *** This is merely a simulated run and no changes are made. ***

$IM_CONFIG_MSG Bogus Configuration Custom Configuration Custom configuration is created by the user or administrator using editor. Do you explicitly select the ${IM_CONFIG_XINPUTRC_TYPE}?

 * Select NO, if you do not wish to update it. (recommended)
 * Select YES, if you wish to update it. E: $IM_CONFIG_NAME is bogus configuration for $IM_CONFIG_XINPUTRC. Doing nothing. E: Configuration for $IM_CONFIG_NAME not found at $IM_CONFIG_CODE. E: Configuration in $IM_CONFIG_XINPUTRC is manually managed. Doing nothing. E: Script for $IM_CONFIG_NAME not found at $IM_CONFIG_CODE. E: X server must be available. E: whiptail must be installed. E: zenity must be installed. Explicit selection is not required to enable the automatic configuration if the active one is default/auto/cjkv/missing. Flexible Input Method Framework (fcitx)
 * Required for all: fcitx
 * Language specific input conversion support:
   * Simplified Chinese: fcitx-pinyin or fcitx-sunpinyin or fcitx-googlepinyin
   * Generic keyboard translation table: fcitx-table* packages
 * Application platform support:
   * GNOME/GTK+: fcitx-frontend-gtk2 and fcitx-frontend-gtk3 (both)
   * KDE/Qt4: fcitx-frontend-qt4 Hangul (Korean) input method
 * XIM: nabi
 * GNOME/GTK+: imhangul-gtk2 and imhangul-gtk3
 * KDE/Qt4: qimhangul-qt4
 * GUI companion: imhangul-status-applet I: Script for $IM_CONFIG_NAME started at $IM_CONFIG_CODE. If a daemon program for the previous configuration is re-started by the X session manager, you may need to kill it manually with kill(1). Input Method Configuration (im-config, ver. $IM_CONFIG_VERSION) Intelligent Input Bus (IBus)
 * Required for all: ibus
 * Language specific input conversion support:
   * Japanese: ibus-mozc (best) or ibus-anthy or ibus-skk
   * Korean: ibus-hangul
   * Simplified Chinese: ibus-pinyin or ibus-sunpinyin or ibus-googlepinyin
   * Traditional Chinese: ibus-chewing
   * Thai: ibus-table-thai
   * Vietnamese: ibus-unikey or ibus-table-viqr
   * X Keyboard emulation: ibus-xkbc
   * Generic keyboard translation table: ibus-m17n or ibus-table* packages
 * Application platform support:
   * GNOME/GTK+: ibus-gtk and ibus-gtk3 (both)
   * KDE/Qt: ibus-qt4
   * Clutter: ibus-clutter
   * EMACS: ibus-el Keeping the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC as missing. Keeping the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC unchanged as $IM_CONFIG_ACTIVE. Missing Missing configuration file. Non existing configuration name is specified. Removing the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC. Setting the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC to $IM_CONFIG_ACTIVE. Smart Common Input Method (SCIM)
 * Required for all: scim
 * Language specific input conversion support:
   * Japanese: scim-mozc (best) or scim-anthy or scim-skk
   * Korean: scim-hangul
   * Simplified Chinese: scim-pinyin or scim-sunpinyin
   * Traditional Chinese: scim-chewing
   * Thai: scim-thai
   * Vietnamese: scim-unikey
   * Generic keyboard translation table: scim-m17 or scim-table* packages
 * Application platform support:
   * GNOME/GTK+: scim-gtk-immodule
   * KDE/Qt4: scim-qt-immodule
   * Clutter: scim-clutter-immodule Thai input method with thai-libthai
 * GNOME/GTK+: gtk-im-libthai and gtk3-im-libthai
 * No XIM nor KDE/Qt4 support (FIXME) The $IM_CONFIG_XINPUTRC_TYPE has been manually modified.
Remove the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC manually to use im-config.
$IM_CONFIG_RTFM This activates the bare XIM with the X Keyboard Extension for all softwares. This does not set any IM from im-config.
This is the automatic configuration choice if no required IM packages are installed. X input method for Chinese with Sunpinyin
 * XIM: xsunpinyin X input method for Japanese with kinput2
 * XIM: one of kinput2-* packages
 * kanji conversion server: canna or wnn activate Chinese input method (gcin) activate Flexible Input Method Framework (fcitx) activate HIME Input Method Editor (hime) activate Hangul (Korean) input method activate Intelligent Input Bus (IBus) activate Smart Common Input Method (SCIM) activate Thai input method with thai-libthai activate XIM for Chinese with Sunpinyin activate XIM for Japanese with kinput2 activate the bare XIM with the X Keyboard Extension activate universal input method (uim) description do not set any IM from im-config name remove IM $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC select system configuration universal input method (uim)
 * Required for all: uim
 * Language specific input conversion support:
   * Japanese: uim-mozc (best) or uim-anthy or uim-skk
   * Korean: uim-byeoru
   * Simplified Chinese: uim-pinyin
   * Traditional Chinese: uim-chewing
   * Vietnamese: uim-viqr
   * General-purpose M17n: uim-m17nlib
 * Application platform support:
   * XIM: uim-xim
   * GNOME/GTK+: uim-gtk2.0 and uim-gtk3 (both)
   * KDE/Qt4: uim-qt
   * EMACS: uim-el use $IM_CONFIG_DEFAULT_MODE mode (bogus content in $IM_CONFIG_DEFAULT) use $IM_CONFIG_DEFAULT_MODE mode (missing $IM_CONFIG_DEFAULT ) use $IM_CONFIG_DEFAULT_MODE mode set by $IM_CONFIG_DEFAULT use auto mode only under CJKV user configuration Project-Id-Version: im-config
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-05-26 23:27+0000
PO-Revision-Date: 2016-06-02 18:45+0000
Last-Translator: Данил Тютюник <den.tyutyunik@ya.ru>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:56+0000
X-Generator: Launchpad (build 18115)
Language: ru
 
$IM_CONFIG_XINPUTRC_TYPE изменён программой im-config.

Перезапустите X-сессию для применения нового $IM_CONFIG_XINPUTRC_TYPE.
$IM_CONFIG_RTFM $IM_CONFIG_ID
(c) Osamu Aoki <osamu@debian.org>, GPL-2+
См. im-config(8), /usr/share/doc/im-config/README.Debian.gz. $IM_CONFIG_MSG
$IM_CONFIG_MSGA $IM_CONFIG_MSG
$IM_CONFIG_MSGA
  Доступные методы вводаs:$IM_CONFIG_AVAIL
Даже если есть необходимость во всех, убедитесь, что установлена только одна система метода ввода. $IM_CONFIG_MSG
Выбрано автоматической конфигурацией: $IM_CONFIG_AUTOMATIC
$IM_CONFIG_AUTOMATIC_LONG
$IM_CONFIG_RTFM $IM_CONFIG_MSG
Чтобы вводить не-ASCII символы родного языка, должна быть установлена одна из систем метода ввода:
 * ibus и его зависимые пакеты (рекомендовано)
   * многоязыковая поддержка
   * конфигурация с помощью GUI
 * fcitx и его зависимые пакеты
   * многоязыковая поддержка с упором на китайский язык
   * конфигурация с помощью GUI
 * uim и его зависимые пакеты
   * многоязыковая поддержка
   * ручная конфигурация на языке Scheme
   * поддержка текстового терминала, в том числе в не-X окружениях
 * любой набор пакетов, зависящих от im-config
$IM_CONFIG_MSGA $IM_CONFIG_MSG
Выбрано ручной конфигурацией: $IM_CONFIG_ACTIVE
$IM_CONFIG_ACTIVE_LONG
$IM_CONFIG_RTFM $IM_CONFIG_RTFM
Подробности см. в im-config(8) и в /usr/share/doc/im-config/README.Debian.gz. *** Это всего лишь тестовый режим, и никакие изменения не внесены. ***

$IM_CONFIG_MSG Фиктивная конфигурация Пользовательская конфигурация Пользовательская конфигурация создаётся с помощью редактора пользователем или администратором. Указать явно тип ${IM_CONFIG_XINPUTRC_TYPE}?

 * Выберите NO, если не требуется обновить тип. (рекомендовано)
 * Выберите YES, если нужно его обновить. E: $IM_CONFIG_NAME — это фиктивная конфигурация для $IM_CONFIG_XINPUTRC. Бездействие. E: Конфигурация для $IM_CONFIG_NAME не найдена в $IM_CONFIG_CODE. E: Конфигурация в $IM_CONFIG_XINPUTRC управляется вручную. Бездействие. E: Сценарий для $IM_CONFIG_NAME не найден в $IM_CONFIG_CODE. E: должен быть доступен X-сервер. E: должен быть установлен whiptail. E: должен быть установлен zenity. Явный выбор не требуется для включения автоматической конфигурации, если одно из default/auto/cjkv/missing активно. Flexible Input Method Framework (fcitx)
 * Обязательные требования: fcitx
 * Поддержка преобразования при вводе для конкретных языков:
   * Китайский упрощённый: fcitx-pinyin, или fcitx-sunpinyin, или fcitx-googlepinyin
   * Общая таблица перевода клавиатуры: пакеты fcitx-table*
 * Поддержка прикладных платформ:
   * GNOME/GTK+: fcitx-frontend-gtk2 и fcitx-frontend-gtk3 (оба)
   * KDE/Qt4: fcitx-frontend-qt4 Метод ввода с хангыль (корейского)
 * XIM: nabi
 * GNOME/GTK+: imhangul-gtk2 и imhangul-gtk3
 * KDE/Qt4: qimhangul-qt4
 * Дополнение к GUI: imhangul-status-applet I: Скрипт для $IM_CONFIG_NAME начался в $IM_CONFIG_CODE. Если демон, использующий предыдущую конфигурацию, перезапущен менеджером X-сессии, необходимо завершить его процесс с помощью kill(1). Конфигурация метода ввода (im-config, версия $IM_CONFIG_VERSION) Intelligent Input Bus (IBus)
 * Обязательные требования: ibus
 * Поддержка преобразования при вводе для конкретных языков:
   * Японский: ibus-mozc (наилучший), или ibus-anthy, или ibus-skk
   * Корейский: ibus-hangul
   * Китайский упрощённый: ibus-pinyin, или ibus-sunpinyin, или ibus-googlepinyin
   * Китайский традиционный: ibus-chewing
   * Тайский: ibus-table-thai
   * Вьетнамский: ibus-unikey или ibus-table-viqr
   * Эмуляция X Keyboard: ibus-xkbc
   * Общая таблица перевода клавиатуры: ibus-m17n или пакеты ibus-table*
 * Поддержка прикладных платформ:
   * GNOME/GTK+: ibus-gtk и ibus-gtk3 (оба)
   * KDE/Qt: ibus-qt4
   * Clutter: ibus-clutter
   * EMACS: ibus-el $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC сохранены как отсутствующие. $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC оставлены как $IM_CONFIG_ACTIVE без изменений. Отсутствует Отсутствует файл конфигурации. Указано имя несуществующей конфигурации. Удаление $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC. Установка $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC в $IM_CONFIG_ACTIVE. Smart Common Input Method (SCIM)
 * Обязательные требования: scim
 * Поддержка преобразования при вводе для конкретных языков:
   * Японский: scim-mozc (наилучший), или scim-anthy, или scim-skk
   * Корейский: scim-hangul
   * Китайский упрощённый: scim-pinyin или scim-sunpinyin
   * Китайский традиционный: scim-chewing
   * Тайский: scim-thai
   * Вьетнамский: scim-unikey
   * Общая таблица перевода клавиатуры: scim-m17 или пакеты scim-table*
 * Поддержка прикладных платформ:
   * GNOME/GTK+: scim-gtk-immodule
   * KDE/Qt4: scim-qt-immodule
   * Clutter: scim-clutter-immodule Метод ввода с тайского с thai-libthai
 * GNOME/GTK+: gtk-im-libthai и gtk3-im-libthai
 * Не поддерживает ни XIM, ни KDE/Qt4 (FIXME) $IM_CONFIG_XINPUTRC_TYPE был изменён вручную.
Для использования im-config удалите вручную $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC.
$IM_CONFIG_RTFM Активирует минимальный XIM с расширением X Keyboard для всех программ. Никакой метод ввода из im-config не устанавливается.
Выбирает автоматическую конфигурацию, если не установлены требуемые IM-пакеты. XIM для китайского с Sunpinyin
 * XIM: xsunpinyin XIM для японского с kinput2
 * XIM: один из пакетов kinput2-*
 * Сервер преобразования кандзи: canna или wnn активировать метод ввода с китайского (gcin) активировать Flexible Input Method Framework (fcitx) активировать HIME Input Method Editor (hime) активировать метод ввода с хангыль (корейского) активировать Intelligent Input Bus (IBus) активировать Smart Common Input Method (SCIM) активировать метод ввода с тайского с thai-libthai активировать XIM для китайского с Sunpinyin активировать XIM для японского с kinput2 активировать минимальный XIM с расширением X Keyboard активировать универсальный метод ввода (uim) описание не устанавливать никакого метода ввода из im-config имя удалить IM $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC выбрать системная конфигурация Универсальный метод ввода (uim)
 * Обязательные требования: uim
 * Поддержка преобразования при вводе для конкретных языков:
   * Японский: uim-mozc (наилучший), или uim-anthy, или uim-skk
   * Корейский: uim-byeoru
   * Китайский упрощённый: uim-pinyin
   * Китайский традиционный: uim-chewing
   * Вьетнамский: uim-viqr
   * Таблицы общего назначения M17n: uim-m17nlib
 * Поддержка прикладных платформ:
   * XIM: uim-xim
   * GNOME/GTK+: uim-gtk2.0 и uim-gtk3 (оба)
   * KDE/Qt4: uim-qt
   * EMACS: uim-el использовать режим $IM_CONFIG_DEFAULT_MODE (фиктивные данные в $IM_CONFIG_DEFAULT) использовать режим $IM_CONFIG_DEFAULT_MODE (отсутствует $IM_CONFIG_DEFAULT ) использовать режим $IM_CONFIG_DEFAULT_MODE, установленный $IM_CONFIG_DEFAULT использовать автоматический режим только при CJKV конфигурация пользователя 