��   !  0   8	  �  @     T     \  `      �  �   �     `     u     |  .   �  .   �  %   �          $  0   8     i  "   �     �     �  0   �  !   �          -  /   J     z     �     �     �  d   �     I     a     x  v   �  >     +   M  -   y     �     �  ,   �       %   %  ,   K  -   x      �  (   �     �               +     F     X     t     �     �     �     �  "   �  4   �  I   (   /   r   /   �      �   .   �      !  J   ;!  3   �!  E   �!      "  7   "  �   W"  �   �"  G   d#     �#  "   �#  
   �#  '   �#  (   "$     K$     c$     k$     �$     �$     �$     �$     �$     %      %  i   :%  2   �%     �%  '   �%  )   &  $   G&  `   l&  7   �&  (   '  (   .'  >   W'     �'     �'  T   �'      (  3   0(      d(     �(     �(     �(  \   �(  '   �(     )     2)  E   K)  5   �)  C   �)  +  *     7+     G+  ?   d+  B   �+  B   �+     *,     E,     X,     o,     �,     �,  &   �,  2   �,     -     -  s   0-     �-     �-     �-     �-     �-     �-     .  ;   .  &   O.  8   v.  9   �.  :   �.  /   $/  0   T/  1   �/     �/     �/     �/     �/  @   �/  D   =0  #   �0  &   �0  F   �0  J   1  6   _1     �1  ,   �1  '   �1  !   2     '2     >2  2   R2  ;   �2     �2     �2     �2     �2     �2     3  ,   3      J3  $   k3  0   �3  3   �3  I   �3     ?4  +   _4  &   �4  R   �4  ,   5  6   25  q   i5     �5  /   �5     *6  Z   =6  6   �6     �6     �6     �6     7  0    7     Q7     X7  !   q7  +   �7  �   �7     J8  "   _8  v   �8  6   �8  .   09     _9  ;   v9  3   �9  /   �9  +   :  '   B:  #   j:     �:     �:     �:  ]   �:  	   8;     B;     T;     l;     �;  "   �;     �;     �;  B   �;  +   <     H<     ]<     x<     �<     �<     �<     �<     �<     �<  ,   =  4   4=     i=     |=     �=     �=     �=     �=  -   �=  	   >  !    >  	   B>  I   L>     �>     �>     �>  $   �>     �>     	?  5   ?  e   L?     �?     �?     �?     �?     @     !@     .@     ?@  	   Q@     [@     t@  	   �@     �@     �@     �@     �@     �@  
   �@  
   �@  
    A     A  %   A     ?A     OA     jA     A     �A     �A     �A     �A     �A     �A  *   �A  !   B     3B  .   LB  D   {B     �B    �B  V  �D  O   'F  	   wF     �F  U   �F  Q   �F  ;   @G     |G     �G  6   �G  %   �G  Q   H  +   eH  >   �H  h   �H  I   9I     �I  9   �I  L   �I  P   %J  E   vJ  %   �J  P   �J  �   3K  3   �K  8   L  C   HL  �   �L  �   jM  W   �M  S   FN  3   �N  5   �N  J   O  3   OO  B   �O  J   �O  ]   P  8   oP  M   �P     �P     Q     "Q  I   7Q     �Q  )   �Q  ,   �Q  ;   �Q  5   0R     fR     hR  Z   lR  d   �R  �   ,S  D   �S  D   T  K   JT  T   �T  A   �T  �   -U  D   �U  �   V  @   �V  ~   �V  3  PW  5  �X  r   �Y  F   -Z  M   tZ     �Z  =   �Z  >   [  >   W[     �[  _   �[  %   \  )   +\  1   U\  P   �\  =   �\  7   ]  7   N]  �   �]  a   v^  =   �^  X   _  E   o_  A   �_  �   �_  u   �`  >   Fa  R   �a  _   �a  :   8b  6   sb  �   �b  !   ;c  Z   ]c  H   �c     d      d     )d  �   Dd  H   �d  /   5e  3   ee  �   �e  |   f  �   �f     g  &   1i     Xi  u   xi  �   �i  e   oj  D   �j  ,   k  0   Gk  <   xk  6   �k  )   �k  A   l  z   Xl  )   �l  ;   �l  /  9m  $   in     �n     �n  +   �n  -   �n  P   o     _o  �   po  L   �o  ]   Hp  ^   �p  _   q  V   eq  W   �q  X   r  2   mr     �r     �r     �r  �   �r  s   �s  J   �s  M   Jt  �   �t  �   -u  f   �u  4   =v  V   rv  E   �v  9   w  R   Iw  =   �w  ]   �w  �   8x  &   �x     �x     �x     �x  N    y  H   Oy  d   �y  T   �y  U   Rz  n   �z  [   {  �   s{  B   (|  O   k|  B   �|  w   �|  P   v}  _   �}  �   '~  ?   �~  Z   7  ,   �  �   �  �   d�  /   ��  ,   )�  $   V�  6   {�  d   ��     �  R   3�  M   ��  {   Ԃ  8  P�  (   ��  T   ��  �   �  �   ��  j   )�     ��  B   ��  4   ��  /   %�  ,   U�  (   ��  $   ��  !   Ї     �     �  �    �     �  ,   �  7   H�  %   ��     ��  H   ��  :   �  8   ,�  �   e�  i   �  ;   k�  7   ��  %   ߋ  '   �  S   -�  4   ��  1   ��  V   �  8   ?�  l   x�  x   �  9   ^�  .   ��  .   ǎ     ��  1   �  +   H�  X   t�     ͏  R   �     <�  o   T�     Đ  P   Ȑ  $   �  I   >�  5   ��     ��  _   ϑ  �   /�  6   ג  0   �  2   ?�     r�  S   ��  )   �     �     .�     L�  ;   j�     ��     ��     ϔ  6   �  4   $�  .   Y�     ��     ��     ��     ��  #   ͕  F   �  ,   8�  B   e�  8   ��  ,   �  .   �  $   =�     b�     h�  .   o�     ��  T   ��  K   ��  6   C�  z   z�  �   ��     ��     C   �                   �   }   m   �   �       !      �   �     �   �   a          �          �       7         �         (   �   �   �   U   3         �       �   ?       {   g   J   E   �   D       %   �   <   �   �       n   �   �       t   .   �       �           4   r                  -   �     �       G   �        W       ;   \   P         /   c   #   �   �   v   �       �                       �   �              o               	  �   �     �   �   X   A       �   �       �   �           �   �   0   �   �   �     S   �   �         *       �   �       �       �   �   M   �   `   �   y      +   e              �   �   p           L            �   V   �   )   �   �                   �   �     �   I      �   k   �   �           �   �       h   �      �   �   >   [       ]   l   �       �       B         �           !            _   '   R     �      Y   �   �   �   �   �   z           �     �   �       b      �          �   9   �       �           �   u     �          K           �          5           s      ,      O   �   Z   �       �   q       �       �   �       N   
  T   �   �                   �   �       �                 �       i   �   Q   �   �   "   
   �       �   �   �   �   :       �   �              H   d   ~                �   �          �   �   @     �   �         �   ^   F       �      w                           �     �     2   f                 6   x   �          �   |   8     �   �   &   �   $   =   	   1   j      ��  d  x  Ù         C   �����  #       p          ���� 
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

 
Report bugs to: %s
   or:   [OPTION...] %.*s: ARGP_HELP_FMT parameter must be positive %.*s: ARGP_HELP_FMT parameter requires a value %.*s: Unknown ARGP_HELP_FMT parameter %lu block
 %lu blocks
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s is meaningless with %s %s is not a character special file %s is not a directory %s linked to %s %s not created: newer or same age version exists %s not dumped: not a regular file %s: Cannot %s %s: Cannot change mode to %s %s: Cannot change ownership to uid %lu, gid %lu %s: Cannot create symlink to %s %s: Cannot hard link to %s %s: Cannot seek to %s %s: Cannot symlink to %s %s: Read error at byte %s, while reading %lu byte %s: Read error at byte %s, while reading %lu bytes %s: Too many arguments
 %s: Warning: Cannot %s %s: Warning: Cannot seek to %s %s: Warning: Read error at byte %s, while reading %lu byte %s: Warning: Read error at byte %s, while reading %lu bytes %s: Wrote only %lu of %lu byte %s: Wrote only %lu of %lu bytes %s: checksum error (0x%lx, should be 0x%lx) %s: field width not sufficient for storing %s %s: file name too long %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous
 %s: option '%s' requires an argument
 %s: option '--%s' doesn't allow an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option requires an argument -- '%c'
 %s: rmtclose failed %s: rmtioctl failed %s: rmtopen failed %s: symbolic link too long %s: truncating %s %s: truncating inode number %s: unknown file type %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' (C) (PROGRAM ERROR) No version known!? (PROGRAM ERROR) Option should have been recognized!? --append is used but no archive file name is given (use -F or -O options) --no-preserve-owner cannot be used with --owner --owner cannot be used with --no-preserve-owner --stat requires file names -F can be used only with --create or --extract -T reads null-terminated names A list of filenames is terminated by a null character instead of a newline ARGP_HELP_FMT: %s value is less than or equal to %s Append SIZE bytes to FILE. SIZE is given by previous --length option. Append to an existing archive. Archive file is local, even if its name contains colons Archive filename to use instead of standard input. Optional USER and HOST specify the user and host names in case of a remote archive Archive filename to use instead of standard output. Optional USER and HOST specify the user and host names in case of a remote archive Archive format is not specified in copy-pass mode (use --format option) Archive format multiply defined Archive value %.*s is out of range BLOCK-SIZE Both -I and -F are used in copy-in mode Both -O and -F are used in copy-out mode Byte count out of range COMMAND Cannot execute remote shell Cannot open %s Command dumped core
 Command exited successfully
 Command failed with status %d
 Command stopped on signal %d
 Command terminated
 Command terminated on signal %d
 Control warning display. Currently FLAG is one of 'none', 'truncate', 'all'. Multiple options accumulate. Create all files relative to the current directory Create file of the given SIZE Create leading directories where needed Create the archive (run in copy-out mode) Creating intermediate directory `%s' Dereference  symbolic  links  (copy  the files that they point to instead of copying the links). Display executed checkpoints and exit status of COMMAND Do not change the ownership of the files Do not print the number of blocks copied Do not strip file system prefix components from the file names Enable debugging info Error parsing number near `%s' Execute ARGS. Useful with --checkpoint and one of --cut, --append, --touch, --unlink Execute COMMAND Extract files from an archive (run in copy-in mode) Extract files to standard output FILE FLAG FORMAT File %s shrunk by %s byte, padding with zeros File %s shrunk by %s bytes, padding with zeros File %s was modified while being copied File creation options: File statistics options: Fill the file with the given PATTERN. PATTERN is 'default' or 'zeros' Found end of tape.  Load next tape and press RETURN.  Found end of tape.  To continue, type device/file name when ready.
 GNU `cpio' copies files to and from archives

Examples:
  # Copy files named in name-list to the archive
  cpio -o < name-list [> archive]
  # Extract files from the archive
  cpio -i [< archive]
  # Copy files named in name-list to destination-directory
  cpio -p destination-directory < name-list
 Garbage command Garbage in ARGP_HELP_FMT: %s General help using GNU software: <http://www.gnu.org/gethelp/>
 Generate sparse file. Rest of the command line gives the file map. In the verbose table of contents listing, show numeric UID and GID Interactively rename files Invalid byte count Invalid operation code Invalid seek direction Invalid seek offset Invalid size: %s Invalid value for --warning option: %s Link files instead of copying them, when  possible Main operation mode: Malformed number %.*s Mandatory or optional arguments to long options are also mandatory or optional for any corresponding short options. Mode already defined NAME NUMBER Negative size: %s Not enough arguments Number out of allowed range: %s OFFSET Only copy files that do not match any of the given patterns Operation modifiers valid in any mode: Operation modifiers valid in copy-in and copy-out modes: Operation modifiers valid in copy-in and copy-pass modes: Operation modifiers valid in copy-out and copy-pass modes: Operation modifiers valid only in copy-in mode: Operation modifiers valid only in copy-out mode: Operation modifiers valid only in copy-pass mode: Operation not supported PATTERN Packaged by %s
 Packaged by %s (%s)
 Perform given action (see below) upon reaching checkpoint NUMBER Print STRING when the end of a volume of the backup media is reached Print a "." for each file processed Print a table of contents of the input Print contents of struct stat for each given file. Default FORMAT is:  Read additional patterns specifying filenames to extract or list from FILE Read error at byte %lld in file %s, padding with zeros Read file names from FILE Removing leading `%s' from hard link targets Removing leading `%s' from member names Replace all files unconditionally Report %s bugs to: %s
 Report bugs to %s.
 Reset the access times of files after reading them Retain previous file modification times when creating files Run in copy-pass mode SECS SIZE STRING Seek direction out of range Seek offset out of range Seek to the given offset before writing data Set date for next --touch option Set the I/O block size to 5120 bytes Set the I/O block size to BLOCK-SIZE * 512 bytes Set the I/O block size to the given NUMBER of bytes Set the ownership of all files created to the specified USER and/or GROUP Size of a block for sparse file Substituting `.' for empty hard link target Substituting `.' for empty member name Swap both halfwords of words and bytes of halfwords in the data. Equivalent to -sS Swap the bytes of each halfword in the files Swap the halfwords of each word (4 bytes) in the files Synchronous execution actions. These are executed when checkpoint number given by --checkpoint option is reached. Synchronous execution options: To continue, type device/file name when ready.
 Too many arguments Truncate FILE to the size specified by previous --length option (or 0, if it is not given) Try `%s --help' or `%s --usage' for more information.
 Unexpected arguments Unknown date format Unknown field `%s' Unknown system error Update the access and modification times of FILE Usage: Use given archive FORMAT Use remote COMMAND instead of rsh Use the old portable (ASCII) archive format Use this FILE-NAME instead of standard input or output. Optional USER and HOST specify the user and host names in case of a remote archive Valid arguments are: Verbosely list the files processed When reading a CRC format archive, only verify the CRC's of each file in the archive, don't actually extract the files Write files with large blocks of zeros as sparse files Write to file NAME, instead of standard output Written by %s and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, %s, and others.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
and %s.
 Written by %s, %s, %s,
%s, %s, %s, and %s.
 Written by %s, %s, %s,
%s, %s, and %s.
 Written by %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
and %s.
 Written by %s, %s, and %s.
 Written by %s.
 You must specify one of -oipt options.
Try `%s --help' or `%s --usage' for more information.
 [ARGS...] [USER][:.][GROUP] [[USER@]HOST:]FILE-NAME [destination-directory] ` `%s' exists but is not a directory ambiguous argument %s for %s blank line ignored cannot generate sparse files on standard output, use --file option cannot get the login group of a numeric UID cannot link %s to %s cannot make directory `%s' cannot open %s cannot open `%s' cannot read checksum for %s cannot remove current %s cannot seek cannot seek on output cannot set time on `%s' cannot swap bytes of %s: odd number of bytes cannot swap halfwords of %s: odd number of halfwords cannot unlink `%s' device major number device minor number device number error closing archive exec/tcp: Service not available failed to return to initial working directory file mode file name contains null character file size genfile manipulates data files for GNU paxutils test suite.
OPTIONS are:
 gid give a short usage message give this help list hang for SECS seconds (default 3600) incorrect mask (near `%s') inode number internal error: tape descriptor changed from %d to %d invalid archive format `%s'; valid formats are:
crc newc odc bin ustar tar (all-caps also recognized) invalid argument %s for %s invalid block size invalid count value invalid group invalid header: checksum error invalid user memory exhausted modification time name size no tape device specified number of links operation operation [count] premature end of archive premature end of file print program version rdev rdev major rdev minor read error rename %s ->  requested file length %lu, actual %lu set debug level set debug output file name set the program name standard input is closed standard output is closed stat(%s) failed stdin stdout too many arguments uid unable to record current working directory use remote COMMAND instead of rsh virtual memory exhausted warning: archive header has reverse byte-order warning: skipped %ld byte of junk warning: skipped %ld bytes of junk write error Project-Id-Version: cpio 2.9.91
Report-Msgid-Bugs-To: bug-cpio@gnu.org
POT-Creation-Date: 2010-03-10 15:04+0200
PO-Revision-Date: 2016-06-02 04:23+0000
Last-Translator: Sly_tom_cat <Unknown>
Language-Team: Russian <gnu@mx.ru>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 16:09+0000
X-Generator: Launchpad (build 18115)
 
Лицензия GPLv3+: GNU GPL версии 3 или более поздней <http://gnu.org/licenses/gpl.html>.
Это свободное ПО: вы можете свободно изменять и распространять его.
Нет НИКАКИХ ГАРАНТИЙ до степени, разрешённой законом.

 
Отчёты об ошибках отправляйте по адресу: %s
   или   [ПАРАМЕТР...] %.*s: параметр ARGP_HELP_FMT должен быть положительным %.*s: для параметра ARGP_HELP_FMT требуется значение %.*s: неизвестный параметр ARGP_HELP_FMT %lu блок
 %lu блоков
 %s веб-сайт: <%s>
 %s веб-сайт: <http://www.gnu.org/software/%s/>
 %s не имеет смысла с %s %s не является специальным символьным файлом %s не является каталогом создана ссылка %s, указывающая на %s %s не создан: существует версия новее или того же возраста %s не сброшен: не является обычным файлом %s: не удаётся %s %s: не удаётся сменить режим на %s %s: не удаётся сменить владельца (uid %lu, gid %lu) %s: не удаётся создать символьную ссылку на %s %s: не удаётся создать жёсткую ссылку %s %s: не удаётся найти %s %s: Не удаётся создать символьную ссылку на %s %s: ошибка чтения в позиции %s при чтении %lu байт %s: ошибка чтения в позиции %s при чтении %lu байт %s: слишком много аргументов
 %s: предупреждение: не удаётся %s %s: предупреждение: не удаётся найти %s %s: предупреждение: ошибка чтения в байте %s при чтении %lu байта %s: предупреждение: ошибка чтения в байте %s при чтении %lu байт %s: записано только %lu байт из %lu байта %s: записано только %lu байт из %lu байт %s: ошибка контрольной суммы (0x%lx, должно быть 0x%lx) %s: недостаточная ширина поля для сохранения %s %s: слишком длинное имя файла %s: недопустимый параметр — %c
 %s: параметр «%c%s» не может иметь аргумент
 %s: параметр '%s' неоднозначен
 %s: параметру «%s» требуется аргумент
 %s: параметр «--%s» не может иметь аргумент
 %s: с параметром «-W %s» нельзя использовать аргумент
 %s: параметр «-W %s» неоднозначен
 %s: для параметра требуется аргумент — «%c»
 %s: сбой rmtclose %s: сбой rmtioctl %s: сбой rmtopen %s: символическая ссылка слишком длинная %s: обрезается %s %s: обрезается номер inode %s: неизвестный тип файла %s: нераспознанный параметр «%c%s»
 %s: неизвестный параметр «--%s»
 ' (C) (ОШИБКА ПРОГРАММЫ) Неизвестная версия программы?! (ОШИБКА ПРОГРАММЫ) Параметр должен был быть распознан?! используется --append, но не указано имя файла архива (используйте параметры -F или -O) --no-preserve-owner нельзя использовать с --owner --owner нельзя использовать с --no-preserve-owner для параметра --stat требуются имена файлов -F может использоваться только с --create или --extract -T читает имена с нулевым окончанием Список имён файлов, оканчивающихся пустым символом вместо символа новой строки ARGP_HELP_FMT: значение %s меньше или равно %s Добавить к ФАЙЛУ РАЗМЕР байтов РАЗМЕР задаётся предыдущим параметром --length. Добавление к существующему архиву. Файл архива является локальным, даже если его имя содержит двоеточие Имя файла архива для использования вместо стандартного ввода При работе с удалённым архивом необязательные ПОЛЬЗОВАТЕЛЬ и ХОСТ определяют имена пользователя и хоста Имя файла архива для использования вместо стандартного вывода При работе с удалённым архивом необязательные ПОЛЬЗОВАТЕЛЬ и ХОСТ определяют имена пользователя и хоста Не указан формат архива в режиме copy-pass (используйте опцию --format) Формат архива определён несколько раз Значение архива %.*s за пределами диапазона РАЗМЕР-БЛОКА И -I, и -F используются в режиме copy-in И -O, и -F используются в режиме copy-out Число байт за пределами диапазона КОМАНДА Не удаётся запустить удалённый командный процессор Не удается открыть %s Команда сбросила дамп
 Команда успешно выполнена
 Команда завершилась неудачей со статусом %d
 Команда остановлена по сигналу %d
 Выполнение команды завершено
 Команда прервана по сигналу %d
 Управление выводом предупреждений. В настоящее время ФЛАГом может быть 'none', 'truncate' или 'all'.  Многократные параметры аккумулируются. Создание всех файлов относительно текущего каталога Создать файла указанного РАЗМЕРА Создание при необходимости начальных каталогов Создание архива (работа в режиме copy-out) Создаётся промежуточный каталог `%s' Разыменовывание символических ссылок (копирование файлов, на которые они указывают, вместо копирования самих ссылок) Вывести выполненные контрольные точки и статусы выхода КОМАНДЫ Не изменять права владения файлов Не выводить количество скопированных блоков Не удалять из имён файлов префиксы файловой системы Включить отладочную информацию Ошибка анализа числа возле `%s' Выполнение АРГУМЕНТОВ. Полезно с параметром --checkpoint и одним из --cut, --append или --touch Выполнить КОМАНДУ Извлечение файлов из архива (работа в режиме copy-in) Извлечение файлов на стандартный вывод Работа с файлами ФЛАГ Форматировать Файл %s укорочен на %s байт, заполняется нулями Файл %s укорочен на %s байт с заполнением нулями Файл %s был изменён во время копирования Параметры создания файла: Параметры статистики файла: Заполнить файл по указанному ШАБЛОНУ. ШАБЛОНОМ может быть 'default' или 'zeros' Обнаружен конец ленты.  Заправьте новую ленту и нажмите клавишу ENTER.  Обнаружен конец ленты.  Для подолжения введите имя устройства или файла.
 GNU `cpio' копирует файлы в архивы и из них

Примеры:
  # Копирование файлов, перечисленных в списке_имён, в архив
  cpio -o < список_имён [> архив]
  # Извлечение файлов из архива
  cpio -i [< архив]
  # Копирование файлов, перечисленных в списке_имён, в каталог_назначения
  cpio -p каталог_назначения < список_имён
 Команда сбора мусора Мусор в ARGP_HELP_FMT: %s Общая  информация по использованию программ GNU: <http://www.gnu.org/gethelp/>
 Создать разрежённый файл. Остальная часть команды задаёт карту файла. Вывод числового UID и GID в таблице с подробным содержимым Интерактивное переименование файлов Недопустимое число байт Недопустимый код операции Недопустимое направление поиска Недопустимое смещение поиска Недопустимый размер: %s Неверное значение для опции --warning: %s Создание ссылок на файлы вместо их копирования, когда это возможно Основной режим работы: Неверно сформированное число %.*s Обязательные или необязательные аргументы для длинных параметров также являются обязательными или необязательными для любых соответствующих коротких параметров. Режим уже определён НАЗВАНИЕ Количество Отрицательный размер: %s Недостаточно аргументов Число за пределами допустимого диапазона: %s СМЕЩЕНИЕ Копирование только тех файлов, которые не совпадают с указанными шаблонами Модификаторы, действующие в любом режиме: Модификаторы, действительные в режимах copy-in и copy-out: Модификаторы, действительные в режимах copy-in и copy-pass: Модификаторы, действительные в режимах copy-out и copy-pass: Модификаторы, действующие только в режиме copy-in: Модификаторы, действующие только в режиме copy-out: Модификаторы, действующие только в режиме copy-pass: Действие не поддерживается ШАБЛОН Упаковано %s
 Упаковано %s (%s)
 Выполнить указанное действие (см. ниже), пока не будет достигнута контрольная точка с НОМЕРОМ Вывод СТРОКИ, когда на резервном носителе достигнут конец тома Вывод "." для каждого обработанного файла Вывод таблицы с содержимым входных данных Вывести статистику структуры для каждого указанного файла. ФОРМАТ по умолчанию:  Чтение из ФАЙЛА дополнительных шаблонов, определяющих имена извлекаемых файлов, или списка Ошибка чтения в позиции %lld в файле %s, заполняется нулями Читать имена файлов из ФАЙЛА Из целей жёстких ссылок удаляются начальные `%s' Из имён членов удаляются начальные `%s' Безусловная замена всех файлов Отчёты об ошибках %s отправляйте по адресу <%s>
 Об ошибках сообщайте по адресу %s.
 Сброс времени доступа к файлам после их считывания Запоминание предыдущего времени изменения файлов при создании файлов Работа в режиме copy-pass SECS РАЗМЕР СТРОКА Направление поиска за пределами диапазона Смещение поиска за пределами диапазона Перемещение на заданное смещение перед записью данных Установить дату для следующего параметра --touch Установка размера блока ввода/вывода в 5120 байт Установка размера блока ввода/вывода в РАЗМЕР-БЛОКА * 512 байт Установка размера блока ввода/вывода в ЧИСЛО байт Назначение прав владения, определённых через ПОЛЬЗОВАТЕЛЬ и/или ГРУППА, для всех созданных файлов Размер блока для разрежённых файлов `.' заменяются на пустые цели жёстких ссылок `.' заменяются на пустые имена членов Менять местами полуслова и байты полуслов в данных. Эквивалент -sS Менять местами байты всех полуслов в файлах Менять местами полуслова всех слов (4 байта) в файлах Действия синхронного выполнения. Эти действия выполняются при достижении номера, указанного в параметре --checkpoint. Параметры синхронного выполнения: Для подолжения введите имя устройства или файла.
 Слишком много аргументо Усечь ФАЙЛ до размера, указанного предыдущим параметром --length (или 0, если размер не указан) Попробуйте указать `%s --help' или `%s --usage', чтобы получить дополнительную информацию.
 Непредвиденные аргументы Неизвестный формат даты Неизвестное поле `%s' Неизвестная системная ошибка Обновить время последнего обращения и изменения ФАЙЛА Использование: Использование для архива указанного ФОРМАТА Использование удалённой КОМАНДЫ вместо rsh Использование для архива старого машиннонезависимого формата (ASCII) Использование ИМЕНИ_ФАЙЛА вместо стандартного ввода или вывода. При работе с удалённым архивом необязательные ПОЛЬЗОВАТЕЛЬ и ХОСТ определяют имена пользователя и хоста Допустимые аргументы: Вывести подробный список обработанных файлов При чтении архива в формате CRC только проверять CRC-суммы всех файлов, а не извлекать их Запись файлов с большими блоками из нулей в качестве разрежённых файлов Писать в файл НАЗВАНИЕ вместо вывода на стандартный вывод Авторы: %s и %s.
 Авторы: %s, %s, %s,
%s, %s, %s, %s,
%s, %s и другие.
 Авторы: %s, %s, %s,
%s, %s, %s, %s,
%s и %s.
 Авторы: %s, %s, %s,
%s, %s, %s, %s
и%s.
 Авторы: %s, %s, %s,
%s, %s, %s и %s.
 Авторы: %s, %s, %s,
%s, %s и %s.
 Авторы: %s, %s, %s,
%s и %s.
 Авторы: %s, %s, %s
 и %s.
 Авторы: %s, %s и %s.
 Автор: %s.
 Необходимо указать один из апарметров -oipt.
Попробуйте указать `%s --help' или `%s --usage',
чтобы получить дополнительную информацию.
 [АРГУМЕНТЫ...] [ПОЛЬЗОВАТЕЛЬ][:.][ГРУППА] [[ПОЛЬЗОВАТЕЛЬ@]ХОСТ:]ИМЯ_ФАЙЛА [каталог_назначения] ` `%s' существует, но не является каталогом неопределённый аргумент %s для %s пустая строка проигнорирована не удалось вывести разрежённые файлы на стандартный вывод, используйте параметр --file невозможно определить группу регистрации по числовому UID невозможно создать ссылку %s на %s невозможно создать каталог `%s' не удаётся открыть %s не удается открыть `%s' невозможно прочитать контрольную сумму для %s не удается удалить текущий %s не удаётся выполнить поиск позиционирование в выходном потоке невозможно не удаётся установить время `%s' невозможно поменять местами байты %s: нечетное число байтов невозможно поменять местами полуслова %s: нечетное число полуслов не удаётся разорвать связь с `%s' старшее число устройства младшее число устройства номер устройства ошибка при закрытии архива exec/tcp: служба недоступна не удалось вернуться в исходный рабочий каталог файловый режим имя файла не должно содержать пустые символы размер файла genfile обрабатывает файлы данных для пакета GNU paxutils.
ПАРАМЕТРЫ:
 gid вывести краткое сообщение об использовании вывести эту справку задержка на SECS секунд (по умолчанию 3600 с) неправильная маска (возле `%s') номер inode внутренняя ошибка: дескриптор ленты изменен с %d на %d недопустимый формат архива `%s'; допустимые форматы:
crc newc odc bin ustar tar (all-caps также распознаётся) недопустимый аргумент %s для %s недопустимый размер блока неверное значение счетчика неверная группа неверный заголовок: ошибка контрольной суммы неверный пользователь память исчерпана время изменения размер названия не указано ленточное устройство число ссылок операция операция [число] преждевременный конец архива Преждевременный конец файла вывести версию программы rdev старший rdev младший rdev Ошибка чтени переименование %s ->  запрошенная длина файла %lu, реальная %lu задайте уровень отладки задайте имя выходного файла отладки установить название программы стандартный ввод закрыт стандартный вывод закрыт ошибка команды stat(%s) stdin stdout слишком много аргументов uid не удаётся выполнить запись в рабочий каталог использовать удалённую КОМАНДУ вместо rsh виртуальная память исчерпана предупреждение: в заголовке архива принят обратный порядок байтов предупреждение: пропущен мусор объёмом %ld байт предупреждение: пропущен мусор объёмом %ld байт ошибка записи PRIuMAX File %s grew, % new byte not copied File %s grew, %<PRIuMAX> new bytes not copied Файл %s увеличился, % новых байт не скопировано Файл %s увеличился, не скопировано % новых байт 