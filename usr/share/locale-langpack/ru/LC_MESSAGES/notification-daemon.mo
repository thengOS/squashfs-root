��          |      �             !     9     R     h  (   ~     �     �     �     �       '       D  .   c  *   �  +   �  ,   �  W     B   n  #   �  3   �  7   	     A  K   X                      	                          
           Clear all notifications Closes the notification. Display notifications Enable debugging code Exceeded maximum number of notifications Invalid notification identifier Notification Daemon Notification body text. Notification summary text. Notifications Replace a currently running application Project-Id-Version: notification-daemon
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=notification-daemon&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-19 01:30+0200
PO-Revision-Date: 2016-06-02 04:21+0000
Last-Translator: Yuri Myasoedov <ymyasoedov@yandex.ru>
Language-Team: Русский <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:46+0000
X-Generator: Launchpad (build 18115)
Language: ru
 Очистить все уведомления Закрывает уведомление. Показывать уведомления Включить отладочный код Превышено максимальное количество уведомлений Неверный идентификатор уведомлений Служба уведомлений Основной текст уведомления. Заголовок текста уведомления. Уведомления Заменить текущее выполняемое приложение 