��    �      �  �   �      �  b   �     L  �   j       +   ,  1   X  .   �  #   �  ;   �  #     -   =  #   k  #   �  8   �  -   �  *        E     T     m     �  %   �     �     �  $   �  B     /   X  *   �  8   �  7   �  -   $  4   R  .   �  0   �  )   �  J     .   \  5   �  B   �  .     0   3  &   d  5   �  "   �  /   �  4     $   I     n     }  #   �     �     �     �  B   �  +     )   B     l  %   �      �     �  	   �     �     �     �     �     �  
   �     	                         $     (  
   +     6     J     N     S     V     Z  
   c     n     }       
   �     �     �     �     �  "   �     �     �     �     �          %  
   ;     F     c     |  $   �     �     �     �     �          *     A     W  ,   p  D   �     �  <         =  7   N  F   �  F   �  9     1   N     �     �     �     �  ,   �       '        D  3   Z     �     �     �     �     �     �  !   �          4     M     Y  @   o  �  �  �   I  V   �  l  2  .   �   E   �   P   !  8   e!  /   �!  `   �!  /   /"  D   _"  /   �"  /   �"  [   #  I   `#  N   �#     �#  (   $  %   <$     b$  G   z$      �$      �$  =   %  [   B%  B   �%  0   �%  S   &  |   f&  R   �&  T   6'  8   �'  G   �'  >   (  w   K(  8   �(  Q   �(  ~   N)  8   �)  R   *  ;   Y*  O   �*  9   �*  Q   +  T   q+  2   �+     �+     ,  B    ,     c,     w,     �,  �   �,  F   -  C   \-  =   �-  0   �-  +   .  
   ;.  /   F.     v.     }.     �.     �.  '   �.     �.     �.     �.     �.     �.     �.     /     	/     /  4   "/     W/     [/     `/     c/     j/  +   }/     �/     �/     �/  '   �/     �/     0     0     0  T   0  !   s0  &   �0  )   �0  .   �0  *   1  .   @1     o1  -   �1  7   �1  =   �1  C   %2  0   i2  !   �2  $   �2  !   �2  "   3  (   &3  4   O3  1   �3  O   �3  �   4  E   �4  �   �4     d5  c   x5  �   �5  �   t6  c   7  \   v7     �7  A   �7  A   58  9   w8  T   �8     9  C   9     Y9  k   y9  ?   �9  .   %:  
   T:     _:  +   o:  8   �:  O   �:  /   $;  <   T;     �;     �;  j   �;         *         V   8   �           /   ^   k   m   H           `       x       ,      A   i   O                  1           o   �   l   u   Y   &           <                  
   C                  7       �           D      +                        !   b       [   P   |   �   �   h   S   Z   e       5                     B       =   G   @       -   y   (      j   }   4       "   �       ;   $   R   g   %      K   a                     �   N       \   >      ?           W      d   F   2       '      E   M       r   	   U                    L   �   _   3              ]   X   f      )   �   p       s   w   z   t       v   �   {      T   I   �       c   #   q   �   J      Q   6   �      .          n   9   �      :   0   ~    
 Try '%s --%s <%s|%s|%s|%s|%s|%s>'
  or '%s --%s <%s|%s|%s|%s|%s|%s>'
 for additional help text.
 
For more details see ps(1).
 
The default priority is +4. (snice +4 ...)
Priority numbers range from +20 (slowest) to -20 (fastest).
Negative priority numbers are restricted to administrative users.
 
Usage:
 %s [options]
      --help     display this help and exit
      --si            use powers of 1000 not 1024
      --tera          show output in terabytes
   -A                   alias of -a
   -N, --names          print variable names without values
   -X                   alias of -a
   -a, --all            display all variables
   -d                   alias of -h
   -f                   alias of -p
   -n, --values         print only values of a variables
   -p, --load[=<file>]  read values from file
  %s [new priority] [options] <expression>
  %s [options]
  %s [options] <pattern>
  %s [options] <pid> [...]
  %s [options] [tty]
  %s [options] [variable[=value] ...]
  %s [options] command
  %s [options] pid...
  %s [signal] [options] <expression>
  -<sig>, --signal <sig>    signal to send (either number or name)
  -F, --pidfile <file>      read PIDs from file
  -b, --bytes         show output in bytes
  -c N, --count N     repeat printing N times, then exit
  -c, --count               count of matching processes
  -d, --delay <secs>  update delay in seconds
  -d, --delimiter <string>  specify output delimiter
  -g, --giga          show output in gigabytes
  -h, --human         show human-readable output
  -h, --no-header     do not print header
  -i, --ip-addr       display IP address instead of hostname (if possible)
  -k, --kilo          show output in kilobytes
  -l, --list-name           list PID and process name
  -l, --lohi          show detailed low and high memory statistics
  -m, --mega          show output in megabytes
  -n, --one-header       do not redisplay header
  -o, --old-style     old style output
  -s N, --seconds N   repeat printing every N seconds
  -s, --short         short format
  -t, --total         show total for RAM + swap
  -v, --version  output version information and exit
  -w, --wide             wide output
  total %16ldK
  total %8ldK
 "%s" must be of the form name=value %13d disks 
 %13d partitions 
 %CPU -L without -F makes no sense
Try `%s --help' for more information. -L/-T with H/m/-m and -o/-O/o/O is nonsense -S requires k, K, m or M (default is KiB) -d requires positive argument -i makes no sense with -v, -f, and -n -v makes no sense with -i and -f Address CPU Usage EiB GID GROUP GiB Group Id Group Name High: Kbytes KiB Low: Mem: MiB NI Nice Value Only 1 cpu detected PID PPID PR PiB Priority Process Id Process Status S SID Session Id Swap: TiB Total: UID Unknown command - try 'h' for help argument missing can not open tty cannot open "%s" cannot open file %s couldn't create ~/.%src couldn't read ~/.%src error: %s
 error: can not access /proc
 failed to parse argument failed to parse count argument failed to parse count argument: '%s' group name does not exist internal error invalid argument %c invalid group name: %s invalid pid number %s invalid process id: %s invalid user name: %s memory allocation failed modifier -y without format -l makes no sense no matching criteria specified
Try `%s --help' for more information. no process selection criteria no variables specified
Try `%s --help' for more information. not a number: %s obsolete W option not supported (you have a /dev/drum?) only one pattern can be provided
Try `%s --help' for more information. options -N and -q cannot coexist
Try `%s --help' for more information. options -c, -C, -d, -n, -N, -x, -X are mutually exclusive options -p, -q are mutually exclusive with -n, -N partition was not found
 please report this bug priority %lu out of range seconds argument `%s' failed seconds argument `%s' is not positive number sectors seriously crashing: goodbye cruel world something at line %d
 the file already exists - delete or rename it first too large delay value too many arguments total total kB unable to execute '%s' unable to open directory "%s" unknown option '%c'
Usage:
  %s%s unknown signal name %s user name does not exist write error writing to tty failed ~/.%src file successfully created, feel free to edit the content Project-Id-Version: procps
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-09-23 13:53+0200
PO-Revision-Date: 2015-03-02 11:40+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:04+0000
X-Generator: Launchpad (build 18115)
 
 Попробуйте '%s --%s <%s|%s|%s|%s|%s|%s>'
  или '%s --%s <%s|%s|%s|%s|%s|%s>'
 для дополнительной справки.
 
Для дополнительных подробностей смотрите ps(1).
 
Приоритет по умолчанию +4. (snice +4 ...)
Диапазон приоритетов: от +20 (самый медленный) до -20 (самый быстрый).
Отрицательные значения приоритета могут применять только пользователи с правами администратора.
 
Использование:
 %s [опции]
      --help     показать эту справку и выйти
      --si            использовать степени 1000, а не 1024
      --tera          вывод в терабайтах
   -A                   то же, что и -a
   -N, --names          выводить имена переменных без значений
   -X                   то же, что и -a
   -a, --all            показать все переменные
   -d                   то же, что и -h
   -f                   то же, что и -p
   -n, --values         выводить только значения переменных
   -p, --load[=<файл>]  читать значения из файла
  %s [новый приоритет] [параметры] <выражение>
  %s [параметры]
  %s [параметры] <шаблон>
  %s [параметры] <pid> [...]
  %s [опции] [tty]
  %s [параметры] [переменная[=значение] ...]
  %s [опции] команда
  %s [параметры] pid...
  %s [сигнал] [параметры] <выражение>
  -<sig>, --signal <sig>    сигнал для отправки (число или имя)
  -F, --pidfile <файл>      читать PIDы из файла
  -b, --bytes         вывод в байтах
  -c N, --count N     повторить вывод N раз, затем выйти
  -c, --count               вывод количества соответствующих шаблону процессов
  -d, --delay <сек.>  задержка обновления в секундах
  -d, --delimiter <строка>  указать разделитель вывода
  -g, --giga          вывод в гигабайтах
  -h, --human         вывод в удобочитаемом виде
  -h, --no-header     не выводить заголовок
  -i, --ip-addr       отображать IP-адрес вместо имени хоста (если возможно)
  -k, --kilo          вывод в килобайтах
  -l, --list-name           выводить PID и имена процессов
  -l, --lohi          показывать подробную статистику нижней и верхней памяти
  -m, --mega          вывод в мегабайтах
  -n, --one-header       не выводить заголовок повторно
  -o, --old-style     вывод в старом стиле
  -s N, --seconds N   повторять вывод каждые N секунд
  -s, --short         сокращённый формат
  -t, --total         показать общее количество RAM + swap
  -v, --version  показать информацию о версии и выйти
  -w, --wide             широкий вывод
  всего %16ldK
  всего %8ldK
 «%s» должен быть в форме имя=значение %13d дисков 
 %13d разделов 
 %CPU -L без -F не имеет смысла
Наберите `%s --help' для дополнительной информации. -L/-T с H/m/-m и -o/-O/o/O являются бессмыслицей -S требует k, K, m или M (по умолчанию КиБ) -d требует положительный аргумент -i не имеет смысла с -v, -f, и -n -v не имеет смысла с -i и -f Адрес Использование процессора ЭиБ GID GROUP ГиБ Идентификатор группы Имя группы Верхняя: Кб КиБ Нижняя: Память: МиБ NI Значение nice Обнаружен только 1 процессор PID PPID PR ПиБ Приоритет Идентификатор процесса Статус процесса S SID Идентификатор сеанса Подкачка: ТиБ Всего: UID Неизвестная команда — наберите 'h' для справки пропущен аргумент не удалось открыть tty не удаётся открыть «%s» не удаётся открыть файл %s не удалось создать ~/.%src не удалось прочитать ~/.%src ошибка: %s
 ошибка: нет доступа к /proc
 не удалось разобрать аргумент не удалось разобрать аргумент count не удалось разобрать аргумент count: '%s' несуществующее имя группы внутренняя ошибка неверный аргумент %c неверная группа: %s неверный номер pid %s неверный ID процесса: %s неверное имя пользователя: %s не удалось выделить память модификатор -y без формата -l не имеет смысла не указаны критерии соответствия
Наберите `%s --help' для дополнительной информации. отсутствует критерий выбора процесса не указаны переменные
Выполните `%s --help' для более подробной информации. не число: %s устаревшая опция W не поддерживается (у вас есть /dev/drum?) можно указывать только один шаблон
Наберите `%s --help' для дополнительной информации. опции -N и -q нельзя использовать вместе
Наберите `%s --help' для дополнительной информации. параметры -c, -C, -d, -n, -N, -x, -X взаимно исключают друг друга параметры -p, -q являются взаимно исключающими с -n, -N раздел не найден
 пожалуйста, сообщите об этой ошибке приоритет %lu за пределами диапазона количество секунд  `%s' не верное количество секунд  `%s' - не положительное число секторы серьёзный сбой: прощай, жестокий мир! что-то в строке %d
 файл уже существует - сначала удалите или переименуйте его слишком большое значение задержки слишком много аргументов всего всего Кб не удалось выполнить '%s' не удаётся открыть каталог «%s» неизвестный параметр '%c'
Использование:
  %s%s неизвестное имя сигнала %s несуществующее имя пользователя ошибка записи сбой записи в tty файл ~/.%src успешно создан, можете редактировать содержимое 