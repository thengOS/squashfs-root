��          �   %   �      p     q     �  +   �  @   �  5        M  ,   _     �     �     �  $   �  	   �  2   �  $   2     W     Z     l  #   �  !   �     �  %   �               8     <     E     Y  �  h  ;   �  2   5  ;   h  O   �  Q   �     F  P   b  *   �  '   �  #   	  J   *	     u	  Q   �	  B   �	     $
     +
  0   G
  O   x
  C   �
       M   +  K   y  <   �            /        M                    
                                                                                          	             <b>%s</b>
Hardware present: %s <b>%s</b>
Invalid Driver! <b>Currently Installed Windows Drivers:</b> <span size="larger" weight="bold">Select <i>inf</i> file:</span> Are you sure you want to remove the <b>%s</b> driver? Configure Network Could not find a network configuration tool. Driver is already installed. Error while installing. Install Driver Is the ndiswrapper module installed? Location: Module could not be loaded. Error was:

<i>%s</i>
 Ndiswrapper driver installation tool No No file selected. Not a valid driver .inf file. Please drag an '.inf' file instead. Root or sudo privileges required! Select inf File Unable to see if hardware is present. Windows Wireless Drivers Wireless Network Drivers Yes _Install _Install New Driver _Remove Driver Project-Id-Version: ndisgtk
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-10-02 17:50+0200
PO-Revision-Date: 2010-02-04 07:32+0000
Last-Translator: Michael Sinchenko <sinchenko@krvgarm.net>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:44+0000
X-Generator: Launchpad (build 18115)
 <b>%s</b>
Обнаруженные устройства: %s <b>%s</b>
Неправильный драйвер! <b>Установленные драйверы Windows:</b> <span size="larger" weight="bold">Выберете <i>.inf</i>-файл:</span> Вы уверены, что хотите удалить драйвер <b>%s</b>? Настроить сеть Не удалось найти инструмент настройки сети. Драйвер уже установлен Ошибка при установке. Установить драйвер Вы уверены, что модуль ndiswrapper установлен? Расположение: Модуль не может быть загружен. Ошибка:

<i>%s</i>
 Программа установки драйвера ndiswrapper Нет Файл не выбран. Неверный .inf-файл драйвера. Пожалуйста укажите действительный .inf-файл. Необходимы права суперпользователя! Выберите .inf-файл Невозможно определить наличие устройства Драйверы Windows для беспроводных устройств Драйверы беспроводных устройств Да _Установить _Установить новый драйвер _Удалить драйвер 