��    E      D  a   l      �     �  @   	  /   J  &   z  )   �  -   �     �     �          &     =     I      P     q  -   v  =   �  +   �       
   &     1  B   6     y     �  )   �     �  
   �     �  !   �      �  	   	     "	     /	  -   7	     e	     k	     r	  
   �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	  
   �	  #   
     ,
     2
     9
  !   A
  
   c
  )   n
  $   �
  (   �
  *   �
  -     $   ?  +   d  4   �  (   �     �     �                    (    .  -   7  �   e  L   �  +   ;  )   g  &   �     �  &   �     �  *        ,     >  5   G     }  F   �  H   �  8     %   U     {     �  Q   �  D   �     4  h   G     �     �  
   �  ;   �  %   '     M     ^     u  2   �     �     �  '   �     �       
   9  /   D  
   t          �     �  
   �  
   �     �  =   �          *     9  ,   L     y  T   �  A   �  ,   0  0   ]  ;   �  9   �       >     @   [  
   �  (   �     �     �                  7                     B                 6   1      =   >       E               8       $              ;   %   -       ?   *             D       )   0       '              &      !      <   	   :   +   3                 A      /              C   
   #                          2                (   @   4       5             "       9   ,         .        A triangle Kaleidoscope Add a loopback alpha blending effector with rotating and scaling Add age to video input using scratches and dust Add more saturation to the video input Add some hallucination to the video input Add the ripple mark effect on the video input Bulge Bulges the center of the video Cartoon Cartoonify video input Che Guevara Chrome Detect radioactivity and show it Dice Dices the video input into many small squares Display video input like good old low resolution computer way Dissolves moving objects in the video input Distort the video input Distortion Edge Extracts edges in the video input through using the Sobel operator Fake heat camera toning Flip Flip the image, as if looking at a mirror Heat Historical Hulk Invert and slightly shade to blue Invert colors of the video input Invertion Kaleidoscope Kung-Fu Makes a square out of the center of the video Mauve Mirror Mirrors the video Noir/Blanc Optical Illusion Pinch Pinches the center of the video Quark Radioactive Ripple Saturation Sepia Sepia toning Shagadelic Show what was happening in the past Sobel Square Stretch Stretches the center of the video Time delay Traditional black-white optical animation Transform motions into Kung-Fu style Transform video input into a mauve color Transform video input into a metallic look Transform video input into a waveform monitor Transform video input into grayscale Transform video input into realtime goo'ing Transform video input into typical Che Guevara style Transform yourself into the amazing Hulk Twirl Twirl the center of the video Vertigo Warp Waveform X-Ray Project-Id-Version: cheese gnome-2-22
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-video-effects&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-10-29 03:54+0000
PO-Revision-Date: 2014-04-10 07:51+0000
Last-Translator: Yuri Myasoedov <ymyasoedov@yandex.ru>
Language-Team: Russian <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:02+0000
X-Generator: Launchpad (build 18115)
 Треугольный калейдоскоп Добавить петлевой эффект альфа-смешивания с вращением и масштабированием Добавить возраст с помощью царапин и пыли Увеличить насыщенность Добавить галлюцинации Добавить эффект ряби Выпуклость Выпуклость по центру Комикс Стилизовать под комикс Че Гевара Хром Обнаружение радиоактивности Кусочки Разбивает видео на множество кусочков Старый добрый стиль низкого разрешения Растворить движущиеся объекты Исказить видеопоток Искажение Границы Выделить границы с помощью оператора Собеля Температурное фиктивное тонирование Переворот Перевернуть изображение, как будто вы смотрите в зеркало Температура Исторический Зомби Инвертировать и добавить синего Инвертировать цвета Инверсия Калейдоскоп Кун-фу Выделяет по центру  квадрат Лиловость Зеркало Зеркальное отражение Чёрное и белое Оптический обман Щепок Защипнуть видео по центру Кварк Излучение Рябь Насыщенность Сепия Сепия Психоделия Показать, что случилось в прошлом Собель Квадрат Растянуть Растягивает центр видео Задержка времени Традиционная чёрно-белая оптическая анимация Преобразует движения в стиль кун-фу Добавить лилового цвета Придать металлический вид Преобразовать видео в колебания Преобразовать в оттенки серого Вязкое видео Преобразовать в стиль «Че Гевара» Превращение в изумительного зомби. Вихрь Закрутить центр видео Головокружение Искривление Колебания Рентген 