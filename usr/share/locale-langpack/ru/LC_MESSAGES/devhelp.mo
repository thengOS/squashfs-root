��    l      |  �   �      0	  I   1	  J   {	  F   �	  A   
     O
     T
     Y
     ^
     c
     h
     m
     r
     v
  $   z
     �
     �
  
   �
     �
     �
     �
     �
          
  K     �   f     (     8     N     k  
   �     �     �  !   �  -   �     �       6     "   H     k     q     y     �     �     �  ,   �     �     �            #   #     G     M  4   i     �     �     �     �     �     �     �  2        9  #   N     r     �  '   �  "   �  '   �  "     #   &     J  "   i  '   �     �     �     �     �     �  5     0   B  1   s  /   �  5   �       "   )     L     e     �     �     �     �     �     �     �     �               (     5     B     I  
   O     Z     l  /   ~  Z   �     	  �    h     g   z  i   �  ]   L     �     �     �     �     �     �     �     �     �  e   �  
   ;  
   F     Q     k  !   w  @   �  C   �          &  z   >  �  �     N     i  +   �  L   �     �          .  I   G  X   �  F   �     1  }   P  F   �          "     /  *   >  (   i  &   �  g   �     !     =     Y  !   f  I   �     �  D   �  Y   $      ~      �   .   �      �      �      !     !  e   <!  -   �!  Q   �!  2   ""     U"  =   h"  ;   �"  =   �"  ;    #  )   \#  '   �#  '   �#  ?   �#  '   $     >$     Q$  8   X$  Q   �$  j   �$  G   N%  H   �%  N   �%  ^   .&  (   �&  >   �&  &   �&  <   '  :   Y'  <   �'  :   �'     (     %(     >(     N(  #   Z(  +   ~(     �(     �(     �(     �(     )     )  9   9)  $   s)  `   �)  Z   �)  �  T*     _   V      ^          N   W   /          #   5   !   M   a   ?   .   e   7       b               `   1   I       4   j      K       "      <   3                      2   H               (   R             h   
       [       c   8   \   g             	      f         -   '   6       E   ]   S              T       &          k       :       =             C   i       J           %   0      B       F           U   *   D       +   G      X       d   A   Q   L       ,               P         Z          9          $      ;                Y   l   >   )      @   O    "name" and "link" elements are required inside '%s' on line %d, column %d "name" and "link" elements are required inside <sub> on line %d, column %d "title", "name" and "link" elements are required at line %d, column %d "type" element is required inside <keyword> on line %d, column %d 100% 125% 150% 175% 200% 300% 400% 50% 75% A developers' help browser for GNOME Back Book Book Shelf Book: Books disabled Cannot uncompress book '%s': %s Developer's Help program Devhelp Devhelp Website Devhelp integrates with other applications such as Glade, Anjuta, or Geany. Devhelp is an API documentation browser. It provides an easy way to navigate through libraries, search by function, struct, or macro. It provides a tabbed interface and allows to print results. Devhelp support Devhelp — Assistant Display the version and exit Documentation Browser Empty Page Enabled Enum Error opening the requested link. Expected '%s', got '%s' at line %d, column %d Font for fixed width text Font for text Font for text with fixed width, such as code examples. Font for text with variable width. Fonts Forward Function Group by language Height of assistant window Height of main window Invalid namespace '%s' at line %d, column %d KEYWORD Keyword Language: %s Language: Undefined List of books disabled by the user. Macro Main window maximized state Makes F2 bring up Devhelp for the word at the cursor New _Tab New _Window Opens a new Devhelp window Page Preferences Quit any running Devhelp S_maller Text Search and display any hit in the assistant window Search for a keyword Selected tab: "content" or "search" Show API Documentation Struct The X position of the assistant window. The X position of the main window. The Y position of the assistant window. The Y position of the main window. The height of the assistant window. The height of the main window. The width of the assistant window. The width of the index and search pane. The width of the main window. Title Type Use system fonts Use the system default fonts. Whether books should be grouped by language in the UI Whether the assistant window should be maximized Whether the assistant window should be maximized. Whether the main window should start maximized. Which of the tabs is selected: "content" or "search". Width of the assistant window Width of the index and search pane Width of the main window X position of assistant window X position of main window Y position of assistant window Y position of main window _About _About Devhelp _Close _Find _Fixed width: _Group by language _Larger Text _Normal Size _Preferences _Print _Quit _Side pane _Use system fonts _Variable width:  documentation;information;manual;developer;api; https://wiki.gnome.org/Apps/Devhelp?action=AttachFile&#x26;do=view&#x26;target=devhelp.png translator-credits Project-Id-Version: ru
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=devhelp&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-16 12:08+0000
PO-Revision-Date: 2016-06-09 11:00+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: русский <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:10+0000
X-Generator: Launchpad (build 18115)
Language: ru
 Элементы «name» и «link» требуются в «%s» в строке %d, столбец %d Элементы «name» и «link» требуются в <sub> в строке %d, столбец %d Элементы «title», «name» и «link» требуются в строке %d, столбец %d Элемент «type» требуется в <keyword> в строке %d, столбце %d 100% 125% 150% 175% 200% 300% 400% 50% 75% Приложение для просмотра справки GNOME для разработчиков Назад Книга Книжная полка Книга: Выключенные книги Не удалось распаковать книгу «%s»: %s Программа справки для разработчиков Devhelp Веб-сайт Devhelp Devhelp интегрируется с другими приложениями, например с Glade, Anjuta и Geany. Devhelp — это браузер документации по API. Devhelp предоставляет простую навигацию по библиотекам, позволяет выполнять поиск по функциям, структурам и макросам. Браузер использует интерфейс со вкладками и с возможностью печати. Поддержка Devhelp Devhelp — Помощник Показать версию и выйти Поиск и чтение документации разработчика Пустая страница Включено Перечисление Ошибка при открытии запрошенной ссылки. Ожидалось «%s», получено «%s» в строке %d, столбец %d Шрифт для текста фиксированной ширины Шрифт для текста Шрифт для текста с фиксированной шириной, например, фрагментов кода. Шрифт для текста с переменной шириной. Шрифты Вперёд Функция Группировать по языкам Высота окна помощника Высота главного окна Недопустимое пространство имён «%s» в строке %d, столбце %d КЛЮЧЕВОЕ СЛОВО Ключевое слово Язык: %s Язык: не определён Список книг, выключенных пользователем. Макрос Главное окно в развёрнутом состоянии Вызывать Devhelp по нажатию F2 для слова под курсором Новая _вкладка _Создать окно Открывает новое окно Devhelp Страница Параметры Выйти из Devhelp У_меньшить текст Поиск и отображение любого совпадения в окне помощника Поиск по ключевому слову Выбранная вкладка: «содержание» или «поиск» Показать документацию по API Структура Положение окна помощника по оси X. Положение главного окна по оси X. Положение окна помощника по оси Y. Положение главного окна по оси Y. Высота окна помощника. Высота главного окна. Ширина главного окна. Ширина панели содержания и поиска. Ширина главного окна. Заголовок Тип Использовать системные шрифты Использовать системные шрифты по умолчанию. Должны ли книги быть сгруппированы в интерфейсе по языкам Разворачивать ли вспомогательное окно Разворачивать ли вспомогательное окно. Разворачивать ли главное окно при запуске. Какая из вкладок выбрана: «содержание» или «поиск». Ширина окна помощника Ширина панели содержания и поиска Ширина главного окна Положение окна помощника по оси X Положение главного окна по оси X Положение окна помощника по оси Y Положение главного окна по оси Y _О приложении _О приложении _Закрыть _Найти _Постоянная ширина: _Группировать по языкам У_величить текст О_бычный размер _Параметры _Печать _Завершить _Боковая панель _Использовать системные шрифты _Переменная ширина:  документация;информация;руководство;разработчик;api; https://wiki.gnome.org/Apps/Devhelp?action=AttachFile&#x26;do=view&#x26;target=devhelp.png Валек Филиппов <frob@df.ru>
Сергей Сафонов <spoof@spoofa.info>
Юрий Мясоедов <ymyasoedov@yandex.ru>, 2014.

Launchpad Contributions:
  Alexander Telenga https://launchpad.net/~telenga
  Dennis A. Eliseev https://launchpad.net/~danius
  Dennis Kowalski https://launchpad.net/~denimnumber1
  Dmitriy S. Seregin https://launchpad.net/~dseregin
  Dmitry Belonogov https://launchpad.net/~nougust
  Eugene Marshal https://launchpad.net/~lowrider
  Leonid Kanter https://launchpad.net/~lkanter
  Nickolay V. Shmyrev https://launchpad.net/~nshmyrev
  Oleg Koptev https://launchpad.net/~koptev-oleg
  Sergey Rebko https://launchpad.net/~arch1s
  Somebody32 https://launchpad.net/~som32
  Valek Filippov https://launchpad.net/~frob-df
  Yuri Kozlov https://launchpad.net/~kozlov-y
  Yuri Myasoedov https://launchpad.net/~ymyasoedov
  anisim0ff https://launchpad.net/~vicanis
  boa https://launchpad.net/~b-o-a
  Александр AldeX Крылов https://launchpad.net/~aldex 