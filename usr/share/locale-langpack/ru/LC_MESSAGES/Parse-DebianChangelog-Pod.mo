��    !      $  /   ,      �  �   �  �   �     �     �     �     �     �     �     �  '     !   +     M     Y  	   ^     h     p  S   x     �  J   �     .     7  +   @  6   l     �     �     �     �     �     �  (   �          1  �  5  �   �  �   �	     �
     �
     �
     �
     �
     �
  =   �
  c   '  D   �     �     �     �     �       �        �  z   �     d  #   u  ^   �  W   �     P  $   n     �     �  0   �     �  )   �     �                                                                             
                                   	                       !                               pod2usage(   -msg     => $message_text ,
               -exitval => $exit_status  ,
               -verbose => $verbose_level,
               -output  => $filehandle,
               -textdomain => $textdomain );

   pod2usage( { -message => gettext( $message_text ) ,
               -exitval => $exit_status  ,
               -verbose => $verbose_level,
               -output  => $filehandle,
               -textdomain => $textdomain } );

   pod2usage($exit_status);

   pod2usage($message_text);

 1. 2. 3. 4. COPYRIGHT AND LICENSE Copyright (C) 2005 by Frank Lichtenheld Creates a new object, no options. DESCRIPTION Date Functions METHODS Methods NOTE: This format isn't stable yet and may change in later versions of this module. Parse::DebianChangelog Parse::DebianChangelog::Entry - represents one entry in a Debian changelog SEE ALSO SYNOPSIS The output formats supported are currently: Timestamp (Date expressed in seconds since the epoche) an error description date of the (first) entry dpkg html not yet documented rfc822 the line number where the error occurred the original line xml Project-Id-Version: libparse-debianchangelog-perl
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-08-25 20:38+0000
PO-Revision-Date: 2013-03-07 07:52+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
   pod2usage(   -msg     => $message_text ,
               -exitval => $exit_status  ,
               -verbose => $verbose_level,
               -output  => $filehandle,
               -textdomain => $textdomain );

   pod2usage( { -message => gettext( $message_text ) ,
               -exitval => $exit_status  ,
               -verbose => $verbose_level,
               -output  => $filehandle,
               -textdomain => $textdomain } );

   pod2usage($exit_status);

   pod2usage($message_text);

 1. 2. 3. 4. АВТОРСКИЕ ПРАВА И ЛИЦЕНЗИРОВАНИЕ Авторские права (C) 2005 принадлежат Фрэнку Лихтенхельду Создаёт новый объект, без параметров. ОПИСАНИЕ Дата Функции МЕТОДЫ Методы ВНИМАНИЕ: этот формат не является стабильным и может измениться в более поздних версиях данного модуля. Parse::DebianChangelog Parse::DebianChangelog::Entry - представляет одну запись в журнале изменений Debian СМ. ТАКЖЕ КРАТКОЕ СОДЕРЖАНИЕ Поддерживаемые в настоящее время выходные форматы: Временная метка (дата в секундах с начала эпохи) описание ошибки дата (первой) записи dpkg html ещё не задокументированно rfc822 номер строки с ошибкой исходная строка xml 