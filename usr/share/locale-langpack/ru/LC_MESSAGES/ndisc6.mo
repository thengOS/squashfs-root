��    $      <  5   \      0     1     D     X     e     w  	   �     �     �     �  (   �     �               1     E  6   Z  (   �     �     �     �     �     �     �            
   5     @  "   L  0   o     �     �  
   �     �     �     �    �     	          9     H     e  	   w     �     �     �  L   �  =   �  0   :	  9   k	  ,   �	     �	  6   �	  (   "
  ,   K
     x
     �
     �
  $   �
     �
     �
  $     +   &     R  f   c  2   �      �          #  H   >  	   �  *   �     
                    #                                     	                                          "   !                $                                                                             %3u%% completed...  %u.%03u ms   (%0.3f kbytes/s)  Prefix                   :   from %s
 %12u (0x%08x) %s
 %s port %s: %s
 %s: %s
 Cannot create %s (%m) - already running? Cannot find user "%s" Cannot run "%s": %m Cannot send data: %s
 Cannot write %s: %m Configured with: %s
 Copyright (C) %u-%u Pierre Ynard, Remi Denis-Courmont
 Copyright (C) %u-%u Remi Denis-Courmont
 Fatal error (%s): %m Input error No No response. Receive error: %s
 Received Receiving ICMPv6 packet Sending ICMPv6 packet Timed out. Transmitted Try "%s -h" for more information.
 Written by Pierre Ynard and Remi Denis-Courmont
 Written by Remi Denis-Courmont
 Yes byte bytes millisecond milliseconds port  second seconds Project-Id-Version: ndisc6
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2011-02-20 10:14+0200
PO-Revision-Date: 2012-03-12 05:19+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 18:10+0000
X-Generator: Launchpad (build 18115)
                     %3u%% выполнено...  %u.%03u мс   (%0.3f Килобайт/с)  Префикс:   от %s
 %12u (0x%08x) %s
 %s порт %s: %s
 %s: %s
 Невозможно создать %s (%m) - уже выполняется? Невозможно найти пользователя "%s" Невозможно выполнить "%s": %m Невозможно отправить данные: %s
 Невозможно записать %s: %m Настроен с: %s
 Copyright (C) %u-%u Pierre Ynard, Remi Denis-Courmont
 Copyright (C) %u-%u Remi Denis-Courmont
 Критическая ошибка (%s): %m Ошибка ввода Нет Нет отклика. Получения ошибка: %s
 Принято Приём пакета ICMPv6 Передача пакета ICMPv6 Время ожидания истекло. Передано Попробуйте "%s -h" для получения дополнительных сведений.
 Авторы: Pierre Ynard и Remi Denis-Courmont
 Автор: Remi Denis-Courmont
 Да байт байт байт миллисекунда миллисекунды миллисекунд порт  секунда секунды секунд 