��    j      l  �   �      	     	  >   -	  <   l	  ;   �	     �	  	   �	  
   
     
     #
  '   ,
  D   T
  6   �
  2   �
            &   4     [     s     �     �     �  %   �     �  %   �  #   #  $   G     l     n     �  0   �     �  I   �       *   =     h  B   ~     �  (   �       	          '   2  )   Z  �   �  &        C     X     t  0   �     �     �     �     �     �               =     Z  -   v     �     �     �      �  /   
     :     @  &   X          �     �     �     �     �     �          $  +   5     a  *   z  *   �     �  "   �          .  "   C     f  '   �  #   �     �     �  '   �          8  ,   L  A   y     �     �     �           $  /   C  *   s     �     �     �    �  +   �  o   	  j   y  j   �     O     m     |     �     �  C   �  �   �  �   �  �   �  4     ,   Q  M   ~  0   �  ,   �  '   *  %   R  '   x  6   �     �  3   �  1     2   N     �  )   �     �  0   �     �  �     A   �  p   �  ?   H   {   �   D   !  W   I!     �!     �!  H   �!  l   "  S   �"  M  �"  n   &$  6   �$  8   �$  (   %  H   .%     w%     z%  8   �%     �%  +   �%  4   &  ,   8&  1   e&  1   �&  O   �&  9   '  *   S'  B   ~'  F   �'  �   (     �(  .   �(  L   �(     )  (   =)  $   f)  B   �)  6   �)  '   *  4   -*  2   b*     �*  X   �*  .   +  N   =+  R   �+  N   �+  E   .,  ?   t,  %   �,  ]   �,  A   8-  >   z-  W   �-  ,   .  =   >.  W   |.  7   �.  5   /  _   B/  |   �/  0   0  ;   P0  D   �0  :   �0  $   1  q   11  W   �1  +   �1  6   '2     ^2     W          /   6   [      b          Z   ;   S   X   \   M       _       +                        P             &   $       :   5   8       B         Y   >   O   R   f   g       T   0               `   e   ,   (   i   U                         ?             
       E   C       V   ]       "   '            a   *       !       	   L                   A      %   3      )                       j      N   @      7           G   K           9   I   J      2              -       c   D   1          F   #      <   4      Q   =   h      .              H   d   ^        
Execution times (seconds)
     Conflict between rule %d and token %s resolved as an error     Conflict between rule %d and token %s resolved as reduce     Conflict between rule %d and token %s resolved as shift  TOTAL                 :  on left:  on right:  type %d is %s
 $default %%expect-rr applies only to GLR parsers %d nonterminal useless in grammar %d nonterminals useless in grammar %d rule useless in grammar %d rules useless in grammar %d shift/reduce conflict %d shift/reduce conflicts %s affects only GLR parsers %s home page: <%s>.
 %s must be followed by positive number %s redeclaration for %s %s redeclaration for <%s> %s: cannot open %s: invalid language %s: missing operand %u bitset_allocs, %u freed (%.2f%%).
 %u bitset_lists
 %u bitset_resets, %u cached (%.2f%%)
 %u bitset_sets, %u cached (%.2f%%)
 %u bitset_tests, %u cached (%.2f%%)
 ' Accumulated runs = %u
 Bitset statistics:

 Copyright (C) %d Free Software Foundation, Inc.
 Grammar Mandatory arguments to long options are mandatory for short options too.
 Nonterminals useless in grammar Nonterminals, with rules where they appear Report bugs to <%s>.
 Report translation bugs to <http://translationproject.org/team/>.
 Rules useless in grammar Rules useless in parser due to conflicts State %d State %d  Terminals unused in grammar Terminals, with rules where they appear The same is true for optional arguments.
 This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 Try '%s --help' for more information.
 Unknown system error Usage: %s [OPTION]... FILE
 Valid arguments are: Written by Robert Corbett and Richard Stallman.
 ` accept ambiguous argument %s for %s bison (GNU Bison) %s cannot close file column number overflow conflicting outputs to file %s conflicts: %d reduce/reduce
 conflicts: %d shift/reduce
 conflicts: %d shift/reduce, %d reduce/reduce
 count log histogram
 density histogram
 deprecated directive, use %s deprecated directive: %s, use %s empty rule for typed nonterminal, and no action error error (nonassociative)
 explicit type given in untyped grammar fatal error go to state %d
 input/output error integer out of range: %s invalid argument %s for %s invalid directive: %s invalid null character line number overflow memory exhausted missing identifier in parameter declaration multiple %s declarations multiple language declarations are invalid multiple skeleton declarations are invalid no rules in the input grammar nonterminal useless in grammar: %s only one %s allowed per rule previous declaration redefining user token number of %s reduce using rule %d (%s) refusing to overwrite the input file %s rule given for %s, which is a token rule is too long rule useless in grammar rule useless in parser due to conflicts shift, and go to state %d
 size log histogram
 start symbol %s does not derive any sentence symbol %s is used, but is not defined as a token and has no rules symbol %s redeclared symbol %s redefined the start symbol %s is a token the start symbol %s is undefined time in %s: %ld.%06ld (%ld%%)
 too many symbols in input grammar (limit is %d) type clash on default action: <%s> != <%s> unset value: $$ unused value: $%d warning Project-Id-Version: bison 2.4.1
Report-Msgid-Bugs-To: bug-bison@gnu.org
POT-Creation-Date: 2015-01-23 13:55+0100
PO-Revision-Date: 2015-12-03 18:36+0000
Last-Translator: Pavel Maryanov <Unknown>
Language-Team: Russian <gnu@mx.ru>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 16:04+0000
X-Generator: Launchpad (build 18115)
Language: ru
 
Время выполнения (сек.)
     Конфликт между правилом %d и лексемой %s разрешен как ошибка     Конфликт между правилом %d и лексемой %s разрешен выводом     Конфликт между правилом %d и лексемой %s разрешен сдвигом  СУММА                 :  налево:  направо:  тип %d является %s
 $default %%expect-rr применимо только к GLR парсерам %d нетерминал бесполезен в грамматике %d нетерминала бесполезно в грамматике %d нетерминалов бесполезно в грамматике %d правило бесполезно в грамматике %d правила бесполезно в грамматике %d правила бесполезно в грамматике %d конфликт сдвига/вывода %d конфликта сдвига/вывода %d конфликтов сдвига/вывода %s влияет только на GLR парсеры Домашняя страница %s: <%s>.
 за %s должно следовать положительное число повторное описание %s для %s %s повторно описан для <%s> %s: невозможно открыть %s: недопустимый язык %s: отсутсвует операнд %u bitset_allocs, %u освобождено (%.2f%%).
 %u bitset_lists
 %u bitset_resets, %u кэшировано (%.2f%%)
 %u bitset_sets, %u кэшировано (%.2f%%)
 %u bitset_tests, %u кэшировано (%.2f%%)
 » Накоплено запусков = %u
 Статистика bitset:

 Copyright (C) %d Free Software Foundation, Inc.
 Грамматика Соответствующие длянные опции также соответствуют и коротким опциям.
 Нетерминал бесполезен в грамматике Нетерминальные символы с правилами, в которых они появляются Об ошибках сообщайте по адресу <%s>.
 Об ошибках в переводе сообщайте по адресу <http://translationproject.org/team/ru.html>.
 Правила не используются в грамматике Правило не применимо в парсере из-за конфликтов Состояние %d Состояние %d  Терминалы не используются в грамматике Терминальные символы с правилами, в которых они появляются То-же допустимо для опциональных параметров.
 Это свободная программа; условия распространения смотрите в исходных текстах.
НИКАКИХ гарантий не предоставляется, даже гарантии ПОЛЕЗНОСТИ или ПРИГОДНОСТИ
ДЛЯ ОПРЕДЕЛЕННОЙ ЦЕЛИ.
 Запустите «%s --help» для получения дополнительной информации.
 Неизвестная системная ошибка Использование: %s [КЛЮЧИ]... ФАЙЛ
 Допустимые аргументы: Авторы: Роберт Корбет и Ричард Столмен.
 « принять неоднозначный аргумент %s для %s bison (GNU Bison) %s не удается закрыть файл переполнение номера столбца конфликт вывода в файл %s конфликты: %d вывода/вывода
 конфликты: %d сдвига/вывода
 конфликты: %d сдвига/вывода, %d вывода/вывода
 посчитать гистограмму журнала
 гистограмма плотности
 устаревшая директива, используйте %s устаревшая директива: %s, используйте %s пустое правило для типизированного нетерминального символа, и нет действия ошибка ошибка (неассоциативная)
 указан тип в нетипизированной грамматике фатальная ошибка переход в состояние %d
 ошибка ввода-вывода выход за границы диапазона целого: %s недопустимый аргумент %s для %s неверная директива: %s недопустимый нулевой символ переполнение номера строки память исчерпана в описании параметра отсутствует идентификатор множественное описание %s множественные описания языка не применимо множественные описания скелета не применимо отсутствуют правила во входной грамматике нетерминал бесполезен в грамматике: %s только одно %s разрешено на правило предыдущее описание переопределение номера лексемы пользователя для %s вывод с использованием правила %d (%s) отказ перезаписи входного файла %s правило задано для %s, который является лексемой слишком длинное правило бесполезное правило в грамматике правило не применимо в парсере из-за конфликтов сдвиг, и переход в состояние %d
 гистограмма размера журнала
 начальный символ %s не выводит ни одного предложения символ %s используется, но не определен как лексема и не имеет правил переопределение символа %s повторное определение символа %s начальный символ %s является лексемой начальный символ %s не определен время в %s: %ld.%06ld (%ld%%)
 слишком много символов во входящей грамматике (максимально %d) конфликт типов на действии по умолчанию: <%s> != <%s> неуказанное значение: $$ неиспользуемая переменная: $%d предупреждение 