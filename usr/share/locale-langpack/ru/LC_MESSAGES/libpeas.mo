��    #      4  /   L           	     $  #   *  %   N     t     �     �     �     �     �     �     �          
           -     N     V     c     o     �  \   �  ?   �  #   2  '   V     ~     �     �     �     �     �     �     �     �    �  A   �       R   )  P   |  #   �  D   �  1   6	     h	     �	     �	  &   �	     �	     �	     �	  *   
  :   8
     s
     �
     �
  5   �
  6   �
  �     g   �  =   #  M   a     �     �     �     �     �               (     <                         #            
          "                                                                !            	                                  - libpeas demo application About Additional plugins must be disabled An additional plugin must be disabled An error occurred: %s Dependency '%s' failed to load Dependency '%s' was not found Disable Plugins E_nable All Enabled Failed to load Peas Gtk Plugin Plugin Manager Plugin Manager View Plugin loader '%s' was not found Plugins Pr_eferences Preferences Run from build directory Show Builtin The '%s' plugin depends on the '%s' plugin.
If you disable '%s', '%s' will also be disabled. The following plugins depend on '%s' and will also be disabled: The plugin '%s' could not be loaded There was an error displaying the help. View _About _Cancel _Close _Disable All _Enabled _Help _Preferences _Quit Project-Id-Version: libpeas master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libpeas&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-24 00:33+0000
PO-Revision-Date: 2015-11-10 03:51+0000
Last-Translator: Stas Solovey <whats_up@tut.by>
Language-Team: Русский <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:58+0000
X-Generator: Launchpad (build 18115)
Language: ru
 — демонстрационное приложение libpeas О приложении Дополнительные модули должны быть выключены Дополнительный модуль должен быть отключен Произошла ошибка: %s Не удалось загрузить зависимость «%s» Не найдена зависимость «%s» Отключить модули Вкл_ючить все Включён Не удалось загрузить Peas Gtk Модуль Менеджер модулей Окно менеджера модулей Не найден загрузчик модулей «%s» Модули _Параметры Параметры Запустить из каталога сборки Показывать встроенные модули Модуль «%s» зависит от модуля «%s».
Если вы отключаете «%s», то «%s» также должен быть отключен. Следующие модули зависят от «%s» и также будут отключены: Модуль «%s» не может быть загружен Произошла ошибка при отображении справки. Вид _О приложении _Отмена _Закрыть В_ыключить все _Включён _Справка _Параметры _Завершить 