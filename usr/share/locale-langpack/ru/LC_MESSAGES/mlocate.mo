��    4      �  G   \      x  4   y  F   �  #   �          2     G  1   \  �   �     @     N     e       *   �  �  �  �  �  +        �  #   �  0   �            =  )   ^  '   �     �     �  %   �            &   *  .   Q     �     �     �  )   �  !   �  8     $   J     o     �  '   �     �  "   �  .        0  $   L  "   q     �  "   �  !   �     �       8  $  �   ]  �   �  ;   �  ;   �  7     $   C  l   h  m  �     C  ?   ^  B   �  K   �  f   -  
  �  %  �&  <   �,  .   -  C   5-  S   y-  C   �-  =   .  R   O.  A   �.  :   �.  2   /  Q   R/  5   �/  *   �/  L   0  g   R0  5   �0  2   �0      #1  =   D1  3   �1  c   �1  Q   2  *   l2  4   �2  I   �2  @   3  8   W3  Z   �3  5   �3  Z   !4  N   |4  0   �4  H   �4  5   E5  0   {5  H   �5     	   2   "       +       #            %                1                    ,                 *   )   (       3             
   '                        4   /      !                         -       &   .                 0                            $    	%'ju byte in file names
 	%'ju bytes in file names
 	%'ju byte used to store database
 	%'ju bytes used to store database
 	%'ju directory
 	%'ju directories
 	%'ju file
 	%'ju files
 
Report bugs to %s.
 --%s specified twice --%s would override earlier command-line argument Copyright (C) 2007 Red Hat, Inc. All rights reserved.
This software is distributed under the GPL v.2.

This program is provided with NO WARRANTY, to the extent permitted by law. Database %s:
 I/O error reading `%s' I/O error seeking in `%s' I/O error while writing to `%s' I/O error while writing to standard output Usage: locate [OPTION]... [PATTERN]...
Search for entries in a mlocate database.

  -A, --all              only print entries that match all patterns
  -b, --basename         match only the base name of path names
  -c, --count            only print number of found entries
  -d, --database DBPATH  use DBPATH instead of default database (which is
                         %s)
  -e, --existing         only print entries for currently existing files
  -L, --follow           follow trailing symbolic links when checking file
                         existence (default)
  -h, --help             print this help
  -i, --ignore-case      ignore case distinctions when matching patterns
  -l, --limit, -n LIMIT  limit output (or counting) to LIMIT entries
  -m, --mmap             ignored, for backward compatibility
  -P, --nofollow, -H     don't follow trailing symbolic links when checking file
                         existence
  -0, --null             separate entries with NUL on output
  -S, --statistics       don't search for entries, print statistics about each
                         used database
  -q, --quiet            report no error messages about reading databases
  -r, --regexp REGEXP    search for basic regexp REGEXP instead of patterns
      --regex            patterns are extended regexps
  -s, --stdio            ignored, for backward compatibility
  -V, --version          print version information
  -w, --wholename        match whole path name (default)
 Usage: updatedb [OPTION]...
Update a mlocate database.

  -f, --add-prunefs FS           omit also FS
  -n, --add-prunenames NAMES     omit also NAMES
  -e, --add-prunepaths PATHS     omit also PATHS
  -U, --database-root PATH       the subtree to store in database (default "/")
  -h, --help                     print this help
  -o, --output FILE              database to update (default
                                 `%s')
      --prune-bind-mounts FLAG   omit bind mounts (default "no")
      --prunefs FS               filesystems to omit from database
      --prunenames NAMES         directory names to omit from database
      --prunepaths PATHS         paths to omit from database
  -l, --require-visibility FLAG  check visibility before reporting files
                                 (default "yes")
  -v, --verbose                  print paths of files as they are found
  -V, --version                  print version information

The configuration defaults to values read from
`%s'.
 `%s' does not seem to be a mlocate database `%s' has unknown version %u `%s' has unknown visibility flag %u `%s' is locked (probably by an earlier updatedb) `=' expected after variable name can not change directory to `%s' can not change group of file `%s' to `%s' can not change permissions of file `%s' can not drop privileges can not find group `%s' can not get current working directory can not lock `%s' can not open `%s' can not open a temporary file for `%s' can not read two databases from standard input can not stat () `%s' configuration is too large error replacing `%s' file name length %zu in `%s' is too large file name length %zu is too large file system error: zero-length file name in directory %s invalid empty directory name in `%s' invalid regexp `%s': %s invalid value `%s' of --%s invalid value `%s' of PRUNE_BIND_MOUNTS missing closing `"' no pattern to search for specified non-option arguments are not allowed with --%s unexpected EOF reading `%s' unexpected data after variable value unexpected operand on command line unknown variable `%s' value in quotes expected after `=' variable `%s' was already defined variable name expected warning: Line number overflow Project-Id-Version: mlocate
Report-Msgid-Bugs-To: https://fedorahosted.org/mlocate/
POT-Creation-Date: 2012-09-22 04:14+0200
PO-Revision-Date: 2013-06-20 21:09+0000
Last-Translator: Yulia Poyarkova <Unknown>
Language-Team: Russian <trans-ru@lists.fedoraproject.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 18:27+0000
X-Generator: Launchpad (build 18115)
Language: ru
 	»%'ju байт в названии файла
 	»%'ju байта в названии файла
 	»%'ju байт в названии файла
 	база данных заняла »%'ju байт
 	база данных заняла »%'ju байта
 	база данных заняла »%'ju байт
 	»%'ju папка
 	»%'ju папки
 	»%'ju папок
 	»%'ju файл
 	»%'ju файла
 	»%'ju файлов
 
Отчёт об ошибках отправьте %s.
 --%s определен дважды --%s переопределит заданный ранее аргумент командной строки (C) 2007 Red Hat, Inc. Все права защищены.
Это программное обеспечение распространяется в соответствии с условиями лицензии GPL v.2.

Эта программа распространяется БЕЗ ГАРАНТИЙ, в пределах, допускаемых законом. База данных %s:
 Ошибка ввода/вывода при чтении «%s» ошибка ввода/вывода при поиске в «%s» ошибка ввода/вывода во время записи в «%s» ошибка ввода/вывода во время записи в стандартный вывод Использование: locate [ПАРАМЕТР]... [ШАБЛОН]...
Поиск записей в базе денных mlocate.

  -A, --all              вывести записи, которые соответствуют всем шаблонам
  -b, --basename         сравнить только базовое имя путей
  -c, --count            вывести только количество найденных записей
  -d, --database ПУТЬ_БД  использовать ПУТЬ_БД вместо базы данных по умолчанию (которой является
                         %s)
  -e, --existing         вывести только записи для существующих файлов
  -L, --follow           переходить по символьным ссылкам при проверке существования
                         файла (значение по умолчанию)
  -h, --help             вывести данную справку
  -i, --ignore-case      игнорировать регистр при сравнении с шаблонами
  -l, --limit, -n ЛИМИТ  ограничить выходные данные (или их число) до ЛИМИТного количества записей
  -m, --mmap             игнорируется, для обратной совместимости
  -P, --nofollow, -H     не переходить по символьным ссылкам при проверке существования
                         файла
  -0, --null             выделить записи с нулевыми выходными данными
  -S, --statistics       не искать записи, вывести статистику по каждой
                         используемой базе данных
  -q, --quiet            не выводить сообщения об ошибках при чтении баз данных
  -r, --regexp REGEXP    искать, используя основные регулярные выражения REGEXP вместо шаблонов
      --regex            шаблоны - это расширенные регулярные выражения
  -s, --stdio            игнорируется, для обратной совместимости
  -V, --version          вывести информацию о версии
  -w, --wholename        искать полное соответствие пути (по умолчанию)
 Применение: updatedb [ОПЦИИ]...
Обновление базы данные mlocate.

  -f, --add-prunefs ФС           пропускать указанные ФС
  -n, --add-prunenames ИМЕНА     пропускать ИМЕНА
  -e, --add-prunepaths ПУТИ     пропускать ПУТИ
  -U, --database-root ПУТЬ       поддерево, хранящееся в базе данных (по умолчанию "/")
  -h, --help                     вывести эту справку
  -o, --output FILE              база данных для обновления (по умолчанию
                                 `%s')
      --prune-bind-mounts ФЛАГ   пропускать точки подключения (по умолчанию "нет")
      --prunefs ФС               ФС для исключения из базы данных
      --prunenames ИМЕНА         каталог для исключения из базы данных
      --prunepaths ПУТИ         пути для исключения из базы данных
  -l, --require-visibility ФЛАГ  проверить существование файлов прежде чем добавлять
                                 (по умолчанию "да")
  -v, --verbose                  вывести пути к найденным файлам
  -V, --version                  вывести информацию о версии

Используемые по умолчанию параметры получены из
«%s».
 «%s» не является базой данных mlocate неизвестная версия «%s»: %u «%s» имеет неизвестный видимый флаг %u «%s» заблокирован (возможно более ранним updatedb) «=» ожидается после имени переменной не удалось перейти к каталогу «%s» не удалось изменить группу файла с «%s» на «%s» не удалось изменить права файла «%s» невозможно понизить привилегии не удалось найти группу «%s» не удаётся получить текущий рабочий каталог не удалось заблокировать «%s» невозможно открыть «%s» не удалось открыть временный файл для «%s» невозможно читать две базы данных из стандартного ввода не удалось выполнить stat () «%s» конфигурация очень большая ошибка замены «%s» слишком длинное имя файла %zu в «%s» слишком длинное имя файла %zu ошибка файловой системы: нулевая длина имени файла в %s недействительное имя пустого каталога в «%s» недопустимое regexp «%s»: %s неверное значение «%s» для --%s недопустимое значение «%s» для PRUNE_BIND_MOUNTS отсутствуют закрывающие кавычки `"' не определён шаблон для поиска не является аргументом опции или не разрешен с --%s неожиданный EOF при чтении «%s» непредвиденные данные после значения переменной непредвиденный операнд в командной строке неизвестная переменная `%s' значение в кавычках ожидается после «=» переменная `%s' уже определена ожидание имени переменной внимание: Количество строк переполнено 