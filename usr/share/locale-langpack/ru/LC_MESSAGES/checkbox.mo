��    c     4  5  L&       3  �   !3  c   �3  	   >4     H4     N4  �   Z4  �   �4  �   �5  �   u6  !   >7     `7  	   d7     n7     ~7     �7  "   �7     �7  #   �7  E   �7     18  #   ?8  5   c8  I   �8  8   �8  $   9  3   A9  &   u9  +   �9     �9     �9  *   �9     ":  /   7:  ?   g:  3   �:  2   �:  7   ;  /   F;  9   v;     �;  7   �;  .   <  F   5<  D   |<  E   �<  C   =  C   K=  =   �=  <   �=  8   
>  I   C>  A   �>  #   �>  
   �>     �>     
?     "?  B   <?     ?  R   �?  a   �?  G   Q@  L   �@     �@     �@     A     A     .A  *   EA     pA     �A     �A  	   �A  "   �A     �A     �A  (   �A  .   
B  /   9B  V   iB  <   �B  9   �B  7   7C  !   oC  )   �C  &   �C  +   �C  )   D  9   8D  Z   rD     �D  �   �D  M   gE  ?   �E  <   �E  >   2F  i   qF     �F  #   �F     G     'G  �   4G  -   �G     H     *H  C   JH  +   �H     �H  ?   �H     I     I  
   1I  "   <I     _I     hI  n   |I     �I  3   �I     /J     <J     IJ  2   aJ  6   �J  P   �J  Q   K  	   nK  
   xK  #   �K  %   �K     �K     �K  J   �K     0L  )   6L     `L  r   gL     �L     �L  )   �L     M  
   /M     :M     KM  �   ]M     �M     N  3   ,N     `N     yN     �N     �N     �N     �N     �N     �N  )   �N     O     $O  G   3O     {O     �O  :   �O  f   �O  S   <P     �P     �P     �P  	   �P  $   �P     �P     �P  
   Q  %   Q     =Q     UQ  1   nQ  	   �Q     �Q  	   �Q     �Q  :   �Q  7   R  :   DR     R     �R     �R     �R     �R     �R  (   �R     S     S     +S  "   9S     \S     bS     hS     |S     �S     �S     �S     �S     �S     �S  
   �S     �S  /   �S  *   (T  )   ST  9   }T     �T     �T     �T  
   �T  [  �T     RW  �   SX  �  Y  �  �Z  �  �\  �   >^  �   �^  �   �_  �   �`  �   �a    #b  �   ;c    d    e  �   4f  �  �f  �   �h  �   >i  �   1j  �   �j  �   �k  (  �l    �n  T  �p  K  *s  �   vu  �   Ov  ?  w  �  Wx  K  Nz  ~  �{  o  ~  >  �  V  Ȁ  h  �  �   ��  �   H�  �   �  )  �  m  9�  m  ��  o  �  q  ��  m  ��  n  e�  r  ԏ  q  G�  �  ��  �  O�  �  �  �  s�  �  �  �  ��  �  .�  M  ŝ  .  �  \  B�    ��  |  ��  �   0�  �  �  �  ��  �  4�  �  ԫ    l�    ~�  �   ��  V  k�  \  ³  <  �    \�  �   e�  R  Z�  Z  ��  �   �  �  ߻  `  ~�  �   ߾  �   ��  �   ��    (�  �   -�    �  �   �  �   ��  `  ��  �   5�  �  ��  �  ��  �  p�  S  V�  �   ��  I  W�  �   ��  Q  O�  ,  ��  �   ��    ��  �   ��    ��    ��  �   ��    ��    ��    ��  �  ��  j   ��  i   ��  i   Y�  h   ��  I  ,�    v�  �  ��  �   W�    Q�  �  a�  �   �  �   ��  �   ��  �   ��    ��     ��  �   ��  �   D�  �   ,�  N  �  @  g�  F  ��  9  ��  J  )�  �   t�  �   S�  �   2�    
�    �  �   )�  +  ��          �  < �    f  � ]  A 5  � D  �	 �   �  � �  o        0 @   P    �    � 8   � P   �    9 '   N 1   v    �    �    �    �    	 #    :   6 *   q    � (   � -   � 0       7    I    O Z   W .   �    �    �        :    X /   q    �    �    �    �    �        * '   I *   q 2   � 5   � )       /    H    b    }    �    �    � [   � X   N 7   � ?   � $       D A   R 9   � 1   � (        ) 
   F 
   Q 
   \    g .   ~ 3   � .   � 3       D    I    U    d L  �    �    �    �    �     #    !   9    [    h    {    �    �    �    �    �    �    �    �         $    
   @    K    e    t @   � A   � B    B   Q A   � B   � A    A   [ D   � B   � E   %     k  *   |  �   �  0   P! #   �! K   �!     �! �   "    �" 0   �" !   �"    # 7   3# F   k# 0   �#    �# �   �# �   �$ p   % �   �% m   & �   �& o   ' �   �' l   ( a   �( N   �( �   =) ;   �) 	   '*    1* A  O* `   �+ 9   �+ 0   ,, o   ], �   �, �   �- }   . �   �. �    / Z   �/ W   0 �   j0 :   21 *   m1 B   �1 �   �1 {   �2 �   -3 �   �3 {   F4 �   �4 }   p5 �   �5 {   �6 �   7 }   �7 �   F8 {   �8 �   r9     : �   �:    R; �   �; {   �< �    = E   �= F   �= �   ;> �  �> Q   �@ O   �@ �   1A U   *B    �B N   �B    �B    �B D   C 	   PC    ZC    bC 	   kC    uC    �C    �C    �C    �C    �C E   �C 0   +D +   \D &   �D 3   �D    �D @   E F   CE ?   �E /   �E @   �E 5   ;F V   qF H   �F e   G    wG    �G �   �G    VH    dH ?   ~H    �H 0   �H    �H    I    I    I 	   I    !I    -I    =I    CI    OI    TI 5   jI    �I A   �I 6   �I    *J    -J    2J 
   7J     BJ !   cJ    �J �  �J F  L   RM    YN    pN    �N �   �N �  tO �  Q �  �R (   uT    �T    �T    �T    �T    �T 0   �T E   (U I   nU L   �U /   V >   5V U   tV �   �V l   �W 8   �W ]   /X 7   �X ^   �X '   $Y *   LY D   wY &   �Y E   �Y o   )Z Y   �Z I   �Z S   =[ @   �[ r   �[ @   E\ g   �\ �   �\ �   �] �   S^ �   &_ �   �_ �   �` p   a Q   xa Y   �a j   $b �   �b 7   c    Rc    nc `   �c U   �c �   Ad j   �d �   Oe �   �e �   �f �   Yg *   Vh /   �h    �h    �h    �h h   i (   li ;   �i    �i    �i W   j    Yj    wj a   �j s   �j w   kk �   �k u   �l m   m w   �m P   n k   Sn E   �n ]   o [   co �   �o �   Jp 0   �p   q �   "r �   �r �   [s �   �s �   �t S   �u ]   �u    ;v    [v K  sv X   �w G   x F   `x �   �x \   5y #   �y T   �y    z G   z    `z B   uz    �z 2   �z �    { 0   �{ �   �{    n|    �| *   �| ^   �| ~   2} �   �} �   �~    O    k Y   � q   � &   S�    z� �   ��    +� e   <�    �� �   ��    ��    �� 0   �� "   �    �    )�    >�   U� 4   ]� 7   �� }   ʄ C   H�    �� &   �� @   ͅ    �    ,� 
   D� 2   O� =   �� <   �� 6   �� �   4� ,   � =   � i   \� �   ƈ �   �� -   ��    ��    ˊ %   �� B   � '   I� .   q�    �� W   �� .   � 0   C� �   t�    ��     � '   ;� #   c� p   �� C   �� �   <� &   ώ &   ��    �    ;�    Y�    w� M   �� U   � !   9� !   [� =   }�    ��    ϐ    ې 8   ��    1� 
   K� 1   V�    �� $   �� !   Ñ ,   �    � �   *� O   �� M   � �   R� 2   � 2   �     G�    h� �  w� �  � �  
� �  Ŝ z  O� "  ʣ s  �� �  a� �  %� �  �� 5  �� �  Ү J  �� �  � �  �� Q  {� �  ͷ L  �� 5  �� a  3� �  �� !  � h  =� E  ��   �� l  l� h  �� �  B� z  �� }  K� �  �� �  �� �  X� �  � �  �� �  b� g  � �  i� "  �� w  � |  �� u  � w  �� c  �� A  _ |  � �   �  �	 �  6 �  � �  � �  � �  E �  � �  � H  � �  �! �  �&   () I  C- �  �0 '  s2 �  �5 <  �8   �; Z  �> �  JC �  �G R  �I �  "L �  O �  �Q �  �S g  dU ^  �W �  +Z D  \ �  G_ l  �a �  Rc ;   e   \f �  eh   Aj �  Nl   �m ^  �o g  Qs �  �t �  vx )  &} �  P� �  � �  �� {  "� �  �� &  _� l  �� �  � '  �� N  �� U  �� �  T� �  S� X  ؞ D  1�   v�   ��   ��   ��   ҩ �  � �  �� �  �� �  � �  ո �  o� 	  f�   p� �  t� �  >� �  <� �  �� [  �� "  �� �  � a  � W  �� Y  �� L  2� a  � 9  �� 9  �   U� P  ]� �  �� �  �� �  1� -  "� 3  P� �  �� �  b� �  K� �  B� t  
 y     � �  	 =  � -   , ?   Z j   � D       J �   \ �   � -   � [   � j   ' 7   � D   � D    L   T 
   � B   � p   � T   ` 3   � l   � d   V �   � 0   U #   �    � �   � }   u ]   � ]   Q R   � T    [   W }   � H   1 L   z F   � G    G   V E   � g   � d   L g   � e    h    \   � G   E H   � I   � I      T   j  V   �  U   ! �   l! �   !" z   �" �   D# f   �#    ;$ �   X$ �   �$ Z   �% ]   �% 4   ;&    p&    &    �& .   �& A   �& F    ' A   g' F   �'    �' )   ( >   /( S   n( �  �( ,   R+    +    �+ #   �+ <   �+ [   , P   m,    �, )   �, '    - 3   (- ,   \- ,   �-     �- .   �-    .    &.    7.    H.    b. =   u.    �. U   �.    '/ (   ?/ q   h/ r   �/ s   M0 s   �0 r   51 s   �1 r   2 r   �2 u   3 s   x3 v   �3    c4 f   {4 .  �4 p   6 9   �6 �   �6 ~   i7 �   �7 K   �8 c   9 Q   �9 C   �9 x   : �   �: l   \;    �; $  �;   = �   >    �> �   �?   �@ �   �A   �B �   �C �   �D �   �E �  >F g   H    uH    �H 8  �H �   �J I   �K t   �K �   TL �  �L   �N �   �O �   lP   [Q �   {R �   aS w  T R   yU i   �U W   6V �  �V �   2X �   �X   �Y   �Z   �[   �\ �  
^ �   �_ '  a` �   �a )  ]b �   �c ~  zd �   �e "  �f �   h +  �h �   j   �j ~   l �   �l r  m �  �n �   ;r �   �r   [s �   vu >   !v �   `v 0   %w ,   Vw �   �w    x    0x )   Ex    ox #   �x ,   �x .   �x 5   y /   :y /   jy s   �y Y   z G   hz ;   �z ^   �z <   K{ \   �{ �   �{ �   �| }   &} {   �} p    ~ �   �~ �    �   � '   �� %   ŀ p  � B   \� 4   �� �   Ԃ #   �� S   Ճ    )�    H�    T�    h�    p�    |� '   �� 	   �� !   ń    �    � p   � 7   ~� A   �� g   ��    `�    g�    |�    �� H   �� C   �    *�    5      &  6  �    _  �           y    �  �  �      Y  �  �   �       K  �      S  x      5              �    �   �  o   �  f  ]  X   �   �  ;       �   \      �  <   q   �       �      �  P   �        �  �       C  �           e     �   �  {   T  B  �            ?  �       }   7  )  �  J   �   s      �   ;     �           4   �   �   �   �   t   �   �     �  �   _   )  �   $   0    �   A  �      !                   �   (  K  �     [  �  �          h                          O  7      �     *   $  �  �    �  �      O  �      �   �   �  :  -   d      �   (   �   �   7   >          :   �       �      �   \   �   W  <  �   )   �          �   /        V                  U   #  V      �      �  �   �   �                   B   l  6     �         �   �   �     n  �  �   �  �      2  |   �   a             �  �   �  #      a   U  !  %  �  ;     l   �     �      �   S   �      U          �   +   �   y   �       G  �      �  �   �                      �     �     x   �               9  u   �   �      �      /   �   �  M  ,  j   �   �  
  k  �          �   �               �   E      T   8  H  �   �  �             D  �      �       D  `   J  I   �  �  �   	  �   �   w  �   B  b   �      h   �   O         I  �      	   �      �      �  `      �  P                 &   �   �  '  �          2  �   v           H        R    F  �         6  .  �  ^     �  �   4  b  �     �  Q          �         $  �  	      �             D   �         Z  �       
      F   �     �      �   E      �  t  c       4  �   '   �   �  J    �  �      �   �   ~                      V        �     �  ~   =  j    M        "  .     �  �  �  d   �  c  �       z              ?   ,         c     R  N   �      �  v  Y                 �      �               @  /      q      �  �   A   �   �      �   T  �       3       H   �   �   �   �  �  -     �             _     &  g       G   i  R             �  �      �   '  `      �       �   �   F  ^  @   �  Z      �   �       "   !  �   (  �      �       �   Q            �  Y      f   o  =       �  *          I  a  �   @      �    ,   1  �      C          �      1      �  |  �      �  �  0   >  ?            �  �  �   �         �  �   �    \  �  �        
   �      �                   �  8   L  �  z           �   =      X  9   *      �      �   ]   �          �  .   3  �   :  �      8  r   L   �   M         Q   p   �     i   P      �  A  W   �       �  g      S  n     �       ]  W  2   �  +      �      %  �               �  �       �  b  %   N      3  <  X          �           +              �              �   �   w   Z              �  �    E                 r        �          �  m   �            �   �  �   �      �   9  �  �   5         K   �   m      G      0        }  �  "  �      C   �  �           p  u  s      L    �   ^   N  [   >   e   1       �      [  {  -               �  k   �   #       

Warning: Some tests could cause your system to freeze or become unresponsive. Please save all your work and close all other running applications before beginning the testing process.    Determine if we need to run tests specific to portable computers that may not apply to desktops.  Results   Run   Selection   Takes multiple pictures based on the resolutions supported by the camera and
 validates their size and that they are of a valid format.  Test that the system's wireless hardware can connect to a router using the
 802.11a protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11a protocol.  Test that the system's wireless hardware can connect to a router using the
 802.11b protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11b protocol.  Test that the system's wireless hardware can connect to a router using the
 802.11g protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11g protocol. %(key_name)s key has been pressed &No &Previous &Skip this test &Test &Yes 10 tests completed out of 30 (30%) Abort signal from abort(3) All required keys have been tested! Archives the piglit-summary directory into the piglit-results.tar.gz. Are you sure? Attach log from fwts wakealarm test Attaches a copy of /var/log/dmesg to the test results Attaches a dump of the udev database showing system hardware information. Attaches a list of the currently running kernel modules. Attaches a report of CPU information Attaches a report of installed codecs for Intel HDA Attaches a report of sysfs attributes. Attaches a tarball of gcov data if present. Attaches dmidecode output Attaches info on DMI Attaches information about disk partitions Attaches lshw output Attaches the FWTS results log to the submission Attaches the audio hardware data collection log to the results. Attaches the bootchart log for bootchart test runs. Attaches the bootchart png file for bootchart runs Attaches the contents of /proc/acpi/sleep if it exists. Attaches the contents of the /etc/modules file. Attaches the contents of the various modprobe conf files. Attaches the firmware version Attaches the graphics stress results to the submission. Attaches the installer debug log if it exists. Attaches the log from the 250 cycle Hibernate/Resume test if it exists Attaches the log from the 250 cycle Suspend/Resume test if it exists Attaches the log from the 30 cycle Hibernate/Resume test if it exists Attaches the log from the 30 cycle Suspend/Resume test if it exists Attaches the log from the single suspend/resume test to the results Attaches the log generated by cpu/scaling_test to the results Attaches the output of udev_resource, for debugging purposes Attaches the screenshot captured in graphics/screenshot. Attaches the screenshot captured in graphics/screenshot_fullscreen_video. Attaches very verbose lspci output (with central database Query). Attaches very verbose lspci output. Audio Test Audio tests Automated CD write test Automated DVD write test. Automated check of the hibernate log for errors discovered by fwts Automated optical read test. Automated test case to make sure that it's possible to download files through HTTP Automated test case to verify availability of some system on the network using ICMP ECHO packets. Automated test to store bluetooth device information in checkbox report Automated test to walk multiple network cards and test each one in sequence. Benchmark for each disk Benchmarks tests Bluetooth Test Bluetooth tests Bootchart information. Broken pipe: write to pipe with no readers Building report... CD write test. CPU Test CPU tests CPU utilization on an idle system. Camera Test Camera tests Check failed result from shell test case Check job is executed when dependency succeeds Check job is executed when requirements are met Check job result is set to "not required on this system" when requirements are not met Check job result is set to uninitiated when dependency fails Check logs for the stress poweroff (100 cycles) test case Check logs for the stress reboot (100 cycles) test case Check stats changes for each disk Check success result from shell test case Check that VESA drivers are not in use Check that hardware is able to run Unity 3D Check that hardware is able to run compiz Check the time needed to reconnect to a WIFI access point Check to see if CONFIG_NO_HZ is set in the kernel (this is just a simple regression check) Checkbox System Testing Checkbox did not finish completely.
Do you want to rerun the last test,
continue to the next test, or
restart from the beginning? Checks that a specified sources list file contains the requested repositories Checks the battery drain during idle.  Reports time until empty Checks the battery drain during suspend.  Reports time until Checks the battery drain while watching a movie.  Reports time Checks the length of time it takes to reconnect an existing wifi connection after a suspend/resume cycle. Child stopped or terminated Choose tests to run on your system: Codec tests Collapse All Collect audio-related system information. This data can be used to simulate this computer's audio subsystem and perform more detailed tests under a controlled environment. Collect info on color depth and pixel format. Collect info on fresh rate. Collect info on graphic memory. Collect info on graphics modes (screen resolution and refresh rate) Combine with character above to expand node Command not found. Command received signal %(signal_name)s: %(signal_description)s Comments Common Document Types Test Components Configuration override parameters. Continue Continue if stopped Creates a mobile broadband connection for a CDMA based modem and checks the connection to ensure it's working. DVD write test. Dependencies are missing so some jobs will not run. Deselect All Deselect all Detailed information... Detects and displays disks attached to the system. Detects and shows USB devices attached to this system. Determine whether the touchpad is detected as a multitouch device automatically. Determine whether the touchpad is detected as a singletouch device automatically. Disk Test Disk tests Disk utilization on an idle system. Do you really want to skip this test? Don't ask me again Done Dumps memory info to a file for comparison after suspend test has been run Email Email address must be in a proper format. Email: Ensure the current resolution meets or exceeds the recommended minimum resolution (800x600). See here for details: Enter text:
 Error Exchanging information with the server... Executing %(test_name)s Expand All ExpressCard Test ExpressCard tests Failed to contact server. Please try
again or upload the following file name:
%s

directly to the system database:
https://launchpad.net/+hwdb/+submit Failed to open file '%s': %s Failed to process form: %s Failed to upload to server,
please try again later. Fingerprint reader tests Firewire Test Firewire disk tests Floating point exception Floppy disk tests Floppy test Form Further information: Gathering information from your system... Graphics Test Graphics tests Hangup detected on controlling terminal or death of controlling process Hibernation tests Hotkey tests I will exit automatically once all keys have been pressed. If a key is not present in your keyboard, press the 'Skip' button below it to remove it from the test. If your keyboard lacks one or more keys, press its number to skip testing that key. Illegal Instruction In Progress Info Info Test Information not posted to Launchpad. Informational tests Input Devices tests Input Test Internet connection fully established Interrupt from keyboard Invalid memory reference Jobs will be reordered to fix broken dependencies Keys Test Kill signal LED tests List USB devices Lists the device driver and version for all audio devices. Make sure that the RTC (Real-Time Clock) device exists. Maximum disk space used during a default installation test Media Card Test Media Card tests Memory Test Memory tests Miscellanea Test Miscellaneous tests Missing configuration file as argument.
 Mobile broadband tests Monitor Test Monitor tests Move a 3D window around the screen Ne&xt Ne_xt Network Information Networking Test Networking tests Next No Internet connection Not Resolved Not Started Not Supported Not Tested Not required One of debug, info, warning, error or critical. Open and close 4 3D windows multiple times Open and close a 3D window multiple times Open, suspend resume and close a 3D window multiple times Optical Drive tests Optical Test Optical read test. PASSWORD:  PURPOSE:
     Check that external line out connection works correctly
STEPS:
     1. Insert cable to speakers (with built-in amplifiers) on the line out port
     2. Open system sound preferences, 'Output' tab, select 'Line-out' on the connector list. Click the Test button
     3. On the system sound preferences, select 'Internal Audio' on the device list and click 'Test Speakers' to check left and right channel
VERIFICATION:
     1. Do you hear a sound in the speakers? The internal speakers should *not* be muted automatically
     2. Do you hear the sound coming out on the corresponding channel? PURPOSE:
    Block cap keys LED verification
STEPS:
    1. Press "Block Cap Keys" to activate/deactivate cap keys blocking
    2. Cap Keys LED should be switched on/off every time the key is pressed
VERIFICATION:
    Did the Cap Keys LED light as expected? PURPOSE:
    Camera LED verification
STEPS:
    1. Select Test to activate camera
    2. Camera LED should light for a few seconds
VERIFICATION:
    Did the camera LED light? PURPOSE:
    Check that balance control works correctly on external headphone
STEPS:
    1. Check that moving the balance slider from left to right works smoothly
    2. Click the Test button to play an audio tone for 10 seconds.
    3. Move the balance slider from left to right and back.
    4. Check that actual headphone audio balance follows your setting.
VERIFICATION:
    Does the slider move smoothly, as well as being followed by the setting by the actual audio output? PURPOSE:
    Check that balance control works correctly on internal speakers
STEPS:
    1. Check that moving the balance slider from left to right works smoothly
    2. Click the Test button to play an audio tone for 10 seconds.
    3. Move the balance slider from left to right and back.
    4. Check that actual speaker audio balance follows your setting.
VERIFICATION:
    Does the slider move smoothly, as well as being followed by the setting by the actual audio output? PURPOSE:
    Check that external line in connection works correctly
STEPS:
    1. Use a cable to connect the line in port to an external line out source.
    2. Open system sound preferences, 'Input' tab, select 'Line-in' on the connector list. Click the Test button
    3. After a few seconds, your recording will be played back to you.
VERIFICATION:
    Did you hear your recording? PURPOSE:
    Check that the various audio channels are working properly
STEPS:
    1. Click the Test button
VERIFICATION:
    You should clearly hear a voice from the different audio channels PURPOSE:
    Check touchscreen drag & drop
STEPS:
    1. Double tap, hold, and drag an object on the desktop
    2. Drop the object in a different location
VERIFICATION:
    Does the object select and drag and drop? PURPOSE:
    Check touchscreen pinch gesture for zoom
STEPS:
    1. Place two fingers on the screen and pinch them together
    2. Place two fingers on the screen and move then apart
VERIFICATION:
    Does the screen zoom in and out? PURPOSE:
    Check touchscreen tap recognition
STEPS:
    1. Tap an object on the screen with finger. The cursor should jump to location tapped and object should highlight
VERIFICATION:
    Does tap recognition work? PURPOSE:
    Create jobs that use the CPU as much as possible for two hours. The test is considered passed if the system does not freeze. PURPOSE:
    DisplayPort audio interface verification
STEPS:
    1. Plug an external DisplayPort device with sound (Use only one HDMI/DisplayPort interface at a time for this test)
    2. Click the Test button
VERIFICATION:
    Did you hear the sound from the DisplayPort device? PURPOSE:
    HDD LED verification
STEPS:
    1. Select "Test" to write and read a temporary file for a few seconds
    2. HDD LED should light when writing to/reading from HDD
VERIFICATION:
    Did the HDD LED light? PURPOSE:
    HDMI audio interface verification
STEPS:
    1. Plug an external HDMI device with sound (Use only one HDMI/DisplayPort interface at a time for this test)
    2. Click the Test button
VERIFICATION:
    Did you hear the sound from the HDMI device? PURPOSE:
    Keep tester related information in the report
STEPS:
    1. Tester Information
    2. Please enter the following information in the comments field:
       a. Name
       b. Email Address
       c. Reason for this test run
VERIFICATION:
    Nothing to verify for this test PURPOSE:
    Manual detection of accelerometer.
STEPS:
    1. Look at the specifications for your system.
VERIFICATION:
    Is this system supposed to have an accelerometer? PURPOSE:
    Numeric keypad LED verification
STEPS:
    1. Press "Block Num" key to toggle numeric keypad LED
    2. Click on the "Test" button to open a window to verify your typing
    3. Type using the numeric keypad both when the LED is on and off
VERIFICATION:
    1. Numeric keypad LED status should toggle everytime the "Block Num" key is pressed
    2. Numbers should only be entered in the keyboard verification window when the LED is on PURPOSE:
    Power LED verification
STEPS:
    1. Power LED should be on while device is switched on
VERIFICATION:
    Does the power LED light as expected? PURPOSE:
    Power LED verification
STEPS:
    1. The Power LED should blink or change color while the system is suspended
VERIFICATION:
    Did the Power LED blink or change color while the system was suspended for the previous suspend test? PURPOSE:
    Take a screengrab of the current screen (logged on Unity desktop)
STEPS:
    1. Take picture using USB webcam
VERIFICATION:
    Review attachment manually later PURPOSE:
    Take a screengrab of the current screen after suspend (logged on Unity desktop)
STEPS:
    1. Take picture using USB webcam
VERIFICATION:
    Review attachment manually later PURPOSE:
    Take a screengrab of the current screen during fullscreen video playback
STEPS:
    1. Start a fullscreen video playback
    2. Take picture using USB webcam after a few seconds
VERIFICATION:
    Review attachment manually later PURPOSE:
    Tests that the systems wireless hardware can connect to a router using
    no security and the 802.11b/g protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the B and G wireless bands
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use no security
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using
    no security and the 802.11n protocol.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the N wireless band
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use no security
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using WPA
    security and the 802.11b/g protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the B and G wireless bands
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use WPA2 and ensure the PSK matches that set in ROUTER_PSK
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using WPA
    security and the 802.11n protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the N wireless band
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use WPA2 and ensure the PSK matches that set in ROUTER_PSK
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    This test checks that the manual plugin works fine
STEPS:
    1. Add a comment
    2. Set the result as passed
VERIFICATION:
    Check that in the report the result is passed and the comment is displayed PURPOSE:
    This test cycles through the detected video modes
STEPS:
    1. Click "Test" to start cycling through the video modes
VERIFICATION:
    Did the screen appear to be working for each mode? PURPOSE:
    This test tests the basic 3D capabilities of your video card
STEPS:
    1. Click "Test" to execute an OpenGL demo. Press ESC at any time to close.
    2. Verify that the animation is not jerky or slow.
VERIFICATION:
    1. Did the 3d animation appear?
    2. Was the animation free from slowness/jerkiness? PURPOSE:
    This test will check that a DSL modem can be configured and connected.
STEPS:
    1. Connect the telephone line to the computer
    2. Click on the Network icon on the top panel.
    3. Select "Edit Connections"
    4. Select the "DSL" tab
    5. Click on "Add" button
    6. Configure the connection parameters properly
    7. Click "Test" to verify that it's possible to establish an HTTP connection
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will check that a USB audio device works correctly
STEPS:
    1. Connect a USB audio device to your system
    2. Click "Test", then speak into the microphone
    3. After a few seconds, your speech will be played back to you
VERIFICATION:
    Did you hear your speech played back through the USB headphones? PURPOSE:
    This test will check that bluetooth connection works correctly
STEPS:
    1. Enable bluetooth on any mobile device (PDA, smartphone, etc.)
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. In the device write the PIN code automatically chosen by the wizard
    6. The device should pair with the computer
    7. Right-click on the bluetooth icon and select browse files
    8. Authorize the computer to browse the files in the device if needed
    9. You should be able to browse the files
VERIFICATION:
    Did all the steps work? PURPOSE:
    This test will check that headphones connector works correctly
STEPS:
    1. Connect a pair of headphones to your audio device
    2. Click the Test button to play a sound to your audio device
VERIFICATION:
    Did you hear a sound through the headphones and did the sound play without any distortion, clicks or other strange noises from your headphones? PURPOSE:
    This test will check that internal speakers work correctly
STEPS:
    1. Make sure that no external speakers or headphones are connected
       If testing a desktop, external speakers are allowed
    2. Click the Test button to play a brief tone on your audio device
VERIFICATION:
    Did you hear a tone? PURPOSE:
    This test will check that recording sound using an external microphone works correctly
STEPS:
    1. Connect a microphone to your microphone port
    2. Click "Test", then speak into the external microphone
    3. After a few seconds, your speech will be played back to you
VERIFICATION:
    Did you hear your speech played back? PURPOSE:
    This test will check that recording sound using the onboard microphone works correctly
STEPS:
    1. Disconnect any external microphones that you have plugged in
    2. Click "Test", then speak into your internal microphone
    3. After a few seconds, your speech will be played back to you.
VERIFICATION:
    Did you hear your speech played back? PURPOSE:
    This test will check that the built-in camera works
STEPS:
    1. Click on Test to display a still image from the camera for ten seconds.
VERIFICATION:
    Did you see the image? PURPOSE:
    This test will check that the built-in camera works
STEPS:
    1. Click on Test to display a video capture from the camera for ten seconds.
VERIFICATION:
    Did you see the video capture? PURPOSE:
    This test will check that the display is correct after suspend and resume
STEPS:
    1. Check that your display does not show up visual artifacts after resuming.
VERIFICATION:
    Does the display work normally after resuming from suspend? PURPOSE:
    This test will check that the system can switch to a virtual terminal and back to X
STEPS:
    1. Click "Test" to switch to another virtual terminal and then back to X
VERIFICATION:
    Did your screen change temporarily to a text console and then switch back to your current session? PURPOSE:
    This test will check that the system correctly detects
    the removal of a CF card from the systems card reader.
STEPS:
    1. Click "Test" and remove the CF card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a MS card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MS card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a MSP card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MSP card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a SDXC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SDXC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a xD card from the systems card reader.
STEPS:
    1. Click "Test" and remove the xD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of an SD card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of an SDHC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SDHC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of the MMC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MMC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the insertion of
    a USB storage device
STEPS:
    1. Click "Test" and insert a USB storage device (pen-drive/HDD).
       (Note: this test will time-out after 20 seconds.)
    2. Do not unplug the device after the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a CF card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the CF card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a MS card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MS card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a MSP card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MSP card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a xD card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the xD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of an MMC card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MMC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of an SDHC card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the SDHC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal of
    a USB storage device
STEPS:
    1. Click "Test" and remove the USB device.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that you can record and hear audio using a bluetooth audio device
STEPS:
    1. Enable the bluetooth headset
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. In the device write the PIN code automatically chosen by the wizard
    6. The device should pair with the computer
    7. Click "Test" to record for five seconds and reproduce in the bluetooth device
VERIFICATION:
    Did you hear the sound you recorded in the bluetooth PURPOSE:
    This test will check that you can transfer information through a bluetooth connection
STEPS:
    1. Make sure that you're able to browse the files in your mobile device
    2. Copy a file from the computer to the mobile device
    3. Copy a file from the mobile device to the computer
VERIFICATION:
    Were all files copied correctly? PURPOSE:
    This test will check that you can use a BlueTooth HID device
STEPS:
    1. Enable either a BT mouse or keyboard
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. For mice, perform actions such as moving the pointer, right and left button clicks and double clicks
    6. For keyboards, click the Test button to lauch a small tool. Enter some text into the tool and close it.
VERIFICATION:
    Did the device work as expected? PURPOSE:
    This test will check that you can use a USB HID device
STEPS:
    1. Enable either a USB mouse or keyboard
    2. For mice, perform actions such as moving the pointer, right and left button clicks and double clicks
    3. For keyboards, click the Test button to lauch a small tool. Type some text and close the tool.
VERIFICATION:
    Did the device work as expected? PURPOSE:
    This test will check that your system detects USB storage devices.
STEPS:
    1. Plug in one or more USB keys or hard drives.
    2. Click on "Test".
INFO:
    $output
VERIFICATION:
    Were the drives detected? PURPOSE:
    This test will check the system can detect the insertion of a FireWire HDD
STEPS:
    1. Click 'Test' to begin the test. This test will
       timeout and fail if the insertion has not been detected within 20 seconds.
    2. Plug a FireWire HDD into an available FireWire port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the insertion of an eSATA HDD
STEPS:
    1. Click 'Test' to begin the test. This test will
       timeout and fail if the insertion has not been detected within 20 seconds.
    2. Plug an eSATA HDD into an available eSATA port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the removal of a FireWire HDD
STEPS:
    1. Click 'Test' to begin the test. This test will timeout and fail if
       the removal has not been detected within 20 seconds.
    2. Remove the previously attached FireWire HDD from the FireWire port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the removal of an eSATA HDD
STEPS:
    1. Click 'Test' to begin the test. This test will timeout and fail if
       the removal has not been detected within 20 seconds.
    2. Remove the previously attached eSATA HDD from the eSATA port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check to make sure your system can successfully hibernate (if supported)
STEPS:
    1. Click on Test
    2. The system will hibernate and should wake itself within 5 minutes
    3. If your system does not wake itself after 5 minutes, please press the power button to wake the system manually
    4. If the system fails to resume from hibernate, please restart System Testing and mark this test as Failed
VERIFICATION:
    Did the system successfully hibernate and did it work properly after waking up? PURPOSE:
    This test will check your CD audio playback capabilities
STEPS:
    1. Insert an audio CD in your optical drive
    2. When prompted, launch the Music Player
    3. Locate the CD in the display of the Music Player
    4. Select the CD in the Music Player
    5. Click the Play button to listen to the music on the CD
    6. Stop playing after some time
    7. Right click on the CD icon and select "Eject Disc"
    8. The CD should be ejected
    9. Close the Music Player
VERIFICATION:
    Did all the steps work? PURPOSE:
    This test will check your DVD  playback capabilities
STEPS:
    1. Insert a DVD that contains any movie in your optical drive
    2. Click "Test" to play the DVD in Totem
VERIFICATION:
    Did the file play? PURPOSE:
    This test will check your USB 3.0 connection.
STEPS:
    1. Plug a USB 3.0 HDD or thumbdrive into a USB 3.0 port in the computer.
    2. An icon should appear on the Launcher.
    3. Click "Test" to begin the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check your USB connection.
STEPS:
    1. Connect a USB storage device to an external USB slot on this computer.
    2. An icon should appear on the Launcher.
    3. Confirm that the icon appears.
    4. Eject the device.
    5. Repeat with each external USB slot.
VERIFICATION:
    Do all USB slots work with the device? PURPOSE:
    This test will check your USB connection.
STEPS:
    1. Plug a USB HDD or thumbdrive into the computer.
    2. An icon should appear on the Launcher.
    3. Click "Test" to begin the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check your monitor power saving capabilities
STEPS:
    1. Click "Test" to try the power saving capabilities of your monitor
    2. Press any key or move the mouse to recover
VERIFICATION:
    Did the monitor go blank and turn on again? PURPOSE:
    This test will check your system shutdown/booting cycle.
STEPS:
    1. Shutdown your machine.
    2. Boot your machine.
    3. Repeat steps 1 and 2 at least 5 times.
VERIFICATION:
    Did the system shutdown and rebooted correctly? PURPOSE:
    This test will check your wired connection
STEPS:
    1. Click on the Network icon in the top panel
    2. Select a network below the "Wired network" section
    3. Click "Test" to verify that it's possible to establish a HTTP connection
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will check your wireless connection.
STEPS:
    1. Click on the Network icon in the panel.
    2. Select a network below the 'Wireless networks' section.
    3. Click "Test" to verify that it's possible to establish an HTTP connection.
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will cycle through the detected display modes
STEPS:
    1. Click "Test" and the display will cycle trough the display modes
VERIFICATION:
    Did your display look fine in the detected mode? PURPOSE:
    This test will send the image 'JPEG_Color_Image_Ubuntu.jpg' to a specified device
STEPS:
    1. Click "Test" and you will be prompted to enter the Bluetooth device name of a device that can accept file transfers (It may take a few moments after entering the name for the file to begin sending)
    2. Accept any prompts that appear on both devices
VERIFICATION:
    Was the data correctly transferred? PURPOSE:
    This test will test display rotation
STEPS:
    1. Click "Test" to test display rotation. The display will be rotated every 4 seconds.
    2. Check if all rotations (normal right inverted left) took place without permanent screen corruption
VERIFICATION:
    Did the display rotation take place without without permanent screen corruption? PURPOSE:
    This test will test the brightness key
STEPS:
    1. Press the brightness buttons on the keyboard
VERIFICATION:
    Did the brightness change following to your key presses? PURPOSE:
    This test will test the brightness key after resuming from suspend
STEPS:
    1. Press the brightness buttons on the keyboard
VERIFICATION:
    Did the brightness change following to your key presses after resuming from suspend? PURPOSE:
    This test will test the default display
STEPS:
    1. Click "Test" to display a video test.
VERIFICATION:
    Do you see color bars and static? PURPOSE:
    This test will test the mute key of your keyboard
STEPS:
    1. Click test to open a window on which to test the mute key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Does the mute key work as expected? PURPOSE:
    This test will test the sleep key
STEPS:
    1. Press the sleep key on the keyboard
    2. Wake your system up by pressing the power button
VERIFICATION:
    Did the system go to sleep after pressing the sleep key? PURPOSE:
    This test will test the super key of your keyboard
STEPS:
    1. Click test to open a window on which to test the super key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Does the super key work as expected? PURPOSE:
    This test will test your keyboard
STEPS:
    1. Click on Test
    2. On the open text area, use your keyboard to type something
VERIFICATION:
    Is your keyboard working properly? PURPOSE:
    This test will test your pointing device
STEPS:
    1. Move the cursor using the pointing device or touch the screen.
    2. Perform some single/double/right click operations.
VERIFICATION:
    Did the pointing device work as expected? PURPOSE:
    This test will verify that the GUI is usable after manually changing resolution
STEPS:
    1. Open the Displays application
    2. Select a new resolution from the dropdown list
    3. Click on Apply
    4. Select the original resolution from the dropdown list
    5. Click on Apply
VERIFICATION:
    Did the resolution change as expected? PURPOSE:
    This test will verify the default display resolution
STEPS:
    1. This display is using the following resolution:
INFO:
    $output
VERIFICATION:
    Is this acceptable for your display? PURPOSE:
    To make sure that stressing the wifi hotkey does not cause applets to disappear from the panel or the system to lock up
STEPS:
    1. Log in to desktop
    2. Press wifi hotkey at a rate of 1 press per second and slowly increase the speed of the tap, until you are tapping as fast as possible
VERIFICATION:
    Verify the system is not frozen and the wifi and bluetooth applets are still visible and functional PURPOSE:
    Touchpad LED verification
STEPS:
    1. Click on the touchpad button or press key combination to enable/disable touchpad button
    2. Slide your finger on the touchpad
VERIFICATION:
    1. Touchpad LED status should toggle everytime the button is clicked or the key combination is pressed
    2. When the LED is on, the mouse pointer should move on touchpad usage
    3. When the LED is off, the mouse pointer should not move on touchpad usage PURPOSE:
    Touchpad LED verification after resuming from suspend
STEPS:
    1. Click on the touchpad button or press key combination to enable/disable touchpad button
    2. Slide your finger on the touchpad
VERIFICATION:
    1. Touchpad LED status should toggle everytime the button is clicked or the key combination is pressed
    2. When the LED is on, the mouse pointer should move on touchpad usage
    3. When the LED is off, the mouse pointer should not move on touchpad usage PURPOSE:
    Touchpad horizontal scroll verification
STEPS:
    1. Select "Test" when ready and place your cursor within the borders of the displayed test window.
    2. Verify that you can move the horizontal slider by moving your finger right and left in the lower part of the touchpad.
VERIFICATION:
    Could you scroll right and left? PURPOSE:
    Touchpad manual detection of multitouch.
STEPS:
    1. Look at the specifications for your system.
VERIFICATION:
    Is the touchpad supposed to be multitouch? PURPOSE:
    Touchpad vertical scroll verification
STEPS:
    1. Select "Test" when ready and place your cursor within the borders of the displayed test window.
    2. Verify that you can move the vertical slider by moving your finger up and down in the right part of the touchpad.
VERIFICATION:
    Could you scroll up and down? PURPOSE:
    Touchscreen manual detection of multitouch.
STEPS:
    1. Look at the specifications for your system.
VERIFICATION:
    Is the screen supposed to be multitouch? PURPOSE:
    Validate that the Bluetooth LED turns on and off when BT is enabled/disabled
STEPS:
    1. Switch bluetooth off from a hardware switch (if present)
    2. Switch bluetooth back on
    3. Switch bluetooth off from the panel applet
    4. Switch bluetooth back on
VERIFICATION:
    Did the bluetooth LED turn off and on twice? PURPOSE:
    Validate that the External Video hot key is working as expected
STEPS:
    1. Plug in an external monitor
    2. Press the display hot key to change the monitors configuration
VERIFICATION:
    Check that the video signal can be mirrored, extended, displayed on external or onboard only. PURPOSE:
    Validate that the battery LED indicated low power
STEPS:
    1. Let system run on battery for several hours
    2. Monitor battery LED carefully
VERIFICATION:
    Does the LED light orange when battery is low? PURPOSE:
    Validate that the battery LED properly displays charged status
STEPS:
    1. Let system run on battery for a short time
    2. Plug in AC
    3. Let system run on AC
VERIFICATION:
    Does the orange battery LED shut off when system is fully charged? PURPOSE:
    Validate that the battery light shows charging status
STEPS:
    1. Let system run on battery for a while
    2. Plug in AC plug
VERIFICATION:
    Did the battery indicator LED turn orange? PURPOSE:
    Validate that the battery light shows charging status after resuming from suspend
STEPS:
    1. Let system run on battery for a while
    2. Plug in AC plug
VERIFICATION:
    Did the battery indicator LED still turn orange after resuming from suspend? PURPOSE:
    Validate that the camera LED still works as expected after resuming from suspend
STEPS:
    1. Select Test to activate camera
    2. Camera LED should light for a few seconds
VERIFICATION:
    Did the camera LED still turn on and off after resuming from suspend? PURPOSE:
    Validate that the power LED operated the same after resuming from suspend
STEPS:
    1. Power LED should be on while device is switched on
VERIFICATION:
    Does the power LED remain on after resuming from suspend? PURPOSE:
    Verify touchpad hotkey toggles touchpad functionality on and off
STEPS:
    1. Verify the touchpad is functional
    2. Tap the touchpad toggle hotkey
    3. Tap the touchpad toggle hotkey again
VERIFICATION:
    Verify the touchpad has been disabled and re-enabled. PURPOSE:
    Wake up by USB keyboard
STEPS:
    1. Enable "Wake by USB KB/Mouse" item in BIOS
    2. Press "Test" to enter suspend (S3) mode
    3. Press any key of USB keyboard to wake system up
VERIFICATION:
    Did the system wake up from suspend mode when you pressed a keyboard key? PURPOSE:
    Wake up by USB mouse
STEPS:
    1. Enable "Wake by USB KB/Mouse" item in BIOS
    2. Press "Test" to enter suspend (S3) mode
    3. Press any button of USB mouse to wake system up
VERIFICATION:
    Did the system wake up from suspend mode when you pressed the mouse button? PURPOSE:
    Wireless (WLAN + Bluetooth) LED verification
STEPS:
    1. Make sure WLAN connection is established and Bluetooth is enabled.
    2. WLAN/Bluetooth LED should light
    3. Switch WLAN and Bluetooth off from a hardware switch (if present)
    4. Switch them back on
    5. Switch WLAN and Bluetooth off from the panel applet
    6. Switch them back on
VERIFICATION:
    Did the WLAN/Bluetooth LED light as expected? PURPOSE:
   This is an automated stress test that will force the system to hibernate/resume for 250 cycles PURPOSE:
   This is an automated stress test that will force the system to hibernate/resume for 30 cycles PURPOSE:
   This is an automated stress test that will force the system to suspend/resume for 250 cycles. PURPOSE:
   This is an automated stress test that will force the system to suspend/resume for 30 cycles. PURPOSE:
   This test will verify that a USB DSL or Mobile Broadband modem works
STEPS:
   1. Connect the USB cable to the computer
   2. Right click on the Network icon in the panel
   3. Select 'Edit Connections'
   4. Select the 'DSL' (for ADSL modem) or 'Mobile Broadband' (for 3G modem) tab
   5. Click on 'Add' button
   6. Configure the connection parameters properly
   7. Notify OSD should confirm that the connection has been established
   8. Select Test to verify that it's possible to establish an HTTP connection
VERIFICATION:
   Was the connection correctly established? PURPOSE:
   This test will verify that a fingerprint reader can be used to unlock a locked system.
STEPS:
   1. Click on the Session indicator (Cog icon on the Left side of the panel) .
   2. Select 'Lock screen'.
   3. Press any key or move the mouse.
   4. A window should appear that provides the ability to unlock either typing your password or using fingerprint authentication.
   5. Use the fingerprint reader to unlock.
   6. Your screen should be unlocked.
VERIFICATION:
   Did the authentication procedure work correctly? PURPOSE:
   This test will verify that a network printer is usable
STEPS:
   1. Make sure that a printer is available in your network
   2. Click on the Gear icon in the upper right corner and then click on Printers
   3. If the printer isn't already listed, click on Add
   4. The printer should be detected and proper configuration values  should be displayed
   5. Print a test page
VERIFICATION:
   Were you able to print a test page to the network printer? PURPOSE:
   This test will verify that the desktop clock displays the correct date and time
STEPS:
   1. Check the clock in the upper right corner of your desktop.
VERIFICATION:
   Is the clock displaying the correct date and time for your timezone? PURPOSE:
   This test will verify that the desktop clock synchronizes with the system clock.
STEPS:
   1. Click the "Test" button and verify the clock moves ahead by 1 hour.
   Note: It may take a minute or so for the clock to refresh
   2. Right click on the clock, then click on "Time & Date Settings..."
   3. Ensure that your clock application is set to manual.
   4. Change the time 1 hour back
   5. Close the window and reboot
VERIFICATION:
   Is your system clock displaying the correct date and time for your timezone? PURPOSE:
   This test will verify that you can reboot your system from the desktop menu
STEPS:
   1. Click the Gear icon in the upper right corner of the desktop and click on "Shut Down"
   2. Click the "Restart" button on the left side of the Shut Down dialog
   3. After logging back in, restart System Testing and it should resume here
VERIFICATION:
   Did your system restart and bring up the GUI login cleanly? PURPOSE:
   This test will verify your system's ability to play Ogg Vorbis audio files.
STEPS:
   1. Click Test to play an Ogg Vorbis file (.ogg)
   2. Please close the player to proceed.
VERIFICATION:
   Did the sample play correctly? PURPOSE:
   This test will verify your system's ability to play Wave Audio files.
STEPS:
   1. Select Test to play a Wave Audio format file (.wav)
   2. Please close the player to proceed.
VERIFICATION:
   Did the sample play correctly? PURPOSE:
 Simulates a failure by rebooting the machine
STEPS:
 1. Click test to trigger a reboot
 2. Select "Continue" once logged back in and checkbox is restarted
VERIFICATION:
 You won't see the user-verify PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
  1. Cut
  2. Copy
  3. Paste
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
  1. Memory set
  2. Memory reset
  3. Memory last clear
  4. Memory clear
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
 1. Simple math functions (+,-,/,*)
 2. Nested math functions ((,))
 3. Fractional math
 4. Decimal math
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator.
VERIFICATION:
 Did it launch correctly? PURPOSE:
 This test checks that gedit works.
STEPS:
 1. Click the "Test" button to open gedit, and re-open the file you created previously.
 2. Edit then save the file, then close gedit.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test checks that gedit works.
STEPS:
 1. Click the "Test" button to open gedit.
 2. Enter some text and save the file (make a note of the file name you use), then close gedit.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the AOL Instant Messaging (AIM) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Facebook Chat service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Google Talk (gtalk) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Jabber service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Microsoft Network (MSN) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a IMAP account.
VERIFICATION:
 Were you able to receive and read e-mail correctly? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a POP3 account.
VERIFICATION:
 Were you able to receive and read e-mail correctly? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a SMTP account.
VERIFICATION:
 Were you able to send e-mail without errors? PURPOSE:
 This test will check that Firefox can play a Flash video. Note: this may
 require installing additional software to successfully complete.
STEPS:
 1. Select Test to launch Firefox and view a short flash video.
VERIFICATION:
 Did the video play correctly? PURPOSE:
 This test will check that Firefox can play a Quicktime (.mov) video file.
 Note: this may require installing additional software to successfully
 complete.
STEPS:
 1. Select Test to launch Firefox with a sample video.
VERIFICATION:
 Did the video play using a plugin? PURPOSE:
 This test will check that Firefox can render a basic web page.
STEPS:
 1. Select Test to launch Firefox and view the test web page.
VERIFICATION:
 Did the Ubuntu Test page load correctly? PURPOSE:
 This test will check that Firefox can run a java applet in a web page. Note:
 this may require installing additional software to complete successfully.
STEPS:
 1. Select Test to open Firefox with the Java test page, and follow the instructions there.
VERIFICATION:
 Did the applet display? PURPOSE:
 This test will check that Firefox can run flash applications. Note: this may
 require installing additional software to successfully complete.
STEPS:
 1. Select Test to launch Firefox and view a sample Flash test.
VERIFICATION:
 Did you see the text? PURPOSE:
 This test will check that Gnome Terminal works.
STEPS:
 1. Click the "Test" button to open Terminal.
 2. Type 'ls' and press enter. You should see a list of files and folder in your home directory.
 3. Close the terminal window.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test will check that the file browser can copy a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the file called Test File 1 and click Copy.
 3. Right click in the white space and click Paste.
 4. Right click on the file called Test File 1(copy) and click Rename.
 5. Enter the name Test File 2 in the name box and hit Enter.
 6. Close the File Browser.
VERIFICATION:
 Do you now have a file called Test File 2? PURPOSE:
 This test will check that the file browser can copy a folder
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the folder called Test Folder and click on Copy.
 3. Right Click on any white area in the window and click on Paste.
 4. Right click on the folder called Test Folder(copy) and click Rename.
 5. Enter the name Test Data in the name box and hit Enter.
 6. Close the File browser.
VERIFICATION:
 Do you now have a folder called Test Data? PURPOSE:
 This test will check that the file browser can create a new file.
STEPS:
 1. Click Select Test to open the File Browser.
 2. Right click in the white space and click Create Document -> Empty Document.
 3. Enter the name Test File 1 in the name box and hit Enter.
 4. Close the File browser.
VERIFICATION:
 Do you now have a file called Test File 1? PURPOSE:
 This test will check that the file browser can create a new folder.
STEPS:
 1. Click Test to open the File Browser.
 2. On the menu bar, click File -> Create Folder.
 3. In the name box for the new folder, enter the name Test Folder and hit Enter.
 4. Close the File browser.
VERIFICATION:
 Do you now have a new folder called Test Folder? PURPOSE:
 This test will check that the file browser can delete a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the file called Test File 1 and click on Move To Trash.
 3. Verify that Test File 1 has been removed.
 4. Close the File Browser.
VERIFICATION:
  Is Test File 1 now gone? PURPOSE:
 This test will check that the file browser can delete a folder.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the folder called Test Folder and click on Move To Trash.
 3. Verify that the folder was deleted.
 4. Close the file browser.
VERIFICATION:
 Has Test Folder been successfully deleted? PURPOSE:
 This test will check that the file browser can move a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Click and drag the file called Test File 2 onto the icon for the folder called Test Data.
 3. Release the button.
 4. Double click the icon for Test Data to open that folder up.
 5. Close the File Browser.
VERIFICATION:
 Was the file Test File 2 successfully moved into the Test Data folder? PURPOSE:
 This test will check that the update manager can find updates.
STEPS:
 1. Click Test to launch update-manager.
 2. Follow the prompts and if updates are found, install them.
 3. When Update Manager has finished, please close the app by clicking the Close button in the lower right corner.
VERIFICATION:
 Did Update manager find and install updates (Pass if no updates are found,
 but Fail if updates are found but not installed) PURPOSE:
 This test will verify that the file browser can move a folder.
STEPS:
 1. Click Test to open the File Browser.
 2. Click and drag the folder called Test Data onto the icon called Test Folder.
 3. Release the button.
 4. Double click the folder called Test Folder to open it up.
 5. Close the File Browser.
VERIFICATION:
 Was the folder called Test Data successfully moved into the folder called Test Folder? Panel Clock Verification tests Panel Reboot Verification tests Parses Xorg.0.Log and discovers the running X driver and version Peripheral tests Piglit tests Ping ubuntu.com and restart network interfaces 100 times Play back a sound on the default output and listen for it on the  default input. Please choose (%s):  Please press each key on your keyboard. Please type here and press Ctrl-D when finished:
 Pointing device tests. Power Management Test Power Management tests Press any key to continue... Previous Print version information and exit. Provides information about displays attached to the system Provides information about network devices Quit from keyboard Record mixer settings before suspending. Record the current network before suspending. Record the current resolution before suspending. Rendercheck tests Rerun Restart Returns the name, driver name and driver version of any touchpad discovered on the system. Run Cachebench Read / Modify / Write benchmark Run Cachebench Read benchmark Run Cachebench Write benchmark Run Compress 7ZIP benchmark Run Compress PBZIP2 benchmark Run Encode MP3 benchmark Run Firmware Test Suite (fwts) automated tests. Run GLmark2 benchmark Run GLmark2-ES2 benchmark Run GnuPG benchmark Run Himeno benchmark Run Lightsmark benchmark Run N-Queens benchmark Run Network Loopback benchmark Run Qgears2 OpenGL gearsfancy benchmark Run Qgears2 OpenGL image scaling benchmark Run Qgears2 XRender Extension gearsfancy benchmark Run Qgears2 XRender Extension image scaling benchmark Run Render-Bench XRender/Imlib2 benchmark Run Stream Add benchmark Run Stream Copy benchmark Run Stream Scale benchmark Run Stream Triad benchmark Run Unigine Heaven benchmark Run Unigine Santuary benchmark Run Unigine Tropics benchmark Run a stress test based on FurMark (OpenGL 2.1 or 3.2) Fullscreen 1920x1080 no antialiasing Run a stress test based on FurMark (OpenGL 2.1 or 3.2) Windowed 1024x640 no antialiasing Run gtkperf to make sure that GTK based test cases work Run the graphics stress test. This test can take a few minutes. Run x264 H.264/AVC encoder benchmark Running %s... Runs a test that transfers 100 10MB files 3 times to a SDHC card. Runs a test that transfers 100 10MB files 3 times to usb. Runs piglit tests for checking OpenGL 2.1 support Runs the piglit results summarizing tool SATA/IDE device information. SMART test Select All Select all Server Services checks Shorthand for --config=.*/jobs_info/blacklist. Shorthand for --config=.*/jobs_info/blacklist_file. Shorthand for --config=.*/jobs_info/whitelist. Shorthand for --config=.*/jobs_info/whitelist_file. Skip Smoke tests Sniff Sniffers Software Installation tests Some new hard drives include a feature that parks the drive heads after a short period of inactivity. This is a power-saving feature, but it can have a bad interaction with the operating system that results in the drive constantly parked then activated. This produces excess wear on the drive, potentially leading to early failures. Space when finished Start testing Status Stop process Stop typed at tty Stress poweroff system (100 cycles) Stress reboot system (100 cycles) Stress tests Submission details Submit results Successfully finished testing! Suspend Test Suspend tests System 
Testing System Daemon tests System Testing Tab 1 Tab 2 Termination signal Test Test ACPI Wakealarm (fwts wakealarm) Test Again Test and exercise memory. Test cancelled Test for clock jitter. Test if the atd daemon is running when the package is installed. Test if the cron daemon is running when the package is installed. Test if the cupsd daemon is running when the package is installed. Test if the getty daemon is running when the package is installed. Test if the init daemon is running when the package is installed. Test if the klogd daemon is running when the package is installed. Test if the nmbd daemon is running when the package is installed. Test if the smbd daemon is running when the package is installed. Test if the syslogd daemon is running when the package is installed. Test if the udevd daemon is running when the package is installed. Test if the winbindd daemon is running when the package is installed. Test interrupted Test offlining CPUs in a multicore system. Test that the /var/crash directory doesn't contain anything. Lists the files contained within if it does, or echoes the status of the directory (doesn't exist/is empty) Test that the X is not running in failsafe mode. Test that the X process is running. Test the CPU scaling capabilities using Firmware Test Suite (fwts cpufreq). Test the network after resuming. Test to check that virtualization is supported and the test system has at least a minimal amount of RAM to function as an OpenStack Compute Node Test to detect audio devices Test to detect the available network controllers Test to detect the optical drives Test to output the Xorg version Test to see if we can sync local clock to an NTP server Test to see that we have the same resolution after resuming as before. Test your system and submit results to Launchpad Tested Tests oem-config using Xpresser, and then checks that the user has been created successfully. Cleans up the newly created user after the test has passed. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11b/g protocols after the system has been suspended. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11b/g protocols. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11n protocol after the system has been suspended. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11n protocol. Tests that the systems wireless hardware can connect to a router using no security and the 802.11b/g protocols after the system has been suspended. Tests that the systems wireless hardware can connect to a router using no security and the 802.11b/g protocols. Tests that the systems wireless hardware can connect to a router using no security and the 802.11n protocol after the system has been suspended. Tests that the systems wireless hardware can connect to a router using no security and the 802.11n protocol. Tests the performance of a systems wireless connection through the iperf tool, using UDP packets. Tests the performance of a systems wireless connection through the iperf tool. Tests to see that apt can access repositories and get updates (does not install updates). This is done to confirm that you could recover from an incomplete or broken update. Tests whether the system has a working Internet connection. TextLabel The file to write the log to. The following report has been generated for submission to the Launchpad hardware database:

  [[%s|View Report]]

You can submit this information about your system by providing the email address you use to sign in to Launchpad. If you do not have a Launchpad account, please register here:

  https://launchpad.net/+login The generated report seems to have validation errors,
so it might not be processed by Launchpad. There is another checkbox running. Please close it first. This Automated test attempts to detect a camera. This attaches screenshots from the suspend/cycle_resolutions_after_suspend_auto test to the results submission. This is a fully automated version of mediacard/sd-automated and assumes that the system under test has a memory card device plugged in prior to checkbox execution. It is intended for SRU automated testing. This is an automated Bluetooth file transfer test. It sends an image to the device specified by the BTDEVADDR environment variable. This is an automated Bluetooth test. It emulates browsing on a remote device specified by the BTDEVADDR environment variable. This is an automated Bluetooth test. It receives the given file from a remote host specified by the BTDEVADDR environment variable This is an automated test to gather some info on the current state of your network devices. If no devices are found, the test will exit with an error. This is an automated test which performs read/write operations on an attached FireWire HDD This is an automated test which performs read/write operations on an attached eSATA HDD This is an automated version of usb/storage-automated and assumes that the server has usb storage devices plugged in prior to checkbox execution. It is intended for servers and SRU automated testing. This is the automated version of suspend/suspend_advanced. This test checks cpu topology for accuracy This test checks that CPU frequency governors are obeyed when set. This test checks that the wireless interface is working after suspending the system. It disconnects all interfaces and then connects to the wireless interface and checks that the connection is working as expected. This test checks the amount of memory which is reporting in meminfo against the size of the memory modules detected by DMI. This test disconnects all connections and then connects to the wireless interface. It then checks the connection to confirm it's working as expected. This test grabs the hardware address of the bluetooth adapter after suspend and compares it to the address grabbed before suspend. This test is automated and executes after the mediacard/cf-insert test is run. It tests reading and writing to the CF card. This test is automated and executes after the mediacard/cf-insert-after-suspend test is run. It tests reading and writing to the CF card after the system has been suspended. This test is automated and executes after the mediacard/mmc-insert test is run. It tests reading and writing to the MMC card. This test is automated and executes after the mediacard/mmc-insert-after-suspend test is run. It tests reading and writing to the MMC card after the system has been suspended. This test is automated and executes after the mediacard/ms-insert test is run. It tests reading and writing to the MS card. This test is automated and executes after the mediacard/ms-insert-after-suspend test is run. It tests reading and writing to the MS card after the system has been suspended. This test is automated and executes after the mediacard/msp-insert test is run. It tests reading and writing to the MSP card. This test is automated and executes after the mediacard/msp-insert-after-suspend test is run. It tests reading and writing to the MSP card after the system has been suspended. This test is automated and executes after the mediacard/sd-insert test is run. It tests reading and writing to the SD card. This test is automated and executes after the mediacard/sd-insert-after-suspend test is run. It tests reading and writing to the SD card after the system has been suspended. This test is automated and executes after the mediacard/sdhc-insert test is run. It tests reading and writing to the SDHC card. This test is automated and executes after the mediacard/sdhc-insert-after-suspend test is run. It tests reading and writing to the SDHC card after the system has been suspended. This test is automated and executes after the mediacard/sdxc-insert test is run. It tests reading and writing to the SDXC card. This test is automated and executes after the mediacard/sdxc-insert-after-suspend test is run. It tests reading and writing to the SDXC card after the system has been suspended. This test is automated and executes after the mediacard/xd-insert test is run. It tests reading and writing to the xD card. This test is automated and executes after the mediacard/xd-insert-after-suspend test is run. It tests reading and writing to the xD card after the system has been suspended. This test is automated and executes after the usb/insert test is run. This test is automated and executes after the usb3/insert test is run. This test will check to make sure supported video modes work after a suspend and resume. This is done automatically by taking screenshots and uploading them as an attachment. This test will verify that the volume levels are at an acceptable level on your local system.  The test will validate that the volume is greater than or equal to minvol and less than or equal to maxvol for all sources (inputs) and sinks (outputs) recognized by PulseAudio.  It will also validate that the active source and sink are not muted.  You should not manually adjust the  volume or mute before running this test. This will attach any logs from the power-management/poweroff test to the results. This will attach any logs from the power-management/reboot test to the results. This will check to make sure that your audio device works properly after a suspend and resume.  This may work fine with speakers and onboard microphone, however, it works best if used with a cable connecting the audio-out jack to the audio-in jack. This will run some basic connectivity tests against a BMC, verifying that IPMI works. Timer signal from alarm(2) To fix this, close checkbox and add the missing dependencies to the whitelist. Touchpad tests Touchscreen tests Try to enable a remote printer on the network and print a test page. Type Text UNKNOWN USB Test USB tests Unknown signal Untested Usage: checkbox [OPTIONS] User Applications User-defined signal 1 User-defined signal 2 Validate that the Vector Floating Point Unit is running on ARM device Verifies that DNS server is running and working. Verifies that Print/CUPs server is running. Verifies that Samba server is running. Verifies that Tomcat server is running and working. Verifies that sshd is running. Verifies that the LAMP stack is running (Apache, MySQL and PHP). Verify USB3 external storage performs at or above baseline performance Verify system storage performs at or above baseline performance Verify that all CPUs are online after resuming. Verify that all memory is available after resuming from suspend. Verify that all the CPUs are online before suspending Verify that an installation of checkbox-server on the network can be reached over SSH. Verify that mixer settings after suspend are the same as before suspend. Verify that storage devices, such as Fibre Channel and RAID can be detected and perform under stress. View results Virtualization tests Welcome to System Testing!

Checkbox provides tests to confirm that your system is working properly. Once you are finished running the tests, you can view a summary report for your system. Wireless Test Wireless networking tests Wireless scanning test. It scans and reports on discovered APs. Working You can also close me by pressing ESC or Ctrl+C. _Deselect All _Exit _Finish _No _Previous _Select All _Skip this test _Test _Test Again _Yes and capacity as well. attaches the contents of various sysctl config files. eSATA disk tests https://help.ubuntu.com/community/Installation/SystemRequirements installs the installer bootchart tarball if it exists. no skip test test again tty input for background process tty output for background process yes Project-Id-Version: checkbox
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-01-27 17:46+0000
PO-Revision-Date: 2014-09-14 07:09+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:34+0000
X-Generator: Launchpad (build 18115)
 

Внимание: Некоторые тесты могут вызвать увеличение времени ответа системы. Прежде чем начать тестирование, пожалуйста, сохраните свою работу и закройте все открытые программы.    Решите, нужно ли нам запускать тесты, предназначенные для портативных компьютеров, которые могут быть не применимы к настольным компьютерам.  Результаты   Выполнить   Выбор   Создаёт несколько изображений на основе разрешения поддерживаемого камерой,
 проверяет их размер и правильность формата.  Проверяет способность беспроводного оборудования системы подключаться к маршрутизатору
используя протокол 802.11a. Для этого, необходимо предварительно настроить маршрутизатор, так
чтобы он отвечал только на запросы протокола 802.11a.  Проверяет способность беспроводного оборудования системы подключаться к маршрутизатору
используя протокол 802.11b. Для этого, необходимо предварительно настроить маршрутизатор, так
чтобы он отвечал только на запросы протокола 802.11b.  Проверяет способность беспроводного оборудования системы подключаться к маршрутизатору
используя протокол 802.11g. Для этого, необходимо предварительно настроить маршрутизатор, так
чтобы он отвечал только на запросы протокола 802.11g. нажата клавиша %(key_name)s &Нет П_редуыдущий _Пропустить тест &Проверить &Да Выполнено 10 тестов из 30 (30%) Получен сигнал отмены от функции abort(3) Все необходимые клавиши были проверены! Архивирует каталог piglit-summary в piglit-results.tar.gz. Вы действительно уверены? Прикрепить отчёт из теста fwts wakealarm Добавляет копию /var/log/dmesg в результаты проверки Прикреплять фрагмент содержимого базы данных «udev», показывающий информацию об аппаратном обеспечении. Добавляет список выполняемых в данный момент модулей ядра. Добавляет отчёт сведений о ЦПУ Добавляет отчёт об установленных кодеках для Intel HDA Добавляет отчёт атрибутов sysfs. Добавляет архив tarball данных gcov, если они существуют. Добавляет вывод dmidecode Добавляет сведения о DMI Добавляет сведения о разделах дисков Прикрепляет вывод lshw Прикрепляет журнал событий FWTS в отчёт Прикрепляет данные об аудиоустройствах к общим результатам. Добавление журнала bootchart при запуске теста bootchart. Добавляет файл bootchart png для запуска bootchart Добавляет содержимое /proc/acpi/sleep, если оно есть. Добавляет содержимое файла /etc/modules. Добавляет содержимое различных конфигурационных файлов modprobe. Прикрепляет версию микропрограммы Добавляет результаты графических стресс-тестов в отчёт. Прикрепляет отладочную информацию программы установки, если такая существует. Прикрепляет журнал событий из 250-циклового теста перехода и выхода из спящего режима, если такой журнал существует Прикрепляет журнал событий из 250-циклового теста перехода и выхода из ждущего режима, если такой журнал существует Позволяет включить отчёт 30-циклового теста спящий режим/возобновление работы, если такой имеется Добавляет отчёт 30-циклового теста ждущего режима/возобновление работы, если такой имеется Добавляет к результатам отчет однократного теста засыпания-пробуждения Прикрепляет журнал событий созданный cpu/scaling_test к результатам Прикрепляет вывод udev_resource, для целей отладки Прикрепить снимок экрана созданный в graphics/screenshot. Прикрепить снимок экрана созданный в graphics/screenshot_fullscreen_video. Прикрепляет исчерпывающий вывод lspci  (с центральным запросом к базе данных). Добавляет подробный вывод lspci. Проверка звука Проверка звука Автоматизированная проверка записи на компакт-диск. Автоматизированная проверка записи на DVD диск. Автоматическая проверка журнала гибернации на наличие ошибок, обнаруженных утилитой fwts Автоматизированная проверка чтения оптических носителей Автоматизированная проверка возможности загрузки файлов с использованием HTTP Автоматизированная проверка возможности использования ICMP ECHO пакетов некоторыми системами в сети. Автоматизированная проверка для хранения сведений о устройстве bluetooth в отчёте программы проверки системы Автоматизированная проверка позволяющая переключаться между несколькими сетевыми картами и последовательно тестировать каждую из них. Проверка каждого диска Тесты производительности Проверка Bluetooth Проверка Bluetooth Сведения Bootchart. Неверный канал: запись в канал без считывающей программы Формирование отчета... Проверка записи на компакт-диск. Проверка ЦПУ Проверка ЦПУ Использование процессора в режиме бездействия. Проверка камеры Проверка камеры Посмотреть сбойный результат набора тестов оболочки Проверка выполнения задания после подтверждения зависимостей Проверяет, что задание запущено после согласования зависимостей Проверяет, что результат выполнения отмечен, как "не является необходимым" при не соблюдении зависимостей Проверка, что задание не запущено из-за отсутствия зависимостей Проверяет отчеты теста многократного выключения (100 циклов) Проверяет результаты теста многократной перезагрузки (100 циклов) Проверка изменения состояния каждого диска Посмотреть пложительный результат набора тестов оболочки Проверка задействования драйверов VESA Проверка аппаратных возможностей для запуска Unity 3D Проверка аппаратных возможностей для запуска compiz Проверить время необходимое для повторного подключения к точке доступа Wi-Fi Проверьте установлена ли в ядре директива CONFIG_NO_HZ (это простой тест на регрессию) Тестирование системы Checkbox Проверка системы не завершена.
Вы действительно хотите запустить последний тест снова,
начать со следующего теста или
перезапустить с самого начала? Убедитесь, что заданный файл списков источников содержит необходимые репозитории Проверяет разряд аккумулятора во время ожидания. Ведёт учёт времени до полного разряда Проверка разряда батареи во время приостановки работы. Определяется время пока Проверяет разряд аккумулятора во время воспроизведения кинофильма. Ведёт учёт времени Проверяет время, необходимое на повторное установление существующего WiFi-соединения после цикла перехода в ждущий режим/выхода из него. Дочерний процесс был остановлен или завершён Выберите тесты для выполнения на вашем компьютере: Проверка кодеков Свернуть всё Собрать информацию об аудиостройствах. Эти данные могут быть использованы для эмуляции звуковой подсистемы этого компьютера и более тщательных тестов в контролируемом окружении. Сбор сведений о глубине цвета и формате пикселя. Собрать сведения о частоте обновления. Собрать сведения о графической памяти Сбор сведений о графических режимах (разрешение экрана и частота обновления) Совместите с символами сверху для расширения узла Команда не найдена. Команда получила сигнал %(signal_name)s: %(signal_description)s Отзывы Проверка стандартных типов документов Компоненты Параметры не приняты конфигурацией. Продолжить Продолжить после остановки Создаёт мобильное широкополосное соединение для CDMA-модема и проверяет его работоспособность. Проверка записи на DVD диск. Отсутствуют зависимости, таким образом некоторые задания не будут выполнены. Отменить выбор Снять выделение Подробная информация... Находит и отображает диски, подключенные к системе. Определяет и показывает USB-устройства подсоединённые к этой системе. Определяет, была ли сенсорная панель распознана автоматически как устройство поддерживающее множественные касания. Определяет, была ли сенсорная панель распознана автоматически как устройство поддерживающее одно касание. Проверка диска Проверка диска Обращение к жёсткому диску в режиме бездействия. Вы действительно хотите пропустить выполнение этой проверки? Больше не спрашивать Готово Сохраняет фрагмент памяти в файл для сравнения после выполнения проверки ждущего режима Эл. почта Адрес электронной почты должен быть правильно написан. Эл. почта: Убедитесь, что текущее разрешение соответствует или превосходит рекомендуемое минимальное разрешение (800x600). Сведения: Введите текст:
 Ошибка Обмен данными с сервером... Выполнение %(test_name)s Развернуть всё Тест ExpressCard Тесты ExpressCard Ошибка при соединении с сервером.
Попробуйте, пожалуйста, ещё раз 
или загрузите файл:
%s
напрямую в базу данных оборудования:
https://launchpad.net/+hwdb/+submit Не удалось открыть файл '%s': %s Не удалось обработать форму: %s Не удалось загрузить данные на сервер,
Пожалуйста, попробуйте позже. Проверка сканера отпечатков пальцев Проверка Firewire Проверка дисков Firewire Ошибка операции с плавающей точкой Проверка дискет Тест дискеты Форма Дополнительная информация: Сбор информации о вашей системе... Проверка графической подсистемы Проверка графической системы Обнаружено зависание контролирующего терминала или неожиданное завершение контролирующего процесса Проверка спящего режима Проверка клавиш быстрого доступа Тест завершится автоматически после нажатия всех клавиш. Если клавиша отсутствует на вашей клавиатуре, нажмите кнопку «Пропустить», чтобы не использовать её в тесте. Если на вашей клавиатуре отсутствует одна или несколько клавиш, введите её номер для того чтобы отказаться от проверки данной клавиши. Недопустимая инструкция Выполняется Информация Информационный тест Информация не опубликована на Launchpad. Информационные тесты Проверка устройств ввода Проверка ввода Интернет-подключение полностью работоспособно Прерывание от клавиатуры Неверный указатель памяти Задания будут переупорядочены для исправления неполадок с зависимостями Проверка клавиш Получен сигнал Kill Тесты светодиодов (LED) Список USB устройств Показывает имена и версии драйверов всех звуковых устройств. Проверьте доступность RTC устройства. Максимальное использование места на диске при тесте с настройками по умолчанию Проверка карт памяти Проверка карт памяти Проверка памяти Проверка памяти Различные тесты Различные тесты Отсутствует параметр файла конфигурации.
 Тесты мобильного широкополосного подключения Проверка монитора Проверка монитора Перетаскивайте окно с 3D по экрану _Следующий _Далее Сведения о сети Проверка сетевых возможностей Проверка сети Далее Нет подключения к Интернет Нет отклика Пока не выполнялась Не поддерживается Проверка не выполнялась Не требуется Одно из следующего: отладка, информация, предупреждение, ошибка, опасность. Откройте и закройте 4 окна с 3D несколько раз Откройте и закройте окно с 3D несколько раз Откройте, приостановите - восстановите работу и закройте 3D окно несколько раз Проверка оптических дисков Проверка оптических дисков Проверка чтения CD ПАРОЛЬ:  ЦЕЛЬ:
     Проверка работоспособности внешнего линейного выхода
ДЕЙСТВИЯ:
     1. Подсоедините кабель к громкоговорителям (со встроенными усилителями) в порт линейного выхода
     2. Откройте параметры звука, далее вкладку 'Выходы', выберите из списка элемент 'Линейный выход'. Щёлкните кнопку Проверить
     3. В параметрах звука, выберите 'Internal Audio' из списка устройств и щёлкните 'Проверить громкоговорители', чтобы проверить левый и правый канал
ПРОВЕРКА:
     1. Был ли слышен звук из громкоговорителей? Внутренние громкоговорители не должны перейти в режим отключённого звука автоматически.
     2. Вы слышали звук исходящий из соответствующего канала? ЦЕЛЬ:
    Проверка индикации блокировки заглавных
ДЕЙСТВИЯ:
    1. Нажмите "Block Cap Keys" для активации/деактивации блокировки заглавных
    2. Индикатор блокировки заглавных должен включатся/выключаться каждый раз когда вы нажимаете кнопку
ПРОВЕРКА:
    Индикация работала как положено? ЦЕЛЬ:
    Проверка светодиодного индикатора камеры
ДЕЙСТВИЯ:
    1. Выберите Проверить, чтобы задействовать камеру
    2. Cветодиодный индикатор камеры должен загореться на несколько секунд
ПРОВЕРКА:
    Загорался ли светодиодный индикатор камеры? ЦЕЛЬ:
    Проверить, что контроль баланса корректно работает на внешних динамиках (наушниках)
ШАГИ:
    1. Проверьте, что передвижение ползунка баланса слева направо происходит плавно.
    2. Кликните на кнопку Проверка, чтобы проиграть звуковой сигнал на 10 секунд.
    3. Передвиньте ползунок баланса слева направо и обратно.
    4. Проверьте, что актуальный баланс колонок соответствует вашим настройкам.
ПРОВЕРКА:
    Движется ли ползунок плавно, а также соответствует ли настройке актуальный аудиовыход? ЦЕЛЬ:
    Проверить, что контроль баланса корректно работает на встроенных динамиках
ШАГИ:
    1. Проверьте, что передвижение ползунка баланса слева направо происходит плавно.
    2. Кликните на кнопку Проверка, чтобы проиграть звуковой сигнал на 10 секунд.
    3. Передвиньте ползунок баланса слева направо и обратно.
    4. Проверьте, что актуальный баланс колонок соответствует вашим настройкам.
ПРОВЕРКА:
    Движется ли ползунок плавно, а также соответствует ли настройке актуальный аудиовыход? ЦЕЛЬ:
    Проверка работоспособности задействованного в подключении внешнего линейного входа
ДЕЙСТВИЯ:
    1. Используйте кабель, чтобы подключить порт линейного входа к внешнему линейному выходу устройства.
    2. Откройте параметры звука, далее вкладку 'Входы', выберите из списка элемент 'Линейный вход'. Щёлкните кнопку Проверить
    3. Через несколько секунд будет выполнено воспроизведение записанного.
ПРОВЕРКА:
    Вы услышали записанное? ЦЕЛЬ:
    Проверка надлежащей работоспособности различных звуковых каналов
ДЕЙСТВИЯ:
    1. Щёлкните на кнопке Проверить
ПРОВЕРКА:
    Вы должны отчётливо и чисто слышать голос из различных звуковых каналов ЦЕЛЬ:
    Проверка перетаскивания на сенсорном экране
ДЕЙСТВИЯ:
    1. Выполните двойное касание и, не отрывая палец, перетаскивайте объект на рабочем столе
    2. Отпустите объект в другом месте
ПРОВЕРКА:
    Удалось ли выполнить перетаскивание объекта? ЦЕЛЬ:
    Проверка масштабирования жестом «щипок» на сенсорном экране
ДЕЙСТВИЯ:
    1. Коснитесь экрана двумя пальцами и сведите их вместе
    2. Коснитесь экрана двумя пальцами и разведите их в стороны
ПРОВЕРКА:
    Изменяется ли масштаб изображения на экране? ЦЕЛЬ:
    Проверка распознавания касания сенсорного экрана
ДЕЙСТВИЯ:
    1. Коснитесь пальцем объекта на экране. Указатель должен переместиться в место касания и объект должен стать выделенным
ПРОВЕРКА:
    Было ли распознано касание? ЦЕЛЬ:
    Создаёт задачи, которые загружают ЦПУ в течении двух часов, насколько это возможно. Проверка считается выполненной, если система продолжает отвечать на запросы. НАЗНАЧЕНИЕ:
    Проверка DisplayPort аудио интерфейса 
ШАГИ:
    1. Подключите внешнее устройство DisplayPort со звуком (Используйте только один HDMI/DisplayPort интерфейс во время этого теста)
    2. Нажмите на кнопку Тест 
Проверка:
    Слышали ли Вы звук от устройства DisplayPort? ЦЕЛЬ:
    Проверить светодиодный индикатор жёсткого диска
ДЕЙСТВИЯ:
    1. Выберите "Проверить", чтобы выполнить краткую запись и чтение временного файла
    2. Светодиодный индикатор жёсткого диска должен загореться во время записи или чтения с жёсткого диска
ПРОВЕРКА:
    Загорелся ли светодиодный индикатор жёсткого диска? НАЗНАЧЕНИЕ:
    Проверка HDMI аудио интерфейса 
ШАГИ:
    1. Подключите внешнее устройство HDMI со звуком (Используйте только один HDMI/DisplayPort интерфейс во время этого теста)
    2. Нажмите на кнопку Тест 
Проверка:
    Слышали ли Вы звук от устройства HDMI? ЦЕЛЬ:
    Сохранить сведения об исполнителе теста в отчёте
ДЕЙСТВИЯ:
    1. Сведения об исполнителе теста
    2. Пожалуйста, введите следующие сведения в поле:
       a. Имя
       b. Адрес эл. почты
       c. Причина выполнения этого теста
ПРОВЕРКА:
    Этот тест не требует проверки ЦЕЛЬ:
    Обнаружение акселерометра вручную.
ПОРЯДОК ДЕЙСТВИЙ:
    1. Взгляните на спецификации вашего компьютера.
ПРОВЕРКА:
    Предполагается ли наличие акселерометра в вашем компьютере? ЦЕЛЬ:
    Проверка светодиодного индикатора блока цифровой клавиатуры
ДЕЙСТВИЯ:
    1. Нажмите клавишу "Block Num", чтобы загорелся светодиодный индикатор блока цифровой клавиатуры
    2. Щёлкните кнопку "Проверить", чтобы открыть окно и выполнить проверку ввода
    3. Произведите ввод используя блок цифровой клавиатуры с включённым и выключенным индикатором
ПРОВЕРКА:
    1. Состояние индикатора блока цифровой клавиатуры должно изменяться при каждом нажатии на клавишу "Block Num"
    2. Цифры должны вводится только в окне проверки при включённом индикаторе ЦЕЛЬ:
    Проверка светодиодного индикатора питания
ДЕЙСТВИЯ:
    1. Индикатор питания должен светиться, когда устройство включено
ПРОВЕРКА:
    Правильно ли работает индикатор питания? ЦЕЛЬ:
    Проверка светодиодного индикатора питания
ДЕЙСТВИЯ:
    1. Индикатор питания должен мигать или менять цвет при переходе системы в ждущий режим
ПРОВЕРКА:
    Наблюдалось ли мигание или изменение цвета индикатора питания, когда система была переведена в ждущий режим для предыдущего теста ждущего режима? ЦЕЛЬ:
    Создать изображение текущего экрана (после выполнения входа в Unity)
ДЕЙСТВИЯ:
    1. Создайте изображение используя веб-камеру с USB
ПРОВЕРКА:
    Просмотрите вложение самостоятельно, позднее ЦЕЛЬ:
    Создать снимок текущего экрана после режима приостановления (выполнив вход в Unity)
ДЕЙСТВИЯ:
    1. Создайте снимок используя веб-камеру с интерфейсом USB
ПРОВЕРКА:
    Просмотрите вложение самостоятельно позже ЦЕЛЬ:
    Создать изображение текущего экрана во время воспроизведения видео в полноэкранном режиме
ДЕЙСТВИЯ:
    1. Запустите воспроизведение в полноэкранном режиме
    2. Создайте изображение используя веб-камеру с USB, через несколько секунд
ПРОВЕРКА:
    Просмотрите вложение самостоятельно, позднееr ЦЕЛЬ:
    Проверка возможностей подключения беспроводного оборудования системы к маршрутизатору по протоколу 802.11b/g без использования шифрования.
ДЕЙСТВИЯ:
    1. Откройте средство настройки маршрутизаторов
    2. Измените настройки таким образом, чтобы разрешать только соединения в диапазонах B и G
    3. Убедитесь, что SSID установлен как ROUTER_SSID
    4. Измените настройки безопасности, таким образом, чтобы не использовалось шифрование
    5. Щёлкните кнопку 'Проверить', чтобы создать соединение с маршрутизатором и проверить соединение
ПРОВЕРКА:
    Проверка автоматизирована, не изменяйте автоматически выбранный результат. ЦЕЛЬ:
    Проверяет возможность беспроводного оборудования подключаться к маршрутизатору
    не используя шифрование, по протоколу 802.11n.
ДЕЙСТВИЯ:
    1. Откройте средство настройки маршрутизатора
    2. Измените настройки таким образом, чтобы допускались беспроводные соединения только в режиме N
    3. Убедитесь, что SSID назначен ROUTER_SSID
    4. Измените настройки безопасности, так чтобы не использовалось шифрование
    5. Щёлкните кнопку 'Проверить', чтобы создать соединение с маршрутизатором и проверить подключение
ПРОВЕРКА:
    Проверка автоматизирована, не изменяйте автоматически выбранный результат. ЦЕЛЬ:
    Проверка возможностей подключения беспроводного оборудования системы к маршрутизатору используя протоколы WPA
    и 802.11b/g.
ДЕЙСТВИЯ:
    1. Откройте средство настройки маршрутизаторов
    2. Измените настройки таким образом, чтобы разрешать только соединения в диапазонах B и G
    3. Убедитесь, что SSID установлен как ROUTER_SSID
    4. Измените настройки безопасности, чтобы использовать WPA2 и убедиться, что PSK соответствует установленному в  ROUTER_PSK
    5. Щёлкните кнопку 'Проверить', чтобы создать соединение с маршрутизатором и проверить соединение
ПРОВЕРКА:
    Проверка автоматизирована, не изменяйте автоматически выбранный результат. ЦЕЛЬ:
    Проверка возможностей подключения беспроводного оборудования системы к маршрутизатору используя протоколы WPA
    и  802.11n.
STEPS:
    1. Откройте средство настройки маршрутизаторов
    2. Измените настройки таким образом, чтобы разрешать только соединения в диапазоне N
    3. Убедитесь, что SSID установлен как ROUTER_SSID
    4. Измените настройки безопасности, чтобы использовать WPA2 и убедиться, что PSK соответствует установленному в  ROUTER_PSK
    5. Щёлкните кнопку 'Проверить', чтобы создать соединение с маршрутизатором и проверить соединение
ПРОВЕРКА:
    Проверка автоматизирована, не изменяйте автоматически выбранный результат. ЦЕЛЬ:
    Установить, что модуль ручного ввода работает
ЗАДАЧИ:
    1. Добавить комментарий
    2. Установить работоспособность
ПРОВЕРКА:
    Проверить, что результат достигнут и комментарий отображается ЦЕЛЬ:
    Эта проверка позволит выполнить переходы по всем определённым режимам видео
ДЕЙСТВИЯ:
    1. Щёлкните "Проверить" для запуска переходов по режимам видео
ПРОВЕРКА:
    Работает ли экран при переходе в каждый режим? ЦЕЛЬ:
    Этот тест позволяет проверить основные возможности 3D графической карты
ДЕЙСТВИЯ:
    1. Щёлкните "Проверить" для выполнения демонстрации OpenGL. Нажмите ESC в любое время для закрытия.
    2. Убедитесь, что отсутствуют рывки или замедления.
ПРОВЕРКА:
    1. Появился ли трёхмерный видеофрагмент?
    2. В видеофрагменте отсутствовали рывки и замедления? ЦЕЛЬ:
    Эта проверка позволит удостовериться в возможности настройки DSL модема и его подключении.
ДЕЙСТВИЯ:
    1. Подсоедините телефонный кабель к компьютеру
    2. Щёлкните на значке сети в верхней панели.
    3. Выберите "Изменить соединения"
    4. Выберите вкладку "DSL"
    5. Щёлкните кнопку "Добавить"
    6. Правильно настройте параметры соединения
    7. Щёлкните "Проверить" для проверки возможности установления HTTP соединения
ПРОВЕРКА:
    Было ли отображено оповещение об установлении соединения? ЦЕЛЬ:
    Эта проверка позволит убедиться в надлежащей работоспособности звукового устройства с интерфейсом USB
ДЕЙСТВИЯ:
    1. Подсоедините звуковое устройство с интерфейсом USB к вашей системе
    2. Щёлкните "Проверить", затем произнесите что-нибудь в микрофон
    3. Через несколько секунд ваша речь будет воспроизведена
ПРОВЕРКА:
    Вы слышали вашу речь воспроизведённую с использованием наушников с интерфейсом USB? ЦЕЛЬ:
    Эта проверка позволит убедиться в работоспособности bluetooth соединения
ДЕЙСТВИЯ:
    1. Включите bluetooth на любом из мобильных устройств (КПК, смартфон и т.п.)
    2. Щёлкните на значке bluetooth в панели меню
    3. Выберите 'Установить новое устройство'
    4. Выберите устройство из списка
    5. На устройстве введите PIN-код, автоматически присваиваемый мастером
    6. Устройство должно быть сопряжено с компьютером
    7. Щёлкните правой кнопкой мыши на значке bluetooth и выберите обзор файлов
    8. Авторизируйте компьютер для обзора файлов на устройстве (если необходимо)
    9. У вас появится возможность обзора файлов
ПРОВЕРКА:
    Все ли действия выполнены успешно? ЦЕЛЬ:
    Эта проверка позволит убедиться в работоспособности штекера наушников
ДЕЙСТВИЯ:
    1. Подсоедините наушники к вашему звуковому устройству
    2. Щёлкните кнопку Проверить для воспроизведения звукового сигнала через звуковое устройство
ПРОВЕРКА:
    Был ли слышен звук в наушниках и был ли он воспроизведён в ваших наушниках без искажений, щелчков или других искажённых звуков? ЦЕЛЬ:
    Эта проверка позволит убедиться в работоспособности встроенных колонок
ДЕЙСТВИЯ:
    1. Убедитесь, что к устройству не подключены наушники или внешние колонки
       При тестировании настольного ПК, допустимо подключение внешних колонок
    2. Нажмите кнопку Проверить для воспроизведения короткого сигнала на вашем звуковом устройстве
ПРОВЕРКА:
    Вы слышали сигнал? ЦЕЛЬ:
    Эта проверка позволит убедиться в правильности записи звука с использованием внешнего микрофона
ДЕЙСТВИЯ:
    1. Подсоедините микрофон в разъём для подключения микрофона
    2. Щёлкните "Проверить" и затем начните говорить во внешний микрофон
    3. После нескольких секунд, произнесённая вами речь будет воспроизведена.
ПРОВЕРКА:
    Была ли воспроизведена ваша речь? ЦЕЛЬ:
    Эта проверка позволит убедиться в правильности записи звука с использованием встроенного микрофона
ДЕЙСТВИЯ:
    1. Отсоедините все подключённые внешние микрофоны
    2. Щёлкните "Проверить" и затем начните говорить во встроенный микрофон
    3. После нескольких секунд, произнесённая вами речь будет воспроизведена.
ПРОВЕРКА:
    Была ли воспроизведена ваша речь? ЦЕЛЬ:
    Проверка работоспособности встроенной камеры
ДЕЙСТВИЯ:
    1. Нажмите «Проверить», чтобы отобразить в течение 10 секунд фотоснимок со встроенной камеры.
ПРОВЕРКА:
    Видите ли вы изображение? Цель:
    Эта проверка позволит убедиться в работоспособности встроенной камеры
ДЕЙСТВИЯ:
    1. Щёлкните Проверить, для отображения 10-и секундного фрагмента с видео с камеры.
ПРОВЕРКА:
    Был ли отображён видеофрагмент? ЦЕЛЬ:
    Этот тест проверять правильность работы экрана после перехода и выхода из режима приостановления работы
ДЕЙСТВИЯ:
    1. Убедитесь, что ваш экран не отображает визуальные искажения после выхода из этого режима.
ПРОВЕРКА:
    Работает ли экран надлежащим образом после выхода из ждущего режима? ЦЕЛЬ:
    Эта проверка позволит удостовериться, что система может переключаться в виртуальный терминал и возвращаться обратно в X
ДЕЙСТВИЯ:
    1. Щёлкните "Проверить" для переключения в другой виртуальный терминал, а затем возвращения в X
ПРОВЕРКА:
    Переключается ли временно ваш экран в текстовую консоль и затем возвращается в ваш рабочий сеанс? ЦЕЛЬ:
    Этот тест проверит возможность определения считывателем карт памяти,
    извлечение карты CF
ДЕЙСТВИЯ:
    1. Щёлкните "Проверить" и извлеките карту CF из считывателя.
        (Примечание: время ожидания до начала проверки, равняется 20 секундам.)
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте автоматически
    выбираемый результат. ЦЕЛЬ:
    Этот тест проверяет что системный медиа-ридер может определить
    извлечение MS карты из системного карт-ридера.
ДЕЙСТВИЯ:
    1. Кликните на "Тест" и удалите MS карту из ридера.
       (Обратите внимание: этот тест имеет 20 секундный тайм-аут.)
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте 
    выбранный автоматически результат. ЦЕЛЬ:
    Этот тест проверяет что системный медиа-ридер может определить
    извлечение MSP карты из системного карт-ридера.
ДЕЙСТВИЯ:
    1. Кликните на "Тест" и удалите MSP карту из ридера.
       (Обратите внимание: этот тест имеет 20 секундный тайм-аут.)
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте 
    выбранный автоматически результат. ЦЕЛЬ:
    Этот тест проверяет что система корректно определяет
    удаление SDXC карты из системного карт-ридера.
ДЕЙСТВИЯ:
    1. Кликните на "Тест" и удалите SDXC карту из ридера.
       (Обратите внимание: этот тест имеет 20 секундный тайм-аут.)
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте 
    выбранный автоматически результат. ЦЕЛЬ:
    Этот тест проверяет что системный медиа-ридер может определить
    удаление xD карты.
ДЕЙСТВИЯ:
    1. Кликните на "Тест" и удалите xD карту из ридера.
       (Обратите внимание: этот тест имеет 20 секундный тайм-аут.)
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте 
    выбранный автоматически результат. ЦЕЛЬ:
    Этот тест проверит возможность определения считывателем карт памяти,
    извлечение карты SD
ДЕЙСТВИЯ: 
    1. Щёлкните "Проверить" и извлеките карту SD из считывателя.
       (Примечание: время ожидания до начала проверки, равняется 20 секундам.)
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте автоматически
    выбираемый результат. ЦЕЛЬ:
    Этот тест проверит возможность определения считывателем карт памяти,
    извлечение карты SDHC
ДЕЙСТВИЯ:
    1. Щёлкните "Проверить" и извлеките карту SDHC из считывателя.
        (Примечание: время ожидания до начала проверки, равняется 20 секундам.)
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте автоматически
    выбираемый результат. ЦЕЛЬ:
    Этот тест проверит возможность определения считывателем карт памяти,
    извлечение карт типа Multimedia Card (MMC)
ДЕЙСТВИЯ:
    1. Щёлкните "Проверить" и извлеките карту MMC из считывателя.
       (Примечание: время ожидания до начала проверки, равняется 20 секундам.)
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте автоматически
    выбираемый результат. ЦЕЛЬ:
    Этот тест позволит проверить правильность определения вставки
    USB-накопителя
ДЕЙСТВИЯ:
    1. Щёлкните "Проверить" и вставьте USB-накопитель (флешка/жёсткий диск).
       (Примечание: время отведённое для начала проверки составляет 20 секунд.)
    2. Не отсоединяйте устройство после проверки.
ПРОВЕРКА:
    Проверка автоматизирована. Не изменяйте
    автоматически выбранный результат. ЦЕЛЬ:
    Этот тест проверит возможность определения считывателем карт памяти,
    извлечение карты CF после перевода компьютера в режим приостановления работы.
ДЕЙСТВИЯ: 
    1. Щёлкните "Проверить" и извлеките карту CF из считывателя.
       (Примечание: время ожидания до начала проверки, равняется 20 секундам.)
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте автоматически
    выбираемый результат. ЦЕЛЬ:
    Этот тест проверяет что системный медиа-ридер может определить
    извлечение MS карты из системного карт-ридера после того как система приостанавливала работу.
ДЕЙСТВИЯ:
    1. Кликните на "Тест" и удалите MS карту из ридера.
       (Обратите внимание: этот тест имеет 20 секундный тайм-аут.)
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте 
    выбранный автоматически результат. ЦЕЛЬ:
    Этот тест проверяет что системный медиа-ридер может определить
    удаление MSP карты после того как система приостанавливала работу.
ДЕЙСТВИЯ:
    1. Кликните на "Тест" и удалите MSP карту из ридера.
       (Обратите внимание: этот тест имеет 20 секундный тайм-аут.)
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте 
    выбранный автоматически результат. ЦЕЛЬ:
    Этот тест проверяет что системный медиа-ридер может определить
    удаление xD карты после того как система приостанавливала работу.
ДЕЙСТВИЯ:
    1. Кликните на "Тест" и удалите xD карту из ридера.
       (Обратите внимание: этот тест имеет 20 секундный тайм-аут.)
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте 
    выбранный автоматически результат. ЦЕЛЬ:
    Этот тест проверит возможность определения считывателем карт памяти,
    извлечение карты MMC после перевода компьютера в режим приостановления работы
ДЕЙСТВИЯ:
    1. Щёлкните "Проверить" и вставьте карту MMC в считыватель.
       (Примечание: время ожидания до начала проверки, равняется 20 секундам.)
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте автоматически
    выбираемый результат. ЦЕЛЬ:
    Этот тест проверит возможность определения считывателем карт памяти,
    извлечение карты SDHC после перевода компьютера в режим приостановления работы.
ДЕЙСТВИЯ: 
    1. Щёлкните "Проверить" и извлеките карту SDHC из считывателя.
       (Примечание: время ожидания до начала проверки, равняется 20 секундам.)
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте автоматически
    выбираемый результат. ЦЕЛЬ:
    Этот тест позволит проверить правильность определения извлечения
    USB-накопителя
ДЕЙСТВИЯ:
    1. Щёлкните "Проверить" и отсоедините USB-накопитель.
      (Примечание: время отведённое для начала проверки составляет 20 секунд.)
ПРОВЕРКА:
    Проверка автоматизирована. Не изменяйте
    автоматически выбранный результат. ЦЕЛЬ:
    Эта проверка позволит убедиться в работоспособности записи и воспроизведении звука с использованием звукового устройства с bluetooth
ДЕЙСТВИЯ:
    1. Включите гарнитуру с bluetooth
    2. Щёлкните на значке bluetooth в панели меню
    3. Выберите 'Установить новое устройство'
    4. Найдите устройство в списке и выполните его выбор
    5. На устройстве запишите PIN-код, который автоматически присваивается мастером
    6. Устройство должно быть сопряжено с компьютером
    7. Щёлкните "Проверить" для записи 5-ти секундного звукового фрагмента и его воспроизведения на устройстве с bluetooth
ПРОВЕРКА:
    Был ли слышен звук записанный вами с использованием bluetooth? ЦЕЛЬ:
    Данный тест проверит возможность передачи информации через bluetooth соединение
ДЕЙСТВИЯ:
    1. Убедитесь, что вы имеете возможность просматривать файлы в вашем мобильном устройстве
    2. Скопируйте файл с вашего компьютера на мобильное устройство
    3. Скопируйте файл с мобильного устройства на ваш компьютер
ПРОВЕРКА:
    Все ли файлы были скопированы без ошибок? ЦЕЛЬ:
    Тест позволит убедиться в возможности использования BlueTooth HID устройства
ДЕЙСТВИЯ:
    1. Включите мышь или клавиатуру с Bluetooth
    2. Щёлкните на значке Bluetooth на панели меню
    3. Выберите 'Установить новое устройство'
    4. Найдите устройство в списке и выберите его
    5. В случае с мышью, выполните действия такие как перемещение указателя, щелчки правой и левой кнопкой, а также двойные щелчки
    6. В случае с клавиатурой, щёлкните кнопку Проверить, чтобы запустить средство проверки. Введите текст, а затем закройте окно.
ПРОВЕРКА:
    Работает ли устройство надлежащим образом? ЦЕЛЬ:
        Проверка возможности использовать устройства USB HID 
ДЕЙСТВИЯ:
    1. Включите мышь или клавиатуру, использующую интерфейс USB
    2. В случае с мышью, выполните такие действия как движение указателя, щелчки правой и 
        левой кнопкой, а также двойные щелчки
    3. В случае с клавиатурой, щёлкните на кнопке Проверить, чтобы открыть вспомогательное 
        окно теста. Введите любой текст и закройте окно.
ПРОВЕРКА:
        Работало ли устройство надлежащим образом? ЦЕЛЬ:
    Эта проверка позволит убедиться в возможности определения накопителей с интерфейсом USB.
ДЕЙСТВИЯ:
    1. Подсоедините один или более портативных USB устройств или жёстких дисков.
    2. Щёлкните "Проверить".
СВЕДЕНИЯ:
    $output
ПРОВЕРКА:
    Накопители были определены? ЦЕЛЬ:
    Эта проверка позволит убедиться в возможности определения подсоединённого жёсткого диска с интерфейсом FireWire
ДЕЙСТВИЯ:
    1. Щёлкните 'Проверить', чтобы выполнить проверку. Если подсоединение 
       не было определено в течении 20 секунд, тест будет не пройден.
    2. Подсоедините жёсткий диск с интерфейсом FireWire в свободный разъём FireWire.
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте автоматически
    выбранный результат ЦЕЛЬ:
    Этот тест проверяет способность системы определить подключение жёсткого диска eSATA
ДЕЙСТВИЯ:
    1. Нажмите «Проверить», чтобы начать проверку. Этот тест будет считаться непройденным,
       если подключение не будет обнаружено в течение 20 секунд.
    2. Подключите жёсткий диск с интерфейсом eSATA к доступному порту eSATA.
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте автоматически выбранный результат ЦЕЛЬ:
    Эта проверка позволит убедиться в возможности определения отсоединённого жёсткого диска с интерфейсом FireWire
ДЕЙСТВИЯ:
    1. Щёлкните 'Проверить', чтобы выполнить проверку. Если отсоединение 
       не было определено в течении 20 секунд, тест будет не пройден.
    2. Отсоедините ранее подсоединённый жёсткий диск с интерфейсом FireWire из разъёма FireWire.
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте автоматически
    выбираемый результат ЦЕЛЬ:
    Этот тест проверяет способность системы обнаружить отсоединение жёсткого диска eSATA
ДЕЙСТВИЯ:
    1. Нажмите «Проверить», чтобы начать проверку. Этот тест будет считаться непройденным,
       если отсоединение диска не будет обнаружено в течение 20 секунд.
    2. Отсоедините подключённый жёсткий диск с интерфейсом eSATA от порта eSATA.
ПРОВЕРКА:
    Проверка этого теста автоматизирована. Не изменяйте автоматически выбранный результат ЦЕЛЬ:
    Эта проверка позволяет убедиться в надлежащем выполнении перехода в спящий режим (если поддерживается)
ДЕЙСТВИЯ:
    1. Щёлкните Проверить
    2. Система перейдёт в спящий режим и выйдет из него самостоятельно в течении 5 минут
    3. Если система не вышла самостоятельно по прошествии 5 минут, нажмите кнопку включения/выключения электропитания для выполнения выхода из спящего режима.
    4. Если обнаружена ошибка при выходе из спящего режима, пожалуйста, перезапустите Проверку системы и отметьте эту проверку как не пройденную
ПРОВЕРКА:
    Выполнен ли переход и выход из спящего режима надлежащим образом? ЦЕЛЬ:
    Это тест для проверки возможности воспроизведения аудио-CD
ДЕЙСТВИЯ:
    1. Вставьте аудио-CD в привод оптических дисков
    2. После приглашения запустите музыкальный проигрыватель
    3. Найдите компакт-диск в окне музыкального проигрывателя
    4. Выберите компакт-диск в музыкальном проигрывателе
    5. Щёлкните на кнопке воспроизведения, чтобы прослушать музыку с компакт-диска
    6. Через некоторое время остановите воспроизведение
    7. Щёлкните правой кнопкой на значке компакт-диска и выберите «Извлечь диск»
    8. Компакт-диск должен быть извлечён из привода
    9. Закройте музыкальный проигрыватель
ПРОВЕРКА:
    Все ли шаги удалось выполнить успешно? ЦЕЛЬ:
    Эта проверка позволяет удостовериться в возможности воспроизведения DVD
ДЕЙСТВИЯ:
    1. Вставьте DVD содержащий любую видеозапись в дисковод оптических дисков
    2. Щёлкните "Проверить" для воспроизведения DVD в Totem
ПРОВЕРКА:
    Был ли файлы воспроизведён? ЦЕЛЬ:
    Проверка соединения USB 3.0.
ДЕЙСТВИЯ:
    1. Подсоедините жёсткий диск с интерфейсом USB 3.0 или внешний накопитель
        в порт USB 3.0 компьютера.
    2. На панели запуска должен появится значок.
    3. Щёлкните "Проверить", чтобы начать тест.
ПРОВЕРКА:
    Проверка автоматизирована. Не изменяйте автоматически
    выбранный результаты. ЦЕЛЬ:
    Эта проверка позволит убедиться в работоспособности соединения USB.
ДЕЙСТВИЯ:
    1. Присоедините устройство хранения USB во внешний разъём USB на этом компьютере.
    2. Должен появиться значок на панели запуска.
    3. Убедитесь, что значок появился.
    4. Выполните извлечение устройства.
    5. Повторите тоже самое с каждый внешним разъёмом USB.
ПРОВЕРКА:
    Все ли разъёмы USB работают надлежащим образом с устройством? ЦЕЛЬ:
    Эта проверка позволит вам убедиться в работоспособности вашего USB соединения.
ДЕЙСТВИЯ:
    1. Подсоедините жёсткий диск с интерфейсом USB или портативный накопитель к компьютеру.
    2. На панели запуска должен появиться значок.
    3. Щёлкните "Проверить" для начала выполнения проверки.
ПРОВЕРКА:
    Проверка автоматизирована. Не изменяйте автоматически выбираемый результат. ЦЕЛЬ:
    Проверка поддержки вашим монитором режима энергосбережения
ДЕЙСТВИЯ:
    1. Нажмите «Проверить», чтобы попытаться перевести монитор в режим энергосбережения
    2. Нажмите любую клавишу для выхода из этого режима
ПРОВЕРКА:
    Погас ли экран монитора и включился ли он снова? ЦЕЛЬ:
    Проверка цикла выключения/загрузки компьютера.
ДЕЙСТВИЯ:
    1. Выключите компьютер.
    2. Включите компьютер.
    3. Повторите этапы 1 и 2 по меньшей мере 5 раз.
ПРОВЕРКА:
    Правильно ли выключалась и перезагружалась система? ЦЕЛЬ:
    Эта проверка позволит убедиться в работоспособности проводного соединения
ДЕЙСТВИЯ:
    1. Щёлкните на значке сети в верхней панели
    2. Выберите сеть под элементом "Проводная сеть"
    3. Щёлкните "Проверить" для проверки возможности установления HTTP соединения
ПРОВЕРКА:
    Было ли отображено оповещение об установлении соединения? ЦЕЛЬ:
    Этот тест позволит проверить ваше беспроводное соединение.
ДЕЙСТВИЯ:
    1. Щёлкните на значке сети на верхней панели.
    2. Выберите сеть из раздела 'Беспроводные сети'.
    3. Щёлкните "Проверить" для выполнения возможности установки НTTP соединения.
ПРОВЕРКА:
    Было ли показано уведомление, а также работоспособно ли соединение? ЦЕЛЬ:
    Эта проверка позволит выполнить цикл переходов по определённым режимам экрана
ДЕЙСТВИЯ:
    1. Щёлкните "Проверить" и экран пройдёт цикл переходов по режимам экрана
ПРОВЕРКА:
    Отображение в определённом режиме на экране выглядит надлежащим образом? ЦЕЛЬ:
    Эта проверка позволит отправить изображение 'JPEG_Color_Image_Ubuntu.jpg' на заданное устройство
ДЕЙСТВИЯ:
    1. Щёлкните "Проверить", после чего вам будет необходимо ввести имя устройства с Bluetooth, которое поддерживает передачу файлов (это может занять несколько секунд после ввода имени, после чего начнётся передача файла)
    2. Согласитесь со всеми запросами, которые будут появляться на обоих устройствах
ПРОВЕРКА:
    Были ли данные переданы надлежащим образом? ЦЕЛЬ:
    Этот тест предназначен для проверки поворота экрана
ДЕЙСТВИЯ:
    1. Щёлкните "Проверить", чтобы повернуть экран. Экран будет поворачиваться каждые 4 секунды.
    2. Проверьте, все ли операции поворота (стандартный, перевёрнутый, вправо, влево) прошли успешно без постоянных неполадок
         экрана
ПРОВЕРКА:
    Выполнены ли повороты без постоянных неполадок экрана? ЦЕЛЬ:
    Эта проверка позволит убедиться в работоспособности клавиш яркости
ДЕЙСТВИЯ:
    1. Нажимайте клавиши яркости на клавиатуре
ПРОВЕРКА:
    Происходит ли изменение яркости при нажатии на клавиши? ЦЕЛЬ:
    Проверка работоспособности клавиш управления яркостью после выхода из ждущего режима
ДЕЙСТВИЯ:
    1. Нажимайте клавиши управления яркостью на клавиатуре
ПРОВЕРКА:
    Изменяется ли яркость при нажатии этих клавиш после выхода из ждущего режима? ЦЕЛЬ:
    Будет выполнения проверка экрана по умолчанию
ДЕЙСТВИЯ:
    1. Щёлкните "Проверить" для отображения видео теста.
ПРОВЕРКА:
    Видите ли вы цветные полосы и статические? ЦЕЛЬ:
    Проверка клавиши отключения звука на вашей клавиатуре
ДЕЙСТВИЯ:
    1. Нажмите «Проверить», чтобы открыть окно для проверки клавиши отключения звука.
    2. Если клавиша работает, тест будет считаться пройденным и окно закроется.
ПРОВЕРКА:
    Работает ли клавиша так, как ожидалось? ЦЕЛЬ:
    Эта проверка позволит убедиться в работоспособности клавиши перехода в спящий режим
ДЕЙСТВИЯ:
    1. Нажмите клавишу перехода в спящий режим на клавиатуре
ПРОВЕРКА:
    Происходит ли переход системы в спящий режим после нажатия на клавишу спящего режима? ЦЕЛЬ:
    Этот тест проверит клавишу "Super" вашей клавиатуры
ДЕЙСТВИЯ:
    1. Щёлкните Проверить, чтобы открыть окно в котором будет производиться проверка клавиши "Super".
    2. Если клавиша работает, тест будет пройден, а окно будет закрыто.
ПРОВЕРКА:
    Работает ли клавиша "Supe" надлежащим образом? ЦЕЛЬ:
    Эта проверка позволит убедиться в работоспособности клавиатуры
ДЕЙСТВИЯ:
    1. Щёлкните Проверить
    2. Введите текст в текстовом поле используя клавиатуру
ПРОВЕРКА:
    Клавиатура работает надлежащим образом? ЦЕЛЬ:
    Эта проверка позволяет убедиться в работоспособности манипулятора
ДЕЙСТВИЯ:
    1. Переместите указатель, используя манипулятор или сенсорный экран.
    2. Выполните несколько одиночных или двойных щелчков и щелчков правой кнопкой.
ПРОВЕРКА:
    Работает ли манипулятор так, как ожидалось? ЦЕЛЬ:
    Эта проверка позволит убедиться в работоспособности графического интерфейса пользователя, после того как пользователь самостоятельно изменил разрешение
ДЕЙСТВИЯ:
    1. Откройте приложение «Настройка экранов»
    2. Выберите новое разрешение из раскрывающегося списка
    3. Щёлкните на кнопке «Применить»
    4. Выберите стандартное разрешение из раскрывающегося списка
    5. Щёлкните на кнопке «Применить»
ПРОВЕРКА:
    Было ли разрешение изменено так, как ожидалось? ЦЕЛЬ:
    Эта проверка позволит узнать разрешение экрана по умолчанию
ДЕЙСТВИЯ:
    1. Этот экран использует следующее разрешение:
СВЕДЕНИЯ:
    $output
ПРОВЕРКА:
    Устраивает ли вас отображение на экране? ЦЕЛЬ:
    Чтобы убедиться, что нажатие клавиши быстрого доступа Wi-Fi не приводит к тому, что система не отвечает на запросы или исчезновению мини-программы с верхней панели
ДЕЙСТВИЯ:
    1. Выполните вход на рабочий стол
    2. Нажимайте клавишу быстрого доступа к Wi-Fi с частотой 1 нажатие в секунду, постепенно увеличивая скорость прикосновений, до тех пор пока частота прикосновений не станет максимально возможной 
ПРОВЕРКА:
    Убедитесь, что система отвечает на запросы, а мини-программы Wi-Fi и Bluetooth отображаются и работоспособны ЦЕЛЬ:
    Проверка светодиодного индикатора сенсорной панели
ДЕЙСТВИЯ:
    1. Щёлкните на кнопке сенсорной панели или нажмите сочетание клавиш быстрого доступа, чтобы включить/отключить кнопку сенсорной панели
    2. Перемещайте палец по сенсорной панели
ПРОВЕРКА:
    1. Состояние светодиодного индикатора должно меняться при каждом нажатии на кнопке или нажатии сочетания клавиш быстрого доступа
    2. Когда светодиодный индикатор загорелся, вы должны получить возможность управлять указателем мыши при помощи сенсорной панели
    3. Когда светодиодный индикатор выключен, вы не будете иметь возможность управлять указателем мыши при помощи сенсорной панели ЦЕЛЬ:
    Проверка работоспособности светодиодного индикатора после выхода из ждущего режима
ДЕЙСТВИЯ:
    1. Щёлкните на кнопке сенсорной панели или нажмите сочетание клавиш для включения и выключения сенсорной панели
    2. Перемещайте палец по сенсорной панели
ПРОВЕРКА:
    1. Состояние светодиодного индикатора должно изменяться каждый раз при нажатии кнопки или сочетания клавиш
    2. Когда индикатор светится, можно использовать сенсорную панель для управления указателем мыши
    3. Когда индикатор не светится, сенсорную панель нельзя использовать для управления указателем мыши ЦЕЛЬ:
    Проверка горизонтальной прокрутки с помощью сенсорной панели
ДЕЙСТВИЯ:
    1. Когда будете готовы, нажмите «Проверить» и поместите указатель в границах проверочного окна.
    2. Проверьте, можете ли вы переместить горизонтальный ползунок, передвигая палец вправо и влево в нижней части сенсорной панели.
ПРОВЕРКА:
    Удалось ли выполнить прокрутку вправо и влево? ЦЕЛЬ:
    Ручной режим определения возможности множественного касания.
ДЕЙСТВИЯ:
    1. Обратитесь к техническим характеристикам вашей системы.
ПРОВЕРКА:
    Поддерживает ли ли сенсорная панель возможность множественного касания? ЦЕЛЬ:
    Проверка вертикальной прокрутки с помощью сенсорной панели
ДЕЙСТВИЯ:
    1. Когда будете готовы, нажмите «Проверить» и поместите указатель в границах проверочного окна.
    2. Проверьте, можете ли вы переместить вертикальный ползунок, передвигая палец вверх и вниз в правой части сенсорной панели.
ПРОВЕРКА:
    Удалось ли выполнить прокрутку вверх и вниз? ЦЕЛЬ:
    Ручное определение возможности множественного касания.
ДЕЙСТВИЯ:
    1. Обратитесь к техническим характеристикам вашей системы.
ПРОВЕРКА:
     Поддерживает ли экран возможность множественного касания? ЦЕЛЬ:
    Проверка включения и выключения светодиодного индикатора Bluetooth при включении Bluetooth и его отключении
ДЕЙСТВИЯ:
    1. Отключите Вluetooth используя физический переключатель (если оснащено)
    2. Включите Вluetooth
    3. Отключите Вluetooth используя мини-приложение на панели
    4. Включите Вluetooth повторно
ПРОВЕРКА:
    Включался ли светодиодный индикатор Вluetooth дважды при включении и отключении? ЦЕЛЬ:
    Проверка работоспособности клавиши переключения на внешнее видео
ДЕЙСТВИЯ:
    1. Подсоедините внешний монитор
    2. Нажмите сочетание клавиш для изменения настроек монитора
ПРОВЕРКА:
    Убедитесь, что видео сигнал может быть отражён, расширен, отображён как на внешнем так и встроенном дисплее. ЦЕЛЬ:
    Проверить отображение светодиодным индикатором состояние низкого уровня заряда аккумулятора
ДЕЙСТВИЯ:
    1. Дайте компьютеру несколько часов поработать от аккумулятора
    2. Внимательно следите за светодиодным индикатором аккумулятора
ПРОВЕРКА:
    Стал ли светодиодный индикатор оранжевым при низком уровне заряда аккумулятора? ЦЕЛЬ:
    Проверить, правильно ли светодиодный индикатор аккумулятора отображает состояние полной зарядки
ДЕЙСТВИЯ:
    1. Дайте компьютеру поработать немного от аккумулятора
    2. Подключите его к сети переменного тока
    3. Дайте компьютеру поработать, питаясь от сети переменного тока
ПРОВЕРКА:
    Погас ли оранжевый индикатор после полной зарядки аккумулятора? ЦЕЛЬ:
    Проверить, что светодиодный индикатор отображает состояние зарядки аккумулятора
ДЕЙСТВИЯ:
    1. Дайте компьютеру немного поработать от аккумулятора
    2. Подключите компьютер к сети переменного тока
ПРОВЕРКА:
    Изменил ли светодиодный индикатор состояния аккумулятора свой цвет на оранжевый? ЦЕЛЬ:
    Проверить, что индикатор аккумулятора показывает статус «заряжается» после выхода из ждущего режима
ДЕЙСТВИЯ:
    1. Дайте системе поработать некоторое время от аккумулятора
    2. Подключите адаптер переменного тока
ПРОВЕРКА:
    Меняется ли цвет светодиодного индикатора на оранжевый после выхода из ждущего режима? ЦЕЛЬ:
    Проверка работоспособности светодиодного индикатора камеры после выхода из ждущего режима
ДЕЙСТВИЯ:
    1. Выберите Проверить, чтобы включить камеру
    2. Светодиодный индикатор должен загореться на несколько секунд
ПРОВЕРКА:
    Загорается и отключается ли светодиодный индикатор камеры после выхода из ждущего режима? ЦЕЛЬ:
    Проверить, что светодиодный индикатор питания работает так же после выхода из ждущего режима
ДЕЙСТВИЯ:
    1. Индикатор питания должен светиться, когда устройство включено
ПРОВЕРКА:
    Продолжает ли светодиодный индикатор питания светиться после выхода из ждущего режима? ЦЕЛЬ:
    Проверка клавиши включения и выключения функциональности сенсорной панели
ДЕЙСТВИЯ:
    1. Проверьте работоспособность сенсорной панели
    2. Коснитесь клавиши быстрого доступа сенсорной панели
    3. Коснитесь клавиши быстрого доступа сенсорной панели снова
ПРОВЕРКА:
    Убедитесь, что сенсорная панель была отключена, а затем повторно включена. ЦЕЛЬ:
    Выход из ждущего режима по нажатию клавиши на USB-клавиатуре
ДЕЙСТВИЯ:
    1. Включите «Wake by USB KB/Mouse» в BIOS
    2. Нажмите «Проверить», чтобы перейти в ждущий режим (S3)
    3. Нажмите любую клавишу на USB-клавиатуре, чтобы вернуть систему в рабочий режим
ПРОВЕРКА:
    Вышла ли система из ждущего режима при нажатии клавиши на клавиатуре? ЦЕЛЬ:
    Выход из ждущего режима с помощью мыши с интерфейсом USB
ДЕЙСТВИЯ:
    1. Включите «Wake by USB KB/Mouse» в BIOS
    2. Нажмите «Проверить», чтобы перейти в ждущий режим (S3)
    3. Нажмите любую кнопку мыши с интерфейсом USB, чтобы вернуть систему в рабочий режим
ПРОВЕРКА:
    Вышла ли система из ждущего режима при нажатии кнопки мыши? ЦЕЛЬ:
    Проверка индикатора беспроводной сети (WLAN + Bluetooth)
ДЕЙСТВИЯ:
    1. Убедитесь, что соединение WLAN установлено и включён Bluetooth.
    2. Должен загореться светодиодный индикатор WLAN/Bluetooth
    3. Выключите WLAN и Bluetooth используя аппаратный переключатель (если оснащено)
    4. Включите снова
    5. Выключите WLAN и Bluetooth используя мини-программу на панели
    6. Включите снова
ПРОВЕРКА:
    Включался ли светодиодный индикатор WLAN/Bluetooth надлежащим образом? ЦЕЛЬ:
   Это автоматизированный стресс-тест, который на протяжении 250 циклов, будет принудительно переводить компьютер в спящий режим и выходить из него ЦЕЛЬ:
   Это автоматизированный стресс-тест, который на протяжении 30 циклов, будет принудительно переводить компьютер в спящий режим и выходить из него ЦЕЛЬ:
   Это автоматизированный стресс-тест, который на протяжении 250 циклов, будет принудительно переводить компьютер в ждущий режим и выходить из него ЦЕЛЬ:
   Это автоматизированный стресс-тест, который на протяжении 30 циклов, будет принудительно переводить компьютер в ждущий режим и выходить из него ЦЕЛЬ:
   Этот тест проверяет работу USB DSL или мобильного широковещательного модема
ДЕЙСТВИЯ:
   1. Подключите USB-кабель к компьютеру
   2. Правый-клик на иконке сети в панели
   3. Выберите "Изменить соединения"
   4. Выберите закладку "DSL" (для ADSL модема) или "Мобильные" (для 3G-модема)
   5. Кликните на кнопку "Добавить"
   6. Настройте параметры соединения
   7. Уведомление должно подтвердить что соединение установлено
   8. Выберите Тест для проверки возможности установления HTTP соединения
ПРОВЕРКА:
   Соединение установлено корректно? ЦЕЛЬ:
   Тест позволяет проверить считыватель отпечатков пальцев, который может использоваться для разблокирования заблокированного компьютера.
ДЕЙСТВИЯ:
   1. Щёлкните на индикаторе сеанса (Значок с изображением шестерёнки, расположенной с левой стороны панели) .
   2. Выберите 'Заблокировать экран'.
   3. Нажмите любую клавишу или подвигайте мышью.
   4. Раскроется окно, которое предоставляет возможность разблокирования компьютера как с вводом пароля, так и с помощью аутентификации через считыватель отпечатков пальцев.
   5. Используйте считыватель отпечатков пальцев для разблокирования.
   6. Теперь ваш экран должен быть разблокирован.
ПРОВЕРКА:
   Прошла ли процедура аутентификации правильно? ЦЕЛЬ:
   Эта проверка позволит удостовериться в работоспособности сетевого принтера
ДЕЙСТВИЯ:
   1. Убедитесь, что принтер доступен в сети
   2. Щёлкните на крайнем правом значке в верхнем углу экрана и щёлкните на элементе Принтеры
   3. Если принтер отсутствует в списке, щёлкните Добавить
   4. Притер должен определиться и будут отображены соответствующие значения конфигурации
   5. Напечатать пробную страницу
ПРОВЕРКА:
   Удалось ли вам напечатать пробную страницу используя сетевой принтер? ЦЕЛЬ:
   Этот тест проверит правильность даты и времени, часов рабочего стола
ДЕЙСТВИЯ:
   1. Проверьте часы в верхнем правом углу вашего рабочего стола.
ПРОВЕРКА:
   Отображают ли часы правильную дату и время в соответствии с вашей часовой зоной? ЦЕЛЬ:
   Этот тест проверяет, чтобы часы шли синхронно с системными часами.
ДЕЙСТВИЯ:
   1. Щёлкните "Проверить" и убедитесь, что часы были переведены на 1 час вперёд.
   Заметка: это может занять минуту или придётся подождать обновления часов
   2. Щёлкните ПКМ на часах и выберите "Параметры времени и даты"
   3. Убедитесь что часы установлены в "Ручную".
   4. Измените время на 1 час назад
   5. Закройте окно и перезагрузитесь
ПРОВЕРКА:
   Ваши системные часы показывают правильно дату и время для вашей временной зоны? ЦЕЛЬ:
   Эта проверка позволит удостовериться в возможности перезагрузки системы из меню рабочего стола
ДЕЙСТВИЯ:
   1. Щёлкните на значке шестерни в правом верхнем углу рабочего стола и выберите в меню пункт «Выключить...»
   2. Щёлкните на кнопке «Перезагрузить», расположенной в левой части диалога «Выключение»
   3. После выполнения входа, перезапустите приложение «Проверка системы», после чего проверка возобновится с этого места
ПРОВЕРКА:
   Была ли система перезагружена, а вход через графический интерфейс пользователя отображён надлежащим образом? ЦЕЛЬ:
   Эта проверка позволит убедиться в возможности воспроизведения звуковых файлов в формате Ogg Vorbis.
ДЕЙСТВИЯ:
   1. Щёлкните Проверить для воспроизведения файла Ogg Vorbis (.ogg)
   2. Пожалуйста, закройте проигрыватель для продолжения.
ПРОВЕРКА:
   Звуковой фрагмент воспроизведён корректно? ЦЕЛЬ:
   Эта проверка позволит убедиться в возможности воспроизведения звуковых файлов в формате Wav.
ДЕЙСТВИЯ:
   1. Щёлкните Проверить для воспроизведения файла формата Wav
   2. Пожалуйста, закройте проигрыватель для продолжения.
ПРОВЕРКА:
   Звуковой фрагмент воспроизведён корректно? ЦЕЛЬ:
 Имитация неполадки во время перезагрузки системы
ДЕЙСТВИЯ:
 1. Щёлкните Проверить, чтобы начать перезагрузку
 2. Выберите "Продолжить" после входа в сеанс и перезапуска проверки системы
ПРОВЕРКА:
 Вы не увидите проверку подлинности пользователя ЦЕЛЬ:
 Эта проверка позволит убедиться в работоспособности gcalctool (Калькулятор).
ДЕЙСТВИЯ:
 Щёлкните на кнопке "Проверить" для открытия калькулятора и затем выполните следующие операции:
  1. Вырезать
  2. Копировать
  3. Вставить
ПРОВЕРКА:
 Операции были выполнены надлежащим образом? ЦЕЛЬ:
 Эта проверка позволит убедиться в работоспособности gcalctool (Калькулятор).
ДЕЙСТВИЯ:
 Щёлкните на кнопке "Проверить" для открытия калькулятора и выполните следующие действия:
  1. Занесите число в память (MS)
  2. Вызовите число из памяти (MR)
  3. Очистка последнего добавленного в память числа (MLC)
  4. Очистите память (MС)
ПРОВЕРКА:
 Функции были выполнены надлежащим образом? ЦЕЛЬ:
 
 Эта проверка позволит убедиться в работоспособности gcalctool (Калькулятор).
ДЕЙСТВИЯ:
Щёлкните на кнопке "Проверить" для открытия калькулятора и выполните следующие действия: 
 
 1. Простые математические функции (+,-/, *)
 
 2. Вложенные математические функции ((,))
 
 3. Действия с дробными числами
 
 4. Действия с десятичными числами
ПРОВЕРКА:
 
 Функции были выполнены надлежащим образом? ЦЕЛЬ:
 Эта проверка позволит убедиться в работоспособности gcalctool (Calculator).
ДЕЙСТВИЯ:
 Щёлкните на кнопке "Проверить" для открытия калькулятора.
ПРОВЕРКА:
 Запуск произведён надлежащим образом? ЦЕЛЬ:
 Эта проверка позволит убедиться в работоспособности gedit (Текстовый редактор).
ДЕЙСТВИЯ:
 1. Щёлкните на кнопке "Проверить" для открытия gedit, затем откройте в нём созданный вами ранее файл.
 2. Внесите изменения в текст, затем сохраните в файл и закройте gedit.
ПРОВЕРКА:
 Выполнено надлежащим образом? ЦЕЛЬ:
 Эта проверка позволит убедиться в работоспособности gedit (Текстовый редактор).
ДЕЙСТВИЯ:
 1. Щёлкните на кнопке "Проверить" для открытия gedit.
 2. Введите любой текст и сохраните в файл (запомните присвоенное файлу имя), затем закройте gedit.
ПРОВЕРКА:
 Выполнено надлежащим образом? ЦЕЛЬ:
 Этот тест проверит, как работает программа обмена сообщениями Empathy.
ДЕЙСТВИЯ:
 1. Выберите «Проверить» для запуска Empathy.
 2. Настройте её для подключения к сервису AOL Instant Messaging (AOL).
 3. После того, как вы завершите тест, закройте Empathy для продолжения.
ПРОВЕРКА:
 Удалось ли вам корректно подключиться и отправлять (принимать) сообщения? ЦЕЛЬ:
 Этот тест проверит, как работает программа обмена сообщениями Empathy.
ДЕЙСТВИЯ:
 1. Выберите «Проверить» для запуска Empathy.
 2. Настройте её для подключения к сервису чата Facebook.
 3. После того, как вы завершите тест, закройте Empathy для продолжения.
ПРОВЕРКА:
 Удалось ли вам корректно подключиться и отправлять (принимать) сообщения? ЦЕЛЬ:
 Этот тест проверит, как работает программа обмена сообщениями Empathy.
ДЕЙСТВИЯ:
 1. Выберите «Проверить» для запуска Empathy.
 2. Настройте её для подключения к сервису Google Talk (gtalk).
 3. После того, как вы завершите тест, закройте Empathy для продолжения.
ПРОВЕРКА:
 Удалось ли вам корректно подключиться и отправлять (принимать) сообщения? ЦЕЛЬ:
 Этот тест проверит, как работает программа обмена сообщениями Empathy.
ДЕЙСТВИЯ:
 1. Выберите «Проверить» для запуска Empathy.
 2. Настройте её для подключения к сервису Jabber.
 3. После того, как вы завершите тест, закройте Empathy для продолжения.
ПРОВЕРКА:
 Удалось ли вам корректно подключиться и отправлять (принимать) сообщения? ЦЕЛЬ:
Этот тест проверит, как работает программа обмена сообщениями Empathy.
ДЕЙСТВИЯ:
1. Выберите «Проверить» для запуска Empathy.
2. Настройте её для подключения к сервису чата Microsoft Network (MSN).
3. После того, как вы завершите тест, закройте Empathy для продолжения.
ПРОВЕРКА:
Удалось ли вам корректно подключиться и отправлять (принимать) сообщения? ЦЕЛЬ:
 Эта проверка позволяет удостовериться в работоспособности Evolution.
ДЕЙСТВИЯ:
 1. Щёлкните на кнопке "Проверить" для запуска Evolution.
 2. Настройте программу для подключения с использованием учётной записи IMAP.
ПРОВЕРКА:
 Появилась ли возможность получать и читать сообщения электронной почты надлежащим образом? ЦЕЛЬ:
 Эта проверка позволяет удостовериться в работоспособности Evolution.
ДЕЙСТВИЯ:
 1. Щёлкните на кнопке "Проверить" для запуска Evolution.
 2. Настройте программу для подключения с использованием учётной записи POP3.
ПРОВЕРКА:
 Появилась ли возможность получать и читать сообщения электронной почты надлежащим образом? ЦЕЛЬ:
 Эта проверка позволяет удостовериться в работоспособности Evolution.
ДЕЙСТВИЯ:
 1. Щёлкните на кнопке "Проверить" для запуска Evolution.
 2. Настройте программу для подключения с использованием учётной записи SMTP.
ПРОВЕРКА:
 Отправление сообщений электронной почты выполняется без ошибок? ЦЕЛЬ:
 Этот тест проверит, может ли Firefox проигрывать видеозаписи в формате Flash.
 Примечание: это может потребовать установки дополнительного ПО для успешного завершения.
ДЕЙСТВИЯ:
 1. Выберите «Проверить» для запуска Firefox и просмотра короткой видеозаписи в формате Flash.
ПРОВЕРКА:
 Была ли видеозапись воспроизведена правильно? ЦЕЛЬ:
 Эта проверка позволит убедиться, в возможности воспроизведения видеофрагментов в формате Quicktime (.mov) в веб-обозревателе Firefox.
 Примечание: для выполнения проверки, возможно потребуется установка дополнительного программного обеспечения.
ДЕЙСТВИЯ:
 1. Выберите Проверить для запуска веб-обозревателя Firefox и видеофрагмента.
ПРОВЕРКА:
 Воспроизвёлся ли видеофрагмент с использованием надстройки? ЦЕЛЬ:
 Эта проверка позволит убедиться в возможности обработки обычной веб-страницы в Firefox.
ДЕЙСТВИЯ:
 1. Выберите Проверить для запуска Firefox и просмотра пробной веб-страницы.
ПРОВЕРКА:
 Пробная страница Ubuntu была загружена правильно? ЦЕЛЬ:
 Эта проверка позволит убедиться в возможности выполнения на веб-странице мини-приложения Java в Firefox. Примечание:
 может потребоваться установка дополнительного программного обеспечения для завершения выполнения проверки.
ДЕЙСТВИЯ:
 1. Выберите Проверить для открытия Firefox с пробной страницей, содержащий компонент Java и следуйте указанному в ней руководству.
ПРОВЕРКА:
 Было ли отображено мини-приложение? ЦЕЛЬ:
 Эта проверка позволит убедиться в возможности выполнения Flash приложений в Firefox. Примечание:
 может потребоваться установка дополнительного программного обеспечения для завершения выполнения проверки.
ДЕЙСТВИЯ:
 1. Выберите Проверить для открытия Firefox с Flash-содержимым.
ПРОВЕРКА:
 Видите ли вы текст? ЦЕЛЬ:
 Эта проверка позволит удостовериться в работоспособности терминала Gnome Terminal.
ДЕЙСТВИЯ:
 1. Щёлкните на кнопке "Проверить" для открытия Терминала.
 2. Введите 'ls' и нажмите клавишу Ввод. Вы увидите перечень файлов и папок в домашней папке.
 3. Закройте окно терминала.
ПРОВЕРКА:
 Выполнено надлежащим образом? ЦЕЛЬ:
 Эта проверка позволяет удостовериться в возможности копирования файлов используя обозреватель файлов.
ДЕЙСТВИЯ:
 1. Щёлкните Проверить для открытия обозревателя файлов.
 2. Щёлкните правой кнопкой на файле с именем Test File 1 и щёлкните Копировать.
 3. Щёлкните правой кнопкой в пустой области и щёлкните Вставить.
 4. Щёлкните правой кнопкой на файле с именем Test File 1(копия) и щёлкните Переименовать.
 5. Введите имя Test File 2 в поле названия и нажмите клавишу Ввод (Enter).
 6. Закройте обозреватель файлов.
ПРОВЕРКА:
 Был ли создан файл с именем Test File 2? ЦЕЛЬ:
 Эта проверка позволит удостовериться в возможности копирования папок используя обозреватель файлов
ДЕЙСТВИЯ:
 1. Щёлкните Проверить для открытия обозревателя файлов.
 2. Щёлкните правой кнопкой на папке с именем Test Folder и щёлкните Копировать.
 3. Щёлкните правой кнопкой в любом пустом месте окна и щёлкните Вставить.
 4. Щёлкните правой кнопкой на папке с именем Test Folder(копия) и щёлкните Переименовать.
 5. Введите имя Test Data в поле названия и нажмите клавишу Ввод (Enter).
 6. Закройте обозреватель файлов.
ПРОВЕРКА:
  Существует ли папка с именем Test Data? ЦЕЛЬ:
 Эта проверка позволяет удостовериться в возможности создания нового файла используя обозреватель файлов.
ДЕЙСТВИЯ:
 1. Щёлкните Проверить для открытия обозревателя файлов.
 2. Щёлкните правой кнопкой в пустой области и выберите Создать документ -> Пустой документ.
 3. Введите название Test File 1 в поле имени и нажмите клавишу Ввод (Enter).
 4. Закройте обозреватель файлов.
ПРОВЕРКА:
 Был ли создан файл с именем Test File 1? ЦЕЛЬ:
 Эта проверка позволит вам убедиться, в возможности создания новой папки обозревателем файлов.
ДЕЙСТВИЯ:
 1. Щёлкните Проверить для открытия обозревателя файлов.
 2. На панели меню, щёлкните Файл -> Создать новую папку.
 3. В поле имени новой папки, введите в качестве имени "Test Folder" и нажмите клавишу Ввода.
 4. Закройте обозреватель файлов.
ПРОВЕРКА:
 Создана ли новая папка с именем "Test Folder"? ЦЕЛЬ:
 Эта проверка позволит убедиться в возможности удаления файлов используя обозреватель файлов.
ДЕЙСТВИЯ:
 1. Щёлкните Проверить для открытия обозревателя файлов.
 2. Щёлкните правой кнопкой на файле с именем Test File 1 и щёлкните Удалить..
 3. Убедитесь, что файл Test File 1 удалён.
 4. Закройте обозреватель файлов.
ПРОВЕРКА:
  Был ли файл Test File 1 удалён? ЦЕЛЬ:
 Эта проверка позволит убедиться в возможности удаления папок используя обозреватель файлов.
ДЕЙСТВИЯ:
 1. Щёлкните Проверить для открытия обозревателя файлов.
 2. Щёлкните правой кнопкой на папке с именем Test Folder и щёлкните Удалить..
 3. Убедитесь, что папка была удалена.
 4. Закройте обозреватель файлов.
ПРОВЕРКА:
  Была ли удалена папка Test Folder? ЦЕЛЬ:
 Эта проверка позволит убедиться в возможности перемещения файлов используя обозреватель файлов.
ДЕЙСТВИЯ:
 1. Щёлкните Проверить для открытия обозревателя файлов.
 2. Щёлкните и перенесите файл с именем Test File 2 на значок папки  именем Test Data..
 3. Отпустите кнопку.
 4. Щёлкните дважды на значке Test Data для открытия этой папки.
 5. Закройте обозреватель файлов.
ПРОВЕРКА:
  Был ли файл Test File 2 перемещён в папку Test Data надлежащим образом? ЦЕЛЬ:
 Эта проверка позволит удостовериться в возможности менеджера обновлений осуществлять поиск обновлений.
ДЕЙСТВИЯ:
 1. Щёлкните Проверить для запуска менеджера обновлений.
 2. Следуйте запросам и если обновления найдены, установите их.
 3. После завершения работы менеджера обновлений, пожалуйста, закройте программу, щёлкнув на кнопке Закрыть в нижнем правом углу.
ПРОВЕРКА:
 Нашёл ли менеджер обновлений, обновления и установил ли их?  (Проверка пройдена, если обновления не найдены, не пройдена, если обновления найдены, но не установлены) ЦЕЛЬ:
 Эта проверка позволяет удостовериться в возможности перемещения папок используя обозреватель файлов.
ДЕЙСТВИЯ:
 1. Щёлкните Проверить для открытия обозревателя файлов.
 2. Щёлкните и перенесите папку с именем Test Data на значок под именем Test Folder.
 3. Отпустите кнопку.
 4. Щёлкните дважды на папке с именем Test Folder для её открытия.
 5. Закройте обозреватель файлов.
ПРОВЕРКА:
 Была ли папка с именем Test Data перемещена надлежащим образом в папку с именем Test Folder? Проверка часов на панели Тест проверки перезагрузки панели Просмотр файла Xorg.0.Log и определение X-драйвера и его версии Проверка периферийного оборудования Тесты Piglit Выполнить команду «ping» для ubuntu.com и перезапустить сетевые интерфейсы 100 раз Воспроизведите звуковой сигнал через выход по умолчанию и прослушайте его через вход по умолчанию. Пожалуйста, выберите (%s):  Пожалуйста, нажмите каждую клавишу на клавиатуре. Введите, пожалуйста, текст и нажмите Ctrl+D, когда закончите:
 Тесты указательных устройств. Проверка управления электропитанием Проверка управления электропитанием Чтобы продолжить, нажмите любую клавишу... Назад Вывести информацию о версии и выйти. Предоставляет сведения о подключённых к компьютеру дисплеях Предоставляет сведения о сетевых устройствах Сигнал выхода от клавиатуры Записать параметры микшера перед переходом в ждущий режим. Запомнить текущую сеть перед переходом в спящий режим. Запись текущего разрешения перед переходом в режим пониженного энергопотребления. Тесты проверки рендеринга Запустить повторно Перезапустить Сообщает название, имя драйвера, версию драйвера всех сенсорных панелей обнаруженных системой. Запуск теста производительности чтения-изменения-записи кэша Cachebench Запуск теста производительности чтения кэша Cachebench Запуск теста производительности записи кэша Cachebench Запустить тест производительности сжатия 7ZIP Запустить тест производительности сжатия PBZIP2 Запустить тест производительности кодирования MP3 Запустить набор автоматизированных тестов встроенных программ (FWTS). Запустить тест производительности GLmark2 Запустить тест производительности GLmark2-ES2 Запустить тест производительности GnuPG Запустить тест производительности Himeno Запуск теста производительности Lightsmark Запуск теста производительности N-Queens Запуск теста производительности сетевой петли Network Loopback Запуск теста производительности "gearsfancy" из Qgears2 для OpenGL Запуск теста производительности "image scaling" из Qgears2 для OpenGL Запуск теста производительности "gearsfancy" из Qgears2 для XRender Запуск теста производительности "image scaling" из Qgears2 для XRender Запустить тест производительности Render-Bench XRender/Imlib2 Запуск теста производительности Stream Add Запуск теста производительности Stream Copy Запуск теста производительности Stream Scale Запуск теста производительности Stream Triad Запуск теста производительности "Heaven" для Unigine Запуск теста производительности "Santuary" для Unigine Запуск теста производительности "Tropics" для Unigine Запустить нагрузочный тест на основе FurMark (OpenGL 2.1 или 3.2), полноэкранный режим 1920x1080, без сглаживания Запустить нагрузочный тест на основе FurMark (OpenGL 2.1 или 3.2), оконный режим 1024x640, без сглаживания Выполнить gtkperf для определения работоспособности тестов на базе GTK Выполнить графический стресс-тест. Эта проверка может занять несколько минут. Запустить тест производительности кодирования x264 H.264/AVC Запускается %s... Запускает тест, который 3 раза передаёт 100 файлов размером по 10 МБ на карту памяти SDHC. Запускает тест, который 3 раза передаёт 100 файлов размером по 10 МБ через порт USB. Запускает тесты piglit для проверки поддержки OpenGL 2.1 Запускает инструмент подведения итогов тестов piglit Сведения о устройствах SATA/IDE. SMART-тест Выделить всё Выделить всё Проверки серверных служб Краткая форма для --config=.*/jobs_info/blacklist. Краткая форма для --config=.*/jobs_info/blacklist_file. Краткая форма для --config=.*/jobs_info/whitelist. Краткая форма для --config=.*/jobs_info/whitelist_file. Пропустить "Дымовое" тестирование Слежка  за анализаторами траффика Проверка установки программного обеспечения Некоторые новые жёсткие диски имеют возможность "парковки" головок диска, через некоторый период времени бездействия. Это одна из возможностей энергосбережения, но это приводит к ненадлежащему взаимодействию с операционной системой в результате постоянной "парковки" и обращению. Это приводит к износу диска, что может привести к его ранней неисправности. Пробел после завершения Начать проверку Состояние Остановить процесс С терминала поступила команда Stop Стресс-тест выключения питания системы (100 циклов) Стресс-тест перезагрузки системы (100 циклов) Стресс-тесты Подробности сообщения Отправить результаты Проверка успешно завершена! Проверка ждущего режима Проверка ждущего режима Проверка 
системы Проверка системных служб Проверка системы Вкладка 1 Вкладка 2 Сигнал выхода Проверить Проверка пробуждения ACPI (fwts wakealarm) Проверить снова Проверить и выполнить задачу с помощью памяти. Тест отменён Тест джиттера таймера Проверить, если служба atd выполняется, когда пакет установлен. Проверить, если служба cron выполняется, когда пакет установлен. Проверить, если служба cupsd выполняется, когда пакет установлен. Проверить, если служба getty выполняется, когда пакет установлен. Проверить, если служба init выполняется, когда пакет установлен. Проверить, если служба klogd выполняется, когда пакет установлен. Проверить, если служба nmbd выполняется, когда пакет установлен. Проверить, если служба smbd выполняется, когда пакет установлен. Проверить, если служба syslogd выполняется, когда пакет установлен. Проверить, если служба udevd выполняется, когда пакет установлен. Проверить, если служба winbindd выполняется, когда пакет установлен. Тест прерван Проверка незадействованных ЦПУ в многоядерной системе. Проверяет, содержит ли каталог /var/crash какие-либо файлы. Отображает список файлов, если они имеются в каталоге, либо показывает состояние каталога (не существует/пуст) Проверка незадействованности процесса X в безопасном режиме. Проверка выполнения процесса X. Проверка возможности масштабирования ЦПУ, используя комплект тестов микропрограммы (fwts cpufreq). Проверка сети после выхода из режима пониженного энергопотребления. Тест проверяет, что система поддерживает виртуализацию и оснащена минимальным количеством ОЗУ, чтобы работать как OpenStack Compute Node Проверка определения звуковых устройств Проверка определения  доступных сетевых контроллеров Проверка определения оптических дисководов Тест, выводящий сведения о версии X.Org Проверка возможности синхронизации локальных часов с NTP сервером Убедимся, что разрешение системы осталось тем же, что было до перехода в режим пониженного энергопотребления. Протестируйте вашу систему и отправите результаты на Launchpad Проверена Тестирует oem-config с помощью Xpresser, а затем проверяет, что пользователь был создан успешно. Удаляет созданную учётную запись пользователя после прохождения теста. Проверяет возможности беспроводного оборудования подключаться к маршрутизатору используя протоколы WPA и 802.11b/g после перехода системы в ждущий режим. Тестирует возможность беспроводного оборудования подключиться к маршрутизатору по протоколу 802.11b/g используя WPA шифрование. Проверяет возможности беспроводного оборудования подключаться к маршрутизатору используя WPA и 802.11n, после перехода системы в ждущий режим. Тестирует возможность беспроводного оборудования подключиться к маршрутизатору по протоколу 802.11n используя WPA шифрование. Проверяет возможности беспроводного оборудования подключаться к маршрутизатору не используя шифрование и 802.11b/g, после перехода системы в ждущий режим. Тестирует возможность беспроводного оборудования подключиться к маршрутизатору по протоколу 802.11b/g не используя шифрование. Проверяет возможности беспроводного оборудования подключаться к маршрутизатору не используя шифрование и 802.11n, после перехода системы в ждущий режим. Тестирует возможность беспроводного оборудования подключиться к маршрутизатору по протоколу 802.11n не используя шифрование. Проверяет производительность системного беспроводного соединения с помощью средства iperf, используя пакеты UDP. Тестирует производительность беспроводного соединения посредством утилиты iperf. Тесты для проверки работоспособности APT при доступе к репозиториям и получении обновлений (без их установки). Они производятся для того, чтобы удостовериться в возможности восстановления после неполного обновления или установки повреждённых пакетов. Проверка работоспособности соединения с сетью Интернет Поле текста Файл журнала. Создан следующий отчёт для отправки в базу данных оборудования Launchpad:

  [[%s|View Report]]

Вы можете отправить эту информацию о своей системе, указав адрес электронной почты, который использовали при регистрации на Launchpad. Если у вас ещё нет учётной записи на Launchpad, пожалуйста, зарегистрируйтесь здесь:

  https://launchpad.net/+login Сформированный отчет, скорее всего, содержит ошибки проверки,
поэтому может быть не обработан Launchpad. Checkbox уже запущен. Сначала завершите его. Эта автоматизированная проверка попытается определить камеру. Прикрепляет снимок экрана теста «suspend/cycle_resolutions_after_suspend_auto» к результатам для отправки. Это полностью автоматизированная версия mediacard/sd-automated, и она предполагает, что к тестируемому компьютеру подключено устройство чтения карт памяти перед запуском checkbox. Предназначена для автоматического тестирования SRU. Это автоматизированная проверка передачи файлов через Bluetooth. Она отправляет изображение на устройство, указанное в переменной окружения BTDEVADDR. Это автоматизированная проверка Bluetooth. Она эмулирует обзор удалённого устройства, заданного переменной окружения BTDEVADDR. Это автоматизированная проверка Bluetooth. Выполняется приём заданного файла от удалённого узла, заданный переменной окружения BTDEVADDR Это автоматизированный тест для сбора информации о текущем состоянии ваших сетевых устройств. Если устройства не будут найдены, тест завершиться с ошибкой. Это автоматизированный тест, который выполняет операции чтения/записи на присоединенённом жёстком диске с интерфейсом FireWire Это автоматизированный тест, выполняющий операции чтения/записи на подключённом eSATA HDD Это автоматизированная версия хранилища usb, она подразумевает, что сервер имеет подключенные устройства хранения данных usb до исполнения. Тест предназначен для автоматического тестирования серверов и SRU. Это автоматизированная версия suspend/suspend_advanced. Эта проверка позволит убедиться в точности топологии ЦПУ Проверка на соблюдение ограничения скорости CPU. Этот тест проверяет работоспособность беспроводного интерфейса, после выхода из ждущего режима. Он отключает все интерфейсы, а затем производит подключение к беспроводному интерфейсу и проверяет работоспособность соединения. Этот тест сверяет объем памяти сообщаемый meminfo с объемом модулей памяти, определенным с помощью DMI. Этот тест выполняет отключение всех соединений, а затем производит подключение к беспроводному интерфейсу. Затем проверяет Этот тест извлекает аппаратный адрес bluetooth-адаптера после выхода из ждущего режима и сравнивает его с адресом извлечённым дом перехода в ждущий режим. Этот тест запускается автоматически после выполнения теста «mediacard/cf-insert». Он предназначен для проверки чтения с карты памяти CF и записи на неё. Этот тест автоматизирован и выполняется после mediacard/cf-insert-after-suspend. Тест проверяет чтение и запись CF карты после того, как система была приостановлена. Эта проверка автоматизирована и выполняется после запуска проверки вставки карты памяти mediacard/mmc. Будет выполнено чтение и запись на карту памяти MMC. Эта проверка автоматизирована и выполняется после запуска проверки вставки карты памяти mediacard/mmc, после выхода из спящего режима. Будет выполнено чтение и запись на карту памяти MMC, после выхода из спящего режима. Это автоматизированный тест, выполняющийся после теста mediacard/ms-insert. Он проверяет чтение и запись на карту памяти MS. Это автоматизированный тест, выполняющийся после теста mediacard/ms-insert-after-suspend. Он проверяет чтение и запись на карту памяти MS после выхода системы из ждущего режима. Это автоматизированный тест, выполняющийся после теста mediacard/msp-insert. Он проверяет чтение и запись на карту памяти MSP. Это автоматизированный тест, выполняющийся после теста mediacard/msp-insert-after-suspend. Он проверяет чтение и запись на карту памяти MSP после выхода системы из ждущего режима. Эта проверка автоматизирована и выполняется после вставки карты памяти. Будет выполнена проверка чтения и записи на карту памяти SD. Эта проверка автоматизирована и выполняется после запуска проверки карты памяти/вставки sd после спящего режима. Выполняется проверка чтения и записи на карте памяти SD, после выхода системы из спящего режима. Этот тест запускается автоматически после выполнения теста «mediacard/sdhc-insert». Он предназначен для проверки чтения с карты SDHC и записи на неё. Этот тест автоматизирован и выполняется после тестов mediacard/sdhc-insert-after-suspend. Тест проверяет чтение и запись карты SDHC после того, как система была приостановлена. Это автоматизированный тест, выполняющийся после теста mediacard/sdxc-insert. Он проверяет чтение и запись на карту памяти SDXC. Это автоматизированный тест, выполняющийся после теста mediacard/sdxc-insert-after-suspend. Он проверяет чтение и запись на карту памяти SDXC после выхода системы из ждущего режима. Это автоматизированный тест, выполняющийся после теста mediacard/xd-insert. Он проверяет чтение и запись на карту памяти xD. Это тест автоматизирован и выполняется после теста mediacard/xd-insert-after-suspend. Он проверяет запись и чтение с карты памяти xD после выхода системы из ждущего режима. Этот тест запускается автоматически после выполнения теста «usb/insert». Этот тест автоматизирован и запускается после выполнения теста usb3/insert. Этот тест проверяет работоспособность видео режимов после перехода в ждущий режим и возобновления рабочего режима. Он выполняется автоматически, сохранением снимков экрана и передачей их во вложении. Эта проверка позволит убедиться, что уровни громкости вашей системы находятся на допустимом уровне.  Тест произведёт проверку громкости, которая превышает или сопоставима с минимальной громкостью или меньше, или сопоставима с максимальной громкостью для всех источников (входов) и приёмников (выходов), распознанных PulseAudio.  А также выполнит проверку того, что звук активного источника или приёмника не отключён.  Вы должны самостоятельно настроить уровень громкости или выключить звук, перед выполнением этой проверки. Это позволит включить все отчёты из теста power-management/poweroff в итоговые результаты. Это позволит включить все отчёты из теста power-management/reboot в итоговые результаты. Будет выполнения проверка работоспособности звукового устройства, после выхода из ждущего режима в рабочий режим.  Это подходит для динамиков и встроенного микрофона, но наиболее подходящий вариант при использовании кабельного соединения разъёма звукового выхода и разъёма звукового входа. Будут выполнены некоторые базовые тесты подключения к BMC, для проверки работоспособности IPMI. Получен сигнал Timer от функции alarm(2) Чтобы исправить неполадку, закройте проверку системы и добавьте отсутствующие зависимости в белый список. Проверка сенсорной панели Тесты сенсорного экрана Попытаться включить удалённый принтер в сети и напечатать пробную страницу. Напечатать текст НЕИЗВЕСТНО Проверка интерфейса USB Проверка USB Неизвестный сигнал Проверка не выполнялась Использование: checkbox [OPTIONS] Пользовательские приложения Пользовательский сигнал 1 Пользовательский сигнал 2 Проверка работы математического сопроцессора для ARM устройств Проверяет, запущен и работоспособен ли сервер DNS. Проверяет, запущен ли сервер Печати/CUPs. Проверяет, запущен ли сервер Samba. Проверяет, работает и работоспособен ли сервер Tomcat. Проверяет доступность службы sshd. Проверка работоспособности стека LAMP (Apache, MySQL, и PHP). Проверяет, что внешнее устройство USB3 обеспечивает или превышает требуемую производительность. Проверка системных хранилищ выполняется по стандартным и повышенным условиям Проверяет доступность всех ЦПУ после возобновления рабочего режима Проверять доступность всей памяти после выхода из режима ожидания. Проверяет доступность все ЦПУ перед переходом в ждущий режим Проверка возможности установки checkbox-сервера в сети с использованием SSH. Проверить, являются ли пареметры микшера после ждущего режима такими же, как и до него. Проверить возможность обнаружения устройств хранения, таких как волоконно-оптический канал и RAID, затем выполнить стресс-тест. Просмотр результатов Тесты виртуализации Добро пожаловать в программу проверки системы!

Эта программа предназначена для тестирования работоспособности системы. После окончания тестирования вы сможете просмотреть сводный отчёт по системе. Проверка беспроводных возможностей Проверка беспроводных сетей Проверка сканирование беспроводных устройств. Выполняется проверка и создаются отчёты об обнаруженных точках доступа. Выполняется работа Тест можно также прервать нажатием ESC или Ctrl+C. _Снять выделение _Выход _Завершить _Нет Н_азад _Выбрать всё _Пропустить этот тест _Тест Тестировать снова Д_а , а также ёмкость. Добавляет содержимое различных конфигурационных файлов sysctl. Тесты дисков с интерфейсом eSATA https://help.ubuntu.com/community/Installation/SystemRequirements устанавливает инсталлятор bootchart tarball если он существует. нет пропустить тест повтор теста устройство ввода для фонового процесса устройство вывода фонового процесса да 