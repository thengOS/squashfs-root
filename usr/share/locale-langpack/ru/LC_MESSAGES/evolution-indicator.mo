��          �      L      �     �     �     �     �     �       3        M     [     v  7   �  ,   �     �          '  #   ?  
   c  	   n    x  b   �     �     �       #   %     I  q   Z     �  P   �  C   :  �   ~  P     7   R  3   �  ,   �  A   �     -	     G	                                                 	                                     
              %d New Message %d New Messages : Compose New Message Contacts Evolution Indicator Inbox Only create notifications for new mail in an Inbox. Pla_y a sound Play a sound for new mail. Show a notification bubble. Show new message count in the message indicator applet. Shows new mail count in a message indicator. When New Mail Arrives When new mail arri_ves in _Display a notification _Indicate new messages in the panel any Folder any Inbox Project-Id-Version: evolution-indicator
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-24 15:14+0000
PO-Revision-Date: 2013-04-11 09:03+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 18:39+0000
X-Generator: Launchpad (build 18115)
 %d новое сообщение %d новых сообщения %d новых сообщений : Написать письмо Контакты Индикатор для Evolution Входящие Только создавать уведомления о новой почте в папке "Входящие". _Проиграть звук Проигрывать звук при получении новой почты. Показывать всплывающие уведомления. Показывать количество новых сообщений в апплете индикатора сообщений. Показывает число новых сообщений на панели. При получении новых сообщений При получении нового письма _Отобразить уведомление _Показать новые сообщения на панели любой каталог любой полученный 