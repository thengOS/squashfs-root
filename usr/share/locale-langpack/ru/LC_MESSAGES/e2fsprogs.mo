��    �      4  3  L        A     &   [  b   �     �     �  #   �  "   !  ,   D  3   q     �  &   �     �     �       .   )     X     m     �     �     �     �     �     �     �     �       s   (  +   �      �  !   �  -     '   9  )   a     �     �     �  '   �  N   �  )   F  )   p     �     �  	   �     �     �     �     �  5   �  $   #     H     P     X     `     r      �     �     �     �     �     �     �          )  $   2  )   W     �     �     �     �     �  O   �     &  &   C     j  $        �  +   �  .   �            #   =     a  (   z     �  %   �     �  (        1  +   Q     }     �  %   �     �  1   �               !     4     I     ^     b     p     �     �     �     �     �  
   �     �  "        %     8  9   R     �     �  %   �     �     �      �                    "         >   	   J      T      ]      f      t      }   
   �      �      �      �      �   6   �   6   �   	   1!  D   ;!  :   �!  0   �!  g   �!     T"     t"     }"     �"  /   �"     �"     �"     �"  %   #     (#     G#  '   U#  $   }#  >   �#     �#     �#     $     $$     2$     D$     K$  	   X$     b$     s$  
   �$  	   �$     �$     �$     �$     �$      �$     �$     %     %  )   %     G%     d%     k%     x%     %     �%     �%  6   �%     �%  %   &     (&     @&     I&  
   U&     `&     i&     |&  #   �&     �&     �&     �&     �&     '     '  
   '     %'     3'     S'  /   ['     �'     �'     �'     �'     �'     (  -    (     N(     j(  "   �(  *   �(     �(     �(     )     )     4)     O)     i)     �)     �)    �)  z   �+  _   H,  �   �,     R-     q-  C   �-  @   �-  B   .  P   I.  1   �.  8   �.  0   /  3   6/  1   j/  v   �/     0  %   10     W0  -   `0  ,   �0     �0     �0     �0     �0      1     ,1  �   @1  0   �1  ,    2  6   M2  P   �2  ;   �2  ?   3  9   Q3     �3  &   �3  6   �3  �   	4  J   �4  T   �4     -5     L5     ]5     n5     5  "   �5     �5  u   �5  J   G6     �6     �6     �6  9   �6  1   �6  )   (7  9   R7     �7     �7  !   �7     �7  1   �7  '   8     D8  <   Y8  O   �8     �8  )   �8  >   9     ^9     n9  �   �9  F   6:  2   }:     �:  @   �:     ;  <   ;  Q   V;  ,   �;  2   �;  :   <  ,   C<  <   p<  0   �<  B   �<  *   !=  <   L=  0   �=  M   �=     >  -   >  :   I>  =   �>  y   �>     <?     R?     g?  .   ~?  &   �?     �?  1   �?     @     9@     L@     l@  <   |@  #   �@     �@     �@  8   A  '   >A  8   fA  P   �A  +   �A     B  J   +B     vB     �B  ;   �B     �B     �B     �B  3   
C     >C     SC     hC     �C  '   �C     �C     �C     �C     �C  1   D     AD  (   TD  {   }D  {   �D     uE  W   �E  Q   �E  P   0F  �   �F  A   G     SG     dG  *   �G  i   �G  9   H  )   OH  (   yH  F   �H  <   �H  &   &I  I   MI  H   �I  �   �I  /   aJ  &   �J  -   �J  !   �J  &   K  	   /K  #   9K     ]K  )   kK  #   �K     �K     �K  '   �K      L     L  /   L  3   ML  ,   �L  ,   �L     �L  K   �L  H   -M     vM     �M     �M  -   �M  M   �M     N  c   -N  -   �N  8   �N  *   �N     #O  '   1O  !   YO     {O  1   �O  (   �O  7   �O  1   P     PP  0   hP     �P     �P  
   �P     �P  '   �P  >   Q     LQ  :   bQ  ,   �Q  >   �Q  5   	R     ?R  ;   ZR  -   �R  H   �R  *   S  .   8S  1   gS  N   �S  8   �S  +   !T  %   MT  '   sT  -   �T  :   �T  9   U  ,   >U  *   kU     |       Q   �   �   �       "       �   �   L   i      �       0       �   �              J   �       <   �       -   �   +   �   t   }           �   	   �       z                 �                 �   �   .          �              3       r   �   '   �   �   �   �   a   :   e       �   �   9          8   l       �   \      �       *   �       4   >   �   5       !   �   )   �   D   �   P   �   �   �           �   �   @                  T      H      �       A   �       �   `   _   ?   �   I       �   �   1   v   w   y   Z   �   �   j   �       �   �   �      c   �   V   �   {   �             o   u   �   �   W       �   �   �           �       �                           �   2   �      [   $       F   ]   �      x   U   �   �   h           O   &   B       �       q           �       �               %   �       �   �           (       �                      �          �   Y      �      #   ;   ~   �   ^   �   �   �   6      =   X   �   K   �   �   m          n      �       /   k   �   �   S           
   �   R   �   �   �           �   G   7       d          �   �   �          �   b   p   g       �   �       �       �   �   ,   E          C   f      �   �   M   N       s        
%s: ********** WARNING: Filesystem still has errors **********

 
Journal size too big for filesystem.
 
The requested journal size is %d blocks; it must be
between 1024 and 10240000 blocks.  Aborting.
   Free inodes:   Done.
  contains a file system with errors %s: %s filename nblocks blocksize
 %s: Error %d while executing fsck.%s for %s
 %s: The -n and -w options are mutually exclusive.

 %s: journal too short
 %s: no valid journal superblock found
 %s: recovering journal
 %s: too many arguments
 %s: too many devices
 %s: won't do journal recovery while read-only
 %u inodes per group
 %u inodes scanned.
 (NONE) <Reserved inode 10> <Reserved inode 9> <The NULL inode> <The boot loader inode> <The journal inode> @A @a @b %b.   @I @i %i in @o @i list.
 @I @o @i %i in @S.
 @S @b_size = %b, fragsize = %c.
This version of e2fsck does not support fragment sizes different
from the @b size.
 @S @bs_per_group = %b, should have been %c
 @a @b %b is corrupt (@n name).   @a @b %b is corrupt (@n value).   @a @b %b is corrupt (allocation collision).   @b @B for @g %g is not in @g.  (@b %b)
 @f did not have a UUID; generating one.

 @i %i has a bad @a @b %b.   @i %i is a @z @d.   @i %i is too big.   @i @B for @g %g is not in @g.  (@b %b)
 @i table for @g %g is not in @g.  (@b %b)
WARNING: SEVERE DATA LOSS POSSIBLE.
 @j @i is not in use, but contains data.   @j version not supported by this e2fsck.
 @r is not a @d.   ABORTED ALLOCATED Abort Aborting....
 Aerror allocating Allocate BLKFLSBUF ioctl not supported!  Can't flush buffers.
 Bad block %u out of range; ignored.
 Bbitmap CLEARED CREATED Can not continue. Can't find external @j
 Cconflicts with some other fs @b Checking all file systems.
 Clear Clear @j Clear HTree index Clear inode Clone multiply-claimed blocks Connect to /lost+found Continue Corruption found in @S.  (%s = %N).
 Couldn't allocate block buffer (size=%d)
 Create Creating journal inode:  Creating journal on device %s:  Ddeleted Delete file Disk write-protected; use the -n option to do a read-only
check of the device.
 Duplicate or bad @b in use!
 E2FSCK_JBD_DEBUG "%s" not an integer

 E@e '%Dn' in %p (%i) ERROR: Couldn't open /dev/null (%s)
 EXPANDED Empty directory block %u (#%d) in inode %u
 Error determining size of the physical @v: %m
 Error moving @j: %m

 Error reading @a @b %b (%m).   Error reading @a @b %b for @i %i.   Error reading @i %i: %m
 Error reading block %lu (%s) while %s.   Error reading block %lu (%s).   Error while scanning inodes (%i): %m
 Error writing @a @b %b (%m).   Error writing block %lu (%s) while %s.   Error writing block %lu (%s).   Error: ext2fs library version out of date!
 Expand Extending the inode table External @j does not support this @f
 External @j has bad @S
 External @j has multiple @f users (unsupported).
 FILE DELETED FIXED Ffor @i %i (%Q) is Filesystem label=%s
 First data block=%u
 Fix Force rewrite HTREE INDEX CLEARED INODE CLEARED Ignore error Iillegal Illegal number of blocks!
 Invalid UUID format
 Lis a link MULTIPLY-CLAIMED BLOCKS CLONED Moving @j from /%s to hidden @i.

 Moving inode table Must use '-v', =, - or +
 Only one of the options -p/-a, -n or -y may be specified. Optimizing directories:  Pass 1 Pass 1: Checking @is, @bs, and sizes
 Pass 2 Pass 3 Pass 3A: Optimizing directories
 Pass 4 Pass 5 Peak memory Please run 'e2fsck -f %s' first.

 RECONNECTED RELOCATED Recreate Relocate Run @j anyway SALVAGED SPLIT SUPPRESSED Salvage Scanning inode table Split Suppress messages Syntax error in e2fsck config file (%s, line #%d)
	%s
 Syntax error in mke2fs config file (%s, line #%d)
	%s
 TRUNCATED The -c and the -l/-L options may not be both used at the same time.
 The -t option is not supported on this version of e2fsck.
 The Hurd does not support the filetype feature.
 The resize_inode and meta_bg features are not compatible.
They can not be both enabled simultaneously.
 Too many illegal @bs in @i %i.
 Truncate UNLINKED Unlink Usage: %s [-F] [-I inode_buffer_blocks] device
 Usage: %s [-RVadlv] [files...]
 Usage: %s [-r] [-t]
 Usage: %s disk
 WARNING: bad format on line %d of %s
 WARNING: couldn't open %s: %s
 WILL RECREATE Warning: could not erase sector %d: %s
 Warning: could not read block 0: %s
 Warning: illegal block %u found in bad block inode.  Cleared.
 Writing inode tables:  aextended attribute bad gid/group name - %s bad inode map bad interval - %s bblock block device ccompress character device check aborted.
 ddirectory directory directory inode map done
 done

 e2label: cannot open %s
 e2label: not an ext2 filesystem
 empty dir map empty dirblocks ffilesystem fsck: cannot check %s: fsck.%s not found
 getting next inode from scan ggroup hHTREE @d @i iinode in-use inode map inode in bad block map inode table internal error: couldn't lookup EA inode record for %u invalid block size - %s invalid inode size %d (min %d/max %d) invalid inode size - %s jjournal llost+found named pipe ninvalid opening inode scan reading directory block reading indirect blocks of inode %u reading journal superblock
 regular file regular file inode map rroot @i size of inode=%d
 socket sshould be symbolic link unknown file type with mode 0%o vdevice while calling ext2fs_block_iterate for inode %d while clearing journal inode while doing inode scan while getting next inode while opening %s while opening %s for flushing while opening inode scan while reading in list of bad blocks from file while reading journal inode while reading root inode while reading the bad blocks inode while sanity checking the bad blocks inode while starting inode scan while trying popen '%s' while trying to flush %s while trying to open %s while trying to re-open %s while trying to resize %s while updating bad block inode while writing inode table while writing journal inode Project-Id-Version: e2fsprogs
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-05-17 20:31-0400
PO-Revision-Date: 2015-01-28 12:16+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 16:07+0000
X-Generator: Launchpad (build 18115)
 
%s: ********** ВНИМАНИЕ: Файловая система всё ещё содержит ошибки **********

 
Размер журнала слишком велик для файловой системы.
 
Запрошенный размер журнала %d блоков. Он должен
быть от 1024 до 10240000 блоков. Действие прервано.
   Свободные inod'ы:   Готово.
  содержит файловую систем с ошибками %s: %s имяфайла нблоков размерблоков
 %s: Ошибка %d при выполнении fsck.%s для %s
 %s: опции -n и -w взаимно исключают друг друга.

 %s: журнал слишком короткий
 %s: не найден суперблок журнала
 %s: восстановление журнала
 %s: слишком много аргументов
 %s: слишком много устройств
 %s: нельзя восстановить по журналу из-за режима только для чтения
 %u inod'ов в группе
 просмотрено %u inod'ов.
 (НЕТ) <Зарезервированная inode 10> <Зарезервированная inode 9> <Пустой inode> <Загрузчик> <inode журнала> @A @a @b %b.   @I @i %i в списке @o @i.
 @I @o @i %i в @S.
 @S @b_size = %b, fragsize = %c.
Эта версия e2fsck не поддерживает размеры фрагментов отличающиеся от
размера @b.
 @S @bs_per_group = %b, должно быть %c
 @a @b %b неисправен (@n имя).   @a @b %b неисправен (@n значение).   @a @b %b неисправен (неполадка распределения).   @b @B для @g %g отстутствует в @g. (@b %b)
 У @f отсутствовал UUID; создан новый.

 @i %i содержит неисправный @a @b %b.   @i %i является @z @d.   @i %i слишком большой.   @i @B для @g %g отсутствует @g. (@b %b)
 Таблица @i для @g %g отсутствует в @g. (@b %b)
ВНИМАНИЕ: ВОЗМОЖНА ПОТЕРЯ ДАННЫХ.
 @j @i не используется, но содержит данные.   Версия @j не поддерживается этим выпуском e2fsck.
 @r не является @d.   ПРЕРВАНО ВЫДЕЛЕНО Прервать Прерывание....
 Aошибка размещения Разместить Функция ioctl BLKFLSBUF не поддерживается! Не удалось записать буферы.
 Битый блок %u вне досягаемости, пропущен.
 Bкарта ОЧИЩЕНО СОЗДАНО Продолжение работы невозможно. Не удалось найти внешний @j
 Cконфликт с другой ФС @b Проверка всех файловых систем.
 Очистить Очистить @j Создать индекс HTree Очистить inode Создать копию общих блоков Присоединить к /lost+found Продолжить Обнаружено повреждение: @S. (%s = %N).
 Не могу распределить буфер блока (размер=%d)
 Создать Создание inod'а журнала:  Создание журнала на устройстве %s:  Dудалено Удалить файл Диск защищён от записи; используйте опцию -n для
проверки устройства в режиме только для чтения.
 Используется копия или неисправный @b!
 E2FSCK_JBD_DEBUG "%s" не целое число

 E@e '%Dn' в %p (%i) ОШИБКА: не удалось открыть /dev/null (%s)
 РАСШИРЕНО Пустой блок каталога %u (#%d) в inode %u
 Ошибка определения размера физического @v: %m
 Ошибка перемещения @j: %m

 Ошибка считывания @a @b %b (%m).   Ошибка считывания @a @b %b для @i %i.   Ошибка считывания @i %i: %m
 Ошибка чтения блока %lu (%s) пока %s.   Ошибка чтения блока %lu (%s).   Ошибка при сканированнии inod'ов (%i): %m
 Ошибка записи @a @b %b (%m).   Ошибка записи блока %lu (%s) пока %s.   Ошибка записи блока %lu (%s).   Ошибка: устаревшая версия библиотеки ext2fs!
 Расширить Увеличение таблицы inod'ов Внешний @j не поддерживает эту @f
 Внешний @j содержит неисправный @S
 Внешний @j содержит несколько пользователей @f (не поддерживается).
 ФАЙЛ УДАЛЕН ИСПРАВЛЕНО Fдля @i %i (%Q) — Метка файловой системы=%s
 Первый блок данных=%u
 Исправить Принудительная перезапись ОЧИЩЕН ИНДЕКС HTREE INODE ОЧИЩЕН Игнорирую ошибку Iневерно Неправильное количество блоков!
 Неверный формат UUID
 Lссылка СОЗДАНА КОПИЯ Перемещение @j с /%s в скрытый @i.

 Перенос таблицы inod'ов Нужно использовать '-v', =, - или +
 Можно указать лишь одну из опций -p/-a, -n или -y. Оптимизация каталогов:  Проход 1 Проход 1: Проверка @is, @bs, а также размеров
 Проход 2 Проход 3 Проход 3A: оптимизация каталогов
 Проход 4 Проход 5 Пиковая память Запустите сначала 'e2fsck -f %s'.

 ПОДКЛЮЧЕНО ПЕРЕМЕЩЕНО Создать заново Переместить Запустить @j всё равно СОХРАНЕНО РАЗДЕЛЕНО ПОДАВЛЕНО Сохранить Сканирование таблицы inod'ов Разделить Не выводить сообщения Синтаксическая ошибка в конфигурационном файле e2fsck (%s, строка #%d)
	%s
 Синтаксическая ошибка в конфигурационном файле mke2fs (%s, строка #%d)
	%s
 ОБРЕЗАНО Опции -c и -l/-L нельзя использовать одновременно.
 Опция -t не поддерживается в этой версии e2fsck.
 ОС Hurd не поддерживает функцию типов файлов.
 Функции resize_inode и meta_bg не совеместимы.
Их совместное использование невозможно.
 Слишком много недопустимых @bs в @i %i.
 Обрезать УДАЛЕНА ССЫЛКА Удалить жесткую ссылку Использование: %s [-F] [-I блоков-для-буфера-inod'ов] устройство
 Использование: %s [-RVadlv] [файлы...]
 Использование: %s [-r] [-t]
 Использование: %s диск
 ВНИМАНИЕ: неверный формат в строке %d %s
 ВНИМАНИЕ: не удалось открыть %s: %s
 БУДЕТ СОЗДАНО ЗАНОВО Внимание: не удалось стереть сектор %d: %s
 Внимание: не удалось прочитать блок 0: %s
 Предупреждение: в inode битых блоков найден недопустимый блок %u. Очищен.
 Сохранение таблицы inod'ов:  aрасширенный атрибут неверный gid/имя группы - %s карта плохих inod'ов неверный интервал - %s bблок блочное устройство cсжатие символьное устройство проверка прервана.
 dкаталог каталог карта inod'ов каталогов готово
 готово

 e2label: не удалось открыть %s
 e2label: не файловая система ext2
 пустая карта директории пустые блоки директории fФС fsck: не удалось проверить %s: fsck.%s не найден
 получение следующего сканируемого inod'а gгруппа hHTREE @d @i iinode карта используемых inod'ов карта inod'ов, расположенных в плохих блоках таблица inod'ов внутренняя ошибка: не могу просмотреть блок inode EA для %u неверный размер блока - %s неверный размер inod'а %d (min %d/max %d) неверный размер inod'а - %s jжурнал потерянные+найденные именованный канал nневерно вначале сканирования inod'ов чтение блока каталога чтение косвенных блоков inod'а %u чтение суперблока журнала
 обычный файл карта inod'ов обычных файлов rкорень @i размер inod'а=%d
 сокет sдолжно быть символическая ссылка неизвестный тип файла с режимом 0%o vустройство при вызове ext2fs_block_iterate для inod'а %d при очистке inod'а журнала при выполнении сканирования inod'ов при получении следующего inod'a при открытии %s при открытии %s для записи буфера вначале сканирования inode при чтении списка битых блоков из файла при чтении inod'а журнала при чтении корневого inod'а при чтении inode битых блоков при проверке корректности inode битых блоков при запуске сканирования inod'ов при попытке открытия '%s' при сбросе буферов %s при попытке открыть %s при повторном открытии %s при попытке изменения размера %s при обновлении inode битых блоков при записи таблицы inod'ов при записи inod'а журнала 