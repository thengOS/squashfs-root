��    x      �  �   �      (
     )
     I
     O
     U
     k
     q
     z
     |
     
     �
     �
     �
     �
     �
  *   �
     �
               !     <     M     _     n     �     �     �  	   �     �     �     �     �  $   �  )        :     >     K     \     s     x     �     �  �   �     Q     _     l  �   x     W     ^     {     �     �     �     �     �     �     �  	   �                          $     '     7      <  &   ]     �     �     �     �     �     �     �     �     �            '   &     N     U     [     a     x  >   �     �     �     �  )   �       �   "     �     �     
          5     A     a     f     m  
   s     ~     �     �     �     �     �     �          /     J     d     ~     �     �     �     �               1  ;  D  $   �     �     �     �     �     �     �     �     �                    ,     G  ;   a     �     �     �     �     �               &     8     L     \     w          �     �  	   �  $   �  )   �                     /     N     W     i  
   �  �   �     C     Y     n  �        s     z     �     �     �     �     �     �     �          '     9     Q     V     \     h     p     �  %   �  3   �  *   �  (        6     J     Y     \     i     |     �     �     �  9   �     
               %     D  M   Z     �     �     �  =   �       �        �           .   !   F      h   +   ~      �   
   �      �      �      �      �      �      �   	   !     !     !     !!  
   -!     8!     K!     X!  
   i!  
   t!     !     �!     �!     �!  �  �!                   C   .       p   a   #   i   E      "   2   9   8                 &   +       *   n                       t      N   P      V   K   
          ]   G   m                      A   o   5   \   U              -   l   w   s              7      x       ?           g   Z           /   1   >   (      r         M       W              '   _   ^      e       [           B   Y      @           %       k      X   :   L   Q   F   b   )   c   J   v       O       H   $       <       f   0   ;       6   `   !   S               	   ,   j   R           =   q      3      T      D       d                 4   u   I   h    %s (this calendar is read-only) %s AM %s PM - Calendar management 00:00 00:00 PM : AM Access, and manage calendar Account Add Add Calendar Add Eve_nt… Add Event… Add new events to this calendar by default All day Another event deleted Calendar Calendar <b>%s</b> removed Calendar Address Calendar Settings Calendar files Calendar for GNOME Calendar management Calendar name Calendar;Event;Reminder; Calendars Cancel Click to set up Color Connect Copyright © %d The Calendar authors Copyright © %d–%d The Calendar authors Day Delete event Display calendar Display version number Done Edit Calendar Edit Details… Ends Enter the address of the calendar that you want to add. If the calendar belongs to one of your online accounts, you can add it through the <a href="GOA">online account settings</a>. Event deleted From File… From Web… GNOME Calendar is a simple and beautiful calendar application designed to perfectly fit the GNOME desktop. By reusing the components which the GNOME desktop is build on, Calendar nicely integrates with the GNOME ecosystem. Google List of the disabled sources Location Manage your calendars Microsoft Exchange Midnight Month New Event from %s to %s New Event on %s New Local Calendar… No events No results found Noon Notes Off On Online Accounts Open Open calendar on the passed date Open calendar showing the passed event Open online account settings Other event Other %d events Other events Overview PM Password Remove Calendar Save Search for events Select a calendar file Settings Sources disabled last time Calendar ran Starts Title Today Try a different search Type of the active view Type of the active window view, default value is: monthly view Undo Unnamed Calendar Unnamed event Use the entry above to search for events. User We aim to find the perfect balance between nicely crafted features and user-centred usability. No excess, no lacks. You'll feel comfortable using Calendar, like you've been using it for ages! Window maximized Window maximized state Window position Window position (x and y). Window size Window size (width and height). Year _About _Quit _Search… _Synchronize event date format%B %d ownCloud shortcut windowClose window shortcut windowGeneral shortcut windowGo back shortcut windowGo forward shortcut windowMonth view shortcut windowNavigation shortcut windowNew event shortcut windowNext view shortcut windowPrevious view shortcut windowSearch shortcut windowShortcuts shortcut windowShow help shortcut windowShow today shortcut windowView shortcut windowYear view translator-credits Project-Id-Version: gnome-calendar master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-calendar&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-09-14 16:36+0000
PO-Revision-Date: 2016-09-21 15:18+0000
Last-Translator: Guillaume Bernard <Unknown>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-10-09 11:58+0000
X-Generator: Launchpad (build 18227)
Language: fr
 %s (cet agenda est en lecture seule) %s du matin %s de l'après-midi - Gestion de l'agenda 00:00 00:00 : AM Gérer et accéder à l'agenda Compte Ajouter Ajouter un agenda Ajouter un événe_ment… Ajouter un événement… Ajouter les nouveaux événements à cet agenda par défaut Toute la journée Autre événement supprimé Agenda Agenda <b>%s</b> supprimé Adresse de l'agenda Paramètres d'Agenda Fichiers agenda Agenda pour GNOME Gestion de l'agenda Nom de l'agenda Agenda;Événement;Rappel; Agendas Annuler Cliquer pour configurer Couleur Connexion Copyright © %d Les auteurs d'Agenda Copyright © %d–%d Les auteurs d'Agenda Jour Supprimer l'événement Afficher l'agenda Afficher le numéro de version Terminé Modifier l'agenda Modifier les informations… Se termine Saisissez l'adresse de l'agenda que vous souhaitez ajouter. S'il appartient à un de vos comptes en ligne, vous pouvez l'ajouter via les <a href="GOA">paramètres du compte</a>. Événement supprimé Depuis un fichier… Depuis le Web… GNOME Agenda est un agenda simple et de belle facture, conçu pour s'intégrer parfaitement au bureau GNOME. En réutilisant les composants sur lesquels le bureau GNOME est construit, Agenda s'intègre à la perfection à l'écosystème GNOME. Google Liste des sources désactivées Emplacement Gérez vos agendas Microsoft Exchange Minuit Mois Nouvel événement du %s au %s Nouvel événement le %s Nouvel agenda local… Aucun événement Aucun résultat trouvé Midi Notes Désactivé Activé Comptes en ligne Ouvrir Ouvrir l'agenda sur la date transmise Ouvrir l'agenda en affichant l'événement transmis Ouvrir les paramètres de comptes en ligne Autre événement %d autres événements Autres événements Vue d'ensemble PM Mot de passe Supprimer l'agenda Enregistrer Rechercher les événements Sélectionner un fichier agenda Paramètres Sources désactivées à la dernière exécution d'Agenda Démarre Titre Aujourd'hui Essayer une nouvelle recherche Type de la vue active Type de la fenêtre de vue active, la valeur par défaut est : vue mensuelle Annuler Agenda anonyme Événement anonyme Utilisez la barre ci-dessus pour rechercher des événements. Utilisateur Nous recherchons l'équilibre parfait entre fonctionnalités bien conçues et facilité d'utilisation. Aucun excès, aucun manque. Vous serez à l'aise en utilisant Agenda, comme si vous l'utilisiez depuis des siècles ! Fenêtre maximisée État maximisé de la fenêtre Position de la fenêtre Position de la fenêtre (x et y). Taille de la fenêtre Taille de la fenêtre (largeur et hauteur). Année À _propos _Quitter _Recherche… _Synchroniser %B %d ownCloud Fermer la fenêtre Général Précédent Suivant Vue du mois Navigation Nouvel événement Vue suivante Vue précédente Rechercher Raccourcis Afficher l'aide Afficher aujourd'hui Vue Vue de l'année Ahmed Kachakch <ahmed.kachkach@gmail.com>, 2013
Guillaume Bernard <translate@filorin.fr>, 2015
Alain Lojewski <allomervan@gmail.com>, 2015

Launchpad Contributions:
  Alain Lojewski https://launchpad.net/~allomervan
  Guillaume Bernard https://launchpad.net/~9-contact-guib
  Jean-Marc https://launchpad.net/~m-balthazar
  Olivier Febwin https://launchpad.net/~febcrash
  Stéphane V https://launchpad.net/~svergeylen 