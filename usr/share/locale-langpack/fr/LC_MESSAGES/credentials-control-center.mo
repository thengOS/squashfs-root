��          �   %   �      P      Q     r     �  #   �  =   �  @   �     5     B     O     \     l  '   �     �  #   �  1   �  2        A  F   P  $   �  "   �  [   �  :   ;  X   v  S   �  '   #  �  K  0   �     *     ?  4   W  ;   �  D   �     	     "	     4	     F	  "   W	  1   z	     �	  2   �	  8   �	  ;    
     \
  g   p
  2   �
  )     X   5  ?   �  `   �  V   /  ,   �                                                                                                    	               
           - Edit Web credentials settings Add account… All applications Another instance is already running Are you sure that you wish to remove this Ubuntu Web Account? Control whether this application integrates with Online Accounts Edit Options Grant access Legal notice Online Accounts Online Accounts preferences Online account credentials and settings Options Output version information and exit Please authorize Ubuntu to access your %s account Please authorize Ubuntu to access your %s account: Remove Account Run '%s --help' to see a full list of available command line options.
 Select to configure a new %s account Show accounts that integrate with: The Web Account which manages the integration of %s with your applications will be removed. The following applications integrate with your %s account: There are currently no account providers available which integrate with this application There are currently no applications installed which integrate with your %s account. Your online %s account is not affected. Project-Id-Version: gnome-control-center-signon
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-07-19 13:32+0000
PO-Revision-Date: 2013-03-26 13:08+0000
Last-Translator: gisele perreault <Unknown>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-10-09 11:50+0000
X-Generator: Launchpad (build 18227)
  - Modifier les paramètres d'identification Web Ajouter un compte… Toutes les applications Une autre instance est déjà en cours d'éxécution Voulez-vous vraiment supprimer ce compte en ligne Ubuntu ? Contrôler si cette application s'intègre avec les comptes en ligne Modifier les options Accorder l'accès Mentions légales Comptes en ligne Préférences des comptes en ligne Identifications et paramètres de compte en ligne Options Afficher les informations de la version et quitter Veuillez autoriser Ubuntu à accéder à votre compte %s Veuillez autoriser Ubuntu à accéder à votre compte %s : Supprimer le compte Exécutez « %s --help » pour voir la liste complète des options disponibles en ligne de commande.
 Sélectionner pour configurer un nouveau compte %s Voir les comptes qui s'intègrent avec : Le compte en ligne qui gère l'intégration de %s  avec vos applications sera supprimé. Les applications suivantes s'intègrent avec votre compte %s : Il n'y a actuellement aucun fournisseur de compte disponible s’intégrant à cette application Il n'y a actuellement aucune application installée qui s'intègre à votre compte %s. Votre compte en ligne %s n'est pas affecté. 