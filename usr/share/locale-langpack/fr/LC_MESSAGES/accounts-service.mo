��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  \   y  K   �  K   "  2   n     �     �  #   �     �  l        �               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
POT-Creation-Date: 2013-01-17 09:48-0500
PO-Revision-Date: 2011-07-21 23:46+0000
Last-Translator: Claude Paroz <claude@2xlibre.net>
Language-Team: French <traduc@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:01+0000
X-Generator: Launchpad (build 18115)
Language: fr
 Il est nécessaire de s'authentifier pour modifier la configuration de l'écran de connexion Il est nécessaire de s'authentifier pour modifier des données utilisateur Vous devez vous authentifier pour modifier vos propres données utilisateur Modifier la configuration de l'écran de connexion Modifier ses propres données Active le code de débogage Gérer les comptes des utilisateurs Affiche la version et quitte Fournit des interfaces D-Bus pour interroger et manipuler
des informations sur les comptes des utilisateurs. Remplace une instance existante 