��    L      |  e   �      p  $   q     �     �  	   �     �  
   �     �  #   �  #        :     @     G      L     m  !        �    �     �  <   �     	  
    	     +	     8	     D	     J	  "   S	     v	     �	     �	     �	     �	     �	     �	     �	  ;   
  S   A
  2   �
  [   �
  /   $     T     Z  
   f     q     }  $   �     �  #   �     �     �     �          -  �   K                    "  	   )     3     ;     A  	   H     R     V     ]  
   i     t     z     �     �     �     �     �     �     �  I  �  $   >     c  
   �     �     �     �     �  -   �  0        2     9     H  ,   M     z  -   �     �  A  �        K   2     ~     �     �     �     �     �  1   �  /        B     K     a     u  .   �     �     �  V   �  n   3  J   �  ~   �  :   l  	   �     �     �     �     �  1   �     *  '   =     e     l     �  ,   �      �  �   �  
   �     �     �     �  	   �                         .     7     >     R     d  
   m     x     �  -   �     �  
   �  �  �  "   �     C   D                !   *   <   E          J      	       G   :   L              +      &      ,                    #   )   3   $               "         @   /             2   H       A      1              '      I   F   .   >                    0   K   ?   6            (               7   9                     B       ;   =       4   5       
             %   -   8    %u × %u, %u mine %u × %u, %u mines <b>%d</b> mine <b>%d</b> mines A_ppearance Big board Big game Board size Change _Difficulty Clear explosive mines off the board Clear hidden mines from a minefield Close Custom Date Do you want to start a new game? Enable animations Enable automatic placing of flags GNOME Mines GNOME Mines is a puzzle game where you search for hidden mines. Flag the spaces with mines as quickly as possible to make the board a safer place. You win the game when you've flagged every mine on the board. Be careful not to trigger one, or the game is over! Height of the window in pixels If you start a new game, your current progress will be lost. Keep Current Game Main game: Medium board Medium game Mines New Game Number of columns in a custom game Number of rows in a custom game Paused Percent _mines Play _Again Print release version and exit Resizing and SVG support: Score: Select Theme Set to false to disable theme-defined transition animations Set to true to automatically flag squares as mined when enough squares are revealed Set to true to be able to mark squares as unknown. Set to true to enable warning icons when too many flags are placed next to a numbered tile. Size of the board (0-2 = small-large, 3=custom) Size: Small board Small game St_art Over Start New Game The number of mines in a custom game The theme to use The title of the tile theme to use. Time Use _animations Use the unknown flag Warning about too many flags Width of the window in pixels You can select the size of the field you want to play on at the start of the game. If you get stuck, you can ask for a hint: there's a time penalty, but that's better than hitting a mine! _About _Best Times _Cancel _Close _Contents _Height _Help _Mines _New Game _OK _Pause _Play Again _Play Game _Quit _Resume _Scores _Show Warnings _Use Question Flags _Width minesweeper; translator-credits true if the window is maximized Project-Id-Version: gnome-mines master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-mines&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-11 17:13+0000
PO-Revision-Date: 2016-02-12 02:20+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 19:21+0000
X-Generator: Launchpad (build 18115)
Language: fr
 %u × %u, %u mine %u × %u, %u mines <b>%d</b> mine <b>%d</b> mines A_pparence Grande grille Grande grille Taille du plateau Changer la _difficulté Neutraliser les mines explosives d'un plateau Nettoyer un champ de mines de ses mines cachées Fermer Personnalisée Date Voulez-vous commencer une nouvelle partie ? Activer les animations Activer le placement automatique des drapeaux Démineur GNOME Le démineur GNOME est un casse-tête où vous cherchez des mines cachées. Signalez les cases contenant une mine aussi vite que possible pour faire du plateau un lieu sûr. Vous gagnez lorsque vous avez signalé toutes les mines du plateau. Faites attention à ne pas en déclencher une, au risque de perdre la partie ! Hauteur de la fenêtre en pixels Si vous commencez une nouvelle partie, votre avancée actuelle sera perdue. Continuer la partie en cours Jeu principal : Grille moyenne Grille moyenne Mines Nouvelle partie Nombre de colonnes dans une partie personnalisée Nombre de lignes dans une partie personnalisée Suspendu Pourcentage de _mines Re_faire une partie Affiche la version et quitte Redimensionnement et prise en charge de SVG : Score : Choisir un thème Définir à faux pour désactiver les animations de transition définies par le thème Définir à vrai pour poser automatiquement des drapeaux sur les cases lorsqu'assez de cases sont révélées. Définir à vrai pour être capable de marquer les carrés comme inconnus. Définir à vrai pour activer l'icône d'avertissement lorsque trop de drapeaux sont placés à côté d'une case numérotée. Taille du plateau (0-2 = petit-grand, 3 = personnalisé) Taille : Petite grille Petite grille Re_commencer Commencer une nouvelle partie Le nombre de mines pour une partie personnalisée Thème à utiliser Titre du thème de pièces à utiliser. Durée Activer les _animations Utiliser le drapeau inconnu Avertissement lorsqu'il y a trop de drapeaux Largeur de la fenêtre en pixels Vous pouvez choisir la taille du plateau que vous voulez au début de la partie. Si vous êtes bloqué, vous pouvez demander un indice. Vous recevrez une pénalité de temps, mais cela vaut mieux que de sauter sur une mine ! À _propos _Meilleurs temps A_nnuler _Fermer _Sommaire _Hauteur Aid_e _Mines _Nouvelle partie _Valider _Pause _Refaire une partie _Démarrer le jeu _Quitter _Reprendre _Scores Afficher les a_vertissements _Utiliser des drapeaux points d'interrogation _Largeur démineur; Christophe Merlet <redfox@redfoxcenter.org>
Guy Daniel Clotilde <guy.clotilde@wanadoo.fr>
Sébastien Bacher <seb128@debian.org>
Vincent Carriere <carriere_vincent@yahoo.fr>
Christophe Bliard <christophe.bliard@netcourrier.com>
Xavier Claessens <x_claessens@skynet.be>
Jonathan Ernst <jonathan@ernstfamily.ch>
Claude Paroz <claude@2xlibre.net>
Stéphane Raimbault <stephane.raimbault@gmail.com>
Didier Vidal <didier-devel@melix.net>
Pierre Lemaire <pierre.lemaire@kamick.org>
Bruno Cauet <brunocauet@gmail.com>

Launchpad Contributions:
  Anne https://launchpad.net/~anneonyme017
  Brun Cauet https://launchpad.net/~brunocauet
  Claude Paroz https://launchpad.net/~paroz
  Jean-Marc https://launchpad.net/~m-balthazar
  bruno https://launchpad.net/~annoa.b vrai si la fenêtre est maximisée 