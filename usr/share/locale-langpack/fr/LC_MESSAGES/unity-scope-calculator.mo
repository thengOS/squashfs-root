��          \      �       �   	   �   
   �      �      �   3   �   �   ,  !   �  �       �     �     �  	   �  ;   �  �   6  %                                          Calculate Calculator Open in calculator Result Sorry, there are no results that match your search. This is an Ubuntu search plugin that enables Calculator results to be displayed in the Dash underneath the Info header. If you do not wish to search this content source, you can disable this search plugin. calculator;result;calculate;calc; Project-Id-Version: unity-scope-calculator
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-28 09:22+0000
PO-Revision-Date: 2016-03-26 17:58+0000
Last-Translator: Jean-Marc <Unknown>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:07+0000
X-Generator: Launchpad (build 18115)
 Calculer Calculatrice Ouvrir dans la calculatrice Résultat Désolé, aucun résultat ne correspond à votre recherche. Il s'agit d'un greffon de recherche Ubuntu qui permet d'afficher le résultat de calculs dans le tableau de bord. Si vous ne souhaitez pas utiliser cette source de contenu, vous pouvez désactiver ce greffon. calculatrice;résultat;calculer;calc; 