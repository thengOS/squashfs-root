��          �   %   �      0     1     4     :     @     I     V     f     y     �     �     �     �     �  $   �  D        H     b     n  %   �  F   �  �   �  *   �  i   �    X     l     o     u     {     �     �     �     �     �     �     �     �  ,   �  4   !  L   V      �     �  %   �  0   �  G   )	  �   q	  6   e
  �   �
                                                             
                        	                                          %R %R:%S %a %R %a %R:%S %a %b %e, %R %a %b %e, %R:%S %a %b %e, %l:%M %p %a %b %e, %l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-desktop&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:18+0000
PO-Revision-Date: 2015-12-04 14:54+0000
Last-Translator: Claude Paroz <claude@2xlibre.net>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:58+0000
X-Generator: Launchpad (build 18115)
Language: 
 %R %R:%S %a %R %a %R:%S %a %e %b, %R %a %e %b, %R:%S %a %e %b, %l:%M %p %a %e %b, %l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %l:%M %l:%M:%S le CRTC %d ne peut pas piloter une sortie %s le CRTC %d ne prend pas en charge la rotation = %d CRTC %d : test du mode %dx%d@%dHz avec une sortie à %dx%d@%dHz (passe %d)
 Tests des modes pour le CRTC %d
 Non précisé impossible de cloner sur la sortie %s impossible d'assigner des CRTC aux sorties :
%s aucun des modes choisis n'est compatible avec les modes possibles :
%s La sortie %s ne possède pas les mêmes paramètres que l'autre sortie clone :
mode actuel = %d, nouveau mode = %d
coordonnées actuelles = (%d, %d), nouvelles coordonnées = (%d, %d)
rotation actuelle = %d, nouvelle rotation = %d la sortie %s ne prend pas en charge le mode %dx%d@%dHz la taille virtuelle demandée n'est pas adaptée à la taille disponible : demande=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) 