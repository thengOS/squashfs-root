��          �      �       H  (   I  '   r     �     �     �  K   �  +     %   3  %   Y  %     %   �  b   �     .  �  ;  *     ,   -     Z     c     o  p   �  4   �  4   ,  2   a  +   �  )   �  �   �     ~                          	                     
                  A cover is open on the printer “%s”. A door is open on the printer “%s”. Paused Printers Printing Problem The printer “%s” can’t be used, because required software is missing. The printer “%s” is currently off-line. The printer “%s” is low on paper. The printer “%s” is low on toner. The printer “%s” is out of paper. The printer “%s” is out of toner. You have %d job queued to print on this printer. You have %d jobs queued to print on this printer. _Settings… Project-Id-Version: indicator-printers
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-04 13:07+0000
PO-Revision-Date: 2012-08-26 13:44+0000
Last-Translator: Thibault D <Unknown>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 19:14+0000
X-Generator: Launchpad (build 18115)
 L'imprimante « %s » a un capot ouvert. L'imprimante « %s » a une porte ouverte. En pause Imprimantes Problème d'impression L'imprimante « %s » ne peut pas être utilisée car il manque un logiciel nécessaire à son fonctionnement. L'imprimante « %s » est actuellement hors ligne. L'imprimante « %s » n'a plus beaucoup de papier. L'imprimante « %s » n'a plus beaucoup d'encre. L'imprimante « %s » n'a plus de papier. L'imprimante « %s » n'a plus d'encre. Vous avez %d travail en file d'attente d'impression sur cette imprimante. Vous avez %d travaux en file d'attente d'impression sur cette imprimante. _Paramètres... 