��    	      d      �       �      �      �      �      �   4        H  �   \     D  �  R     �                6  A   U     �  .  �     �        	                                      Manpages Open Search Manpages Search for Manpages Sorry, there are no Manpages that match your search. Technical Documents This is an Ubuntu search plugin that enables information from local manpages to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. manpages;man; Project-Id-Version: unity-scope-manpages
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-24 16:55+0000
PO-Revision-Date: 2014-04-10 10:49+0000
Last-Translator: Olivier Febwin <febwin@free.fr>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Pages de manuel Ouvrir Rechercher dans les pages manuel Rechercher des pages de manuel Désolé, aucune page de manuel ne correspond à votre recherche. Documents Techniques Ceci est un greffon de recherche Ubuntu permettant aux informations des pages de manuel locales d'être recherchées et affichées dans le tableau de bord sous l'en-tête Code. Si vous ne souhaitez pas utiliser la recherche dans cette source de contenu, vous pouvez désactiver ce greffon de recherche. manpages;man;manuel 