��    	      d      �       �      �      �      �      �      	  "     1   @     r  #  {     �     �     �     �     �  :   �  4   $  	   Y                     	                         Audio CD Blu-ray DVD Digital Television Failed to mount %s. No media in drive for device '%s'. Please check that a disc is present in the drive. Video CD Project-Id-Version: totem-pl-parser HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=totem&keywords=I18N+L10N&component=playlist parser
POT-Creation-Date: 2016-01-15 04:36+0000
PO-Revision-Date: 2016-01-18 21:34+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:26+0000
X-Generator: Launchpad (build 18115)
Language: 
 CD audio Blu-ray DVD Télévision numérique Le montage de %s a échoué. Aucun média dans le lecteur du périphérique « %s ». Vérifiez qu'un disque est présent dans le lecteur. Vidéo CD 