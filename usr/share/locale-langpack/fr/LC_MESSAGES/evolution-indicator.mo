��          �      L      �     �     �     �     �     �       3        M     [     v  7   �  ,   �     �          '  #   ?  
   c  	   n  �  x  )   E     o     q     �     �     �  c   �     *  7   ;  #   s  G   �  E   �  $   %  *   J     u  8   �     �  %   �                                                 	                                     
              %d New Message %d New Messages : Compose New Message Contacts Evolution Indicator Inbox Only create notifications for new mail in an Inbox. Pla_y a sound Play a sound for new mail. Show a notification bubble. Show new message count in the message indicator applet. Shows new mail count in a message indicator. When New Mail Arrives When new mail arri_ves in _Display a notification _Indicate new messages in the panel any Folder any Inbox Project-Id-Version: evolution-indicator
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-24 15:13+0000
PO-Revision-Date: 2013-04-11 09:03+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-10-09 11:41+0000
X-Generator: Launchpad (build 18227)
 %d nouveau courriel %d nouveaux courriels : Rédiger un nouveau courriel Contacts Indicateur pour Evolution Boîte de réception Ne créer des notifications que pour les nouveaux courriels arrivant dans une boîte de réception. Éme_ttre un son Émettre un son à la réception d'un nouveau courriel. Afficher une bulle de notification. Afficher le nombre de nouveaux courriels dans l'applet de notification. Affiche le nombre de nouveaux courriels dans un indicateur de message Quand de nouveaux courriels arrivent Quand de nou_veaux courriels arrivent dans _Afficher une notification _Signaler les nouveaux courriels dans le tableau de bord n'importe quel dossier n'importe quelle boîte de réception 