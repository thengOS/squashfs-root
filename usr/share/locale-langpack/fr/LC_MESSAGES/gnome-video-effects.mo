��    E      D  a   l      �     �  @   	  /   J  &   z  )   �  -   �     �     �          &     =     I      P     q  -   v  =   �  +   �       
   &     1  B   6     y     �  )   �     �  
   �     �  !   �      �  	   	     "	     /	  -   7	     e	     k	     r	  
   �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	  
   �	  #   
     ,
     2
     9
  !   A
  
   c
  )   n
  $   �
  (   �
  *   �
  -     $   ?  +   d  4   �  (   �     �     �                    (    .     =  L   [  K   �  0   �  /   %  %   U     {     �     �  3   �     �     �  .   �     ,  >   4  E   s  /   �     �     �     	  9     +   L  	   x  >   �     �  
   �     �  ,   �  !     	   (     2     @  $   H     m     s     z     �     �  	   �     �     �  
   �     �  
   �                 +   .     Z     `  
   g     r     �  <   �  *   �       *   "  $   M  '   r  9   �  6   �  /        ;     G     f     n     v     |        7                     B                 6   1      =   >       E               8       $              ;   %   -       ?   *             D       )   0       '              &      !      <   	   :   +   3                 A      /              C   
   #                          2                (   @   4       5             "       9   ,         .        A triangle Kaleidoscope Add a loopback alpha blending effector with rotating and scaling Add age to video input using scratches and dust Add more saturation to the video input Add some hallucination to the video input Add the ripple mark effect on the video input Bulge Bulges the center of the video Cartoon Cartoonify video input Che Guevara Chrome Detect radioactivity and show it Dice Dices the video input into many small squares Display video input like good old low resolution computer way Dissolves moving objects in the video input Distort the video input Distortion Edge Extracts edges in the video input through using the Sobel operator Fake heat camera toning Flip Flip the image, as if looking at a mirror Heat Historical Hulk Invert and slightly shade to blue Invert colors of the video input Invertion Kaleidoscope Kung-Fu Makes a square out of the center of the video Mauve Mirror Mirrors the video Noir/Blanc Optical Illusion Pinch Pinches the center of the video Quark Radioactive Ripple Saturation Sepia Sepia toning Shagadelic Show what was happening in the past Sobel Square Stretch Stretches the center of the video Time delay Traditional black-white optical animation Transform motions into Kung-Fu style Transform video input into a mauve color Transform video input into a metallic look Transform video input into a waveform monitor Transform video input into grayscale Transform video input into realtime goo'ing Transform video input into typical Che Guevara style Transform yourself into the amazing Hulk Twirl Twirl the center of the video Vertigo Warp Waveform X-Ray Project-Id-Version: gnome-video-effects master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-video-effects&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-10-29 03:54+0000
PO-Revision-Date: 2014-04-10 16:40+0000
Last-Translator: bruno <annoa.b@gmail.com>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:02+0000
X-Generator: Launchpad (build 18115)
 Un kaléidoscope triangulaire Ajoute un effet de fondu en transparence avec rotation et mise à l'échelle Vieillit la vidéo en y ajoutant des éraflures et des grains de poussière Augmente la saturation des couleurs de la vidéo Ajoute des effets psychédéliques à la vidéo Ajoute un effet de pluie à la vidéo Boursouflure Gonfle le centre de la vidéo Bande dessinée Donne un effet de type bande dessinée à la vidéo Che Guevara Chrome Détecte et affiche un effet de radioactivité En dés Fractionne la vidéo en une multitude de petits carrés (dés) Affiche la vidéo comme sur les anciens écrans à faible définition Pixelise les objets en mouvement dans la vidéo Déforme la vidéo Déformation Contours Extrait les contours de la vidéo avec l'algorithme Sobel Simule les couleurs d'une caméra thermique Retourner Retourne l'image comme si elle était regardée dans un miroir Chaleur Historique Hulk Met en négatif légèrement teinté de bleu Inverse les couleurs de la vidéo Inversion Kaléidoscope Kung-fu Met le centre de la vidéo au carré Mauve Miroir Ajoute un miroir à la vidéo Noir et blanc Illusion d'optique Pincement Pince le centre de la vidéo Quark Radioactif Ondulations Saturation Sépia Couleur sépia Psychédélique Affiche ce qui s'est produit dans le passé Sobel Carré Étirement Étire le centre de la vidéo Délai temporel Animation optique classique avec des bandes en noir et blanc Transforme les mouvements en style kung-fu Colore la vidéo en mauve Applique à la vidéo un effet métallique Transforme la vidéo en oscilloscope Transforme la vidéo en niveaux de gris Déforme la vidéo à l'aide d'un contra-zoom temps réel Transforme la vidéo dans le style typique Che Guevara Transformez-vous à l'image de l'étonnant Hulk Enroulement Enroule le centre de la vidéo Vertigo Torsion Ondes Rayon X 