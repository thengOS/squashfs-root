��    I      d  a   �      0     1  4  N     �  R   �  g   �  Z   W	  '   �	     �	     �	     �	     
  	   '
  1   1
  	   c
  	   m
  (   w
  )   �
     �
     �
     �
  #      '   $  "   L     o     �     �     �     �     �     �     �      �          	             !   /  '   Q  %   y  	   �     �  '   �     �  P   �     1     H     X     e  
   t  ;     3   �  Q   �     A     P  &   g     �      �  #   �      �  *   	      4      U  /   v     �  =   �  	         
               #  (   0     Y  �  i  '     �  ;     �  l   �  �   f  h   �  ,   \     �  "   �  -   �     �       2        C     P  6   `  6   �  (   �     �       <     <   Y     �  )   �     �     �               %     3     E      L     m     }  
   �     �  +   �  1   �  5   	  	   ?  	   I  1   S     �  ]   �     �     	          *  
   D  D   O  ;   �  _   �     0  ,   D  ,   q     �  ,   �  0   �  *     6   B  $   y  &   �  <   �       [     
   {     �     �     �     �  3   �     �         )      +   I   E   A       #   D   =      "          ;          C   *           0      /              H            9   4      	      
              >   <   8       $           -                   B   ,          3       ?              :   &          5   2   7                @          6                           G   1      '   %   F   .   !       (     - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Add Startup Program Additional startup _programs: Allow logout Browse… Choose what applications to start when you log in Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Could not display help document Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don't prompt for user confirmation Edit Startup Program Enable debugging code Enabled Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Icon Ignoring any existing inhibitors Log out No description No name Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Select Command Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Applications Startup Applications Preferences The startup command cannot be empty The startup command is not valid This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session.HEAD
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-08-22 10:46+0000
PO-Revision-Date: 2015-07-10 03:29+0000
Last-Translator: bruno <annoa.b@gmail.com>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-10-09 10:49+0000
X-Generator: Launchpad (build 18227)
Language: 
  - le gestionnaire de sessions de GNOME %s [OPTION...] COMMANDE

Exécute COMMANDE tout en inhibant des fonctionnalités de la session.

  -h, --help        Afficher cette aide
  --version         Afficher la version du programme
  --app-id ID       L'identifiant d'application à utiliser
                    lors de l'inhibition (optionnel)
  --reason REASON   La raison de l'inhibition (optionnel)
  --inhibit ARG     Les choses à inhiber, liste séparée par des doubles-points parmi :
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Ne pas lancer COMMANDE et attendre indéfiniment à la place

Si aucune option --inhibit n'est précisée, « idle » est supposé.
 %s nécessite un paramètre
 Un problème est survenu et le système ne peut pas se récupérer.
Déconnectez-vous et essayez à nouveau. Un problème est survenu et le système ne peut pas se récupérer. Toutes les extensions ont été désactivées par mesure de précaution. Un problème est survenu et le système ne peut pas se récupérer. Contactez un administrateur système Une session nommée « %s » existe déjà RÉP_AUTOSTART Ajout d'un programme de démarrage _Programmes supplémentaires au démarrage : Autoriser la déconnexion Parcourir… Choisir les applications à lancer à la connexion _Commande : Co_mmentaire : Impossible de se connecter au gestionnaire de sessions Impossible de créer le connecteur d'écoute ICE : %s Impossible d'afficher le document d'aide Personnalisé Session personnalisée Désactiver la vérification de l'accélération matérielle Ne charger pas les applications demandées par l'utilisateur Ne demande pas de confirmation Modification d'un programme de démarrage Activer le code de débogage Activé Impossible d'exécuter %s
 GNOME GNOME factice GNOME sur Wayland Icône Ignorer tout inhibiteur existant Se déconnecter Pas de description Pas de nom Pas de réponse Oh mince ! Quelque chose s'est mal passé. Passer outre les répertoires autostart standards Veuillez choisir une session personnalisée à lancer S'éteint Programme Programme appelé avec des options conflictuelles Redémarrer Refus de la connexion d'un nouveau client car la session est actuellement en cours d'arrêt.
 Applications mémorisées Reno_mmer la session NOM_SESSION Sélection d'une commande Session %d Les noms de sessions ne peuvent pas contenir le caractère « / » Les noms de sessions ne peuvent pas commencer par « . » Les noms de sessions ne peuvent pas commencer par « . » ou contenir le caractère « / » Session à utiliser Afficher l'avertissement pour les extensions Afficher le dialogue d'erreur pour le tester Applications au démarrage Préférences des applications au démarrage La commande de démarrage ne peut pas être vide La commande de démarrage n'est pas valide Ce choix permet d'accéder à une session enregistrée Ce programme bloque la déconnexion. Cette session vous connecte dans GNOME Cette session vous connecte dans GNOME, en utilisant Wayland Version de cette application Se souvenir _automatiquement des applications en cours d'exécution lors de la déconnexion _Continuer _Fermer la session _Se déconnecter _Nom : _Nouvelle session _Se souvenir des applications en cours d'exécution Supp_rimer la session 