��    M      �  g   �      �     �     �     �  �   �  G   |  :   �  ?   �  >   ?  "   ~  (   �     �  (   �  7   	  6   I	     �	     �	  -   �	  (   �	     
     -
  +   K
  $   w
  %   �
  2   �
  (   �
  "     %   A  .   g  A   �  -   �       0      >   Q  2   �  -   �     �       @        X     m  "   �  B   �  5  �  
     ;   *  :   f     �     �  (   �  4   �     )  B   ;  *   ~     �  �   �     E  &   V     }  �   �  :   G  &   �  $  �  K   �  -        H  %   J     p      �     �     �     �     �     �     �     �     �  �  �       4   �     �  �   �  d   �  C   �  H   <  H   �  )   �  0   �     )  8   H  <   �  G   �        $   '  4   L  /   �  !   �  +   �  1   �      1  2   R  :   �  H   �  #   	  (   -  1   V  S   �  6   �       2   ,  Z   _  ?   �  0   �  /   +      [  Z   |  -   �  -      9   3   f   m   �  �   	   w#  6   �#  C   �#     �#     $  6   +$  K   b$     �$  L   �$  6   %     G%  �   V%     &  /   !&     Q&  �   e&  X   \'  .   �'  T  �'  R   9)  2   �)     �)  4   �)      �)  #   *  "   ;*     ^*     `*     b*     d*     f*     h*     j*        G              D            '      J   A   1       5   !   4       
      M   H          E   :   0           K          ?              (   I          #                                 -          +       &   )          "       *       >   9         C          /         3       7      ,   %   @          <         $   .         =   L   B       2   ;   6   	       8      F       	%s		File: %s

 
-- Type space to continue -- 
 
Commands are:

 
Copyright (C) 2002-2014 László Németh. License: MPL/GPL/LGPL.

Based on OpenOffice.org's Myspell library.
Myspell's copyright (C) Kevin Hendricks, 2001-2002, License: BSD.

 
[SPACE] R)epl A)ccept I)nsert U)ncap S)tem Q)uit e(X)it or ? for help
   --check-apostrophe	check Unicode typographic apostrophe
   --check-url	check URLs, e-mail addresses and directory paths
   -1		check only first field in lines (delimiter = tabulator)
   -D		show available dictionaries
   -G		print only correct words or lines
   -H		HTML input file format
   -L		print lines with misspelled words
   -O		OpenDocument (ODF or Flat ODF) input file format
   -P password	set password for encrypted dictionaries
   -X		XML input file format

   -a		Ispell's pipe interface
   -d d[,d2,...]	use d (d2 etc.) dictionaries
   -h, --help	display this help and exit
   -i enc	input encoding
   -l		print misspelled words
   -m 		analyze the words of the input text
   -n		nroff/troff input file format
   -p dict	set dict custom dictionary
   -r		warn of the potential mistakes (rare words)
   -s 		stem the words of the input text
   -t		TeX/LaTeX input file format
   -v, --version	print version number
   -vv		print Ispell compatible version number
   -w		print misspelled words (= lines) from one word/line input.
 0-n	Replace with one of the suggested words.
 ?	Show this help screen.
 A	Accept the word for the rest of this session.
 AVAILABLE DICTIONARIES (path is not mandatory for -d option):
 Are you sure you want to throw away your changes?  Bug reports: http://hunspell.sourceforge.net
 Can't create tempfile Can't open %s.
 Can't open affix or dictionary files for dictionary named "%s".
 Can't open inputfile Can't open outputfile Cannot update personal dictionary. Check spelling of each FILE. Without FILE, check standard input.

 Example: hunspell -d en_US file.txt    # interactive spelling
         hunspell -i utf-8 file.txt    # check UTF-8 encoded file
         hunspell -l *.odt             # print misspelled words of ODF files

         # Quick fix of ODF documents by personal dictionary creation

         # 1 Make a reduced list from misspelled and unknown words:

         hunspell -l *.odt | sort | uniq >words

         # 2 Delete misspelled words of the file by a text editor.
         # 3 Use this personal dictionary to fix the deleted words:

         hunspell -p words *.odt

 FORBIDDEN! Hunspell has been compiled without Ncurses user interface.
 I	Accept the word, and put it in your private dictionary.
 LOADED DICTIONARY:
%s
%s
 Line %d: %s ->  Model word (a similar dictionary word):  Model word must be in the dictionary. Press any key! New word (stem):  Q	Quit immediately. Asks for confirmation. Leaves file unchanged.
 R	Replace the misspelled word completely.
 Replace with:  S	Ask a stem and a model word and store them in the private dictionary.
	The stem will be accepted also with the affixes of the model word.
 SEARCH PATH:
%s
 Space	Accept the word this time only.
 Spelling mistake? This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE,
to the extent permitted by law.
 U	Accept and add lowercase version to private dictionary.
 Usage: hunspell [OPTION]... [FILE]...
 Whenever a word is found that is not in the dictionary
it is printed on the first line of the screen.  If the dictionary
contains any similar words, they are listed with a number
next to each one.  You have the option of replacing the word
completely, or choosing one of the suggested words.
 X	Write the rest of this file, ignoring misspellings, and start next file.
 ^Z	Suspend program. Restart with fg command.
 a error - %s exceeds dictionary limit.
 error - iconv_open: %s -> %s
 error - iconv_open: UTF-8 -> %s
 error - missing HOME variable
 i q r s u x y Project-Id-Version: hunspell
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-06-02 13:54+0200
PO-Revision-Date: 2015-06-23 19:55+0000
Last-Translator: schuko24 <gerdsaenger@t-online.de>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
 	%s		Datei: %s

 
-- Drücken Sie die Leertaste, um fortzufahren -- 
 
Die Befehle sind:

 
Copyright (C) 2002-2014 László Németh. Lizenz: MPL/GPL/LGPL.

Basierend auf OpenOffice.org's Myspell Bibliothek.
Myspell's Copyright (C) Kevin Hendricks, 2001-2002, Lizenez: BSD.

 
[SPACE] E(R)setzen A)nnehmen E)infügen Öff(N)en Ab(L)eiten V)erlassen B)eenden oder ? für Hilfe
   --check-apostrophe	überprüfe Unicode typographische Apostrophe
   --check-url	überprüft URLs, E-Mail Adressen und Ordnerverzeichnisse
   -1		Nur das erste Feld einer Zeile prüfen (Trennzeichen = Tabulator)
   -D		Vorhandene Wörterbücher anzeigen
   -G		Nur richtige Wörter oder Zeilen ausgeben
   -H		HTML-Eingabedateiformat
   -L		Zeilen mit falsch geschriebenen Wörtern anzeigen
   -O		OpenDocument (ODF oder Flat ODF) Eingangs-Dateiformat
   -P password	Ein Passwort für verschlüsselte Wörterbücher angeben
   -X		XML Eingangs-Dateiformat

   -a		Pipe-Schnittstelle von Ispell
   -d d[,d2,...]	D (d2 usw.) Wörterbücher benutzen
   -h, --help	Diese Hilfe anzeigen und beenden.
   -i enc	Eingabezeichenkodierung
   -l		Falsch geschriebene Wörter anzeigen
   -m 		Die Wörter des Eingabetextes analysieren
   -n		nroff/troff-Eingabeformat
   -p dict	Benutzerdefiniertes Wörterbuch angeben
   -r		Bei potentiellen Fehlern (seltenen Wörtern) warnen
   -s 		Die Wörter des Eingabetextes auf ihren Wortstamm zurückführen
   -t		TeX/LaTeX-Eingabedateiformat
   -v, --version	Versionsnummer anzeigen
   -vv		Ispell-kompatible Versionsnummer anzeigen
   -w		 zeige falsch geschriebene Wörter (= Zeilen) aus einer Wort-/Zeileneingabe.
 0-n	Durch eines der vorgeschlagenen Wörter ersetzen.
 ?	Diese Hilfe anzeigen.
 A	Das Wort für den Rest der Sitzung akzeptieren.
 VERFÜGBARE WÖRTERBÜCHER (ein Pfad ist nicht zwingend erforderlich für die Option -d):
 Sind Sie sicher, dass Sie Ihre Änderungen verwerfen möchten?  Fehlerberichte: http://hunspell.sourceforge.net
 Es kann keine temporäre Datei erstellt werden. %s kann nicht geöffnet werden.
 Affix oder Wörterbuchdateien für das Wörterbuch »%s« können nicht geöffnet werden.
 Die Eingabedatei kann nicht geöffnet werden. Die Ausgabedatei kann nicht geöffnet werden. Persönliches Wörterbuch kann nicht aktualisiert werden. Rechtschreibprüfung für jede DATEI. Ohne die Angabe von DATEI, wird die Standardeingabe abgefragt.

 Beispiel: hunspell -d en_US file.txt    # interaktives Buchstabieren
         hunspell -i utf-8 file.txt    # prüfe UTF-8 kodierte Datei
         hunspell -l *.odt             # zeige Schreibfehler in ODF-Dateien

         # Schnellkorrektur von ODF-Dokumenten durch Erstellen eines persönlichen Wörterbuchs

         # 1 Erstellen einer einfachen Liste der falsch geschriebenen oder unbekannten Wörter:

         hunspell -l *.odt | sort | uniq >words

         # 2 Entfernen der falsch geschriebenen Wörter aus der Datei mit einem Texteditor.

         # 3 Persönliches Wörterbuch zur Korrektur der gelöschten Wörter verwenden:

         hunspell -p words *.odt

 VERBOTEN! Hunspell wurde ohne Ncurses-Schnittstelle kompiliert.
 I	Das Wort akzeptieren und im persönlichen Wörterbuch speichern.
 GELADENES WÖRTERBUCH:
%s
%s
 Zeile %d: %s ->  Modellwort (ein ähnliches Wort aus dem Wörterbuch):  Das Modellwort muss im Wörterbuch vorhanden sein. Drücken Sie eine Taste! Neues Wort (Stamm):  Q	Sofort beenden. Bestätigung wird erfragt. Die Datei bleibt unverändert.
 R	Das falsch geschriebene Wort vollständig ersetzen.
 Ersetzen mit:  S	Geben Sie einen Wortstamm und ein Modellwort an und diese werden im persönlichen Wörterbuch gespeichert.
	Der Wortstamm wird auch angenommen, wenn er Affixe des Modellwortes enthält.
 SUCHPFAD:
%s
 Leertaste	Das Wort nur dieses Mal akzeptieren.
 Rechtschreibfehler? Dies ist Freie Software; Hinweise zum Urheberrecht finden Sie im Quelltext.  Es gibt KEINE
Garantie; nicht einmal die Garantie der VERMARKTBARKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK,
soweit dies im Rahmen des geltenden Rechts möglich ist.
 U	Das Wort akzeptieren und in Kleinschreibweise im persönlichen Wörterbuch speichern.
 Verwendung: hunspell [OPTIONEN]… [DATEI]…
 Immer wenn ein Wort gefunden wird, das noch nicht im
Wörterbuch eingetragen ist, wird es in der ersten Zeile
angezeigt. Falls das Wörterbuch ähnliche Wörter enthält, werden
diese jeweils mit einer Nummer versehen aufgelistet.
Sie haben die Möglichkeit das Wort komplett zu ersetzen oder
eins der vorgeschlagenen Wörter auszuwählen.
 X	Den Rest der Datei schreiben, Schreibfehler ignorieren und neue Datei beginnen.
 ^Z	Programm unterbrechen. Neustart mit fg-Befehl.
 a Fehler – %s überschreitet die Wörterbuchgrenze.
 Fehler – iconv_open: %s -> %s
 Fehler – iconv_open: UTF-8 -> %s
 Fehler – Fehlende HOME-Variable
 i q r s u x y 