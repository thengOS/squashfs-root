��          \      �       �      �      �   �   �      �     �          	  �    !   �     �  6  �     (     -     >     C                                       AppArmor Desktop Preferences AppArmor Rejections AppArmorApplet is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version. Path Profile Generation YAST genprof Project-Id-Version: apparmorapplet
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-02-06 14:27-0800
PO-Revision-Date: 2015-05-29 19:52+0000
Last-Translator: Tobias Bannert <tobannert@gmail.com>
Language-Team: Novell Language <language@novell.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:13+0000
X-Generator: Launchpad (build 18115)
 AppAmor-Schreibtischeinstellungen AppAmor-Zurückweisungen AppAmorApplet ist freie Software, Sie können sie gemäß der Bestimmungen
der »GNU General Public License« (in der von der »Free Software Foundation«
veröffentlichten Form) weiter verteilen und/oder bearbeiten. Das gilt für
Version 2 der Lizenz bzw. eine beliebige höhere Version (nach Ihrem Ermessen). Pfad Profilerstellung YaST genprof 