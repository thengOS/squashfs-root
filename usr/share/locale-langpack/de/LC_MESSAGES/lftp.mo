��    �       �  �      �'     �'     (  	   "(     ,(  #   B(  "   f(  �  �(  �  �+  	  k1     u2  �   �2  	   �3     �3     �3     �3  
   �3     �3  
   �3     �3     �3     �3  "   4     *4  !   E4  <   g4     �4  '   �4      �4     �4     5     05     D5  "   Z5  "   }5     �5     �5     �5     �5     �5  G   6     ]6  /   t6  6   �6     �6  /   �6  ,   #7     P7  "   j7     �7  /   �7     �7      �7     8  1   18  *   c8  /   �8  /   �8     �8     9      "9  %   C9     i9  #   �9  *   �9     �9     �9     :  #    :     D:  ,   _:     �:  ,   �:  ,   �:  '   ;  -   ,;      Z;  (   {;  (   �;  !   �;      �;  $   <  @   5<  ,   v<  .   �<     �<     �<     =  <   *=  (   g=  #   �=  ;   �=  9   �=     *>  
   ,>  -   7>  3   e>  1   �>  0   �>     �>     ?  
    ?     +?      4?  *   U?     �?  *   �?     �?     �?  >   �?     !@  <   5@  +   r@  �   �@    )A  j  +B     �C  j   �C     D     6D     RD     iD     }D     �D  	   �D  &   �D  /   �D     E     'E     EE     SE     cE      |E     �E     �E     �E  ,   �E  *   F  �   GF     �F     G  /   G     NG     ^G  )   cG  R   �G  =  �G     I  @   2I  ,   sI     �I     �I     �I  
   �I     �I     �I     J  b  J  �  uK      M     ;M     YM     sM  `   �M     �M     �M     N      N  	   9N     CN     `N     }N  4   �N  8   �N  2   �N  T  0O  @   �Q     �Q     �Q     �Q  ;   R  o   KR  �  �R  �   �Y  ~   �Z  �   [     \     \      \  ,   =\  V   j\     �\     �\  !   �\     ]  #   ,]  6   P]     �]     �]     �]     �]     �]  <   �]     $^     2^  >   A^  !   �^     �^     �^     �^     �^  
   _     _     &_  -   8_  <   f_     �_     �_     �_     �_     �_  "   `  *   )`  %   T`     z`     �`     �`     �`  S   �`  0   a     Na     ja     �a     �a     �a  ^  �a     2c  K  Lc  �   �d     Oe     ce  ,   {e  (   �e  D   �e  0   f  /   Gf  1   wf  0   �f     �f     �f  ;  g  9   Hh  �   �h     {i     �i     �i     �i  ,   �i  6   �i  �   $j  �  �j     jl     ~l  '   �l  "   �l  %   �l     �l     m      "m     Cm      Zm  �  {m     6q     Qq     kq     �q  �   �q     "r     9r  "   Or  )   rr     �r  %   �r  $   �r     �r     s     *s  k  ?s  \  �t  
   v     v     .v     ?v     \v     {v  9   �v     �v     �v     �v     w     ,w  !   Aw     cw     zw     �w     �w     �w     �w     x     x     5x    Kx     Sy  :   ny  ;   �y  A   �y  3   'z  �   [z  f   /{  g   �{     �{     |  '   1|     Y|  Z   f|     �|     �|     �|     }     4}      L}     m}  '   �}     �}  !   �}     �}      ~  :   "~  .   ]~     �~  3   �~     �~  !   �~          (     ;     =  "   S    v     ��     ��     ʁ     �  &   �      �     9�  �  K�     ܃  X  �  )   D�     n�     ��     ��  d   ��     &�  	   7�     A�     P�     f�     ��  -   ��  +   Ɔ  7   �     *�  7   ?�  #   w�     ��     ��  %   ć     �     �     �     )�     .�     I�     `�    w�     �     ��     ��  #   ��  8   �  -   �     I�  @   ^�  =   ��     ݊     ��     �  #   #�     G�     a�     r�     ��     ��     ��     ԋ     �     �     &�     5�     R�     q�     ��  
   ��     ��     ��     ό     �     �     �     �     -�  1   L�     ~�  (   ��     ��     Ǎ     ۍ     �     ��     �     �  #   2�     V�     i�     z�      ��     ��     Ɏ  $   �     �  )   $�  $   N�     s�  +   ��     ��  #   ˏ  ,   �      �     =�     ]�     m�     ��     ��     ��  6   ͐     �     �     7�     L�     ^�     x�     ��     ��     ��     đ     ґ     �     �     +�     1�     M�     j�     ��     ��     ��  �  ��  *   n�     ��     ��     Ɣ  #   ڔ      ��  �  �  a  �  T  m�     ¡  *  �     �     $�     4�     =�  
   E�     P�  
   X�     c�     k�     ��  1   ��  '   ̣  !   ��  >   �     U�  2   a�  ,   ��  %   ��     �     �     �  $   7�  $   \�     ��     ��     ��     ѥ     �  S   �     Y�  )   n�  [   ��     ��  0   �  )   E�     o�      ��  *   ��  7   ӧ     �  )   +�     U�  O   o�  ;   ��  9   ��  8   5�     n�  $   ��  1   ��  -   �     �     1�  5   O�  %   ��      ��     ̪  .   �  '   �  .   >�  !   m�  1   ��  .   ��  /   �  /    �  (   P�  ,   y�  2   ��  $   ٬  4   ��  :   3�  A   n�  >   ��  B   �     2�     R�     r�  F   ��  ,   ծ  ,   �  A   /�  E   q�     ��  	   ��  4   į  5   ��  3   /�  2   c�     ��     ��     ǰ     Ӱ  !   ٰ  +   ��     '�  @   @�      ��     ��  F   ��     �  P   �  A   m�  �   ��  E  N�  �  ��  #   �  �   >�     ʶ     �     �     "�     =�     ]�  	   w�  &   ��  )   ��     ҷ  .   �      �     ,�  )   <�  )   f�  #   ��  '   ��     ܸ  8   ��  0   1�  �   b�      $�      E�  9   f�     ��     ��  2   ź  [   ��  �  T�     �  P   
�  4   [�     ��     ��      ��     ӽ     �     ��  
   �  �  �  �  ��     ��     ��     ��     ��  �   �     ��     ��  ,   ��  )   ��     '�  #   /�  #   S�     w�  B   ��  C   ��  2   �  �  F�  @   �     \�     |�  "   ��  B   ��  �   ��  `  ��  '  ��  �   $�  $  ��     ��     	�  )   �  >   H�  m   ��     ��     �  3   .�     b�  ,   ~�  C   ��     ��     �  '   �  5   A�  &   w�  I   ��     ��     ��  q   
�  2   |�  +   ��  !   ��  #   ��     !�  
   ?�  !   J�      l�  ;   ��  G   ��  '   �     9�     Y�     i�     ��  )   ��  -   ��  )   ��     �     /�     >�  #   Q�  |   u�  :   ��     -�     H�  !   c�      ��      ��  �  ��     |�  �  ��  �   �  8   ��     ��  0   �  9   F�  [   ��  4   ��  3   �  /   E�  .   u�  "   ��     ��  v  ��  F   Y�  q  ��     �     %�     1�     A�  6   X�  @   ��  �   ��  $  ��     ��     ��  *   ��  )   �  0   /�  "   `�     ��  '   ��  "   ��  7   ��  �  �     ��     ��     ��     �  �   /�     ��     ��  )   �  :   6�     q�  ,   ��  )   ��     ��     ��     �    +�  �  3�     $�  !   3�     U�  !   m�  *   ��     ��  D   ��  #   �     =�     Q�     n�     ��  !   ��     ��  !   ��  '   �  )   .�  !   X�  !   z�  "   ��  #   ��  '   ��  i  �      u�  ]   ��  Q   ��  V   F�  R   ��    ��  v   ��  �   m�     ��      �  :   2�     m�  �   ~�     �     -�  #   J�  2   n�     ��     ��  &   ��  +   ��     %�  !   6�     X�     t�  I   ��  ,   ��     
  M   "     p  !   �  "   �     �     �     �  +   �    %    @ !   X    z    �     � 2   �     �      � �   2   � ,   � !   � "    �   8    �    �    �    	    "	    B	 :   ]	 )   �	 D   �	    
 ;   %
 &   a
 &   �
    �
 )   �
    �
        /    G "   S    v    � �   �    �    �    � 0   � E    >   b    � M   � L    !   Q %   s    � &   � )   �    � #    $   0    U    o *   �    �    �    �     &        G    g    z    �    �    �    �    �    �     "    >   ?    ~ .   �    �    �    �    �        "    9 C   P    �    � )   � %   �        * &   E #   l >   � 3   � .    8   2    k /   � 6   � !   � %       7 ,   N     {    �    � Q   �     (   /    X    o !   �    �    � "   �    �     (       ? .   ] 	   �     � &   �    �    �                �  �   �   4  G   �  �      D   �  ]   �       @  �       2          Y   �  �  �   8  �   �  9   '   �      �  b  �   �       5  �   ^   3      �  �   a     \        B  (         y   �  G  �   7           �  �     k  �       #  q           �  �   �     �              `  8   H  �  Y      �   �   C  B   ?  �      U       }  �   Z  �     �   �  �      R   �      "  �   |          Q   �  �   �       F           �  1   �         `     N          �  u  �  �  )  2   �       �   ^                         �   �  �  n          �  �             f   c  �  �   t               �  M         u   �  �  �  d                         _  I  y  e   �      �         �   �       v       �   �   �   [   c       �   �  F  K   �       �   �   �   E                  �      /           n   �   �   w     R  H   �       C   i  �   !   m  P         �      �  �   �   [  _   0        >  �         �  �       ;   =  ;  s  �           �  �   �           L   |  '  A   V    $        �  �     �        g   �   �   �   f      w       q  �       N  %   �       <                   ~   �   �   \   i   0               x  4        �   (      U  �   �   �   �      �   �    �  W      �   &  /  7   �      �   Q      �   -     ]      {    �  �  �   }           �   �   �       	   A         9  �    �   �  �   �  
      x   �   m       3   �     #     �  �   �   �  	  M      l             z   �   b       :  �   �   �   z  �  �         ,  5   �      �   T         �       �                    �       �  e  J  �   h         �           �   ,   �   h   6        r  �       �   �       �              X         P       �        >   �       "         �  �   �     S   O      �  ~  �  �  �   �   �   �   +   �  �     .           �      *  �   v             V           �   �   �  �  S     �  �  {   �          �  �  �   T  �   �  X       �          -  �   6   l             �   �   �  
       �   �   �     W           �       o    g      I   o   k   s   !  �      D  �  �   �   �        a                       &       �   Z   j   �   �         *   j  �       =   �           �   �       �   1  p   O   @   +      L  �   p  d     t          �  �  �   �  �       �       :   �    �  E  J   �      )   ?   �  K  .  $       �      �   r   �                  �      %  �   �   �       �   �   �   �       <  �    	Executing builtin `%s' [%s]
 	Repeat count: %d
 	Running
 	Waiting for command
 	Waiting for job [%d] to terminate
 	Waiting for termination of jobs:  
       queue [-n num] <command>

Add the command to queue for current site. Each site has its own command
queue. `-n' adds the command before the given item in the queue. It is
possible to queue up a running job by using command `queue wait <jobno>'.

       queue --delete|-d [index or wildcard expression]

Delete one or more items from the queue. If no argument is given, the last
entry in the queue is deleted.

       queue --move|-m <index or wildcard expression> [index]

Move the given items before the given queue index, or to the end if no
destination is given.

Options:
 -q                  Be quiet.
 -v                  Be verbose.
 -Q                  Output in a format that can be used to re-queue.
                     Useful with --delete.
 
Mirror specified remote directory to local directory

 -c, --continue         continue a mirror job if possible
 -e, --delete           delete files not present at remote site
     --delete-first     delete old files before transferring new ones
 -s, --allow-suid       set suid/sgid bits according to remote site
     --allow-chown      try to set owner and group on files
     --ignore-time      ignore time when deciding whether to download
 -n, --only-newer       download only newer files (-c won't work)
 -r, --no-recursion     don't go to subdirectories
 -p, --no-perms         don't set file permissions
     --no-umask         don't apply umask to file modes
 -R, --reverse          reverse mirror (put files)
 -L, --dereference      download symbolic links as files
 -N, --newer-than=SPEC  download only files newer than specified time
 -P, --parallel[=N]     download N files in parallel
 -i RX, --include RX    include matching files
 -x RX, --exclude RX    exclude matching files
                        RX is extended regular expression
 -v, --verbose[=N]      verbose operation
     --log=FILE         write lftp commands being executed to FILE
     --script=FILE      write lftp commands to FILE, but don't execute them
     --just-print, --dry-run    same as --script=-

When using -R, the first directory is local and the second is remote.
If the second directory is omitted, basename of first directory is used.
If both directories are omitted, current local and remote directories are used.
 
lftp now tricks the shell to move it to background process group.
lftp continues to run in the background despite the `Stopped' message.
lftp will automatically terminate when all jobs are finished.
Use `fg' shell command to return to lftp if it is still running.
  - not supported protocol  -w <file> Write history to file.
 -r <file> Read history from file; appends to current history.
 -c  Clear the history.
 -l  List the history (default).
Optional argument cnt specifies the number of history lines to list,
or "all" to list all entries.
  [cached] !<shell-command> %.0f B/s %.0fb/s %.1f KiB/s %.1fK/s %.2f MiB/s %.2fM/s %d address$|es$ found %d file$|s$ found %d file$|s$ found, now scanning %s %ld $#l#byte|bytes$ cached %lld $#ll#byte|bytes$ transferred %lld $#ll#byte|bytes$ transferred in %ld $#l#second|seconds$ %s (filter) %s failed for %d of %d director$y|ies$
 %s failed for %d of %d file$|s$
 %s is a built-in alias for %s
 %s is an alias to `%s'
 %s must be one of:  %s must be one of: %s %s ok, %d director$y|ies$ created
 %s ok, %d director$y|ies$ removed
 %s ok, %d file$|s$ removed
 %s ok, `%s' created
 %s ok, `%s' removed
 %s%d error$|s$ detected
 %s: %d - no such job
 %s: %s - no such cached session. Use `scache' to look at session list.
 %s: %s - not a number
 %s: %s. Use `set -a' to look at all variables.
 %s: %s: file already exists and xfer:clobber is unset
 %s: %s: no files found
 %s: --share conflicts with --output-directory.
 %s: -m: Number expected as second argument.  %s: -n: Number expected.  %s: -n: positive number expected.  %s: BUG - deadlock detected
 %s: GetPass() failed -- assume anonymous login
 %s: No queue is active.
 %s: Operand missed for `expire'
 %s: Operand missed for size
 %s: Please specify a file or directory to share.
 %s: Please specify meta-info file or URL.
 %s: ambiguous source directory (`%s' or `%s'?)
 %s: ambiguous target directory (`%s' or `%s'?)
 %s: argument required.  %s: bookmark name required
 %s: cannot create local session
 %s: command `%s' is not compiled in.
 %s: date-time parse error
 %s: date-time specification missed
 %s: import type required (netscape,ncftp)
 %s: invalid block size `%s'
 %s: invalid option -- '%c'
 %s: no current job
 %s: no old directory for this site
 %s: no such bookmark `%s'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: received redirection to `%s'
 %s: regular expression `%s': %s
 %s: some other job waits for job %d
 %s: source directory is required (mirror:require-source is set)
 %s: spaces in bookmark name are not allowed
 %s: summarizing conflicts with --max-depth=%i
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 %s: wait loop detected
 %s: warning: summarizing is the same as using --max-depth=0
 %sModified: %d file$|s$, %d symlink$|s$
 %sNew: %d file$|s$, %d symlink$|s$
 %sRemoved: %d director$y|ies$, %d file$|s$, %d symlink$|s$
 %sTotal: %d director$y|ies$, %d file$|s$, %d symlink$|s$
 ' (commands) **** FXP: giving up, reverting to plain copy
 **** FXP: trying to reverse ftp:fxp-passive-source
 **** FXP: trying to reverse ftp:fxp-passive-sscn
 **** FXP: trying to reverse ftp:ssl-protect-fxp
 , maximum size %ld
 , no size limit , or empty =1 =0|>1 Accepted connection from [%s]:%d Accepted data connection from (%s) port %u Access failed:  Account is required, set ftp:acct variable Added job$|s$ Ambiguous command `%s'.
 Ambiguous command `%s'. Use `help' to see available commands.
 Ambiguous command.  Attach the terminal to specified backgrounded lftp process.
 Cannot bind a socket for torrent:port-range Change current local directory to <ldir>. The previous local directory
is stored as `-'. You can do `lcd -' to change the directory back.
 Change current remote directory to <rdir>. The previous remote directory
is stored as `-'. You can do `cd -' to change the directory back.
The previous directory for each site is also stored on disk, so you can
do `open site; cd -' even after lftp restart.
 Change the mode of each FILE to MODE.

 -c, --changes        - like verbose but report only when a change is made
 -f, --quiet          - suppress most error messages
 -v, --verbose        - output a diagnostic for every file processed
 -R, --recursive      - change files and directories recursively

MODE can be an octal number or symbolic mode (see chmod(1))
 Changing remote directory... Close idle connections. By default only with current server.
 -a  close idle connections with all servers
 Closing HTTP connection Closing aborted data socket Closing control socket Closing data socket Closing idle connection Commands queued: Connected Connecting data socket to (%s) port %u Connecting data socket to proxy %s (%s) port %u Connecting to %s%s (%s) port %u Connecting to peer %s port %u Connecting... Connection idle Connection limit reached Could not parse HTTP status line Created a stopped queue.
 DNS resolution not trusted. Data connection established Data connection peer has mismatching address Data connection peer has wrong port number Define or undefine alias <name>. If <value> omitted,
the alias is undefined, else is takes the value <value>.
If no argument is given the current aliases are listed.
 Delaying before reconnect Delaying before retry Delete specified job with <job_no> or all jobs
 Deleted job$|s$ Done Execute commands recorded in file <file>
 Execute site command <site_cmd> and output the result
You can redirect its output
 Expand wildcards and run specified command.
Options can be used to expand wildcards to list of files, directories,
or both types. Type selection is not very reliable and depends on server.
If entry type cannot be determined, it will be included in the list.
 -f  plain files (default)
 -d  directories
 -a  all types
 FEAT negotiation... Failed to change mode of `%s' because no old mode is available.
 Failed to change mode of `%s' to %04o (%s).
 Fatal error Fetching headers... File cannot be accessed File moved File moved to ` File name missed.  Finished %s Gets selected files with expanded wildcards
 -c  continue, resume transfer
 -d  create directories the same as in file names and get the
     files into them instead of current directory
 -E  delete remote files after successful transfer
 -a  use ascii mode (binary is the default)
 -O <base> specifies base directory or URL where files should be placed
 Gets the specified file using several connections. This can speed up transfer,
but loads the net heavily impacting other users. Use only if you really
have to transfer the file ASAP.

Options:
 -c  continue transfer. Requires <lfile>.lftp-pget-status file.
 -n <maxconn>  set maximum number of connections (default is is taken from
     pget:default-n setting)
 -O <base> specifies base directory where files should be placed
 Getting directory contents Getting file list (%lld) [%s] Getting files information Getting meta-data: %s Group commands together to be executed as one command
You can launch such a group in background
 Handshaking... Hit EOF Hit EOF while fetching headers Host name lookup failure Interrupt Invalid IPv4 numeric address Invalid IPv6 numeric address Invalid command.  Invalid range format. Format is min-max, e.g. 10-20. Invalid time format. Format is <time><unit>, e.g. 2h30m. Invalid time unit letter, only [smhd] are allowed. LFTP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LFTP.  If not, see <http://www.gnu.org/licenses/>.
 LFTP | Version %s | Copyright (c) 1996-%d Alexander V. Lukyanov
 Launch shell or shell command
 Libraries used:  Link <file1> to <file2>
 List cached sessions or switch to specified session number
 List remote file names.
By default, nlist output is cached, to see new listing use `renlist' or
`cache flush'.
 List remote files. You can redirect output of this command to file
or via pipe to external command.

 -1                   - single-column output
 -a, --all            - show dot files
 -B, --basename       - show basename of files only
     --block-size=SIZ - use SIZ-byte blocks
 -d, --directory      - list directory entries instead of contents
 -F, --classify       - append indicator (one of /@) to entries
 -h, --human-readable - print sizes in human readable format (e.g., 1K)
     --si             - likewise, but use powers of 1000 not 1024
 -k, --kilobytes      - like --block-size=1024
 -l, --long           - use a long listing format
 -q, --quiet          - don't show status
 -s, --size           - print size of each file
     --filesize       - if printing size, only print size for files
 -i, --nocase         - case-insensitive pattern matching
 -I, --sortnocase     - sort names case-insensitively
 -D, --dirsfirst      - list directories first
     --sort=OPT       - "name", "size", "date"
 -S                   - sort by file size
 --user, --group, --perms, --date, --linkcount, --links
                      - show individual fields
 --time-style=STYLE   - use specified time format

By default, cls output is cached, to see new listing use `recls' or
`cache flush'.

The variables cls-default and cls-completion-default can be used to
specify defaults for cls listings and completion listings, respectively.
For example, to make completion listings show file sizes, set
cls-completion-default to "-s".

Tips: Use --filesize with -D to pack the listing better.  If you don't
always want to see file sizes, --filesize in cls-default will affect the
-s flag on the commandline as well.  Add `-i' to cls-completion-default
to make filename completion case-insensitive.
 List remote files. You can redirect output of this command to file
or via pipe to external command.
By default, ls output is cached, to see new listing use `rels' or
`cache flush'.
See also `help cls'.
 List running jobs. -v means verbose, several -v can be specified.
If <job_no> is specified, only list a job with that number.
 Load module (shared object). The module should contain function
   void module_init(int argc,const char *const *argv)
If name contains a slash, then the module is searched in current
directory, otherwise in directories specified by setting module:path.
 Logging in... Login failed MLSD is disabled by ftp:use-mlsd MLST and MLSD are not supported by this site Make remote directories
 -p  make all levels of path
 -f  be quiet, suppress messages
 Making data connection... Making directory `%s' Making symbolic link `%s' to `%s' Mirroring directory `%s' Mode of `%s' changed to %04o (%s).
 Module for command `%s' did not register the command.
 Moved job$|s$ No address found No queued job #%i.
 No queued jobs match "%s".
 No queued jobs.
 No such command `%s'. Use `help' to see available commands.
 Not connected Now executing: Object is not cached and http:cache-control has only-if-cached Old directory `%s' is not removed Old file `%s' is not removed Operation not supported Overwriting old file `%s' POST method failed Password:  Peer closed connection Persist and retry Print current remote URL.
 -p  show password
 Print help for command <cmd>, or list of available commands
 Proxy protocol unsupported Queue is stopped. Received all Received all (total) Received last chunk Received not enough data, retrying Received valid info about %d IPv6 peer$|s$ Received valid info about %d peer$|s$ Receiving body... Receiving data Receiving data/TLS Remove remote directories
 Remove remote files
 -r  recursive directory removal, be careful
 -f  work quietly
 Removes specified files with wildcard expansion
 Removing old directory `%s' Removing old file `%s' Removing old local file `%s' Removing source file `%s' Rename <file1> to <file2>
 Repeat specified command with a delay between iterations.
Default delay is one second, default command is empty.
 -c <count>  maximum number of iterations
 -d <delay>  delay between iterations
 --while-ok  stop when command exits with non-zero code
 --until-ok  stop when command exits with zero code
 --weak      stop when lftp moves to background.
 Resolving host address... Retrieve remote file <rfile> and store it to local file <lfile>.
 -o <lfile> specifies local file name (default - basename of rfile)
 -c  continue, resume transfer
 -E  delete remote files after successful transfer
 -a  use ascii mode (binary is the default)
 -O <base> specifies base directory or URL where files should be placed
 Retrieve remote file to a temporary location, run a local editor on it
and upload the file back if changed.
 -k  keep the temporary file
 -o <temp>  explicit temporary file location
 Retrying mirror...
 Running connect program SITE CHMOD is disabled by ftp:use-site-chmod SITE CHMOD is not supported by this site Same as `cat <files> | more'. if PAGER is set, it is used as filter
 Same as cat, but filter each file through bzcat
 Same as cat, but filter each file through zcat
 Same as more, but filter each file through bzcat
 Same as more, but filter each file through zcat
 Scanning directory `%s' Seeding in background...
 Select a server, URL or bookmark
 -e <cmd>            execute the command just after selecting
 -u <user>[,<pass>]  use the user/password for authentication
 -p <port>           use the port for connection
 -s <slot>           assign the connection to this slot
 <site>              host name, URL or bookmark name
 Send bug reports and questions to the mailing list <%s>.
 Send the command uninterpreted. Use with caution - it can lead to
unknown remote state and thus will cause reconnect. You cannot
be sure that any change of remote state because of quoted command
is solid - it can be reset by reconnect at any time.
 Sending commands... Sending data Sending data/TLS Sending request... Server reply matched ftp:retry-530, retrying Server reply matched ftp:retry-530-anonymous, retrying Set debug level to given value or turn debug off completely.
 -o <file>  redirect debug output to the file
 -c  show message context
 -p  show PID
 -t  show timestamps
 Set variable to given value. If the value is omitted, unset the variable.
Variable name has format ``name/closure'', where closure can specify
exact application of the setting. See lftp(1) for details.
If set is called with no variable then only altered settings are listed.
It can be changed by options:
 -a  list all settings, including default values
 -d  list only default values, not necessary current ones
 Shows lftp version
 Shutting down:  Skipping directory `%s' (only-existing) Skipping file `%s' (only-existing) Skipping symlink `%s' (only-existing) Sleep time left:  Sleeping forever Socket error (%s) - reconnecting Sorry, no help for %s
 Store failed - you have to reput Summarize disk usage.
 -a, --all             write counts for all files, not just directories
     --block-size=SIZ  use SIZ-byte blocks
 -b, --bytes           print size in bytes
 -c, --total           produce a grand total
 -d, --max-depth=N     print the total for a directory (or file, with --all)
                       only if it is N or fewer levels below the command
                       line argument;  --max-depth=0 is the same as
                       --summarize
 -F, --files           print number of files instead of sizes
 -h, --human-readable  print sizes in human readable format (e.g., 1K 234M 2G)
 -H, --si              likewise, but use powers of 1000 not 1024
 -k, --kilobytes       like --block-size=1024
 -m, --megabytes       like --block-size=1048576
 -S, --separate-dirs   do not include size of subdirectories
 -s, --summarize       display only a total for each argument
     --exclude=PAT     exclude files that match PAT
 Switching passive mode off Switching passive mode on Switching to NOREST mode TLS negotiation... There are running jobs and `cmd:move-background' is not set.
Use `exit bg' to force moving to background or `kill all' to terminate jobs.
 Timeout - reconnecting Too many redirections Total %d $file|files$ transferred
 Transfer of %d of %d $file|files$ failed
 Transferring file `%s' Try `%s --help' for more information
 Try `help %s' for more information.
 Turning on sync-mode Unknown command `%s'.
 Unknown system error Upload <lfile> with remote name <rfile>.
 -o <rfile> specifies remote file name (default - basename of lfile)
 -c  continue, reput
     it requires permission to overwrite remote files
 -E  delete local files after successful transfer (dangerous)
 -a  use ascii mode (binary is the default)
 -O <base> specifies base directory or URL where files should be placed
 Upload files with wildcard expansion
 -c  continue, reput
 -d  create directories the same as in file names and put the
     files into them instead of current directory
 -E  delete local files after successful transfer (dangerous)
 -a  use ascii mode (binary is the default)
 -O <base> specifies base directory or URL where files should be placed
 Usage: %s
 Usage: %s %s[-f] files...
 Usage: %s <cmd>
 Usage: %s <jobno> ... | all
 Usage: %s <user|URL> [<pass>]
 Usage: %s [-d #] dir
 Usage: %s [-e cmd] [-p port] [-u user[,pass]] <host|url>
 Usage: %s [-e] <file|command>
 Usage: %s [-p]
 Usage: %s [-v] [-v] ...
 Usage: %s [<exit_code>]
 Usage: %s [<jobno>]
 Usage: %s [OPTS] command args...
 Usage: %s [OPTS] file
 Usage: %s [OPTS] files...
 Usage: %s [OPTS] mode file...
 Usage: %s [options] <dirs>
 Usage: %s cmd [args...]
 Usage: %s command args...
 Usage: %s local-dir
 Usage: %s module [args...]
 Usage: cd remote-dir
 Usage: find [OPTS] [directory]
Print contents of specified directory or current directory recursively.
Directories in the list are marked with trailing slash.
You can redirect output of this command.
 -d, --maxdepth=LEVELS  Descend at most LEVELS of directories.
 Usage: mv <file1> <file2>
 Usage: reget [OPTS] <rfile> [-o <lfile>]
Same as `get -c'
 Usage: rels [<args>]
Same as `ls', but don't look in cache
 Usage: renlist [<args>]
Same as `nlist', but don't look in cache
 Usage: reput <lfile> [-o <rfile>]
Same as `put -c'
 Usage: sleep <time>[unit]
Sleep for given amount of time. The time argument can be optionally
followed by unit specifier: d - days, h - hours, m - minutes, s - seconds.
By default time is assumed to be seconds.
 Usage: slot [<label>]
List assigned slots.
If <label> is specified, switch to the slot named <label>.
 Use specified info for remote login. If you specify URL, the password
will be cached for future usage.
 Valid arguments are: Validation: %u/%u (%u%%) %s%s Verify command failed without a message Verifying... Wait for specified job to terminate. If jobno is omitted, wait
for last backgrounded job.
 Waiting for TLS shutdown... Waiting for data connection... Waiting for meta-data... Waiting for other copy peer... Waiting for response... Waiting for transfer to complete Warning: chdir(%s) failed: %s
 Warning: discarding incomplete command
 [%d] Done (%s) [%u] Attached to terminal %s. %s
 [%u] Attached to terminal.
 [%u] Detached from terminal. %s
 [%u] Detaching from the terminal to complete transfers...
 [%u] Exiting and detaching from the terminal.
 [%u] Finished. %s
 [%u] Moving to background to complete transfers...
 [%u] Started.  %s
 [%u] Terminated by signal %d. %s
 [re]cls [opts] [path/][pattern] [re]nlist [<args>] ` `%s' at %lld %s%s%s%s `%s', got %lld of %lld (%d%%) %s%s `lftp' is the first command executed by lftp after rc files
 -f <file>           execute commands from the file and exit
 -c <cmd>            execute the commands and exit
 --help              print this help and exit
 --version           print lftp version and exit
Other options are the same as in `open' command
 -e <cmd>            execute the command just after selecting
 -u <user>[,<pass>]  use the user/password for authentication
 -p <port>           use the port for connection
 <site>              host name, URL or bookmark name
 alias [<name> [<value>]] ambiguous argument %s for %s ambiguous variable name announced via  anon - login anonymously (by default)
 assuming failed host name lookup bookmark [SUBCMD] bookmark command controls bookmarks

The following subcommands are recognized:
  add <name> [<loc>] - add current place or given location to bookmarks
                       and bind to given name
  del <name>         - remove bookmark with the name
  edit               - start editor on bookmarks file
  import <type>      - import foreign bookmarks
  list               - list bookmarks (default)
 cache [SUBCMD] cache command controls local memory cache

The following subcommands are recognized:
  stat        - print cache status (default)
  on|off      - turn on/off caching
  flush       - flush cache
  size <lim>  - set memory limit
  expire <Nx> - set cache expiration time to N seconds (x=s)
                minutes (x=m) hours (x=h) or days (x=d)
 cannot create socket of address family %d cannot get current directory cannot parse EPSV response cannot seek on data source cat - output remote files to stdout (can be redirected)
 -b  use binary mode (ascii is the default)
 cat [-b] <files> cd <rdir> cd ok, cwd=%s
 chdir(%s) failed: %s
 chmod [OPTS] mode file... chunked format violated copy: all data received, but get rolled back
 copy: destination file is already complete
 copy: get rolled back to %lld, seeking put accordingly
 copy: put is broken
 copy: put rolled back to %lld, seeking get accordingly
 copy: received redirection to `%s'
 debug [OPTS] [<level>|off] debug is off
 debug level is %d, output goes to %s
 depend module `%s': %s
 du [options] <dirs> edit [OPTS] <file> eta: execl(/bin/sh) failed: %s
 execlp(%s) failed: %s
 execvp(%s) failed: %s
 exit - exit from lftp or move to background if jobs are active

If no jobs active, the code is passed to operating system as lftp
termination status. If omitted, exit code of last command is used.
`bg' forces moving to background if cmd:move-background is false.
 exit [<code>|bg] extra server response file name missed in URL file size decreased during transfer ftp over http cannot work without proxy, set hftp:proxy. ftp:fxp-force is set but FXP is not available ftp:proxy password:  ftp:skey-force is set and server does not support OPIE nor S/KEY ftp:ssl-force is set and server does not support or allow SSL get [OPTS] <rfile> [-o <lfile>] glob [OPTS] <cmd> <args> help [<cmd>] history -w file|-r file|-c|-l [cnt] host name resolve timeout integer overflow invalid argument %s for %s invalid argument for `--sort' invalid block size invalid boolean value invalid boolean/auto value invalid floating point number invalid mode string: %s
 invalid number invalid peer response format invalid server response format invalid unsigned number kill all|<job_no> lcd <ldir> lcd ok, local cwd=%s
 lftp [OPTS] <site> ln [-s] <file1> <file2> ls [<args>] max-retries exceeded memory exhausted mget [OPTS] <files> mirror [OPTS] [remote [local]] mirror: protocol `%s' is not suitable for mirror
 module name [args] modules are not supported on this system more <files> mput [OPTS] <files> mrm <files> must be one of:  mv <file1> <file2> next announce in %s next request in %s no closure defined for this setting no such %s service no such variable non-option arguments found only PUT and POST values allowed open [OPTS] <site> parse: missing filter command
 parse: missing redirection filename
 peer closed connection peer closed connection (before handshake) peer closed just accepted connection peer handshake timeout peer sent unknown info_hash=%s in handshake peer short handshake peer unexpectedly closed connection peer unexpectedly closed connection after %s pget [OPTS] <rfile> [-o <lfile>] pget: falling back to plain get pipe() failed:  pseudo-tty allocation failed:  put [OPTS] <lfile> [-o <rfile>] queue [OPTS] [<cmd>] quote <cmd> recls [<args>]
Same as `cls', but don't look in cache
 rename successful
 repeat [OPTS] [delay] [command] rm [-r] [-f] <files> rmdir [-f] <dirs> saw file size in response scache [<session_no>] seek failed set [OPT] [<var> [<val>]] site <site-cmd> source <file> the source file size is unknown the target file is remote this encoding is not supported total unknown address family `%s' unsupported network protocol user <user|URL> [<pass>] wait [<jobno>] zcat <files> zmore <files> Project-Id-Version: de
Report-Msgid-Bugs-To: lftp-bugs@lftp.yar.ru
POT-Creation-Date: 2015-06-17 17:10+0300
PO-Revision-Date: 2016-02-22 16:02+0000
Last-Translator: Ubuntu Archive Auto-Sync <archive@ubuntu.com>
Language-Team: German <kde-i18n-de@lists.kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:53+0000
X-Generator: Launchpad (build 18115)
Language: de
 	Eingebautes »%s« wird ausgeführt [%s]
 	Wiederhole Zählung: %d
 	Wird ausgeführt
 	Warten auf Befehl
 	Warte auf Beendigung von Job [%d]
 	Warte auf Erledigung der Jobs:  
       queue [-n num] <Befehl>

Den Befehl in die Warteschlange für die aktuelle Gegenstelle einfügen.
Jede Gegenstelle hat ihre eigene Warteschlange für Befehle.
»-n« fügt den Befehl vor dem angegebenen Eintrag in die Warteschlange ein. Es
ist möglich einen bereits laufenden Job wieder in die Warteschlange zu stellen
mit »queue wait <Jobnr.>«.

        queue --delete|-d <Index oder Joker-Ausdruck>

Ein oder mehrere Einträge in der Warteschlange löschen. Ohne genaue Angabe
wird der letzte Eintrag entfernt.

        queue --move|-m <Index oder Joker-Ausdruck> [Index]

Die angegebenen Einträge vor den angegebenen Warteschlangen-Index stellen,
oder an das Ende, wenn kein Ziel angegeben wurde.

Optionen:
 -q                   wenige Ausgaben
 -v                   ausführliche Ausgaben
 -Q                   Ausgabe in einem Format, das zum Wiedereinreihen in
                      die Warteschlange verwendet werden kann.
                      Nützlich in Verbindung mit --delete.
 
Das angegebene nichtlokale Verzeichnis in ein lokales Verzeichnis spiegeln

 -c, --continue         mit dem Spiegeln fortfahren, wenn möglich
 -e, --delete           Dateien die nicht auf dem entfernten Verzeichnis 
                        liegen löschen
     --delete-first     alte Dateien vor der Übertragung neuer Dateien löschen
 -s, --allow-suid       SUID/SGID Berechtigungen setzen wie auf dem
                        Server
     --allow-chown      wenn möglich Benutzer und Gruppe setzen
 -n, --only-newer       nur neuere Dateien herunterladen (schließt -c aus)
 -r, --no-recursion     Unterverzeichnisse nicht mit einschließen
 -p, --no-perms         keine Dateiberechtigungen setzen
     --no-umask         umask nicht auf Dateiattribute anwenden
 -R, --reverse          umgedrehter Spiegel (Dateien schicken)
 -L, --dereference      symbolische Links als Files herunterladen
 -N, --newer-than DATEI nur Dateien die neuer sind als DATEI herunterladen
 -P, --parallel[=N]     N Dateien gleichzeitig herunterladen
 -i RA, --include RA    zum regulären Ausdruck passende Dateien einschließen
 -x RA, --exclude RA    zum regulären Ausdruck passende Dateien
                        ausschließen
                        RA sind erweiterte reguläre Ausdrücke (Joker)
 -v, --verbose[=N]      mehr Informationen ausgeben
     --log=DATEI        ausgeführte lftp Befehle in DATEI protokollieren
     --script=DATEI     lftp Befehle in DATEI protkollieren, aber nicht
                        ausführen
     --jusz.print, --dry-run    entspricht »--script=-«

Gibt man -R an, so ist das erste Verzeichnis lokal und das andere auf dem 
Gegenstelle.
Wird das zweite Verzeichnis nicht angegeben, wird die Namenswurzel(basename)
des ersten Verzeichnisses verwendet.
Werden beide Verzeichnisse weggelassen, so werden das aktuelle lokale und
nichtlokale Verzeichnis verwendet.
 
lftp wird jetzt die Shell austricksen, um sich selbst zur Gruppe der Hintergrundprozesse zuzuordnen.
lftp wird, trotz der »Gestoppt-Meldung«, im Hintergrund weiterlaufen.
lftp wird sich, wenn alle Aufgaben erledigt sind, automatisch beenden.
Benutzen Sie den Kommandozeilenbefehl »fg«, um zu lftp zurückzukehren, wenn es noch läuft.
  - Protokoll nicht unterstützt  -w <Datei> Verlauf in eine Datei schreiben.
 -r <Datei> Verlauf aus einer Datei lesen, an aktuellen Verlauf anhängen.
 -c  Verlauf löschen.
 -l  Verlauf anzeigen (Vorgabe).
Das entbehrliche Argument »Zahl« gibt die Anzahl der aufzulistenden
Verlaufszeilen an. »all« gibt alle Einträge aus.
  [zwischengespeichert] !<Shell-Befehl> %.0f B/s %.0fb/s %.1f KiB/s %.1fK/s %.2f MiB/s %.2fM/s %d  Adresse$|n$ gefunden %d Datei$|s$ gefunden %d Datei$|s$ gefunden, derzeit wird %s durchsucht %ld $#l#Byte|Bytes$ zwischengespeichert %lld $#ll#Byte|Bytes$ übertragen %lld $#ll#Byte|Bytes$ übertragen in %ld $#l#Sekunde|Sekunden$ %s (Filter) %s fehlgeschlagen für %d von %d Verzeichnis$|en$
 %s fehlgeschlagen für %d von %d Datei$|en$
 %s ist ein eingebauter alias für %s
 %s ist ein alias für »%s«
 %s muss eins sein aus:  %s muss Teil von: %s sein %s OK, %d Verzeichnis$|se$ angelegt
 %s OK, %d Verzeichnis$|se$ entfernt
 %s OK, %d Datei$|en$ gelöscht
 %s OK, »%s« erzeugt
 %s OK, »%s« gelöscht
 %s%d Fehler gefunden
 %s: %d - kein derartiger Job
 %s: %s - keine derartige Sitzung zwischengespeichert. »scache« zeigt eine Liste.
 %s: %s - keine Zahl
 %s: %s. »set -a« zeigt alle Variablen.
 %s: %s: Datei existiert bereits und xfer:clobber (=überschreiben) ist nicht eingeschaltet
 %s: %s: Keine Dateien gefunden
 %s: --teile Konflikte mit --Ausgabeverzeichnis.
 %s: -m: Zahl als zweite Angabe erwartet.  %s: -n: Zahl erwartet.  %s: -n: positive Zahl erwartet.  %s: Fehler - Schleife (deadlock) entdeckt
 %s: GetPass() fehlgeschlagen -- benutze anonymen Login
 %s: Keine Warteschlange aktiv.
 %s: Angabe für »expire«(Ablauf) fehlt
 %s: Größenangabe fehlt
 %s: Bitte geben Sie die zu teilende Datei oder das zu teilende Verzeichnis an.
 %s: Bitte geben Sie eine Meta-info-Datei oder eine URL an.
 %s: mehrdeutiges Quell-Verzeichnis (»%s« oder »%s«?)
 %s: mehrdeutiges Ziel-Verzeichnis (»%s« oder »%s«?)
 %s: Argument vorgeschrieben.  %s: Name des Lesezeichens notwendig
 %s: Es kann keine lokale Sitzung angelegt werden
 %s: Befehl »%s« wurde nicht mitkompiliert.
 %s: Datums-/Zeitanalyse-Fehler
 %s: Datums-/Zeitangabe fehlt
 %s: Importtyp muss angegeben werden (Netscape,ncftp)
 %s: unzulässige Blockgrösse »%s«
 %s: Ungültige Option -- »%c«
 %s: - kein derartiger Job
 %s: kein vorheriges Verzeichnis an diesem Ort
 %s: kein derartiges Lesezeichen »%s«
 %s: Die Option »%c%s« erlaubt kein Argument
 %s: Option »%s« ist mehrdeutig
 %s: Option »%s« ist mehrdeutig; Möglichkeiten: %s: Die Option »--%s« erlaubt kein Argument
 %s: Die Option »--%s« benötigt ein Argument
 %s: Die Option »-W %s« erlaubt kein Argument
 %s: Die Option »-W %s« ist mehrdeutig
 %s: Option »-W %s« benötigt ein Argument
 %s: Diese Option benötigt ein Argument -- »%c«
 %s: Umleitung nach »%s« empfangen
 %s: Regulärer Ausdruck (regular expression) %s: %s
 %s: ein anderer Job wartet auf die Beendigung des Jobs %d
 %s: Quellverzeichnis erforderlich (mirror:require-source is set)
 %s: Leerstellen sind im Namen von Lesezeichen nicht zulässig
 %s: »summarize« kann nicht mit --max-depth=%i kombiniert werden
 %s: Unbekannte Option »%c%s«
 %s: Unbekannte Option »--%s«
 %s: Warteschleife entdeckt
 %s: Achtung: »summarize« (Zusammenzählen) entspricht --max-depth=0
 %sVerändert: %d Datei$|en$, %d Symlink$|s$
 %sNeu: %d Datei$|en$, %d Verknüpfungen$|s$
 %sGelöscht: %d Verzeichnis$|sse$, %d Datei$|en$, %d Symlink$|s$
 %sGesamt: %d Verzeichni$s|sse$, %d Datei$|en$, %d Verknüpfungen$|s$
 « (Befehle) **** FXP: gebe auf, kehre zu einfachem copy zurück
 **** FXP: Versuche ftp:fxp-passive-source umzukehren
 **** FXP: Versuche ftp:fxp-passive-sscn umzukehren
 **** FXP: Versuche ftp:ssl-protect-fxp umzukehren
 , Maximalgröße %ld
 , keine Größenbegrenzung , oder leer <2 >1 Verbindung von [%s]:%d angenommen Datenverbindung von (%s) Port %u akzeptiert Zugriff nicht möglich:  Eingerichteter Zugang ist erforderlich, ftp:acct Variable setzen Auftrag$|Aufträge$ hinzugefügt Mehrdeutiger Befehl »%s«.
 Zweideutiger Befehl »%s«. »help« zeigt alle verfügbaren Befehle.
 Mehrdeutiger Befehl.  Das Terminal einem angegebenen, im Hintergrund laufenden lftp-Prozess zuordnen.
 Es kann kein Socket für den torrent:port-Bereich gebunden werden Wechsle das geltende lokale Verzeichnis zu <lverz>. Das letzte lokale Verzeichnis
wird als »-« gespeichert. Man kommt mit »lcd -« wieder dorthin zurück.
 Wechsle in das nichtlokale Verzeichnis. Das vorherige Verzeichnis auf dem anderen
Rechner wird unter »-« gespeichert. Man kann mit »cd -« wieder zurückgehen.
Das vorherige Verzeichnis für jeden Server wird auch auf Festplatte gesichert, so
dass man auch bei einem Neustart von Lftp »open Server; cd -« benutzen kann.
 Den Modus jeder Datei auf MODUS ändern.

 -c, --changes        - entspricht »verbose«, aber meldet nur Änderungen
 -f, --quiet          - die meisten Fehlermeldungen abschalten
 -v, --verbose        - Bericht für jede bearbeitete Datei ausgeben
 -R, --recursive      - Dateien und Verzeichnisse rekursiv ändern

Der MODUS kann binär oder symbolisch angegeben werden (vgl. chmod(1))
 Wechsle nichtlokales Verzeichnis... Schließe unbenutzte Verbindungen. Standardmäßig nur mit dem aktuellen Server.
 -a  unbenutzte Verbindungen auf allen Servern schließen
 Schließe HTTP-Verbindung Schließe die Datenverbindung Schließe den Kontroll - Socket Schließe den Daten Socket Schließe unbenutzte Verbindung Befehle in Warteschlange: Verbunden Verbinde Daten Socket mit (%s) Port %u Datenverbindung mit Proxy %s (%s) Port %u Verbinde mit %s%s (%s) Port %u Verbindung zu Peer %s Port %u wird hergestellt Verbinde... Verbindung ruht Zahl der Verbindungen hat Grenze erreicht Konnte HTTP Statusanzeige nicht auswerten Angehaltene Warteschlange erzeugt.
 Der DNS-Auflösung wird nicht vertraut. Datenverbindung hergestellt Data connection peer hat nicht übereinstimmende Adresse Data connection peer hat die falsche Port Nummer Definiert oder löscht alias <Name>. Wenn <Wert> fehlt,
wird der Alias gelöscht, sonst nimmt er den Wert <Wert> an.
Wird kein Argument angegeben, so werden die bestehenden Aliase aufgelistet.
 Pausiere vor erneuter Verbindung Pausiere vor erneuter Verbindung Löscht den angegebenen Job mit <job_nr.> oder alle Jobs
 Auftrag$|Aufträge$ gelöscht Fertig Führe die in der Datei gespeicherten Befehle aus
 Führt den Befehl auf dem Host aus und gibt das Resultat aus
Ausgabeumleitung ist möglich
 Platzhalter (Joker) ausfüllen und angegebenen Befehl ausführen.
Optionen können die Joker auf Verzeichnisse, Dateien oder beides erstrecken.
Die Unterscheidung der Typen ist unzuverlässig und hängt von der Gegenstelle ab.
Wenn keine Unterscheidung getroffen werden kann, wird ein  Eintrag im 
Zweifel in die Liste aufgenommen.
  -f  einfache Dateien (Standardwert)
  -d  Verzeichnisse
  -a  alle Einträge
 FEAT Verbindungsaufbau... Konnte den Modus von »%s« nicht ändern, da kein alter Modus zugänglich war.
 Konnte Modus nicht von »%s« zu %04o (%s) ändern.
 Fataler Fehler Hole Kopfzeilen... Zugriff auf Datei nicht möglich Datei verschoben Datei verschoben nach » Dateiname fehlt.  Beendet %s Findet ausgewählte Dateien mit erweiterten Platzhaltern
 -c  fortsetzen, Übertragung wieder aufnehmen
 -d  erstelle Verzeichnisse genauso wie in Dateinamen und speichere 
    Dateien dort statt im aktiven Verzeichnis
 -E  lösche entfernte Dateien nach erfolgreicher Übertragung
 -a  verwende ASCII-Modus (Binär-Modus ist Vorgabe)
 -O <base> bestimmt das Hauptverzeichnis oder die URL als Speicherort der Dateien
 Holt die angegebene Datei über mehrere Verbindungen gleichzeitig
Dies kann die Übertragung beschleunigen, aber führt zu hoher Netzlast,
die andere Benutzer beeinträchtigt. Nur verwenden, wenn eine Datei wirklich
SOFORT übertragen werden muß.)

Optionen:
 -c  Übertragung fortsetzen. Erfordert <lfile>.lftp-pget-status Datei.
 -n <maxconn>  Höchstzahl an Verbindungen angeben
     (Vorgabe aus pget:default-n setting)
 -O <base> Verzeichnis angeben, in den Dateien abgelegt werden
 Lese Verzeichnisinhalt aus Hole Dateiliste (%lld) [%s] Lese Dateiinformationen Metadaten werden abgerufen: %s Befehle zu Gruppen zusammenfassen um sie mit einem Kommando auszuführen
Es ist möglich derartige Gruppen im Hintergrund laufen zu lassen
 Händeschütteln … Dateiende (EOF) angetroffen Dateiende beim Lesen der Kopfzeilen erhalten Nachschlagen des Hostnamen fehlgeschlagen Abbruch Ungültige IPv4 Adressierungsnummer Ungültige IPv6 Adressierungsnummer Ungültiger Befehl.  Ungültige Angabe der Spanne. Das Format ist min-max - z.B. 10-20. Ungültige Zeitangabe. Das Format ist <Zeit><Einheit> - z.B. 2h30m. Ungültige Zeiteinheit, nur [smhd] sind zulässig. LFTP ist freie Software. Sie können es unter den Bedingungen der GNU 
General Public License, wie von der Free Software Foundation veröffentlicht, 
weitergeben und/oder modifizieren, entweder gemäß Version 3 der Lizenz oder (nach Ihrer Wahl) 
jeder späteren Version.

Die Veröffentlichung dieses Programms erfolgt in der Hoffnung, dass es Ihnen von Nutzen
sein wird, aber OHNE IRGENDEINE GARANTIE, sogar ohne die implizite Garantie 
der MARKTREIFE oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK. 
Einzelheiten finden Sie in der GNU General Public License.

Sie sollten ein Exemplar der GNU General Public License zusammen mit LFTP erhalten haben. 
Falls nicht, finden Sie es unter <http://www.gnu.org/licenses/>.
 LFTP | Version %s | Copyright (c) 1996-%d Alexander V. Lukyanov
 Starte Shell oder Shell Befehl
 Verwendete Libraries:  <Datei1> mit <Datei2> verknüpfen
 Zeigt gespeicherte Sitzungen oder wechselt zur angegebenen Nummer
 Nichtlokale Dateien auflisten.
Als Standardvorgabe, wird die Ausgabe zwischengespeichert, um die Liste zu erneuern
dient entweder »rels« oder »cache flush«.
 Anzeige von entfernten Dateien. Die Ausgabe dieses Befehls kann in eine Datei 
oder mittels Pipe an einen externen Befehl übergeben werden.

 -1                   - einspaltige Ausgabe
 -a, --all            - versteckte Dateien anzeigen
 -B, --basename       - nur Basisnamen anzeigen
     --block-size=SIZ - SIZ-Byte Blöcke verwenden
 -d, --directory      - Verzeichniseinträge statt Inhalte anzeigen
 -F, --classify       - Nummer an Einträge anhängen (eins von /@)
 -h, --human-readable - Größenangaben in Klarschrift (z.B. 1K)
     --si             - wie oben, jedoch mit Basis 1000 statt 1024
 -k, --kilobytes      - wie oben --Blockgröße=1024
 -l, --long           - ausführliches Listenformat verwenden
 -q, --quiet          - kein Status anzeigen
 -s, --size           - Größenangabe für jede Datei anzeigen
     --filesize       - Druckgröße nur für Dateien anzeigen
 -i, --nocase         - groß- oder kleinschreibungsunabhängige Übereinstimmungsmuster
 -I, --sortnocase     - groß- oder kleinschreibungsunabhängige Namenssortierung
 -D, --dirsfirst      - Verzeichnisse zuerst anzeigen
     --sort=OPT       - »Name«, »Größe«, »Datum«
 -S                   - Sortierung nach Dateigröße
 --user, --group, --perms, --date, --linkcount, --links
                      - show individual fields
 --time-style=STYLE   - definiertes Zeitformat verwenden

Standardmäßig wird die CLS-Ausgabe zwischengespeichert. eine neue Liste kann mit `recls' oder
`cache flush' angezeigt werden.

Die Variablen cls-default und cls-completion-default können verwendet werden,
um Standardvorgaben für CLS-Listen bzw. Vervollständigungslisten zu definieren.
Wenn  z.B. Vervollständigungslisten die Dateigrößen zeigen sollen, 
ergänzen Sie cls-completion-default mit "-s".

Tipps: Verwenden Sie --filesize mit Option -D für eine übersichtlichere Anzeige.  Wenn keine
ständige Dateigrößenanzeige gewünscht wird, --filesize bei cls-default beeinflusst das -s Flag
auch mittels Kommandozeile.  Ein hinzugefügtes `-i' zur cls-completion-default macht die 
Dateinamenergänzung groß- oder kleinschreibungsunabhängig.
 Nichtlokale Dateien auflisten. Man kann die Ausgabe dieses Befehls in
eine Datei oder mittels einer Pipe zu einem externen Befehl umleiten.
Als Standardvorgabe, wird die Ausgabe zwischengespeichert, um die Liste zu erneuern
dient entweder »rels« oder »cache flush«.
Siehe auch »help cls«.
 Zeige laufende Vorgänge. -v bedeutet ausführliche Meldungen, einige -v können definiert werden.
Wenn <job_no> angegeben ist, wird nur der Vorgang mit dieser Nummer angezeigt.
 Lade Modul (shared object). Das Modul sollte folgende Funktion enthalten
   void module_init(int argc,const char *const *argv
Wenn der Name einen Querstrich enthält, wird das Modul im aktuellen 
Verzeichnis gesucht, andernfalls wird in den via module:path angegebenen
Verzeichnissen gesucht
 Logge ein... Login fehlgeschlagen MLSD wird abgeschaltet durch ftp:use-mlsd MLST und MLSD werden von dieser Gegenstelle nicht unterstützt Entfernte Verzeichnisse erstellen
 -p  alle Pfadebenen erstellen
 -f  keine Anzeige, Meldungen unterdrücken
 Stelle Datenverbindung her... Lege Verzeichnis »%s« an Erzeuge symbolische Verknüpfung »%s« nach »%s« Spiegele Verzeichnis »%s« Modus von »%s«  nach %04o (%s) geändert.
 Das Modul für den Befehl »%s« hat den Befehl nicht registriert.
 Job$|s$ verschoben Keine Adresse gefunden Kein Auftrag #%i in der Warteschlange.
 Kein mit »%s« übereinstimmender Auftrag gefunden.
 Keine Aufträge in der Warteschlange.
 Kein derartiger Befehl »%s«. »help« zeigt alle verfügbaren Befehle.
 Keine Verbindung Momentan läuft: Objekt ist nicht zwischengespeichert und http:cache-control bestimmt only-if-cached (nur mit Zwischenspeicherung) Bestehendes Verzeichnis »%s« wird nicht entfernt Bestehende Datei »%s« wird nicht entfernt Operation wird nicht unterstützt Alte Datei `%s' wird überschrieben POST - Methode fehlgeschlagen Passwort:  Gegenüber hat Verbindung beendet Verbleibe und versuche es weiter Drucke momentane URL der Gegenstelle.
 -p  Passwort zeigen
 Zeige die Hilfe für den Befehl <cmd>, sonst zeige verfügbare Befehle
 Proxy Protokoll wird nicht unterstützt Warteschlange wurde angehalten. alles empfangen Alles empfangen (insgesamt) Letztes Teil empfangen Nicht genug Daten erhalten, neuer Versuch Gültige Info über %d IPv6 peer$|s$ erhalten Gültige Info über %d peer$|s$ empfangen Empfange BODY... Empfange Daten Empfange Daten/TLS Entferne nichtlokale Verzeichnisse
 Lösche nichtlokale Dateien
 -r  rekursive Entfernung von Verzeichnissen, Vorsicht
 -f  ohne jegliche Rückmeldung löschen
 Löscht die mit Platzhaltern(Joker) ausgewählten Dateien
 Entferne alte Datei »%s« Entferne alte Datei »%s« Entferne alte lokale Datei »%s« Quelldatei wird gelöscht »%s« <datei1> in <datei2> umbenennen
 Angegebenen Befehl mit Verzögerung wiederholen.
Voreingestellte Verzögerung ist eine Sekunde, voreingestellter Befehl ist leer.
 -c <Anzahl>  Maximale Zahl an Wiederholungen.
 -d <Verzögerung>  Verzögerung zwischen Wiederholungen.
 --while-ok  Anhalten, wenn Befehl mit Nicht-Null-Code beendet wird.
 --until-ok  Anhalten, wenn Befehl mit Null-Code beendet wird.
 --weak      Anhalten, wenn lftp in den Hintergrund verschoben wird.
 Löse Hostadresse auf... Entfernte Datei <rfile> empfangen und lokal speichern als <lfile>.
 -o <lfile> bestimmt den lokalen Dateinamen (Vorgabe – Ursprungsdateiname von rfile)
 -c  fortsetzen, Übertragung wieder aufnehmen
 -E  lösche entfernte Dateien nach erfolgreicher Übertragung
 -a  benutze ASCII-Modus (Vorgabe ist Binär-Modus)
 -O <base> bestimmt das Hauptverzeichnis bzw. eine URL als Speicherort
 Datei herunterladen, mit lokalem Editor bearbeiten
und die Datei hochladen, sofern geändert.
 -k  aktuelle Datei behalten
 -o <temp>  eindeutiger Zwischenspeicherort
 Verbindungsaufbau zum Spiegelserver wird wiederholt …
 Starte Verbindungsprogramm SITE CHMOD abgeschaltet durch ftp:use-site-chmod SITE CHMOD wird von dieser Gegenstelle nicht unterstützt Entspricht `cat <dateien> | more'. Wenn PAGER gesetzt ist, wird dieser als Filter benutzt.
 Entspricht cat, aber filtert jede Datei durch bzcat
 Entspricht cat, aber filtert jede Datei durch zcat
 Entspricht more, aber benutzt bzcat als Filter
 Entspricht more, aber benutzt zcat als Filter
 Verzeichnis wird durchsucht »%s« Sähen im Hintergrund …
 Server, URL oder Lesezeichen auswählen
 -e <Befehl>         <Befehl> sofort nach der Auswahl ausführen
 -u <user>[,<pass>]  angegebenen Benutzer und Passwort zu Authentifikation verwenden
 -p <Port>           angegebenen Port zur Verbindung benutzen
 -s <slot>           angegebenen Slot zur Verbindung benutzen
 <site>              Name eines Hosts, URL oder Lesezeichen
 Senden Sie Fehlerberichte und Fragen bitte an die Mailing-Liste <%s>.
 Den angegebenen Befehl ohne Interpretation/Bearbeitung schicken. Mit
Vorsicht zu genießen, da dies zu einem unbekanntem Zustand auf dem anderen Rechner
und zum Verbindungsneuaufbau führen kann. Man sollte sich nicht auf den Zustand
des nichtlokalen Rechners verlassen, da das geschickte Kommando jederzeit durch einen
Verbindungsneuaufbau überschrieben werden kann.
 Schicke Befehle... Sende Daten Sende Daten/TLS Schicke Anforderung... Server Antwort entspricht ftp:retry-530, neuer Versuch Server Antwort entspricht ftp:retry-530-anonymous, neuer Versuch Fehlerbeseitigungsebene auf Vorgabewert setzen oder komplett abschalten.
 -o <file>  Ausgabe in die Datei umleiten
 -c  zeige Meldungszusammenhang
 -p  zeige PID
 -t  zeige Zeitstempel
 Variable auf angegebenen Wert setzen. Wenn die Wertangabe ausgelassen wird,
wird die Variable gelöscht.
Der Variablenname hat das Format »name/closure«, wobei closure die genaue
Anwendung der Einstellung angibt. Details finden sich unter lftp(1).
Wenn set ohne Angabe einer Variablen aufgerufen wird, dann werden nur die
veränderten Werte angezeigt.
Folgende Optionen ändern dies Verhalten:
 -a  zeigt alle Variablen einschließlich der unveränderten Standardvorgaben
 -d  zeigt nur die Standardvorgaben, nicht unbedingt die aktuellen Werte.
 Gibt die Lftp - Version an
 Wird beendet:  Lege Verzeichnis »%s« an (only-existing) Überspringe Datei »%s« (only-existing) Überspringe Verknüpfung »%s« (only-existing) Verbleibende Zeit im Ruhezustand:  Ständiger Ruhezustand Socket Fehler (%s) - erneutes Verbinden Tut mir Leid, keine Hilfe für %s
 Speichern fehlgeschlagen - erneutes Hochladen notwendig Plattenspeicherverbrauch anzeigen.
 -a, --all             Angaben für alle Dateien und nicht nur Verzeichnisse
                       ausgeben.
     --block-size=GRÖßE
                       Blocks der angegebenen GRÖßE in Bytes verwenden
 -b, --bytes           Größe in Bytes ausgeben
 -c, --total           Gesamtsumme des Speicherbedarfs ausgeben
 -d, --max-depth=N     Summe des Speicherbedarfs für ein Verzeichnis
                       angeben (oder für eine Datei mit --all) soweit es
                       N oder weniger Ebenen unter dem Befehlsargument liegt;
                       --max-depth=0 entspricht --summarize
 -F  --files           Anzahl der Dateien statt der Größe angeben
 -h, --human-readable  Größen in verständlichem Format ausgeben.
                       (z.B. 1K 234M 2G)
 -H, --si              ähnlich, aber verwendet Vielfache von 1000 nicht 1024
 -k, --kilobytes       wie --block-size=1024
 -m, --megabytes       wie --block-size=1048576
 -S, --separate-dirs   Größe der Unterverzeichnisse nicht mit einrechnen
 -s, --summarize       Nur die Summe für jedes Argument angeben
     --exclude=MUSTER  Dateien die MUSTER entsprechen nicht mitzählen
 Schalte Passivmodus ab Schalte Passivmodus ein Schalte NOREST - Modus ein TLS Verbindungsaufbau... Jobs laufen noch und »cmd:move-background« ist nicht eingeschaltet.
Bitte »exit bg« verwenden um im Hintergrund fortzufahren oder
»kill all« um alle Jobs zu beenden.
 Zeitablauf - neu verbinden Zu viele Umleitungen Insgesamt %d $Datei|Dateien$ übertragen
 Übertragung von %d der %d $Datei|Dateien$ fehlgeschlagen
 Sende Datei »%s« »%s --help' gibt weitere Informationen aus
 »help %s« gibt mehr Informationen aus.
 sync - Modus eingeschaltet Unbekannter Befehl »%s«.
 Unbekannter Systemfehler Lade <ldatei> hoch unter dem Namen <nldatei>.
 -o <nldatei> gibt den Namen der nichtlokalen Datei an. 
    (Vorgabe - Name von ldatei)
 -c  nach Abbruch fortfahren, erneut schicken
     Man braucht die Berechtigung Dateien auf dem Server zu überschreiben.
 -e  Lokal vorliegende Dateien nach erfolgreicher Übertragung löschen 
    (Vorsicht!! Riskant!)
 -a  ASCII / Textmodus verwenden (Binärmodus ist die Vorgabe)
 -O <basis> gibt das Oberverzeichnis bzw. die URL an, wo die Dateien
    gespeichert werden sollen.
 Speichert mittels Platzhaltern (Joker) ausgewählte Dateien auf dem Server
 -c  nach Abbruch fortfahren, erneut hochladen
 -d  Verzeichnisse mit dem Namen der ausgewählten Dateien erstellen und
     die Dateien dorthin hochladen statt im normalen Verzeichnis
 -e  lokale Dateien nach erfolgreicher Übertragung löschen (Vorsicht!!)
 -a  ASCII / Textmodus verwenden (Binärmodus ist die Vorgabe)
 -O  <basis> gibt das Oberverzeichnis bzw. die URL an, wo die Dateien
     abgelegt werden sollen.
 Benutzung: %s
 Benutzung: %s %s [-f] Dateien...
 Benutzung: %s <Befehl>
 Benutzung: %s <jobnr.> ... | all
 Verwendung: %s user <user|URL> [<passwt>]
 Benutzung: %s [-d #] dir
 Benutzung: %s [-e Befehl] [-p Port] [-u user[,passwort]] <Host|URL>
 Benutzung: %s  [-e] <datei|befehl>
 Benutzung: %s [-p]
 Benutzung: %s [-v] [-v] ...
 Benutzung:%s [<Exit_kode>]
 Benutzung: %s [<Jobnr.>]
 Benutzung: %s Befehl Argumente..
 Benutzung: %s [OPTS] <Datei>
 Benutzung: %s [OPTS] Dateien ...
 Benutzung: %s [OPTS] Modus Dateien ...
 Benutzung: %s [Optionen] <verzeichnisse>
 Aufruf: %s Befehl [Argumente…]
 Benutzung: %s Befehl Argumente..
 Benutzung: %s lokales-Verzeichnis
 Benutzung: %s module [Argumente..]
 Benutzung: cd nichtlokales-Verzeichnis
 Verwendung: find [OPTIONEN] [Verzeichnis]
Gibt rekursiv den Inhalt des angegebenen oder des aktuellen Verzeichnisses 
aus. Verzeichnisse werden in der Liste durch einen angehängten Schrägstrich 
(/) gekennzeichnet. Man kann die Ausgabe dieses Befehls umleiten.
       -d, --maxdepth=EBENEN Maximal bis zu EBENEN tief in Verzeichnisstruktur
        eindringen
 Benutzung: mv <datei1> <datei2>
 Verwendung: reget [OPTIONEN] <nichtlokale Datei> [-o <lokale Datei>]
Identisch zu »get -c«
 Verwendung: rels [<Argumente>]
Entspricht »ls«, aber schaut nicht in den Cache
 Verwendung: renlist [<Argumente]
Entspricht »nlist«, aber schaut nicht in den Cache
 Verwendung: reput <lokale Datei> [-o <nichtlokale Datei>]
Dasselbe wie »put -c«
 Verwendung: sleep <Zeitdauer>[Zeiteinheit]
Schlafe für die angegebene Zeitdauer. Das Zeitargument kann mit der Angabe 
einer Einheit ergänzt werden: d - Tage, h - Stunden, m - Minuten, s - Sekunden.
Ohne Angabe wird die Zeitangabe als Sekunden interpretiert.
 Verwendung: slot [<label>]
Zugewiesene Slots anzeigen.
Wenn <label> angegeben wird, zum Slot namens <label> wechseln.
 Angegebene Info für das Einloggen verwenden. Wird eine URL angegeben,
so wird das Passwort für spätere Verwendung zwischengespeichert.
 Gültige Argumente sind: Überprüfung: %u/%u (%u%%) %s%s Überprüfung des Befehls fehlgeschlagen ohne Rückmeldung Überprüfen … Darauf warten, dass der angegebene Job beendet ist. Wird keine Jobnr. angegeben,
wartet lftp auf den letzten in den Hintergrund gestellten Job.
 Warte auf Beendung von LTS... Warte auf Datenverbindung... Es wird auf Meta-Daten gewartet … Warte auf die Gegenstelle für die andere Kopie... Warte auf Antwort... Warte auf Übertragungsende Warnung: chdir(%s) fehlgeschlagen: %s
 Warnung: unvollständiger Befehl verworfen
 [%d] Fertig (%s) [%u] An Terminal %s gebunden. %s
 [%u] An Terminal gebunden.
 [%u] Vom Terminal gelöst. %s
 [%u] Wird vom Terminal gelöst, um die Übertragungen abzuschließen …
 [%u] Wird beendet und vom Terminal gelöst.
 [%u] Abgeschlossen. %s
 [%u] Wird zum Abschluss der Übertragungen in den Hintergrund verschoben …
 [%u] Gestartet.  %s
 [%u] Beendet durch Signal %d. %s
 [re]cls [Optionen] [Pfad/][Muster] [re]nlist [<args>] » »%s« bei %lld %s%s%s%s »%s«, empfange %lld von %lld (%d%%) %s %s »lftp« ist der erste Befehl, der von lftp nach den rc-Dateien ausgeführt wird.
 -f <Datei>          Befehle aus dieser Datei ausführen und beenden
 -c <Befehl>         Befehle ausführen und beenden
Die anderen Optionen sind die selben wie beim »open« Befehl
 -e <Befehl>         Den Befehl direkt nach der Auswahl ausführen
 -u <user>[,<pass>]  Benutze Benutzer und Passwort zur Authentifikation
 -p <Port>           Benutze den angegebenen Port zur Verbindung
 <site>              Name des Hosts, URL oder Name eines Lesezeichen
 alias [<Name> [<Wert>]] mehrdeutiges Argument  %s für %s Mehrdeutiger Name für Variable angekündigt über  anon - anonymes Login (Vorgabe)
 nehme fehlgeschlagene Auflösung des Hostnamens an bookmark [UNTERBEFEHL] Der bookmark Befehl verwaltet die Lesezeichen

Die folgenden Unterbefehle werden erkannt:
  add <Name> [<Ort>] - momentanen Verzeichnis oder angegebenes Verzeichnis
                       als Lesezeichen ablegen und dem Namen zuordnen
  del <Name>         - Lesezeichen diesen Namens entfernen
  edit               - Lesezeichendatei mit Editor bearbeiten
  import <Typ>       - fremdformatige Lesezeichen einlesen
  list               - Lesezeichen auflisten (Vorgabe)
 cache [UNTERBEFEHL] Der cache Befehl kontrolliert den lokalen Zwischenspeiche (Cache)

Die folgenden Unterbefehle werden erkannt:
  stat        - Status des Cache ausgeben (Vorgabe)
  on|off      - Cache an- oder ausschalten
  flush       - Cache leeren
  size <Lim>  - Speichergrenze setzen
  expire <Nx> - Ablaufzeit des Cache setzen auf N Sekunden (x=s)
                Minuten (x=m), Stunden (x=h) oder Tage (x=d)
 Kann kein Socket für die Adressfamilie %d anlegen Kann aktuelles Verzeichnis nicht feststellen Kann EPSV Antwort nicht auswerten Kann Datenquelle nicht durchsuchen cat - nichtlokale Dateien an STDOUT ausgeben. (Kann umgeleitet werden)
 -b Binärmodus verwenden (Textmodus ASCII ist der Standardwert)
 cat [-b] <Dateien> cd <verzeichnis> Verzeichniswechsel OK, cwd=%s
 chdir(%s) fehlgeschlagen: %s
 chmod [OPTIONEN] modus Datei... Blockformatierung verletzt copy: Alle Daten empfangen, aber get wurde zurückgesetzt
 copy: Zieldatei ist bereits vollständig
 copy: get wurde zurückgesetzt zu %lld, suche dementsprechendes put
 copy: put funktioniert nicht
 copy: put kehrte zu %lld zurück, suche entsprechendes get
 copy: Umleitung nach »%s« empfangen
 Fehlerbeseitigung [OPTS] [<level>|off] Debug ist abgeschaltet
 Debug Stufe ist %d, Ausgabe geht nach %s
 Voraussetzung Modul »%s«: %s
 du [Optionen] <verzeichnisse> bearbeite [OPTS] <file> ca. fertig: execl(/bin/sh) fehlgeschlagen: %s
 execlp(%s) fehlgeschlagen: %s
 execvp(%s) fehlgeschlagen: %s
 exit - lftp verlassen oder in den Hintergrund schicken, falls noch Jobs laufen

Wenn keine Jobs aktiv sind, wird der code als Beendigungsstatus an
das Betriebssystem weitergegeben. Ohne Angabe wird der Status des letzten
Befehls benutzt.
 exit [<code>|bg] zusätzliche Server Antwort Dateiname fehlt in der URL Dateigröße ist während des Transfers gesunken FTP über HTTP funktioniert nur mit Proxy, hftp:proxy Option angeben. ftp:fxp-force ist eingeschaltet, aber FXP ist nicht verfügbar ftp:proxy Passwort:  ftp:skey-force ist gesetzt und Gegenstelle unterstützt weder OPIE noch S/KEY ftp:ssl-force ist gesetzt und Gegenstelle unterstützt oder erlaubt kein SSL get [OPTS] <nldatei> [-o <ldatei> glob [OPTIONEN] <Befehle> <Argumente> help [<Befehl>] history -w Datei|-r Datei|-c|-l [Zahl] Zeitablauf: Rechnernamen nicht aufgelöst Integer-Überlauf ungültiges Argument %s für »%s« ungültiges Argument für »--sort« Unzulässige Blockgrösse unzulässiger boolscher Wert unzulässiger boolscher/automatischer Wert Unzulässige Gleitkommazahl Ungültige Modusangabe: %s
 Unzulässige Zahl Ungültiges Peer-Antwortformat unzulässiges Format der Serverantwort Ungültige Zahl ohne Vorzeichen kill all|<job_nr.> lcd <lverz> lcd OK, lokales cwd=%s
 lftp [OPTS] <site> ln [-s] <Datei1> <Datei2> ls [<args>] max-retries überschritten Speicher erschöpft mget [OPTS] <Dateien> mirror [OPTS] [nichtlokal [lokal]] mirror: Protokoll »%s« ist nicht für das spiegeln geeignet
 module name [argumente] Module sind auf diesem System nicht verwendbar more <Dateien> mput [OPTS] <Dateien> mrm <dateien> muss eins sein aus:  mv <datei1> <datei2> nächste Meldung in %s Nächste Anfrage in %s kein Gültigkeitsbereich (closure) für diese Einstellung definiert kein derartiger %s Dienst keine derartige Variable Parameter gefunden, die keine Option sind nur PUT und POST Werte sind zulässig open [OPTS] <site> parse: Filterbefehl fehlt
 parse: Dateiname für Umleitung fehlt
 Der Peer hat die Verbindung beendet Der Peer hat die Verbindung beendet (vor dem Händeschütteln) Der Peer hat nur die akzeptierte Verbindung beendet Zeitüberschreitung beim Peer-Händeschütteln Peer sendete unbekannten info_hash=%s während Handshake Kurzes Peer-Händeschütteln Peer hat unerwartet die Verbindung unterbrochen Der Peer hat die Verbindung nach %s unerwartet beendet pget [OPTS] <nlfile> [-o <lfile>] pget: falle auf einfaches get zurück pip() fehlgeschlagen:  Allokation eines Pseudo-tty fehlgeschlagen:  put [OPTS] <lfile> [-o <nlfile>] queue [OPTIONEN] [<Befehl>] quote <cmd> recls [<Argumente>]
Entspricht »ls«, aber schaut nicht in den Zwischenspeicher
 Umbenennung erfolgreich
 repeat [OPTIONEN][Verzögerung] [Befehl] rm [-r] [-f] <dateien> mkdir [-f] <verzeichnisse> Dateigröße in Antwort empfangen scache [<sitzungs_nr.>] Suche erfolglos set [OPTION] [<variable> [<wert>]] site <site-cmd> source <datei> die Größe der Quelldatei ist unbekannt Die Zieldatei ist nicht lokal Diese Zeichenkodierung wird nicht unterstützt insgesamt unbekannte Adress Familie »%s« nicht unterstütztes Netzwerkprotokoll user <user|URL> [<passwt>] wait <jobnr.> zcat <Dateien> zmore <Dateien> 