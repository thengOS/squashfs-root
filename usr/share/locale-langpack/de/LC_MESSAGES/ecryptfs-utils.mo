��          D      l       �      �   !   �   &   �   R  �   �  >     �  )      7   *  �  b                          Access Your Private Data Record your encryption passphrase Setup Your Encrypted Private Directory To encrypt your home directory or "Private" folder, a strong passphrase has been automatically generated. Usually your directory is unlocked with your user password, but if you ever need to manually recover this directory, you will need this passphrase. Please print or write it down and store it in a safe location. If you click "Run this action now", enter your login password at the "Passphrase" prompt and you can display your randomly generated passphrase. Otherwise, you will need to run "ecryptfs-unwrap-passphrase" from the command line to retrieve and record your generated passphrase. Project-Id-Version: ecryptfs-utils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-27 00:09+0000
PO-Revision-Date: 2012-12-30 22:00+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:21+0000
X-Generator: Launchpad (build 18115)
 Zugang zu Ihren privaten Daten Ihre Verschlüsselungspassphrase notieren Erstellen Sie Ihr privates verschlüsseltes Verzeichnis Um Ihren persönlichen oder »privaten« Ordner zu verschlüsseln, wurde automatisch eine starke Passphrase erzeugt. Normalerweise wird Ihr Verzeichnis mit Ihrem Benutzerpasswort entsperrt, aber falls Sie dieses Verzeichnis einmal manuell wiederherstellen müssen, benötigen Sie diese Passphrase. Bitte drucken Sie die Phrase aus oder schreiben Sie sie auf und verfahren sie an einem sicheren Ort. Wenn Sie auf »Diese Aktion jetzt ausführen« klicken, geben Sie Ihr Anmeldepasswort im Feld »Passphrase« ein und Sie können Ihre zufällig erzeugte Passphrase anzeigen. Ansonsten müssen Sie »ecryptfs-unwrap-passphrase« von der Befehlszeile ausführen, um Ihre erzeugte Passphrase abzurufen und zu notieren. 