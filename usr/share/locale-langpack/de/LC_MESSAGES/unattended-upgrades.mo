��    3      �  G   L      h     i     �     �  "   �  "   �  '   �  -        L  j   `     �  $   �            ,     M  =   m  o   �  N     B   j     �  $   �  $   �       "   /  '   R  /   z     �  ,   �  '   �  #   	  )   8	  !   b	  ?   �	     �	  .   �	  <   
  9   J
     �
  +   �
     �
     �
     �
  +     ,   8     e  ,   y     �  ?   �             M   )  �  w     U     o  %   �  0   �  ;   �  ;     9   [     �  x   �  $   "  6   G  7   ~  !   �  )   �  L     �   O  ]   �  ^   @     �  /   �  >   �  3   +  4   _  .   �  c   �     '  3   ?  2   s  5   �  A   �  .     d   M     �  E   �  A     M   Z  +   �  G   �          /  #   G  =   k  6   �     �  C   �  2   :  �   m  $   �        R   4               !             	   0          "             -   .   &             #   /   (   +                                  
                            3                                  ,   )         1   2                       *       '       %   $    All upgrades installed Allowed origins are: %s An error occurred: '%s' Auto-removing the packages failed! Cache has broken packages, exiting Cache lock can not be acquired, exiting Download finished, but file '%s' not there?!? Error message: '%s' Found %s, but not rebooting because %s is logged in. Found %s, but not rebooting because %s are logged in. GetArchives() failed: '%s' Giving up on lockfile after %s delay Initial blacklisted packages: %s Initial whitelisted packages: %s Installing the upgrades failed! Lock could not be acquired (another package manager running?) No '/usr/bin/mail' or '/usr/sbin/sendmail',can not send mail. You probably want to install the 'mailx' package. No packages found that can be upgraded unattended and no pending auto-removals Package '%s' has conffile prompt and needs to be upgraded manually Package installation log: Packages that are auto removed: '%s' Packages that attempted to upgrade:
 Packages that were upgraded:
 Packages that will be upgraded: %s Packages were successfully auto-removed Packages with upgradable origin but kept back:
 Progress: %s %% (%s) Running unattended-upgrades in shutdown mode Simulation, download but do not install Starting unattended upgrades script The URI '%s' failed to download, aborting Unattended upgrade returned: %s

 Unattended-upgrade in progress during shutdown, sleeping for 5s Unattended-upgrades log:
 Unclean dpkg state detected, trying to correct Upgrade in minimal steps (and allow interrupting with SIGINT Warning: A reboot is required to complete this upgrade.

 Writing dpkg log to '%s' You need to be root to run this application [package on hold] [reboot required] dpkg --configure -a output:
%s dpkg returned a error! See '%s' for details dpkg returned an error! See '%s' for details error message: '%s' make apt/libapt print verbose debug messages package '%s' not upgraded package '%s' upgradable but fails to be marked for upgrade (%s) print debug messages print info messages {hold_flag}{reboot_flag} unattended-upgrades result for '{machine}': {result} Project-Id-Version: unattended-upgrades 0.82.3
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-21 04:07+0000
PO-Revision-Date: 2016-03-24 10:22+0000
Last-Translator: Phillip Sz <Unknown>
Language-Team: German <debian-l10n-german@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
Language: de
 alle Upgrades installiert erlaubte Ursprünge sind: %s Es ist ein Fehler aufgetreten: »%s« Automatische Paketentfernung ist fehlgeschlagen! Zwischenspeicher enthält beschädigte Pakete, wird beendet Zwischenspeicher konnte nicht gesperrt werden, wird beendet Herunterladen beendet, aber Datei »%s« ist nicht dort!? Fehlermeldung: '%s' %s gefunden, aber kein Neustart, da %s noch eingeloggt ist. %s gefunden, aber kein Neustart, da %s noch eingeloggt sind. GetArchives() fehlgeschlagen: »%s« Sperrdatei, hier wird nach %s Verzögerung aufgegeben. Pakete, die anfangs auf die schwarzen Liste standen: %s Erst-Pakete der Positivliste:  %s Installation der Upgrades fehlgeschlagen! Sperrung konnte nicht erreicht werden (läuft eine weitere Paketverwaltung?) kein »/usr/bin/mail« oder »/usr/sbin/sendmail«, Mail kann nicht gesandt werden. Möglicherweise möchten Sie das Paket »mailx« installieren. Keine Pakete zur Aktualisierung gefunden, es sind keine automatischen Löschungen vorzunehmen Das Paket »%s« hat eine Conffile-Abfrage und muss einem manuellen Upgrade unterzogen werden. Paketinstallationsprotokoll: Pakete, die automatisch entfernt wurden: »%s« Pakete, bei denen versucht wurde, ein Upgrade durchzuführen:
 Pakete, von denen ein Upgrade durchgeführt wurde:
 Pakete, von denen ein Upgrade durchgeführt wird: %s Pakete wurden erfolgreich automatisch entfernt Pakete, von deren Ursprung ein Upgrade durchgeführt werden kann, die aber
zurückgehalten werden.
 Fortschritt: %s %% (%s) Unattended-Upgrades im Herunterfahrmodus ausführen Simulation, herunterladen, aber nicht installieren Skript für unbeaufsichtigte Upgrades wird gestartet. Herunterladen von der URI »%s« fehlgeschlagen, wird abgebrochen Das unbeaufsichtigte Upgrade gab %s zurück.

 Unattended-Upgrade läuft während des Herunterfahrens weiter, es wird fünf Sekunden lang gewartet. Unattended-Upgrades-Protokoll:
 unsauberer Dpkg-Status entdeckt, es wird versucht dies zu korrigieren Upgrade in minimalen Schritten (Unterbrechung mit SIGINT erlaubt) Warnung: Um dieses Upgrade zu komplettieren, ist ein Neustart erforderlich.

 Dpkg-Protokoll wird nach »%s« geschrieben Sie benötigen Systemverwaltungsrechte, um diese Anwendung auszuführen [Paket angehalten] [Neustart erforderlich] »dpkg --configure -a«-Ausgabe:
%s Dpkg gab einen Fehler zurück. Siehe »%s« für Einzelheiten dpkg meldete einen Fehler! Einzelheiten dazu hier '%s' Fehlermeldung: »%s« APT/LibAPT detaillierte Nachrichten zur Fehlersuche ausgeben lassen Von Paket »%s« wurde kein Upgrade durchgeführt. Von Paket »%s« könnte ein Upgrade durchgeführt werden, es ist jedoch fehlgeschlagen, dies für das Upgrade zu markieren (%s) Nachrichten zur Fehlersuche ausgeben Informationsnachrichten ausgeben {hold_flag}{reboot_flag} unattended-upgrades Ergebnis für »{machine}«: {result} 