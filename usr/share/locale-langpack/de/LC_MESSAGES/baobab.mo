��    9      �  O   �      �     �               $     7  '   H  ;   p  �   �     V  !   c     �  +   �     �     �     �     �     �  %        .  4   I     ~     �     �      �     �     �     
          &     2     A  "   J      m     �     �     �     �      �     �     �     	     	  (   	     A	     M	     Z	  	   c	     m	     t	     |	     �	     �	     �	     �	     �	     �	  A  �	           @     O     d     w  <   �  L   �  �        �  %   �     �  6   �  
   4     ?     G  3   N  ,   �  0   �  -   �  P        _     p  )   �  0   �     �          "     )     B     W  	   v  *   �  $   �     �     �     �               /     K     Q  	   `  5   j     �     �     �     �     �  
   �      �          "     +     ;  2   D  L  w     .                3            ,                              6                7   9          	             -       %             +   !                      $         (       &           #   '          8   /   
   4          "       1      )            2       *   0       5            "%s" is not a valid folder %d day %d days %d item %d items %d month %d months %d year %d years A graphical tool to analyze disk usage. A list of URIs for partitions to be excluded from scanning. A simple application which can scan either specific folders (local or remote) or volumes and give a graphical representation including each directory size or percentage. Active Chart Apparent sizes are shown instead. Baobab Check folder sizes and available disk space Close Computer Contents Could not analyze disk usage. Could not analyze volume. Could not detect occupied disk sizes. Could not scan folder "%s" Could not scan some of the folders contained in "%s" Devices and locations Disk Usage Analyzer Excluded partitions URIs Failed to move file to the trash Failed to open file Failed to show help Folder Go to _parent folder Home folder Mo_ve to Trash Modified Print version information and exit Recursively analyze mount points Rings Chart Scan Folder… Select Folder Size The GdkWindowState of the window The initial size of the window Today Treemap Chart Unknown Which type of chart should be displayed. Window size Window state Zoom _in Zoom _out _About _Cancel _Copy Path to Clipboard _Help _Open _Open Folder _Quit storage;space;cleanup; translator-credits Project-Id-Version: baobab master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=baobab&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 14:31+0000
PO-Revision-Date: 2015-07-20 13:56+0000
Last-Translator: Benjamin Steinwender <Unknown>
Language-Team: Deutsch <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:41+0000
X-Generator: Launchpad (build 18115)
Language: de
X-DamnedLies-Scope: partial
 »%s« ist kein gültiger Ordner %d Tag %d Tage %d Objekt %d Objekte %d Monat %d Monate %d Jahr %d Jahre Ein grafisches Werkzeug zur Analyse der Festplattenbelegung. Eine Liste von Adressen der Partitionen, die nicht analysiert werden sollen. Eine einfache Anwendung, die entweder spezifische Ordner (lokale Ordner und Netzwerkordner) oder Laufwerke untersuchen kann und die Speicherplatznutzung grafisch darstellt. Aktives Diagramm Scheinbare Größen werden angezeigt. Baobab Ordnergrößen und freien Festplattenplatz analysieren Schließen Rechner Inhalt Festplattenbelegung konnte nicht analysiert werden. Datenträger konnte nicht analysiert werden. Festplattenbelegung konnte nicht erkannt werden. Ordner »%s« konnte nicht analysiert werden. Ordner »%s« oder darin enthaltene Unterordner konnten nicht analysiert werden. Geräte und Orte Festplattenbelegung analysieren Adressen der ausgeschlossenen Partitionen Konnte Datei nicht in den Papierkorb verschieben Konnte Datei nicht öffnen Konnte Hilfe nicht anzeigen Ordner Eine Ebene höher _gehen Persönlicher Ordner In den _Papierkorb verschieben Geändert Versionsinformationen anzeigen und beenden Einhängepunkte rekursiv analysieren Kreisdiagramm Ordner analysieren … Ordner auswählen Größe Das GdkWindowState des Fensters Anfängliche Fenstergröße Heute Kacheldiagramm Unbekannt Legt fest, welcher Diagrammtyp angezeigt werden soll. Fenstergröße Fensterzustand Ansicht ver_größern Ansicht ver_kleinern _Info _Abbrechen Pfad in Zwischenablage _kopieren _Hilfe _Öffnen _Ordner öffnen _Beenden Speicherplatz;Belegung;Kapazität;Frei;Aufräumen; Manuel Borchers <webmaster@matronix.de>
Karl Eichwalder <ke@suse.de>
Christian Meyer <chrisime@gnome.org>
Christian Neumair <chris@gnome-de.org>
Benedikt Roth <Benedikt.Roth@gmx.net>
Matthias Warkus <mawarkus@gnome.org>
Simon Westhues <westhues@muenster.de>
Hendrik Brandt <heb@gnome-de.org>
Hendrik Richter <hendrikr@gnome.org>
Christian Kirbach <christian.kirbach@googlemail.com>
Nathan-J. Hirschauer <nathanhirschauer@verfriemelt.org>
Mario Blättermann <mario.blaettermann@gmail.com>
Paul Seyfert <pseyfert@mathphys.fsk.uni-heidelberg.de>
Hedda Peters <hpeters@redhat.com>
Benjamin Steinwender <b@stbe.at>

Launchpad Contributions:
  Benjamin Steinwender https://launchpad.net/~b-u
  Daniel Winzen https://launchpad.net/~q-d
  Hedda Peters https://launchpad.net/~hpeters-redhat
  Mario Blättermann https://launchpad.net/~mario.blaettermann 