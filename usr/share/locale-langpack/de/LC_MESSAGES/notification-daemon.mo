��          |      �             !     9     R     h  (   ~     �     �     �     �       '     )  D  !   n     �     �     �  :   �  #   !     E     ]  ,   x     �  ,   �                      	                          
           Clear all notifications Closes the notification. Display notifications Enable debugging code Exceeded maximum number of notifications Invalid notification identifier Notification Daemon Notification body text. Notification summary text. Notifications Replace a currently running application Project-Id-Version: notification-daemon master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=notification-daemon&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-19 01:30+0200
PO-Revision-Date: 2016-02-19 09:17+0000
Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>
Language-Team: Deutsch <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:46+0000
X-Generator: Launchpad (build 18115)
Language: de
 Alle Benachrichtigungen entfernen Schließt die Benachrichtigung. Benachrichtigungen anzeigen Debugging-Code aktivieren Maximale Anzahl an Benachrichtigungen wurde überschritten Ungültige Benachrichtigungskennung Benachrichtigungsdienst Text der Benachrichtigung. Zusammenfassender Text der Benachrichtigung. Benachrichtigungen Eine aktuell ausgeführte Anwendung ersetzen 