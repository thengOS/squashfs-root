��    "      ,  /   <      �     �     
       
        #  .   :     i  	   |     �     �     �  !   �     �     �     �     �          .  ,   E     r     �     �     �     �  $   �     �     �       �   '     �     �     �     �  �  �     �     �     �     �     �  <   �      4     U     c     z     �  1   �     �     �     �     �     	     ,	  /   J	     z	     �	  3   �	  "   �	     �	  )   �	     
     (
  !   >
  �   `
  	   $     .  
   >     I                                                                                 	             
         !                                             "          Add input method Addon Advance Appearance Available Input Method Cannot load currently used user interface info Clear font setting Configure Current Input Method Default Default keyboard layout Didn't install related component. Empty Global Config Input Method Input Method Configuration Input Method Default Input method settings: Keyboard layout to use when no input window: Keyboard layout: Language No configuration option for %s. Only Show Current Language Other Please press the new key combination Search Addon Search Input Method Show Advance Option The first input method will be inactive state. Usually you need to put <b>Keyboard</b> or <b>Keyboard - <i>layout name</i></b> in the first place. Unknown Unspecified _Cancel _OK Project-Id-Version: fcitx
Report-Msgid-Bugs-To: fcitx-dev@googlegroups.com
POT-Creation-Date: 2013-10-18 03:07-0400
PO-Revision-Date: 2015-03-16 03:09+0000
Last-Translator: csslayer <Unknown>
Language-Team: German (http://www.transifex.com/projects/p/fcitx/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:00+0000
X-Generator: Launchpad (build 18115)
Language: de
 Eingabemethode hinzufügen Addon Fortschritt Aussehen Verfügbare Eingabemethoden Die Info zur aktuellen Oberfläche kann nicht geladen werden Font-Einstellungen zurücksetzen Konfigurieren Aktive Eingabemethoden Standard Standard Tastaturlayout Die verwandte Komponente wurde nicht installiert. Leer Globale Einstellungen Eingabemethode Eingabemethode Konfiguration Standard-Eingabemethode Einstellungen Eingabemethode: Ohne aktives Fenster verwendetes Tastaturlayout Tastaturlayout: Sprache Für %s sind keine Einstellmöglichkeiten vorhanden Nur gegenwärtige Sprache anzeigen Andere Bitte die neue Tastenkombination drücken Addons suchen Eingabemethode suchen Erweiterte Einstellungen anzeigen Die erste Eingabemethode wird von Fcitx im inaktiven Zustand verwendet. Normalerweise wird es nötig sein <b>Tastatur</b> or <b>Tastatur - <i>Layout Name</i></b> an der ersten Stelle einzufügen. Unbekannt Nicht angegeben _Abbrechen _OK 