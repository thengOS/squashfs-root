��    (      \  5   �      p  "   q  ,   �     �     �     �     �  "        $  /   9      i  )   �     �     �     �     �  :   �  4   '     \     i  ]   �  $   �       	   '  6   1     h  *   v     �     �  2   �            )   2     \  2   w  6   �  !   �  !     	   %     /  �  A  "   �	  8   
     L
     ]
     p
     �
  #   �
     �
  /   �
  '  �
  2   '     Z     p     ~     �  I   �  D   �     &  x  3  �   �  0   /  !   `     �  ?   �     �  =   �  #        >  2   X      �     �  6   �       ;   !  :   ]  $   �     �  	   �  $   �                                                                      '       !   %       
   #      "                  (   	   &                                          $              (C) 2007 - 2008 Anthony Mercatante <b>Incorrect password, please try again.</b> <b>Warning: </b> Anthony Mercatante Command not found! Command: Do not display « ignore » button Do not keep password Do not show the command to be run in the dialog EMAIL OF TRANSLATORSYour emails Fake option for KDE's KdeSu compatibility Forget passwords Harald Sitter Jonathan Riddell KdeSudo Makes the dialog transient for an X app specified by winid Manual override for automatic desktop file detection Martin Böhm NAME OF TRANSLATORSYour names No command arguments supplied!
Usage: kdesudo [-u <runas>] <command>
KdeSudo will now exit... Please enter password for <b>%1</b>. Please enter your password. Priority: Process priority, between 0 and 100, 0 the lowest [50] Robert Gruber Specify icon to use in the password dialog Sudo frontend for KDE The command to execute The comment that should be displayed in the dialog Use existing DCOP server Use realtime scheduling Use target UID if <file> is not writeable Wrong password! Exiting... Your user is not allowed to run sudo on this host! Your user is not allowed to run the specified command! Your username is unknown to sudo! needs administrative privileges.  realtime: sets a runas user Project-Id-Version: kdesudo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-05 00:10+0000
PO-Revision-Date: 2014-02-20 08:22+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:23+0000
X-Generator: Launchpad (build 18115)
 (C) 2007 - 2008 Anthony Mercatante <b>Falsches Passwort. Bitte versuchen Sie es erneut.</b> <b>Warnung: </b> Anthony Mercatante Befehl nicht gefunden! Befehl: »Ignorieren«-Knopf nicht anzeigen Passwörter nicht speichern Auszuführenden Befehl im Dialog nicht anzeigen ,,blizzz@arthur-schiwon.de,,,,julian.helfferich@googlemail.com,matthias@tenstral.net,wuerdemann@engram.de,,ogni@stcim.de,till@thoran.eu,,,,,blizzz@arthur-schiwon.de,,,,julian.helfferich@googlemail.com,matthias@tenstral.net,wuerdemann@engram.de,,ogni@stcim.de,till@thoran.eu,tobannert@gmail.com,, Fake-Option für die KdeSu-Kompatibilität von KDE Passwörter vergessen Harald Sitter Jonathan Riddell KdeSudo Macht den Dialog temporär für das durch „winid“ angegebene Fenster. Manuelle Überschreibung für die automatisch erkannte desktop-Datei Martin Böhm ,Launchpad Contributions:,Arthur Schiwon,BajK,FriedChicken,Jannick Kuhr,Julian Helfferich,Matthias Klumpp,Michael Wuerdemann,Moritz Baumann,Ogni,Till Freier,fg1234567890,zeroathome, ,Launchpad Contributions:,Arthur Schiwon,BajK,FriedChicken,Jannick Kuhr,Julian Helfferich,Matthias Klumpp,Michael Wuerdemann,Moritz Baumann,Ogni,Till Freier,Tobias Bannert,benzimmer,fg1234567890 Es wurden keine Argumente für den Befehl übergeben.
Benutzung: kdesudo [-u <Benutzername>] <Befehl>
KdeSudo wird nun beendet ... Bitte geben Sie das Passwort für <b>%1</b> ein. Bitte geben Sie Ihr Passwort ein. Priorität: Prozesspriorität, zwischen 0 und 100, 0 ist die geringste [50] Robert Gruber Symbol festlegen, das im Passwortdialog angezeigt werden soll Grafische Sudo-Oberfläche für KDE Der auszuführende Befehl Der Kommentar, der im Dialog angezeigt werden soll Bestehenden DCOP-Server benutzen Echtzeitablaufplanung benutzen Ziel-UID verwenden, wenn <file> nicht beschreibbar ist Falsches Passwort! Beenden … Ihr Benutzer darf auf diesem Rechner sudo nicht ausführen! Ihr Benutzer darf den angegebenen Befehl nicht ausführen! Ihr Benutzername ist sudo unbekannt! benötigt Administratorrechte.  Echtzeit: legt den ausführenden Benutzer fest 