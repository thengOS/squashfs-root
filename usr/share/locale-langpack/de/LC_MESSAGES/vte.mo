��          �      \      �  $   �  )   �        6   9     p  /   �  (   �     �  +   �     $     B  $   ^  -   �  +   �  7   �       &   -  /   T     �  �  �  ;   |  A   �  '   �  H   "     k  ?   }  8   �  .   �  E   %     k  *   �  2   �  4   �  8   	  N   W	     �	  -   �	  >   �	  "   /
                                  	                      
                                           Attempt to set invalid NRC map '%c'. Attempt to set invalid wide NRC map '%c'. Could not open console.
 Could not parse the geometry spec passed to --geometry Duplicate (%s/%s)! Error (%s) converting data for child, dropping. Error compiling regular expression "%s". Error creating signal pipe. Error reading PTY size, using defaults: %s. Error reading from child: %s. Error setting PTY size: %s. Got unexpected (key?) sequence `%s'. No handler for control sequence `%s' defined. Unable to convert characters from %s to %s. Unable to send data to child, invalid charset convertor Unknown pixel mode %d.
 Unrecognized identified coding system. _vte_conv_open() failed setting word characters can not run %s Project-Id-Version: vte master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=vte&component=general
POT-Creation-Date: 2007-10-06 10:12+0000
PO-Revision-Date: 2016-02-08 00:37+0000
Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>
Language-Team: German <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:07+0000
X-Generator: Launchpad (build 18115)
 Es wurde versucht, die ungültige NRC-Map »%c« zu setzen. Es wurde versucht, die ungültige weite NRC-Map »%c« zu setzen. Konsole konnte nicht geöffnet werden.
 Mittels --geometry übergebene Ausmaße konnten nicht verarbeitet werden Duplikat (%s/%s)! Fehler (%s) beim Konvertieren der Daten für Kind, abgebrochen. Fehler beim Kompilieren des regulären Ausdrucks »%s«. Fehler beim Erstellen der Signalweiterleitung. Fehler beim Ermitteln der PTY-Größe, Vorgaben werden verwendet: %s. Fehler beim Lesen von Kind: %s. Fehler beim Festlegen der PTY-Größe: %s. Unerwartete (Schlüssel?)-Sequenz »%s« erhalten. Kein Handler für Kontrollsequenz »%s« festgelegt. Zeichen konnten nicht von %s nach %s konvertiert werden. Daten konnten nicht an Kind gesendet werden, ungültiger Zeichensatz-Konverter Unbekannter Pixelmodus %d.
 Nicht erkanntes, identifiziertes Kodiersystem _vte_conv_open() konnte die Zeichen des Wortes nicht festlegen %s konnte nicht ausgeführt werden 