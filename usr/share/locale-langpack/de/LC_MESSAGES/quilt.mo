��    �      �  �   �	        �     *    r   9    �  Z   �    '  �  �  _  �  }  �  �   f  �   Z  @  �  �   )   m   �   r   B!  \   �!  �  "  j  �#  �   I%  [   C&  `   �&  N    '  \   O'  �  �'    �,  �   �1  p  V2    �4  @   �5  '  6    87     T8     k8  %   ~8  *   �8     �8     �8  B   9  E   D9  L   �9     �9     �9  $   �9  !   :     @:     X:  .   n:  =   �:  0   �:     ;  -   (;     V;     r;  ,   �;  !   �;  (   �;     <     "<     A<     ]<     s<      �<     �<     �<     �<     �<     =  #   <=  -   `=      �=  %   �=  #   �=  0   �=  H   *>  .   s>     �>     �>     �>     �>     �>  =   ?  C   @?  (   �?  '   �?  +   �?  .   @  B   0@  9   s@  3   �@     �@  #   �@     A     =A     SA     sA      �A     �A     �A     �A  '   �A  &   #B  #   JB     nB     �B     �B     �B     �B  0   �B  1   C     =C  #   TC     xC  %   �C  #   �C  <   �C  &   D  O   DD  �   �D  3   E  X   �E  /   F     <F  @   SF  (   �F     �F     �F  "   �F  �   G     �G  =   �G  $   *H     OH  U   nH  *   �H  d   �H  <   TI  g   �I     �I     J     4J  '   RJ     zJ  9   �J  �   �J  +   ]K  )   �K     �K  @   �K     L     ,L      ?L     `L  .   wL  /   �L  �  �L  V  YN  f  �O  z   S  F  �S  e   �U  �  ?V  �   Y  C  ]  �  T^  =  `  �   ?a  {  �a  �   ^c  �   .d  �   �d  a   7e  �  �e  �  �g     +i  h   ,j  ]   �j  9   �j  i   -k    �k  -  �p  �   �v  �  �w    Zz  ^   r{  q  �{  B  C}     �~     �~  '   �~  D   �~     !  4   5  O   j  [   �  �   �     ��     ��  %   ڀ  "    �     #�     C�  -   ^�  :   ��  :   ǁ     �  :   "�      ]�  #   ~�  5   ��  )   ؂  .   �  !   1�  &   S�  #   z�     ��     ��  &   ΃  !   ��  *   �  %   B�  '   h�      ��  5   ��  8   �  %    �  &   F�  '   m�  ?   ��  V   Յ  6   ,�     c�     s�     ��     ��     ��  U   ˆ  U   !�  .   w�  *   ��  8   ч  1   
�  [   <�  ?   ��  7   ؈     �  .   -�      \�     }�  !   ��     ��  2   ׉  2   
�      =�     ^�  5   z�  +   ��  )   ܊     �     "�     >�     V�     j�  7   ~�  <   ��     �  ,   �     >�  '   \�  !   ��  d   ��  -   �  W   9�  G  ��  A   َ  u   �  5   ��     Ǐ  D   ܏  .   !�  $   P�  #   u�  '   ��  �   ��  "   y�  B   ��  *   ߑ  %   
�  R   0�  4   ��  i   ��  D   "�  m   g�     Փ      �  "   �  ,   7�  $   d�  >   ��  �   Ȕ  1   V�  0   ��     ��  J   ٕ  !   $�     F�  %   ^�     ��  7   ��  <   ؖ     v           �          \   =              �           Q   I   c          �   �   y   G          ~   w      �      p           h   �      #   a          �   L           F       �   /   n   m             �   0              d      l   ?      4   9   ^       o          E   3   �   [   i   �   _   D   �          H   Y   b       r   J   e             >   �       W   8          .   f       2   !           }   6       ;   Z   �       O   j   `   (       )   
   U   s       �   x   �           �   A          K   5       +          �   &   �   	   <   7           :   @                      $      %   g           1   k       �   "   �   �   X          �   P   u   t   -   N       ,   �      R          T           �   �      �   ]   z   C   |   M   *       '               S   �   {      q       B   V              
Add one or more files to the topmost or named patch.  Files must be
added to the patch before being modified.  Files that are modified by
patches already applied on top of the specified patch cannot be added.

-p patch
	Patch to add files to.
 
Apply patch(es) from the series file.  Without options, the next patch
in the series file is applied.  When a number is specified, apply the
specified number of patches.  When a patch name is specified, apply
all patches up to and including the specified patch.  Patch names may
include the patches/ prefix, which means that filename completion can
be used.

-a	Apply all patches in the series file.

-f	Force apply, even if the patch has rejects.

-q	Quiet operation.

-v	Verbose operation.

--leave-rejects
	Leave around the reject files patch produced, even if the patch
	is not actually applied.

--interactive
	Allow the patch utility to ask how to deal with conflicts. If
	this option is not given, the -f option will be passed to the 
	patch program.

--color[=always|auto|never]
	Use syntax coloring.
 
Create a new patch with the specified file name, and insert it after the
topmost patch in the patch series file.
 
Create mail messages from all patches in the series file, and either store
them in a mailbox file, or send them immediately. The editor is opened
with a template for the introductory message. Please see the file
%s for details.

--mbox file
	Store all messages in the specified file in mbox format. The mbox
	can later be sent using formail, for example.

--send
	Send the messages directly using %s.

--from, --subject
	The values for the From and Subject headers to use.

--to, --cc, --bcc
	Append a recipient to the To, Cc, or Bcc header.
 
Edit the specified file(s) in \$EDITOR (%s) after adding it (them) to
the topmost patch.
 
Fork the topmost patch.  Forking a patch means creating a verbatim copy
of it under a new name, and use that new name instead of the original
one in the current series.  This is useful when a patch has to be
modified, but the original version of it should be preserved, e.g.
because it is used in another series, or for the history.  A typical
sequence of commands would be: fork, edit, refresh.

If new_name is missing, the name of the forked patch will be the current
patch name, followed by \"-2\".  If the patch name already ends in a
dash-and-number, the number is further incremented (e.g., patch.diff,
patch-2.diff, patch-3.diff).
 
Generate a dot(1) directed graph showing the dependencies between
applied patches. A patch depends on another patch if both touch the same
file or, with the --lines option, if their modifications overlap. Unless
otherwise specified, the graph includes all patches that the topmost
patch depends on.
When a patch name is specified, instead of the topmost patch, create a
graph for the specified patch. The graph will include all other patches
that this patch depends on, as well as all patches that depend on this
patch.

--all	Generate a graph including all applied patches and their
	dependencies. (Unapplied patches are not included.)

--reduce
	Eliminate transitive edges from the graph.

--lines[=num]
	Compute dependencies by looking at the lines the patches modify.
	Unless a different num is specified, two lines of context are
	included.

--edge-labels=files
	Label graph edges with the file names that the adjacent patches
	modify.

-T ps	Directly produce a PostScript output file.
 
Global options:

--trace
	Runs the command in bash trace mode (-x). For internal debugging.

--quiltrc file
	Use the specified configuration file instead of ~/.quiltrc (or
	/etc/quilt.quiltrc if ~/.quiltrc does not exist).  See the pdf
	documentation for details about its possible contents.

--version
	Print the version number and exit immediately. 
Grep through the source files, recursively, skipping patches and quilt
meta-information. If no filename argument is given, the whole source
tree is searched. Please see the grep(1) manual page for options.

-h	Print this help. The grep -h option can be passed after a
	double-dash (--). Search expressions that start with a dash
	can be passed after a second double-dash (-- --).
 
Import external patches.

-p num
	Number of directory levels to strip when applying (default=1)

-n patch
	Patch filename to use inside quilt. This option can only be
	used when importing a single patch.

-f	Overwite/update existing patches.
 
Initializes a source tree from an rpm spec file or a quilt series file.

-d	optional path prefix (sub-directory).

-v	verbose debug output.
 
Integrate the patch read from standard input into the topmost patch:
After making sure that all files modified are part of the topmost
patch, the patch is applied with the specified strip level (which
defaults to 1).

-p strip-level
	The number of pathname components to strip from file names
	when applying patchfile.
 
Please remove all patches using \`quilt pop -a' from the quilt version used to create this working tree, or remove the %s directory and apply the patches from scratch.\n 
Print a list of applied patches, or all patches up to and including the
specified patch in the file series.
 
Print a list of patches that are not applied, or all patches that follow
the specified patch in the series file.
 
Print an annotated listing of the specified file showing which
patches modify which lines.
 
Print or change the header of the topmost or specified patch.

-a, -r, -e
	Append to (-a) or replace (-r) the exiting patch header, or
	edit (-e) the header in \$EDITOR (%s). If none of these options is
	given, print the patch header.
	
--strip-diffstat
	Strip diffstat output from the header.

--strip-trailing-whitespace
	Strip trailing whitespace at the end of lines of the header.

--backup
	Create a backup copy of the old version of a patch as patch~.
 
Print the list of files that the topmost or specified patch changes.

-a	List all files in all applied patches.

-l	Add patch name to output.

-v	Verbose, more user friendly output.

--combine patch
	Create a listing for all patches between this patch and
	the topmost applied patch. A patch name of \"-\" is equivalent
	to specifying the first applied patch.

 
Print the list of patches that modify the specified file. (Uses a
heuristic to determine which files are modified by unapplied patches.
Note that this heuristic is much slower than scanning applied patches.)

-v	Verbose, more user friendly output.
 
Print the name of the next patch after the specified or topmost patch in
the series file.
 
Print the name of the previous patch before the specified or topmost
patch in the series file.
 
Print the name of the topmost patch on the current stack of applied
patches.
 
Print the names of all patches in the series file.

-v	Verbose, more user friendly output.
 
Produces a diff of the specified file(s) in the topmost or specified
patch.  If no files are specified, all files that are modified are
included.

-p n	Create a -p n style patch (-p0 or -p1 are supported).

-u, -U num, -c, -C num
	Create a unified diff (-u, -U) with num lines of context. Create
	a context diff (-c, -C) with num lines of context. The number of
	context lines defaults to 3.

--no-timestamps
	Do not include file timestamps in patch headers.

--no-index
	Do not output Index: lines.

-z	Write to standard output the changes that have been made
	relative to the topmost or specified patch.

-R	Create a reverse diff.

-P patch
	Create a diff for the specified patch.  (Defaults to the topmost
	patch.)

--combine patch
	Create a combined diff for all patches between this patch and
	the patch specified with -P. A patch name of \"-\" is equivalent
	to specifying the first applied patch.

--snapshot
	Diff against snapshot (see \`quilt snapshot -h').

--diff=utility
	Use the specified utility for generating the diff. The utility
	is invoked with the original and new file name as arguments.

--color[=always|auto|never]
	Use syntax coloring.

--sort	Sort files by their name instead of preserving the original order.
 
Refreshes the specified patch, or the topmost patch by default.
Documentation that comes before the actual patch in the patch file is
retained.

It is possible to refresh patches that are not on top.  If any patches
on top of the patch to refresh modify the same files, the script aborts
by default.  Patches can still be refreshed with -f.  In that case this
script will print a warning for each shadowed file, changes by more
recent patches will be ignored, and only changes in files that have not
been modified by any more recent patches will end up in the specified
patch.

-p n	Create a -p n style patch (-p0 or -p1 supported).

-u, -U num, -c, -C num
	Create a unified diff (-u, -U) with num lines of context. Create
	a context diff (-c, -C) with num lines of context. The number of
	context lines defaults to 3.

--no-timestamps
	Do not include file timestamps in patch headers.
	
--no-index
	Do not output Index: lines.

--diffstat
	Add a diffstat section to the patch header, or replace the
	existing diffstat section.

-f	Enforce refreshing of a patch that is not on top.

--backup
	Create a backup copy of the old version of a patch as patch~.

--sort	Sort files by their name instead of preserving the original order.

--strip-trailing-whitespace
	Strip trailing whitespace at the end of lines.
 
Remove one or more files from the topmost or named patch.  Files that
are modified by patches on top of the specified patch cannot be removed.

-p patch
	Patch to remove files from.
 
Remove patch(es) from the stack of applied patches.  Without options,
the topmost patch is removed.  When a number is specified, remove the
specified number of patches.  When a patch name is specified, remove
patches until the specified patch end up on top of the stack.  Patch
names may include the patches/ prefix, which means that filename
completion can be used.

-a	Remove all applied patches.

-f	Force remove. The state before the patch(es) were applied will
	be restored from backup files.

-R	Always verify if the patch removes cleanly; don't rely on
	timestamp checks.

-q	Quiet operation.

-v	Verbose operation.
 
Remove the specified or topmost patch from the series file.  If the
patch is applied, quilt will attempt to remove it first. (Only the
topmost patch can be removed right now.)

-n	Delete the next patch after topmost, rather than the specified
	or topmost patch.
 
Rename the topmost or named patch.

-p patch
	Patch to rename.
 
Take a snapshot of the current working state.  After taking the snapshot,
the tree can be modified in the usual ways, including pushing and
popping patches.  A diff against the tree at the moment of the
snapshot can be generated with \`quilt diff --snapshot'.

-d	Only remove current snapshot.
 
Upgrade the meta-data in a working tree from an old version of quilt to the
current version. This command is only needed when the quilt meta-data format
has changed, and the working tree still contains old-format meta-data. In that
case, quilt will request to run \`quilt upgrade'.
        quilt --version %s: I'm confused.
 Appended text to header of patch %s\n Applied patch %s (forced; needs refresh)\n Applying patch %s\n Cannot add symbolic link %s\n Cannot diff patches with -p%s, please specify -p0 or -p1 instead\n Cannot refresh patches with -p%s, please specify -p0 or -p1 instead\n Cannot use --strip-trailing-whitespace on a patch that has shadowed files.\n Commands are: Conversion failed\n Converting meta-data to version %s\n Delivery address '%s' is invalid
 Diff failed, aborting\n Directory %s exists\n Display name '%s' contains invalid characters
 Display name '%s' contains non-printable or 8-bit characters
 Display name '%s' contains unpaired parentheses
 Failed to back up file %s\n Failed to copy files to temporary directory\n Failed to create patch %s\n Failed to import patch %s\n Failed to insert patch %s into file series\n Failed to patch temporary files\n Failed to remove file %s from patch %s\n Failed to remove patch %s\n Failed to rename %s to %s: %s
 File %s added to patch %s\n File %s disappeared!
 File %s exists\n File %s is already in patch %s\n File %s is not being modified\n File %s is not in patch %s\n File %s may be corrupted\n File %s modified by patch %s\n File %s removed from patch %s\n File \`%s' is located below \`%s'\n File series fully applied, ends at patch %s\n Fork of patch %s created as %s\n Fork of patch %s to patch %s failed\n Importing patch %s (stored as %s)\n Interrupted by user; patch %s was not applied.\n More recent patches modify files in patch %s. Enforce refresh with -f.\n More recent patches modify files in patch %s\n No next patch\n No patch removed\n No patches applied\n Nothing in patch %s\n Now at patch %s\n Option \`-n' can only be used when importing a single patch\n Options \`-c patch', \`--snapshot', and \`-z' cannot be combined.\n Patch %s appears to be empty, removing\n Patch %s appears to be empty; applied\n Patch %s does not apply (enforce with -f)\n Patch %s does not exist; applied empty patch\n Patch %s does not remove cleanly (refresh it or enforce with -f)\n Patch %s exists already, please choose a different name\n Patch %s exists already, please choose a new name\n Patch %s exists already\n Patch %s exists. Replace with -f.\n Patch %s is already applied\n Patch %s is applied\n Patch %s is currently applied\n Patch %s is not applied\n Patch %s is not in series file\n Patch %s is not in series\n Patch %s is now on top\n Patch %s is unchanged\n Patch %s needs to be refreshed first.\n Patch %s not applied before patch %s\n Patch %s not found in file series\n Patch %s renamed to %s\n Patch is not applied\n Refreshed patch %s\n Removed patch %s\n Removing patch %s\n Removing trailing whitespace from line %s of %s
 Removing trailing whitespace from lines %s of %s
 Renaming %s to %s: %s
 Renaming of patch %s to %s failed\n Replaced header of patch %s\n Replacing patch %s with new version\n SYNOPSIS: %s [-p num] [-n] [patch]
 The %%prep section of %s failed; results may be incomplete\n The -v option will show rpm's output\n The quilt meta-data in %s are already in the version %s format; nothing to do\n The quilt meta-data in this tree has version %s, but this version of quilt can only handle meta-data formats up to and including version %s. Please pop all the patches using the version of quilt used to push them before downgrading.\n The topmost patch %s needs to be refreshed first.\n The working tree was created by an older version of quilt. Please run 'quilt upgrade'.\n USAGE: %s {-s|-u} section file [< replacement]
 Unpacking archive %s\n Usage: quilt [--trace[=verbose]] [--quiltrc=XX] command [-h] ... Usage: quilt add [-p patch] {file} ...\n Usage: quilt annotate {file}\n Usage: quilt applied [patch]\n Usage: quilt delete [patch | -n]\n Usage: quilt diff [-p n] [-u|-U num|-c|-C num] [--combine patch|-z] [-R] [-P patch] [--snapshot] [--diff=utility] [--no-timestamps] [--no-index] [--sort] [--color] [file ...]\n Usage: quilt edit file ...\n Usage: quilt files [-v] [-a] [-l] [--combine patch] [patch]\n Usage: quilt fold [-p strip-level]\n Usage: quilt fork [new_name]\n Usage: quilt graph [--all] [--reduce] [--lines[=num]] [--edge-labels=files] [patch]\n Usage: quilt grep [-h|options] {pattern}\n Usage: quilt header [-a|-r|-e] [--backup] [--strip-diffstat] [--strip-trailing-whitespace] [patch]\n Usage: quilt import [-f] [-p num] [-n patch] patchfile ...\n Usage: quilt mail {--mbox file|--send} [--from ...] [--to ...] [--cc ...] [--bcc ...] [--subject ...]\n Usage: quilt new {patchname}\n Usage: quilt next [patch]\n Usage: quilt patches {file}\n Usage: quilt pop [-afRqv] [num|patch]\n Usage: quilt previous [patch]\n Usage: quilt push [-afqv] [--leave-rejects] [num|patch]\n Usage: quilt refresh [-p n] [-f] [--no-timestamps] [--no-index] [--diffstat] [--sort] [--backup] [--strip-trailing-whitespace] [patch]\n Usage: quilt remove [-p patch] {file} ...\n Usage: quilt rename [-p patch] new_name\n Usage: quilt series [-v]\n Usage: quilt setup [-d path-prefix] [-v] {specfile|seriesfile}\n Usage: quilt snapshot [-d]\n Usage: quilt top\n Usage: quilt unapplied [patch]\n Usage: quilt upgrade\n Warning: trailing whitespace in line %s of %s
 Warning: trailing whitespace in lines %s of %s
 Project-Id-Version: de
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-27 17:14+0000
PO-Revision-Date: 2014-06-18 05:17+0000
Last-Translator: Andreas Gruenbacher <Unknown>
Language-Team: <en@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:14+0000
X-Generator: Launchpad (build 18115)
Language: 
 
Fügt eine oder mehrere Dateien zum obersten oder angegebenen Patch
hinzu. Dateien müssen einem Patch hinzugefügt werden, bevor sie
verändert werden. Dateien können einem Patch nur hinzugefügt werden,
solange sie von keinem Patch höher auf dem Stapel verändert werden.

-p patch
	Patch, zu dem die Dateien hinzugefügt werden sollen.
 
Patches in der series-Datei anwenden. Es kann eine Anzahl von Patches,
die angewandt werden soll, oder der Name eines Patches angegeben werden.
Wenn ein Name angegeben wird, werden alle Patches bis einschließlich zum
angegebenen Patch angewandt. Wenn weder ein Patchname noch eine Nummer
angegeben wird, wird der nächste Patch angewandt.

-a	Alle folgenden Patches anwenden.

-f	Anwenden erzwingen, solbst wenn dabei Fehler (Rejects) auftreten.

-q	Wenige Meldungen ausgeben.

-v	Viele Meldungen ausgeben.

--leave-rejects
	Reject-Dateien (von \`patch') bestehen lassen, selbst wenn ein Patch
	nicht sauber angewandt werden kann.

--interactive
	Erlaubt GNU patch, interaktiv Fragen zu stellen. Wenn diese Option
	nicht angegeben wird, wird die Option -f an GNU patch übergeben.

--color[=always|auto|never]
	Syntax-Einfärbung aktivieren (immer, automatisch, nie).
 
Erzeuge einen neuen Patch mit dem angegebenen Dateinamen, und fügt
ihn nach dem obersten Patch in die series-Datei ein.
 
Erzeuge E-Mail-Nachrichten für alle Patches in der series-Datei, und
speichere sie in einer Mailbox-Datei, oder versende sie sofort. Der Editor
wird mit der Vorlage einer Einleitungs-Nachricht geöffnet. Für Details siehe
%s.

--mbox datei
	Alle Nachrichten in der angegebenen Datei im mbox-Format speichern. Die
	Mailbox kann dann später mit formail o.ä. verschickt werden.

--send
	Verschicke die Nachrichten direkt über %s.

--from, --subject
	Die Werte für die From- und Subject-Kopfzeilen.

--to, --cc, --bcc
	Füre einen Empfänger an die To, Cc, or Bcc-Kopfzeilen an.
 
Füge die angegebene(n) Datei(en) dem obersten Patch hinzu, und editiere
sie dann in \$EDITOR (%s).
 
Forkt den obersten Patch. Einen Patch forken bedeutet,  eine originalgetreue Kopie 
unter einem anderen Namen anzulegen und diesen Namen dann an Stelle des Originals in der aktuellen Serie zu benutzen. Das ist sinnvoll, wenn ein Patch verändert werden soll, das Original jedoch erhalten 

bleiben soll, weil es z.B. in einer anderen Reihe benutzt wird, oder in der History auftauchen soll. Eine typische Folge von Befehlen wäre: fork, edit, refresh (forken, bearbeiten, aktualisieren).

Wenn new_name fehlt, ist der Name des geforkten Patches der des aktuellen Patches, gefolgt von \"-2\". Wenn der Patchname schon auf einen
Bindestrich und eine Zahl endet, wird die Zahl inkrementiert (z.B. patch.diff,
patch.diff-2, patch.diff-3).
 
Erzeuge einen gerichteten dot(1)-Graphen, der die Abhängigkeiten zwischen
den angewandten Patches zeigt. Ein Patch hängt voni einem anderen ab, wenn
beide dieselbe Datei verändern, oder mit der --lines-Option, wenn sich die
Anderungen in den Patches überlappen. Wenn nicht anders angegeben, zeigt
der Graph alle Patches, von denen der oberste Patch abhängt.
Wenn ein Patchname angegeben wird, wird ein Graph erzeugt, der alle Patches
beinhaltet, von denen der angegebene Patch abhängt, sowie alle, die von
diesem Patch abhängen.

--all	Erzeuge einen Graph, der alle Abhängigkeiten zwischen allen
	angewandten Patches beinhaltet. (Nicht angewandte Patches sind nicht
	beinhaltet.)

--reduce
	Eliminiere transitive Kanten.

--lines[=num]
	Ermittle Abhängigkeiten durch Vergleich der veränderten Zeilen.
	Solange nicht anders angegeben, werden zwei Kontextzeilen angenommen.

--edge-labels=files
	Beschrifte Kanten mit den Namen der betroffenen Dateien.

-T ps	Erzeuge direkt eine PostScript-Datei.
 
Globale Optionen:

--trace
	Führt die Kommandos im bash Trace-Modus (-x) aus. Für Debugging.

--quiltrc datei
	Verwende die angegebene Konfigurationsdatei statt ~/.quiltrc (oder
	/etc/quilt.quiltrc, wenn ~/.quiltrc fehlt). Siehe die pdf-Dokumentation
	für mögliche Einstellungen.
--version
	Gibt die Versionsummer aus. 
Durchsuche die Quelldateien rekursiv, und überspringe Patches und Quilt-
Metainformation. Wenn kein Dateiname angegeben wird, wird der gesamte
Quellbaum durchsucht. Siehe die grep(1) Manual Page zu Optionen.

-h	Gib diese Hilfsmeldung aus. Die grep -h - Option kann nach einem
	Doppelstrick (--) angegeben werden. Suchausdrücke, die mit einem Strich
	beginnen, können nach einem weiteren Doppelstrich (--) angegeben werden.
 
Importiere externe Patches.

-p num
	Die Anzahl der Komponenten im Pfadnamen, die beim Anwenden des Patches
	entfernt werden sollen.
-n patch
	Patch-Dateiname, den Quilt verwenden soll. Diese Option kann nur beim
	Import eines einzelnen Patches angegeben werden.

-f	Überschreibe/aktualisiere existierende Patches.
 
Initialisiere einen Quellbaum aus einer rpm spec-Datei oder einer quilt series-Datei.

-d	Optionaler Pfad-Präfix (Unterverzeichnis).

-v	Ausführliche Ausgabe.
 
Integriert den Patch von der Standardeingabe in den obersten Patch:
Stellt zuerst sicher, dass alle Dateien, die verändert werden, im
obersten Patch enthalten sind, und wendet dann den neuen Patch
mit der angegebenen Anzahl an Strip-Ebenen an (Standardwert = 1).

-p strip-ebenen
	Die Anzahl der Komponenten im Pfadnamen, die beim Anwenden des Patches
	entfernt werden sollen.
 
Bitte entfernen Sie alle Patches mit \`quilt pop -a' der quilt-Version, die zur Erzeugung des Arbeitsverzeichnis verwendet wurden, oder entfernen Sie das Verzeichnis %s, und wenden Sie die Patches neu an.\n 
Gibt eine Liste der angewandten Patches aus, oder eine Liste aller Patches
vom Anfang der series-Datei bis zum angegebenen Patch.
 
Gint eine Liste der Patches aus, die momentan nicht angewandt sind, bzw.
eine Liste der Patches, die dem angegebenen Patch folgen.
 
Erzeuge ein Listing der angegebenen Datei das anzeigt, welche
Patches welche Zeilen verändern.
 
Zeige den Header des obersten oder angegebenen Patches an, oder verändere ihn.

-a, -r, -e
	Füge Text von der Standardeingabe an den Header an (-a), ersetze den Header
	mit der Standardeingabe, oder editiere den Header in \$EDITOR (%s). Wenn keine
	dieser Optionen angegeben ist, gib den Header aus.

--strip-diffstat
	Entferne diffstat-Ergebnisse vom Header.

--strip-trailing-whitespace
	Entferne Whitespace an Zeilenenden im Header.

--backup
	Erzeuge eine Sicherungskopie von patch als patch~.
 
Gibt eine Liste der Dateien aus, die der oberste oder angegebene Patch
beinhaltet.

-a	Zeigt alle Dateien in allen angewandten Patches an.

-l	Gibt auch die Patchnamen mit aus.

-v	Ausführlichere, benutzerfreundliche Ausgabe.

--combine patch
	Erzeige eine Liste für alle Patches zwischen dem angegebenen Patch und
	dem obersten Patch am Stack. Der Patchname \"-\" entspricht dem
	ersten angewandten Patch.

 
Gibt die Liste der Patches aus, die die angegebene Datei verändern.
(Verwendet für Patches, die nicht angewandt sind, eine Heuristik. Diese
Heuristik ist langsamer als die Suche in angewandten Patches.)

-v	Ausführlichere, benutzerfreundliche Ausgabe.
 
Gibt den Namen des nächsten Patches nach dem obersten oder angegebenen
Patch in der series-Datei aus.
 
Gibt den Namen des Patches vor dem obersten oder angegebenen
Patch in der series-Datei aus.
 
Gibt den Namen des obersten Patches auf dem Stapel aus.
 
Gibt die Namen aller Dateien in der series-Datei aus.

-v	Ausführlichere, benutzerfreundliche Ausgabe.
 
Erzeugt ein Diff der angegebenen Dateien im obersten oder angegebenen
Patch. Wenn keine Dateien angegeben sind, werden alle im Patch
enthaltenen Dateien einbezogen.

-p n	Erzeuge einen -p n Patch (n=0 oder n=1 wird unterstützt).

-u, -U anzahl, -c, -C anzahl
	Erzeuge ein Unified Diff (-u, -U) mit anzahl Kontextzeilen. Erzeuge
	ein Context Diff (-c, -C) mit anzahl Kontextzeilen. Die Anzahl der
	Kontextzeilen ist 3, wenn nicht anders angegeben.

--no-timestamps
	Keine Zeitstempel in Patches angeben.

--no-index
	Erzeuge keine "Index:"-Zeilen

-z	Änderungen relativ zum angewandten Patch ausgeben.

-P patch
	Erzeuge ein Diff für den angegebenen Patch. (Wenn nicht angegeben,
	oberster Patch.)

--combine patch
	Erzeuge ein kombiniertes Diff für alle Patches zwischen diesem und
	dem mit -P angegebenen Patch.

-R	Erzeuge ein umgekehrtes Diff.

--snapshot
	Erzeuge einen Patch gegen den Snapshot (siehe \`quilt snapshot -h').

--diff=programm
	Verwendet das angegebene Programm, um den Patch zu generieren. Das
	Programm wird mit dem Dateinamen der ursprünglichen und der
	neuen Datei aufgerufen.

--color[=always|auto|never]
	Syntaxeinfärbung verwenden (immer, nur für Terminals, nie).

--sort	Sortiere Dateien im Patch nach ihrem namen, statt die ursprüngliche
	Reihenfolge zu erhalten.
 
Frischt den obersten oder angegebenen Patch auf. Dokumentation in der
Patch-Datei, die vor dem eigentlichen Patch steht, bleibt dabei
erhalten.

Es können beliebige angewandte Patches aufgefrischt werden. Wenn
Patches, die nach dem angegebenen Patch angewandt sind, dieselben
Dateien verändern, die auch dieser Patch verändert, bricht dieses Script
normalerweise ab. Mit der Option -f kann das Auffrischen trotzdem
erzwungen werden. Dann wird für jede Datei, die später noch verändert
wurde, eine Warnung ausgegeben, und nur Änderungen in Dateien, die
danach nicht von anderen Patches weiter verändert werden, werden beim
Auffrischen berücksichtigt.

-p n	Erzeuge einen -p n Patch (n=0 oder n=1 werden unterstützt).

-u, -U anzahl, -c, -C anzahl
	Erzeuge ein Unified Diff (-u, -U) mit anzahl Kontextzeilen. Erzeuge
	ein Context Diff (-c, -C) mit anzahl Kontextzeilen. Die Anzahl der
	Kontextzeilen ist 3, wenn nicht anders angegeben.

--no-timestamps
	Keine Zeitstempel in Patches angeben.

--no-index
	Erzeuge keine "Index:"-Zeilen

--diffstat
	Füge dem Patch-Kopf einen diffstat-Abschnitt hinzu, oder ersetzte den
	bestehenden Abschnitt.

-f	Erzwinge das Auffrischen eines Patches, der sich nicht an oberster
	Position befindet.

--no-timestamps
	Keine Zeitstempel in Patches angeben (wie QUILT_DIFF_NO_TIMESTAMPS in~/.quiltrc).

--backup
	Erzeuge ein Backup der alten Version von patch als patch~

--sort	Sortiere Dateien im Patch nach ihrem namen, statt die ursprüngliche
	Reihenfolge zu erhalten.

--strip-trailing-whitespace
	Entferne Whitespace an Zeilenenden.
 
Entfernt Dateien aus dem obersten oder angegebenen Patch. Dateien, die
durch Patches über dem angegebenen Patch verändert werden, können nicht
entfernt werden.

-p patch
	Patch, aus dem Dateien entfernt werden sollen.
 
Entferne Patches vom Stapel der angewandten Patches. Es kann eine Anzahl
von Patches, die entfernt werden soll, oder der Name eines Patches
angegeben werden. Wenn ein Name angegeben wird, werden alle Patches über
dem angegebenen Patch entfernt. Wenn weder ein Patchname noch eine Nummer
angegeben wird, wird der oberste Patch entfernt.

-a	Alle angewandten Patches entfernen.

-f	Erzwungenes Entfernen. Der Zustand vor dem Anwenden das Patches wird
	über die Sicherungsdateien wiederhergestellt.

-R	 Überprüfe immer, ob sich Patches vollständig entfernen lassen;
	nicht auf Datei-Zeitspempel verlassen.

-q	Wenige Meldungen ausgeben.

-v	Viele Meldungen ausgeben.
 
Löscht den obersten oder angegebenen Patch aus der series-Datei. Falls
dieser Patch angewandt ist, entfernt quilt ihn zuerst. (Momentan kann 
nur der oberste Patch entfernt werden.)

-n	Lösche den nächsten (dem obersten folgenden) Patch statt des obersten
	oder angegebenen.
 
Benennt den obersten oder angegebenen Patch um.

-p patch
	Patch, der umbenannt werden soll.
 
Erzeuge einen Snapshot des aktuellen Zustands des Quellbaums. Danach kann
der Quellbaum auf die üblichen Arten verändert werden, inklusive dem
Hinzufügen und Entfernen von Patches. Ein Patch zwischen dem Quellbaum
zum Zeitpunkt des Snapshots und dem aktuellen Zustand kann mit
\`quilt diff --snapshot' erzeugt werden.

-d	Entferne lediglich den aktuellen Snapshot.
 
Aktualisiere die Metadaten in einem Arbeitsverzeichnis von einer älteren Version
von quilt zur aktuellen. Dieses Kommando wird nur benötigt, wenn sich das
Format der Metadaten verändert hat, und das Arbeitsverzeichnis noch alte
Metadaten enthält. In diesem Fall fordert quilt dazu auf, \`quilt upgrade'
auszuführen.
        quilt --version %s: Ich bin verwirrt.
 Text an Header von Patch %s angefügt\n Patch %s angewandt (erzwungen, muss aktualisiert werden (Refresh))\n Wende Patch %s an\n Kann symbolische Verknüpfung %s nicht hinzufügen\n Kann kein Diff mit -p$opt_strip_level erzeugen, bitte -p0 oder -p1 verwenden.\n Kann Patches mit Level -p$opt_strip_level nicht aktualisieren, bitte -p0 oder -p1 angeben\n Kann --strip-trailing-whitespace nicht für Patches verwenden, die Dateien enthalten, die danach von anderen Patches weiter verändert werden.\n Vorhandene Befehle: Konvertierung fehlgeschlagen\n Konvertiere Metadaten in Version %s\n Zustelladresse '%s' ist ungültig
 Diff fehlgeschlagen, Abbruch.\n Verzeichnis %s existiert\n Anzeigename '%s' enthält ungültige Zeichen
 Anzeigename '%s' enthält nichtdruckbare or 8-Bit-Zeichen
 Anzeigename '%s' enthält eine unvollständige Klammerung
 Konnte Datei %s nicht sichern\n Konnte Dateien nicht in temporäres Verzeichnis kopieren\n Konnte Patch %s nicht erzeugen\n Konnte Patch %s nicht importieren\n Konnte Patch %s nicht in die series-Datei einfügen\n Konnte temporäre Dateien nicht patchen\n Konnte Datei %s aus Patch %s nicht entfernen\n Konnte Patch %s nicht entfernen\n Konnte %s nicht auf %s umbenennen: %s
 Datei %s zu Patch %s hinzugefügt\n Datei %s ist verschwunden
 Datei %s existiert\n Datei %s ist bereits in Patch $patch\n Datei %s wird nicht verändert.\n Datei %s ist nicht in Patch %s enthalten\n Datei %s ist möglicherweise defekt\n Datei %s wird von Patch %s verändert\n Datei %s aus Patch %s entfernt\n Die Datei %s befindet sich unter dem Verzeichnis %s\n series-Datei vollständig angewandt, endet in Patch %s\n Neue Version von %s erstellt als %s\n Fehler beim Aufspalten von %s auf %s\n Importiere Patch %s (abgelegt als %s)\n Unterbrechung durch Benutzer; Patch %s wurde nicht angewandt.\n Später angewandte Patches verändern Dateien von %s. Aktalisieren mit -f erzwingen.\n Später angewandte Patches verändern Dateien von %s\n Kein Patch %s\n Kein Patch entfernt\n Keine Patches angewandt\n Patch %s ist leer\n Jetzt in Patch %s\n Die Option \`-n' kann nur beim Importieren eines einzelnen Patches verwendet werden\n Die Optionen \`-c patch', \`--snapshot', und \`-z' können nicht kombiniert werden.\n Patch %s scheint leer zu sein; wird entfernt\n Patch %s scheint leer zu sein; angewandt\n Patch %s lässt sich nicht anwenden (erzwingen mit -f)\n Patch %s existiert nicht; wende leeren patch an\n Patch %s kann nicht entfernt werden (Patch aktualisieren oder entfernen erzwingen mit -f)\n Patch %s existiert bereits, bitte einen anderen Namen wählen\n Patch %s existiert bereits, bitte neuen Namen wählen\n Patch %s existiert bereits\n Patch %s existiert bereits. Ersetzen mit -f.\n Patch %s ist bereits angewandt\n Patch %s ist angewandt\n Patch %s ist momentan angewandt\n Patch %s ist nicht angewandt\n Patch %s ist nicht in der series-Datei enthalten\n Patch %s ist nicht in der series-Datei enthalten\n Der oberste Patch ist jetzt %s\n Patch %s ist unverändert\n Patch %s muss zuerst aktualisiert werden (Refresh).\n Patch %s ist nicht vor Patch %s angewandt\n Patch %s nicht in series-Datei gefunden\n Patch %s auf %s umbenannt\n Patch ist nicht angewandt\n Patch %s aktualisiert\n Entferne Patch %s\n Entferne patch %s\n Entferne abschliessende Leerzeichen in Zeile %s von %s
 Entferne abschliessende Leerzeichen in den Zeilen %s von %s
 Umbenennen von %s auf %s: %s
 Fehler beim Umbenennen von Patch %s auf %s\n Header von Patch %s ersetzt\n Ersetze Patch %s durch neuere Version\n Aufruf: %s [-p num] [-n] [patch]
 Der %%prep-Abschnitt von %s ist fehlgeschlagen; die Ergebnisse sind möglicherweise unvollständig\n Die Option -v zeigt die Ausgaben von rpm an\n Die Metadaten in $QUILT_PC/ sind bereits in Format Version $DB_VERSION; nichts zu tun\n Die quilt-Metadaten in diesem Arbeitsverzeichnis haben Version $version, aber diese Version von quilt kann nur mit Metadaten in den Versionen $DB_VERSION und darunter umgehen. Bitte entfernen Sie vor dem Downgrade von quilt alle Patches mit der Version von quilt, die zur Erstellung des Arbeitsverzeichnisses verwendet wurde.\n Der oberste Patch %s muss zuerst aktualisiert werden (Refresh).\n Das Arbeitsverzeichnis wurde von einer älteren Version von quilt erstellt. Bitte führen Sie \`quilt upgrade' aus.\n Verwendung: %s {-s|-u} Abschnitt Datei [< Ersetzung]
 Entpacke Archiv %s\n Verwendung: quilt [--trace[=verbose]] [--quiltrc=XX] befehl [-h] ... Verwendung: quilt add [-p patch] {datei} ...\n Verwendung: quilt annotate {datei}\n Verwendung: quilt applied [patch]\n Verwendung: quilt delete [patch | -n]\n Verwendung: quilt diff [-p n] [-u|-U num|-c|-C num] [--combine patch|-z] [-R] [-P patch] [--snapshot] [--diff=programm] [--no-timestamps] [--no-index] [--sort] [--color] [datei ...]\n Verwendung: quilt edit datei ...\n Verwendung: quilt files [-v] [-a] [-l] [--combine patch] [patch]\n Verwendung: quilt fold [-p strip-ebenen]\n Verwendung: quilt fork [neuer_name]\n Verwendung: quilt graph [--all] [--reduce] [--lines[=num]] [--edge-labels=files]\n Verwendung: quilt grep [-h|options] {suchausdruck}\n Verwendung: quilt header [-a|-r|-e] [--backup] [--strip-diffstat] [--strip-trailing-whitespace] [patch]\n Verwendung: quilt import [-f] [-p num] [-n patch] [patchdatei] ...\n Verwendung: quilt mail {--mbox datei|--send} [--from ...] [--to ...] [--cc ...] [--bcc ...] [--subject ...]\n Verwendung: new {patchname}\n Verwendung: quilt next [patch]\n Verwendung: quilt patches {file}\n Verwendung: quilt pop [-afRqv] [num|patch]\n Verwendung: quilt previous [patch]\n Verwendung: quilt push [-afqv] [--leave-rejects] [num|patch]\n Verwendung: quilt refresh [-p n] [-f] [--no-timestamps] [--no-index] [--diffstat] [--sort] [--backup] [--strip-trailing-whitespace] [patch]\n Verwendung: quilt remove [-p patch] {datei} ...\n Verwendung: quilt rename [-p patch] neuer_name\n Verwendung: quilt series [-v]\n Verwendung: quilt setup [-d Pfad-Präfix] [-v] {Spec-Datei|Series-Datei}\n Verwendung: quilt snapshot [-d]\n Verwendung: quilt top\n Verwendung: quilt unapplied [patch]\n Verwendung: quilt upgrade\n Warnung: abschliessende Leerzeichen in Zeile %s von %s
 Warnung: abschliessende Leerzeichen in den Zeilen %s von %s
 