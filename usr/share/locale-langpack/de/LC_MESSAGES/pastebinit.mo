��            )   �      �     �  "   �  %   �            0   -      ^  :     &   �  %   �          $     A     a      {  !   �      �     �     �  4     J   B  C   �  5   �       E        b  y   z  R   �  2   G  �  z     9	  $   W	  +   |	     �	     �	  9   �	  %   
  S   4
  *   �
  *   �
      �
     �
  !        A  0   _  -   �  -   �     �        G   $  S   l  U   �  6        M  X   f     �  �   �  o   �  5   &                                              
                                                                                 	          	-a <author:default is '%s'> 	-b <pastebin url:default is '%s'> 	-f <format of paste:default is '%s'> 	-h This help screen 	-i <input file> 	-j <jabberid for notifications:default is '%s'> 	-l List all supported pastebins 	-m <permatag for all versions of a post:default is blank> 	-r <parent posts ID:defaults to none> 	-t <title of paste:default is blank> 	-u <username> -p <password> 	-v Print the version number %s: no 'basename' in [pastebin] %s: no section [pastebin] Could not find any json library. Error parsing configuration file! Failed to contact the server: %s Invalid arguments!
 KeyboardInterrupt caught. Optional arguments (not supported by all pastebins): Please ensure that your configuration file looks similar to the following: Reads on stdin for input or takes a list of filenames as parameters Return the parameters array for the selected pastebin Supported pastebins: The content you are trying to send exceeds the pastebin's size limit. Unable to read from: %s Unable to read or parse the result page, it could be a server timeout or a change server side, try with another pastebin. Unknown website, please post a bugreport to request this pastebin to be added (%s) You are trying to send an empty document, exiting. Project-Id-Version: pastebinit 0.8.1
Report-Msgid-Bugs-To: pastebinit@packages.debian.org
POT-Creation-Date: 2016-03-03 07:58+0000
PO-Revision-Date: 2016-02-04 19:24+0000
Last-Translator: Rolf Leggewie <Unknown>
Language-Team: German <debian-l10n-german@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:12+0000
X-Generator: Launchpad (build 18115)
 	-a <Autor: Vorgabe ist '%s'> 	-b <Pastebin-URL: Vorgabe ist '%s'> 	-f<Format des Dokuments: Vorgabe ist '%s'> 	-h Diese Hilfe anzeigen 	-i <Eingabedatei> 	-j <Jabber-ID für Benachrichtigungen: Vorgabe ist '%s'> 	-l Liste der unterstützen Pastebins 	-m <Dauerhafte Kennzeichnung für alle Versionen eines Beitrags: Vorgabe ist leer> 	-r <ID des Elternbeitrags: keine Vorgabe> 	-t <Titel des Beitrags: Vorgabe ist leer> 	-u <Benutzername> -p <Passwort> 	-v Gibt die Versionsnummer aus %s: Kein 'basename' in [pastebin] %s: kein Abschnitt [pastebin] Es konnte keine JSON-Bibliothek gefunden werden. Fehler beim Auslesen der Konfigurationsdatei! Konnte keinen Kontakt zum Server %s aufnehmen Ungültige Argumente!
 Beendet durch manuellen Abbruch. Zusätzliche Argumente, die nicht von allen Pastebin verstanden werden: Bitte stellen Sie sicher, dass Ihre Konfigurationsdatei in etwa wie folgt aussieht: Wartet auf Eingaben via stdin oder akzeptiert eine Liste von Dateinamen als Argumente Die Parameter für den ausgewählten Pastebin ausgeben Unterstützte Pastebins: Der von Ihnen gesendete Inhalt überschreitet das Größenlimit des gewählten Pastebin. Lesen nicht möglich von: %s Die Ergebnisseite kann nicht gelesen oder ausgewertet werden, dies könnte durch eine Zeitüberschreitung des Servers oder eine Server-seitige Veränderung verursacht worden sein. Versuchen Sie einen anderen Pastebin. Unbekannte Webseite.  Bitte senden Sie einen Fehlerbericht, damit dieser Pastebin hinzugefügt werden kann (%s) Sie versuchen ein leeres Dokument zu senden, Abbruch. 