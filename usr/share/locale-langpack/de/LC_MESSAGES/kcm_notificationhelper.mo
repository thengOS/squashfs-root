��          �      ,      �  :   �     �  5   �               8  )   H     r     �     �     �     �     �               )  �  H  :   �     +  =   ?     }  $   �     �  2   �     �  %     $   3     X  &   j  !   �  1   �     �  U                                                   	             
                 (C) 2009-2010 Jonathan Thomas, (C) 2009-2014 Harald Sitter Application crashes Configure the behavior of Kubuntu Notification Helper Harald Sitter Incomplete language support Jonathan Thomas Kubuntu Notification Helper Configuration Notification type: Popup notifications only Proprietary Driver availability Required reboots Restricted codec availability Show notifications for: Tray icons only Upgrade information Use both popups and tray icons Project-Id-Version: kubuntu-notification-helper
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-04-01 17:07+0000
PO-Revision-Date: 2015-05-25 09:44+0000
Last-Translator: Phillip Sz <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:48+0000
X-Generator: Launchpad (build 18115)
 (C) 2009-2010 Jonathan Thomas, (C) 2009-2014 Harald Sitter Anwendungsabstürze Das Verhalten des Kubuntu-Benachrichtigungshelfers einstellen Harald Sitter Unvollständige Sprachunterstützung Jonathan Thomas Einstellungen für Kubuntu-Benachrichtigungshelfer Art der Benachrichtigung: Nur Hinweisfenster-Benachrichtigungen Verfügbarkeit proprietärer Treiber Nötige Neustarts Verfügbarkeit eingeschränkter Codecs Benachrichtigungen anzeigen für: Nur Symbole im Systemabschnitt der Kontrollleiste Aktualisierungsinformationen Sowohl Hinweisfenster als auch Symbole im Systemabschnitt der Kontrolleiste verwenden 