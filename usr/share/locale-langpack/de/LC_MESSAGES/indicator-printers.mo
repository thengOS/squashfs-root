��          �      �       H  (   I  '   r     �     �     �  K   �  +     %   3  %   Y  %     %   �  b   �     .  �  ;  /     ,   A  
   n     y     �  ]   �  1   �  .     -   M  (   {  )   �  �   �     T                          	                     
                  A cover is open on the printer “%s”. A door is open on the printer “%s”. Paused Printers Printing Problem The printer “%s” can’t be used, because required software is missing. The printer “%s” is currently off-line. The printer “%s” is low on paper. The printer “%s” is low on toner. The printer “%s” is out of paper. The printer “%s” is out of toner. You have %d job queued to print on this printer. You have %d jobs queued to print on this printer. _Settings… Project-Id-Version: indicator-printers
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-04 13:07+0000
PO-Revision-Date: 2015-10-25 17:15+0000
Last-Translator: schuko24 <gerdsaenger@t-online.de>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 19:14+0000
X-Generator: Launchpad (build 18115)
 Am Drucker »%s« ist eine Abdeckung geöffnet. Am Drucker »%s« ist eine Klappe geöffnet. Angehalten Drucker Druckproblem Der Drucker »%s« kann nicht verwendet werden, weil die erforderliche Software dafür fehlt. Der Drucker »%s« ist zur Zeit nicht verfügbar. Papiervorrat für Drucker »%s« geht zu Ende. Tonervorrat für Drucker »%s« geht zu Ende. Der Drucker »%s« hat kein Papier mehr. Der Drucker »%s« hat keinen Toner mehr. Es befindet sich %d Auftrag in der Warteschlange dieses Druckers. Es befinden sich %d Aufträge in der Warteschlange dieses Druckers. _Einstellungen … 