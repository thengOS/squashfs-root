��          |      �          &   !  '   H  A   p  >   �  >   �  +   0  9   \     �  6   �  �   �  v   �  �  D  .   �  -   +  H   Y  Z   �  `   �  1   ^  F   �  &   �  \   �  �   [  y   S	                   
                                    	    Change Screen Resolution Configuration Change the effect of Ctrl+Alt+Backspace Changing the Screen Resolution configuration requires privileges. Changing the effect of Ctrl+Alt+Backspace requires privileges. Could not connect to Monitor Resolution Settings DBUS service. Enable or disable the NVIDIA GPU with PRIME Enabling or disabling the NVIDIA GPU requires privileges. Monitor Resolution Settings Monitor Resolution Settings can't apply your settings. Monitor Resolution Settings has detected that the virtual resolution must be set in your configuration file in order to apply your settings.

Would you like Screen Resolution to set the virtual resolution for you? (Recommended) Please log out and log back in again.  You will then be able to use Monitor Resolution Settings to setup your monitors Project-Id-Version: screen-resolution-extra
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-18 12:57+0000
PO-Revision-Date: 2013-12-18 14:35+0000
Last-Translator: Mele Coronato <portals@melecoronato.com>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:34+0000
X-Generator: Launchpad (build 18115)
 Einstellungen der Bildschirmauflösung ändern Verhalten von »Strg+Alt+Rücktaste« ändern Das Ändern der Bildschirmauflösung erfordert bestimmte Zugriffsrechte. Das Ändern des Verhaltens von »Strg+Alt+Rücktaste« erfordert bestimmte Zugriffsrechte. Die Verbindung zum DBUS-Dienst für die Einstellung der Bildschirmauflösung ist fehlgeschlagen. NVIDIA-GPU mit PRIME aktivieren oder deaktivieren Aktivierung oder Deaktivierung der NVIDIA-GPU erfordert Schreibrechte. Einstellungen der Bildschirmauflösung Die gewählten Einstellungen für die Bildschirmauflösung konnten nicht übernommen werden. Es wurde festgestellt, dass die virtuelle Auflösung in Ihrer Konfigurationsdatei geändert werden muss, damit Ihre Einstellungen übernommen werden können.

Soll die virtuelle Auflösung in die Konfigurationsdatei eingetragen werden? (Empfohlen) Bitte abmelden und erneut anmelden. Danach können Sie die Bildschirmauflösung zur Einstellung Ihrer Monitore verwenden. 