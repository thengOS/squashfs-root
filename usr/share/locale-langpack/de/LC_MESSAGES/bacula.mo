��            )   �      �  3  �  3   �     	  B        S  7   [     �     �     �  ,   �     �        (   0  .   Y     �     �  #   �     �     �     �  &   	     0  &   ?  +   f     �     �  +   �  +   �  +   #  �  O  A  	  E   F
     �
  ^   �
     �
  @   �
     =     V     j  @   |     �  0   �  (     9   *     d     |  )   �     �     �     �  .        1  .   @  1   o     �     �  /   �  /     /   >             
                                                                                                                 	           
Version: %s (%s) %s %s %s

Usage: bat [-s] [-c config_file] [-d debug_level] [config_file]
       -c <file>   set configuration file to file
       -dnn        set debug level to nn
       -s          no signals
       -t          test - read configuration and exit
       -?          print this message.

 %s item is required in %s resource, but not found.
 *None* Attempt to define second %s resource named "%s" is not permitted.
 Bacula  Bad response from File daemon to Hello command: ERR=%s
 Command completed ... Command failed. Console: name=%s
 Cryptography library initialization failed.
 Director disconnected. Director rejected Hello command
 Director: name=%s address=%s DIRport=%d
 Error sending Hello to Storage daemon. ERR=%s
 Failed ASSERT: %s
 File daemon File daemon rejected Hello command
 Initializing ... No %s resource defined
 No record for %d %s
 Please correct configuration file: %s
 Storage daemon Storage daemon rejected Hello command
 TLS required but not configured in Bacula.
 Too many items in %s resource
 Unknown resource type %d
 Unknown resource type %d in dump_resource.
 Unknown resource type %d in free_resource.
 Unknown resource type %d in save_resource.
 Project-Id-Version: de
Report-Msgid-Bugs-To: bacula-devel@lists.sourceforge.net
POT-Creation-Date: 2014-07-29 18:18+0200
PO-Revision-Date: 2016-02-11 03:38+0000
Last-Translator: Kern Sibbald <kern@sibbald.com>
Language-Team: German bacula-devel
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:24+0000
X-Generator: Launchpad (build 18115)
Language: German
 
Version: %s (%s) %s %s %s

Aufruf: bat [-s] [-c Konfig_datei] [-d Fehlerdiagnoseebene] [Konfig_datei]
       -c <Datei> Datei als Konfigurationsdatei festlegen
       -dnn Fehlerdiagnoseebene auf nn festlegen
       -s Keine Signale
       -t test – Konfiguration lesen und beenden
       -? Diese Nachricht anzeigen

 %s wird in %s-Resource benötigt, konnte aber nicht gefunden werden.
 *Nichts* Der Versuch der Definition einer zweiten %s-Ressource mit dem Namen »%s« ist nicht erlaubt.
 Bacula  Fehlerhafte Antwort von  File daemon auf Hello Kommando: ERR=%s
 Befehl abgeschlossen … Befehl gescheitert. Konsole: Name=%s
 Initialisierung der Verschlüsselungsbibliothek fehlgeschlagen.
 Direktor getrennt. Die Weiterleitung wies den Hallo-Befehl zurück
 Direktor: Name=%s Adresse=%s DIRport=%d
 Fehler beim senden von "Hello" an Storage daemon. ERR=%s
 ASSERT gescheitert: %s
 File daemon File daemon hat Hello Kommando abgelehnt
 Initialisierung läuft … Keine %s resource definiert
 Kein Eintrag für %d%s
 Bitte die Konfigurationsdatei korrigieren: %s
 Storage daemon Storage daemon hat Hello Kommando abgewiesen.
 TLS benötigt aber nicht konfiguriert in Bacula.
 zu viele items in %s resource
 Unbekannter Ressourcentyp %d
 Unbekannter resource type %d in dump_resource.
 Unbekannter resource type %d in free_resource.
 Unbekannter resource type %d in save_resource.
 