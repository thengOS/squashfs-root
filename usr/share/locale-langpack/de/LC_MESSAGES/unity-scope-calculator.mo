��          \      �       �   	   �   
   �      �      �   3   �   �   ,  !   �  �    	   �     �     �     �  U     �   ]  (   N                                       Calculate Calculator Open in calculator Result Sorry, there are no results that match your search. This is an Ubuntu search plugin that enables Calculator results to be displayed in the Dash underneath the Info header. If you do not wish to search this content source, you can disable this search plugin. calculator;result;calculate;calc; Project-Id-Version: unity-scope-calculator
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-28 09:22+0000
PO-Revision-Date: 2015-08-19 19:15+0000
Last-Translator: Ivan Panchenko <pivan@opmbx.org>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:07+0000
X-Generator: Launchpad (build 18115)
 Berechnen Taschenrechner Im Taschenrechner öffnen Ergebnis Entschuldigung, es gibt leider keine Ergebnisse, die mit Ihrer Suche übereinstimmen. Dies ist eine Ubuntu-Such-Erweiterung, welche es ermöglicht Taschenrechner-Ergebnisse in der Dash unterhalb der Informations-Kopfzeile anzuzeigen. Falls Sie diese Quelle nicht durchsuchen wollen, können Sie diese Erweiterung deaktivieren. Taschenrechner;Ergebnis;rechnen;Rechner; 