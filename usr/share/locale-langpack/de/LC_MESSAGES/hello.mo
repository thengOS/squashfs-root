��            )         �  l   �  n        �  0   �     �  ,   �  ,     ,   H  '   u  -   �      �  (   �  (        >     ^     ~  �   �  ?   x     �  )   �     �               0     G  '   I     q          �     �  �  �  �   v	  r   �	     n
  /   �
  "   �
  ,   �
  3     ,   5  *   b  -   �  &   �  -   �  -     !   >  !   `     �  �   �  N   �     �  5   �  n     h   �     �            3        R     `     l     �                  	                                      
                                                                                  -h, --help          display this help and exit
  -v, --version       display version information and exit
   -t, --traditional       use traditional greeting
  -g, --greeting=TEXT     use TEXT as the greeting message
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' Copyright (C) %d Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
 General help using GNU software: <http://www.gnu.org/gethelp/>
 Hello, world! Print a friendly, customizable greeting.
 Report %s bugs to: %s
 Report bugs to: %s
 Unknown system error Usage: %s [OPTION]...
 ` conversion to a multibyte string failed extra operand hello, world memory exhausted write error Project-Id-Version: GNU hello 2.9
Report-Msgid-Bugs-To: bug-hello@gnu.org
POT-Creation-Date: 2014-11-16 11:53+0000
PO-Revision-Date: 2015-08-27 18:57+0000
Last-Translator: schuko24 <gerdsaenger@t-online.de>
Language-Team: German <translation-team-de@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:33+0000
X-Generator: Launchpad (build 18115)
Language: de
   -h, --help          Diese Hilfe anzeigen und Programm beenden
  -v, --version       Programmversion anzeigen und Programm beenden
   -t, --traditional       übliche Grußformel verwenden
  -g, --greeting=TEXT     TEXT als Grußformel verwenden
 %s-Homepage: <%s>
 %s-Homepage: <http://www.gnu.org/software/%s/>
 %s: Unerlaubte Option -- „%c“
 %s: Option „%c%s“ erlaubt kein Argument
 %s: Option „%s“ ist mehrdeutig; Möglichkeiten: %s: Option „--%s“ erlaubt kein Argument
 %s: Option „%s“ verlangt ein Argument
 %s: Option „-W %s“ erlaubt kein Argument
 %s: Option „-W %s“ ist mehrdeutig
 %s: Option „-W %s“ verlangt ein Argument
 %s: Option verlangt ein Argument -- „%c“
 %s: Unbekannte Option „%c%s“
 %s: Unbekannte Option „--%s“
 « Copyright (C) %d Free Software Foundation, Inc.
Lizenz: GPLv3+: GNU GPL Version 3 oder neuer <http://gnu.org/licenses/gpl.html>
Dies ist Freie Software: Sie dürfen sie ändern und weiterverteilen.
Es gibt KEINE GARANTIE, soweit rechtlich zulässig.
 Allgemeine Hilfe zum Benutzen von GNU-Software: <http://www.gnu.org/gethelp/>
 Hallo, Welt! Gibt eine freundliche, einstellbare Begrüßung aus.
 Melden Sie Fehler in %s an: %s
Melden Sie Übersetzungsfehler an: <translation-team-de@lists.sourceforge.net>
 Melden Sie Fehler an: %s
Melden Sie Übersetzungsfehler an: <translation-team-de@lists.sourceforge.net>
 Unbekannter Systemfehler Aufruf: %s [OPTION]...
 » Umwandlung in einen Multibyte-String fehlgeschlagen Zusatzoperand hallo, Welt Arbeitsspeicher voll. Schreibfehler 