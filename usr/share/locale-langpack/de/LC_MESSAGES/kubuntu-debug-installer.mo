��          �            h     i     �  o   �  b     U   j  )   �  *   �  *     %   @     f      ~      �     �     �  �  �     �     �  R   �  y     [   �      �       5   -  Z   c  +   �     �  @        G  �   U                          	                             
                         (C) 2010 Harald Sitter @action:buttonInstall @infoDo you want to install the following debug packages so that the necessary debug symbols become available? @infoNo debug packages could be found for the files listed below.
Do you want to continue anyway? @info Error messageNo file paths were provided, so no debug packages could be found. @info:progressLooking for debug packages @title:windowConfirm package installation @title:windowCouldn't find debug packages A debug package installer for Kubuntu Debug package installer EMAIL OF TRANSLATORSYour emails Files to find debug packages for Harald Sitter NAME OF TRANSLATORSYour names Project-Id-Version: kubuntu-debug-installer
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-19 03:52+0000
PO-Revision-Date: 2013-10-10 19:23+0000
Last-Translator: Jonathan Riddell <riddell@gmail.com>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:50+0000
X-Generator: Launchpad (build 18115)
 (C) 2010 Harald Sitter Installieren Sollen die folgenden Pakete installiert werden, um Fehleranalysen zu ermöglichen? Für die unten aufgelisteten Pakete konnten keine Fehleranalyse-Pakete gefunden werden.
Möchten Sie trotzdem fortfahren? Es wurden keine Dateipfade angegeben, also konnte kein Fehlerdiagnosepaket gefunden werden. Suche nach Fehleranalyse-Paketen Paketinstallation bestätigen Es konnten keine Fehleranalyse-Pakete gefunden werden Ein Installationsprogramm für die Installation von Fehleranalysations-Paketen mit Kubuntu Installationsprogramm für Debugging-Pakete ,,,,,,,,riddell@gmail.com,, Datein für die Fehleranalysations-Pakete gefunden werden sollen Harald Sitter ,Launchpad Contributions:,Hendrik Knackstedt,Moritz Baumann,sputnik, ,Launchpad Contributions:,Hendrik Knackstedt,Jonathan Riddell,Moritz Baumann,sputnik 