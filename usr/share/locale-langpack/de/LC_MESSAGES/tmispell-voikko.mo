��            )   �      �     �  <   �  9   �  3   (     \  /   y     �     �     �  2   �  '   #     K     h  $   w     �  0   �  0   �          3     P     k     }  7  �  �  �  M   y     �     �     �  �  �  4   �  K   �  .     F   >  (   �  8   �  	   �  4   �  !   &  @   H  5   �  $   �     �  2   �  $   (  B   M  F   �  /   �  /     +   7     c  <   x  M  �  �    Q   �!     �!     
"     "         
                      	                                                                                                           -- Press any key to continue -- An Ispell program was not given in the configuration file %s Are you sure you want to throw away your changes? (y/n):  Conversion of '%s' to character set '%s' failed: %s Error initialising libvoikko Error initializing character set conversion: %s File: %s Incomplete spell checker entry Missing argument for option %s Parse error in file "%s" on line %d, column %d: %s Parse error in file "%s" on line %d: %s Parse error in file "%s": %s Replace with:  Unable to open configuration file %s Unable to open file %s Unable to open file %s for reading a dictionary. Unable to open file %s for writing a dictionary. Unable to open temporary file Unable to set encoding to %s Unable to write to file %s Unknown option %s Unterminated quoted string Usage: %s [options] [file]...
Options: [FMNLVlfsaAtnhgbxBCPmSdpwWTv]

 -F <file>  Use given file as the configuration file.

The following flags are same for ispell:
 -v[v]      Print version number and exit.
 -M         One-line mini menu at the bottom of the screen.
 -N         No mini menu at the bottom of the screen.
 -L <num>   Number of context lines.
 -V         Use "cat -v" style for characters not in the 7-bit ANSI
            character set.
 -l         Only output a list of misspelled words.
 -f <file>  Specify the output file.
 -s         Issue SIGTSTP at every end of line.
 -a         Read commands.
 -A         Read commands and enable a command to include a file.
 -e[e1234]  Expand affixes.
 -c         Compress affixes.
 -D         Dump affix tables.
 -t         The input is in TeX format.
 -n         The input is in [nt]roff format.
 -h         The input is in sgml format.
 -b         Create backup files.
 -x         Do not create backup files.
 -B         Do not allow run-together words.
 -C         Allow run-together words.
 -P         Do not generate extra root/affix combinations.
 -m         Allow root/affix combinations that are not in dictionary.
 -S         Sort the list of guesses by probable correctness.
 -d <dict>  Specify an alternate dictionary file.
 -p <file>  Specify an alternate personal dictionary.
 -w <chars> Specify additional characters that can be part of a word.
 -W <len>   Consider words shorter than this always correct.
 -T <fmt>   Assume a given formatter type for all files.
 -r <cset>  Specify the character set of the input.
 Whenever an unrecognized word is found, it is printed on
a line on the screen. If there are suggested corrections
they are listed with a number next to each one. You have
the option of replacing the word completely, or choosing
one of the suggested words. Alternatively, you can ignore
this word, ignore all its occurrences or add it in the
personal dictionary.

Commands are:
 r       Replace the misspelled word completely.
 space   Accept the word this time only.
 a       Accept the word for the rest of this session.
 i       Accept the word, and put it in your personal dictionary.
 u       Accept and add lowercase version to personal dictionary.
 0-9     Replace with one of the suggested words.
 x       Write the rest of this file, ignoring misspellings,
         and start next file.
 q       Quit immediately.  Asks for confirmation.
         Leaves file unchanged.
 ^Z      Suspend program.
 ?       Show this help screen.
 [SP] <number> R)epl A)ccept I)nsert L)ookup U)ncap Q)uit e(X)it or ? for help \ at the end of a string aiuqxr yn Project-Id-Version: tmispell-voikko
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-02-07 18:46+0200
PO-Revision-Date: 2012-06-12 20:04+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:10+0000
X-Generator: Launchpad (build 18115)
 - Drücken Sie eine beliebige Taste zum Fortfahren - Eine Ispell-Anwendung wurde in der Konfigurationsdatei %s nicht festgelegt. Wollen Sie Ihre Änderungen verwerfen? (j/n):  Umwandlung von »%s« in den Zeichensatz »%s« ist fehlgeschlagen: %s Fehler beim Initialisieren von libvoikko Fehler beim Initialisieren der Zeichensatzumwandlung: %s Datei: %s Unvollständiger Eintrag in der Rechtschreibprüfung Fehlendes Argument für Option %s Einlesefehler in der Datei »%s« in der Zeile %d, Spalte %d: %s Einlesefehler in der Datei »%s« in der Zeile %d: %s Einlesefehler in der Datei »%s«:%s Ersetzen durch:  Konfigurationsdatei %s kann nicht geöffnet werden Datei %s kann nicht geöffnet werden Datei %s konnte nicht zum Lesen des Wörterbuchs geöffnet werden. Datei %s konnte nicht zum Schreiben des Wörterbuchs geöffnet werden. Temporäre Datei %s kann nicht geöffnet werden Kodierung konnte nicht auf %s festgelegt werden In Datei %s konnte nicht geschrieben werden Unbekannte Option %s Fehlendes schließendes Anführungszeichen für Zeichenkette Aufruf: %s [Optionen] [Datei] …
Optionen: [FMNLVlfsaAtnhgbxBCPmSdpwWTv]

 -F <Datei>  Angegebene Datei als Konfigurationsdatei verwenden.

Die folgenden Optionen sind die gleichen wie für ispell:
 -v[v]      Versionsnummer anzeigen und beenden.
 -M         Einzeiliges Minimenü am unteren Bildschirmrand anzeigen.
 -N         Kein Minimenü am unteren Bildschirmrand.
 -L <Num>   Anzahl an Inhaltszeilen.
 -V         »cat -v«-Stil für Zeichen verwenden, die nicht aus dem 7-bit-ANSI-
            Zeichensatz stammen.
 -l         Nur eine Liste falsch geschriebener Wörter anzeigen.
 -f <Datei>  Die Ausgabedatei festlegen.
 -s         SIGTSTP am Ende jeder Zeile ausgeben.
 -a         Befehle lesen.
 -A         Befehle lesen und einem Befehl das Hinzufügen einer Datei ermöglichen.
 -e[e1234]  Affixe erweitern.
 -c         Affixe komprimieren.
 -D         Affix-Tabellen ausgeben.
 -t         Die Eingabe ist im TeX-Format.
 -n         Die Eingabe ist im [nt]roff-Format.
 -h         Die Eingabe ist im sgml-Format.
 -b         Sicherungsdateien anlegen.
 -x         Keine Sicherungsdateien anlegen.
 -B         Zusammenlaufende Wörter nicht erlauben.
 -C         Zusammenlaufende Wörter erlauben.
 -P         Keine zusätzlichen Stamm/Affix-Kombinationen erzeugen.
 -m         Stamm/Affix-Kombinationen erlauben, die nicht im Wörterbuch sind.
 -S         Die Liste der Vermutungen nach wahrscheinlicher Genauigkeit ordnen.
 -d <Wört>  Eine alternative Wörterbuchdatei festlegen.
 -p <Datei>  Ein alternatives persönliches Wörterbuch festlegen.
 -w <Zeichen> Zusätzliche Zeichen festlegen, die Teil eines Wortes sein können.
 -W <Länge>   Wörter, die kürzer sind als die angegebene Länge, automatisch als richtig werten.
 -T <Fmt>   Eine angegebene Formatierungsart für alle Dateien annehmen.
 -r <Zsatz>  Den Zeichensatz der Eingabe angeben.
 Immer wenn ein unbekanntes Wort gefunden wird, wird es
in einer Zeile auf dem Bildschirm angezeigt. Falls es
Verbesserungsvorschläge für das Wort gibt, werden diese
mit einer Nummer versehen angezeigt. Sie haben die Möglichkeit
das Wort vollständig zu ersetzen oder eins der vorgeschlagenen
Wörter auszuwählen. Alternativ können Sie das Wort ignorieren,
alle Vorkommnisse des Wortes ignorieren oder das Wort zu
Ihrem persönlichen Wörterbuch hinzufügen.

Befehle sind:
 r       Das falsch geschriebene Wort vollständig ersetzen.
 space   Das Wort nur dieses Mal akzeptieren.
 a       Das Wort für den Rest der Sitzung akzeptieren.
 i       Das Wort akzeptieren und dem persönlichen Wörterbuch hinzufügen.
 u       Das Wort akzeptieren und die kleingeschriebene Variante zum persönlichen Wörterbuch hinzufügen.
 0-9     Das Wort mit einem der vorgeschlagenen Wörter ersetzen.
 x       Den Rest dieser Datei schreiben, Rechtschreibfehler ignorieren
         und die nächste Datei beginnen.
 q       Sofort beenden.  Fragt nach Bestätigung.
         Datei bleibt unverändert.
 ^Z      Programm in Bereitschaft versetzen.
 ?       Diesen Hilfetext anzeigen.
 [SP] <Nr.> R=Antw. A=Akzep. I=Einf. L=Anzeigen U=Öff. Q=Ende X=Verlassen ?=Hilfe \ am Ende einer Zeichenkette aiuqxr jn 