��          �      \      �     �     �                     '  &   6     ]     f     w     �  (   �  ,   �     �  	          !   &     H     U    ]  %   a     �     �     �     �     �  '   �               +     @  &   Y  &   �     �     �     �  +   �       	                                     
                     	                                            Automatic login in %d seconds Change _Language Change _Session Default Failsafe xterm Login as Guest No response from server, restarting... Password Select _Host ... Select _Language ... Select _Session ... Select the host for your session to use: Select the language for your session to use: Select your session manager: Shut_down Username Verifying password.  Please wait. _Preferences _Reboot Project-Id-Version: ldm 2.1.1
Report-Msgid-Bugs-To: sbalneav@ltsp.org
POT-Creation-Date: 2013-11-24 08:57+0200
PO-Revision-Date: 2012-03-12 22:49+0000
Last-Translator: Ghenrik <Unknown>
Language-Team: German <debian-l10n-german@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:26+0000
X-Generator: Launchpad (build 18115)
X-Poedit-Country: GERMANY
Language: de
X-Poedit-Language: German
X-Poedit-SourceCharset: utf-8
 Automatische Anmeldung in %d Sekunden _Sprache wechseln _Sitzungsart ändern Voreinstellung Xterm als Notlösung Anmeldung als Gast Keine Antwort des Servers, Neustart … Passwort _Rechner wählen … _Sprache wählen … _Sitzungsart wählen … Rechner für diese Sitzung auswählen: Sprache für diese Sitzung auswählen: Sitzungsmanager auswählen: _Ausschalten Benutzer Passwort wird überprüft. Bitte warten … _Einstellungen _Neustart 