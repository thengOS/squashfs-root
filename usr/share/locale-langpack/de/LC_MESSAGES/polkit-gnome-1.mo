��          �      ,      �     �     �     �     �  �   �  �   �  ~        �  /   �     �     �     �  ?        C     Q  
   c    n     {     �     �     �  �   �  �   �  �   =     �  1   �     	     (	     B	  N   W	     �	     �	  
   �	        	                                                                  
    %s (%s) <small><b>Action:</b></small> <small><b>Vendor:</b></small> <small><b>_Details</b></small> An application is attempting to perform an action that requires privileges. Authentication as one of the users below is required to perform this action. An application is attempting to perform an action that requires privileges. Authentication as the super user is required to perform this action. An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Authenticate Authentication dialog was dismissed by the user Click to edit %s Click to open %s Select user... Your authentication attempt was unsuccessful. Please try again. _Authenticate _Password for %s: _Password: Project-Id-Version: policykit-gnome
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=policykit-gnome&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-04 00:05+0000
PO-Revision-Date: 2015-12-04 21:27+0000
Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>
Language-Team: German <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:41+0000
X-Generator: Launchpad (build 18115)
 %s (%s) <small><b>Aktion:</b></small> <small><b>Anbieter:</b></small> <small><b>_Details</b></small> Eine Anwendung versucht eine Aktion auszuführen, die Privilegien erfordert. Eine Legitimation als einer der unten aufgelisteten Benutzer ist erforderlich, um diese Aktion auszuführen. Eine Anwendung versucht eine Aktion auszuführen, die Privilegien erfordert. Eine Legitimation als Systemverwalter ist erforderlich, um diese Aktion auszuführen. Eine Anwendung versucht eine Aktion auszuführen, die Privilegien erfordert. Eine Legitimation ist erforderlich, um diese Aktion auszuführen. Legitimieren Legitimierungsdialog wurde vom Nutzer geschlossen Klicken, um %s zu bearbeiten Klicken, um %s zu öffnen Benutzer wählen … Ihr Legitimationsversuch war nicht erfolgreich. Bitte versuchen Sie es erneut. _Legitimieren _Passwort für %s: _Passwort: 