��          t      �                      .  "   A      d  7   �  c   �  )   !  R   K  0   �  �  �     g     m     �  %   �  )   �  >   �  }   (  .   �  h   �  E   >           	                              
                   %(2)s Internal error: %(1)s. Permission denied. The %(1)s handle %(2)s is invalid. The method %(1)s is unsupported. The method %(1)s takes %(2)s argument(s) (%(3)s given). The network you specified already has a PIF attached to it, and so another one may not be attached. This map already contains %(1)s -> %(2)s. Value "%(2)s" for %(1)s is not supported by this server.  The server said "%(3)s". You attempted an operation that was not allowed. Project-Id-Version: xen
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-03-31 17:40+0100
PO-Revision-Date: 2012-02-20 17:16+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:40+0000
X-Generator: Launchpad (build 18115)
 %(2)s Interner Fehler: %(1)s. Zugriff verweigert. Das %(1)s-Handle %(2)s ist ungültig. Die Methode %(1)s wird nicht unterstützt Die Methode %(1)s benötigt %(2)s Argument(e) (%(3)s gegeben). Dem von Ihnen angegebenen Netzwerk wurde bereits eine PIF-Datei beigefügt, weshalb keine weitere mehr angefügt werden kann. Diese Karte beinhaltet bereits %(1)s -> %(2)s. Der Wert »%(2)s« für %(1)s wird von diesem Server nicht unterstützt. Antwort des Servers: »%(3)s«. Sie haben versucht einen Vorgang auszuführen, der nicht erlaubt ist. 