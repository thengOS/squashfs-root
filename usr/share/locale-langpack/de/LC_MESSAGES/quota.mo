��    �     �  �  L      �)  3   �)  D   *  C   J*  6   �*  @   �*  \   +     c+  �  +  �  A-  =   �.     /  K   ,/  Y   x/  C   �/     0     -0     D0     [0     r0     �0     �0     �0     �0     �0  B   �0  	   1  	   (1     21     @1     G1     J1  	   X1     b1  0   �1  )   �1     �1     �1     2     :2      U2     v2     �2     �2     �2     �2     �2     3  0   3     E3     b3     i3  	   q3  	   {3  &   �3  &   �3  7  �3  �   6  �   �6  �   7     �7  2   �7      28  &   S8  9   z8  8   �8  "   �8  -   9  G   >9     �9     �9  A   �9  7   �9  3   *:     ^:  !   d:     �:  
   �:  +   �:     �:     �:     �:  -   ;  5   3;  !   i;  -   �;  *   �;  #   �;  #   <  ?   ,<  "   l<  %   �<  "   �<  .   �<  4   =  #   <=     `=  &   x=     �=  !   �=  A   �=  Q   >  6   o>  0   �>  4   �>  -   ?      :?     [?     k?  *   �?  5   �?  '   �?  .   @  M   <@  %   �@  9   �@  7   �@  &   "A     IA  4   ZA     �A  <   �A     �A  8   B      >B  "   _B  (   �B  1   �B  )   �B  *   C  !   2C     TC  O   pC  *   �C  /   �C  #   D  "   ?D  &   bD     �D  *   �D  (   �D     �D  /   
E  "   :E  *   ]E  %   �E     �E     �E     �E  /   �E  3   /F     cF  2   }F  .   �F  )   �F  %   	G  .   /G  (   ^G  -   �G  *   �G     �G     �G  2   H  G   PH  �   �H  V   BI  .   �I  )   �I     �I     J      J  &   ?J     fJ  &   �J  2   �J  *   �J  8   K  1   @K     rK     �K  /   �K     �K     �K     L  3   <L  2   pL  $   �L      �L     �L  C   M  $   EM  T   jM  S   �M  #   N     7N     SN  "   pN  -   �N     �N  $   �N     O  $   $O     IO     hO     �O  5   �O     �O  %   �O     P     %P  !   CP  !   eP     �P  (   �P  .   �P     �P     
Q     $Q  H   >Q  $   �Q  '   �Q  !   �Q  $   �Q  $   R     @R     `R     tR  ,   �R  $   �R  =   �R  !   "S     DS     cS     �S     �S  )   �S     �S  !   �S     T  9   2T  5   lT  *   �T  !   �T  0   �T  %    U     FU  
   VU     aU  6   }U  6   �U  "   �U  2   V  7   AV  3   yV  6   �V     �V     �V  
   W  L   W     cW     }W     �W     �W     �W  _   �W  m   JX     �X     �X  #   �X  3   �X     /Y  /   5Y     eY  2   wY  �   �Y  }   6Z     �Z  n   �Z  (   7[     `[     y[     �[     \  G   *\     r\  '   w\     �\  2   �\     �\     �\     �\     ]  i   3]     �]     �]     �]  ,   �]     ^  8   
^  1   C^  N   u^  <   �^  #   _  =   %_     c_  +   ~_  (   �_  )   �_  !   �_     `  $   9`  5   ^`  /   �`  *   �`     �`  1   a  F   5a  F   |a      �a      �a     b     !b     ?b     [b     xb  1   �b     �b     �b     �b  %   �b     c  4   (c  4   ]c     �c     �c  9   �c  7   �c  G   +d  2   sd  G   �d  6   �d  0   %e  0   Ve  W   �e  x   �e  *   Xf  :   �f  O   �f  �   g  ,   �g  &   �g  '   �g  S   h  D   bh     �h     �h     �h  3   i     8i  $   Vi     {i     �i     �i  *   �i  ,   �i      j  1   2j  -   dj  X   �j     �j     �j  P   k     hk     zk  2   �k     �k  ;   �k  /   �k  J   /l     zl  .   �l     �l  (   �l  4   �l  0   !m     Rm  I   rm  H   �m  1   n  2   7n  2   jn  (   �n     �n  (   �n  $   o  1   -o  N   _o     �o     �o  �   Dp  6   q  !   Fq    hq  �  �r  (   �v  �  �v  q  �x  j  z  ;   v{     �{    �{  �  �  N   ��  5   �  5   9�  .   o�  N   ��  )   �  M   �  8   e�     ��  O   ��  )   ��  6    �  $   W�  1   |�  #   ��  *   ҄  $   ��  '   "�  <   J�  �   ��  X   -�     ��     ��     ��     ��     φ     �     �     �     ,�     G�  I   [�  A   ��  A   �     )�     -�     2�     8�  $   U�  %   z�  5   ��     ֈ     �     ��     �  7    �     X�     i�     k�     |�     ��     ��     ��     ҉     ؉     މ  5   ��  1   .�     `�     e�     k�     m�     s�     u�     |�     ��     ��     ��     ��     ��     ��          ۊ     �     �     �     �     /�     5�     G�     I�  	   c�     m�     ��     ��     ��     ��  �  ��  3   ��  F   �  G   )�  7   q�  @   ��  n   �  !   Y�    {�    ��  =   ��     �  Q   �  Z   a�  A   ��     ��     �     ,�     C�     Z�     q�     ��     ��     ��     ��  B   Õ  	   �  	   �     �     (�     /�     2�  	   @�      J�  4   k�  2   ��  (   Ӗ      ��      �     >�  -   [�  %   ��     ��     ��     җ      �      �     )�  :   5�     p�     ��     ��  	   ��  	   ��  (   ��  (   ܘ  �  �  �   ��  �   Q�  �   �     �  H   ��  "   ?�  )   b�  ?   ��  @   ̞  0   �  5   >�  h   t�     ݟ     �  P   �  D   b�  E   ��     ��  *   �     �  
   ;�  +   F�     r�     ��     ��  6   ��  K   ޡ  ?   *�  B   j�  ;   ��  -   �  3   �  k   K�  *   ��  ,   �  )   �  :   9�  =   t�  *   ��     ݤ  /   ��  !   -�  (   O�  N   x�  c   ǥ  D   +�  F   p�  9   ��  C   �  -   5�     c�  !   ��  0   ��  B   ֧  4   �  8   N�  `   ��  0   �  H   �  E   b�  .   ��     ש  B   �  )   5�  U   _�  %   ��  K   ۪  ,   '�  :   T�  C   ��  ;   ӫ  7   �  8   G�  ,   ��  *   ��  |   ج  6   U�  ?   ��  3   ̭  ,    �  7   -�     e�  8   ��  /   ��      �  8   
�  *   C�  ;   n�  9   ��  *   �  %   �  (   5�  @   ^�  C   ��     �  :   �  :   =�  9   x�  A   ��  3   ��  3   (�  ?   \�  .   ��  #   ˲  %   �  F   �  f   \�  �   ó  c   ��  3   �  3   8�  ,   l�  /   ��  -   ɵ  ,   ��  !   $�  -   F�  N   t�  G   ö  I   �  9   U�  &   ��  '   ��  X   ޷  3   7�  2   k�  9   ��  A   ظ  A   �  7   \�  1   ��  $   ƹ  O   �  4   ;�  u   p�  s   �  +   Z�  +   ��  $   ��  +   ׻  6   �  '   :�  .   b�  2   ��  )   ļ  -   �  '   �     D�  D   W�     ��  ,   ��  0   ޽  %   �  0   5�  0   f�  )   ��  @   ��  7   �     :�  "   M�     p�  b   ��  '   �  ,   �  )   @�  +   j�  ;   ��  $   ��     ��  %   �  8   1�  '   j�  b   ��  .   ��  &   $�  '   K�     s�  *   z�  6   ��  ,   ��  *   	�  #   4�  <   X�  6   ��  0   ��      ��  4   �  5   S�     ��     ��      ��  M   ��  I   �  (   [�  ;   ��  >   ��  @   ��  A   @�  !   ��     ��     ��  N   ��  1   �  '   I�     q�     ��      ��  h   ��  s   2�     ��  -   ��  #   ��  C   ��     >�  J   E�     ��  8   ��  �   ��  �   ��     �  �   -�  '   ��     ��  �   ��     ��     ��  S   ��     &�  (   +�     T�  5   c�     ��     ��     ��     ��     ��     q�     }�  #   ��  0   ��     ��  8   ��  E    �  a   f�  Q   ��  %   �  P   @�      ��  0   ��  =   ��  .   !�  2   P�     ��  4   ��  7   ��  5   �  ?   C�     ��  2   ��  U   ��  U   #�  .   y�  #   ��  !   ��  %   ��  $   �  '   9�  )   a�  ;   ��  -   ��     ��     ��  ?   ��     <�  ;   X�  ;   ��     ��     ��  B   �  E   E�  N   ��  B   ��  u   �  >   ��  4   ��  ;   �  j   C�  �   ��  4   L�  A   ��  ^   ��  �   "�  4   ��  0   �  +   <�  f   h�  T   ��  !   $�  *   F�  )   q�  I   ��      ��  /   �     6�  "   R�     u�  '   ��  =   ��     ��  <   	�  ;   F�  d   ��     ��  #   ��  V   �     p�     ��  7   ��     ��  Y   ��  ?   3�  \   s�     ��  Q   ��     :�  +   <�  ?   h�  K   ��  $   ��  n   �  n   ��  H   ��  I   @�  B   ��  -   ��     ��  6   �  -   O�  >   }�  `   ��     �  �   6�  �   ��  @   ��  ?   ��  9  �  G  R�  .   ��    ��  �  ��  �  g�  @   �     Y�  �  b�  (  #�  e   L�  8   ��  8   ��  9   $�  e   ^�  1   ��  e   ��  H   \�     ��  [   ��  2   	�  D   <�  6   ��  2   ��  $   ��  3   �  3   D�  /   x�  H   ��  �   ��  r   ��     C�     T�     i�     }�  "   ��     ��  (   ��  !   ��  !        /  d   J  L   �  M   �     J    N    S     [ +   | '   � 8   �    	     "   7    Z D   b    �    �    �    �    �        %    ?    K    R D   n F   �    �        	                    "    '    +    .    M    Z    `    z    �    �    �    �    �    �    �    �        %    D    U    ^    |    R             �      �              �  �   F  �   �         F   �   �      �       �      �  E  Z  �  s   �  �  o      c          �  �   �      e  �  �  *   �   �   �      �  �     c   �          �   C  �          _       i   t   g       �      �     <   ]  M   "      *    �   �      �     ~          ]   �  9  h  A  `  �   �  �   !       �              {   �  �   (    �      �   |       �             U   �   �  �      z   �  �   �         �   �         j       �  �  �  >   3       �   �  n   �   �       J   f   �     +  9                       �   �  4  �           ,  �   �   L  a  �   x   /   }  �   �      �   B  C   �                   y           �              �   2         �   �  �          �  �  �   �      �   �   �   �       r   (   B           l       j  J  �   �   �  �      E       �       �          .  �   w       H           K   �   �  �  �   �  �  6          6   �   �   k       r  <  q   ;   _      5   �   �          �   1   �  �     �   �    �   t    �   b   �      �       v      �  �   �      �       |  #  �  a            8  X  �   2                   �   �           w  �   �  �      �   �               U     �   -          -   o  �   "       �   �   �           �  5      D  �   x  �  {          �  �      O         �  u   :   K  �   �  $  �  >           !      $   �   W       v       m          s  S     �      Q  ;  �     �   h       u  �       �  ^           �  �  V  �   3         q      N  �     G  '   T   7   .   �  �  d      �   �   0   �   �   �      �  �        �  �   d   Z   &  �  �      �                �   \  Q   	   �   �           g  �  I   �           �      �  	      �   +       G   �   �  :      #       �   �         '  4   7  �   �     �       ?   �       e   1      �    @    �   �      )   H  �   O       }   f      W     �      Y      P      T  D   y  �  z  ^     �   k      �     i  �       �  �      �  X   �   �      ,   Y   �      �   [       �  �   �   �  p  �     �   n  &   �   �  �      �       m   �    �   b  �   @         �   �      ~           �   �     )  �   �      �             �   �   p           [          �  �  
   �   `   �   �    R   M     A   �             �    �  N        \   �         �  �        �   �              
  %           =   �       %  0  �     /          P          �       �       �   �  �   8   L   l  V   �   �      �  =  �   �  �   �  �   ?      �   �   S       �   �  I               	Adding %s size %lld ino %d links %d uid %u gid %u
 	quota [-qvswim] [-l | [-Q | -A]] [-F quotaformat] -g groupname ...
 	quota [-qvswim] [-l | [-Q | -A]] [-F quotaformat] -u username ...
 	quota [-qvswugQm] [-F quotaformat] -f filesystem ...
 
                        Block limits               File limits
 
-u, --user                    edit user data
-g, --group                   edit group data
 
Can open directory %s: %s
 
Please cleanup the group data before the grace period expires.

Basically, this means that the system thinks group is using more disk space
on the above partition(s) than it is allowed.  If you do not delete files
and get below group quota before the grace period expires, the system will
prevent you and other members of the group from creating new files owned by
the group.

For additional assistance, please contact us at %s
or via phone at %s.
 
We hope that you will cleanup before your grace period expires.

Basically, this means that the system thinks you are using more disk space
on the above partition(s) than you are allowed.  If you do not delete files
and get below your quota before the grace period expires, the system will
prevent you from creating new files.

For additional assistance, please contact us at %s
or via phone at %s.
                         %s limits                File limits
     %8llu    %8llu    %8llu   Filesystem                         block grace               inode grace
   Filesystem                   blocks       soft       hard     inodes     soft     hard
   Filesystem             Block grace period     Inode grace period
   cache hits:      %u
   cache misses:    %u
   dquot dups:      %u
   dquot wants:     %u
   inact reclaims:  %u
   missed reclaims: %u
   reclaims:        %u
   shake reclaims:  %u
 #%-7d %-8.8s %-9s       used    soft    hard  grace    used  soft  hard  grace
 %02d:%02d %8llu     %d	%llu	%llu
 %ddays %s %s (%s) %ss:
 %s (%s):
 %s (gid %d): Permission denied
 %s (gid %d): error while trying getgroups(): %s
 %s (gid %d): gid set allocation (%d): %s
 %s (uid %d): Permission denied
 %s [%s]: %s quotas turned off
 %s [%s]: %s quotas turned on
 %s quota on %s (%s) is %s
 %s quota sync failed for %s: %s
 %s quota sync failed: %s
 %s: %s
 %s: %s quotas turned off
 %s: %s quotas turned on
 %s: %s root_squash turned off
 %s: %s root_squash turned on
 %s: %s: %s
 %s: Quota cannot be turned on on NFS filesystem
 %s: deleted %s quota blocks
 %udays %uhours %uminutes %useconds *** Report for %s quotas on device %s
 *** Status for %s quotas on device %s
 -F, --format=formatname       edit quotas of a specific format
-p, --prototype=name          copy data from a prototype user/group
    --always-resolve          always try to resolve name, even if it is
                              composed only of digits
-f, --filesystem=filesystem   edit data only on a specific filesystem
-t, --edit-period             edit grace period
-T, --edit-times              edit grace time of a user/group
-h, --help                    display this help text and exit
-V, --version                 display version information and exit

 -r, --remote                  edit remote quota (via RPC)
-m, --no-mixed-pathnames      trim leading slashes from NFSv4 mountpoints
 -r, --remote               set remote quota (via RPC)
-m, --no-mixed-pathnames      trim leading slashes from NFSv4 mountpoints
 -t, --edit-period          edit grace period
-T, --edit-times           edit grace times for user/group
-h, --help                 display this help text and exit
-V, --version              display version information and exit

 0seconds Accounting [ondisk]: %s; Enforcement [ondisk]: %s
 Accounting: %s; Enforcement: %s
 Adding dquot structure type %s for %d
 Administrator for a group %s not found. Cancelling mail.
 Allocated %d bytes memory
Free'd %d bytes
Lost %d bytes
 Already accounting %s quota on %s
 As you wish... Canceling check of this file.
 Bad file magic or version (probably not quotafile with bad endianity).
 Bad format:
%s
 Bad number of arguments.
 Bad time units. Units are 'second', 'minute', 'hour', and 'day'.
 Batch mode and prototype user cannot be used together.
 Batch mode cannot be used for setting grace times.
 Block Block %u in tree referenced twice Block %u is truncated.
 Block %u:  Block grace time: %s; Inode grace time: %s
 Block limit reached on Bugs to %s
 Bugs to: %s
 Cannot access the specified xtab file %s: %s
 Cannot allocate new quota block (out of disk space).
 Cannot bind to given address: %s
 Cannot change grace times over RPC protocol.
 Cannot change owner of temporary file: %s
 Cannot change permission of %s: %s
 Cannot change state of GFS2 quota.
 Cannot change state of XFS quota. It's not compiled in kernel.
 Cannot commit dquot for id %u: %s
 Cannot connect to netlink socket: %s
 Cannot connect to system DBUS: %s
 Cannot create DBUS message: No enough memory.
 Cannot create file for %ss for new format on %s: %s
 Cannot create new quotafile %s: %s
 Cannot create pipe: %s
 Cannot create set for sigaction(): %s
 Cannot create socket: %s
 Cannot create temporary file: %s
 Cannot delete %s quota on %s - switch quota accounting off first
 Cannot delete %s quota on %s - switch quota enforcement and accounting off first
 Cannot detect quota format for journalled quota on %s
 Cannot duplicate descriptor of file to edit: %s
 Cannot duplicate descriptor of file to write to: %s
 Cannot duplicate descriptor of temp file: %s
 Cannot duplicate descriptor: %s
 Cannot exec %s
 Cannot execute '%s': %s
 Cannot find a device with %s.
Skipping...
 Cannot find a filesystem mountpoint for directory %s
 Cannot find any quota file to work on.
 Cannot find checked quota file for %ss on %s!
 Cannot find filesystem to check or filesystem not mounted with quota option.
 Cannot find mountpoint for device %s
 Cannot find quota file on %s [%s] to turn quotas on/off.
 Cannot find quota option on filesystem %s with quotas!
 Cannot finish IO on new quotafile: %s
 Cannot fork: %s
 Cannot gather quota data. Tree root node corrupted.
 Cannot get device name for %s
 Cannot get exact used space... Results might be inaccurate.
 Cannot get host name: %s
 Cannot get info for %s quota file from kernel on %s: %s
 Cannot get name for uid/gid %u.
 Cannot get name of new quotafile.
 Cannot get name of old quotafile on %s.
 Cannot get quota for %s %d from kernel on %s: %s
 Cannot get quota information for user %s
 Cannot get quota information for user %s.
 Cannot get quotafile name for %s
 Cannot get system info: %s
 Cannot guess format from filename on %s. Please specify format on commandline.
 Cannot initialize IO on new quotafile: %s
 Cannot initialize IO on xfs/gfs2 quotafile: %s
 Cannot initialize mountpoint scan.
 Cannot initialize quota on %s: %s
 Cannot join quota multicast group: %s
 Cannot open %s: %s
 Cannot open %s: %s
Will use device names.
 Cannot open any file with mount points.
 Cannot open file %s: %s
 Cannot open file with group administrators: %s
 Cannot open new quota file %s: %s
 Cannot open old format file for %ss on %s
 Cannot open old quota file on %s: %s
 Cannot open pipe: %s
 Cannot open quotafile %s: %s
 Cannot parse input line %d.
 Cannot parse line %d in quotatab (missing ':')
 Cannot parse time at CC_BEFORE variable (line %d).
 Cannot read block %u: %s
 Cannot read entry for id %u from quotafile %s: %s
 Cannot read first entry from quotafile %s: %s
 Cannot read header from quotafile %s: %s
 Cannot read header of old quotafile.
 Cannot read individual grace times from file.
 Cannot read info from quota file %s: %s
 Cannot read information about old quotafile.
 Cannot read quota structure for id %u: %s
 Cannot read quotas from file.
 Cannot read stat file %s: %s
 Cannot register callback for netlink messages: %s
 Cannot remount filesystem %s read-write. cannot write new quota files.
 Cannot remount filesystem mounted on %s read-only so counted values might not be right.
Please stop all programs writing to filesystem or use -m flag to force checking.
 Cannot remount filesystem mounted on %s read-only. Counted values might not be right.
 Cannot rename new quotafile %s to name %s: %s
 Cannot rename old quotafile %s to %s: %s
 Cannot reopen temp file: %s
 Cannot reopen! Cannot reset signal handler: %s
 Cannot resolve mountpoint path %s: %s
 Cannot resolve path %s: %s
 Cannot resolve quota netlink name: %s
 Cannot set both individual and global grace time.
 Cannot set grace times over RPC protocol.
 Cannot set info for %s quota file from kernel on %s: %s
 Cannot set quota for %s %d from kernel on %s: %s
 Cannot set signal handler: %s
 Cannot set socket options: %s
 Cannot stat device %s (maybe typo in quotatab)
 Cannot stat directory %s: %s
 Cannot stat mountpoint %s: %s
 Cannot stat quota file %s: %s
 Cannot stat() a mountpoint with %s: %s
Skipping...
 Cannot stat() given mountpoint %s: %s
Skipping...
 Cannot stat() mounted device %s: %s
 Cannot stat() mountpoint %s: %s
 Cannot statfs() %s: %s
 Cannot switch off %s quota accounting on %s when enforcement is on
 Cannot sync quotas on device %s: %s
 Cannot turn %s quotas off on %s: %s
Kernel won't know about changes quotacheck did.
 Cannot turn %s quotas on on %s: %s
Kernel won't know about changes quotacheck did.
 Cannot turn on/off quotas via RPC.
 Cannot wait for mailer: %s
 Cannot write block (%u): %s
 Cannot write grace times to file.
 Cannot write individual grace times to file.
 Cannot write quota (id %u): %s
 Cannot write quota for %u on %s: %s
 Cannot write quotas to file.
 Checked %d directories and %d files
 Checking quotafile headers...
 Checking quotafile info...
 Compiled with:%s
 Continue checking assuming version from command line? Corrupted blocks:  Corrupted number of used entries (%u) Could not close PID file '%s'.
 Could not get values for %s.
 Could not open PID file '%s': %s
 Could not setup ldap connection.
 Could not store my PID %jd.
 Could not write daemon's PID into '%s'.
 Creation of %s quota format is not supported.
 Data dumped.
 Denied access to host %s
 Detected quota format %s
 Device (%s) filesystem is mounted on unsupported device type. Skipping.
 Disabling %s quota accounting on %s
 Disabling %s quota enforcement %son %s
 Disk quotas for %s %s (%cid %d):
 Disk quotas for %s %s (%cid %u): %s
 Do not know how to buffer format %d
 Dumping gathered data for %ss.
 Duplicated entries. EXT2_IOC_GETFLAGS failed: %s
 Enable XFS %s quota accounting during mount
 Enabling %s quota enforcement on %s
 Enabling %s quota on root filesystem (reboot to take effect)
 Enforcing %s quota already on %s
 Entry for id %u is truncated.
 Entry not found for client %s.
 Error Error checking device name: %s
 Error in config file (line %d), ignoring
 Error parsing netlink message.
 Error while editing grace times.
 Error while editing quotas.
 Error while getting old quota statistics from kernel: %s
 Error while getting quota statistics from kernel: %s
 Error while opening old quota file %s: %s
 Error while releasing file on %s
 Error while searching for old quota file %s: %s
 Error while syncing quotas on %s: %s
 Error with %s.
 Exitting.
 Failed to delete quota: %s
 Failed to find tty of user %llu to report warning to.
 Failed to open tty %s of user %llu to report warning.
 Failed to parse grace times file.
 Failed to read or parse quota netlink message: %s
 Failed to remove IMMUTABLE flag from quota file %s: %s
 Failed to write message to dbus: No enough memory.
 Failed to write quota message for user %llu to %s: %s
 File info done.
 File limit reached on Filesystem Filesystem           used    soft    hard  grace    used  soft  hard  grace
 Filesystem remounted RW.
 Filesystem remounted read-only
 First entry loaded.
 Found an invalid UUID: %s
 Found i_num %ld, blocks %ld
 Found more structures for ID %u. Using values: BHARD: %lld BSOFT: %lld IHARD: %lld ISOFT: %lld
 Found more structures for ID %u. Values: BHARD: %lld/%lld BSOFT: %lld/%lld IHARD: %lld/%lld ISOFT: %lld/%lld
 G Gid set allocation (%d): %s
 Going to check %s quota file of %s
 Grace period before enforcing soft limits for %ss:
 Group Group and user quotas cannot be used together.
 Headers checked.
 Headers of file %s checked. Going to load data...
 Hi,

We noticed that the group %s you are member of violates the quotasystem
used on this system. We have found the following violations:

 Hi,

We noticed that you are in violation with the quotasystem
used on this system. We have found the following violations:

 High uid detected.
 ID %u has more structures. User intervention needed (use -i for interactive mode or -n for automatic answer).
 Illegal free block reference to block %u Illegal port number: %s
 Illegal reference (%u >= %u) in %s quota file on %s. Quota file is probably corrupted.
Please run quotacheck(8) and try again.
 In block grace period on In file grace period on Incorrect format string for variable %s.
Unrecognized expression %%%c.
 Info Inode: #%llu (%llu blocks, %u extents)
 Inode: none
 Inserting already present quota entry (block %u).
 Invalid argument "%s"
 K Kernel quota version: %u.%u.%u
 Kernel quota version: old
 LDAP library version >= 2.3 detected. Please use LDAP_URI instead of hostname and port.
Generated URI %s
 Leaving %s
 Line %d too long.
 Line %d too long. Truncating.
 Loading first quota entry with grace times.
 M Maximum %u dquots (currently %u incore, %u on freelist)
 Maybe create new quota files with quotacheck(8)?
 Metadata init_io called when kernel does not support generic quota interface!
 Metadata init_io called when kernel support is not enabled.
 Mountpoint %s is not a directory?!
 Mountpoint (or device) %s not found or has no quota enabled.
 Mountpoint not specified.
 Multiple entries found for client %s (%d).
 Name must be quotaon or quotaoff not %s
 Name of quota file too long. Contact %s.
 No correct mountpoint specified.
 No filesystem specified.
 No filesystems with quota detected.
 No possible destination for messages. Nothing to do.
 Not all specified mountpoints are using quota.
 Not enough memory to build PID file name.
 Not enough memory.
 Not found any corrupted blocks. Congratulations.
 Not setting block grace time on %s because softlimit is not exceeded.
 Not setting inode grace time on %s because softlimit is not exceeded.
 Number of allocated dquots: %ld
 Number of dquot cache hits: %ld
 Number of dquot drops: %ld
 Number of dquot lookups: %ld
 Number of dquot reads: %ld
 Number of dquot writes: %ld
 Number of free dquots: %ld
 Number of in use dquot entries (user/group): %ld
 Number of quotafile syncs: %ld
 OFF ON Old file found removed during check!
 Old file not found.
 Only RPC quota format is allowed on NFS filesystem.
 Only XFS quota format is allowed on XFS filesystem.
 Over block quota on Over file quota on Parse error at line %d. Cannot find administrators name.
 Parse error at line %d. Cannot find end of group name.
 Parse error at line %d. Trailing characters after administrators name.
 Possible error in config file (line %d), ignoring
 Prototype name does not make sense when editing grace period or times.
 Prototype user has no sense when editing grace times.
 Quota enforcement already disabled for %s on %s
 Quota file %s has IMMUTABLE flag set. Clearing.
 Quota file %s has corrupted headers. You have to specify quota format on command line.
 Quota file format version %d does not match the one specified on command line (%d). Quota file header may be corrupted.
 Quota file not found or has wrong format.
 Quota file on %s [%s] does not exist or has wrong format.
 Quota for %ss is enabled on mountpoint %s so quotacheck might damage the file.
 Quota for %ss is enabled on mountpoint %s so quotacheck might damage the file.
Please turn quotas off or use -f to force checking.
 Quota for id %u referenced but not present.
 Quota format not supported in kernel.
 Quota not supported by the filesystem.
 Quota structure for %s owning quota file not present! Something is really wrong...
 Quota structure has offset to other block (%u) than it should (%u).
 Quota utilities version %s.
 Quota write failed (id %u): %s
 RPC quota format not compiled.
 RPC quota format specified for non-NFS filesystem.
 Reference to illegal block %u Renaming new files to proper names.
 Renaming new quotafile
 Renaming old quotafile to %s~
 Report bugs to <%s>.
 Repquota cannot report through RPC calls.
 Required format %s not supported by kernel.
 Scanning %s [%s]  Scanning stored directories from directory stack
 Setting grace period on %s is not supported.
 Setting grace times and other flags to default values.
Assuming number of blocks is %u.
 Should I continue? Should I use new values? Size of file: %lu
Blocks: %u Free block: %u Block with free entry: %u Flags: %x
 Skipping %s [%s]
 Skipping line.
 Something weird happened while scanning. Error %d
 Space Specified both -n and -t but only one of them can be used.
 Specified path %s is not directory nor device.
 Statistics:
Total blocks: %u
Data blocks: %u
Entries: %u
Used average: %f
 Substracted %lu bytes.
 Substracting space used by old %s quota file.
 T The running kernel does not support XFS
 Time units may be: days, hours, minutes, or seconds
 Times to enforce softlimit for %s %s (%cid %d):
 Too many parameters to editor.
 Trying to set quota limits out of range supported by quota format on %s.
 Trying to set quota usage out of range supported by quota format on %s.
 Trying to write info to readonly quotafile on %s
 Trying to write info to readonly quotafile on %s.
 Trying to write quota to readonly quotafile on %s
 Unable to resolve name '%s' on line %d.
 Undefined program name.
 Unexpected XFS quota state sought on %s
 Unknown action should be performed.
 Unknown decimal unit. Valid units are k, m, g, t. Unknown format of kernel netlink message!
Maybe your quota tools are too old?
 Unknown option '%c'.
 Unknown output format: %s
Supported formats are:
  default - default
  csv     - comma-separated values
  xml     - simple XML
 Unknown quota format: %s
Supported formats are:
  vfsold - original quota format
  vfsv0 - standard quota format
  vfsv1 - quota format with 64-bit limits
  rpc - use RPC calls
  xfs - XFS quota format
 Unknown space binary unit. Valid units are K, M, G, T. Unterminated last line, ignoring
 Usage:
	edquota %1$s[-u] [-F formatname] [-p username] [-f filesystem] username ...
	edquota %1$s-g [-F formatname] [-p groupname] [-f filesystem] groupname ...
	edquota [-u|g] [-F formatname] [-f filesystem] -t
	edquota [-u|g] [-F formatname] [-f filesystem] -T username|groupname ...
 Usage:
  setquota [-u|-g] %1$s[-F quotaformat] <user|group>
	<block-softlimit> <block-hardlimit> <inode-softlimit> <inode-hardlimit> -a|<filesystem>...
  setquota [-u|-g] %1$s[-F quotaformat] <-p protouser|protogroup> <user|group> -a|<filesystem>...
  setquota [-u|-g] %1$s[-F quotaformat] -b [-c] -a|<filesystem>...
  setquota [-u|-g] [-F quotaformat] -t <blockgrace> <inodegrace> -a|<filesystem>...
  setquota [-u|-g] [-F quotaformat] <user|group> -T <blockgrace> <inodegrace> -a|<filesystem>...

-u, --user                 set limits for user
-g, --group                set limits for group
-a, --all                  set limits for all filesystems
    --always-resolve       always try to resolve name, even if is
                           composed only of digits
-F, --format=formatname    operate on specific quota format
-p, --prototype=protoname  copy limits from user/group
-b, --batch                read limits from standard input
-c, --continue-batch       continue in input processing in case of an error
 Usage: %s [-acfugvViTq] [filesystem...]
 Usage: %s [options]
Options are:
 -h --help             shows this text
 -V --version          shows version information
 -F --foreground       starts the quota service in foreground
 -I --autofs           do not ignore mountpoints mounted by automounter
 -p --port <port>      listen on given port
 -s --no-setquota      disables remote calls to setquota (default)
 -S --setquota         enables remote calls to setquota
 -x --xtab <path>      set an alternative file with NFSD export table
 Usage: %s [options]
Options are:
 -h --help             shows this text
 -V --version          shows version information
 -F --foreground       starts the quota service in foreground
 -I --autofs           do not ignore mountpoints mounted by automounter
 -p --port <port>      listen on given port
 -x --xtab <path>      set an alternative file with NFSD export table
 Usage: %s [options]
Options are:
 -h --help         shows this text
 -V --version      shows version information
 -C --no-console   do not try to write messages to console
 -b --print-below  write to console also information about getting below hard/soft limits
 -D --no-dbus      do not try to write messages to DBUS
 -F --foreground   run daemon in foreground
 Usage: quota [-guqvswim] [-l | [-Q | -A]] [-F quotaformat]
 User Utility for checking and repairing quota files.
%s [-gucbfinvdmMR] [-F <quota-format>] filesystem|-a

-u, --user                check user files
-g, --group               check group files
-c, --create-files        create new quota files
-b, --backup              create backups of old quota files
-f, --force               force check even if quotas are enabled
-i, --interactive         interactive mode
-n, --use-first-dquot     use the first copy of duplicated structure
-v, --verbose             print more information
-d, --debug               print even more messages
-m, --no-remount          do not remount filesystem read-only
-M, --try-remount         try remounting filesystem read-only,
                          continue even if it fails
-R, --exclude-root        exclude root when checking all filesystems
-F, --format=formatname   check quota files of specific format
-a, --all                 check all filesystems
-h, --help                display this message and exit
-V, --version             display version information and exit

 Utility for converting quota files.
Usage:
	%s [options] mountpoint

-u, --user                          convert user quota file
-g, --group                         convert group quota file
-e, --convert-endian                convert quota file to correct endianity
-f, --convert-format oldfmt,newfmt  convert from old to VFSv0 quota format
-h, --help                          show this help text and exit
-V, --version                       output version information and exit

 WARNING -  Quotafile %s was probably truncated. Cannot save quota settings...
 WARNING - %s: cannot change current block allocation
 WARNING - %s: cannot change current inode allocation
 WARNING - Quota file %s has corrupted headers
 WARNING - Quota file %s was probably truncated. Cannot save quota settings...
 WARNING - Quota file info was corrupted.
 WARNING - Quotafile %s was probably truncated. Cannot save quota settings...
 WARNING - Some data might be changed due to corruption.
 Warning Warning: Cannot open export table %s: %s
Using '/' as a pseudofilesystem root.
 Warning: Cannot set EXT2 flags on %s: %s
 Warning: Ignoring -%c when filesystem list specified.
 Warning: Mailer exitted abnormally.
 Warning: No quota format detected in the kernel.
 XFS Quota Manager dquot statistics
 XFS quota allowed only on XFS filesystem.
 XFS_IOC_FSBULKSTAT ioctl failed: %s
 You have to specify action to perform.
 You have to specify source and target format of conversion.
 Your kernel probably supports journaled quota but you are not using it. Consider switching to journaled quota to avoid running quotacheck after an unclean shutdown.
 Your quota file is stored in wrong endianity. Please use convertquota(8) to convert it.
 and accounting  bad format:
%s
 block limit reached block quota exceeded block quota exceeded too long blocks cannot create TCP service.
 cannot create udp service.
 cannot find %s on %s [%s]
 cannot open %s: %s
 cannot write times for %s. Maybe kernel does not support such operation?
 copy_user_quota_limits: Failed to get userquota for uid %ld : %s
 copy_user_quota_limits: Failed to set userquota for uid %ld : %s
 day days done
 error (%d) while opening %s
 error (%d) while opening inode scan
 error (%d) while starting inode scan
 error while getting quota from %s for %s (id %u): %s
 file limit reached file quota exceeded file quota exceeded too long files find_free_dqentry(): Data block full but it shouldn't.
 fsname mismatch
 g getgroups(): %s
 got below block limit got below block quota got below file limit got below file quota grace group group %s does not exist.
 host %s attempted to call setquota from port >= 1024
 host %s attempted to call setquota when disabled
 hour hours k limit m minute minutes none off on popd %s
Entering directory %s
 pushd %s/%s
 quota quotactl on %s [%s]: %s
 quotactl on %s: %s
 quotactl() on %s: %s
 second seconds set root_squash on %s: %s
 space svc_run returned
 t unable to free arguments
 undefined unknown quota warning unset user user %s does not exist.
 using %s on %s [%s]: %s
 Project-Id-Version: quota-tools
Report-Msgid-Bugs-To: jack@suse.cz
POT-Creation-Date: 2015-12-16 18:19+0100
PO-Revision-Date: 2016-03-26 17:36+0000
Last-Translator: Michael Bunk <Unknown>
Language-Team: German <translation-team-de@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:15+0000
X-Generator: Launchpad (build 18115)
X-Poedit-Country: GERMANY
Language: de
X-Poedit-Language: German
 	Adding %s size %lld ino %d links %d uid %u gid %u
 	quota [-qvswim] [-l | [-Q | -A]] [-F Quotaformat] -g Gruppenname ...
 	quota [-qvswim] [-l | [-Q | -A]] [-F Quotaformat] -u Benutzername ...
 	quota [-qvswugQm] [-F Quotaformat] -f Dateisystem ...
 
                        Blocklimits                Dateilimits
 
-u, --user                    Benutzerdaten bearbeiten
-g, --group                   Gruppendaten bearbeiten
 
Kann Verzeichnis öffnen %s: %s
 
Wir hoffen, dass Sie aufräumen bevor die Gnadenfrist abgelaufen ist.

Sie erhalten diese Mitteilung, weil Sie auf den oben genannten Dateisystemen
mehr Speicherplatz belegen bzw. Dateien angelegt haben, als ihnen erlaubt ist.
Wenn Sie keinen Speicherplatz freigeben bzw. Dateien löschen und wieder unter
ihr Quota kommen bevor die Gnadenfrist abgelaufen ist, werden Sie keinen
weiteren Speicherplatz belegen bzw. keine weiteren Dateien anlegen können.

Für Unterstützung wenden Sie sich bitte per E-Mail an %s
oder per Telefon an %s.
 
Wir hoffen, dass Sie aufräumen bevor die Gnadenfrist abgelaufen ist.

Sie erhalten diese Mitteilung, weil Sie auf den oben genannten Dateisystemen
mehr Speicherplatz belegen bzw. Dateien angelegt haben, als Ihnen erlaubt ist.
Wenn Sie keinen Speicherplatz freigeben bzw. Dateien löschen und wieder unter
ihr Quota kommen bevor die Gnadenfrist abgelaufen ist, werden Sie keinen
weiteren Speicherplatz belegen bzw. keine weiteren Dateien anlegen können.

Für Unterstützung wenden Sie sich bitte per E-Mail an %s
oder per Telefon an %s.
                         %s Limits                Dateilimits
     %8llu    %8llu    %8llu   Dateisystem                        Blockgnadenfrist           Inodegnadenfrist
   Dateisystem                  Blöcke       weich      hart     Inodes     weich    hart
   Dateisystem            Blockgnadenfrist       Inodegnadenfrist
   cache hits:      %u
   cache misses:    %u
   dquot dups:      %u
   dquot wants:     %u
   inact reclaims:  %u
   missed reclaims: %u
   reclaims:        %u
   shake reclaims:  %u
 #%-7d %-8.8s %-9s       belegt  weich   hart  Gnade   belegt weich hart  Gnade
 %02d:%02d %8llu     %d	%llu	%llu
 %ddays %s %s (%s) %ss:
 %s (%s):
 %s (Gid %d): Zugriff verweigert
 %s (Gid %d): Fehler beim Aufruf von getgroups(): %s
 %s (Gid %d): gid set Speicheranforderung (%d): %s
 %s (Benutzer-Id %d): Zugriff verweigert
 %s [%s]: %s Quotas abgeschalten
 %s [%s]: %s Quotas angeschalten
 %s-Quota auf %s (%s) ist %s
 %s Quota-Abgleich fehlgeschlagen für %s: %s
 %s Quota-Abgleich fehlgeschlagen: %s
 %s: %s
 %s: %s-Quotas deaktiviert
 %s: %s-Quotas aktiv
 %s: %s root_squash abgeschalten
 %s: %s root_squash angeschalten
 %s: %s: %s
 %s: Quota kann auf NFS-Dateisystem nicht aktiviert werden
 %s: %s-Quota-Blöcke gelöscht
 %udays %uhours %uminutes %useconds *** Report für %s Quotas auf Gerät %s
 *** Status für %s-Quotas auf Gerät %s
 -F, --format=formatname       Quotas eines bestimmten Formats bearbeiten
-p, --prototype=name          Kopiere Daten von einem Vorlagenutzer/einer Vorlagegruppe
    --always-resolve          Versuche stets, einen Namen aufzulösen, auch wenn er
                              nur aus Ziffern besteht
-f, --filesystem=filesystem   Bearbeite nur Daten eines bestimmten Dateisystems
-t, --edit-period             Bearbeite Standard-Gnadenfrist
-T, --edit-times              Bearbeite Gradenfrist eines Nutzers/einer Gruppe
-h, --help                    Zeige diesen Hilfetext and und beende das Programm
-V, --version                 Zeige Versionsinformationen and und beende das Programm

 -r, --remote                  entferne Quota bearbeiten (über RPC)
-m, --no-mixed-pathnames      entferne führende Schrägstriche von NFSv4-Mountpunkten
 -r, --remote               Setze entferntes Quota (über RPC)
-m, --no-mixed-pathnames      Entferne führende Schrägstriche (/) von NFSv4-Einhängepunkten
 -t, --edit-period          Bearbeite Standard-Gnadenfrist
-T, --edit-times           Bearbeite Gnadenfristen für Benutzer/Gruppe
-h, --help                 Zeige diese Hilfe und beende
-V, --version              Zeige Versionsinformationen und beende

 0seconds Kontierung [auf Datenträger]: %s; Vollstreckung [auf Datenträger]: %s
 Kontierung: %s; Vollstreckung: %s
 Füge dquot-Strukturtyp %s für %d hinzu
 Administrator für Gruppe %s nicht gefunden. Sende keine Mail.
 %d Bytes Speicher belegt
%d Bytes freigegeben
%d Bytes verloren
 Kontierung für %s-Quota auf %s ist schon aktiv
 WIe du wünschst... breche Prüfung dieser Datei ab.
 Falsche Magische Bytefolge oder Version (wahrscheinlich keine Quotadatei mit falscher Bytereihenfolge).
 Falsches Format:
%s
 Falsche Anzahl an Argumenten.
 Fehlerhafte Zeiteinheiten. Einheiten sind 'second', 'minute', 'hour' und 'day'.
 Stapelmodus und Prototypen können nicht zusammen verwendet werden.
 Stapelmodus kann nicht verwendet werden, um Gnadenfristen zu setzen.
 Block Block %u wird im Baum doppelt referenziert Block %u ist abgeschnitten.
 Block %u:  Blockgnadenfrist: %s; Inodegnadenfrist: %s
 Blocklimit erreicht auf Fehler an %s
 Fehler an: %s
 Kann auf angegebene xtab-Datei %s nicht zugreifen: %s
 Kann keinen neune Quotablock belegen (kein Festplattenspeicherplatz mehr).
 Kinnd bind-Operation zu gegebener Adresse nicht ausführen: %s
 Kulanzzeiten kann nicht über das RPC-Protokoll geändert werden.
 Kann Eigentümer einer temporären Datei nicht ändern: %s
 Kann Zugriffsrechte von %s nicht ändern: %s
 Status von GFS2-Quota kann nicht geändert werden.
 Kann Zustand des XFS-Quotas nicht ändern.  Unterstützung dafür wurde nicht in den Kernel einkompiliert.
 Kann dquot für id %u nicht speichern: %s
 Kann nicht zum Netlink-Socket verbinden: %s
 Kann nicht zum System-DBUS verbinden: %s
 Kann DBUS-Nachricht nicht erzeugen: Nicht genug Speicher.
 Kann Datei für %ss im neuen Format auf %s nicht anlegen: %s
 Kann neue Quotadatei %s nicht anlegen: %s
 Konnte keine Pipe erzeugen: %s
 Kann Menge für sigaction() nicht erzeugen: %s
 Konnte Socket nicht erzeugen: %s
 Kann temporäre Datei nicht anlegen: %s
 Kann %s-Quota auf %s nicht löschen - deaktivieren Sie zuerst Quotakontierung
 Kann %s-Quota auf %s nicht löschen - schalten Sie Quotavollstreckuckung und -kontierung zuerst ab
 Kann das Quotaformat für das Journalquota auf %s nicht feststellen
 Kann Dateideskriptor der zu bearbeitenden Datei nicht duplizieren: %s
 Kann Dateideskriptor nicht zum Schreiben duplizieren: %s
 Kann Dateideskriptor einer temporären Datei nicht duplizieren: %s
 Konnte Dateideskriptor nicht duplizieren: %s
 Kann 'exec %s nicht ausführen
 Konnte '%s' nicht ausführen: %s
 Kann kein Gerät mit %s finden.
Überspringe...
 Kann keinen Dateisystem-Einhängepunkt für Verzeichnis %s finden
 Kann keine Quotadatei finden, um sie zu bearbeiten.
 Kann geprüfte Quotadatei für %ss auf %s nicht finden!
 Kann kein Dateisystem zum Prüfen finden oder Dateisystem nicht mit der Quota-Option gemountet.
 Kann Einhängepunkt für Gerät %s nicht finden
 Kann Quotadatei auf %s [%s] nicht finden, um Quotas zu (de-)aktivieren.
 Kann die Quotaoption auf dem Dateisystem %s mit Quotas nicht finden!
 Kann IO an neuer Quotadatei nicht beenden: %s
 fork() fehlgeschlagen: %s
 Kann Quotadaten nicht laden.  Der Baumwurzelknoten ist zerstört.
 Kann Gerätenamen für %s nicht bekommen
 Kann genau verwendeten Platz nicht bestimmen... Die Ergebnisse können ungenau sein.
 Kann Rechnernamen nicht bekommen: %s
 Kann Informationen für %s-Quotadatei auf %s nicht vom Kernel bekommen: %s
 Kann Namen für uid/gid %u nicht auflösen.
 Kann Namen der Quotadatei im neuen Format nicht bekommen.
 Kann Namen der alten Quotadatei auf Dateisystem %s nicht bekommen.
 Kann Quota für %s %d vom Kernel auf %s nicht bekommen: %s
 Kann Quota-Informationen für Nutzer %s nicht erhalten
 Kann Quota-Informationen für Nutzer %s nicht erhalten.
 Kann Quotadateinamen für %s nicht erhalten
 Kann Syseminformationen nicht abrufen: %s
 Kann das Format nicht aus dem Dateinamen auf Dateisystem %s ableiten.  Bitte geben Sie das Format auf der Kommandozeile an.
 Kann IO für neue Quotadatei nicht initialisieren: %s
 IO kann auf xfs/gfs2-Quotadatei nicht initialisiert werden: %s
 Kann die Mountpunkt-Prüfung nicht initialisieren.
 Kann Quotas auf %s nicht initialisieren: %s
 Kann mich Quota-Muticast-Gruppe nicht anschließen: %s
 Kann %s nicht öffnen: %s
 Kann %s nicht öffnen: %s
Werde Gerätenamen verwenden.
 Kann keine Datei mit Einhängepunkten öffnen.
 Kann Datei nicht öffnen %s: %s
 Kann Datei der Gruppenadministratoren nicht öffnen: %s
 Kann neue Quotadatei %s nicht öffnen: %s
 Kann Quotadatei alten Formats nicht für %ss in %s öffnen
 Kann Quotadatei im alten Format auf %s nicht öffnen: %s
 Datenstrom %s kann nicht geöffnet werden
 Kann Quotadatei nicht öffnen %s: %s
 Kann Eingabezeile %d nicht verarbeiten.
 Kann Zeile %d in Quotatabelle nicht verarbeiten (fehlender ':')
 Kann die Zeit bei CC_BEFORE-Variable nicht verarbeiten (Zeile %d).
 Kann Block nicht lesen %u: %s
 Kann Eintrag für Id %u aus Quotadatei %s nicht lesen: %s
 Kann ersten Eintrag aus der Quotadatei %s nicht lesen: %s
 Kann Kopfinformationen aus Quotadatei %s nicht lesen: %s
 Kann Kopfinformation einer Quotadatei alten Formats nicht lesen.
 Kann einzelne Gnadenfristen nicht aus Datei lesen.
 Kann Information nicht aus Quotadatei %s lesen: %s
 Kann Information über Quotadatei im alten Format nicht lesen.
 Kann Quotastruktur für Id %u nicht lesen: %s
 Kann Quotas nicht aus Datei lesen.
 Kann 'stat'-Datei %s nicht lesen: %s
 Kann Callbackfunktion für Netlink-Nachrichten nicht registrieren: %s
 Kann Dateisystem %s nicht als les- und schreibbar ummounten.  Kann neue Quotadateien nicht schreiben.
 Kann das Dateisystem auf %s nicht als nur-lesbar ummounten.  Gezählte Werte könnten falsch sein.
Bitte beenden Sie alle Programme, die auf das Dateisystem schreiben oder verwenden Sie -m, um die Prüfung zu erzwingen.
 Kann das Dateisystem auf %s nicht als nur-lesbar ummounten.  Gezählte Werte könnten falsch sein.
 Kann neue Quotadatei %s nicht in %s umbenennen: %s
 Kann alte Quotadatei %s nicht in %s umbenennen: %s
 Kann temporäre Datei nicht neu öffnen: %s
 Kann Datei nicht zum wiederholten Male öffnen! Kann Signalbehandler nicht zurücksetzen: %s
 Kann Mountpunkt-Pfad %s nicht auflösen: %s
 Kann Pfad %s nicht auflösen: %s
 Kann Quota-Netlink-Namen nicht auflösen: %s
 Kann nicht gleichzeitig die individuelle und die Standard-Gnadenfrist setzen.
 Die Kulanzzeiten kann nicht über das RPC-Protokoll festgelegt werden.
 Kann Informationen für %s-Quotadatei vom Kernel auf %s nicht setzen: %s
 Kann Quota für %s %d vom Kernel auf %s nicht setzen: %s
 Kann Signalbehandler nicht setzen: %s
 Konnte Socketoptionen nicht setzen: %s
 stat()-Aufruf für Gerät %s fehlgeschlagen (vielleicht ein Fehler in der Quotatabelle)
 Kann Verzeichnisinformationen nicht abrufen %s: %s
 Kann stat() für Mountpunkt %s nicht aufrufen: %s
 Kann Informationen über Quotadatei %s nicht abrufen: %s
 stat() für Einhängepunkt %s fehlgeschlagen: %s
Überspringe...
 stat() für Einhängepunkt %s fehlgeschlagen: %s
Überspringe...
 stat() für eingebundenes Gerät %s fehlgeschlagen: %s
 stat() für Einhängepunkt %s fehlgeschlagen: %s
 statfs() für %s fehlgeschlagen: %s
 Kann %s-Quota-Kontierung nicht abschalten auf %s, wenn Vollstreckung aktiv ist
 Kann Quotas auf Gerät %s nicht synchronisieren: %s
 Kann Quotas vom Typ %s auf %s nicht deaktivieren: %s
Der Kernel wird nichts von quotachecks Änderungen mitbekommen.
 Kann Quotas vom Typ %s auf %s nicht aktivieren: %s
Der Kernel wird nichts von quotachecks Änderungen mitbekommen.
 Kann Quotas via RPC nicht an-/ausschalten.
 Kann nicht auf das Mailprogramm warten: %s
 Kann Block nicht schreiben (%u): %s
 Kann Gnadenfristen nicht in Datei ablegen.
 Kann einzelne Gnadenfristen nicht in Datei schreiben.
 Kann Quota nicht schreiben (Id %u): %s
 Kann Quota für %u auf %s nicht schreiben: %s
 Kann Quotainformationen nicht in Datei schreiben.
 %d Verzeichnisse und %d Dateien geprüft
 Prüfe Kopfinformationen der Quotadateien...
 Prüfe Informationen der Quotadatei...
 Compiliert mit:%s
 Prüfung unter Annahme der Version von der Kommandozeile fortsetzen? Zerstörte Blöcke:  Ungültige Anzahl verwendeter Einträge (%u) PID Datei '%s' konnte nicht geschlossen werden.
 Konnte keine Werte für %s bekommen.
 PID file '%s': %s konnte nicht geöffnet werden
 LDAP-Verbindung kann nicht eingerichtet werden.
 PID %jd konnte nicht gespeichert werden.
 PID des Systemdienstes konnte nicht in '%s' geschrieben werden.
 Erzeugung des %s-Quotaformats wird nicht unterstützt.
 Daten ausgegeben.
 Zugang verweigert für Rechner %s
 Quotaformat %s erkannt
 Gerät (%s): Dateisystem ist auf einem nicht unterstützten Gerätetyp eingehängt. Überspringe.
 Deaktiviere %s-Quota-Kontierung auf %s
 Deaktiviere %s-Quota-Vollstreckung %sauf %s
 Datenträgerquotas für %s %s (%cid %d):
 Dateisystemquotas für %s %s (%cid %u): %s
 Ich weiß nicht, wie das Format %d zwischenzuspeichern ist
 Gebe gesammelte Daten für %ss aus.
 Doppelte Einträge. EXT2_IOC_GETFLAGS fehlgeschlagen: %s
 Aktiviere XFS-%s-Quota-Kontierung während des Mountens
 Aktiviere %s-Quotavollstreckung auf %s
 Aktiviere %s-Quota auf dem root-Dateisystem (starten Sie neu, damit diese Änderung wirksam wird)
 Vollstreckung der %s-Quota schon aktiv auf %s
 Eintrag für Id %u ist abgeschnitten.
 Eintrag für Client %s nicht gefunden.
 Fehler Fehler bei Prüfung des Gerätenamens: %s
 Fehler in Konfigurationsdatei (Zeile %d).  Ignoriere.
 Fehler beim Parsen einer Netlink-Nachricht.
 Fehler beim Bearbeiten von Gnadenfristen.
 Fehler beim Bearbeiten von Quotas.
 Fehler beim Holen von alten Quotastatistiken vom Kernel: %s
 Fehler beim Holen von Quotastatistiken vom Kernel: %s
 Fehler beim Öffnen der alten Quotadatei %s: %s
 Fehler bei Dateifreigabe auf %s
 Fehler beim Suchen nach der alten Quotadatei %s: %s
 Fehler beim Syncen der Quotas auf Dateisystem %s: %s
 Fehler mit %s.
 Beende.
 Konnte Quota nicht löschen: %s
 Konnte TTY von Nutzer %llu nicht finden, um eine Warnung dorthin auszugeben.
 Konnte TTY %s von Nutzer %llu nicht öffnen, um eine Warnung auszugeben.
 Konnte Gnadenfristendatei nicht parsen.
 Konnte Quota-Netlink-Nachricht nicht lesen oder parsen: %s
 Konnte IMMUTABLE-Flag in der Quotadatei nicht löschen %s: %s
 Konnte Nachricht nicht an DBUS schreiben: Nicht genug Speicher.
 Konnte Quotanachricht für Nutzer %llu an %s nicht schreiben: %s
 Dateiinformationen abgearbeitet.
 Dateilimit erreicht auf Dateisystem Dateisystem          belegt  soft    hart  Gnadenf. belegt soft hart Gnadenf.
 Dateisystem als les- und schreibbar umgemountet.
 Dateisystem als nur-lesbar umgemountet
 Ersten Eintrag geladen.
 Ungültige UUID gefunden: %s
 i_num %ld, Blöcke %ld gefunden
 Mehrere Strukturen für Id %u gefunden. Verwende Werte: BHARD: %lld BSOFT: %lld IHARD: %lld ISOFT: %lld
 Mehrere Strukturen für Id %u gefunden. Werte: BHARD: %lld/%lld BSOFT: %lld/%lld IHARD: %lld/%lld ISOFT: %lld/%lld
 G Gid-set-Speicheranforderung (ngroups=%d): %s
 Going to check %s quota file of %s
 Gnadenfrist bevor die weichen Limits durchgesetzt werden für %ss:
 Gruppe Gruppen- und Benutzerquotas können nicht gleichzeitig verändert werden.
 Kopfinformationen geprüft.
 Kopfinformationen von Datei %s geprüft.  Lade Daten...
 Hallo,

wir haben festgestellt, dass die Gruppe %s, in der Sie Mitglied sind, Quotas dieses Systems
überschreitet.  Folgende Quotas wurden überschritten:

 Hallo,

wir haben festgestellt, dass Sie die Quotalimits auf diesem System
überschreiten.  Wir haben folgende Überschreitungen festgestellt:

 Hohe uid festgestellt.
 Id %u hat mehrere Strukturen.  Benutzereingriff erforderlich (verwende -i für interaktiven Modus oder -n für automatische Antworten).
 Ungültige Referenz auf freien Block %u Illegale Portnummer: %s
 Ungültige Referenz (%u >= %u) in %s-Quotadatei auf %s. Quotadatei ist wahrscheinlich defekt.
Bitte lassen Sie quotacheck(8) laufen und versuchen Sie es nochmal.
 In der Blockgnadenfrist auf In der Dateignadenfrist auf Ungültige Formatierungsanweisung für Variable %s.
Nicht erkannter Ausdruck %%%c.
 Info Inode: #%llu (%llu Blöcke, %u Extents)
 Inode: keines
 Füge schon vorhandenen Quotaeintrag ein (Block %u).
 Ungültiges Argument "%s"
 K Kernel Quota Version: %u.%u.%u
 Kernel Quota Version: alt
 LDAP-Bibliotheksversion >= 2.3 festgestellt. Bitte verwenden Sie LDAP_URI anstelle von Rechnername und Port.
Generierte URI %s
 Leaving %s
 Zeile %d zu lang.
 Zeile %d zu lang. Schneide sie ab.
 Lade den ersten Quotaeintrag mit Gnadenfristen.
 M Maximum %u dquots (currently %u incore, %u on freelist)
 Vielleicht sollten Sie neue Quotadateien mit quotacheck(8) erzeugen?
 Metadata init_io aufgerufen, obwohl Kernel die allgemeine Quotaschnittstelle nicht unterstützt!
 Metadata init_io aufgerufen, obwohl Kernelunterstützung dafür nicht aktiviert.
 Mountpunkt %s ist kein Verzeichnis?!
 Einhängepunkt oder Gerät %s nicht gefunden oder hat keine aktivierten Quotas.
 Einhängepunkt nicht angegeben.
 Mehrere Einträge gefunden für Client %s (%d).
 Name muss entweder quotaon oder quotaoff sein, aber nicht %s
 Name der Quotadatei zu lang.  Kontaktiere %s.
 Es wurde kein korrekter Einhängepunkt angegeben.
 Kein Dateisystem angegeben.
 Keine Dateisysteme mit Quotainformationen gefunden.
 Kein mögliches Ziel für Nachrichten.  Nichts zu tun.
 Nicht alle angegebenen Mountpunkte verwenden Quotas.
 Nicht genug Speicherplatz für Erstellung des PID Dateinamens.
 Nicht genug Speicher.
 Keine zerstörten Blöcke gefunden.  Gratulation.
 Setze Blockgnadenfrist auf %s nicht, denn das weiche Limit ist nicht überschritten.
 Setze Inodegnadenfrist auf %s nicht, denn das weiche Limit ist nicht überschritten.
 Anzahl an belegten dquot-Datenstrukturen: %ld
 Anzahl an dquot-Cachetreffern: %ld
 Anzahl an dquot-Löschungen: %ld
 Anzahl an dquot-Nachschlagungen: %ld
 Anzahl an dquot-Lesevorgängen: %ld
 Anzahl an dquot-Schreibvorgängen: %ld
 Anzahl freier dquot-Datenstrukturen: %ld
 Anzahl verwendeter dquot-Einträge (Benutzer/Gruppen): %ld
 Anzahl an Quotadatei-Synchronisierungen: %ld
 AUS AN Die alte gefundene Datei wurde während der Prüfung entfernt!
 Alte Datei nicht gefunden.
 Nur das RPC-Quotaformat ist bei NFS-Dateisystemen erlaubt.
 Nur das XFS-Quotaformat ist auf XFS-Dateisystemen erlaubt.
 Über dem Blockquota auf Über dem Dateiquota auf Einlesefehler in Zeile %d.  Kann Administratornamen nicht finden.
 Einlesefehler in Zeile %d.  Kann kein Ende des Gruppennamens finden.
 Einlesefehler in Zeile %d.  Zusätzliche Zeichen nach dem Administratornamen.
 Möglicher Fehler in Konfigurationsdatei in Zeile %d.  Ignoriere.
 Die Angabe eines Vorlagenamens macht keinen Sinn, wenn Gnadenfrist oder Standard-Gnadenfrist bearbeitet werden soll.
 Prototyp macht keinen Sinn beim Bearbeiten von Gnadenfristen.
 Quotavollstreckung schon deaktiviert für %s auf %s
 Quotadatei %s hat das IMMUTABLE-Flag gesetzt.  Lösche es.
 Kopfinformationen der Quotadatei %s sind zerstört.  Bitte geben Sie das Format auf der Kommandozeile an.
 Quotadateiformatversion %d stimmt nicht mit der Version überein, die auf der Kommandozeile angegeben wurde (%d).  Quotadateikopf könnte inkonsistent sein.
 Quotadatei nicht gefunden oder hat falsches Format.
 Quotadatei auf %s [%s] existiert nicht oder hat falsches Format.
 Quota für %ss ist am Mountpunkt %s aktiviert, dh. quotacheck könnte die Datei beschädigen.
 Quota für %ss ist bei Mountpunkte %s aktiviert, dh. quotacheck könnte die Datei beschädigen.
Bitte deaktivieren Sie Quotas oder verwenden Sie -f, um die Prüfung zu erzwingen.
 Quota für Id %u referenziert aber nicht vorhanden.
 Quotaformat wird vom Kernel nicht unterstützt.
 Das Dateisystem unterstützt keine Quotas.
 Quota-Datenstruktur in Quotadatei für Typ %s nicht vorhanden! Etwas ist wirklich nicht in Ordnung...
 Quotastruktur hat Versatz (offset) zu einem anderen Block (%u) als sie sollte (%u).
 Quota-Hilfsprogramme Version %s.
 Quotaschreiben fehlgeschlagen (Id %u): %s
 RPC-Quotaformat nicht mit einkompiliert.
 Es wurde das RPC-Quotaformat für ein Nicht-NFS-Dateisystemen angegeben.
 Referenz zu ungültigem Block %u Benenne neue Dateien mit letztendlichen Namen.
 Benenne neue Quotadatei um
 Benenne alte Quotadatei in %s~ um
 Fehler bitte an <%s> melden.
 Repquota funktioniert nicht über RPC.
 Erforderliches Format %s wird nicht vom Kernel unterstützt.
 Prüfe %s [%s]  Scanne auf dem Verzeichnisstapel gespeicherte Verzeichnisse
 Festlegen der Kulanzzeiten auf %s wird nicht unterstützt.
 Setze Gnadenfristen und andere Flags auf Standardwerde.
Nehme an, daß die Blockanzahl %u beträgt.
 Weitermachen? Sollen neue Werte verwendet werden? Dateigröße: %lu
Blöcke: %u Freier Block: %u Block mit freiem Eintrag: %u Flags: %x
 Überspringe %s [%s]
 Überspringe Zeile.
 Etwas seltsames ist beim Scannnen passiert.  Fehler %d
 Platz Sowohl -n als auch -t wurden angegeben, aber nur eines von beiden kann verwendet werden.
 Angegebener Pfad %s ist weder ein Verzeichnis noch ein Gerät.
 Statistik:
Gesamtblockzahl: %u
Datenblöcke: %u
Enträge: %u
Durchschnittlich verwendet: %f
 %lu Bytes subtrahiert.
 Subtrahiere den Platz, den die alte Quotadatei für Quotas vom Typ %s verwendet.
 T Der laufende Kernel unterstützt kein XFS.
 Zeiteinheiten dürfen sein: days, hours, minutes, oder seconds
 Gnadenfristen, bis weiche Limits durchgesetzt werden für %s %s (%cid %d):
 Zu viele Parameter für den Editor.
 Es wird versucht, die Quota-Grenzen außerhalb des vom Quota-Format unterstützten Bereichs auf %s zu setzen.
 Es wird versucht, die Quota-Nutzung außerhalb des vom Quota-Format unterstützten Bereichs auf %s zu setzen.
 Versuche Information in eine nur-lesbare Quotadatei auf %s zu schreiben
 Versuche Information in eine nur-lesbare Quotadatei auf %s zu schreiben.
 Versuche Quota in eine nur-lesbare Quotadatei auf %s zu schreiben
 Kann Namen '%s' in Zeile %d nicht auflösen.
 Undefinierter Programmname.
 Unerwarteter XFS-Quota-Zielzustand angefordert auf %s
 Unbekannte Aktion sollte ausgeführt werden.
 Unbekannte Dezimaleinheit. Gültige Einheiten sind k, m, g, t. Unbekanntes Format einer Netlink-Nachricht vom Kernel!
Vielleicht sind Ihre Quota-Tools zu alt?
 Unbekannte Option '%c'.
 Unbekanntes Ausgabeformat: %s
Unterstützte Formate sind:
  default - Standard
  csv     - Komma separierte Werte
  xml     - einfaches XML
 Unbekanntes Quotaformat: %s
Unterstützte Formate sind:
  vfsold - originalas Quotaformat
  vfsv0 - neues Quotaformat
  vfsv1 - Quotaformat mit 64-bit-Limits
  rpc - verwende RPC-Aufrufe
  xfs - XFS-Quotaformat
 Unbekanntes Mengeneinheiten. Gültige Einheiten sind K, M, G, T. Fehlendes Zeilendendezeichen in der letzten Zeile.  Ignoriere.
 Verwendung:
	edquota %1$s[-u] [-F Formatname] [-p Benutzername] [-f Dateisystem] Benutzername …
	edquota %1$s-g [-F Formatname] [-p Gruppenname] [-f Dateisystem] Gruppenname …
	edquota [-u|g] [-F Formatname] [-f Dateisystem] -t
	edquota [-u|g] [-F Formatname] [-f Dateisystem] -T Benutzername|Gruppenname …
 Verwendung:
  setquota [-u|-g] %1$s[-F Quotaformat] <Benutzer|Gruppe>
	<block-softlimit> <block-hardlimit> <inode-softlimit> <inode-hardlimit> -a|<Dateisystem>...
  setquota [-u|-g] %1$s[-F Quotaformat] <-p protouser|protogroup> <Benutzer|Gruppe> -a|<Dateisystem>...
  setquota [-u|-g] %1$s[-F Quotaformat] -b [-c] -a|<Dateisystem>...
  setquota [-u|-g] [-F Quotaformat] -t <blockgrace> <inodegrace> -a|<Dateisystem>...
  setquota [-u|-g] [-F Quotaformat] <Benutzer|Gruppe> -T <blockgrace> <inodegrace> -a|<Dateisystemsystem>...

-u, --user                 Setze Limit für Benutzer
-g, --group                Setze Limits für Gruppe
-a, --all                  Setze Limits für alle Dateisysteme
    --always-resolve       Versuche stets einen Namen aufzulösen, auch wenn er
                           nur aus Ziffern besteht
-F, --format=Quotaformat    Operiere auf einem bestimmten Quotaformat
-p, --prototype=protoname  Kopiere Limits von Benutzer/Gruppe
-b, --batch                Lese Limits von Standardeingabe
-c, --continue-batch       Eingabeverarbeitung auch bei Fehlern fortsetzen
 Verwendung: %s [-acfugvViTq] [Dateisystem...]
 Verwendung: %s [Optionen]
Optionen sind:
 -h --help             zeigt diesen Text
 -V --version          zeigt Versionsinformationen
 -F --foreground       Startet den Quota-Service im Vordergrund
 -I --autofs           Einhängepunkte vom Automounter nicht ignorieren
 -p --port <port>      Lausche auf gegebenem Port
 -s --no-setquota      Deaktiviert entfernte Aufrufe für setquota (Voreinstellung)
 -S --setquota         Aktivierte entfernte Aufrufe für setquota
 -x --xtab <path>      Daeipfad zu alternativer NFSD-export-Tabelle
 Verwendung: %s [Optionen]
Optionen sind:
 -h --help             zeigt diesen Text
 -V --version          zeigt Versionsinformationen
 -F --foreground       Startet den Quota-Service im Vordergrund
 -I --autofs           Einhängepunkte vom Automounter nicht ignorieren
 -p --port <port>      Lausche auf gegebenem Port
 -x --xtab <path>      Daeipfad zu alternativer NFSD-export-Tabelle
 Verwendung: %s [Optionen]
Optionen sind:
 -h --help         Zeigt diesen Text
 -V --version      Zeigt Versionsinformation
 -C --no-console   Versuche, keine Nachrichten auf die Konsole auszugeben
 -b --print-below  Auch Nachrichten auf Konsole ausgeben, wenn weiche oder harte Limits unterschritten werden
 -D --no-dbus      Versuche nicht, Nachrichten an DBUS zu schicken
 -F --foreground   Lasse den Dämon im Vordergrund laufen
 Verwendung: quota [-guqvswim] [-l | [-Q | -A]] [-F quotaformat]
 Benutzer Hilfsprogramm zum Prüfen und Reparieren von Quotadateien.
%s [-gucbfinvdmMR] [-F <quota-format>] filesystem|-a

-u, --user                Benutzerdateien prüfen
-g, --group               Gruppendateien prüfen
-c, --create-files        Neue Quotadateien erzeugen
-b, --backup              Sicherungskopien alter Quotadateien erzeugen
-f, --force               Prüfung erzwingen, sogar wenn Quotas aktiviert sind
-i, --interactive         interaktiver Modus
-n, --use-first-dquot     Verwende die erste Kopie einer "duplicated structure"
-v, --verbose             gib mehr Informationen aus
-d, --debug               gib noch mehr Informationen aus
-m, --no-remount          Dateisystem nicht als nur-lesbar neu mounten
-M, --try-remount         Versuche, das Dateisystem als nur-lesbar zu mounten,
                          aber setze fort, wenn das fehlschlägt
-R, --exclude-root        Das Root-Dateisystem nicht prüfen, wenn alle Dateisysteme geprüft werden
-F, --format=formatname   Quotadateien eines bestimmten Formats prüfen
-a, --all                 Prüfe alle Dateisysteme
-h, --help                Zeige diese Nachricht an und beende
-V, --version             Zeige Versionsinformation und beende

 Hilfsprogramm zur Konvertierung von Quota-Dateien.
Verwendung:
	%s [Optionen] Mountpunkt

-u, --user                          Eine Nutzerquotadatei umwandeln
-g, --group                         Eine Gruppenquotadatei umwandel
-e, --convert-endian                Quotadatei in richtige Bytereihenfolge umwandeln
-f, --convert-format oldfmt,newfmt  Aus altem Format in das  VFSv0-Quotaformat umwandeln
-h, --help                          Diesen Hilfetext anzeigen und beenden
-V, --version                       Versionsinformation ausgeben und beenden

 WARNUNG - Quotadatei %s wurde wahrscheinlich abgeschnitten.  Kann Quotaeinstellungen nicht retten...
 WARNUNG - %s: Kann aktuelle Blockbelegung nicht ändern
 WARNUNG - %s: Kann aktuelle Inodebelegung nicht ändern
 WARNUNG - Quotadatei %s hat ungültige Kopfinformationen
 WARNUNG - Quotadatei %s wurde wahrscheinlich abgeschnitten.  Kann Quotaeinstellungen nicht retten...
 WARNUNG - Quotadateiinformation wurde zerstört.
 WARNUNG - Quotadatei %s wurde wahrscheinlich abgeschnitten.  Kann Quotaeinstellungen nicht retten...
 WARNUNG - Einige Daten könnten wegen der Zerstörung geändert werden.
 Warnung Warnung: Kann Exporttabelle %s nicht öffnen: %s
Verwende '/' als Pseudodateisystemwurzel.
 Warnung: Kann EXT2-Flags für %s nicht setzen: %s
 Warnung: Ignoriere -%c, wenn eine Dateisystemliste angegeben wurde.
 Warnung: Mailprogramm hat sich nicht korrekt beendet.
 Warnung: Kein Quotaformat im Kernel festgestellt.
 XFS-Quota-Manager dquot-Statistiken
 XFS-Quotaformat nur auf XFS-Dateisystemen erlaubt.
 XFS_IOC_FSBULKSTAT ioctl-Aufruf fehlgeschlagen: %s
 Sie müssen die auszuführende Aktion angeben.
 Für Konvertierungen müssen Sie das Quell- und das Zielformat angeben.
 Ihr Kernel unterstützt wahrscheinlich Journalquotas, aber Sie verwenden sie nicht.  Vielleicht sollten Sie auf Journalquotas umsteigen, um nicht mehr quotacheck nach jedem unsauberen Herunterfahren ausführen zu müssen.
 Ihre Quotadatei ist in der falschen Bytereihenfolge gespeichert.  Bitte konvertieren Sie sie mit convertquota(8).
 und -kontierung  falsches Format:
%s
 Blocklimit erreicht Blockquota überschritten Blockquota zu lange überschritten Blöcke TCP Dienst kann nicht gestartet werden.
 Kann keinen UDP-Dienst erzeugen.
 Kann %s auf %s [%s] nicht finden
 Kann nicht öffnen %s: %s
 Kann die Zeiten für %s nicht schreiben.  Vielleicht unterstützt der Kernel diese Operation nicht?
 copy_user_quota_limits: Benutzerquota für uid %ld holen fehlgeschlagen: %s
 copy_user_quota_limits: Benutzerquota für uid %ld setzen fehlgeschlagen: %s
 Tag Tage fertig
 Fehler (%d) beim Öffnen von %s
 Fehler (%d) beim Öffnen des "Inode-Scans"
 Fehler (%d) beim Start des Inode-Scans
 Fehler beim Holen der Quotas von %s für %s (Id %u): %s
 Dateilimit erreicht Dateiquota überschritten Dateiquota zu lange überschritten Dateien find_free_dqentry(): Datenblock voll, aber er sollte es nicht sein.
 Dateisystemname paßt nicht
 g getgroups(): %s
 Blocklimit unterschritten Blockquota unterschritten Dateilimit unterschritten Dateiquota unterschritten Gnadenfrist Gruppe Gruppe %s existiert nicht.
 Rechner %s hat versucht, setquota von einem Port >= 1024 aufzurufen
 Rechner %s versuchte setquota aufzurufen, während es deaktiviert ist
 Stunde Stunden k Limit m Minute Minuten kein aus an popd %s
Entering directory %s
 pushd %s/%s
 Quota quotactl auf %s [%s]: %s
 quotactl auf %s: %s
 quotactl() auf %s: %s
 Sekunde Sekunden Setze root_squash auf %s: %s
 Platz svc_run ist zurückgekehrt
 t Unfähig Argumente freizugeben
 undefiniert Unbekannte Block-Quota-Warnung nicht festgelegt Benutzer Benutzer %s existiert nicht.
 verwende %s auf %s [%s]: %s
 