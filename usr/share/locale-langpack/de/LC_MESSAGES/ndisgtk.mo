��          �   %   �      p     q     �  +   �  @   �  5        M  ,   _     �     �     �  $   �  	   �  2   �  $   2     W     Z     l  #   �  !   �     �  %   �               8     <     E     Y  �  h  !   �       -   5  B   c  5   �     �  6   �     *      J     k  &   �     �  :   �  *   �     	     	  !   .	  !   P	  "   r	     �	  ;   �	     �	     �	     
     
     
     2
                    
                                                                                          	             <b>%s</b>
Hardware present: %s <b>%s</b>
Invalid Driver! <b>Currently Installed Windows Drivers:</b> <span size="larger" weight="bold">Select <i>inf</i> file:</span> Are you sure you want to remove the <b>%s</b> driver? Configure Network Could not find a network configuration tool. Driver is already installed. Error while installing. Install Driver Is the ndiswrapper module installed? Location: Module could not be loaded. Error was:

<i>%s</i>
 Ndiswrapper driver installation tool No No file selected. Not a valid driver .inf file. Please drag an '.inf' file instead. Root or sudo privileges required! Select inf File Unable to see if hardware is present. Windows Wireless Drivers Wireless Network Drivers Yes _Install _Install New Driver _Remove Driver Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-10-02 17:50+0200
PO-Revision-Date: 2008-10-15 18:33+0000
Last-Translator: Julian Andres Klode <juliank@ubuntu.com>
Language-Team: German
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:44+0000
X-Generator: Launchpad (build 18115)
 <b>%s</b>
Hardware verfügbar: %s <b>%s</b>
Fehlerhafter Treiber! <b>Momentan installierte Windows Treiber:</b> <span size="larger" weight="bold"><i>inf</i> Datei wählen:</span> Möchten Sie den <b>%s</b> Treiber wirklich entfernen Netzwerk konfigurieren Konnte kein Programm zur Netzwerkkonfiguration finden. Treiber ist bereits installiert Fehler während der Installation Treiber installieren Ist das ndiswrapper Modul installiert? Ort Modul konnte nicht geladen werden. Fehler war:

<i>%s</i>
 Ndiswrapper Treiber Installations Programm Nein Keine Datei ausgewählt Keine korrekte .inf Treiber Datei Bitte wählen Sie eine .inf Datei Root oder sudo Rechte erforderlich Inf Datei wählen Es kann nicht geprüft werden ob die Hardware vorhanden ist Windows WLAN Treiber WiFi Treiber Ja _Installieren Neuen Treiber _installieren Treiber entfe_rnen 