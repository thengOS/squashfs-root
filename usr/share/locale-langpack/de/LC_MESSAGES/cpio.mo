��   *  0   �	  �  �                   T  �   U           5     <  .   I  .   x  %   �     �     �  0   �     )  "   C     f     |  0   �  !   �     �     �  /   
     :     Z     u     �  d   �     	     !     8  v   W  >   �  +     -   9     g     ~  ,   �     �  %   �  ,     -   8      f  (   �     �     �     �     �                 4      J      j      �      �   "   �   4   �   I   �   /   2!  /   b!     �!  .   �!     �!  J   �!  3   F"  E   z"     �"  7   �"  �   #  �   �#  G   $$     l$  "   �$  
   �$  '   �$  (   �$     %     #%  $   +%     P%     l%     {%     �%     �%     �%     �%      �%  i   &  2   �&     �&  '   �&  )   '  $   ,'     Q'  `   X'  7   �'  (   �'  (   (  >   C(     �(     �(  T   �(     )  3   )      P)     q)     v)     {)  \   �)  '   �)     *     *  E   7*  5   }*  C   �*  +  �*     #,     3,  ?   P,  B   �,  B   �,     -     1-     D-     [-     r-     �-  &   �-  2   �-     �-     .  s   .  A   �.     �.     �.     �.     �.     /     /     :/     A/  ;   H/  &   �/  8   �/  9   �/  :   0  /   Y0  0   �0  1   �0     �0     1     1     1  @   11     r1  D   �1  #   �1  &   �1  F   2  J   W2  6   �2     �2  ,   �2  '    3  !   H3     j3     �3  2   �3  ;   �3     4     4     4     $4     +4     G4  ,   `4      �4  $   �4  0   �4  3   5  I   85     �5  +   �5  &   �5  R   �5  ,   H6  6   u6  q   �6     7  /   =7     m7  Z   �7  6   �7     8     '8     ;8     N8     c8  0   o8     �8     �8  !   �8  +   �8  �   9     �9  "   �9  v   �9  6   H:  .   :     �:  ;   �:  3   ;  /   5;  +   e;  '   �;  #   �;     �;     �;     <  ]   )<  	   �<     �<     �<     �<     �<  "   �<     �<     =  B   (=  +   k=     �=     �=     �=     �=     �=     >     >     (>     >>  ,   V>  4   �>     �>  %   �>     �>     ?      ?     4?     B?     X?  -   x?  	   �?  !   �?  	   �?  I   �?     &@     *@     E@  $   Y@     ~@     �@  5   �@  e   �@     BA     ]A     pA     �A     �A     �A     �A     �A  	   �A     �A     B  	   B     B     0B     IB     _B     uB  
   zB  
   �B  
   �B     �B  %   �B     �B     �B     �B     C     (C     BC     RC     XC     _C     rC  *   vC  ;   �C  !   �C     �C  .   D  D   GD     �D  �  �D  �   |F     WG     rG     {G  /   �G  2   �G  )   �G     H     -H  /   @H     pH  %   �H     �H     �H  @   �H  .   #I  "   RI  8   uI  7   �I  1   �I  $   J     =J     ]J  o   }J     �J  +   K  (   1K  �   ZK  J   �K  8   'L  1   `L     �L      �L  *   �L  !   �L  )   M  *   @M  +   kM  (   �M  2   �M     �M     N     +N  +   FN     rN     �N     �N     �N     �N     O     O  (   	O  .   2O  Y   aO  D   �O  D    P     EP  7   aP     �P  [   �P  1   Q  _   GQ  (   �Q  F   �Q  �   R  �   �R  [   �S     %T  5   ET     {T  <   �T  =   �T  ,   U     0U  6   7U  @   nU  %   �U      �U     �U  -   V     DV     cV  #   sV  �   �V  7   !W  #   YW  2   }W  4   �W  &   �W     X  j   X  E   ~X  /   �X  /   �X  :   $Y  4   _Y  5   �Y  a   �Y     ,Z  G   >Z  +   �Z     �Z  
   �Z     �Z  m   �Z  5   8[  "   n[     �[  V   �[  G    \  G   H\  �  �\     "^     5^  @   Q^  l   �^  _   �^     __     }_     �_  '   �_  &   �_     �_  *   `  .   @`     o`     �`  �   �`  J   !a     la     �a     �a     �a     �a  ,   �a     �a     �a  B   �a  +   ;b  F   gb  G   �b  H   �b  2   ?c  3   rc  4   �c     �c     �c     �c     d  O   $d     td  D   �d  9   �d  +   	e  U   5e  m   �e  ;   �e     5f  ;   Tf  )   �f  #   �f     �f     �f  ^   g  \   wg  "   �g     �g     �g     h  -   h  :   6h  1   qh  2   �h  $   �h  2   �h  C   .i  H   ri  +   �i  5   �i  '   j  h   Ej  7   �j  A   �j  �   (k  $   �k  P   �k     /l  q   Bl  I   �l     �l     m     -m     Gm     `m  3   ym     �m  !   �m  1   �m  /   	n  �   9n     o  *   .o  }   Yo  Z   �o  4   2p     gp  @   �p  7   �p  3   �p  /   0q  +   `q  '   �q  #   �q     �q     �q  �   r     �r     �r     �r     �r     �r  )   �r  (   s     Es  \   [s  >   �s     �s  %   t     <t  !   [t  #   }t     �t     �t      �t  "   �t  J   u  a   bu  &   �u  1   �u  !   v     ?v     Rv     ev  "   rv  "   �v  :   �v  
   �v     �v     w  f   +w     �w  #   �w     �w  #   �w      �w     x  =   &x  d   dx  '   �x     �x     	y      y  %   :y     `y     uy     �y     �y     �y     �y  	   �y     �y     �y     
z     %z     >z  
   Cz  
   Nz     Yz     kz  *   }z     �z  2   �z     �z     {     .{     N{     f{     v{     �{     �{  2   �{  f   �{  .   7|     f|  3   �|  N   �|     	}     �   �            �       �   �       �   �       	               [   �       #   U   �      �      �   �       ,   �   �   �   a   .          3   &   ^   
  !  :   �       W   �   �   �   �   �   �       �   C   "        �   �   �   $       �       z   r   �          �   c   #  `   �   �               �       �   |   0   �     �         �           @   <      �       �   �       �   �      �          �         !   9   �   �     v   7   J   h       u       4   �       K           i   P   �   m               �   k   �       '  �   �     f   �       �   �       �   �   �           �          �   \   O   I      �   �   )     �   V   ;   M   D          G             s   �   �          �       �      g   8   �   6                       �                   B   
   �                  	  �     %   �                           �     �         �       �       �             2   �   *   �   x   �   �   �   �   1   �   H   �   �      E       "       (   S       L   �   �   j       �       �      �   Q            _   ~   �   F       �           q       �       �   �   p   �   �   &  Z   -            �       ]     �          {   �   ?   �   (      �   �       �   �   n   o                   �   �   )      N     T   }     �      %  l           �   �       w              �   >   �   �       �   �   +   �          5     �       *  Y   �       e   /                     $   �   y   '     �       �       �   t   �   R        �   �      d   X      =       �   �           �                  b   A      }  $  8  '}         C   ����y}         9       !   ���� 
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

 
Report bugs to: %s
   or:   [OPTION...] %.*s: ARGP_HELP_FMT parameter must be positive %.*s: ARGP_HELP_FMT parameter requires a value %.*s: Unknown ARGP_HELP_FMT parameter %lu block
 %lu blocks
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s is meaningless with %s %s is not a character special file %s is not a directory %s linked to %s %s not created: newer or same age version exists %s not dumped: not a regular file %s: Cannot %s %s: Cannot change mode to %s %s: Cannot change ownership to uid %lu, gid %lu %s: Cannot create symlink to %s %s: Cannot hard link to %s %s: Cannot seek to %s %s: Cannot symlink to %s %s: Read error at byte %s, while reading %lu byte %s: Read error at byte %s, while reading %lu bytes %s: Too many arguments
 %s: Warning: Cannot %s %s: Warning: Cannot seek to %s %s: Warning: Read error at byte %s, while reading %lu byte %s: Warning: Read error at byte %s, while reading %lu bytes %s: Wrote only %lu of %lu byte %s: Wrote only %lu of %lu bytes %s: checksum error (0x%lx, should be 0x%lx) %s: field width not sufficient for storing %s %s: file name too long %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous
 %s: option '%s' requires an argument
 %s: option '--%s' doesn't allow an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option requires an argument -- '%c'
 %s: rmtclose failed %s: rmtioctl failed %s: rmtopen failed %s: symbolic link too long %s: truncating %s %s: truncating inode number %s: unknown file type %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' (C) (PROGRAM ERROR) No version known!? (PROGRAM ERROR) Option should have been recognized!? --append is used but no archive file name is given (use -F or -O options) --no-preserve-owner cannot be used with --owner --owner cannot be used with --no-preserve-owner --stat requires file names -F can be used only with --create or --extract -T reads null-terminated names A list of filenames is terminated by a null character instead of a newline ARGP_HELP_FMT: %s value is less than or equal to %s Append SIZE bytes to FILE. SIZE is given by previous --length option. Append to an existing archive. Archive file is local, even if its name contains colons Archive filename to use instead of standard input. Optional USER and HOST specify the user and host names in case of a remote archive Archive filename to use instead of standard output. Optional USER and HOST specify the user and host names in case of a remote archive Archive format is not specified in copy-pass mode (use --format option) Archive format multiply defined Archive value %.*s is out of range BLOCK-SIZE Both -I and -F are used in copy-in mode Both -O and -F are used in copy-out mode Byte count out of range COMMAND Cannot connect to %s: resolve failed Cannot execute remote shell Cannot open %s Command dumped core
 Command exited successfully
 Command failed with status %d
 Command stopped on signal %d
 Command terminated
 Command terminated on signal %d
 Control warning display. Currently FLAG is one of 'none', 'truncate', 'all'. Multiple options accumulate. Create all files relative to the current directory Create file of the given SIZE Create leading directories where needed Create the archive (run in copy-out mode) Creating intermediate directory `%s' DEVICE Dereference  symbolic  links  (copy  the files that they point to instead of copying the links). Display executed checkpoints and exit status of COMMAND Do not change the ownership of the files Do not print the number of blocks copied Do not strip file system prefix components from the file names Enable debugging info Error parsing number near `%s' Execute ARGS. Useful with --checkpoint and one of --cut, --append, --touch, --unlink Execute COMMAND Extract files from an archive (run in copy-in mode) Extract files to standard output FILE FLAG FORMAT File %s shrunk by %s byte, padding with zeros File %s shrunk by %s bytes, padding with zeros File %s was modified while being copied File creation options: File statistics options: Fill the file with the given PATTERN. PATTERN is 'default' or 'zeros' Found end of tape.  Load next tape and press RETURN.  Found end of tape.  To continue, type device/file name when ready.
 GNU `cpio' copies files to and from archives

Examples:
  # Copy files named in name-list to the archive
  cpio -o < name-list [> archive]
  # Extract files from the archive
  cpio -i [< archive]
  # Copy files named in name-list to destination-directory
  cpio -p destination-directory < name-list
 Garbage command Garbage in ARGP_HELP_FMT: %s General help using GNU software: <http://www.gnu.org/gethelp/>
 Generate sparse file. Rest of the command line gives the file map. In the verbose table of contents listing, show numeric UID and GID Interactively rename files Invalid byte count Invalid operation code Invalid seek direction Invalid seek offset Invalid size: %s Invalid value for --warning option: %s Link files instead of copying them, when  possible Main operation mode: Malformed number %.*s Mandatory or optional arguments to long options are also mandatory or optional for any corresponding short options. Manipulate a tape drive, accepting commands from a remote process Mode already defined NAME NUMBER Negative size: %s Not enough arguments Number out of allowed range: %s OFFSET OPTION Only copy files that do not match any of the given patterns Operation modifiers valid in any mode: Operation modifiers valid in copy-in and copy-out modes: Operation modifiers valid in copy-in and copy-pass modes: Operation modifiers valid in copy-out and copy-pass modes: Operation modifiers valid only in copy-in mode: Operation modifiers valid only in copy-out mode: Operation modifiers valid only in copy-pass mode: Operation not supported PATTERN Packaged by %s
 Packaged by %s (%s)
 Perform given action (see below) upon reaching checkpoint NUMBER Premature eof Print STRING when the end of a volume of the backup media is reached Print a "." for each file processed Print a table of contents of the input Print contents of struct stat for each given file. Default FORMAT is:  Read additional patterns specifying filenames to extract or list from FILE Read error at byte %lld in file %s, padding with zeros Read file names from FILE Removing leading `%s' from hard link targets Removing leading `%s' from member names Replace all files unconditionally Report %s bugs to: %s
 Report bugs to %s.
 Reset the access times of files after reading them Retain previous file modification times when creating files Run in copy-pass mode SECS SIZE STRING Seek direction out of range Seek offset out of range Seek to the given offset before writing data Set date for next --touch option Set the I/O block size to 5120 bytes Set the I/O block size to BLOCK-SIZE * 512 bytes Set the I/O block size to the given NUMBER of bytes Set the ownership of all files created to the specified USER and/or GROUP Size of a block for sparse file Substituting `.' for empty hard link target Substituting `.' for empty member name Swap both halfwords of words and bytes of halfwords in the data. Equivalent to -sS Swap the bytes of each halfword in the files Swap the halfwords of each word (4 bytes) in the files Synchronous execution actions. These are executed when checkpoint number given by --checkpoint option is reached. Synchronous execution options: To continue, type device/file name when ready.
 Too many arguments Truncate FILE to the size specified by previous --length option (or 0, if it is not given) Try `%s --help' or `%s --usage' for more information.
 Unexpected arguments Unknown date format Unknown field `%s' Unknown system error Unlink FILE Update the access and modification times of FILE Usage: Use given archive FORMAT Use remote COMMAND instead of rsh Use the old portable (ASCII) archive format Use this FILE-NAME instead of standard input or output. Optional USER and HOST specify the user and host names in case of a remote archive Valid arguments are: Verbosely list the files processed When reading a CRC format archive, only verify the CRC's of each file in the archive, don't actually extract the files Write files with large blocks of zeros as sparse files Write to file NAME, instead of standard output Written by %s and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, %s, and others.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
and %s.
 Written by %s, %s, %s,
%s, %s, %s, and %s.
 Written by %s, %s, %s,
%s, %s, and %s.
 Written by %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
and %s.
 Written by %s, %s, and %s.
 Written by %s.
 You must specify one of -oipt options.
Try `%s --help' or `%s --usage' for more information.
 [ARGS...] [USER][:.][GROUP] [[USER@]HOST:]FILE-NAME [destination-directory] ` `%s' exists but is not a directory ambiguous argument %s for %s blank line ignored cannot generate sparse files on standard output, use --file option cannot get the login group of a numeric UID cannot link %s to %s cannot make directory `%s' cannot open %s cannot open `%s' cannot read checksum for %s cannot remove current %s cannot seek cannot seek on output cannot set time on `%s' cannot swap bytes of %s: odd number of bytes cannot swap halfwords of %s: odd number of halfwords cannot unlink `%s' control magnetic tape drive operation created file is not sparse device major number device minor number device number error closing archive exec/tcp: Service not available failed to return to initial working directory file mode file name contains null character file size genfile manipulates data files for GNU paxutils test suite.
OPTIONS are:
 gid give a short usage message give this help list hang for SECS seconds (default 3600) incorrect mask (near `%s') inode number internal error: tape descriptor changed from %d to %d invalid archive format `%s'; valid formats are:
crc newc odc bin ustar tar (all-caps also recognized) invalid argument %s for %s invalid block size invalid count value invalid group invalid header: checksum error invalid user memory exhausted modification time name size no tape device specified number of links operation operation [count] premature end of archive premature end of file print program version rdev rdev major rdev minor read error rename %s ->  requested file length %lu, actual %lu set debug level set debug output file name set the program name standard input is closed standard output is closed stat(%s) failed stdin stdout too many arguments uid unable to record current working directory use device as the file name of the tape drive to operate on use remote COMMAND instead of rsh virtual memory exhausted warning: archive header has reverse byte-order warning: skipped %ld byte of junk warning: skipped %ld bytes of junk write error Project-Id-Version: cpio 2.10
Report-Msgid-Bugs-To: bug-cpio@gnu.org
POT-Creation-Date: 2010-03-10 15:04+0200
PO-Revision-Date: 2016-02-19 02:07+0000
Last-Translator: Roland Illig <roland.illig@gmx.de>
Language-Team: German <translation-team-de@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 16:09+0000
X-Generator: Launchpad (build 18115)
 
Lizenz GPLv3+: GNU GPL Version 3 oder später <http://gnu.org/licenses/gpl.html>.
Dies ist freie Software: Sie dürfen sie verändern und weiterverteilen.
Es gibt keine Garantie, soweit es Recht und Gesetz zulassen.

 
Melden Sie Fehler an: %s
   oder:   [OPTION...] %.*s: ARGP_HELP_FMT-Parameter muss positiv sein %.*s: ARGP_HELP_FMT-Parameter benötigt einen Wert %.*s: Unbekannter ARGP_HELP_FMT-Parameter %lu Block
 %lu Blöcke
 %s Webseite: <%s>
 %s Webseite: <http://www.gnu.org/software/%s/>
 %s ist bedeutungslos mit %s %s ist keine Buchstaben-Spezial-Datei »%s« ist kein Verzeichnis %s verbunden mit %s %s nicht erzeugt: Neuere oder gleichalte Version existiert schon %s nicht festgeschrieben: Kein reguläre Datei %s: Funktion »%s« fehlgeschlagen %s: Zugriffsrechte können nicht auf %s geändert werden %s: Kann den Besitzer nich auf uid %lu, gid %lu ändern %s: Kann symbolischen Link auf %s nicht erstellen %s: Kann nicht hart auf %s verlinken %s: Kann nicht nach %s springen %s: Kann nicht auf %s verlinken %s: Lesefehler bei Byte %s beim Lesen von %lu Byte %s: Lesefehler bei Byte %s beim Lesen von insgesamt %lu Byte %s: Zu viele Argumente
 %s: Warnung: Funktion »%s« fehlgeschlagen %s: Warnung: Kann nicht nach %s springen %s: Warnung: Lesefehler bei Byte %s beim Lesen von %lu Byte %s: Warnung: Lesefehler bei Byte %s beim Lesen von insgesamt %lu Byte %s: Nur %lu von %lu Byte geschrieben %s: Nur %lu von %lu Bytes geschrieben %s: Prüfsummenfehler (0x%lx, hätte 0x%lx sein müssen) %s: Feldbreite nicht passend zum Speichern von %s %s: Dateiname zu lang %s: Ungültige Option -- »%c«
 %s: Option »%c%s« erlaubt kein Argument
 %s: Option »%s« ist mehrdeutig
 %s: Option »%s« benötigt ein Argument
 %s: Option »--%s« erlaubt kein Argument
 %s: Option »-W %s« erlaubt kein Argument
 %s: Die Option »-W %s« ist mehrdeutig
 %s: Diese Option benötigt ein Argument -- »%c«
 %s: rmtclose fehlgeschlagen %s: rmtioctl fehlgeschlagen %s: rmtopen fehlgeschlagen %s: Symbolische Verknüpfung (link) zu lang %s: schneide Inode-Nummer %s ab %s: kürze inode-Nummer %s: unbekannter Dateityp %s: Unbekannte Option »%c%s«
 %s: Unbekannte Option »--%s«
 « © (PROGRAMMFEHLER) Keine Version bekannt!? (PROGRAM ERROR) Option sollte erkannt werden!? --append wurde angegeben, aber kein Archiv-Dateiname (benutzen Sie die Option -F oder -O) --no-preserve-owner kann nicht zusammen mit --owner verwendet werden --owner kann nicht zusammen mit --no-preserve-owner verwendet werden --stat erfordert Dateinamen -F kann nur mit --create oder --extract benutzt werden. -T liest null-terminierte Namen Eine Dateiliste wird durch Null-Zeichen separiert statt durch eine neue Zeile (»newline«) ARGP_HELP_FMT: Wert %s ist kleiner oder gleich %s GRÖSSE Bytes an DATEI anfügen. GRÖSSE wird mit einer vorhergehenden Option --length gesetzt. Zu einem vorhandenen Archiv hinzufügen. Archivdatei ist lokal, selbst wenn der Name einen Doppelpunkt enthält Benutze diesen Archivdateinamen statt der Standardeingabe. Die optionalen Parameter BENUTZER und RECHNER geben bei einem Archiv auf einem entfernten Rechner den Benutzernamen und den Namen des entfernten Rechners an. Benutze diesen Archivdateinamen statt der Standardausgabe. Die optionalen Parameter BENUTZER und RECHNER geben bei einem Archiv auf einem entfernten Rechner den Benutzernamen und den Namen des entfernten Rechners an. Im Modus »copy-pass« wurde kein Archivformat angegeben (benutzen Sie die Option --format) Archivformat mehrfach angegeben Archivwert %.*s ist außerhalb des gültigen Bereichs BLOCKGRÖSSE Sowohl -I als auch -F werden im Modus »copy-in« verwendet. Sowohl -O als auch -F werden im Modus »copy-out« verwendet. Bytezahl außerhalb des zulässigen Bereichs BEFEHL Kann nicht mit %s verbinden: Auflösung fehlgeschlagen Kann keine 'remote shell' ausführen (Shell auf anderen Rechner) Kann Verzeichnis »%s« nicht öffnen Befehl erzeugte einen Core-Dump
 Befehl erfolgreich ausgeführt
 Befehl schlug mit Beendigungsstatus %d fehl.
 Befehl hielt bei Signal %d an
 Befehl beendet
 Befehl beendete sich bei Signal %d
 Anzeige von Warnungen beeinflussen. Derzeit kann MARKIERUNG »none«, »truncate« und »all« sein. Mehrere Optionen werden akkumuliert. Alle Dateien relativ zum aktuellen Verzeichnis erzeugen Datei der gegebenen GRÖSSE anlegen Übergeordnete Verzeichnisse erzeugen, wenn nötig Archiv erstellen (cpio läuft im Modus »copy-out«) Erzeuge temporäres Verzeichnis ´%s´ GERÄT Symbolische Links dereferenzieren (die Dateien kopieren, auf die sie zeigen, statt die Links zu kopieren). ausgeführte Kontrollpunkte und Beendigungsstatus von BEFEHL anzeigen Eigentümerkennung von Dateien nicht verändern Die Anzahl der kopierten Blöcke nicht anzeigen Dateisystem-Erweiterungen nicht von Dateinamen abschneiden Ausgabe von Informationen zur Fehlersuche aktivieren Fehler beim Parser der Zahl in der Nähe von „%s“ ARGUMENTE ausführen. Nützlich mit --checkpoint und einem aus --cut, --append, --touch, --unlink BEFEHL ausführen Dateien aus einem Archiv extrahieren (cpio läuft im Modus »copy-in«) Dateien auf die Standardausgabe extrahieren DATEI MARKIERUNG FORMAT Datei %s um %s Byte geschrumpft, fülle mit Nullen auf Datei %s um %s Byte geschrumpft, fülle mit Nullen auf Datei %s wurde während des Kopiervorgangs verändert Optionen beim Anlegen von Dateien: Dateistatistikoptionen: Die Datei mit dem gegebenen MUSTER füllen. MUSTER ist „default“ oder „zeros“. Ende des Bandes gefunden. Nächstes Band einlegen und RETURN drücken.  Ende des Bandes gefunden. Zum Weitermachen Gerät/Datei-Namen angeben.
 GNU »cpio« kopiert Dateien in Archive hinein und aus diesen heraus.

Beispiele:
  # Kopiere die Dateien, die in »Namensliste« aufgeführt sind, in das »Archiv«
  cpio -o < Namensliste [> Archiv]
  # Extrahiere die Dateien aus dem »Archiv«
  cpio -i [< Archiv]
  # Kopiere die Dateien, die in »Namensliste« aufgeführt sind, in das »Zielverzeichnis«
  cpio -p Zielverzeichnis < Namensliste
 Ungültiger Befehl Unsinn in ARGP_HELP_FMT: %s Allgemeine Hilfe zu GNU-Software: <http://www.gnu.org/gethelp/>
 Löchrige („sparse“) Datei erzeugen. Der Rest der Kommandozeile gibt die die Dateibildungsvorschrift an. Zeige numerische Benutzer-ID (UID) und Gruppen-ID (GID) in der ausführlichen Inhaltsübersicht Dateien interaktiv umbenennen Ungültige Bytezahl Ungültiger Verarbeitungscode Ungültige Richtung für Positionierung Ungültiger Betrag für Positionierung Ungültige Größe: %s Ungültiger Wert für --warning-Option: %s Wenn möglich Dateien verlinken statt kopieren Hauptoperationsmodus: Ungültige Nummer %.*s Notwendige oder optionale Argumente für lange Optionen sind ebenso notwendig oder optional für die entsprechenden kurzen Optionen. Ein Bandlaufwerk bearbeiten, dabei Befehle von entferntem Prozess annehmen Modus bereits angegeben NAME NUMMER Negative Größe: %s Nicht genug Argumente Zahl außerhalb des zulässigen Bereichs: %s POSITION OPTION Nur Dateien kopieren, die auf keines der angegebenen Muster passen In allen Modi gültige Operationsparameter: In den Modi »copy-in« und »copy-out« gültige Operationsparameter: In den Modi »copy-in« und »copy-pass« gültige Operationsparameter: In den Modi »copy-out« und »copy-pass« gültige Operationsparameter: Im Modus »copy-in« gültige Operationsparameter: Im Modus »copy-out« gültige Operationsparameter: Im Modus »copy-pass« gültige Operationsparameter: Vorgang nicht unterstützt MUSTER Verpackt von %s
 Verpackt von %s (%s)
 gegebene Aktion (siehe unten) beim Erreichen des Kontrollpunkts ZAHL ausführen Vorzeitiges Dateiende TEXT ausgeben, wenn das Volume-Ende des Backup-Mediums erreicht wird Einen Punkt (».«) für jede verarbeitete Datei ausgeben Eine Liste des Inhalts der Eingabe anzeigen Inhalt des „struct stat“ für jede gegebene Datei ausgeben. Standard-FORMAT ist:  Zusätzliche Suchmuster (zur Auswahl der zu extrahierenden oder aufzulistenden Dateinamen) aus DATEI auslesen Lesefehler bei Byte %lld in Datei %s, fülle mit Nullen auf Dateinamenlist aus DATEI lesen Entferne führendes »%s« von Zielen harter Verknüpfungen Entferne führendes »%s« von Dateinamen Alle Dateien bedingungslos ersetzen Melden Sie %s-Fehler an: %s
 Berichten Sie Fehler an %s.
 Den Zeitstempel des letzten Dateizugriffs nach dem Lesen auf den vorherigen Wert zurücksetzen Den ursprünglichen Zeitstempel der letzten Änderung beim Erstellen von Dateien beibehalten cpio läuft im Modus »copy-pass« SEK GRÖSSE TEXT Richtung für Positionierung nicht zulässig. Positionierungsangabe außerhalb des zulässigen Bereichs. vor dem Schreiben zur gegebenen Position springen Datum für nächste „--touch“-Operation setzen Setze I/O-Blockgröße auf 5120 Byte Setze I/O-Blockgröße auf BLOCKGRÖSSE * 512 Byte Setze die I/O-Blockgröße auf die in NUMMER angegebene Byte-Anzahl Die Eigentümerkennung aller Dateien auf BENUTZER und/oder GRUPPE setzen Größe eines Blocks für löchrige Dateien Füge ».« statt leerem Ziel harter Verknüpfung ein Füge ».« statt leerem Dateinamen ein Sowohl die Byte-Paare in Datenwörtern wie auch die Bytes in Byte-Paaren vertauschen. Entspricht »-sS« In den Dateien die Bytes in jedem Byte-Paar vertauschen In den Dateien die Byte-Paare in jedem Wort (4 Bytes) vertauschen Aktionen für synchrone Ausführung. Diese werden beim Erreichen des Kontrollpunkts, der mit der Option --checkpoint gesetzt wurde, ausgeführt. Optionen für synchrone Ausführung: Zum Weitermachen Geräte- bzw. Datei-Namen angeben, wenn das Gerät bereit ist.
 Zu viele Argumente DATEI kürzen auf die Länge, die mit einer vorhergehenden Option --length (oder 0 bei Abwesenheit) gesetzt wurde Versuchen Sie »%s --help« oder »%s --usage« für mehr Informationen.
 Unerwartete Argumente Unbekanntes Datumsformat unbekanntes Feld „%s“ Unbekannter Systemfehler DATEI entfernen (unlink) Zugriffs- und Änderungszeiten der DATEI neu setzen Aufruf: Benutze angegebenes Archiv-FORMAT Benutze das BEFEHL statt rsh für den Fernzugriff Benutze das alte portable (ASCII-) Archivformat Nutze diesen DATEINAMEN statt der Standardein- oder -ausgabe. Die optionalen Parameter BENUTZER und RECHNER geben bei einem Archiv auf einem entfernten Rechner den Benutzernamen und den Namen des entfernten Rechners an. Gültige Argumente sind: Verarbeitete Dateien mit Details auflisten Beim Lesen eines Archivs im CRC-Format nur die CRC-Prüfsummen jeder Datei im Archiv verifizieren, keine Dateien extrahieren. Dateien mit großen Blöcken aus Nullen als löchrige Dateien (»sparse files«) schreiben in Datei NAME anstelle der Standardausgabe schreiben Geschrieben von %s und %s.
 Geschrieben von %s, %s, %s,
%s, %s, %s, %s,
%s, %s und anderen.
 Geschrieben von %s, %s, %s,
%s, %s, %s, %s,
%s und %s.
 Geschrieben von %s, %s, %s,
%s, %s, %s, %s
und %s.
 Geschrieben von %s, %s, %s,
%s, %s, %s und %s.
 Geschrieben von %s, %s, %s,
%s, %s und %s.
 Geschrieben von %s, %s, %s,
%s und %s.
 Geschrieben von %s, %s, %s
und %s.
 Geschrieben von %s, %s und %s.
 Geschrieben von %s.
 Sie müssen eine der Optionen -oipt angeben.
Versuchen Sie »%s --help« oder »%s --usage«, wenn Sie weitere Informationen benötigen.
 [ARGUMENTE …] [BENUTZER][:.][GRUPPE] [[BENUTZER@]RECHNER:]DATEINAME [Zielverzeichnis] » `%s' existiert, ist aber kein Verzeichnis Mehrdeutiges Argument »%s« für »%s« leere Zeile ignoriert kann auf der Standardausgabe keine löchrige Datei erstellen, benutzen Sie die Option --file Kann nicht die Login-Gruppe zu einer numerischen UID ermitteln Kann %s nicht mit %s verbinden Kann Verzeichnis `%s' nicht erstellen %s kann nicht geöffnet werden Öffnen von »%s« fehlgeschlagen Kann Prüfsumme für %s nicht lesen Kann `%s' nicht löschen kann nicht positionieren Kann nicht in der Ausgabe suchen kann an „%s“ keine Zeit setzen Kann die Bytes von %s nicht vertauschen: unterschiedliche Anzahl von Bytes Kann halfwords (Byte-Paare) von %s nicht vertauschen: ungerade Anzahl von halfwords (Byte-Paaren) kann „%s“ nicht entfernen (unlink) Vorgänge auf magnetischen Bandlaufwerken steuern erzeugte Datei ist nicht löchrig Hauptgerätenummer Nebengerätenummer Geräenummer Fehler beim Schliessen des Archivs exec/tcp: Service nicht verfügbar Rückkehr zum anfänglichen Arbeitsverzeichnis schlug fehl Dateimodus Dateiname enthält Null-Zeichen Dateigröße genfile dient zum Bearbeiten von Dateien für die GNU-Testsuite paxutils.
Es gibt folgende PARAMETER:
 gid Eine kurze Aufrufnachricht ausgeben Diese Hilfeliste ausgeben SEK Sekunden warten (Vorgabe: 3600) inkorrekte Maske (nahe „%s“) Inode-Nummer interner Fehler: Bandbeschreibung änderte sich von %d auf %d Ungültiges Archiv-Format `%s'; gültige Formate sind:
crc, newc, odc, bin, ustar, tar, hpodc, hpbin Ungültiges Argument »%s« für »%s« Ungültige Blockgröße Ungültiger Zahlenwert Ungültige Benutzergruppe Ungültiger Header: Prüfsummenfehler ungültiger Benutzer Speicher erschöpft Änderungsdatum Namensgröße Kein Bandlaufwerk angegeben Anzahl der Links Operation Vorgang [Zahl] verfrühtes Ende des Archivs verfrühtes Ende der Datei Programmversion ausgeben rdev rdev major rdev minor Fehler beim Lesen Benenne %s um ->  Dateilänge %lu verlangt, tatsächlich %lu Fehlerdiagnose-Stufe setzen Dateinamen für Fehlerdiagnose-Ausschriften setzen Programmname festlegen Standardeingabe ist geschlossen Standardeingabe ist geschlossen stat(%s) fehlgeschlagen Standardeingabe Standardausgabe Zu viele Argumente uid Kann aktuelles Arbeitsverzeichnis nicht festhalten Verwenden Sie Gerät als Dateinamen für das Bandlaufwerk, auf dem der Vorgang ausgeführt werden soll statt „rsh“ den entfernten BEFEHL benutzen Virtueller Speicher überfüllt Warnung: Archivkopf hat umgekehrte Byte-Reihenfolge Warnung: %ld Bytes Müll  übersprungen Warnung: %ld Bytes Müll übersprungen Fehler beim Schreiben PRIuMAX File %s grew, % new byte not copied File %s grew, %<PRIuMAX> new bytes not copied Datei %s ist gewachsen, % neues Byte wurde nicht kopiert Datei %s ist gewachsen, % neue Bytes wurden nicht kopiert 