��    3     �	  �  L      �  9   �  J   �  '   6  (   ^  $   �  .   �  5   �  ?     6   Q  :   �  :   �     �  )        >  '   \  5   �  -   �  9   �  8   "  )   [  &   �  "   �  "   �     �            .   (  "   W     z  .   �  1   �  #   �       I     /   _  &   �     �     �  	   �     �  &   �        ,   3   *   `   -   �      �   $   �   !   �       !     9!  +   S!     !     �!  )   �!  C   �!  >   �!  "   9"     \"  0   d"  1   �"  2   �"  $   �"     #     8#     O#     n#     v#     |#  !   �#  !   �#  !   �#  !   �#  '   $  
   3$  
   >$  
   I$  
   T$  
   _$  
   j$  
   u$  	   �$  =   �$     �$  4   �$     	%     &%     ,%     ;%     I%  !   a%  %   �%  �   �%     +&     8&     S&     Z&     v&     |&  	   �&     �&  	   �&     �&     �&     �&     �&     '     4'     N'     e'     �'  
   �'     �'     �'     �'     �'     �'  !   �'     (     )(     /(     1(     G(  $   K(     p(     r(     �(     �(     �(     �(     �(     �(     �(     �(  %   �(  "   )     @)     N)     W)     n)     �)  +   �)  &   �)  +   �)     *     *  )    *  6   J*  "   �*     �*  	   �*  
   �*     �*  	   �*     �*     +      !+     B+  -   ^+  &   �+     �+     �+     �+     �+  	   ,  
   ,     ,  
   3,     >,     X,  (   d,     �,     �,     �,     �,  ,   �,     �,  O   -  #   V-  $   z-     �-  5   �-  -   �-  6   #.     Z.     q.  &   �.  6   �.     �.  '   /  )   )/  /   S/  1   �/     �/  )   �/  +   �/  1   *0  4   \0  )   �0     �0     �0  !   �0     1  )   "1     L1     e1  	  m1     �:  �   �:     r;     �;     �;     �;     �;     �;  !   �;  %   �;  %   <  !   7<  =   Y<  #   �<  !   �<     �<     �<     �<  +   =     A=  -   O=  2   }=     �=     �=     �=  !   �=     >     0>     E>     S>  D   i>  $   �>  $   �>     �>     ?     ,?     I?     d?  4   v?  %   �?     �?     �?  /   �?     -@     6@     D@     Y@     f@     u@     �@     �@     �@     �@     �@     �@     �@     A     .A     @A  	   PA     ZA     bA     wA     �A  
   �A      �A     �A     �A     �A      B  +   B     EB     dB  #   uB     �B     �B  ,   �B     �B     C     C      =C  ?   ^C  J   �C     �C     �C     D     !D      &D     GD     eD     yD     �D     �D     �D     �D     �D  +   �D  !   #E     EE     ]E  �  zE  6   G  J   >G  8   �G  1   �G  ,   �G  .   !H  :   PH  :   �H  >   �H  9   I  >   ?I     ~I  &   �I     �I  )   �I  :   J  8   =J  C   vJ  8   �J  )   �J  &   K  !   DK  "   fK     �K     �K     �K  .   �K  #   �K     L  5   L  $   SL     xL     �L  L   �L  3   �L  %   $M     JM     fM  
   nM  
   yM  *   �M     �M  1   �M  /   N  2   1N  )   dN  ,   �N  )   �N     �N  !   O  %   $O     JO     OO  3   XO  @   �O  N   �O     P     ;P  Q   DP  G   �P  F   �P  1   %Q      WQ  #   xQ  ,   �Q     �Q     �Q     �Q  !   �Q  !   R  "   $R  !   GR  0   iR     �R     �R     �R     �R     �R     �R     �R     �R  S   �R  
   5S  '   @S     hS     �S     �S     �S     �S  !   �S  $   �S  r   T     �T  )   �T     �T  %   �T     �T     �T     �T     U  
   U     %U     BU  !   \U     ~U     �U     �U     �U  $   �U     V     V     *V     7V     WV     ]V     vV      �V     �V     �V     �V  !   �V     �V  "    W     #W     %W  "   BW     eW     jW     �W     �W     �W     �W     �W  4   �W      �W     X  
   X     )X  )   CX     mX  +   yX  4   �X  3   �X     Y     Y  "   Y  N   AY  $   �Y     �Y     �Y     �Y     �Y  	   �Y  !   �Y  !   !Z  !   CZ      eZ  %   �Z  -   �Z     �Z  .   �Z  .   [     F[     M[     \[      l[  
   �[     �[     �[  4   �[     �[     \     \     %\  ,   ,\  $   Y\  Q   ~\  ,   �\  @   �\     >]  ,   ^]  $   �]  .   �]     �]     �]  +   
^  -   6^  *   d^  N   �^  5   �^  -   _  /   B_  4   r_  .   �_  -   �_  /   `  1   4`  .   f`     �`     �`  )   �`  *   �`  5   a     Sa     sa  �  �a      1m  �   Rm  "   An  $   dn     �n     �n     �n     �n  &   �n  %   �n  %   �n  !   o  H   ?o  /   �o  -   �o     �o     �o     p  5   "p     Xp  .   hp  5   �p  $   �p     �p  +   q  &   1q  %   Xq      ~q     �q  #   �q  G   �q  /   !r  G   Qr  .   �r  %   �r  ,   �r  "   s  %   >s  B   ds  &   �s     �s     �s  %   t     .t     <t     Qt     kt  .   xt     �t     �t     �t     �t     u     #u     8u     >u  )   ]u     �u     �u  
   �u  	   �u     �u      �u     v  
   v  $   v     =v      Lv     mv     �v  -   �v  !   �v     �v  +   w  .   .w     ]w  1   tw  *   �w  	   �w     �w  "   �w  L   x  X   gx     �x     �x     �x     �x     y  %   !y     Gy     Uy     qy     �y     �y     �y  %   �y  3   �y  ?   !z  @   az  %   �z        J      '   �   m   �       �   �         )  7           H      �   �       �   �   �               s       *       2              �   �      �   �     �   	   �   �       �   p   �   S   �   a       �      F                       =       �   b       o   �   &  5   #  �   Z       y   }   �   �       "   B   �         �       ,   �   g   '      �   �   �   %     �               !  1          �   �       �       �   �   ,  w   �             4   �   �   u       �   �   �   �         G   �   �   !       9   f   �             2     �   $  &   .   �   /       �   l   )       �   ?          �       �         �   \   v         >   �       �   n             |              X   %   �   �           �   x       �   �   3  "  �   {           -   �      e   �   W       I   U       �   
  c   �   �           �     �   �   �       �             R   z   D        �   �   �   A              �   �       �   i   �           �   �   �   $   �   �   /          *  �   �   C   V   �   �              �          �       �   �       �   �   �   �   �       �   (     K   L   M   N      P   Q   3   �   +                              �     �   �      (      �             <   j          �   
   :   T   [   8   �   �       �          1     ;       .                    �        q   r       @   �      �       �   ^      �   O   �     0  �           �   +      �   d       k   �              �   h          #           �     6           Y     �   	  �   0   _   �   E           ~       �   ]       �   t   �   `         �   �   -  �    
Some of these may not be available on selected hardware
 === PAUSE ===                                                             PAUSE command ignored (no hw support)
          please, try the plug plugin %s
      -d,--disconnect     disconnect
      -e,--exclusive      exclusive connection
      -i,--input          list input (readable) ports
      -l,--list           list current connections of each port
      -o,--output         list output (writable) ports
      -r,--real #         convert real-time-stamp on queue
      -t,--tick #         convert tick-time-stamp on queue
      -x, --removeall
      sender, receiver = client:port pair
    aconnect -i|-o [-options]
    aconnect [-options] sender receiver
   -d,--dest addr : write to given addr (client:port)
   -i, --info : print certain received events
   -p,--port # : specify TCP port (digit or service name)
   -s,--source addr : read from given addr (client:port)
   -v, --verbose : print verbose messages
   Clemens Ladisch <clemens@ladisch.de>   Copyright (C) 1999 Takashi Iwai
   Jaroslav Kysela <perex@perex.cz>   Subdevice #%i: %s
   Subdevices: %i/%i
   Tim Janik   client mode: aseqnet [-options] server_host
   server mode: aseqnet [-options]
  !clip    * Connection/disconnection between two ports
  * List connected ports (no subscription action)
  * Remove all exported connections
  [%s %s, %s]  can't play WAVE-files with sample %d bits in %d bytes wide (%d channels)  can't play WAVE-files with sample %d bits wide %s is not a mono stream (%d channels)
 %s!!! (at least %.3f ms long)
 %s: %s
 (default) (unplugged) **** List of %s Hardware Devices ****
 + -        Change volume -f cd (16 bit little endian, 44100, stereo)
 -f cdr (16 bit big endian, 44100, stereo)
 -f dat (16 bit little endian, 48000, stereo)
 0-9        Set volume to 0%-90% ; '        Toggle left/right capture < >        Toggle left/right mute Aborted by signal %s...
 Access type not available Access type not available for playback: %s
 All Authors: B          Balance left and right volumes Broken configuration for playback: no configurations available: %s
 Broken configuration for this PCM: no configurations available Buffer size range from %lu to %lu
 CAPTURE Can't recovery from suspend, prepare failed: %s
 Can't recovery from underrun, prepare failed: %s
 Can't use period equal to buffer size (%lu == %lu) Cannot create process ID file %s: %s Cannot open WAV file %s
 Cannot open file "%s". Cannot open mixer device '%s'. Capture Card: Center Channel %2d: Control event : %5d
 Channel %2d: Note Off event: %5d
 Channel %2d: Note On event : %5d
 Channel %2d: Pitchbender   : %5d
 Channel %d doesn't match with hw_parmas Channel 10 Channel 11 Channel 12 Channel 13 Channel 14 Channel 15 Channel 16 Channel 9 Channel numbers don't match between hw_params and channel map Channels %i Channels count (%i) not available for playbacks: %s
 Channels count non available Chip: Connected From Connecting To Connection failed (%s)
 Connection is already subscribed
 Copyright (C) 1999-2000 Takashi Iwai
 Debugging options:
  -g, --no-color          toggle using of colors
  -a, --abstraction=NAME  mixer abstraction level: none/basic Device name: Disconnection failed (%s)
 Done.
 End        Set volume to 0% Error Esc     Exit Esc: Exit F1 ? H  Help F1:  Help F2 /    System information F2:  System information F3      Show playback controls F4      Show capture controls F5      Show all controls F6 S    Select sound card F6:  Select sound card Failed. Restarting stream.  Front Front Left Front Right HW Params of device "%s":
 Help Invalid WAV file %s
 Invalid number of periods %d
 Invalid parameter for -s option.
 Invalid test type %s
 Item: L L       Redraw screen LFE Left    Move to the previous control M M          Toggle mute Max peak (%li samples): 0x%08x  Mono No enough memory
 No subscription is found
 Not a WAV file: %s
 O Off On Page Up/Dn Change volume in big steps Period size range from %lu to %lu
 Periods = %u
 Playback Playback device is %s
 Playback open error: %d,%s
 Playing Playing Creative Labs Channel file '%s'...
 Press F6 to select another sound card. Q W E      Increase left/both/right volumes R Rate %d Hz,  Rate %iHz not available for playback: %s
 Rate doesn't match (requested %iHz, get %iHz, err %d)
 Rate set to %iHz (requested %iHz)
 Rear Rear Left Rear Right Recognized sample formats are: Recording Requested buffer time %u us
 Requested period time %u us
 Right   Move to the next control Sample format non available Sample format not available for playback: %s
 Sample rate doesn't match (%d) for %s
 Select File Setting of hwparams failed: %s
 Setting of swparams failed: %s
 Side Side Left Side Right Sine wave rate is %.4fHz
 Sound Card Space      Toggle capture Sparc Audio Sparc Audio doesn't support %s format... Status(DRAINING):
 Status(R/W):
 Status:
 Stereo Stream parameters are %iHz, %s, %i channels
 Suspended. Trying resume.  Suspicious buffer position (%li total): avail = %li, delay = %li, buffer = %li
 Tab     Toggle view mode (F3/F4/F5) The available format shortcuts are:
 The sound device was unplugged. This sound device does not have any capture controls. This sound device does not have any controls. This sound device does not have any playback controls. Time per period = %lf
 Transfer failed: %s
 Try `%s --help' for more information.
 Unable to determine current swparams for playback: %s
 Unable to install hw params: Unable to parse channel map string: %s
 Unable to set avail min for playback: %s
 Unable to set buffer size %lu for playback: %s
 Unable to set buffer time %u us for playback: %s
 Unable to set channel map: %s
 Unable to set hw params for playback: %s
 Unable to set nperiods %u for playback: %s
 Unable to set period time %u us for playback: %s
 Unable to set start threshold mode for playback: %s
 Unable to set sw params for playback: %s
 Undefined channel %d
 Unknown option '%c'
 Unsupported WAV format %d for %s
 Unsupported bit size %d.
 Unsupported sample format bits %d for %s
 Up/Down    Change volume Usage:
 Usage: %s [OPTION]... [FILE]...

-h, --help              help
    --version           print current version
-l, --list-devices      list all soundcards and digital audio devices
-L, --list-pcms         list device names
-D, --device=NAME       select PCM by name
-q, --quiet             quiet mode
-t, --file-type TYPE    file type (voc, wav, raw or au)
-c, --channels=#        channels
-f, --format=FORMAT     sample format (case insensitive)
-r, --rate=#            sample rate
-d, --duration=#        interrupt after # seconds
-M, --mmap              mmap stream
-N, --nonblock          nonblocking mode
-F, --period-time=#     distance between interrupts is # microseconds
-B, --buffer-time=#     buffer duration is # microseconds
    --period-size=#     distance between interrupts is # frames
    --buffer-size=#     buffer duration is # frames
-A, --avail-min=#       min available space for wakeup is # microseconds
-R, --start-delay=#     delay for automatic PCM start is # microseconds 
                        (relative to buffer size if <= 0)
-T, --stop-delay=#      delay for automatic PCM stop is # microseconds from xrun
-v, --verbose           show PCM structure and setup (accumulative)
-V, --vumeter=TYPE      enable VU meter (TYPE: mono or stereo)
-I, --separate-channels one file for each channel
-i, --interactive       allow interactive operation from stdin
-m, --chmap=ch1,ch2,..  Give the channel map to override or follow
    --disable-resample  disable automatic rate resample
    --disable-channels  disable automatic channel conversions
    --disable-format    disable automatic format conversions
    --disable-softvol   disable software volume control (softvol)
    --test-position     test ring buffer position
    --test-coef=#       test coefficient for ring buffer position (default 8)
                        expression for validation is: coef * (buffer_size / 2)
    --test-nowait       do not wait for ring buffer - eats whole CPU
    --max-file-time=#   start another output file when the old file has recorded
                        for this many seconds
    --process-id-file   write the process ID here
    --use-strftime      apply the strftime facility to the output file name
    --dump-hw-params    dump hw_params of the device
    --fatal-errors      treat all errors as fatal
 Usage: alsamixer [options] Useful options:
  -h, --help              this help
  -c, --card=NUMBER       sound card number or id
  -D, --device=NAME       mixer device name
  -V, --view=MODE         starting view mode: playback/capture/all Using 16 octaves of pink noise
 Using max buffer size %lu
 VOC View: WAV file(s)
 WAVE Warning: format is changed to %s
 Warning: format is changed to MU_LAW
 Warning: format is changed to S16_BE
 Warning: format is changed to U8
 Warning: rate is not accurate (requested = %iHz, got = %iHz)
 Warning: unable to get channel map
 Wave doesn't support %s format... Woofer Write error: %d,%s
 You need to specify %d files Z X C      Decrease left/both/right volumes accepted[%d]
 aconnect - ALSA sequencer connection manager
 aseqnet - network client/server on ALSA sequencer
 audio open error: %s bad speed value %i buffer to small, could not use
 can't allocate buffer for silence can't get address %s
 can't get client id
 can't malloc
 can't open sequencer
 can't play WAVE-file format 0x%04x which is not PCM or FLOAT encoded can't play WAVE-files with %d tracks can't play loops; %s isn't seekable
 can't play packed .voc files can't set client info
 cannot enumerate sound cards cannot load mixer controls cannot open mixer capture stream format change? attempting recover...
 card %i: %s [%s], device %i: %s [%s]
 client %d: '%s' [type=%s]
 closing files..
 command should be named either arecord or aplay dB gain: disconnected
 enter device name... fatal %s: %s info error: %s invalid card index: %s
 invalid destination address %s
 invalid sender address %s
 invalid source address %s
 kernel malloc error mute no soundcards found... nonblock setting error: %s not enough memory ok.. connected
 options:
 overrun pause push error: %s pause release error: %s raw data read error read error (called from line %i) read error: %s read/write error, state = %s readv error: %s sequencer opened: %d:%d
 service '%s' is not found in /etc/services
 snd_pcm_mmap_begin problem: %s status error: %s stdin O_NONBLOCK flag setup failed
 suspend: prepare error: %s too many connections!
 try `alsamixer --help' for more information
 unable to install sw params: underrun unknown abstraction level: %s
 unknown blocktype %d. terminate. unknown length of 'fmt ' chunk (read %u, should be %u at least) unknown length of extensible 'fmt ' chunk (read %u, should be %u at least) unknown option: %c
 unrecognized file format %s usage:
 user value %i for channels is invalid voc_pcm_flush - silence error voc_pcm_flush error was set buffer_size = %lu
 was set period_size = %lu
 write error write error: %s writev error: %s wrong extended format '%s' wrong format tag in extensible 'fmt ' chunk xrun(DRAINING): prepare error: %s xrun: prepare error: %s xrun_recovery failed: %d,%s
 Project-Id-Version: alsa-utils 1.0.23
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-10-27 17:34+0100
PO-Revision-Date: 2013-08-17 20:31+0000
Last-Translator: Torino <torinobuntu@gmx.de>
Language-Team: German
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:56+0000
X-Generator: Launchpad (build 18115)
Language: de
 
Nicht alle davon sind auf jeder Hardware verfügbar.
 === PAUSE ===                                                             PAUSE-Befehl ignoriert (keine Hardware-Unterstützung)
          probieren Sie bitte das plug-Plugin: %s
      -d,--disconnect     Verbindung trennen
      -e,--exclusive      exklusive Verbindung
      -i,--input          Eingabe-Ports (lesbar) auflisten
      -l,--list           Verbindungen der Ports auflisten
      -o,--output         Ausgabe-Ports (schreibbar) auflisten
      -r,--real #         benutze Zeitstempel der Queue #
      -t,--tick #         benutze Tick-Zeitstempel der Queue #
      -x,--removeall
      Sender, Empfänger = Client:Port
    aconnect -i|-o [Optionen]
    aconnect [Optionen] Sender Empfänger
   -d,--dest # : schreibe auf Sequenzer-Port (Client:Port)
   -i,--info : Ausgabe bestimmter empfangener Ereignisse
   -p, --port# : Legt TCP-Port fest (Nummer oder Name des Dienstes)
   -s,--source # : lese von Sequenzer-Port (Client:Port)
   -v,--verbose : ausführliche Meldungen
   Clemens Ladisch <clemens@ladisch.de>   Copyright © 1999 Takashi Iwai
   Jaroslav Kysela <perex@perex.cz>   Sub-Gerät #%i: %s
   Sub-Geräte: %i/%i
   Tim Janik   Client-Modus: aseqnet [Optionen] ServerHost
   Server-Modus: aseqnet [Optionen]
  !clip    * Verbindung zwischen zwei Ports herstellen/trennen
  * Ports und Verbindungen auflisten
  * alle Verbindungen trennen
  [%s %s; %s]  kann WAVE-Datei mit %d-Bit-Samples in %d Bytes (%d Kanäle) nicht abspielen  kann WAVE-Datei mit %d-Bit-Samples nicht abspielen %s ist keine Mono-Datei (%d Kanäle)
 %s!!! (mindestens %.3f ms)
 %s: %s
 (Standard) (entfernt) **** Liste der Hardware-Geräte (%s) ****
 + -         Lautstärke ändern -f cd (16 Bits, Little Endian, 44100 Hz, stereo)
 -f cdr (16 Bits, Big Endian, 44100 Hz, stereo)
 -f dat (16 Bits, Little Endian, 48000 Hz, stereo)
 0-9         Lautstärke auf 0%-90% setzen Einfg Entf  Aufnahme links/rechts umschalten , .         stumm links/rechts umschalten Abbruch durch Signal %s ...
 Zugriffs-Modus nicht unterstützt Zugriffsmodus nicht unterstützt: %s
 Alle Autoren: B           linke und rechte Lautstärke angleichen Ungültige Konfiguration: keine unterstützte Konfiguration: %s
 ungültige Konfiguration für dieses Gerät: keine unterstützte Konfiguration Puffergröße von %lu bis %lu
 AUFNAHME Fehler beim Aufwachen aus dem Ruhezustand, Re-Initialisierung fehlgeschlagen: %s
 Fehler bei Unterlauf-Behandlung, Re-Initialisierung fehlgeschlagen: %s
 Periode gleich der Puffer-Größe wird nicht unterstützt (%lu == %lu) Fehler beim Schreiben der Prozess-ID-Datei %s: %s Kann WAV-Datei %s nicht öffnen
 Fehler beim Öffnen der Datei "%s". Fehler beim Öffnen des Mixer-Gerätes '%s'. Aufnahme Gerät: Mitte Channel %2d: Control event : %5d
 Channel %2d: Note Off event: %5d
 Channel %2d: Note On evenet : %5d
 Channel %2d: Pitchbender   : %5d
 Der Kanal %d stimmt nicht mit hw_params überein Kanal 10 Kanal 11 Kanal 12 Kanal 13 Kanal 14 Kanal 15 Kanal 16 Kanal 9 Die Kanalnummern zwischen hw_params und der Kanalaufstellung stimmen nicht überein %i Kanäle Kanal-Anzahl %i nicht unterstützt: %s
 Kanalanzahl nicht unterstützt Chip: verbunden von verbunden zu Verbindung fehlgeschlagen (%s)
 Verbindung ist bereits vorhanden
 Copyright © 1999-2000 Takashi Iwai
 Debugging-Optionen:
  -g, --no-color          keine Farben
  -a, --abstraction=NAME  Mixer-Abstraktion: none/basic Gerätename: Verbindungs-Trennung fehlgeschlagen (%s)
 Fertig.
 Ende        Lautstärke auf 0% setzen Fehler Esc     Beenden Esc: Beenden F1 ? H  Hilfe F1:  Hilfe F2 /    System-Informationen F2:  System-Informationen F3      Ansicht Wiedergabe-Regler F4      Ansicht Aufnahme-Regler F5      Ansicht alle Regler F6 S    Klangkarte auswählen F6:  Klangkarte auswählen Fehlgeschlagen. Re-Initialisierung.  Vorne Vorne links Vorne rechts »HW Params« von Gerät "%s":
 Hilfe Ungültige WAV-Datei %s
 Ungültige Periodenanzahl %d
 Ungültiger Wert für Option -s
 Ungültiger Test-Typ %s
 Element: L L       Bildschirm neu darstellen Bass Links   gehe zum vorherigen Regler M M           stumm umschalten Höchstwert (%li Samples): 0x%08x  mono Nicht genug Speicher
 keine Verbindung gefunden
 Keine WAV-Datei: %s
 O Aus An Bild ^/v    Lautstärke in großen Schritten ändern Periodengröße von %lu bis %lu
 Perioden = %u
 Wiedergabe Wiedergabe-Gerät ist %s
 Fehler beim Öffnen des Gerätes: %d, %s
 Wiedergabe: Spiele Creative Labs Channel-Datei '%s'...
 F6 drücken, um eine andere Klangkarte auszuwählen. Q W E       linke/beide/rechte Lautstärke erhöhen R Rate: %d Hz,  Rate %i Hz nicht unterstützt: %s
 Rate ist nicht exakt (angefordert: %i Hz, unterstützt: %i Hz, Fehlercode %d)
 Rate ist %i Hz (angefordert: %i Hz)
 Hinten Hinten links Hinten rechts Unterstützte Sample-Formate: Aufnahme: Angeforderte Pufferlänge %u µs
 Angeforderte Periodenzeit %u µs
 Rechts  gehe zum nächsten Regler Sample-Format nicht unterstützt Sample-Format nicht unterstützt: %s
 Sample-Rate (%d) stimmt nicht überein in %s
 Datei wählen Fehler beim Setzen der Hardware-Parameter: %s
 Fehler beim Setzen der Software-Parameter: %s
 Seiten Seitlich links Seitlich rechts Sinuswelle mit Frequenz %.4f Hz
 Klangkarte Leertaste   Aufnahme umschalten Sparc-Audio Format %s wird in Sparc-Audio nicht unterstützt ... Status (DRAINING):
 Status (R/W):
 Status:
 stereo Stream-Parameter sind %i Hz, %s, %i Kanäle
 Ruhezustand. Versuche, aufzuwecken.  verdächtige Puffer-Position (total %li): avail = %li, delay = %li, buffer = %li
 Tab     Ansichts-Modus umschalten (F3/F4/F5) Die zur Verfügung stehenden Format-Tastenkombinationen lauten:
 Das Klanggerät wurde entfernt. Dieses Klanggerät hat keine Aufnahmeregler. Dieses Klanggerät hat keine Regler. Dieses Klanggerät hat keine Wiedergaberegler. Zeit pro Periode = %lf
 Schreibfehler: %s
 Siehe `%s --help' für mehr Informationen.
 Fehler beim Lesen der Software-Parameter: %s
 Fehler beim Setzen der Hardware-Parameter: Die Zeichenkette für die Kanalaufstellung konnte nicht verstanden werden: %s
 Fehler beim Setzen des Mindest-verfügbar-Wertes: %s
 Fehler beim Setzen der Puffergröße %lu: %s
 Fehler beim Setzen der Pufferlänge %u µs: %s
 Kanalaufstellung konnte nicht festgelegt werden: %s
 Fehler beim Setzen der Hardware-Parameter: %s
 Fehler beim Setzen der Periodenanzahl %u: %s
 Fehler beim Setzen der Periodenzeit %u µs: %s
 Fehler beim Setzen des Start-Schwellenwertes: %s
 Fehler beim Setzen der Software-Parameter: %s
 Kanal %d nicht definiert
 Unbekannte Options '%c'
 Nicht unterstütztes WAV-Format %d in %s
 %d-Bit-Samples werden nicht unterstützt.
 Nicht unterstütztes Sample-Format mit %d Bits in %s
 Oben/Unten  Lautstärke ändern Verwendung:
 Aufruf: %s [OPTION]... [Datei]...

-h, --help Hilfe
                  --version Zeigt aktuelle Version an
-l, --list-devices Listet alle Soundkarten und digitalen Audiogeräte auf
-L, --list-pcms Listet Gerätenamen auf
-D, --device=NAME Wählt PCM nach Name aus
-q, --quiet Stiller Modus
-t, --file-type TYPE Dateityp (voc, wav, raw oder au)
-c, --channels=# Kanäle
-f, --format=FORMAT Abtastformat (Groß-/ Kleinschreibung beachten!)
-r, --rate=# Abtastrate
-d, --duration=# Unterbricht nach # Sekunden
-M, --mmap »mmap«-Stream
-N, --nonblock Nicht-Blockieren-Modus
-F, --period-time=# Dauer zwischen Unterbrechungen beträgt # Mikrosekunden
-B, --buffer-time=# Pufferzeit beträgt # Mikrosekunden
                  --period-size=# Abstand zwischen Unterbrechungen beträgt # Frames
                  --buffer-size=# Pufferzeit beträgt # Frames
-A, --avail-min=# Mindestens verfügbarer Platz für Wakeups beträgt # Mikrosekunden
-R, --start-delay=# Verzögerung für automatischen PCM-Start beträgt # Mikrosekunden 
                                                                                                                                             (relativ zur Puffergröße wenn <= 0)
-T, --stop-delay=# Verzögerung für automatischen PCM-Stopp beträgt # Mikrosekunden ab »xrun»
-v, --verbose Zeigt PCM-Struktur und -Einrichtung an (zunehmend)
-V, --vumeter=TYPE Aktiviert das VU-Meter (TYPE: mono oder stereo)
-I, --separate-channels Eine Datei pro Kanal
-i, --interactive Erlaube interaktiven Betrieb von »stdin«
-m, --chmap=ch1,ch2,.. Lässt Kanalaufstellung überbrücken oder folgen
                  --disable-resample Deaktiviert erneute automatische Abtastung 
                  --disable-channels Deaktiviert automatische Kanal-Umwandlung
                  --disable-format Deaktiviert automatische Format-Umwandlung
                  --disable-softvol Deaktiviert softwareseitige Lautstärkeregelung (»softvol«)
                  --test-position Testet Position des Ringpuffers
                  --test-coef=# Testet Koeffizienten des Ringpuffers (Voreinstellung: 8)
                                                                                                                                 Ausdruck für Überprüfung lautet: coef * (buffer_size / 2)
                  --test-nowait Wartet nicht auf Ringpuffer – belastet gesamte CPU
                  --max-file-time=# Beginnt weitere Dateiausgabe, sobald die vorherige abgeschlossen ist
                                                                                                                                 für angegebene Dauer in Sekunden
                  --process-id-file Gibt Prozesskennung hier aus
                  --use-strftime Hängt die »strftime« (Zeitstempel) an den Namen der ausgegebenen Datei an
                  --dump-hw-params Verwirft »hw_params« (Hardwareparameter) des Geräts
                  --fatal-errors Alle Fehler werden als schwerwiegend behandelt
 Verwendung: alsamixer [Optionen] Optionen:
  -h, --help              Hilfe
  -c, --card=NUMMER       Klangkartennummer oder -Kennung
  -D, --device=NAME       Mixergerätename
  -V, --view=MODUS        Ansicht beim Starten: playback=Wiedergabe, capture=Aufnahme, all=alle Verwende 16 Oktaven rosa Rauschen
 Verwende maximale Puffergröße %lu
 VOC Ansicht: WAV-Datei(en)
 WAVE Achtung: Format wurde geändert zu %s
 Warnung: benutztes Format ist MU_LAW
 Warnung: benutztes Format ist S16_BE
 Warnung: benutztes Format ist U8
 Warnung: Rate ist nicht exakt (angefordert: %i Hz, unterstützt: %i Hz)
 Achtung: Kanalaufstellung wurde nicht gefunden
 Format %s wird in WAVE nicht unterstützt ... Bass Schreibfehler: %d, %s
 Es werden %d Dateien benötigt. Y X C       linke/beide/rechte Lautstärke verringern angenommen[%d]
 aconnect - ALSA Sequenzer Verbindungs-Manager
 aseqnet - Netzwerk-Client/Server für ALSA Sequenzer
 Fehler beim Öffnen des Gerätes: %s ungültige Rate %i Puffer zu klein, kann nicht benutzt werden
 nicht genug Speicher für Stille-Block kann Adresse für %s nicht bestimmen
 Fehler beim Lesen der Client-ID
 nicht genug Speicher
 Fehler beim Öffnen des Sequenzers
 kann WAVE-Datei-Format 0x%04x nicht abspielen; ist weder PCM noch FLOAT kann WAVE-Datei mit %d Kanälen nicht abspielen kann Schleife nicht abspielen; Dateiposition in %s ist nicht änderbar
 kann komprimierte .voc-Dateien nicht abspielen Fehler beim Setzen des Client-Namens
 Klangkarten können nicht aufgezählt werden Fehler beim Laden der Mixer-Regler Fehler beim Öffen des Mixer-Gerätes Format-Wechsel der Aufnahme-Daten? Versuche Wiederherstellung ...
 Karte %i: %s [%s], Gerät %i: %s [%s]
 Client %d: '%s' [Typ=%s]
 Dateien werden geschlossen …
 Befehl sollte arecord oder aplay sein dB-Änderung: Verbindung getrennt
 Gerätenamen eingeben … Fatal %s: %s Fehler beim Lesen der Geräteinformationen: %s ungültige Karten-Nummer: %s
 ungültige Ziel-Adresse %s
 ungültige Sender-Adresse %s
 ungültige Quell-Adresse %s
 Kernel nicht genug Speicher stumm keine Klangkarten gefunden … Fehler beim Setzen des nonblock-Modus: %s nicht genug Speicher OK ... verbunden
 Optionen:
 Überlauf Wiedergabe-Pausieren-Fehler: %s Wiedergabe-Fortsetzen-Fehler: %s Rohdaten Lesefehler Lesefehler (aufgerufen von Zeile %i) Lesefehler: %s Lese-/Schreibfehler, Status = %s Vektor-Lese-Fehler: %s Sequenzer geöffnet: %d:%d
 Service '%s' in /etc/services nicht gefunden
 Fehler bei snd_pcm_mmap_begin: %s Status-Fehler: %s Fehler beim Setzen von O_NONBLOCK in stdin
 Ruhezustand: Fehler beim Re-Initialisieren: %s zu viele Verbindungen
 siehe `alsamixer --help' für mehr Informationen
 Fehler beim Setzen der Software-Parameter: Unterlauf unbekannte Abstraktion: %s
 Unbekannter Block-Typ %d. Abbruch. unbekannte Länge des 'fmt '-Blocks (gelesen: %u, sollte mindestens %u sein) unbekannte Länge des erweiterten 'fmt '-Blocks (gelesen: %u, sollte mindestens %u sein) unbekannte Option: %c
 unbekanntes Dateiformat %s Verwendung:
 User Kanalanzahl %i ist ungültig voc_pcm_flush - Fehler in set_silence Schreibfehler gesetzt: buffer_size = %lu
 gesetzt: period_size = %lu
 Schreibfehler Schreibfehler: %s Vektor-Schreib-Fehler: %s erweitertes Format '%s' ist ungültig ungültiger Format-Wert im erweiterten 'fmt '-Block XRUN (DRAINING): Fehler beim Re-Initialisieren des Gerätes: %s Unter-/Überlauf: Fehler beim Re-Initialisieren des Gerätes: %s xrun_recovery fehlgeschlagen: %d, %s
 