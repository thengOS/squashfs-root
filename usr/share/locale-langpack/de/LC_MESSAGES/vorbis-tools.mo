��    �     \  e  �      0&  (   1&     Z&     x&      �&     �&  $   �&     �&     �&     '     )'     ;'  F   O'  F   �'  B   �'  ;    (  7   \(  1   �(  >   �(  @   )  *   F)  ?   q)  �   �)  F   m*  L   �*  �   +  ,   �+  J   �+  &  �+  N   #-  �   r-  F   n.  <   �.  7   �.  i   */  H   �/  F   �/  1   $0  >   V0  ?   �0  <   �0    1  z   .2  -   �2  �   �2  1   n3  +   �3  c   �3  ,   04  �   ]4  J  )5     t6     �6  4   �6     �6     �6  ,   
7     77  %   U7  ,   {7  -   �7      �7  &   �7     8     >8     ^8     d8     m8  $   �8     �8     �8     �8     �8     �8     9  ,   
9  !   79  Z   Y9  7   �9  *   �9  -   :  S   E:  <   �:  S   �:  +   *;  Q   V;  !   �;     �;  4   �;  *   <  ,   B<     o<     �<     �<     �<     �<     �<  $   �<  W   	=     a=     t=  	   �=  A   �=  9   �=     >     0>     A>     T>  0   b>     �>     �>     �>  1   �>  7   ?     <?     E?  	   R?  &   \?     �?  #   �?  %   �?     �?  .   @  #   2@  /   V@  @   �@     �@     �@     A     "A  %   @A     fA     zA     �A     �A     �A     �A     �A  !   �A     �A  "   B  ?   9B  ?   yB  5   �B  G   �B     7C  (   VC  '   C  (   �C  :   �C  ?   D  ;   KD  I   �D  !   �D     �D     E     (E  2   FE  &   yE  %   �E  *   �E  &   �E  '   F     @F  1   `F  :   �F  1   �F  M   �F  ?   MG  1   �G  2   �G  '   �G  2   H  >   MH  )   �H  0   �H  2   �H  -   I     HI  4   _I  &   �I  .   �I  \   �I  X   GJ     �J     �J  #   �J     �J  [   K  @   ]K  6   �K  R   �K  >   (L  1   gL     �L  B   �L      �L  !   M  "   =M     `M      �M  *   �M  $   �M  +   �M     N     9N     RN  L   aN     �N  &   �N  >   �N  4   /O  ,   dO  v   �O     P     P       P     AP  3   ^P  2   �P  +   �P     �P  "   Q  2   4Q  .   gQ  ,   �Q  %   �Q  6   �Q  /    R  '   PR  4   xR     �R     �R     �R     �R     �R  ,   �R  +   !S     MS     jS     �S  �   �S  4   VT  6   �T     �T     �T     �T      U  ,   U  -   GU  '   uU  &   �U  8   �U     �U     V     4V  :   OV  %   �V     �V     �V  +   �V     �V     	W  >   W     NW     gW     ~W  (   �W     �W  ;   �W  ;   X  :   NX  )   �X  8  �X     �Y     �Y     Z  0   !Z     RZ     YZ  *   jZ  +   �Z     �Z     �Z  #   �Z  ]   [  �   v[     \  ,   +\  2   X\  %   �\     �\  +   �\  5   �\     !]     =]     L]     \]  $   t]      �]     �]     �]     �]     �]     ^     ^     *^     =^  #   W^  -   {^     �^     �^     �^     �^     �^     _  ;   _  %   [_     �_  '   �_  %   �_  +   �_  '   `  >   8`     w`     �`     �`     �`     �`     �`     �`  (   �`  ^   a  q   ta  <   �a  '   #b  L   Kb     �b  ;   �b     �b     �b     �b     c     c  "   7c     Zc     qc  �   �c  L   d  =   ^d  )   �d  �   �d  G   �e  !   �e     �e     �e     f     f     ,f     ?f  <   Yf  '   �f  9   �f  H   �f  7   Ag  2   yg  L   �g  0   �g  2   *h  P   ]h  O   �h  &   �h  ,   %i  0   Ri  1   �i  "   �i     �i  ?   �i  >   2j  ?   qj  U   �j  O   k  I   Wk  0   �k  W   �k  5   *l  6   `l  E   �l  .   �l  8   m  ;   Em  )   �m  .   �m  &   �m  *   n  F   ,n  #   sn     �n  '   �n  C   �n  -   o  -   Mo  &   {o  -   �o  ?   �o  <   p  0   Mp  (   ~p     �p  7   �p  "   �p  3   q  -   Fq  &   tq  V   �q  *   �q  ,   r  W   Jr  /   �r  &   �r  /   �r  .   )s  ,   Xs  1   �s  o   �s  
   't     2t     Et     Jt     Ot     et     lt     rt     vt     �t     �t     �t     �t  F   �t     u     u     1u     Du     Xu     ^u     vu     �u     �u     �u     �u     �u     �u     v  M   
v      Xv  Q  yv  �  �w  /   ly  )   �y      �y  (   �y     z  (   %z     Nz     Pz  +   oz     �z     �z  N   �z  V   {  I   m{  B   �{  D   �{  2   ?|  E   r|  N   �|  /   }  G   7}  �   }  P   q~  b   �~  �   %  /   �  Z   �  8  G�  ^   ��    ߁  I   �  8   <�  @   u�  p   ��  O   '�  F   w�  <   ��  H   ��  G   D�  E   ��  �  ҅  �   ��  -   �  �   :�  6   ψ  .   �  t   5�  +   ��  �   ։  s  ��     �     2�  5   I�  '   �     ��  .   Č  *   �  -   �  .   L�  /   {�  -   ��  .   ٍ     �     (�     H�     N�     [�  <   r�     ��      ��     ׎     �     	�     %�  C   .�  +   r�  �   ��  D    �  =   e�  E   ��  b   �  <   L�  f   ��  -   �  S   �  $   r�     ��  0   ��  .   �  7   �     O�  "   d�     ��     ��     ��     ��  6   ֓  l   �     z�  !   ��     ��  L   ʔ  D   �  '   \�      ��      ��     ƕ  6   Օ     �     �     /�  .   I�  4   x�  
   ��     ��     ǖ  6   Ԗ     �  /   (�  1   X�  +   ��  N   ��  /   �  *   5�  P   `�  ,   ��  )   ޘ  -   �  2   6�  6   i�  *   ��     ˙      �     �     �     &�      .�  %   O�  #   u�  $   ��  C   ��  `   �  J   c�  b   ��  3   �  <   E�  <   ��  9   ��  I   ��  D   C�  @   ��  V   ɝ  0    �     Q�     n�  %   ��  E   ��  7   ��  5   2�  <   h�  5   ��  3   ۟  (   �  A   8�  @   z�  E   ��  M   �  X   O�  B   ��  C   �  <   /�  <   l�  J   ��  W   ��  <   L�  >   ��  7   ȣ      �  ;   �  2   X�  7   ��  p   ä  d   4�  -   ��     ǥ  -   ݥ     �  a   �  Q   }�  :   Ϧ  j   
�  =   u�  ;   ��      �  Q   �  /   b�  /   ��  0   ¨  -   �  -   !�  6   O�  .   ��  F   ��  .   ��  ,   +�     X�  b   l�  #   Ϫ  2   �  C   &�  9   j�  +   ��  ~   Ы     O�     _�  Y   f�  *   ��  V   �  S   B�  P   ��  (   �  0   �  <   A�  Q   ~�  :   Ю  :   �  @   F�  <   ��  <   į  B   �     D�  	   K�     U�     m�     ��  1   ��  2   ΰ     �     �  
   :�  �   E�  6   1�  R   h�     ��     ۲     ��      
�  8   +�  =   d�  6   ��  &   ٳ  D    �  '   E�     m�  "   ��  D   ��  )   �     �     3�  @   @�     ��     ��  S   ��      �      �     -�  )   F�     p�  I   ��  J   Ͷ  7   �  1   P�  �  ��     �  *   �     J�  F   f�     ��     ��  2   ׹  9   
�  "   D�     g�  ,   ��  z   ��  �   ,�     Ի  3   �  9    �  +   Z�     ��  *   ��  @   ��      �     �     4�     E�  2   e�  %   ��     ��     ҽ     �     ��  
   �     �     7�  "   T�  0   w�  >   ��     �     ��     �     �     *�     E�  B   a�  -   ��     ҿ  6   �  3   '�  :   [�  3   ��  O   ��     �     ,�     4�     ;�     T�     a�     x�  ,   ��  �   ��  �   U�  M   ��  0   3�  Y   d�     ��  A   ��     	�     �      �     ;�  %   N�  #   t�     ��     ��  �   ��  _   b�  B   ��  .   �  �   4�  Q   #�  %   u�     ��     ��     ��     ��     ��     ��  J   �  0   Q�  W   ��  a   ��  >   <�  ;   {�  D   ��  :   ��  F   7�  `   ~�  `   ��  0   @�  @   q�  H   ��  A   ��  9   =�      w�  D   ��  A   ��  >   �  a   ^�  \   ��  Q   �  9   o�  ~   ��  ?   (�  J   h�  K   ��  6   ��  A   6�  M   x�  /   ��  7   ��  *   .�  7   Y�  T   ��  2   ��     �  ,   2�  K   _�  7   ��  7   ��  )   �  7   E�  K   }�  C   ��  8   �  0   F�     w�  J   ��  &   ��  G   ��  0   @�  .   q�  t   ��  /   �  6   E�  `   |�  9   ��  /   �  6   G�  5   ~�  6   ��  2   ��  �   �     ��     ��     ��     ��     ��     ��     ��     �  "   �     9�     Q�     X�     _�  F   p�     ��     ��     ��     ��     ��  !   �  "   (�     K�     c�     s�     ��  "   ��  "   ��     ��  M   ��     *�  �  J�         3   '       �       :   �  �      �           �               L  D  }  X  0   �   K   �       �          �  %           �      �  �  M  `           �      �  �   k  {       A       �   A      �   �  >  w       E   �   g   �    a       q   �  B      s  m     �   �   �   z      i       s   h  �  �   �  �      u           �    �  �                  �      H   �          +     �   �   �  �   �      ^       �                  �     �    �      �  2  n          I  F  v      N   #     �   !       4     [  "   V   �   �              '  U      �   �   �   &  G           H  �   >   ~   R      _  �   Z       o   8              �          �           �   c  �              �   ;   �   �           �  u  6      a  �     F   5  �   �      �         P       �      @   ~          _   �   �   w      v   �   �     �  �   �   �   �   �  �       1       3      �  �   �    �      )   &   �   E  �   e      e   4   �  J       R   �  �   .   |  �      �       P    -   �    
      \  �       �   y   7         �   �  
      W       D   "    t   �   �       �  �   ;      $  o  �    )  �   S   �      �          �    �   �  �      x   �  T  �   �   (   �   �  �                     �  I   �      �  �             q      ?  �   �  g  �               �         �  �   2       M           f      ^  ?   Q       =  Q          �      �  %          ,   L   *  p   �     l  9     5   (  �             �   j  �              �   �       W  �   �       r      p  �   S          B  Y              *   d  {  }      	   Z  �   [   �  ,  �  Y   �       �   f       h   �   �      z       �   6   <   �            �   C  �   #  l       -  �       �  �   �       �   n  �   +       �   �  /       �           c   �   �   �  �   �       	  \   �   ]      O    X     $   �       �   J  b   �       �   .  m   r   �      �   �       �       9   �   0  �  N  �                          y  �   `  t         1  �   �                      !  =                  �  C       @      i  �     j     d   U  �       <  �   8   x  V  �                      G   �   O   |   K  �   k   �  �   �   7  �   T   �       �      �   �  �  �   �  �            �   �       �  �   �       �  �  �  �       �   �   b  :        �      /  ]   	-V Output version information and exit
 	Average bitrate: %.1f kb/s

 	Elapsed time: %dm %04.1fs
 	Encoding [%2dm%.2ds so far] %c  	Rate:         %.4f
 	[%5.1f%%] [%2dm%.2ds remaining] %c  
 
	File length:  %dm %04.1fs
 

Done encoding file "%s"
 

Done encoding.
 
Audio Device:   %s   --audio-buffer n        Use an output audio buffer of 'n' kilobytes
   -@ file, --list file    Read playlist of files and URLs from "file"
   -K n, --end n           End at 'n' seconds (or hh:mm:ss format)
   -R, --raw               Read and write comments in UTF-8
   -R, --remote            Use remote control interface
   -V, --version           Display ogg123 version
   -V, --version           Output version information and exit
   -Z, --random            Play files randomly until interrupted
   -a, --append            Append comments
   -b n, --buffer n        Use an input buffer of 'n' kilobytes
   -c file, --commentfile file
                          When listing, write comments to the specified file.
                          When editing, read comments from the specified file.
   -d dev, --device dev    Use output device "dev". Available devices:
   -e, --escapes           Use \n-style escapes to allow multiline comments.
   -f file, --file file    Set the output filename for a file device
                          previously specified with --device.
   -h, --help              Display this help
   -k n, --skip n          Skip the first 'n' seconds (or hh:mm:ss format)
   -l s, --delay s         Set termination timeout in milliseconds. ogg123
                          will skip to the next song on SIGINT (Ctrl-C),
                          and will terminate if two SIGINTs are received
                          within the specified timeout 's'. (default 500)
   -l, --list              List the comments (default if no options are given)
   -o k:v, --device-option k:v
                          Pass special option 'k' with value 'v' to the
                          device previously specified with --device. See
                          the ogg123 man page for available device options.
   -p n, --prebuffer n     Load n%% of the input buffer before playing
   -q, --quiet             Don't display anything (no title)
   -r, --repeat            Repeat playlist indefinitely
   -t "name=value", --tag "name=value"
                          Specify a comment tag on the commandline
   -v, --verbose           Display progress and other status information
   -w, --write             Write comments, replacing the existing ones
   -x n, --nth n           Play every 'n'th block
   -y n, --ntimes n        Repeat every played block 'n' times
   -z, --shuffle           Shuffle list of files before playing
  --bits, -b       Bit depth for output (8 and 16 supported)
  --discard-comments   Prevents comments in FLAC and Ogg FLAC files from
                      being copied to the output Ogg Vorbis file.
 --ignorelength       Ignore the datalength in Wave headers. This allows
                      support for files > 4GB and STDIN data streams. 

  --endianness, -e Output endianness for 16-bit output; 0 for
                  little endian (default), 1 for big endian.
  --help,  -h      Produce this help message.
  --output, -o     Output to given filename. May only be used
                  if there is only one input file, except in
                  raw mode.
  --quiet, -Q      Quiet mode. No console output.
  --raw, -R        Raw (headerless) output.
  --sign, -s       Sign for output PCM; 0 for unsigned, 1 for
                  signed (default 1).
  --version, -V    Print out version number.
  -N, --tracknum       Track number for this track
 -t, --title          Title for this track
 -l, --album          Name of album
 -a, --artist         Name of artist
 -G, --genre          Genre of track
  -q, --quality        Specify quality, between -1 (very low) and 10 (very
                      high), instead of specifying a particular bitrate.
                      This is the normal mode of operation.
                      Fractional qualities (e.g. 2.75) are permitted
                      The default quality level is 3.
  Input Buffer %5.1f%%  Output Buffer %5.1f%%  by the Xiph.Org Foundation (http://www.xiph.org/)

 %s: illegal option -- %c
 %s: invalid option -- %c
 %s: option `%c%s' doesn't allow an argument
 %s: option `%s' is ambiguous
 %s: option `%s' requires an argument
 %s: option `--%s' doesn't allow an argument
 %s: option `-W %s' doesn't allow an argument
 %s: option `-W %s' is ambiguous
 %s: option requires an argument -- %c
 %s: unrecognized option `%c%s'
 %s: unrecognized option `--%s'
 %sEOS %sPaused %sPrebuf to %.1f%% '%s' is not valid UTF-8, cannot add
 (NULL) (min %d kbps, max %d kbps) (min %d kbps, no max) (no min or max) (no min, max %d kbps) (none) --- Cannot open playlist file %s.  Skipped.
 --- Cannot play every 0th chunk!
 --- Cannot play every chunk 0 times.
--- To do a test decode, use the null output driver.
 --- Driver %s specified in configuration file invalid.
 --- Hole in the stream; probably harmless
 --- Prebuffer value invalid. Range is 0-100.
 255 channels should be enough for anyone. (Sorry, but Vorbis doesn't support more)
 === Cannot specify output file without specifying a driver.
 === Could not load default driver and no driver specified in config file. Exiting.
 === Driver %s is not a file output driver.
 === Error "%s" while parsing config option from command line.
=== Option was: %s
 === Incorrect option format: %s.
 === No such device %s.
 === Option conflict: End time is before start time.
 === Parse error: %s on line %d of %s (%s)
 === Vorbis library reported a stream error.
 AIFF/AIFC file reader Aspect ratio undefined
 Author:   %s Available codecs:  Available options:
 Avg bitrate: %5.1f BOS not set on first page of stream
 BUG: Got zero samples from resampler: your file will be truncated. Please report this.
 Bad comment: "%s"
 Bad type in options list Bad value Big endian 24 bit PCM data is not currently supported, aborting.
 Bitrate hints: upper=%ld nominal=%ld lower=%ld window=%ld Bitstream error, continuing
 Cannot open %s.
 Cannot read header Category: %s
 Changed lowpass frequency from %f kHz to %f kHz
 Channels: %d
 Character encoding: %s
 Colourspace unspecified
 Colourspace: Rec. ITU-R BT.470-6 System M (NTSC)
 Colourspace: Rec. ITU-R BT.470-6 Systems B and G (PAL)
 Comment: Comments: %s Copyright Corrupt or missing data, continuing... Corrupt secondary header. Could not skip %f seconds of audio. Could not skip to %f in audio stream. Couldn't close output file
 Couldn't convert comment to UTF-8, cannot add
 Couldn't create directory "%s": %s
 Couldn't get enough memory for input buffering. Couldn't get enough memory to register new stream serial number. Couldn't initialise resampler
 Couldn't open %s for reading
 Couldn't open %s for writing
 Couldn't parse cutpoint "%s"
 Couldn't write packet to output file
 Cutpoint not found
 Decode options
 Decoding "%s" to "%s"
 Default Description Done. Downmixing stereo to mono
 EOF before end of Vorbis headers. EOF before recognised stream. ERROR - line %u: Syntax error: %s
 ERROR - line %u: end time must not be less than start time: %s
 ERROR: %s requires an output filename to be specified with -f.
 ERROR: An output file cannot be given for %s device.
 ERROR: Can only specify one input file if output filename is specified
 ERROR: Cannot open device %s.
 ERROR: Cannot open file %s for writing.
 ERROR: Cannot open input file "%s": %s
 ERROR: Cannot open output file "%s": %s
 ERROR: Could not allocate memory in malloc_buffer_stats()
 ERROR: Could not allocate memory in malloc_data_source_stats()
 ERROR: Could not allocate memory in malloc_decoder_stats()
 ERROR: Could not create required subdirectories for output filename "%s"
 ERROR: Could not set signal mask. ERROR: Decoding failure.
 ERROR: Device %s failure.
 ERROR: Device not available.
 ERROR: Failed to load %s - can't determine format
 ERROR: Failed to open input as Vorbis
 ERROR: Failed to open input file: %s
 ERROR: Failed to open lyrics file %s (%s)
 ERROR: Failed to open output file: %s
 ERROR: Failed to write Wave header: %s
 ERROR: File %s already exists.
 ERROR: Input file "%s" is not a supported format
 ERROR: Input filename is the same as output filename "%s"
 ERROR: Multiple files specified when using stdin
 ERROR: Multiple input files with specified output filename: suggest using -n
 ERROR: No Ogg data found in file "%s".
Input probably not Ogg.
 ERROR: No input files specified. Use -h for help
 ERROR: No input files specified. Use -h for help.
 ERROR: No lyrics filename to load from
 ERROR: Out of memory in create_playlist_member().
 ERROR: Out of memory in decoder_buffered_metadata_callback().
 ERROR: Out of memory in malloc_action().
 ERROR: Out of memory in new_audio_reopen_arg().
 ERROR: Out of memory in new_status_message_arg().
 ERROR: Out of memory in playlist_to_array().
 ERROR: Out of memory.
 ERROR: This error should never happen (%d).  Panic!
 ERROR: Unable to create input buffer.
 ERROR: Unsupported option value to %s device.
 ERROR: Wav file is unsupported subformat (must be 8,16, or 24 bit PCM
or floating point PCM
 ERROR: Wav file is unsupported type (must be standard PCM
 or type 3 floating point PCM
 ERROR: buffer write failed.
 Editing options
 Enabling bitrate management engine
 Encoded by: %s Encoding %s%s%s to 
         %s%s%s 
at approximate bitrate %d kbps (VBR encoding enabled)
 Encoding %s%s%s to 
         %s%s%s 
at average bitrate %d kbps  Encoding %s%s%s to 
         %s%s%s 
at quality %2.2f
 Encoding %s%s%s to 
         %s%s%s 
at quality level %2.2f using constrained VBR  Encoding %s%s%s to 
         %s%s%s 
using bitrate management  Error checking for existence of directory %s: %s
 Error in header: not vorbis?
 Error opening %s using the %s module.  The file may be corrupted.
 Error opening comment file '%s'
 Error opening comment file '%s'.
 Error opening input file "%s": %s
 Error opening input file '%s'.
 Error opening output file '%s'.
 Error reading first page of Ogg bitstream. Error reading initial header packet. Error removing erroneous temporary file %s
 Error removing old file %s
 Error renaming %s to %s
 Error unknown. Error writing stream to output. Output stream may be corrupted or truncated. Error writing to file: %s
 Error: Could not create audio buffer.
 Error: Out of memory in decoder_buffered_metadata_callback().
 Error: Out of memory in new_print_statistics_arg().
 Error: path segment "%s" is not a directory
 Examples:
  vorbiscomment -a in.ogg -c comments.txt
  vorbiscomment -a in.ogg -t "ARTIST=Some Guy" -t "TITLE=A Title"
 FLAC file reader FLAC,  Failed encoding Kate EOS packet
 Failed encoding Kate header
 Failed encoding karaoke motion - continuing anyway
 Failed encoding karaoke style - continuing anyway
 Failed encoding lyrics - continuing anyway
 Failed to convert to UTF-8: %s
 Failed to open file as Vorbis: %s
 Failed to set advanced rate management parameters
 Failed to set bitrate min/max in quality mode
 Failed to write comments to output file: %s
 Failed writing data to output stream
 Failed writing fisbone header packet to output stream
 Failed writing fishead packet to output stream
 Failed writing header to output stream
 Failed writing skeleton eos packet to output stream
 File: File: %s Frame aspect %f:1
 Frame aspect 16:9
 Frame aspect 4:3
 Frame offset/size invalid: height incorrect
 Frame offset/size invalid: width incorrect
 Framerate %d/%d (%.02f fps)
 Header packet corrupt
 Height: %d
 If no output file is specified, vorbiscomment will modify the input file. This
is handled via temporary file, such that the input file is not modified if any
errors are encountered during processing.
 Input buffer size smaller than minimum size of %dkB. Input filename may not be the same as output filename
 Input is not an Ogg bitstream. Input not ogg.
 Input options
 Input truncated or empty. Internal error parsing command line options
 Internal error parsing command line options.
 Internal error parsing command options
 Internal error: Unrecognised argument
 Internal error: attempt to read unsupported bitdepth %d
 Internal stream parsing error
 Invalid zero framerate
 Invalid/corrupted comments Kate headers parsed for stream %d, information follows...
 Kate stream %d:
	Total data length: % Key not found Language: %s
 List or edit comments in Ogg Vorbis files.
 Listing options
 Live: Logical bitstreams with changing parameters are not supported
 Logical stream %d ended
 Lower bitrate not set
 Lower bitrate: %f kb/s
 Memory allocation error in stats_init()
 Miscellaneous options
 Mode initialisation failed: invalid parameters for bitrate
 Mode initialisation failed: invalid parameters for quality
 Mode number %d does not (any longer) exist in this version Multiplexed bitstreams are not supported
 NOTE: Raw mode (--raw, -R) will read and write comments in UTF-8 rather than
converting to the user's character set, which is useful in scripts. However,
this is not sufficient for general round-tripping of comments in all cases,
since comments can contain newlines. To handle that, use escaping (-e,
--escape).
 Name Negative or zero granulepos (% No category set
 No input files specified. "ogginfo -h" for help
 No key No language set
 No module could be found to read from %s.
 No value for advanced encoder option found
 Nominal bitrate not set
 Nominal bitrate: %f kb/s
 Nominal quality setting (0-63): %d
 Note: Stream %d has serial number %d, which is legal but may cause problems with some tools.
 OPTIONS:
 General:
 -Q, --quiet          Produce no output to stderr
 -h, --help           Print this help text
 -V, --version        Print the version number
 Ogg FLAC file reader Ogg Speex stream: %d channel, %d Hz, %s mode Ogg Speex stream: %d channel, %d Hz, %s mode (VBR) Ogg Vorbis stream: %d channel, %ld Hz Ogg Vorbis.

 Ogg bitstream does not contain Vorbis data. Ogg bitstream does not contain a supported data-type. Opening with %s module: %s
 Out of memory
 Output options
 Page error, continuing
 Page found for stream after EOS flag Pixel aspect ratio %d:%d (%f:1)
 Pixel format 4:2:0
 Pixel format 4:2:2
 Pixel format 4:4:4
 Pixel format invalid
 Playing: %s Playlist options
 Processing failed
 Processing file "%s"...

 Processing: Cutting at %lf seconds
 Quality option "%s" not recognised, ignoring
 RAW file reader Rate: %ld

 ReplayGain (Album): ReplayGain (Track): ReplayGain Peak (Album): ReplayGain Peak (Track): Requesting a minimum or maximum bitrate requires --managed
 Resampling input from %d Hz to %d Hz
 Scaling input to %f
 Set optional hard quality restrictions
 Setting advanced encoder option "%s"
 Setting advanced encoder option "%s" to %s
 Skipping chunk of type "%s", length %d
 Specify "." as the second output file to suppress this error.
 Speex version: %s Speex,  Success Supported options:
 System error Target bitrate: %d kbps
 Text directionality: %s
 The file format of %s is not supported.
 The file was encoded with a newer version of Speex.
 You need to upgrade in order to play it.
 The file was encoded with an older version of Speex.
You would need to downgrade the version in order to play it. Theora headers parsed for stream %d, information follows...
 Theora stream %d:
	Total data length: % This version of libvorbisenc cannot set advanced rate management parameters
 Time: %s To avoid creating an output file, specify "." as its name.
 Track number: Type Unknown character encoding
 Unknown error Unknown text directionality
 Unrecognised advanced option "%s"
 Upper bitrate not set
 Upper bitrate: %f kb/s
 Usage: 
  vorbiscomment [-Vh]
  vorbiscomment [-lRe] inputfile
  vorbiscomment <-a|-w> [-Re] [-c file] [-t tag] inputfile [outputfile]
 Usage: ogg123 [options] file ...
Play Ogg audio files and network streams.

 Usage: oggdec [options] file1.ogg [file2.ogg ... fileN.ogg]

 Usage: oggenc [options] inputfile [...]

 Usage: ogginfo [flags] file1.ogg [file2.ogx ... fileN.ogv]

ogginfo is a tool for printing information about Ogg files
and for diagnosing problems with them.
Full help shown with "ogginfo -h".
 Usage: vcut infile.ogg outfile1.ogg outfile2.ogg [cutpoint | +cuttime]
 User comments section follows...
 Vendor: %s
 Vendor: %s (%s)
 Version: %d
 Version: %d.%d
 Version: %d.%d.%d
 Vorbis format: Version %d Vorbis headers parsed for stream %d, information follows...
 Vorbis stream %d:
	Total data length: % WARNING - line %d: failed to get UTF-8 glyph from string
 WARNING - line %d: failed to process enhanced LRC tag (%*.*s) - ignored
 WARNING - line %d: lyrics times must not be decreasing
 WARNING - line %u: missing data - truncated file?
 WARNING - line %u: non consecutive ids: %s - pretending not to have noticed
 WARNING - line %u: text is too long - truncated
 WARNING: Can't downmix except from stereo to mono
 WARNING: Comment %d in stream %d has invalid format, does not contain '=': "%s"
 WARNING: Could not decode Vorbis header packet %d - invalid Vorbis stream (%d)
 WARNING: Could not read directory %s.
 WARNING: Couldn't parse scaling factor "%s"
 WARNING: Couldn't read endianness argument "%s"
 WARNING: Couldn't read resampling frequency "%s"
 WARNING: EOS not set on stream %d
 WARNING: Expected frame % WARNING: Failure in UTF-8 decoder. This should not be possible
 WARNING: Hole in data (%d bytes) found at approximate offset % WARNING: Ignoring illegal escape character '%c' in name format
 WARNING: Illegal UTF-8 sequence in comment %d (stream %d): invalid sequence "%s": %s
 WARNING: Illegal UTF-8 sequence in comment %d (stream %d): length marker wrong
 WARNING: Illegal UTF-8 sequence in comment %d (stream %d): too few bytes
 WARNING: Illegal comment used ("%s"), ignoring.
 WARNING: Insufficient lyrics languages specified, defaulting to final lyrics language.
 WARNING: Invalid bits/sample specified, assuming 16.
 WARNING: Invalid channel count specified, assuming 2.
 WARNING: Invalid header page in stream %d, contains multiple packets
 WARNING: Invalid header page, no packet found
 WARNING: Invalid sample rate specified, assuming 44100.
 WARNING: Multiple output files specified, suggest using -n
 WARNING: No filename, defaulting to "%s"
 WARNING: Unknown option specified, ignoring->
 WARNING: discontinuity in stream (%d)
 WARNING: failed to add Kate karaoke style
 WARNING: failed to allocate memory - enhanced LRC tag will be ignored
 WARNING: found EOS before cutpoint
 WARNING: hole in data (%d)
 WARNING: input file ended unexpectedly
 WARNING: language can not be longer than 15 characters; truncated.
 WARNING: maximum bitrate "%s" not recognised
 WARNING: minimum bitrate "%s" not recognised
 WARNING: no language specified for %s
 WARNING: nominal bitrate "%s" not recognised
 WARNING: quality setting too high, setting to maximum quality.
 WARNING: stream start flag found in mid-stream on stream %d
 WARNING: stream start flag not set on stream %d
 WARNING: subtitle %s is not valid UTF-8
 WAV file reader Warning from playlist %s: Could not read directory %s.
 Warning: AIFF-C header truncated.
 Warning: Can't handle compressed AIFF-C (%c%c%c%c)
 Warning: Corrupted SSND chunk in AIFF header
 Warning: Could not read directory %s.
 Warning: INVALID format chunk in wav header.
 Trying to read anyway (may not work)...
 Warning: No SSND chunk found in AIFF file
 Warning: No common chunk found in AIFF file
 Warning: OggEnc does not support this type of AIFF/AIFC file
 Must be 8 or 16 bit PCM.
 Warning: Truncated common chunk in AIFF header
 Warning: Unexpected EOF in AIFF chunk
 Warning: Unexpected EOF in reading AIFF header
 Warning: Unexpected EOF in reading WAV header
 Warning: Unexpected EOF reading AIFF header
 Warning: Unrecognised format chunk in WAV header
 Warning: WAV 'block alignment' value is incorrect, ignoring.
The software that created this file is incorrect.
 Width: %d
 bad comment: "%s"
 bool char default output device double float int left to right, top to bottom no action specified
 none of %s ogg123 from %s %s ogg123 from %s %s
 by the Xiph.Org Foundation (http://www.xiph.org/)

 oggdec from %s %s
 oggenc from %s %s oggenc from %s %s
 ogginfo from %s %s
 other repeat playlist forever right to left, top to bottom shuffle playlist standard input standard output string top to bottom, left to right top to bottom, right to left utf-8 vorbiscomment from %s %s
 by the Xiph.Org Foundation (http://www.xiph.org/)

 vorbiscomment from vorbis-tools  vorbiscomment handles comments in the format "name=value", one per line. By
default, comments are written to stdout when listing, and read from stdin when
editing. Alternatively, a file can be specified with the -c option, or tags
can be given on the commandline with -t "name=value". Use of either -c or -t
disables reading from stdin.
 Project-Id-Version: vorbis-tools
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-03-26 03:08-0400
PO-Revision-Date: 2013-06-19 17:22+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:21+0000
X-Generator: Launchpad (build 18115)
 	-V Versionsinformationen anzeigen und beenden
 	Durchschnittliche Bitrate: %.1f kbit/s

 	Verstrichene Zeit: %dm %04.1fs
 	Kodierung läuft [bisher %2dm%.2ds] %c  	Rate:         %.4f
 	[%5.1f%%] [%2dmin%.2ds verbleibend] %c  
 
	Dateilänge:  %dmin %04.1fs
 

Kodieren von Datei »%s« abgeschlossen.
 

Kodieren abgeschlossen.
 
Audiogerät: %s   --audio-buffer n        Benutze einen »n« Kilobyte großen Ausgabepuffer.
   -@ Datei, --list Datei    Lese Wiedergabelisten von Dateien und URLs aus »Datei«.
   -K n, --end n           Ende ab »n« Sekunden (oder hh:mm:ss-Format).
   -R, --raw               Kommentare in UTF-8 lesen und schreiben
   -R, --remote            Verwende die Fernverwaltungsschnittstelle
   -V, --version           ogg123-Version anzeigen
   -V, --version           Versionsinformationen anzeigen und beenden
   -Z, --random            Spiele Dateien zufällig ab, bis unterbrochen wird.
   -a, --append            Kommentare erweitern
   -b n, --buffer n        Benutze einen »n« Kilobytes großen Puffer
   -c Datei, --commentfile Datei
                        Kommentare werden während der Wiedergabe in die angegebene Datei geschrieben.
                        Während der Bearbeitung werden die Kommentare aus der angegebenen Datei gelesen.
   -d dev, --device dev    Ausgabegerät »dev« verwenden. Verfügbare Geräte:
   -e, --escapes           \n-Stil-Umbrüche verwenden, um mehrzeilige Kommentare zu ermöglichen.
   -f Datei, --file Datei    Bestimmen des Ausgabedateinamen für ein Dateigerät,
                          das vorher mit --device festgelegt wurde.
   -h, --help              Diese Hilfe anzeigen
   -k n, --skip n          Die ersten »n« Sekunden (oder hh:mm:ss-Format) überspringen.
   -l s, --delay s         Verzögerung zum Beenden festlegen. ogg123
                          springt zum nächsten Titel in SIGINT (Strg-C) und
                          beendet sich, sobald zwei SIGINTs innerhalb der ange-
                          gebenen Verzögerung »s« empfangen wurden. (Standard 500)
   -l, --list              Kommentare auflisten (Standard, wenn keine Optionen angegeben sind)
   -o k:v, --device-option k:v
                          Übergebe die Option ‚k‘ mit dem Inhalt ‚v‘ an das Gerät,
                          das vorher in --device angegeben wurde. Für mögliche
                          Geräteoptionen siehe ogg123-Handbuchseite.
   -p n, --prebuffer n     n%% des Eingabepuffers vor dem Abspielen laden
   -q, --quiet             Zeige nichts an (kein Titel).
   -r, --repeat            Wiedergabeliste unendlich wiederholen
   -t »Name=Wert«, --tag »Name=Wert«
                          Einen Kommentar in der Befehlszeile festlegen
   -v, --verbose           Fortschritt und andere Statusinformationen anzeigen.
   -w, --write             Kommentare schreiben, existierende ersetzen
   -x n, --nth n           Jeden »n«-ten Block wiedergeben
   -y n, --ntimes n        Jeden gespielten Block »n«-mal wiederholen.
   -z, --shuffle           Mische die Liste der Dateien vorm Abspielen.
  --bits, -b       Bit-Tiefe für die Ausgabe (8 und 16 unterstützt)
  --discard-comments   Verhindert das Kopieren von Kommentaren in FLAC-
                      und Ogg-FLAC-Dateien in die resultierende
                      OggVorbis-Datei.
 --ignorelength       Die Datenlänge in Wave-Headers ignorieren. Dies
                      ermöglicht die Unterstützung für Dateien größer als
                      4 GB und die Verarbeitung von Datenströmen
                      aus der Standardeingabe. 

  --endianness, -e Endianness für 16-Bit-Ausgabe ausgeben; 0 für
                  Little Endian (Standard), 1 für Big Endian.
  --help,  -h      Diesen Hilfetext anzeigen.
  --output, -o     In angegebenen Dateinamen ausgeben.
                  Nur mit einer Eingabedatei verwenden, außer im
                  Rohmodus.
  --quiet, -Q      Stumm-Modus. Keine Konsolenausgabe.
  --raw, -R        Rohe (header-lose) Ausgabe.
  --sign, -s       Signatur für Ausgabe-PCM; 0 für nicht signiert, 1 für
                  signiert (Standard 1).
  --version, -V    Versionsnummer anzeigen.
  -N, --tracknum       Titelnummer dieses Titels
 -t, --title          Name dieses Titels
 -l, --album          Name des Albums
 -a, --artist         Name des Künstlers
 -G, --genre          Genre des Titels
  -q, --quality        Qualität angeben, zwischen -1 (sehr niedrig) und 10 
                      (sehr hoch), anstelle der Angabe einer spezifischen
                      Bitrate. Dies ist der Normalmodus dieser Operation.
                      Gebrochene Zahlen zur Qualitätsangabe sind zulässig
                      (z.B. 2.75). Die Standard-Qualitätsstufe ist 3.
  Eingabepuffer %5.1f%%  Ausgabepuffer %5.1f%%  von der Xiph.Org-Foundation (http://www.xiph.org/)

 %s: Die Option ist nicht erlaubt -- %c
 %s: Ungültige Option -- %c
 %s: Die Option »%c%s« erlaubt kein Argument
 %s: Die Option »%s« ist nicht eindeutig
 %s: Die Option »%s« erfordert ein Argument
 %s: Die Option »--%s« erlaubt kein Argument
 %s: Die Option »-W %s« erlaubt kein Argument
 %s: Die Option »-W %s« ist nicht eindeutig
 %s: Diese Option benötigt ein Argument -- %c
 %s: Unbekannte Option »%c%s«
 %s: Unbekannte Option »--%s«
 %sEOS %sAngehalten %sPräpuffer zu %.1f%% »%s« ist kein gültiges UTF-8, Hinzufügen nicht möglich
 (NULL) (min. %d kbit/s, max. %d kbit/s) (min, %d kbit/s, kein max.) (kein min. oder max.) (kein min., max. %d kbit/s) (Nichts) --- Wiedergabeliste %s kann nicht geöffnet werden. Übersprungen.
 --- Kann nicht jedes 0te Stück abspielen.
 --- Kann nicht jedes Stück 0 mal abspielen.
--- Verwenden Sie den null-Ausgabetreiber, um eine Test-Dekodierung durchzuführen.
 --- In der Konfigurationsdatei festgelegter Treiber%s ist ungütig.
 --- Unterbrechung im Datenstrom; wahrscheinlich unbedenklich
 --- Der Vorpufferwert ist nicht korrekt. Der Bereich geht von 0-100.
 255 Kanäle sollten für jeden genug sein. (Entschuldigung, aber Vorbis unterstützt nicht mehr.)
 === Ausgabedatei kann nicht ohne Treiber festgelegt werden.
 === Der Standardtreiber kann nicht geladen werden und kein Treier in Eingabedatei festgelegt. Beende.
 === Treiber %s ist kein Dateiausgabetreiber.
 === Fehler »%s« bei Verarbeitung der Kommandozeilenparameter.
=== Option war: %s
 === Fehlerhaftes Optionenformat: %s
 === Gerät %s nicht vorhanden.
 === Optionskonflikt: Ende liegt vor dem Anfang.
 === Analysefehler: %s in Zeile %d von %s (%s)
 === Vorbis-Bibliothek lieferte einen Datenstromfehler.
 AIFF/AIFC-Dateileser Seitenverhältnis nicht definiert
 Künstler: %s Verfügbare Codecs:  Verfügbare Optionen:
 Bit-Rate i.D.: %5.1f BOS auf der ersten Seite des Streams nicht festgelegt
 FEHLER: Null Samples vom Resampler erhalten, Ihre Datei
wird abgeschnitten. Bitte melden Sie diesen Fehler.
 Falscher Kommentar: »%s«
 Fehlerhafter Typ in Optionenliste Fehlerhafter Wert »Big Endian 24-Bit-PCM«-Daten werden derzeit nicht unterstützt, Abbruch.
 Bit-Raten-Hinweise: Maximum=%ld Nominal=%ld Minimumn=%ld Fenster=%ld Bitstream-Fehler, es wird fortgefahren
 %s kann nicht geöffnet werden.
 Header kann nicht gelesen werden Kategorie: %s
 Tiefpassfrequenz wird von %f kHz auf %f kHz geändert
 Kanäle: %d
 Zeichenkodierung: %s
 Farbraum nicht angegeben
 Farbraum: Rec. ITU-R BT.470-6 System M (NTSC)
 Farbraum: Rec. ITU-R BT.470-6 Systems B und G (PAL)
 Kommentar: Kommentare: %s Urheberrecht Beschädigte oder fehlende Daten, wird fortgesetzt … Defekter sekundärer Header. %f Sekunden konnten nicht übersprungen werden. Springen zu %f im Audiodatenstrom nicht möglich. Ausgabedatei kann nicht geschlossen werden
 Kommentar kann nicht in UTF-8 umgewandelt
und daher nicht hinzugefügt werden
 Ordner »%s« konnte nicht erstellt werden: %s
 Ungenügender Speicher für Eingabepuffer. Ungenügender Speicher zur Registrierung der Seriennummer des neuen Datenstroms. Resampler konnte nicht initialisiert werden
 %s kann nicht zum Lesen geöffnet werden
 %s kann nicht zum Schreiben geöffnet werden
 Schnittpunkt »%s« kann nicht verarbeitet werden
 Paket konnte nicht in Ausgabedatei geschrieben werden
 Schnittpunkt konnte nicht gefunden werden
 Dekodierungs-Optionen
 »%s« wird in »%s« dekodiert
 Standardeinstellungen Beschreibung Fertig. Stereo wird in Mono umgewandelt
 Dateiende vor Ende der Vorbis-Header. Dateiende vor erkanntem Datenstrom. FEHLER - Zeile %u: Syntaxfehler: %s
 FEHLER - Zeile %u: Endzeit darf nicht vor der Startzeit liegen: %s
 FEHLER: %s erfordert, dass mit der Option »-f« ein Dateiname für die Ausgabe angegeben wird.
 FEHLER: Eine Ausgabedatei kann für das Gerät %s nicht angegeben werden.
 FEHLER: Es darf nur eine Eingabedatei angegeben werden, wenn ein Ausgabedateiname festgelegt wird
 FEHLER: Das Gerät %s kann nicht geöffnet werden.
 FEHLER: Datei %s kann nicht zum Schreiben geöffnet werden.
 FEHLER: Eingabedatei »%s« kann nicht geöffnet werden: %s
 Fehler: Zieldatei »%s« kann nicht geöffnet werden: %s
 FEHLER: In malloc_buffer_stats() konnte kein Speicher zugewiesen werden.
 FEHLER: Speicherzuordnung in malloc_data_source_stats() gescheitert
 FEHLER: Speicherzuweisung in malloc_decoder_stats() gescheitert
 FEHLER: Benötigte Unterordner für Ausgabedatei »%s« können nicht angelegt werden
 FEHLER: Signalmaske konnte nicht gesetzt werden. FEHLER: Dekodierungsfehler.
 FEHLER: Fehler beim Gerät %s.
 FEHLER: Gerät ist nicht verfügbar.
 FEHLER: Laden von %s gescheitert - Format kann nicht bestimmt werden
 FEHLER: Eingabe kann nicht als Vorbis geöffnet werden
 FEHLER: Eingabedatei kann nicht geöffnet werden: %s
 FEHLER: Liedtextdatei %s konnte nicht geöffnet werden (%s)
 FEHLER: Ausgabedatei kann nicht geöffnet werden: %s
 FEHLER: Schreiben des Wave-Headers gescheitert: %s
 FEHLER: Die Datei %s existiert bereits.
 FEHLER: Eingabedatei »%s« nicht in einem unterstützten Format
 FEHLER: Namen für Eingabe- und Ausgabedatei »%s« sind gleich
 FEHLER: Mehrere Dateien bei Verwendung der Standardeingabe angegeben
 WARNUNG: Mehrere Eingabedateien angegeben, Verwendung von -n wird empfohlen.
 FEHLER: Keine Ogg-Daten in Datei »%s« gefunden.
Eingabe ist wahrscheinlich nicht Ogg.
 FEHLER: Keine Eingabedatei angegeben. Verwenden Sie -h für Hilfe
 FEHLER: Keine Eingabedateien angegeben. Mit -h erhalten Sie Hilfe.
 FEHLER: Kein Songtext-Dateiname von dem geladen werden kann
 FEHLER: Speicher ausgeschöpft in create_playlist_member().
 FEHLER: Nicht genügend Speicher in decoder_buffered_metadata_callback().
 FEHLER: Bei der Speicherzuordnung (malloc_action()) wurde der Speicher überschritten.
 FEHLER: Nicht genügend Speicher in new_audio_reopen_arg().
 FEHLER: Nicht genügend Speicher in new_status_message_arg().
 FEHLER: Speicher ausgeschöpft in playlist_to_array().
 FEHLER: Speicherüberlauf.
 FEHLER: Eigentlich sollte das nicht passieren (%d). Panik!
 FEHLER: Erstellen des Eingabepuffers gescheitert.
 FEHLER: Nicht unterstützte Option für das Gerät %s.
 FEHLER: Wav-Datei ist nicht unterstütztes Unterformat (muss 8-, 16- oder 24-Bit-PCM
oder Fließkomme-PCM sein)
 FEHLER: Wav-Datei ist nicht unterstützter Typ (muss Standard-PCM
 oder Typ-3-Fließkomma-PCM sein)
 FEHLER: Schreiben in Puffer ist gescheitert.
 Bearbeitungsoptionen
 Engine zur Bitratenverwaltung wird aktiviert
 Kodiert von: %s Kodierung %s%s%s in 
         %s%s%s 
bei geschätzter Bitrate von %d kbps (VBR-Kodierung aktiv)
 Kodierung %s%s%s in 
         %s%s%s 
bei durchschnittlicher Bitrate von %d kbps  Kodierung %s%s%s in 
         %s%s%s 
mit Qualität %2.2f
 Kodierung %s%s%s in 
         %s%s%s 
mit Qualitätsstufe %2.2f unter Verwendung von eingeschränktem VBR  Kodierung %s%s%s in 
         %s%s%s 
mit Bitratenverwaltung  Fehler beim Prüfen der Existenz des Verzeichnisses %s: %s
 Fehler im Header: nicht Vorbis?
 Fehler beim Öffnen von %s mit dem Modul %s. Die Datei könnte beschädigt sein.
 Fehler beim Öffnen der Kommentardatei »%s«.
 Fehler beim Öffnen der Kommentardatei »%s«.
 Fehler beim Öffnen der Eingabedatei »%s«: %s
 Fehler beim Öffnen der Eingabedatei »%s«.
 Fehler beim Öffnen der Ausgabedatei »%s«.
 Fehler beim Lesen der ersten Seite des Ogg-Bitstreams. Fehler beim Lesen des initialen Header-Pakets. Fehler beim Entfernen der fehlerhaften zwischengespeicherten Datei %s
 Fehler beim Entfernen der alten Datei »%s«.
 Fehler beim Umbenennen von »%s« in »%s«
 Unbekannter Fehler. Fehler beim Schreiben des Ausgabestroms. Ausgabestrom könnte beschädigt oder abgeschnitten sein. Fehler beim Schreiben in Datei: %s
 Fehler: Audiopuffer konnte nicht erstellt werden.
 Fehler: Zu wenig Speicher in decoder_buffered_metadata_callback().
 Fehler: Zu wenig Speicher in new_print_statistics_arg().
 Fehler: Pfadsegment »%s« ist kein Ordner
 Beispiele:
  vorbiscomment -a in.ogg -c kommentare.txt
  vorbiscomment -a in.ogg -t »ARTIST=Ein Typ« -t »TITLE=Ein Titel«
 FLAC-Dateileser FLAC,  Entschlüsseln des »Kate EOS«-Pakets gescheitert – Vorgang wird trotzdem fortgesetzt
 Kate-Header konnte nicht enkodiert werden
 Entschlüsseln der Karaoke-Bewegung gescheitert – Vorgang wird trotzdem fortgesetzt
 Entschlüsseln des Karaoke-Stils gescheitert – Vorgang wird trotzdem fortgesetzt
 Entschlüsseln des Liedtextes gescheitert – Vorgang wird trotzdem fortgesetzt
 Umwandlung in UTF-8 ist gescheitert: %s
 Öffnen der Datei als Vorbis fehlgeschlagen: %s
 Festlegen erweiterter Ratenverwaltungsparameter gescheitert
 Setzen der Minimal-/Maximalwerte für Bitrate
im Qualitätsmodus ist gescheitert
 Schreiben von Kommentaren in Ausgabedatei gescheitert: %s
 Schreiben von Daten in den Ausgabe-Datenstrom gescheitert
 Schreiben des »Fisbone«-Kopfpakets in die Ausgabe gescheitert
 Schreiben des »Fishead«-Pakets in die Ausgabe gescheitert
 Schreiben des Headers in den Ausgabe-Datenstrom gescheitert
 Schreiben des  »Skeleton EOS«-Pakets in die Ausgabe gescheitert
 Datei: Datei: %s Seitenverhältnis %f:1
 Seitenverhältnis 16:9
 Seitenverhältnis 4:3
 Rahmen-Versatz/-Größe ungültig: Falsche Höhe
 Rahmen-Versatz/-Größe ungültig: Falsche Breite
 Bildrate %d/%d (%.02f fps)
 Headerpaket ist beschädigt
 Höhe: %d
 Wenn keine Ausgabedatei angegeben ist, verändert vorbiscomment die Eingabedatei. Dies
geschieht über eine zwischengespeicherte Datei, sodass die Eingabedatei unverändert bleibt, 
falls ein Fehler während der Verarbeitung auftritt.
 Eingabepuffer kleiner als die Minimalgröße von %dkB. Der Name der Eingabedatei darf nicht der selbe wie der Name der Ausgabedatei sein
 Eingabe ist kein Ogg-Bitstream. Eingabe nicht im ogg-Format.
 Eingabeoptionen
 Eingabe abgeschnitten oder leer. Interner Fehler beim Einlesen der Befehlszeilenoptionen
 Interner Fehler bei Bearbeitung der Kommandozeilenparameter.
 Interner Fehler beim Übersetzen der Befehlsoptionen.
 Interner Fehler: Unbekanntes Argument
 Interner Fehler: Versuch, nicht unterstützte Bit-Tiefe %d zu lesen
 Interner Datenstromverarbeitungsfehler
 Ungültige Bildrate Null
 Ungültige/Beschädigte Kommentare Kate-Header für Datenstrom %d analysiert, Informationen folgen …
 Kate-Datenstrom %d:
	Gesamtdatenlänge: % Schlüssel nicht gefunden Sprache: %s
 Informationen aus »Ogg Vorbis«-Dateien anzeigen oder ändern.
 Listenoptionen
 Live: Logische Bit-Datenströme mit sich ändernden Parametern werden nicht unterstützt
 Logischer Datenstrom %d beendet
 Untere Bitrate nicht festgelegt
 Untere Bitrate: %f Kb/s
 Speicherzuordnungsfehler in stats_init()
 Sonstige Optionen
 Initialisierung des Modus gescheitert: Ungültige Parameter für Bitrate
 Modusinitialisierung gescheitert: Ungültige Parameter für die Qualität
 Modusnummer %d existiert in dieser Version nicht (mehr) Multiplexte Bitstreams werden nicht unterstützt
 HINWEIS: Der Raw-Modus (--raw, -R) liest und schreibt Kommentare in UTF-8,
anstatt diese in den Zeichensatz des Benutzers umzuwandeln. Dies ist
insbesondere in Skripten nützlich. Es ist allerdings nicht in allen Fällen
für die allgemeine Verarbeitung von Kommentaren ausreichend, da Kommentare
Zeilenumbrüche (newline) enthalten können. Um dies zu erreichen, verwenden
Sie Escaping mit (-e, --escape).
 Name Feldgröße von %q+D ist null oder negativ Keine Kategorie festgelegt
 Keine Eingabedateien angegeben. Mit »ogginfo -h« erhalten Sie Hilfe
 Kein Schlüssel Keine Sprache festgelegt
 Es wurde kein Modul gefunden, um von %s zu lesen.
 Kein Wert für erweiterte Kodierungseinstellung gefunden
 Nominale Bitrate nicht festgelegt
 Nominale Bitrate: %f kbit/s
 Nominelle Qualitätseinstellung: (0-63): %d
 Hinweis: Datenstrom %d hat die Seriennummer %d, was zwar legal ist,
 aber mit einigen Werkzeugen Fehler hervorrufen kann.
 OPTIONEN:
 Allgemein:
 -Q, --quiet          Keine Ausgaben an stderr
 -h, --help           Diesen Hilfetext ausgeben
 -V, --version        Die Versionsnummer ausgeben
 »Ogg FLAC«-Dateileser »Ogg Speex«-Datenstrom: %d-Kanal, %d Hz, %s Modus »Ogg Speex«-Datenstrom: %d-Kanal, %d Hz, %s Modus (VBR) »Ogg Vorbis«-Datenstrom: %d-Kanal, %ld Hz Ogg Vorbis.

 Ogg-Bitstream enthält keine Vorbis-Daten. Der Ogg-Bit-Datenstrom enthält kein unterstütztes Datenformat. Öffnen mit Modul %s: %s
 Nicht genügend Speicher
 Ausgabeoptionen
 Seitenfehler, wird fortgesetzt
 Seite für Datenstrom nach EOS-Markierung gefunden Pixel-Seitenverhältnis %d:%d (%f:1)
 Pixel-Format 4:2:0
 Pixel-Format 4:2:2
 Pixel-Format 4:4:4
 Pixel-Format ungültig
 Spiele: %s Wiedergabeliste-Optionen
 Verarbeitung fehlgeschlagen
 Datei »%s« wird verarbeitet...

 Verarbeitung: Bei %lf Sekunden wird geschnitten
 Qualitätsoption »%s« nicht berücksichtigt, wird ignoriert
 RAW-Dateileser Rate: %ld

 ReplayGain (Album): ReplayGain (Titel): ReplayGain-Spitze (Album): ReplayGain-Spitze (Ttitel): Anforderung einer minimalen/maximalen Bitrate erfordert --managed
 Eingangssignal von %d Hz in %d Hz umwandeln.
 Eingabe wird auf %f skaliert
 Optional erhöhte Qualitätsbeschränkungen festlegen
 Erweiterte Kodierereinstellung »%s« wird gesetzt
 Erweiterte Kodierereinstellung »%s« wird auf %s gesetzt
 Teil des Typs »%s«, Länge %d wird übersprungen
 Geben Sie ».« als zweite Ausgabedatei an, um diesen Fehler zu unterdrücken.
 Speex-Version: %s Speex,  Erfolg Unterstützte Optionen:
 Systemfehler Ziel-Bitrate: %d Kbps
 Schreibrichtung des Texts: %s
 Das Dateiformat %s wird nicht unterstützt.
 Die Datei wurde mit einer neueren Speex-Version erstellt.
 Sie müssen auf diese Version aktualisieren, um diese
 Datei wiedergeben zu können.
 Die Datei wurde mit einer älteren Speex-Version erstellt.
 Sie müssen auf diese Version zurückgehen, um diese
 Datei wiedergeben zu können. Theora-Header werden für Datenstrom %d verarbeitet, Informationen folgen...
 Theora-Datenstrom %d:
	Gesamtlänge der Daten: % Diese Version von libvorbisenc kann keine erweiterten Raten-Verwaltungsparameter setzen.
 Zeit: %s Um keine Ausgabedatei zu erstellen, geben Sie ».« als Name an.
 Titelnummer: Typ Unbekannte Zeichensatzkodierung
 Unbekannter Fehler Unbekannte Schreibrichtung des Texts
 Unbekannte Disassembler-Option: %s
 Obere Bitrate nicht festgelegt
 Obere Bitrate: %f Kb/s
 Aufruf: 
  vorbiscomment [-Vh]
  vorbiscomment [-lRe] Eingabedatei
  vorbiscomment <-a|-w> [-Re] [-c Datei] [-t Tag] Eingabedatei [Ausgabedatei]
 Benutzung: ogg123 [Optionen] Datei …
Ogg-Audiodateien und Netzwerkdatenströme wiedergeben.

 Aufruf: oggdec [Optionen] Datei1.ogg [Datei2.ogg … DateiN.ogg]

 Aufruf: oggenc [Optionen] Eingabedatei [...]

 Aufruf: ogginfo [flags] Datei1.ogg [Datei2.ogx ... DateiN.ogv]

ogginfo ist ein Werkzeug zur Ausgabe von Informationen zu
Ogg-Dateien und zur Diagnose dabei auftretender Probleme.
Eine vollständige Hilfe erhalten Sie mit »ogginfo -h«.
 Aufruf: vcut Eingabe.ogg Ausgabe1.ogg Ausgabe2.ogg [Schnittpunkt | +Schnittzeit]
 Benutzerkommentarabschnitt folgt …
 Anbieter: %s
 Anbieter: %s (%s)
 Version: %d
 Version: %d.%d
 Version: %d.%d.%d
 Vorbis-Format: Version %d Vorbis-Header für Datenstrom %d werden verarbeitet, Information folgt...
 Vorbis-Datenstrom %d:
	Gesamtlänge der Daten: % WARNUNG – Zeile %d: UTF-8-Glyphe konnte nicht aus der Zeichenkette extrahiert werden
 WARNUNG – Zeile %d: Verarbeitung des erweiterten LRC-Tags fehlgeschlagen (%*.*s) – ignoriert
 WARNUNG – Zeile %d: Songtext-Zeiten müssen absteigend sein
 WARNUNG - Zeile %u: Fehlende Daten - abgeschnittene Datei?
 WARNUNG – Zeile %u: Nicht fortlaufende IDs: %s – wird ignoriert
 WARNUNG - Zeile %u: Text ist zu lang - wird abgeschnitten
 WARNUNG: Heruntermischen nicht möglich, außer von Stereo nach Mono.
 WARNUNG: Kommentar %d im Datenstrom %d hat ein ungültiges Format, er enthält kein '=': »%s«
 WARNUNG: Vorbis-Headerpaket %d kann nicht dekodiert werden - ungültiger Vorbis-Datenstrom (%d)
 Warnung: Ordner %s konnte nicht gelesen werden.
 WARNUNG: Skalierungsfaktor »%s« kann nicht verarbeitet werden
 WARNUNG: Byte-Reihenfolgen-Argument »%s« konnte nicht gelesen werden.
 WARNUNG: Resampling-Frequenz »%s« konnte nicht gelesen werden.
 WARNUNG: Datenstromende nicht gesetzt für Datenstrom %d
 WARNUNG: Erwartetes Einzelbild % WARNUNG: Fehler im UTF-8-Dekodierer. Das sollte nicht möglich sein
 WARNUNG: Datenloch (%d Byte) gefunden bei einem Versatz von ca. % WARNUNG: Illegales Escape-Zeichen »%c« im Format des Namens
 WARNung: Ungültige UTF-8-Sequenz im Kommentar %d (Datenstrom %d): Ungültige Sequenz »%s«: %s
 WARNUNG: Ungültige UTF-8-Sequenz im Kommentar %d (Datenstrom %d): Längenmarkierung falsch
 WARNUNG: Ungültige UTF-8-Sequenz im Kommentar %d (Datenstrom %d): Zu wenig Byte
 WARNUNG: Unerlaubter Kommentar (»%s«), wird ignoriert.
 WARNUNG: Nicht genügend Sprachen für Liedtexte angegeben, standardmäßig wird die finale Sprache für Liedtexte verwendet.
 WARNUNG: Ungültige Bits/Sample angegeben, 16 wird angenommen.
 WARNUNG: Ungültige Anzahl an Kanälen angegeben, es werden 2 angenommen.
 WARNUNG: Ungültige Header-Seite in Datenstrom %d, enthält mehrere Pakete
 WARNUNG: Ungültige Header-Seite, kein Paket gefunden
 WARNUNG: Ungültige Abtastrate angegeben, 44100 wird angenommen.
 WARNUNG: Mehrere Ausgabedateien angegeben, Verwendung von -n wird empfohlen.
 WARNUNG: Kein Dateiname, »%s« wird verwendet
 WARNUNG: Unbekannte Option angegeben, wird ignoriert->
 WARNUNG: Unterbrechung im Datenstrom (%d)
 WARNUNG: Hinzufügen des Kate-Karaokestils gescheitert
 WARNUNG: Speicherreservierung fehlgeschlagen – Erweiterter LRC-Tag wird ignoriert
 WARNUNG: Datenstromende gefunden vor Schnittpunkt
 WARNUNG: Datenleck (%d)
 WARNUNG: Unerwartetes Ende der Eingabedatei
 WARNUNG: Sprache darf 15 Zeichen nicht überschreiten; wird abgeschnitten.
 WARNUNG: Maximale Bitrate »%s« nicht berücksichtigt
 WARNUNG: Minimale Bitrate »%s« nicht berücksichtigt
 WARNUNG: Keine Sprache angegeben für %s
 WARNUNG: Nominale Bitrate »%s« nicht berücksichtigt
 WARNUNG: Qualitätseinstellung zu hoch, maximale Qualität wird verwendet.
 WARNUNG: Startmarkierung im Mittelteil des Datenstroms %d gefunden
 WARNUNG: Keine Startmarkierung im Datenstrom %d gesetzt
 Warnung: Untertitel %s ist kein gültiges UTF-8
 WAV-Dateileser Warnung von Wiedergabeliste %s: Verzeichnis %s kann nicht gelesen werden.
 Warnung: AIFF-C-Header abgeschnitten.
 Warnung: Komprimierter AIFF-C (%c%c%c%c) kann nicht verarbeitet werden
 Warnung: Beschädigter SSND-Teil im AIFF-Header
 Warnung: Ordner %s kann nicht gelesen werden.
 Warnung: Teil in UNTÜLTIGEM Format im WAV-Header.
 Es wird versucht, trotzdem zu lesen (könnte funktionieren) …
 Warnung: Kein SSND-Teil in AIFF-Datei gefunden
 Warnung: Kein gemeinsamer Teil in AIFF-Datei gefunden
 Warnung: OggEnc unterstützt diesen Typ von AIFF/AIFC-Datei nicht
 Muss 8 oder 16-Bit PCM sein.
 Warnung: Abgeschnittener gemeinsamer Teil im AIFF-Header
 Warnung: Unerwartetes Dateiende im AIFF-Stück
 Warnung: Unerwarteter EOF beim Lesen des AIFF-Headers
 Warnung: Unerwarteter EOF beim Lesen des WAV-Headers
 Warnung: Unerwarteter EOF beim Lesen des AIFF-Headers
 Warnung: Teil in unbekanntem Format im WAV-Header
 Warnung: WAV-»Blockausrichtungs«-Wert ist falsch, wird ignoriert.
Das Programm, welches diese Datei erstellt hat, ist fehlerhaft.
 Breite: %d
 Falscher Kommentar: »%s«
 Wahrheitswert Zeichen Standardausgabegerät Double Gleitkommazahl Ganzzahl Links nach rechts, oben nach unten Keine Aktion angegeben
 Nichts von %s ogg123 aus %s %s ogg123 aus %s %s
 von der Xiph.Org Foundation (http://www.xiph.org/)

 oggdec aus %s %s
 oggenc von %s %s oggenc von %s %s
 ogginfo von %s %s
 Andere Wiedergabeliste immer wiederholen Rechts nach links, oben nach unten Wiedergabeliste mischen Standardeingabe Standardausgabe Zeichenkette Oben nach unten, links nach rechts Oben nach unten, rechts nach links UTF-8 vorbiscomment von %s %s
 von der Xiph.Org Foundation (http://www.xiph.org/)

 vorbiscomment von vorbis-tools  Vorbiscomment verarbeitet Kommentare im Format »Name=Wert«, zeilenweise.
In der Voreinstellung werden Kommentare beim Hören in die Standardausgabe geschrieben und 
beim Bearbeiten aus der Standardeingabe gelesen. Alternativ kann eine Datei mit der Option -c angegeben
werden oder die Einträge werden mit -t »Name=Wert« über die Kommandozeile eingegeben.
Um das Einlesen von der Standardeingabe zu verhindern, kann entweder -c oder -t genutzt werden.
 