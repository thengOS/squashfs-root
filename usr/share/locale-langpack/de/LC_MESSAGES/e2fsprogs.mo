��    �     ,  �  <,      �:     �:     ;  
   ;     (;  *   7;      b;  %   �;  %   �;  U   �;     %<     8<  #   S<  *   w<  A   �<  D   �<  �   )=  H  >  $   \?  3   �?     �?  -   �?  �   @  &   �@  h   �@      GA  ~   hA  E   �A  0   -B  b   ^B  .   �B  C   �B     4C     HC     [C     kC     {C     �C     �C     �C  $  �C     �D     �D  #   E  0   8E  '   iE  2   �E     �E     �E  *   �E  *   F  3   EF     yF  '   �F  -   �F     �F  "   �F     !G  "   >G     aG     �G  ,   �G  3   �G      H     H  &   -H     TH  N   lH     �H     �H  #   �H  .   I     <I  	   EI     OI     _I  ,   pI     �I     �I  -   �I  .   �I  (   #J     LJ  '   SJ     {J     �J     �J     �J     �J     �J     �J     K     0K     AK     UK     hK     yK     �K     �K     �K     �K     �K     �K      L  .    L     OL     ^L     qL     �L     �L     �L     �L     �L      M     -M     MM     hM     wM     �M     �M     �M     �M  *   �M  /   N     ?N  '   UN     }N     �N     �N  +   �N     �N     O  s   (O  +   �O  +   �O  (   �O     P      7P  !   XP  -   zP     �P  +   �P  8   �P  0   !Q  .   RQ     �Q  '   �Q  :   �Q  )   �Q  L   "R  5   oR     �R     �R     �R     �R     S  -   -S     [S  +   wS  1   �S     �S     �S     T  "   +T     NT  7   kT  -   �T  9   �T     U  )   'U     QU     lU  ;   �U     �U  %   �U     �U      V  #   4V     XV     vV     �V  '   �V     �V  N   �V  =   >W  )   |W     �W  )   �W     �W  &   �W     $X  "   5X  !   XX     zX  0   �X     �X     �X     �X  	   Y     Y      Y  	   (Y     2Y     8Y     FY     cY     �Y     �Y  5   �Y  "   �Y     �Y  *   �Y  D   'Z     lZ  $   �Z     �Z  ,   �Z     �Z     �Z  =   [  :   M[     �[  B   �[     �[  
   �[     �[     �[     \     (\     E\      c\     �\     �\  :   �\  *   �\  ;   "]  *   ^]  +   �]     �]     �]     �]     �]     �]  0   �]     ^     :^     Q^  $   Z^     ^     �^  '   �^  )   �^      _  .   (_  3   W_  +   �_  2   �_     �_  C   `  L   G`  .   �`  +   �`  "   �`  :   a  &   Ma     ta     {a     �a     �a     �a     �a  �   �a  5   �b  O   c     Sc     rc  ;   �c     �c  &   �c     d  $   d     Bd  [   Kd  +   �d  2   �d  '   e  '   .e     Ve      ve     �e  .   �e      �e     f     f  #   :f  #   ^f     �f  (   �f     �f  ;   �f  9    g  (   Zg  (   �g  1   �g  ,   �g  "   h  %   .h  #   Th     xh  #   �h  (   �h     �h  +   i     0i     7i  %   Qi     wi  1   �i     �i     �i     �i  >   �i     &j  ,   ;j  =   hj  /   �j  "   �j  0   �j     *k     ?k     Ck     Wk  O   ek     �k  '   �k  2   �k  '   +l  2   Sl     �l     �l     �l     �l     �l     m     
m     m     %m  #   .m     Rm  /   mm  +   �m     �m     �m     �m     n      +n  .   Ln  "   {n     �n     �n     �n     �n  #   o  !   2o  =   To     �o     �o     �o     �o  3   �o  
   /p     :p     Yp  >   xp  1   �p     �p     q  "   q     Aq     Tq     nq    �q  ;   �r  9   �r     s  $   (s  _   Ms  $   �s  $   �s     �s  %   �s  2   $t     Wt     tt     {t     �t  !   �t      �t     �t  "   �t     u  (   u     >u  "   Ju  %   mu  &   �u     �u     �u  D   �u     v  	   *v  4   4v     iv  :   �v     �v     �v  '   �v     �v     w  "   (w  "   Kw     nw  7   �w  (   �w     �w     �w     	x     x  
   x     #x     +x  -   @x  "   nx     �x  /   �x     �x  /   �x  "   /y  #   Ry  #   vy     �y     �y  +   �y  +   �y  =   )z  3   gz  V   �z     �z     �z  %   {     '{     ;{     M{  6   i{  6   �{  	   �{     �{     �{  D   |  :   \|  �   �|  0   (}  &   Y}  v   �}  Y   �}  +   Q~  =   }~  g   �~  2   #  �   V     �     �  
   �     �     �     /�     J�     h�     ��     ��     ��     ��  /   Ԁ     �     $�     9�  !   I�  L   k�     ��     ́  �   �  %   ��     ��     ւ     �  3   ��  H   2�  $   {�  B   ��  2   �  '   �  (   >�  $   g�  *   ��  >   ��  %   ��  O   �  S   l�     ��     ޅ     ��     �  ;   +�  n   g�  5   ֆ     �     %�     -�     A�     Y�     q�     �     ��     ��     ��     ه     ��     �     '�     9�  7   S�     ��     ��     ��  #   ��  &   Ј     ��  +   
�     6�  	   B�     L�     ]�  
   m�  	   x�     ��     ��     ��  !   ��     ŉ     ߉  !   �  3   �     A�  #   Z�  )   ~�  "   ��  "   ˊ      �     �     �     +�     9�     I�  0   \�  0   ��  /   ��  8   �     '�  
   3�     >�  )   S�     }�     ��     ��     ��     ˌ     �     �     �     6�     O�     V�  !   g�     ��     ��     ��     ��     ʍ     �     ��  "   	�  6   ,�     c�  &   {�  %   ��     Ȏ  $   ��      �     &�     /�     7�     C�     T�  4   k�     ��     ��     ͏     �  
   �  %   ��     �     %�     (�  	   ,�     6�     I�     U�  #   m�     ��     ��     ͐     ڐ     �  D   �     F�     e�     n�     ��  
   ��     ��     ��  *   ��  -   �  %   �     8�     X�     h�     t�  (   |�  (   ��     Β  !   �  (   	�  /   2�  "   b�     ��     ��  (   ��     �  (   ��     '�     >�     Z�  %   s�  %   ��     ��      ܔ     ��     �     ,�     E�  0   c�     ��     ��  -   ĕ     �      �     /�  "   H�  #   k�  (   ��     ��  %   Ж  *   ��     !�     ?�  "   Y�     |�     ��     ��     Η  *   �  %   �  )   7�  .   a�     ��  "   ��     ̘  %   �  '   
�     2�     M�     g�     ��     ��     ��     ҙ     �     �      #�     D�     ]�     w�     z�     ~�     ��  �  ��     r�     ��     ��     ��  +   ��     �  !   �  #   (�  W   L�     ��     ��  +   ٝ  .   �  A   4�  K   v�        ޟ  /   �  F   �     e�  9   ��  �   ��  3   q�  j   ��  3   �  �   D�  L   �  5   2�  �   h�  3   �  R    �     s�     ��     ��     ��  %   ��  %   �  (   	�     2�  j  9�     ��     ��  &   ɧ  ,   �  )   �  F   G�  (   ��     ��  1   Ǩ  ,   ��  3   &�  /   Z�  +   ��  3   ��     �  %   ��     %�  &   B�  *   i�  0   ��  2   Ū  =   ��     6�     O�  1   l�  "   ��  t   ��     6�     N�  #   d�  9   ��  
   ¬     ͬ     ֬     �  /   ��     '�     =�  2   T�  3   ��  +   ��     �  7   �     (�     ;�     U�     h�     �  "   ��  "   ��     �     ��     �     +�     E�     V�     q�      ��     ��  "   ¯     �     �     ��  -   �     ;�     J�      ]�     ~�     ��     ��     ð     �      ��     �     6�     S�     f�     �     ��  #   ��  $   б  ;   ��  :   1�     l�  +   ��  (   ��     ۲      ��  ,   �     I�     c�  {   w�  '   �  (   �  +   D�     p�  )   ��  )   ��  -   �     �  3   .�  3   b�  3   ��  3   ʵ     ��  +   �  N   >�  '   ��  U   ��  E   �  #   Q�     u�  #   ��     ��     ͷ  3   �  ,    �  5   M�  ?   ��  .   ø     �     �  "   -�     P�  @   o�  3   ��  B   �     '�  3   F�      z�      ��  Q   ��     �  1   %�     W�  '   m�  '   ��     ��     ܻ     ��  +   �  (   <�  \   e�  B   ¼  1   �     7�  6   W�     ��  6   ��     ؽ  %   �  !   �     6�  >   V�     ��     ��     þ  	   ھ     �     ��     �  	   �     �  (   )�      R�     s�  	   ��  ?   ��     ӿ     �  .   ��  Z   )�  $   ��  ;   ��     ��  >   ��     3�  #   ;�  =   _�  B   ��     ��  V   ��  	   Q�  
   [�     f�  "   o�     ��  (   ��     ��  '   ��     "�     B�  H   `�  0   ��  I   ��  0   $�  5   U�  	   ��     ��     ��     ��  	   ��  N   ��     !�     <�  
   V�  *   a�  !   ��  &   ��  .   ��  4   �  -   9�  8   g�  C   ��  A   ��  9   &�      `�  N   ��  U   ��  ?   &�  5   f�  .   ��  L   ��  "   �     ;�     D�     d�      �  
   ��     ��  6  ��  ;   ��  ^   ,�     ��     ��  G   ��  -   ��  )   -�     W�  *   n�  	   ��  b   ��  -   �  @   4�  )   u�  )   ��  )   ��  *   ��  &   �  B   E�  '   ��  %   ��  "   ��  '   ��      !�      B�  *   c�     ��  E   ��  G   ��  6   ;�  7   r�  2   ��  -   ��  '   �  (   3�  '   \�  &   ��  #   ��  -   ��  !   ��  /   �  	   O�     Y�  "   x�     ��  8   ��     ��  	   �     �  V   #�     z�  ?   ��  Z   ��  9   +�      e�  8   ��     ��  	   ��      ��      �  [   �     u�  .   ��  5   ��  .   ��  :   &�     a�  $   x�     ��  /   ��     ��  	   ��     �     �     $�  +   5�     a�  5   �  ,   ��     ��     ��  "   �     7�  (   P�  7   y�  *   ��  (   ��  #   �     )�  #   B�  '   f�  %   ��  E   ��     ��     �  #   *�     N�  D   k�     ��  #   ��  !   ��  >   �  <   C�     ��     ��  .   ��     ��     �     $�  =  ;�  R   y�  >   ��     �  /   %�  i   U�  2   ��  2   ��     %�  +   1�  ?   ]�  &   ��     ��      ��     ��  &   ��  &   $�     K�  -   W�     ��  ,   ��     ��  .   ��  2   ��  L   ,�     y�     ��  ?   ��     ��     ��  ;   ��     9�  M   Q�     ��     ��  (   ��  !   ��     �  5   �  0   U�     ��  M   ��     ��     ��     �     !�     *�     6�     C�     I�  5   b�  )   ��  "   ��  G   ��      -�  5   N�  '   ��  /   ��  /   ��     �     )�  4   I�  D   ~�  S   ��  _   �  _   w�     ��     ��  9   ��     &�     ;�  "   R�  F   u�  F   ��     �     �     �  G   ?�  A   ��  �   ��  .   U�  '   ��  �   ��  l   0�  /   ��  Q   ��  {   �  >   ��  �   ��  $   ��  	   ��     ��     ��  !   ��     ��     �     '�  !   G�     i�     ��  !   ��  0   ��  "   ��     ��     �  $   *�  X   O�     ��     ��  �   ��  ,   ��  %   ��     ��     ��  :   �  O   P�  ,   ��  D   ��  C   �  -   V�  ,   ��  %   ��  0   ��  Q   �  #   Z�  \   ~�  f   ��  $   B�     g�     ��     ��  ?   ��  y   ��  :   u�  )   ��     ��     ��  !   ��  '    �     H�     `�     {�     ��  0   ��  0   ��     �  #   3�     W�  #   m�  L   ��     ��     ��     ��  @   ��  <   ?�     |�  5   ��     ��     ��     ��     �     �     &�     2�  	   L�  
   V�  %   a�     ��     ��  (   ��  4   ��      �  #   .�  )   R�  "   |�  /   ��     ��     ��     ��     �     )�     B�  =   U�  =   ��  A   ��  C   �     W�     d�     p�  9   ��     ��     ��     ��     ��     �     2�     L�     i�     ��     ��     ��  !   ��     ��     ��     �     �     +�     I�     i�  1   w�  ;   ��     ��  1   �  -   4�     b�  5   ��  *   ��     ��     ��     ��             @   2     s     �  !   �     �  
   �  3   �  
   	         	       )    ;    G #   ]    �    �    �    �    � �   � #   �    �    �    �    �    �    � 3    C   D 5   � "   �    �    �     4    +   C    o )   � .   � ;   � /    "   N    q $   � '   � (   �         !    B ,   b +   �    � 3   �     ,       L %   g 3   �    �    � 0   �    % "   C    f ,   � 2   �    �     	 ?   	 8   ^	 $   �	    �	 '   �	     
    
    =
 1   \
 %   �
 .   �
 %   �
 )   	 *   3    ^    ~ "   � /   � "   � +    *   <    g %   �    �    �     � !    &   0    W    v    �    �    �    �            Y  �  �  ,       �  q                 h   �      �        d  ]   G  q  b  �    ;  H  )      C   X          R              �     <  ?  7     �  $      N  l   D  �   1  3     8  w  -   y   '  �   D   �            {   2  �   �   ^  C  h  9  j   �  �  f  �   �       �   s  2  �  �  �  �        @          �       �   �   �  "     {  �   �  2   '                   k  �             6      �          !  ~  �  c      �   �  �  |   =   a  Z        7   
  �  O  r  �   �   c   �       �   �    #   B  �     �           �   �  V   �   4  E           �       m          u  �   �                    �   �   
      ]  V  �   !  �        �      �   �       x  [         <      �         �   i   �  L      �      �   `  0   n    �  +   �      3  	  �  �        �  B   �      ^  O   M      �    )      �  �        �      y  �      �   B  f         !   A   }  �  s  �   �   �  �  \          F  �  5  �    �  �  �      �  o  �       v  H       }  �     �       g  �   P       >  K  �    z      �  J   �  �  �              �          �  �   p       k   �   �   �  m   �   �          �       U  �      �      |  �       �   �  �         {    �     &   �   }   p  u  �  �      �   �     7  �  �       �           �   8      �  9  �  �  ~       �   �  �          �   *      k              �      �   �      �   �       3  �     t  �  j  �   �       <   1  _  e      �          �       �      �               w       �         �          �  [  �  T      g      �   *  �       ]      [   0      R   �  �   S     (       a  �  �  i     *       �  �      g  :   �   �  �  +              x              �        �   �      �  |  �  $        �       �  D  .   9     .  n   #      �  �     #  �  \     V  �   �  M   &  �    �   &  :  >  b  �   �  4       �      �  5       ,          �  r  '  �  �      �     �  l  �  �       �          �      �   >         �   �      �   �   S  w  m  �  �  �  U   �      �  �      +    .  ;            
   J  E    K  �      P  �  c  �  b       �   )   �   ;         �  �                   �  6      F   �  �      `   Z       t       �      �   ,  N   Q      S         u   h      �      r   W     j  L      �  �   �   �     �  �  �  T  �  �  �   =  o                      �  �          �  �  M  �  %       (  �          o      �       L  �       �  �  N  f  �  X      �  �  �   �  �  e  5  �   �                      �   _       0  �   �      v   �    \      I          �  �  �       Y    �       �           �  �     ^   �  �  �  Q   �  �  �   8   �         �       �  �      �      s   �   n  /       �      	  �    �  �   /  p  �            �      ~  F        	   P      e   ?   �      �     l    �  �   �   �      U          G        1   _  d   �  �   4  O      �          �      "      I   �     (  z      y     �  �   :         �      -      �   �              �         �   �      @   v  �   I      �  �  �   �       �   6      x  $     �  �  �  �      �  W  �   �  @  �   -  �  R  �      C  ?  �      `  /  �   �  E  =      �  �          Y   %      Z  �   �  z   �   �   �  �   �  X   A                    i  �   �       H  "  t       a          �  �  T   �   J      A  �  d  %      W     Q  K       �     �   �          �      �                 G       �  �  �          q               �  �       	%Q (@i #%i, mod time %IM)
 	<@f metadata>
 	Using %s
 	Using %s, %s
 
	while trying to add journal to device %s 
	while trying to create journal 
	while trying to create journal file 
	while trying to open journal on %s
 

%s: UNEXPECTED INCONSISTENCY; RUN fsck MANUALLY.
	(i.e., without -a or -p options)
 
  Inode table at  
  Reserved GDT blocks at  
%s: %s: error reading bitmaps: %s
 
%s: ***** FILE SYSTEM WAS MODIFIED *****
 
%s: ********** WARNING: Filesystem still has errors **********

 
*** journal has been re-created - filesystem is now ext3 again ***
 
Bad extended option(s) specified: %s

Extended options are separated by commas, and may take an argument which
	is set off by an equals ('=') sign.

Valid extended options are:
	superblock=<superblock number>
	blocksize=<blocksize>
 
Emergency help:
 -p                   Automatic repair (no questions)
 -n                   Make no changes to the filesystem
 -y                   Assume "yes" to all questions
 -c                   Check for bad blocks and add them to the badblock list
 -f                   Force checking even if filesystem is marked clean
 
Filesystem too small for a journal
 
If the @b is really bad, the @f can not be fixed.
 
Interrupt caught, cleaning up
 
Invalid non-numeric argument to -%c ("%s")

 
Journal block size:       %u
Journal length:           %u
Journal first block:      %u
Journal sequence:         0x%08x
Journal start:            %u
Journal number of users:  %u
 
Journal size too big for filesystem.
 
Running additional passes to resolve @bs claimed by more than one @i...
Pass 1B: Rescanning for @m @bs
 
Sparse superblock flag set.  %s 
The bad @b @i has probably been corrupted.  You probably
should stop now and run e2fsck -c to scan for bad blocks
in the @f.
 
The device apparently does not exist; did you specify it correctly?
 
The filesystem already has sparse superblocks.
 
The requested journal size is %d blocks; it must be
between 1024 and 10240000 blocks.  Aborting.
 
Warning, had trouble writing out superblocks. 
Warning: RAID stripe-width %u not an even multiple of stride %u.

   %s superblock at    Block bitmap at    Free blocks:    Free inodes:   (check after next mount)  (check deferred; on battery)  (check in %ld mounts)  (y/n)  -v                   Be verbose
 -b superblock        Use alternative superblock
 -B blocksize         Force blocksize when looking for superblock
 -j external_journal  Set location of the external journal
 -l bad_blocks_file   Add to badblocks list
 -L bad_blocks_file   Set badblocks list
  Done.
  Group descriptor at   contains a file system with errors  has been mounted %u times without being checked  has gone %u days without being checked  primary superblock features different from backup  was not cleanly unmounted # Extent dump:
 %d-byte blocks too big for system (max %d) %s %s: status is %x, should never happen.
 %s @o @i %i (uid=%Iu, gid=%Ig, mode=%Im, size=%Is)
 %s has unsupported feature(s): %s is apparently in use by the system;  %s is entire device, not just one partition!
 %s is mounted;  %s is not a block special device.
 %s is not a journal device.
 %s: %s filename nblocks blocksize
 %s: %s trying backup blocks...
 %s: ***** REBOOT LINUX *****
 %s: Error %d while executing fsck.%s for %s
 %s: The -n and -w options are mutually exclusive.

 %s: e2fsck canceled.
 %s: journal too short
 %s: no valid journal superblock found
 %s: recovering journal
 %s: skipping bad line in /etc/fstab: bind mount with nonzero fsck pass number
 %s: too many arguments
 %s: too many devices
 %s: wait: No more child process?!?
 %s: won't do journal recovery while read-only
 %s? no

 %s? yes

 %u block group
 %u block groups
 %u blocks per group, %u fragments per group
 %u inodes per group
 %u inodes scanned.
 '.' @d @e in @d @i %i is not NULL terminated
 '..' @d @e in @d @i %i is not NULL terminated
 '..' in %Q (%i) is %P (%j), @s %q (%d).
 (NONE) (There are %N @is containing @m @bs.)

 (no prompt) , Group descriptors at  , Inode bitmap at  , check forced.
 --waiting-- (pass %d)
 -O may only be specified once -o may only be specified once /@l is not a @d (ino=%i)
 /@l not found.   <Reserved inode 10> <Reserved inode 9> <The NULL inode> <The bad blocks inode> <The boot loader inode> <The group descriptor inode> <The journal inode> <The undelete directory inode> <n> <y> = is incompatible with - and +
 @A %N contiguous @b(s) in @b @g %g for %s: %m
 @A @a @b %b.   @A @b @B (%N): %m
 @A @b buffer for relocating %s
 @A @d @b array: %m
 @A @i @B (%N): %m
 @A @i @B (@i_dup_map): %m
 @A icount link information: %m
 @A icount structure: %m
 @A new @d @b for @i %i (%s): %m
 @A refcount structure (%N): %m
 @D @i %i has zero dtime.   @E @L to '.'   @E @L to @d %P (%Di).
 @E @L to the @r.
 @E has @D/unused @i %Di.   @E has @n @i #: %Di.
 @E has a @z name.
 @E has a non-unique filename.
Rename to %s @E has an incorrect filetype (was %Dt, @s %N).
 @E has filetype set.
 @E has illegal characters in its name.
 @E has rec_len of %Dr, @s %N.
 @E is duplicate '.' @e.
 @E is duplicate '..' @e.
 @E points to @i (%Di) located in a bad @b.
 @I @i %i in @o @i list.
 @I @o @i %i in @S.
 @S @b_size = %b, fragsize = %c.
This version of e2fsck does not support fragment sizes different
from the @b size.
 @S @bs_per_group = %b, should have been %c
 @S first_data_@b = %b, should have been %c
 @S hint for external superblock @s %X.   @a @b %b has h_@bs > 1.   @a @b %b is corrupt (@n name).   @a @b %b is corrupt (@n value).   @a @b %b is corrupt (allocation collision).   @a @b @F @n (%If).
 @a in @i %i has a namelen (%N) which is @n
 @a in @i %i has a value @b (%N) which is @n (must be 0)
 @a in @i %i has a value offset (%N) which is @n
 @a in @i %i has a value size (%N) which is @n
 @b @B differences:  @b @B for @g %g is not in @g.  (@b %b)
 @f contains large files, but lacks LARGE_FILE flag in @S.
 @f did not have a UUID; generating one.

 @f does not have resize_@i enabled, but s_reserved_gdt_@bs
is %N; @s zero.   @f has feature flag(s) set, but is a revision 0 @f.   @g %g's @b @B (%b) is bad.   @g %g's @b @B at %b @C.
 @g %g's @i @B (%b) is bad.   @g %g's @i @B at %b @C.
 @g %g's @i table at %b @C.
 @h %i has a tree depth (%N) which is too big
 @h %i has an @n root node.
 @h %i has an unsupported hash version (%N)
 @h %i uses an incompatible htree root node flag.
 @i %i (%Q) has @n mode (%Im).
 @i %i (%Q) is an @I @b @v.
 @i %i (%Q) is an @I FIFO.
 @i %i (%Q) is an @I character @v.
 @i %i (%Q) is an @I socket.
 @i %i has @cion flag set on @f without @cion support.   @i %i has INDEX_FL flag set but is not a @d.
 @i %i has INDEX_FL flag set on @f without htree support.
 @i %i has a bad @a @b %b.   @i %i has a extra size (%IS) which is @n
 @i %i has illegal @b(s).   @i %i has imagic flag set.   @i %i is a %It but it looks like it is really a directory.
 @i %i is a @z @d.   @i %i is in use, but has dtime set.   @i %i is too big.   @i %i ref count is %Il, @s %N.   @i %i was part of the @o @i list.   @i %i, i_@bs is %Ib, @s %N.   @i %i, i_size is %Is, @s %N.   @i @B differences:  @i @B for @g %g is not in @g.  (@b %b)
 @i count in @S is %i, @s %j.
 @i table for @g %g is not in @g.  (@b %b)
WARNING: SEVERE DATA LOSS POSSIBLE.
 @is that were part of a corrupted orphan linked list found.   @j @i is not in use, but contains data.   @j is not regular file.   @j version not supported by this e2fsck.
 @m @b(s) in @i %i: @m @bs already reassigned or cloned.

 @n @h %d (%q).   @n @i number for '.' in @d @i %i.
 @p @h %d (%q): bad @b number %b.
 @p @h %d: root node is @n
 @r has dtime set (probably due to old mke2fs).   @r is not a @d.   @r is not a @d; aborting.
 @r not allocated.   @u @i %i
 @u @z @i %i.   ABORTED ALLOCATED Abort Aborting....
 Adding dirhash hint to @f.

 Adding journal to device %s:  Aerror allocating Allocate BLKFLSBUF ioctl not supported!  Can't flush buffers.
 Backing up @j @i @b information.

 Backup Bad @b %b used as bad @b @i indirect @b.   Bad @b @i has an indirect @b (%b) that conflicts with
@f metadata.   Bad @b @i has illegal @b(s).   Bad block %u out of range; ignored.
 Bad blocks: %u Bad or non-existent /@l.  Cannot reconnect.
 Bbitmap Begin pass %d (max = %lu)
 Block %b in the primary @g descriptors is on the bad @b list
 Block %d in primary superblock/group descriptor area bad.
 Block size=%u (log=%u)
 Blocks %u through %u must be good in order to build a filesystem.
 CLEARED CONTINUING CREATED Can not continue. Can't find external @j
 Cannot continue, aborting.

 Cannot proceed without a @r.
 Cconflicts with some other fs @b Checking all file systems.
 Checking blocks %lu to %lu
 Checking for bad blocks (non-destructive read-write test)
 Checking for bad blocks (read-only test):  Checking for bad blocks in non-destructive read-write mode
 Checking for bad blocks in read-only mode
 Checking for bad blocks in read-write mode
 Clear Clear @j Clear HTree index Clear inode Clearing Clearing filesystem feature '%s' not supported.
 Clone multiply-claimed blocks Connect to /lost+found Continue Corruption found in @S.  (%s = %N).
 Could not expand /@l: %m
 Could not reconnect %i: %m
 Could this be a zero-length partition?
 Couldn't allocate block buffer (size=%d)
 Couldn't allocate header buffer
 Couldn't allocate memory for filesystem types
 Couldn't allocate memory to parse journal options!
 Couldn't allocate memory to parse options!
 Couldn't allocate path variable in chattr_dir_proc Couldn't clone file: %m
 Couldn't determine device size; you must specify
the size manually
 Couldn't determine device size; you must specify
the size of the filesystem
 Couldn't find journal superblock magic numbers Couldn't find valid filesystem superblock.
 Couldn't fix parent of @i %i: %m

 Couldn't fix parent of @i %i: Couldn't find parent @d @e

 Couldn't parse date/time specifier: %s Create Creating journal (%d blocks):  Creating journal inode:  Creating journal on device %s:  Ddeleted Delete file Device size reported to be zero.  Invalid partition specified, or
	partition table wasn't reread after running fdisk, due to
	a modified partition being busy and in use.  You may need to reboot
	to re-read your partition table.
 Directories count wrong for @g #%g (%i, counted=%j).
 Disk write-protected; use the -n option to do a read-only
check of the device.
 Do you really want to continue Duplicate @E found.   Duplicate @e '%Dn' found.
	Marking %p (%i) to be rebuilt.

 Duplicate or bad @b in use!
 E2FSCK_JBD_DEBUG "%s" not an integer

 E@e '%Dn' in %p (%i) ERROR: Couldn't open /dev/null (%s)
 EXPANDED Either all or none of the filesystem types passed to -t must be prefixed
with 'no' or '!'.
 Empty directory block %u (#%d) in inode %u
 Error adjusting refcount for @a @b %b (@i %i): %m
 Error copying in replacement @b @B: %m
 Error copying in replacement @i @B: %m
 Error creating /@l @d (%s): %m
 Error creating root @d (%s): %m
 Error deallocating @i %i: %m
 Error determining size of the physical @v: %m
 Error iterating over @d @bs: %m
 Error moving @j: %m

 Error reading @a @b %b (%m).   Error reading @a @b %b for @i %i.   Error reading @d @b %b (@i %i): %m
 Error reading @i %i: %m
 Error reading block %lu (%s) while %s.   Error reading block %lu (%s).   Error storing @d @b information (@i=%i, @b=%b, num=%N): %m
 Error storing @i count information (@i=%i, count=%N): %m
 Error validating file descriptor %d: %s
 Error while adjusting @i count on @i %i
 Error while iterating over @bs in @i %i (%s): %m
 Error while iterating over @bs in @i %i: %m
 Error while scanning @is (%i): %m
 Error while scanning inodes (%i): %m
 Error while trying to find /@l: %m
 Error writing @a @b %b (%m).   Error writing @d @b %b (@i %i): %m
 Error writing block %lu (%s) while %s.   Error writing block %lu (%s).   Error: ext2fs library version out of date!
 Expand Extending the inode table External @j does not support this @f
 External @j has bad @S
 External @j has multiple @f users (unsupported).
 FILE DELETED FIXED Ffor @i %i (%Q) is Filesystem features not supported with revision 0 filesystems
 Filesystem label=%s
 Filesystem larger than apparent device size. Filesystem mounted or opened exclusively by another program?
 Filesystem's UUID not found on journal device.
 Finished with %s (exit status %d)
 First @e '%Dn' (@i=%Di) in @d @i %i (%p) @s '.'
 First data block=%u
 Fix Flags of %s set as  Force rewrite Found @n V2 @j @S fields (from V1 @j).
Clearing fields beyond the V1 @j @S...

 Fragment size=%u (log=%u)
 Free @bs count wrong (%b, counted=%c).
 Free @bs count wrong for @g #%g (%b, counted=%c).
 Free @is count wrong (%i, counted=%j).
 Free @is count wrong for @g #%g (%i, counted=%j).
 From block %lu to %lu
 Get a newer version of e2fsck! Group %lu: (Blocks  Group descriptors look bad... HTREE INDEX CLEARED IGNORED INODE CLEARED Ignore error Iillegal Illegal number for blocks per group Illegal number of blocks!
 Internal error: couldn't find dir_info for %i.
 Internal error: fudging end of bitmap (%N)
 Invalid EA version.
 Invalid RAID stride: %s
 Invalid RAID stripe-width: %s
 Invalid UUID format
 Invalid blocksize parameter: %s
 Invalid completion information file descriptor Invalid filesystem option set: %s
 Invalid mount option set: %s
 Invalid resize parameter: %s
 Invalid stride length Invalid stride parameter: %s
 Invalid stripe-width parameter: %s
 Invalid superblock parameter: %s
 Journal dev blocksize (%d) smaller than minimum blocksize %d
 Journal removed
 Journal size:              Journal superblock not found!
 Journal users:            %s
 Journals not supported with revision 0 filesystems
 Lis a link MULTIPLY-CLAIMED BLOCKS CLONED Maximum filesystem blocks=%lu
 Maximum of one test_pattern may be specified in read-only mode Memory used: %d, elapsed time: %6.3f/%6.3f/%6.3f
 Missing '.' in @d @i %i.
 Missing '..' in @d @i %i.
 Moving @j from /%s to hidden @i.

 Moving inode table Must use '-v', =, - or +
 No room in @l @d.   Note: if several inode or block bitmap blocks or part
of the inode table require relocation, you may wish to try
running e2fsck with the '-b %S' option first.  The problem
may lie only with the primary block group descriptors, and
the backup block group descriptors may be OK.

 On-line resizing not supported with revision 0 filesystems
 Only one of the options -p/-a, -n or -y may be specified. Optimizing directories:  Out of memory erasing sectors %d-%d
 PROGRAMMING ERROR: @f (#%N) @B endpoints (%b, %c) don't match calculated @B endpoints (%i, %j)
 Padding at end of @b @B is not set.  Padding at end of @i @B is not set.  Pass 1 Pass 1: Checking @is, @bs, and sizes
 Pass 1C: Scanning directories for @is with @m @bs
 Pass 1D: Reconciling @m @bs
 Pass 2 Pass 2: Checking @d structure
 Pass 3 Pass 3: Checking @d connectivity
 Pass 3A: Optimizing directories
 Pass 4 Pass 4: Checking reference counts
 Pass 5 Pass 5: Checking @g summary information
 Peak memory Please run 'e2fsck -f %s' first.

 Please run e2fsck on the filesystem.
 Possibly non-existent or swap device?
 Primary Proceed anyway? (y,n)  Programming error?  @b #%b claimed for no reason in process_bad_@b.
 RECONNECTED RELOCATED Random test_pattern is not allowed in read-only mode Reading and comparing:  Recovery flag not set in backup @S, so running @j anyway.
 Recreate Relocate Relocating @g %g's %s from %b to %c...
 Relocating @g %g's %s to %c...
 Relocating blocks Reserved @i %i (%Q) has @n mode.   Resize @i (re)creation failed: %m. Resize @i not valid.   Resize_@i not enabled, but the resize @i is non-zero.   Restarting e2fsck from the beginning...
 Run @j anyway Running command: %s
 SALVAGED SPLIT SUPPRESSED Salvage Scanning inode table Second @e '%Dn' (@i=%Di) in @d @i %i @s '..'
 Setting current mount count to %d
 Setting error behavior to %d
 Setting filesystem feature '%s' not supported.
 Setting filetype for @E to %N.
 Setting interval between checks to %lu seconds
 Setting maximal mount count to %d
 Setting reserved blocks gid to %lu
 Setting reserved blocks uid to %lu
 Setting stride size to %d
 Setting stripe width to %d
 Setting time filesystem last checked to %s
 Should never happen: resize inode corrupt!
 Sparse superblocks not supported with revision 0 filesystems
 Special (@v/socket/fifo) @i %i has non-zero size.   Special (@v/socket/fifo/symlink) file (@i %i) has immutable
or append-only flag set.   Split Ssuper@b Superblock backups stored on blocks:  Superblock invalid, Suppress messages Symlink %Q (@i #%i) is @n.
 Syntax error in e2fsck config file (%s, line #%d)
	%s
 Syntax error in mke2fs config file (%s, line #%d)
	%s
 TRUNCATED Testing with pattern 0x Testing with random pattern:  The -c and the -l/-L options may not be both used at the same time.
 The -t option is not supported on this version of e2fsck.
 The @f size (according to the @S) is %b @bs
The physical size of the @v is %c @bs
Either the @S or the partition table is likely to be corrupt!
 The Hurd does not support the filetype feature.
 The filesystem already has a journal.
 The filesystem revision is apparently too high for this version of e2fsck.
(Or the filesystem superblock is corrupt)

 The needs_recovery flag is set.  Please run e2fsck before clearing
the has_journal flag.
 The primary @S (%b) is on the bad @b list.
 The resize maximum must be greater than the filesystem size.
 The resize_inode and meta_bg features are not compatible.
They can not be both enabled simultaneously.
 This doesn't bode well, but we'll try to go on...
 This filesystem will be automatically checked every %d mounts or
%g days, whichever comes first.  Use tune2fs -c or -i to override.
 Too many illegal @bs in @i %i.
 Truncate Truncating UNLINKED Unable to resolve '%s' Unconnected @d @i %i (%p)
 Unexpected @b in @h %d (%q).
 Unhandled error code (0x%x)!
 Unknown extended option: %s
 Unknown pass?!? Unlink Updating inode references Usage: %s [-F] [-I inode_buffer_blocks] device
 Usage: %s [-RVadlv] [files...]
 Usage: %s [-r] [-t]
 Usage: %s disk
 Usage: e2label device [newlabel]
 Usage: fsck [-AMNPRTV] [ -C [ fd ] ] [-t fstype] [fs-options] [filesys ...]
 Usage: mklost+found
 Version of %s set as %lu
 WARNING: PROGRAMMING BUG IN E2FSCK!
	OR SOME BONEHEAD (YOU) IS CHECKING A MOUNTED (LIVE) FILESYSTEM.
@i_link_info[%i] is %N, @i.i_links_count is %Il.  They @s the same!
 WARNING: bad format on line %d of %s
 WARNING: couldn't open %s: %s
 WILL RECREATE Warning!  %s is mounted.
 Warning... %s for device %s exited with signal %d.
 Warning: %d-byte blocks too big for system (max %d), forced to continue
 Warning: Group %g's @S (%b) is bad.
 Warning: Group %g's copy of the @g descriptors has a bad @b (%b).
 Warning: blocksize %d not usable on most systems.
 Warning: could not erase sector %d: %s
 Warning: could not read @b %b of %s: %m
 Warning: could not read block 0: %s
 Warning: could not write @b %b for %s: %m
 Warning: illegal block %u found in bad block inode.  Cleared.
 Warning: label too long, truncating.
 Warning: skipping journal recovery because doing a read-only filesystem check.
 Warning: the backup superblock/group descriptors at block %u contain
	bad blocks.

 Weird value (%ld) in do_read
 While reading flags on %s While reading version on %s Writing inode tables:  Writing superblocks and filesystem accounting information:  You can remove this @b from the bad @b list and hope
that the @b is really OK.  But there are no guarantees.

 You must have %s access to the filesystem or be root
 Zeroing journal device:  aborted aextended attribute bad error behavior - %s bad gid/group name - %s bad inode map bad interval - %s bad mounts count - %s bad num inodes - %s bad reserved block ratio - %s bad reserved blocks count - %s bad revision level - %s bad uid/user name - %s bad version - %s
 badblocks forced anyway.
 badblocks forced anyway.  Hope /etc/mtab is incorrect.
 bblock block bitmap block device blocks per group count out of range blocks per group must be multiple of 8 blocks to be moved can't allocate memory for test_pattern - %s cancelled!
 ccompress character device check aborted.
 ddirectory directory directory inode map done
 done

 done                            
 during ext2fs_sync_device during seek during test data write, block %lu e2fsck_read_bitmaps: illegal bitmap block(s) for %s e2label: cannot open %s
 e2label: cannot seek to superblock
 e2label: cannot seek to superblock again
 e2label: error reading superblock
 e2label: error writing superblock
 e2label: not an ext2 filesystem
 eentry elapsed time: %6.3f
 empty dir map empty dirblocks ext attr block map ext2fs_new_@b: %m while trying to create /@l @d
 ext2fs_new_@i: %m while trying to create /@l @d
 ext2fs_new_dir_@b: %m while creating new @d @b
 ext2fs_write_dir_@b: %m while writing the @d @b for /@l
 ffilesystem filesystem fsck: %s: not found
 fsck: cannot check %s: fsck.%s not found
 getting next inode from scan ggroup hHTREE @d @i i_blocks_hi @F %N, @s zero.
 i_dir_acl @F %Id, @s zero.
 i_faddr @F %IF, @s zero.
 i_file_acl @F %If, @s zero.
 i_frag @F %N, @s zero.
 i_fsize @F %N, @s zero.
 iinode imagic inode map in malloc for bad_blocks_filename in-use block map in-use inode map inode bitmap inode done bitmap inode in bad block map inode loop detection bitmap inode table inodes (%llu) must be less than %u internal error: couldn't lookup EA inode record for %u invalid block size - %s invalid inode ratio %s (min %d/max %d) invalid inode size %d (min %d/max %d) invalid inode size - %s invalid reserved blocks percent - %s it's not safe to run badblocks!
 jjournal journal llost+found meta-data blocks mke2fs forced anyway.
 mke2fs forced anyway.  Hope /etc/mtab is incorrect.
 mmultiply-claimed multiply claimed block map multiply claimed inode map nN named pipe need terminal for interactive repairs ninvalid no no
 oorphaned opening inode scan pproblem in reading directory block reading indirect blocks of inode %u reading inode and block bitmaps reading journal superblock
 regular file regular file inode map reserved blocks reserved online resize blocks not supported on non-sparse filesystem returned from clone_file_block rroot @i size of inode=%d
 socket sshould be symbolic link time: %5.2f/%5.2f/%5.2f
 too many inodes (%llu), raise inode ratio? too many inodes (%llu), specify < 2^32 inodes unable to set superblock flags on %s
 unknown file type with mode 0%o unknown os - %s uunattached vdevice while adding filesystem to journal on %s while adding to in-memory bad block list while allocating buffers while allocating zeroizing buffer while beginning bad block list iteration while calling ext2fs_block_iterate for inode %d while checking ext3 journal for %s while clearing journal inode while creating /lost+found while creating in-memory bad blocks list while creating root dir while determining whether %s is mounted. while doing inode scan while expanding /lost+found while getting next inode while getting stat information for %s while initializing journal superblock while looking up /lost+found while marking bad blocks as used while opening %s while opening %s for flushing while opening inode scan while printing bad block list while processing list of bad blocks from program while reading bitmaps while reading flags on %s while reading in list of bad blocks from file while reading journal inode while reading journal superblock while reading root inode while reading the bad blocks inode while recovering ext3 journal of %s while reserving blocks for online resize while resetting context while retrying to read bitmaps for %s while sanity checking the bad blocks inode while setting bad block inode while setting flags on %s while setting root inode ownership while setting up superblock while setting version on %s while starting inode scan while trying popen '%s' while trying to allocate filesystem tables while trying to determine device size while trying to determine filesystem size while trying to determine hardware sector size while trying to flush %s while trying to initialize program while trying to open %s while trying to open external journal while trying to open journal device %s
 while trying to re-open %s while trying to resize %s while trying to run '%s' while trying to stat %s while updating bad block inode while writing block bitmap while writing inode bitmap while writing inode table while writing journal inode while writing journal superblock while writing superblock will not make a %s here!
 yY yes yes
 zzero-length Project-Id-Version: e2fsprogs-1.40.8
Report-Msgid-Bugs-To: tytso@alum.mit.edu
POT-Creation-Date: 2015-05-17 20:31-0400
PO-Revision-Date: 2012-04-15 20:23+0000
Last-Translator: Dennis Baudys <Unknown>
Language-Team: German <translation-team-de@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 16:06+0000
X-Generator: Launchpad (build 18115)
 	%Q (@i #%i, mod time %IM)
 	<@f metadata>
 	Benutze %s
 	Benutze %s, %s
 
	beim Erstellen des Journals auf Gerät %s 
	beim Erstellen des Journals 
	beim Erstellen der Journaldatei 
	beim Öffnen des Journals auf %s
 

%s: UNERWARTETE INKONSISTENZ; fsck MANUELL AUSFÜHREN
	(d.h. ohne -a oder -p Option)
 
  Inode-Tabelle in  
  reservierte GDT Blöcke bei  
%s: %s: Fehler beim Lesen von Bitmaps: %s
 
%s: ***** DATEISYSTEM WURDE VERÄNDERT *****
 
%s: ********** WARNUNG: Noch Fehler im Dateisystem  **********

 
*** Journal wurde wiederhergestellt - Dateisystem ist nun wieder ext3 ***
 
Falsche erweiterte Optionen angegeben: %s

Erweiterte Optionen werden durch Kommatas getrennt. Manche erwarten ein
	Argument, welches mit Gleichheitszeichen (»=«) zugewiesen wird.

Gültige erweiterte Optionen sind:
	superblock=<Nummer des Spuperblocks>
	blocksize=<Blockgrösse>
 
Notfallhilfe:
 -p        automatische Reparatur (keine Fragen)
 -n        keine Veränderungen am Dateisystem vornehmen
 -y        alle Fragen mit »Ja« beantworten
 -c        defekte Blöcke suchen
 -f        Überprüfung auch dann erzwingen, wenn alles i.O. scheint
 
Das Dateisystem ist zu klein für ein Journal
 
Wenn der @b wirklich defekt ist, kann der @f nicht repariert werden.
 
Unterbrochen, räume auf
 
Ungültiges nicht-numerisches Argument für -%c ("%s")

 
Journal Blockgröße:       %u
Journal Länge:            %u
Journal Startblock:       %u
Journal Sequenz:          0x%08x
Journal Start:            %u
Journal Anzahl Nutzer:    %u
 
Das Journal ist zu groß für dieses Dateisystem.
 
Doppelter @bs gefunden... starte Scan nach doppelten @b.
Durchgang 1B: Suche nach doppelten/defekten @bs
 
Kennzeichen für verteilten Superblock gesetzt. %s 
Defekter @b @i ist wahrscheinlich beschädigt worden. Sie sollten
nun eher innehalten und »e2fsck -c« ausführen, um nach defekten
Blöcken in @f zu suchen.
 
Das Gerät existiert offensichtlich nicht; haben Sie es richtig angegeben?
 
Das Dateisystem hat bereits verteilte Superblöcke.
 
Die angegebene Journalgröße beträgt %d Blöcke. Sie muss aber zwischen
1024 und 10240000 Dateisystem-Blöcken liegen. Abbruch!
 
Warnung: Probleme beim Schreiben der Superblöcke. 
Warnung: RAID Stripe-Breite %u ist kein ganzzahliges Vielfaches von
	Stride %u.

   %s Superblock in    Block bitmap in    Freie Blöcke:    Freie Inodes:   (Prüfung nach nächstem Einhängen)  (Prüfung nach nächstem Einhängen)  (Prüfung nach %ld Einhängevorgängen)  (j/n)  -v                   ausführliche Meldungen
 -b Superblock        Superblockkopie verwenden
 -B Blockgröße        Blockgröße beim Suchen vom Superblock erzwingen
 -j externes-Journal  Angabe des Speicherortes des externen Journals
 -l bad_blocks_file   zur Liste der defekten Blöcke hinzufügen
 -L bad_blocks_file   Liste der defekten Blöcke definieren
  Erledigt.
  , Gruppendeskriptor in   enthält ein fehlerhaftes Dateisystem  wurde %u mal ohne Überprüfung eingehängt  wurde %u Tage ohne Überprüfung genutzt  Eigenschaften des primären Superblocks unterscheiden sich vom Backup  wurde nicht ordnungsgemäß ausgehängt # Extent dump:
 %d-Byte Blöcke zu groß für das System (max %d) %s %s: Status ist %x, sollte nie vorkommen.
 %s @o @i %i (uid=%Iu, gid=%Ig, mode=%Im, size=%Is)
 %s besitzt nicht unterstützte Eigenschaft(en): %s wird offensichtlich vom System genutzt;  %s ist das ganze Gerät, nicht nur eine Partition!
 %s ist eingehängt;  %s ist kein spezielles Block-Gerät.
 %s ist kein Journal-Gerät.
 %s: %s Dateiname nblocks Blockgröße
 %s: %s versuche es mit Backup-Blöcken...
 %s: ***** LINUX MUSS NEU GESTARTET WERDEN *****
 %s: Fehler %d bei Ausführung von fsck.%s für %s
 %s: Die Optionen -n und -w schliessen sich gegenseitig aus.

 %s: e2fsck abgebrochen.
 %s: Das Journal ist zu kurz
 %s: keinen gültigen Journal-Superblock gefunden
 %s: stelle das Journal wieder her
 %s: überspringe die ungültige Zeile in /etc/fstab: bind mount mit
 Durchgangsnummer für fsck, die nicht Null ist
 %s: zu viele Argumente
 %s: zu viele Geräte
 %s: wait: kein Kindprozess mehr?!?
 %s: Das Journal ist nur lesbar - Keine Wiederherstellung
 %s? nein

 %s? ja

 %u Blockgruppe
 %u Blockgruppen
 %u Blöcke pro Gruppe, %u Fragmente pro Gruppe
 %u Inodes pro Gruppe
 %u Inodes untersucht.
 ».« @d @e in @d @i %i ist nicht NULL-terminiert
 »..« @d @e in @d @i %i ist nicht NULL-terminiert
 »..« in %Q (%i) ist %P (%j), @s %q (%d).
 (NICHTS) (es gibt %N @is, die doppelte/defekte @bs enthalten.)

 (nicht interaktiv) , Gruppendeskriptoren in  , Inode Bitmap in  , Prüfung erzwungen.
 --warten-- (Durchgang %d)
 -O darf nur eimal angegeben werden -o darf nur eimal angegeben werden /@l ist kein @d (ino=%i)
 /@l nicht gefunden.   <Der reservierte Inode 10> <Der reservierte Inode 9> <Der NULL Inode> <Der »Bad Blocks«-Inode> <Der »Boot Loader«-Inode> <Des »group descriptor«-Inode> <Der Journal-Inode> <Der »undelete directory«-Inode> <n> <j> = ist inkompatibel mit - und +
 @A %N grenzt an @b(s) in @b @g %g for %s: %m
 @A @a @b %b.   @A @b @B (%N): %m
 @A @b Puffer zum Verschieben %s
 @A @d @b array: %m
 @A @i @B (%N): %m
 @A @i @B (inode_dup_map): %m
 @A icount link information: %m
 @A icount structure: %m
 @A neu @d @b for @i %i (%s): %m
 @A icount structure: %m
 @D @i %i hat "zero dtime".   @E @L nach ».«   @E @L nach @d %P (%Di).
 @E @L zur @r.
 @E hat @D/unbenutzt @i %Di.   @E hat ungültigen @i-Nummer: %Di.
 @E hat einen Namen der Länge Null.
 @E hat keinen eindeutigen Dateinamen.
Wird in %s umbenannt. @E hat einen falschen Dateityp (war %Dt, sollte %N sein).
 @E hat Dateityp gesetzt.
 @E hat ein unzulässiges Zeichen im Namen.
 @E hat rec_len von %Dr, sollte %N sein.
 @E ist ein doppelter ».« @e.
 @E ist ein doppelter »..« @e.
 @E zeigt auf @i (%Di) in einem defekten @b.
 @I @i %i in @o @i Liste.
 @I @o @i %i in @S.
 @S @b_size = %b, fragsize = %c.
Diese Version von e2fsck unterstützt keine von @b-Größen verschiedene Fragmentgrößen.
 @S @bs_per_group = %b, sollte %c sein.
 @S first_data_@b = %b, sollte %c haben.
 @S Hinweis für externen Superblock @s %X   @a @b %b hat h_blocks > 1.   @a @b %b ist defekt (ungültiger Name).   @a @b %b ist defekt (ungültiger Wert).   @a @b %b ist defekt (allocation collision).   @a @b @F ist ungültig (%If).
 @h %i hat eine zu große Verzeichnistiefe von (%N)
 @h %i hat eine zu große Verzeichnistiefe von (%N)
 @h %i hat eine zu große Verzeichnistiefe von (%N)
 @h %i hat eine zu große Verzeichnistiefe von (%N)
 @b @B differieren:  @b @B für @g %g ist nicht in @g.  (@b %b)
 @f enthält große Dateien, aber das LARGE_FILE Flag in @S ist nicht gesetzt.
 @f hat keinen UUID ; generiere einen.

 Für @f ist resize_@i nicht eingeschaltet, aber s_reserved_gdt_@bs
ist %N; @s Null.   @f hat Eigenschfts-Kennzeichen gesetzt, ist aber ein Revision 0 @f.   @g %g's @b @B (%b) ist ungültig.   @g %g's @b @B auf %b @C.
 @g %g's @i @B (%b) ist ungültig.   @g %g's @i @B auf %b @C.
 @g %g's @i Tabelle auf %b @C.
 @h %i hat eine zu große Verzeichnistiefe von (%N)
 @h %i hat einen unvollständigen root node.
 @h %i hat eine nicht unterstützte hash-Version (%N)
 @h %i benutzt einen nicht unterstützten htree root node flag.
 @i %i (%Q) hat einen ungültigen Modus (%Im).
 @i %i (%Q) ist ein @I @b @v.
 @i %i (%Q) ist ein @I FIFO.
 @i %i (%Q) ist ein @I Zeichen @v.
 @i %i (%Q) ist ein @I Socket.
 @i %i hat @cion Flag gesetzt auf @f ohne @cion Unterstützung.   @i %i hat INDEX_FL flag gesetzt, ist aber kein @d.
 @i %i hat INDEX_FL Flag auf @f gesetzt ohne HTREE-Unterstützung.
 @i %i hat defekten @a @b %b.   @h %i hat eine zu große Verzeichnistiefe von (%N)
 @i %i hat unzulässigen @b(s).   @i %i hat Imagic-Flag gesetzt.   @i ist ein %It aber es sieht so aus, als ob es tatsächlich ein Verzeichnis ist.
 @i %i ist ein @z @d.   @i %i ist in Benutzung, aber hat dtime gesetzt.   @i %i ist zu groß.   @i %i Referenzzähler ist %Il, @s %N.   @i %i war Teil der orphaned @i Liste.   @i %i, i_@bs ist %Ib, @s %N.   @i %i, i_size ist %Is, @s %N.   @i @B differieren:  @i @B für @g %g ist nicht in @g.  (@b %b)
 @i-Anzahl in @S ist %i, sollte %j sein.
 @i Tabelle für @g %g ist nicht in @g.  (@b %b)
WARNUNG: GROSSER DATENVERLUST IST MÖGLICH.
 @is,  die Teile einer defekten Liste mit verwaisten Links waren.   @i %i ist in Benutzung, aber hat dtime gesetzt.   @j ist keine reguläre Datei.   @j Version wird von diesem e2fsck nicht unterstützt.
 @m @b(s) in @i %i: Duplizierte @bs bereits neu zugeordnet bzw. geklont.

 Invalid @h %d (%q).   @n @i-Nummer für ».« in @d-@i %i.
 @p @h %d (%q): bad @b number %b.
 @p @h %d: root node is invalid
 @r hat dtime gesetzt (vielleicht durch ein zu altes mke2fs).   @r ist kein @d.   @r ist kein @d; breche ab.
 @r nicht zugeordnet.   @u @i %i
 @I @o @i %i in @S.   ABGEBROCHEN FREIGEGEBEN Abbrechen Breche ab...
 Füge Verzeinishash-Hilfe zu @f hinzu.

 Erstelle Journal auf Gerät %s:  AFehler beim Zuweisen Gebe frei BLKFLSBUF ioctl nicht unterstützt!  Kann Puffer nicht leeren.
 Sichere @j @i @b Information.

 Backup Bad @b %b benutzt als bad @b @i indirekt @b.   Defekter @b @i hat einen indirekten @b (%b), der mit
den @f Metadaten in Konflikt steht.   Bad @b @i hat unzulässigen @b(s).   Bad block %u außerhalb des gültigen Bereichs; ignoriert.
 Bad Blocks: %u Falscher oder fehlender /@l.  Wiederverbinden nicht möglich.
 BBitmap Start von Durchgang %d (max = %lu)
 Block %b im primären Deskriptor @g ist auf der bad @b Liste
 Block %d im primären Superblock/Gruppendeskriptorbereich defekt.
 Blockgröße=%u (log=%u)
 Blöcke %u bis einschließlich %u müssen i.O. sein, um ein Dateisystem zu erstellen.
 BEREINIGT SETZE FORT ANGELEGT Es kann nicht fortgefahren werden. Kann kein externes @j finden.
 Fortsetzung nicht möglich, breche ab.

 Kann ohne @r nicht fortsetzen.
 CKonflikte mit anderen Dateisystemen @b Überprüfe alle Dateisysteme.
 Prüfe von Block %lu bis %lu
 Suche nach defekten Blöcken (zerstörungsfreier Lesen+Schreiben-Modus)
 Suche nach defekten Blöcken (Nur-Lesen-Modus):  Suche nach defekten Blöcken im zerstörungsfreien Lesen+Schreiben-Modus
 Suche nach defekten Blöcken im Nur-Lesen-Modus
 Suche nach defekten Blöcken (Lesen+Schreiben-Modus)
 Bereinige Bereinige @j Bereinige HTree-Index Bereinige Inode Bereinige Das Zurücksetzen von Dateisystem-Eigenschaft »%s« wird nicht unterstützt.
 multiply claimed block map Verbinde nach /lost+found Fortsetzen Beschädigung gefunden in @S.  (%s = %N).
 Erweitern nicht möglich /@l: %m
 Wiederverbinden nicht möglich %i: %m
 Könnte es eine Partion der Länge Null sein?
 Konnte keinen Blockpuffer (Größe=%d) reservieren.
 Header-Puffer konnte nicht zugewiesen werden
 Kann keinen Speicher für Dateisystemtypen reservieren.
 Konnte keinen Speicher zur Analyse der Journal-Optionen anfordern!
 Speicher zum Parsen der Optionen konnte nicht reserviert werden!
 Konnte Pfad-Variable in chattr_dir_proc nicht reservieren Kann die Datei %m nicht klonen.
 Größe des Gerätes ist nicht feststellbar. Sie müssen sie manuell angeben.
 Konnte die Gerätegröße nicht ermitteln. Geben
Sie die Größe des Dateisystems an
 Konnte die magische Nummer des Journal-Superblocks nicht finden Kann keinen gültigen Dateisystem-Superblock finden.
 Konnte Parent von @i %i: %m nicht reparieren

 Parent von @i %i konnte nicht repariert werden: 
parent @d nicht gefunden.

 Konnte Datum/Zeit nicht parsen: %s Erstelle Erstelle Journal (%d Blöcke):  E5Arstelle Journal-Inode:  Erstelle Journal auf Gerät %s:  Dgelöscht Lösche Datei Zurückgegebene Gerätegröße ist gleich null. Ungültige Partition
	angegeben oder die Partitionstabelle wurde nach einem fdisk-Lauf
	nicht wieder eingelesen, weil eine veränderte Partition in Gebrauch
	und gesperrt ist. Sie könnten gezwungen sein, neu zu booten, um die
	Partitionstabelle neu einzulesen.
 Verzeichnisanzahl ist falsch für @g #%g (%i, counted=%j).
 Laufwerk ist schreibgeschützt, nutzen Sie die -n Option
um es im Nur-Lesen-Modus zu prüfen.
 Wirklich fortfahren Doppelte @E gefunden.   Doppelte @e '%Dn' gefunden.
	Markiere %p (%i) für die Neuerstellung.

 Doppelter oder unzulässiger @b in Gebrauch!
 2FSCK_JBD_DEBUG "%s" ist keine Ganzzahl

 E@e »%Dn« in %p (%i) Fehler: Kann /dev/null (%s) nicht öffnen
 ERWEITERT Bei -t müssen entweder allen oder keinem Dateisystem ein »no« bzw. »!«
vorangestellt werden.
 leerer Verzeichnisblock %u (#%d) im Inode %u
 Fehler bei der Anpassung des refcount für @a @b %b (@i %i): %m
 Fehler beim hineinkopieren von @b @B: %m
 Fehler beim hineinkopieren von @i @B: %m
 Fehler beim Erzeugen des /@l @d (%s): %m
 Fehler beim Erzeugen des root @d (%s): %m
 Fehler bei der Freigabe von @i %i: %m
 Fehler bei der Feststellung der Größe des physikalischen @v: %m
 Fehler beim Durchlaufen der @d @bs: %m
 Fehler beim Verschieben von  @j: %m

 Fehler beim Lesen @a @b %b (%m).   Fehler beim Lesen @a @b %b for @i %i.   Lesefehler @d @b %b (@i %i): %m
 Fehler beim Lesen von @i %i: %m
 Lesefehler - Block %lu (%s) während %s.   Lesefehler - Block %lu (%s).   Fehler beim Speichern @d @b Informationen (@i=%i, @b=%b, num=%N): %m
 Fehler beim Speichern von @i count Informationen (@i=%i, count=%N): %m
 Fehler bei Überprüfung des Datei-Deskriptors %d: %s
 Fehler während der Einstellung von @i count auf @i %i
 Fehler beim Durchlaufen der @bs in @i %i (%s): %m
 Fehler beim Iterieren über @bs in @i %i: %m
 Fehler während der Suche @is (%i): %m
 Fehlen beim Prüfen von Inodes (%i): %m
 Fehler während der Suche nach /@l: %m
 Fehler beim Schreiben @a @b %b (%m).   Schreibfehler @d @b %b (@i %i): %m
 Schreibfehler - Block %lu (%s) während %s.   Schreibfehler - Block %lu (%s).   Fehler: ext2fs-Bibliotheks-Version ist zu alt!
 Erweitere Vergrößere die Inode-Tabelle Externes @j unterstützt nicht @f
 Externes @j hat ungültigen @S
 Externes @j hat mehrere @f Nutzer (nicht unterstützt).
 DATEI GELÖSCHT REPARIERT Ffür @i %i (%Q) ist Dateisystem-Eigenschaften werden für Dateisysteme der Revision 0 nicht
	unterstützt
 Dateisystem-Label=%s
 Dateisystem ist größer als augenscheinlich das Gerät selbst. Ist das Dateisystem eingehängt or exklusiv von einem anderen Programm

geöffnet worden?
 UUID des Dateisystems auf Journal-Gerät nicht gefunden.
 Beendet mit %s (exit status %d)
 Erster @e »%Dn« (inode=%Di) in @d @i %i (%p) @s ».«
 Erster Datenblock=%u
 Repariere Flags von %s wie folgt gesetzt:  Rückschreiben erzwingen Fand ungültige V2 @j @S Felder (vom V1 Journal).
Bereinige die Felder hinter V1 @j @S...

 Fragmentgröße=%u (log=%u)
 Freie @bs Anzahl ist falsch (%b, counted=%c).
 Freie @bs Anzahl ist falsch @g #%g (%b, counted=%c).
 Freie @is Anzahl ist falsch (%i, counted=%j).
 Freie @is Anzahl ist falsch für @g #%g (%i, counted=%j).
 Von Block %lu bis %lu
 Neuere Version von e2fsck benötigt! Gruppe %lu: (Blöcke  Gruppen-Deskriptoren scheinen defekt zu sein... HTREE INDEX BEREINIGT IGNORIERT INODE BEREINIGT Ignoriere Fehler Inicht zulässig unzulässige Angabe für Blöcke pro Gruppe Blockanzahl nicht zulässig!
 Interner Fehler: kann dir_info für %i nicht finden.
 Interner Fehler: fudging end of bitmap (%N)
 Invalid EA version.
 Ungültiger RAID Stride: %s
 Ungültige RAID Stripe-Breite: %s
 Ungültiges UUID Format
 Ungültiger Blockgrössen-Parameter: %s
 Ungültiger »completion information«-Datei-Deskriptor Ungültige Dateisystem-Option gesetzt: %s
 Ungültige Einhänge-Option gesetzt: %s
 Ungültiger "resize"-Parameter: %s
 Ungültige Stride-Länge Ungültiger "stride"-Parameter: %s
 Ungültiger Stripebreite-Parameter: %s
 Ungültiger Superblock-Parameter: %s
 Journal-Device Blockgröße (%d) kleiner als Minimum-Blockgröße %d
 Journal gelöscht
 Journalgrösse:              Journal-Superblock nicht gefunden!
 Jounalnutzer:            %s
 Journale werden für Dateisysteme der Revision 0 nicht unterstützt
 List ein Link DOPPELTE/DEFEKTE BLÖCKE DUPLIZIERT Maximale Dateisystem-Blöcke=%lu
 Es darf im Nur-Lesen-Modus nur ein Testmuster angegeben werden benutzter Speicher: %d, vergangende Zeit: %6.3f/%6.3f/%6.3f
 Fehlende ».« in @d @i %i.
 Fehlende »..« in @d @i %i.
 Verschiebe @j von /%s zum versteckten Inode.

 Verschiebe die Inode-Tabelle Benutze »-v«, =, - oder +
 Kein Platz in @l @d.   Hinweis: Wenn mehrere Inodes oder Bitmap-Blöcke
neu geordnet werden müssen, oder ein Teil der Inode-Tabelle
verschoben werden muss, könnte es helfen, e2fsck erst einmal
mit der Option »-b %S« zu starten. Das Problem könnte
im primären Blockgruppenbezeichner liegen, und seine
Sicherungskopie in Ordnung sein.

 Online-Grössenänderungen werden bei Revison 0 Dateisystemen nicht
	unterstützt
 Nur eine der Optionen -p/-a, -n oder -y darf angegeben werden. Optimiere Verzeichnisse:  Speicher voll beim Löschen der Sektoren %d-%d
 PROGRAMMIERFEHLER: @f (#%N) @B Endpunkte (%b, %c) passen nicht zu den berechneten @B Endpunkten (%i, %j)
 Auffüllbyte am Ende von @b @B ist nicht gesetzt.  Auffüllbyte am Ende von @i @B ist nicht gesetzt.  Durchgang 1 Durchgang 1: Prüfe @is, @bs, und Größen
 Durchgang 1C: Prüfe Verzeichnisse nach @is mit doppelten @bs.
 Durchgang 1D: Gleiche doppelte @bs ab
 Durchgang 2 Durchgang 2: Prüfe @d Struktur
 Durchgang 3 Durchgang 3: Prüfe @d Verknüpfungen
 Durchgang 3A: Optimiere Verzeichnisse
 Durchgang 4 Durchgang 4: Überprüfe die Referenzzähler
 Durchgang 5 Durchgang 5: Überprüfe @g Zusammenfassung
 Peak-Memory Bitte zuerst »e2fsck -f %s« laufen lassen.

 Bitte e2fsck über das Dateisystem laufen lassen.
 Möglicherweise ist die Partition nicht vorhanden oder eine Swap-Partition?
 Primary Trotzdem fortsetzen? (j,n)  Programmfehler?  @b #%b verlangt ohne Grund in process_bad_@b.
 WIEDER VERBUNDEN ZURÜCKGESETZT Zufälliges Testmuster ist im Nur-Lesen-Modus nicht erlaubt Lesen und Vergleichen:  Recovery-Kennzeichen in Backup @S nicht gesetzt, @j wird trotzdem gestartet.
 Zurücksetzen Zurücksetzen Verschiebe @g %g's %s von %b nach %c...
 Verschiebe @g %g's %s nach %c...
 Verteile die Blöcke neu Reservierte @i %i (%Q) hat einen ungültigen Modus.   Erzeugung von Vergrösserungs-@i scheiterte: %m. @r ist kein @d.   Resize_@i nicht aktiviert, aber die zu modifgizierende Inod ist nicht-Null.   Beginne e2fsck neu ...
 Starte @j trotzdem Führe aus: %s
 GERETTET ABGESPALTET UNTERDRÜCKT Rette Prüfe die Inode-Tabelle Zweiter @e »%Dn« (inode=%Di) in @d @i %i @s »..«
 Setze die derzeitige Mount-Anzahl auf %d
 Setze das Fehler-Verhalten auf %d
 Das Setzen der Dateisystem-Eigenschaft »%s« wird nicht unterstützt.
 Setze Dateitype für @E auf %N.
 Setze das Intervall zwischen Checks auf %lu Sekunden
 Setze die maximale Mount-Anzahl auf %d
 Setze die GID für reservierte Blöcke auf %lu
 Setze die UID für reservierte Blöcke auf %lu
 Setze Stride-Größe auf %d
 Setze die Stripe-Breite auf %d
 Setze die Zeit des letzten Dateisystemchecks auf %s
 Das sollte niemals passieren: Die zu verändernde Inode ist defekt!
 Verteilte Superblöcke werden für Dateisysteme der Revision 0 nicht
	unterstützt
 Spezielle (@v/socket/fifo/symlink) Datei (@i %i) hat immutable
oder append-only Flag gesetzt.   Spezielle (@v/socket/fifo/symlink) Datei (@i %i) hat immutable
oder append-only Flag gesetzt.   Aufsplitten SSuper@b Superblock-Sicherungskopien gespeichert in den Blöcken:  Superblock ungültig Ausgaben unterdrücken Symlink %Q (@i #%i) is ungültig.
 Syntaxfehler in der Konfigurationsdatei von e2fsck (%s, Zeile %d)
	%s
 Syntax Fehler in der Konfigurationsdatei von mkefs (%s, Zeile %d)
	%s
 BEENDET Teste mit Muster 0x Teste mit zufälligen Mustern:  Die -c und -l/-L Optionen dürfen nicht gleichzeitig verwendet werden.
 Die -t Option wird von dieser e2fsck-Version nicht unterstützt.
 Die @f Größe ( laut @S) ist %b @bs
Die physikalische Größe von @v ist %c @bs
Entweder der @S oder die Partionstabelle ist beschädigt!
 Hurd unterstützt das Dateityp-Feature nicht.
 Das Dateisystem hat schon ein Journal.
 Diese Dateisystem-Revision ist offensichtlich zu neu für diese Version 
von e2fsck (oder der Dateisystem-Superblock ist defekt).

 Das needs_recovery Flag ist gesetzt. Bitte starten sie e2fsck vor
der Zurücksetzung des has_journal Flags.
 Der primäre @S (%b) ist auf der bad @b Liste.
 Das Maximum der Vergrösserung muss oberhalb als der Dateisystem-Grösse liegen.
 Die Leistungsmerkmale resize_inode und meta_bg sind nicht kompatibel.
Sie können nicht beide gleichzeitig aktiviert sein.
 Das verheißt nichts gutes, aber wir versuchen es trotzdem ..
 Das Dateisystem wird automatisch nach jeweils %d Einhäng-Vorgängen bzw.
alle %g Tage überprüft, je nachdem, was zuerst eintritt. Veränderbar mit
tune2fs -c oder -t .
 Zu viele unzulässige @bs in @i %i.
 Verkürze Kürze GETRENNT Nicht möglich »%s« aufzulösen Nicht verbundene @d @i %i (%p)
 Unerwarteter @b in @h %d (%q).
 Unbenutzter Fehlercode (0x%x)!
 Unbekannte erweiterte Option: %s
 Unbekannter Durchgang?!? Unlink Aktualisiere die Inode-Referenzen Aufruf: %s [-F] [-I inode_buffer_blocks] Gerät
 Aufruf: %s [-RVadlv] [Dateien...]
 Aufruf: %s [-r] [-t]
 Aufruf: %s Laufwerk
 Aufruf: e2label Gerät [neuer_Name]
 Aufruf: fsck [-AMNPRTV] [ -C [ fd ] ] [-t Datesystemtyp] [FS-Optionen] [Dateisystem...]
 Aufruf: mklost+found
 Version von %s gesetzt auf %lu
 WARNUNG: PROGRAMMIERFEHLER IN E2FSCK!
	ODER EIN TROTTEL (SIE) PRÜFT EIN EINGEHÄNGTES (LIVE) DATEISYSTEM.
@i_link_info[%i] ist %N, @i.i_links_count ist %Il.  Sie sollten identisch sein!
 WARNUNG: falsches Format in Zeile %d von %s
 WARNUNG: Konnte %s nicht öffnen: %s
 ANGELEGT Warnung!  %s ist eingehängt.
 Warnung... %s für Gerät %s wurde mit Signal %d beendet.
 Warnung: %d-byte Blöcke zu groß für das System (max %d), fahre dennoch fort
 Warnung: Gruppe %g's @S (%b) ist ungültig.
 Warnung: Gruppe %g's Kopie vom Deskriptor @g hat einen bad @b (%b).
 Warnung: Blockgröße %d ist auf den meisten Systemen unbrauchbar.
 Warnung: konnte Sektor %d: %s nicht löschen
 Warnung: kann  @b %b von %s: %m nicht lesen
 Warnung: konnte Block %s nicht lesen
 Warnung: kann  @b %b von %s: %m nicht schreiben
 Warnung: Nicht zulässiger Block %u im »Bad Blocks«-Inode gefunden! Bereinigt.
 Warnung: Name zu lang, kürze ihn.
 Warnung: Überspringe Journal-Wiederherstellung, da das Dateisystem im Nur-Lesen-Modus ist.
 Warnung: die Sicherung des Superblock bzw. Gruppendeskriptors in Block %u enthält
	defekte Blöcke.

 Merkwürdiger Wert (%ld) in do_read
 Beim Lesen der Flags von %s Beim Lesen der Version von %s Schreibe Inode-Tabellen:  Schreibe Superblöcke und Dateisystem-Accountinginformationen:  Sie können @b von der @b - Liste  löschen  
und hoffen das @b wirklich in Ordnung ist, es 
gibt aber KEINE GARANTIEN.

 Sie benötigen %s- oder root-Rechte für das Dateisystem.
 Überschreibe Journal-Device mit Nullen:  abgebrochen aerweiterte Eigenschaft ungültiges Fehler-Verhalten - %s ungültige(r) GID oder Gruppenname - %s fehlerhafte Inode-Liste ungültiges Intervall - %s Ungültiger Mounts-Zähler - %s ungültiges Intervall - %s ungültiges Reservierte-Blöcke-Verhältnis - %s ungültige Anzahl von reservierten Blöcken - %s ungültiges Intervall - %s ungültige(r) UID/Benutzername - %s falsche Version - %s
 Badblocks wird trotzdem erzwungen.
 Badblocks wird trotzdem erzwungen. Hoffentlich ist /etc/mtab nicht korrekt.
 bBlock block bitmap Blockgerät Anzahl der Blöcke pro Gruppe ausserhaalb des gültigen Bereichs Anzahl der Blöcke pro Gruppe muss ein Vielfaches von 8 sein zu verschiebende Blöcke Kann keinen Speicher für Testmuster reservieren - %s abgebrochen!
 ckomprimieren zeichenorientiertes Gerät Prüfung abgebrochen.
 dVerzeichnis Verzeichnis »directory inode«-Liste erledigt
 erledigt

 erledigt                            
 während ext2fs_sync_device beim Suchen beim Schreiben der Test-Daten; Block %lu e2fsck_read_bitmaps: illegal bitmap block(s) für %s e2label: Kann %s nicht öffnen.
 e2label: cannot seek to superblock
 e2label: cannot seek to superblock again
 e2label: Lesefehler im Superblock
 e2label: Fehler beim Schreiben des Superblocks
 e2label: Kein ext2 Dateisystem
 eEintrag abgelaufende Zeit: %6.3f
 leere Verzeichnisliste leere Verzeichnisblöcke ext attr block map ext2fs_new_@b: %m während des Versuches /@l @d zu erzeugen.
 ext2fs_new_@i: %m während des Versuches /@l @d zu erzeugen.
 ext2fs_new_dir_@b: %m während des Versuches /@l @d zu erzeugen.
 ext2fs_write_dir_@b: %m während des Schreibens von @d @b für /@l
 fDateisystem Dateisystem fsck: %s: nicht gefunden
 fsck: kann %s nicht überprüfen: fsck.%s nicht gefunden
 beim Lesen des nächsten Inodes gGruppe hHTREE @d @i i_blocks_hi @F %N, @s zero.
 i_dir_acl @F %Id, @s null.
 i_faddr @F %IF, @s null.
 i_file_acl @F %If, @s null.
 i_frag @F %N, @s null.
 i_fsize @F %N, @s null.
 iInode i»magic inode«-Liste in malloc for bad_blocks_filename »in-use block«-Liste »in-use inode«-Liste inode bitmap »inode done«-Bitmap Inode in »Bad Blocks«-Liste »inode loop detection«-Bitmap Inode-Tabelle Die Anzahl der Indoes (%llu) muss unter %u liegen Interner Fehler: EA Inodeliste für %u wurde nicht gefunden ungültige Blockgröße - %s Unzulässiges Inode-Verhältnis %s (Min %d/Max %d ungültige Inode-Größe %d (min %d / max %d) Unzulässige Inode-Größe - %s Unzulässige "Reservierte Blöcke"-Prozentangabe - %s es ist zu unsicher, Badblocks zu starten!
 jJournal Journal llost+found Metadaten-Blöcke mke2fs wird sowieso erzwungen.
 mke2fs trotzdem erzwungen. Hoffentlich ist /etc/mtab ungültig.
 mmehrfach beanspruchte multiply claimed block map mehrfach beanspruchte Inode-Liste nN named pipe Benötige ein Terminal für interaktive Reparaturen nungültig nein nein
 overwaist Starte Inode-Scan pProblem in lese Verzeichnisblock lese indirekte Blöcke von Inode %u lese Inode und Block bitmaps Lese Journal-Superblock
 'reguläre Datei »regular file inode«-Liste reservierte Blöcke Für Online-Grössenänderungen reservierte Blöcke werden auf Dateisystemen
	ohne Unterstützung für Lückenkompression  nicht unterstützt zurückgegeben von clone_file_block rRoot @i Größe des Inode=%d
 Socket ssollte sein symbolische Verknüpfung Zeit: %5.2f/%5.2f/%5.2f
 zu viele Inodes (%llu), Inode-Verhältnis erhöhen? zu viele Inodes (%llu), sie müssen weniger als 2^32 Inodes angeben Superblock-Flags konntan auf %s nicht gesetzt werden
 unbekannter Dateityp mit Modus 0%o unbekanntes OS - %s unicht verbunden vGerät beim Hinzufügen des Dateisystems zum Journal auf %s füge zur Bad-Block-Liste im Speicher hinzu beim Zuweisen von Puffern beim reservieren eines Puffers zum Nullen beim Beginn des »Bad Block«-Listendurchlaufs während des Aufrufs von ext2fs_block_iterate für Inode %d während der Prüfung des ext3-Journals für %s beim Bereinigen des Journal-Inodes beim Erstellen von /lost+found erstelle Bad-Block-Liste im Speicher beim Erstellen des Wurzelverzeichnisses bei der Prüfung, ob %s eingehängt ist. während der Inodeprüfung beim Expandieren von /lost+found beim Laden des nächsten Inodes beim Ermitteln der Statusinformation für %s beim Initialisieren des Journal-Superblocks beim Suchen von /lost+found beim Markieren von defekten Blöcken als »belegt« beim Öffnen von %s beim Öffnen von %s für die Puffer-Leerung. beim Start des Inode-Scans beim Ausgeben der »Bad Block«-Liste beim Auswerten der »Bad Block«-Liste vom Programm beim Lesen von Bitmaps beim Lesens der Flags in %s beim Lesen der »Bad Block«-Liste aus der Datei beim Lesen des Journal-Inodes beim Lesen des Journal-Superblocks beim Lesen des Root-Inode während des Lesens des »Bad Block«-Inodes bei der Wiederherstellung des ext3-Journals von %s beim Lesen des Bad-Block-Inodes beim Rücksetzen des Kontexts während des wiederholten Versuches, Bitmaps für %s einzulesen während der logischen Prüfung des »Bad Block«-Inodes beim Setzen des »Bad Block«-Inodes beim Setzen der Flags in %s beim Setzen des Root-Inode-Eigentümers beim Erstellen des Superblocks beim Setzen der Version in %s beim Starten der Inodeprüfung beim Versuch, »%s« mittels »popen« zu öffnen beim Zuordnen von Dateisystemtabellen beim Versuch, die Gerätegröße festzustellen beim Bestimmen der Dateisystemgröße beim Ermitteln der Hardware-Sektorgröße während des Rückschreibeversuches auf %s bei der Programminitialisierung beim Versuch, %s zu öffnen beim Öffnen des externen Journals beim Versuch, das Journal-Device %s zu öffnen
 beim Versuch, %s erneut zu öffnen beim Versuch, die Größe von %s zu ändern während des Versuchs, »%s« auszuführen beim Auslesen des Status von %s beim Updaten des »Bad Block«-Inodes beim Schreiben der Block-Bitmap beim Schreiben der Inode-Bitmap beim Schreiben der Inode-Tabelle beim Schreiben des Journal-Inodes beim Schreiben des Journal-Superblocks beim Schreiben des Superblocks werde dort kein %s erstellen!
 jJ ja ja
 zNull-Länge 