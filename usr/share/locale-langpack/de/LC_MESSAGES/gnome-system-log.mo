��    M      �  g   �      �     �     �     �     �     �     �     �     �  #         $  "   >     a     m  E   u     �  -   �     �  )     -   1     _  #   k     �     �  	   �     �     �     �  
   �      	     	     ?	     Q	     b	     n	     w	  5   	     �	     �	  !   �	     �	  
   
     
      %
  (   F
  k   o
  =   �
  �     �   �  <   <  
   y  5   �  &   �  �   �  �   �  9   �      �  "        9  3   A  �   u     B     J     S     \     a     i     p     w     }     �     �     �     �     �     �  	   �    �  *   �          +     D     [  $   h  
   �     �  5   �     �  '   �          1  e   9     �  0   �     �  1   �  8        W  ,   d     �  	   �     �  ,   �     �  5   �     +  &   8  (   _     �  !   �     �     �     �  G   �     5     =  %   [     �     �     �  $   �  <   �  �   %  ;   �  �   �  �   �  <   B       6   �  6   �  !  �      W   9  *   �  -   �     �  -   �  �   $     �               &  
   3     >     J     Q     Z     i  
     +   �     �  �  �     �#     �#            %      *      "      3   J   >      ,       5   =           #       ?              '   F                (   +      8   0      
          G   M                        H   )          <   9   @      E   K   :   6   !           &              2      B   7                 1         ;                     L      .   	          I       -      $   A       /   4       D   C        A system log viewer for GNOME. About System Log Add new filter Auto Scroll Background: Can't read from "%s" Close Copy Could not open the following files: Could not parse arguments Could not register the application Edit filter Effect: Error while uncompressing the GZipped log. The file might be corrupt. Filter name is empty! Filter name may not contain the ':' character Filters Find next occurrence of the search string Find previous occurrence of the search string Foreground: Height of the main window in pixels Help Hide Highlight Impossible to open the file %s List of saved filters List of saved regexp filters Loading... Log file to open up on startup Log files to open up on startup Manage Filters... No matches found Normal Size Open Log Open... Please specify either foreground or background color! Quit Regular expression is empty! Regular expression is invalid: %s Search in "%s" Select All Show Matches Only Show the version of the program. Size of the font used to display the log Specifies a list of log files to open up at startup. A default list is created by reading /etc/syslog.conf. Specifies the height of the log viewer main window in pixels. Specifies the log file displayed at startup. The default is either /var/adm/messages or /var/log/messages, depending on your operating system. Specifies the size of the fixed-width font used to display the log in the main tree view. The default is taken from the default terminal font size. Specifies the width of the log viewer main window in pixels. System Log The file is not a regular file or is not a text file. There was an error displaying help: %s This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version. This version of System Log does not support GZipped logs. View or monitor system log files Width of the main window in pixels Wrapped You don't have enough permissions to read the file. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA Zoom In Zoom Out [URI...] _Add _Cancel _Close _Name: _Open _Properties _Regular Expression: _Remove logs;debug;error; today translator-credits updated yesterday Project-Id-Version: gnome-utils master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-system-log&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:33+0000
PO-Revision-Date: 2014-07-14 12:52+0000
Last-Translator: Phillip Sz <Unknown>
Language-Team: Deutsch <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:16+0000
X-Generator: Launchpad (build 18115)
Language: de_DE
 Ein Systemprotokoll-Betrachter für GNOME. Info zu Systemprotokoll Neuen Filter hinzufügen Automatischer Bildlauf Hintergrund: Von »%s« kann nicht gelesen werden Schließen Kopieren Die folgenden Dateien konnten nicht geöffnet werden: Konnte Argumente nicht einlesen Konnte die Anwendung nicht registrieren Filter bearbeiten Effekt: Fehler beim Dekomprimieren des mit GZip komprimierten Protokolls. Die Datei könnte beschädigt sein. Filtername ist leer. Dateiname darf nicht das Zeichen »:« enthalten Filter Nächste Übereinstimmung mit dem Suchtext finden Vorhergehende Übereinstimmungen mit dem Suchtext finden Vordergrund: Höhe des Systemprotokoll-Fensters in Pixeln Hilfe Verbergen Hervorheben Die Datei »%s« kann nicht geöffnet werden Liste gespeicherter Filter Liste gespeicherter Filter mit regulären Ausdrücken Einlesen … Beim Start zu öffnende Protokolldatei Beim Start zu öffnende Protokolldateien Filter verwalten … Keine Übereinstimmungen gefunden Normale Größe Protokoll öffnen Öffnen … Bitte geben Sie entweder die Vordergrund- oder die Hintergrundfarbe an. Beenden Regulärer Ausdruck ist leer. Regulärer Ausdruck ist ungültig: %s In »%s« suchen Alles auswählen Nur Übereinstimmungen anzeigen Die Version des Programmes anzeigen. Beim Anzeigen eines Protokolls zu verwendende Schriftgröße Gibt an, welche Protokolldateien beim Start geöffnet werden soll. Die Vorgabe wird durch die Datei »/etc/syslog.conf« festgelegt. Legt die Höhe des Systemprotokoll-Fensters in Pixeln fest. Gibt an, welche Protokolldatei beim Start geöffnet werden soll. Die Vorgabe ist entweder »/var/adm/messages« oder »/var/log/messages«. Dies ist von Ihrer Distribution abhängig. Legt die Größe dicktengleicher Schriften zur Anzeige der Protokollen im Systemprotokoll-Fenster fest. Die Vorgabe wird der Vorgabe »Terminal-Schrift« entnommen. Legt die Breite des Systemprotokoll-Fensters in Pixeln fest. Systemprotokoll Die Datei ist keine gewöhnliche oder keine Textdatei. Beim Anzeigen der Hilfe ist ein Fehler aufgetreten: %s Die Veröffentlichung von diesem Programm erfolgt in der Hoffnung, dass es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, sogar ohne die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General Public License. Dieses Programm ist freie Software. Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation veröffentlicht, weitergeben und/oder modifizieren, entweder gemäß Version 2 der Lizenz oder (nach Ihrer Option) jeder späteren Version. Diese Version von Systemprotokoll unterstützt keine mit GZip komprimierten Protokolle. Systemprotokolle anzeigen oder überwachen Breite des Systemprotokoll-Fensters in Pixeln Fortlaufend Sie haben keine Leserechte für das Dokument. Sie sollten ein Exemplar der GNU General Public License zusammen mit diesem Programm erhalten haben. Falls nicht, schreiben Sie an die Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA. Vergrößern Verkleinern [Adresse …] Hinzu_fügen _Abbrechen _Schließen _Name: _Öffnen _Eigenschaften _Regulärer Ausdruck: E_ntfernen Logdateien;Fehler;Systemprotokoll;Diagnose; heute Manuel Borchers <webmaster@matronix.de>
Karl Eichwalder <ke@suse.de>
Christian Meyer <chrisime@gnome.org>
Christian Neumair <chris@gnome-de.org>
Benedikt Roth <Benedikt.Roth@gmx.net>
Matthias Warkus <mawarkus@gnome.org>
Simon Westhues <westhues@muenster.de>
Hendrik Brandt <heb@gnome-de.org>
Hendrik Richter <hendrikr@gnome.org>
Christian Kirbach <christian.kirbach@googlemail.com>
Nathan-J. Hirschauer <nathanhirschauer@verfriemelt.org>
Mario Blättermann <mario.blaettermann@gmail.com>
Paul Seyfert <pseyfert@mathphys.fsk.uni-heidelberg.de>
Hedda Peters <hpeters@redhat.com>
Tobias Endrigkeit <tobiasendrigkeit@googlemail.com>

Launchpad Contributions:
  Benjamin Steinwender https://launchpad.net/~b-u
  Daniel Winzen https://launchpad.net/~q-d
  Mario Blättermann https://launchpad.net/~mario.blaettermann
  Phillip Sz https://launchpad.net/~phillip-sz
  Sebastien Bacher https://launchpad.net/~seb128
  Tobias Endrigkeit https://launchpad.net/~tobiasendrigkeit aktualisiert gestern 