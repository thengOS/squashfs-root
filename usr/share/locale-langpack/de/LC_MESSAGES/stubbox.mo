��          �      l      �     �  $         %     C  (   [  .   �  /   �  V   �  <   :  )   w  F   �  %   �       #   .     R  �   c  �   <  O    S  T     �  �  �  5   c
  +   �
  /   �
  %   �
  D     U   `  R   �  �   	  n   �  D   �  V   @  1   �  "   �  ,   �       N  )  �   x  �  u  �       �                                         	                                  
                       A job generated by another job A job generating more generator jobs A job generating one more job A job that runs as root Check failed result from shell test case Check job is executed when dependency succeeds Check job is executed when requirements are met Check job result is set to "not required on this system" when requirements are not met Check job result is set to uninitiated when dependency fails Check success result from shell test case Check success result from shell test case (generated from a local job) Job killing the parent, if KILLER=yes Job sleeping for sixty seconds  Kill $PPID if $KILLER is set to yes Multilevel tests PURPOSE:
    This test checks that the manual plugin works fine
STEPS:
    1. Add a comment
    2. Set the result as passed
VERIFICATION:
    Check that in the report the result is passed and the comment is displayed PURPOSE:
    This test checks that the user-interact plugin works fine
STEPS:
    1. Read this description
    2. Press the test button
VERIFICATION:
    Check that in the report the result is passed PURPOSE:
    This test checks that the user-interact-verify plugin works fine
STEPS:
    1. Read this description
    2. Ensure that the command has not been started yet
    3. Press the test button
    4. Look at the output and determine the outcome of the test
VERIFICATION:
    The command should have printed "Please select 'pass'" PURPOSE:
    This test checks that the user-verify plugin works fine
STEPS:
    1. Read this description
    2. Ensure that the command has been started automatically
    3. Do not press the test button
    4. Look at the output and determine the outcome of the test
VERIFICATION:
    The command should have printed "Please select 'pass'" Sleep for sixty seconds Project-Id-Version: plainbox
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-04-26 11:52+0000
PO-Revision-Date: 2016-01-02 13:07+0000
Last-Translator: schuko24 <gerdsaenger@t-online.de>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:20+0000
X-Generator: Launchpad (build 18115)
 Aufgabe, die von einer anderen Aufgabe erstellt wurde Eine Aufgabe, die weitere Aufgaben erstellt Eine Aufgabe, die eine weitere Aufgabe erstellt Eine Aufgabe, die als »root« läuft Überprüft, ob der Shell-Test das Ergebnis »Gescheitert« liefert. Überprüft, ob der Auftrag ausgeführt wird, wenn die Abhängigkeiten erfüllt sind. Überprüft, ob der Auftrag ausgeführt wird, wenn die Erfordernisse gegeben sind. Überprüft, ob das Ergebnis des Auftrags »auf diesem System nicht erforderlich« ist, wenn die Erfordernisse nicht gegeben sind. Überprüft, ob das Ergebnis des Auftrags »nicht ausgeführt« ist, wenn Abhängigkeiten nicht erfüllt sind. Überprüft, ob der Shell-Test das Ergebnis »Erfolgreich« liefert. Prüfe Erfolgsergebnis des Testfalls im Terminal (durch eine lokale Aufgabe generiert) Job der den Vater-Prozess beendet, wenn KILLER=Ja Aufgabe pausiert für eine Minute  Beende $PPID wenn $KILLER auf Ja gesetzt ist Mehrlevel-Tests ZWECK:
    Dieser Test überprüft, ob der Erweiterung »Manuell« einwandfrei funktioniert.
DURCHFÜHRUNG:
    1. Fügen Sie eine Bemerkung hinzu.
    2. Markieren Sie das Ergebnis als bestanden.
ÜBERPRÜFUNG:
    Überprüfen Sie im Bericht, ob das Ergebnis des Tests »Bestanden« ist und die eingegebene Bemerkung angezeigt wird. ZWECK:
    Dieser Test überprüft, dass das Benutzer-Interaktions-Plugin funktioniert
SCHRITTE:
    1. Lesen Sie diese Beschreibung
    2.Drücken Sie den Testknopf
BESTÄTIGUNG:
    Überprüfen Sie, ob im Prüfbericht das Resultat "bestanden" steht. Zweck:
    Prüfung, ob das benutzerbediente Bestätigungs-PlugIn richtig arbeitet.
SCHRITTE:
    1. Lesen Sie bitte diese Beschreibung
    2. Stellen Sie sicher, dass der Befehl automatisch gestartet wurde
    3. Nicht den »Testknopf« betätigen
    4. Sehen Sie sich die Ausgabe an und beurteilen Sie das Testergebnis
BESTÄTIGUNG:
    Der Befehl sollte folgendes ausgegeben haben: »Bitte wählen Sie 'weiter'« . ZWECK:
    Prüfung, ob das Benutzerbestätigungs-PlugIn richtig arbeitet.
SCHRITTE:
    1. Lesen Sie bitte diese Beschreibung
    2. Stellen Sie sicher, dass der Befehl automatisch gestartet wurde
    3. Nicht den »Testknopf« betätigen
    4. Sehen Sie sich die Ausgabe an und beurteilen Sie das Testergebnis
BESTÄTIGUNG:
    Der Befehl sollte folgendes ausgegeben haben: »Bitte wählen Sie 'weiter'« . eine Minute pausieren 