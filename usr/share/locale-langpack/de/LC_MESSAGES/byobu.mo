��    -      �  =   �      �     �  "   �  @        _     v     |  
   �  4   �  .   �  9   �  +   -     Y     `     v     �     �     �  	   �     �     �     �            @   '     h     t  �   y          $     )     1  	   6     @     H     \     t     �  &   �     �     �     �  $     /   ,     \  �  e     �	  9   
  G   L
     �
     �
     �
     �
  0   �
  *     :   /  2   j  	   �     �     �     �     �  
   �     �          #     ;     V     ^  9   r     �     �  �   �     �     �  	   �     �     �     �     �     �     �  &     )   =     g     o  #   �  #   �  1   �                
                                                     %                     )          *   "       !          -      ,      &                $         	   '      (       #             +                       Byobu Configuration Menu  file exists, but is not a symlink <Tab>/<Alt-Tab> between elements | <Enter> selects | <Esc> exits Add to default windows Apply Archive Byobu Help Byobu currently does not launch at login (toggle on) Byobu currently launches at login (toggle off) Byobu will be launched automatically next time you login. Byobu will not be used next time you login. Cancel Change Byobu's colors Change escape sequence Change escape sequence: Change keybinding set Choose Command:  Create new window(s): Create new windows ERROR: Invalid selection Error: Escape key: ctrl- Extract the archive in your home directory on the target system. File exists Help If you are using the default set of keybindings, press\n<F5> to activate these changes.\n\nOtherwise, exit this screen session and start a new one. Manage default windows Menu Message Okay Presets:  Profile Remove file? [y/N]  Run "byobu" to activate Select a color:  Select a screen profile:  Select window(s) to create by default: Title:  Toggle status notifications Toggle status notifications: Which profile would you like to use? Which set of keybindings would you like to use? Windows: Project-Id-Version: screen-profiles
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-11-23 20:48-0600
PO-Revision-Date: 2012-02-21 17:15+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:40+0000
X-Generator: Launchpad (build 18115)
Language: de
  Byobu Konfiguration  Datei existiert, ist aber keine symbolische Verknüpfung <Tab>/<Alt-Tab> zwischen den Elementen| <Enter> auswählen | <Esc> Ende Hinzufügen zu Standardfenstern Übernehmen Archiv Byobu Hilfe Byobu startet nicht beim einloggen (einschalten) Byobu startet beim einloggen (ausschalten) Byobu wird beim nächsten Einloggen automatisch gestartet. Byobu wird beim nächsten Einloggen nicht genutzt. Abbrechen Farben von Byobu ändern Escape-Folge ändern Menü Tastaurbelegung wechseln Auswählen Befehl:  Neue(s) Fenster erstellen: neues Fenster erstellen FEHLER: ungültige Auswahl Fehler: Escape-Taste: Strg- Archiv im Home Verzeichnis auf dem Zielsystem extrahieren Datei existiert bereits Hilfe Wenn sie die standard Tastenkombinationen benutzen\ndrücken sie <F5> um die Änderungen zu aktivieren.\n\nAnderenfalls verlassen sie diese screen Sitzung und starten\nsie eine neue. Standardfenster verwalten Menü Nachricht Ok Voreinstellungen:  Profil Lösche Datei? [j/N]  "byobu" eingeben um zu starten Wählen Sie eine Farbe:  Wählen Sie ein Bildschirmprofil aus:  Wähle Fenster die immer erstellt werden: Titel:  Statusmeldungen ändern Benachrichtigungen ein/aus schalten Welches Profil möchten Sie nutzen? Welche Tastenbelegung würden Sie gerne benutzen? Fenster: 