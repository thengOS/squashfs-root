��    I      d  a   �      0     1  4  N     �  R   �  g   �  Z   W	  '   �	     �	     �	     �	     
  	   '
  1   1
  	   c
  	   m
  (   w
  )   �
     �
     �
     �
  #      '   $  "   L     o     �     �     �     �     �     �     �      �          	             !   /  '   Q  %   y  	   �     �  '   �     �  P   �     1     H     X     e  
   t  ;     3   �  Q   �     A     P  &   g     �      �  #   �      �  *   	      4      U  /   v     �  =   �  	         
               #  (   0     Y  �  i  !     �  1     �  �   �  �   l  v   �  /   q     �     �     �     �     �  H        T     ]  7   i  4   �  ,   �            4   0  ,   e     �     �     �  	   �     �     
          "     6  &   =     d     m     �     �  #   �  0   �  N   �     ;     G  E   P     �  \   �     �          #     0  
   B  3   M  4   �  I   �              7   8     p       &   �     �  K   �  (     %   H  3   n     �  9   �     �       	             %  #   3     W         )      +   I   E   A       #   D   =      "          ;          C   *           0      /              H            9   4      	      
              >   <   8       $           -                   B   ,          3       ?              :   &          5   2   7                @          6                           G   1      '   %   F   .   !       (     - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Add Startup Program Additional startup _programs: Allow logout Browse… Choose what applications to start when you log in Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Could not display help document Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don't prompt for user confirmation Edit Startup Program Enable debugging code Enabled Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Icon Ignoring any existing inhibitors Log out No description No name Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Select Command Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Applications Startup Applications Preferences The startup command cannot be empty The startup command is not valid This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session master
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-05-18 08:40+0000
PO-Revision-Date: 2016-05-26 01:02+0000
Last-Translator: Christian Kirbach <Unknown>
Language-Team: Deutsch <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:39+0000
X-Generator: Launchpad (build 18115)
Language: de
  – Die GNOME-Sitzungsverwaltung %s [OPTION …] BEFEHL

BEFEHL ausführen und bestimme Funktionalitäten der Sitzung unterdrücken.

  -h, --help        Diese Hilfe anzeigen
  --version         Die Programmversion anzeigen
  --app-id KENNUNG  Die beim Unterdrücken zu benutzende
                    Programmkennung (optional)
  --reason GRUND    Der Grund für das Unterdrücken (optional)
  --inhibit ARG     Zu unterdrückende Funktionalitäten als mit Kommata getrennte Liste:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    BEFEHL nicht starten und stattdessen dauerhaft warten

Wenn die Option »--inhibit« nicht angegeben ist, so wird »idle« angenommen.
 %s benötigt ein Argument
 Ein Problem ist aufgetreten, welches vom System nicht behoben werden kann.
Bitte melden Sie sich ab und versuchen Sie es erneut. Ein Problem ist aufgetreten, welches vom System nicht behoben werden kann. Als Vorsichtsmaßnahme sind alle Erweiterungen deaktiviert worden. Ein Problem ist aufgetreten, welches vom System nicht behoben werden kann. Bitte kontaktieren Sie den Systemverwalter. Eine Sitzung mit Namen »%s« existiert bereits AUTOSTARTORDNER Startprogramm hinzufügen _Zusätzliche Startprogramme: Abmelden erlauben Durchsuchen … Wählen Sie, welche Anwendungen gestartet werden, wenn Sie sich anmelden _Befehl: Ko_mmentar: Das Verbinden zur Sitzungsverwaltung war nicht möglich ICE-Listening-Socket konnte nicht erzeugt werden: %s Die Hilfeseite konnte nicht angezeigt werden Benutzerdefiniert Benutzerdefinierte Sitzung Überprüfung des Hardware-Beschleunigers abschalten Keine benutzerspezifischen Anwendungen laden Keine Bestätigung abfragen Startprogramm bearbeiten Debugging-Code aktivieren Aktiviert Fehler beim Ausführen von %s
 GNOME GNOME Platzhalter GNOME unter Wayland Symbol Bestehende Unterdrückungen ignorieren Abmelden Keine Beschreibung Namenlos Keine Antwort Leider ist ein Problem aufgetreten. Vorgegebene Autostart-Ordner außer Kraft setzen Bitte wählen Sie eine benutzerdefinierte Sitzung, die ausgeführt werden soll Ausschalten Programm Programm wurde mit Optionen aufgerufen, die zu einem Konflikt führen Neustart Neue Client-Verbindung wurde abgelehnt, weil die aktuelle Sitzung gegenwärtig beendet wird
 Gemerkte Anwendung Sitzung _umbenennen SITZUNGSNAME Befehl auswählen Sitzung %d Sitzungsnamen dürfen keine »/«-Zeichen enthalten Sitzungsnamen dürfen nicht mit einem ».« anfangen Sitzungsnamen dürfen nicht mit einem ».« anfangen oder »/« enthalten Zu verwendende Sitzung Warnung der Erweiterung anzeigen Den Dialog zum Fehlschlag für Prüfungszwecke anzeigen Startprogramme Startprogramme Sie müssen einen Startbefehl eingeben Der Startbefehl ist ungültig Dieser Eintrag ermöglicht es Ihnen, eine gespeicherte Sitzung auszuwählen Dieses Programm blockiert die Abmeldung. Diese Sitzung meldet Sie bei GNOME an Diese Sitzung meldet Sie bei GNOME unter Wayland an Version dieser Anwendung _Automatisch die laufenden Programme beim Abmelden merken _Fortfahren Benutzer ab_melden _Abmelden _Name: _Neue Sitzung Momentan laufende Programme _merken Sitzung _entfernen 