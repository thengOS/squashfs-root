��          �            x     y     �  "   �  )   �     �  #     (   3  (   \  (   �     �     �     �  )   �       $   2  �  W            )   4  7   ^  "   �  %   �  *   �  ,   
  2   7      j  #   �     �  ,   �  #   �  '                                    	                                   
           %s failed: %s %s: archive libraries: %u
 %s: bad archive symbol table names %s: bad extended name entry at header %zu %s: loaded archive members: %u
 %s: malformed archive header at %zu %s: malformed archive header name at %zu %s: malformed archive header size at %zu %s: no archive symbol table (run ranlib) %s: short archive header at %zu %s: total archive members: %u
 ** PLT PREL31 overflow in EXIDX_CANTUNWIND entry cannot open %s: %s: script or expression reference to %s Project-Id-Version: binutils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2015-09-08 18:46+0000
Last-Translator: Ettore Atalan <atalanttore@googlemail.com>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:03+0000
X-Generator: Launchpad (build 18115)
 %s fehlgeschlagen: %s %s: Archivbibliotheken: %u
 %s: Ungültige Archivsymbol-Tabellennamen %s: Ungültiger erweiterter Namenseintrag im Header %zu %s: Geladene Archivmitglieder: %u
 %s: Ungültiger Archiv-Header bei %zu %s: Ungültiger Archiv-Header-Name bei %zu %s: Ungültige Archiv-Header-Größe bei %zu %s: Keine Archivsymbol-Tabelle (ranlib ausführen) %s: Kurzer Archiv-Header bei %zu %s: Archivmitglieder insgesamt: %u
 ** PLT PREL31 Überlauf in Eintrag EXIDX_CANTUNWIND %s kann nicht geöffnet werden: %s: Skript oder Ausdruck referenziert zu %s 