��            )         �     �  �  �  �  �  !      !   B  /   d  =   �  2   �  $        *  %   C  .   i  +   �  *   �  .   �     	     9	     U	     s	     �	  &   �	     �	     �	  @   �	  3   /
  R  c
     �     �     �               �  �  �  �  �  $   /  %   T  5   z  E   �  4   �  )   +     U  4   l  B   �  3   �  /     ;   H     �  3   �  #   �  !   �       7   4     l     �  g   �  Y   �  �  X     �          '     F                                                                                     	                                      
                    %s %s
   -n, --name=name         get the named extended attribute value
  -d, --dump              get all extended attribute values
  -e, --encoding=...      encode values (as 'text', 'hex' or 'base64')
      --match=pattern     only get attributes with names matching pattern
      --only-values       print the bare values only
  -h, --no-dereference    do not dereference symbolic links
      --absolute-names    don't strip leading '/' in pathnames
  -R, --recursive         recurse into subdirectories
  -L, --logical           logical walk, follow symbolic links
  -P  --physical          physical walk, do not follow symbolic links
      --version           print version and exit
      --help              this help text
   -n, --name=name         set the value of the named extended attribute
  -x, --remove=name       remove the named extended attribute
  -v, --value=value       use value as the attribute value
  -h, --no-dereference    do not dereference symbolic links
      --restore=file      restore extended attributes
      --version           print version and exit
      --help              this help text
 %s %s -- get extended attributes
 %s %s -- set extended attributes
 %s: %s: No filename found in line %d, aborting
 %s: No filename found in line %d of standard input, aborting
 %s: Removing leading '/' from absolute path names
 %s: invalid regular expression "%s"
 -V only allowed with -s
 A filename to operate on is required
 At least one of -s, -g, -r, or -l is required
 Attribute "%s" had a %d byte value for %s:
 Attribute "%s" has a %d byte value for %s
 Attribute "%s" set to a %d byte value for %s:
 Could not get "%s" for %s
 Could not list "%s" for %s
 Could not remove "%s" for %s
 Could not set "%s" for %s
 No such attribute Only one of -s, -g, -r, or -l allowed
 Unrecognized option: %c
 Usage: %s %s
 Usage: %s %s
       %s %s
Try `%s --help' for more information.
 Usage: %s %s
Try `%s --help' for more information.
 Usage: %s [-LRSq] -s attrname [-V attrvalue] pathname  # set value
       %s [-LRSq] -g attrname pathname                 # get value
       %s [-LRSq] -r attrname pathname                 # remove attr
       %s [-LRq]  -l pathname                          # list attrs 
      -s reads a value from stdin and -g writes a value to stdout
 getting attribute %s of %s listing attributes of %s setting attribute %s for %s setting attributes for %s Project-Id-Version: attr-2.2.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 13:57+0000
PO-Revision-Date: 2015-12-04 01:14+0000
Last-Translator: Andreas Grünbacher <Unknown>
Language-Team: <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:59+0000
X-Generator: Launchpad (build 18115)
        %s %s
   -n, --name=name         Das angegebene erweiterte Attribut lesen
  -d, --dump              Alle erweiterten Attribute lesen
  -e, --encoding=...      Wert codieren (als 'text', 'hex' oder 'base64')
      --match=muster      Nur Attribute, die mit muster übereinstimmen
      --only-values       Die Attributwerte ohne Codierung ausgeben
  -h, --no-dereference    Symbolische Links nicht dereferenzieren
      --absolute-names    Führende '/' in Pfadnamen nicht entfernen
  -R, --recursive         In Unterverzeichnisse wechseln
  -L, --logical           Alle symbolischen Links verfolgen
  -P  --physical          Symbolische Links nicht verfolgen
      --version           Die Version ausgeben
      --help              Diese Hilfe
   -n, --name=name         Name des Attributs, das gesetzt werden soll
  -x, --remove=name       Entfernt das angegebene erweiterte Attribut
  -v, --value=wert        Wert für das erweiterte Attribut
  -h, --no-dereference    Symbolische Links nicht dereferenzieren
      --restore=datei     Erweiterte Attribute wiederhestellen
      --version           Die Version ausgeben
      --help              Diese Hilfe
 %s %s -- erweiterte Attribute lesen
 %s %s -- Erweiterte Attribute setzen
 %s: %s: Kein Dateiname in Zeile %d gefunden, Abbruch
 %s: Kein Dateiname in Zeile %d der Standardeingabe gefunden; Abbruch
 %s: Entferne führenden '/' von absoluten Pfadnamen
 %s: ungültiger regulärer Ausdruck "%s"
 -V nur mit -s erlaubt
 Der Name der zu verwendenden Datei ist erforderlich
 Mindestens einer der Parameter -s, -g, -r, oder -l wird benötigt
 Attribut "%1$s" von %3$s hat einen %2$d-Byte-Wert:
 Attribut »%s« hat einen %d Byte-Wert für %s
 Attribut "%1$s" von %3$s auf einen %2$d-Byte-Wert gesetzt:
 Konnte "%s" von %s nicht lesen
 »%s« konnte für »%s« nicht aufgelistet werden
 Konnte "%s" von %s nicht entfernen
 Konnte "%s" für %s nicht setzen
 Kein solches Attribut Nur einer der Parameter -s, -g, -r oder -l ist erlaubt
 Unbekannte Option: %c
 Verwendung: %s %s
 Aufruf: %s %s
        %s %s
Geben Sie »%s --help« ein, um weiterführende Informationen zu erhalten.
 Aufruf: %s %s
Geben Sie »%s --help« ein, um weiterführende Informationen zu erhalten.
 Verwendung: %s [-LRSq] -s attrname [-V attrwert] pfadname  # Wert festlegen
       %s [-LRSq] -g attrname pfadname                 # Wert auslesen
       %s [-LRSq] -r attrname pfadname                 # Attribut entfernen
       %s [-LRq]  -l pfadname                          # Attribute auflisten 
      -s liest einen Wert aus der Standardeingabe und -g schreibt einen Wert in die Standardausgabe
 Lesen von Attribut %s von %s Auslisten von Attributen von %s Setzen von Attribut %s für %s Setzen von Attributen für %s 