��    b      ,  �   <      H  "   I  �   l     �     	  p   	  	   �	     �	     �	     �	  
   �	     �	  	   �	     �	     �	     �	     �	  :   �	  	   
     '
     4
     D
     L
     T
     Z
     c
     h
     p
     u
  	   z
  -   �
  #   �
     �
     �
     �
  	          	         *  	   0  	   :     D     I     O     l     s     �     �     �     �     �     �     �     �     �  	   �     �     �     �       	   "     ,     9     G     U  '   j     �     �  1   �     �  9   �  9     7   K     �     �     �     �     �  
   �     �     �     �     �  	   �     �     �  	   �     �     �     �     �          
       
        &     4     =  �  B  )   �  �        �     �  x   �     _     k  	   s     }  
   �  	   �  	   �     �     �  	   �     �  <   �          ,     :     K     R     Z     h     q     w  
        �     �  4   �  *   �            "        ?     D     X     m     s          �     �  !   �     �     �     �     �     �     �     �           0     A     U     ]  
   f     q     z     �  	   �     �     �     �     �  1   �     %     -  0   3     d  1   l  2   �  1   �                         *  
   1     <     C     L  
   U  	   `  
   j     u     ~     �     �     �     �     �     �     �  	   �     �     �  	   �                     C          ;          <   7   Q       0   9       @   M      "   B   /             A      ]         &       K   ^   O           G   H   #   4   Z                 T   =      a   6       :             [   -   E   
                     	   F   V       2              J   Y   b           P       *          _            !   (           5       $   '      X      U      >   S             .   \   R      `   I   ,       )      ?   1   D   8   3   %   W   N   L                  +    1KB;100KB;1MB;10MB;100MB;1GB;>1GB; Accessories;Education;Games;Graphics;Internet;Fonts;Office;Media;Customisation;Accessibility;Developer;Science & Engineering;Dash plugins;System Albums Applications Blues;Classical;Country;Disco;Funk;Rock;Metal;Hip-hop;House;New-wave;R&B;Punk;Jazz;Pop;Reggae;Soul;Techno;Other; Bookmarks Books Boxes Calendar Categories Code Community Dash plugins Date Decade Documentation Documents;Folders;Images;Audio;Videos;Presentations;Other; Downloads Encyclopedia Files & Folders Folders Friends Genre Graphics Help History Home Info Installed Last 7 days;Last 30 days;Last 6 months;Older; Last 7 days;Last 30 days;Last year; Last modified Local Local apps;Software center Locations More suggestions Most used Music My photos My videos News Notes Old;60s;70s;80s;90s;00s;10s; Online Online photos People Photos Popular online Radio Recent Recent apps Recently played Recently used Recipes Reference Results Scholar Search applications Search files & folders Search in Search music Search photos Search videos Search your computer Search your computer and online sources Size Songs Sorry, there is nothing that matches your search. Sources There are no photos currently available on this computer. There are no videos currently available on this computer. There is no music currently available on this computer. Top Type Upcoming Updates Videos Vocabulary Weather Web books; boxes; calendar; code; files; graphics; help; info; music; news; notes; photos; recipes; reference; videos;video; weather; web; Project-Id-Version: unity-scope-home
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-12 20:00+0000
PO-Revision-Date: 2016-06-22 11:55+0000
Last-Translator: Ettore Atalan <atalanttore@googlemail.com>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:22+0000
X-Generator: Launchpad (build 18115)
 1 kB;100 kB;1 MB;10 MB;100 MB;1 GB;>1 GB; Zubehör;Bildung;Spiele;Grafik;Internet;Schriftarten;Büro;Multimedia;Personalisierung;Barrierefreiheit;Entwicklungswerkzeuge;Wissenschaft & Ingenierwesen;Dash-Erweiterungen;System Alben Anwendungen Blues;Klassik;Country;Disco;Funk;Rock;Metal;Hip-Hop;Neue Welle;Rhythm and Blues;Punk;Jazz;Pop;Reggae;Soul;Techno;Andere; Lesezeichen Bücher Container Kalender Kategorien Quelltext Community Dash-Erweiterungen Datum Jahrzehnt Dokumentation Dokumente;Ordner;Bilder;Audio;Videos;Präsentationen;Andere; Heruntergeladene Dateien Enzyklopädie Dateien & Ordner Ordner Freunde Musikrichtung Grafiken Hilfe Verlauf Startseite Informationen Installiert Letzte 7 Tage;Letzte 30 Tage;Letzte 6 Monate;Älter; Letzte 7 Tage;Letzte 30 Tage;Letztes Jahr; Zuletzt geändert Lokal Lokale Anwendungen;Software-Center Orte Weitere Vorschläge Am meisten verwendet Musik Meine Fotos Meine Videos News Notizen Alt;60er;70er;80er;90er;00er;10er Online Fotos im Internet Personen Fotos Beliebt im Internet Radio Zuletzt verwendet Kürzlich verwendete Anwendungen Zuletzt gespielt Kürzlich verwendet Rezepte Referenz Ergebnisse Schüler Anwendungen suchen Dateien & Verzeichnisse suchen Suchen in Musik suchen Fotos suchen Videos suchen Computer durchsuchen Durchsuchen Sie Ihren Computer und Online-Quellen Größe Titel Entschuldigung, es wurden keine Treffer erzielt. Quellen Auf diesem Computer gibt es momentan keine Fotos. Auf diesem Computer gibt es momentan keine Videos. Auf diesem Computer gibt es momentan keine Musik. Oben Typ Bevorstehend Aktualisierungen Videos Wortschatz Wetter Internet Bücher; Container; Kalender; Quelltext; Dateien; Bilder; Hilfe; Informationen; Musik; Neuigkeiten; Notizen; Fotos; Rezepte; Referenz; Videos;Video; Wetter; Internet; 