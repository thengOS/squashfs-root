��          �      �       0  %   1     W     s     �     �  "   �  1   �  3     .   J  7   y  0   �  -   �  �    6   �  2   �  0   $  8   U  6   �  2   �  C   �  L   <  @   �  N   �  C     >   ]                                         	             
    Check if the package system is locked Get current global keyboard Get current global proxy Set current global keyboard Set current global proxy Set current global proxy exception System policy prevents querying keyboard settings System policy prevents querying package system lock System policy prevents querying proxy settings System policy prevents setting global keyboard settings System policy prevents setting no_proxy settings System policy prevents setting proxy settings Project-Id-Version: ubuntu-system-service
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-11-04 00:35+0000
PO-Revision-Date: 2012-09-21 22:23+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:35+0000
X-Generator: Launchpad (build 18115)
 Bitte prüfen Sie, ob die Paketverwaltung gesperrt ist Systemweite Tastatureinstellungen werden abgefragt Systemweite Proxy-Einstellungen werden abgefragt Aktuelle Tastatureinstellungen werden systemweit gesetzt Aktuelle Proxy-Einstellungen werden systemweit gesetzt Aktuelle Proxy-Ausnahmen werden systemweit gesetzt Systemrichtlinie verhindert das Abfragen der Tastatur-Einstellungen Eine Systemrichtlinie verhindert das Abfragen der Sperre der Paketverwaltung Systemrichtlinie verhindert das Abfragen der Proxy-Einstellungen Systemrichtlinie verhindert das Setzen der systemweiten Tastatur-Einstellungen Systemrichtlinie verhindert das Setzen der »no_proxy«-Einstellung Systemrichtlinie verhindert das Setzen der Proxy-Einstellungen 