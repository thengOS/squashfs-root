��     0   �  {  @     ,     4  T      �  �   �  �   �  )   M     w  �   �  �   �  :     O   J  B   �  C   �     !  �   9  �     �   �  3   �  }   �  I   z   L   �   �   !  �   �!  �   �"  �   P#  O   �#  >   ?$  �   ~$  I   S%  D   �%  E   �%     (&  0   <&  H   m&  N   �&  7   '  "   ='  '   `'  |   �'     (      (  t   =(  $   �(  "   �(     �(  ,   )  ,   C)  ,   p)  '   �)  -   �)      �)  (   *  (   =*     f*     *     �*     �*  *   �*  *   +     1+     3+  _   7+  g   �+  g   �+  h   g,     �,     �,  1   �,     &-  #   B-     f-  $   �-  $   �-  !   �-  !   �-  -   .     <.  !   \.  &   ~.      �.  <   �.     /  >   /     Q/     i/  +   �/     �/  )   �/  @   �/  $   :0     _0  "   ~0     �0     �0  �   �0  �   f1  I   2    d2  ?   s3  9   �3  #   �3  8   4  1   J4     |4      �4  <   �4  b   �4  b   Z5  c   �5  �   !6     �6     �6     �6  >   �6  t   7     �7  6   �7  7   �7  g   8     8     �8  4   �8     �8  �   �8  M   �9  +   �9  3   ":  W   V:  x   �:     ';  ?   E;  �   �;  X   1<  <   �<  )   �<  "   �<  �   =  3   >  6   C>  9   z>  (   �>  �   �>  �   o?  D   @     Y@     t@  7   �@     �@  [   �@  -   ;A  '   iA  �  �A  0   1C     bC  *   wC  �   �C  /   -D  *   ]D  2   �D  4   �D  A   �D  o   2E     �E  ;   �E  3   �E  /   )F  +   YF  '   �F  #   �F     �F     �F     G  q   G  :   �G  �   �G     vH     �H     �H     �H     �H     �H     �H  7   �H  =   !I  C   _I  3   �I     �I  :   �I     #J     /J     @J     ^J  !   oJ     �J     �J  !   �J  Y   �J     K     ,K     IK     ^K  !   ~K  =   �K  !   �K  +    L     ,L      LL      mL  #   �L  6   �L  -   �L  $   M  %   <M  '   bM     �M     �M     �M     �M     �M     N  P   N  6   pN  n   �N  )   O  P   @O     �O     �O  3   �O     �O  $   P  D   0P  (   uP  �   �P     'Q     8Q  )   QQ  %   {Q      �Q  �   �Q  8   �R  !   �R  	  �R  L   �S  6   CT     zT  *   �T     �T     �T  )  �T     �U     V  *   'V     RV     gV     �V     �V     �V  W   �V  ?   W  I  YW  C   �X  .   �X  H   Y  +   _Y  :   �Y  q   �Y  >   8Z  o   wZ  "   �Z  -   
[  @   8[  �   y[  �   [\     A]     M]    c]  �   o_  �   3`  �   a  w   �a  �   /b  �   (c  @   �c  Q   d  M   Vd  H   �d     �d  �   e  5  �e  =  g  ?   Qh  �   �h  T   !i  y   vi  J  �i  �   ;k  �   l  �   �l  �   �m  W   *n  �   �n  T   uo  D   �o  y   p     �p  3   �p  _   �p  a   4q  @   �q  $   �q  &   �q  �   #r  1   �r  "   �r  �   s  +   �s  .   �s      �s  /   t  5   Nt  /   �t  2   �t  0   �t  )   u  .   Bu  1   qu  )   �u  *   �u      �u      v  N   :v  I   �v     �v     �v  v   �v  �   Pw  �   �w  �   ax     �x     �x  9   
y  2   Dy  ?   wy  0   �y  :   �y  >   #z  /   bz  /   �z  4   �z  -   �z  )   %{  -   O{  !   }{  D   �{     �{  Z   �{      N|  ,   o|  G   �|  *   �|  ?   }  V   O}  0   �}  >   �}  -   ~  =   D~     �~  �   �~  �   k  J   �    j�  @   m�  9   ��  *   �  <   �  0   P�  #   ��  )   ��  D   ς  q   �  q   ��  s   ��  �   l�       �     !�     0�  D   =�  �   ��     �  G   '�  >   o�  i   ��     �     (�  3   =�  !   q�  �   ��  k   a�  2   ͈  @    �  q   A�  �   ��  #   4�  ,   X�  �   ��  Y   #�  @   }�  (   ��  0   �  0  �  U   I�  X   ��  b   ��  :   [�  �   ��  �   8�  W   �     9�  "   V�  H   y�       _   ܐ  .   <�  (   k�  �  ��  5   7�     m�  6   ��  �   ��  A   P�  ?   ��  6   Ҕ  6   	�  \   @�  ~   ��     �  A   8�  7   z�  3   ��  /   �  +   �  '   B�  #   j�     ��     ��  �   ×  @   H�  �   ��     c�     ��     ��     ��  !   ��     ��     ϙ  ;   �  7   *�  P   b�  U   ��     	�  \   )�  .   ��     ��  @   қ     �  J   +�     v�     {�  :   ��  w   ��     7�  %   U�     {�  3   ��  *   ɝ  R   ��  (   G�  6   p�  .   ��  .   ֞  .   �  0   4�  U   e�  I   ��  >   �  0   D�  C   u�     ��      נ  )   ��      "�  $   C�     h�  @   ~�  8   ��  �   ��  %   y�  V   ��     ��  !   �  =   .�     l�  +   ��  <   ��  7   �  �   &�  #   Ѥ     ��  =   �  &   S�  !   z�  �   ��  Q   ��  )   Ӧ    ��  r   �  O   u�     Ũ  -   Ψ     ��     �  ,  #�  $   P�  (   u�  ?   ��  #   ު     �  %   "�  	   H�     R�  �   k�  F   �  Q  H�  U   ��  /   �  L    �  5   m�  H   ��  x   �  W   e�  �   ��  ,   ?�  -   l�  V   ��    �  �   �     ��     ��     �          H   �   �          �   �   s   {   +   �   �   R       �         B      �   �   �   #           �   r   �   j       
  �   h   6   u   :   �        �       e   �   Z   �          @   v         }   c   �                       \   �   �   |                  p   M   �   �   �   A           �   �   �   <   �   �   �   C   "   3   �      �   �             w   ?          �       �   I     1       �       �   !       �   �              [   �   �   f       �   9   �   x   U      X           (   P              �   
       )   W           �       �   k   ^   �   �          �   �         D         �       �   �         .         �   �      �       4   *   �   �       �   �       y      o   =   �   ~       �   J      /       �   �     -   >       �   	       �                     i   F   �   �   �   K   �   �   �   n                   _   �       �      �          �   N   �     G          �   �   �   �   �               8       �       �   �   �   Q       5           �           	          &   %   �       t   �   �       �   �       �   �       �       Y       a   g   �   `   �       �                �         �   �   �   �   E      �   �                   q   �          ]   �   S                 �   7   �   �         �   �           �              �   T           �   �       �   �     �   z           O   2   L       �   V   �   �   m   �   d           �     l   ;   0   $   �   �       '       �   b          �              ,      �  t  �  �  �  �  �  �    $  @  \  p  �  �  �  �  �  7              $   ������  =              �   ������  $          ����ɴ  5          ���� �  H          ����J�  2          ����~�  /          ������  7          �����  Q              '   ����}�  Q              �   ����Ϸ  %       	   ������  G          ����F�  F          ������  /          ������  6          ������  A          ���� 
Execution of xargs will continue now, and it will try to read its input and run commands; if this is not what you wanted to happen, please type the end-of-file keystroke.
 
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

 
Report bugs to <bug-findutils@gnu.org>.
 
Report bugs to: %s
 
actions: -delete -print0 -printf FORMAT -fprintf FILE FORMAT -print 
      -fprint0 FILE -fprint FILE -ls -fls FILE -prune -quit
      -exec COMMAND ; -exec COMMAND {} + -ok COMMAND ;
      -execdir COMMAND ; -execdir COMMAND {} + -okdir COMMAND ;
 
default path is the current directory; default expression is -print
expression may consist of: operators, options, tests, and actions:
       --help                   display this help and exit
       --process-slot-var=VAR   set environment variable VAR in child processes
       --show-limits            show limits on command-line length
       --version                output version information and exit
       -context CONTEXT
       -nouser -nogroup -path PATTERN -perm [-/]MODE -regex PATTERN
      -readable -writable -executable
      -wholename PATTERN -size N[bcwkMG] -true -type [bcdpflsD] -uid N
      -used N -user NAME -xtype [bcdpfls]   -0, --null                   items are separated by a null, not whitespace;
                                 disables quote and backslash processing and
                                 logical EOF processing
   -E END                       set logical EOF string; if END occurs as a line
                                 of input, the rest of the input is ignored
                                 (ignored if -0 or -d was specified)
   -I R                         same as --replace=R
   -L, --max-lines=MAX-LINES    use at most MAX-LINES non-blank input lines per
                                 command line
   -P, --max-procs=MAX-PROCS    run at most MAX-PROCS processes at a time
   -a, --arg-file=FILE          read arguments from FILE, not standard input
   -d, --delimiter=CHARACTER    items in input stream are separated by CHARACTER,
                                 not by whitespace; disables quote and backslash
                                 processing and logical EOF processing
   -e, --eof[=END]              equivalent to -E END if END is specified;
                                 otherwise, there is no end-of-file string
   -i, --replace[=R]            replace R in INITIAL-ARGS with names read
                                 from standard input; if R is unspecified,
                                 assume {}
   -l[MAX-LINES]                similar to -L but defaults to at most one non-
                                 blank input line if MAX-LINES is not specified
   -n, --max-args=MAX-ARGS      use at most MAX-ARGS arguments per command line
   -p, --interactive            prompt before running commands
   -r, --no-run-if-empty        if there are no arguments, then do not run COMMAND;
                                 if this option is not given, COMMAND will be
                                 run at least once
   -s, --max-chars=MAX-CHARS    limit length of command line to MAX-CHARS
   -t, --verbose                print commands before executing them
   -x, --exit                   exit if the size (see -s) is exceeded
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s is an slocate database of unsupported security level %d; skipping it. %s is an slocate database.  Support for these is new, expect problems for now. %s is an slocate database.  Turning on the '-e' option. %s is not the name of a known user %s is not the name of an existing group %s is not the name of an existing group and it does not look like a numeric group ID because it has the unexpected suffix %s %s terminated by signal %d %s%s argument '%s' too large %s%s changed during execution of %s (old device number %ld, new device number %ld, file system type is %s) [ref %ld] %s: exited with status 255; aborting %s: invalid number for -%c option
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: stopped by signal %d %s: terminated by signal %d %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 %s: value for -%c option should be <= %ld
 %s: value for -%c option should be >= %ld
 ' (C) -type %c is not supported because FIFOs are not supported on the platform find was compiled on. -type %c is not supported because Solaris doors are not supported on the platform find was compiled on. -type %c is not supported because named sockets are not supported on the platform find was compiled on. -type %c is not supported because symbolic links are not supported on the platform find was compiled on. < %s ... %s > ?  All Filenames: %s
 Arguments to -type should contain only one letter Cannot close standard input Cannot obtain birth time of file %s Cannot open input file %s Cannot read list of mounted devices. Cannot read mounted file system list Cannot set SIGUSR1 signal handler Cannot set SIGUSR2 signal handler Compression ratio %4.2f%% (higher is better)
 Compression ratio is undefined
 Database %s is in the %s format.
 Database was last modified at %s.%09ld Empty argument to the -D option. Environment variable %s is not set to a valid decimal number Eric B. Decker Expected a positive decimal integer argument to %s, but got %s Expected an integer: %s Failed to fully drop privileges Failed to initialize shared-file hash table Failed to read from stdin Failed to safely change directory into %s Failed to save working directory in order to run a command on %s Failed to write output (at stage %d) Failed to write prompt for -ok Failed to write to standard output Failed to write to stderr Features enabled:  File descriptor %d will leak; please report this as a bug, remembering to include a detailed description of the simplest way to reproduce this problem. File names have a cumulative length of %s bytes.
Of those file names,

	%s contain whitespace, 
	%s contain newline characters, 
	and %s contain characters with the high bit set.
 File system loop detected; %s is part of the same file system loop as %s. Filesystem loop detected; %s has the same device number and inode as a directory which is %d level higher in the file system hierarchy Filesystem loop detected; %s has the same device number and inode as a directory which is %d levels higher in the file system hierarchy General help using GNU software: <http://www.gnu.org/gethelp/>
 I cannot figure out how to interpret %s as a date or time Ignoring unrecognised debug flag %s In %s the %s must appear by itself, but you specified %s Invalid argument %s for option --max-database-age Invalid argument %s to -used Invalid argument `%s%s' to -size Invalid escape sequence %s in input delimiter specification. Invalid escape sequence %s in input delimiter specification; character values must not exceed %lo. Invalid escape sequence %s in input delimiter specification; character values must not exceed %lx. Invalid escape sequence %s in input delimiter specification; trailing characters %s not recognised. Invalid input delimiter specification %s: the delimiter must be either a single character or an escape sequence starting with \. Invalid optimisation level %s James Youngman Kevin Dalley Locate database size: %s byte
 Locate database size: %s bytes
 Mandatory and optional arguments to long options are also
mandatory or optional for the corresponding short option.
 Matching Filenames: %s
 Old-format locate database %s is too short to be valid Only one instance of {} is supported with -exec%s ... + Optimisation level %lu is too high.  If you want to find files very quickly, consider using GNU locate. Packaged by %s
 Packaged by %s (%s)
 Please specify a decimal number immediately after -O Report %s bugs to: %s
 Report (and track progress on fixing) bugs via the findutils bug-reporting
page at http://savannah.gnu.org/ or, if you have no web access, by sending
email to <bug-findutils@gnu.org>. Run COMMAND with arguments INITIAL-ARGS and more arguments read from input.

 Security level %s has unexpected suffix %s. Security level %s is outside the convertible range. Some filenames may have been filtered out, so we cannot compute the compression ratio.
 Symbolic link %s is part of a loop in the directory hierarchy; we have already visited the directory to which it points. The %s test needs an argument The -O option must be immediately followed by a decimal integer The -delete action automatically turns on -depth, but -prune does nothing when -depth is in effect.  If you want to carry on anyway, just explicitly use the -depth option. The -show-control-chars option takes a single argument which must be 'literal' or 'safe' The argument for option --max-database-age must not be empty The argument to -user should not be empty The atexit library function failed The current directory is included in the PATH environment variable, which is insecure in combination with the %s action of find.  Please remove the current directory from your $PATH (that is, remove ".", doubled colons, or leading or trailing colons) The database has big-endian machine-word encoding.
 The database has little-endian machine-word encoding.
 The database machine-word encoding order is not obvious.
 The environment is too large for exec(). The environment variable FIND_BLOCK_SIZE is not supported, the only thing that affects the block size is the POSIXLY_CORRECT environment variable The relative path %s is included in the PATH environment variable, which is insecure in combination with the %s action of find.  Please remove that entry from $PATH This system does not provide a way to find the birth time of a file. Unexpected suffix %s on %s Unknown argument to -type: %c Unknown regular expression type %s; valid types are %s. Unknown system error Usage: %s [--version | --help]
or     %s most_common_bigrams < file-list > locate-database
 Usage: %s [-0 | --null] [--version] [--help]
 Usage: %s [-H] [-L] [-P] [-Olevel] [-D  Usage: %s [-d path | --database=path] [-e | -E | --[non-]existing]
      [-i | --ignore-case] [-w | --wholename] [-b | --basename] 
      [--limit=N | -l N] [-S | --statistics] [-0 | --null] [-c | --count]
      [-P | -H | --nofollow] [-L | --follow] [-m | --mmap] [-s | --stdio]
      [-A | --all] [-p | --print] [-r | --regex] [--regextype=TYPE]
      [--max-database-age D] [--version] [--help]
      pattern...
 Usage: %s [OPTION]... COMMAND [INITIAL-ARGS]...
 Valid arguments are: WARNING: Lost track of %lu child processes WARNING: a NUL character occurred in the input.  It cannot be passed through in the argument list.  Did you mean to use the --null option? WARNING: cannot determine birth time of file %s WARNING: file %s appears to have mode 0000 WARNING: file system %s has recently been mounted. WARNING: file system %s has recently been unmounted. WARNING: locate database %s was built with a different byte order Warning: %s will be run at least once.  If you do not want that to happen, then press the interrupt keystroke.
 Written by %s and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, %s, and others.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
and %s.
 Written by %s, %s, %s,
%s, %s, %s, and %s.
 Written by %s, %s, %s,
%s, %s, and %s.
 Written by %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
and %s.
 Written by %s, %s, and %s.
 Written by %s.
 You may not use {} within the utility name for -execdir and -okdir, because this is a potential security problem. You need to specify a security level as a decimal integer. You specified the -E option, but that option cannot be used with slocate-format databases with a non-zero security level.  No results will be generated for this database.
 ] [path...] [expression]
 ^[nN] ^[yY] ` ambiguous argument %s for %s argument line too long argument list too long argument to -group is empty, but should be a group name arithmetic overflow when trying to calculate the end of today arithmetic overflow while converting %s days to a number of seconds can't call exec() due to argument size restrictions cannot delete %s cannot fit single argument within argument list size limit cannot fork cannot search %s cannot stat current directory command too long could not create pipe before fork days double environment is too large for exec errno-buffer safe_read failed in xargs_do_exec (this is probably a bug, please report it) error closing file error reading a word from %s error waiting for %s error waiting for child process error: %s at end of format string error: the format directive `%%%c' is reserved for future use expected an expression after '%s' expected an expression between '%s' and ')' failed to drop group privileges failed to drop setgid privileges failed to drop setuid privileges failed to open /dev/tty for reading failed to restore working directory after searching %s failed to return to initial working directory failed to return to parent directory failed to set environment variable %s failed to unset environment variable %s getfilecon failed: %s invalid %s%s argument '%s' invalid -size type `%c' invalid argument %s for %s invalid argument `%s' to `%s' invalid expression invalid expression; I was expecting to find a ')' somewhere but did not see one. invalid expression; empty parentheses are not allowed. invalid expression; expected to find a ')' but didn't see one.  Perhaps you need an extra predicate after '%s' invalid expression; you have too many ')' invalid expression; you have used a binary operator '%s' with nothing before it. invalid mode %s invalid null argument to -size invalid predicate -context: SELinux is not enabled. invalid predicate `%s' invalid suffix in %s%s argument '%s' locate database %s contains a filename longer than locate can handle locate database %s is corrupt or invalid locate database %s looks like an slocate database but it seems to have security level %c, which GNU findutils does not currently support memory exhausted missing argument to `%s' oops -- invalid default insertion of and! oops -- invalid expression type (%d)! oops -- invalid expression type! operators (decreasing precedence; -and is implicit where no others are given):
      ( EXPR )   ! EXPR   -not EXPR   EXPR1 -a EXPR2   EXPR1 -and EXPR2
      EXPR1 -o EXPR2   EXPR1 -or EXPR2   EXPR1 , EXPR2
 option --%s may not be set to a value which includes `=' paths must precede expression: %s positional options (always true): -daystart -follow -regextype

normal options (always true, specified before other expressions):
      -depth --help -maxdepth LEVELS -mindepth LEVELS -mount -noleaf
      --version -xdev -ignore_readdir_race -noignore_readdir_race
 read returned unexpected value %zu; this is probably a bug, please report it sanity check of the fnmatch() library function failed. single slocate security level %ld is unsupported. standard error standard output tests (N can be +N or -N or N): -amin N -anewer FILE -atime N -cmin N
      -cnewer FILE -ctime N -empty -false -fstype TYPE -gid N -group NAME
      -ilname PATTERN -iname PATTERN -inum N -iwholename PATTERN -iregex PATTERN
      -links N -lname PATTERN -mmin N -mtime N -name PATTERN -newer FILE time system call failed unable to allocate memory unable to record current working directory unexpected EOF in %s unexpected extra predicate unexpected extra predicate '%s' unknown unknown predicate `%s' unmatched %s quote; by default quotes are special to xargs unless you use the -0 option warning: -%s %s will not match anything because it ends with /. warning: Unix filenames usually don't contain slashes (though pathnames do).  That means that '%s %s' will probably evaluate to false all the time on this system.  You might find the '-wholename' test more useful, or perhaps '-samefile'.  Alternatively, if you are using GNU grep, you could use 'find ... -print0 | grep -FzZ %s'. warning: database %s is more than %d %s old (actual age is %.1f %s) warning: escape `\' followed by nothing at all warning: format directive `%%%c' should be followed by another character warning: not following the symbolic link %s warning: the -E option has no effect if -0 or -d is used.
 warning: the -d option is deprecated; please use -depth instead, because the latter is a POSIX-compliant feature. warning: the locate database can only be read from stdin once. warning: there is no entry in the predicate evaluation cost table for predicate %s; please report this as a bug warning: unrecognized escape `\%c' warning: unrecognized format directive `%%%c' warning: value %ld for -s option is too large, using %ld instead warning: you have specified a mode pattern %s (which is equivalent to /000). The meaning of -perm /000 has now been changed to be consistent with -perm -000; that is, while it used to match no files, it now matches all files. warning: you have specified the %s option after a non-option argument %s, but options are not positional (%s affects tests specified before it as well as those specified after it).  Please specify options before other arguments.
 write error you have too many ')' Project-Id-Version: findutils 4.5.15
Report-Msgid-Bugs-To: bug-findutils@gnu.org
POT-Creation-Date: 2015-12-27 20:34+0000
PO-Revision-Date: 2016-02-08 06:52+0000
Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>
Language-Team: German <translation-team-de@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 16:21+0000
X-Generator: Launchpad (build 18115)
Language: de
 
Ausführung von xargs wird nun fortgesetzt. Es wird versucht, dessen Eingabe zu lesen und Befehle auszuführen. Falls Sie dies nicht wollen, drücken Sie die Tastenkombination für end-of-file.
 
Lizenz GPLv3+: GNU GPL Version 3 oder neuer <http://gnu.org/licenses/gpl.html>
Dies ist freie Software: Es ist Ihnen freigestellt, sie zu verändern und zu verbreiten.
Es gibt in dem gesetzlich gegebenen Umfang KEINE GARANTIE.

 
Fehler bitte an <bug-findutils@gnu.org> melden.
Für die deutsche Übersetzung ist die Mailingliste
<translation-team-de@lists.sourceforge.net> zuständig.
 
Melden Sie Fehler an: %s
Melden Sie Übersetzungsfehler für findutils an <translation-team-de@lists.sourceforge.net>
 
Aktionen: -delete -print0 -printf FORMAT -fprintf DATEI FORMAT -print 
      -fprint0 DATEI -fprint DATEI -ls -fls DATEI -prune -quit
      -exec BEFEHL ; -exec BEFEHL {} + -ok BEFEHL ;
      -execdir BEFEHL ; -execdir BEFEHL {} + -okdir BEFEHL ;
 
Der Standardpfad ist das aktuelle Verzeichnis; der Standardausdruck ist -print
Der Ausdruck darf bestehen aus: Operatoren, Optionen, Tests und Aktionen:
       --help                   diese Hilfe anzeigen und beenden
       --process-slot-var=VAR   setzt die Umgebungsvariable VAR in Unterprozessen
       --show-limits            zeigt Begrenzungen der Befehlszeilenlänge an
       --version                Versionsinformation anzeigen und beenden
       -context KONTEXT
       -nouser -nogroup -path MUSTER -perm [-/]MODUS -regex MUSTER
      -readable -writable -executable
      -wholename MUSTER -size N[bcwkMG] -true -type [bcdpflsD] -uid N
      -used N -user NAME -xtype [bcdpfls]   -0, --null                   Objekte werden durch »null« und nicht durch Leer-
                                 zeichen getrennt; dies deaktiviert die Verarbeitung
                                 von Zitatzeichen, Backslashes und logischen Dateienden
                                 (EOF-Markierungen).
   -E ENDE                      setzt die logische EOF-Zeichenkette (Dateiende); falls
                                 ENDE als eine Zeile in der Eingabe auftritt, wird der
                                 Rest ignoriert (dies wird wiederum ignoriert, falls
                                 -0 oder -d angegeben war)
   -I R                         gleichbedeutend mit --replace=R
   -L, --max-lines=MAX-ZEILEN   höchstens MAX-ZEILEN als nicht-leere Eingabezeilen
                                 pro Befehlszeile verwenden
   -P, --max-procs=ANZAHL       höchstens diese ANZAHL Prozesse zugleich ausführen
   -a, --arg-file=DATEI         liest Argumente aus DATEI, nicht aus der
                                 Standardeingabe
   -d, --delimiter=ZEICHEN     Objekte in Eingabe-Datenströmen werden durch ZEICHEN
                                 getrennt und nicht durch Leerzeichen; dies deaktiviert
                                 die Verarbeitung von Zitatzeichen, Backslashes und
                                 logischen Dateienden (EOF-Markierungen).
   -e, --eof[=ENDE]             gleichbedeutend mit -E ENDE, sofern ENDE angegeben
                                 ist; anderenfalls gibt es keine Zeichenkette
                                 für end-of-file (EOF)
   -i, --replace[=R]            ersetzt R in INITIAL-ARGUMENTEN durch die aus der
                                 Standardeingabe gelesenen Namen; falls R nicht angegeben
                                 ist, wird {} angenommen
   -l[MAX-ZEILEN]               ähnlich wie -L, aber die Vorgabe ist höchstens eine
                                 leere Eingabezeile, wenn MAX-ZEILEN nicht angegeben ist.
   -P, --max-args=ANZAHL        höchstens diese ANZAHL Argumente in der
                                 Befehlszeile verwenden
   -p, --interactive            bittet vor der Ausführung von Befehlen um Bestätigung
   -r, --no-run-if-empty        falls keine Argumente angegeben sind, BEFEHL nicht
                                 ausführen; falls diese Option nicht angegeben ist,
                                 wird BEFEHL mindestens einmal ausgeführt
   -s, --max-chars=MAX-ZEICHEN  begrenzt die Länge der Befehlszeile auf MAX-ZEICHEN
   -t, --verbose                gibt Befehle vor der Ausführung aus
   -x, --exit                   beenden, falls die Größe überschritten ist
                                 (siehe -s)
 Webseite von %s: <%s>
 Webseite von %s: <http://www.gnu.org/software/%s/>
 %s ist eine slocate-Datenbank der nicht unterstützten Sicherheitsstufe %d, wird übersprungen. %s ist eine slocate-Datenbank. Die Unterstützung dafür ist neu, seien Sie auf Probleme gefasst. %s ist eine slocate-Datenbank. Die Option »-e« wird aktiviert. %s ist ein unbekannter Benutzername. %s ist kein existierender Gruppenname. %s ist kein existierender Gruppenname und scheint auch keine numerische Gruppenbezeichnung zu sein, da sie die unerwartete Endung %s aufweist. Prozess %s wurde durch das Signal %d abgebrochen. %s%s-Argument »%s« ist zu groß. %s%s wurde während des Ausführens von %s geändert (die alte Gerätenummer ist %ld, die neue Gerätenummer ist %ld, der Dateisystemtyp ist %s) [ref %ld] %s: mit Rückgabewert 255 beendet, Abbruch. %s: Ungültiger Wert für die Option »-%c«.
 %s: ungültige Option -- »%c«
 %s: Die Option »%c%s« erlaubt kein Argument.
 %s: Die Option »%s« ist mehrdeutig; Möglichkeiten: %s: Die Option »--%s« erlaubt kein Argument.
 %s: Die Option „--%s“ benötigt ein Argument.
 %s: Die Option »-W %s« erlaubt kein Argument.
 %s: Die Option »-W %s« ist mehrdeutig.
 %s: Die Option »%s« erfordert ein Argument.
 %s: Die Option benötigt ein Argument -- »%c«.
 %s: wurde durch das Signal %d angehalten. %s: wurde durch das Signal %d abgebrochen. %s: Unbekannte Option »%c%s«.
 %s: Unbekannte Option »--%s«.
 %s: Der Wert für die Option »-%c« sollte kleiner als oder gleich %ld sein.
 %s: Der Wert für die Option »-%c« muss größer oder gleich %ld sein.
 " (C) -type %c wird nicht unterstützt, da FIFOs auf der Plattform nicht unterstützt werden, auf der find kompiliert wurde. -type %c wird nicht unterstützt, da das Doors-Subsystem von Solaris auf der Plattform nicht unterstützt wird, auf der find kompiliert wurde. -type %c wird nicht unterstützt, da benannte Sockets auf der Plattform nicht unterstützt werden, auf der find kompiliert wurde. -type %c wird nicht unterstützt, da symbolische Links auf der Plattform nicht unterstützt werden, auf der find kompiliert wurde. < %s ... %s > ?  Alle Dateinamen: %s
 Das Argument von -type muss ein einzelner Buchstabe sein. Die Standardeingabe kann nicht geschlossen werden. Die Erstellungszeit der Datei %s konnte nicht ermittelt werden. Die Eingabedatei %s kann nicht geöffnet werden. Liste der eingehängten Geräte kann nicht gelesen werden. Liste der eingehängten Dateisysteme kann nicht gelesen werden Signalhandler SIGUSR1 kann nicht gesetzt werden SIGUSR2-Signalhandler kann nicht gesetzt werden Kompressionsverhältnis %4.2f%% (höher ist besser)
 Das Kompressionsverhältnis ist undefiniert.
 Die Datenbank %s liegt im %s-Format vor.
 Datenbank wurde zuletzt um %s.%09ld geändert Fehlendes Argument zur Option -D. Umgebungsvariable %s ist nicht auf eine gültige Dezimalzahl gesetzt Eric B. Decker Eine positive dezimale Ganzzahl wird als Argument von %s erwartet, aber %s wurde erhalten. Eine Ganzzahl wurde erwartet: %s Verzichten auf alle Privilegien gescheitert. Initialisieren der dateiübergreifenden Hash-Tabelle ist fehlgeschlagen Lesen aus der Standardeingabe gescheitert. Es konnte nicht sicher in das Verzeichnis %s gewechselt werden. Arbeitsverzeichnis konnte nicht gespeichert werden, um einen Befehl in %s auszuführen Schreiben der Ausgabe gescheitert (bei Stufe %d) Schreiben in die Eingabeaufforderung für -ok ist gescheitert. Schreiben in die Standardausgabe gescheitert. Schreiben in den Fehlerkanal der Standardausgabe gescheitert. Aktivierte Eigenschaften:  Dateideskriptor %d wird wird einen Überlauf verursachen. Bitte melden Sie dies als Fehler. Bitte fügen Sie eine detaillierte Beschreibung hinzu, wie dieser Fehler am einfachsten reproduziert werden kann. Die Dateinamen haben eine gesamte Länge von %s Byte.
Von diesen Dateinamen enthalten:

	%s Leerzeichen, 
	%s Zeilenumbrüche, 
	und %s enthalten Zeichen mit gesetztem hohem Bit.
 Dateisystemschleife erkannt; %s ist ein Teil der gleichen Schleife wie %s. Dateisystemschleife erkannt; %s hat die gleiche Gerätenummer und Inode wie ein %d Ebene höheres Verzeichnis in der Hierarchie. Dateisystemschleife erkannt; %s hat die gleiche Gerätenummer und Inode wie ein %d Ebenen höheres Verzeichnis in der Hierarchie. Allgemeine Hilfe zu GNU-Software: <http://www.gnu.org/gethelp/>
 %s konnte nicht als Datum oder Zeit interpretiert werden. Unerkanntes Debugsymbol %s wird ignoriert. In %s muss %s selbst erscheinen, aber Sie haben %s angegeben Ungültiges Argument %s für --max-database-age. Ungültiges Argument %s für -used. Ungültiges Argument »%s%s« für -size. Ungültige Escape-Sequenz %s in der Angabe des Eingabetrennzeichens. Ungültige Escape-Sequenz %s in der Angabe des Eingabetrennzeichens. Zeichenwerte dürfen %lo nicht übersteigen. Ungültige Escape-Sequenz %s in der Angabe des Eingabetrennzeichens. Zeichenwerte dürfen %lx nicht übersteigen. Ungültige Escape-Sequenz %s in der Angabe des Eingabetrennzeichens, angehängte Zeichen %s werden nicht anerkannt. Ungültige Angabe des Eingabetrennzeichens %s: Das Trennzeichen muss entweder ein einzelnes Zeichen oder eine mit \ beginnende Escape-Sequenz sein. Ungültige Optimierungsstufe %s. James Youngman Kevin Dalley Locate-Datenbankgröße: %s Byte
 Locate-Datenbankgröße: %s Bytes
 Obligatorische und optionale Argumente zu langen Optionen sind
ebenso obligatorisch oder optional für die entsprechenden
Kurzoptionen.
 Zutreffende Dateinamen: %s
 Die Datenbank %s im alten locate-Format ist zu kurz, um gültig zu sein Es wird nur eine Instanz von {} unterstützt mit -exec%s ... + Die Optimierungsstufe %lu ist zu hoch. Um schnell Dateien zu finden, ist GNU locate das bessere Werkzeug. Gepackt von %s
 Gepackt von %s (%s)
 Bitte eine Dezimalzahl unmittelbar nach -O angeben. Fehler in %s bitte melden an: %s
 Fehler (und Informationen über deren Abarbeitungsstand) können
auf der Fehlerberichtseite der findutils auf http://savannah.gnu.org/
eingegeben oder per E-Mail an <bug-findutils@gnu.org> gesendet werden. Der BEFEHL wird mit den INITIAL-ARGUMENTEN und weiteren aus der Eingabe gelesenen Argumenten ausgeführt.

 Sicherheitsstufe %s hat das unerwartete Suffix %s. Sicherheitsstufe %s ist außerhalb des konvertierbaren Bereichs. Da einige Dateinamen ausgefiltert sein können, ist es nicht möglich, das
Kompressionsverhältnis zu berechnen.
 Der symbolische Link %s bildet eine Schleife in der Verzeichnishierarchie. Das bezeichnete Verzeichnis wurde bereits durchsucht. Der Test %s erfordert ein Argument. Auf die Option -O muss eine Ganzzahl folgen. Die Option -delete aktiviert -depth automatisch, aber -prune ist wirkungslos, wenn -depth aktiv ist. In diesem Fall muss -depth zusätzlich angegeben werden. Die Option -show-control-chars erfordert entweder »literal« oder »safe« als Argument. Das Argument der Option --max-database-age darf nicht leer sein. Die Option -user erfordert ein Argument. Die Bibiliotheksfunktion atexit ist gescheitert. Das aktuelle Verzeichnis ist in der Umgebungsvariable PATH enthalten. Dies ist bei der Kombination mit der Aktion %s von find unsicher. Entfernen Sie bitte das aktuelle Verzeichnis aus der $PATH-Variable, indem Punkte, Doppel-Doppelpunkte oder führende oder abschließende Doppelpunkte gelöscht werden. Die Datenbank hat die Big-Endian-Bytereihenfolge der Kodierung der Maschinenwörter.
 Die Datenbank hat die Little-Endian-Bytereihenfolge der Kodierung der Maschinenwörter.
 Die Bytereihenfolge der Kodierung der Maschinenwörter in der Datenbank ist nicht offensichtlich.
 Der benötigte Umgebungsspeicher ist für exec() zu groß. Die Umgebungsvariable FIND_BLOCK_SIZE wird nicht unterstützt. Die einzige Variante zur Beeinflussung der Blockgröße ist die Umgebungsvariable POSIXLY_CORRECT. Der relative Pfad %s ist in der Umgebungsvariable PFAD enthalten. Dies ist bei Verwendung der Aktion %s von find unsicher. Entfernen Sie bitte diesen Eintrag aus $PATH. Dieses System stellt keine Funktion zum Ermitteln der Erstellungszeit der Datei bereit. Unerwartete Endung %s an %s. Unbekanntes Argument von -type: %c Unbekannter Typ %s für den regulären Ausdruck. Gültige Typen sind %s. Unbekannter Systemfehler. Aufruf: %s [--version | --help]
oder    %s most_common_bigrams < Dateiliste > Locate-Datenbank
 Aufruf: %s [-0 | --null] [--version] [--help]
 Aufruf: %s [-H] [-L] [-P] [-Olevel] [-D  Aufruf: %s [-d Pfad | --database=Pfad] [-e | -E | --[non-]existing]
      [-i | --ignore-case] [-w | --wholename] [-b | --basename] 
      [--limit=N | -l N] [-S | --statistics] [-0 | --null] [-c | --count]
      [-P | -H | --nofollow] [-L | --follow] [-m | --mmap ] [ -s | --stdio ]
      [-A | --all] [-p | --print] [-r | --regex ] [--regextype=TYP]
      [--max-database-age D] [--version] [--help]
      Muster...
 Aufruf: %s [OPTION]... BEFEHL [INITIAL-ARGUMENTE]...
 Gültige Argumente sind: WARNUNG: Verfolgung von %lu Kindprozessen ist verloren WARNUNG: Ein NUL-Zeichen trat in der Eingabe auf. Es kann nicht in die Argumentliste durchgegeben werden. Wollten Sie die Option --null verwenden? Warnung: Die Erstellungszeit der Datei %s ist nicht zu ermitteln. Warnung: Die Datei %s scheint die Zugriffsrechte 0000 zu haben. Warnung:  Das Dateisystem %s wurde gerade eingehängt. Warnung:  Das Dateisystem %s wurde gerade ausgehängt. Warnung:  Die locate-Datenbank %s wurde mit einer unterschiedlichen Bytereihenfolge erzeugt. Warnung: %s wird mindestens einmal ausgeführt. Wenn Sie dies nicht wollen, drücken Sie die Tastenkombination zum Abbrechen.
 Geschrieben von %s und %s.
 Geschrieben von %s, %s, %s,
%s, %s, %s, %s,
%s, %s und weiteren.
 Geschrieben von %s, %s, %s,
%s, %s, %s, %s,
%s und %s.
 Geschrieben von %s, %s, %s,
%s, %s, %s, %s
und %s.
 Geschrieben von %s, %s, %s,
%s, %s, %s und %s.
 Geschrieben von %s, %s, %s,
%s, %s und %s.
 Geschrieben von %s, %s, %s,
%s und %s.
 Geschrieben von %s, %s, %s
und %s.
 Geschrieben von %s, %s und %s.
 Geschrieben von %s.
 Innerhalb eines Befehlsnamens für -execdir und -okdir sollte {} nicht verwendet werden, da es ein mögliches Sicherheitsrisiko ist. Sie müssen eine Sicherheitsstufe als dezimale Ganzzahl angeben. Sie haben die Option -E angegeben, diese kann aber mit einer slocate-formatierten Datenbank einer von Null verschiedenen Sicherheitsstufe nicht verwendet werden. Für diese Datenbank werden keine Ergebnisse erstellt.
 ] [Pfad...] [Suchkriterium]
 ^[nN] ^[yY] " Mehrdeutiges Argument %s für %s. Die Argumentzeile ist zu lang. Die Argumentliste ist zu lang. Das Argument zu -group fehlt. Es muss ein Gruppenname sein. Arithmetischer Überlauf beim Berechnen des Tagesendes. Arithmetischer Überlauf beim Konvertieren von %s Tagen in deren Sekundenanzahl. exec() kann aufgrund von Einschränkungen der Argumentgröße nicht aufgerufen werden %s kann nicht gelöscht werden. Ein einzelnes Argument kann nicht innerhalb der Grenzen der Argumentliste eingefügt werden. Es konnte kein neuer Prozess gestartet werden. %s kann nicht gesucht werden Status des aktuellen Verzeichnisses kann nicht ermittelt werden. Der Befehl ist zu lang. Pipe vor dem Starten des Unterprozesses (fork) konnte nicht erzeugt werden Tage doppelte Der Umgebungsspeicher ist für »exec« nicht ausreichend. safe_read im errno-Puffer fehlgeschlagen in xargs_do_exec (dies ist möglicherweise ein Fehler, den Sie melden sollten) Fehler beim Datei schließen. Fehler beim Lesen eines Worts von %s. Fehler beim Warten auf %s Fehler beim Warten auf das Ende des Unterprozesses. Fehler: %s am Ende der Formatzeichenkette. Fehler:  Die Formatanweisung »%%%c« ist für zukünftige Anwendungen reserviert. Ein Ausdruck wurde nach »%s« erwartet. Ein Ausdruck wurde zwischen »%s« und »)« erwartet. Verzichten auf Gruppenprivilegien gescheitert. Verzichten auf Setgid-Privilegien gescheitert. Verzichten auf Setuid-Privilegien gescheitert. /dev/tty konnte nicht zum Lesen geöffnet werden Die Rückkehr zum Arbeitsverzeichnis war nach dem Suchen nach %s nicht mehr möglich. Ursprüngliches Arbeitsverzeichnis konnte nicht wiederhergestellt werden. zum übergeordneten Verzeichnis konnte nicht gewechselt werden Umgebungsvariable %s konnte nicht gesetzt werden Gesetzte Umgebungsvariable %s konnte nicht unwirksam gemacht werden getfilecon fehlgeschlagen: %s ungültiges %s%s-Argument »%s« Ungültige Einheit »%c« für »-size«. Ungültiges Argument %s für %s. Ungültiges Argument %s für »%s«. Ungültiger Ausdruck. ungültiger Ausdruck; »)« wurde erwartet, aber nicht gefunden. ungültiger Ausdruck; leere Klammern sind nicht erlaubt. Ungültiger Ausdruck: »)« konnte nicht gefunden werden. Möglicherweise wird nach »%s« ein zusätzliches Argument benötigt. Ungültiger Ausdruck; zu viele »)«. Ungültiger Ausdruck:  Ein binärer Operator »%s« wurde ohne Argument davor benutzt. Ungültiger Modus %s. »-size« erfordert ein Argument. Ungültige Eigenschaft -context: SELinux ist nicht aktiviert. ungültige Option »%s« Ungültiges Suffix im %s%s-Argument »%s«. Die locate-Datenbank %s enthält einen zu langen Dateinamen. Die locate-Datenbank %s ist beschädigt oder ungültig. Die locate-Datenbank %s sieht wie eine slocate-Datenbank aus, scheint aber Sicherheitsstufe %c zu haben, die durch die GNU findutils gegenwärtig nicht unterstützt wird. Der Arbeitsspeicher ist erschöpft. Fehlendes Argument für »%s«. Oops -- Das automatische Einfügen von »and« ist ungültig! Oops -- ungültiger Ausdruckstyp (%d)! Oops -- ungültiger Ausdruckstyp! Operatoren (abnehmende Priorität; -and ist eingeschlossen, wenn es nirgendwo anders angegeben ist):
      ( AUSDR ) ! AUSDR -not AUSDR AUSDR1 -a AUSDR2 AUSDR1 -and AUSDR2
      AUSDR1 -o AUSDR2 AUSDR1 -or AUSDR2 AUSDR1, AUSDR2
 Die Option --%s darf nicht auf einen Wert gesetzt werden, der ein »=« enthält. Der Pfad muss vor dem Ausdruck stehen: %s Positionsoptionen (immer wahr): -daystart -follow -regextype

Normale Optionen (immer wahr, vor anderen Optionen angegeben):
      -depth --help -maxdepth EBENEN -mindepth EBENEN -mount -noleaf
      --version -xdev -ignore_readdir_race -noignore_readdir_race
 Zurückgegebener unerwarteter Wert %zu wurde gelesen. Dies ist möglicherweise ein Fehler, den Sie melden sollten. Die Plausibilitätsprüfung der Bibiliotheksfunktion fnmatch() ist gescheitert. einfache Sicherheitsstufe %ld wird nicht unterstützt. Standard-Fehlerausgabe Standardausgabe Tests (N kann +N oder -N oder N sein): -amin N -anewer DATEI -atime N -cmin N
      -cnewer DATEI -ctime N -empty -false -fstype TYP -gid N -group NAME
      -ilname MUSTER -iname MUSTER -inum N -iwholename MUSTER -iregex MUSTER
      -links N -lname MUSTER -mmin N -mtime N -name MUSTER -newer DATEI time-Systemaufruf ist fehlgeschlagen Speicher konnte nicht zugewiesen werden. Aktuelles Arbeitsverzeichnis konnte nicht aufgezeichnet werden. Unerwartetes Dateiende (EOF) in %s. Ungültige zusätzliche Option. ungültige zusätzliche Option »%s« unbekannt unbekannte Option »%s« Fehlendes korrespondierendes Anführungszeichen %s; per Vorgabe sind Anführungszeichen für xargs bestimmt, sofern Sie nicht die Option -O verwenden Warnung: -%s %s wird keine Übereinstimmung finden, da es auf / endet. Warnung: Unix-Dateinamen enthalten gewöhnlich keine Schrägstriche (anders als Pfadbezeichnungen). Deshalb wird »%s %s« wahrscheinlich immer »Falsch« auf diesem System ergeben. Möglicherweise sind »-wholename« oder »-samefile« bessere Tests. Alternativ kann auch GNU grep verwendet werden:  »find ... -print0 | grep -FzZ %s«. Warnung: Die Datenbank %s ist älter als %d %s (das tatsächliche Alter ist %.1f %s). Warnung: Auf Escape-Zeichen »\« folgt nichts. Warnung: Auf die Formatanweisung »%%%c« sollte ein weiteres Zeichen folgen Warnung: Dem symbolischen Link %s wird nicht gefolgt. Warnung: Die Option -E ist wirkungslos, wenn -0 oder -d verwendet wird.
 Warnung:  Die Option -d soll nicht mehr verwendet werden. Statt dessen steht das POSIX-kompatible -depth zur Verfügung. Warnung: Die Locate-Datenbank kann nur einmalig aus der Standardeingabe gelesen werden. Warnung: Es gibt keinen Eintrag in der »predicate evaluation cost table« für Eigenschaft %s; bitte melden Sie dies als Fehler. Warnung: Unerkanntes Escape-Zeichen »\%c«. Warnung: Unerkannte Formatanweisung »%%%c«. Warnung: Der Wert %ld für die Option -s ist zu groß, stattdessen wird %ld verwendet. Warnung: Eine Bezeichnung für Zugriffsrechte %s wurde angegeben, die gleichbedeutend mit /000 ist. Die Bedeutung von -perm /000 wurde jetzt geändert, um mit der Verwendung von -perm -000 konsistent zu sein. Deshalb trifft es jetzt auf alle Dateien zu, vorher auf keine. Warnung: Sie haben die Option %s nach einem Argument %s angegeben, das keine Option ist, weil Optionen nicht positional sind (%s beeinflusst sowohl Tests davor als auch jene danach). Bitte geben Sie Optionen vor anderen Argumenten an.
 Schreibfehler. zu viele »)« PRIuMAX %s%s changed during execution of %s (old inode number %, new inode number %, file system type is %s) [ref %ld] WARNING: Hard link count is wrong for %s (saw only st_nlink=% but we already saw % subdirectories): this may be a bug in your file system driver.  Automatically turning on find's -noleaf option.  Earlier results may have failed to include directories that should have been searched. Your environment variables take up % bytes
 POSIX upper limit on argument length (this system): %
 POSIX smallest allowable upper limit on argument length (all systems): %
 Maximum length of command we could actually use: %
 Size of command buffer we are actually using: %
 Maximum parallelism (--max-procs must be no greater): %
 %s%s wurde während des Ausführens von %s geändert (die alte Inode-Nummer ist %, die neue Inode-Nummer ist %, der Dateisystemtyp ist %s) [ref %ld] Warnung: Die Anzahl der harten Links ist für %s falsch (Es wurden nur st_nlink=% erkannt, aber bereits % Unterverzeichnisse erkannt):  Das könnte durch einen Fehler im Dateisystemtreiber hervorgerufen worden sein.  Die Option -noleaf wurde automatisch aktiviert.  Vorhergehende Ergebnisse könnten Unterverzeichnisse übergangen haben. Die Umgebungsvariablen beanspruchen % Bytes.
 In POSIX erlaubte Obergrenze der Argumentlänge (für dieses System): %
 In POSIX erlaubte Obergrenze für die Argumentlänge (alle Systeme): %
 Maximale tatsächlich nutzbare Befehlslänge: %
 Größe des tatsächlich verwendeten Befehlspuffers: %
 Maximaler Parallelismus (--max-procs darf nicht größer sein): %
 