��    G     T    �$      �0  P   �0  9   *1  J   d1  A   �1  H   �1  +   :2     f2     �2  $   �2  %   �2  %   �2  "   
3  $   -3  6   R3  G   �3  B   �3     4     .4     H4  :   d4  :   �4  :   �4  9   5  G   O5  A   �5  O   �5  B   )6  8   l6  P   �6  J   �6  5   A7  :   w7  @   �7  Q   �7  E   E8  G   �8  9   �8  B   9  5   P9  0   �9  F   �9  7   �9  5   6:  K   l:  ,   �:  $   �:  (   
;  $   3;  $   X;  L   };  B   �;  ?   <  D   M<  2   �<  B   �<  1   =  L   :=  K   �=     �=  @   �=      2>  *   S>  4   ~>  N   �>  !   ?  G   $?  F   l?  K   �?  A   �?  K   A@  O   �@  U   �@      3A  K   TA  &   �A  *   �A  0   �A  5   #B  +   YB  *   �B  ,   �B  @   �B  P  C  #   oD  9   �D  .   �D     �D  C   E  N   WE  +   �E     �E      �E  =   F  %   MF  7   sF  #   �F     �F     �F  ;   	G     EG  )   UG      G      �G  1   �G  ?   �G     3H  ?   PH  "   �H     �H     �H  P   �H  Q   +I  P   }I  P   �I  
   J     *J  	   =J  
   GJ     RJ     YJ     aJ     hJ     nJ     �J     �J  	   �J     �J     �J     �J     �J  
   �J     K  ,   $K  ,   QK  ;   ~K  "   �K     �K     �K  -   L  !   :L     \L     uL     �L  /   �L  -   �L  -   �L  +   -M     YM     lM  '   �M     �M     �M     �M  1   �M  7   -N     eN     }N  !   �N     �N  /   �N  /   �N  /   +O     [O  4   zO     �O     �O  *   �O  %   
P  =   0P  1   nP     �P  9   �P  -   �P  %   &Q     LQ  ,   gQ  *   �Q  +   �Q  8   �Q     $R  #   9R     ]R     {R     �R     �R     �R  "   �R     �R     �R     S     6S     TS     rS  *   �S  +   �S     �S     �S     T     7T     TT  >   mT  /   �T  K   �T      (U  7   IU  $   �U  E   �U     �U     V  B   V     ]V  $   mV     �V  5   �V     �V     W     W     =W  1   RW     �W     �W     �W     �W     �W     �W     X  +   "X     NX  &   nX     �X  :   �X  4   �X  :   Y  0   ZY  -   �Y     �Y     �Y     �Y     Z     Z  G   +Z  }   sZ     �Z     �Z     [     [     .[     <[     Q[     l[     �[     �[  	   �[  
   �[     �[  	   �[     �[  %   �[     �[  b   \     r\     �\     �\     �\     �\  Q   �\  /   B]  #   r]     �]     �]  
   �]     �]     �]  ,   �]  
   ^     ^  
   ^     ^  	   %^  	   /^  
   9^     D^  '   V^     ~^     �^     �^     �^     �^  	   �^     �^     �^     �^     �^  M   �^  C   D_  r   �_  @   �_  O   <`  N   �`  c   �`     ?a  1   Za  '   �a  '   �a  7   �a  -   b     Bb  /   Sb     �b     �b     �b  "   �b     �b  	   �b  	   �b     �b     �b      �b  ,   c  -   Kc     yc     �c     �c     �c     �c     �c     �c  "   �c      d     d     d  +   2d  ,   ^d     �d     �d  #   �d     �d     �d     e     e  O   e  N   me     �e     �e     �e     f     f     f     9f     Rf     kf     �f     �f     �f     �f     �f  (   g     7g     <g     Eg  	   Lg  	   Vg     `g     eg     tg  
   |g     �g     �g     �g  -   �g  '   �g  "   h     /h     6h  W   Bh  y   �h     i  $   )i  &   Ni  $   ui  %   �i  "   �i     �i  
   j     j     j  2   $j     Wj     oj  %   �j     �j     �j     �j  $   �j     �j     �j  7   k  >   >k  <   }k  7   �k     �k     l      l     8l  	   Ml  	   Wl     al     hl     ol     xl     �l     �l     �l     �l     �l     �l  O   �l  N   Gm  l   �m  �   n     �n  	   �n  @   �n  <   �n  #   -o  #   Qo     uo     �o     �o     �o     �o     �o     �o     �o  W   �o  @   ,p  F   mp  $   �p  W   �p  2   1q  7   dq  +   �q  D   �q  H   r  N   Vr     �r     �r  *   �r  .   �r  /   )s  -   Ys  /   �s  1   �s  S   �s  .   =t     lt  &   �t  (   �t  
   �t     �t  
   �t  	   �t     �t     u     1u  ,   Ku  &   xu  !   �u      �u     �u      �u  3   v  '   Sv  )   {v  /   �v  #   �v     �v     w     4w     Iw  0   gw     �w     �w     �w     �w  @   �w     )x     :x     Kx     ]x     ex  
   �x     �x     �x     �x     �x  <   �x  7   y  ,   My  &   zy  '   �y  #   �y     �y  %   z  "   2z     Uz  #   sz  !   �z     �z  %   �z  "   �z     {  '   ={     e{     �{     �{     �{     �{     �{     |     3|     Q|     ]|  )   k|     �|     �|     �|     �|     �|     �|  U   	}  >   _}  !   �}     �}     �}  "   �}     ~     7~     D~  ,   R~     ~     �~     �~     �~  7   �~  +     (   J  "   s      �  *   �     �     �      �     @�  *   ^�  2   ��  .   ��  '   �  )   �  )   =�     g�     y�     ��     ��     ��     ́     ��     ��     �  )   %�     O�     ^�     u�  N   }�  !   ̂  "   �     �     /�     M�     k�  &   ��     ��  q  ƃ  P   8�  :   ��  P   ą  A   �  H   W�  +   ��     ̆     �  '   �  (   +�  (   T�  %   }�  '   ��  =   ˇ  E   	�  E   O�     ��     ��     ͈  3   �  3    �  7   T�  ?   ��  A   ̉  E   �  A   T�  C   ��  9   ڊ  C   �  P   X�  @   ��  C   �  A   .�  A   p�  M   ��  D    �  A   E�  I   ��  3   э  1   �  I   7�  8   ��  :   ��  J   ��  8   @�  #   y�  &   ��  $   ď  $   �  P   �  R   _�  A   ��  E   ��  7   :�  E   r�  6   ��  K   �  J   ;�     ��  >   ��      �  *   �  4   /�  S   d�  !   ��  H   ړ  M   #�  Q   q�  I   Ô  M   �  O   [�  M   ��  %   ��  J   �  &   j�  )   ��  1   ��  6   �  -   $�  *   R�  ,   }�  @   ��  i  �  (   U�  T   ~�  C   ә     �  ?   ,�  N   l�  2   ��  0   �  .   �  =   N�  &   ��  9   ��  "   �     �  "   +�  ;   N�     ��  )   ��  $   Ĝ  $   �  1   �  >   @�     �  ?   ��  "   ܝ     ��     �  U   *�  Q   ��  Z   Ҟ  O   -�  
   }�     ��  
   ��  
   ��     ��     ��     Ο  	   ՟     ߟ     ��     
�     �     �     5�     Q�     j�     w�  #   ��  6   ��  4   ߠ  G   �  &   \�      ��     ��  ;   á  $   ��  &   $�     K�     g�  *   y�  +   ��  +   Т  '   ��     $�  %   8�  (   ^�  "   ��     ��     ƣ  2   �  H   �     _�     x�  #   ��  "   ��  N   פ  N   &�  &   u�     ��  8   ��     ��  "   �  3   7�  6   k�  E   ��  7   �  &    �  T   G�  9   ��  7   ֧     �  N   (�  8   w�  7   ��  G   �     0�  "   M�  "   p�     ��  .   ��     �     ��  /   
�     :�     V�  !   q�     ��  %   ��  "   ت  4   ��  0   0�  #   a�     ��     ��  '   «     �  [    �  <   \�  U   ��  +   �  W   �  5   s�  Q   ��  #   ��     �  E   3�     y�  '   ��      ��  9   ծ     �  !   /�  8   Q�     ��  @   ��     �  2   ��  1   .�  /   `�     ��     ��     Ű  6   ݰ  4   �  F   I�  *   ��  M   ��  6   	�  =   @�  ?   ~�  =   ��     ��     �     6�     Q�     n�  Z   {�  �   ֳ     h�     o�     ��  "   ��     ��     ��     Ӵ     �      �     �  	   &�  
   0�     ;�  	   H�     R�  0   Y�     ��  d   ��     �     "�  #   :�     ^�     s�  C   ��  E   ̶  ,   �     ?�     M�  
   Q�     \�     r�  <   ��  
   ·     ͷ  
   ӷ     ޷     �  	   ��     	�     �  .   .�     ]�     w�     ��     ��     ��     ��     ��     ϸ     ݸ     �  1   ��  D   ,�  }   q�  B   �  O   2�  N   ��  i   Ѻ     ;�  1   [�  "   ��  "   ��  F   ӻ  /   �     J�  /   `�  	   ��  	   ��     ��  -   ��     ټ  	   �  	   �     ��  "   ��      �  4   ?�  5   t�     ��     ��     ٽ     �     ��     �     
�  '    �     H�     Q�     i�  6   ��  7   ��     �     	�  .   �  '   D�     l�     ��     ��  O   ��  N   �     =�  !   \�     ~�     ��     ��     ��     ��     ��     ��     �     )�     D�     b�     ��  0   ��     ��     ��     ��     ��  	   ��  
   ��     ��     �  
   �     �  	   7�     A�  7   S�  0   ��  +   ��     ��     ��  Y   ��  {   U�     ��  %   ��  2   �  -   D�  .   r�  +   ��  %   ��  
   ��     ��     �  &   �     <�     V�  *   r�     ��     ��     ��  -   ��     ��     ��  7   �  C   =�  <   ��  L   ��     �     !�     ?�     [�     t�     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     �  O   3�  N   ��  s   ��  �   F�  $   ��  	   ��  C   ��  <   <�  &   y�  #   ��     ��     ��  	   ��     ��     ��  	   ��  !   ��     �  ;   2�  A   n�  I   ��  &   ��  Z   !�  3   |�  ;   ��  /   ��  L   �  N   i�  O   ��     �     �  9   ,�  <   f�  =   ��  ;   ��  =   �  ?   [�  q   ��  C   �     Q�  1   n�  5   ��     ��     ��     ��     �     �     )�      A�  )   b�  +   ��  #   ��  "   ��  !   ��  &   !�  ?   H�  :   ��  -   ��  0   ��  8   "�      [�  +   |�  $   ��      ��  J   ��  !   9�     [�     l�     ��  Q   ��     ��     ��     �  	   �  %    �     F�     R�     c�     {�     ��  X   ��  B    �  B   C�  %   ��  *   ��  '   ��  $   ��  *   $�  &   O�  #   v�  &   ��  &   ��  "   ��  (   �  '   4�  #   \�  (   ��  $   ��      ��  (   ��  (   �  (   A�     j�     ��     ��     ��     ��  /   ��     �     /�     A�     I�     \�  $   q�  U   ��  >   ��  #   +�     O�  &   k�  +   ��     ��     ��     ��  .   ��     .�     J�  "   h�      ��  6   ��  /   ��  1   �  $   E�  )   j�  *   ��  #   ��     ��      �     $�  0   B�  J   s�  ;   ��  .   ��  1   )�  1   [�     ��     ��     ��  $   ��     ��     �     '�     ?�     X�  *   p�     ��     ��  	   ��  U   ��  $   0�  $   U�      z�      ��     ��  !   ��  +   ��     *�           Q  7     ~   �         �       r                 |        H       3      �  �     �          �   D      �  [    �      �   C    �      e     �   "  o  �   (      �  '  �  6   �  J      p   �     /  �   �     ?      �  �  h     X  C       �       �   �   8           �  �   +       0      �  �  �  �  �  k  �      >   �              E  1       �   �   �   <   z  �     �             �  N   y   �       c  �  �      #     t  O       �   c   T   �   �   �    h   B      �   |       �   -          �   �   �      :   �    u     *  G   �           �   
  2  �   �      2  )            �       �  A   +  �            X                 �     0   u  _  �  �  
  n       ;  �   *   �  L       b   n  �  \  O              �  M        ?  ,   .     )  �           �  �  9  �          �  �                              �   b  E   �   G  �      "  w   $      �      ?      w  �   �   �       �   =      �  -           �      0  C  �           R   G      {       �  �   �   �  +  �     �   4  �       �     @   �      �  �       �  ~      �      �      Z  �   �   �  �  j     �  �  <  �     �   g  �  *  �   ;   �   �                       H  �   �     i   #  T  �   �  l  �  �   x       �  �          `       �   �   '   �  K  �   �  �  �  �  &  6  �   y  v       �       9   �           �    8  E  �  :          �  �  S  �                 �        t           3  �   �  �  f   F       �              U       '                      W       �   ^           @  �            m   �  5  %  
   �                  3       �     �     A  6  D  �   [   d        �   Y  �      F  �   �   �           �          �   	   Z   �   �       �  �  d             �          �   4   Q   �  �  �       g           p      �  R  �       B  �   �  �   �           �   �  U  �  �  W  �   �  �       �   I      2   �           �   .     r                �  P       �     f          �   N      =     5   "   v  $       :  �  _       �  B     �           !      �      �  �            (                    �      �   �   (     �  �  �            {   �   %              �    �         %   L  ]     D   �       F  �  �       �  k       V   $          5         .  !   �      �       >  /  ;  V  K   �   �  �         e   �  Y       �  1      �       �  �  �      �  �  �                 �  �  !  l   �      -  j           &   @  I     �   �   1  J       x  �  �   �  �   7  �             �   o   P  m  �   �  �                 }  a  �   4      /   s   �    �  �   ,      ]  �       7  >      �  s            �     �       \       8      �   S   �   �  �  q   �   <             �      �           �   a   �   `      �     i          &  	  	     z   ^  �   �    q  #   �  A      =  �   9      �   M   �   }   �      ,  )       �     �  �    �                  �       
Proto Recv-Q Send-Q Local Address           Foreign Address         State       
Proto RefCnt Flags       Type       State         I-Node 
Unless you are using bind or NIS for host lookups you can change the DNS
                               [mod] [dyn] [reinstate] [[dev] If]
                               [netmask N] [mss Mss] [window W] [irtt I]
                   [nibble NN] [trigger NN]
              compressed:%lu
           %s addr:%s            EtherTalk Phase 2 addr:%s
           IPX/Ethernet 802.2 addr:%s
           IPX/Ethernet 802.3 addr:%s
           IPX/Ethernet II addr:%s
           IPX/Ethernet SNAP addr:%s
           [ [i|o]seq ] [ [i|o]key KEY ] [ [i|o]csum ]
           [ mode { ipip | gre | sit } ] [ remote ADDR ] [ local ADDR ]
           [ ttl TTL ] [ tos TOS ] [ nopmtudisc ] [ dev PHYS_DEV ]
           collisions:%lu            econet addr:%s
           inet6 addr: %s/%d         --numeric-hosts          don't resolve host names
         --numeric-ports          don't resolve port names
         --numeric-users          don't resolve user names
         -A, -p, --protocol       specify protocol family
         -C, --cache              display routing cache instead of FIB

         -D, --use-device         read <hwaddr> from given device
         -F, --fib                display Forwarding Information Base (default)
         -M, --masquerade         display masqueraded connections

         -N, --symbolic           resolve hardware names
         -a                       display (all) hosts in alternative (BSD) style
         -a, --all, --listening   display all sockets (default: connected)
         -c, --continuous         continuous listing

         -d, --delete             delete a specified entry
         -e, --extend             display other/more information
         -f, --file               read new entries from file or from /etc/ethers

         -g, --groups             display multicast group memberships
         -i, --device             specify network interface (e.g. eth0)
         -i, --interfaces         display interface table
         -l, --listening          display listening server sockets
         -n, --numeric            don't resolve names
         -o, --timers             display timers
         -p, --programs           display PID/Program name for sockets
         -r, --route              display routing table
         -s, --set                set a new ARP entry
         -s, --statistics         display networking statistics (like SNMP)
         -v, --verbose            be verbose
        ADDR := { IP_ADDRESS | any }
        KEY  := { DOTTED_QUAD | NUMBER }
        TOS  := { NUMBER | inherit }
        TTL  := { 1..255 | inherit }
        domainname [-v] {nisdomain|-F file}   set NIS domainname (from file)
        hostname -V|--version|-h|--help       print info and exit

        hostname [-v]                         display hostname

        hostname [-v] [-d|-f|-s|-a|-i|-y|-n]  display formatted name
        inet6_route [-FC] flush      NOT supported
        inet6_route [-vF] add Target [gw Gw] [metric M] [[dev] If]
        inet_route [-FC] flush      NOT supported
        inet_route [-vF] add {-host|-net} Target[/prefix] [gw Gw] [metric M]
        inet_route [-vF] add {-host|-net} Target[/prefix] [metric M] reject
        ipmaddr -V | -version
        ipmaddr show [ dev STRING ] [ ipv4 | ipv6 | link | all ]
        iptunnel -V | --version

        netstat [-vnNcaeol] [<Socket> ...]
        netstat { [-veenNac] -i | [-cnNe] -M | -s }

        nodename [-v] {nodename|-F file}      set DECnet node name (from file)
        plipconfig -V | --version
        rarp -V                               display program version.

        rarp -d <hostname>                    delete entry from cache.
        rarp -f                               add entries from /etc/ethers.
        rarp [<HW>] -s <hostname> <hwaddr>    add entry to cache.
        route [-v] [-FC] {add|del|flush} ...  Modify routing table for AF.

        route {-V|--version}                  Display version/author and exit.

        route {-h|--help} [<AF>]              Detailed usage syntax for specified AF.
      - no statistics available -     -F, --file            read hostname or NIS domainname from given file

     -a, --alias           alias names
     -d, --domain          DNS domain name
     -f, --fqdn, --long    long host name (FQDN)
     -i, --ip-address      addresses for the hostname
     -n, --node            DECnet node name
     -s, --short           short host name
     -y, --yp, --nis       NIS/YP domainname
     dnsdomainname=hostname -d, {yp,nis,}domainname=hostname -y

    This command can read or set the hostname or the NIS domainname. You can
   also read the DNS domain or the FQDN (fully qualified domain name).
   Unless you are using bind or NIS for host lookups you can change the
   FQDN (Fully Qualified Domain Name) and the DNS domain name (which is
   part of the FQDN) in the /etc/hosts file.
   <AF>=Address family. Default: %s
   <AF>=Use '-6|-4' or '-A <af>' or '--<af>'; default: %s
   <AF>=Use '-A <af>' or '--<af>'; default: %s
   <HW>=Hardware Type.
   <HW>=Use '-H <hw>' to specify hardware address type. Default: %s
   <Socket>={-t|--tcp} {-u|--udp} {-w|--raw} {-x|--unix} --ax25 --ipx --netrom
   Checksum in received packet is required.
   Checksum output packets.
   Drop packets out of sequence.
   List of possible address families (which support routing):
   List of possible address families:
   List of possible hardware types (which support ARP):
   List of possible hardware types:
   Outfill:%d  Keepalive:%d   Sequence packets on output.
   [[-]broadcast [<address>]]  [[-]pointopoint [<address>]]
   [[-]dynamic]
   [[-]trailers]  [[-]arp]  [[-]allmulti]
   [add <address>[/<prefixlen>]]
   [del <address>[/<prefixlen>]]
   [hw <HW> <address>]  [metric <NN>]  [mtu <NN>]
   [mem_start <NN>]  [io_addr <NN>]  [irq <NN>]  [media <type>]
   [multicast]  [[-]promisc]
   [netmask <address>]  [dstaddr <address>]  [tunnel <address>]
   [outfill <NN>] [keepalive <NN>]
   [txqueuelen <NN>]
   [up|down] ...

   arp [-v]          [-i <if>] -d  <host> [pub]               <-Delete ARP entry
   arp [-v]   [<HW>] [-i <if>] -Ds <host> <if> [netmask <nm>] pub          <-''-

   arp [-v]   [<HW>] [-i <if>] -s  <host> <hwaddr> [temp]            <-Add entry
   arp [-vnD] [<HW>] [-i <if>] -f  [<filename>]            <-Add entry from file
  Bcast:%s   MTU:%d  Metric:%d  Mask:%s
  P-t-P:%s   Path
  Scope:  Timer  User  User       Inode       interface %s
  on %s  users %d %-9.9s Link encap:%s   %s	nibble %lu  trigger %lu
 %s (%s) -- no entry
 %s (%s) at  %s started %s: %s/ip  remote %s  local %s  %s: ERROR while getting interface flags: %s
 %s: ERROR while testing interface flags: %s
 %s: You can't change the DNS domain name with this command
 %s: address family not supported!
 %s: bad hardware address
 %s: can't open `%s'
 %s: error fetching interface information: %s
 %s: hardware type not supported!
 %s: illegal option mix.
 %s: invalid %s address.
 %s: name too long
 %s: you must be root to change the domain name
 %s: you must be root to change the host name
 %s: you must be root to change the node name
 %u DSACKs for out of order packets received %u DSACKs received %u DSACKs sent for old packets %u DSACKs sent for out of order packets %u ICMP messages failed %u ICMP messages received %u ICMP messages sent %u ICMP packets dropped because socket was locked %u ICMP packets dropped because they were out-of-window %u SYN cookies received %u SYN cookies sent %u SYNs to LISTEN sockets ignored %u TCP data loss events %u TCP sockets finished time wait in fast timer %u TCP sockets finished time wait in slow timer %u acknowledgments not containing data received %u active connections openings %u active connections rejected because of time stamp %u bad SACKs received %u bad segments received. %u congestion window recovered using DSACK %u congestion windows fully recovered %u congestion windows partially recovered using Hoe heuristic %u congestion windows recovered after partial ack %u connection resets received %u connections aborted after user close in linger timeout %u connections aborted due to memory pressure %u connections aborted due to timeout %u connections established %u connections reset due to early user close %u connections reset due to unexpected SYN %u connections reset due to unexpected data %u delayed acks further delayed because of locked socket %u delayed acks sent %u dropped because of missing route %u failed connection attempts %u fast retransmits %u forward retransmits %u forwarded %u fragments created %u fragments dropped after timeout %u fragments failed %u fragments received ok %u incoming packets delivered %u incoming packets discarded %u input ICMP message failed. %u invalid SYN cookies received %u of bytes directly received from backlog %u of bytes directly received from prequeue %u other TCP timeouts %u outgoing packets dropped %u packet headers predicted %u packet reassembles failed %u packet receive errors %u packets collapsed in receive queue due to low socket buffer %u packets directly queued to recvmsg prequeue. %u packets dropped from out-of-order queue because of socket buffer overrun %u packets dropped from prequeue %u packets header predicted and directly queued to user %u packets pruned from receive queue %u packets pruned from receive queue because of socket buffer overrun %u packets reassembled ok %u packets received %u packets rejects in established connections because of timestamp %u packets sent %u packets to unknown port received. %u passive connection openings %u passive connections rejected because of time stamp %u predicted acknowledgments %u reassemblies required %u reno fast retransmits failed %u requests sent out %u resets received for embryonic SYN_RECV sockets %u resets sent %u retransmits in slow start %u retransmits lost %u sack retransmits failed %u segments received %u segments retransmited %u segments send out %u time wait sockets recycled by time stamp %u timeouts after SACK recovery %u timeouts after reno fast retransmit %u timeouts in loss state %u times receiver scheduled too late for direct processing %u times recovered from packet loss due to SACK data %u times recovered from packet loss due to fast retransmit %u times the listen queue of a socket overflowed %u times unabled to send RST due to no memory %u total packets received %u with invalid addresses %u with invalid headers %u with unknown protocol (Cisco)-HDLC (No info could be read for "-p": geteuid()=%d but you should be root.)
 (Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
 (auto) (incomplete) (only servers) (servers and established) (w/o servers) 16/4 Mbps Token Ring 16/4 Mbps Token Ring (New) 6-bit Serial Line IP <from_interface> <incomplete>  ALLMULTI  AMPR AX.25 AMPR NET/ROM AMPR ROSE ARCnet AX.25 not configured in this system.
 Active AX.25 sockets
 Active IPX sockets
Proto Recv-Q Send-Q Local Address              Foreign Address            State Active Internet connections  Active NET/ROM sockets
 Active UNIX domain sockets  Active X.25 sockets
 Adaptive Serial Line IP Address                  HWtype  HWaddress           Flags Mask            Iface
 Address deletion not supported on this system.
 Address family `%s' not supported.
 Appletalk DDP Ash BROADCAST  Bad address.
 Base address:0x%x  Broadcast tunnel requires a source address.
 CCITT X.25 CLOSE CLOSE_WAIT CLOSING CONN SENT CONNECTED CONNECTING Callsign too long Cannot change line discipline to `%s'.
 Cannot create socket Compat DARPA Internet DEBUG  DGRAM DISC SENT DISCONNECTING DMA chan:%x  DYNAMIC  Default TTL is %u Dest         Source          Device  LCI  State        Vr/Vs  Send-Q  Recv-Q
 Dest       Source     Device  State        Vr/Vs    Send-Q  Recv-Q
 Destination                                 Next Hop                                Flags Metric Ref    Use Iface
 Destination               Router Net                Router Node
 Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
 Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
 Destination     Gateway         Genmask         Flags Metric Ref    Use Iface    MSS   Window irtt
 Destination  Iface    Use
 Destination  Mnemonic  Quality  Neighbour  Iface
 Detected reordering %u times using FACK Detected reordering %u times using SACK Detected reordering %u times using reno fast retransmit Detected reordering %u times using time stamp Device not found Don't know how to set addresses for family %d.
 ESTAB ESTABLISHED Econet Entries: %d	Skipped: %d	Found: %d
 Ethernet FIN_WAIT1 FIN_WAIT2 FREE Failed to get type of [%s]
 Fiber Distributed Data Interface Flushing `inet' routing table not supported
 Flushing `inet6' routing table not supported
 Forwarding is %s Frame Relay Access Device Frame Relay DLCI Generic EUI-64 Global HIPPI HWaddr %s   Hardware type `%s' not supported.
 Host ICMP input histogram: ICMP output histogram: INET (IPv4) not configured in this system.
 INET6 (IPv6) not configured in this system.
 IP masquerading entries
 IPIP Tunnel IPX not configured in this system.
 IPX: this needs to be written
 IPv4 Group Memberships
 IPv6 IPv6-in-IPv4 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR    TX-OK TX-ERR TX-DRP TX-OVR Flg
 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR   TX-OK TX-ERR TX-DRP TX-OVR Flg
 Interface       RefCnt Group
 Interface %s not initialized
 Interrupt:%d  Invalid callsign IrLAP Kernel AX.25 routing table
 Kernel IP routing cache
 Kernel IP routing table
 Kernel IPX routing table
 Kernel IPv6 Neighbour Cache
 Kernel IPv6 routing table
 Kernel Interface table
 Kernel NET/ROM routing table
 Kernel ROSE routing table
 Keys are not allowed with ipip and sit.
 LAPB LAST_ACK LISTEN LISTENING LOOPBACK  Link Local Loopback MASTER  MULTICAST  Malformed Ash address Media:%s Memory:%lx-%lx  Modifying `inet' routing cache not supported
 NET/ROM not configured in this system.
 NET/ROM: this needs to be written
 NOARP  NOTRAILERS  Neighbour                                   HW Address        Iface    Flags Ref State
 Neighbour                                   HW Address        Iface    Flags Ref State            Stale(sec) Delete(sec)
 No ARP entry for %s
 No routing for address family `%s'.
 No support for ECONET on this system.
 No support for INET on this system.
 No support for INET6 on this system.
 No usable address families found.
 Node address must be ten digits Novell IPX POINTOPOINT  PROMISC  Please don't supply more than one address family.
 Point-to-Point Protocol Problem reading data from %s
 Quick ack mode was activated %u times RAW RDM RECOVERY ROSE not configured in this system.
 RTO algorithm is %s RUNNING  RX bytes:%llu (%lu.%lu %s)  TX bytes:%llu (%lu.%lu %s)
 RX packets:%llu errors:%lu dropped:%lu overruns:%lu frame:%lu
 RX: Packets    Bytes        Errors CsumErrs OutOfSeq Mcasts
 Ran %u times out of system memory during packet sending Resolving `%s' ...
 Result: h_addr_list=`%s'
 Result: h_aliases=`%s'
 Result: h_name=`%s'
 SABM SENT SEQPACKET SLAVE  STREAM SYN_RECV SYN_SENT Serial Line IP Setting domainname to `%s'
 Setting hostname to `%s'
 Setting nodename to `%s'
 Site Sorry, use pppd!
 Source          Destination     Gateway         Flags   MSS Window  irtt Iface
 Source          Destination     Gateway         Flags Metric Ref    Use Iface
 Source          Destination     Gateway         Flags Metric Ref    Use Iface    MSS   Window irtt  HH  Arp
 Source          Destination     Gateway         Flags Metric Ref    Use Iface    MSS   Window irtt  TOS HHRef HHUptod     SpecDst
 TCP ran low on memory %u times TIME_WAIT TX packets:%llu errors:%lu dropped:%lu overruns:%lu carrier:%lu
 TX: Packets    Bytes        Errors DeadLoop NoRoute  NoBufs
 This kernel does not support RARP.
 Too much address family arguments.
 UNIX Domain UNK. UNKNOWN UNSPEC UP  Unknown Unknown address family `%s'.
 Unknown media type.
 Usage:
  arp [-vn]  [<HW>] [-i <if>] [-a] [<hostname>]             <-Display ARP cache
 Usage:
  ifconfig [-a] [-v] [-s] <interface> [[<AF>] <address>]
 Usage: hostname [-v] {hostname|-F file}      set hostname (from file)
 Usage: inet6_route [-vF] del Target
 Usage: inet_route [-vF] del {-host|-net} Target[/prefix] [gw Gw] [metric M] [[dev] If]
 Usage: ipmaddr [ add | del ] MULTIADDR dev STRING
 Usage: iptunnel { add | change | del | show } [ NAME ]
 Usage: plipconfig [-a] [-i] [-v] interface
 Usage: rarp -a                               list entries in cache.
 Usage: route [-nNvee] [-FC] [<AF>]           List kernel routing tables
 User       Dest       Source     Device  State        Vr/Vs    Send-Q  Recv-Q
 VJ 6-bit Serial Line IP VJ Serial Line IP WARNING: at least one error occured. (%d)
 Warning: Interface %s still in ALLMULTI mode.
 Warning: Interface %s still in BROADCAST mode.
 Warning: Interface %s still in DYNAMIC mode.
 Warning: Interface %s still in MULTICAST mode.
 Warning: Interface %s still in POINTOPOINT mode.
 Warning: Interface %s still in promisc mode... maybe other application is running?
 Warning: cannot open %s (%s). Limited output.
 Where: NAME := STRING
 Wrong format of /proc/net/dev. Sorry.
 You cannot start PPP with this program.
 [NO FLAGS] [NO FLAGS]  [NONE SET] [UNKNOWN] address mask replies: %u address mask request: %u address mask requests: %u arp: %s: hardware type without ARP support.
 arp: %s: kernel only supports 'inet'.
 arp: %s: unknown address family.
 arp: %s: unknown hardware type.
 arp: -N not yet supported.
 arp: cannot open etherfile %s !
 arp: cannot set entry on line %u of etherfile %s !
 arp: cant get HW-Address for `%s': %s.
 arp: device `%s' has HW address %s `%s'.
 arp: format error on line %u of etherfile %s !
 arp: in %d entries no match found.
 arp: invalid hardware address
 arp: need hardware address
 arp: need host name
 arp: protocol type mismatch.
 cannot determine tunnel mode (ipip, gre or sit)
 cannot open /proc/net/snmp compressed:%lu  destination unreachable: %u disabled domain name (which is part of the FQDN) in the /etc/hosts file.
 echo replies: %u echo request: %u echo requests: %u enabled error parsing /proc/net/snmp family %d  generic X.25 getdomainname()=`%s'
 gethostname()=`%s'
 getnodename()=`%s'
 hw address type `%s' has no handler to set address. failed.
 ifconfig: Cannot set address for this protocol family.
 ifconfig: `--help' gives usage information.
 ifconfig: option `%s' not recognised.
 in_arcnet(%s): invalid arcnet address!
 in_arcnet(%s): trailing : ignored!
 in_arcnet(%s): trailing junk!
 in_ether(%s): invalid ether address!
 in_ether(%s): trailing : ignored!
 in_ether(%s): trailing junk!
 in_fddi(%s): invalid fddi address!
 in_fddi(%s): trailing : ignored!
 in_fddi(%s): trailing junk!
 in_hippi(%s): invalid hippi address!
 in_hippi(%s): trailing : ignored!
 in_hippi(%s): trailing junk!
 in_tr(%s): invalid token ring address!
 in_tr(%s): trailing : ignored!
 in_tr(%s): trailing junk!
 ip: %s is invalid IPv4 address
 ip: %s is invalid inet address
 ip: %s is invalid inet prefix
 ip: argument is wrong: %s
 keepalive (%2.2f/%ld/%d) missing interface information netmask %s  netrom usage
 netstat: unsupported address family %d !
 no RARP entry for %s.
 off (0.00/%ld/%d) on %s
 on (%2.2f/%ld/%d) on%d (%2.2f/%ld/%d) problem reading data from %s
 prot   expire    initseq delta prevd source               destination          ports
 prot   expire source               destination          ports
 rarp: %s: unknown hardware type.
 rarp: %s: unknown host
 rarp: cannot open file %s:%s.
 rarp: cannot set entry from %s:%u
 rarp: format error at %s:%u
 redirect: %u redirects: %u route: %s: cannot use a NETWORK as gateway!
 route: Invalid MSS/MTU.
 route: Invalid initial rtt.
 route: Invalid window.
 route: bogus netmask %s
 route: netmask %.8x doesn't make sense with host route
 route: netmask doesn't match route address
 rresolve: unsupport address family %d !
 slattach: /dev/%s already locked!
 slattach: cannot write PID file
 slattach: setvbuf(stdout,0,_IOLBF,0) : %s
 slattach: tty name too long
 slattach: tty_hangup(DROP): %s
 slattach: tty_hangup(RAISE): %s
 slattach: tty_lock: (%s): %s
 slattach: tty_lock: UUCP user %s unknown!
 slattach: tty_open: cannot get current line disc!
 slattach: tty_open: cannot get current state!
 slattach: tty_open: cannot set %s bps!
 slattach: tty_open: cannot set 8N1 mode!
 slattach: tty_open: cannot set RAW mode!
 source quench: %u source quenches: %u time exceeded: %u timeout in transit: %u timestamp replies: %u timestamp reply: %u timestamp request: %u timestamp requests: %u timewait (%2.2f/%ld/%d) ttl != 0 and noptmudisc are incompatible
 txqueuelen:%d  unkn-%d (%2.2f/%ld/%d) unknown usage: netstat [-veenNcCF] [<Af>] -r         netstat {-V|--version|-h|--help}
 warning, got bogus igmp line %d.
 warning, got bogus igmp6 line %d.
 warning, got bogus raw line.
 warning, got bogus tcp line.
 warning, got bogus udp line.
 warning, got bogus unix line.
 warning: no inet socket available: %s
 wrong parameters: %u Project-Id-Version: net-tools 1.51
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-06-30 12:28+0900
PO-Revision-Date: 2015-12-04 23:55+0000
Last-Translator: Ralf Bächle <Unknown>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:13+0000
X-Generator: Launchpad (build 18115)
 
Proto Recv-Q Send-Q Local Address           Foreign Address         State       
Proto RefZäh Flaggen     Typ        Zustand       I-Node 
Wenn Bind oder NIS nicht zur Hostnamensauflösung benutzt werden, kann der DNS
                               [mod] [dyn] [reinstate] [[dev] If]
                               [netmask N] [mss Mss] [window W] [irtt I]
                   [nibble NN] [trigger NN]
              komprimiert:%lu
           %s Adresse:%s            EtherTalk Phase 2 Adresse:%s
           IPX/Ethernet 802.2 Adresse:%s
           IPX/Ethernet 802.3 Adresse:%s
           IPX/Ethernet II Adresse:%s
           IPX/Ethernet SNAP Adresse:%s
           [ [i|o]seq ] [ [i|o]key SCHLÜSSEL ] [ [i|o]csum ]
           [ mode { ipip | gre | sit } ] [ remote ADR ] [ local ADR ]
           [ ttl TTL ] [ tos TOS ] [ nopmtudisc ] [ dev PHYS_GERÄt ]
           Kollisionen:%lu            econet Adresse:%s
           inet6-Adresse: %s/%d         --numeric-hosts Host-Namen nicht auflösen
         --numeric-ports Port-Namen nicht auflösen
         --numeric-users Benutzer-Namen nicht auflösen
         -A, -p, --protocol              Routentabelle anzeigen
         -C, --cache              Routencache statt FIB anzeigen

         -D, --use-device         <hwaddr> von gegebenem Gerät lesen
         -F, --fib Forwarding Infomation Base anzeigen (Standard)
         -M, --masquerade         Maskierte Verbindungen auflisten

         -N, --symbolic           Hardwarenamen auflösen
         -a                       Alle Hosts im BSD-Format anzeigen
         -a, --all, --listening   Alle Sockets anzeigen (normal: nur verbundene)
         -c, --continuous         Anzeige laufend aktualisieren

         -d, --delete             Einen bestimmten Eintrag löschen
         -e, --extend Weitere/zusätzliche Informationen anzeigen
         -f, --file               Neue Einträge aus Datei lesen

         -g, --groups             Mitgliedschaft in Multicastgruppen anzeigen
         -i, --device             Netzwerkgerät (z.B. eth0) angeben
         -i, --interfaces         Schnittstellentabelle auflisten
         -l, --listening          Empfangsbereite Serversockets auflisten
         -n, --numeric Rechnernamen nicht auflösen
         -o, --timers             Timer auflisten
         -p, --programs           PID/Programmnamen für Sockets anzeigen
         -r, --route              Routentabelle anzeigen
         -s, --set                Neuen ARP-Eintrag setzen
         -s, --statistics         Netzwerksstatistiken anzeigen (wie SNMP)
         -v, --verbose            Ausführliche Ausgaben
        ADR := { IP_ADRESSE | any }
        KEY  := { DOTTED_QUAD | ZAHL }
        TOS  := { NUMBER | inherit }
        TTL  := { 1..255 | inherit }
        domainname [-v] {nisdomain|-F file}   NIS-Domainname (aus Datei) setzen.
        hostname -V|--version|-h|--help       Information ausdrucken und beenden.

        hostname [-v]                         Hostnamen anzeigen

        hostname [-v] [-d|-f|-s|-a|-i|-y|-n] Zeigt formatierten Namen
        inet6_route [-FC] flush      NICHT unterstützt
        inet6_route [-vF] add Ziel [gw Gateway] [metric M] [[dev] If]
        inet_route [-FC] flush      NICHT unterstützt
        inet_route [-vF] add {-host|-net} Ziel[/Präfix] [gw Gw] [metric M]
        inet_route [-vF] add {-host|-net} Ziel[/Präfix] [metric M] reject
        ipmaddr -V | -version
        ipmaddr show [ dev NAME ] [ ipv4 | ipv6 | link | all ]
        iptunnel -V | --version

        netstat [-vnNcaeol] [<Socket> ...]
        netstat { [-veenNac] -i | [-cnNe] -M | -s }

        nodename [-v] {Rechnername|-F Datei} DECnet-Rechnername (aus Datei) setzten
        plipconfig -V | --version
        rarp -V                               Programmversion anzeigen.

        rarp -d <hostname>                    Eintrag aus dem Cache löschen.
        rarp -f                               Einträge aus /etc/ethers zufügen.
        rarp [<HW>] -s <hostname> <hwaddr>    Eintrag zum Cache zufügen.
        route [-v] [-FC] {add|del|flush} ...  Routentabelle für AF ändern.

        route {-V|--version}                  Version/Autor anzeigen und Ende.

        route {-h|--help} [<AF>]              Genaue Syntax für AF anzeigen.
      - keine Statistiken verfügbar -     -F, --file            Hostnamen oder NIS-Domainnamen aus Datei lesen

     -a, --alias           Namensalias
     -d, --domain          DNS-Domainname
     -f, --fqdn, --long    Langer Hostname (FQDN)
     -i, --ip-address      Adressen für den Hostnamen
     -n, --node            DECnet-Knotennamen
     -s, --short           Kurzer Hostname
     -y, --yp, --nis       NIS/YP-Domainname
     dnsdomainname=hostname -d, {yp,nis,}domainname=hostname -y

    Dies Kommando setzt oder gibt den Hostnamen oder NIS-Domainnamen aus.
   Es ist ebenfalls möglich die DNS-Domain oder den FQDN (langen Hostnamen)
   ausgeben zu lassen.  Außer wenn DNS oder NIS als Namensdienst verwendet
   wird, können FQDN (Fully Qualified Domain Name) und DNS-Domainname (welcher
   Teil des FQDNs ist) in /etc/hosts geändert werden.
   <AF>=Adressfamilie.  Standardwert: %s
   <AF>=Verwenden Sie »-6 | -4« oder »-A <af>« oder »--<af>«; Standardwert: %s
   <AF>=Verwenden Sie »-A <af>« oder »--<af>«; Standardwert: %s
   <HW>=Hardwaretyp.
   <HW>='-H <hw>' um Hardwareadresstyp anzugeben.  Standard: %s
   <Socket>={-t|--tcp} {-u|--udp} {-w|--raw} {-x|--unix} --ax25 --ipx --netrom
   Prüfsumme im empfangenen Paket wird benötigt.
   Prüfsumme für ausgegebene Pakete berechnen.
   Pakete außer der Reihenfolge fallenlassen.
   Liste möglicher Adressfamilien, die Routen unterstützen:
   List der möglichen Adressfamilien:
   Liste möglicher Hardwaretypen, die ARP unterstützen:
   Liste möglicher Hardwaretypen:
   Outfill:%d  Keepalive:%d   Pakete in Reihenfolge ausgeben.
   [[-]broadcast [<Adresse>]]  [[-]pointopoint [<Adresse>]]
   [[-]dynamic]
   [[-]trailers]  [[-]arp]  [[-]allmulti]
   [add <Adresse>[/<Präfixlänge>]]
   [del <Adresse>[/<Präfixlänge>]]
   [hw <HW> <Adresse>]  [metric <NN>]  [mtu <NN>]
   [mem_start <NN>]  [io_addr <NN>]  [irq <NN>]  [media <Typ>]
   [multicast]  [[-]promisc]
   [netmask <Adresse>]  [dstaddr <Adresse>]  [tunnel <Adresse>]
   [outfill <NN>] [keepalive <NN>]
   [txqueuelen <Länge>]
   [up|down] ...

   arp [-v]          [-i <if>] -d  <Host> [pub]               <-ARP-Eintrag entfernen
   arp [-v]   [<HW>] [-i <if>] -Ds <Host> <if> [netmask <nm>] pub          <-''-

   arp [-v]   [<HW>] [-i <if>] -s  <Host> <Hwaddr> [Temp]            <-Eintrag hinzufügen
   arp [-vnD] [<HW>] [-i <if>] -f [<dateiname>] <-Eintrag aus Datei hinzufügen
  Bcast:%s   MTU:%d  Metrik:%d  Maske:%s
  P-z-P:%s   Pfad
  Gültigkeitsbereich:  Timer  Benutzer  Benutzer   Inode       Schnittstelle: %s
  auf %s  Benutzer %d %-9.9s Protokoll:%s   %s	nibble %lu  trigger %lu
 %s (%s) -- kein Eintrag
 %s (%s) auf  %s gestartet %s: %s/ip  Gegenseite %s  lokal %s  %s: FEHLER beim Auslesen der Schnittstellenmerker: %s
 %s: FEHLER beim Testen der Schnittstellenmerker: %s
 %s: Mit diesem Programm kann der DNS-Domainname nicht geändert werden
 %s: Adressfamilie nicht unterstützt!
 %s: fehlerhafte Hardwareadresse
 %s: Kann »%s« nicht öffnen
 %s: Fehler beim Auslesen der Schnittstelleninformation: %s
 %s: Hardwaretyp nicht unterstützt!
 %s: Unerlaubte Mischung von Optionen.
 %s: ungültige %s Adresse.
 %s: Name zu lang
 %s: Nur Root darf den Domainnamen ändern
 %s: Nur Root darf den Rechnernamen ändern
 %s: Nur Root darf den Rechnernamen ändern
 %u DSACKs für defekte Pakete empfangen %u DSACKs empfangen %u DSACKs für alte Pakete verschickt %u DSACKs für defekte Pakete verschickt %u ICMP Nachrichten fehlgeschlagen %u ICMP-Meldungen empfangen %u ICMP Nachrichten gesendet %u IMCP-Pakete verworfen, weil Socket gesperrt war %u ICMP-Pakete verloren da sie außerhalb des zulässigen Bereichs lagen %u SYN Cookies empfangen %u SYN Cookies gesendet %u SYNs zu LISTEN Sockets ignoriert %u TCP-Ereignisse mit Datenverlust %u TCP-Sockets haben das Ende der Wartezeit des schnellen Zeitmessers erreicht %u TCP-Sockets haben das Ende der Wartezeit des langsamen Zeitmessers erreicht %u Bestätigungen ohne Daten empfangen %u aktive Verbindungsöffnungen %u aktive Verbindungen wegen des Zeitstempels abgelehnt. %u fehlerhafte SACKs empfangen %u fehlerhafte Segmente empfangen. %u Überlastungsfenster mit DSACK wiederhergestellt %u Überlastungsfenster vollständig wiederhergestellt %u Überlastungsfenster mit Hoe-Heuristik teilweise wiederhergestellt %u Überlastungsfenster nach Teil-Ack wiederhergestellt %u Verbindungsrücksetzungen empfangen %u Verbindungen abgebrochen durch Anwender während anhaltender Zeitüberschreitung. %u Verbindungen aufgrund von Speicherdruck zurückgesetzt %u Verbindungen infolge Zeitüberschreitung abgebrochen %u Verbindungen aufgebaut %u Verbindungen aufgrund vorzeitigem Abbruch durch den Benutzer zurückgesetzt %u Verbindungen aufgrund unerwarteter SYN zurückgesetzt %u Verbindungen wegen unerwarteter Daten zurückgesetzt %u verzögerte Acks aufgrund eines gesperrten Sockets weiter verzögert %u verzögerte Acks gesendet %u wegen fehlender Route verworfen %u fehlerhafte Verbindungsversuche %u Pakete schnell neu versendet %u weitergeleitete Wiederholungsübertragungen %u weitergeleitet %u Fragmente erstellt %u Fragmente nach Zeitüberschreitung verworfen %u Fragmente fehlgeschlagen %u empfangene Fragmente OK %u eingehende Pakete ausgeliefert %u eingehende Pakete verworfen %u Input-ICMP-Meldung fehlgeschlagen. %u ungültige SYN Cookies emfangen %u Bytes direkt über Rückprotokollierung empfangen %u Bytes direkt über Vorwarteschlange empfangen %u andere TCP-Zeitüberschreitungen %u ausgehende Pakete verworfen %u Paketköpfe prognostiziert %u Paket-Zussamenfügung fehlgeschlagen %u Paketempfangfehler %u Pakete wurden in der Empfangsschlange verloren aufgrund eine zu niedrigen Socket-Puffers %u Pakete direkt in die recvmsg-Vorwarteschlange eingereiht. %u Pakete aus der Außer-Betrieb-Warteschlange verloren wegen Socket-Puffer-Überlauf %u Pakete aus der Vorwarteschlange verloren %u Paket-Kopfzeilen geschätzt und direkt in die Warteschlange des Benutzers eingereiht %u Pakete aus der Empfangswarteschlange abgeschnitten %u Pakete von Empfangswarteschlange abgeschnitten wegen Pufferüberlauf im Socket %u Pakete wurden zusammengefügt OK %u Pakete empfangen %u Pakete aufgrund des Zeitstempels in aktiven Verbindungen abgelehnt %u Pakete gesendet %u Pakete an unbekannten Port empfangen %u passive Verbindungsöffnungen %u passive Verbindungen wegen des Zeitstempels abgelehnt. %u vorhergesagte Bestätigungen %u Zusammenfügungen erforderlich %u schnelle reno-Wiederholungsübertragungen gescheitert %u Anforderung gesendet %u Rücknahmen empfangen für nicht ausgereifte SYN_RECV Sockets %u Rücknahmen gesendet %u Wiederholungsübertragungen mit langsamem Start %u erneut gesendete Pakete sind verloren gegangen %u sack-Wiederholungsübertragungen gescheitert %u Segmente empfangen %u Segmente erneut übertragen %u Segmente ausgesendet %u Wartezeit-Sockets durch Zeitstempel wiederverwendet %u Zeitüberschreitungen nach SACK-Wiederherstellung %u Zeitüberschreitungen nach schneller reno-Wiederholungsübertragung %u Zeitüberschreitungen im Verlustzustand %u mal wurde der Empfänger für eine direkte Verarbeitung zu spät angesetzt %u Mal von Paketverlust aufgrund von SACK-Daten erholt %u mal wurde Paketverlust für schnellen Neuversand vermieden Die Zuhör-Warteschlange eines Sockets ist %u mal übergelaufen %u Mal wegen Speichermangel keine Möglichkeit, RST zu senden %u Pakete insgesamt empfangen %u mit ungültigen Adressen %u mit ungültigen Headern %u mit unbekanntem Protokoll (Cisco)-HDLC (Für "-p": geteuid()=%d konnte keine Information gelesen werden; sie sollten Root sein.)
 (Es konnten nicht alle Prozesse identifiziert werden; Informationen über
nicht-eigene Processe werden nicht angezeigt; Root kann sie anzeigen.)
 (auto) (unvollständig) (Nur Server) (Server und stehende Verbindungen) (ohne Server) 16/4 Mb/s Token-Ring 16/4 Mb/s Token-Ring (neu) 6-bit Serielle IP <von_schnittstelle> <unvollständig>  ALLMULTI  AMPR AX.25 AMPR NET/ROM AMPR ROSE ARCnet AX.25 ist auf diesem System nicht konfiguriert.
 Aktive AX.25 Sockets
 Aktive IPX Sockets
Proto Recv-Q Send-Q Lokale Adresse             Gegenadresse               Zustand Aktive Internetverbindungen  Aktive NET/ROM Sockets
 Aktive Sockets in der UNIX-Domäne  Aktive X.25-Sockets
 Adaptive Serielle IP Adresse Hardware-Typ Hardware-Adresse Optionen Maske Schnittstelle
 Das Löschen von Adressen wird auf diesem System nicht unterstützt.
 Adressfamilie `%s' wird nicht unterstützt.
 Appletalk DDP Ash BROADCAST  Fehlerhafte Adresse.
 Basisadresse:0x%x  Ein Broadcasttunnel ist nur mit einer Quelladresse möglich
 CCITT X.25 CLOSE CLOSE_WAIT CLOSING VERBINGSAUFBAU GESCHICKT VERBUNDEN VERBINDUNGSAUFBAU Rufzeichen zu lang Kann line discipline nicht auf »%s« setzen.
 Kann Socket nicht öffnen Kompatibilität DARPA-Internet DEBUG  DGRAM VERBINDUNGSABBAU GESCHICKT VERBINDUNGSABBAU DMA Kanal:%x  DYNAMIC  Standard-TTL ist %u Dest Source Device LCI State Vr/Vs Send-Q Recv-Q
 Ziel       Quelle     Gerät   Zustand      Vr/Vs    Send-Q  Empf-Q
 Ziel                                        Nächster Hop                                 Flags Metric Ref    Benutzer Iface
 Ziel                      Router-Netz               Router-Knoten
 Ziel            Router          Genmask         Flags   MSS Fenster irtt Iface
 Ziel            Router          Genmask         Flags Metric Ref    Use Iface
 Ziel            Gateway         Maske           Flags Metric Ref    Benutzer Iface    MSS   Fenster irtt
 Ziel         SStelle  Benutzer
 Ziel         Mnemonic  Qualität  Nachbar  Iface
 %u Neuanordnungen mit FACK erkannt %u Neuanordnungen mit SACK erkannt %u Neuanordnungen mit schneller reno-Wiederholungsübertragung erkannt %u Neuanordnungen über den Zeitstempel erkannt Gerät nicht gefunden Kann die Adressen der Familie %d nicht setzen.
 VERBUNDEN VERBUNDEN Econet Einträge: %d   Ignoriert: %d   Gefunden: %d
 Ethernet FIN_WAIT1 FIN_WAIT2 FREI Kann den Typ von [%s] nicht holen
 Fiber Distributed Data Interface »Flush« der Inet-Routentabelle nicht unterstützt
 »Flush« für IPv6-Routentabelle nicht unterstützt
 Weiterleitung ist %s Frame Relay Access Device Frame Relay DLCI Generisches EUI-64 Global HIPPI Hardware Adresse %s   Hardwaretyp »%s« nicht unterstützt.
 Maschine ICMP-Eingabehistogramm: ICMP-Ausgabehistogramm: INET (IPv4) ist auf diesem System nicht konfiguriert.
 INET6 (IPv6) ist auf diesem System nicht konfiguriert.
 IP-Maskierungseinträge
 IPIP Tunnel IPX ist auf diesem System nicht konfiguriert.
 IPX: dies muss noch geschrieben werden
 IPv4-Gruppenmitgliedschaften
 IPv6 IPv6-nach-IPv4 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR    TX-OK TX-ERR TX-DRP TX-OVR Flg
 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR   TX-OK TX-ERR TX-DRP TX-OVR Flg
 Schnittstelle   RefZäh Grupp
 Interface %s nicht initialisiert
 Interrupt:%d  Ungültiges Rufzeichen IrLAP Kernel AX.25 Routentabelle
 Kernel-IP-Routencache
 Kernel-IP-Routentabelle
 Kernel-IPX-Routentabelle
 Kernel IPv6 Nachbarcache
 Kernel-IPv6-Routentabelle
 Kernel-Schnittstellentabelle
 Kernel-NET/ROM-Routentabelle
 ROSE-Kernel-Routentabelle
 Schlüssel sind mit ipip und sit nicht erlaubt.
 LAPB LAST_ACK LISTEN HÖRT LOOPBACK  Verbindung Lokale Schleife MASTER  MULTICAST  Fehlerhafte Ash-Adresse Medium:%s Speicher:%lx-%lx  Änderung des »Inet« Routencaches nicht unterstützt
 NET/ROM ist auf diesem System nicht verfügbar.
 NET/ROM: Dies muss noch geschrieben werden
 NOARP  NOTRAILERS  Nachbar                                     HW-Adresse        Iface    Flags Ref Zustand
 Nachbar                                     HW-Adresse        Iface    Flags Ref Zustand          Stale(sec) Löschen(sec)
 Kein ARP-Eintrag für %s
 Kein Routen für Adressfamilie `%s'.
 ECONET wird auf diesem System nicht unterstützt.
 INET ist auf diesem System nicht verfügbar.
 INET6 ist auf diesem System nicht verfügbar.
 Keine benutzbaren Adressfamilien gefunden.
 Knotenadresse muss zehn Ziffern haben Novell IPX PUNKTZUPUNKT  PROMISC  Bitte nur eine Adressfamilie angeben.
 Punkt-zu-Punkt-Verbindung Probleme beim Lesen von %s
 Schneller Ack-Modus wurde %u mal aktiviert RAW RDM WIEDERHERSTELLUNG ROSE ist auf diesem System nicht verfügbar.
 RTO-Algorithmus ist %s RUNNING  RX-Bytes:%llu (%lu.%lu %s)  TX-Bytes:%llu (%lu.%lu %s)
 RX-Pakete:%llu Fehler:%lu Verloren:%lu Überläufe:%lu Fenster:%lu
 RX: Pakete     Bytes        Fehler CsumErrs OutOfSeq Mcasts
 Der Systemspeicher reichte %u mal nicht aus, während Pakete gesendet wurden Löse »%s« auf ...
 Ergebnis: h_addr_list=»%s«
 Ergebnis: h_aliases=»%s«
 Ergebnis: h_name=»%s«
 SABM GESCHICKT SEQPAKET SLAVE  STREAM SYN_RECV SYN_SENT Serielle IP Setze domainname auf »%s«
 Setze Hostname auf »%s«
 Rechnernamen auf »%s« setzen
 Standort Bitte benutzen Sie pppd.
 Quelle          Ziel            Maske           Flags   MSS Fenster irtt Iface
 Ziel            Ziel            Genmask         Flags Metric Ref    Ben Iface
 Quelle           Ziel           Quelle          Flags Metrik Ref    Benutzer Iface    MSS   Fenster irtt   HH  Arp
 Quelle          Ziel            Gateway         Flags Metrik Ref    Ben Iface    MSS   Window irtt  TOS HHRef HHUptod     SpecDst
 TCP lief %u Mal mit knappem Speicher TIME_WAIT TX-Pakete:%llu Fehler:%lu Verloren:%lu Überläufe:%lu Träger:%lu
 TX: Pakete     Bytes        Fehler DeadLoop NoRoute  NoBufs
 Dieser Kernel unterstützt kein RARP.
 Zu viele Adressfamilien angegeben.
 UNIX-Domain UNB. UNBEKANNT UNSPEC UP  Unbekannt Unbekannte Adressfamilie »%s«.
 Typ des Mediums unbekannt.
 Benutzung:
  arp [-vn]  [<HW>] [-i <if>] [-a] [<Hostname>]
 Aufruf:
  ifconfig [-a] [-v] [-s] <interface> [[<AF>] <adresse>]
 Benutzung: Hostname [-v] {Hostname|-F Datei} Hostname (aus Datei) setzen
 Benutzung: inet6_route [-vF] del Ziel
 Benutzung: inet_route [-vF] del {-host|-net} Ziel[/Präfix] [gw Gw] [metric M] [[dev] If]
 Benutzung: ipmaddr [ add | del ] MULTIADR dev NAME
 Benutzung: iptunnel { add | change | del | show } [ NAME ]
 Benutzung: plipconfig [-a] [-i] [-v] Interface
 Benutzung: rarp -a                               Einträge im Cache listen.
 Benutzung: route [-nNvee] [-FC] [<AF>]           Kernelroutentabelle anzeigen
 Benutzer   Ziel       Quelle     Gerät   Zustand      Vr/Vs    Send-Q  Recv-Q
 VJ 6-bit Serielle IP Serielle VJ-IP WARNUNG: Es ist mindestens ein Fehler aufgetreten. (%d)

 Warnung: Das Interface %s ist immer noch im ALLMULTI-Modus.
 Warnung: Das Interface %s ist immer noch im BROADCAST-Modus.
 Warnung: Das Interface %s ist immer noch im DYNAMIC-Modus.
 Warnung: Das Interface %s ist immer noch im MULTICAST-Modus.
 Warnung: Das Interface %s ist immer noch im POINTOPOINT-Modus.
 Warnung: Das Interface %s ist immer noch im promiskuitiven Modus... Läuft eventuell noch eine andere Anwendung?
 Warnung: %s kann nicht geöffnet werden (%s). Ausgabe beschränkt.
 Wobei: NAME := ZEICHENKETTE
 Falsches Format von /proc/net/dev. Tut mir leid.
 Mit diesem Programm kann PPP nicht gestartet werden.
 [KEINE FLAGS] [KEINE FLAGS]  [NICHT GESETZT] [UNBEKANNT] Adressmaske Antworten: %u Adressmaske Abfrage: %u Adressierungsmasken-Anfragen: %u arp: %s: Hardware unterstützt kein ARP.
 arp: %s: Kernel unterstützt nur »inet«.
 arp: %s: unbekannte Adressfamilie.
 arp: %s: unbekannter Hardwaretyp.
 arp: -N noch nicht unterstützt.
 arp: Kann Etherfile %s nicht öffnen!
 arp: Kann Eintrag auf Zeile %u von Etherdatei %s nicht setzen!
 arp: Kann Hardware-Adresse für '%s' nicht ermitteln: %s.
 arp: Gerät »%s« hat HW-Adresse %s »%s«.
 arp: Formatfehler in Zeile %u von Etherfile %s.
 arp: In %d Einträgen wurde kein zutreffender gefunden.
 arp: ungültige Hardwareadresse
 arp: Hardwareadresse muss angegeben werden
 arp: Hostname muss angegeben werden
 arp: unpassende Protokolltypen.
 Die Tunnelbetriebsart (ipip, fre oder sit) kann nicht festgestellt werden
 Kann /proc/net/snmp nicht öffnen Komprimiert:%lu  Ziel unerreichbar: %u deaktiviert Domainname (welcher Teil des FQDN ist) in der Datei /etc/hosts geändert werden.
 Echo Antworten: %u Echo Abfrage: %u Echo Anfragen: %u aktiviert Fehler beim Parsen von /proc/net/snmp familie %d  Generisches X.25 getdomainname()=»%s«
 gethostname()=»%s«
 getnodename()=»%s«
 Hardware Address Typ `%s' hat keinen 'Handler' zum Setzen der Addresse. Fehlgeschlagen.
 ifconfig: Kann Adresse nicht für diese Protokoll-Familie setzen.
 ifconfig: `--help' gibt weitere Informationen zur Verwendung aus.
 ifconfig: Option '%s' nicht erkannt.
 in_arcnet(%s): Ungültige ARCnet-Adresse!
 in_arcnet(%s): angehängt : ignoriert!
 in_arcnet(%s): Nachfolgender Müll!
 in_ether(%s): ungültige Ethernetadresse!
 in_ether(%s): angehängt : ignoriert!
 in_ether(%s): Nachfolgender Müll!
 in_fddi(%s): Ungültige FDDI-Adresse!
 in_fddi(%s): nachfolgend : ignoriert!
 in_fddi(%s): Nachfolgender Müll!
 in_hippi(%s): Ungültige HIPPI-Adresse!
 in_hippi(%s): nachfolgend : ignoriert!
 in_hippi(%s): Nachfolgender Müll!
 in_tr(%s): ungültige Tokenringadresse!
 in_tr(%s): nachfolgend : ignoriert!
 in_tr(%s): nachfolgender Müll!
 ip: %s ist eine ungültige IPv4-Adresse
 ip: %s ist eine ungültige INET-Adresse
 ip: %s ist ein ungültiges INET-Präfix
 ip: Fehlerhaftes Argument: %s
 keepalive (%2.2f/%ld/%d) Fehlende Interfaceinformationen netzmaske %s  NET/ROM-Benutzung
 netstat: Nicht unterstützte Adressfamilie %d!
 Kein RARP-Eintrag für %s.
 aus (0.00/%ld/%d) auf %s
 ein (%2.2f/%ld/%d) ein%d (%2.2f/%ld/%d) Problem beim Lesen von Daten von %s
 Prot   Ablauf    Anf-Seq Delta Prevd Quelle               Ziel                 Ports
 Prot   expire Quelle               Ziel                 Ports
 rarp: %s: unbekannter Hardwaretyp.
 rarp: %s: Unbekannter Host
 rarp: kann Datei %s:%s nicht öffnen.
 rarp: Kann Eintrag aus %s:%u nicht setzen.
 rarp: Formatfehler bei %s:%u
 weitergeleitet: %u Umleitungen: %u route: %s: Netzadresse als Gateway ungültig!
 route: Ungültige MSS/MTU.
 route: Ungültige Start-RTT.
 route: Ungültige Fenstergröße.
 Route: Fehlerhafte Netzmaske %s
 route: Netzmaske %.8x macht keinen Sinn mit Hostroute
 route: Netzmaske passt nicht zur Routenadresse
 rresolve: nicht unterstützte Adressfamilie %d !
 slattach: /dev/%s bereits gesperrt!
 slattach: Kann PID-Datei nicht schreiben
 slattach: setvbuf(stdout,0,_IOLBF,0) : %s
 slattach: tty-Bezeichnung zu lang!
 slattach: tty_hangup(DROP): %s
 slattach: tty_hangup(RAISE): %s
 slattach: tty_lock: (%s): %s
 slattach: tty_lock: UUCP-Benutzer %s unbekannt!
 slattach: tty_open: Kann augenblicklichen Leitungszustand nicht auslesen!
 slattach: tty_open: kann aktuellen Zustand nicht auslesen!
 slattach: tty_open: Kann %s bps nicht setzen!
 slattach: tty_open: Kann 8N1-Modus nicht setzen!
 slattach: tty_open: Kann RAW-Modus nicht setzen!
 Quelle angefangen: %u Quellen gelöscht: %u Zeit überschritten: %u Zeitüberschreitung in Durchgang: %u Zeitstempelantworten: %u Zeitstempel Antwort: %u Zeitstempel Anfrage: %u Zeitstempel Abfragen: %u timewait (%2.2f/%ld/%d) ttl != 0 und noptmudisc sind inkompatibel
 Sendewarteschlangenlänge:%d  unkn-%d (%2.2f/%ld/%d) Unbekannt Benutzung: netstat [-veenNcCF] [<Af>] -r
           netstat {-V|--version|-h|--help}
 Warnung, fehlerhafte igmp-Zeile %d.
 Warnung, fehlerhafte igmp6 line %d.
 Warnung, fehlerhafte raw-Zeile.
 Warnung, fehlerhafte TCP Zeile.
 Warnung, fehlerhafe UDP-Zeile.
 Warnung, fehlerhafte UNIX-Zeile.
 Warnung: Keine INET-Sockets verfügbar: %s
 Falsche Parameter: %u 