��    �      �  �   L	      `  $   a     �     �  	   �  9   �     �     �  ,   �       	   %     /     6     =     P  "   h  -   �     �  !   �  	   �  
   �     �     �     �                    6     M     _  %   z  *   �     �     �     �           5     I     R     Z     f     m     y  	   �  #   �  ?   �     �     �       /        L     \     j     o  	   v     �     �     �  
   �     �     �     �     �     �     �  !        9     B     ]     l     �  $   �  -   �     �      �                ?     G  
   a  $   l  	   �  =   �     �  /   �          :     B     Z     u     �     �     �     �     �     �     �       #     .   3     b     j     }  *   �     �  >   �     �  ,     )   1  5   [  I   �     �     �     �     
           &     G     a     g     �     �     �      �  	   �  &   �  
   �  .     
   7     B     N  3   W     �     �  "   �     �     �     �       !        A     V     p  !   �     �  .   �     �  �  �  )   �     �     �     �  M   �     5     =  ;   B     ~     �     �     �     �     �  1   �  @        F  %   O     u  	   ~     �     �     �     �     �     �  '   �     �       /   9  .   i  )   �  ,   �     �  1     !   @  	   b     l     t     �     �     �     �  (   �  H   �     (     8  )   @  <   j     �     �     �     �     �     �     �     �                 5      D      Z      g      �   '   �   	   �      �      �   )   !  
   6!  /   A!  <   q!     �!     �!     �!     �!  	   �!     "     #"  .   /"  	   ^"  E   h"     �"  M   �"     #     +#     3#     L#     e#     u#     �#     �#     �#     �#     �#     �#  
   $  -   $  8   I$     �$     �$  
   �$  :   �$     �$  J   �$     >%  ,   D%  (   q%  L   �%  U   �%     =&     Z&     j&     y&  	   �&  1   �&  "   �&  
   �&     �&     '     *'     /'  !   I'  	   k'  /   u'  
   �'  4   �'     �'     �'      (  2   (  	   ?(     I(  -   V(     �(  3   �(     �(     �(  +   �(     ))     D)     c)  *   �)      �)  >   �)     *     �   �   +               �       "      k   w   \           %   !   �   s   z          �   �   ]   )   K       .         S      2           6   @   &      3   y   ~       v   8   f   X   �   d   T   
                 b   D   <      R   C   V           {               -       I           9                 1   (   $   G   7   c   >   Y   �   e                  i   j   q             L          W      /   F   _   =      P   ,   `         a      #   J   �       �   n       5              	   }       �   E       m   x   Q   0                  �   H   [   �   B   �       p   ;   M             ^   l           U      t   Z           �      �   4   �   o      N       h   r   '       �   g   ?           A       *          |   �   O   �           u   :                     Usage     Device name
     0 mW   Core   Package  <ESC> Exit | <Enter> Toggle tunable | <r> Window refresh  CPU %i %7sW %s device %s has no runtime power management .... device %s 
 1 units/s Active Actual Audio codec %s: %s Audio codec %s: %s (%s) Autosuspend for USB device %s [%s] Autosuspend for unknown USB device %s (%s:%s) Bad Bluetooth device interface status C0 active C0 polling CPU CPU %d CPU Information CPU use CPU:  Calibrating USB devices
 Calibrating backlight
 Calibrating idle
 Calibrating radio devices
 Calibrating: CPU usage on %i threads
 Calibrating: CPU wakeup power consumption
 Calibrating: disk usage 
 Cannot create temp file
 Cannot load from file Cannot open output file %s (%s)
 Cannot save to file Category Core %d Description Device Device Name Device stats Disk IO/s Enable Audio codec power management Estimated power: %5.1f    Measured power: %5.1f    Sum: %5.1f

 Events/s Exit Failed to mount debugfs!
 Finishing PowerTOP power estimate calibration 
 Frequency stats GFX Wakeups/s GFX: GPU %d GPU ops/s GPU ops/seconds GPU: Good Idle stats Intel built in USB hub Kernel Version Leaving PowerTOP Link Loaded %i prior measurements
 Measuring workload %s.
 NMI watchdog should be turned off Navigate Network interface: %s (%s) OS Information Optimal Tuned Software Settings Overview Overview of Software Power Consumers PCI Device %s has no runtime power management PCI Device: %s PS/2 Touchpad / Keyboard / Mouse Package Parameters after calibration:
 Partial Power Aware CPU scheduler Power est. Power est.    Usage     Device name
 PowerTOP  PowerTOP %s needs the kernel to support the 'perf' subsystem
 PowerTOP Version PowerTOP is out of memory. PowerTOP is Aborting Preparing to take measurements
 Process Process Device Activity Processor Frequency Report Radio device: %s Runtime PM for %s device %s Runtime PM for PCI Device %s SATA controller SATA disk: %s SATA link: %s Script Set refresh time out Slumber Software Settings in Need of Tuning Starting PowerTOP power estimate calibration 
 Summary System Information System Name System baseline power is estimated at %sW
 System:  Taking %d measurement(s) for a duration of %d second(s) each.
 Target: The battery reports a discharge rate of %sW
 The battery reports a discharge rate of:  The estimated remaining time is %i hours, %i minutes
 Too many open files, please increase the limit of open file descriptors.
 Top 10 Power Consumers Tunables USB device: %s USB device: %s (%s) Unknown Unknown issue running workload!
 Untunable Software Issues Usage Usage: powertop [OPTIONS] VFS ops/sec and VFS: VM writeback timeout Wake-on-lan status for device %s Wakeups/s Wireless Power Saving for interface %s [=devnode] [=iterations] number of times to run each test [=seconds] [=workload] \% usage as well as support for trace points in the kernel:
 cpu package cpu package %i cpu_idle event returned no state?
 exiting...
 file to execute for workload generate a csv report generate a html report generate a report for 'x' seconds print this help menu print version information run in "debug" mode runs powertop in calibration mode suppress stderr output uses an Extech Power Analyzer for measurements wakeups/second Project-Id-Version: de
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-11-05 16:49-0800
PO-Revision-Date: 2016-06-22 12:00+0000
Last-Translator: Torsten Franz <Unknown>
Language-Team: <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:21+0000
X-Generator: Launchpad (build 18115)
X-Poedit-Language: German
X-Poedit-SourceCharset: utf-8
               Auslastung     Gerätename
     0 mW   Kern   Paket  <ESC> Beenden | <Eingabe> Steuerbares umschalten | <r> Fenster aktualisieren  CPU %i %7sW %s-Gerät %s verfügt über keine Laufzeitenergieverwaltung … Gerät %s 
 1 Einheit/s Aktiv Tatsächlich Audiocodec %s:%s Audiocodec %s:%s(%s) Automatische Bereitschaft für USB-Gerät %s [%s] Automatische Bereitschaft für unbekanntes USB-Gerät %s [%s:%s] Schlecht Bluetooth-Geräteschnittstellenstatus C0 aktiv C0 sendet CPU CPU %d CPU Information CPU-Auslastung CPU:  USB-Geräte wird kalibriert
 Hintergrundbeleuchtung wird kalibriert
 Ruhezustand wird kalibriert
 Funkgeräte werden kalibriert
 Kalibrierung läuft: CPU-Nutzung in %i Threads
 Kalibrierung läuft: CPU-Aufwachenergiebedarf
 Kalibrierung läuft: Festplattennutzung 
 Temporäre Datei kann nicht erstellt werden
 Laden aus Datei nicht möglich Ausgabedatei %s (%s) kann nicht geöffnet werden
 Speichern in Datei nicht möglich Kategorie Kern %d Beschreibung Gerät Gerätename Gerätestatistik Festplatten-EA/s Audiocodec-Energieverwaltung einschalten Geschätzte Energie: %5.1f    Gemessene Energie: %5.1f    Summe: %5.1f

 Ereignisse/Sek. Beenden Debugfs konnte nicht eingehängt werden!
 PowerTOP-Energieschätzungskalibrierung wird abgeschlossen 
 Frequenzstatistik GFX-Wakeups/s GFX: GPU %d GPU-Vorgänge/s GPU-Vorgänge/Sekunde GPU: Gut Untätigkeitsstatistiken Eingebauter Intel-USB-Hub Kernel-Version PowerTOP wird beendet Verknüpfung %i alte Messungen geladen
 Arbeitslast %s wird gemessen.
 NMI-Watchdog sollte abgeschaltet werden Navigiere Netzwerkschnittstelle: %s(%s) Betriebssystem Information Optimal abgestimmte Programmeinstellungen Übersicht Übersicht über die Programmenergieverbraucher PCI-Gerät %s verfügt über keine Laufzeitenergieverwaltung PCI-Gerät: %s PS/2-Touchpad/Tastatur/Maus Paket Parameter nach Kalibrierung:
 Teilweise Energiebewusster CPU-Planer Energie ca. Energieschätz.    Auslastung     Gerätename
 PowerTOP  PowerTOP %s benötigt »perf«-Untersystemunterstützung des Kernels
 PowerTOP-Version PowerTOP steht nicht genug Speicher zur Verfügung. PowerTOP wird abgebrochen Messungen werden vorbereitet
 Prozess Prozessgeräteaktivität Prozessorfrequenzbericht Funkgeräte: %s Laufzeit-PM für %s-Gerät %s Laufzeit-PM für PCI-Gerät %s SATA-Steuerung SATA-Festplatte: %s SATA-Verknüpfung: %s Skript Aktualisierungszeit aussetzen Schlummern Programmeinstellungen benötigen abstimmungen PowerTOP-Energieschätzungskalibrierung wird gestartet 
 Zusammenfassung Systeminformation Systemname Grundsätzlich benötigte Energie wird auf %sW geschätzt
 System:  %d Messung(en) mit einer jeweiligen Dauer von %d Sekunde(n) durchführen.
 Ziel: Der Akku meldet eine Entladungsrate von %sW
 Der Akku zeigt eine Entladungsrate von:  Die verbleibende Zeit beträgt vorraussichtlich noch %i Stunden, %i Minuten
 Zu viele offene Dateien. Bitte die Begrenzung von offenen Dateideskriptoren anheben.
 Die 10 größten Verbraucher Einstellbarkeit USB-Gerät: %s USB-Gerät: %s (%s) Unbekannt Unbekannter Fehler beim Ausführen der Arbeiten.
 Nicht abstimmbare Programmprobleme Auslastung Aufruf: powertop [OPTIONEN] VFS-Vorgänge/Sek. und VFS: VM-Rückschreibezeitlimit Wake-on-lan-Status für Gerät %s Wakeups/s Funknetzwerkenergiesparen für Schnittstelle %s [=devnode] [=Wiederholungen] Anzahl der Durchläufe jedes Tests [=Sekunden] [=Arbeitslast] \% uNutzung sowie Unterstützung für Trace-Punkte im Kernel:
 CPU-Paket CPU-Paket %i cpu_idle-Ereignis gab keinen Status zurück?
 Beende …
 Auszuführende Datei zur Simulation der Arbeitslast Einen CVS-Bericht erstellen Einen HTML-Bericht erstellen Einen Bericht für »x« Sekunden erstellen Dieses Hilfemenü anzeigen Versionsinformationen ausgeben Im »debug«-Modus ausführen Powertop im Kalibrierungs-Modus ausführen »stderr«-Ausgabe unterdrücken Einen Extech-Leistungsanalysierer für die Messungen verwenden Wakeups/Sekunde 