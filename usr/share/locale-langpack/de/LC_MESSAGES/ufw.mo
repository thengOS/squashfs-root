��    �      �  �   \            !     #  "   +  	  N     X  (   v  #   �     �     �  &   �          4     H  *   ]     �     �     �  9   �  )   �       "   0     S     k  /   ~  +   �     �     �     �       #     #   ;  %   _      �     �     �     �     �     �       W   (     �  "   �     �  &   �  "        (     D     \     u     �  !   �     �  '   �  "        7     N  !   e  #   �     �  !   �  %   �          %  !   8     Z  &   u  5   �  *   �  C   �  =   A       (   �  %   �  %   �  0     &   7     ^  /   p     �  >   �     �     �       3   $  ,   X     �     �     �     �     �     �  '        0  '   I     q     �     �  +   �     �     �          *     @     P     e     x     �  	   �     �  %   �  /   �           /      J   &   X         !   �      �      �      �   1   �   /   !     J!  !   W!     y!  ,   �!  T   �!     "  
   �"      �"     �"     �"     �"     �"     �"     #     +#  4   F#  $   {#     �#     �#     �#     �#     $     4$  *   K$  :   v$     �$      �$  )   �$  $   %     ?%  #   \%  #   �%  0   �%     �%     �%  
   �%     &     &     &     1&     L&     g&  %   &     �&     �&     �&     �&  &   '     :'     <'     L'     c'     '  4   �'     �'     �'     �'  �  �'     f)     h)  +   q)    �)  0   �.  -   �.  9   /     O/     l/  (   �/     �/     �/     �/  5   0  +   90     e0     q0  H   x0  <   �0     �0  '   1     ?1  '   W1  Z   1  8   �1  %   2     92     J2     ]2  3   v2  3   �2  *   �2  3   	3     =3     [3  &   x3  %   �3     �3     �3  Z   �3  &   U4  1   |4  "   �4  0   �4  0   5     35     S5     q5     �5  #   �5  4   �5  3   �5  &   26  '   Y6  "   �6  '   �6  ,   �6  .   �6     (7  0   A7  0   r7  #   �7  $   �7  +   �7  %   8  B   >8  @   �8  4   �8  M   �8  7   E9     }9  8   �9  *   �9  *   �9  6   :  4   V:     �:  2   �:      �:  L   �:     C;  (   G;     p;  =   �;  1   �;     �;     <     '<     @<     Z<     q<  *   �<     �<  0   �<  %   =     .=     G=  4   Y=  #   �=     �=     �=     �=     >     >     +>  !   B>     d>     �>     �>  1   �>  9   �>  &    ?  !   G?     i?  +   w?  '   �?  +   �?  #   �?     @     !@  =   (@  5   f@     �@  (   �@  (   �@  <   �@  W   7A  �   �A     B  #   1B     UB     eB     xB     �B     �B  $   �B  #   �B  A   C  .   CC  8   rC  A   �C  %   �C  )   D  %   =D  !   cD  9   �D  R   �D  6   E  0   IE  ;   zE  ,   �E  '   �E  :   F  0   FF  /   wF     �F     �F  
   �F     �F     �F     �F  &   G      2G  #   SG  .   wG  #   �G     �G  "   �G     
H  >   'H     fH     hH  &   �H  +   �H     �H  3   �H  	   !I     +I     -I         �          a   �       �   $          �       u   `       �          w      �   �   �           �   P   �   [   c   H   h   Y       s       _   �   F       �   x       ~       z   �              5   �   �           v   '   g       }   J      d   �   y          �   �       L   �           *   "       @       m   N   K   �   �   �   =   k          �           X   A              �   Q      >      &      ;   S      /   	   �       T   +   i   8   �       q   �          D          V   �       ^   �   �   �       �   %   G   �      #   U       2       {   �   �      E       �   R   r      I   �   4       (   �   ?         Z   �   ,   O      9   1       �   �       �   �   
   �       M   �   3       \       �       )          �          �   -       p         f   j   �   :          n       �          6   B       <       �   e   |       7          W   o       C       0   l   �              t   ]   .   �           !   b    
 
(None) 
Error applying application rules. 
Usage: %(progname)s %(command)s

%(commands)s:
 %(enable)-31s enables the firewall
 %(disable)-31s disables the firewall
 %(default)-31s set default policy
 %(logging)-31s set logging to %(level)s
 %(allow)-31s add allow %(rule)s
 %(deny)-31s add deny %(rule)s
 %(reject)-31s add reject %(rule)s
 %(limit)-31s add limit %(rule)s
 %(delete)-31s delete %(urule)s
 %(insert)-31s insert %(urule)s at %(number)s
 %(route)-31s add route %(urule)s
 %(route-delete)-31s delete route %(urule)s
 %(route-insert)-31s insert route %(urule)s at %(number)s
 %(reload)-31s reload firewall
 %(reset)-31s reset firewall
 %(status)-31s show firewall status
 %(statusnum)-31s show firewall status as numbered list of %(rules)s
 %(statusverbose)-31s show verbose firewall status
 %(show)-31s show firewall report
 %(version)-31s display version information

%(appcommands)s:
 %(applist)-31s list application profiles
 %(appinfo)-31s show information on %(profile)s
 %(appupdate)-31s update %(profile)s
 %(appdefault)-31s set default application policy
  (skipped reloading firewall)  Attempted rules successfully unapplied.  Some rules could not be unapplied. %s is group writable! %s is world writable! '%(f)s' file '%(name)s' does not exist '%s' already exists. Aborting '%s' does not exist '%s' is not writable (be sure to update your rules accordingly) : Need at least python 2.6)
 Aborted Action Added user rules (see 'ufw status' for running firewall): Adding IPv6 rule failed: IPv6 not enabled Available applications: Backing up '%(old)s' to '%(new)s'
 Bad destination address Bad interface name Bad interface name: can't use interface aliases Bad interface name: reserved character: '!' Bad interface type Bad port Bad port '%s' Bad source address Cannot insert rule at position '%d' Cannot insert rule at position '%s' Cannot specify 'all' with '--add-new' Cannot specify insert and delete Checking ip6tables
 Checking iptables
 Checking raw ip6tables
 Checking raw iptables
 Checks disabled Command '%s' already exists Command may disrupt existing ssh connections. Proceed with operation (%(yes)s|%(no)s)?  Could not back out rule '%s' Could not delete non-existent rule Could not find '%s'. Aborting Could not find a profile matching '%s' Could not find executable for '%s' Could not find profile '%s' Could not find protocol Could not find rule '%d' Could not find rule '%s' Could not get listening status Could not get statistics for '%s' Could not load logging rules Could not normalize destination address Could not normalize source address Could not perform '%s' Could not set LOGLEVEL Could not update running firewall Couldn't determine iptables version Couldn't find '%s' Couldn't find parent pid for '%s' Couldn't find pid (is /proc mounted?) Couldn't open '%s' for reading Couldn't stat '%s' Couldn't update application rules Couldn't update rules file Couldn't update rules file for logging Default %(direction)s policy changed to '%(policy)s'
 Default application policy changed to '%s' Default: %(in)s (incoming), %(out)s (outgoing), %(routed)s (routed) Deleting:
 %(rule)s
Proceed with operation (%(yes)s|%(no)s)?  Description: %s

 Duplicate profile '%s', using last found ERROR: this script should not be SGID ERROR: this script should not be SUID Firewall is active and enabled on system startup Firewall not enabled (skipping reload) Firewall reloaded Firewall stopped and disabled on system startup Found exact match Found multiple matches for '%s'. Please use exact profile name From IPv6 support not enabled Improper rule syntax Improper rule syntax ('%s' specified with app rule) Insert position '%s' is not a valid position Invalid '%s' clause Invalid 'from' clause Invalid 'port' clause Invalid 'proto' clause Invalid 'to' clause Invalid IP version '%s' Invalid IPv6 address with protocol '%s' Invalid interface clause Invalid interface clause for route rule Invalid log level '%s' Invalid log type '%s' Invalid option Invalid policy '%(policy)s' for '%(chain)s' Invalid port with protocol '%s' Invalid ports in profile '%s' Invalid position ' Invalid position '%d' Invalid profile Invalid profile name Invalid token '%s' Logging disabled Logging enabled Logging:  Missing policy for '%s' Mixed IP versions for 'from' and 'to' Must specify 'tcp' or 'udp' with multiple ports Need 'from' or 'to' with '%s' Need 'to' or 'from' clause New profiles: No rules found for application profile Option 'log' not allowed here Option 'log-all' not allowed here Port ranges must be numeric Port: Ports: Profile '%(fn)s' has empty required field '%(f)s' Profile '%(fn)s' missing required field '%(f)s' Profile: %s
 Profiles directory does not exist Protocol mismatch (from/to) Protocol mismatch with specified protocol %s Resetting all rules to installed defaults. Proceed with operation (%(yes)s|%(no)s)?  Resetting all rules to installed defaults. This may disrupt existing ssh connections. Proceed with operation (%(yes)s|%(no)s)?  Rule added Rule changed after normalization Rule deleted Rule inserted Rule updated Rules updated Rules updated (v6) Rules updated for profile '%s' Skipped reloading firewall Skipping '%(value)s': value too long for '%(field)s' Skipping '%s': also in /etc/services Skipping '%s': couldn't process Skipping '%s': couldn't stat Skipping '%s': field too long Skipping '%s': invalid name Skipping '%s': name too long Skipping '%s': too big Skipping '%s': too many files read already Skipping IPv6 application rule. Need at least iptables 1.4 Skipping adding existing rule Skipping inserting existing rule Skipping malformed tuple (bad length): %s Skipping malformed tuple (iface): %s Skipping malformed tuple: %s Skipping unsupported IPv4 '%s' rule Skipping unsupported IPv6 '%s' rule Status: active
%(log)s
%(pol)s
%(app)s%(status)s Status: active%s Status: inactive Title: %s
 To Unknown policy '%s' Unsupported action '%s' Unsupported default policy Unsupported direction '%s' Unsupported policy '%s' Unsupported policy for direction '%s' Unsupported protocol '%s' WARN: '%s' is world readable WARN: '%s' is world writable Wrong number of arguments You need to be root to run this script n problem running problem running sysctl problem running ufw-init
%s running ufw-init uid is %(uid)s but '%(path)s' is owned by %(st_uid)s unknown y yes Project-Id-Version: ufw
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-17 18:51-0600
PO-Revision-Date: 2016-01-02 12:19+0000
Last-Translator: Torsten Franz <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:31+0000
X-Generator: Launchpad (build 18115)
 
 
(Keine) 
Fehler beim Anwenden der Anwendungsregeln. 
Benutzung: %(progname)s %(command)s

%(commands)s:
 %(enable)-31s Aktivieren der Firewall
 %(disable)-31s Deaktivieren der Firewall
 %(default)-31s Festlegen der voreingestellten Richtlinie
 %(logging)-31s Festlegen des Logging-Level auf %(level)s
 %(allow)-31s Hinzufügen einer Erlaubnis %(rule)s
 %(deny)-31s Hinzufügen eines Verbots %(rule)s
 %(reject)-31s Hinzufügen einer Ablehnung %(rule)s
 %(limit)-31s Hinzufügen einer Begrenzung %(rule)s
 %(delete)-31s Entfernen %(urule)s
 %(insert)-31s Einfügen %(urule)s at %(number)s
 %(route)-31s Hinzufügen einer Route %(urule)s
 %(route-delete)-31s Entfernen einer Route %(urule)s
 %(route-insert)-31s Einfügen einer Route %(urule)s an %(number)s
 %(reload)-31s Neuladen der Firewall
 %(reset)-31s Neustarten der Firewall
 %(status)-31s Anzeigen des Firewall-Status
 %(statusnum)-31s Anzeigen des Firewall-Status als nummerierte Liste von %(rules)s
 %(statusverbose)-31s Anzeigen des detaillierten Firewall-Status
 %(show)-31s Anzeigen des Firewall-Reports
 %(version)-31s Anzeigen der Versionsinformationen

%(appcommands)s:
 %(applist)-31s Liste der Anwendungsprofile
 %(appinfo)-31s Anzeigen der Informationen für %(profile)s
 %(appupdate)-31s Aktualisierung %(profile)s
 %(appdefault)-31s Festlegen der voreingestellten Anwendungs-Richtlinie
  (das Neuladen der Firewall wurde übersprungen)  Versuchte Regeln erfolgreich zurückgezogen.  Einige Regeln konnten nicht rückgängig gemacht werden. %s hat Gruppenschreibrechte! %s ist global schreibbar! '%(f)s' Datei '%(name)s' existiert nicht '%s' existiert bereits. Abbruch '%s' ist nicht vorhanden '%s' ist schreibgeschützt (die Regeln müssen entsprechend aktualisiert werden) : Es wird mindestens Python 2.6 benötigt)
 Abgebrochen Aktion Benutzerregeln hinzugefügt (siehe »ufw status« für aktive Firewall): Hinzufügen von IPv6-Regel schlug fehl: IPv6 nicht aktiviert Verfügbare Anwendungen: '%(old)s' wird als '%(new)s' gesichert
 Fehlerhafte Zieladresse Falscher Name der Netzwerkschnittstelle Falscher Name der Netzwerkschnittstelle: Es kann kein Schnittstellenalias verwendet werden Fehlerhafter Schnittstellenname: belegtes Zeichen: »!« Falsche Art der Netzwerkschnittstelle Ungültiger Port Falscher Port '%s' Fehlerhafte Quelladresse Regel kann nicht an Position '%d' eingefügt werden Regel kann an Position '%s' nicht eingefügt werden Kann 'all' mit '--add-new' nicht bestimmen Einfügen und Löschen kann nicht festgelegt werden Ip6tables werden überprüft
 Iptables werden überprüft
 Ip6tables-Rohdaten werden überprüft
 Iptables-Rohdaten werden überprüft
 Kontrollen deaktiviert Befehl '%s' existiert bereits Der Befehl könnte bestehende SSH-Verbindungen unterbrechen. Fortfahren (%(yes)s|%(no)s)?  Konnte aus Regel '%s' nicht aussteigen Nicht vorhandene Regel kann nicht entfernt werden '%s' wurde nicht gefunden. Abbruch Konnte kein Profil finden, welches zu '%s' passt Konnte ausführbare Datei für '%s' nicht finden Konnte Profil '%s' nicht finden Konnte Protokoll nicht finden Regel '%d' nicht gefunden Regel '%s' nicht gefunden Konnte Zuhörstatus nicht empfangen Statistiken für '%s' konnten nicht abgerufen werden Protokollierungsregeln konnten nicht geladen werden Konnte Zieladresse nicht normalisieren Konnte Quelladresse nicht normalisieren '%s' kann nicht ausgeführt werden LOGLEVEL konnte nicht festgelegt werden Konnte laufende Firewall nicht aktualisieren Iptables-Version konnte nicht ermittelt werden Konnte '%s' nicht finden Konnte übergeordnete PID für '%s' nicht finden Konnte PID nicht finden (ist /proc eingehängt?) Konnte '%s' nicht zum Lesen öffnen Konnte stat für '%s' nicht aufrufen Konnte Anwendungsregeln nicht aktualisieren Konnte Regeldatei nicht aktualisieren Regeldatei für die Protokollierung kann nicht aktualisiert werden Voreingestellte %(direction)s-Regel in »%(policy)s« geändert
 Standardrichtlinie der Anwendung geändert in »%s« Voreinstellung: %(in)s (eingehend), %(out)s (abgehend), %(routed)s (gesendet) Wird gelöscht:
 %(rule)s
Fortfahren (%(yes)s|%(no)s)?  Beschreibung: %s

 Doppeltes Profil '%s', zuletzt gefundenes wird verwendet FEHLER: SGID-Bit sollte nicht gesetzt sein FEHLER: SUID-Bit sollte nicht gesetzt sein Die Firewall ist beim System-Start aktiv und aktiviert Firewall nicht eingeschaltet (überspringe Neustart) Firewall neu gestartet Firewall gestoppt und beim Systemstart deaktiviert Exakte Übereinstimmung gefunden Mehrere Treffer für '%s' gefunden. Bitte nutzen Sie den exakten Profilnamen Von Unterstützung für IPv6 nicht aktiviert Ungültige Regelsyntax Unzulässige Regelsyntax ('%s' angegeben mit Anwendungsregel) Einfügeposition '%s' ist keine gültige Position Ungültige '%s'-Klausel Ungültige 'Von'-Angabe Ungültige 'Port'-Angabe Ungültige 'Proto'-Angabe Ungültige 'Zu'-Angabe Ungültige IP Version '%s' Ungültige IPv6-Adresse bei Protokoll '%s' Ungültige Schnittstellenangabe Ungültige Oberflächen Ursache für Route-Regel Ungültige Protokollierungsstufe '%s' Ungültiger Log-Typ '%s' Ungültige Option Fehlerhafte Richtlinie '%(policy)s' für '%(chain)s' Ungültiger Port mit Protokoll '%s' Ungültige Ports in Profil '%s' Ungültige Position ' Ungültige Position '%d' Ungültiges Profil Ungültiger Profilname Ungültiges Token '%s' Protokollierung wurde deaktiviert Protokollierung wurde aktiviert Protokollierung:  Fehlende Richtlinie für '%s' Unterschiedliche IP-Versionen für 'Von' und 'Zu' Bei mehreren Ports muss 'tcp' oder 'udp' angegeben werden Benötige »von« oder »an« mit '%s' 'Zu'- oder 'Von'-Angabe benötigt Neue Profile: Keine Regeln für Anwendungsprofil gefunden Die Option 'log' ist hier nicht erlaubt Die Option 'log-all' ist hier nicht erlaubt Portbereiche müssen numerisch sein Port: Ports: Das benötigte Feld »%(f)s« für Profil »%(fn)s« ist leer Profil »%(fn)s« fehlt das benötigte Feld »%(f)s« Profil: %s
 Verzeichnis für Profile existiert nicht Keine Protokollübereinstimmung (Von/Zu) Keine Protokollübereinstimmung bei angegebenem Protokoll %s Alle Regeln auf Installationseinstellungen zurücksetzen. Fortfahren (%(yes)s|%(no)s)?  Alle Regeln auf Installationseinstellungen zurücksetzen. Dies könnte bestehende SSH-Verbindungen unterbrechen. Fortfahren (%(yes)s|%(no)s)?  Regel hinzugefügt Regel nach Normalisierung geändert Regel gelöscht Regel hinzugefügt Regel aktualisiert Regeln aktualisiert Regeln aktualisiert (v6) Regeln für Profil '%s' aktualisiert Neustart der Firewall übersprungen »%(value)s« wird übersprungen: Wert zu lang für »%(field)s« '%s' wird übersprungen: Auch in /etc/services '%s' wird übersprungen: Konnte nicht verarbeitet werden '%s' wird übersprungen: Dateistatus konnte nicht bestimmt werden '%s' wird übersprungen: Feld zu lang '%s' wird übersprungen: Ungültiger Name '%s' wird übersprungen: Name zu lang '%s' wird übersprungen: Zu groß '%s' wird übersprungen: Bereits zu viele Dateien gelesen IPv6-Anwendungsregel wird übersprungen. Es wird mindestens iptables 1.4 benötigt Hinzufügen einer vorhandenen Regel wird übersprungen Hinzufügen vorhandener Regel wird übersprungen Überspringe falsch geformtes Tupel (ungültige Länge): %s Überspringe ferhlerhaften tuple (iface): %s Überspringe falsch geformtes Tupel: %s Nicht unterstützte IPv4-»%s«-Regel werden übersprungen Überspringe nicht unterstützte IPv6 Regel '%s' Status: Aktiv
%(log)s
%(pol)s
%(app)s%(status)s Status: Aktiv%s Status: Inaktiv Titel: %s
 Zu Unbekannte Regel '%s' Nicht unterstützte Aktion '%s' Nicht unterstützte Standardrichtlinie Nicht unterstütze Richtung '%s' Nicht unterstützte Richtlinie '%s' Nicht unterstützte Regel für Richtung »%s« Nicht unterstütztes Protokoll '%s' WARN: '%s' ist global lesbar WARN: '%s' ist global beschreibbar Falsche Anzahl an Argumenten Es werden Root-Rechte benötigt, um dieses Skript auszuführen n Problem beim Aufruf von Problem bei der Ausführung von sysctl Problem bei der Ausführung von ufw-init
%s ufw-init wird ausgeführt uid ist %(uid)s, aber '%(path)s' gehört %(st_uid)s unbekannt j ja 