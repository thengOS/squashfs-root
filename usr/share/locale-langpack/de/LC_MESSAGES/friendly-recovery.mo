��    /      �  C                A   /     q     �  &   �     �     �                :     K  �   b     �                2     O     e     s     �  "   �     �     �     �  +   �  ,   "     O  !   f     �     �     �     �  �   �     �  U   �     	     7	     N	    ]	     u
     ~
     �
  	   �
     �
     �
  
   �
  �  �
  !   �  L   �  '   �  $     *   D      o     �     �     �     �     �  �        �  &   �       )   /     Y     r     �     �  #   �     �  	   �     �  6     <   H     �  0   �  $   �     �              $  #   ?     c  ,   �  !        2  �  G     �     �     �     �  ,         5     V     	           "          #       *         
      /          !                                                            -                     &   ,                      +         )                $       %      '   .   (                    Resume normal boot (Use arrows/PageUp/PageDown keys to scroll and TAB key to select) === Detailed disk usage === === Detailed memory usage === === Detailed network configuration === === General information === === LVM state === === Software RAID state === === System database (APT) === CPU information: Check all file systems Continuing will remount your / filesystem in read/write mode and mount any other filesystem defined in /etc/fstab.
Do you wish to continue? Database is consistent: Drop to root shell prompt Enable networking Finished, please press ENTER IP and DNS configured IP configured Network connectivity: No LVM detected (vgscan) No software RAID detected (mdstat) Physical Volumes: Read-only mode Read/Write mode Recovery Menu (filesystem state: read-only) Recovery Menu (filesystem state: read/write) Repair broken packages Revert to old snapshot and reboot Run in failsafe graphic mode Snapshot System mode: System summary The option you selected requires your filesystem to be in read-only mode. Unfortunately another option you selected earlier, made you exit this mode.
The easiest way of getting back in read-only mode is to reboot your system. Try to make free space Trying to find packages you don't need (apt-get autoremove), please review carefully. Unknown (must be run as root) Update grub bootloader Volume Groups: You are now going to exit the recovery mode and continue the boot sequence. Please note that some graphic drivers require a full graphical boot and so will fail when resuming from recovery.
If that's the case, simply reboot from the login screen and then perform a standard boot. no (BAD) none not ok (BAD) ok (good) unknown (must be run as root) unknown (read-only filesystem) yes (good) Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-03-08 14:38-0500
PO-Revision-Date: 2012-03-15 22:05+0000
Last-Translator: Ettore Atalan <atalanttore@googlemail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:23+0000
X-Generator: Launchpad (build 18115)
Language: 
    Startvorgang normal fortsetzen (Pfeiltasten/Bild auf/Bild ab zum Scrollen und die TAB-Taste zum Auswählen) === Detaillierte Festplattennutzung === === Detaillierte Speichernutzung === === Detaillierte Netzwerkkonfiguration === === Allgemeine Informationen === === LVM-Zustand === === Software-RAID-Status === === Systemdatenbank (APT) === Prozessorinformationen: Alle Dateisysteme überprüfen Wenn Sie fortfahren, wird Ihr /-Dateisystem im Lese-/Schreibmodus neu eingehängt und andere Dateisysteme, die in /etc/fstab definiert sind, werden ebenfalls eingehängt.
Möchten Sie fortfahren? Datenbank ist konsistent: Zur root-Befehlszeile (Shell) wechseln Netzwerk aktivieren Abgeschlossen, bitte drücken Sie EINGABE IP- und DNS-konfiguriert IP-konfiguriert Netzerkverbindungen: Kein LVM erkannt (vgscan) Kein Software-RAID erkannt (mdstat) Physikalische Datenträger: Nur Lesen Lesen/Schreiben-Modus Wiederherstellungsmenü (Dateisystemstatus: Nur Lesen) Wiederherstellungsmenü (Dateisystemstatus: Lesen/Schreiben) Kaputte Pakete reparieren Sicherungsabbild wiederherstellen und neustarten Im abgesicherten Grafikmodus starten Sicherungsabbild Systemmodus: Systemübersicht Für die getroffene Auswahl muss sich Ihr Dateisystem im »Nur Lesen«-Modus befinden. Leider hatte eine zuvor getroffene Auswahl zur Folge, dass dieser Modus verlassen wurde.
Die einfachste Möglichkeit, um in den »Nur Lesen«-Modus zurückzukehren ist ein Neustart Ihres Systems. Versuchen Speicherplatz freizugeben Es wird versucht, nicht mehr benötigte Pakete zu finden (apt-get autoremove), bitte prüfen Sie vor dem Entfernen sorgfältig. Unbekannt (muss als Root ausgeführt werden) Den grub-Bootloader aktualisieren Datenträgergruppen: Sie verlassen nun den Wiederherstellungsmodus und der Bootvorgang wird fortgesetzt. Bitte beachten Sie, dass einige Grafiktreiber einen vollständigen grafischen Start benötigen und somit nicht geladen werden, wenn Sie den Bootvorgang aus dem Wiederherstellungsmodus fortsetzen.
Sollte dies der Fall sein, starten Sie einfach vom Anmeldebildschirm neu und führen Sie einen normalen Start durch. Nein (Schlecht) Nichts Nicht OK (Schlecht) OK (Gut) Unbekannt (muss als Root ausgeführt werden) Unbekannt (Nur-Lese-Dateisystem) Ja (Gut) 