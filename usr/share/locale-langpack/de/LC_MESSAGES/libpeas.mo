��    #      4  /   L           	     $  #   *  %   N     t     �     �     �     �     �     �     �          
           -     N     V     c     o     �  \   �  ?   �  #   2  '   V     ~     �     �     �     �     �     �     �     �  �  �     �     �  *   �  +        2  0   Q  )   �     �     �  	   �     �     �     �     	     	  (   1	     Z	     b	     q	  $   	     �	  t   �	  Y   -
  -   �
  3   �
     �
     �
  
   �
            
   !     ,     3     B                         #            
          "                                                                !            	                                  - libpeas demo application About Additional plugins must be disabled An additional plugin must be disabled An error occurred: %s Dependency '%s' failed to load Dependency '%s' was not found Disable Plugins E_nable All Enabled Failed to load Peas Gtk Plugin Plugin Manager Plugin Manager View Plugin loader '%s' was not found Plugins Pr_eferences Preferences Run from build directory Show Builtin The '%s' plugin depends on the '%s' plugin.
If you disable '%s', '%s' will also be disabled. The following plugins depend on '%s' and will also be disabled: The plugin '%s' could not be loaded There was an error displaying the help. View _About _Cancel _Close _Disable All _Enabled _Help _Preferences _Quit Project-Id-Version: libpeas master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libpeas&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-24 00:33+0000
PO-Revision-Date: 2015-11-10 03:50+0000
Last-Translator: Paul Seyfert <Unknown>
Language-Team: German <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:58+0000
X-Generator: Launchpad (build 18115)
Language: de
 - libpeas-Demoanwendung Info Weitere Plugins müssen deaktiviert werden Ein weiteres Plugin muss deaktiviert werden Ein Fehler ist aufgetreten: %s Abhängigkeit »%s« konnte nicht geladen werden Abhängigkeit »%s« wurde nicht gefunden Plugins deaktivieren Alle aktiviere_n Aktiviert Laden fehlgeschlagen Peas Gtk Plugin Plugin-Verwaltung Ansicht der Plugin-Verwaltung Plugin-Lader »%s« wurde nicht gefunden Plugins _Einstellungen Einstellungen Aus dem Erstellungsordner ausführen Eingebaute anzeigen Das Plugin »%s« ist vom Plugin »%s« abhängig.
Falls Sie »%s« deaktivieren, wird »%s« ebenfalls deaktiviert. Die folgenden Plugins sind von »%s« abhängig und müssen ebenfalls deaktiviert werden: Das Plugin »%s« konnte nicht geladen werden Beim Anzeigen der Hilfe ist ein Fehler aufgetreten. Ansicht _Info _Abbrechen _Schließen Alle _deaktivieren Aktivi_ert _Hilfe _Einstellungen _Beenden 