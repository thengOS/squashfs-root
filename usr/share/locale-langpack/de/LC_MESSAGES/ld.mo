��    �      �    L      �     �     �     �  P      K   Q  P   �  I   �  O   8  �   �  N     M   k  �   �  @   D  L   �  �   �  �   �  �   M  �   �  K   �  O      M   P  +   �  K   �  M     8   d  4   �  B   �  I     L   _  F   �  �   �  J     E   �  O     K   `  F   �  P   �  &   D  =   k  L   �  ;   �  C   2  K   v  H   �  A      K   M   N   �   C   �   *   ,!     W!  !   p!     �!      �!  !   �!     �!      "     "     3"  6   N"  )   �"  8   �"  s   �"  )   \#  6   �#     �#  3   �#     $     0$     L$     k$     |$  4   �$  <   �$  ,   �$  <   +%  5   h%  "   �%  !   �%  %   �%  &   	&      0&     Q&  ,   p&  %   �&  /   �&      �&     '     2'     H'  -   f'     �'  !   �'     �'  .   �'  5   (  &   U(  %   |(     �(     �(     �(  3   �(  -   )  $   K)  1   p)  6   �)  A   �)  5   *  /   Q*  0   �*     �*     �*  /   �*  "   +  "   (+  7   K+  "   �+  /   �+  3   �+  "   
,  +   -,  -   Y,     �,  3   �,  8   �,     -  %   ,-  1   R-  +   �-     �-  "   �-     �-     .     .     %.  $   ).  
   N.  .   Y.     �.  	   �.  -   �.     �.     �.  )   /  )   //     Y/  	   y/     �/  %   �/  &   �/  7   �/     0     /0     40     =0  "   C0  
   f0     q0  (   y0     �0     �0  2   �0     1     1     $1     +1     C1     ^1     p1     u1     �1     �1  %   �1     �1     �1     �1  !   �1  '   �1     !2     ;2     Y2     _2     d2     k2     }2     �2  (   �2     �2     �2  #   �2     #3     83  .   J3  (   y3     �3     �3     �3     �3  �   �3     �4  #   �4  4   �4  5   	5  '   ?5     g5     p5     �5     �5  	   �5     �5  	   �5  �  �5     �7     �7     �7  V   �7  R   08  ]   �8  ;   �8  I   9  �   g9  c   �9  L   S:  �   �:  I   I;  B   �;  �   �;  �   �<  �   e=  �   >  R   �>  Y   L?  I   �?  ,   �?  G   @  P   e@  8   �@  C   �@  3   3A  H   gA  M   �A  4   �A  �   3B  R   �B  ]   C  n   lC  S   �C  ^   /D  o   �D  -   �D  1   ,E  B   ^E  @   �E  9   �E  _   F  E   |F  U   �F  F   G  J   _G  ?   �G  /   �G     H  .   8H     gH  6   zH  /   �H     �H     �H     I     4I  ?   QI  0   �I  P   �I  �   J  0   �J  ?   �J      K  I   ?K  0   �K     �K  "   �K     �K     L  B   !L  P   dL  2   �L  D   �L  :   -M  .   hM  &   �M  *   �M  2   �M  %   N  -   BN  A   pN  7   �N  >   �N  #   )O     MO     iO  %   �O  @   �O     �O  0   
P  +   ;P  =   gP  6   �P  ;   �P  6   Q  *   OQ  )   zQ     �Q  =   �Q  5   �Q  (   &R  7   OR  =   �R  S   �R  @   S  1   ZS  ;   �S     �S     �S  8   �S  )   (T  "   RT  7   uT  )   �T  8   �T  G   U  .   XU  +   �U  <   �U     �U  9   V  ?   JV  ,   �V  6   �V  L   �V  =   ;W  )   yW  5   �W     �W     �W     X     X  /   X     DX  1   RX  "   �X     �X  .   �X     �X     �X  4   Y  5   LY     �Y  	   �Y     �Y  :   �Y  .    Z  J   /Z  $   zZ     �Z  	   �Z     �Z  0   �Z  
   �Z  	   �Z  1   �Z  #   .[  $   R[  ;   w[     �[     �[     �[     �[  !   �[     \     /\     4\     C\     ]\  '   f\     �\     �\     �\  0   �\  ,   �\     ]     %]     D]     J]     R]     Y]  "   i]  5   �]  ?   �]     ^     ^  7   -^     e^     �^  R   �^  7   �^     #_     8_     T_     [_  �   `_     S`  3   r`  M   �`  J   �`  0   ?a     pa  /   xa  ,   �a     �a     �a      b  	   b                 S              �   �          n   O   �           �   �   �       G       �   �   X       �   x       E   s   �          �   @   �   �   u   ~   F   i                  /   {   �      :   b      &           �           �   J   �   �       �   ,   (   ;   �       A   �   �   q       j   M   �   P       h   �      k           �      �   o      �       �   }   r   �      �       K   �   #      +   ]   	   �   c   a   5   m   �   2   0   W          9       z   �   �   C         �   �      `      �                  R   �   Q   '       _   �   .           �                   7   %   "   =   �   1   �   �       e   �          ^   T   �          v   �   �   �   �       Y      �   �   �   [       N   B       �   f   �          \      3   )   �   �           �          �       �   I   �   *   8       �   p   �   y       �           �       d   t   4   w   D       <   Z       �   H   ?   U   6   �   �   �           !   l   �       �       �          �   �   �   �          >       �       L       $       �       �   �   �       
   �       �         V   g      -      |   �    
Cross Reference Table

 
Memory Configuration

 
Set                 Symbol

                                      Exclude objects, archive members from auto
                                      This makes binaries non-deterministic
                                      export, place into import library instead.
   --add-stdcall-alias                Export symbols with and without @nn
   --base_file <basefile>             Generate a base file for relocatable DLLs
   --compat-implib                    Create backward compatible import libs;
                                       create __imp_<SYMBOL> as well.
   --disable-auto-image-base          Do not auto-choose image base. (default)
   --disable-auto-import              Do not auto-import DATA items from DLLs
   --disable-runtime-pseudo-reloc     Do not add runtime pseudo-relocations for
                                       auto-imported DATA.
   --disable-stdcall-fixup            Don't link _sym to _sym@nn
   --dll                              Set image base to the default for DLLs
   --dll-search-prefix=<string>       When linking dynamically to a dll without
                                       an importlib, use <string><basename>.dll
                                       in preference to lib<basename>.dll 
   --enable-auto-image-base           Automatically choose image base for DLLs
                                       unless user specifies one
   --enable-extra-pe-debug            Enable verbose debug output when building
                                       or linking to DLLs (esp. auto-import)
   --enable-runtime-pseudo-reloc      Work around auto-import limitations by
                                       adding pseudo-relocations resolved at
                                       runtime.
   --enable-stdcall-fixup             Link _sym to _sym@nn without warnings
   --exclude-all-symbols              Exclude all symbols from automatic export
   --exclude-libs lib,lib,...         Exclude libraries from automatic export
   --exclude-modules-for-implib mod,mod,...
   --exclude-symbols sym,sym,...      Exclude symbols from automatic export
   --export-all-symbols               Automatically export all globals to DLL
   --file-alignment <size>            Set file alignment
   --forceinteg		 Code integrity checks are enforced
   --heap <size>                      Set initial size of the heap
   --image-base <address>             Set start address of the executable
   --insert-timestamp                 Use a real timestamp rather than zero.
   --kill-at                          Remove @nn from exported symbols
   --large-address-aware              Executable supports virtual addresses
                                       greater than 2 gigabytes
   --major-image-version <number>     Set version number of the executable
   --major-os-version <number>        Set minimum required OS version
   --major-subsystem-version <number> Set minimum required OS subsystem version
   --minor-image-version <number>     Set revision number of the executable
   --minor-os-version <number>        Set minimum required OS revision
   --minor-subsystem-version <number> Set minimum required OS subsystem revision
   --no-bind			 Do not bind this image
   --out-implib <file>                Generate import library
   --output-def <file>                Generate a .DEF file for the built DLL
   --section-alignment <size>         Set section alignment
   --stack <size>                     Set size of the initial stack
   --subsystem <name>[:<version>]     Set required OS subsystem [& version]
   --support-old-code                 Support interworking with old code
   --support-old-code          Support interworking with old code
   --thumb-entry=<sym>         Set the entry point to be Thumb symbol <sym>
   --thumb-entry=<symbol>             Set the entry point to be Thumb <symbol>
   --warn-duplicate-exports           Warn about duplicate exports.
   --wdmdriver		 Driver uses the WDM model
   Supported emulations:
   no emulation specific options.
 %8x something else
 %B%F: could not read relocs: %E
 %B%F: could not read symbols: %E
 %B: In function `%T':
 %B: file not recognized: %E
 %B: matching formats: %B: warning: defined here
 %B: warning: more undefined references to `%T' follow
 %B: warning: undefined reference to `%T'
 %C: Cannot get section contents - auto-import exception
 %C: variable '%T' can't be auto-imported. Please read the documentation for ld's --enable-auto-import for details.
 %C: warning: undefined reference to `%T'
 %D: warning: more undefined references to `%T' follow
 %F%B: file not recognized: %E
 %F%P: attempted static link of dynamic object `%s'
 %F%P: cannot open base file %s
 %F%P: internal error %s %d
 %F%P: invalid BFD target `%s'
 %F%S %% by zero
 %F%S / by zero
 %F%S can not PROVIDE assignment to location counter
 %F%S cannot move location counter backwards (from %V to %V)
 %F%S invalid assignment to location counter
 %F%S: undefined MEMORY region `%s' referenced in expression
 %F%S: undefined symbol `%s' referenced in expression
 %P%F: Failed to create hash table
 %P%F: bfd_hash_lookup failed: %E
 %P%F: bfd_hash_table_init failed: %E
 %P%F: bfd_link_hash_lookup failed: %E
 %P%F: bfd_new_link_order failed
 %P%F: can't set start address
 %P%F: cannot open linker script file %s: %E
 %P%F: cannot open output file %s: %E
 %P%F: invalid hex number for PE parameter '%s'
 %P%F: invalid subsystem type %s
 %P%F: missing argument to -m
 %P%F: no input files
 %P%F: please report this bug
 %P%F: strange hex info for PE parameter '%s'
 %P%F: target %s not found
 %P%F:%s: can't set start address
 %P%F:%s: hash creation failed
 %P%X: %s does not support reloc %s for set %s
 %P%X: Different object file formats composing set %s
 %P%X: Different relocs used in set %s
 %P%X: Unsupported size %d for set %s
 %P: Error closing file `%s'
 %P: Error writing file `%s'
 %P: mode %s
 %P: skipping incompatible %s when searching for %s
 %P: symbol `%T' missing from main hash table
 %P: unrecognised emulation mode: %s
 %P: warning, file alignment > section alignment.
 %P: warning: '--thumb-entry %s' is overriding '-e %s'
 %P: warning: address of `%s' isn't multiple of maximum page size
 %P: warning: bad version number in -subsystem option
 %P: warning: cannot find thumb start symbol %s
 %P:%S: warning: memory region `%s' not declared
 %S HLL ignored
 %S SYSLIB ignored
 %X%B: more undefined references to `%T' follow
 %X%B: undefined reference to `%T'
 %X%C: multiple definition of `%T'
 %X%C: prohibited cross reference from %s to `%T' in %s
 %X%C: undefined reference to `%T'
 %X%D: more undefined references to `%T' follow
 %X%P: bfd_hash_table_init of cref table failed: %E
 %X%P: cref_hash_lookup failed: %E
 %X%P: error: duplicate retain-symbols-file
 %X%P: unable to find version dependency `%s'
 %X%P: unknown feature `%s'
 %X%P: unknown language `%s' in version information
 %X%S: unresolvable symbol `%s' referenced in expression
 %XCan't open .lib file: %s
 %XCannot export %s: symbol not found
 %XCannot export %s: symbol wrong type (%d vs %d)
 %XError, ordinal used twice: %d (%s vs %s)
 %XError: %d-bit reloc in dll
 %s: Can't open output def file %s
 %s: data size %ld
 ; no contents available
 ADDRESS ARG Add DIRECTORY to library search path Attributes Copyright 2014 Free Software Foundation, Inc.
 Creating library file: %s
 DIRECTORY Default search path for Solaris compatibility Define a symbol Discard all local symbols Discard temporary local symbols (default) Do not treat warnings as errors (default) Don't discard any local symbols EMULATION End a group Errors encountered processing file %s Errors encountered processing file %s
 Errors encountered processing file %s for interworking
 Export all dynamic symbols FILE FILENAME File
 Force common symbols to be defined GNU ld %s
 Ignored Ignored for GCC LTO option compatibility Ignored for SVR4 compatibility Ignored for SunOS compatibility Info: resolving %s by linking to %s (auto-import)
 KEYWORD LIBNAME Length Link big-endian objects Link little-endian objects Load named plugin Name No symbols
 Optimize output file Origin Override the default sysroot location PATH PLUGIN PROGRAM Print map file on standard output Print version and emulation information Print version information Read MRI format linker script SHLIB SIZE SYMBOL SYMBOL=EXPRESSION Search for library LIBNAME Send arg to last-loaded plugin Set PROGRAM as the dynamic linker to use Set architecture Set emulation Set internal name of shared library Set output file name Set start address Small data size (if no size, same as --shared) Specify target for following input files Start a group Supported emulations:  Symbol TARGET This program is free software; you may redistribute it under the terms of
the GNU General Public License version 3 or (at your option) a later version.
This program has absolutely no warranty.
 Treat warnings as errors Undo the effect of --export-dynamic Use --disable-stdcall-fixup to disable these fixups
 Use --enable-stdcall-fixup to disable these warnings
 Warning: resolving %s by linking to %s
 [=STYLE] attempt to open %s failed
 attempt to open %s succeeded
 cannot find script file %s
 no symbol opened script file %s
 warning:  Project-Id-Version: binutils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2015-10-21 17:56+0000
Last-Translator: schuko24 <gerdsaenger@t-online.de>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:03+0000
X-Generator: Launchpad (build 18115)
 
Kreuzreferenz-Tabelle

 
↵
Speichereinrichtung
↵

 
Satzsymbol

                                      Objekte, Archivmitglieder aus auto ausschließen
                                      Dieses macht Binärdateien undeterministisch
                                      Exportieren, abweichend jedoch in die Importbibliothek.
   --add-stdcall-alias Symbole mit und ohne @nn exportieren
   --base_file <Basisdatei> Basisdatei für verschiebbare DLLs generieren
   --compat-implib Abwärtskompatible Importbibliothek erzeugen;
                                       ebenso __imp_<SYMBOL> erzeugen.
   --disable-auto-image-base Basis für Abbild von DLLs nicht automatisch wählen. (Voreinstellung)
   --disable-auto-import DATA-Objekte nicht automatisch aus DLLs importieren
   --disable-runtime-pseudo-reloc Keine Pseudo-Verschiebungen zur Laufzeit für
                                       automatisch importierte DATA-Objekte hinzufügen.
   --disable-stdcall-fixup _sym Keine symbolische Verknüpfung zu _sym@nn
   --dll Basis für Abbild auf die Voreinstellung für DLLs setzen
   --dll-search-prefix=<Zeichenkette> Beim dynamischen Binden an eine DLL ohne
                                       Importbibliothek: <Zeichenkette><Basisname>.dll gegenüber
                                       lib<Basisname>.dll bevorzugen 
   --enable-auto-image-base Basis für Abbild von DLLs automatisch wählen
                                       außer es wird vom Benutzer angegeben
   --enable-extra-pe-debug Ausführliche Fehlerdiagnoseausgabe beim Erstellen von
                                       oder Binden mit DLLs (bes. auto-import)
   --enable-runtime-pseudo-reloc Einschränkungen beim automatischen Import
                                       durch Hinzufügen von Pseudo-Verschiebungen
                                       umgehen, die zur Laufzeit aufgelöst werden.
   --enable-stdcall-fixup _sym Symbolische Verknüpfung mit _sym@nn ohne Warnungen
   --exclude-all-symbols              Alle Symbole vom automatischen Export ausschließen
   --exclude-libs lib,lib, … Bibliotheken nicht automatisch exportieren
   --exclude-modules-for-implib mod,mod, …
   --exclude-symbols sym,sym, … Symbole nicht automatisch exportieren
   --export-all-symbols Automatisch alle globalen Symbole in die DLL exportieren
   --file-alignment <Größe> Dateiausrichtung festlegen
   --forceinteg		 Integritätsprüfungen des Codes werden erzwungen
   --heap <Größe> Startgröße des Heap festlegen
   --image-base <Adresse> Startadresse der ausführbaren Datei festlegen
   --insert-timestamp                 Nutze den echten Zeitstempel statt Null
   --kill-at @nn aus exportierten Symbolen entfernen
   --large-address-aware Die ausführbare Datei unterstützt virtuelle
                                       Adressen über 2 Gigabyte
   --major-image-version <Nummer> Versionsnummer der ausführbaren Datei festlegen
   --major-os-version <Nummer> Mindestens erforderliche Version des Betriebssystems festlegen
   --major-subsystem-version <Nummer> Mindestens erforderliche Version des Betriebssystem-Subsystems festlegen
   --minor-image-version <Nummer> Revisionsnummer der ausführbaren Datei festlegen
   --minor-os-version <Nummer> Mindestens erforderliche Revision des Betriebssystems festlegen
   --minor-subsystem-version <Nummer> Mindestens erforderliche Revision des Betriebssystem-Subsystems festlegen
   --no-bind			 Dieses Abbild nicht verbinden
   --out-implib <Datei> Importbibliothek erzeugen
   --output-def <Datei> .DEF-Datei für die erstellte DLL erzeugen
   --section-alignment <Größe> Abschnittssusrichtung festlegen
   --stack <Größe> Größe des Anfangsstapels festlegen
   --subsystem <Name>[:<Version>] Erforderliches Betriebssystem-Subsystem [& Version] festlegen
   --support-old-code Zusammenspiel mit altem Quelltext unterstützen
   --support-old-code Unterstützt die zusammenarbeit mit älteren Quelltextversionen
   --thumb-entry=<sym> Den Startpunkt auf Thumb-Symbol <sym> festlegen
   --thumb-entry=<Symbol> Anfangspunkt auf Thumb-Symbol <Symbol> festlegen
   --warn-duplicate-exports Warnung beim Export von Duplikaten.
   --wdmdriver		 Treiber benutzt das WDM Modell
   Unterstützte Emulationen:
   Keine emulationsspezifischen Einstellungen.
 %8x etwas anderes
 %B%F: Verschiebungen konnten nicht gelesen werden: %E
 %B%F: Symbole konnten nicht gelesen werden: %E
 %B: In Funktion `%T':
 %B: Datei nicht erkannt: %E
 %B: Übereinstimmende Formate: %B: Warnung: Hier definiert
 %B: Warnung: Weitere nicht definierte Verweise auf `%T' folgen
 %B: Warnung: Nicht definierter Verweis auf `%T'
 %C: Der Inhalt des Abschnitts ist nicht verfügbar – »auto-import«-Ausnahme
 %C: Variable »%T« kann nicht automatisch importiert werden. Einzelheiten entnehmen Sie bitte der Dokumentation zur Funktion --enable-auto-import von ld.
 %C: Warnung: Nicht definierter Verweis auf `%T'
 %D: Warnung: Weitere nicht definierte Verweise auf `%T' folgen
 %F%B: Datei nicht erkannt: %E
 %F%P: Es wurde versucht, das dynamische Objekt »%s« statisch zu binden
 %F%P: Basisdatei %s kann nicht geöffnet werden
 %F%P: Interner Fehler %s %d
 %F%P: Ungültiges BFD-Ziel »%s«
 %F%S %% durch Null
 %F%S / durch Null
 %F%S PROVIDE-Zuweisung an den Positionszähler ist nicht möglich
 %F%S der Positionszähler kann nicht rückwärts bewegt werden (von %V nach %V)
 %F%S Ungültige Zuweisung an den Positionszähler
 %F%S: Verweis auf undefinierten SPEICHER-Bereich »%s« im Ausdruck
 %F%S: Verweis auf undefiniertes Symbol »%s« im Ausdruck
 %P%F: Anlegen der Hash-Tabelle fehlgeschlagen
 %P%F: bfd_hash_lookup gescheitert: %E
 %P%F: bfd_hash_table_init gescheitert: %E
 %P%F: bfd_link_hash_lookup ist fehlgeschlagen: %E
 %P%F: bfd_new_link_order gescheitert
 %P%F: Startadresse kann nicht gesetzt werden
 %P%F: Verknüpfte Skriptdatei kann nicht geöffnet werden %s: %E
 %P%F: Ausgabedatei  %s: %E kann nicht geöffnet werden
 %P%F: Ungültige Hexadezimalzahl für den PE-Parameter »%s«
 %P%F: Ungültiger Subsystem-Typ %s
 %P%F: Argument zu -m fehlt
 %P%F: Keine Eingabedateien
 %P%F: Bitte melden Sie diesen Fehler
 %P%F: Merkwürdige Hex-Information für den PE-Parameter »%s«
 %P%F: Ziel %s nicht gefunden
 %P%F:%s: Startadresse kann nicht gesetzt werden
 %P%F:%s: Hash-Erzeugung ist fehlgeschlagen
 %P%X: %s unterstützt keine Verschiebung %s für den Satz %s
 %P%X: Unterschiedliche Objektdatei-Formate im Satz %s
 %P%X: Unterschiedliche Verschiebungen im Satz %s verwendet
 %P%X: Nicht unterstützte Größe %d für den Satz %s
 %P: Fehler beim Schließen der Datei `%s'
 %P: Fehler beim Schreiben der Datei `%s'
 %P: Modus %s
 %P: Inkompatible %s wird übersprungen bei der Suche nach %s
 %P: Das Symbol »%T« fehlt in der Haupt-Hashtabelle
 %P: Nicht erkannter Emulationsmodus: %s
 %P: Warnung, Dateiausrichtung > Abschnittsausrichtung.
 %P: Warnung: »--thumb-entry %s« wird übergangen »-e %s«
 %P: Achtung:: Die Adresse von `%s' ist kein Vielfaches der maximalen Seitengröße
 %P: Warnung: Ungültige Versionsnummer in der Option -subsystem
 %P: Warnung: Thumb-Startsymbol %s nicht gefunden
 %P:%S: Achtung: Speicherbereich `%s' wurde nicht angegeben
 %S HLL ignoriert
 %S SYSLIB ignoriert
 %X%B: Weitere nicht definierte Verweise auf `%T' folgen
 %X%B: Nicht definierter Verweis auf `%T'
 %X%C: Mehrfachdefinition von `%T'
 %X%C: Verbotene Kreuzreferenz von %s nach »%T« in %s
 %X%C: Nicht definierter Verweis auf `%T'
 %X%D: Weitere nicht definierte Verweise auf `%T' folgen
 %X%P: bfd_hash_table_init für die cref-Tabelle ist fehlgeschlagen: %E
 %X%P: cref_hash_lookup ist fehlgeschlagen: %E
 %X%P: Fehler: Doppelte retain-symbols-file
 %X%P: Versionsabhängigkeit `%s' kann nicht gefunden werden
 %X%P: Unbekannte Funktion `%s'
 %X%P: unbekannte Sprache `%s' in der Versionsinformation
 %X%S: Verweis auf nicht auflösbares Symbol »%s« im Ausdruck
 %X.lib-Datei %s kann nicht geöffnet werden
 %XExport von %s nicht möglich: Symbol nicht gefunden
 %XExport von %s nicht möglich: Das Symbol hat den falschen Typ (%d vs. %d)
 %XFehler, Ordinalzahl wird doppelt verwendet: %d (%s vs. %s)
 %XFehler: %d-bit Verschiebung in der DLL
 %s: .def-Ausgabedatei %s kann nicht geöffnet werden
 %s: Datengröße %ld
 ; Kein Inhalt vorhanden
 ADRESSE ARG VERZEICHNIS zum Bibliothekssuchpfad hinzufügen Eigenschaften Urheberrecht 2014 Free Software Foundation, Inc.
 Bibliotheksdatei wird erzeugt: %s
 VERZEICHNIS Standard-Suchpfad für Solaris-Kompatibilität Ein Symbol definieren Alle lokalen Symbole verwerfen Temporäre lokale Symbole verwerfen (Voreinstellung) Warnungen nicht wie Fehler behandeln (Voreinstellung) Keine lokalen Symbole verwerfen EMULATION Eine Gruppe abschließen Bei der Verarbeitung der Datei %s sind Fehler aufgetreten. Fehler gefunden beim Verarbeiten der Datei %s
 Es sind Fehler während der Zwischenverarbeitung der Datei %s aufgetreten
 Alle dynamischen Symbole exportieren DATEI DATEINAME Datei
 Das definieren gebräuchlicher Symbole erzwingen GNU ld %s
 Ignoriert Für Kompatibilität zur GCC-LTO-Option ignoriert Für SVR4-Kompatibilität ignoriert Für SunOS-Kompatibilität ignoriert Info: %s wird durch Bindung an %s aufgelöst (auto-import)
 SCHLÜSSELWORT LIBNAME Länge big-endian-Objekte verknüpfen little-endian-Objekte verknüpfen Angegebene Erweiterung laden Name Keine Symbole
 Ausgabedateien optimieren Ursprung Den Standard-Sysroot-Ort überschreiben PFAD ERWEITERUNG PROGRAMM Zuordnungsdatei auf der Standardausgabe ausgeben Version und Emulationsinformationen anzeigen Versionsinformationen anzeigen MRI-Format-Linker-Skript lesen SHLIB GRÖßE SYMBOL SYMBOL=AUSDRUCK Nach der Bibliothek LIBNAME suchen Argument an die zuletzt geladene Erweiterung schicken Über PROGRAMM den zu verwendenden dynamischen Linker festlegen Architektur festlegen Emulation auswählen Den internen Namen der gemeinsamen Bibliothek festlegen Ausgabedateinamen festlegen Startadresse festlegen Kleine Datengröße (falls keine Größe angegeben wird, das gleiche wie --shared) Geben Sie das Ziel für die folgenden Eingabedateien an Eine Gruppe beginnen Unterstützte Emulationen:  Symbol ZIEL Dieses Programm ist freie Software; Sie können es unter den Bedingungen der
GNU General Public License Version 3 oder (freigestellt) einer späteren 
Version weiterverteilen . Es wird keinerlei Gewährleistung für das Programm übernommen.
 Warnungen wie Fehler behandeln Den Effekt von --export-dynamic rückgängig machen Verwenden Sie --disable-stdcall-fixup, um diese Korrekturen zu unterdrücken
 Verwenden Sie --enable-stdcall-fixup, um diese Warnungen zu unterdrücken
 Warnung: %s wird durch Bindung an %s aufgelöst
 [=STIL] Der Versuch, %s zu öffnen, ist fehlgeschlagen
 Der Versuch, %s zu öffnen, war erfolgreich
 Skriptdatei %s nicht gefunden
 Kein Symbol Skriptdatei %s geöffnet
 Warnung:  