��          �      L      �     �  �   �  -   �  -   �  >   �     >  0   C  ;   t     �     �  "   �     �           :     H     d          �  �  �  ,   =  �   j  :   X  A   �  K   �     !  >   &  C   e  &   �     �  .   �  *   	  .   A	     p	     �	     �	  %   �	     �	                                
                          	                                          Convert key to lower case Copyright (C) %s Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 Create simple DB database from textual input. Do not print messages while building database INPUT-FILE OUTPUT-FILE
-o OUTPUT-FILE INPUT-FILE
-u INPUT-FILE NAME Print content of database file, one entry a line Report bugs using the `glibcbug' script to <bugs@gnu.org>.
 Write output to file NAME Written by %s.
 cannot open database file `%s': %s cannot open input file `%s' cannot open output file `%s': %s duplicate key problems while reading `%s' while reading database: %s while writing database file: %s wrong number of arguments Project-Id-Version: nss_db 2.1.93
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2000-09-10 16:37+0200
PO-Revision-Date: 2006-03-20 18:10+0000
Last-Translator: Mark Kettenis <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:03+0000
X-Generator: Launchpad (build 18115)
 Übersetze den Schlüssel in Kleinbuchstaben Copyright © %s Free Software Foundation, Inc.
Dies ist freie Software; in den Quellen befinden sich die Lizenzbedingungen.
Es gibt KEINERLEI Garantie; nicht einmal für die TAUGLICHKEIT oder
VERWENDBARKEIT FÜR EINEN ANGEGEBENEN ZWECK.
 Erstellt eine einfach DB Datenbank aus einer Text-Eingabe. Gebe keine Nachrichten aus, während die Datenbank aufgebaut wird EINGABE-DATEI AUSGABE-DATEI
-o AUSGABE-DATEI EINGABE-DATEI
-u EINGABE-DATEI NAME Gibt den Inhalt der Datenbank-Datei aus, je Eintrag eine Zeile Fehler bitte mit dem »glibcbug«-Skript an <bugs@gnu.org> melden.
 Schreibe die Ausgabe in die Datei NAME Implementiert von %s.
 Kann die Ausgabedatei »%s« nicht öffnen: %s Kann die Eingabedatei »%s« nicht öffnen Kann die Ausgabedatei »%s« nicht öffnen: %s Doppelter Schlüssel Probleme beim Lesen von »%s« beim Lesen der Datenbank: %s beim Schreiben der Datenbankdatei: %s Falsche Anzahl an Argumenten 