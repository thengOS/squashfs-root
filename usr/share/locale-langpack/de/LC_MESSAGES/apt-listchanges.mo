��    !      $  /   ,      �  $   �       /   *  g   Z     �     �  *   �       M        j     �  (   �     �     �     �     �     �        *   ,  �   W  ;   �       0   2  <   c  r   �  2        F     b  "   x     �     �  #   �  �  �  +   �	     �	  C   
  l   O
  	   �
     �
  <   �
       w   0     �     �  3   �               '     >     R  #   b  2   �  �   �  O   b     �  4   �  D     �   H  C   �          )  #   F  $   j  $   �  *   �     !                                                                   	                                
                                                       %s: Version %s has already been seen %s: will be newly installed --since=<version> expects a only path to a .deb <big><b>Changelogs</b></big>

The following changes are found in the packages you are about to install: Aborting Changes for %s Confirmation failed, don't save seen state Continue Installation? Could not run apt-changelog (%s), unable to retrieve changelog for package %s Do you want to continue? [Y/n]  Done Ignoring `%s' (seems to be a directory!) Informational notes List the changes Mailing %s: %s News for %s Reading changelogs Reading changelogs. Please wait. The %s frontend is deprecated, using pager The gtk frontend needs a working python-gtk2 and python-glade2.
Those imports can not be found. Falling back to pager.
The error is: %s
 The mail frontend needs a installed 'sendmail', using pager Unknown frontend: %s
 Unknown option %s for --which.  Allowed are: %s. Usage: apt-listchanges [options] {--apt | filename.deb ...}
 Wrong or missing VERSION from apt pipeline
(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version set to 2?)
 You can abort the installation if you select 'no'. apt-listchanges: Changelogs apt-listchanges: News apt-listchanges: changelogs for %s apt-listchanges: news for %s database %s failed to load.
 didn't find any valid .deb archives Project-Id-Version: apt-listchanges 2.3
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-17 14:04+0000
PO-Revision-Date: 2013-01-18 10:24+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <debian-l10n-german@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:57+0000
X-Generator: Launchpad (build 18115)
Generated-By: pygettext.py 1.4
 %s: Version %s wurde schon einmal angezeigt %s: wird erstmals installiert --since=<version> Erwartet einen einzelnen Pfad zu einem .deb-Paket <big><b>Changelogs</b></big>

Die folgenden Änderungen betreffen die Pakete, die Sie installieren möchten: Breche ab Änderungen für %s Bestätigung schlug fehl, speichere »gesehen«-Status nicht Installation fortfahren? Apt-changelog (%s) konnte nicht ausgeführt werden, Änderungsprotokoll für das Paket %s konnte nicht abgerufen werden Wollen Sie fortsetzen? [J/n]  Fertig Ignoriere »%s« (scheint ein Verzeichnis zu sein!) Anmerkungen Liste die Änderungen auf Sende E-Mail an %s: %s Neuigkeiten für %s Lese Changelogs Lese Neuerungslisten. Bitte warten. Die Oberfläche %s ist veraltet, benutze den Pager Die GTK-Oberfläche benötigt ein funktionierendes Python-gtk2 und Python-glade2.
Diese können nicht importiert werden. Falle auf Pager zurück.
Der Fehler lautet: %s
 Die E-Mail-Oberfläche benötigt ein installiertes »sendmail«, verwende Pager Unbekannte Oberfläche: %s
 Unbekannte Option %s für --which. Erlaubt sind: %s. Verwendung: apt-listchanges [Optionen] {--apt | Dateiname.deb ....}
 Falsche oder fehlende VERSION von der apt-Pipeline
(ist Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version auf 2 gesetzt?)
 Sie können die Installation abbrechen, falls Sie »nein« wählen. apt-listchanges: Changelogs apt-listchanges: Neuigkeiten apt-listchanges: Changelogs für %s apt-listchanges: Neuigkeiten für %s Laden der Datenbank %s schlug fehl.
 konnte keine gültigen .deb-Archive finden 