��    &      L  5   |      P  ,   Q  '   ~  (   �  	   �  z   �     T     g     �     �     �     �     �          +     B  !   Y     {  $   �     �  -   �  "     "   &  #   I     m  &   �     �     �  (   �          '     F     \  +   o     �  .   �  ,   �  A   	  �  K  2   �	  3   0
  (   d
  
   �
  �   �
     /     E  )   e     �  (   �  &   �  *   �  (   '  '   P  (   x  >   �  (   �  $   	     .  E   L  6   �  (   �  0   �  "   #  4   F  $   {     �  *   �  (   �  ,        ;     Z  5   n     �  8   �  6   �  Y   (                                         
   #                 $                                           &                              	                            %   !         "    'since' option specifies most recent version 'until' option specifies oldest version Copyright (C) 2005 by Frank Lichtenheld
 FATAL: %s This is free software; see the GNU General Public Licence version 2 or later for copying conditions. There is NO warranty. WARN: %s(l%s): %s
 WARN: %s(l%s): %s
LINE: %s
 bad key-value after `;': `%s' badly formatted heading line badly formatted trailer line badly formatted urgency value can't close file %s: %s can't load IO::String: %s can't lock file %s: %s can't open file %s: %s changelog format %s not supported couldn't parse date %s fatal error occured while parsing %s field %s has blank lines >%s< field %s has newline then non whitespace >%s< field %s has trailing newline >%s< found blank line where expected %s found change data where expected %s found eof where expected %s found start of entry where expected %s found trailer where expected %s ignored option -L more than one file specified (%s and %s) no changelog file specified output format %s not supported repeated key-value %s too many arguments unknown key-value key %s - copying to XS-%s unrecognised line you can only specify one of 'from' and 'since' you can only specify one of 'to' and 'until' you can't combine 'count' or 'offset' with any other range option Project-Id-Version: libparse-debianchangelog-perl
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-08-25 20:38+0000
PO-Revision-Date: 2013-05-24 13:42+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 Die Option »since« legt die neueste Version fest Die Option »until« legt die älteste Version fest Copyright © 2005 von Frank Lichtenheld
 FEHLER: %s Dieses Programm ist freie Software, die unter der GNU General Public Licence Version 2 oder neueren Versionen lizenziert wird. Es gibt KEINE Garantie. WARNUNG: %s(l%s): %s
 WARNUNG: %s(l%s): %s
ZEILE: %s
 ungültiger Schlüsselwert nach »;«: %s falsch formatierte Kopfzeile ungültig formatierte Zeile im Nachspann falsch formatierter Dringlichkeitswert Datei %s kann nicht geschlossen werden: %s IO::String kann nicht geladen werden: %s Datei %s kann nicht gesperrt werden: %s Datei %s kann nicht geöffnet werden: %s Format %s für das Änderungsprotokoll wird nicht unterstützt Datum %s konnte nicht ausgewertet werden Fataler Fehler beim Auswerten von %s Feld %s hat Leerzeilen »%s« Feld %s hat einen Zeilenumbruch und dann ein nicht-Leerzeichen »%s« Feld %s hat einen abschließenden Zeilenumbruch »%s« Leerzeile statt des erwarten %s gefunden Änderungsdaten statt des erwarteten %s gefunden EOF gefunden, wo %s erwartet wurde Beginn eines Eintrags gefunden, wo %s erwartet wurde Nachspann gefunden, aber %s erwartet Option -L wurde ignoriert Mehr als eine Datei angegeben (%s und %s). Keine Änderungsprotokolldatei angegeben Das Ausgabeformat %s wird nicht unterstützt wiederholter Schlüsselwert %s Zu viele Argumente. unbekannter Schlüssel %s – wird nach XS-%s kopiert Zeile nicht erkannt Sie können nur entweder »from« oder »since« angeben Sie können nur entweder »to« oder »until« angeben Sie können »count« oder »offset« nicht mit einer anderen Bereichsoption kombinieren. 