��    8      �  O   �      �  +   �  %     /   +     [  /   t     �     �     �     �  2     2   ;     n     �     �     �     �     �  1   �     0  G   K     �     �     �     �     �  (   �  '        8     I     ]  5   |     �     �     �     �     	     .	     6	     J	  '   W	  !   	     �	  )   �	     �	     �	  "   
     .
  <   G
  :   �
  0   �
  *   �
  :        V  &   b  ]   �  �  �  +   s  %   �  <   �       2         S  2   p  *   �  .   �  J   �  E   H  <   �  5   �            *   #  &   N  >   u  '   �  W   �     4     D     Z     k     ~  5   �  +   �  $   �  $     3   =  6   q     �     �     �      �          7     ?     \  0   i  4   �     �  C   �     &     ?  (   V        J   �  @   �  9   ,  ;   f  J   �     �  '   �  m   !     -                       *         .   7       
                       &       (   )                         8   ,      2         /              	      +   '   0   $       #   1       !                        4           %                               6   3                "      5      stack[%d] =>   stack: %d   ind_level: %d
 %s: missing argument to parameter %s
 %s: option ``%s'' requires a numeric parameter
 %s: unknown option "%s"
 (Lines with comments)/(Lines with code): %6.3f
 CANNOT FIND '@' FILE! Can't close output file %s Can't open backup file %s Can't open input file %s Can't preserve modification time on backup file %s Can't preserve modification time on output file %s Can't stat input file %s Can't write to backup file %s EOF encountered in comment Error Error closing input file %s Error reading input file %s File %s contains NULL-characters: cannot proceed
 File %s is too big to read File named by environment variable %s does not exist or is not readable GNU indent %s
 Internal buffering error Line broken Line broken 2 ParseStack [%d]:
 Profile contains an unterminated comment Profile contains unpalatable characters Read profile %s
 Stmt nesting error. System problem reading file %s There were %d non-blank output lines and %d comments
 Unexpected end of file Unknown code to parser Unmatched 'else' Unterminated character constant Unterminated string constant Warning Zero-length file %s command line indent:  Strange version-control value
 indent:  Using numbered-existing
 indent: %s:%d: %s: indent: Can't make backup filename of %s
 indent: Fatal Error:  indent: System Error:  indent: Virtual memory exhausted.
 indent: can't create %s
 indent: can't have filenames when specifying standard input
 indent: only one input file when output file is specified
 indent: only one input file when stdout is used
 indent: only one output file (2nd was %s)
 old style assignment ambiguity in "=%c".  Assuming "= %c"
 option: %s
 set_option: internal error: p_type %d
 usage: indent file [-o outfile ] [ options ]
       indent file1 file2 ... fileN [ options ]
 Project-Id-Version: GNU indent 2.2.8
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-01-31 17:27+0100
PO-Revision-Date: 2012-03-21 08:05+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:47+0000
X-Generator: Launchpad (build 18115)
   stack[%d] =>   stack: %d   ind_level: %d
 %s: Argument für Parameter %s fehlt
 %s: die Option »%s« benötigt einen numerischen Parameter
 %s: unbekannte Option »%s«
 (Zeilen mit Kommentaren)/(Zeilen mit Code): %6.3f
 KANN DATEI '@' NICHT FINDEN! Die Ausgabedatei %s kann nicht geschlossen werden. Backup-Datei %s läßt sich nicht öffnen. Die Eingabedatei %s läßt sich nicht öffnen. Die Modifikationszeit kann für die Backup-Datei %s nicht erhalten werden. Die Modifikationszeit der Ausgabedatei %s kann nicht erhalten werden. Für die Eingabedatei %s kann stat nicht ausgeführt werden. In die Backup-Datei %s kann nicht geschrieben werden. EOF in Kommentar gefunden. Fehler Fehler beim Schließen der Eingabedatei %s Fehler beim Lesen der Eingabedatei %s. Die Datei %s enthält NULL-Zeichen: Fortfahren nicht möglich
 Die Datei %s ist zu groß zum Einlesen. Die in der Umgebungsvariable %s angegebene Datei existiert nicht oder ist nicht lesbar. GNU indent: %s
 Interner Pufferfehler Zeile umbrochen. Zeile umbrochen 2. Parse-Stack [%d]:
 Das Profile enthält einen nicht-beendeten Kommentar. Das Profile enthält ungenießbare Zeichen. Lese Vorgabedatei (»profile«) %s.
 Fehler in Anweisungsverschachtelung. Ein Systemproblem trat beim Lesen der Datei %s auf. Es gab %d nichtleere Ausgabezeilen und %d Kommentare.
 Unerwartetes Dateiende. Code ist dem Parser unbekannt. Unerwartetes »else«. Nicht-beendete Zeichenkonstante. Nicht-beendete Stringkonstante. Warnung Datei %s mit der Länge Null Befehlszeile indent: Seltsamer Wert für Versionsverwaltung.
 indent:  nummeriert-falls-vorhanden wird verwendet.
 indent: %s:%d: %s: indent: Name der Backup-Datei für %s kann nicht ermittelt werden.
 indent: Fataler Fehler:  indent: Systemfehler:  indent: Virtueller Speicher erschöpft.
 indent: Kann %s nicht erzeugen.
 indent: Dateinamen sind nicht erlaubt bei Verwendung der Standardeingabe.
 indent: Nur eine Eingabedatei, wenn Ausgabedatei angegeben ist.
 indent: Nur eine Eingabedatei bei Verwendung von stdout.
 indent: Nur eine Ausgabedatei erlaubt (die zweite war %s).
 Mehrdeutigkeit der Zuweisung in "=%c" (alte Form). "= %c" wird verwendet.
 option: %s
 set_option: Interner Fehler: p_type %d
 Verwendung: indent Datei [-o Ausgabedatei ] [ Optionen ]
       indent Datei1 Datei2 … DateiN [ Optionen ]
 