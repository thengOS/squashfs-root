��    T     �    \%      �1      �1  (   �1     2  0   .2     _2     q2     �2  +   �2  o   �2     @3     Q3     l3     �3     �3     �3     �3     �3  !   �3     4  A   ?4  #   �4  &   �4  "   �4     �4     5     )5  '   =5     e5     �5  1   �5     �5     �5     6     (6     B6  m   `6  <  �6  �   8  A   �8  2   �8     )9  A   89     z9  -   �9  =   �9  l   :  A   n:  K   �:  J   �:  C   G;  4   �;  %   �;     �;     �;     <      <      =<     ^<     k<     ~<     �<     �<  _   �<     *=     A=  `   ]=  8   �=     �=     >  �   />  �   ?  +   
@  8   6@  :   o@     �@  �   �@  o   nA  �   �A  �   �B  P   C  �   ]C    HD  K   TE  #   �E  �   �E  -   `F  8   �F  6   �F  �   �F  7   �G  Y   �G  �   %H  4   �H  '   "I  <   JI  m   �I  .   �I  *   $J  /   OJ  &   J  5   �J     �J  @   �J  ,   :K     gK     {K     �K  	   �K  6   �K     �K      �K  ,   L     LL  !   fL  &   �L  	   �L  -   �L  >   �L     &M  -   DM  S   rM  (   �M     �M  J   
N  :   UN  �  �N  A   DP  .   �P  .   �P     �P     �P     Q     Q     Q  
   4Q  )   ?Q  3   iQ  ]   �Q     �Q     R     �R     �R     �R     �R     �R     �R     �R     	S     S     (S     8S     DS  
   RS  
   ]S     hS     qS     zS  
   �S     �S     �S     �S     �S  )   �S  1   �S  `   T  #   hT  �   �T  �   yU  !   cV     �V     �V     �V  '   �V  f   �V  $   ^W  %   �W  %   �W  t   �W     DX     LX  +   iX     �X  '   �X     �X     �X     �X     	Y     Y     )Y  !   GY     iY     �Y     �Y     �Y  ,   �Y     Z     Z  �   �Z  !   �[     �[  D   �[  /   \     3\  +   A\  J   m\  J   �\     ]      ]  ,   ;]     h]     q]     �]     �]  +   �]     �]  	   �]     �]     ^     .^  P   G^  9   �^  .   �^  )   _  )   +_  (   U_  (   ~_  /   �_  '   �_  ,   �_  '   ,`  9   T`  !   �`  Q   �`  F   a     Ia     fa     za     �a     �a     �a  %   �a  &   �a  +   b     Ib     ]b     ob      �b     �b     �b     �b  .   �b  '   c  (   .c  8   Wc  )   �c  *   �c     �c     d     d  ,   3d  .   `d  (   �d     �d  &   �d  2   �d     1e     Ne  (   je  2   �e     �e  3   �e     f     +f  "   If     lf     �f  &   �f  6   �f     �f     g     +g  "   Cg     fg     lg  '   xg     �g     �g     �g  4   �g  ?   
h  A   Jh  #   �h  -   �h  &   �h  5   i     ;i  0   Ji  �   {i  �   j  7   �j  �   0k    �k    �l  �   n  �   �n  '   o     �o  �   �o  �  jp  �   r  !   �r  (   �r     �r  (   s     ?s     Qs     ps     �s     �s     �s     �s     �s     �s     �s     t     t  ;   =t  2   yt  9   �t  B   �t  !   )u  !   Ku     mu  :   �u  +   �u     �u     v  4   #v     Xv  ,   tv     �v  
   �v  &   �v  <   �v     "w  !   Bw  )   dw  "   �w  �   �w     5x  Q   Ex     �x  R   �x     y  	   y  .   (y  F   Wy  !   �y  8   �y     �y     z  :   %z     `z  !   sz     �z  $   �z  $   �z     �z  !   {     ;{  #   U{  �   y{  �   |     �|     �|  '   �|  !   �|  )   }  1   C}     u}  1   �}     �}  /   �}     ~  #   "~     F~     [~  0   z~  .   �~  2   �~  &     .   4  /   c     �     �     �     �  C   �     7�  &   Q�  Q   x�  d   ʀ  %   /�     U�  -   j�     ��  .   ��     ؁     ��  .   �  F   6�  5   }�     ��  ;   ǂ  '   �  P   +�  /   |�  <   ��  7   �  8   !�  3   Z�  
   ��     ��     ��     ��  #   Մ     ��     �     $�     ;�  #   U�  $   y�  &   ��     Ņ     �  0   ��  (   .�  -   W�  .   ��     ��  .   ҆     �  +   �     @�      [�     |�  "   ��     ��  '   ڇ     �      �     @�  8   U�  R   ��     �  5   �     �     0�     =�     M�     f�     ��     ��     ��  <   ω  %   �  (   2�  1   [�  :   ��  �   Ȋ  �   |�  @   0�  ?   q�  '   ��     ٌ  *   ��  E   �  #   a�     ��  .   ��  "   ύ  2   �  %   %�  3   K�  ,   �  ?   ��  �   �  �   t�     �  +   &�  .   R�  3   ��  T   ��  "   
�  N   -�  8   |�  @   ��  c   ��  0   Z�  i   ��  2   ��  b   (�  �   ��     S�     i�  '   u�  0   ��  1   Δ      �  1   �  #   M�  )   q�  
   ��     ��  S   ĕ     �  %   8�     ^�  .   k�     ��     ��      і  "   �  L   �     b�      t�     ��     ��  %   ��     �  3   �     :�  <   O�  4   ��  >   ��  3    �  #   4�  :   X�  2   ��     ƙ     ͙  I   ә  	   �  _   '�  1   ��  -   ��  7   �  .   �  @   N�  B   ��  0   қ  C   �     G�     c�     i�     ��     ��  
   ��     ��  f   ɜ  !   0�     R�     g�     n�     }�     ��     ��  J   ȝ     �  
   $�     /�     E�  :   N�     ��     ��  )   ��     ؞  ^   �     @�  �   H�     �  +   ��     *�  B   H�  %   ��     ��     Π  	   �  =   �     *�     ?�  0   Y�     ��  $   ��  "   ��  �  �      ��  *   ˣ     ��  0   �     <�     N�     l�  +   ��  y   ��     '�     8�     S�     h�     ��     ��     ��     ɥ  !   �     �  A   &�  #   h�  &   ��  "   ��     ֦     �     �  '   $�     L�     i�  0   ��     ��     է     �     �     )�  y   G�  R  ��  �   �  B   ª  4   �     :�  F   J�  "   ��  7   ��  I   �  o   6�  <   ��  N   �  M   2�  C   ��  4   ĭ  %   ��     �     /�     @�     U�      p�     ��     ��     ��     Ǯ     �  e   ��     c�     z�  U   ��  8   �     %�     =�  �   ]�  �   =�  +   8�  H   d�  F   ��     ��  �   �  �   �  �   k�  �   -�  t   ۵  
  P�  J  [�  J   ��  #   �  �   �  1   �  <   �  8   W�  �   ��  7   %�  Y   ]�  �   ��  ?   ��  +   �  C   �  ~   c�  1   �  /   �  <   D�  &   ��  9   ��     �  ?   �  +   A�     m�     ��     ��  	   ��  H   ��     �  B   !�  4   d�  &   ��  %   ��  *   ��  	   �  /   �  P   K�     ��  /   ��  x   ��  /   c�     ��  \   ��  @   �    M�  P   [�  1   ��  >   ��     �  
   :�     E�     R�     o�     ��  -   ��  C   ��  r   �     v�  �   ��     �     *�     A�     S�     f�     u�     ��     ��     ��     ��     ��     ��  
   ��  
   ��     �     �     �  
   �     '�     /�     7�     @�  0   F�  7   w�  t   ��  2   $�  %  W�  
  }�  (   ��     ��     ��      ��  +   �  {   /�  0   ��  1   ��  6   �  �   E�     ��  !   ��  B   ��     @�  '   P�     x�     ��     ��     ��     ��     ��  !   ��     �     4�     T�  %   d�  <   ��     ��  �   ��  b  ��  1   ��     �  B   2�  D   u�     ��  ;   ��  Q   	�  N   [�     ��     ��  1   ��     �     �     1�     K�  F   e�     ��     ��  )   ��     ��     �  S   ,�  B   ��  9   ��  ,   ��  .   *�  )   Y�  3   ��  6   ��  E   ��  ?   4�  !   t�  I   ��  .   ��  h   �  C   x�  %   ��     ��     ��     �     &�  "   ?�  +   b�  7   ��  B   ��     	�     �     -�      =�     ^�     ~�  %   ��  6   ��  /   ��  1   �  ?   I�  *   ��  -   ��      ��     �     #�  /   C�  <   s�  3   ��  "   ��  >   �  3   F�     z�  !   ��  0   ��  6   ��  .   $�  K   S�     ��  #   ��  )   ��      �      �  /   4�  @   d�     ��  "   ��     ��  ,   ��     &�     .�  9   >�     x�     ��     ��  <   ��  P   ��  F   @�  %   ��  3   ��  .   ��  B   �     S�  4   i�  �   ��    M�  F   \�  �   ��  S  _�  c  ��  �   �  �   �  <   ��     �  �   &�  �  ��  �   ��  4   ��  +   ��     ��  /   �     ?�     W�     u�     ��     ��     ��     ��     ��     ��     �     �     -�  B   M�  9   ��  <   ��  V   �  &   ^�  $   ��     ��  :   ��  9   ��  .   3�  '   b�  :   ��  %   ��  6   ��     "�     /�  -   ?�  B   m�  &   ��  &   ��  @   ��  $   ?�  �   d�     ��  u   �     |�  O   ��  '   ��     �  +   '�  Y   S�  *   ��  S   ��     ,�  (   E�  P   n�     ��  <   ��  %   �  :   >�  3   y�  (   ��  !   ��      ��  #   �  �   =�  �   �     ��     ��  '   ��  "   �  1   ?�  6   q�  "   ��  8   ��      �  6   %�     \�  *   s�  %   ��  :   ��  7   ��  7   7�  @   o�  )   ��  =   ��  -   �     F�      c�     ��     ��  P   ��     �  /   -�  L   ]�  �   ��  +   2     ^  '   v     �  6   �  %   �      1   ' j   Y 8   �    � >    9   X U   � 0   � D    =   ^ B   � ;   �        '    B    P +   h    �    �    �    � 6    )   8 ,   b "   �    � =   � /    4   ? 9   t "   � :   �     /     !   P *   r )   � 4   �     � -    #   K    o    � A   � b   � 
   L	 B   W	    �	    �	    �	    �	 $   �	    
 !   )
    K
 G   c
 +   �
 3   �
 4    =   @ �   ~ �   S I   1 ?   { 1   �    � -    Z   6 %   � $   � 4   � *    >   < :   { 8   � 6   � I   & �   p �   � +   � 0   � /    <   K P   � )   � Y    5   ] 8   � e   � ;   2 �   n A   � ~   9 �   �    �    � +   � =   � >   8 &   w D   � '   � 1    
   = &   H d   o -   � ,       / =   D !   � %   � &   � 3   � h   %    � =   � :   �     /   , '   \ 9   �    � 6   � 3    C   H 7   � (   � K   � =   9    w    ~ Z   �    � p   � 1   h -   � 7   � 6     K   7 P   � 3   � \     /   e     �  "   �     �     �     �     �  g   ! 0   s! #   �!    �!    �! %   �!    	"     " X   ="    �"    �"    �" 	   �" 
   �" "   �"    �" 1   # 	   4# m   ># 	   �# �   �#    {$ 1   �$ ,   �$ S   �$ %   I% (   o%    �%    �% W   �%    & $   4& 8   Y&    �& )   �& +   �&    �       �  �   �   �   �  �      �  �              �  B  "  N   �                  �  �   �           !      y                  w           �       <  �  �         �       �      "   �  �          ;  �      1  m  �  �  T    �           #      �   �  �   �  0     D  �   w  E      /              4      �  �   y  2     	  *      �         �  e  �  {   M   �            �  �  e   �  -  f   9  �   A  J  �       5  ?  [  #   �   �           �  b  �  �  9          s   �            R       �  �  x          &  H   �   �   �   ^  �       �   ,  �  S  X  �  �  `              �   Q   I       �   8  @  �   %  �   �   +  �   �      6   �  �   �   �   �       �       �                   R      
  �      
  �          �  �  /      �     7  �  P      �  �           �   �  (  �   x  �  �  �          ,   1   3  '  �   �          �  �     �  Z   |    5   �       �   #  Q      �  �  �       �          2  �  �      �     �        �   �   R         �   �  "      �       }     �  �      K  �       �   �  �   H      p  �      V           �  �  �  C       �      .   �              �       �    -   �  �  �  �       >          K         S  �          ;   [         �      �   q          �  �   �   ;  {      �   k     �      h     �   ^   <     �          >                     6     :         �  �    �      �    �   �  g      �  /   B   J   l       +   s  �      �   �  �  �     �  h        �   �   (   V     `            !   �  �      X   a  E  �      l  C  �     �  p   W   �           �   �  N  k   �  u      �  �     �     �  �  }    6    j     K   '   ,  M  L  '      !  t      �   .                i   q               F   :      Y  �  T      �   �      I  �     :  F      O  �  -  U       $   c   |   
   A   �       S   H                )      @           Z      �                 7  �   .           t               �             0  �   �      �           \  Y   W  �   G  v       $  �   �   d  P   �  �  �   &   �  �  �   �   z  �       �      �   �  �  �  E   ~   =    7       I  a   �      �  �       d     _   	       =     Q      )      O   �   L  �       ]  r       n      �   0  D  C  �  8         �   f  G  g       �   \   T         �  �  D     b   G       �       +      *  �  �   3   �         z   �   �  u  &      �        �      �  �   �          �            �       �   ]     (  F  �  4  �  )   =   *   �   i  �   �     U      j   �   5      @  A  B    %   �  �   �       �  �                   �           1      �  �              �  �   �   �   9   �   $      �   �   �        _                    >  c  4  �   �   �   �  �         �  ~      �   3  n              �        M  ?  %  J                   2           o   N  m       �  o      �   	    �     O  r  �   L       8  �   �   <  ?                 �   �       �          v  P  �        
BZR_PLUGIN_PATH
~~~~~~~~~~~~~~~ 
Configuration files
------------------- 
Examples
-------- 
http_proxy, https_proxy
~~~~~~~~~~~~~~~~~~~~~~~         bzr alias         bzr alias --remove ll         bzr alias ll         bzr alias ll="log --line -r-10..-1"         bzr checkout --lightweight repo/trunk trunk-checkout
        cd trunk-checkout
        (add files here)         bzr diff         bzr diff --old xxx         bzr diff -c2         bzr diff -r-2..         bzr diff -r1         bzr diff -r1..3         bzr diff -r1..3 xxx         bzr ignore "RE:^#"         bzr ignore "RE:lib/.*\.o"         bzr ignore "lib/**/*.o"         bzr init-repo --no-trees repo
        bzr init repo/trunk         bzr log -r date:yesterday..         bzr merge -r 81..82 ../bzr.dev         bzr merge -r 82 ../bzr.dev         bzr merge ../bzr.dev         bzr merge /tmp/merge         bzr missing         bzr missing --my-revision ..-10         bzr missing -r ..-10         bzr remerge --show-base         bzr whoami "Frank Chu <fchu@example.com>"         bzr whoami --email       $ bzr diff -r submit:       branch:/path/to/branch       bzr launchpad-login       bzr launchpad-login bob       last:1        -> return the last revision
      last:3        -> return the revision 2 before the end.
     -l N        display a maximum of N revisions
    -n N        display N levels of revisions (0 for all, 1 for collapsed)
    -v          display a status summary (delta) for each revision
    -p          display a diff (patch) for each revision
    --show-ids  display revision-ids (and file-ids), not just revnos     -rX      display revision X
    -rX..    display revision X and later
    -r..Y    display up to and including revision Y
    -rX..Y   display from X to Y inclusive     Difference between revision 3 and revision 1 for branch xxx::     Difference between revision 3 and revision 1::     Examples::     One way to display all the changes since yesterday would be::     Set the current user::     Show just the differences for file NEWS::     Show the differences between two branches for file NEWS::     The changes between the current revision and the previous revision
    (equivalent to -c-1 and -r-2..-1)     The changes introduced by revision 2 (equivalent to -r1..2)::     To see the changes introduced by revision X::
    
        bzr diff -cX     [http://mybranches.isp.com/~jdoe/branchdir]
    [/home/jdoe/branches/]     bugtracker_hudson_url = http://issues.hudson-ci.org/browse/{id}     bugzilla_squid_url = http://bugs.squid-cache.org     bzr commit --fixes <tracker>:<id>     bzr info -v     bzr info -vv     bzr ls --ignored     bzr mv SOURCE... DESTINATION     bzr remove-branch repo/trunk     bzr view     bzr view --all     bzr view --delete     bzr view --delete --all     bzr view --switch off     cd ~/project
    bzr init
    bzr add .
    bzr status
    bzr commit -m "imported project"     debug_flags = hpss     debug_flags = hpss,evil     email            = John Doe <jdoe@isp.com>
    gpg_signing_key  = Amy Pond <amy@example.com>     trac_twisted_url = http://www.twistedmatrix.com/trac    ``bzr resolve FILE``   "John Doe <jdoe@example.com>"   $ bzr init
  $ bzr mkdir foo
  $ bzr commit -m "BASE"
  $ bzr branch . ../other
  $ rmdir foo
  $ touch foo
  $ bzr commit -m "THIS"
  $ bzr mkdir ../other/foo/bar
  $ bzr commit ../other -m "OTHER"
  $ bzr merge ../other   $ bzr init
  $ bzr mkdir white
  $ bzr mkdir black
  $ bzr commit -m "BASE"
  $ bzr branch . ../other
  $ bzr mv white black
  $ bzr commit -m "THIS"
  $ bzr mv ../other/black ../other/white
  $ bzr commit ../other -m "OTHER"
  $ bzr merge ../other   BZR_DISABLE_PLUGINS='myplugin:yourplugin'   BZR_PLUGIN_PATH='/path/to/my/site/plugins:-site':+user   Display information on the format and related locations:   In this case:   Note that the default number of levels to display is a function of the
  log format. If the -n option is not used, the standard log formats show
  just the top level (mainline).   See ``bzr help revisionspec`` for details on how to specify X and Y.
  Some common examples are given below::   Status summaries are shown using status flags like A, M, etc. To see
  the changes explained using words like ``added`` and ``modified``
  instead, use the -vv option.   The -r option can be used to specify what revision or range of revisions
  to filter against. The various forms are shown below::   The following options can be used to control what information is
  displayed::   The log format controls how information about each revision is
  displayed. The standard log formats are called ``long``, ``short``
  and ``line``. The default is long. See ``bzr help log-formats``
  for more details on log formats.   To display revisions from oldest to newest, use the --forward option.
  In most cases, using this option will have little impact on the total
  time taken to produce a log, though --forward does not incrementally
  display revisions like --reverse does when it can.   [myprojects]
  scheme=ftp
  host=host.com
  user=joe
  password=secret
     ``bzr resolve FILE --action=done'   bzr add            make files or directories versioned
  bzr ignore         ignore a file or pattern
  bzr mv             move or rename a versioned file   bzr branch ftp://host.com/path/to/my/branch   bzr branch ftp://joe:secret@host.com/path/to/my/branch   bzr+ssh://remote@shell.example.com/~/myproject/trunk   cd /tmp
  bzr log /tmp/%2False
  bzr log %2False
  bzr log file:///tmp/%252False
  bzr log file://localhost/tmp/%252False
  bzr log file:%252False   http://bzruser:BadPass@bzr.example.com:8080/bzr/trunk   http_proxy=http://proxy.example.com:3128/ 
  https_proxy=http://proxy.example.com:3128/   init    Change a directory into a versioned branch.
  branch  Create a new branch that is a copy of an existing branch.
  merge   Perform a three-way merge.
  bind    Bind a branch to another one.
   init    Make a directory into a versioned branch.
 "%(path)s" is not a directory %(extra)s "%s" is not a valid <plugin_name>@<plugin_path> description  "bzr diff -p1" is equivalent to "bzr diff --prefix old/:new/", and
produces patches suitable for "patch -p1". %(context_info)s%(path)s is already versioned. %(context_info)s%(path)s is not versioned. %(exe_name)s could not be found on this machine %(msg)s %(host)s%(port)s%(orig_error)s %(source)s is%(permanently)s redirected to %(target)s %(url)s is not a local path. %(username)r does not seem to contain a reasonable email address %(value)s is not an index of type %(_type)s. %6d ghost revisions %6d revisions %6d revisions not present %6d texts %d conflict auto-resolved. %d conflicts auto-resolved. %d conflicts encountered. %d tag updated. %d tags updated. %r is too short to calculate a relative path %s is already up-to-date. %s is not registered on Launchpad '%(display_url)s' is already a branch. (default) * 0 = Standard behavior
* 1 = Launch debugger * An optional patch that is a preview of the changes requested * ``password``: the password. * ``port``: the port the server is listening, , the header appears corrupt, try passing -r -1 to set the state to the last commit --after cannot be specified with --auto. --dry-run requires --auto. --dry-run will show which files would be added, but not actually
add them. --exclude-common-ancestry requires two different revisions --file-ids-from will try to use the file ids from the supplied path.
It looks up ids trying to find a matching parent directory with the
same filename, and then by pure path. This option is rarely needed
but can be useful when adding the same logical file into two
branches that will be merged later (without showing the two different
adds as a conflict). It is also useful when merging another project
into a subdirectory of this one. --prefix expects two values separated by a colon (eg "old/:new/") --product is deprecated; please use --project. --tree and --revision can not be used together 1. user and password :Aliases:   :Checks: :Custom authors: :Description:
  %s

 :Examples: :Examples:
    Show the current aliases:: :Examples:
    Show the email of the current user:: :Exit values:
    1 - changed
    2 - unrepresentable changes
    3 - error
    0 - no change :From:     plugin "%s"
 :On Unix:   ~/.bazaar/bazaar.conf
:On Windows: C:\\Documents and Settings\\username\\Application Data\\bazaar\\2.0\\bazaar.conf :Options:%s :Ordering control: :Other filtering: :Output control: :Path filtering: :Purpose: %s
 :Revision filtering: :See also: %s :Things to note: :Tips & tricks: :Usage:
%s
 :Usage:   %s
 :ancestor: :annotate: :before: :branch: :date: :mainline: :revid: :revno: :submit: :tag: A target configuration must be specified. A typical config file might look something like:: A warning will be printed when nested trees are encountered,
unless they are explicitly ignored. Add specified files or directories. Adding a file whose parent directory is not versioned will
implicitly add the parent, and so on up to the root. This means
you should never need to explicitly add a directory, they'll just
get added when you add a file in the directory. Additionally for directories, symlinks and files with a changed
executable bit, Bazaar indicates their type using a trailing
character: '/', '@' or '*' respectively. These decorations can be
disabled using the '--no-classify' option. All changes applied successfully. All conflicts resolved. All hidden commands Already a branch: "%(path)s". Alternatively, to list just the files:: Any files matching patterns in the ignore list will not be added
unless they are explicitly mentioned. Apply changes but don't delete them. Are you sure you wish to delete these Ask Launchpad to mirror a branch now. At the same time it is run it may recompress data resulting in
a potential saving in disk space or performance gain. Authors Automatically guess renames. Avoid making changes when guessing renames. BZRPATH
~~~~~~~ BZR_DISABLE_PLUGINS
~~~~~~~~~~~~~~~~~~~ BZR_EDITOR
~~~~~~~~~~ BZR_EMAIL
~~~~~~~~~ BZR_HOME
~~~~~~~~ BZR_LOG
~~~~~~~ BZR_PDB
~~~~~~~ BZR_PLUGINS_AT
~~~~~~~~~~~~~~ BZR_PROGRESS_BAR
~~~~~~~~~~~~~~~~ BZR_REMOTE_PATH
~~~~~~~~~~~~~~~ BZR_SIGQUIT_PDB
~~~~~~~~~~~~~~~ BZR_SSH
~~~~~~~ Backup changed files (default). Bad value "%(value)s" for option "%(name)s". Basic commands Basic commands:
  bzr init           makes this directory a versioned branch
  bzr branch         make a copy of another branch Before merges are committed, the pending merge tip revisions are
shown. To see all pending merge revisions, use the -v option.
To skip the display of pending merge information altogether, use
the no-pending option or specify a file/directory. Binary files section encountered. Body for the email. Branch %(base)s is missing revision %(text_revision)s of %(file_id)s Branch author's email address, if not yourself. Branch format Branch is active. Use --force to remove it. Branch to pull into, rather than the one containing the working directory. Branch to push from, rather than the one containing the working directory. Branch/tree to compare from. Branch/tree to compare to. Branched %d revision. Branched %d revisions. Branches Branches are up to date.
 Bug Tracker Settings Bug tracker settings Can't use both --stacked-on and --unstacked Canceled Canceled
 Cannot pull individual files Certificate error: %(error)s Checking branch at '%s'. Comma also has special meaning in URLs, because it denotes `segment parameters`_ Commonly used like this:
    eval "`bzr bash-completion`" Config file %(filename)s is not UTF-8 encoded
 Connection Timeout: %(msg)s%(orig_error)s Connection closed: %(msg)s %(orig_error)s Connection error: %(msg)s %(orig_error)s Copying repository content as tarball... Corrupt or incompatible data stream: %(reason)s Could not determine branch to refer to. Could not parse options for index %(value)s. Create a branch without a working-tree. Create a new branch that is a copy of an existing branch. Create a new versioned directory. Create a stacked branch that references the public location of the parent branch. Create the path leading up to the branch if it does not already exist. Created a {0} (format: {1})
 Created new branch. Criss-Cross Current Storage Formats Current storage formats Delete all ignored files. Delete changes without applying them. Delete files unknown to bzr (default). Delete from bzr but leave the working copy. Deleting backup.bzr Deletion Strategy Deprecated in: %s Description of the new revision. Detailed log format. Diff format Diff format to use. Diff is not installed on this machine: %(msg)s Diff3 is not installed on this machine. Directory not empty: "%(path)s"%(extra)s Display all the defined values for the matching options. Display changes in the local branch only. Display changes in the remote branch only. Display email address only. Display more information. Display status summary. Display timezone as local, original, or utc. Display, set or remove a configuration option. Do not mark object type using indicator. Do not prompt before deleting. Do not save backups of reverted files. Do not use a shared repository, even if available. Don't actually make changes. Don't backup changed files. Don't check that the user name is valid. Don't recursively add the contents of directories. Don't show pending merges. Dry-run, pretending to remove the above revisions.
 Environment Variables Error in command line options Error in data for index %(value)s. Error reading from %(path)r. Example:
~~~~~~~~ Export format %(format)r not supported Failed to rename %(from_path)s to %(to_path)s: %(why)s Fetching revisions File exists: %(path)r%(extra)s File format
----------- File is binary but should be text. Files Files
----- Files cannot be moved between branches. Finding branch files Finding revisions For example:: For more information see http://help.launchpad.net/
 Format %(format)s cannot be initialised by this version of bzr. Format %(format)s is not compatible with .bzr version %(bzrdir)s. Format used by GNU ChangeLog files. Generic bzr smart protocol error: %(details)s Generic path error: %(path)r%(extra)s) Ghost revision {%(revision_id)s} cannot be used here. Global Options HTTP MIME Boundary missing for %(path)s: %(msg)s If branches have diverged, you can use 'bzr push --overwrite' to replace
the other branch completely, discarding its unmerged changes. If no arguments are specified, the status of the entire working
directory is shown.  Otherwise, only the status of the specified
files or directories is reported.  If a directory is given, status
is reported for everything inside that directory. If no revision is nominated, the last revision is used. If the last argument is a versioned directory, all the other names
are moved into it.  Otherwise, there must be exactly two arguments
and the file is changed to a new name. If there is no default location set, the first pull will set it (use
--no-remember to avoid setting it). After that, you can omit the
location to use the default.  To change the default, use --remember. The
value will only be saved if the remote location can be accessed. If there is no default push location set, the first push will set it (use
--no-remember to avoid setting it).  After that, you can omit the
location to use the default.  To change the default, use --remember. The
value will only be saved if the remote location can be accessed. If you want to ensure you have the different changes in the other branch,
do a merge (see bzr help merge) from the other branch, and commit that.
After that you will be able to do a push without '--overwrite'. If you want to replace your local changes and just want your branch to
match the remote one, use pull --overwrite. This will work even if the two
branches have diverged. Ignoring files outside view. View is %s Importing revisions In non-recursive mode, all the named items are added, regardless
of whether they were previously ignored.  A warning is given if
any of the named files are already versioned. In recursive mode (the default), files are treated the same way
but the behaviour for directories is different.  Directories that
are already versioned do not give a warning.  All directories,
whether already versioned or not, are searched for files or
subdirectories that are neither versioned or ignored, and these
are added.  This search proceeds recursively into versioned
directories.  If no names are given '.' is assumed. In recursive mode, files larger than the configuration option 
add.maximum_file_size will be skipped. Named items are never skipped due
to file size. Include all possible information. Include the last revision for each file. Include the revision-history. Information on choosing a storage format Install revisions Integration with Launchpad.net Introduced in: %s Introduced in: 0.91 Introduced in: 1.13 Introduced in: 1.14 Introduced in: 1.15 Introduced in: 1.17 Introduced in: 1.6 Introduced in: 1.9 Introduced in: 2.4 Invalid branch name: %(name)s Invalid http Content-type "%(ctype)s" for %(path)s: %(msg)s Invalid http range %(range)r for %(path)s: %(msg)s Invalid http response for %(path)s: %(msg)s%(orig_error)s Invalid ignore pattern found. %s Invalid ignore patterns found. %s Invalid pattern(s) found. %(msg)s Invalid revision number %(revno)s Launchpad
--------- Launchpad project short name to associate with the branch. Launchpad user ID exists and has SSH keys.
 Launchpad user ID set to '%s'.
 List files with conflicts. List the branches available at the current location. List the installed plugins. Listen for connections on nominated address. Location
~~~~~~~~ Log format Log format with one line per revision. Longer description of the purpose or contents of the branch. Lookup file ids from this tree. Mail the request to this address. Make a directory into a versioned branch. Malformed line.  %(desc)s
%(line)r Many commands that accept URLs also accept location aliases too.
See :doc:`location-alias-help` and :doc:`url-special-chars-help`.
 Message string. Missing feature %(feature)s not provided by this version of Bazaar or any plugin. Moderately short log format. Move only the bzr identifier of the file, because the file has already been moved. Move or rename a file. Moving %s Name of the generated function (default: _bzr) Never change revnos or the existing log.  Append revisions to it only. No Launchpad user ID configured.
 No error if existing, make parent directories as needed. No files deleted. No help for this command. No known version info format {0}. Supported types are: {1} No matching files. No new revisions or tags to push. No new revisions to push. No pull location known or specified. No push location known or specified. No such file: %(path)r%(extra)s No target configuration specified No working tree to remove Not a branch: "%(path)s"%(detail)s. Note that --short or -S gives status flags for each item, similar
to Subversion's status command. To get output similar to svn -q,
use bzr status -SV. Note: The location can be specified either in the form of a branch,
or in the form of a path to a file containing a merge directive generated
with bzr send. Nothing to delete. Nothing to do. One-sentence description of the branch. Only display errors and warnings. Only one path may be specified to --auto. Only remove files that have never been committed. Only show versioned files. Open a Launchpad branch page in your web browser. Opening %s in web browser Option --change does not accept revision ranges Other Storage Formats Other branch has no new revisions.
 Overwrite tags only. Parent of "%s" does not exist. Pass these options to the external diff program. Path(s) are not versioned: %(paths_as_string)s Path(s) do not exist: %(paths_as_string)s%(extra)s Permission denied: "%(path)s"%(extra)s Please supply either one revision, or a range. Prepare the request but don't actually send it. Print ignored files. Print just the version number. Print unknown files. Print versioned files. Public branch "%(public_location)s" lacks revision "%(revstring)s". Pushed up to revision %d. Recipe for importing a tree of files:: Recursively scan for branches rather than just looking in the specified location. Refuse to push if there are uncommitted changes in the working tree, --no-strict disables the check. Register a branch with launchpad.net. Remaining conflicts: Remember the specified location as a default. Remove a branch. Remove branch even if it is the active branch. Remove files or directories. Remove the alias. Remove the option from the configuration file. Remove the working tree even if it has uncommitted or shelved changes. Remove the working tree from a given branch/checkout. Removing backup ... Repository %r does not support access to raw revision texts Reprocess to reduce spurious conflicts. Requested revision: '%(spec)s' does not exist in branch: %(branch_url)s%(extra)s Revision is not compatible with %(repo_format)s Revision {%(revision_id)s} already present in "%(file_id)s". Revision {%(revision_id)s} already present in %(weave)s Revision {%(revision_id)s} not present in "%(file_id)s". Revision {%(revision_id)s} not present in %(weave)s Revisions
 Rule Patterns
------------- Rules
===== Run the bzr server. SMTP connection to %(host)s refused SMTP error: %(error)s Same as --mine-only. Same as --theirs-only. Say yes to all questions. See "bzr help break-lock" for more. See "help revisionspec" for details. Select a different SSH implementation. Select changes interactively. Select the output format. Server sent an unexpected error: %(error_tuple)r Set the branch of a checkout and update. Setting ssh/sftp usernames for launchpad.net. Show changes, but do not apply or remove them. Show current revision number. Show files to delete instead of deleting them. Show help message. Show help on a command or other topic.
     Show help on all commands. Show list of renamed files.
     Show logs of pulled revisions. Show or set the Launchpad user ID. Show revno of working tree. Show the origin of each line in a file. Show the tree root directory. Show usage message and options. Show version of bzr. Show what would be done, but don't actually do anything. Some smart servers or protocols *may* put the working tree in place in
the future. Sorting Specify a format for this branch. See "help formats". Standard Options Status Flags Storage Formats Supported URL prefixes:: Supported generic values are: Supported modifiers:: Supported transport protocols Switched to branch: %s Tags can only be placed on a single revision, not on a range Target directory "%s" already exists. Text did not match its checksum: %(msg)s The "%(config_id)s" configuration does not exist. The "%(option_name)s" configuration option does not exist. The --verbose option will display the revisions pulled using the log_format
configuration option. You can use a different format by overriding it with
-Olog_format=<other_format>. The --verbose option will display the revisions pushed using the log_format
configuration option. You can use a different format by overriding it with
-Olog_format=<other_format>. The 2a format with experimental support for colocated branches.
 The Bazaar smart server protocol over TCP. (default port: 4155) The above revision(s) will be removed.
 The action to perform. The alias "%(alias_name)s" does not exist. The branch *MUST* be on a listable system such as local disk or sftp. The branch {0} has no revision {1}. The bug this branch fixes. The content being inserted is already present. The file deletion mode to be used. The key '%(key)s' is already in index '%(index)s'. The key '%(key)s' is not a valid key. The path %(path)s is not permitted on this platform The repository {0} contains no revision {1}. The synonyms 'clone' and 'get' for this command are deprecated. The target branch will not have its working tree populated because this
is both expensive, and is not supported on remote file systems. The tree does not appear to be corrupt. You probably want "bzr revert" instead. Use "--force" if you are sure you want to reset the working tree. The user aborted the operation. The value '%(value)s' is not a valid value. There are two possible values for this option: There is no public branch set for "%(branch_url)s". Therefore simply saying 'bzr add' will version all files that
are currently unknown. This branch has no new revisions.
 This command will print the names of all the branches at the current
location. This is equal to the number of revisions on this branch. This is equivalent to creating the directory and then adding it. This reports on versioned and unknown files, reporting them
grouped by state.  Possible states are: To check what clean-tree will do, use --dry-run. To compare the working directory to a specific revision, pass a
single revision to the revision argument. To re-create the working tree, use "bzr checkout". To see ignored files use 'bzr ignored'.  For details on the
changes to file texts, use 'bzr diff'. To see which files have changed in a specific revision, or between
two revisions, pass a revision range to the revision argument.
This will produce the same results as calling 'bzr diff --summarize'. Too many redirections Topics list Transport error: %(msg)s %(orig_error)s Tree is up to date at revision {0} of branch {1} Turn this branch into a mirror of another branch. Type of file to export to. Unable to import library "%(library)s": %(error)s Unknown %(kind)s format: %(format)r Unknown rules detected: %(unknowns_str)s. Unstacking Unsupported export format: %s Unsupported timezone format "%(timezone)s", options are "utc", "original", "local". Update a mirror of this branch. Updated to revision {0} of branch {1} Upgrading %s Use bzr resolve when you have fixed a problem. Use short status indicators. Use specified log format. Use the specified output format. Use this command to compare files. Use this to create an empty branch, or before importing an
existing project. Useful commands:: Using saved parent location: %s
 Using saved push location: %s Valid values: Variable {%(name)s} is not available. Version info in Python format. Version info in RIO (simple text) format (default). Versioned file error You are missing %d revision:
 You are missing %d revisions:
 You can only supply one of revision_id or --revision You cannot remove the working tree from a lightweight checkout You cannot remove the working tree of a remote path You cannot specify a NULL revision. You have %d extra revision:
 You have %d extra revisions:
 You must supply either --revision or a revision_id action added added
    Versioned in the working copy but not in the previous revision. added %s
 branch has no revision %s
bzr update --revision only works for a revision in the branch history bugtracker_<tracker>_url
------------------------ bugzilla_<tracker>_url
---------------------- bzr %s --revision takes exactly one revision identifier bzr alias --remove expects an alias to remove. bzr diff --revision takes exactly one or two revision specifiers bzr status --revision takes exactly one or two revision specifiers bzr update --revision takes exactly one revision bzr update can only update a whole tree, not a file or subdirectory can not move root of branch check checked branch {0} format {1} checking revisions converting revision deleted %s deleting paths: example:
    bzr register-branch http://foo.com/bzr/fooproject.mine \
            --project fooproject failed to reset the tree state{0} fixes bug fixes bugs format found error:%s ignored {0} matching "{1}"
 invalid direction %r invalid kind %r specified kind changed
    File kind has been changed (e.g. from file to directory). loading revision missing %s missing file argument modified modified
    Text has changed since the previous revision. not a valid revision-number: %r or:: please specify either --message or --file protocol removed
    Versioned in the previous revision but removed or deleted
    in the working copy. renamed renamed
    Path of this file changed from the previous revision;
    the text may also have changed.  This includes files whose
    parent directory was renamed. renamed {0} to {1} skipping {0} (larger than {1} of {2} bytes) sorry, %r not allowed in path to mv multiple files the destination must be a versioned directory trac_<tracker>_url
------------------ unable to remove "{0}": {1}. unable to remove %s unchanged unknown
    Not versioned and not matching an ignore pattern. unknown command "%s" unknown log formatter: %r would refer to ``/home/remote/myproject/trunk``. write revision {0!r} is not present in revision {1} {0} and {1} are mutually exclusive Project-Id-Version: bzr
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-07-27 12:45+0200
PO-Revision-Date: 2015-01-07 20:32+0000
Last-Translator: Tobias Bannert <tobannert@gmail.com>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:42+0000
X-Generator: Launchpad (build 18115)
 
BZR_PLUGIN_PATH
~~~~~~~~~~~~~~~ 
Konfigurationsdateien
------------------- 
Beispiele
--------- 
http_proxy, https_proxy
~~~~~~~~~~~~~~~~~~~~~~~         bzr alias         bzr alias --remove ll         bzr alias ll         bzr alias ll="log --line -r-10..-1"         bzr checkout --lightweight repo/trunk trunk-checkout
        cd trunk-checkout
        (Hier Dateien hinzufügen)         bzr diff         bzr diff --old xxx         bzr diff -c2         bzr diff -r-2..         bzr diff -r1         bzr diff -r1..3         bzr diff -r1..3 xxx         bzr ignore "RE:^#"         bzr ignore "RE:lib/.*\.o"         bzr ignore "lib/**/*.o"         bzr init-repo --no-trees repo
        bzr init repo/trunk         bzr log -r date:yesterday..         bzr merge -r 81..82 ../bzr.dev         bzr merge -r 82 ../bzr.dev         bzr merge ../bzr.dev         bzr merge /tmp/merge         bzr missing         bzr missing --my-revision ..-10         bzr missing -r ..-10         bzr remerge --show-base         bzr whoami "Frank Chu <ich@beispiel.de>"         bzr whoami --email       $ bzr diff -r submit:       branch:/pfad/zur/branch       bzr launchpad-login       bzr launchpad-login bob       last:1        -> Gibt die letzte Version zurück
      last:3        -> Gibt die Version zwei vor dem Ende zurück
     -l N        Maximal N Revisionen anzeigen
    -n N         N Revisionsstufen anzeigen (0 für alle, 1 für eingeklappte)
    -v          Eine Status-Zusammenfassung (Delta) für jede Revision anzeigen
    -p          Einen diff (Patch) für jede Revision anzeigen
    --show-ids  Revision-ids (und file-ids) anzeigen, nicht nur revnos     -rX      Revision X anzeigen
    -rX..    Revision X und spätere anzeigen
    -r..Y    Bis Revision Y (inklusive) anzeigen
    -rX..Y   Von X bis Y (inklusive) anzeigen     Unterschied zwischen Revision 3 und Revision 1 von Zweig xxx::     Unterschied zwischen Revision 3 und Revision 1::     Beispiele::     Eine Möglichkeit, alle Änderungen seit gestern anzuzeigen wäre:     Aktuellen Benutzer festlegen::     Nur die Unterschiede für die Datei NEWS anzeigen::     Die Unterschiede zwischen zwei Zweigen für die Datei NEWS anzeigen::     Die Änderungen zwischen der gegenwärtigen Revision und der vorigen
    (äquivalent zu -c-1 und -r-2..-1)     Die Änderungen von Revision 2 (äquivalent zu -r1..2)::     So erhalten Sie die Änderungen von Revision X::
    
        bzr diff -cX     [http://meinebranchen.isp.com/~jdoe/branchdir]
    [/home/jdoe/branches/]     bugtracker_hudson_url = http://issues.hudson-ci.org/browse/{id}     bugzilla_squid_url = http://bugs.squid-cache.org     bzr commit --fixes <tracker>:<id>     bzr info -v     bzr info -vv     bzr ls --ignored     bzr mv QUELLE … ZIEL     bzr remove-branch repo/trunk     bzr view     bzr view --all     bzr view --delete     bzr view --delete --all     bzr view --switch off     cd ~/Projekt
    bzr init
    bzr add .
    bzr status
    bzr commit -m »Importiertes Projekt«     debug_flags = hpss     debug_flags = hpss,evil     email = John Doe <jdoe@isp.com>
    gpg_signing_key  = Amy Pond <amy@beispiel.de>     trac_twisted_url = http://www.twistedmatrix.com/trac    ``bzr resolve FILE``   "John Doe <jdoe@beispiel.de>"   $ bzr init
  $ bzr mkdir foo
  $ bzr commit -m "BASE"
  $ bzr branch . ../other
  $ rmdir foo
  $ touch foo
  $ bzr commit -m "THIS"
  $ bzr mkdir ../other/foo/bar
  $ bzr commit ../other -m "OTHER"
  $ bzr merge ../other   $ bzr init
  $ bzr mkdir white
  $ bzr mkdir black
  $ bzr commit -m "BASE"
  $ bzr branch . ../other
  $ bzr mv white black
  $ bzr commit -m "THIS"
  $ bzr mv ../other/black ../other/white
  $ bzr commit ../other -m "OTHER"
  $ bzr merge ../other   BZR_DISABLE_PLUGINS='myplugin:yourplugin'   BZR_PLUGIN_PATH='/Pfad/zu/meiner/Seite/Erweiterungen:-Seite':+Benutzer   Informationen über das Format und die dazugehörigen Orte anzeigen:   In diesem Fall:   Beachten Sie, dass die Vorgabeanzahl zum Anzeigen von Stufen eine Funktion
  des Protokoll-Formats ist. Wenn die Option -n nicht genutzt wird, dann zeigen die
  Standard-Protokoll-Formate nur die oberste Stufe (mainline).   Siehe ``bzr help revisionspec`` für Informationen dazu, wie X und Y angegeben werden.
  Es folgen ein paar allgemeine Beispiele::   Status-Zusammenfassungen werden mit Status-Markern wie A, M, usw.
  angezeigt. Nutzen Sie die Option  -vv, um Änderungen durch
  Wörter wie ``added`` oder ``modified`` erklärt zu bekommen.   Die Option -r kann genutzt werden, um eine Revision oder einen Bereich von Revisionen
  zu bestimmen, die/der gefiltert werden soll. Es gibt folgende verschiedene Formen::   Die folgenden Einstellungen können genutzt werden, um zu kontrollieren,
  welche Informationen angezeigt werden::   Das Protokoll-Format bestimmt wie Informationen zu jeder Revision
  angezeigt werden. Die Standard-Protokoll-Formate heißen ``long``,
  ``short`` und ``line``. Die Vorgabe ist ``long``.
  Siehe``bzr help log-formats`` für mehr Einzelheiten zu Protokoll-Formaten.   Nutzen Sie die Option --forward, um Revisionen von den ältesten zu den neusten
  anzuzeigen. Der Gebrauch dieser Option hat meistens kleine
  Auswirkungen auf die Gesamtzeit, die benötigt wird, um ein Protokoll zu erstellen.
  Außerdem zeigt --forward Revisionen nicht inkrementell an, wie --reverse dies tut, falls möglich.   [myprojects]
  scheme=ftp
  host=host.de
  user=joe
  password=secret
     ``bzr resolve FILE --action=done'   bzr add            Dateien oder Verzeichnisse werden versioniert
  bzr ignore         Eine Datei oder ein Dateimuster wird ignoriert
  bzr mv             Eine versionierte Datei wird verschoben oder umbenannt   bzr branch ftp://host.com/pfad/zu/meiner/branch   bzr branch ftp://joe:secret@host.com/pfad/zu/meiner/branch   bzr+ssh://remote@shell.beispiel.de/~/meinprojekt/trunk   cd /tmp
  bzr log /tmp/%2False
  bzr log %2False
  bzr log file:///tmp/%252False
  bzr log file://localhost/tmp/%252False
  bzr log file:%252False   http://bzruser:BadPass@bzr.beispiel.de:8080/bzr/trunk   http_proxy=http://proxy.beispiel.de:3128/ 
  https_proxy=http://proxy.beispiel.de:3128/   init    Ein Verzeichnis wird als ein versionierter Zweig festgelegt
  branch  Ein neuer Zweig wird durch Kopieren eines bestehenden Zweigs erstellt
  merge   Ein Dreiwege-Merge wird durchgeführt
  bind    Ein Zweig wird mit einem anderen verbunden
   init    Ein Verzeichnis zu einem versionierten Zweig machen.
 »%(path)s« ist kein Verzeichnis %(extra)s »%s« ist keine gültige <plugin_name>@<plugin_path>-Beschreibung  »bzr diff -p1« ist das gleiche wie »bzr diff --prefix alt/:neu/«
und produziert angepasste Korrekturen für »patch -p1«. %(context_info)s%(path)s ist bereits versioniert. %(context_info)s%(path)s ist nicht versioniert. %(exe_name)s konnte nicht auf diesem Rechner gefunden werden %(msg)s %(host)s%(port)s%(orig_error)s %(source)s wird%(permanently)s umgeleitet nach %(target)s %(url)s ist kein lokaler Pfad. %(username)r scheint keine vernünftige E-Mail-Adresse zu haben %(value)s ist kein Index vom Typ %(_type)s. %6d Geister-Revisionen %6d Revisionen %6d Revisionen nicht vorhanden %6d Texte %d Konflikt automatisch aufgelöst. %d Konflikte automatisch aufgelöst. Auf %d Konflikte gestoßen. %d Schlüsselwort aktualisiert. %d Schlüsselwörter aktualisiert. %r ist zu kurz, um einen relativen Pfad zu berechnen %s ist bereits auf dem neuesten Stand. %s ist nicht in Launchpad registriert »%(display_url)s« ist bereits ein Zweig. (Vorgabe) * 0 = Standard Verhalten
* 1 = Debugger starten * Ein optionaler Patch, der eine Vorschau der gewünschten Änderungen darstellt * ``password``: das Passwort. * ``port``: der Port auf dem der Server horcht, , die Kopfzeilen scheinen beschädigt zu sein. Versuchen Sie -r -1 um den Status der letzten Einreichung zu übernehmen. --after kann nicht mit --auto angegeben werden. --dry-run erfordert --auto. --dry-run zeigt die Dateien an, die hinzugefügt würden,
ohne diese wirklich hinzuzufügen. --exclude-common-ancestry erfordert zwei verschiedene Revisionen --file-ids-from versucht die Datei-IDs des angegebenen Pfades zu benutzen.
Hiermit werden die Datei-IDs verglichen und gemeinsame Elternverzeichnisse
des selben Namen, danach des selben Pfades, gesucht. Diese Option wird selten benötigt,
kann aber sehr hilfreich sein, wenn die selbe Datei in zwei Zweige eingefügt wird, die
später zusammengeführt werden (ohne dabei das doppelte Hinzufügen als Konflikt auszugeben).
Außerdem ist diese Option hilfreich, wenn ein anderes Projekt in ein Unterverzeichnis
eingefügt wird. --prefix erwartet zwei Werte getrennt mit einem Doppelpunkt (z.B. »alt/:neu/«) --product ist veraltet; bitte --project benutzen. --tree und --revision können nicht zusammen verwendet werden. 1. Benutzername und Passwort :Aliase:   :Prüfungen: :Benutzerdefinierte Autoren: :Beschreibung:
  %s

 :Beispiele: :Beispiele:
    Die aktuellen Aliase zeigen:: :Beispiele:
    Die E-Mail-Adresse des aktuellen Benutzers zeigen:: :Rückgabewerte:
    1 - Geändert
    2 - Nicht darstellbare Änderungen
    3 - Fehler
    0 - Keine Änderungen :Von:     Erweiterung »%s«
 :In Unix:   ~/.bazaar/bazaar.conf
:In Windows: C:\\Dokumente und Einstellungen\\Benutzername\\Anwendungsdaten\\bazaar\\2.0\\bazaar.conf :Optionen: %s :Sortierungssteuerung: :Anderes Filtern: :Ausgabesteuerung: :Pfad-Filtern: :Zweck: %s
 :Revision-Filtern: :Siehe auch: %s :Dinge, die zu beachten sind: :Tipps & Tricks: :Aufruf:
%s
 :Aufruf: %s
 :ancestor: :annotate: :before: :branch: :date: :mainline: :revid: :revno: :submit: :tag: Es muss eine Zielkonfiguration angegeben werden. Eine typische Konfigurationsdatei könnte so aussehen:: Es wird eine Warnung ausgegebern, wenn verschachtelte Bäume auftreten,
es sei denn diese werden explizit ignoriert. Angegebene Dateien oder Verzeichnisse hinzufügen. Wird eine Datei hinzugefügt, deren übergeordneter Ordner noch nicht
versioniert wurde, wird dieser ebenfalls automatisch hinzugefügt.
Dies bedeutet, dass Sie niemals explizit einen Ordner hinzufügen
müssen, da dieser hinzugefügt wird, sobald Sie eine Datei aus diesem Ordner
hinzufügen. Für Verzeichnisse, Symbolische Verweise sowie Dateien
mit veränderter Ausführungsberechtigung zeigt Bazaar
den Typ zusätzlich durch ein angehängtes Zeichen
»/«, »@« oder »*« an. Diese Kennzeichnung kann mit der Option
»--no-classify« unterbunden werden. Alle Änderungen erfolgreich angewendet. Alle Konflikte gelöst. Alle versteckten Befehle Bereits ein Zweig: »%(path)s«. Alternativ, um nur die Dateien aufzulisten: Alle Dateien, die auf Muster in der Ignorierliste zutreffen, werden
nicht hinzugefügt, außer Sie geben diese explizit an. Änderungen anwenden, aber diese nicht löschen. Sind Sie sicher, dass Sie diese löschen möchten Launchpad jetzt darum bitten, einen Zweig zu spiegeln. Es kann während der Ausführung Daten rekomprimieren, wodurch
potentiell Speicherplatz gespart und die Performanz verbessert werden können. Autoren Umbenennungen automatisch erraten Keine Änderungen durchführen, wenn Umbenennungen erraten werden. BZRPATH
~~~~~~~ BZR_DISABLE_PLUGINS
~~~~~~~~~~~~~~~~~~~ BZR_EDITOR
~~~~~~~~~~ BZR_EMAIL
~~~~~~~~~ BZR_HOME
~~~~~~~~ BZR_LOG
~~~~~~~ BZR_PDB
~~~~~~~ BZR_PLUGINS_AT
~~~~~~~~~~~~~~ BZR_PROGRESS_BAR
~~~~~~~~~~~~~~~~ BZR_REMOTE_PATH
~~~~~~~~~~~~~~~ BZR_SIGQUIT_PDB
~~~~~~~~~~~~~~~ BZR_SSH
~~~~~~~ Geänderte Dateien sichern (Vorgabe). Ungültiger Wert »%(value)s« für die Option »%(name)s«. Einfache Befehle Grundbefehle:
  bzr init           Aktuelles Verzeichnis wird zu einem versionierten Zweig gemacht
  bzr branch         Eine Kopie eines anderen Entwicklungszweigs wird erstellt Bevor Änderungen übertragen werden, wird für jede ausstehende
Zusammenführung die neueste Revision angezeigt. Mit der Option
-v werden alle ausstehenden Revisionen angezeigt. Um die
Informationen über ausstehende Zusammenführungen komplett
zu unterdrücken kann die Option no-pending verwendet oder
eine Datei oder ein Verzeichnis angegeben werden. Auf einen Abschnitt für Binärdateien gestoßen. Inhalt der E-Mail. Im Zweig %(base)s fehlt Revision %(text_revision)s von %(file_id)s E-Mail-Adresse des Autors des Zweigs, wenn Sie es nicht selbst sind. Format des Zweiges Zweig ist aktiv. Benutzen Sie --force, um ihn zu entfernen. Zweig, in den an Stelle des aktuellen Arbeitsverzeichnisses heruntergeladen wird. Der Zweig zum Hochladen, statt dem Zweig, der das Arbeitsverzeichnis enthält. Von Zweig/Baum vergleichen. Mit Zweig/Baum vergleichen. %d Revision abgezweigt. %d Revisionen abgezweigt. Zweige Zweige sind aktuell.
 Bug Tracker Einstellungen Bug Tracker Einstellungen Es kann nicht --stacked-on und --unstacked gleichzeitig benutzt werden Abgebrochen Abgebrochen
 Nicht möglich, einzelne Dateien zu laden Zertifikatsfehler: %(error)s Zweig wird geprüft bei »%s«. Kommatas haben eine spezielle Bedeutung in URLs und markieren `Abschnittparameter`_ Gebräuchlicher Aufruf wie folgt:
    eval "`bzr bash-completion`" Konfigurationsdatei %(filename)s ist nicht UTF-8-kodiert
 Verbindung abgelaufen: %(msg)s%(orig_error)s Verbindung geschlossen: %(msg)s %(orig_error)s Verbindungsfehler: %(msg)s %(orig_error)s Inhalt der Paketquelle wird als Tarball kopiert … Fehlerhafter oder inkompatibler Datenstrom: %(reason)s Der Zweig, auf den Verwiesen werden soll, kann nicht bestimmt werden. Optionen für Index %(value)s konnten nicht verarbeitet werden. Zweig ohne Arbeitsbaum erstellen. Einen neuen Zweig erstellen, der eine Kopie eines vorhandenen Zweigs ist. Ein neues versioniertes Verzeichnis erstellen. Einen gestapelten Zweig anlegen, der auf den öffentlichen Pfad des übergeordneten Zweigs referenziert. Den Pfad zum Zweig erzeugen, falls dieser noch nicht vorhanden ist. Ein {0} wurde erstellt (Format: {1})
 Neuer Zweig wurde erstellt. Kreuz und Quer Aktuelle Speicherformate Aktuelle Speicherformate Alle ignorierten Dateien löschen. Änderungen löschen ohne diese anzuwenden. Dateien löschen, die bzr nicht bekannt sind (Vorgabe). Aus bzr entfernen, aber die funktionierende Arbeitskopie erhalten. Lösche backup.bzr Lösch-Strategie Veraltet in: %s Beschreibung der neuen Revision. Ausführliches Protokollformat. Diff-Format Diff-Format, das benutzt werden soll. Diff ist auf diesem Rechner nicht installiert: %(msg)s Diff3 ist auf diesem Rechner nicht installiert. Verzeichnis ist nicht leer: »%(path)s«%(extra)s Alle definierten Werte für die zutreffenden Optionen anzeigen. Nur Änderungen im lokalen Zweig anzeigen. Nur Änderungen im entfernten Zweig anzeigen. Nur die E-Mail-Adresse anzeigen. Weitere Informationen anzeigen. Statuszusammenfassung anzeigen. Zeitzone als local, original oder utc anzeigen. Eine Konfigurationsoption anzeigen, festlegen oder löschen. Objekte nicht entsprechend ihres Typs kennzeichnen. Vor dem Löschen nicht nachfragen. Keine Sicherungskopien von zurückgesetzten Dateien erstellen. Keine verteilte Quelle nutzen, auch wenn vorhanden. Keine Änderungen durchführen. Geänderte Dateien nicht sichern. Den Benutzernamen nicht auf Gültigkeit prüfen. Inhalte von Verzeichnissen nicht rekursiv hinzufügen. Ausstehende Zusammenführungen nicht anzeigen. Testlauf, es wird so getan, als ob die obigen Revisionen entfernt würden.
 Umgebungsvariablen Fehler in den Befehlszeilenoptionen Fehler in den Daten für Index %(value)s. Fehler beim Lesen von %(path)r. Beispiel:
~~~~~~~~~ Exportformat %(format)r wird nicht unterstützt Umbenennen von %(from_path)s in %(to_path)s gescheitert: %(why)s Revisionen herunterladen Datei existiert: %(path)r%(extra)s Dateiformat
----------- Die Datei ist binär, sollte aber Text sein. Dateien Dateien
------- Dateien können nicht zwischen Zweigen verschoben werden. Dateien des Zweigs finden Revisionen finden Zum Beispiel: Für weitere Informationen siehe http://help.launchpad.net/
 Das Format %(format)s kann von dieser Bazaar-Version nicht initialisiert werden. Das Format %(format)s ist inkompatibel zu der .bzr-Version %(bzrdir)s. Format von »GNU ChangeLog«-Dateien. Allgemeiner bzr-Smart-Protokoll-Fehler: %(details)s Allgemeiner Fehler im Pfad: %(path)r%(extra)s) Geister-Revision {%(revision_id)s} kann hier nicht benutzt werden. Globale Einstellungen »HTTP MIME Boundary« für %(path)s fehlt: %(msg)sy Wenn sich Zweige unterscheiden, können Sie »bzr push --overwrite« zum Ersetzen
des anderen Zweiges benutzen, wobei die nicht abgeglichenen Veränderungen verworfen werden. Wenn keine Argumente angegeben wurden, wird der Status des
gesamten Arbeitsverzeichnisses angezeigt, andernfalls nur der
Status der angegebenen Dateien und Verzeichnisse. Wenn ein
Verzeichnis angegeben wurde, wird der Status aller darin
enthaltenen Elemente aufgelistet. Wenn keine Revision bestimmt wurde, wird die neueste Revision benutzt. Wenn das letzte Argument ein versioniertes Verzeichnis ist, dann werden
alle anderen Namen in dieses verschoben. Ansonsten müssen es exakt 2
Argumente sein, und die Datei wird umbenannt. Wird kein Standardverzeichnis angegeben, wird dies mit dem ersten Abgleich definiert
(benutzen Sie --no-remember, um es nicht zu definieren). Danach können Sie
das Standardverzeichnis bei der Voreinstellung belassen. Benutzen Sie --remember, um den
Standard zu ändern . Der Wert wird nur gespeichert, wenn die Gegenstelle erreichbar ist. Wenn kein Standardspeicherort für das Hochladen bekannt ist, wird dies mit dem 
ersten Hochladen gesetzt. (Benutzen Sie --no-remember um das Setzen zu vermeiden).
Danach können Sie den Speicherort auf Standard belassen. Benutzen Sie --remember, um den Standard zu verändern. Der Wert wird nur gespeichert, wenn das entfernte Verzeichnis erreichbar ist. Wenn Sie sicherstellen möchten, dass in dem anderen Zweig abweichende Veränderungen vorliegen,
so führen Sie diese zusammen (lesen Sie dazu bzr help merge). Danach können Sie die Zweige ohne 
die Option »--overwrite« hochladen. Wenn Sie Ihre lokalen Änderungen ersetzen möchten und Ihr Zweig
nur mit dem entfernten Zweig übereinstimmen soll, nutzen Sie pull --overwrite.
Das funktioniert auch, wenn zwei Zweige voneinander abweichen. Ignoriere Dateien außerhalb der Ansicht. Die Ansicht ist %s Revisionen importieren Im nicht-rekursiven Modus werden alle Einträge mit Namen hinzugefügt,
auch wenn sie vorher ignoriert wurden. Es wird eine Warnung ausgegeben,
wenn eine der gewählten Dateien schon vorhanden ist. Im rekursiven Modus (Standard) werden Dateien auf die selbe Art und 
Weise behandelt, aber das Verhalten für Ordner ist anders. Ordner, die
bereits versioniert wurden, erzeugen keine Warnung. Alle Ordner,
bereits versioniert oder nicht, werden nach Dateien und Unterordnern
durchsucht, die weder versioniert sind noch ignoriert werden, die dann
hinzugefügt werden. Diese Suche setzt sich rekursiv in versionierten
Ordnern fort. Wenn kein Name angegeben wurde, wird ».« angenommen. Im rekursiven Modus werden Dateien übersprungen, die größer als die Einstellung 
von add.maximum_file_size sind. Einträge mit Namen
werden niemals aufgrund ihrer Größe übersprungen. Sämtliche verfügbaren Informationen einschließen. Neueste Revision jeder Datei einschließen. Revisionsverlauf einschließen. Informationen zur Auswahl eines Speicherformats Revisionen installieren Integration mit Launchpad.net Eingeführt in: %s Eingeführt in: 0.91 Eingeführt in: 1.13 Eingeführt in: 1.14 Eingeführt in: 1.15 Eingeführt in: 1.17 Eingeführt in: 1.6 Eingeführt in: 1.9 Eingeführt in: 2.4 Ungültiger Zweigname: %(name)s Ungültiger HTTP-Content-type »%(ctype)s« für %(path)s: %(msg)s Ungültiger HTTP-Bereich %(range)r für %(path)s: %(msg)s Ungültige HTTP-Antwort für %(path)s: %(msg)s%(orig_error)s Ungültiges Ignorierungsmuster gefunden. %s Ungültige Ignorierungsmuster gefunden. %s Ungültige(s) Muster gefunden. %(msg)s Ungültige Revisionsnummer %(revno)s Launchpad
--------- Launchpad-Projekt-Kurzname, dem der Zweig zugeordnet wird. Launchpad-Benutzername existiert und hat SSH-Schlüssel.
 Launchpad-Benutzername auf »%s« festgelegt.
 In Konflikt stehende Dateien auflisten. Die verfügbaren Zweige an der aktuellen Stelle auflisten. Installierte Erweiterungen auflisten. An den angegebenen Adressen auf Verbindungen lauschen. Ort
~~~~~~~~ Protokollformat Protokollformat mit einer Zeile pro Revision. Längere Beschreibung für den Zweck oder die Inhalte des Zweiges. Datei-IDs von diesem Baum nachschauen. Die Anfrage an diese Adresse schicken. Ein Verzeichnis zu einem versionierten Entwicklungszweig machen. Ungültige Zeile.  %(desc)s
%(line)r Viele Kommandos die URLs akzeptieren, akzeptieren auch Platzaliase.
Siehe :doc:`location-alias-help` und :doc:`url-special-chars-help`.
 Nachrichtenzeichenkette. Die fehlende Funktion %(feature)s wird weder von dieser Bazaar-Version noch von irgendeiner Erweiterung unterstützt. Relativ kurzes Protokollformat. Verschiebe nur den bzr-Bezeichner der Datei, da diese bereits verschoben wurde. Eine Datei verschieben oder umbenennen. %s wird verschoben Name der erzeugten Funktion (Vorgabe: _bzr) Revnos oder das vorhandene Protokoll niemals ändern. Revisionen nur am Ende hinzufügen. Kein Launchpad-Benutzername konfiguriert.
 Kein Fehler, falls vorhanden, übergeordnete Verzeichnisse erstellen, falls nötig. Keine Dateien gelöscht. Keine Hilfe zu diesem Befehl verfügbar. Unbekanntes Format {0} für Versionsinformationen. Unterstützte Typen sind: {1} Keine zutreffenden Dateien. Keine neuen Revisionen oder Schlüsselwörter zum Hochladen. Keine neuen Revisionen zum Hochladen. Kein Verzeichnis zum Herunterladen bekannt oder angegeben. Kein Ort zum Hochladen bekannt oder nicht gewählt. Datei existiert nicht: %(path)r%(extra)s Keine Zielkonfiguration angegeben Keinen Arbeitsbaum zum Entfernen Kein Zweig: »%(path)s«%(detail)s. Beachten Sie, dass --short oder -S für jedes Element eine
Statusmarkierung anzeigt, ähnlich des Statusbefehls von
Subversion. Um eine mit svn -q vergleichbare Ausgabe zu
erhalten, verwenden Sie bzr status -SV. Beachten Sie: Das Verzeichnis kann in Form eines Zweiges oder in Form
eines Pfades angegeben werden, in dem sich eine bei der bzr-Übertragung
entstandene Zusammenführungsanweisung befindet. Nichts zu löschen. Nichts zu tun. Beschreibung des Zweiges in einem Satz. Nur Fehler und Warnungen anzeigen. Es kann nur ein Pfad mit --auto angegeben werden. Nur Dateien entfernen, die niemals eingereicht wurden. Nur versionierte Dateien anzeigen. Eine Launchpad-Zweig-Seite in Ihrem Web-Browser öffnen. %s wird im Web-Browser geöffnet Die Option --change akzeptiert keine Revisionsbereiche Andere Speicherformate Anderer Zweig hat keine neuen Revisionen.
 Nur Schlüsselwörter überschreiben. Übergeordntes Verzeichnis von »%s« ist nicht vorhanden. Diese Optionen an das externe Programm diff übergeben. Pfad(e) ist/sind nicht versioniert: %(paths_as_string)s Pfad(e) existiert/existieren nicht: %(paths_as_string)s%(extra)s Zugriff verweigert: »%(path)s«%(extra)s Bitte geben Sie entweder eine Revision oder einen Bereich an. Die Anfrage vorbereiten, aber nicht absenden. Ignorierte Dateien ausgeben. Nur die Versionsnummer ausgeben. Unbekannte Dateien ausgeben. Versionierte Dateien ausgeben. Im öffentlichen Zweig »%(public_location)s« fehlt Revision »%(revstring)s«. In Revision %d hochgeladen. Rezept für das Importieren eines Dateibaumes:: Rekursiv nach Zweigen suchen, statt nur an der angegebenen Stelle zu suchen. Hochladen verweigern, wenn Veränderungen in dem Arbeitsbaum noch nicht eingereicht wurden,  --no-strict deaktiviert die Überprüfung. Einen Zweig bei launchpad.net registrieren. Verbleibende Konflikte: Den angegebenen Ort als Vorgabe merken. Einen Zweig entfernen. Zweig entfernen, selbst wenn es ein aktiver Zweig ist. Dateien oder Verzeichnisse entfernen. Den Alias entfernen. Die Option aus der Konfigurationsdatei entfernen. Den Arbeitsbaum entfernen, auch wenn dieser nicht übertragene oder zurückgestellte Änderungen enthält. Entfernt den Arbeitsbaum zu einer Branch/einem Checkout. Sicherung wird entfernt … Quelle %r unterstützt keinen Zugriff auf reine Revisionstexte Erneut ausführen, um scheinbare Konflikte zu verringern. Angeforderte Revision: »%(spec)s« existiert nicht im Zweig: %(branch_url)s%(extra)s Die Revision ist inkompatibel zu %(repo_format)s Revision {%(revision_id)s} ist bereits in »%(file_id)s« vorhanden. Revision {%(revision_id)s} ist bereits in %(weave)s vorhanden Revision {%(revision_id)s} ist nicht in »%(file_id)s« vorhanden. Revision {%(revision_id)s} ist nicht in %(weave)s vorhanden Revisionen
 Regel-Muster
------------- Regeln
====== Den bzr-Server starten. SMTP-Verbindung zu %(host)s wurde abgelehnt SMTP-Fehler: %(error)s Dasselbe wie --mine-only. Dasselbe wie --theirs-only. Alle Fragen mit Ja beantworten. Siehe »bzr help break-lock« für mehr Informationen. Siehe »help revisionspec« für Details. Wählen Sie eine andere SSH-Implementierung. Änderungen interaktiv auswählen. Wählen Sie das Ausgabeformat. Der Server meldete einen unerwarteten Fehler: %(error_tuple)r Branch eines Checkout setzen und aktualisieren. SSH/SFTP-Benutzernamen für launchpad.net festlegen. Änderungen anzeigen, aber nicht anwenden oder entfernen. Aktuelle Revisionsnummer anzeigen. Dateien zum Löschen anzeigen, anstelle diese zu löschen. Hilfetext anzeigen. Hilfe zu einem Befehl oder Thema anzeigen.
     Hilfe zu allen Befehlen anzeigen. Liste der umbenannten Dateien zeigen.
     Protokolle geladener Revisionen anzeigen. Den Launchpad-Benutzernamen anzeigen oder festlegen. Revno des Arbeitsbaums anzeigen. Ursprung jeder Zeile in einer Datei anzeigen. Den Wurzelverzeichnisbaum anzeigen. Aufruf und Optionen anzeigen. Version von bzr anzeigen. Nur zeigen, was getan werden würde, aber nichts wirklich machen. Einige intelligente Server oder Protokolle legen den Arbeitsbaum
*möglicherweise* in die Zukunft. Sortierung Geben Sie ein Format für diesen Zweig an. Siehe »help formats«. Standard-Optionen Status-Flags Speicherformate Unterstützte URL-Präfixe:: Unterstützte generische Werte sind: Unterstützte Modifikatoren:: Unterstützte Transportprotokolle Zu Zweig gewechselt: %s Tags können nur einzelnen Revisionen zugewiesen werden, nicht mehreren Zielverzeichnis »%s« ist schon vorhanden. Der Text entsprach nicht seiner Prüfsumme: %(msg)s Die Konfiguration »%(config_id)s« existiert nicht. Die Konfigurationsoption »%(option_name)s« existiert nicht. Die Option --verbose zeigt die heruntergeladenen Versionen unter Benutzung der
log_format-Konfiguration an. Sie können ein anderes Format verwenden, indem Sie
es mit -Olog_format=<Anderes_Format> überschreiben. Die Option --verbose zeigt die verschiedenen hochgeladenen Versionen
unter Benutzung der log_format-Konfiguration an. Sie können ein anderes Format
benutzen, indem Sie es mit -Olog_format=<Anderes_Format> überschreiben. Das 2a-Format mit experimenteller Unterstützung für gemeinsame Zweige.
 Das Bazaar-Smart-Server-Protokoll via TCP. (Vorgabe-Port: 4155) Oben stehende Revision(en) wird/werden entfernt.
 Die auszuführende Aktion. Der Alias »%(alias_name)s« existiert nicht. Der Zweig *MUSS* ein auflistbares System sein, wie z.B. eine lokale Festplatte oder stftp. Der Zweig {0} hat keine Revision {1}. Der Fehler, den dieser Zweig behebt. Der gerade eingefügte Inhalt ist bereits vorhanden. Der Datei-Löschmodus, der verwendet wird. Der Schlüssel »%(key)s« ist bereits im Index »%(index)s«. Der Schlüssel »%(key)s« ist ein ungültiger Schlüssel. Der Pfad %(path)s ist auf dieser Plattform nicht erlaubt Die Quelle {0} enthält keine neue Überarbeitung {1}. DIe Synonyme »clone« und »get« sind für diesen Befehl sind veraltet. Die Arbeitsstruktur des Zielzweiges wird nicht eingepflegt, weil dies
aufwändig ist und auf entfernten Dateisystemen nicht unterstützt wird. Der Baum scheint nicht beschädigt zu sein. Wahrscheinlich möchten Sie stattdessen "bzr revert" ausführen. Benutzen Sie "--force", wenn Sie den Arbeitsbaum wirklich zurücksetzen möchten. Der Vorgang wurde vom Benutzer abgebrochen. Der Wert »%(value)s« ist ein ungültiger Wert. Es gibt zwei mögliche Werte für diese Option: Kein öffentlicher Zweig für »%(branch_url)s« festgelegt. Einfach gesagt: »bzr add« versioniert alle Dateien,
die bisher unbekannt sind. Dieser Zweig hat keine neuen Revisionen.
 Dieser Befehl wird die Namen aller verfügbaren Zweige
an der aktuellen Stelle auflisten. Entspricht der Anzahl der Revisionen in diesem Zweig. Dies entspricht dem Anlegen und Hinzufügen des Ordners. Dies zeigt versionierte und unbekannte Dateien an –
nach Zuständen gruppiert. Mögliche Zustände: Um zu prüfen, was clean-tree tun wird, --dry-run benutzen. Um das Arbeitsverzeichnis mit einer bestimmten Revision zu vergleichen,
geben Sie eine einzelne Revision mit dem Argument "revision" an. Benutzen Sie »bzr checkout«, um den Arbeitsbaum neu aufzubauen. Mit »bzr ignored« werden ignorierte Dateien aufgelistet.
Änderungen an den Dateiinhalten werden mit »bzr diff« angezeigt. Um zu sehen, welche Dateien in einer bestimmten Revision
oder zwischen zwei Revisionen verändert wurden,
geben Sie einen Revisionebereich mit dem Argument "revision" an.
Dies erzeugt das gleiche Ergebnis wie der Aufruf 'bzr diff --summarize'. Zu viele Umleitungen Themen-Liste Übertragungsfehler: %(msg)s %(orig_error)s Baum ist auf den neusten Stand bei Revision {0} von Zweig {1} Diesen Zweig zu einer Spiegelung eines anderen Zweiges machen. Art der Datei, in die exportiert wird. Importieren der Bibliothek »%(library)s« nicht möglich: %(error)s Unbekanntes %(kind)s-Format: %(format)r Unbekannte Regeln festgestellt: %(unknowns_str)s. Entstapeln Nicht unterstütztes Export-Format: %s Ungültiges Zeitzonenformat »%(timezone)s«, die Optionen sind »utc«, »original« und »local«. Eine Spiegelung dieses Zweiges aktualisieren. Auf Revision {0} von Zweig {1} aktualisieren %s wird aktualisiert Benutzen Sie bzr resolve, wenn Sie ein Problem gelöst haben. Kurze Zustandsanzeiger verwenden. Angegebenes Protokollformat benutzen. Das angegebene Ausgabeformat benutzen. Diesen Befehl zum Vergleichen von Dateien benutzen. Nutzen Sie dies, um einen leeren Zweig zu erstellen, oder bevor
ein vorhandenes Projekt importiert wird. Nützliche Befehle:: Gespeichertes übergeordnetes Verzeichnis wird verwendet: %s
 Gespeichertes Verzeichnis zum Hochladen wird verwendet: %s Gültige Werte: Variable {%(name)s} steht nicht zur Verfügung. Versionsinformationen im Python-Format. Versionsinformationen im RIO (nur Text)-Format (Vorgabe). Fehler bei versionierter Datei Ihnen fehlt %d Revision:
 Ihnen fehlen %d Revisionen:
 Sie können nur revision_id oder --revision angeben Entfernen des Arbeitsbaums von einem schlanken Abruf nicht möglich Sie können einen entfernten Arbeitsbaum nicht löschen Sie können keine NULL-Revision angeben. Sie haben %d zusätzliche Revision:
 Sie haben %d zusätzliche Revisionen:
 Sie müssen entweder --revision oder eine revision_id angeben Aktion Hinzugefügt hinzugefügt
    In der Arbeitskopie versioniert, jedoch nicht in der vorherigen Revision. %s hinzugefügt
 Zweig hat keine Revision %s
bzr update --revision funktioniert nur mit einer Revision aus dem Verlauf des Zweigs bugtracker_<tracker>_url
------------------------ bugzilla_<tracker>_url
---------------------- bzr %s --revision benötigt genau eine Revisions-Nummer bzr alias --remove erwartet einen Alias zum Entfernen. bzr diff --revision benötigt exakt ein oder zwei Revisions-Spezifikationen bzr status --revision benötigt exakt eine oder zwei Revisionsspezifikation(en). bzr update --revision benötigt exakt eine Revision bzr update kann nur den gesamten Baum aktualisieren, keine Datei oder kein Unterverzeichnis. Wurzel des Zweiges kann nicht verschoben werden Prüfen überprüfter Zweig {0} Format {1} Revisionen prüfen Revision umwandeln %s gelöscht Pfade werden gelöscht: Beispiel:
    bzr register-branch http://foo.com/bzr/fooproject.mine \
            --project fooproject Baumstatus{0} konnte nicht zurückgesetzt werden behebt den Fehler behebt die Fehler Format Fehler gefunden: %s ignoriert {0} übereinstimmend "{1}"
 Ungültige Richtung %r Ungültiger Typ %r angegeben Dateiart wurde geändert
    Dateiart hat sich geändert (z.B. von Datei zu Verzeichnis) Revision laden %s fehlt Datei-Argument fehlt Geändert verändert Keine gültige Revisionsnummer: %r oder:: Bitte geben Sie entweder --message oder --file an Protokoll entfernt
    In der vorherigen Revision versioniert, jedoch entfernt oder aus der
    Arbeitskopie gelöscht. Umbenannt umbenannt
    Pfad dieser Datei unterscheidet sich von der vorigen Revision;
    der Text hat sich evtl. auch geändert. Das betrifft die Dateien, deren
    übergeordneter Ordner umbenannt wurde. {0} wurde in {1} umbenannt Überspringe {0} (Größer als {1} von {2} Bytes) Entschuldigung, %r ist im Pfad nicht erlaubt um mehrere Dateien zu verschieben, muss das Ziel ein versioniertes Verzeichnis sein trac_<tracker>_url
------------------ »{0}« kann nicht entfernt werden: {1}. %s kann nicht entfernt werden Ungeändert unbekannt
    Nicht versioniert und stimmt nicht mit einem Ignorierungsmuster überein. Unbekannter Befehl »%s« Unbekannter Protokollformatierer: %r Würde verweisen auf »/home/remote/meinprojekt/trunk«. Revision schreiben {0!r} ist in Revision {1} nicht vorhanden {0} und {1} schließen sich gegenseitig aus 