��    F      L  a   |                      	                     '  ,   4     a  &   s     �     �  $   �     �                    '     3  
   :     E     T     \  	   m     w  
   |     �     �     �     �     �  �   �    �     �
     �
     �
     �
     �
             )   7     a  	   g  
   q  �   |  �  -  E  �  �  %  �   �  �  �  ]     
   t       
   �  �   �  +   /     [  �   d     �          $     C     `  ,   y     �     �     �  D  �  �    �  �     J     L     Q  	   ]     g     s     �  9   �     �  ;   �  %   %  )   K  4   u  -   �     �     �     �     �                    1     9     W     d  
   i     t     �     �     �     �  �   �  �  �  	   ,      6   #   G   #   k   1   �   2   �   2   �   V   '!  
   ~!     �!  
   �!  �   �!  �  t"  �  U$  e  �%  �   M(    <)  e   C+     �+     �+     �+    �+  S   �,     1-  �   >-      .     .      6.     W.     w.  )   �.     �.     �.     �.  �  �.  <  �0                  @          :   <   7       4                  ?          %       ;   9           >         *            
   )   .   0      1          E   /       "   F      2                             D          A   -       5                       +          '   $       #         C   6              (   =          ,          B   &   3          	   !   8      About Add Application Browse Config Configure... Could not construct a property list for (%s) Could not load %s Could not write property list for (%s) Could not write to %s Could not write to (%s) Couldn't create pixmap from file: %s Couldn't find pixmap file: %s Credits DSN Database System Description Driver Driver Lib Driver Manager Drivers Enter a DSN name FileUsage Name ODBCConfig ODBCConfig - Credits ODBCConfig - Database Systems ODBCConfig - Drivers ODBCConfig - odbc.ini ODBCConfig - odbcinst.ini Open DataBase Connectivity (ODBC) was developed to be an Open and portable standard for accessing data. unixODBC implements this standard for Linux/UNIX. Perhaps the most common type of Database System today is an SQL Server

SQL Servers with Heavy Functionality
  ADABAS-D
  Empress
  Sybase - www.sybase.com
  Oracle - www.oracle.com

SQL Servers with Lite Functionality
  MiniSQL
  MySQL
  Solid

The Database System may be running on the local machine or on a remote machine. It may also store its information in a variety of ways. This does not matter to an ODBC application because the Driver Manager and the Driver provides a consistent interface to the Database System. Remove Select File Select a DSN to Remove Select a DSN to configure Select a driver to Use Select a driver to configure Select a driver to remove Select the DRIVER to use or Add a new one Setup Setup Lib System DSN System data sources are shared among all users of this machine.These data sources may also be used by system services. Only the administrator can configure system data sources. The Application communicates with the Driver Manager using the standard ODBC calls.

The application does not care; where the data is stored, how it is stored, or even how the system is configured to access the data.

The Application only needs to know the data source name (DSN)

The Application is not hard wired to a particular database system. This allows the user to select a different database system using the ODBCConfig Tool. The Driver Manager carries out a number of functions, such as:
1. Resolve data source names via odbcinst lib)
2. Loads any required drivers
3. Calls the drivers exposed functions to communicate with the database. Some functionality, such as listing all Data Source, is only present in the Driver Manager or via odbcinst lib). The ODBC Drivers contain code specific to a Database System and provides a set of callable functions to the Driver Manager.
Drivers may implement some database functionality when it is required by ODBC and is not present in the Database System.
Drivers may also translate data types.

ODBC Drivers can be obtained from the Internet or directly from the Database vendor.

Check http://www.unixodbc.org for drivers These drivers facilitate communication between the Driver Manager and the data server. Many ODBC drivers  for Linux can be downloaded from the Internet while others are obtained from your database vendor. This is the main configuration file for ODBC.
It contains Data Source configuration.

It is used by the Driver Manager to determine, from a given Data Source Name, such things as the name of the Driver.

It is a simple text file but is configured using the ODBCConfig tool.
The User data sources are typically stored in ~/.odbc.ini while the System data sources are stored in /etc/odbc.ini
 This is the program you are using now. This program allows the user to easily configure ODBC. Trace File Tracing Tracing On Tracing allows you to create logs of the calls to ODBC drivers. Great for support people, or to aid you in debugging applications.
You must be 'root' to set Unable to find a Driver line for this entry User DSN User data source configuration is stored in your home directory. This allows you configure data access without having to be system administrator gODBCConfig - Add DSN gODBCConfig - Appication gODBCConfig - Configure Driver gODBCConfig - Driver Manager gODBCConfig - New Driver gODBCConfig - ODBC Data Source Administrator http://www.unixodbc.org odbc.ini odbcinst.ini odbcinst.ini contains a list of all installed ODBC Drivers. Each entry also contains some information about the driver such as the file name(s) of the driver.

An entry should be made when an ODBC driver is installed and removed when the driver is uninstalled. This can be done using ODBCConfig or the odbcinst command tool. unixODBC consists of the following components

- Driver Manager
- GUI Data Manager
- GUI Config
- Several Drivers and Driver Config libs
- Driver Code Template
- Driver Config Code Template
- ODBCINST lib
- odbcinst (command line tool for install scripts)
- INI lib
- LOG lib
- LST lib
- TRE lib
- SQI lib
- isql (command line tool for SQL)

All code is released under GPL and the LGPL license.
 Project-Id-Version: unixodbc
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2003-12-02 14:45+0000
PO-Revision-Date: 2015-03-05 13:38+0000
Last-Translator: Tobias Bannert <tobannert@gmail.com>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:46+0000
X-Generator: Launchpad (build 18115)
   Info Hinzufügen Anwendung Durchsuchen Konfiguration Einrichten … Es konnte keine Eigenschaftsliste für %s angelegt werden %s konnte nicht geladen werden Es konnte keine Eigenschaftsliste für (%s) erstellt werden In %s konnte nicht geschrieben werden Nach (%s) konnte nicht geschrieben werden Aus der Datei %s konnte keine Pixmap erstellt werden Pixmap-Datei konnte nicht gefunden werden: %s Mitwirkende DSN Datenbanksystem Beschreibung Treiber Treiberbibliothek Treiberverwaltung Treiber Geben Sie einen DSN-Namen ein Dateinutzung Name ODBCConfig ODBCConfig - Mitwirkende ODBCConfig - Datenbanksysteme ODBCConfig - Treiber ODBCConfig - odbc.ini ODBCConfig - odbcinst.ini Open DataBase Connectivity (ODBC) wurde entwickelt, um einen offenen und portablen Standard für den Datenzugriff zu schaffen. unixODBC führt diesen Standard in Linux/Unix ein. Heutzutage ist ein SQL-Server der wahrscheinlich am weitesten verbreitete Typ eines Datenbanksystems.

SQL-Server-Systeme mit großem Funktionsumfang:
  ADABAS-D
  Empress
  Sybase - http://www.sybase.com
  Oracle - http://www.oracle.com

SQL-Server-Systeme mit etwas weniger Funktionen:
  MiniSQL
  MySQL
  Solid

Das Datenbanksystem kann sowohl auf dem lokalen, als auch auf einem entfernten Rechner laufen. Die Speicherung der Daten kann dabei auf unterschiedliche Weise geschehen. Das ist jedoch für eine ODBC-Anwendung unerheblich, da die Treiberverwaltung und der jeweilige Treiber eine einheitliche Schnittstelle zum Datenbanksystem liefern. Entfernen Datei auswählen DSN auswählen, um ihn zu entfernen DSN auswählen, um ihn einzurichten Wählen Sie einen Treiber aus, um ihn zu benutzen Wählen Sie einen Treiber aus, um ihn einzurichten Wählen Sie einen Treiber aus, um ihn zu entfernen Wählen Sie den TREIBER aus, der benutzt werden soll oder fügen Sie einen neuen hinzu Einrichten Einrichtungsbibliothek System-DSN Systemdatenquellen werden von allen Benutzern auf diesem Rechner geteilt. Diese Datenquellen können auch von Systemdiensten genutzt werden. Nur der Systemverwalter kann Systemdatenquellen einrichten. Die Anwendung nutzt die Standard-ODBC-Aufrufe, um mit der Treiberverwaltung zu kommunizieren.

Für die Anwendung ist es unerheblich, wo und wie die Dateien gespeichert sind, oder auch wie der Datenzugriff im System eingerichtet ist.

Die Anwendung muss nur den Namen der Datenquelle (DSN) kennen.

Die Anwendung ist nicht an ein spezielles Datenbanksystem gebunden. Das erlaubt dem Benutzer, mithilfe des Einrichtungswerkzeugs ODBCConfig ein anderes Datenbanksystem auszuwählen. Die Treiberverwaltung bietet Ihnen viele Funktionen, wie zum Beispiel:
1. Ermittlung der Namen der Datenquellen (DSN) über »ODBCINST lib«.
2. Laden des jeweils benötigten Treibers.
3. Aufrufen der offenen Funktionen des Treibers, um mit der Datenbank zu kommunizieren. Einige Funktionen, wie das Auflisten aller Datenquellen, sind nur in der Treiberverwaltung oder über »ODBCINST lib« möglich. Die ODBC-Treiber beinhalten jeweils den für ein Datenbanksystem spezifischen Programmcode und stellen der Treiberverwaltung entsprechende Funktionen zur Verfügung, die von ihr aufgerufen werden können.
Die Treiber könnten einige Datenbankfunktionen ausführen, wenn das von ODBC benötigt wird und die Funktionen nicht bereits im Datenbanksystem selbst vorhanden sind.
Die Treiber können auch Datentypen umwandeln.

ODBC-Treiber können aus dem Internet heruntergeladen oder direkt vom Datenbankanbieter bezogen werden.

Schauen Sie auf der Internetseite http://www.unixodbc.org nach, um Treiber zu erhalten. Diese Treiber vereinfachen die Kommunikation zwischen der Treiberverwaltung und dem Datenserver. Viele ODBC-Treiber für Linux können aus dem Internet heruntergeladen werden, einige andere erhalten Sie jedoch von Ihrem Datenbankanbieter. Das ist die Hauptkonfigurationsdatei für ODBC.
Sie enthält die Einrichtung der Datenquellen.

Sie wird von der Treiberverwaltung dazu benutzt, um zum Namen einer Datenquelle (DSN) z.B. den dazugehörigen Treiber ausfindig zu machen.

Es handelt sich dabei um eine einfache Textdatei, welche durch das ODBC-Einrichtungswerkzeug gepflegt wird.
Die Benutzerdatenquellen werden normalerweise in der Datei ~/.odbc.ini gespeichert, während sich die Einrichtung der Systemdatenquellen in der Datei /etc/odbc.ini befindet.
 Das ist die Anwendung, die Sie gerade benutzen. Diese Anwendung vereinfacht die Einrichtung von ODBC. Protokolldatei Protokollieren Protokollierung An Die Fehlerprotokollierung bietet Ihnen die Möglichkeit, Aufrufe von ODBC-Treibern nachzuvollziehen. Dies erleichtert die Arbeit des Supports und hilft Ihnen bei der Fehlerdiagnose.
Sie müssen Systemverwaltungsrechte besitzen (root), um diese einzuschalten. Für diesen Eintrag konnte in der Zeile keine Angabe eines Treibers gefunden werden Benutzer-DSN Die benutzerdefinierte Datenbankkonfiguration ist in Ihrem persönlichen Ordner gespeichert. Dadurch haben Sie die Möglichkeit, auch ohne Systemverwaltungsrechte den Datenzugriff einzurichten. gODBCConfig - DSN hinzufügen gODBCConfig - Anwendung gODBCConfig - Treiber einrichten gODBCConfig - Treiberverwaltung gODBCConfig - Neuer Treiber gODBCConfig - ODBC-Datenquellenverwaltung http://www.unixodbc.org odbc.ini odbcinst.ini Die Datei odbcinst.ini enthält eine Liste mit allen installierten ODBC-Treibern. Jeder Eintrag enthält außerdem einige Informationen über den jeweiligen Treiber, z.B. dessen Dateiname.

Es sollte stets ein Eintrag in der Datei erfolgen, wenn ein ODBC-Treiber installiert wurde bzw. entfernt werden, wenn der Treiber deinstalliert wurde. Sie können dies entweder mit ODBCConfig oder dem Werkzeug »odbcinst« in der Befehlszeile machen. unixODBC besteht aus den folgenden Komponenten:

- Treiberverwaltung
- Datenpflegewerkzeug mit grafischer Oberfläche
- Einrichtungswerkzeug mit grafischer Oberfläche
- Mehrere Treiber und Bibliotheken zur Treibereinrichtung
- Vorlage für Treiberprogrammcode
- Vorlage für Treibereinrichtungen
- ODBCINST lib
- odbcinst (Werkzeug für die Befehlszeile, für Installationsskripten gedacht)
- INI lib
- LOG lib
- LST lib
- TRE lib
- SQI lib
- isql (Werkzeug für die Befehlszeile für SQL)

Der gesamte Quelltext wurde unter der GPL- und der LGPL-Lizenz veröffentlicht.
 