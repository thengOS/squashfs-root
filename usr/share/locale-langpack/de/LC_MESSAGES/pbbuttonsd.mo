��    �      ,  �   <      �
  g   �
  �   Y     8  *   Q  2   |  &   �  5   �  /        <     U  1   m  B   �     �  	   �  >   �  
   :     E     `     }  !   �     �  )   �  *   �  =      2   ^  )   �  D   �  !         "  ,   <  '   i     �     �     �  $   �  !        .     H     d  4   �  >   �     �  /        D  9   J     �  %   �  7   �  6   �  $   3     X     k     �     �     �     �  ?   �     �          7  3   Q  A   �  =   �  =     ,   C  	   p  2   z  �   �  �  w  R   t     �     �  8   �     '     3  <   G  ?   �     �  /   �       (      *   I  1   t  *   �  -   �  "   �     "  ,   9  =   f     �     �  $   �     �     
       $   2     W  '   w  +   �  $   �     �  ?        P     `     t     �     �     �     �     �     �  !   �  <     A   N     �     �     �     �      �     �  #        @  
   ]     h  %   {     �     �     �     �  �  �  j   �!    �!     #  6   "#  %   Y#  (   #  6   �#  <   �#  N   $  #   k$  6   �$  D   �$     %     %  \   &%  
   �%  1   �%     �%     �%  -   �%  &   #&  4   J&  7   &  9   �&  P   �&  8   B'  P   {'  (   �'      �'  8   (  E   O(  '   �(  '   �(  !   �(  -   )  "   5)  #   X)  $   |)  +   �)  >   �)  H   *  !   U*  5   w*     �*  5   �*     �*  '   +  =   -+  8   k+  8   �+     �+     �+     ,      ,,     M,     R,  M   a,  $   �,  (   �,     �,  6   -  e   Q-  I   �-  U   .  6   W.  	   �.  :   �.  �   �.  �  �/  V   �1  +   �1     &2  K   92     �2     �2  G   �2  K   �2     C3  ;   S3     �3  7   �3  6   �3  :   4  *   W4  +   �4  -   �4     �4  &   �4  -   5     G5  "   ]5  &   �5     �5     �5  -   �5  .   6  !   16  3   S6  5   �6  &   �6  !   �6  A   7     H7     \7     t7     �7     �7     �7     �7  )   �7     �7  -   8  Q   =8  K   �8     �8  '   �8     9     $9  +   @9  (   l9  ,   �9     �9     �9     �9  -   :     4:     Q:     o:      :         N   f   U   %   X   &           3      y   h           '       c   `         +   ~      C   u      #          t      >   q   Y                 /                    \   Z   ?   
      A       o   9   E               P          k   -   w   T   ,       7                   s   z      b   Q   6   g       [   d   i       "   W       x   V           |       l   G   <   J      a   M       _              }   $      �   2   m   	   R       =       @   ]           S       r   .   8       v   *       �   F       :                 0   B   !   p   �   e             4   n   (   {       ^   O   )      D                H   1   j   5      ;             K   L   I      PID   | Name                 | Port    | Flags
  ------+----------------------+---------+-----------
  Configuration file for pbbuttonsd >= version 0.8.0
 For complete list of options please see pbbuttonsd.cnf man-page.
 For description of the file format please see
    http://freedesktop.org/Standards/desktop-entry-spec.
 %-14s - eject the CDROM
 %-14s - prints a list of clients attached
 %-14s - queries the server for certain parameters
 %-14s - reconfigure server parameters
 %-14s - reinitialising the keyboard and the trackpad
 %-14s - save the current configuration to disk
 %-14s - suspend to Disk
 %-14s - suspend to RAM
 %s (version %s) - control client for pbbuttonsd.
 %s - daemon to support special features of laptops and notebooks.
 %s, version %s <unknown> ADB keyboard can't disable fnmode, force mode to 'fkeyslast'.
 ALSA Mixer ATI X1600 Backlight Driver Ambient light sensor found.
 Buffer overflow Can't access i2c device %s - %s.
 Can't attach card '%s': %s
 Can't create IBaM object, out of memory.
 Can't create message port for server: %s.
 Can't find pmud on port 879. Trigger PMU directly for sleep.
 Can't get devmask from mixer [%s]; using default.
 Can't get volume of master channel [%s].
 Can't install PMU input handler. Some functionality may be missing.
 Can't install any signal handler
 Can't load card '%s': %s
 Can't load config file: %s, using defaults.
 Can't open %s. Eject CDROM won't work.
 Can't open ADB device %s: %s
 Can't open PMU device %s: %s
 Can't open card '%s': %s
 Can't open framebuffer device '%s'.
 Can't open mixer device [%s]. %s
 Can't register mixer: %s
 Can't write config file %s
 Card '%s' has no '%s' element.
 Configuration file [%s] is insecure, saving denied.
 Configuration file [%s] is not writable, saving not possible.
 Configuration saved to %s.
 Current battery cycle: %d, active logfile: %s.
 ERROR ERROR: Have problems reading configuration file [%s]: %s
 ERROR: Missing arguments
 ERROR: Not enough memory for buffer.
 ERROR: Problems with IPC, maybe server is not running.
 ERROR: Unexpected answer from server, actioncode %ld.
 ERROR: tag/data pairs not complete.
 File doesn't exist File not a block device File not a file Help or version info INFO Initialized: %s
 Insufficient permissions for object %s, at least '0%o' needed.
 Memory allocation failed.
 Memory allocation failed: %s
 Messageport not available Mixer element '%s' has no playback volume control.
 No backlight driver available - check your Kernel configuration.
 No event devices available. Please check your configuration.
 No mixer driver available - check your Kernel configuration.
 Not all signal handlers could be installed.
 OSS Mixer Option 'ALSA_Elements' contains no valid elements
 Options:
    -%c, --help               display this help and exit
    -%c, --version            display version information and exit
    -%c, --ignore             ignore config return and error values
 Options:
   -%c, --help               display this help text and exit
   -%c, --version            display version information and exit
   -%c, --quiet              suppress welcome message
   -%c, --detach[=PIDFILE]   start %s as background process and
                            optional use an alternative pid-file
                            (default: %s)
   -%c, --configfile=CONFIG  use alternative configuration file
                            (default: %s)
see configuration file for more options.
 Orphaned server port found and removed. All running clients have to be restarted.
 PMU Backlight Driver Permission denied Please check your CDROM settings in configuration file.
 Private Tag Registration failed SECURITY: %s must be owned by the same owner as pbbuttonsd.
 SECURITY: %s must only be writable by the owner of pbbuttonsd.
 Script '%s' %s
 Script '%s' can't be launched - fork() failed.
 Script '%s' doesn't exist.
 Script '%s' lauched but exitcode is %d.
 Script '%s' lauched but exited by signal.
 Script '%s' lauched but killed after %d seconds.
 Script '%s' launched and exited normally.
 Script '%s' skipped because it's not secure.
 Script must be write-only by owner Server already running Server didn't send an answer and timed out.
 Server is already running. Sorry, only one instance allowed.
 Server not found Setting of %s failed: %s.
 Sorry, Couldn't get data for %s: %s. Supported commands:
 Supported tags:
 SysFS Backlight Driver The leading 'TAG_' could be omited.
 The object '%s' doesn't exist.
 The object '%s' is not a block device.
 The object '%s' is not a character device.
 The object '%s' is not a directory.
 The object '%s' is not a file.
 Too many formatsigns. Max three %%s allowed in TAG_SCRIPTPMCS.
 Unknown MacBook Unknown MacBook Pro Unknown Machine Unknown PowerBook Usage:
 Usage: %s [OPTION]... 
 WARNING WARNING: tag %s not supported.
 argument invalid can't be launched - fork() failed config error: %s/%s contains non-integers - using defaults.
 config error: %s/%s needs exactly %d arguments - using defaults.
 doesn't exist failed - unknown error code format error function not supported lauched but exitcode is not null lauched but exited by signal lauched but killed after %d seconds launched and exited normally open error pbbcmd, version %s pmud support compiled in and active.
 read-only value skipped because it's not secure unknown tag write-only value Project-Id-Version: pbbuttons 0.8.0
Report-Msgid-Bugs-To: matthiasgrimm@users.sourceforge.net
POT-Creation-Date: 2007-07-07 19:36+0200
PO-Revision-Date: 2016-02-11 02:58+0000
Last-Translator: Matthias Grimm <Unknown>
Language-Team: german
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:35+0000
X-Generator: Launchpad (build 18115)
   PID   | Name                 | Port    | Schalter
  ------+----------------------+---------+-----------
  Konfigurationsdatei für pbbuttonsd >= Verision 0.8.0
Für die komplette Liste aller verfügbaren Optionen sehen Sie bitte in die pbbuttonsd.cnf man-page.
Für die Beschreibung des Dateiformates sehen Sie unter
    http://freedesktop.org/Standards/desktop-entry-spec.
 %-14s - wirft die CDROM aus
 %-14s - gibt eine Liste der angemeldeten Klienten aus
 %-14s - Afragen von Serverparametern
 %-14s - verändern von Serverparametern
 %-14s - Reinitialisiert die Tastatur und das Trackpad
 %-14s - speichert die aktuelle Konfiguration auf Festplatte
 %-14s - sichert den Systemzustand auf Festplatte und schaltet den Rechner aus
 %-14s - legt die Maschine schlafen
 %s (version %s) - Steuerungsprogramm für PBButtonsd.
 %s - Daemon zur Nutzung spezieller Laptop und Notebook Fähigkeiten
 %s, Version %s <unbekannt> Die Taste Fn kann auf ADB Tastaturen nicht abgeschaltet werden, erzwinge Modus 'fkeyslast'.
 ALSA Mixer ATI X1600 Treiber für LCD Hintergrundbeleuchtung Umgebungslichtsensor gefunden.
 Buffer übergelaufen Kann auf I2C Gerät %s nicht zugreifen - %s.
 Kann Card '%s' nicht hinzufügen: %s.
 Kann IBaM Objekt nicht erzeugen, zu wenig Speicher.
 Kann den Messageport für den Server nicht anlegen: %s
 Kann pmud nicht finden. Schalte in den Replacement Mode.
 Kann verfügbare Kanäle des Mixer [%s] nicht ermitteln, benutze Standardwerte.
 Kann Lautstärke des Masterkanals [%s] nicht ermitteln.
 Kann PMU Inputhandler nicht installieren. Einige Funktionen werden nicht gehen.
 Kann keinen Signalhandler installieren.
 Kann Card '%s' nicht laden: %s.
 Kann Konfigdatei %s nicht lesen, benutze Standardwerte.
 Kann %s nicht öffnen. Auswerfen der CDROM wird nicht funktionieren.
 Kann ADB Device [%s] nicht öffnen: %s
 Kann PMU Device [%s] nicht öffnen: %s
 Kann Card '%s' nicht öffnen: %s
 Kann Framebuffer Device '%s'  nicht öffnen.
 Kann Mixer [%s] nicht öffnen: %s
 Kann Mixer nicht registrieren: %s.
 Kann Konfigdatei %s nicht speichern
 Card '%s' hat kein Element mit Namen '%s'.
 Konfigdatei [%s] ist unsicher; speichern der Konfig gesperrt.
 Konfigdatei [%s] nicht schreibbar; speichern der Konfig nicht möglich.
 Konfiguration in %s gespeichert.
 Aktueller Akkuzyklus: %d, aktive Protokolldatei: %s.
 FEHLER FEHLER: Problem beim Lesen der Konfigdatei [%s]: %s.
 FEHLER: Argumente fehlen.
 FEHLER: Zu wenig Speicher für Buffer.
 FEHLER: IPC Problem, wahrscheinlich läuft der Server nicht.
 FEHLER: Unerwartete Antwort vom Server, Actioncode %ld.
 FEHLER: Tag und Daten müssen immer paarweise auftreten
 Datei existiert nicht Die Datei ist kein Blockgerät Die Datei ist keine Datei Hilfe oder Versionsinformationen INFO Aktiviert: %s
 Zugriffsrechte für Objekt %s reichen nicht aus, benötige mindestens '0%o'.
 Speicheranforderung fehlgeschlagen.
 Speicheranforderung fehlgeschlagen: %s.
 Messageport nicht verfügbar Mixer Element '%s' hat keine Lautstärke-Eigenschaft.
 Kein Treiber für die Hintergrundbeleuchtung verfügbar, bitte prüfen Sie Ihre Kernelkonfiguration.
 Kein Event Device verfügbar, bitte prüfen Sie Ihre Systemkonfiguration
 Kein Treiber für Soundmixer verfügbar, bitte prüfen Sie Ihre Systemkonfiguration.
 Nicht alle Signal-Handler konnten installiert werden.
 OSS Mixer Option 'ALSA_Elements' enthält keine gültigen Elemente.
 Optionen:
    -%c, --help               zeigt die Kurzhilfe an
    -%c, --version            zeigt Versionsinformationen an
    -%c, --ignore             ignoriere 'config'-Rückgabewerte und Fehlermeldungen
 Optionen:
   -%c, --help               zeigt die Hilfe
   -%c, --version            zeigt die Programmverion an
   -%c, --quiet              unterdrückt die Startmeldung
   -%c, --detach[=PIDFILE]   startet %s als Hintergrundprozess und
                            verwendet ggf. eine alternative PID-Datei
                            (Default: %s)
   -%c, --configfile=CONFIG  benutze alternative Konfigurationsdatei
                            (Default: %s)
Siehe Konfigurationsdatei für weitere Optionen.
 Verwaisten Serverport gelöscht. Alle laufenden Clients müssen neu gestartet werden.
 PMU Treiber für LCD Hintergrundbeleuchtung Zugriff verweigert Bitte überprüfen Sie ihre CDROM Einstellungen in der Konfigurationsdatei
 Privater Tag Registrierung fehlgeschlagen SICHERHEIT: Der Besitzer von %s und PBButtonsd müssen identisch sein.
 SICHERHEIT: %s darf nur durch den Besitzer von PBButtonsd schreibbar sein.
 Script '%s' %s
 Script '%s' nicht ausgeführt - fork() ist fehlgeschlagen.
 Script '%s' existiert nicht.
 Script '%s' gestartet, aber mit Fehlercode %d beendet.
 Script '%s' gestartet, aber durch ein Signal beendet.
 Script '%s' gestartet, aber nach %d Sekunden abgebrochen.
 Script '%s' gestartet und normal beendet.
 Script '%s' übergangen weil nicht sicher.
 Script darf nur vom Besitzer schreibbar sein. Server läuft bereits Der Server antwortete nicht, Timeout.
 Der Server darf nur einmal gestartet werden.
 Server nicht gefunden Setzen von %s fehlgeschlagen: %s.
 Kann Daten für %s nicht bekommen: %s. Unterstützte Befehle:
 Unterstützte Tags:
 SysFS Treiber für LCD Hintergrundbeleuchtung Das führende 'TAG_' kann weggelassen werden.
 Das Objekt '%s' existiert nicht.
 Das Object '%s' ist kein blockorientiertes Gerät.
 Das Objekt '%s' ist kein zeichenorientiertes Gerät.
 Das Objekt '%s' ist kein Verzeichnis.
 Das Objekt '%s' ist keine Datei.
 Zu viele Formatzeichen. Max. drei %%s in TAG_SCRIPTPMCS erlaubt.
 Unbekanntes MacBook Unbekanntes MacBook Pro Unbekannter Rechner Unbekanntes PowerBook Benutzung:
 Benutzung: %s [Optionen]... 
 WARNUNG WARNUNG: Tag %s wird nicht unterstützt.
 Ungültiges Argument nicht ausgeführt - fork() ist fehlgeschlagen Konfigurationsfehler: %s/%s enthält ungültige Zahlen - verwende Standardwerte.
 Konfigurationsfehler: %s/%s braucht %d Argumente - verwende Standardwerte.
 existiert nicht fehlgeschlagen - unbekannter Fehlercode Fehler im Format Funktion nicht unterstützt gestartet, aber der Exitcode ist nicht Null gestartet, aber beendet durch ein Signal gestartet, aber nach %d Sekunden abgebrochen gestartet und normal beendet Fehler beim Öffnen pbbcmd, Version %s Pmud Unterstützung vorhanden und aktiviert.
 Wert kann nur gelesen werden übergangen weil nicht sicher Unbekannter Tag Wert kann nur geschrieben werden 