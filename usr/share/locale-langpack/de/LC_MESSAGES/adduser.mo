��    t      �  �   \      �	  ]   �	     /
  �   6
              8  #   X  .   |  2   �  )   �  %        .  #   D  �   h  '   C  )   k  8   �     �     �     �  =     ,   Y  )   �     �  !   �     �     �       =   5  �   s     (  "   7  '   Z  ?   �  G   �  G   
  ?   R  P   �  H   �      ,  3   M  -   �  "   �     �  1   �  6   $     [  *   n     �     �     �     �     �  '     &   9  &   `  7   �     �  $   �     �  1     B   >     �     �      �     �  B   �  :   ?  A   z     �     �  /   �     ,      J  '   k  @   �  .   �  <     8   @     y  '   �  ?   �        +     -   K  -   y     �  �  �     �  (   �  J   �  ?   J  7   �  =   �  Y      )   Z  6   �  %   �  %   �  &     ,   .  �  [     V#  �  k#  �   5'     �'     �'  2   �'  -   +(     Y(     u(     �(  0   �(  !   �(  (   )  �  .)  �   +     �+    �+  X  �,  #   .  $   8.  (   ].  9   �.  =   �.  C   �.  +   B/     n/  2   �/  �   �/  <   �0  8   �0  V   #1  4   z1  "   �1     �1  =   �1  G   02  4   x2  )   �2  %   �2     �2  >   3  A   E3  P   �3  �   �3     �4  (   �4  '   �4  E   5  M   T5  M   �5  E   �5  U   66  M   �6  '   �6  F   7  E   I7  (   �7  !   �7  <   �7  9   8     Q8  *   e8     �8      �8     �8     �8     9  /   9  1   O9  1   �9  L   �9  '    :  .   (:     W:  K   d:  K   �:     �:  #   ;  '   =;  #   e;  N   �;  D   �;  M   <  %   k<  #   �<  8   �<  "   �<  $   =  3   6=  Q   j=  ;   �=  K   �=  C   D>  '   �>  5   �>  R   �>  %   9?  @   _?  9   �?  ;   �?  &   @  [  =@     �B  0   �B  d   �B  L   IC  I   �C  \   �C  Y   =D  6   �D  F   �D  /   E  8   EE  4   ~E  2   �E  U  �E     <J  H  QJ  �   �N     -O  '   BO  K   jO  9   �O  !   �O  $   P  >   7P  L   vP  &   �P  ,   �P     ;          `       W   <   :       ^   g                  .       *      (      X       6      j       G           K   7      9   S      R   P   b   %   H       T   #   0       l          5       F   @       O       ,   +   t       U      J   N           p   )   ?   &   C   Q   A              	                  Y      _      2       B      '           4              \       D      i   3   V   E      8       >          
       e   1   h   ]   Z   s   -   =   c   I       /       "       m   d   o          !       $      L   q       a           M               f   k         r   n      [       %s failed with return code 15, shadow not enabled, password aging cannot be set. Continuing.
 %s: %s %s: Please enter a username matching the regular expression configured
via the NAME_REGEX[_SYSTEM] configuration variable.  Use the `--force-badname'
option to relax this check or reconfigure NAME_REGEX.
 %s: To avoid problems, the username should consist only of
letters, digits, underscores, periods, at signs and dashes, and not start with
a dash (as defined by IEEE Std 1003.1-2001). For compatibility with Samba
machine accounts $ is also supported at the end of the username
 Adding group `%s' (GID %d) ...
 Adding new group `%s' (%d) ...
 Adding new group `%s' (GID %d) ...
 Adding new user `%s' (%d) with group `%s' ...
 Adding new user `%s' (UID %d) with group `%s' ...
 Adding new user `%s' to extra groups ...
 Adding system user `%s' (UID %d) ...
 Adding user `%s' ...
 Adding user `%s' to group `%s' ...
 Adds a user or group to the system.
  
Copyright (C) 1997, 1998, 1999 Guy Maor <maor@debian.org>
Copyright (C) 1995 Ian Murdock <imurdock@gnu.ai.mit.edu>,
                   Ted Hajek <tedhajek@boombox.micro.umn.edu>

 Allowing use of questionable username.
 Backing up files to be removed to %s ...
 Cannot deal with %s.
It is not a dir, file, or symlink.
 Cannot handle special file %s
 Caught a SIG%s.
 Copying files from `%s' ...
 Copyright (C) 2000 Roland Bauerschmidt <roland@copyleft.de>

 Could not find program named `%s' in $PATH.
 Couldn't create home directory `%s': %s.
 Couldn't parse `%s', line %d.
 Creating home directory `%s' ...
 Done.
 Enter a group name to remove:  Enter a user name to remove:  If you really want this, call deluser with parameter --force
 In order to use the --remove-home, --remove-all-files, and --backup features,
you need to install the `perl-modules' package. To accomplish that, run
apt-get install perl-modules.
 Internal error Is the information correct? [Y/n]  Looking for files to backup/remove ...
 No GID is available in the range %d-%d (FIRST_GID - LAST_GID).
 No GID is available in the range %d-%d (FIRST_SYS_GID - LAST_SYS_GID).
 No UID is available in the range %d-%d (FIRST_SYS_UID - LAST_SYS_UID).
 No UID is available in the range %d-%d (FIRST_UID - LAST_UID).
 No UID/GID pair is available in the range %d-%d (FIRST_SYS_UID - LAST_SYS_UID).
 No UID/GID pair is available in the range %d-%d (FIRST_UID - LAST_UID).
 No options allowed after names.
 Not backing up/removing `%s', it is a mount point.
 Not backing up/removing `%s', it matches %s.
 Not creating home directory `%s'.
 Only one or two names allowed.
 Only root may add a user or group to the system.
 Only root may remove a user or group from the system.
 Permission denied
 Removes users and groups from the system.
 Removing crontab ...
 Removing directory `%s' ...
 Removing files ...
 Removing group `%s' ...
 Removing user `%s' ...
 Removing user `%s' from group `%s' ...
 Selecting GID from range %d to %d ...
 Selecting UID from range %d to %d ...
 Setting quota for user `%s' to values of user `%s' ...
 Setting up encryption ...
 Specify only one name in this mode.
 Stopped: %s
 Stopping now without having performed any action
 The --group, --ingroup, and --gid options are mutually exclusive.
 The GID %d does not exist.
 The GID %d is already in use.
 The GID `%s' is already in use.
 The UID %d is already in use.
 The group `%s' already exists and is not a system group. Exiting.
 The group `%s' already exists as a system group. Exiting.
 The group `%s' already exists, but has a different GID. Exiting.
 The group `%s' already exists.
 The group `%s' does not exist.
 The group `%s' is not a system group. Exiting.
 The group `%s' is not empty!
 The group `%s' was not created.
 The home dir must be an absolute path.
 The home directory `%s' already exists.  Not copying from `%s'.
 The system user `%s' already exists. Exiting.
 The user `%s' already exists with a different UID. Exiting.
 The user `%s' already exists, and is not a system user.
 The user `%s' already exists.
 The user `%s' already exists. Exiting.
 The user `%s' does not exist, but --system was given. Exiting.
 The user `%s' does not exist.
 The user `%s' is already a member of `%s'.
 The user `%s' is not a member of group `%s'.
 The user `%s' is not a system user. Exiting.
 The user `%s' was not created.
 This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License, /usr/share/common-licenses/GPL, for more details.
 Try again? [y/N]  Unknown variable `%s' at `%s', line %d.
 Usually this is never required as it may render the whole system unusable
 WARNING: You are just about to delete the root account (uid 0)
 Warning: The home dir %s you specified already exists.
 Warning: The home dir %s you specified can't be accessed: %s
 Warning: The home directory `%s' does not belong to the user you are currently creating.
 Warning: group `%s' has no more members.
 You may not remove the user from their primary group.
 `%s' does not exist. Using defaults.
 `%s' exited from signal %d. Exiting.
 `%s' returned error code %d. Exiting.
 `%s' still has `%s' as their primary group!
 adduser [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]
[--firstuid ID] [--lastuid ID] [--gecos GECOS] [--ingroup GROUP | --gid ID]
[--disabled-password] [--disabled-login] [--encrypt-home] USER
   Add a normal user

adduser --system [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]
[--gecos GECOS] [--group | --ingroup GROUP | --gid ID] [--disabled-password]
[--disabled-login] USER
   Add a system user

adduser --group [--gid ID] GROUP
addgroup [--gid ID] GROUP
   Add a user group

addgroup --system [--gid ID] GROUP
   Add a system group

adduser USER GROUP
   Add an existing user to an existing group

general options:
  --quiet | -q      don't give process information to stdout
  --force-badname   allow usernames which do not match the
                    NAME_REGEX[_SYSTEM] configuration variable
  --extrausers      uses extra users as the database
  --help | -h       usage message
  --version | -v    version number and copyright
  --conf | -c FILE  use FILE as configuration file

 adduser version %s

 deluser USER
  remove a normal user from the system
  example: deluser mike

  --remove-home             remove the users home directory and mail spool
  --remove-all-files        remove all files owned by user
  --backup                  backup files before removing.
  --backup-to <DIR>         target directory for the backups.
                            Default is the current directory.
  --system                  only remove if system user

delgroup GROUP
deluser --group GROUP
  remove a group from the system
  example: deluser --group students

  --system                  only remove if system group
  --only-if-empty           only remove if no members left

deluser USER GROUP
  remove the user from a group
  example: deluser mike students

general options:
  --quiet | -q      don't give process information to stdout
  --help | -h       usage message
  --version | -v    version number and copyright
  --conf | -c FILE  use FILE as configuration file

 deluser is based on adduser by Guy Maor <maor@debian.org>, Ian Murdock
<imurdock@gnu.ai.mit.edu> and Ted Hajek <tedhajek@boombox.micro.umn.edu>

 deluser version %s

 fork for `find' failed: %s
 fork for `mount' to parse mount points failed: %s
 getgrnam `%s' failed. This shouldn't happen.
 invalid argument to option
 invalid combination of options
 passwd file busy, try again
 pipe of command `mount' could not be closed: %s
 unexpected failure, nothing done
 unexpected failure, passwd file missing
 Project-Id-Version: adduser 3.95
Report-Msgid-Bugs-To: adduser-devel@lists.alioth.debian.org
POT-Creation-Date: 2015-07-02 22:08+0200
PO-Revision-Date: 2015-09-06 13:32+0000
Last-Translator: schuko24 <gerdsaenger@t-online.de>
Language-Team: German <debian-l10n-german@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:56+0000
X-Generator: Launchpad (build 18115)
Language: de
 %s schlug mit dem Rückgabewert 15 fehl, shadow ist nicht aktiviert, das Altern von Passwörtern kann nicht eingestellt werden. Programm fährt fort.
 %s: %s %s: Bitte geben Sie einen Benutzernamen ein, der mit dem regulären Ausdruck übereinstimmt, der durch die NAME_REGEX[_SYSTEM]-Variable konfiguriert wurde.  Nutzen Sie die Option »--force-badname«,
um diese Prüfung zu deaktivieren oder ändern Sie die NAME_REGEX-Variable.
 %s: Um Probleme zu vermeiden, sollte der Benutzername nur aus Buchstaben,
Ziffern, Unterstrichen, Punkten, Klammeraffen (@) und Bindestrichen bestehen
und nicht mit einem Bindestrich anfangen (wie durch IEEE Std 1003.1-2001
festgelegt). Zur Kompatibilität mit Konten auf Samba-Rechnern wird
außerdem $ am Ende des Benutzernamens unterstützt
 Lege Gruppe »%s« (GID %d) an ...
 Lege neue Gruppe »%s« (%d) an ...
 Lege neue Gruppe »%s« (GID %d) an ...
 Lege neuen Benutzer »%s« (%d) mit Gruppe »%s« an ...
 Lege neuen Benutzer »%s« (UID %d) mit Gruppe »%s« an ...
 Füge neuen Benutzer »%s« zu den zusätzlichen Gruppen hinzu ...
 Lege Systembenutzer »%s« (UID %d) an ...
 Lege Benutzer »%s« an ...
 Füge Benutzer »%s« der Gruppe »%s« hinzu ...
 Fügt einen Benutzer oder eine Gruppe zum System hinzu.
  
Copyright (C) 1997, 1998, 1999 Guy Maor <maor@debian.org>
Copyright (C) 1995 Ian Murdock <imurdock@gnu.ai.mit.edu>,
                   Ted Hajek <tedhajek@boombox.micro.umn.edu>

 Verwendung eines zweifelhaften Benutzernamens wird erlaubt.
 Die zu löschenden Dateien werden nach %s gesichert ...
 Kann mit »%s« nicht umgehen.
Es ist kein Verzeichnis, keine Datei und kein Symlink.
 Die spezielle Datei %s kann nicht bearbeitet werden
 Das Signal SIG%s wurde empfangen.
 Kopiere Dateien aus »%s« ...
 Copyright (C) 2000 Roland Bauerschmidt <roland@copyleft.de>

 Kein Programm mit dem Namen »%s« in Verzeichnisliste $PATH gefunden.
 Konnte Home-Verzeichnis »%s« nicht erstellen: %s.
 Konnte »%s«, Zeile %d nicht auswerten.
 Erstelle Home-Verzeichnis »%s« ...
 Fertig.
 Geben Sie den Namen der Gruppe ein, die entfernt werden soll:  Geben Sie den Namen des Benutzers ein, der entfernt werden soll:  Wenn Sie dies wirklich wollen, rufen Sie deluser mit dem Parameter --force auf.
 Um die Optionen --remove-home, --remove-all-files und --backup nutzen zu
können, müssen Sie das »perl-modules«-Paket installieren. Um dies zu tun,
führen Sie den Befehl »apt-get install perl-modules« aus.
 Interner Fehler Sind diese Informationen korrekt? [J/n]  Suche Dateien zum Sichern/Löschen ...
 Es ist keine GID im Bereich %d-%d verfügbar (FIRST_GID - LAST_GID).
 Es ist keine GID im Bereich %d-%d verfügbar (FIRST_SYS_GID - LAST_SYS_GID).
 Es ist keine UID im Bereich %d-%d verfügbar (FIRST_SYS_UID - LAST_SYS_UID).
 Es ist keine UID im Bereich %d-%d verfügbar (FIRST_UID - LAST_UID).
 Es ist kein UID/GID-Paar im Bereich %d-%d verfügbar (FIRST_SYS_UID - LAST_SYS_UID).
 Es ist kein UID/GID-Paar im Bereich %d-%d verfügbar (FIRST_UID - LAST_UID).
 Keine Optionen nach dem Namen erlaubt.
 »%s« wurde nicht gesichert/gelöscht, da es ein Einhängepunkt ist.
 »%s« wurde nicht gesichert/gelöscht, da es mit %s übereinstimmt.
 Erstelle Home-Verzeichnis »%s« nicht.
 Nur ein oder zwei Namen erlaubt.
 Nur root darf Benutzer oder Gruppen zum System hinzufügen.
 Nur root darf Benutzer oder Gruppen vom System löschen.
 Zugriff verweigert
 Entfernt Benutzer und Gruppen vom System.
 Entferne crontab ...
 Entferne Verzeichnis »%s« ...
 Dateien werden gelöscht ...
 Entferne Gruppe »%s« ...
 Entferne Benutzer »%s« ...
 Entferne Benutzer »%s« aus Gruppe »%s« ...
 Wähle GID aus dem Bereich von %d bis %d aus ...
 Wähle UID aus dem Bereich von %d bis %d aus ...
 Setze Quota für den Benutzer »%s« auf die Werte des Benutzers »%s« ...
 Verschlüsselung wird eingerichtet …
 Geben Sie in diesem Modus nur einen Namen an.
 Beendet: %s
 Das Programm wird nun beendet, ohne eine Änderung durchgeführt zu haben.
 Die Optionen --group, --ingroup und --gid schließen sich gegenseitig aus.
 Die GID %d existiert nicht.
 Die GID %d wird bereits verwendet.
 Die GID »%s« wird bereits verwendet.
 Die UID %d wird bereits verwendet.
 Die Gruppe »%s« existiert bereits und ist keine Systemgruppe. Programmende.
 Die Gruppe »%s« existiert bereits als Systemgruppe. Programmende.
 Die Gruppe »%s« existiert bereits, hat aber eine andere GID. Programmende.
 Die Gruppe »%s« existiert bereits.
 Die Gruppe »%s« existiert nicht.
 Die Gruppe »%s« ist keine Systemgruppe. Programmende.
 Die Gruppe »%s« ist nicht leer!
 Gruppe »%s« wurde nicht angelegt.
 Das Home-Verzeichnis muss ein absoluter Pfad sein.
 Das Home-Verzeichnis »%s« existiert bereits. Kopiere keine Dateien aus »%s«.
 Der Systembenutzer »%s« existiert bereits. Programmende.
 Der Benutzer »%s« existiert bereits mit einer anderen UID. Programmende.
 Der Benutzer »%s« existiert bereits und ist kein Systembenutzer.
 Der Benutzer »%s« existiert bereits.
 Der Benutzer »%s« existiert bereits. Programmende.
 Der Benutzer »%s« existiert nicht, aber --system wurde angegeben.
Programmende.
 Der Benutzer »%s« existiert nicht.
 Der Benutzer »%s« ist bereits ein Mitglied der Gruppe »%s«.
 Der Benutzer »%s« ist kein Mitglied der Gruppe »%s«.
 Der Benutzer »%s« ist kein Systembenutzer. Programmende.
 Benutzer »%s« wurde nicht angelegt.
 Dieses Programm ist freie Software. Sie können es unter den Bedingungen der
GNU General Public License, wie von der Free Software Foundation
veröffentlicht, weitergeben und/oder modifizieren, entweder gemäß Version 2
der Lizenz oder (nach Ihrer Option) jeder späteren Version.

Die Veröffentlichung dieses Programms erfolgt in der Hoffnung, dass es Ihnen
von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, sogar ohne die implizite
Garantie der MARKTREIFE oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
Details finden Sie in der GNU General Public License
(/usr/share/common-licenses/GPL).
 Nochmal versuchen? [j/N]  Unbekannte Variable »%s« in »%s«, Zeile %d.
 Üblicherweise ist dies niemals notwendig, weil dadurch das gesamte System unbrauchbar werden kann.
 WARNUNG: Sie sind gerade dabei, das root-Benutzerkonto (UID 0) zu löschen.
 Warnung: Das von Ihnen angegebene Home-Verzeichnis %s existiert bereits.
 Warnung: Auf das von Ihnen angegebene Home-Verzeichnis %s kann nicht zugegriffen werden: %s
 Warnung: Das Home-Verzeichnis »%s« gehört nicht dem Benutzer, den Sie gerade anlegen.
 Warnung: Die Gruppe »%s« hat keine Mitglieder mehr.
 Sie dürfen den Benutzer nicht aus seiner primären Gruppe entfernen.
 »%s« existiert nicht. Benutze Standardwerte.
 »%s« wurde durch das Signal %d beendet. Programmende.
 »%s« gab den Fehlercode %d zurück. Programmende.
 »%s« hat immer noch »%s« als primäre Gruppe!
 adduser [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]
[--firstuid ID] [--lastuid ID] [--gecos GECOS] [--ingroup GROUP | --gid ID]
[--disabled-password] [--disabled-login] [--encrypt-home] USER
   Standard-User hinzufügen
adduser --system [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]
[--gecos GECOS] [--group | --ingroup GROUP | --gid ID] [--disabled-password]
[--disabled-login] USER
   System-User hinzufügen

adduser --group [--gid ID] GROUP
addgroup [--gid ID] GROUP
   User-Gruppe hinzufügen

addgroup --system [--gid ID] GROUP
   System-Gruppe hinzufügen

adduser USER GROUP
   Bestehenden User einer bestehenden Gruppe zuordnen

Allgemeine Einstellungen:
  --quiet | -q      Prozessinformationen nicht an Standardausgabe senden
  --force-badname   Benutzernamen erlauben, die nicht zur Systemvorgabe passen
                    NAME_REGEX[_SYSTEM] configuration variable
  --extrausers      verwendet Extra-User als  Datenbestand
  --help | -h       Benutzungshilfe
  --version | -v    Versionsnummer und Copyright
  --conf | -c FILE  benutze FILE als Konfigurationsdatei

 adduser version %s

 deluser BENUTZER
  Entfernt einen normalen Benutzer vom System
  Beispiel: deluser mike

  --remove-home             Entfernt das Home-Verzeichnis und Mail-Spool des
                            Benutzers
  --remove-all-files        Entfernt alle Dateien des Benutzers
  --backup		    Dateien vor dem Löschen sichern.
  --backup-to <VERZ>        Zielverzeichnis für die Sicherheitskopien.
                            Standard ist das aktuelle Verzeichnis.
  --system                  Nur entfernen, wenn Systembenutzer

delgroup GRUPPE
deluser --group GRUPPE
  Entfernt eine Gruppe vom System
  Beispiel: deluser --group students

  --system                  Nur entfernen, wenn Systemgruppe
  --only-if-empty           Nur entfernen, wenn ohne Mitglieder

deluser BENUTZER GRUPPE
  Entfernt den Benutzer aus einer Gruppe
  Beispiel: deluser mike students

Allgemeine Optionen:
  --quiet | -q      Keine Prozessinformationen an stdout senden
  --help | -h       Hilfstext zur Verwendung
  --version | -v    Versionsnummer und Copyright
  --conf | -c DATEI benutze DATEI als Konfigurationsdatei

 deluser basiert auf adduser von Guy Maor <maor@debian.org>, Ian Murdock
<imurdock@gnu.ai.mit.edu> und Ted Hajek <tedhajek@boombox.micro.umn.edu>

 deluser version %s

 Der Fork für »find« schlug fehl: %s
 Der Fork für »mount« zum Auswerten von Einhängepunkten schlug fehl:
%s
 getgrnam »%s« schlug fehl. Das sollte nicht passieren.
 Ungültiger Parameter zur Option
 Ungültige Kombination von Optionen
 Die Datei passwd wird gerade benutzt, versuchen Sie es erneut
 Die Weiterleitung des Befehls »mount« konnte nicht geschlossen werden:
%s
 Unerwarteter Fehler, nichts geändert
 Unerwarteter Fehler, die Datei passwd fehlt
 