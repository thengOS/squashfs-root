��   �  0   �  �  �*     L9     T9  \9      �9     �9     �9     �9  
   �9  	   �9     :  "   :  &   7:  2   ^:     �:  +   �:      �:     �:      ;     ;;     K;     Z;     f;     i;  2   �;     �;     �;     �;     �;  (   
<  &   3<  5   Z<  2   �<  3   �<     �<     =     $=  $   4=     Y=  +   y=     �=     �=     �=     �=     �=  B   �=  !   @>     b>     �>     �>     �>     �>     �>  &   	?     0?     >?  -   ^?  1   �?     �?  $   �?     �?     @  "   3@     V@     n@     �@  !   �@     �@      �@  !   A  "   %A     HA  #   cA  M   �A     �A     �A     �A     �A     B     B  
   -B     8B     GB     NB     [B     jB     yB     �B     �B  	   �B     �B     �B     �B     �B     �B  <   C  9   IC  ?   �C  I   �C  >   D  <   LD  >   �D  ?   �D  F   E  U   OE  G   �E  6   �E  J   $F  ?   oF  D   �F  $   �F  
   G     $G  %   *G     PG  
   \G     gG     vG     }G  
   �G  	   �G  ,   �G     �G  )   �G  *   H     >H  !   \H     ~H     �H     �H     �H     �H     �H  	   I     I     2I     GI     fI     zI     �I  2   �I  A   �I     J     2J     QJ     UJ  $   rJ     �J  1   �J  )   �J  (   K  $   7K     \K  6   sK  #   �K     �K     �K      L  $   "L  *   GL     rL  +   �L     �L     �L  	   �L     �L  ;   �L     8M      HM     iM     �M  $   �M  "   �M     �M     �M  "   N     .N     IN     _N  "   |N     �N     �N     �N     �N     �N     O     4O  &   JO     qO  !   �O     �O  #   �O     �O       P  4   !P  !   VP     xP      �P  *   �P     �P  !   Q     %Q  %   2Q     XQ     oQ     �Q     �Q     �Q     �Q  0   �Q     R     #R     7R     KR     fR  !   �R     �R  ,   �R     �R     S     S     /S     ES     ]S     mS     �S     �S     �S     �S     �S  '   �S  )   T  !   7T  !   YT     {T     �T     �T      �T      �T  !   �T      U     $U  "   +U  ,   NU     {U     �U     �U  !   �U     �U     �U     �U     V     )V     EV  !   aV  ,   �V     �V     �V     �V  5   �V  A   %W     gW     }W     �W     �W  #   �W  '   �W     X     
X  .   X     IX     XX     kX  &   yX  H   �X  $   �X  '   Y     6Y  e   VY  $   �Y  *   �Y  #   Z     0Z  %   GZ     mZ      �Z     �Z     �Z     �Z  #   �Z  '   #[  %   K[     q[     �[  6   �[     �[     �[     \     \      7\     X\  !   l\  "   �\  $   �\     �\  '   �\     ]  *   .]     Y]     s]  %   �]     �]  !   �]  
   �]     �]  .   �]     "^     )^  <   D^     �^  )   �^  ,   �^  /   �^     (_     E_  *   __  )   �_  (   �_  0   �_  /   `  )   >`  (   h`  :   �`  9   �`  J   a  D   Qa  C   �a  F   �a  E   !b  )   gb      �b  #   �b     �b     �b     	c     &c  $   :c     _c  4   {c  %   �c     �c  &   �c     d      1d     Rd     bd     d  3   �d  	   �d  	   �d  +   �d     e  &   &e  )   Me  (   we  *   �e  '   �e  )   �e  (   f  (   Ff  +   of  /   �f  .   �f  +   �f  9   &g  &   `g  #   �g     �g  "   �g     �g  -   h  ;   /h      kh  "   �h  (   �h     �h  .   �h  $   &i  /   Ki  "   {i  .   �i     �i  '   �i  #   j     6j     Tj  %   pj     �j  '   �j     �j  !   �j  !   k     9k  %   Xk  !   ~k     �k  "   �k     �k     �k     l     l      l  %   :l     `l  "   |l  #   �l  *   �l  1   �l  %    m  '   Fm  &   nm  %   �m     �m  !   �m  	   �m     �m     �m     n     #n  "   >n  ?   an  "   �n  2   �n     �n  #   o     2o     @o  9   Eo  >   o  $   �o     �o  1   �o  F   "p  7   ip  D   �p     �p     �p  '   q  %   Cq  /   iq  1   �q  -   �q  &   �q      r     <r     Sr     pr     �r     �r     �r  G   �r     s     s  !   1s     Ss     fs     zs     �s     �s  !   �s  $   �s     t     *t     It  #   Zt     ~t      �t  *   �t      �t     u     %u     >u  (   Zu     �u  4   �u  )   �u  #   �u  &   v     Ev  "   dv     �v  &   �v     �v  3   �v  $   w  ,   Aw     nw  )   �w     �w     �w     �w  /   �w  ;   +x  0   gx     �x     �x  #   �x  1   �x  )   y  	   Dy     Ny     ky     �y     �y     �y     �y     �y     �y     z  '   +z     Sz     jz     rz  !   �z  !   �z  $   �z  %   �z  #   {  !   5{     W{     t{     �{     �{  
   �{     �{     �{     �{      |     |     ;|     R|     j|     �|     �|     �|     �|     �|     }     &}     /}     6}     E}     X}     l}     z}  3   �}  -   �}  $   �}  <   ~     M~  2   d~  '   �~     �~     �~  8   �~     %  ,   E  0   r     �     �  0   �  *   �  +   ;�  '   g�  8   ��  .   Ȁ  -   ��  +   %�  )   Q�  3   {�  @   ��     ��     �  (   (�     Q�  !   q�  3   ��     ǂ  %   �  #   �     /�     L�  )   i�     ��  -   ��     ԃ     �     �  0   )�      Z�  #   {�  .   ��  /   ΄  6   ��  6   5�  *   l�     ��     ��  >   ̅     �  +   (�  '   T�  )   |�  -   ��     Ԇ     �  )   �  s   ;�     ��     ˇ     �  1   ��     0�     9�     H�     a�     s�     ��     ��  1   ��     ֈ  V   �     I�     \�  +   c�     ��  (   ��     ȉ  )   ۉ  /   �     5�     =�  &   \�  &   ��  &   ��     ъ     �     �     +�  8   J�     ��  W   ��     �     ��     �     &�     3�     J�     b�     z�  :   ��          Ќ     ܌     �  $   ��  &   !�     H�  5   h�     ��  '   ��  V   ٍ     0�     P�     j�  $   ��  %   ��  $   Ԏ     ��     �     +�  	   F�     P�     \�      k�      ��     ��     ͏  �  �     ��     ̑     �     ��     �     �      0�  3   Q�  2   ��  !   ��  -   ڒ  %   �  "   .�  #   Q�     u�     ��     ��     ��     ��  A   ��  !   �     #�     :�     N�  )   e�  9   ��  D   ɔ  <   �  >   K�     ��     ��     ��  /   Օ  '   �  3   -�     a�     i�     u�     ��     ��  I   і  /   �  )   K�  %   u�  $   ��     ��  %   ��  .   �  >   5�     t�  3   ��  &   ��  *   ޘ     	�  0   )�  8   Z�  -   ��  8   ��     ��     �  '   :�  )   b�  !   ��  !   ��  !   К  "   �  "   �  *   8�  a   c�  
   ś     Л     �     ��     �     �     4�     B�     U�     ]�     i�     x�     ��     ��     ��     ��     ˜     ͜  $   М  "   ��     �  C   *�  H   n�  F   ��  Y   ��  E   X�  C   ��  E   �  F   (�  V   o�  e   Ɵ  W   ,�  B   ��  Y   Ǡ  F   !�  T   h�  !   ��     ߡ     �  /   ��     )�     C�     L�     [�     b�     q�     ~�  +   ��     ��  3   ΢  4   �  #   7�  %   [�     ��     ��  9   ��  -   �  '   �     D�     ^�  '   o�  #   ��  #   ��     ߤ     ��     �  =   )�  I   g�     ��  $   ȥ     ��  ,   �  (   �     G�  9   a�  :   ��  (   ֦  ,   ��     ,�  =   L�  0   ��  "   ��     ާ  '   ��  +   #�  3   O�  '   ��  3   ��     ߨ  $   ��     $�     0�  K   D�      ��  (   ��     ک     ��  #   �  !   8�     Z�     v�  .   ��  #   ê     �  .   �  +   5�  +   a�     ��     ��  $   ��  #   ٫  '   ��  "   %�  5   H�     ~�  :   ��     ֬  4   ��  /   +�  2   [�  @   ��  %   ϭ  %   ��  ,   �  /   H�  &   x�  !   ��     ��  0   Ϯ      �      �  #   @�  $   d�     ��     ��  0   ��     �     �     �  -   �  %   F�  (   l�  8   ��  9   ΰ     �     �  !   (�     J�     f�     ��     ��     ��     ɱ     ϱ  )   �  &   
�  2   1�  4   d�  -   ��  +   ǲ     �     �     #�  "   ;�  "   ^�  '   ��     ��     ��      ��  5   ٳ  "   �     2�     I�  "   a�     ��     ��  &   ��  (   Ĵ      ��  $   �  "   3�  3   V�     ��  $   ��     ʵ  <   �  ;    �     \�     r�     ��     ��  '   ��  '   ۶     �     �  7   %�     ]�     j�     }�  4   ��  T   ��  /   �  2   F�  ,   y�  ~   ��  3   %�  7   Y�  %   ��  "   ��  9   ڹ     �  2   4�  )   g�  -   ��     ��  1   ݺ  =   �  9   M�  #   ��  ,   ��  >   ػ     �     ,�     ?�      Y�  %   z�     ��  %   ��  -   ޼  /   �  "   <�  -   _�     ��  3   ��      ܽ     ��  .   �     K�  %   ]�     ��     ��  8   ��     ;     վ  Q   �     E�  4   b�  7   ��  5   Ͽ  %   �  !   +�  (   M�  *   v�  *   ��  .   ��  .   ��  *   *�  *   U�  H   ��  I   ��  V   �  S   j�  T   ��  K   �  L   _�  0   ��  0   ��  2   �  "   A�     d�  $   �     ��  )   ��  (   ��  O   �  :   b�  )   ��  3   ��     ��     �     .�  "   I�     l�  9   ��     ��     ��  2   ��     �  .   0�  1   _�  2   ��  4   ��  1   ��  3   +�  (   _�  (   ��  '   ��  +   ��  *   �  '   0�  )   X�  4   ��  !   ��     ��  '   ��     �  )   ?�  J   i�  -   ��  -   ��  A   �  *   R�  I   }�  ,   ��  =   ��  %   2�  0   X�  .   ��  9   ��  +   ��  (   �  +   G�  6   s�  2   ��  <   ��  /   �  )   J�  -   t�  $   ��  -   ��  $   ��  '   �  6   B�  )   y�     ��     ��     ��     ��  3   ��      �  $   @�  5   e�  6   ��  A   ��  3   �  3   H�  2   |�  1   ��     ��  2   ��     �     &�  *   B�     m�  )   |�  1   ��  I   ��  3   "�  9   V�  +   ��  -   ��     ��     �  H   	�  N   R�  %   ��     ��  8   ��  O   �  ?   ^�  K   ��     ��  (   �      ,�  )   M�  ;   w�  1   ��  2   ��  0   �     I�     e�  5   |�  %   ��     ��     ��     �  e   �     {�     ��  '   ��     ��     ��  #   �     %�  #   B�  $   f�  '   ��     ��     ��     ��  +   ��  #   �  )   B�  3   l�  0   ��  %   ��      ��  #   �  H   <�     ��  @   ��  3   ��  -   �  0   B�  (   s�  *   ��  %   ��  *   ��  !   �  9   :�  %   t�  9   ��     ��  ,   ��     �  %   5�     [�  1   z�  @   ��  4   ��     "�     <�  $   R�  H   w�  3   ��  
   ��  #   ��  #   #�     G�  -   g�     ��     ��     ��  $   ��     �  2   +�     ^�     y�     �      ��  %   ��  *   ��  '   �  %   )�  %   O�     u�      ��     ��     ��     ��     ��  #   ��  )   !�  4   K�  :   ��  !   ��  "   ��  "    �  &   #�     J�     `�     y�  8   ��  6   ��     �     �     �     $�     7�     G�     Y�  K   l�  2   ��  -   ��  ?   �  "   Y�  R   |�  ?   ��     �     '�  ;   C�  $   �  3   ��  6   ��  #   �      3�  9   T�  5   ��  '   ��  +   ��  ?   �  5   X�  4   ��  2   ��  2   ��  C   )�  S   m�  !   ��  9   ��  .   �  %   L�  )   r�  D   ��  '   ��  0   	�     :�  !   U�     w�  E   ��     ��  )   ��  *   �  '   G�     o�  4   ��  /   ��  9   ��  4   #�  7   X�  6   ��  3   ��  /   ��     +�  7   J�  J   ��  '   ��  8   ��  5   .�  4   d�  P   ��  +   ��  &   �  3   =�  �   q�  -   �  %   1�  #   W�  D   {�     ��     ��  #   ��     ��     �     #�     7�  :   R�     ��  w   ��     %�     B�  ,   K�     x�  +   ��     ��  ,   ��  2   �     4�  .   B�  '   q�  -   ��  .   ��     ��  !   �  !   3�  !   U�  A   w�     ��  w   ��     =�      \�     }�     ��     ��     ��     ��     ��  E   �     H�     \�     l�     ��  -   ��  -   ��  %   ��  A   �     V�  /   m�  d   ��  2   �  !   5�  !   W�  +   y�  -   ��  .   ��     �      �      ?�  	   `�     j�     x�  !   ��  !   ��      ��     ��     �      7   �  T      �  �   <  C   +  �   0             x     1   �   �   J           �     �   �  Q  �         :  y  �  ^  �   �  E  W   �       x      )  �  r  �    *  �   �   *  �  e  �   !  �   �     �         >   �  �      �  U      �   �   �   �       g   o          �              �  )     `          �  �      D  	   l  #   �  �  	  �      �      �       g  �   �  p       -   �      �  q   }  �      �                �   �               L   �  �   �   �           �  �   �               �  �  �   �  �   0  �       P  �   X  �   u                T   �   ]  �          �  �           �      Y  �           p  ;  �      �              )  \   f      �      h  �      ?      _   &           !  '             7  j           �       �   V       �   F  4       �       �   �        2  �   F      (       �        v   w           �  �   &  u       �  H          �      F   �   
   {   ,  �  �   �   \  �      4  �  9     �  �   �  :                       �      f  Z  a  r   G  	  �   �   �      �  5  B  �   W      _        �   0  t   �     �          �  �  �       l  T  �  �      j  A      ~   k  �  `   �   R  �      X  A   �        �  R  �         %    r    �  o               �   *   ^  �      �      2  �  �             %       �  �      z  e  G   �  O       d          ~        �  �      �      �   A  m  �  \  �       K  3      �  /         $  y       x   �     �  �  B   h  �      M  m  Q  5   �      �  %    t  Z     w  �   :      P   �   C      k   �  �  �  |  ;  a          �  �   6  N     �  �   D      9   <  �   �           "   =      v  +      �  �  �       �  [      v      �   #  '   z   �   �  �  I  �    �  $    �  �      �               �  ,       �      �  �  (  e      �  [   6           8               t  �  �     �   �      H      q  �   ?   I   ~  !   g  $       |   �   �  9      �     "  �   �   �  ;       l   �       Z   �           H            4  {      /  D  �  �                 �      i  �       �  b              �         �     =   O  �    b  w        �  �  �  �  V  �  K   �  a   ,  �   B      �   |    �          J    U   (  �      �      o  �   <   �  �  K  �  �           M   +   2   �  �           �     c       5          >  �          �  s   W            �  -      �  @  �  �       �  N  &  n  �           �  ?  �   �          -  Y  �         �              h   ^      �   8   i   �   �  /  m                         X   �  s        �  �           �                  �     n   �       V      �  y  
  M          .   �  �   �  �   �       �  S   �  Q   f   k        @     O  L  7      �   z  p  �       �          �  �  L        S  i     �           �  C             �                         >      �       I  �      �   �  �   �     j  .  "  �          �   �  P  d     G  s  {  �  c      }  �  J  �   �       �          S      �  .  E   �       �    �   �   �  �   �  �      �   �   `  U  �  �  6        8  =      �  �      �  �  �                  �        1      �   1      '      Y   �  �   �  �  �   n          �      �   ]  #      �  [        �   
  �   R   �                 3   q  3    b  �  u  @       d  }   E          �          �  c  �  �  �      �  �  �    �  �   �  ]   �      _    N             �  d9  x9  �9  �9  �            ����B�            ����|�  &          ������  &          ���� 

RPM build errors:
  (MISSING KEYS:  (UNTRUSTED KEYS:  failed -   on file  ! only on numbers
 "%s" specifies multiple packages:
 %%changelog entries must start with *
 %%changelog not in descending chronological order
 %%dev glob not permitted: %s
 %%patch without corresponding "Patch:" tag
 %%semodule requires a file path
 %%{buildroot} can not be "/"
 %%{buildroot} couldn't be empty
 %3d<%*s(empty)
 %3d>%*s(empty) %a %b %d %Y %c %s %d defined multiple times
 %s already contains identical signature, skipping
 %s cannot be installed
 %s conflicts with %s%s %s created as %s
 %s failed: %x
 %s field must be present in package: %s
 %s has invalid numeric value, skipped
 %s has too large or too small integer value, skipped
 %s has too large or too small long value, skipped
 %s is a Delta RPM and cannot be directly installed
 %s is needed by %s%s %s is obsoleted by %s%s %s saved as %s
 %s scriptlet failed, exit status %d
 %s scriptlet failed, signal %d
 %s scriptlet failed, waitpid(%d) rc %d: %s
 %s: %s
 %s: %s: %s
 %s: Fflush failed: %s
 %s: Fread failed: %s
 %s: Fwrite failed: %s
 %s: Immutable header region could not be read. Corrupted package?
 %s: can't load unknown tag (%d).
 %s: cannot read header at 0x%x
 %s: chroot directory not set
 %s: failed to encode
 %s: headerRead failed: %s
 %s: import read failed(%d).
 %s: key %d import failed.
 %s: key %d not an armored public key.
 %s: line: %s
 %s: not an armored public key.
 %s: not an rpm package (or package manifest)
 %s: not an rpm package (or package manifest): %s
 %s: open failed: %s
 %s: option table misconfigured (%d)
 %s: public key read failed.
 %s: read manifest failed: %s
 %s: reading of public key failed.
 %s: regcomp failed: %s
 %s: regexec failed: %s
 %s: rpmReadSignature failed: %s %s: rpmWriteSignature failed: %s
 %s: writeLead failed: %s
 %s:%d: Argument expected for %s
 %s:%d: Got a %%else with no %%if
 %s:%d: Got a %%endif with no %%if
 %s:%d: bad %%if condition
 && and || not suported for strings
 '%s' type given with other types in %%semodule %s. Compacting types to '%s'.
 'EXPR' 'MACRO EXPR' (contains no files)
 (installed)  (invalid type) (invalid xml type) (no error) (no state)     (none) (not a blob) (not a number) (not a string) (not an OpenPGP signature) (not base64) (unknown %3d)  (unknown) ) )  * / not suported for strings
 - not suported for strings
 - only on numbers
 --allfiles may only be specified during package installation --allmatches may only be specified during package erasure --excludedocs may only be specified during package installation --hash (-h) may only be specified during package installation and erasure --ignorearch may only be specified during package installation --ignoreos may only be specified during package installation --ignoresize may only be specified during package installation --includedocs may only be specified during package installation --justdb may only be specified during package installation and erasure --nodeps may only be specified during package installation, erasure, and verification --percent may only be specified during package installation and erasure --prefix may only be used when installing new packages --relocate and --excludepath may only be used when installing new packages --replacepkgs may only be specified during package installation --test may only be specified during package installation and erasure : expected following ? subexpression <FILE:...> <dir> <lua> scriptlet support not built in
 <old>=<new> <package>+ <packagefile>+ <path> <source package> <specfile> <tarball> ======================== active %d empty %d
 ? expected in expression A %% is followed by an unparseable macro
 Arch dependent binaries in noarch package
 Architecture is excluded: %s
 Architecture is not included: %s
 Archive file not in header Bad CSA data
 Bad arch/os number: %s (%s:%d)
 Bad dirmode spec: %s(%s)
 Bad exit status from %s (%s)
 Bad file: %s: %s
 Bad magic Bad mode spec: %s(%s)
 Bad owner/group: %s
 Bad package specification: %s
 Bad source: %s: %s
 Bad syntax: %s(%s)
 Bad/unreadable  header Base modules '%s' and '%s' have overlapping types
 Build options with [ <specfile> | <tarball> | <source package> ]: Building for target %s
 Building target platforms: %s
 CMD Cannot sign RPM v3 packages
 Checking for unpackaged file(s): %s
 Cleaning up / removing...
 Common options for all rpm modes and executables: Conversion of %s to long integer failed.
 Copyright (C) 1998-2002 - Red Hat, Inc.
 Could not canonicalize hostname: %s
 Could not exec %s: %s
 Could not generate output filename for package %s: %s
 Could not open %%files file %s: %m
 Could not open %s file: %s
 Could not open %s: %s
 Couldn't create pipe for %s: %m
 Couldn't create pipe for signing: %m Couldn't create temporary file for %s: %s
 Couldn't download %s
 Couldn't duplicate file descriptor: %s: %s
 Couldn't exec %s: %s
 Couldn't fork %s: %s
 DIRECTORY Database options: Dependency tokens must begin with alpha-numeric, '_' or '/' Digest mismatch Directory not found by glob: %s
 Directory not found: %s
 Downloading %s to %s
 Duplicate %s entries in package: %s
 Duplicate locale %s in %%lang(%s)
 Empty file classifier
 Enter pass phrase:  Error executing scriptlet %s (%s)
 Error parsing %%setup: %s
 Error parsing %s: %s
 Error parsing tag field: %s
 Error reading %%files file %s: %m
 Exec of %s failed (%s): %s
 Executing "%s":
 Executing(%s): %s
 Execution of "%s" failed.
 Expecting %%semodule tag: %s
 Failed build dependencies:
 Failed dependencies:
 Failed to determine a policy name: %s
 Failed to dlopen %s %s
 Failed to encode policy file: %s
 Failed to find %s:
 Failed to get policies from header
 Failed to open tar pipe: %m
 Failed to read  policy file: %s
 Failed to read auxiliary vector, /proc not mounted?
 Failed to read spec file from %s
 Failed to rename %s to %s: %m
 Failed to resolve symbol %s: %s
 File %s does not appear to be a specfile.
 File %s is not a regular file.
 File %s is smaller than %u bytes
 File %s: %s
 File capability support not built in
 File listed twice: %s
 File must begin with "/": %s
 File needs leading "/": %s
 File not found by glob: %s
 File not found: %s
 Finding  %s: %s
 Generating %d missing index(es), please wait...
 Header  Header SHA1 digest: Header size too big Ignoring invalid regex %s
 Incomplete data line at %s:%d
 Incomplete default line at %s:%d
 Install/Upgrade/Erase options: Installed (but unpackaged) file(s) found:
%s Installing %s
 Internal error Internal error: Bogus tag %d
 Invalid %s token: %s
 Invalid capability: %s
 Invalid date %u Invalid patch number %s: %s
 Keyring options: MACRO MD5 digest: Macro %%%s failed to expand
 Macro %%%s has empty body
 Macro %%%s has illegal name (%%define)
 Macro %%%s has illegal name (%%undefine)
 Macro %%%s has unterminated body
 Macro %%%s has unterminated opts
 Missing %s in %s %s
 Missing '(' in %s %s
 Missing ')' in %s(%s
 Missing module path in line: %s
 Missing rpmlib features for %s:
 More than one file on a line: %s
 NO  NOT OK No "Source:" tag in the spec file
 No compatible architectures found for build
 No file attributes configured
 No patch number %u
 No source number %u
 Non-white space follows %s(): %s
 Not a directory: %s
 OK OS is excluded: %s
 OS is not included: %s
 Package already exists: %s
 Package check "%s" failed.
 Package has no %%description: %s
 Pass phrase check failed or gpg key expired
 Pass phrase is good.
 Please contact %s
 Plugin %s not loaded
 Policy module '%s' duplicated with overlapping types
 PreReq:, Provides:, and Obsoletes: dependencies support versions. Preparing packages... Preparing... Processing files: %s
 Processing policies: %s
 Query options (with -q or --query): Query/Verify package selection options: ROOT RPM version %s
 Recognition of file "%s" failed: mode %06o %s
 Retrieving %s
 Signature options: Spec options: Symlink points to BuildRoot: %s -> %s
 This program may be freely redistributed under the terms of the GNU GPL
 Too many args in data line at %s:%d
 Too many args in default line at %s:%d
 Too many arguments in line: %s
 Too many levels of recursion in macro expansion. It is likely caused by recursive macro declaration.
 Unable to change root directory: %m
 Unable to create immutable header region.
 Unable to open %s for reading: %m.
 Unable to open %s: %s
 Unable to open current directory: %m
 Unable to open icon %s: %s
 Unable to open spec file %s: %s
 Unable to open stream: %s
 Unable to open temp file: %s
 Unable to read icon %s: %s
 Unable to reload signature header.
 Unable to restore current directory: %m Unable to restore root directory: %m
 Unable to write package: %s
 Unable to write temp header
 Unknown file digest algorithm %u, falling back to MD5
 Unknown file type Unknown format Unknown icon type: %s
 Unknown option %c in %s(%s)
 Unknown payload compression: %s
 Unknown system: %s
 Unsatisfied dependencies for %s:
 Unsupported PGP hash algorithm %u
 Unsupported PGP pubkey algorithm %u
 Unsupported PGP signature
 Unsupported payload (%s) in package %s
 Unterminated %c: %s
 Unusual locale length: "%s" in %%lang(%s)
 Updating / installing...
 V%d %s/%s %s, key ID %s Verify options (with -V or --verify): Version required Versioned file name not permitted Wrote: %s
 YES You must set "%%_gpg_name" in your macro file
 [none] ] expected at end of array a hardlink file set may be installed without being complete. argument is not an RPM package
 arguments to --prefix must begin with a / arguments to --root (-r) must begin with a / array iterator used with different sized arrays bad date in %%changelog: %s
 bad option '%s' at %s:%d
 build binary package from <source package> build binary package only from <specfile> build binary package only from <tarball> build source and binary packages from <specfile> build source and binary packages from <tarball> build source package only from <specfile> build source package only from <tarball> build through %build (%prep, then compile) from <specfile> build through %build (%prep, then compile) from <tarball> build through %install (%prep, %build, then install) from <source package> build through %install (%prep, %build, then install) from <specfile> build through %install (%prep, %build, then install) from <tarball> build through %prep (unpack sources and apply patches) from <specfile> build through %prep (unpack sources and apply patches) from <tarball> buildroot already specified, ignoring %s
 can't create %s lock on %s (%s)
 cannot add record originally at %u
 cannot create %s: %s
 cannot get %s lock on %s/%s
 cannot open %s at %s:%d: %m
 cannot open %s: %s
 cannot open Packages database in %s
 cannot re-open payload: %s
 cannot use --prefix with --relocate or --excludepath create archive failed on file %s: %s
 create archive failed: %s
 creating a pipe for --pipe failed: %m
 debug file state machine debug payload file state machine debug rpmio I/O define MACRO with value EXPR delete package signatures dependency comparison supports versions with tilde. different directory display final rpmrc and macro configuration display known query tags display the states of the listed files do not accept i18N msgstr's from specfile do not execute %%post scriptlet (if any) do not execute %%postun scriptlet (if any) do not execute %%pre scriptlet (if any) do not execute %%preun scriptlet (if any) do not execute %check stage of the build do not execute %clean stage of the build do not execute any %%triggerin scriptlet(s) do not execute any %%triggerpostun scriptlet(s) do not execute any %%triggerprein scriptlet(s) do not execute any %%triggerun scriptlet(s) do not execute any scriptlet(s) triggered by this package do not execute any stages of the build do not execute package scriptlet(s) do not glob arguments do not install configuration files do not install documentation do not process non-package files as manifests do not reorder package installation to satisfy dependencies do not verify build dependencies do not verify package dependencies don't check disk space before installing don't execute verify script(s) don't import, but tell if it would work or not don't install file security contexts don't install, but tell if it would work or not don't verify capabilities of files don't verify database header(s) when retrieved don't verify digest of files don't verify digest of files (obsolete) don't verify file security contexts don't verify files in package don't verify group of files don't verify header+payload signature don't verify mode of files don't verify modification time of files don't verify owner of files don't verify package architecture don't verify package dependencies don't verify package digest(s) don't verify package operating system don't verify package signature(s) don't verify size of files don't verify symlink path of files dump basic file information empty tag format empty tag name erase erase (uninstall) package error creating temporary file %s: %m
 error reading from file %s
 error reading header from package
 error(%d) adding header #%d record
 error(%d) allocating new package instance
 error(%d) getting "%s" records from %s index: %s
 error(%d) removing header #%d record
 error(%d) removing record "%s" from %s
 error(%d) storing record "%s" into %s
 error(%d) storing record #%d into %s
 error:  exclude paths must begin with a / exclusive exec failed
 extra '(' in package label: %s
 failed failed to create directory failed to create directory %s: %s
 failed to rebuild database: original database remains in place
 failed to remove directory %s: %s
 failed to replace old database with new database!
 failed to stat %s: %m
 failed to write all data to %s: %s
 fatal error:  file file %s conflicts between attempted installs of %s and %s file %s from install of %s conflicts with file from package %s file %s is not owned by any package
 file %s: %s
 file digest algorithm is per package configurable file name(s) stored as (dirName,baseName,dirIndex) tuple, not as path. files may only be relocated during package installation generate package header(s) compatible with (legacy) rpm v3 packaging gpg exec failed (%d)
 gpg failed to write signature
 group %s does not contain any packages
 group %s does not exist - using root
 header #%u in the database is bad -- skipping.
 header tags are always sorted after being loaded. ignore ExcludeArch: directives from spec file ignore file conflicts between packages illegal _docdir_fmt %s: %s
 illegal signature type import an armored public key incomplete %%changelog entry
 incorrect format: %s
 initialize database install install all files, even configurations which might otherwise be skipped install documentation install package(s) internal support for lua scripts. invalid dependency invalid field width invalid index type %x on %s/%s
 invalid package number: %s
 invalid syntax in lua file: %s
 invalid syntax in lua script: %s
 invalid syntax in lua scriptlet: %s
 line %d: %s
 line %d: %s is deprecated: %s
 line %d: %s: %s
 line %d: Bad %%setup option %s: %s
 line %d: Bad %s number: %s
 line %d: Bad %s: qualifiers: %s
 line %d: Bad BuildArchitecture format: %s
 line %d: Bad arg to %%setup: %s
 line %d: Bad no%s number: %u
 line %d: Bad number: %s
 line %d: Bad option %s: %s
 line %d: Docdir must begin with '/': %s
 line %d: Empty tag: %s
 line %d: Epoch field must be an unsigned number: %s
 line %d: Error parsing %%description: %s
 line %d: Error parsing %%files: %s
 line %d: Error parsing %%policies: %s
 line %d: Error parsing %s: %s
 line %d: Illegal char '%c' in: %s
 line %d: Illegal char in: %s
 line %d: Illegal sequence ".." in: %s
 line %d: Malformed tag: %s
 line %d: Only noarch subpackages are supported: %s
 line %d: Package does not exist: %s
 line %d: Prefixes must not end with "/": %s
 line %d: Second %s
 line %d: Tag takes single token only: %s
 line %d: Too many names: %s
 line %d: Unclosed %%if
 line %d: Unknown tag: %s
 line %d: internal script must end with '>': %s
 line %d: interpreter arguments not allowed in triggers: %s
 line %d: script program must begin with '/': %s
 line %d: second %%prep
 line %d: second %s
 line %d: triggers must have --: %s
 line %d: unclosed macro or bad line continuation
 line %d: unsupported internal script: %s
 line: %s
 list all configuration files list all documentation files list files in package list keys from RPM keyring lua hook failed: %s
 lua script failed: %s
 magic_load failed: %s
 magic_open(0x%x) failed: %s
 malformed %s: %s
 memory alloc (%u bytes) returned NULL.
 miFreeHeader: skipping missing missing   %c %s missing '(' in package label: %s
 missing ')' in package label: %s
 missing ':' (found 0x%02x) at %s:%d
 missing architecture for %s at %s:%d
 missing architecture name at %s:%d
 missing argument for %s at %s:%d
 missing name in %%changelog
 missing second ':' at %s:%d
 missing { after % missing } after %{ net shared net shared     no arguments given no arguments given for parse no arguments given for query no arguments given for verify no dbpath has been set no dbpath has been set
 no description in %%changelog
 no package matches %s: %s
 no package provides %s
 no package requires %s
 no package triggers %s
 no packages given for erase no packages given for install no state normal normal         not an rpm package not an rpm package
 not installed not installed  one type of query/verify may be performed at a time only installation and upgrading may be forced only one major mode may be specified only one of --excludedocs and --includedocs may be specified open of %s failed: %s
 operate on binary rpms generated by spec (default) operate on source rpm generated by spec override build root override target platform package %s (which is newer than %s) is already installed package %s is already installed package %s is intended for a %s architecture package %s is intended for a %s operating system package %s is not installed
 package %s is not relocatable
 package %s was already added, replacing with %s
 package %s was already added, skipping %s
 package has neither file owner or id lists
 package has not file owner/group lists
 package name-version-release is not implicitly provided. package payload can be compressed using bzip2. package payload can be compressed using lzma. package payload can be compressed using xz. package payload file(s) have "./" prefix. package scriptlets can be expanded at install time. package scriptlets may access the rpm database while installing. parse error in expression
 parse spec file(s) to stdout path %s in package %s is not relocatable predefine MACRO with value EXPR print dependency loops as warning print hash marks as package installs (good with -v) print macro expansion of EXPR print percentages as package installs print the version of rpm being used provide less detailed output provide more detailed output query of specfile %s failed, can't parse
 query spec file(s) query the package(s) triggered by the package query/verify a header instance query/verify a package file query/verify all packages query/verify package(s) from install transaction query/verify package(s) in group query/verify package(s) owning file query/verify package(s) with header identifier query/verify package(s) with package identifier query/verify the package(s) which provide a dependency query/verify the package(s) which require a dependency read <FILE:...> instead of default file(s) read failed: %s (%d)
 reading symlink %s failed: %s
 rebuild database inverted lists from installed package headers record %u could not be read
 reinstall if the package is already present relocate files from path <old> to <new> relocate files in non-relocatable package relocate the package to <dir>, if relocatable relocations must begin with a / relocations must contain a = relocations must have a / following the = remove all packages which match <package> (normally an error is generated if <package> specified multiple packages) remove build tree when done remove sources when done remove specfile when done replace files in %s with files from %s to recover replaced replaced       replacing %s failed: %s
 rpm checksig mode rpm query mode rpm verify mode rpmMkTemp failed
 rpmdb: damaged header #%u retrieved -- skipping.
 rpmdbNextIterator: skipping script disabling options may only be specified during package installation and erasure send stdout to CMD shared short hand for --replacepkgs --replacefiles sign package(s) sign package(s) (identical to --addsign) skip %%ghost files skip files with leading component <path>  skip straight to specified stage (only for c,i) skipped skipping %s - transfer failed
 source package contains no .spec file
 source package expected, binary found
 support for POSIX.1e file capabilities syntax error in expression
 syntax error while parsing &&
 syntax error while parsing ==
 syntax error while parsing ||
 the scriptlet interpreter can use arguments from header. transaction trigger disabling options may only be specified during package installation and erasure types must match
 unable to read the signature
 undefine MACRO unexpected ] unexpected query flags unexpected query format unexpected query source unexpected } unknown error %d encountered while manipulating package %s unknown state unknown tag unknown tag: "%s"
 unmatched (
 unpacking of archive failed%s%s: %s
 unrecognized db option: "%s" ignored.
 unsupported RPM package version update the database, but do not modify the filesystem upgrade package(s) upgrade package(s) if already installed upgrade to an old version of the package (--force on upgrades does this automatically) use ROOT as top level directory use database in DIRECTORY use the following query format user %s does not exist - using root
 verify %files section from <specfile> verify %files section from <tarball> verify database files verify package signature(s) waiting for %s lock on %s
 warning:  wrong color wrong color    { expected after : in expression { expected after ? in expression | expected at end of expression } expected in expression Project-Id-Version: RPM
Report-Msgid-Bugs-To: rpm-maint@lists.rpm.org
POT-Creation-Date: 2014-09-18 14:01+0300
PO-Revision-Date: 2013-12-18 05:49+0000
Last-Translator: Roman Spirgi <Unknown>
Language-Team: German (http://www.transifex.com/projects/p/rpm/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:05+0000
X-Generator: Launchpad (build 18115)
Language: de
 

Fehler beim Bauen des RPM:
  (FEHLENDE SCHLÜSSEL:  (UNBESTÄTIGTE SCHLÜSSEL:  fehlgeschlagen -   bei Datei  ! nur bei Zahlen
 "%s" bezeichnet mehrere Pakete:
 %%changelog-Einträge müssen mit einem * beginnen
 %%changelog ist nicht in absteigender Reihenfolge
 %%dev-Klecks nicht gestattet: %s
 %%patch ohne korrespondierenden "Patch:"-Tag
 %%semodule benötigt einen Dateipfad
 %%{buildroot} kann nicht "/" sein
 %%{buildroot} kann nicht leer sein
 %3d<%*s(leer)
 %3d>%*s(leer) %a %b %d %Y %c %s %d ist mehrfach definiert
 %s enthält bereits eine identische Signatur, wird übersprungen
 %s kann nicht installiert werden
 %s kollidiert mit %s%s %s erstellt als %s
 %s fehlgeschlagen: %x
 %s-Feld muss im Paket vorhanden sein: %s
 %s hat einen ungültigen numerischen Wert, übersprungen
 %s hat einen zu grossen oder zu kleinen Integer-Wert, übersprungen
 %s hat einen zu grossen oder zu kleinen Wert, übersprungen
 %s ist ein Delta RPM und kann nicht direkt installiert werden
 %s wird benötigt von %s%s %s wird ersetzt durch %s%s %s als %s gesichert
 %s Scriptlet fehlgeschlagen, Beenden-Status %d
 %s Scriptlet fehlgeschlagen, Signal %d
 %s Scriptlet fehlgeschlagen, waitpid(%d) rc %d: %s
 %s: %s
 %s: %s: %s
 %s: Fflush fehlgeschlagen: %s
 %s: Fread fehlgeschlagen: %s
 %s: Fwrite fehlgeschlagen: %s
 %s: Immuner Header-Bereich konnte nicht gelesen werden. Korruptes Paket?
 %s: Kann den unbekannten Tag (%d) nicht laden.
 %s: Kann den Header bei 0x%x nicht lesen
 %s: chroot-Verzeichnis nicht gesetzt
 %s: Verschlüsselung fehlgeschlagen
 %s: headerRead gescheitert: %s
 %s: Importieren fehlgeschlagen (%d).
 %s: Import des Schlüssels %d fehlgeschlagen.
 %s: Schlüssel %d nicht gesicherter öffentlicher Schlüssel.
 %s: Zeile: %s
 %s: Ist kein gepanzerter öffentlicher Schlüssel.
 %s: Kein RPM-Paket (oder Paket-Liste)
 %s: Kein RPM-Paket (oder Paket-Liste): %s
 %s: Öffnen fehlgeschlagen: %s
 %s: Optionstabelle ist falsch konfiguriert (%d)
 %s: Lesen des öffentlichen Schlüssels fehlgeschlagen.
 %s: Lesen der Paket-Liste fehlgeschlagen: %s
 %s: Lesen des öffentlichen Schlüssels fehlgeschlagen.
 %s: regcomp fehlgeschlagen: %s
 %s: regexec fehlgeschlagen: %s
 %s: rpmReadSignature fehlgeschlagen: %s %s: rpmWriteSignature fehlgeschlagen: %s
 %s: writeLead fehlgeschlagen: %s
 %s:%d: Argument für %s erwartet
 %s:%d: %%else ohne %%if erhalten
 %s:%d: %%endif ohne %%if erhalten
 %s:%d: Fehlerhafte %%if-Bedingung
 && und || nicht unterstützt für Strings
 »%s«-Typ mit anderen Typen im %%semodule %s angegeben. Typen werden zu »%s« zusammengefasst.
 'AUSDRUCK' »MAKRO AUSDRUCK« (enthält keine Dateien)
 (installiert)  (ungültiger Typ) (ungültiger XML-Typ) (kein Fehler) (kein Zustand)     (keine) (kein Blob) (keine Nummer) (kein String) (keine OpenPGP-Signatur) (nicht Base64) (unbekannt %3d)  (unbekannt) ) )  * / nicht unterstützt für Strings
 - nicht unterstützt für Strings
 - nur bei Zahlen
 --allfiles darf nur während der Paketinstallation angegeben werden --allmatches darf nur während der Paket-Deinstallation angegeben werden --excludedocs darf nur während der Paketinstallation angegeben werden --hash (-h) kann nur während der Paketinstallation oder -deinstallation angegeben werden --ignorearch darf nur während der Paketinstallation angegeben werden --ignoreos darf nur während der Paketinstallation angegeben werden --ignoresize darf nur während der Paketinstallation angegeben werden --includedocs darf nur während der Paketinstallation angegeben werden --justdb darf nur während der Paketinstallation oder -deinstallation angegeben werden --nodeps kann nur während der Paketinstallation, -deinstallation und -überprüfung angegeben werden --percent kann nur während der Paketinstallation oder -deinstallation angegeben werden --prefix darf nur bei der Installation neuer Pakete benutzt werden --relocate und --excludepath dürfen nur bei der Installation neuer Pakete benutzt werden --replacepkgs darf nur während der Paketinstallation angegeben werden --test kann nur während der Paketinstallation oder -deinstallation angegeben werden : erwartet ein ? im Unterausdruck <DATEI:...> <Verzeichnis> <lua>-Scriptlet-Unterstützung nicht eingebaut
 <alter Pfad>=<neuer Pfad> <Paket>+ <Paket-Datei>+ <Pfad> <Source-Paket> <Spec-Datei> <Tar-Archiv> ======================== aktiv %d  leer %d
 ? im Ausdruck erwartet Ein %% ist gefolgt von einem nicht parsbaren Makro
 Architekturabhängige Binärdateien in noarch-Paket
 Architektur ist ausgeschlossen: %s
 Architektur ist nicht einbezogen: %s
 Archiv-Datei nicht im Header Ungültige CSA-Daten
 Ungültige Architektur/Betriebssystem-Nummer: %s (%s:%d)
 Ungültige Verzeichnisberechtigungen: %s(%s)
 Fehler-Status beim Beenden von %s (%s)
 Ungültige Datei: %s: %s
 Ungültige Magic Ungültige Dateiberechtigungen: %s(%s)
 Ungültiger Eigentümer/Gruppe: %s
 Ungültige Paket-Spezifikation: %s
 Ungültige Quelle: %s: %s
 Ungültiger Syntax: %s(%s)
 Ungültiger Header Basismodule »%s« und »%s« beinhalten überlappende Typen
 Erstellungsoptionen mit [ <Spec-Datei> | <Tar-Archiv> | <Source-Paket> ]: Baue für das Ziel %s
 Baue für die Zielplattform(en): %s
 CMD RPM-v3-Pakete können nicht signiert werden
 Prüfe auf nicht gepackte Datei(en): %s
 Aufräumen/ Entfernen...
 Gemeinsame Optionen für alle RPM-Modi und Ausführungen: Umwandlung von %s in einen langen Integer fehlgeschlagen.
 Copyright (C) 1998-2002 - Red Hat, Inc.
 Rechnername konnte nicht erkannt werden: %s
 Konnte %s nicht ausführen: %s
 Konnte den Dateinamen für das Paket %s nicht generieren: %s
 Datei %s aus %%files konnte nicht geöffnet: %m
 Konnte %s-Datei nicht öffnen: %s
 Konnte %s nicht öffnen: %s
 Konnte keine Pipe für %s erzeugen: %m
 Konnte kein Pipe zum Signieren erzeugen: %m Konnte keine temporäre Datei erzeugen für %s: %s
 %s konnte nicht heruntergeladen werden
 Konnte Datei-Beschreiber nicht duplizieren: %s: %s
 Konnte %s nicht ausführen: %s
 Konnte fork %s nicht ausführen: %s
 VERZEICHNIS Datenbank-Optionen: Token mit Abhängigkeiten müssen alphanumerisch, mit '_' oder '/' beginnen Prüfsumme stimmt nicht überein Verzeichnis von Glob nicht gefunden: %s
 Verzeichnis nicht gefunden: %s
 %s herunterladen nach %s
 Doppelte %s-Einträge im Paket: %s
 Locale-Duplikat %s in %%lang(%s)
 Leerer Dateiklassifizierer
 Bitte das Passwort eingeben:  Fehler beim Ausführen des Scriptlets %s (%s)
 Fehler beim Parsen von %%setup: %s
 Fehler beim Parsen von %s: %s
 Fehler bei der Analyse eines Markerfeldes: %s
 Fehler beim Lesen der %%files Datei %s: %m
 Ausführung von %s fehlgeschlagen (%s): %s
 Ausschluss "%s":
 Ausführung(%s): %s
 Ausschluss von "%s" fehlgeschlagen.
 Erwarte %%semodule Kennzeichen: %s
 Fehlgeschlagene Paket-Abhängigkeiten:
 Fehlgeschlagende Abhängigkeiten:
 Bestimmung des Richtlinien-Namens fehlgeschlagen: %s
 Dlopen %s %s fehlgeschlagen
 Verschlüsselung der Richtlinien-Datei fehlgeschlagen: %s
 Fehlgeschlagenes zu finden %s:
 Richtlinien konnten nicht aus Header gelesen werden
 Die Tar-Pipe konnte nicht geöffnet werden: %m
 Lesevorgang fehlgeschlagen  Richtlinien-Datei: %s
 Hilfsvektor konnte nicht gelesen werden, ist /proc eingehängt?
 Konnte Spec-Datei von %s nicht lesen
 Konnte %s nicht in %s umbenennen: %m
 Auflösen des Symbols fehlgeschlagen %s: %s
 Die Datei %s scheint keine Spec-Datei zu sein.
 Die Datei %s ist keine normale Datei.
 Datei %s ist kleiner als %u Byte
 Datei %s: %s
 Dateifähigkeits-Unterstützung nicht eingebaut
 Datei doppelt aufgelistet: %s
 Datei muss mit "/" beginnen: %s
 Datei benötigt führenden "/": %s
 Datei von "glob" nicht gefunden: %s
 Datei nicht gefunden: %s
 Finde %s: %s
 Generiere %d fehlende Index(e), bitte warten...
 Header  SHA1-Kurzfassung des Headers: Header zu gross Ungültiger Regex-Ausdruck %s wird ignoriert
 Unvollständige Datenzeile bei %s:%d
 Unvollständige Standardzeile bei %s:%d
 Installations-/Aktualisierungs-/Deinstallationsoptionen: Installierte (aber nicht gepackte) Datei(en) gefunden:
%s Installiere %s
 Interner Fehler Interner Fehler: Falscher Tag %d
 Ungültiges %s Zeichen: %s
 Ungültige Fähigkeit: %s
 Ungültiges Datum %u Ungültige Patch-Nummer %s: %s
 Signatur-Optionen: MAKRO MD5-Kurzfassung: Makro %%%s konnte nicht erweitert werden
 Makro %%%s hat einen leeren Hauptteil
 Makro %%%s hat einen ungültigen Namen (%%define)
 Makro %%%s hat einen ungültigen Namen (%%undefine)
 Makro %%%s hat keinen terminierten Hauptteil
 Makro %%%s hat keine terminierten Optionen
 Fehlendes %s bei %s %s
 Fehlende '(' bei %s %s
 Fehlende ')' bei %s(%s
 Fehlender Modul-Pfad bei Zeile %s
 Fehlende rpmlib-Features für %s:
 Mehr als eine Datei in einer Zeile: %s
 NEIN  NICHT OK Kein "Source"-Tag in Spec-Datei
 Keine für das Bauen kompatible Architektur gefunden
 Keine Dateiattribute konfiguriert
 Keine Patch-Nummer %u
 Keine Source-Nummer %u
 Druckbarem Zeichen folgt %s(): %s
 Kein Verzeichnis: %s
 OK Betriebssystem ist ausgeschlossen: %s
 Betriebssystem ist nicht einbezogen: %s
 Paket ist bereits vorhanden: %s
 Paket-Prüfung "%s" fehlgeschlagen.
 Paket hat keine %%description: %s
 Falsche Passphrase oder ungültiger GPG-Schlüssel
 Das Passwort ist richtig.
 Bitte (in Englisch) %s kontaktieren
 Plugin %s nicht geladen
 Richtlinienmodul »%s« dupliziert mit überlappenden Typen
 PreReq:, Provides:, und Obsoletes: unterstützen Versionen. Pakete vorbereiten... Vorbereiten... Verarbeite Daten: %s
 Verarbeite Richtlinien: %s
 Abfrage-Optionen (mit -q oder --query): Abfragen/überprüfen der Paketauswahl: WURZELVERZEICHNIS RPM-Version %s
 Erkennung der Datei "%s" fehlgeschlagen: Modus %06o %s
 Empfange %s
 Signatur-Optionen: Spec-Optionen: Symbolischer Link zeigt auf den BuildRoot: %s -> %s
 Dieses Programm darf unter den Bedingungen der GNU GPL frei weiterverbreitet werden
 Zu viele Argumente in der Datenzeile bei %s:%d
 Zu viele Argumente in der Standardzeile bei %s:%d
 Zu viele Argumente in der Datenzeile bei %s
 Zu viele Rekursionslevel in der Makro-Expansion. Dies wurde wahrscheinlich durch eine rekursive Makro-Deklaration verursacht.
 Wechseln des Wurzel.Verzeichnis fehlgeschlagen: %m
 Konnte keine unveränderliche Header-Region erstellen.
 Kann %s nicht zum Lesen öffnen: %m.
 Öffnen von %s fehlgeschlagen: %s
 Das aktuelle Verzeichnis kann nicht geöffnet werden: %m
 Kann Icon %s nicht öffnen: %s
 Die Spec-Datei %s kann nicht geöffnet werden: %s
 Stream konnte nicht geöffnet werden: %s
 Temp-Datei konnte nicht geöffnet werden: %s
 Kann Icon %s nicht lesen: %s
 Kann den Header der Signatur nicht erneut laden.
 Aktuelles Verzeichnis kann nicht wiederhergestellt werden: %m Root-Verzeichnis kann nicht wiederhergestellt werden: %m
 Kann das Paket nicht schreiben: %s
 Kann den temporären Header nicht schreiben
 Unbekannter Datei--Auszug-Algorithmus %u, gehe zurück zu MD5
 Unbekannter Dateityp Unbekanntes Format Unbekannter Icon-Typ: %s
 Unbekannte Option %c in %s (%s)
 Unbekannte Nutzdaten.Kompression: %s
 Unbekanntes System: %s
 Unerfüllte Abhängigkeiten für %s:
 Nicht unterstützter PGP-Hash-Algorithmus %u
 Nicht unterstützter PGP-Pubkey-Algorithmus %u
 Nicht unsterstützte PGP-Signatur
 Nicht unterstütze Nutzlast (%s) in Paket %s
 Nicht terminiertes %c: %s
 Ungewöhnliche Locale-Länge: »%s« in %%lang(%s)
 Aktualisierung/ Installation...
 V%d %s/%s %s, Schlüssel-ID %s Überprüfungsoptionen (mit -V oder --verify): Version benötigt Versionierter Dateiname nicht erlaubt Erstellt: %s
 JA »%%_gpg_name« muss in der Makrodatei eingestellt sein
 [Keine] ] erwartet am Ende des Arrays Eine harte Verlinkung kann auch ohne eine komplette Installation angelegt werden. Argument ist kein RPM-Paket
 Argumente für --prefix müssen mit einem / beginnen Argumente für --root (-r) müssen mit einem / beginnen Zählvariable wird mit ungleich grossem Array benutzt Ungültiges Datum im %%changelog: %s
 Ungültige Option '%s' bei %s:%d
 Baue ein Binär-Paket vom <Source-Paket> Baue nur ein Binär-Paket von <Spec-Datei> Baue nur ein Binär-Paket vom <Tar-Archiv> Baue Source- und Binär-Paket von <Spec-Datei> Baue Source- und Binär-Paket vom <Tar-Archiv> Baue nur ein Source-Paket von <Spec-Datei> Baue nur ein Source-Paket vom <Tar-Archiv> %build (%prep, anschliessendes Kompilieren) der <Spec-Datei> durchlaufen %build (%prep, anschliessendes Kompilieren) des <Tar-Archivs> durchlaufen %install (%prep, %build, anschliessendes Installieren) des <Source-Pakets> durchlaufen %install (%prep, %build, anschliessendes Installieren) der <Spec-Datei> durchlaufen %install (%prep, %build, anschliessendes Installieren) des <Tar-Archivs> durchlaufen %prep (Quellen entpacken, Patches übernehmen) der <Spec-Datei> durchlaufen %prep (Quellen entpacken, Patches übernehmen) des <Tar-Archivs> durchlaufen BuildRoot wurde bereits angegeben, ignoriere %s
 kann keine %s Blockierung auf %s (%s) erstellen
 Kann ursprünglichen Eintrag %u nicht hinzufügen
 Kann Datei %s nicht erstellen: %s
 Keine %s-Sperre auf %s/%s
 Kann %s bei %s:%d nicht öffnen: %m
 Kann %s nicht öffnen: %s
 Kann Paket-Datenbank in %s nicht öffnen
 Kann Nutzdaten nicht erneut öffnen: %s
 --prefix kann nicht zusammen mit --relocate oder --excludepath verwendet werden Erstellen des Archivs fehlgeschlagen bei der Datei %s: %s
 Erstellen des Archivs fehlgeschlagen: %s
 erzeugen einer Pipe für --pipe fehlgeschlagen: %m
 Debugge Datei-Status Debugge Nutzdaten-Dateistatus Debugge rpmio Ein-/Ausgabe MAKRO mit Wert AUSDRUCK definieren Paketsignatur(en) überprüfen Abhängigkeitsvergleich unterstützt Versionen mit Tilde. unterschiedlich Verzeichnis endgültige rpmrc- und Makrokonfiguration anzeigen Zeige bekannte Abfrage-Tags Den Zustand der aufgelisteten Dateien anzeigen Keine i18n-Übersetzungen der Spec-Datei zulassen %%post-Scriptlet nicht ausführen (wenn vorhanden) %%postun-Scriptlet nicht ausführen (wenn vorhanden) %%pre-Scriptlet nicht ausführen (wenn vorhanden) %%preun-Scriptlet nicht ausführen (wenn vorhanden) %check-Stufe des Bauens nicht ausführen %clean-Stufe des Bauens nicht ausführen %%triggerin-Scriptlets nicht ausführen %%triggerpostun-Scriptlets nicht ausführen %%triggerprein-Scriptlets nicht ausführen %%triggerun-Scriptlets nicht ausführen Keine Scriptlets dieses Pakets ausführen Keine der Phasen des Erstellungsvorganges ausführen Keine Paket-Scriptlets ausführen "Globe" nicht nach Argumenten Installiere keine Konfigurationsdateien Installiere keine Dokumentation Dateien nicht als Paket-Liste verarbeiten Paketinstallation nicht neu sortieren, um die Abhängigkeiten zu erfüllen Keine Überprüfung der Paket-Abhängigkeiten Keine Überprüfung der Paket-Abhängigkeiten Keine Überprüfung des Festplattenspeichers vor der Installation Kein(e) Überprüfungsskript(e) ausführen Nicht importieren, sondern mitteilen, ob Import möglich wäre oder nicht Installiere keine Sicherheitskontext-Dateien Nicht installieren - nur anzeigen, ob es funktionieren würde Keine Fähigkeitsprüfung der Dateien Datenbank-Header beim Abrufen nicht überprüfen Keine Überprüfung der Prüfsumme der Dateien Keine Überprüfung der Prüfsumme der Dateien (veraltet) Keine Überprüfung des Sicherheitskontexts Keine Überprüfung der Dateien im Paket Keine Überprüfung der Gruppen der Dateien Keine Überprüfung der Header- und Nutzdaten-Signatur Keine Überprüfung der Berechtigungen der Dateien Keine Überprüfung der letzten Bearbeitungszeit der Dateien Keine Überprüfung der Eigentümer der Dateien Keine Überprüfung der Paket-Architektur Keine Überprüfung der Paket-Abhängigkeiten Paket-Kurzfassung nicht überprüfen Keine Überprüfung des Paket-Betriebssystems Paketsignatur(en) nicht überprüfen Keine Grössenüberprüfung der Dateien Keine Überprüfung der symbolischen Links der Dateien Grundlegende Dateiinformationen auflisten Leeres Tag-Format Unbekannter Tag löschen Lösche (deinstalliere) Paket Fehler beim Erstellen der temporären Datei %s: %m
 Fehler beim Lesen von Datei %s
 Fehler beim Lesen des Paket-Headers
 Fehler(%d) beim Hinzufügen des Header-Registers #%d
 Fehler(%d) beim Reservieren einer neuen Paket-Instanz
 Fehler(%d) beim Abruf von »%s« Einträgen aus dem %s-Index: %s
 Fehler(%d) beim Entfernen des Header-Registers #%d
 Fehler(%d) beim Entfernen des Eintrags "%s" aus %s
 Fehler(%d) beim Speichern des Eintrags "%s" in %s
 Fehler(%d) beim Speichern des Eintrags #%d in %s
 Fehler:  Ausgeschlossene Pfade müssen mit einem / beginnen exklusiv Ausführung fehlgeschlagen
 Zusätzliche '(' im Paket-Bezeichnung: %s
 fehlgeschlagen Erzeugen des Verzeichnises fehlgeschlagen Erzeugen des Verzeichnises %s fehlgeschlagen: %s
 Neu bauen der Datenbank fehlgeschlagen: Datenbank verbleibt entsprechend
 Entfernen des Verzeichnisses %s fehlgeschlagen: %s
 Konnte die alte Datenbank nicht durch die neue ersetzen!
 Aufruf von stat für %s nicht möglich: %m
 Konnte nicht all Daten nach %s: %s schreiben
 Schwerwiegender Fehler:  Datei Datei %s kollidiert zwischen den versuchten Installationen von %s und %s Datei %s aus der Installation von %s kollidiert mit der Datei aus dem Paket %s Die Datei %s gehört zu keinem Paket
 Datei %s: %s
 Dateiprüfsummenalgorithmus ist pro Paket konfigurierbar Dateiname(n) als (dirName,baseName,dirIndex)-Tupel gespeichert, nicht als Pfad. Dateien können nur bei der Paketinstallation verschoben werden Generiere Paket-Header, die mit veralteten RPM-Paketen (v3) kompatibel sind GPG fehlgeschlagen (%d)
 GPG konnte die Signatur nicht schreiben
 Gruppe %s enthält keine Pakete
 Gruppe %s existiert nicht - benutze Root
 Header #%u in der Datenbank ist ungültig -- überspringe.
 Header-Tags werden immer nach dem Laden sortiert. ExcludeArch ignorieren: Anweisungen der Spec-Datei Dateikonflikte zwischen Paketen vernachlässigen illegal _docdir_fmt %s: %s
 Illegaler Signatur-Typ Importiere einen gepanzerten öffentlichen Schlüssel Unvollständiger %%changelog-Eintrag
 Ungültiges Format: %s
 Initialisiere Datenbank installieren Installiere alle Dateien, sogar Konfigurationsdateien, die sonst möglicherweise übersprungen werden Installiere Dokumentation Installiere Paket(e) Interne Unterstützung für LUA-Scripte Ungültige Abhängigkeit Ungültige Feldbreite Ungültiger Index-Typ %x auf %s/%s
 Ungültige Paket-Nummer: %s
 Ungültige Syntax in lua-Datei: %s
 Ungültige Syntax in lua-Script: %s
 Ungültige Syntax in lua-Scriptlet: %s
 Zeile %d: %s
 Zeile %d: %s ist veraltet: %s
 Zeile %d: %s: %s
 Zeile %d: Ungültige %%setup-Option %s: %s
 Zeile %d: Ungültige %s Nummer: %s
 Zeile %d: Ungültig %s: Kennzeichner: %s
 Zeile %d: Ungültiges BuildArchitecture-Format: %s
 Zeile %d: Ungültiges Argument für %%setup: %s
 Zeile %d: Ungültige no%s-Nummer: %u
 Zeile %d: Ungültige Nummer: %s
 Zeile %d: Ungültige Option %s: %s
 Zeile %d: Das Dokumentationsverzeichnis muss mit einem "/" beginnen: %s
 Zeile %d: Leerer Tag: %s
 Zeile %d: Das Epoch-Feld muss eine vorzeichenlose Zahl sein: %s
 Zeile %d: Fehler beim Parsen von %%description: %s
 Zeile %d: Fehler beim Parsen von %%files: %s
 Zeile %d: Fehler beim Parsen von %%policies: %s
 Zeile %d: Fehler beim Parsen von %s: %s
 Zeile %d: Ungültiges Zeichen '%c' in: %s
 Zeile %d: Ungültiges Zeichen in: %s
 Zeile %d: Ungültiges Zeichen ".." in: %s
 Zeile %d: Missgebildeter Tag: %s
 Zeile %d: Nur noarch-Unterpakete werden unterstützt: %s
 Zeile: %d: Paket existiert nicht: %s
 Zeile %d: Präfixe dürfen nicht mit einem "/" enden: %s
 Zeile %d: Zweites %s
 Zeile %d: Tag benötigt nur ein Zeichen: %s
 Zeile %d: Zu viele Namen: %s
 Zeile %d: Nicht abgeschlossenes %%if
 Zeile %d: Unbekannter Tag: %s
 Zeile %d: Internes Skript muss mit '>' enden: %s
 Zeile %d: Interpreter-Argumente in Auslösern nicht erlaubt: %s
 Zeile %d: Skript/Programm muss mit '/' beginnen: %s
 Zeile %d: Zweites %%prep
 Zeile %d: Zweites %s
 Zeile %d: Trigger benötigen --: %s
 Zeile %d: Nicht geschlossenes Makro oder fehlerhafte Zeilenfortführung
 Zeile %d: Nicht unterstütztes internes Skript: %s
 Zeile: %s
 Alle Konfigurationsdateien anzeigen Alle Dokumentationsdateien anzeigen Alle Dateien im Paket auflisten Schlüssel des RPM-Schlüsselbundes auflisten lua hook fehlgeschlagen: %s
 lua-Script fehlgeschlagen: %s
 magic_load fehlgeschlagen: %s
 magic_open(0x%x) fehlgeschlagen: %s
 Missgebildet: %s: %s
 Speicherbelegung (%u Byte) lieferte NULL zurück.
 miFreeHeader: Überspringe fehlt fehlend   %c %s Fehlende '(' im Paket-Label: %s
 Fehlende ')' im Paket-Bezeichung: %s
 Fehlender ':' (0x%02x gefunden) bei %s:%d
 Fehlende Architektur für %s bei %s:%d
 Fehlender Architektur-Name bei %s:%d
 Fehlendes Argument für %s bei %s:%d
 Fehlender Name im %%changelog
 Fehlender zweiter ':' bei %s:%d
 Fehlende { nach % Fehlende } nach %{ Netzwerk-Mitbenutzung geshared     Es wurden keine Argumente angegeben Keine Argumente zum Analysieren angegeben Es wurden keine Argumente für die Abfrage angegeben Es wurden keine Argumente für die Überprüfung angegeben Kein Datenbank-Pfad wurde gesetzt Kein Datenbank-Pfad wurde gesetzt
 Keine Beschreibung im %%changelog
 Kein Paket stimmt mit %s überein: %s
 Kein Paket bietet %s
 Kein Paket benötigt %s
 Keine Paket-Trigger %s
 Es wurden keine Pakete für die Deinstallation angegeben Es wurden keine Pakete für die Installation angegeben kein Zustand normal normal         ist kein RPM-Paket kein RPM-Paket
 nicht installiert nicht installiert  Nur eine Art von Abfragen/Überprüfungen kann jeweils durchgeführt werden Nur Installation und Upgrade kann erzwungen werden Nur ein wichtiger Modus kann angegeben werden entweder --excludedocs oder --includedocs darf angegeben werden Öffnen von %s fehlgeschlagen: %s
 Arbeite mit dem binären RPMS, das aus der Spezifikation erstellt wurde (Standard) Arbeite mit Quell-RPM, das aus der Spezifikation erstellt wurde Überschreibe BuildRoot Überschreibe Zielplattform Paket %s (welches neuer als %s ist) ist bereits installiert Das Paket %s ist bereits installiert Das Paket %s ist für die Architektur %s vorgesehen Das Paket %s ist für das Betriebssystem %s vorgesehen Das Paket %s ist nicht installiert
 Paket %s ist nicht verschiebbar
 Paket %s wurde bereits hinzugefügt, ersetze es durch %s
 Paket %s wurde bereits hinzugefügt, überspringe %s
 Paket weder Eigentümer- noch ID-Liste
 Paket hat keine Eigentümer-/Gruppen-Liste
 Paket Name-Version-Release wird nicht unbedingt bereitgestellt. Paket-Nutzdaten können mit bzip2 komprimiert werden. Paket-Nutzdaten können mit lzma komprimiert werden. Paket-Nutzdaten können mit xz komprimiert werden. Dateien der Paket-Nutzdaten haben "./" als Präfix Paket-Scriptlets können während der Installation entpackt werden. Paket-Scriptlets können während der Installation auf die RPM-Datenbank zugreifen. Fehler beim Parsen des Ausdrucks
 Analyse für Spec-Datei(en) an Standardausgabe übergeben Der Pfad %s im Paket %s ist nicht verschiebbar MAKRO mit Wert AUSDRUCK vordefinieren Zeige Abhängigkeitsschleifen als Warnung Zeige Rautezeichen während der Installation (empfehlenswert mit -v) Makro-Ausdehnung des AUSDRUCKS anzeigen Prozentangabe bei der Paketinstallation anzeigen Zeige benutzte RPM-Version Zeige weniger informative Ausgabe Zeige detailliertere Ausgabe Abfragen der Spec-Datei %s fehlgeschlagen, kann nicht geparst werden
 Spec-Datei(en) abfragen Abfragen eines Pakets gesteuert vom Paket Abfragen/überprüfen einer Header-Instanz Abfragen/überprüfen einer Paket-Datei Abfrage aller Pakete Abfragen/überprüfen von Paketen einer Installation Abfragen/überprüfen eines Pakets einer Gruppe Abfragen/überprüfen eines Pakets, das die Datei besitzt Abfragen/überprüfen von Paketen mit Header-Kennung Abfragen/überprüfen von Paketen mit der Paket-Kennung Abfrage nach Paketen, die die Fähigkeit bereitstellen Abfrage nach Paketen, die die Fähigkeit benötigen lese <DATEI:...> anstatt der Standard-Datei(en) Lesen fehlgeschlagen: %s (%d)
 Lesen der symbolischen Verknüpfung %s gescheitert: %s
 Erstelle invertierte Datenbank-Liste anhand der installierten Paket-Header Eintrag %u konnte nicht gelesen werden
 Installiere erneut, wenn das Paket bereits vorhanden ist verschiebe Dateien von <alter Pfad> nach <neuer Pfad> Verschiebe Dateien eines nicht verschiebbaren Pakets Verschiebe das Paket, wenn es verschiebbar ist, in das Verzeichnis <Verzeichnis> Verschiebungen müssen mit einem / beginnen Verschiebungen müssen ein = enthalten Verschiebungen müssen einen / nach dem = enthalten Entferne alle Pakete, die mit <Paket> übereinstimmen (normalerweise wird eine Fehlermeldung angezeigt, wenn <Paket> auf mehrere Pakete zutrifft) Erstellungsdateibaum nach Beendigung löschen Quelldateien nach Beendigung löschen Spec-Datei nach Beendigung löschen Ersetze Dateien in %s mit Dateien aus %s für eine Wiederherstellung ersetzt ersetzt       Ersetzen von %s fehlgeschlagen: %s
 Abfrage-Modus der Signatur Abfrage-Modus Überprüfungsmodus rpmMk Temp fehlgeschlagen
 rpmdb: Beschädigten Header #%u erhalten -- überspringe.
 rpmdbNextIterator: Überspringe Optionen zum Deaktivieren von Skripten können nur während der Paketinstallation oder -deinstallation angegeben werden Sende Standardausgabe an CMD verteilt Abkürzung für --replacepkgs --replacefiles Paket(e) signieren Signiere Paket(e) (identisch mit --addsign) Überspringe %%ghost-Dateien Überspringe Dateien mit beginnendem <Pfad>  Springe direkt zur angegeben Phase (nur für c, i) übersprungen überspringe %s - Übertragung fehlgeschlagen
 Source-Paket enthält keine Spec-Datei
 Source-Paket erwartet, Binär-Paket entdeckt
 Unterstützung für POSIX.1e-Dateifähigkeiten Syntax-Fehler im Ausdruck
 Syntax-Fehler beim Parsen von &&
 Syntax-Fehler beim Parsen von ==
 Syntax-Fehler beim Parsen von ||
 Der Scriptlet-Interpreter kann Argumente aus dem Header benutzen. Transaktion Optionen zum Deaktivieren von Trigger können nur während der Paket-Installation oder -Deinstallation angegeben werden Typen müssen übereinstimmen
 Konnte die Signatur nicht lesen
 undefiniertes MAKRO Unerwartete ] Unerwartete Abfrage-Parameter Unerwartetes Abfrage-Format Unerwartete Abfrage-Quelle Unerwartete } Unbekannter Fehler %d trat während dem Verarbeiten des Pakets %s auf unbekannter Zustand Unbekannter Tag Unbekannter Tag: "%s"
 Unerwartete (
 Entpacken des Archivs fehlgeschlagen%s%s: %s
 Unbekannte Datenbank-Option: "%s" ignoriert.
 nicht unterstützte RPM-Paket-Version Aktualisiere die Datenbank, aber verändere nichts im Dateisystem Paket(e) aktualisieren Aktualisiere Paket(e), wenn bereits installiert Aktualisierung auf eine alte Version des Pakets (--force macht das bei Aktualisierungen automatisch) benutze WURZELVERZEICHNIS als oberstes Verzeichnis Datenbank in VERZEICHNIS benutzen Folgendes Abfrage-Format benutzen Benutzer %s existiert nicht - benutze Root
 Überprüfe %files-Abschnitt der <Spec-Datei> Überprüfe %files-Abschnitt des <Tar-Archivs> Überprüfe Datenbank-Dateien Paketsignatur(en) überprüfen warte auf %s Blockierung auf %s
 Warnung:  Falsche Farbe falsche Farbe    { nach dem : im Ausdruck erwartet { nach dem ? im Ausdruck erwartet | am Ende des Ausdrucks erwartet } im Ausdruck erwartet PRIu64 installing package %s needs %%cB on the %s filesystem installing package %s needs % inodes on the %s filesystem Installation des Pakets %s benötigt %%cB auf dem %s-Dateisystem Installation des Pakets %s benötigt %-Inodes auf dem %s-Dateisystem 