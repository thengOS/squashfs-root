��    ,     |!  �  �B      (Y     )Y     >Y     WY  "   vY  +   �Y     �Y     �Y     �Y  &   Z     6Z     BZ      aZ      �Z  '   �Z     �Z  =   �Z     "[     3[     D[  &   b[  (   �[  ;   �[  6   �[  E   %\     k\  
   �\     �\  :   �\  1   �\  ,   ]     B]     U]  #   j]  #   �]  %   �]  +   �]     ^     ^  )   )^     S^     m^     }^  2   �^  0   �^  ,   �^  (   )_  +   R_  %   ~_  ,   �_  +   �_     �_  3   `     B`  E   T`  T   �`     �`     a  C   a  B   ]a  A   �a  =   �a  E    b  X   fb  3   �b  8   �b  N   ,c  ;   {c  :   �c  R   �c  3   Ed  8   yd     �d     �d     �d     �d  '    e     (e     :e     Ie     be     {e     �e  Y   �e  b   f     df  *   f     �f  &   �f  <   �f  3    g  3   Tg  :   �g  /   �g  D   �g  2   8h  4   kh  ,   �h  4   �h  <   i  5   ?i  7   ui  5   �i  3   �i     j  +   /j  8   [j  9   �j  8   �j  8   k  +   @k  0   lk  0   �k  2   �k  '   l  8   )l  "   bl  0   �l  7   �l  H   �l  J   7m  9   �m  7   �m  L   �m  7   An  2   yn  R   �n  :   �n  ?   :o  >   zo  =   �o  >   �o  6   6p  <   mp  7   �p  8   �p  <   q  <   Xq  I   �q  N   �q  =   .r  H   lr  )   �r  =   �r  >   s     \s     os  7   �s  3   �s     �s     �s     t  @   1t  8   rt     �t     �t     �t     �t     u     u  $   2u     Wu     ou     �u     �u  9   �u  +   �u  A   v  )  Xv  �   �w  �   lx  *   &y  (   Qy  
   zy  (   �y  
   �y     �y  "   �y  /   �y  (   z     =z  %   Yz     z     �z     �z  
   �z     �z  -   �z     {     {     0{  "   G{  +   j{  #   �{      �{  "   �{  "   �{  (   !|     J|  "   V|  "   y|      �|  B   �|  :    }  !   ;}  (   ]}  (   �}  H   �}  Q   �}  "   J~  %   m~     �~  "   �~     �~     �~        "      4   C  (   x     �     �     �     �     �     �     "�     @�     U�  +   q�  1   ��  1   π  1   �  5   3�  @   i�  \   ��  J   �  (   R�  #   {�  *   ��  +   ʂ  +   ��  "   "�     E�  (   e�  /   ��  (   ��  &   �  7   �  .   F�     u�  C   ��  Q   Ƅ  Y   �  3   r�  -   ��  %   ԅ     ��  ,   �  1   ;�  @   m�  >   ��  .   �  )   �  6   F�  Q   }�     χ  1   �     �  -   7�  4   e�  5   ��  I   Ј     �  -   :�     h�     ��  2   ��  	   Љ     ډ     �  8   ��  	   8�     B�     Q�     a�     p�     ��     ��  	   ��  7   ��  5   �  7    �  ?   X�  ,   ��     ŋ  0   ۋ  +   �  3   8�  <   l�  ,   ��  ;   ֌  4   �  
   G�  /   R�  Q   ��  5   ԍ  *   
�  /   5�     e�  7   l�  B   ��  K   �  9   3�  B   m�  =   ��  )   �     �    +�  t   @�  N  ��  J   �  �  O�  �   �     ��     ��  '   ��     �     �     �     $�     9�     E�  
   Z�     e�  #   q�     ��     ��     ��     ɘ     ��     ��     �     &�     @�     P�      n�  !   ��     ��     ��     ܙ     ��  (   �  3   ;�  7   o�     ��     ��     К     �     �     �  %   5�     [�     s�  "   ��  *   ��  (   ޛ     �     &�     7�     S�     m�     �     ��     ��  $   ͜  '   �     �     2�     A�     Z�  )   u�     ��     ��     ɝ     �     ��  ,   �     A�     T�  =   b�  2   ��  *   Ӟ  #   ��     "�     A�     ^�     c�     ��     ��     ��     ǟ     ݟ     �     �      �      7�     X�  	   u�     �     ��     ��     ��     ��     Π     ݠ     �     �     �     +�     ?�  	   N�     X�     d�     p�     y�     ��     ��     ��     ��     ҡ     ۡ     �     �     ��     �     ,�     E�     T�  "   f�     ��     ��     ��     ��     ڢ     ��     �     �  	   (�     2�     @�     O�     ]�     m�     t�     ��     ��     ��     ģ     ң     �     ��     �  "   0�  "   S�     v�     ��  %   ��  &   Ť  $   �     �     *�     G�     _�     y�     ��     ��     ��     ��     ڥ     ��     �  <   #�  2   `�     ��     ��     ��     ئ     ��  6   �  "   J�  1   m�  $   ��     ħ     է     �     �     "�     8�     N�     d�      z�  .   ��     ʨ  	   �     ��     �     ,�  !   K�     m�  B   ��  :   ɩ     �  +   �     @�  $   M�  '   r�     ��     ��     ê     Ъ     �     ��  �   �  #   �  $   	�  #   .�  -   R�  -   ��  +   ��     ڬ  E   �     .�     =�     D�     [�     v�     ��     ��     ��      ��  &   �  $   �  &   ,�  N   S�  >   ��  .   �     �       �  !   A�  0   c�  (   ��     ��     ӯ     �     �     #�     A�  !   V�     x�     ��     ��     ��     Ѱ     �     ��  :   �     A�     \�     q�  4   ��     ű     ױ     �     ��  $   �  &   :�  "   a�  $   ��      ��      ʲ  $   �     �     -�     L�     k�  )   ��     ��     ǳ  `   ̳  P   -�     ~�  	   ��     ��     ��     ˴     �  &   �  *   )�     T�     j�  +   ��     ��     ��  <   ��  6   ��     +�  	   D�     N�     ^�  5   p�     ��  4   ��  4   ��     +�     E�  M   d�     ��  ,   ��  -   �     �     "�     A�     \�  ,   {�  3   ��  ?   ܸ  )   �     F�     Y�     o�     ��     ��     ��     Ϲ  )   �  	   �     �     &�     9�     M�  5   e�  7   ��  #   Ӻ  
   ��     �  '   �     ?�     N�     ^�     m�  %   ��     ��     Ļ     ׻     ��  2   �  ?   G�  8   ��  /   ��  7   �  3   (�  ;   \�  w   ��     �     #�     /�     >�  $   D�     i�  !   n�  "   ��     ��  	   ľ  ,   ξ  >   ��  ;   :�  2   v�  (   ��     ҿ     �  "   �  +   0�  $   \�     ��  (   ��  5   ��      �      �     ?�  /   _�     ��     ��     ��  )   ��     ��     �     *�     B�     Y�     n�     ��     ��     ��     ��  #   ��  $   �     A�     Y�     s�  &   ��      ��  !   ��  |   ��  j   t�      ��       �  +   !�  1   M�     �  )   ��     ��  '   ��     �     �  (   (�  6   Q�  ;   ��     ��     ��     ��  5    �  :   6�     q�  +   y�     ��  9   ��  %   ��  *   �  +   A�  6   m�  .   ��     ��     ��     ��     �  4   �  *   C�  +   n�  $   ��     ��     ��     ��  	   ��      �  
   �     �     *�     H�     \�     n�     ��     ��      ��  +   ��     ��     �  &   /�  (   V�  
   �     ��     ��  (   ��      ��  6   ��     .�     E�     _�  '   v�     ��     ��  "   ��     ��     �  (   ,�     U�     j�     �     ��     ��     ��     ��     ��  3   ��      �     <�  !   [�  +   }�  6   ��  2   ��  5   �     I�     U�  -   \�     ��  
   ��     ��  /   ��  1   ��  !   �  '   6�  '   ^�  /   ��  2   ��  7   ��  +   !�      M�  1   n�  #   ��  "   ��  +   ��  &   �  #   :�  )   ^�  .   ��  #   ��     ��  *   ��     &�     5�     I�     \�     s�     ��     ��  	   ��     ��     ��     ��     ��      �     �     #�  #   2�  9   V�     ��     ��     ��  &   ��  *   �  &   1�  *   X�  3   ��      ��  !   ��     ��     �     �     $�     8�     J�     Y�     l�     y�  
   ��     ��     ��     ��     ��  )   ��     �     '�     :�  (   Q�  4   z�     ��  -   ��     ��      �     4�     P�     _�     r�  .   ��     ��     ��     ��     ��     ��     �     �     +�     9�     G�     P�     `�     p�     ��     ��     ��     ��     ��     ��     �  $   �     :�     F�     ^�  S   y�  %   ��     ��     �     �     1�     H�     U�  +   a�     ��     ��  =   ��     ��     ��  4   ��     3�     D�     P�     X�  -   v�  #   ��  
   ��     ��     ��  $   ��     �     &�     4�     G�     ]�  
   i�  8   t�  !   ��  =   ��  D   �     R�     c�  !   p�     ��     ��     ��     ��     ��     ��     �     �     *�     ;�     M�     \�  	   e�  7   o�  ;   ��     ��     ��     �     �  #   +�     O�  !   l�  #   ��     ��     ��     ��     ��     
�     �     3�     S�     f�     ��     ��     ��     ��  :   ��     �  )   )�  $   S�  $   x�     ��  (   ��     ��  !   �     $�     9�     N�     h�     ��  '   ��  )   ��  &   ��     �     9�     W�     i�     ��     ��     ��     ��     ��     ��     ��     �     ,�     <�     Q�     o�     ��     ��  '   ��  *   ��  &   ��     �     /�     O�     f�     ��     ��  %   ��     ��     �  !   "�     D�     c�  '   w�     ��     ��     ��  $   ��     �  @    �     a�     w�     ��     ��     ��     ��     ��     ��     ��  4   ��      �     -�     B�     W�     l�     ��     ��     ��  '   ��  &   ��  ,   ��      *�  '   K�  I   s�  9   ��  4   ��  2   ,�  =   _�     ��     ��  �  ��     I�     ]�      u�  $   ��  *   ��     ��     ��     �  (   �     G�  &   S�  #   z�  "   ��  (   ��     ��  A   �     E�     V�  !   e�  9   ��  A   ��  ?   �  ;   C�  Q   �     ��     ��  #   ��  B   �  ;   [�  5   ��     ��     ��  )   ��  4    �  2   U�  1   ��     ��     ��  0   ��     �     -�     >�  1   Y�  :   ��  0   ��  .   ��  0   &�  *   W�  =   ��  3   ��     ��  8   �     K�  E   \�  V   ��     ��     �  G   #�  F   k�  A   ��  ?   ��  G   4�  p   |�  E   ��  @   3�  I   t�  @   ��  A   ��  M   A�  6   ��  <   ��     �     �     (�     7�  *   V�     ��  	   ��     ��     ��     ��     ��  V   ��  `   V�     ��  (   ��     ��  *   �  I   9�  ;   ��  <   ��  H   ��  9   E�  i   �  8   ��  @   "�  4   c�  D   ��  L   ��  J   *�  C   u�  9   ��  8   ��     ,  1   E  B   w  A   �  <   �  <   9 >   v 9   � 4   � :   $ 3   _ >   � 4   � ;    ;   C N    p   � E   ? :   � V   � <    ?   T f   � 7   � H   3 F   | A   � N    <   T H   � 9   � H    D   ] J   � L   � h   :	 E   �	 s   �	 4   ]
 A   �
 K   �
         3 <   F <   �    �    �    � @   � 8   @    y    �    �    �    �    � $        %    <    X    m @   } 1   � =   � 7  . �   f �   _ 0   � (    
   : (   E    n    } #   � 5   � (   �      '   -    U %   o    �    �    � -   �    �    �     '   1 ,   Y %   � !   � #   � #   � (       ? !   K $   m (   � D   � <     !   = (   _ (   � H   � P   � %   K &   q    � #   �    � #   � !    $   5 @   Z +   �    � !   �             1    M '   h    � %   � 7   � 2   
 3   = 2   q 3   � ?   � \    J   u (   � '   � *    +   < +   h    �    � *   � 2   � *   ! '   L =   t 6   �    � C   � S   : [   � ?   � .   * 2   Y    � )   � 7   � P    S   Y 0   � 0   � 9     V   I  '   �  3   �     �  1   ! 6   G! ;   ~! U   �! &   " -   7"    e"    �" 6   �" 	   �"    �"    �" 9    # 	   :#    D#    [#    l#    |#    �#    �# 	   �# @   �# ?   �# A   ?$ =   �$ -   �$    �$ 5   % ,   9% :   f% 7   �% -   �% O   & @   W&    �& =   �& e   �& C   I' ,   �' 8   �'    �' 7   �' ?   4( I   t( 9   �( C   �( E   <) 2   �)    �) C  �) }   , f  �, :   �-   0. �   A1    �1    2 (   2 #   >2 
   b2    m2    �2    �2    �2    �2    �2 $   �2    �2    3    3    33    K3    d3    3    �3    �3 3   �3 3   �3 3   -4    a4 +   n4 +   �4    �4 -   �4 8   
5 =   C5    �5    �5    �5 /   �5 +   �5 "   &6 /   I6 &   y6 #   �6 /   �6 8   �6 9   -7    g7    �7 #   �7    �7    �7 +   �7    %8 #   B8 *   f8 (   �8    �8    �8    �8    9 4   "9 $   W9    |9     �9    �9    �9 3   �9    :    ': P   5: <   �: '   �: (   �:    ; !   2;    T; &   ];    �;    �;    �;    �;    �; '   
<    2< #   B< &   f<     �< 	   �<    �<    �<    �<    �<    �<    =    =    6=    H=    b=    |=    �=    �=    �=    �= 	   �=    �=    �=    �=    >    >    <>    E>    M>    U>    h> (   z>     �>    �>    �> )   �>    ?    +?    A?    N?    h?    �?    �?    �?    �?    �?    �?    �?    �?    @ %   @ ,   >@    k@ 
   s@    ~@    �@    �@ %   �@    �@ /   �@ 0   )A    ZA    qA +   �A ,   �A (   �A    B    /B    LB    hB    �B    �B    �B /   �B 6   �B 0   C +   GC '   sC I   �C ;   �C    !D    5D    ND    gD    �D :   �D (   �D @   �D 8   @E    yE !   �E    �E "   �E    �E    F    F    2F "   IF 0   lF    �F    �F '   �F &   �F &   G %   <G "   bG I   �G :   �G    
H 2   H    RH -   _H 2   �H    �H    �H    �H    �H    I    2I   QI 2   qJ /   �J .   �J 8   K 1   <K -   nK    �K A   �K    �K     L    L    !L    >L    TL    aL    uL +   �L 0   �L -   �L 0   M Q   PM A   �M 9   �M    N %   -N (   SN 6   |N -   �N    �N    �N )   O (   >O (   gO    �O '   �O    �O    �O    P    P    9P    MP    hP F   {P )   �P    �P &   Q @   +Q    lQ    Q    �Q    �Q    �Q !   �Q !   �Q #   R    AR    aR #   �R    �R    �R    �R    �R (   S '   ;S    cS b   hS R   �S    T 	   0T    :T    ST    pT    �T &   �T .   �T    U %   U ,   AU    nU    uU 0   |U D   �U    �U    V    V    -V :   AV    |V 7   �V F   �V    W    :W N   XW    �W :   �W 9   �W    2X    ;X    YX #   sX 2   �X =   �X ;   Y *   DY    oY    �Y    �Y    �Y    �Y    �Y    Z .   .Z 	   ]Z    gZ    sZ !   �Z    �Z =   �Z E   [ "   Q[    t[    �[ 3   �[    �[    �[    �[    \ 4   "\    W\    t\     �\    �\ J   �\ G   ] 7   ]] ;   �] >   �] D   ^ G   U^ �   �^    "_    5_    Q_    h_ *   n_    �_ 6   �_ 6   �_    `    ` :   *` L   e` K   �` :   �` 3   9a '   ma 0   �a .   �a @   �a 3   6b .   jb <   �b ;   �b 1   c 3   Dc -   xc >   �c    �c    �c     d *   0d    [d    xd    �d    �d    �d    �d    e    e !   5e !   We *   ye -   �e    �e !   �e     f .   /f (   ^f -   �f �   �f n   7g +   �g (   �g :   �g :   6h )   qh 8   �h !   �h /   �h    &i    =i 5   Ri I   �i B   �i    j (   $j    Mj N   Zj ?   �j    �j 1   �j    $k J   5k .   �k 3   �k 9   �k =   l <   [l    �l    �l    �l    �l ?   �l /   m 0   Dm 6   um    �m    �m    �m 	   �m    �m 	    n    
n +   $n    Pn    mn %   �n    �n    �n &   �n ;   o &   Bo &   io 2   �o 9   �o 
   �o    p .   p 1   ?p #   qp E   �p /   �p *   q *   6q 8   aq ,   �q ,   �q 8   �q 2   -r :   `r >   �r $   �r *   �r +   *s "   Vs *   ys *   �s    �s    �s C   �s    !t &   @t .   gt <   �t P   �t D   $u G   iu    �u    �u 0   �u    �u    v    v 4   &v =   [v %   �v +   �v +   �v =   w /   Uw :   �w 6   �w %   �w 9   x '   Wx )   x /   �x +   �x '   y -   -y 6   [y 7   �y $   �y /   �y    z    4z    Oz    iz    �z    �z    �z    �z    �z    �z     {     {    9{    J{    Z{ &   n{ :   �{ )   �{    �{    | 5   .| 7   d| 5   �| 2   �| A   } %   G} %   m}    �}    �}    �}     �} #   �}    ~    +~    @~    M~    a~    n~ 
   �~    �~    �~ -   �~     �~         4   4 B   i     � 7   �    � &   "�    I�    h�    z�    �� 6   ��    �    �    �    �    4�    T�    g�    s�    �� 
   ��    ��    ��    ��    ʁ    �    � !   �    9�    V�    i� 0   ��    ��     !   ݂ y   �� 2   y�    ��    ��    ܃    ��    �    #� .   2�    a�    h� S   w�    ˄ $   ӄ K   ��    D�    Z�    g� 2   p� 2   �� *   օ 
   �    �    � "   .�    Q�    _�    o�    ��    ��    �� =   �� "   �� ?   � J   ]�    ��    �� %   ̇    �    �    �    '�    B�    ^�    x�    ��    ��    ��    ӈ    �    � G   �� M   C�    �� %   ��    ŉ    Չ 1   � *   � 1   B� .   t�    �� %   ��    ފ    �    �    #� &   :�    a� .   w� *   ��    ы    �    �� >   � +   B� 2   n� 1   �� 6   ӌ +   
� 7   6� '   n� -   ��    č    ݍ    ��    �    /� +   O� *   {� &   ��     ͎     �    �    "� $   :�    _�    {� 	   ��    ��    ��     ȏ    �    �    � $   5�    Z�    y�    �� 1   �� 4   ͐ -   �    0� %   D�    j�    ��    ��    �� 0   �� '   �     9� #   Z� %   ~�    �� +   ��    �     � #   � +   ?� #   k� R   ��    �    ��    �    /�    C�    P�    ^�    k�    {� 6   ��    Δ    ݔ    ��    �    )�    =�    N� 
   b� *   m� )   �� 9    '   �� *   $� j   O� D   �� 5   �� 7   5� K   m�    ��    ȗ    x  �  #      =  �  �     �   �      �  �   /  �  r       I  �                 �  �      �   S  �   �              ]          N  �  '   �  �       {  �  f      Y      �  �  7  �       �   �       o      �       t   �  �  �  �  �  �  d  u  �  �  �      ^  �      �  �           �          i      �            �    �   \  b   j   �  �    �   �       �      �  �   �      �  �   �  �  �   `  �     �      �  �   d  0   �              �      �  &  t  R  A  �   �   ;  &      �   )      �      �  >  J  M  �      Z      �       �        �    �   n  M           �   �   y  T  �        V   �  �  �          #  ]    C  �  �  Q      �  �          �  !   #      �  �  +    �  �  �  L  �      �  �  �  �  r  �  {              �  m   {  �      �    �       G  �  �  �        �   �                �   -      �  �          D          �   i  w  o       �  �  .      r  �   �  �  C      �  �   �  �  �    �      �  �  �   �   a  �   I      �  .          I    Z   <     4     �  �      �   �  M   "      �  ~      0      ?  3      N      `   �  )  �  �  Y  �                             <   �  
  �     T  �      p         �              |  �  �  p      �  �  |  2   �   =   �  e     �  �   �   �  �  �        ?   �      3          �   l    �       �  ,  �  K   F      �  j  �   �  0  �  m  �          [   3         �    %  �  (   �    X      �   *  /  O  �  �      �  �    �  �  z  �   f   4            �      /  U  :      �  �  �  w  �        �            Q    �           �  �  �     L   $      
   �  u      �      �  �  �  w  �    �      �        �       �   `  
  \          �      j  �  �      �       >                 �  |      �      m      ;      �   �  �  "      k  �  X  �  X       x  �  �   �  �  �  g  �  D  j    �  ?  �       �    �          �  W       �  �  q  �  g   �            �       "   �  �   g  G   �          W  �      t  1  	   5  O  �   �      �  7   b  N  @  �  9  �      +       �  �      �  �  �  _          }  ~   (  �  ,      �  �  �  �   K          R  ,   !  �  �  �        z   �   �      o      u                 d           �  _      �      �  �      c                  �   E  	      �  !      �  �  k   �  �              k    <  P  E  �   c      /   �   f  g  _            f  )       �  �   �      '      �     U  �   �      �  9  v               ?  �          �   %      �          %  �   D   k  �  ^  �   �  �  �  �  h      6   �    �        �    =  �      �  L      U   \  �           �     S  �  F   `  �  $  �       �   !  �  �      �  I       �      T  �  l   M    $    �      b     �  �              �    %  (      v  s  �      �   �  _       8      �   �     	  �  �  
  �      A  1  X        @                �       �  Q   �  ,  *  7  -  �      �     x   �      �      x      �   �      E  N   1      �      V    B  �   J  �  >  �        h  A   �  9   �  �     �  K  �      �      �  �     �      P  �      �  @  �  �  �           �         �  �          7  �     �  -      u  �  <      @           :             �     -   �    y              �  t  �    �  *  �       w   �  �  �           n  o  �  q  �  �      	        �      8  R   �   O          W  �          �   �     �      �      e  �  L          �   �  #   #        �      �    �  '     �  l  �   �   �  �   �       +      9  �  �   �  �  �  �  F          G  �  [  ,          �   �           �              �             '    �       �  �  +      H  5  �   �  �  H  a  1       �  �  i  �  \  z  B  *   �      2                    �             �  �  �      �      V  q   �  &  �    b      �  �  �   �  >    �  �  �  0  �       �  �  d  '      �  J  6  �  �   5   �   2  (      �        �     �       O     �      }   U      �    {   $  �  �  s   �   q     y  ~  y   �       p       �  �  K  �  �  B     %       �      ^              �       h           �  �  �  �      �      �  �  �      )  �   �  �  �       ;   �              .   C        �    S   �         V  8   �  �      Z  A  5  �  m  6  "          c  �  �  �  $   �      +  �                  v  �               �   �      )   !          �                    �      a  �    s  �   }      �       �      z    R      �                 "  �          [      |       �  i          �  n  �      p  �  �      3         �      �   e                 .  P       �                        �   h      �      T   �  F  S  Q  }  �       c     �  l      �   �  �       ~  �  �     �      �        4   �             &       2      B  E   �  �   �          �   	  :  r  
  ]    Y   s  �  4  �  �   8     �  �  Y         �  �  :  �  �  [    ;  �  *            e             Z  �        =  ]  �  �  �  �          �  �      ^   �  �          �      �      H      C   v  J   �                  �  W          P  �       �  H          �   a   n   D    G  �  �   �  �  �  �      �          �  (      6      &      �  �    

Symbols from %s:

 

Symbols from %s[%s]:

 

Undefined symbols from %s:

 

Undefined symbols from %s[%s]:

 
      [Requesting program interpreter: %s] 
    Address            Length
 
    Address    Length
 
    Offset	Name
 
  Start of program headers:           
 Opcodes:
 
 Section to Segment mapping:
 
 The Directory Table is empty.
 
 The File Name Table is empty.
 
 The following switches are optional:
 
%s:     file format %s
 
'%s' relocation section at offset 0x%lx contains %ld bytes:
 
Address table:
 
Archive index:
 
Assembly dump of section %s
 
Can't get contents for section '%s'.
 
Could not find unwind info section for  
Dynamic info segment at offset 0x%lx contains %d entries:
 
Dynamic section at offset 0x%lx contains %u entries:
 
Dynamic symbol information is not available for displaying symbols.
 
Elf file type is %s
 
File: %s
 
Hex dump of section '%s':
 
Histogram for bucket list length (total of %lu buckets):
 
Library list section '%s' contains %lu entries:
 
No version information found in this file.
 
Program Headers:
 
Relocation section  
Section '%s' contains %d entries:
 
Section '%s' has no data to dump.
 
Section '%s' has no debugging data.
 
Section '.conflict' contains %lu entries:
 
Section Header:
 
Section Headers:
 
Symbol table '%s' contains %lu entries:
 
Symbol table for image:
 
Symbol table:
 
The %s section is empty.
 
There are %d program headers, starting at offset  
There are no dynamic relocations in this file.
 
There are no program headers in this file.
 
There are no relocations in this file.
 
There are no section groups in this file.
 
There are no sections in this file.
 
There are no unwind sections in this file.
 
There is no dynamic section in this file.
 
Unwind section  
Version symbols section '%s' contains %d entries:
 
start address 0x                  FileSiz            MemSiz              Flags  Align
         possible <machine>: arm[_interwork], i386, mcore[-elf]{-le|-be}, ppc, thumb
        %s -M [<mri-script]
        Flags
        Size              EntSize          Flags  Link  Info  Align
        Size              EntSize          Info              Align
        Type              Address          Offset            Link
        Type            Addr     Off    Size   ES   Lk Inf Al
        Type            Address          Off    Size   ES   Lk Inf Al
       --add-stdcall-underscore Add underscores to stdcall symbols in interface library.
       --exclude-symbols <list> Don't export <list>
       --export-all-symbols   Export all symbols to .def
       --leading-underscore   All symbols should be prefixed by an underscore.
       --no-default-excludes  Clear default exclude symbols
       --no-export-all-symbols  Only export listed symbols
       --no-leading-underscore All symbols shouldn't be prefixed by an underscore.
       --plugin NAME      Load the specified plugin
      --yydebug                 Turn on parser debugging
      [Reserved]     Arguments: %s
     Build ID:      Creation date  : %.17s
     DW_MACRO_GNU_%02x has no arguments
     Invalid size
     Location:      Module name    : %s
     Module version : %s
     Name: %s
     OS: %s, ABI: %ld.%ld.%ld
     Offset             Info             Type               Symbol's Value  Symbol's Name
     Offset             Info             Type               Symbol's Value  Symbol's Name + Addend
     Offset   Begin    End
     Offset   Begin    End      Expression
     Provider: %s
     UNKNOWN DW_LNE_HP_SFC opcode (%u)
    --add-indirect         Add dll indirects to export file.
    --add-stdcall-alias    Add aliases without @<n>
    --as <name>            Use <name> for assembler
    --base-file <basefile> Read linker generated base file
    --def <deffile>        Name input .def file
    --dllname <name>       Name of input dll to put into output lib.
    --dlltool-name <dlltool> Defaults to "dlltool"
    --driver-flags <flags> Override default ld flags
    --driver-name <driver> Defaults to "gcc"
    --dry-run              Show what needs to be run
    --entry <entry>        Specify alternate DLL entry point
    --exclude-symbols <list> Exclude <list> from .def
    --export-all-symbols     Export all symbols to .def
    --image-base <base>    Specify image base address
    --implib <outname>     Synonym for --output-lib
    --machine <machine>
    --mno-cygwin           Create Mingw DLL
    --no-default-excludes    Zap default exclude symbols
    --no-export-all-symbols  Only export .drectve symbols
    --no-idata4           Don't generate idata$4 section
    --no-idata5           Don't generate idata$5 section
    --nodelete             Keep temp files.
    --output-def <deffile> Name output .def file
    --output-exp <outname> Generate export file.
    --output-lib <outname> Generate input library.
    --quiet, -q            Work quietly
    --target <machine>     i386-cygwin32 or i386-mingw32
    --verbose, -v          Verbose
    --version              Print dllwrap version
    -A --add-stdcall-alias    Add aliases without @<n>.
    -C --compat-implib        Create backward compatible import library.
    -D --dllname <name>       Name of input dll to put into interface lib.
    -F --linker-flags <flags> Pass <flags> to the linker.
    -L --linker <name>        Use <name> as the linker.
    -M --mcore-elf <outname>  Process mcore-elf object files into <outname>.
    -S --as <name>            Use <name> for assembler.
    -U                     Add underscores to .lib
    -U --add-underscore       Add underscores to all symbols in interface library.
    -V --version              Display the program version.
    -a --add-indirect         Add dll indirects to export file.
    -b --base-file <basefile> Read linker generated base file.
    -c --no-idata5            Don't generate idata$5 section.
    -d --input-def <deffile>  Name of .def file to be read in.
    -e --output-exp <outname> Generate an export file.
    -f --as-flags <flags>     Pass <flags> to the assembler.
    -h --help                 Display this information.
    -k                     Kill @<n> from exported names
    -k --kill-at              Kill @<n> from exported names.
    -l --output-lib <outname> Generate an interface library.
    -m --machine <machine>    Create as DLL for <machine>.  [default: %s]
    -n --no-delete            Keep temp files (repeat for extra preservation).
    -p --ext-prefix-alias <prefix> Add aliases with <prefix>.
    -t --temp-prefix <prefix> Use <prefix> to construct temp file names.
    -v --verbose              Be verbose.
    -x --no-idata4            Don't generate idata$4 section.
    -z --output-def <deffile> Name of .def file to be created.
    0 (*local*)        1 (*global*)       @<file>                   Read options from <file>.
    @<file>                Read options from <file>
    Language: %s
    Last modified  :     Length:        0x%s (%s)
    Num:    Value          Size Type    Bind   Vis      Ndx Name
    Num:    Value  Size Type    Bind   Vis      Ndx Name
    Pointer Size:  %d
    Type Offset:   0x%s
    Version:       %d
    [Index]    Name
   %#06x:   Name index: %lx   %#06x:   Name: %s   %#06x: Parent %d, name index: %ld
   %#06x: Parent %d: %s
   %#06x: Rev: %d  Flags: %s   %#06x: Version: %d   %4u %08x %3u    --plugin <name>              Load the specified plugin
   --plugin <p> - load the specified plugin
   --target=BFDNAME - specify the target object format as BFDNAME
   -I --histogram         Display histogram of bucket list lengths
  -W --wide              Allow output width to exceed 80 characters
  @<file>                Read options from <file>
  -H --help              Display this information
  -v --version           Display the version number of readelf
   -r                           Ignored for compatibility with rc
  @<file>                      Read options from <file>
  -h --help                    Print this help message
  -V --version                 Print version information
   -t                           Update the archive's symbol map timestamp
  -h --help                    Print this help message
  -v --version                 Print version information
   @<file>      - read options from <file>
   ABI Version:                       %d
   Addr: 0x   Class:                             %s
   Cnt: %d
   Copy
   DWARF Version:               %d
   DW_CFA_??? (User defined call frame op: %#x)
   Data:                              %s
   Entry	Dir	Time	Size	Name
   Entry point address:                  Extended opcode %d:    Extension opcode arguments:
   File: %lx   File: %s   Flags   Flags:                             0x%lx%s
   Flags: %s  Version: %d
   Generic options:
   Index: %d  Cnt: %d     Initial value of 'is_stmt':  %d
   Length:                              %ld
   Length:                      %ld
   Length:                   %ld
   Line Base:                   %d
   Line Range:                  %d
   Machine:                           %s
   Magic:      Maximum Ops per Instruction: %d
   Minimum Instruction Length:  %d
   No emulation specific options
   Num Buc:    Value          Size   Type   Bind Vis      Ndx Name
   Num Buc:    Value  Size   Type   Bind Vis      Ndx Name
   Num:    Index       Value  Name   Number of section headers:         %ld   OS/ABI:                            %s
   Offset          Info           Type           Sym. Value    Sym. Name
   Offset          Info           Type           Sym. Value    Sym. Name + Addend
   Offset size:                 %d
   Offset:                      0x%lx
   Opcode %d has %d args
   Opcode Base:                 %d
   Options for %s:
   Options passed to DLLTOOL:
   Pointer Size:             %d
   Prologue Length:             %d
   Rest are passed unmodified to the language driver
   Section header string table index: %ld   Segment Sections...
   Segment Size:             %d
   Set ISA to %lu
   Set ISA to %s
   Set basic block
   Set column to %s
   Set epilogue_begin to true
   Set is_stmt to %s
   Set prologue_end to true
   Size of area in .debug_info section: %ld
   Size of program headers:           %ld (bytes)
   Size of section headers:           %ld (bytes)
   Size of this header:               %ld (bytes)
   Tag        Type                         Name/Value
   Type           Offset             VirtAddr           PhysAddr
   Type           Offset   VirtAddr           PhysAddr           FileSiz  MemSiz   Flg Align
   Type           Offset   VirtAddr   PhysAddr   FileSiz MemSiz  Flg Align
   Type:                              %s
   Unknown opcode %d with operands:    Version:                             %d
   Version:                           %d %s
   Version:                           0x%lx
   Version:                     %d
   Version:                  %d
   [-X32]       - ignores 64 bit objects
   [-X32_64]    - accepts 32 and 64 bit objects
   [-X64]       - ignores 32 bit objects
   [-g]         - 32 bit small archive
   [D]          - use zero for timestamps and uids/gids
   [N]          - use instance [count] of name
   [Nr] Name
   [Nr] Name              Type             Address           Offset
   [Nr] Name              Type            Addr     Off    Size   ES Flg Lk Inf Al
   [Nr] Name              Type            Address          Off    Size   ES Flg Lk Inf Al
   [P]          - use full path names when matching
   [S]          - do not build a symbol table
   [T]          - make a thin archive
   [Truncated data]
   [V]          - display the version number
   [a]          - put file(s) after [member-name]
   [b]          - put file(s) before [member-name] (same as [i])
   [c]          - do not warn if the library had to be created
   [f]          - truncate inserted file names
   [o]          - preserve original dates
   [s]          - create an archive index (cf. ranlib)
   [u]          - only replace files that are newer than current archive contents
   [v]          - be verbose
   d            - delete file(s) from the archive
   flags:         0x%04x    m[ab]        - move file(s) in the archive
   p            - print file(s) found in the archive
   q[f]         - quick append file(s) to the archive
   r[ab][f][u]  - replace existing or insert new file(s) into the archive
   s            - act as ranlib
   t            - display contents of archive
   time and date: 0x%08x  -    version:           %u
   x[o]         - extract file(s) from the archive
  %3u %3u   %s byte block:   (bytes into file)
  (bytes into file)
  Start of section headers:            (bytes)
  (inlined by)   (start == end)  (start > end)  <%d><%lx>: ...
  <%d><%lx>: Abbrev Number: %lu  Addr:   Addr: 0x  At least one of the following switches must be given:
  Convert addresses into line number/file name pairs.
  Convert an object file into a NetWare Loadable Module
  Copies a binary file, possibly transforming it in the process
  DW_MACINFO_define - lineno : %d macro : %s
  DW_MACINFO_end_file
  DW_MACINFO_start_file - lineno: %d filenum: %d
  DW_MACINFO_undef - lineno : %d macro : %s
  DW_MACINFO_vendor_ext - constant : %d string : %s
  Display information about the contents of ELF format files
  Display information from object <file(s)>.
  Display printable strings in [file(s)] (stdin by default)
  Displays the sizes of sections inside binary files
  Entries:
  Generate an index to speed access to archives
  If no addresses are specified on the command line, they will be read from stdin
  If no input file(s) are specified, a.out is assumed
  Length  Number     %% of total  Coverage
  List symbols in [file(s)] (a.out by default).
  None
  Num: Name                           BoundTo     Flags
  Offset     Info    Type                Sym. Value  Symbol's Name
  Offset     Info    Type                Sym. Value  Symbol's Name + Addend
  Offset     Info    Type            Sym.Value  Sym. Name
  Offset     Info    Type            Sym.Value  Sym. Name + Addend
  Print a human readable interpretation of a COFF object file
  Removes symbols and sections from files
  The options are:
  The options are:
  -I --input-target=<bfdname>   Set the input binary file format
  -O --output-target=<bfdname>  Set the output binary file format
  -T --header-file=<file>       Read <file> for NLM header information
  -l --linker=<linker>          Use <linker> for any linking
  -d --debug                    Display on stderr the linker command line
  @<file>                       Read options from <file>.
  -h --help                     Display this information
  -v --version                  Display the program's version
  The options are:
  -h --help        Display this information
  -v --version     Print the program's version number
  The options are:
  -q --quick       (Obsolete - ignored)
  -n --noprescan   Do not perform a scan to convert commons into defs
  -d --debug       Display information about what is being done
  @<file>          Read options from <file>
  -h --help        Display this information
  -v --version     Print the program's version number
  The options are:
  @<file>                      Read options from <file>
  The options are:
  @<file>                Read options from <file>
  -a --addresses         Show addresses
  -b --target=<bfdname>  Set the binary file format
  -e --exe=<executable>  Set the input file name (default is a.out)
  -i --inlines           Unwind inlined functions
  -j --section=<name>    Read section-relative offsets instead of addresses
  -p --pretty-print      Make the output easier to read for humans
  -s --basenames         Strip directory names
  -f --functions         Show function names
  -C --demangle[=style]  Demangle function names
  -h --help              Display this information
  -v --version           Display the program's version

  The options are:
  @<file>                Read options from <file>
  -h --help              Display this information
  -v --version           Display the program's version

  [without DW_AT_frame_base]  at   at offset 0x%lx contains %lu entries:
  command specific modifiers:
  commands:
  emulation options: 
  generic modifiers:
  optional:
  program interpreter #lines %d  #sources %d %ld: .bf without preceding function %ld: unexpected .ef
 %lu
 %s
 (header %s, data %s)
 %s %s%c0x%s never used %s exited with status %d %s has no archive index
 %s is not a library %s is not a valid archive %s section data %s: %s: address out of bounds %s: Can't open input archive %s
 %s: Can't open output archive %s
 %s: Error:  %s: Failed to read ELF header
 %s: Failed to read file header
 %s: Matching formats: %s: Multiple redefinition of symbol "%s" %s: Path components stripped from image name, '%s'. %s: Symbol "%s" is target of more than one redefinition %s: Warning:  %s: bad archive file name
 %s: bad number: %s %s: can't find module file %s
 %s: can't open file %s
 %s: cannot find section %s %s: cannot get addresses from archive %s: cannot set time: %s %s: execution of %s failed:  %s: failed to read archive header
 %s: failed to seek to next archive header
 %s: failed to skip archive symbol table
 %s: file %s is not an archive
 %s: fread failed %s: fseek to %lu failed: %s %s: invalid output format %s: invalid radix %s: no archive map to update %s: no open archive
 %s: no open output archive
 %s: no output archive specified yet
 %s: no recognized debugging information %s: no resource section %s: no symbols %s: not a dynamic object %s: not enough binary data %s: printing debugging information failed %s: read of %lu returned %lu %s: read: %s %s: supported architectures: %s: supported formats: %s: supported targets: %s: the archive has an index but no symbols
 %s: unexpected EOF %s: warning:  %s: warning: shared libraries can not have uninitialized data %s: warning: unknown size for field `%s' in struct %s:%d: Ignoring rubbish found on this line %s:%d: garbage found at end of line %s:%d: missing new symbol name %s:%d: premature end of file '%s' '%s' is not an ordinary file
 '%s': No such file '%s': No such file
 (DW_OP_call_ref in frame info) (Unknown location op) (Unknown: %s) (User defined location op) (base address)
 (declared as inline and inlined) (declared as inline but ignored) (implementation defined: %s) (inlined) (not inlined) (start == end) (start > end) (undefined) (unknown accessibility) (unknown case) (unknown convention) (unknown type) (unknown virtuality) (unknown visibility) (user defined type) (user defined) *invalid* *undefined* , <unknown> , Base:  , unknown CPU 16-byte
 2 bytes
 2's complement, big endian 2's complement, little endian 4 bytes
 4-byte
 8-byte
 :
  No symbols
 : duplicate value
 : expected to be a directory
 : expected to be a leaf
 <End of list>
 <OS specific>: %d <corrupt string table index: %3ld> <no .debug_str section> <offset is too big> <other>: %x <processor specific>: %d <string table index: %3ld> <unknown addend: %lx> <unknown: %lx> <unknown: %x> <unknown> <unknown>: %d <unknown>: %lx <unknown>: %x <unknown>: 0x%x Access Added exports to output file Adding exports to output file Address Application
 Audit library Auxiliary header:
 Auxiliary library BCD float type not supported BFD header file version %s
 Bad sh_info in group section `%s'
 Bad sh_link in group section `%s'
 Bad stab: %s
 C++ base class not defined C++ base class not found in container C++ data member not found in container C++ default values not in a function C++ object has no fields C++ reference is not pointer C++ reference not found C++ static virtual method CORE (Core file) CU: %s/%s:
 CU: %s:
 Can't create .lib file: %s: %s Can't have LIBRARY and NAME Can't open .lib file: %s: %s Can't open def file: %s Can't open file %s
 Cannot interpret virtual addresses without program headers.
 Cannot produce mcore-elf dll from archive file: %s Configuration file Contents of %s section:

 Contents of section %s: Contents of the %s section:
 Contents of the %s section:

 Convert a COFF object file into a SYSROFF object file
 Corrupt header in the %s section.
 Could not locate '%s'.  System error message: %s
 Couldn't get demangled builtin type
 Created lib file Creating library file: %s Creating stub file: %s Current open archive is %s
 DLLTOOL name    : %s
 DLLTOOL options : %s
 DRIVER name     : %s
 DRIVER options  : %s
 DW_FORM_strp offset too big: %s
 DW_OP_GNU_push_tls_address or DW_OP_HP_unknown DYN (Shared object file) Data size Deleting temporary base file %s Deleting temporary def file %s Deleting temporary exp file %s Demangled name is not a function
 Dependency audit library Displaying the debug contents of section %s is not yet supported.
 Don't know about relocations on this machine architecture
 Done reading %s Duplicate symbol entered into keyword list. ELF Header:
 ERROR: Bad section length (%d > %d)
 ERROR: Bad subsection length (%d > %d)
 EXEC (Executable file) End of Sequence

 Entry point  Enum Member offset %x Excluding symbol: %s Execution of %s failed FORMAT is one of rc, res, or coff, and is deduced from the file name
extension if not specified.  A single file name is an input file.
No input-file is stdin, default rc.  No output-file is stdout, default rc.
 Failed to print demangled template
 Failed to read in number of buckets
 Failed to read in number of chains
 File contains multiple dynamic string tables
 File contains multiple dynamic symbol tables
 File contains multiple symtab shndx tables
 File header:
 File name                            Line number    Starting address
 Filter library Flags: Generated exports file Generating export file: %s ID directory entry ID resource ID subdirectory IEEE numeric overflow: 0x IEEE string length overflow: %u
 IEEE unsupported complex type size %u
 IEEE unsupported float type size %u
 IEEE unsupported integer type size %u
 Idx Name          Size      VMA               LMA               File off  Algn Idx Name          Size      VMA       LMA       File off  Algn Import library `%s' specifies two or more dlls In archive %s:
 Input file '%s' is not readable
 Input file '%s' is not readable.
 Internal error: DWARF version is not 2, 3 or 4.
 Internal error: Unknown machine type: %d Invalid option '-%c'
 Invalid radix: %s
 Keeping temporary base file %s Keeping temporary def file %s Keeping temporary exp file %s LIBRARY: %s base: %x Last stabs entries before error:
 Library rpath: [%s] Library runpath: [%s] Library soname: [%s] Line numbers for %s (%u)
 List of blocks  List of source files List of symbols Location list starting at offset 0x%lx is not terminated.
 Machine '%s' not supported Memory section %s+%x Multiple renames of section %s Must provide at least one of -o or --dllname options NAME: %s base: %x NONE (None) NT_ARCH (architecture) NT_AUXV (auxiliary vector) NT_FPREGS (floating point registers) NT_FPREGSET (floating point registers) NT_LWPSINFO (lwpsinfo_t structure) NT_LWPSTATUS (lwpstatus_t structure) NT_PRPSINFO (prpsinfo structure) NT_PRSTATUS (prstatus structure) NT_PRXFPREG (user_xfpregs structure) NT_PSINFO (psinfo structure) NT_PSTATUS (pstatus structure) NT_TASKSTRUCT (task structure) NT_VERSION (version) NT_WIN32PSTATUS (win32_pstatus structure) N_LBRAC not within function
 Name Name                  Value           Class        Type         Size             Line  Section

 Name                  Value   Class        Type         Size     Line  Section

 Name index: %ld
 Name: %s
 NetBSD procinfo structure No %s section present

 No comp units in %s section ? No entry %s in archive.
 No filename following the -fo option.
 No location lists in .debug_info section!
 No mangling for "%s"
 No member named `%s'
 No note segments present in the core file.
 None None
 Not an ELF file - it has the wrong magic bytes at the start
 Not enough memory for a debug info array of %u entries Not needed object: [%s]
 Not used
 Nothing to do.
 OS Specific: (%x) Offset 0x%lx is bigger than .debug_loc section size.
 Only -X 32_64 is supported Only DWARF 2 and 3 aranges are currently supported.
 Only DWARF 2 and 3 pubnames are currently supported
 Opened temporary file: %s Operating System specific: %lx Option -I is deprecated for setting the input format, please use -J instead.
 Out of memory
 Out of memory allocating 0x%lx bytes for %s
 Out of memory allocating dump request table.
 Owner PT_GETFPREGS (fpreg structure) PT_GETREGS (reg structure) Pascal file name not supported Path components stripped from dllname, '%s'. Pointer size + Segment size is not a power of two.
 Print a human readable interpretation of a SYSROFF object file
 Print width has not been initialized (%d) Processed def file Processed definitions Processing def file: %s Processing definitions Processor Specific: %lx Processor Specific: (%x) REL (Relocatable file) Range lists in %s section start at 0x%lx
 Realtime
 Register %d Report bugs to %s
 Report bugs to %s.
 Scanning object file %s Section %d was not dumped because it does not exist!
 Section '%s' was not dumped because it does not exist!
 Section headers are not available!
 Sections:
 Shared library: [%s] Skipping unexpected relocation type %s
 Source file %s Stack offset %x Standalone App Struct Member offset %x Sucking in info from %s section in %s Supported architectures: Supported targets: Symbol  %s, tag %d, number %d Syntax error in def file %s:%d The address table data in version 3 may be wrong.
 The line info appears to be corrupt - the section is too small
 There are %d section headers, starting at offset 0x%lx:
 There is a hole [0x%lx - 0x%lx] in %s section.
 There is a hole [0x%lx - 0x%lx] in .debug_loc section.
 There is an overlap [0x%lx - 0x%lx] in %s section.
 There is an overlap [0x%lx - 0x%lx] in .debug_loc section.
 This instance of readelf has been built without support for a
64 bit data type and so it cannot read 64 bit ELF files.
 Too many N_RBRACs
 Tried `%s'
 Tried file: %s True
 Truncated header in the %s section.
 Type Type file number %d out of range
 Type index number %d out of range
 UNKNOWN (%*.*lx) UNKNOWN:  Unable to change endianness of input file(s) Unable to determine dll name for `%s' (not an import library?) Unable to determine the length of the dynamic string table
 Unable to determine the number of symbols to load
 Unable to find program interpreter name
 Unable to locate %s section!
 Unable to open base-file: %s Unable to open object file: %s: %s Unable to open temporary assembler file: %s Unable to read in 0x%lx bytes of %s
 Unable to read in dynamic data
 Unable to read program interpreter name
 Unable to recognise the format of the input file `%s' Unable to seek to 0x%lx for %s
 Unable to seek to end of file
 Unable to seek to end of file!
 Unable to seek to start of dynamic information
 Undefined N_EXCL Undefined symbol Unexpected demangled varargs
 Unexpected type in v3 arglist demangling
 Unhandled data length: %d
 Unknown AT value: %lx Unknown FORM value: %lx Unknown TAG value: %lx Unknown format '%c'
 Unknown note type: (0x%08x) Unknown type: %s
 Unrecognized XCOFF type %d
 Unrecognized debug option '%s'
 Unrecognized debug section: %s
 Unrecognized demangle component %d
 Unrecognized demangled builtin type
 Unrecognized form: %lu
 Unsupported EI_CLASS: %d
 Unsupported version %lu.
 Usage %s <option(s)> <object-file(s)>
 Usage: %s <option(s)> <file(s)>
 Usage: %s <option(s)> in-file(s)
 Usage: %s [emulation options] [-]{dmpqrstx}[abcDfilMNoPsSTuvV] [--plugin <name>] [member-name] [count] archive-file file...
 Usage: %s [emulation options] [-]{dmpqrstx}[abcDfilMNoPsSTuvV] [member-name] [count] archive-file file...
 Usage: %s [option(s)] [addr(s)]
 Usage: %s [option(s)] [file(s)]
 Usage: %s [option(s)] [in-file [out-file]]
 Usage: %s [option(s)] [input-file] [output-file]
 Usage: %s [option(s)] in-file
 Usage: %s [option(s)] in-file [out-file]
 Usage: %s [options] archive
 Usage: readelf <option(s)> elf-file(s)
 Using `%s'
 Using file: %s Using popen to read preprocessor output
 Using temporary file `%s' to read preprocessor output
 Using the --size-sort and --undefined-only options together VERSION %d.%d
 Value for `N' must be positive. Version %ld
 Version 4 does not support case insensitive lookups.
 Virtual address 0x%lx not located in any PT_LOAD segment.
 Visible Warning, ignoring duplicate EXPORT %s %d,%d Warning: %s: %s
 Warning: '%s' has negative size, probably it is too large Warning: '%s' is not an ordinary file Warning: changing type size from %d to %d
 Warning: could not locate '%s'.  reason: %s Warning: ignoring previous --reverse-bytes value of %d Warning: truncating gap-fill from 0x%s to 0x%x Where [%3u] 0x%lx - 0x%lx
 [%3u] 0x%lx 0x%lx  [truncated]
 `N' is only meaningful with the `x' and `d' options. `u' is not meaningful with the `D' option. `u' is only meaningful with the `r' option. `x' cannot be used on thin archives. accelerator architecture %s unknown architecture: %s,  arguments array [%d] of attributes bad ATN65 record bad C++ field bit pos or size bad dynamic symbol
 bad format for %s bad mangled name `%s'
 bad misc record bad register:  bad type for C++ method function badly formed extended line op encountered!
 bfd_coff_get_auxent failed: %s bfd_coff_get_syment failed: %s bfd_open failed open stub file: %s: %s bfd_open failed reopen stub file: %s: %s big endian blocks blocks left on stack at end byte number must be less than interleave byte number must be non-negative can not determine type of file `%s'; use the -J option can't add section '%s' can't create section `%s' can't execute `%s': %s can't get BFD_RELOC_RVA relocation type can't open %s `%s': %s can't open `%s' for output: %s can't open temporary file `%s': %s can't popen `%s': %s can't redirect stdout: `%s': %s can't set BFD default target to `%s': %s cannot delete %s: %s cannot open '%s': %s cannot open input file %s cannot open: %s: %s cannot read header cannot read line numbers code conflict conflict list found without a dynamic symbol table
 const/volatile indicator missing control data requires DIALOGEX copy from `%s' [%s] to `%s' [%s]
 copy from `%s' [unknown] to `%s' [unknown]
 could not create temporary file whilst writing archive could not determine the type of symbol number %ld
 couldn't open symbol redefinition file %s (error: %s) creating %s cursor cursor file `%s' does not contain cursor data custom section data entry data size %ld debug_add_to_current_namespace: no current file debug_end_block: attempt to close top level block debug_end_block: no current block debug_end_common_block: not implemented debug_end_function: no current function debug_end_function: some blocks were not closed debug_find_named_type: no current compilation unit debug_get_real_type: circular debug information for %s
 debug_make_undefined_type: unsupported kind debug_name_type: no current file debug_record_function: no debug_set_filename call debug_record_label: not implemented debug_record_line: no current unit debug_record_parameter: no current function debug_record_variable: no current file debug_start_block: no current block debug_start_common_block: not implemented debug_start_source: no debug_set_filename call debug_tag_type: extra tag attempted debug_tag_type: no current file debug_write_type: illegal type encountered dialog control dialog control data dialog control end dialog font point size dialog header dialogex control dialogex font information directory directory entry name dynamic section dynamic string table dynamic strings endianness unknown enum definition enum ref to %s error: the input file '%s' is empty error: the start address should be before the end address expression stack mismatch expression stack overflow expression stack underflow failed to open temporary head file: %s failed to open temporary head file: %s: %s failed to open temporary tail file: %s failed to open temporary tail file: %s: %s failed to read the number of entries from base file filename required for COFF input filename required for COFF output fixed version info flags 0x%08x:
 fontdir fontdir device name fontdir face name fontdir header function returning group cursor group cursor header group icon group icon header has children help ID requires DIALOGEX help section icon file `%s' does not contain icon data ignoring the alternative value illegal type index illegal variable index input and output files must be different input file named both on command line and with INPUT interleave must be positive internal error -- this option not implemented internal stat error on %s invalid argument to --format: %s invalid integer argument %s invalid number invalid option -f
 invalid string length invalid value specified for pragma code_page.
 length %d [ liblist string table little endian make .bss section make .nlmsections section make section menu header menuex header menuex offset menuitem menuitem header message section missing index type missing required ASN missing required ATN65 module section more than one dynamic segment
 named directory entry named resource named subdirectory no argument types in mangled string
 no children no entry %s in archive
 no entry %s in archive %s! no export definition file provided.
Creating one, but that may not be what you want no information for symbol number %ld
 no input file no input file specified no name for output file no operation specified no resources no symbols
 no type information for C++ method function none not set
 not stripping symbol `%s' because it is named in a relocation notes null terminated unicode string number of bytes to reverse must be positive and even numeric overflow offset: %s  options out of memory parsing relocs
 overflow when adjusting relocation against %s parse_coff_type: Bad type code 0x%x pointer to program headers pwait returns: %s reference parameter is not a pointer resource ID resource data resource data size resource type unknown rpc section run: %s %s section %s %d %d address %x size %x number %d nrelocs %d section 0 in group section [%5u]
 section [%5u] in group section [%5u] > maximum section [%5u]
 section [%5u] in group section [%5u] already in group section [%5u]
 section contents section data section definition at %x size %x
 section headers set .bss vma set .data size set .nlmsection contents set .nlmsections size set Address to 0x%s
 set section alignment set section flags set section size set start address shared section size %d  size: %s  skipping invalid relocation offset 0x%lx in section %s
 sorry - this program has been built without plugin support
 sp = sp + %ld stab_int_type: bad size %u stack overflow stack underflow stat failed on bitmap file `%s': %s stat failed on file `%s': %s stat failed on font file `%s': %s stat returns negative size for `%s' string table string_hash_lookup failed: %s stringtable string stringtable string length structure definition structure ref to %s structure ref to UNKNOWN struct stub section sizes subprocess got fatal signal %d support not compiled in for %s supported flags: %s symbol information symbols this target does not support %lu alternative machine codes try to add a ill language. two different operation options specified unable to copy file '%s'; reason: %s unable to open file `%s' for input.
 unable to open output file %s unable to parse alternative machine code unable to read contents of %s unable to rename '%s'; reason: %s undefined C++ object undefined C++ vtable undefined variable in ATN undefined variable in TY unexpected DIALOGEX version %d unexpected end of debugging information unexpected fixed version info version %lu unexpected fixed version signature %lu unexpected group cursor type %d unexpected group icon type %d unexpected number unexpected record type unexpected string in C++ misc unexpected version string unexpected version type %d unknown unknown ATN type unknown BB type unknown C++ encoded name unknown C++ visibility unknown TY code unknown builtin type unknown demangling style `%s' unknown format type `%s' unknown mac unknown section unknown virtual character for baseclass unknown visibility character for baseclass unknown visibility character for field unnamed $vb type unrecognized --endian type `%s' unrecognized -E option unrecognized C++ abbreviation unrecognized C++ default type unrecognized C++ misc record unrecognized C++ object overhead spec unrecognized C++ object spec unrecognized C++ reference type unrecognized cross reference type unrecognized section flag `%s' unrecognized: %-7lx unresolved PC relative reloc against %s unsupported ATN11 unsupported ATN12 unsupported C++ object type unsupported IEEE expression operator unsupported menu version %d unsupported or unknown Dwarf Call Frame Instruction number: %#x
 unsupported qualifier unwind info unwind table user defined:  vars %d version data version def version def aux version definition section version length %d does not match resource length %lu version need version need aux (2) version need aux (3) version string table version symbol data version var info version varfileinfo wait: %s warning: CHECK procedure %s not defined warning: EXIT procedure %s not defined warning: FULLMAP is not supported; try ld -M warning: No version number given warning: START procedure %s not defined warning: could not create temporary file whilst copying '%s', (error: %s) warning: could not locate '%s'.  System error message: %s warning: input and output formats are not compatible warning: symbol %s imported but not in import list will produce no output, since undefined symbols have no size. writing stub | <unknown> Project-Id-Version: de
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2014-01-15 11:12+0000
Last-Translator: Dennis Baudys <Unknown>
Language-Team: deutsch <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:01+0000
X-Generator: Launchpad (build 18115)
 

Symbole von %s:

 

Symbole von %s[%s]:

 

Undefinierte Symbole von %s:

 

Undefinierte Symbole von %s[%s]:

 
      [Programminterpreter anfordern: %s] 
    Adresslänge
 
    Adresslänge
 
    Offset	Name
 
  Beginn der Programm-Header:           
 Opcodes:
 
 Abbildung von Sektion nach Segment:
 
 Die Verzeichnistabelle ist leer.
 
 Die Dateinamentabelle ist leer.
 
 Die folgenden Schalter sind optional:
 
%s:     Dateiformat %s
 
‚%s‛ Relozierungssektion an Offset 0x%lx enhält %ld Bytes:
 
Adresstabelle:
 
Archivindex:
 
Assembly-Ausgabe von Sektion %s
 
Inhalt der Sektion ‚%s‛ kann nicht erhalten werden.
 
Abwicklungsinfosektion (unwind) kann nicht gefunden werden für  
Dynamisches Infosegment an Offset 0x%lx enhält %d Einträge:
 
Dynamische Sektion an Offset 0x%lx enthält %u Einträge:
 
Dynamische Symbolinformation ist für die Anzeige der Symbole nicht verfügbar.
 
Elf-Datei-Typ ist %s
 
Datei: %s
 
Hex-Ausgabe der Sektion ‚%s‛:
 
Histogramm der Bucketlistenl-Länge (von insgesamt %lu Buckets):
 
Bibliothekslistensektion ‚%s‛ enthält %lu Einträge:
 
Keine Versionsinformation in dieser Datei gefunden.
 
Programm-Header:
 
Relozierungssektion  
Sektion ‚%s‛ enthält %d Einträge:
 
Sektion ‚%s‛ enhält keine Daten zum Ausgeben.
 
Sektion ‚%s‛ enthält keine Debugging-Daten.
 
Sektion ‚.conflict‛ enthält %lu Einträge:
 
Sektions-Header:
 
Sektions-Header:
 
Symboltabelle ‚%s‛ enthält %lu Einträge:
 
Symboltabelle für Image:
 
Symboltabelle:
 
Die %s-Sektion ist leer.
 
Es gibt %d Programm-Header, beginnend an Offset  
Es gibt keine dynamischen Relozierungen in dieser Datei.
 
Es gibt keine Programm-Header in dieser Datei.
 
Es gibt keine Relozierungen in dieser Datei.
 
Es gibt keine Sektionsgruppen in dieser Datei.
 
Es gibt keine Sektionen in dieser Datei.
 
Es gibt keine Abwicklungssektionen (unwind) in dieser Datei
 
Es gibt keine dynamische Sektion in dieser Datei.
 
Abwicklungssektion (unwind)  
Versionssymbol-Sektion ‚%s‛ enthält %d Einträge:
 
Startadresse 0x                  DateiGr            SpeiGr              Flags  Ausr.
         mögliche <Maschine>: arm[_interwork], i386, mcore[-elf]{-le|-be}, ppc, thumb
        %s -M [<mri-script]
        Flags
        Größe             Eintragsgröße    Flags  Link  Info  Ausr.
        Größe             Eintragsgröße    Info              Ausr.
        Typ               Adresse          Offset            Link
        Typ             Adr      Off    Größe  ES   Lk Inf Ar
        Typ             Adresse          Off    Größe  ES   Lk Inf Ar
       --add-stdcall-underscore Unterstreichung zu stdcall-Symbolen in der Schnittstellenbibliothek hinzufügen.
       --exclude-symbols <Liste> Symbole in <Liste> nicht exportieren
       --export-all-symbols   Alle Symbole nach .def exportieren
       --leading-underscore Allen Symbolen einen Unterstrich voranstellen
       --no-default-excludes  Vorgabe-Ausschlusssymbole löschen
       --no-export-all-symbols  Nur gelistete Symbole exportieren
       --no-leading-underscore Keinen Symbolen einen Unterstrich voranstellen
       --plugin NAME      Angegebene Erweiterung laden
      --yydebug                 Parser-Debugging einschalten
      [Reserviert]     Argumente: %s
     Build-ID:      Erstellungsdatum  : %.17s
     DW_MACRO_GNU_%02x hat keine Argumente
     Ungültige Größe
     Ort:      Modul-Name    : %s
     Modul-Version : %s
     Name: %s
     OS: %s, ABI: %ld.%ld.%ld
     Offset             Info             Typ                Symbolwert      Symbolname
     Offset             Info             Typ                Symbolwert      Symbolname + Summand
     Offset   Anfang   Ende
     Offset   Anfang   Ende     Ausdruck
     Anbieter: %s
     UNBEKANNTER DW_LNE_HP_SFC-Opcode (%u)
    --add-indirect             DLL-Indirects zur Exportdatei hinzufügen.
    --add-stdcall-alias        Aliase ohne @<n> hinzufügen
    --as <Name>                Als Assembler <Name> benutzen
    --base-file <Basisdatei>     Vom Linker erzeugte Basisdatei einlesen
    --def <deffile>            .def-Eingabedatei benennen
    --dllname <Name>           Name der Eingabe-DLL, die in die Ausgabebibliothek eingesetzt werden soll.
    --dlltool-name <dlltool>   Vorgabe ist „dlltool“
    --driver-flags <Flags>     Aufheben der Vorgabe-Flags von ld
    --driver-name <Treiber>    Vorgabe ist „gcc“
    --dry-run                  Anzeigen, was ausgeführt werden muss
    --entry <Eintrag>          Einen alternativen DLL-Einstiegspunkt angeben
    --exclude-symbols <Liste>    Symbole in <Liste> aus .def ausschließen
    --export-all-symbols         Alle Symbole nach .def exportieren
    --image-base <Basis>       Image-Basisadresse angeben
    --implib <Ausgabename>     Synonym für --output-lib
    --machine <Maschine>
    --mno-cygwin               Mingw DLL erzeugen
    --no-default-excludes        Vorgabe-Ausschlussymbole löschen
    --no-export-all-symbols      Nur .drectve-Symbole exportieren
    -no-idata4                Keine idata$4-Sektion erzeugen
    -no-idata5                Keine idata$5-Sektion erzeugen
    --nodelete                 Temporäre Dateien beibehalten.
    --output-def <deffile>     .def-Ausgabedatei benennen
    --output-exp <Ausgabename> Exportdatei erzeugen.
    --output-lib <Ausgabename> Eingabebibliothek erzeugen.
    --quiet, -q                Schweigsam arbeiten.
    --target <Maschine>        i386-cygwin32 oder i386-mingw32
    -verbose, -v               Ausführliche Ausgabe
    --version                  Version von dllwrap ausgeben
    -A --add-stdcall-alias    Aliase ohne @<n> hinzufügen.
    -C --compat-implib        Rückwärtskompatible Importbibliothek erzeugen.
    -D --dllname <name>       Name der Eingabe-DLL, der in die Schnittstellen-Bibliothek eingesetzt werden soll.
    -F --linker-flags <Flags> An den Linker diese <Flags> übergeben.
    -L --linker <Name>        Als Linker <Name> verwenden.
    -M --mcore-elf <Ausgabename> mcore-elf-Objektdateien in <Ausgabename> verarbeiten.
    -S --as <Name>            Als Assembler <Name> benutzen.
    -U                         Unterstriche zu .lib hinzufügen
    -U --add-underscore Unterstreichung zu allen Symbolen in der Schnittstellenbibliothek hinzufügen.
    -V --version              Programmversion anzeigen.
    -a --add-indirect         DLL-Indirects zur Exportdatei hinzufügen.
    -b --base-file <basefile> Vom Linker erzeugte Basisdatei einlesen.
    -c --no-idata5            Die idata$5-Sektion nicht erzeugen.
    -d --input-def <deffile>  Name der .def-Datei, die eingelesen werden soll.
    -e --output-exp <Ausgabename> Eine Exportdatei erzeugen.
    -f --as-flags <Flags>     An den Assembler diese <Flags> übergeben.
    -h --help                 Diese Information anzeigen.
    -k                         Von den exportierten Namen @<n> entfernen
    -k --kill-at              Von exportierten Namen @<n> entfernen.
    -l --output-lib <Ausgabename> Eine Schnittstellen-Bibliothek erzeugen.
    -m --machine <Maschine>   Erzeuge als DLL für <Maschine>. [Vorgabe: %s]
    -n --no-delete            Temporäre Dateien beibehalten (wiederholen für weitergehende Erhaltung).
    -p --ext-prefix-alias <Präfix> Aliase mit <Präfix> hinzufügen.
    -t --temp-prefix <Präfix> Einen <Präfix> bei der Zusammensetzung der Namen der temporären Dateien verwenden.
    -v --verbose              Ausführliche Ausgabe.
    -x --no-idata4            Die idata$4-Sektion nicht erzeugen.
    -z --output-def <deffile> Name der .def-Datei, die erzeugt werden soll.
    0 (*lokal*)        1 (*global*)       @<Datei>                  Optionen aus <Datei> einlesen.
    @<Datei>                   Optionen aus <Datei> einlesen
    Sprache: %s
    Zuletzt geändert  :     Länge: 0x%s (%s)
    Num:    Wert           Size Type    Bind   Vis      Ndx Name
    Num:    Wert   Size Typ     Bind   Vis      Ndx Name
    Zeigergröße:   %d
    Typversatz: 0x%s
    Version:       %d
    [Index]    Name
   %#06x:   Namensindex: %lx   %#06x:   Name: %s   %#06x: Elter %d, Namensindex: %ld
   %#06x: Elter %d: %s
   %#06x: Rev: %d  Flags: %s   %#06x: Version: %d   %4u %08x %3u    --plugin <Name>              Die angegebene Erweiterung laden
   --plugin <p> – Gewünschte Erweiterung laden
   --target=BFDNAME - Spezifiziere das Zielformat als BFDNAME
   -I --histogram         Histogramm der Bucket-Listen-Längen anzeigen
  -W --wide              Ausgabe erlauben, 80 Zeichen zu überschreiten
  @<Datei>               Optionen aus <Datei> einlesen
  -H --help              Diese Information anzeigen
  -v --version           Versionsnummer von readelf anzeigen
   -r                           Ignoriert wegen Kompatibilität mit rc
  @<Datei>                     Optionen aus <Datei> einlesen
  -h --help                    Diesen Hilfetext ausgeben
  -V --version                 Versionsinformation ausgeben
   -t Zeitstempel der Symboltabelle aktualisieren
  -h --help Diese Hilfe anzeigen
  -v --version Versionsinformationen anzeigen
   @<Datei>     - Lesen der Optionen aus <Datei>
   ABI-Version:                       %d
   Adr:  0x   Klasse:                            %s
   Zähler: %d
   Kopieren
   DWARF Version:                %d
   DW_CFA_???(Benutzerdefinierter Aufruf aus OP: %#x)
   Daten:                             %s
   Eintrag	Dir	Zeit	Größe	Name
   Einstiegspunktadresse:                  Erweiterter Opcode %d:    Erweiterung für Opcode-Argumente:
   Datei: %lx   Datei: %s   Flags   Flags:                             0x%lx%s
   Flags: %s  Version: %d
   Allgemeine Optionen:
   Index: %d  Zähler: %d     Initialer Wert von ‚is_stmt‛: %d
   Länge:                               %ld
   Länge:                        %ld
   Länge:                    %ld
   Zeilenbasis:                  %d
   Zeilenreichweite:             %d
   Maschine:                          %s
   Magic:      Maximale Ops pro Anweisung: %d
   Minimale Instruktionslänge:   %d
   Keine emulationsspezifischen Optionen
   Num Buc:    Wert           Größe  Typ    Bind Vis      Ndx Name
   Num Buc:    Wert   Größe  Typ    Bind Vis      Ndx Name
   Num:    Index       Wert   Name   Anzahl der Sektions-Header:        %ld   OS/ABI:                            %s
   Offset          Info           Typ            Sym.-Wert     Sym.-Name
   Offset          Info           Typ            Sym.-Wert     Sym.-Name+Summand
   Versatzgröße:                 %d
   Versatz:                      0x%lx
   Opcode %d hat %d Argumente
   Opcode-Basis:                 %d
   Optionen für %s:
   An DLLTOOL übergebene Optionen:
   Zeigergröße:              %d
   Länge des Prologs:            %d
   Der Rest wird unverändert an den Sprachentreiber übergeben.
   Sektions-Header Stringtabellen-Index: %ld   Segmentsektionen...
   Segmentgröße:             %d
   ISA wird auf %lu gesetzt
   ISA auf %s setzen
   Basic-Block wird gesetzt
   Spalte auf %s festlegen
   epilogue_begin wird auf wahr gesetzt
   is_stmt auf %s festlegen
   prologue_end wird auf wahr gesetzt
   Größe des Bereichs in der .debug_info-Sektion: %ld
   Größe der Programm-Header:         %ld (Byte)
   Größe der Sektions-Header:         %ld (bytes)
   Größe dieses Headers:              %ld (Byte)
   Tag       Typ                          Name/Wert
   Typ            Offset             VirtAdr            PhysAdr
   Typ            Offset   VirtAdr            PhysAdr            DateiGr  SpeiGr   Flg Ausr.
   Typ            Offset   VirtAdr    PhysAdr    DateiGr SpeiGr  Flg Ausr.
   Typ:                               %s
   Unbekannter Opcode %d mit Operanden:    Version:                             %d
   Version:                           %d %s
   Version:                           0x%lx
   Version: %d
   Version:                  %d
   [-X32]       - ignoriert 64 Bit Objekte
   [-X32_64]    - akzeptiert 32 und 64 Bit Objekte
   [-X64]       - ignoriert 32 Bit Objekte
   [-g]         - 32 bit kleines Archiv
   [D]          – 0 für Zeitstempel und uids/gids benutzen
   [N]          - Instanz [Nummer] des Namens benutzen
   [Nr] Name
   [Nr] Name              Typ              Adresse           Offset
   [Nr] Name              Typ             Adr      Off    Größe  ES Flg Lk Inf Ar
   [Nr] Name              Typ             Adresse          Off    Größe  ES Flg Lk Inf Al
   [P]          - Komplette Pfadnamen beim Vergleichen benutzen
   [S]          - Keine Symboltabelle erzeugen
   [T]          – Ein schlankes Archiv erstellen
   [Abgeschnittene Daten]
   [V]          - Versionsnummer anzeigen
   [a]          - Datei(en) nach [Mitgliedsname] setzen
   [b]          - Datei(en) vor [Mitgliedsname] setzen (gleichbedeutend mit [i])
   [c]          - Keine Warnung ausgeben, wenn die Bibliothek erzeugt werden musste
   [f]          - Eingefügte Dateinamen kürzen
   [o]          - Ursprüngliches Datum erhalten
   [s]          - Einen Archivindex erzeugen (cf. ranlib)
   [u]          - Nur Dateien ersetzen, die neuer als der derzeitige Archivinhalt sind
   [v]          - Ausführliche Ausgabe
   d            - Datei(en) aus dem Archiv löschen
   Flags:         0x%04x    m[ab]        - Datei(en) im Archiv verschieben
   p            - Im Archiv gefundene Dateien ausgeben
   q[f]         - Datei(en) schnell an das Archiv anhängen
   r[ab][f][u]  - Existierende Datei(en) ersetzen oder neue zu dem Archiv hinzufügen
   s            – als ranlib agieren
   t            - Inhalt des Archivs anzeigen
   Zeit und Datum: 0x%08x  -    Version:           %u
   x[o]         - Datei(en) aus dem Archiv extrahieren
  %3u %3u   %s-Byte-Block:   (Bytes in Datei)
  (Bytes in Datei)
  Beginn der Sektions-header:            (Bytes)
  (inline-ersetzt von)   (Start == Ende)  (Start > Ende)  <%d><%lx>: …
  <%d><%lx>: Zahl abkürzen: %lu  Adr:   Adr:  0x  Mindestend einer der folgenden Schalter muss angegeben werden:
  Konvertierung der Adressen in Zeilennummern/Dateinamen-Paare.
  Eine Objektdatei in ein von NetWare ladbares Modul konvertieren
  Kopiert eine binäre Datei und transformiert sie bei Bedarf
  DW_MACINFO_define - Zeilennr.: %d Makro: %s
  DW_MACINFO_end_file
  DW_MACINFO_start_file - Zeilennr.: %d Dateinnr.: %d
  DW_MACINFO_undef - Zeilennr.: %d Makro: %s
  DW_MACINFO_vendor_ext - Konstante : %d Zeichenkette : %s
  Information über den Inhalt von ELF-Dateien anzeigen
  Informationen von Objektdatei(en) ausgeben.
  Druckbare Zeichenketten in [Datei(en)] anzeigen (Standardeingabe als Vorgabe)
  Zeigt die Größen der Sektionen innerhalb binärer Dateien an
  Einträge:
  Einen Index erzeugen, um den Archivzugriff zu beschleunigen
  Wenn in der Befehlszeile keine Adressen angegeben sind, werden sie von der Standardeingabe gelesen.
  Wenn keine Eingabedateien angegeben werden, wird a.out angenommen
  Länge   Nummer     %% von insg. Abdeckung
  Symbole der [Datei(en)] auflisten (a.out als Vorgabe).
  Nichts
  Num: Name                           Gebund. an  Flags
  Offset     Info    Typ                 Sym.-Wert   Symbolname
  Offset     Info    Typ                 Sym.-Wert   Symbolname + Summand
  Offset     Info    Typ             Sym.-Wert  Sym.-Name
  Offset     Info    Typ             Sym.-Wert  Sym. Name + Summand
  Eine menschenlesbare Interpretation einer COFF-Objektdatei anzeigen
  Entfernen von Symbolen und Sektionen von Dateien
  Die Optionen lauten:
  Die Optionen lauten:
  -I --input-target=<bfdname>   Eingehendes binäres Dateiformat festlegen
  -O --output-target=<bfdname>  Ausgehendes binäres Dateiformat festlegen
  -T --header-file=<Datei>      Für NLM-Header-Information <Datei> einlesen
  -l --linker=<Linker>          Für sämtliches Linken <Linker> verwenden
  -d --debug                    Auf stderr die Linker-Befehlszeile anzeigen
  @<Datei>                      Optionen aus <Datei> einlesen.
  -h --help                     Diese Information anzeigen
  -v --version                  Programmversion anzeigen
  Die Optionen lauten:
  -h --help        Diese Information anzeigen
  -v --version     Versionsnummer des Programms ausgeben
  Die Optionen lauten:
  -q --quick       (veraltet - wird ignoriert)
  -n --noprescan   Keinen Scan durchführen um commons in defs zu konvertieren
  -d --debug       Informationen darüber ausgeben, was getan wird
  @<DATEI>         Optionen aus <DATEI> einlesen
  -h --help        Diese Information anzeigen
  -v --version     Programmversionsnr. anzeigen
  Die Optionen sind:
  @<Datei> Optionen aus <Datei> lesen
  Folgende Optionen stehen zur Verfügung:
  @<Datei>               Optionen aus der Datei <Datei> einlesen
  -a --addresses        Adressen ausgeben
  -b --target=<Bfdname>  Format der Ausgabebinärdatei angeben
  -e --exe=<Ausführbare>  Name der Eingabedatei (Standard ist a.out)
  -i --inlines           Innenliegende Funktionen auslagern
  -j --section=<Name>    Zum Abschnitt relative Versatzlängen statt Adressen ausgeben
  -p --pretty-print      Die Ausgabe für Menschen einfach lesbar gestalten
  -s --basenames         Verzeichnisnamen weglassen
  -f --functions         Funktionsnamen ausgeben
  -C --demangle[=style] Zusatzinformationen zu Funktionsnamen erzeugen
  -h --help              Diese Information anzeigen
  -v --version           Die Programmversion anzeigen

  Die Optionen lauten:
  @<Datei>               Optionen aus <Datei> einlesen
  -h --help              Diese Information anzeigen
  -v --version           Programmversion anzeigen

  [ohne DW_AT_frame_base]  bei   an Offset 0x%lx enhält %lu Einträge:
  Befehlsspezifische Modifikatoren:
  Befehle:
  Emulationsoptionen: 
  Allgemeine Modifikatoren:
  optional:
  Programminterpreter #Zeilen %d  #Quellen %d %ld: .bf ohne vorhergehende Funktion %ld: Unerwartetes .ef
 %lu
 %s
 (Kopf %s, Datei %s)
 %s %s%c0x%s nie benutzt %s mit Status %d beendet %s hat keinen Archivindex
 %s ist keine Bibliothek %s ist kein gültiges Archiv %s-Sektionsdaten %s: %s: Adresse außerhalb des zulässigen Bereichs %s: Eingabearchiv %s konnte nicht geöffnet werden
 %s: Ausgabearchiv %s konnte nicht geöffnet werden
 %s: Fehler:  %s: ELF-Header konnte nicht gelesen werden
 %s: Lesen des Datei-Headers fehlgeschlagen
 %s: Passende Formate: %s: Mehrfache Definition des Symbols „%s“ %s: Pfadbestandteile aus Image-Namen entfernt. ‚%s‛. %s: Symbol „%s“ ist Ziel von mehr als einer Neudefinition %s: Warnung:  %s: Archivdateiname fehlerhaft
 %s: Fehlerhafte Nummer: %s %s: Moduldatei %s konnte nicht gefunden werden
 %s: Datei %s konnte nicht geöffnet werden
 %s: kann Abschnitt nicht finden %s %s: kann keine Adressen aus dem Archiv beziehen %s: Zeit kann nicht gesetzt werden: %s %s: Ausführen von %s gescheitert:  %s: Einlesen des Archiv-Headers fehlgeschlagen
 %s: Springen zum nächsten Archiv-Header fehlgeschlagen
 %s: Überspringen der Archivsymboltabelle fehlgeschlagen
 %s: Datei %s ist kein Archiv
 %s: fread fehlgeschlagen %s: fseek zu %lu fehlgeschlagen: %s %s: Ungültiges Ausgabeformat %s: Ungültige Basiszahl %s: Keine Archivabbildung zum Aktualisieren %s: Kein geöffnetes Archiv
 %s: Kein geöffnetes Ausgabearchiv
 %s: Es wurde kein Ausgabearchiv angegeben
 %s: Nicht erkannte Debugging-Information %s: Keine Ressourcensektion %s: Keine Symbole %s: Kein dynamisches Objekt %s: Nicht genug binäre Daten %s: Ausgabe der Debugging-Information fehlgeschlagen %s: Einlesen von %lu gab %lu zurück %s: read: %s %s: Unterstützte Architekturen: %s: Unterstützte Formate: %s: Unterstützte Ziele: %s: Das Archive hat einen Index aber keine Symbole
 %s: Unerwartetes EOF %s: Warnung:  %s: Warnung: gemeinsame Bibliotheken können keine uninitialisierten Daten haben %s: Warnung: Unbekannte Größe für Feld ‚%s‛ in Struct %s:%d: Ignoriere Unsinn in dieser Zeile %s:%d: Unsinn am Ende der Zeile gefunden %s:%d: Neuer Symbolname fehlt %s:%d: Verfrühtes Ende der Datei ‚%s‛ ‚%s‛ ist keine gewöhnliche Datei
 ‚%s‛: Keine solche Datei '%s': Keine solche Datei
 (DW_OP_call_ref in frame info) (Unbekannte Location-Operation) (Unbekannt: %s) (Benutzerdefinierte Location-Operation) (Basisadresse)
 (als inline deklariert und inlined) (als inline deklariert aber ignoriert) (Implementierung festgelegt: %s) (inlined) (nicht inlined) (Start == Ende) (Start > Ende) (undefiniert) (unbekannter Zugriff) (unbekannter Fall) (unbekannte Konvention) (unbekannter Typ) (unbekannte Virtualität) (unbekannte Sichtbarkeit) (benutzerdefinierter Typ) (benutzerdefiniert) *ungültig* *nicht definiert* , <unbekannt> , Basis:  , unbekannte CPU 16-Byte
 2 Bytes
 2er-Komplement, Big-Endian 2er-Komplement, Little-Endian 4 Bytes
 4-Byte
 8-Byte
 :
  Keine Symbole
 : Doppelter Wert
 : In Form eines Verzeichnisses erwartet
 : In Form eines Blatts erwartet
 <Listenende>
 <OS-spezifisch>: %d <fehlerhafter Stringtabellen-Index: %3ld> <keine .debug_str-Sektion> <Offset ist zu groß> <andere>: %x <Prozessorspezifisch>: %d <Stringtabellen-Index: %3ld> <unbekannter Summand: %lx> <unbekannt: %lx> <Unbekannt: %x> <unbekannt> <unbekannt>: %d <Unbekannt>: %lx <Unbekannt>: %x <unbekannt>: 0x%x Zugriff Exports zur Ausgabedatei hinzugefügt Exports werden zur Ausgabedatei hinzugefügt Adresse Anwendung
 Prüfungsbibliothek Hilfs-Header:
 Hilfsbibliothek (AUX) BCD-Float-Typ wird nicht unterstützt BFD-Header Dateiversion %s
 Fehlerhafte sh_info in Gruppensektion ‚%s‛
 Fehlerhafter sh_link in Gruppensektion ‚%s‛
 Fehlerhaftes stab: %s
 C++-Basisklasse nicht definiert C++ Basisklasse nicht im Container gefunden C++-Daten-Member nicht im Container gefunden C++-Vorgabewerte nicht in einer Funktion C++-Objekt hat keine Felder C++-Referenz ist kein Zeiger C++-Referenz nicht gefunden C++ statische virtuelle Methode CORE (Core-Datei) CU: %s/%s:
 CU: %s:
 .lib-Datei konnte nicht erstellt werden: %s: %s Es ist nicht möglich BIBLIOTHEK und NAMEN zu erhalten .lib-Datei konnte nicht geöffnet werden: %s: %s .def-Datei %s konnte nicht geöffnet werden Datei %s konnte nicht geöffnet werden
 Virtuelle Adressen können ohne Programmheader nicht ausgewertet werden.
 mcore-elf dll kann nicht aus Archivdatei erzeugt werden: %s Konfigurationsdatei Inhalt der %s-Sektion:

 Inhalt von Abschnitt %s: Inhalt des Abschnitts %s:
 Inhalt der %s-Sektion:

 COFF-Objektdatei in eine SYSROFF-Objektdatei konvertieren
 Beschädigter Kopfteil im Abschnitt %s.
 ‚%s‛ konnte nicht gefunden werden.  Systemfehlermeldung: %s
 Entmangelter eingebauter Typ kann nicht erhalten werden
 lib-Datei wurde erzeugt Bibliotheksdatei wird erzeugt: %s Erzeuge Stub-Datei: %s Derzeit geöffnetes Archiv ist %s
 DLLTOOL Name     : %s
 DLLTOOL Optionen : %s
 TREIBER Name     : %s
 TREIBER Optionen : %s
 DW_FORM_strp-Versatz zu groß: %s
 DW_OP_GNU_push_tls_address oder DW_OP_HP_unknown DYN (geteilte Objektadatei) Datengröße Temporäre Basisdatei %s wird gelöscht Temporäre def-Datei %s wird gelöscht Temporäre exp-Datei %s wird gelöscht Entmangelter Name ist keine Funktion
 Abhängigkeitsprüfungs-Bibliothek Zeigen der Debug-Inhalte der Sektion %s wird derzeit nicht unterstützt.
 Relozierungsart dieser Maschinenarchitektur nicht bekannt
 Lesen von %s beendet Doppeltes Symbol wurde der Wortliste hinzugefügt. ELF-Header:
 FEHLER: Fehlerhafte Sektionslänge (%d > %d)
 FEHLER: Fehlerhafte Untersektionslänge (%d > %d)
 EXEC (ausführbare Datei) Ende der Sequenz

 Einstiegspunkt  Enum-Mitglied Abstand %x Ausgeschlossenes Symbol: %s Ausführen von %s gescheitert: FORMAT ist entweder rc, res, oder coff, und wird abgeleitet von der Dateinamen-
erweiterung wenn nicht anderweitig angegeben. Ein einzelner Dateiname ist eine
Eingabedatei. Keine Eingabedatei bedeutet Standardeingabe, Vorgabe rc.
Keine Ausgabedatei bedeutet Standardausgabe, Vorgabe rc.
 Ausgabe des entmangelten Templates fehlgeschlagen
 Einlesen der Anzahl der Buckets fehlgeschlagen
 Einlesen der Anzahl der Ketten fehlgeschlagen
 Datei enthält mehrere dynamische Zeichenkettentabellen
 Datei enthält mehrere dynamische Symboltabellen
 Datei enthält mehrere symtab shndx Tabellen
 Datei-Kopfteil:
 Dateiname                           Zeilennummer    Startadresse
 Filterbibliothek Flags: Exportdatei wurde erzeugt Exportdatei wird erzeugt: %s ID-Verzeichniseintrag ID-Ressource ID-Unterverzeichnis IEEE numerischer Überlauf: 0x IEEE Überlauf der Zeichenkettenlänge: %u
 IEEE nicht unterstützte komplexe Typgröße %u
 IEEE nicht unterstützte Float-Typgröße %u
 IEEE nicht unterstützte Integer-Typgröße: %u
 Idx Name          Größe     VMA               LMA               Datei-Off Ausr. Idx Name          Größe     VMA       LMA       Datei-Off Ausr. Import-Bibliothek »%s« spezifiziert zwei oder mehr DLLs Im Archiv %s:
 Eingabedatei »%s« ist nicht lesbar
 Eingabedatei ‚%s‛ ist nicht lesbar.
 Interner Fehler: DWARF-Version ist nicht 2, 3 oder 4.
 Interner Fehler: Unbekannter Maschinentyp: %d Ungültige Option '-%c'
 Ungültige Basiszahl: %s
 Temporäre Basisdatei %s wird beibehalten Temporäre def-Datei %s wird beibehalten Temporäre exp-Datei %s wird beibehalten BIBLIOTHEK: %s Basis: %x Letzte Stabs-Einträge vor dem Fehler:
 Bibliothek rpath: [%s] Bibliothek runpath: [%s] soname der Bibliothek: [%s] Zeilennummern für %s (%u)
 Liste von Blöcken  Liste von Quelltextdateien Liste von Symbolen Die Location-Liste beginnend am Offset 0x%lx ist nicht abgeschlossen.
 Maschine ‚%s‛ wird nicht unterstützt Speicherabschnitt %s+%x Mehrfache Umbenennungen der Sektion %s Es muss mindestens eine -o oder -dllname Option angegeben werden NAME: %s Basis: %x NONE (Nichts) NT_ARCH (Architektur) NT_AUXV (Hilfsvektor) NT_FPREGS (Fließkommaregister) NT_FPREGSET (Fließkommaregister) NT_LWPSINFO (lwpsinfo_t-Struktur) NT_LWPSTATUS (lwpstatus_t-Struktur) NT_PRPSINFO (prpsinfo-Struktur) NT_PRSTATUS (prstatus-Struktur) NT_PRXFPREG (user_xfpregs-Struktur) NT_PSINFO (psinfo-Struktur) NT_PSTATUS (pstatus-Struktur) NT_TASKSTRUCT (task-Struktur) NT_VERSION (Version) NT_WIN32PSTATUS (win32_pstatus-Struktur) N_LBRAC nicht innerhalb einer Funktion
 Name Name                  Wert            Klasse       Typ          Größe            Zeile Sektion

 Name                  Wert    Klasse       Typ          Größe    Zeile Sektion

 Namensindex: %ld
 Name: %s
 NetBSD procinfo-Struktur Keine %s-Sektion vorhanden

 Keine Comp Units in %s-Sektion? Kein Eintrag %s im Archiv.
 Kein Dateiname hinter der -fo Option.
 Keine Location-Listen in .debug_info section!
 Kein Mangling für "%s"
 Kein Mitglied mit dem Namen ‚%s‛
 Keine Segmente in der Core-Datei vorhanden.
 Nichts Keine
 Keine ELF-Datei - Falsche Magic-Bytes am Anfang
 Nicht genug Speicher für Debug-Informations-Array mit %u Einträgen Unbenötigtes Objekt: [%s]
 Nicht benutzt
 Nichts zu tun.
 OS-spezifisch: (%x) Offset 0x%lx ist größer als .debug_loc-Sektionsgröße.
 Nur -X 32_64 wird unterstützt Nur DWARF 2 and 3 Aranges werden derzeit unterstützt.
 Nur Pubnames der DWARF-Versionen 2 und 3 werden derzeit unterstützt.
 Geöffnete temporäre Datei: %s Betriebssystemspezifisch: %lx Option -I ist veraltet zum Setzen des Eingabeformats, stattdessen -J benutzen
 Kein Speicher übrig
 Kein Speicher übrig beim Zuweisen von 0x%lx Byte für %s
 Speicherüberlauf beim Zuordnen der Dump-Abfragetabelle.
 Besitzer PT_GETFPREGS (fpreg-Struktur) PT_GETREGS (reg-Struktur) Pascal-Dateiname nicht unterstützt Pfadbestandteile vom DLL-Namen entfernt. ‚%s‛. Zeigergröße + Segmentgröße ist nicht durch zwei teilbar.
 Eine lesbare Auswertung einer SYSROFF-Objektdatei ausgeben
 Druckbreite wurde nicht initialisiert (%d) .def-Datei verarbeitet Definitionen wurden verarbeitet Verarbeiten der .def-Datei: %s Verarbeiten der Definitionen Prozessorspezifisch: %lx Prozessorspezifisch: (%x) REL (relozierbare Datei) Range-Listen in %s-Sektion beginnen bei 0x%lx
 Echtzeit
 Register %d Melden Sie Fehler an %s
 Melden Sie Programmfehler an %s.
 Objektdatei %s wird untersucht Sektion %d wurde nicht ausgegeben, weil sie nicht existiert!
 Abschnitt »%s« wurde nicht abgefragt, weil dieser nicht existiert!
 Sektions-Header nicht verfügbar!
 Sektionen:
 Gemeinsame Bibliothek [%s] Unerwarteter Relozierungstyp %s wird übersprungen
 Quelltextdatei %s Stack-Versatz %x Eigenständige Appl. Struct-Mitglied Abstand %x Informationen aus %s-Sektion in %s werden eingelesen Unterstützte Architekturen: Unterstützte Ziele: Symbol  %s, Marker %d, Nummer %d Syntaxfehler in def-Datei %s:%d Die Daten der Adresstabelle in Version 3 sind möglicherweise fehlerhaft.
 Die Zeileninformation scheint defekt zu sein, die Sektion ist zu klein
 Es gibt %d Sektions-Header, beginnend an Offset 0x%lx:
 Es gibt eine Lücke bei [0x%lx - 0x%lx] in der %s-Sektion.
 Es gibt eine Lücke [0x%lx - 0x%lx] in der .debug_loc-Sektion
 Es gibt eine Überschneidung bei [0x%lx - 0x%lx] in der %s-Sektion.
 Es gibt eine Überschneidung [0x%lx - 0x%lx] in der .debug_loc-Sektion
 Diese Fassung von readelf wurde ohne Unterstützung für 64 Bit Datentypen
übersetzt und kann daher keine 64 Bit ELF-Dateien lesen
 Zu viele N_RBRACs
 Es wurde ‚%s‛ probiert
 Ausprobierte Datei: %s Wahr
 Abgeschnittener Kopfteil im Abschnitt %s.
 Typ Typdateinummer %d außerhalb des zulässigen Bereichs
 Typindexnummer %d außerhalb des zulässigen Bereichs
 UNBEKANNT (%*.*lx) UNBEKANNT:  Endianess der Eingabedatei(en) kann nicht geändert werden Konnte den DLL-Namen für »%s« nicht bestimmen. (Keine Import-Bibliothek?) Die Länge der dynamischen Zeichenkettentabelle kann nicht bestimmt werden
 Anzahl der zu ladenden Symbole kann nicht bestimmt werden
 Programminterpretername kann nicht gefunden werden
 %s-Sektion kann nicht gefunden werden!
 Die Basisdatei konnte nicht geöffnet werden: %s Öffnen der Objektdatei nicht möglich: %s: %s Die temporäre Assembler-Datei konnte nicht geöffnet werden: %s 0x%lx Bytes von %s können nicht eingelesen werden
 Dynamische Daten können nicht gelesen werden
 Der Name des Programminterpreters kann nicht gelesen werden
 Format von Eingabedatei ‚%s‛ kann nicht bestimmt werden Positionierung nach 0x%lx für %s nicht möglich
 Es kann nicht zum Ende der Datei gesprungen werden
 Suche bis zum Ende der Datei fehlgeschlagen!
 Suche zum Anfang der dynamischen Informationen fehlgeschlagen
 Undefiniertes N_EXCL Undefiniertes Symbol Unerwartete entmangelte varargs
 Unerwarteter Typ im v3-Arglist-Demangling
 Unbehandelte Datenlänge %d
 Unbekannter AT-Wert: %lx Unbekannter FORM-Wert: %lx Unbekannter TAG-Wert: %lx Unbekanntes Format ‚%c‛
 Unbekannter Notiztyp: (0x%08x) Unbekannter Typ: %s
 Nicht erkannter XCOFF-Typ %d
 Nicht erkannte Debug-Option '%s'
 Nicht erkannte Debug-Sektion: %s
 Nicht erkannter Demangling-Bestandteil %d
 Nicht erkannter entmangelter eingebauter Typ
 Nicht erkannte Form: %lu
 Nicht unterstützte EI_CLASS: %d
 Version %lu nicht unterstützt.
 Verwendung: %s <Option(en)> <Objektdatei(en)>
 Verwendung: %s <Option(en)> <Datei(en)>
 Verwendung: %s <Option(en)> Eingabedatei(en)
 Gebrauch: %s [Emulationsoptionen][-]{dmpqrstx}[abcDfilMNoPsSTuvV] [--plugin <Name>] [Member-Name] [Anzahl] Archivdatei Datei …
 Gebrauch: %s [Emulationsoption] [-]{dmpqrstx}[abcDfilMNoPsSTuvV] [Member-Name] [Anzahl] Archivdatei Datei …
 Verwendung: %s [Optionen(en)] [Adresse(n)]
 Verwendung: %s [Option(en)] [Datei(en)]
 Verwendung: %s [Option(en)] [Eingabedatei [Ausgabedatei]]
 Verwendung: %s [Option(en)] [Eingabedatei] [Ausgabedatei]
 Verwendung: %s [Option(en)] Eingabedatei
 Verwendung: %s [Option(en)] Eingabedatei [Ausgabedatei]
 Verwendung: %s [Optionen] Archiv
 Verwendung: readelf <Option(en)> elf-Datei(en)
 ‚%s‛ wird benutzt
 Verwendete Datei: %s popen wird benutzt, um Präprozessorausgabe zu lesen
 Temporäre Datei ‚%s‛ wird benutzt, um Präprozessorausgabe zu lesen
 --size-sort and --undefined-only Optionen werden gemeinsam benutzt VERSION %d.%d
 Der Wert für ‚N‛ muss positiv sein. Version %ld
 Version 4 unterstützt kein Suchen ohne Beachtung der Groß-/Kleinschreibung.
 Virtuelle Adresse 0x%lx findet sich in keinem PT_LOAD-Segment.
 Sichtbar Warnung, doppelter EXPORT %s %d,%d wird ignoriert Warnung: %s: %s
 Warnung: »%s« hat eine negative Größe, möglicherweise ist es zu groß Warnung: ‚%s‛ ist keine gewöhnliche Datei Warnung: Typgröße wird von %d nach %d geändert.
 Warnung: ‚%s‛ Konnte nicht gefunden werden. Grund: %s Warnung: Vorheriger --inverse-byte-Wert von %d wird ignoriert Warnung: Füllung der Lücken von 0x%s to 0x%x wird gekürzt Wo [%3u] 0x%lx - 0x%lx
 [%3u] 0x%lx 0x%lx  [abgeschnitten]
 ‚N‛ ist nur sinnvoll mit den ‚x‛- und ‚d‛-Optionen. Kombination von »u« und »D« nicht sinnvoll. ‚u‛ ist nur sinnvoll mit der ‚r‛-Option. »x« kann nicht auf dünne Archive angewendet werden. Accelerator Architektur %s ist unbekannt Architektur: %s,  Argumente Array [%d] von Attribute Fehlerhafter ATN65-Record Falsche C++-Field-Bit Position oder Größe Falsches dynamisches Symbol
 fehlerhaftes Format für %s Fehlerhaft gemangelter Name ‚%s‛
 fehlerhafter Misc-Record Ungültiges Register:  Falscher Typ für C++-Methodenfunktion Fehlerhafte gebildete erweiterte Zeilenoperation entdeckt!
 bfd_coff_get_auxent fehlgeschlagen: %s bfd_coff_get_syment fehlgeschlagen: %s bfd_open konnte die Testdatei %s nicht öffnen: %s bfd_open konnte die Testdatei %s nicht wieder öffnen: %s Big Endian Blöcke Am Ende sind Blöcke auf dem Stapel verblieben Byte-Nummer muss kleiner als Verschachtelung sein Byte-Nummer darf nicht negativ sein Typ der Datei ‚%s‛ kann nicht bestimmt werden; -J Option benutzen Abschnitt »%s« kann nicht hinzugefügt werden Abschnitt »%s« kann nicht erzeugt werden ‚%s‛ kann nicht ausgeführt werden: %s BFD_RELOC_RVA-Relozierungstyp kann nicht erhalten werden %s: ‚%s‛ kann nicht geöffnet werden: %s Kann ‚%s‛ für Ausgabe nicht öffnen: %s Temporäre Datei‚%s‛ kann nicht geöffnet werden: %s ‚%s‛ kann via popen nicht geöffnet werden: %s Standardausgabe kann nicht umgeleitet werden: ‚%s‛: %s Das BFD-Vorgabeziel kann nicht auf ‚%s‛: %s gesetzt werden %s konnte nicht gelöscht werden: %s ‚%s‛ konnte nicht geöffnet werden: %s Eingabedatei %s kann nicht geöffnet werden %s kann nicht geöffnet werden: %s Es ist nicht möglich, den Header zu lesen Zeilennummern können nicht gelesen werden Code Konflikt Es wurde eine Konfliktliste ohne dynamische Symboltabelle gefunden
 const/volatile-Indikator fehlt Steuerelementdaten benötigen DIALOGEX Kopieren von ‚%s‛ [%s] nach ‚%s‛ [%s]
 Kopieren von ‚%s‛ [unbekannt] nach ‚%s‛ [unbekannt]
 Erstellen einer temporären Datei während einer Archiverstellung nicht möglich Der Typ des Symbols mit der Nummer %ld konnte nicht bestimmt werden
 Symbol-Redefinitionsdatei %s konnte nicht geöffnet werden (Fehler: %s) Erzeugen von %s Cursor Cursor-Datei ‚%s‛ enthält keine Cursordaten custom-Sektion Dateneintrag Datengröße %ld debug_add_to_current_namespace: Keine aktuelle Datei debug_end_block: Versuch, einen Top-Level-Block zu schließen debug_end_block: Kein aktueller Block debug_end_common_block: Nicht implementiert debug_end_function: Keine aktuelle Funktion debug_end_function: Ein paar Blöcke wurden nicht geschlossen debug_find_named_type: Keine aktuelle Comp Unit debug_get_real_type: Zirkuläre Debug-Information für %s
 debug_make_undefined_type: Typ wird nicht unterstützt debug_name_type: Keine aktuelle Datei debug_record_function: Kein Aufruf von debug_set_filename debug_record_label: Nicht implementiert debug_record_line: Keine aktuelle Einheit debug_record_parameter: Keine aktuelle Funktion debug_record_variable: Keine aktuelle Datei debug_start_block: Kein aktueller Block debug_start_common_block: Nicht implementiert debug_start_source: Kein Aufruf von debug_set_filename debug_tag_type: Es wurde ein zusätzliches Tag probiert debug_tag_type: Keine aktuelle Datei debug_write_type: Auf ungültigen Typ gestoßen Dialog-Steuerelement Dialog-Steuerelement Daten Dialog-Steuerelement Ende Punktgröße der Dialogschrift Dialog-Header DialogEx-Steuerelement DialogEx-Schriftinformation Verzeichnis Verzeichniseintragsname Dynamische Sektion Dynamische Zeichenkettentabelle Dynamische Zeichenketten Endian unbekannt Enum-Definition Enum-Verweis auf %s Fehler: Eingabedatei ‚%s‛ ist leer Fehler: Die Startadresse sollte vor der Endadresse liegen. Keine Übereinstimmung in Ausdrucksstapel Ausdrucksstapel-Überlauf Ausdrucksstapel-Unterlauf Öffnen der temporären Head-Datei fehlgeschlagen: %s Öffnen einer temporären Kopfdatei gescheitert: %s: %s Öffnen der temporären Tail-Datei fehlgeschlagen: %s Konnte die temoräre Tail-Datei %s nicht laden: %s Anzahl der Einträge aus der Basisdatei kann nicht gelesen werden Dateiname benötigt für COFF-Eingabe Dateiname benötigt für COFF-Ausgabe Feste Versionsinfo Flags 0x%08x:
 Schriftenverzeichnis Schriftenverzeichnis-Gerätename Schriftenverzeichnis Schriftartname Schriftenverzeichnis-Header Funktionsrücksprung Group-Cursor Group-Cursor-Header Gruppen-Icon Group-Icon-Header hat Kinder Hilfe-ID benötigt DIALOGEX help-Sektion Icon-Datei ‚%s‛ enthält keine Icon-Daten Alternativer Wert wird ignoriert Ungültiger Typ-Index Ungültiger Variablenindex Ein- und Ausgabedateien müssen unterschiedlich sein Eingabedatei sowohl in der Befehlszeile als auch mit INPUT benannt Verschachtelung muss postiv sein Interner Fehler -- Diese Option ist nicht implementiert Interner Status-Fehler in %s Ungültiges Argument für --format: %s Ungültiges Integerargument %s Ungültige Nummer Ungültige Option -f
 Ungültige Zeichenkettenläge Ungültiger Wert für »pragma code_page« angegeben.
 Länge %d [ liblist-Zeichenkettentabelle Little Endian .bss-Sektion anfertigen .nlmsections-Sektion anfertigen Sektion anfertigen Menu-Header MenuEx-Header MenuEx-Offset Menüpunkt Menupunkt-Header message-Sektion Indextyp fehlt Benötigtes ASN wird vermisst Benötigtes ATN65 wird vermisst module-Sektion Mehr als ein dynamisches Segment
 Benannter Verzeichniseintrag Benannte Ressource Benanntes Unterverzeichnis Keine Argumenttypen in gemangelter Zeichenkette
 keine Kinder Kein Eintrag %s im Archiv
 Kein Eintrag %s in dem Archiv %s! Es wurde keine Export-Definitionsdatei angegeben
Es wird eine erzeugt, aber das könnte nicht das sein, was Sie möchten. Keine Informationen zum Symbol mit der Nummer %ld
 Keine Eingabedatei Keine Eingabedatei angegeben Kein Name für Ausgabedatei Keine Operation angegeben Keine Ressourcen Keine Symbole
 Keine Typinformation für C++-Methodenfunktion nichts nicht gesetzt
 Symbol »%s« wird nicht entfernt, weil es in der Umlagerungstabelle enthalten ist. Notizen Nullterminierte Unicode-Zeichenkette Anzahl der Bytes, die umgedreht werden sollen, muss positiv und gerade sein Numerischer Überlauf Versatz: %s  Optionen Speicherüberlauf während der Analyse von Relocs
 Überlauf beim Einstellen der Relozierung gegen %s parse_coff_type: Fehlerhafter Typcode 0x%x Zeiger auf Programm-Header pwait liefert: %s Referenzparamenter ist kein Zeiger Ressourcen-ID Ressourcendaten Ressourcendatengröße Ressourcentyp unbekannt rpc-Sektion Ausführen: %s %s Abschnitt %s %d %d Adresse %x Größe %x Nummer %d Nrelocs %d Sektion 0 in Gruppensektion [%5u]
 Sektion [%5u] in Gruppensektion [%5u] > maximale Sektion [%5u]
 Sektion [%5u] in Gruppensektion [%5u] ist bereits in Gruppensektion [%5u]
 Inhalt des Abschnitts Sektionsdaten Sektionsdefinition bei %x Größe %x
 Sektions-Header .bss vma setzen .data-Größe setzen .nlmsection-Inhalte setzen .nlmsections-Größe setzen Adresse auf 0x%s gesetzt
 Sektionsausrichtung setzen Sektionsflags setzen Sektionsgröße setzen Startadresse setzen shared-Sektion Größe %d  Größe: %s  Ungültiger Relozierungs-Offset 0x%lx in Sektion %s wird übersprungen
 Dieses Programm wurde leider ohne Unterstützung von Erweiterungen erstellt.
 sp = sp + %ld stab_int_type: Fehlerhafte Größe %u Stapelüberlauf Stapelunterlauf stat auf Bitmap-Datei ‚%s‛ fehlgeschlagen: %s stat auf Datei ‚%s‛ fehlgeschlagen: %s stat auf Schriftdatei ‚%s‛ fehlgeschlagen: %s stat gibt negativen Wert für ‚%s‛ zurück Zeichenkettentabelle string_hash_lookup fehlgeschlagen: %s Stringtabellen-String Stringtabellen-Stringlänge Strukturdefinition Strukturverweis auf %s Strukturverweis auf UNBEKANNT-Struktur stub-Sektionsgrößen Unterprozess erhielt schwerwiegendes Signal %d Unterstützung für %s nicht einkompiliert Unterstützte Flags: %s Symbolinformation Symbole Dieses Ziel unterstützt keine %lu alternativen Maschinencodes Versuch, eine kranke Sprache hinzuzufügen. Zwei unterschiedliche Operationsoptionen angegeben Datei »%s« kann nicht kopiert werden; Grund: %s Datei »%s« kann nicht zur Eingabe geöffnet werden.
 Ausgabedatei %s kann nicht geöffnet werden Alternativer Maschinencode kann nicht eingelesen werden Inhalt von %s kann nicht gelesen werden »%s« kann nicht umbenannt werden. Grund: %s Undefiniertes C++-Objekt Undefinierter C++ vtable Undefinierte Variable in ATN Undefinierte Variable in TY Unerwartete DIALOGEX Version %d Unerwartetes Ende der Debugging-Information Unerwartete feste Versionsinfo-Version %lu Unerwartete feste Versionssignatur %lu Unerwarteter Group-Cursor-Typ %d Unerwarteter Gruppen-Icon-Typ %d Unerwartete Nummer unerwarteter Record-Typ Unerwartete Zeichenkette in C++-Misc Unerwarteter Versionsstring Unerwarteter Versionstyp %d unbekannt Unbekannter ATN-Typ Unbekannter BB-Typ Unbekannter C++-enkodierter Name Unbekannte C++-Sichtbarkeit Unbekannter TY-Code Unbekannter eingebauter Typ Unbekannter Demangling-Stil ‚%s‛ Unbekannter Formattyp ‚%s‛ Unbekannte Mac Unbekannte Sektion Unbekannter virtueller Character für Basisklasse Unbekannter Sichtbarkeits-Character für Basisklasse Unbekannter Sichtbarkeits-Character für Feld Unbenannter $vb-Typ Nicht erkannter --endian Typ ‚%s‛ Nicht erkannte -E Option Nicht erkannte C++-Abkürzung Nicht erkannter C++-Vorgabetyp Nicht erkannter C++-Misc-Record Nicht erkannte C++-Objekt-Overhead-Spezifikation Nicht erkannte C++-Objekt-Spezifikation Nicht erkannter C++-Referenz-Typ Nicht erkannter Cross-Reference-Typ Nicht erkanntes Sektionsflag ‚%s‛ Nicht erkannt: %-7lx Ungelöste relative PC-Relozierung gegen %s Nicht unterstütztes ATN11 Nicht unterstütztes ATN12 Nicht unterstützter C++-Objekt-Typ Nicht unterstützter IEEE-Ausdrucksoperator Nicht unterstützte Menüversion %d Nicht unterstützte oder unbekannte Anweisungsnummer des Dwarf-Aufrufrahmens: %#x
 Unbekannter Kennzeichner Abwicklungsinfo (unwind) Tabelle abwickeln (unwind) Benutzerdefiniert:  Variablen %d Versionsdaten Versionsdef. Versionsdef AUX Versionsdefinitions-Sektion Versionslänge %d passt nicht zur Ressourcenlänge %lu Versionsbedarf Versionsbedarf AUX (2) Versionsbedarf AUX (3) Versionszeichenketten-Tabelle Versionssymboldaten Version-Var-Info Version varfileinfo Warten: %s Warnung: CHECK-Prozedur %s nicht definiert Warnung: EXIT-Prozedur %s nicht definiert Warnung: FULLMAP wird nicht unterstützt; ld -M versuchen Warnung: Keine Versionsnummer angegeben Warnung: START-Prozedur %s nicht definiert Warnung: Temporäre Dateiliste konnte während des Kopierens von »%s« nicht erzeugt werden, (Fehler: %s) Warnung: %s konnte nicht aufgefunden werden. Systemfehlermeldung: %s Warnung: Ein- und Ausgabeformat sind nicht kompatibel Warnung: symbol %s importiert aber nicht in Importliste Es wird keine Ausgabe erzeugt, da undefinierte Symbole keine Größe haben. Stub schreiben | <unbekannt> 