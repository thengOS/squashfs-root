��    @         $       0  :   !0     \0  (   q0     �0  ;   �0  %   �0  @   1  8   L1  �   �1  V   O2  R   �2  M   �2  O   G3  ?   �3  N   �3  G   &4  ;   n4  >   �4  C   �4  �   -5  P   �5  <   6  M   W6  �   �6  O   ,7  F   |7  O   �7  L   8  H   `8  N   �8  <   �8  Q   59  8   �9  j   �9  f   +:  O   �:  �   �:  C   �;  Q   �;  =   %<  9   c<  B   �<  O   �<  H   0=  M   y=  Q   �=  Q   >  ?   k>  I   �>  J   �>  I   @?  T   �?  J   �?  ?   *@  K   j@  ?   �@  5   �@  ?   ,A  C   lA  0   �A  T   �A  �   6B  8   �B  G   C  =   JC  A   �C  H   �C  A   D  N   UD  J   �D  P   �D  N   @E  �   �E  M   #F  D   qF  @   �F  4   �F  >   ,G  D   kG  >   �G  >   �G  R   .H  Y   �H  @   �H  Q   I  6   nI  ;   �I  @   �I  I   "J  J   lJ  O   �J  R   K  Q   ZK  G   �K  F   �K  A   ;L  �   }L  R   M  ;   ZM  U   �M  S   �M  �   @N  >   �N  F   O  R   NO  9   �O  P   �O  Q   ,P  J   ~P  L   �P  �   Q  >   �Q     �Q     �Q     �Q  I   R  �   NR  B   S  B   FS  O   �S     �S  L   YT  R   �T  <   �T  F   6U  ?   }U  O   �U  O   V  >   ]V  x   �V  ;   W  ;   QW  A   �W  O   �W  9   X  E   YX  M   �X  H   �X  @   6Y  ;   wY  B   �Y  N   �Y  G   EZ  E   �Z  3   �Z  Q   [  �   Y[  S   �[  Q   0\  A   �\  �   �\  <   U]  H   �]  M   �]  8   )^  T   b^  5   �^  >   �^  P   ,_  D   }_  C   �_  A   `  "   H`  $   k`  '   �`  3   �`     �`     �`     a     a     4a     8a     Ua  (   oa     �a  %   �a  )   �a  '   b  $   0b     Ub     gb     zb  &   �b     �b     �b  !   �b     c  $   $c  8   Ic  <   �c      �c  /   �c     d     /d     Kd  "   gd  f   �d     �d     e     ,e  =   Ke     �e     �e  '   �e  (   �e     f  !   -f     Of  $   gf  #   �f  ,   �f  '   �f  5   g  *   ;g  0   fg  B   �g  /   �g  )   
h  .   4h  6   ch  :   �h     �h  2   �h      i     9i     Wi     si  M   �i  ,   �i     �i  ,   j  ,   Jj  '   wj  -   �j      �j  (   �j  (   k  7   @k  &   xk  #   �k     �k     �k     l     l  	   l      l     4l  2   Cl  L   vl     �l     �l  )   �l     m  '   +m     Sm     mm     �m     �m     �m     �m  Y   �m  8   <n  <   un  9   �n  -   �n  <   o     Wo     to  [   �o  (   �o     p     9p      Lp     mp  3   �p  3   �p     �p     
q     $q  %   @q     fq     �q     �q  	   �q     �q     �q     �q     r  "   'r  #   Jr     nr     �r  ,   �r  +   �r     �r  1   s  0   Ks  >   |s  *   �s  P   �s  H   7t  .   �t  $   �t     �t  ;   �t      +u  $   Lu  (   qu  -   �u  .   �u  3   �u     +v  6   Fv     }v  :   �v  J   �v     w     )w  )   6w     `w  
   �w     �w  !   �w  *   �w  %   �w  D   x  *   Ix     tx     �x  $   �x  %   �x     �x  6   	y  (   @y  !   iy  (   �y     �y     �y      �y     z     .z  ,   Gz     tz  N   �z     �z     �z  "   �z  *   {      J{  !   k{     �{  '   �{  (   �{     �{  4   �{  0   1|  1   b|  )   �|  !   �|  0   �|  >   }     P}     i}  2   �}     �}     �}     �}     ~  8   ~     M~     \~     k~     �~     �~  5   �~     �~          $     A     P     l     �     �     �  7   �     �  '   �  $   F�      k�     ��     ��  "   ��  '   ܀     �  4   �  8   K�     ��  
   ��  �   ��     e�     r�  :   y�     ��     Ђ  *   �     �     �     *�     6�     O�  0   e�     ��  7   ��     ��  J   ��     A�     [�     v�  4   ��     ��     ք  #   �     �     &�     9�     B�     `�     i�     ��     ��     ��     ͅ     ��  *    �  5   +�     a�  9   n�     ��     ��  &   ߆  w   �  c   ~�     �  
   ��  -   �  =   2�  #   p�     ��     ��     Έ  +   �     �     1�     F�     a�  -   p�  b   ��  N   �  E   P�     ��  8   ��  "   �  ;   �     D�  )   Q�     {�     ��     ��  1   ��     �  ,   �     �  &   +�  (   R�     {�     ��  ,   ��  -   ƌ  +   �  <    �  k   ]�  &   ɍ     ��     �  \   (�  2   ��  	   ��       -   ʎ  /   ��     (�  $   5�     Z�  +   w�  3   ��     ׏  1   �  2   $�  ,   W�  ;   ��     ��  "   Ӑ     ��  $   �     4�  "   Q�  @   t�     ��     ɑ     �     ��     �  /   �     I�     [�     m�     �     ��  6   ��  (   ��     	�  !   �     A�     ]�  )   }�     ��     Ɠ  Q   Փ  L   '�  )   t�  L   ��     �  |   �  X   p�  #   ɕ  *   �     �  $   !�  3   F�  *   z�  "   ��     Ȗ  5   �  5   �  �   R�  ^   ӗ  3   2�  3   f�     ��     ��     ��     ˘     �  !   ��     �  #   &�  (   J�     s�     z�  	   ��     ��  )   ��     Ù     י     �  '   �     -�     I�     e�  :   m�      ��     ɚ     ښ     �     ��  �  
�  F   ��     E�  3   ^�     ��  C   ��  5   �  �   �  :   ��  �   ٞ  �   ��  �   >�  �   Š  �   S�  =   �  J   %�  �   p�  C   ��  ?   8�  O   x�  �   ȣ  �   ��  >   !�  ~   `�  �   ߥ  N   k�  x   ��  O   3�     ��  u   �  {   y�  G   ��  �   =�  K   ��  j   �  f   w�  v   ު  �   U�  L   ��  �   A�  E   ì  E   	�  G   O�  �   ��  N   "�     q�  w   �  �   i�  N   �  O   9�  y   ��  �   �  �   ��  R   �  ~   `�  G   ߲  F   '�  ;   n�  w   ��  H   "�  8   k�  �   ��  �   '�  G   �  P   3�  :   ��  H   ��  L   �  D   U�  �   ��  v   �  �   ��  �   "�  �   ��  L   ��  H   ͺ  �   �  E   ��  L   �  u   .�  E   ��  M   �  �   8�  �   Խ  H   b�  �   ��  I   5�  N   �     ο  N   N�  x   ��  }   �  �   ��  �   a�  �   ��  x   s�  �   ��  �   m�  M   >�  H   ��  K   ��  w   !�  �   ��  I   ^�  v   ��  {   �  ?   ��  �   ��  �   h�  O   ��  |   A�  �   ��  B   X�     ��     ��     ��  d   ��  �   7�  B   �  B   P�  |   ��  �   �  v   ��  �   �  D   ��  z   ��  F   b�  �   ��  �   4�  B   ��  �   �  @   ��  z   ��  P   A�  �   ��  @   �  E   Y�  {   ��  Q   �  O   m�  @   ��  M   ��  �   L�  :   ��  O   �  8   a�  |   ��  �   �  �   ��  �   :�  D   ��  �   ,�  ?   ��  w   ��  G   t�  J   ��  �   �  ?   ��  7   ��  �   �  E   ��  J   ��  H   2�  6   {�  6   ��  -   ��  U   �     m�     y�     ��  *   ��     ��  "   ��     ��  .   �      5�  &   V�  *   }�  9   ��  7   ��     �     -�  "   @�  .   c�     ��     ��  ,   ��  (   ��  (   �  C   @�  N   ��  *   ��  H   ��  &   G�  $   n�  !   ��  ,   ��  z   ��  &   ]�  !   ��  '   ��  K   ��  &   �     A�  B   `�  /   ��  #   ��  .   ��     &�  &   E�  "   l�  9   ��  *   ��  S   ��  /   H�  4   x�  O   ��  ;   ��  3   9�  <   m�  O   ��  :   ��     5�  K   S�     ��  "   ��      ��     �  m   �  *   ��  !   ��  5   ��  *   �  )   .�  +   X�  $   ��  ,   ��  +   ��  I   �  -   L�  4   z�  #   ��  #   ��     ��     ��     �     �     8�  J   D�  g   ��     ��     �  4   .�  &   c�  @   ��      ��  "   ��     �     -�     D�     c�  h   |�  ?   ��  A   %�  <   g�  G   ��  S   ��  A   @�  )   ��  }   ��  7   *�  '   b�     ��  0   ��  #   ��  Q   ��  P   O�     ��  '   ��  (   ��  3   �  .   C�     r�  &   ��     ��     ��     ��  "   ��      �  8   9�  9   r�     ��  !   ��  8   ��  0   &�  "   W�  2   z�  /   ��  X   ��  3   6�  V   j�  [   ��  @   �  .   ^�  #   ��  W   ��  8   	�  .   B�  8   q�  7   ��  5   ��  c   �  '   |�  B   ��      ��  N   �  W   W�     ��     ��  8   ��  '   �  
   /�     :�  2   A�  9   t�  7   ��  V   ��  %   =�  #   c�  &   ��  ,   ��  <   ��  (   �  B   A�  5   ��  /   ��  5   ��  '    �  )   H�  -   r�     ��  &   ��  :   ��  $     G   C     �     �  )   �  C   �  ?    0   X    � C   � D   �      C   = <   � :   � >   � $   8 I   ] L   � &   � "    L   >    �    �    �    � A   �    +    ;    K (   b '   � 3   �    �    � "       @    S    q "   �    �    � M   �    > A   S .   � #   �    �     '   & 5   N    � B   � ?   �     	 
   )	 �   4	    
 
   
 7   &
 #   ^
    �
 .   �
    �
    �
    �
 '   �
     =   1    o �   �    J X   e !   � #   �     9    "   R    u -   �    �    �    � )   �    ( %   4 '   Z '   �     � !   � 6   � 0   $ R   U    � B   �    � &    '   ; �   c z   �    m 
   � 2   � �   � )   k (   � "   � (   � ?   
 !   J    l #       � @   � w   � G   u K   � !   	 I   + 3   u S   �    � 6       G    Y #   w 6   � "   � ;   �    1 6   E 8   |    �    � 9   � :    N   M U   � �   � 5    %   � "   � s   � K   r    �    � Y   � 6   1    h +   v 4   � 6   � <    %   K A   q B   � '   � o       � *   � *   � 9   � 3   6 )   j P   �     � &       -    @    T 6   g F   � F   � F   ,  ;   s  )   �  N   �  6   (!    _! '   ~!    �!     �! <   �! 1   !"    S" f   c" Y   �" 2   $# R   W#    �# �   �# �   I$ Q   �$ ;   %    Y% )   b% Q   �% 7   �% 1   & ,   H& O   u& Q   �& �   ' n   �' =   _( =   �(    �(    �( !   �( #   )    C) G   ])    �) >   �) (   �)    * 	   "*    ,*    8* /   M* &   }*    �* !   �* >   �* #   !+ #   E+    i+ R   u+ )   �+    �+    ,     ,    6,        *  �  �       �       #  �   R          �  9   �   �  �  n  �   !  �   �  `   0  `  �          \       t   p  v  �   g       (   �  �   �   u   �  �      u  '  �  �       ,   5   Z   �   �      �  {      _  -  �  �               �                 #   �      [   �         \  v   �  �  .      �       m       �   f   �  �  {   �  �   '  >  J   �  +  �   "   �      �   �       �  �  �  :  �  
  �   �   %  �   �   M              �   |   L  �   y           A         (  a      $  #  �   �  B   )      �        <   i  �  �  �  g      �  ]       �    �   �      r  V  �   =          I          �  �  <  �      �    �  8   A          �   �     _               5       2  �      [  7   �  N      $             �   U      .      �       )       �   ?  7                     b        "         �  �   w         &  �  �  Q   ,      �   '   0                  �   l   $     �           �   �     �  �   �  k   +  �   �   m      �               ?      �   ^  s   �     �                q   �   d  �   �       z   �        �   �  J      �  b      �       �       l          �       "  �  �       �              6  N      &  �  �  	   �      �      n          �      �      L   �         G       �   �   �         >         w   y  t       Z            !  �          �   	  6       �      �      �  �   �      X  �  j   �   �  �       �   d   �   3          �      �           �   2  <  :   O  �  �     @   Y  W  %                 K  �  �       K   @  �     E   �  
   �  &   �            �     k  �          +       �      �   �    �   �                �      �   �   2   q      }    �  �       �   �      �         T   �      X          �  -   E  Y   �  B      �       �              3  �  �   �       4  P   }     �     8      �  �  �  �    C   H          Q  �   M   �   R     �      7  O       a      �  �      x   V   z  T  -  �   .   4   ,  �           ;         �         �   �  �  �  �   �  *            8  �  �     *          �   	              f  �  �  ~  �   1  �   |  �       �   I   �   �   p     �   �  �  :  
  �             �   �  x  e   �      �       /  �       3       i      1          �   �       %  �   �   G  �   �   ^   �      �  P     >         ]  c  �               c   �  �  �        9    �     �          �     9     r   !                  �   �       @  �      �         �           o    h          �    s    F  S             �   ?         �      �   5  �   4              H   �  �   /    e  �   U              �   �  /   D        �  �  ;  �   1   )       �   �      D       o   h   �  0     �  �  �   ;       ~   �           �       �  S   =        �   �     �   C      �         F   �   6  �  �      �   �  �   (  =   W       �      j  �  �   
    The file is already fully retrieved; nothing to do.

 
%*s[ skipping %sK ] 
%s received, redirecting output to %s.
 
%s received.
 
Originally written by Hrvoje Niksic <hniksic@xemacs.org>.
 
REST failed, starting from scratch.
        --accept-regex=REGEX        regex matching accepted URLs
        --ask-password              prompt for passwords
        --auth-no-challenge         send Basic HTTP authentication information
                                     without first waiting for the server's
                                     challenge
        --backups=N                 before writing file X, rotate up to N backup files
        --bind-address=ADDRESS      bind to ADDRESS (hostname or IP) on local host
        --body-data=STRING          send STRING as data. --method MUST be set
        --body-file=FILE            send contents of FILE. --method MUST be set
        --ca-certificate=FILE       file with the bundle of CAs
        --ca-directory=DIR          directory where hash list of CAs is stored
        --certificate-type=TYPE     client certificate type, PEM or DER
        --certificate=FILE          client certificate file
        --config=FILE               specify config file to use
        --connect-timeout=SECS      set the connect timeout to SECS
        --content-disposition       honor the Content-Disposition header when
                                     choosing local file names (EXPERIMENTAL)
        --content-on-error          output the received content on server errors
        --crl-file=FILE             file with bundle of CRLs
        --cut-dirs=NUMBER           ignore NUMBER remote directory components
        --default-page=NAME         change the default page name (normally
                                     this is 'index.html'.)
        --delete-after              delete files locally after downloading them
        --dns-timeout=SECS          set the DNS lookup timeout to SECS
        --egd-file=FILE             file naming the EGD socket with random data
        --exclude-domains=LIST      comma-separated list of rejected domains
        --follow-ftp                follow FTP links from HTML documents
        --follow-tags=LIST          comma-separated list of followed HTML tags
        --ftp-password=PASS         set ftp password to PASS
        --ftp-stmlf                 use Stream_LF format for all binary FTP files
        --ftp-user=USER             set ftp user to USER
        --ftps-clear-data-connection    cipher the control channel only; all the data will be in plaintext
        --ftps-fallback-to-ftp          fall back to FTP if FTPS is not supported in the target server
        --ftps-implicit                 use implicit FTPS (default port is 990)
        --ftps-resume-ssl               resume the SSL/TLS session started in the control connection when
                                         opening a data connection
        --header=STRING             insert STRING among the headers
        --hsts-file                 path of HSTS database (will override default)
        --http-password=PASS        set http password to PASS
        --http-user=USER            set http user to USER
        --https-only                only follow secure HTTPS links
        --ignore-case               ignore case when matching files/directories
        --ignore-length             ignore 'Content-Length' header field
        --ignore-tags=LIST          comma-separated list of ignored HTML tags
        --input-metalink=FILE       download files covered in local Metalink FILE
        --keep-session-cookies      load and save session (non-permanent) cookies
        --limit-rate=RATE           limit download rate to RATE
        --load-cookies=FILE         load cookies from FILE before session
        --local-encoding=ENC        use ENC as the local encoding for IRIs
        --max-redirect              maximum redirections allowed per page
        --metalink-over-http        use Metalink metadata from HTTP response headers
        --method=HTTPMethod         use method "HTTPMethod" in the request
        --no-cache                  disallow server-cached data
        --no-check-certificate      don't validate the server's certificate
        --no-config                 do not read any config file
        --no-cookies                don't use cookies
        --no-dns-cache              disable caching DNS lookups
        --no-glob                   turn off FTP file name globbing
        --no-hsts                   disable HSTS
        --no-http-keep-alive        disable HTTP keep-alive (persistent connections)
        --no-if-modified-since      don't use conditional if-modified-since get
                                     requests in timestamping mode
        --no-iri                    turn off IRI support
        --no-passive-ftp            disable the "passive" transfer mode
        --no-proxy                  explicitly turn off proxy
        --no-remove-listing         don't remove '.listing' files
        --no-warc-compression       do not compress WARC files with GZIP
        --no-warc-digests           do not calculate SHA1 digests
        --no-warc-keep-log          do not store the log file in a WARC record
        --password=PASS             set both ftp and http password to PASS
        --post-data=STRING          use the POST method; send STRING as the data
        --post-file=FILE            use the POST method; send contents of FILE
        --prefer-family=FAMILY      connect first to addresses of specified family,
                                     one of IPv6, IPv4, or none
        --preferred-location        preferred location for Metalink resources
        --preserve-permissions      preserve remote file permissions
        --private-key-type=TYPE     private key type, PEM or DER
        --private-key=FILE          private key file
        --progress=TYPE             select progress gauge type
        --protocol-directories      use protocol name in directories
        --proxy-password=PASS       set PASS as proxy password
        --proxy-user=USER           set USER as proxy username
        --random-file=FILE          file with random data for seeding the SSL PRNG
        --random-wait               wait from 0.5*WAIT...1.5*WAIT secs between retrievals
        --read-timeout=SECS         set the read timeout to SECS
        --referer=URL               include 'Referer: URL' header in HTTP request
        --regex-type=TYPE           regex type (posix)
        --regex-type=TYPE           regex type (posix|pcre)
        --reject-regex=REGEX        regex matching rejected URLs
        --rejected-log=FILE         log reasons for URL rejection to FILE
        --remote-encoding=ENC       use ENC as the default remote encoding
        --report-speed=TYPE         output bandwidth as TYPE.  TYPE can be bits
        --restrict-file-names=OS    restrict chars in file names to ones OS allows
        --retr-symlinks             when recursing, get linked-to files (not dir)
        --retry-connrefused         retry even if connection is refused
        --save-cookies=FILE         save cookies to FILE after session
        --save-headers              save the HTTP headers to file
        --secure-protocol=PR        choose secure protocol, one of auto, SSLv2,
                                     SSLv3, TLSv1 and PFS
        --show-progress             display the progress bar in any verbosity mode
        --spider                    don't download anything
        --start-pos=OFFSET          start downloading from zero-based position OFFSET
        --strict-comments           turn on strict (SGML) handling of HTML comments
        --trust-server-names        use the name specified by the redirection
                                     URL's last component
        --unlink                    remove file before clobber
        --user=USER                 set both ftp and http user to USER
        --waitretry=SECONDS         wait 1..SECONDS between retries of a retrieval
        --warc-cdx                  write CDX index files
        --warc-dedup=FILENAME       do not store records listed in this CDX file
        --warc-file=FILENAME        save request/response data to a .warc.gz file
        --warc-header=STRING        insert STRING into the warcinfo record
        --warc-max-size=NUMBER      set maximum size of WARC files to NUMBER
        --warc-tempdir=DIRECTORY    location for temporary files created by the
                                     WARC writer
        --wdebug                    print Watt-32 debug output
     %s (env)
     %s (system)
     %s (user)
     %s: certificate common name %s doesn't match requested host name %s.
     %s: certificate common name is invalid (contains a NUL character).
    This may be an indication that the host is not who it claims to be
    (that is, it is not the real %s).
   -4,  --inet4-only                connect only to IPv4 addresses
   -6,  --inet6-only                connect only to IPv6 addresses
   -A,  --accept=LIST               comma-separated list of accepted extensions
   -B,  --base=URL                  resolves HTML input-file links (-i -F)
                                     relative to URL
   -D,  --domains=LIST              comma-separated list of accepted domains
   -E,  --adjust-extension          save HTML/CSS documents with proper extensions
   -F,  --force-html                treat input file as HTML
   -H,  --span-hosts                go to foreign hosts when recursive
   -I,  --include-directories=LIST  list of allowed directories
   -K,  --backup-converted          before converting file X, back up as X.orig
   -K,  --backup-converted          before converting file X, back up as X_orig
   -L,  --relative                  follow relative links only
   -N,  --timestamping              don't re-retrieve files unless newer than
                                     local
   -O,  --output-document=FILE      write documents to FILE
   -P,  --directory-prefix=PREFIX   save files to PREFIX/..
   -Q,  --quota=NUMBER              set retrieval quota to NUMBER
   -R,  --reject=LIST               comma-separated list of rejected extensions
   -S,  --server-response           print server response
   -T,  --timeout=SECONDS           set all timeout values to SECONDS
   -U,  --user-agent=AGENT          identify as AGENT instead of Wget/VERSION
   -V,  --version                   display the version of Wget and exit
   -X,  --exclude-directories=LIST  list of excluded directories
   -a,  --append-output=FILE        append messages to FILE
   -b,  --background                go to background after startup
   -c,  --continue                  resume getting a partially-downloaded file
   -d,  --debug                     print lots of debugging information
   -e,  --execute=COMMAND           execute a `.wgetrc'-style command
   -h,  --help                      print this help
   -i,  --input-file=FILE           download URLs found in local or external FILE
   -k,  --convert-links             make links in downloaded HTML or CSS point to
                                     local files
   -l,  --level=NUMBER              maximum recursion depth (inf or 0 for infinite)
   -m,  --mirror                    shortcut for -N -r -l inf --no-remove-listing
   -nH, --no-host-directories       don't create host directories
   -nc, --no-clobber                skip downloads that would download to
                                     existing files (overwriting them)
   -nd, --no-directories            don't create directories
   -np, --no-parent                 don't ascend to the parent directory
   -nv, --no-verbose                turn off verboseness, without being quiet
   -o,  --output-file=FILE          log messages to FILE
   -p,  --page-requisites           get all images, etc. needed to display HTML page
   -q,  --quiet                     quiet (no output)
   -r,  --recursive                 specify recursive download
   -t,  --tries=NUMBER              set number of retries to NUMBER (0 unlimits)
   -v,  --verbose                   be verbose (this is the default)
   -w,  --wait=SECONDS              wait SECONDS between retrievals
   -x,  --force-directories         force creation of directories
   Issued certificate has expired.
   Issued certificate not yet valid.
   Self-signed certificate encountered.
   Unable to locally verify the issuer's authority.
  (%s bytes)  (unauthoritative)
  [following] %d redirections exceeded.
 %s
 %s (%s) - %s saved [%s/%s]

 %s (%s) - %s saved [%s]

 %s (%s) - Connection closed at byte %s.  %s (%s) - Data connection: %s;  %s (%s) - Read error at byte %s (%s). %s (%s) - Read error at byte %s/%s (%s).  %s (%s) - written to stdout %s[%s/%s]

 %s (%s) - written to stdout %s[%s]

 %s ERROR %d: %s.
 %s URL: %s %2d %s
 %s has sprung into existence.
 %s request sent, awaiting response...  %s subprocess %s subprocess failed %s subprocess got fatal signal %d %s: %s must only be used once
 %s: %s, closing control connection.
 %s: %s: Failed to allocate %ld bytes; memory exhausted.
 %s: %s: Failed to allocate enough memory; memory exhausted.
 %s: %s: Invalid WARC header %s.
 %s: %s: Invalid boolean %s; use `on' or `off'.
 %s: %s: Invalid byte value %s
 %s: %s: Invalid header %s.
 %s: %s: Invalid number %s.
 %s: %s: Invalid progress type %s.
 %s: %s: Invalid restriction %s,
    use [unix|vms|windows],[lowercase|uppercase],[nocontrol],[ascii].
 %s: %s: Invalid time period %s
 %s: %s: Invalid value %s.
 %s: %s:%d: unknown token "%s"
 %s: %s:%d: warning: %s token appears before any machine name
 %s: %s; disabling logging.
 %s: Cannot read %s (%s).
 %s: Cannot resolve incomplete link %s.
 %s: Couldn't find usable socket driver.
 %s: Error in %s at line %d.
 %s: Invalid --execute command %s
 %s: Invalid URL %s: %s
 %s: No certificate presented by %s.
 %s: Syntax error in %s at line %d.
 %s: The certificate of %s has been revoked.
 %s: The certificate of %s has expired.
 %s: The certificate of %s hasn't got a known issuer.
 %s: The certificate of %s is not trusted.
 %s: The certificate of %s is not yet activated.
 %s: The certificate of %s was signed using an insecure algorithm.
 %s: The certificate signer of %s was not a CA.
 %s: Unknown command %s in %s at line %d.
 %s: WGETRC points to %s, which doesn't exist.
 %s: Warning: Both system and user wgetrc point to %s.
 %s: aprintf: text buffer is too big (%d bytes), aborting.
 %s: cannot stat %s: %s
 %s: cannot verify %s's certificate, issued by %s:
 %s: corrupt time-stamp.
 %s: illegal option -- `-n%c'
 %s: invalid option -- '%c'
 %s: missing URL
 %s: no certificate subject alternative name matches
	requested host name %s.
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unable to resolve bind address %s; disabling bind.
 %s: unable to resolve host address %s
 %s: unknown/unsupported file type.
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' (no description) (try:%2d) , %s (%s) remaining , %s remaining -O not supported for metalink download. Ignoring.
 -k or -r can be used together with -O only if outputting to a regular file.
 ==> CWD not needed.
 ==> CWD not required.
 Address family for hostname not supported All requests done Already have correct symlink %s -> %s

 Argument buffer too small Authentication selected: %s
 BODY data file %s missing: %s
 Bad port number Bad value for ai_flags Bind error (%s).
 Both --no-clobber and --convert-links were specified, only --convert-links will be used.
 CDX file does not list checksums. (Missing column 'k'.)
 CDX file does not list original urls. (Missing column 'a'.)
 CDX file does not list record ids. (Missing column 'u'.)
 Can't be verbose and quiet at the same time.
 Can't timestamp and not clobber old files at the same time.
 Cannot back up %s as %s: %s
 Cannot convert links in %s: %s
 Cannot convert timestamp to http format. Falling back to time 0 as last modification time.
 Cannot get REALTIME clock frequency: %s
 Cannot initiate PASV transfer.
 Cannot open %s: %s Cannot open cookies file %s: %s
 Cannot parse PASV response.
 Cannot specify both --ask-password and --password.
 Cannot specify both --inet4-only and --inet6-only.
 Cannot unlink %s (%s).
 Cannot write to %s (%s).
 Cannot write to WARC file.
 Cannot write to temporary WARC file.
 Certificate must be X.509
 Checksum matches.
 Checksum mismatch for file %s.
 Compile:  Computing checksum for %s
 Connecting to %s:%d...  Connecting to %s|%s|:%d...  Connecting to [%s]:%d...  Continuing in background, pid %d.
 Continuing in background, pid %lu.
 Continuing in background.
 Control connection closed.
 Conversion from %s to UTF-8 isn't supported
 Converted links in %d files in %s seconds.
 Converting links in %s...  Cookie coming from %s attempted to set domain to  Copyright (C) %s Free Software Foundation, Inc.
 Could not create temporary file. Skipping signature download.
 Could not download all resources from %s.
 Could not find Metalink data in HTTP response. Downloading file using HTTP GET.
 Could not find acceptable digest for Metalink resources.
Ignoring them.
 Could not initialize SSL. It will be disabled. Could not open CDX file for output.
 Could not open WARC file.
 Could not open downloaded file for signature verification.
 Could not open downloaded file.
 Could not open temporary WARC file.
 Could not open temporary WARC log file.
 Could not open temporary WARC manifest file.
 Could not read CDX file %s for deduplication.
 Could not seed PRNG; consider using --random-file.
 Creating symlink %s -> %s
 Data matches signature, but signature is not trusted.
 Data transfer aborted.
 Debugging support not compiled in. Ignoring --debug flag.
 Digests are disabled; WARC deduplication will not find duplicate records.
 Directories:
 Directory    Disabling SSL due to encountered errors.
 Download quota of %s EXCEEDED!
 Download:
 ERROR ERROR: Cannot open directory %s.
 ERROR: Failed to load CRL file '%s': (%d)
 ERROR: Failed to open cert %s: (%d).
 ERROR: GnuTLS requires the key and the cert to be of the same type.
 ERROR: Redirection (%d) without location.
 Encoding %s isn't valid
 Error closing %s: %s
 Error in handling the address list.
 Error in proxy URL %s: Must be HTTP.
 Error in server greeting.
 Error in server response, closing control connection.
 Error initializing X509 certificate: %s
 Error matching %s against %s: %s
 Error opening GZIP stream to WARC file.
 Error opening WARC file %s.
 Error parsing certificate: %s
 Error parsing proxy URL %s: %s.
 Error while matching %s: %d
 Error writing to %s: %s
 Error writing warcinfo record to WARC file.
 Exiting due to error in %s
 FINISHED --%s--
Total wall clock time: %s
Downloaded: %d files, %s in %s (%s)
 FTP options:
 FTPS options:
 Failed reading proxy response: %s
 Failed to download %s. Skipping resource.
 Failed to unlink symlink %s: %s
 Failed writing HTTP request: %s.
 File         File %s already there; not retrieving.
 File %s already there; not retrieving.

 File %s exists.
 File %s not modified on server. Omitting download.

 File %s retrieved but checksum does not match. 
 File %s retrieved but signature does not match. 
 File `%s' already there; not retrieving.
 File has already been retrieved.
 Found %d broken link.

 Found %d broken links.

 Found exact match in CDX file. Saving revisit record to WARC.
 Found no broken links.

 GNU Wget %s built on %s.

 GNU Wget %s, a non-interactive network retriever.
 GPGME data_new_from_mem: %s
 GPGME op_verify: %s
 GPGME op_verify_result: NULL
 Giving up.

 GnuTLS: unimplemented 'secure-protocol' option value %d
 HSTS options:
 HTTP options:
 HTTPS (SSL/TLS) options:
 HTTPS support not compiled in IPv6 addresses not supported Incomplete or invalid multibyte sequence encountered
 Index of /%s on %s:%d Interrupted by a signal Invalid IPv6 numeric address Invalid PORT.
 Invalid UTF-8 sequence: %s
 Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid dot style specification %s; leaving unchanged.
 Invalid host name Invalid name of the symlink, skipping.
 Invalid preceding regular expression Invalid pri value. Assuming %d.
 Invalid range end Invalid regular expression Invalid regular expression %s, %s
 Invalid signature. Rejecting resource.
 Invalid user name Last-modified header invalid -- time-stamp ignored.
 Last-modified header missing -- time-stamps turned off.
 Length:  Length: %s License GPLv3+: GNU GPL version 3 or later
<http://www.gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
 Link         Link:  Loaded %d record from CDX.

 Loaded %d records from CDX.

 Loaded CA certificate '%s'
 Loaded CRL file '%s'
 Loading robots.txt; please ignore errors.
 Locale:  Location: %s%s
 Logged in!
 Logging and input file:
 Logging in as %s ...  Logically impossible section reached in getftp() Login incorrect.
 Mail bug reports and suggestions to <bug-wget@gnu.org>
 Malformed status line Mandatory arguments to long options are mandatory for short options too.

 Memory allocation failure Memory allocation problem
 Memory exhausted Metalink headers found. Switching to Metalink mode.
 Name or service not known No URLs found in %s.
 No address associated with hostname No certificate found
 No data received.
 No error No headers, assuming HTTP/0.9 No match No matches on pattern %s.
 No previous regular expression No such directory %s.

 No such file %s.
 No such file %s.

 No such file or directory %s.

 Non-recoverable failure in name resolution Not descending to %s as it is excluded/not-included.
 Not sure     OpenSSL: unimplemented 'secure-protocol' option value %d
 Opening WARC file %s.

 Output will be written to %s.
 Parameter string not correctly encoded Parsing system wgetrc file (env SYSTEM_WGETRC) failed.  Please check
'%s',
or specify a different file using --config.
 Parsing system wgetrc file failed.  Please check
'%s',
or specify a different file using --config.
 Password for user %s:  Password:  Please report this issue to bug-wget@gnu.org
 Please send bug reports and questions to <bug-wget@gnu.org>.
 Premature end of regular expression Processing request in progress Proxy tunneling failed: %s Read error (%s) in headers.
 Recursion depth %d exceeded max. depth %d.
 Recursive accept/reject:
 Recursive download:
 Regular expression too big Rejecting %s.
 Remote file does not exist -- broken link!!!
 Remote file exists and could contain further links,
but recursion is disabled -- not retrieving.

 Remote file exists and could contain links to other resources -- retrieving.

 Remote file exists but does not contain any link -- not retrieving.

 Remote file exists.

 Remote file is newer than local file %s -- retrieving.

 Remote file is newer, retrieving.
 Remote file no newer than local file %s -- not retrieving.
 Removed %s.
 Removing %s since it should be rejected.
 Removing %s.
 Request canceled Request not canceled Required attribute missing from Header received.
 Resolving %s...  Resource type %s not supported, ignoring...
 Retrying.

 Reusing existing connection to %s:%d.
 Reusing existing connection to [%s]:%d.
 Saving to: %s
 Scheme missing Server did not accept the 'PBSZ 0' command.
 Server did not accept the 'PROT %c' command.
 Server error, can't determine system type.
 Server file no newer than local file %s -- not retrieving.

 Server ignored If-Modified-Since header for file %s.
You might want to add --no-if-modified-since option.

 Servname not supported for ai_socktype Signature validation suceeded.
 Skipping directory %s.
 Specifying both --start-pos and --continue is not recommended; --continue will be disabled.
 Spider mode enabled. Check if remote file exists.
 Startup:
 Success Symlinks not supported, skipping symlink %s.
 Syntax error in Set-Cookie: %s at position %d.
 System error Temporary failure in name resolution The certificate has expired
 The certificate has not yet been activated
 The certificate's owner does not match hostname %s
 The server refuses login.
 The sizes do not match (local %s) -- retrieving.
 The sizes do not match (local %s) -- retrieving.

 This version does not have support for IRIs
 To connect to %s insecurely, use `--no-check-certificate'.
 Trailing backslash Try `%s --help' for more options.
 Unable to delete %s: %s
 Unable to establish SSL connection.
 Unable to get cookie for %s
 Unable to parse metalink file %s.
 Unable to read signature content from temporary file. Skipping.
 Unhandled errno %d
 Unknown authentication scheme.
 Unknown error Unknown host Unknown system error Unknown type `%c', closing control connection.
 Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ Unsupported algorithm '%s'.
 Unsupported listing type, trying Unix listing parser.
 Unsupported quality of protection '%s'.
 Unsupported scheme %s Unterminated IPv6 numeric address Usage: %s NETRC [HOSTNAME]
 Usage: %s [OPTION]... [URL]...
 Username/Password Authentication Failed.
 Using %s as listing tmp file.
 WARC options:
 WARC output does not work with --continue or --start-pos, they will be disabled.
 WARC output does not work with --no-clobber, --no-clobber will be disabled.
 WARC output does not work with --spider.
 WARC output does not work with timestamping, timestamping will be disabled.
 WARNING WARNING: combining -O with -r or -p will mean that all downloaded content
will be placed in the single file you specified.

 WARNING: timestamping does nothing in combination with -O. See the manual
for details.

 WARNING: using a weak random seed.
 Warning: wildcards not supported in HTTP.
 Wgetrc:  When downloading signature:
%s: %s.
 Will not retrieve dirs since depth is %d (max %d).
 Write failed, closing control connection.
 Wrote HTML-ized index to %s [%s].
 Wrote HTML-ized index to %s.
 You cannot specify both --body-data and --body-file.
 You cannot specify both --post-data and --post-file.
 You cannot use --post-data or --post-file along with --method. --method expects data through --body-data and --body-file options You must specify a method through --method=HTTPMethod to use with --body-data or --body-file.
 Your OpenSSL version is too old to support TLSv1.1
 Your OpenSSL version is too old to support TLSv1.2
 _open_osfhandle failed ` ai_family not supported ai_socktype not supported cannot create pipe cannot restore fd %d: dup2 failed connected.
 couldn't connect to %s port %d: %s
 cwd_count: %d
cwd_start: %d
cwd_end: %d
 done.
 done.   done.     failed: %s.
 failed: No IPv4/IPv6 addresses for host.
 failed: timed out.
 fake_fork() failed
 fake_fork_child() failed
 gmtime failed. This is probably a bug.
 idn_decode failed (%d): %s
 idn_encode failed (%d): %s
 ignored ioctl() failed.  The socket could not be set as blocking.
 locale_to_utf8: locale is unset
 memory exhausted nothing to do.
 time unknown        unspecified Project-Id-Version: wget 1.16.3.124
Report-Msgid-Bugs-To: bug-wget@gnu.org
POT-Creation-Date: 2016-06-14 08:19+0000
PO-Revision-Date: 2016-06-21 06:06+0000
Last-Translator: Jochen Hein <jochen@jochen.org>
Language-Team: German <translation-team-de@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:22+0000
X-Generator: Launchpad (build 18115)
Language: de
 
    Download der Datei schon vollständig; kein Download notwendig.

 
%*s[ überspringe %sK ] 
%s erhalten, Ausgabe wird nach »%s« umgeleitet.
 
%s empfangen.
 
Ursprünglich geschrieben von Hrvoje Niksic <hniksic@xemacs.org>.
 
REST schlug fehl; es wird wieder von vorn begonnen.
        --accept-regex=REGEX        regulärer Ausdruck, zu dem die akzeptierten
                                     URLs passen
        --ask-password              Nach Passworten fragen
        --auth-no-challenge         »Basic HTTP authentication«-Informationen
                                     senden, ohne zuerst auf die Aufforderung des
                                     Serverszu warten
        --backup=N                  vor dem Speichern der Datei X, bis zu N
                                     Sicherungsdateien behalten.
        --bind-address=ADRESSE      An die ADRESSE (Hostname oder IP) des
                                     lokalen Rechners binden
        --body-data=ZEICHENKETTE    ZEICHENKETTE als Daten verwenden.
                                     »--method« muss angegeben werden
        --body-file=DATEI           Den Inhalt der Datei als Daten senden.
                                     »--method« muss angegeben werden.
        --ca-certificate=DATEI      Datei mit der CA-Sammlung
        --ca-directory=VERZEICHNIS  Verzeichnis mit der Hash-Liste der CAs
        --certificate-type=TYP      Typ des Client-Zertifikates, entweder
                                     »PEM« oder »DER«
        --certificate=DATEI         Datei mit dem Client-Zertifikat
        --config=DATEI              Datei mit der Konfiguration
        --connect-timeout=SEKUNDEN  den Verbindungs-Timeout auf SEKUNDEN setzen
        --content-disposition       Den Content-Disposition-Header bei der
                                     Auswahl lokaler Dateinamen beachten
                                     (EXPERIMENTELL).
        --content-on-error          Den empfangenen Inhalt ausgeben, wenn
                                     der Server einen Fehler meldet.
        --crl-file=DATEI            Datei mit der CRL-Sammlung
        --cut-dirs=ZAHL             ZAHL der Verzeichnisebenen der Gegenseite
                                   überspringen
        --default-page=NAME        Den Namen der Standard-Seite ändern
                                     (normalerweise »index.html«)
        --delete-after              geholte Dateien nach dem Download löschen
        --dns-timeout=SEKUNDEN      den Timeout der DNS-Abfrage auf
                                     SEKUNDEN setzen
        --egd-file=DATEI            Dateiname des EGD-Sockets mit Zufallszahlen
        --exclude-domains=LISTE     komma-unterteilte Liste der
                                     zurückzuweisenden Domains
        --follow-ftp                FTP-Verweisen von HTML-Dokumenten aus
                                     folgen
        --follow-tags=LISTE         komma-unterteilte Liste der zu folgenden
                                     HTML-Tags
        --ftp-password=PASSWORT     PASSWORT als ftp-Passwort verwenden
        --ftp-stmlf                 Stream_LF Format für alle binären
                                     FTP-Dateien verwenden
        --ftp-user=BENUTZER         BENUTZER als ftp-Benutzername verwenden
        --ftps-clear-data-connection    nur den Steuerkanal verschlüsseln; alle Daten bleiben im Klartext
        --ftps-fallback-to-ftp          verwende FTP falls FTPS vom Zielserver nicht unterstützt wird
        --ftps-implicit             Verwende implizit FTPS
                                     (Standardport ist 990)
        --ftps-resume-ssl           Beim Öffnen der Daten-Verbidung die SSL/TLS-Sitzung der
                                     Steuer-Verbindung fortsetzen
        --header=ZEICHENKETTE       ZEICHENKETTE in die Kopfzeilen einfügen
        --hsts-file                 Pfad der HSTS Datenbank
                                     (überschreibt den Standardwert)
        --http-passwd=PASSWORT      http-Passwort auf PASSWORT setzen
        --http-user=BENUTZER        http-Benutzer auf BENUTZER setzen
        --https-only                Nur sicheren HTTPS-Verweisen folgen
        --ignore-case               Groß-/Kleinschreibung bei Datei- und
                                     Verzeichnisnamen ignorieren
        --ignore-length             das »Content-Length«-Kopffeld ignorieren
        --ignore-tags=LISTE         komma-unterteilte Liste der zu
                                     missachtenden HTML-Tags
        --input-metalink=DATEI      in lokaler Metalink DATEI gelistete
                                     URLs holen
        --keep-session-cookies      (nicht-permanente) Session-Cookies
                                      laden und speichern
        --limit-rate=RATE           Datenrate beim Download auf RATE begrenzen
        --load-cookies=DATEI        Cookies vor der Sitzung aus der DATEI laden
        --local-encoding=ENC        ENC als die lokale Kodierung
                                     für IRIs verwenden
        --max-redirect              maximale Anzahl erlaubter »Redirects«
                                     (Umleitungen) pro Seite
        --metalink-over-http        verwende Metalink Metadaten aus dem
                                    HTTP Response Header
        --method=HTTPMethod         die Methode »HTTPMethod« im Header verwenden
        --no-cache                  Serverseitige Zwischenspeicherung der Daten
                                     verbieten
        --no-check-certificate      Das Server-Zertifikat nicht prüfen
        --no-config                 keine Konfigrationsdatei verwenden
        --no-cookies                Cookies nicht verwenden
        --no-dns-cache              Zwischenspeichern von DNS-Abfragen
                                     ausschalten
        --no-glob                   FTP-Dateinamens-Globbing ausschalten
        --no-hsts                   HSTS nicht verwenden
        --no-http-keep-alive        »HTTP keep-alive« (persistente Verbindungen)
                                   deaktivieren
        --no-if-modified-since      Die Bedingung if-modified-since bei
                                     GET-Requests im Zeitstempel-Modus
                                     nicht verwenden
        --no-iri                     Unterstützung für IRI abschalten
        --no-passive-ftp            Nur »aktiven« Übertragungsmodus verwenden
        --no-proxy                  Keinen Proxy verwenden
        --no-remove-listing         ».listing«-Dateien nicht entfernen
        --no-warc-compression       WARC-Dateien nicht mit GZIP komprimieren
        --no-warc-digests           keine SHA1-Prüfsummen berechnen
        --no-warc-keep-log          Die Protokolldatei nicht in einem WARC-Datensatz
                                     speichern
        --password=PASSWORT         PASSWORT als ftp- und http-Passwort
                                     verwenden
        --post-data=ZEICHENKETTE    Die POST-Methode verwenden, dabei die
                                     ZEICHENKETTE als Daten senden
        --post-file=DATEI           Die POST-Methode verwenden, dabei den Inhalt
                                     aus DATEI verwenden
        --prefer-family=FAMILIE     Zunächst eine Verbindung zur angegebenen
                                     Familie aufbauen, eins von »IPv6«,
                                     »IPv4« oder »none«
        --preferred-location        bevorzugter Ort für Metalink Ressourcen
        --preserve-permissions      Die Rechte der fernen Datei erhalten
        --private-key-type=TYP      Typ des geheimen Schlüssels, entweder
                                     »PEM« oder »DER«
        --private-key=DATEI         Datei mit dem geheimen Schlüssel
        --progress=STYLE            Fortschrittsanzeige mit STYLE darstellen
        --protocol-directories     Den Protokollnamen in Verzeichnissen
                                    verwenden
        --proxy-passwd=PASS         PASS als Proxy-Passwort verwenden
        --proxy-user=BENUTZER       BENUTZER als Proxy-Benutzername verwenden
        --random-file=DATEI         Datei mit Zufallsdaten zur Initialisierung
                                     des SSL Pseudo-Zufallszahlen-Generators
        --random-wait               Zwischen 0,5*WAIT und 1,5*WAIT Sekunden
                                     zwischen den Abfragen warten
        --read-timeout=SEKUNDEN     den Lese-Timeout auf SEKUNDEN setzen
        --referer=URL               die Kopfzeile »Referer: URL« der HTTP-
                                     Anforderung hinzufügen
        --regex-type=TYPE           Typ des regulären Ausdrucks (posix).
        --regex-type=TYPE           Typ des regulären Ausdrucks (posix|pcre).
        --reject-regex=REGEX        regulärer Ausdruck, zu dem die abgewiesenen
                                   URLs passen
        --rejected-log=DATEI        Grund der URL-Ablehnung in DATEI schreiben
        --remote-encoding=ENC       ENC als die externe Standardkodierung
                                     verwenden
        --report-speed=TYP          Bandbreite als TYP ausgeben.
                                     TYP kann »bits« sein.
        --restrict-file-names=OS    Verwendbare Zeichen in Dateinamen auf
                                     diejenigen einschränken, die das
                                     Betriebssystem erlaubt
        --retr-symlinks             falls auftretend, verlinkte Dateien holen (keine
                                     Verzeichnisse)
        --retry-connrefused         Wiederholen, auch wenn der Partner die
                                     Verbindung abgelehnt hat
        --save-cookies=DATEI        Cookies nach der Sitzung in der DATEI
                                     speichern
        --save-headers              den HTTP-Vorspann (header lines) in einer
                                     Datei sichern
        --secure-protocol=PR        Als sicheres Protokoll eines aus »auto«,
                                     »SSLv2«, »SSLv3«, »TLSv1« oder »PFS«
                                     verwenden.
        --show-progress             Immer eine Fortschrittsanzeige darstellen
        --spider                    kein Download (nichts herunterladen)
        --start-pos=OFFSET          Start Download ab OFFSET (Null-basiert)
        --strict-comments           Strikte Handhabung (SGML) von
                                     HTML-Kommentaren
        --trust-server-names        Den durch die letzte Komponente der
                                     Weiterleitungs-URL spezifizierten Namen
                                     verwenden.
        --unlink                    Datei löschen vor dem Überschreiben
        --user=BENUTZER             BENUTZER als ftp- und http-Benutzer
                                     verwenden
        --waitretry=SEKUNDEN        1..SEKUNDEN zwischen den erneuten Versuchen
                                     warten
        --warc-cdx                  CDX-Index-Dateien schreiben
        --warc-dedup=DATEINAME      Datensätze nicht speichern, die in dieser
                                     CDX-Datei enthalten sind
        --warc-file=DATEINAME       Die Anfrage- und Antwortdaten in einer
                                     .warc.gz-Datei speichern
        --warc-header=ZEICHENKETTE  ZEICHENKETTE in den warcinfo-Satz einfügen
        --warc-max-size=ZAHL        Die Maximalgröße der WARC-Dateien auf
                                     ZAHL setzen
        --warc-tempdir=VERZEICHNIS  Verzeichnis für temporäre Dateien, die durch
                                     den WARC-Schreiber erzeugt werden
        --wdebug                    Watt-32 Debug-Ausgabe anzeigen
     %s (Umgebung)
     %s (System)
     %s (Benutzer)
     %s: Der Common Name »%s« des Zertifikates entspricht nicht dem angeforderten Hostname »%s«.
     %s: Der »common name« des Zertifikates ist ungültig (enthält ein NUL-Zeichen).
Das könnte ein Zeichen dafür sein, dass der Host nicht derjenige ist, der er
zu sein vorgibt (also nicht der echte »%s«).
   -4,  --inet4-only                Nur zu IPv4-Adressen verbinden
   -6,  --inet6-only                Nur zu IPv6-Adressen verbinden
   -A,  --accept=LISTE              komma-unterteilte Liste der erlaubten
                                     Dateiendungen
   -B,  --base=URL                  Verweise in der HTML Eingabedatei (-i -F)
                                   relativ zur URL auflösen
   -D,  --domains=LISTE             komma-unterteilte Liste der erlaubten
                                     Domains
   -E,  --adjust-extension          alle text/html-Dokumente mit der richtigen
                                     Namenserweiterung speichern
   -F,  --force-html                Eingabe-Datei als HTML behandeln
   -H,  --span-hosts                wenn »--recursive«, auch zu fremden Hosts
                                     gehen
   -I,  --include-directories=LISTE  Liste der erlaubten Verzeichnisse
   -K,  --backup-converted          vor dem Umwandeln der Datei X, eine
                                     Sicherung als X.orig anlegen.
   -K,  --backup-converted          vor dem Umwandeln der Datei X, eine
                                     Sicherung als X_orig anlagen.
   -L,  --relative                  nur relativen Verweisen folgen
   -N,  --timestamping              Nur Dateien holen, die neuer als die lokalen
                                     Dateien sind
   -O   --output-document=DATEI     Dokumente in DATEI schreiben
   -P,  --directory-prefix=PRÄFIX   Dateien unter dem Verzeichnis PRÄFIX/..
                                   speichern
   -Q,  --quota=ZAHL                Kontingent für den Download auf ZAHL setzen
   -R,  --reject=LISTE              komma-unterteilte Liste der
                                     zurückzuweisenden Erweiterungen
   -S,  --server-response           Antwort des Servers anzeigen
   -T,  --timeout=SEKUNDEN          alle Timeouts auf SEKUNDEN setzen
   -U,  --user-agent=AGENT          als AGENT anstelle von Wget/VERSION
                                     identifizieren
   -V,  --version                   Programmversion von Wget anzeigen und beenden
   -X,  --exclude-directories=LISTE  Liste der auszuschließenden Verzeichnisse
   -a,  --append-output=DATEI       Meldungen an DATEI anhängen
   -b,  --background                nach dem Starten in den Hintergrund gehen
   -c,  --continue                  Fortführung des Downloads einer bereits zum
                                     Teil geholten Datei
   -d,  --debug                     Debug-Ausgabe anzeigen
   -e,  --execute=BEFEHL            einen ».wgetrc«-artigen Befehl ausführen
   -h,  --help                      diese Hilfe anzeigen
   -i,  --input-file=DATEI          in lokaler oder externer DATEI
                                     gelistete URLs holen
   -k,  --convert-links             Verweise in HTML- oder CSS-Downloads
                                     in lokale Verknüpfungen umwandeln
   -l,  --level=Zahl                maximale Rekursionstiefe (»inf« oder »0«
                                     steht für ohne Begrenzung)
   -m,  --mirror                    Kurzform, die
                                     »-N -r -l inf --no-remove-listing«
                                     entspricht.
   -nH, --no-host-directories       keine Host-Verzeichnisse anlegen
   -nc, --no-clobber                Downloads überspringen, die bestehende
                                     Dateien überschreiben würden
   -nd  --no-directories            keine Verzeichnisse anlegen
   -np, --no-parent                 nicht in das übergeordnete Verzeichnis
                                   wechseln
   -nv, --no-verbose                weniger Meldungen, aber nicht stumm
   -o,  --output-file=DATEI         Protokoll-Meldungen in DATEI schreiben
   -p,  --page-requisites           alle Bilder usw. holen, die für die Anzeige
                                     der HTML-Seite notwendig sind
   -q,  --quiet                     keine Ausgabe von Meldungen
   -r,  --recursive                 rekursiver Download
   -t,  --tries=ZAHL                Anzahl der Wiederholversuche auf ZAHL setzen
                                   (0 steht für unbegrenzt)
   -v,  --verbose                   ausführliche Meldungen (Vorgabe)
   -w,  --wait=SEKUNDEN             SEKUNDEN zwischen den Downloads warten
   -x,  --force-directories         Anlegen von Verzeichnissen erzwingen
   Das ausgestellte Zertifikat ist nicht mehr gültig.
   Das ausgestellte Zertifikat ist noch nicht gültig.
   Ein selbst-signiertes Zertifikat gefunden.
   Die Authorität des Ausstellers des Zertifikates kann lokal nicht geprüft werden.
  (%s Bytes)  (unmaßgeblich)
  [folge] %d: Die Anzahl der Verweise ist zu groß.
 %s
 %s (%s) - %s gespeichert [%s/%s]

 %s (%s) - %s gespeichert [%s]

 %s (%s) - Verbindung bei Byte %s geschlossen.  %s (%s) - Daten-Verbindung: %s;  %s (%s) - Lesefehler bei Byte %s (%s). %s (%s) - Lesefehler bei Byte %s/%s (%s).  %s (%s) - auf die Standardausgabe geschrieben %s[%s/%s]

 %s (%s) - auf die Standardausgabe geschrieben [%s/%s]

 %s FEHLER %d: %s.
 %s URL: %s %2d %s
 »%s« ist plötzlich entstanden.
 %s-Anforderung gesendet, warte auf Antwort...  %s Unterprozess %s Unterprozess fehlgeschlagen %s Unterprozess erhielt das fatale Signal %d %s: %s darf nur einmal verwendet werden
 %s: %s; Kontroll-Verbindung schließen.
 %s: %s: Fehler beim Allozieren von %ld Bytes; Speicher erschöpft.
 %s: %s: Fehler beim Allozieren von ausreichend Speicher; Speicher erschöpft.
 %s: %s: Ungültige WARC Kopfzeile »%s«.
 %s: %s: Ungültiger Schalter »%s«, bitte »on« oder »off« angeben.
 %s: %s: Ungültiger Byte-Wert »%s.«
 %s: %s: Ungültige Kopfzeile »%s«
 %s: %s: Ungültige Nummer »%s«
 %s: %s: Ungültiger Fortschrittstyp »%s.«
 %s: %s: Ungültige Einschränkung »%s«,
    verwenden Sie [unix|vms|windows],[lowercase|uppercase],[nocontrol],[ascii].
 %s: %s: Ungültige Zeitperiode »%s«
 %s: %s: Ungültiger Wert »%s«.
 %s: %s:%d: unbekannter Wortteil »%s«
 %s: %s:%d: Warnung: »%s«-Wortteil erscheint vor jeglichem Maschinennamen
 %s: %s; Protokoll wird ausgeschaltet.
 %s: »%s« nicht lesbar (%s).
 %s: Der unvollständige Link »%s« kann nicht aufgelöst werden.
 %s: Kein benutzbar "socket driver" auffindbar.
 %s: Fehler in »%s« bei Zeile %d.
 %s: Ungültiges »--execute«-Kommando »%s«
 %s: Ungültige URL »%s«: %s
 %s: Kein Zertifikat angegeben von %s.
 %s: Fehler in »%s« in Zeile %d.
 %s: Das Zertifikat von %s wurde für ungültig erklärt.
 %s: Das Zertifikat von %s ist abgelaufen.
 %s: Das Zertifikat von »%s« wurde von einem unbekannten Austeller herausgegeben.
 %s: Dem Zertifikat von %s wird nicht vertraut.
 %s: Dem Zertifikat von %s ist noch nicht aktiviert.
 %s: Das Zertifikat von »%s« wurde mit einem unsicheren Algorithmus signiert.
 %s: Der Unterzeichner des Zertifikats von %s ist keine CA.
 %s: Unbekanntes Kommando %s in »%s« in Zeile %d.
 %s: WGETRC zeigt auf die Datei »%s«, die nicht existiert.
 %s: Warnung: »wgetrc« des Systems und des Benutzers zeigen beide auf »%s«.
 %s: aprintf: Textpuffer ist zu groß (%d Bytes), Abbruch.
 %s: kann nicht finden %s: %s
 %s: Kann das Zertifikat von »%s« nicht prüfen, ausgestellt von »%s«:.
 %s: beschädigter Zeitstempel.
 %s: ungültige Option -- »-n%c«
 %s: ungültige Option -- »%c«
 %s: URL fehlt
 %s: Keiner der alternativen Namen des Zertifikats stimmt mit dem angefragten Maschinennamen »%s« überein.
 %s: Option »%c%s« erlaubt kein Argument
 %s: Option »%s« ist mehrdeutig
 %s: Option »%s« ist mehrdeutig: mögliche Optionen: %s: Option »--%s« erlaubt kein Argument
 %s: Option »%s« benötigt ein Argument
 %s: Option »-W %s« erlaubt kein Argument
 %s: Option »-W %s« ist mehrdeutig
 %s: Option »-W %s« benötigt ein Argument
 %s: Option verlangt ein Argument -- »%c«
 %s: kann die bind-Adresse %s nicht auflösen; bind wird nicht verwendet.
 %s: kann die Host-Adresse %s nicht auflösen
 %s: unbekannter bzw. nicht unterstützter Dateityp.
 %s: nicht erkannte Option »%c%s«
 %s: nicht erkannte Option »--%s«
 « (keine Beschreibung) (Versuch:%2d) , %s (%s) sind noch übrig , %s übrig -O ist für Metalink Downloads nicht unterstützt. Option wird Ignoriert.
 »-k« oder »-r« kann nur zusammen mit -O verwendet werden, wenn die Ausgabe eine
normale Datei ist.
 ==> CWD nicht notwendig.
 ==> CWD nicht erforderlich.
 Adress-Family wird für Hostnamen nicht unterstützt Alle Anforderungen wurden abgearbeitet Der richtige symbolische Verweis %s -> %s ist schon vorhanden.

 Der Argument-Puffer ist zu klein Authentifizierung ausgewählt: %s
 BODY Datendatei %s fehlt: %s
 Ungültige Port-Nummer Ungültiger Wert für ai_flags Verbindungsfehler (%s).
 Sowohl »--no-clobber« als auch »--convert-links« angegeben, nur
»--convert-links« wird verwendet.
 Die CDX-Datei enthält keine Prüfsummen (Spalte »k« fehlt).
 Die CDX-Datei enthält keine Original-URLs (Spalte »a« fehlt).
 Die CDX-Datei enthält keine Satz-IDs (Spalte »u« fehlt).
 »Ausführliche« und »keine Meldungen« ist gleichzeitig unmöglich.
 »Zeitstempel« und »Überschreibung alter Dateien« ist gleichzeitig unmöglich.
 Anlegen einer Sicherung von »%s« als »%s« nicht möglich: %s
 Verweise nicht umwandelbar in »%s«: %s
 Kann den Zeitstempel nicht in das http-Format umwandeln. Benutze stattdessen die Zeit 0 als Zeitpunkt der letzten Änderung.
 Kann die Frequenz der Echtzeit-Uhr nicht bestimmen: %s
 Kann PASV-Übertragung nicht beginnen.
 Kann »%s« nicht öffnen: %s Cookie-Datei %s kann nicht geöffnet werden: %s
 Kann PASV-Antwort nicht auswerten.
 Die Optionen »--ask-password« und »--password« sind gemeinsam nicht erlaubt.
 Die Optionen »--inet4-only« und »--inet6-only« sind gemeinsam nicht erlaubt
 Kann %s nicht unlinken (%s).
 Kann nicht nach »%s« schreiben (%s).
 Kann nicht in die WARC-Datei schreiben.
 Kann nicht in die temporäre WARC-Datei schreiben.
 Es muss ein X.509-Zertifikat verwendet werden
 Prüfsumme passt.
 Prüfsumme passt nicht für Datei %s.
 Übersetzt:  Berechne Prüfsumme für %s
 Verbindungsaufbau zu %s:%d...  Verbindungsaufbau zu %s|%s|:%d...  Verbindungsaufbau zu [%s]:%d...  Im Hintergrund geht's weiter, die Prozeßnummer ist %d.
 Im Hintergrund geht's weiter, die Prozeßnummer ist %lu.
 Im Hintergrund geht's weiter.
 Kontroll-Verbindung geschlossen.
 Konvertierung von %s nach UTF-8 wird nicht unterstützt
 Links in %d Dateien in %s Sekunden umgewandelt.
 Umwandlung von Links in »%s«…  Cookie von %s versuchte die Domain zu ändern auf  Copyright © %s Free Software Foundation, Inc.
 Kann die temporäre Datei nicht erstellen. Überspringe das Herunterladen der Signatur.
 Konnte nicht alle Ressourcen von %s herunterladen.
 Keine Metalink-Daten in der HTTP-Antwort. Datei wird mittel HTTP GET heruntergeladen.
 Kann keinen akzeptablen Digest für die Metalink Ressource finden.
Diese werden ignoriert.
 SSL kann nicht initialisiert werden. Es wird kein SSL verwendet. Kann die CDX-Datei nicht zur Ausgabe öffnen.
 Kann die WARC-Datei nicht öffnen.
 Die heruntergeladene Datei kann nicht zur Überprüfung der Signatur geöffnet werden.
 Die heruntergeladene Datei kann nicht geöffnet werden.
 Kann die temporäre WARC-Datei nicht öffnen.
 Kann die temporäre WARC-Protokoll-Datei nicht öffnen.
 Kann die temporäre WARC-Manifest-Datei nicht öffnen.
 Kann die CDX-Datei %s nicht zur Deduplikation lesen.
 Der Zufallszahlengenerator konnte nicht initialisiert werden, denken Sie über --random-file nach.
 Symbolischen Verweis %s -> %s anlegen.
 Daten passen zur Signatur, aber der Signatur wird nicht vertraut.
 Daten-Übertragung abgebrochen.
 Debugging-Unterstützung ist nicht einkompiliert, »--debug« wird ignoriert.
 WARC-Digests sind deaktiviert; WARC-Deduplication wird keine doppelten Records finden.
 Verzeichnisse:
 Verzeichnis    SSL wird ausgeschaltet nachdem Fehler aufgetreten sind.
 Download-Kontingent von %s ERSCHÖPFT!
 Download:
 FEHLER ERROR: Kann das Verzeichnis »%s« nicht öffnen.
 ERROR: CRL-Datei »%s« kann nicht geladen werden: (%d).
 ERROR: Kann das Zertifikat »%s« nicht öffnen: (%d).
 ERROR: GnuTLS verlangt, dass der Schlüssel und das Zertifikat vom gleichen Typ sind.
 FEHLER: Umleitung (%d) ohne Ziel(?).
 Die Kodierung %s ist nicht korrekt
 Fehler beim Schließen von »%s«: %s
 Fehler in der Verarbeitung der Adressliste.
 Fehler in der Proxy-URL »%s«: Es muss eine HTTP-URL sein.
 Fehler bei der Begrüßung des Servers.
 Fehler in der Antwort des Servers; schließe Kontroll-Verbindung.
 Fehler beim Initialisieren des X509-Zertifikates: %s
 Fehler beim Vergleichen von »%s« mit %s: %s.
 Fehler beim Öffnen des GZIP-Streams zur WARC-Datei.
 Fehler beim Öffnen der WARC-Datei %s.
 Fehler beim Parsen des Zertifikates: %s.
 Fehler beim Parsen der Proxy-URL »%s«: %s.
 Fehler beim Matchen %s: %d
 Fehler beim Schreiben nach »%s«: %s
 Kann den warcinfo-Satz nicht in die WARC-Datei schreiben.
 Beende aufgrund eines Fehlers in %s
 BEENDET --%s--
Verstrichene Zeit: %s
Geholt: %d Dateien, %s in %s (%s)
 FTP-Optionen:
 FTPS-Optionen:
 Fehler beim Lesen der Proxy-Antwort: %s.
 Herunterladen von %s fehlgeschlagen. Ressource wird übersprungen.
 Entfernen des symbolischen Verweises »%s« fehlgeschlagen: %s
 Fehler beim Schreiben der HTTP-Anforderung: %s.
 Datei         Die Datei »%s« ist schon vorhanden; kein erneutes Herunterladen.
 Die Datei »%s« ist schon vorhanden; kein erneutes Herunterladen.

 Die Datei »%s« existiert.
 Datei %s auf dem Server unverändert. Wird nicht heruntergeladen.

 Datei %s wurde abgerufen, aber die Prüfsumme passt nicht. 
 Datei %s wurde abgerufen, aber die Signatur passt nicht. 
 Die Datei »%s« ist schon vorhanden; kein erneuter Download.
 Die Datei »%s« ist geholt worden.
 Ein %d ungültiger Verweis gefunden.

 %d ungültige Verweise gefunden.

 Exakter Treffer in der CDX-Datei gefunden.  Speichere revisit-Satz in WARC.
 Keine ungültigen Verweise gefunden.

 GNU Wget %s übersetzt unter %s.

 GNU Wget %s, ein nicht-interaktives Netz-Werkzeug zum Download von Dateien.
 GPGME data_new_from_mem: %s
 GPGME op_verify: %s
 GPGME op_verify_result: NULL
 Aufgegeben.

 GnuTLS: Option %d zu »secure-protocol« ist nicht implementiert
 HSTS-Optionen:
 HTTP-Optionen:
 HTTPS (SSL) Optionen:
 Keine HTTPS-Unterstützung einkompiliert IPv6-Adressen werden nicht unterstützt Unvollständige oder ungültige multi-Byte-Sequenz
 Index von /%s auf %s:%d Durch ein Signal unterbrochen Ungültige numerische IPv6-Adresse Ungültiger PORT.
 Ungültige UTF-8 Sequenz: %s
 Ungültige Rückwärtsreferenz Ungültiger Name der Zeichenklasse Ungültiges Vergleichszeichen Ungültiger Inhalt in »\{\}« Ungültiger Stil für den »dot«-Fortschrittsindikator %s; keine Änderung.
 Ungültiger Hostname Ungültiger Name für einen symbolischen Verweis; übersprungen.
 Ungültiger vorhergehender Regulärer Ausdruck ungültiger pri Wert. Verwende %d.
 Ungültiges Ende des Bereichs Ungültiger Regulärer Ausdruck Ungültiger Regulärer Ausdruck %s, %s
 Ungültige Signatur. Ressource wird zurückgewiesen.
 Ungültiger Benutzername »Last-modified«-Kopfzeile ungültig -- Zeitstempel übergangen.
 »Last-modified«-Kopfzeile fehlt -- Zeitstempel abgeschaltet.
 Länge:  Länge: %s License GPLv3+: GNU GPL version 3 or later
<http://www.gnu.org/licenses/gpl.html>.
Dies ist Freie Software; Sie dürfen diese ändern und weitergeben.
Es wird keine Garantie gegeben, soweit das Gesetz es zuläßt.
 Verweis         Gebunden:  %d Satz vom CDX geladen.

 %d Sätze vom CDX geladen.

 CA-Zertifikat »%s« wurde geladen
 CRL-Datei »%s« wurde geladen
 Lade »robots.txt«; bitte Fehler ignorieren.
 Lokale:  Platz: %s%s
 Angemeldet!
 Log-Datei schreiben und Eingabe-Datei:
 Anmelden als %s ...  Logisch unmöglicher Abschnitt in »getftp()« wurde erreicht Fehler bei der Anmeldung.
 Fehlerberichte und Verbesserungsvorschläge bitte an <bug-wget@gnu.org>
schicken.

Für die deutsche Übersetzung ist die Mailingliste
<translation-team-de@lists.sourceforge.net> zuständig.
 Nicht korrekte Statuszeile Erforderliche Argumente zu langen Optionen sind auch bei kurzen Optionen erforderlich.

 Fehler bei der Memory-Anforderung Problem bei der Memory-Anforderung
 Speicher erschöpft Metalink Header gefunden. Metalink Modus wird verwendet.
 Name oder Dienst ist nicht bekannt Keine URLs in %s gefunden.
 Mit dem Hostname ist keine Adresse verknüpft Kein Zertifikat gefunden.
 Keine Daten empfangen.
 Kein Fehler Keine Header, vermutlich ist es HTTP/0.9. Kein Teffer Keine Treffer bei dem Muster »%s«.
 Kein vorhergehender Regulärer Ausdruck Das Verzeichnis »%s« gibt es nicht.

 Die Datei »%s« gibt es nicht.
 Die Datei »%s« gibt es nicht.

 Die Datei oder das Verzeichnis »%s« gibt es nicht.

 Nicht-behebbarer Fehler bei der Namensauflösung Nicht zu »%s« hinabsteigen, da es ausgeschlossen bzw. nicht eingeschlossen ist.
 Nicht sicher     OpenSSL: Option %d zu »secure-protocol« ist nicht implementiert
 Öffne WARC-Datei %s.

 Ausgabe wird nach »%s« geschrieben.
 Der Parameter ist nicht korrekt kodiert Parsen der System wgetrc-Datei (env SYSTEM_WGETRC) fehlgeschlagen.
Bitte »%s« prüfen,
oder eine andere Datei mittels »--config« angeben.
 Parsen der System wgetrc-Datei fehlgeschlagen.
Bitte »%s« prüfen,
oder eine andere Datei mittels »--config« angeben.
 Passwort für Benutzer »%s«:  Passwort:  Bitte dieses Problem an <bug-wget@gnu.org> melden
 Fehlerberichte und Verbesserungsvorschläge bitte an <bug-wget@gnu.org>
schicken.

Für die deutsche Übersetzung ist die Mailingliste <de@li.org> zuständig.
 Vorzeitiges Ende des Regulären Ausdrucks Verarbeitungsanforderung wird bearbeitet Proxy-Tunneling fehlgeschlagen: %s Lesefehler (%s) beim Vorspann (header).
 Die Rekursionstiefe %d übersteigt die max. erlaubte Tiefe %d.
 Rekursiv erlauben/zurückweisen:
 Rekursives Holen:
 Der Reguläre Ausdruck ist zu groß »%s« zurückgewiesen.
 Die Datei auf dem Server existiert nicht -- Link nicht gültig!
 Datei auf dem Server existiert und könnte weitere Links enthalten,
aber Rekursion ist abgeschaltet -- kein Download.

 Datei auf dem Server existiert und enhält Links -- Download erfolgt.

 Datei auf dem Server existiert aber enhält keine Links -- kein Download.

 Datei auf dem Server existiert.

 Datei auf dem Server neuer als die lokale Datei »%s«, -- wird geholt.

 Datei der Gegenseite ist neuer, erneuter Download.
 Datei auf dem Server nicht neuer als die lokale Datei »%s« -- wird nicht geholt.
 »%s« gelöscht.
 Entferne »%s«, da dies zurückgewiesen werden soll.
 Entferne »%s«.
 Anforderung wurde abgebrochen Anforderung wurde nicht abgebrochen Ein notwendiges Attribut im empfangenen Header fehlt.
 Auflösen des Hostnamen »%s«...  Ressourcetyp %s wird nicht unterstützt, wird ignoriert...
 Erneuter Versuch.

 Wiederverwendung der bestehenden Verbindung zu %s:%d.
 Wiederverwendung der bestehenden Verbindung zu [%s]:%d.
 In »%s« speichern.
 Schema fehlt Der Server hat das Kommando »PBSZ 0« nicht akzeptiert.
 Der Server hat das Kommando »PROT %c« nicht akzeptiert.
 Fehler beim Server; es ist nicht möglich, die Art des Systems festzustellen.
 Datei auf dem Server nicht neuer als die lokale Datei »%s« -- kein Herunterladen.

 Der Server ignorierte den If-Modified-Since header für Datei %s.
Sie möchten vielleicht die Option »--no-if-modified-since« verwenden.

 Service-Name wird für ai_socktype nicht unterstützt Signatur wurde erfolgreich geprüft.
 Verzeichnis »%s« übersprungen.
 Sowohl »--start-pos« als auch »--continue« anzugeben ist nicht empfohlen;
»--continue« wird nicht verwendet.
 Spider-Modus eingeschaltet.  Prüfe ob die Datei auf dem Server existiert.
 Beim Start:
 Erfolgreich Symbolischer Verweis wird nicht unterstützt; symbolischer Verweis »%s« übersprungen.
 Syntaxfehler bei Set-Cookie, »%s« an der Stelle %d.
 System-Fehler Temporärer Fehler bei der Namensauflösung Das ausgestellte Zertifikat ist nicht mehr gültig.
 Das ausgestellte Zertifikat ist noch nicht aktiviert.
 Der Zertifikat-Eigentümer passt nicht zum Hostname »%s«.
 Der Server verweigert die Anmeldung.
 Größen stimmen nicht überein (lokal %s) -- erneuter Download.
 Größen stimmen nicht überein (lokal %s) -- erneuter Download.

 Diese Version unterstützt keine IRIs.
 Verwenden Sie »--no-check-certificate«, um zu dem Server »%s« eine nicht gesicherte Verbindung aufzubauen.
 Backslash »\« am Ende »%s --help« gibt weitere Informationen.
 Es ist nicht möglich, %s zu löschen: %s
 Es ist nicht möglich, eine SSL-Verbindung herzustellen.
 Es ist nicht möglich, den Cookie für %s zu holen
 Kann die Metalink Datei %s nicht parsen.
 Kann den Signatur-Inhalt nicht aus der temporären Datei lesen. Überspringe...
 Fehlernummer %d niche behandelt
 Unbekanntes Authentifizierungsschema.
 Unbekannter Fehler Unbekannter Rechner Unbekannter Fehler Unbekannte Art »%c«, schließe Kontroll-Verbindung.
 Öffnende Klammer »(« oder »\(« ohne passende schließende Klammer Schließende Klammer »)« oder »\)« ohne passende öffnende Klammer Öffnende Klammer »[« oder »[^« ohne passende schließende Klammer Öffnende Klammer »\{« ohne passende schließende Klammer nicht unterstützter Algorithmus »%s«.
 Nicht unterstützte Art der Auflistung; Versuch Unix-Auflistung zu verwenden.
 Qualität des Schutzes »%s« ist nicht unterstützt.
 Nicht unterstütztes Schema %s Unvollständige numerische IPv6-Adresse Syntax: %s NETRC [HOSTNAME]
 Syntax: %s [OPTION]... [URL]...
 Authentifizierung mit Benutzername/Passwort fehlgeschlagen.
 »%s« als temporäre Auflistungsdatei benutzen.
 WARC-Optionen:
 WARC-Ausgabe funktioniert nicht mit »--continue« oder »--start-pos«,
die Option wird deaktiviert.
 WARC-Ausgabe funktioniert nicht mit »--no-clobber«, »--no-clobber« wird deaktiviert.
 WARC-Ausgabe funktioniert nicht mit »--spider«.
 WARC-Ausgabe funktioniert nicht mit Zeitstempeln, Zeitstempel werden deaktiviert.
 WARNUNG WARNUNG: Die Option -O zusammen mit einer der Optionen -r oder -p
bedeutet, dass jeglicher Download in genau der angegebenen Datei
gespeichert wird.

 WARNUNG: Zeitstempel funktionieren nicht in Kombination mit der Option
»-O«.  Genauere Erläuterungen finden Sie im Handbuch.

 WARNUNG: Der Zufallszahlengenerator wird mit einem schwachen Wert initialisiert.
 Warnung: Joker-Zeichen werden bei HTTP nicht unterstützt.
 Wgetrc:  Beim Herunterladen der Signatur:
%s: %s.
 Verzeichnisse nicht erneut holen; da die Tiefe bereits %d ist (max. erlaubt %d).
 Schreiben schlug fehl; Kontroll-Verbindung schließen.
 HTML-artigen Index nach »%s« [%s] geschrieben.
 HTML-artiger Index nach »%s« geschrieben.
 Die Optionen »--body-data« und »--body-file« sind gemeinsam nicht erlaubt.
 Die Optionen »--ask-password« und »--password« sind gemeinsam nicht erlaubt.
 Die Optionen »--post-data« oder »--post-file« können nicht zusammen mit »--method« verwendet werden. Bei der Option »--method« werden die Daten mit den Optionen »--body-data« oder »--body-file« angegeben Für »--body-data« oder »--body-file« muss eine Methode mittels
»--method=HTTPMethod« angegeben werden.
 Ihre OpenSSL-Version ist zu alt, um TLSv1.1 zu unterstützen
 Ihre OpenSSL-Version ist zu alt, um TLSv1.2 zu unterstützen
 _open_osfhandle fehlgeschlagen » ai_family wird nicht unterstützt ai_socktype wird nicht unterstützt Kann keine Pipe erstellen Kann den Dateideskriptor %d nicht wiederherstellen: dup2 fehlgeschlagen verbunden.
 Konnte keine Verbindung zu »%s«, Port »%d« herstellen: %s
 cwd_count: %d
cwd_start: %d
cwd_end: %d
 fertig.
 fertig.   fertig.     fehlgeschlagen: %s.
 Fehler: Keine IPv4/IPv6 Adresse für den Host.
 fehlgeschlagen: Wartezeit abgelaufen.
 fake_fork() fehlgeschlagen
 fake_fork_child() fehlgeschlagen
 gmtime hat nicht funktioniert. Das ist vermutlich ein Fehler.
 idn_decode fehlgeschlagen (%d): %s
 idn_encode fehlgeschlagen (%d): %s
 übergangen ioctl() fehlgeschlagen.  Der Socket kann nicht auf blockieren eingestellt werden.
 locale_to_utf8: Lokale ist nicht gesetzt
 Speicher erschöpft kein Download notwendig.
 Zeit unbekannt        nicht spezifiziert 