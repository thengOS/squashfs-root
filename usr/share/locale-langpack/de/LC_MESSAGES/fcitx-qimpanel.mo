��    ;      �  O   �           	                     $     -     :  	   F  
   P     [  	   b  
   l  
   w     �     �     �     �     �  S   �               &     3     A  
   O  
   Z  
   e     p     �     �     �  
   �     �  	   �     �  	   �     �     �  
     )        @     H     Z     b     g  	   s     }     �     �  -   �     �  +   �                    ,     =     E  �  J  	   �	  
   
  
   
     
     
     .
     =
     L
     ^
     j
     p
     ~
     �
     �
     �
     �
     �
     �
  z   �
     s     {     �     �     �  
   �     �     �     �     �          0     =     I     V     b     y     �     �     �  -   �     �     �                    1     A     Y     j  >   �     �  2   �       	        )     :     M     U                    9                    /   '   (       
   *   .                   2   %                        6   	              &             #      ,   :              0   $   )                      +            4   8   ;      1       !   "                  7   5       -   3          &Apply &Cancel &Cannel &Ok &Refresh AdjustHeight AdjustWidth All Skins Appearance Author BackArrow BackArrowX BackArrowY BackImg Character Map ConfigureFcitx ConfigureIM ConfigureIMPanel Description: Click to select and preview, double-click local to edit, save locally. Exit FontSize ForwardArrow ForwardArrowX ForwardArrowY Horizontal IndexColor InputColor InputStringPosX InputStringPosY Keyboard Layout Chart MarginBottom MarginLeft MarginRight MarginTop Mozc Edit mode Mozc Tool No input window Online &Help! OtherColor Please install fcitx-qimpanel-configtool! Preview Qimpanel Settings Restart Skin Skin Design Skin Type SkinFont SkinInfo SkinInputBar Sougo Skin does not support preview and edit! Text Entry Settings... The default configuration has been restored Version Vertical Vertical List Virtual Keyboard Warning tips Project-Id-Version: fcitx-qimpanel
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-10 02:31+0000
PO-Revision-Date: 2016-06-14 08:08+0000
Last-Translator: Ettore Atalan <atalanttore@googlemail.com>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:28+0000
X-Generator: Launchpad (build 18115)
 &Anwenden &Abbrechen &Abbrechen &Ok &Aktualisieren Höhe anpassen Weite anpassen Alle Oberflächen Darstellung Autor Zurück-Pfeil Zurück-Pfeil X Zurück-Pfeil Y Hintergrundbild Zeichentabelle Fctix Konfigurieren IM konfigurieren IMPanel konfigurieren Beschreibung: Anklicken, um auszuwählen und eine Vorschau anzusehen; Doppelklicken zum Bearbeiten, und lokal zu speichern Beenden Schriftgröße Vorwärts-Pfeil Vorwärts-Pfeil X Vorwärts-Pfeil Y Horizontal Farbdatenbank Eingabefarbe Eingabe Text X Position Eingabe Text Y Position Abbild der Tastaturbelegung Unterer Rand Linker Rand Rechter Rand Oberer Rand Mozc-Bearbeitungsmodus Mozc-Werkzeug Kein Eingabefenster &Hilfe im Netz! Andere Farbe Bitte fcitx-qimpanel-configtool installieren! Vorschau Qimpanel-Einstellungen Neu starten Oberfläche Oberflächendesign Oberflächentyp Schrift der Oberfläche Oberflächeninfo Eingabeleiste der Oberfläche Sougo Oberfläche unterstützt keine Vorschau und Bearbeitung! Texteingabeeinstellungen … Die Standardeinstellungen wurden wiederhergestellt Version Senkrecht Senkrechte Liste Bildschirmtastatur Achtung Tipps 