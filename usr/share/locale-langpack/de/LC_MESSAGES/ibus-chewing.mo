��    B      ,  Y   <      �     �     �  ?   �            ,   &     S     X     k     s     �     �  4   �     �     �          !     2     B     O     ^     p     �     �     �  .   �     �     �     �  C   �     <  �   P  `   �     K	     T	  w   b	     �	  P   �	     F
     d
  �   �
     r     �     �  	   �     �     �      �     �          (     5     G     U     r  	   w     �     �  
   �     �     �     �     �     �     �  �  �     z     �  K   �          
  1        H     M     b     j     }     �  :   �  1   �  /        <     E  
   M     X  	   [     e     |  
   �     �     �  $   �  %   �            D        P  �   c  t   &     �     �  �   �     2  F   O      �  "   �  	  �     �     �           5     =     K  3   W  /   �     �     �     �     �     �       	        &     /  
   6     A     F     M     V     \     `            /   ?   4                B       A   "       *                9   #   6       8          	          >            &          3          !      <                  %   5           +                        
   ,   2   :   7           @      ;       1       .      )               '       0   -      =      $          (    Add phrases in front Add phrases in the front. Always input numbers when number keys from key pad is inputted. Auto Auto move cursor Automatically move cursor to next character. Big5 Candidate per page Chewing Chewing component Chi Choose phrases from backward Choose phrases from the back, without moving cursor. Click to switch to Chinese Click to switch to English ConfigureApply ConfigureCancel ConfigureClose ConfigureOk ConfigureSave Easy symbol input Easy symbol input. Editing Eng Esc clean all buffer Escape key cleans the text in pre-edit-buffer. Force lowercase in En mode Full Half Hsu's keyboard selection keys, 1 for asdfjkl789, 2 for asdfzxcv89 . Hsu's selection key Ignore CapsLock status and input lowercase by default.
It is handy if you wish to enter lowercase by default.
Uppercase can still be inputted with Shift. In plain Zhuyin mode, automatic candidate selection and related options are disabled or ignored. Keyboard Keyboard Type Keys used to select candidate. For example "asdfghjkl;", press 'a' to select the 1st candidate, 's' for 2nd, and so on. Maximum Chinese characters Maximum Chinese characters in pre-edit buffer, including inputing Zhuyin symbols Number of candidate per page. Number pad always input number Occasionally, the CapsLock status does not match the IM, this option determines how these status be synchronized. Valid values:
"disable": Do nothing.
"keyboard": IM status follows keyboard status.
"IM": Keyboard status follows IM status. Peng Huang, Ding-Yi Chen Plain Zhuyin mode Select Zhuyin keyboard layout. Selecting Selection keys Setting Shift key to toggle Chinese Mode Shift toggle Chinese Mode Space to select Syncdisable Syncinput method Synckeyboard Sync between CapsLock and IM UTF8 dachen_26 default dvorak dvorak_hsu eten eten26 gin_yieh hanyu hsu ibm Project-Id-Version: ibus-chewing 1.4.11
Report-Msgid-Bugs-To: Ding-Yi Chen <dchen at redhat.com>
POT-Creation-Date: 2016-01-21 05:14+0000
PO-Revision-Date: 2015-12-05 14:34+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German (Germany)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:39+0000
X-Generator: Launchpad (build 18115)
Language: de
 Ausdrücke am Anfang einfügen Ausdrücke am Anfang einfügen. Immer Zahlen eingeben, wenn Zahlentasten vom Nummernblock verwendet werden. Auto Auto-Cursor Cursor automatisch zum nächsten Zeichen bewegen. Big5 Kandidaten pro Seite Chewing Chewing-Komponente Chi Ausdrücke von hinten wählen Ausdrücke von hinten wählen, ohne den Cursor zu bewegen. Klicken um in die chinesische Sprache zu wechseln Klicken um in die englische Sprache zu wechseln Anwenden Abbruch Schließen Ok Speichern Einfache Symboleingabe Einfache Symboleingabe. Bearbeiten Eng Esc löscht Puffer Escape-Taste löscht Text im Puffer. Kleinschreibung in En-Modus erzwingen Voll Halb Hsu's Tastaturauswahl-Tasten, 1 für asdfjkl789, 2 für asdfzxcv89 . Hsu's Auswahltaste CapsLock-Status ignorieren und standardmäßig Kleinschreibung verwenden.
Praktisch für die standardmäßige Eingabe in Kleinschreibung.
Großschreibung kann per Umschalttaste aktiviert werden. In einfachem Zhuyin-Modus werden automatische Kandidatenauswahl und zugehörige Optionen deaktiviert oder ignoriert. Tastatur Tastaturtyp Tasten zur Kandidatenauswahl. Zum Beispiel "asdfghjkl;", drücken Sie 'a', um den 1. Kandidaten zu wählen, 's' für den 2., etc. Maximale Chinesische Zeichen Maximale Chinesische Zeichen im Puffer, einschließlich Zhuyin-Symbole Anzahl der Kandidaten pro Seite. Nummernblock gibt immer Zahlen ein Gelegentlich stimmt der CapsLock-Status nicht mit der IM überein, diese Option legt fest, wie diese Status  synchronisiert werden. Gültige Werte:
"Deaktivieren": Nichts tun.
"Tastatur": IM-Status folgt dem Tastaturstatus.
"IM": Tastaturstatus folgt dem IM-Status. Peng Huang, Ding-Yi Chen Einfacher Zhuyin-Modus Zhuyin-Tastaturbelegung wählen. Wählen Auswahltasten Einstellung Hochstelltaste zum Wechsel auf China-Modus drücken Hochstelltaste schaltet China-Modus ein und aus Leertaste zum Auswählen Deaktivieren Eingabemethode Tastatur Sync zwischen CapsLock und IM UTF8 dachen_26 Standard dvorak dvorak_hsu eten eten26 gin_yieh hanyu hsu ibm 