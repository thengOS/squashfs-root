��    �     �  �  �-      �<  �   �<  c   r=  	   �=     �=     �=  �   �=  �   {>  �   D?  �   @  !   �@     �@  	   �@     A     A     A  "   !A    DA     LD  #   gD  E   �D     �D  #   �D  !   E  5   %E  I   [E  8   �E  $   �E  3   F  &   7F  +   ^F     �F     �F  *   �F     �F  /   �F  ?   )G  3   iG  2   �G  7   �G  /   H  9   8H     rH  7   �H  .   �H  F   �H  D   >I  E   �I  C   �I  C   J  =   QJ  <   �J  8   �J  I   K  A   OK  #   �K  
   �K     �K     �K     �K  J   �K  B   IL  F   �L  A   �L  P   M     fM  R   �M  a   �M  G   8N  L   �N     �N     �N     �N     O     O  *   ,O     WO     jO     yO  	   �O  "   �O     �O     �O  (   �O  .   �O  /    P  V   PP  <   �P  9   �P  7   Q  !   VQ  )   xQ  &   �Q  +   �Q  )   �Q  9   R  Z   YR     �R  �   �R  M   NS  ?   �S  <   �S  >   T  i   XT  k   �T  ]   .U     �U  #   �U     �U     �U  �   �U  -   �V     �V     �V  C   �V  +   ?W     kW  ?   ~W     �W     �W  
   �W  "   �W  /   X     @X     IX  n   ]X  n   �X     ;Y  3   KY     Y     �Y     �Y  2   �Y  6   �Y  N   Z  M   jZ  P   �Z  Q   	[  	   [[  
   e[  #   p[  %   �[     �[     �[  J   �[     \  )   #\     M\  r   T\     �\     �\  )   �\     ]  
   ]     ']     8]  1   J]  �   |]     ^     0^  3   K^     ^     �^     �^     �^     �^     �^     �^     �^  )   _     5_     C_  G   R_     �_     �_  :   �_  f   �_  S   [`     �`     �`     �`  	   �`  $   �`     a     a  
   +a  %   6a     \a     ta  1   �a     �a  	   �a     �a  	   �a     �a  :   �a  7   4b  :   lb     �b     �b     �b     �b     �b     �b  (   c     /c     Fc     Sc  "   ac     �c     �c     �c     �c     �c     �c     �c     �c     �c     �c  
   d     d  /    d  *   Pd  )   {d  9   �d     �d     �d      e  -   e  
   Ae  [  Le  E  �g     �h  �   �i  �  �j  �  }l  �  Yn  �   �o  �   �p  �   rq  �   \r  �   5s    �s  /  �t  �   v    �v    �w  �    y  �  �y  �   m{  �   
|  '  �|  �   %~  �   �~  �   �  (  ��    ��  T  Ʉ  K  �  �   j�  �   C�  ?  �  �  K�  �  �  �  ��  K  ��  ~  ��  o  s�  >  �  V  "�  h  y�  �   �  �   ��  �   l�  )  i�  m  ��  m  �  o  o�  q  ߤ  m  Q�  �  ��  n  V�  r  Ū  q  8�  �  ��  �  Z�  �  $�  �  ��  �  j�  �  ��  �  ��  �  "�  �  ��  �  ��  �  �  U  ��  c  �  M  l�  \  ��  �  �  �  �  �  ��  �  ��  �  ��  �  ��  �  ~�  �  b�  �  A�  �  2�    (�  �  1�  �  �    �    �  �  #�  .  �  \  F�    ��  |  ��  �   4�  �  �  �  ��  �  8�  �  ��    p�    ��  �   ��    o�  %  }�    ��    ��    ��  V  ��  \  3�  <  ��    �  �   � �   � �   t    4  " �   W R  L Z  �	 �   �
 �   � �   � �  > �  � k  � `  9 A  � y  � �   V �    �    2  � j  �   = ,  B �   o   S   o ?  w  5  �! h  �" -  V$   �& ;  �' �   �( �   �) `  �* N  �+ �   C.   / �  0 �  �1 �  �3 S  k5 �   �6 �   l7 I  8 �   X9 �  : ]  �; 4  V= Q  �> �  �? U  gA ,  �B e  �C H  PE �   �F   xG   �H E  �I �   �J   �K   �L �  �M �   �O   �P 3  �Q ,  �R �   T   U   $V �  CW j   �X i   ZY i   �Y h   .Z I  �Z   �\ �  �^ �  �a �   Pc   Jd �  Zf �   �g �   �h �   �i �   }j �  Ok �   On   %o    (p �   Iq �   �q �   �r N  �s @  
u F  Kv 9  �w J  �x �   z �   �z �   �{   �|   �} �   �~ +  �   ��   Á �  ߂ �  �� f  }� ]  � 5  B� D  x� �  �� �  [� �  � �  Ԑ �  W� f   ��    `�    � @   ��    ��    � 8   �� P   7�    �� '   �� 1   ŕ    ��    �    $�    ;�    X� #   a� :   �� *   ��    � (   �� -   '� 0   U�    ��    ��    �� Z   �� .   �    0�    N�    m�    ��    �� /   ��    �    � X    � U   y�    ϙ    �    ��    �    (� '   G� *   o� 2   �� 5   ͚ )   �    -�    F�    `�    {�    ��    ��    қ [   � X   L� [   �� X   �    Z� 7   n� ?   �� $   �    � A   � 9   [� J   �� 1   �� J   � H   ]� i   �� >   � J   O� D   �� (   ߠ    � 
   %� �   0� 
   ޡ 
   �    �� .   � 3   :� .   n� 3   ��    Ѣ    ֢    �    � L  �    Z�    n�    |�    ��    �� #   �� !   Ƥ    �    ��    �    �    &�    E�    R�    `�    p�    ��    ��    ��    ��    �� $   �� 
   ܥ    �    �    � @   '� A   h� B   �� B   �� A   0� B   r� A   �� A   �� D   9� B   ~� E   ��    � *   � �   C� 0   � #   � K   A�     �� E   �� B   �� �   7�    ȫ 0   � !   � `   8�    �� 7   �� F   � 2   8� 0   k�    �� �   �� �   =� p   Ү �   C� m   կ �   C� o   װ �   G� l   ر a   E� N   �� �   �� ;   �� 	   �    � A  � `   J� 9   �� 0   � o   � �   �� �   T� }   ط �   V� �   ٸ Z   p� W   ˹ �   #� �   � :   �� *   � B   � �   a� {   7� �   �� �   I� {   ̾ �   H� }   �� �   t� {   $� �   �� }   N� �   �� {   |� �   ��    �� �   &�    �� �   X� {   
� �   �� \   4� [   �� E   �� F   3� �   z� �  )� Q   �� O    � �   p� U   i�    �� N   ��    )�    8� D   J� 	   ��    ��    �� 	   ��    ��    ��    ��    ��    ��    � E   $� 0   j� +   �� &   �� 3   ��    "� @   A� F   �� ?   �� /   	� @   9� 5   z� V   �� H   � e   P�    ��    �� �   �� #   ��    ��    �� ?   ��    !� 0   )�    Z�    h�    n�    v� 	   z�    ��    ��    ��    ��    ��    �� 5   ��    �    � A   0� 6   r�    ��    ��    �� 
   ��     �� !   �� @   �    E� �  I� �   �� �   ��    >�    K� 	   S� �   ]� �   %� �   � �   � "   �    6�    <�    E�    `�    h� #   l�   ��    �� +   �� `   ��    N� K   _� 2   �� I   �� i   (� Y   �� N   �� a   ;� M   �� W   �� C   C� :   �� 4   ��    �� A   � ]   R� W   �� [   � W   d� H   �� f   �     l� ?   �� m   �� }   ;� ~   �� |   8� }   �� ^   3� Z   �� E   �� o   3� �   �� R   $� K   w� 
   ��    ��    ��     �� h   � X   �� a   �� U   >� h   �� !   �� <   � {   \� Y   �� O   2� "   ��    ��    ��    ��    �� K   ��    B�    \�    n�    w�    �� 
   ��    �� D   �� U   �� R   Q� �   �� n   '� H   �� G   �� 6   '� D   ^� 6   �� 7   �� 5   � j   H� e   ��    � �   6� _   �� S   I� U   �� Y   �� �   M� �   �� �   Z� &   �� ?   !�    a�    m� �   }� 9   @� .   z� /   �� R   �� J   ,�     w� A   ��    �� '   ��    � &   � 6   B� 
   y�    �� �   �� n   (�    �� U   ��     �    �     � <   ?� ;   |� O   �� `     f   i  b   �     3    C    T 0   t    �    � z   �    = 2   D    w |       �     4       H    g    x    � 9   � �   � .   � (   � Y   �    :    S    a    {    �    �    �    � 2   � 
        �   '    �    � >   � �    �   �        +    : 	   ? 9   I    �    �    � +   �     �    	 N   "	 
   q	 
   |	    �	    �	    �	 E   �	 @   
 \   D
    �
    �
    �
    �
    �
    �
 (    +   -    Y    h +   x 
   �    �    �    �    �    �    �    	        '    :    I 4   \ /   � .   � L   �    =    W )   f B   � 
   � �  � �  � L  > �   � c  n g  � �  : �    9  � c  8   � �   � J  Y  �  �! E  X# 5  �$ �  �% �   e' J  '( �   r* 5  2+ i  h, �   �-   �. <  �/ �  	1 �  �3   �6 �  �9 N  �< �   �= t  �>   c@ w  pC q  �F |  ZI e  �J m  =N �  �O v  aQ �  �R �   `T �   EU �  2V �  �W �  iY �  '[ �  �\ �  �^ �  d` �  b �  �c �  �e �  og �  Fi )  <k   fm   �o �  �q �  �s �  nu �  \w �  8y   { �  *} �  ( �  �� �  q� �  � b  �� v  � B  �� b  Ҍ d  5� ?  �� E  ړ M   � f  n� �  ՚ �  c� _  � q  n� �  � �  d� l  � �  Y� �  2� �  ݰ   ��   �� �  �� �  � �  _� �  8� �  !� �  ��   �� @  �� b  � M  r� K  �� W  � �  d� �  � �  �� @  c�   �� L  �� �   � }  � �  �� 7  
� �  B� �  ��   ��   ��   �� �  �� H  �� �  �� �  �� �  y� �  � =  � �  B� �   �� b  �� �  � _  �� �  1� 4  �� �  � n  �� �  O� i  O� �  �� �  |� �   �  � �   � %   �  � o  �	 �    2  � �   ,   �  ; �  � �   � �   k �  R �   # �  � �  � S  " �  v! J  0# �  {% �  B'   �( 
  �* C  �, �  *. y  �/ �  N1   @3 �  N4 �  �5 e  �7 2  �9 c  ; �  �< �  /> 0  �? �  �@ �  �B   DD �   MF �   �F �   lG �   �G �  �H �  {K   N :  -Q O  hS �  �T 0  �W _  �Y S  [ �   p\ -  f]   �^ D  �a �  �b �  �d �   *f f  g o  hh �  �i �  �k �  Pm �  o �  �p +  �r *  �s =  �t t  v �  �w   y �  z d  �{ �   } [  �~ �  � �  փ    �  Ӈ �  �� �  Q� �  7� �  � $  v� �  �� �   }� +   � $   ;� b   `�    ×    �� \   � d   J�    �� 1   Ș H   ��    C�    W�    n� -   ��    �� +   �� D   � )   -�    W� \   r� T   Ϛ V   $�    {�    �� 
   �� n   �� A   � 0   U� 1   �� .   �� ,   � '   � D   <� (   �� ,   �� o   ם u   G� !   �� '   ߞ +   � )   3� 1   ]� :   �� 2   ʟ E   �� H   C� <   �� +   ɠ ,   �� -   "� -   P� /   ~� 1   �� 0   � n   � t   �� g   �� m   ]�    ˣ _   � O   K� 3   ��    Ϥ \   � \   D� S   �� 8   �� T   .� R   �� u   ֦ F   L� T   �� M   � =   6�    t� 
   �� �   ��    ��    ��    �� -   ɩ 2   �� -   *� 2   X�    ��    ��    ��    ��   ۪ #   [�    �    ��    ��    �� 0   �� /   �    �    (�    B�    Z�    n�    ��    ��    ��    ȭ    �    ��    �    �    � /   "�    R� &   `�    ��    �� V   �� X   � X   h� Y   �� X   � X   t� W   Ͱ W   %� Z   }� X   ر [   1�    �� 5   �� �   ղ Z   �� ;   �� V   5� (   �� k   �� d   !� �   �� %   O� 9   u� *   �� i   ڶ !   D� @   f� Q   �� <   �� F   6�    }� �   �� �   L� �   � �   �� �   m� �   � �   ռ �   �� �   H� �   � l   x� %  � R   � 	   ^� 4   h� h  �� y   � b   �� :   �� s   �   �� �   �� �   ;� �   �� �   ��    i� w   �� 	  a�   k� E   �� 5   �� d   ��   d� �   t� �   �� �   �� �   c� �   �� �   �� �   l� �   K�   � �   
�   �� �   �� �   d� �   ?� �   �� �   ��   v� �   ~�   9� i   =� h   �� Q   � V   b� �   �� 1  �� ]   �� [   �   z� h   ��    �� �   �    ��    �� p   ��    <� 	   K�    U� 	   ^�    h� 
   {�    ��    ��    ��    �� P   �� A   A� =   �� 6   �� D   �� .   =� L   l� x   �� N   2� a   �� k   �� \   O� g   �� �   � p   ��    
�    �   4� 0   6�    g�    x� n   ��    �� F   �    M�    _�    h�    w�    }�    ��    ��    ��    ��    ��    �� d   �� !   L� 4   n� A   �� L   ��    2�    7�    E�    L� $   Z� $   � Y   ��    ��    r  N  �   �  �         �  �   �  �   J       S    i  �     �       g  �  �  �   �  w  �       ;   �   4    �  F  O  �  �               �       {  �  �   g  �      G   �   �      p  }      �   �   �  {  e   �           �   �  �  j  �  �  �         ~      m   �   �   &      V       �        `       5  �  �  �   ?         5  �       Q  0  �  q  `  h  �  N  Y   �  L      �                 �   �       )  �       S      �  �  �   V  D            �         �    �   /  �      j      �   U  I  ]   �   �  +  �   f  �  �  T  �  �  �  f       �     c  �   �          �      �   �               �  .                  �   >  .  t   �  /  6    �  p  k  z   �  z          :  !       �        =           �      �  �      �  �      �  C  *  _          J  �  �  �  @   �     '      �  C      [  >  *  �   i  _              �   �      �   #         �  <       �   8   t             �   v      7      a  �  y  �      Q   �    �   G  �       �  �      Z      �          o   x      �   #       �         �              v  [  A       �      �   �   �  �  �  �          n     �  "          �    <  B  �  H  �  n  �  �       �  �  �  �  �  M   �  �     �   �  �     �      $      �   �  F       �  W              '   E     �   @              2           ;      �              R     5            4  �      �       -  /   �  �  &   7  1  �   �  S   
   �  �  W    �     �       X      s  �   �  �         �  �   e  �    b       �   �   �  B      e  �  �       �  u  �      l   Z  �   �          �      �  �     �           �         �      c  "       x  �          �  C  ,   �  �      �  �   �   �  O        �  ^   �      ,  �   �  <  �   9          d   �      �  6   �        �      �  \  �           W   H  P  �   �     �  q   0  ;      �  �  �              x   6  E       Q  z  -  �  |          �  �       �      !     �   �   �   �   �  }    �  :        ]        ?  �      I         K  �  _   9    �  �          y  )  3   �  %   D   @  �      4   �    |  +       o  �       �  �   Z   �  �  o  �      �   [       	                     �          K  \   g   R          3  �                          �  D  �  )   l  L     �   j   "      �  G  %      �   �  h       �  $           �   �  q  a  r  �   �   �  0   �      �      �   �       n  P    w   �       �       �  �       �   d  �  v       �   �  K   9         1  �   s   P       ^  �      J  �  8  =  7           ]  �    ,  �  �   �  
  2    �  �           �   {               k      �   �     �   s  �       �  �  U     �   #  !      \      N       �  �  	  �  �   �  �                               �   B           �           �     �  �  ~  �   �     �  X       %      H   b  �      M      k  -   2              m  �   �           �  V      �  	  �        �      �  �  �      F  X  �  3  �   c   �     �  L      .   (  �   �  d  �      �  �        w  y   Y          �  �   �  �      *   �         �          >   �   �  u      l     $  h  �  �   �  �   �  b  A  t    `  u       �  ~   �                         �  �   �      �    �  |   }   �      T  �      �  �       �   I   �   ^    8  p       Y  r   �           a   �  ?   �  E            O  �  :   m  �   �   �   U            �  T     �  A  �           �  �       �  +  �  �   R   (       �  i   �  f             �    �  =  '            
      (  �  1       �         &  �     �   �       �             �  �         �  M       

Warning: Some tests could cause your system to freeze or become unresponsive. Please save all your work and close all other running applications before beginning the testing process.    Determine if we need to run tests specific to portable computers that may not apply to desktops.  Results   Run   Selection   Takes multiple pictures based on the resolutions supported by the camera and
 validates their size and that they are of a valid format.  Test that the system's wireless hardware can connect to a router using the
 802.11a protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11a protocol.  Test that the system's wireless hardware can connect to a router using the
 802.11b protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11b protocol.  Test that the system's wireless hardware can connect to a router using the
 802.11g protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11g protocol. %(key_name)s key has been pressed &No &Previous &Skip this test &Test &Yes 10 tests completed out of 30 (30%) <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'Ubuntu'; font-size:10pt; font-weight:400; font-style:normal;">
<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=;">Please enter the e-mail address associated with your Launchpad account (if applicable)</span></p>
<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=;">and click the Submit Results button to submit these test results to Launchpad.</span></p></body></html> Abort signal from abort(3) All required keys have been tested! Archives the piglit-summary directory into the piglit-results.tar.gz. Are you sure? Attach log from fwts wakealarm test Attach log from rendercheck tests Attaches a copy of /var/log/dmesg to the test results Attaches a dump of the udev database showing system hardware information. Attaches a list of the currently running kernel modules. Attaches a report of CPU information Attaches a report of installed codecs for Intel HDA Attaches a report of sysfs attributes. Attaches a tarball of gcov data if present. Attaches dmidecode output Attaches info on DMI Attaches information about disk partitions Attaches lshw output Attaches the FWTS results log to the submission Attaches the audio hardware data collection log to the results. Attaches the bootchart log for bootchart test runs. Attaches the bootchart png file for bootchart runs Attaches the contents of /proc/acpi/sleep if it exists. Attaches the contents of the /etc/modules file. Attaches the contents of the various modprobe conf files. Attaches the firmware version Attaches the graphics stress results to the submission. Attaches the installer debug log if it exists. Attaches the log from the 250 cycle Hibernate/Resume test if it exists Attaches the log from the 250 cycle Suspend/Resume test if it exists Attaches the log from the 30 cycle Hibernate/Resume test if it exists Attaches the log from the 30 cycle Suspend/Resume test if it exists Attaches the log from the single suspend/resume test to the results Attaches the log generated by cpu/scaling_test to the results Attaches the output of udev_resource, for debugging purposes Attaches the screenshot captured in graphics/screenshot. Attaches the screenshot captured in graphics/screenshot_fullscreen_video. Attaches very verbose lspci output (with central database Query). Attaches very verbose lspci output. Audio Test Audio tests Automated CD write test Automated DVD write test. Automated check of the 30 cycle hibernate log for errors detected by fwts. Automated check of the hibernate log for errors discovered by fwts Automated check of the suspend log to look for errors reported by fwts Automated job to generate the PXE verification test for each NIC. Automated job to generate the Remote Shared IPMI verification test for each NIC. Automated optical read test. Automated test case to make sure that it's possible to download files through HTTP Automated test case to verify availability of some system on the network using ICMP ECHO packets. Automated test to store bluetooth device information in checkbox report Automated test to walk multiple network cards and test each one in sequence. Benchmark for each disk Benchmarks tests Bluetooth Test Bluetooth tests Bootchart information. Broken pipe: write to pipe with no readers Building report... CD write test. CPU Test CPU tests CPU utilization on an idle system. Camera Test Camera tests Check failed result from shell test case Check job is executed when dependency succeeds Check job is executed when requirements are met Check job result is set to "not required on this system" when requirements are not met Check job result is set to uninitiated when dependency fails Check logs for the stress poweroff (100 cycles) test case Check logs for the stress reboot (100 cycles) test case Check stats changes for each disk Check success result from shell test case Check that VESA drivers are not in use Check that hardware is able to run Unity 3D Check that hardware is able to run compiz Check the time needed to reconnect to a WIFI access point Check to see if CONFIG_NO_HZ is set in the kernel (this is just a simple regression check) Checkbox System Testing Checkbox did not finish completely.
Do you want to rerun the last test,
continue to the next test, or
restart from the beginning? Checks that a specified sources list file contains the requested repositories Checks the battery drain during idle.  Reports time until empty Checks the battery drain during suspend.  Reports time until Checks the battery drain while watching a movie.  Reports time Checks the length of time it takes to reconnect an existing wifi connection after a suspend/resume cycle. Checks the length of time it takes to reconnect an existing wired connection
 after a suspend/resume cycle. Checks the sleep times to ensure that a machine suspends and resumes within a given threshold Child stopped or terminated Choose tests to run on your system: Codec tests Collapse All Collect audio-related system information. This data can be used to simulate this computer's audio subsystem and perform more detailed tests under a controlled environment. Collect info on color depth and pixel format. Collect info on fresh rate. Collect info on graphic memory. Collect info on graphics modes (screen resolution and refresh rate) Combine with character above to expand node Command not found. Command received signal %(signal_name)s: %(signal_description)s Comments Common Document Types Test Components Configuration override parameters. Connection established, but lost {}% of packets Continue Continue if stopped Creates a mobile broadband connection for a CDMA based modem and checks the connection to ensure it's working. Creates a mobile broadband connection for a GSM based modem and checks the connection to ensure it's working.  DVD write test. Dependencies are missing so some jobs will not run. Deselect All Deselect all Detailed information... Detects and displays disks attached to the system. Detects and shows USB devices attached to this system. Determine whether the screen is detected as a multitouch device automatically. Determine whether the screen is detected as a non-touch device automatically. Determine whether the touchpad is detected as a multitouch device automatically. Determine whether the touchpad is detected as a singletouch device automatically. Disk Test Disk tests Disk utilization on an idle system. Do you really want to skip this test? Don't ask me again Done Dumps memory info to a file for comparison after suspend test has been run Email Email address must be in a proper format. Email: Ensure the current resolution meets or exceeds the recommended minimum resolution (800x600). See here for details: Enter text:
 Error Exchanging information with the server... Executing %(test_name)s Expand All ExpressCard Test ExpressCard tests FAIL: {}% packet loss is higherthan {}% threshold Failed to contact server. Please try
again or upload the following file name:
%s

directly to the system database:
https://launchpad.net/+hwdb/+submit Failed to open file '%s': %s Failed to process form: %s Failed to upload to server,
please try again later. Fingerprint reader tests Firewire Test Firewire disk tests Floating point exception Floppy disk tests Floppy test Form Further information: Gathering information from your system... Graphics Test Graphics tests Hangup detected on controlling terminal or death of controlling process Hibernation tests Hotkey tests I will exit automatically once all keys have been pressed. If a key is not present in your keyboard, press the 'Skip' button below it to remove it from the test. If your keyboard lacks one or more keys, press its number to skip testing that key. Illegal Instruction In Progress Info Info Test Information not posted to Launchpad. Informational tests Input Devices tests Input Test Internet connection fully established Interrupt from keyboard Invalid memory reference Jobs will be reordered to fix broken dependencies Key test Keys Test Kill signal LED tests List USB devices Lists the device driver and version for all audio devices. Make sure that the RTC (Real-Time Clock) device exists. Maximum disk space used during a default installation test Media Card Test Media Card tests Memory Test Memory tests Miscellanea Test Miscellaneous tests Missing configuration file as argument.
 Mobile broadband tests Monitor Test Monitor tests Move a 3D window around the screen Ne&xt Ne_xt Network Information Networking Test Networking tests Next No Internet connection Not Resolved Not Started Not Supported Not Tested Not required One of debug, info, warning, error or critical. Open and close 4 3D windows multiple times Open and close a 3D window multiple times Open, suspend resume and close a 3D window multiple times Optical Drive tests Optical Test Optical read test. PASS: {}% packet loss is within {}% threshold PASSWORD:  PURPOSE:
     Check that external line out connection works correctly
STEPS:
     1. Insert cable to speakers (with built-in amplifiers) on the line out port
     2. Open system sound preferences, 'Output' tab, select 'Line-out' on the connector list. Click the Test button
     3. On the system sound preferences, select 'Internal Audio' on the device list and click 'Test Speakers' to check left and right channel
VERIFICATION:
     1. Do you hear a sound in the speakers? The internal speakers should *not* be muted automatically
     2. Do you hear the sound coming out on the corresponding channel? PURPOSE:
    Audio Mute LED verification.
STEPS:
    Skip this test if your system does not have a special Audio Mute LED.
    1. Press the Mute key twice and observe the Audio LED to determine if it
    either turned off and on or changed colors.
VERIFICATION:
    Did the Audio LED turn on and off change color as expected? PURPOSE:
    Block cap keys LED verification
STEPS:
    1. Press "Block Cap Keys" to activate/deactivate cap keys blocking
    2. Cap Keys LED should be switched on/off every time the key is pressed
VERIFICATION:
    Did the Cap Keys LED light as expected? PURPOSE:
    Camera LED verification
STEPS:
    1. Select Test to activate camera
    2. Camera LED should light for a few seconds
VERIFICATION:
    Did the camera LED light? PURPOSE:
    Check that balance control works correctly on external headphone
STEPS:
    1. Check that moving the balance slider from left to right works smoothly
    2. Click the Test button to play an audio tone for 10 seconds.
    3. Move the balance slider from left to right and back.
    4. Check that actual headphone audio balance follows your setting.
VERIFICATION:
    Does the slider move smoothly, as well as being followed by the setting by the actual audio output? PURPOSE:
    Check that balance control works correctly on internal speakers
STEPS:
    1. Check that moving the balance slider from left to right works smoothly
    2. Click the Test button to play an audio tone for 10 seconds.
    3. Move the balance slider from left to right and back.
    4. Check that actual speaker audio balance follows your setting.
VERIFICATION:
    Does the slider move smoothly, as well as being followed by the setting by the actual audio output? PURPOSE:
    Check that external line in connection works correctly
STEPS:
    1. Use a cable to connect the line in port to an external line out source.
    2. Open system sound preferences, 'Input' tab, select 'Line-in' on the connector list. Click the Test button
    3. After a few seconds, your recording will be played back to you.
VERIFICATION:
    Did you hear your recording? PURPOSE:
    Check that the various audio channels are working properly
STEPS:
    1. Click the Test button
VERIFICATION:
    You should clearly hear a voice from the different audio channels PURPOSE:
    Check touchscreen drag & drop
STEPS:
    1. Double tap, hold, and drag an object on the desktop
    2. Drop the object in a different location
VERIFICATION:
    Does the object select and drag and drop? PURPOSE:
    Check touchscreen pinch gesture for zoom
STEPS:
    1. Place two fingers on the screen and pinch them together
    2. Place two fingers on the screen and move then apart
VERIFICATION:
    Does the screen zoom in and out? PURPOSE:
    Check touchscreen tap recognition
STEPS:
    1. Tap an object on the screen with finger. The cursor should jump to location tapped and object should highlight
VERIFICATION:
    Does tap recognition work? PURPOSE:
    Create jobs that use the CPU as much as possible for two hours. The test is considered passed if the system does not freeze. PURPOSE:
    DisplayPort audio interface verification
STEPS:
    1. Plug an external DisplayPort device with sound (Use only one HDMI/DisplayPort interface at a time for this test)
    2. Click the Test button
VERIFICATION:
    Did you hear the sound from the DisplayPort device? PURPOSE:
    Do some challenging operations then check for lockup on the GPU
STEPS:
    1. Create 2 glxgears windows and move them quickly
    2. Switch workspaces with wmctrl
    3. Launch an HTML5 video playback in firefox
VERIFICATION:
    After a 60s workload, check kern.log for reported GPU errors PURPOSE:
    HDD LED verification
STEPS:
    1. Select "Test" to write and read a temporary file for a few seconds
    2. HDD LED should light when writing to/reading from HDD
VERIFICATION:
    Did the HDD LED light? PURPOSE:
    HDMI audio interface verification
STEPS:
    1. Plug an external HDMI device with sound (Use only one HDMI/DisplayPort interface at a time for this test)
    2. Click the Test button
VERIFICATION:
    Did you hear the sound from the HDMI device? PURPOSE:
    Keep tester related information in the report
STEPS:
    1. Tester Information
    2. Please enter the following information in the comments field:
       a. Name
       b. Email Address
       c. Reason for this test run
VERIFICATION:
    Nothing to verify for this test PURPOSE:
    Manual detection of accelerometer.
STEPS:
    1. Look at the specifications for your system.
VERIFICATION:
    Is this system supposed to have an accelerometer? PURPOSE:
    Numeric keypad LED verification
STEPS:
    1. Press "Block Num" key to toggle numeric keypad LED
    2. Click on the "Test" button to open a window to verify your typing
    3. Type using the numeric keypad both when the LED is on and off
VERIFICATION:
    1. Numeric keypad LED status should toggle everytime the "Block Num" key is pressed
    2. Numbers should only be entered in the keyboard verification window when the LED is on PURPOSE:
    Power LED verification
STEPS:
    1. Power LED should be on while device is switched on
VERIFICATION:
    Does the power LED light as expected? PURPOSE:
    Power LED verification
STEPS:
    1. The Power LED should blink or change color while the system is suspended
VERIFICATION:
    Did the Power LED blink or change color while the system was suspended for the previous suspend test? PURPOSE:
    Suspend LED verification.
STEPS:
    Skip this test if your system does not have a dedicated Suspend LED.
    1. The Suspend LED should blink or change color while the system is
    suspended
VERIFICATION
    Did the Suspend LED blink or change color while the system was suspended? PURPOSE:
    Take a screengrab of the current screen (logged on Unity desktop)
STEPS:
    1. Take picture using USB webcam
VERIFICATION:
    Review attachment manually later PURPOSE:
    Take a screengrab of the current screen after suspend (logged on Unity desktop)
STEPS:
    1. Take picture using USB webcam
VERIFICATION:
    Review attachment manually later PURPOSE:
    Take a screengrab of the current screen during fullscreen video playback
STEPS:
    1. Start a fullscreen video playback
    2. Take picture using USB webcam after a few seconds
VERIFICATION:
    Review attachment manually later PURPOSE:
    Tests that the systems wireless hardware can connect to a router using
    no security and the 802.11b/g protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the B and G wireless bands
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use no security
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using
    no security and the 802.11n protocol.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the N wireless band
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use no security
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using WPA
    security and the 802.11b/g protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the B and G wireless bands
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use WPA2 and ensure the PSK matches that set in ROUTER_PSK
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using WPA
    security and the 802.11n protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the N wireless band
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use WPA2 and ensure the PSK matches that set in ROUTER_PSK
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    This test checks that the manual plugin works fine
STEPS:
    1. Add a comment
    2. Set the result as passed
VERIFICATION:
    Check that in the report the result is passed and the comment is displayed PURPOSE:
    This test cycles through the detected video modes
STEPS:
    1. Click "Test" to start cycling through the video modes
VERIFICATION:
    Did the screen appear to be working for each mode? PURPOSE:
    This test tests the basic 3D capabilities of your video card
STEPS:
    1. Click "Test" to execute an OpenGL demo. Press ESC at any time to close.
    2. Verify that the animation is not jerky or slow.
VERIFICATION:
    1. Did the 3d animation appear?
    2. Was the animation free from slowness/jerkiness? PURPOSE:
    This test verifies that multi-monitor output works on your desktop system. This is NOT the same test as the external monitor tests you would run on your laptop.  You will need two monitors to perform this test.
STEPS:
    Skip this test if your video card does not support multiple monitors.
    1. If your second monitor is not already connected, connect it now
    2. Open the "Displays" tool (open the dash and search for "Displays")
    3. Configure your output to provide one desktop across both monitors
    4. Open any application and drag it from one monitor to the next.
VERIFICATION:
    Was the stretched desktop displayed correctly across both screens? PURPOSE:
    This test will check suspend and resume
STEPS:
    1. Click "Test" and your system will suspend for about 30 - 60 seconds
    2. Observe the Power LED to see if it blinks or changes color during suspend
    3. If your system does not wake itself up after 60 seconds, please press the power button momentarily to wake the system manually
    4. If your system fails to wake at all and must be rebooted, restart System Testing after reboot and mark this test as Failed
VERIFICATION:
    Did your system suspend and resume correctly?
    (NOTE: Please only consider whether the system successfully suspended and resumed. Power/Suspend LED verification will occur after this test is completed.) PURPOSE:
    This test will check that a DSL modem can be configured and connected.
STEPS:
    1. Connect the telephone line to the computer
    2. Click on the Network icon on the top panel.
    3. Select "Edit Connections"
    4. Select the "DSL" tab
    5. Click on "Add" button
    6. Configure the connection parameters properly
    7. Click "Test" to verify that it's possible to establish an HTTP connection
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will check that a USB audio device works correctly
STEPS:
    1. Connect a USB audio device to your system
    2. Click "Test", then speak into the microphone
    3. After a few seconds, your speech will be played back to you
VERIFICATION:
    Did you hear your speech played back through the USB headphones? PURPOSE:
    This test will check that bluetooth connection works correctly
STEPS:
    1. Enable bluetooth on any mobile device (PDA, smartphone, etc.)
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. In the device write the PIN code automatically chosen by the wizard
    6. The device should pair with the computer
    7. Right-click on the bluetooth icon and select browse files
    8. Authorize the computer to browse the files in the device if needed
    9. You should be able to browse the files
VERIFICATION:
    Did all the steps work? PURPOSE:
    This test will check that headphones connector works correctly
STEPS:
    1. Connect a pair of headphones to your audio device
    2. Click the Test button to play a sound to your audio device
VERIFICATION:
    Did you hear a sound through the headphones and did the sound play without any distortion, clicks or other strange noises from your headphones? PURPOSE:
    This test will check that internal speakers work correctly
STEPS:
    1. Make sure that no external speakers or headphones are connected
       If testing a desktop, external speakers are allowed
    2. Click the Test button to play a brief tone on your audio device
VERIFICATION:
    Did you hear a tone? PURPOSE:
    This test will check that recording sound using an external microphone works correctly
STEPS:
    1. Connect a microphone to your microphone port
    2. Click "Test", then speak into the external microphone
    3. After a few seconds, your speech will be played back to you
VERIFICATION:
    Did you hear your speech played back? PURPOSE:
    This test will check that recording sound using the onboard microphone works correctly
STEPS:
    1. Disconnect any external microphones that you have plugged in
    2. Click "Test", then speak into your internal microphone
    3. After a few seconds, your speech will be played back to you.
VERIFICATION:
    Did you hear your speech played back? PURPOSE:
    This test will check that the built-in camera works
STEPS:
    1. Click on Test to display a still image from the camera for ten seconds.
VERIFICATION:
    Did you see the image? PURPOSE:
    This test will check that the built-in camera works
STEPS:
    1. Click on Test to display a video capture from the camera for ten seconds.
VERIFICATION:
    Did you see the video capture? PURPOSE:
    This test will check that the display is correct after suspend and resume
STEPS:
    1. Check that your display does not show up visual artifacts after resuming.
VERIFICATION:
    Does the display work normally after resuming from suspend? PURPOSE:
    This test will check that the system can switch to a virtual terminal and back to X
STEPS:
    1. Click "Test" to switch to another virtual terminal and then back to X
VERIFICATION:
    Did your screen change temporarily to a text console and then switch back to your current session? PURPOSE:
    This test will check that the system correctly detects
    the removal of a CF card from the systems card reader.
STEPS:
    1. Click "Test" and remove the CF card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a MS card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MS card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a MSP card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MSP card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a SDXC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SDXC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a xD card from the systems card reader.
STEPS:
    1. Click "Test" and remove the xD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of an SD card from the systems card reader
    after the system has been suspended.
STEPS:
    1. Click "Test" and remove the SD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of an SD card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of an SDHC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SDHC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of the MMC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MMC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the insertion of
    a USB 3.0 storage device
STEPS:
    1. Click "Test" and insert a USB 3.0 storage device (pen-drive/HDD) in
       a USB 3.0 port. (Note: this test will time-out after 20 seconds.)
    2. Do not unplug the device after the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the insertion of
    a USB 3.0 storage device after suspend and resume.
STEPS:
    1. Click "Test" and insert a USB 3.0 storage device (pen-drive/HDD) in
       a USB 3.0 port. (Note: this test will time-out after 20 seconds.)
    2. Do not unplug the device after the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the insertion of
    a USB storage device
STEPS:
    1. Click "Test" and insert a USB storage device (pen-drive/HDD).
       (Note: this test will time-out after 20 seconds.)
    2. Do not unplug the device after the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the insertion of
    a USB storage device after suspend and resume.
STEPS:
    1. Click "Test" and insert a USB storage device (pen-drive/HDD).
       (Note: this test will time-out after 20 seconds.)
    2. Do not unplug the device after the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a CF card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the CF card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a MS card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MS card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a MSP card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MSP card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a SDXC card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the SDXC card from the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a xD card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the xD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of an MMC card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MMC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of an SDHC card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the SDHC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal of
    a USB 3.0 storage device
STEPS:
    1. Click "Test" and remove the USB 3.0 device.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal of
    a USB 3.0 storage device after suspend
STEPS:
    1. Click "Test" and remove the USB 3.0 device.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal of
    a USB storage device
STEPS:
    1. Click "Test" and remove the USB device.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal of
    a USB storage device after suspend.
STEPS:
    1. Click "Test" and remove the USB device.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a CF card after the system has been suspended
STEPS:
    1. Click "Test" and insert a CF card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a Compact Flash (CF) media card
STEPS:
    1. Click "Test" and insert a CF card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a Extreme Digital (xD) media card
STEPS:
    1. Click "Test" and insert a xD card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a MS card after the system has been suspended
STEPS:
    1. Click "Test" and insert a MS card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a MSP card after the system has been suspended
STEPS:
    1. Click "Test" and insert a MSP card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a Memory Stick (MS) media card
STEPS:
    1. Click "Test" and insert a MS card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a Memory Stick Pro (MSP) media card
STEPS:
    1. Click "Test" and insert a MSP card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a Multimedia Card (MMC) media
STEPS:
    1. Click "Test" and insert an MMC card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a SDXC card after the system has been suspended
STEPS:
    1. Click "Test" and insert a SDXC card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a Secure Digital Extended Capacity (SDXC) media card
STEPS:
    1. Click "Test" and insert a SDXC card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a UNLOCKED Secure Digital High-Capacity
    (SDHC) media card
STEPS:
    1. Click "Test" and insert an UNLOCKED SDHC card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a xD card after the system has been suspended
STEPS:
    1. Click "Test" and insert a xD card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of an MMC card after the system has been suspended
STEPS:
    1. Click "Test" and insert an MMC card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of an UNLOCKED SD card after the system
    has been suspended
STEPS:
    1. Click "Test" and insert an UNLOCKED SD card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of an UNLOCKED SDHC media card after the
    system has been suspended
STEPS:
    1. Click "Test" and insert an UNLOCKED SDHC card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of an UNLOCKED Secure Digital (SD) media card
STEPS:
    1. Click "Test" and insert an UNLOCKED SD card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that you can record and hear audio using a bluetooth audio device
STEPS:
    1. Enable the bluetooth headset
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. In the device write the PIN code automatically chosen by the wizard
    6. The device should pair with the computer
    7. Click "Test" to record for five seconds and reproduce in the bluetooth device
VERIFICATION:
    Did you hear the sound you recorded in the bluetooth PURPOSE:
    This test will check that you can transfer information through a bluetooth connection
STEPS:
    1. Make sure that you're able to browse the files in your mobile device
    2. Copy a file from the computer to the mobile device
    3. Copy a file from the mobile device to the computer
VERIFICATION:
    Were all files copied correctly? PURPOSE:
    This test will check that you can use a BlueTooth HID device
STEPS:
    1. Enable either a BT mouse or keyboard
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. For mice, perform actions such as moving the pointer, right and left button clicks and double clicks
    6. For keyboards, click the Test button to lauch a small tool. Enter some text into the tool and close it.
VERIFICATION:
    Did the device work as expected? PURPOSE:
    This test will check that you can use a USB HID device
STEPS:
    1. Enable either a USB mouse or keyboard
    2. For mice, perform actions such as moving the pointer, right and left button clicks and double clicks
    3. For keyboards, click the Test button to lauch a small tool. Type some text and close the tool.
VERIFICATION:
    Did the device work as expected? PURPOSE:
    This test will check that your system detects USB storage devices.
STEPS:
    1. Plug in one or more USB keys or hard drives.
    2. Click on "Test".
INFO:
    $output
VERIFICATION:
    Were the drives detected? PURPOSE:
    This test will check the system can detect the insertion of a FireWire HDD
STEPS:
    1. Click 'Test' to begin the test. This test will
       timeout and fail if the insertion has not been detected within 20 seconds.
    2. Plug a FireWire HDD into an available FireWire port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the insertion of an eSATA HDD
STEPS:
    1. Click 'Test' to begin the test. This test will
       timeout and fail if the insertion has not been detected within 20 seconds.
    2. Plug an eSATA HDD into an available eSATA port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the removal of a FireWire HDD
STEPS:
    1. Click 'Test' to begin the test. This test will timeout and fail if
       the removal has not been detected within 20 seconds.
    2. Remove the previously attached FireWire HDD from the FireWire port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the removal of an eSATA HDD
STEPS:
    1. Click 'Test' to begin the test. This test will timeout and fail if
       the removal has not been detected within 20 seconds.
    2. Remove the previously attached eSATA HDD from the eSATA port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check to make sure your system can successfully hibernate (if supported)
STEPS:
    1. Click on Test
    2. The system will hibernate and should wake itself within 5 minutes
    3. If your system does not wake itself after 5 minutes, please press the power button to wake the system manually
    4. If the system fails to resume from hibernate, please restart System Testing and mark this test as Failed
VERIFICATION:
    Did the system successfully hibernate and did it work properly after waking up? PURPOSE:
    This test will check your CD audio playback capabilities
STEPS:
    1. Insert an audio CD in your optical drive
    2. When prompted, launch the Music Player
    3. Locate the CD in the display of the Music Player
    4. Select the CD in the Music Player
    5. Click the Play button to listen to the music on the CD
    6. Stop playing after some time
    7. Right click on the CD icon and select "Eject Disc"
    8. The CD should be ejected
    9. Close the Music Player
VERIFICATION:
    Did all the steps work? PURPOSE:
    This test will check your DVD  playback capabilities
STEPS:
    1. Insert a DVD that contains any movie in your optical drive
    2. Click "Test" to play the DVD in Totem
VERIFICATION:
    Did the file play? PURPOSE:
    This test will check your DVI port.
STEPS:
    Skip this test if your system does not have a DVI port.
    1. Connect a display (if not already connected) to the DVI port on your system
VERIFICATION:
    Was the desktop displayed correctly on both screens? PURPOSE:
    This test will check your DisplayPort port.
STEPS:
    Skip this test if your system does not have a DisplayPort port.
    1. Connect a display (if not already connected) to the DisplayPort port on your system
VERIFICATION:
    Was the desktop displayed correctly on both screens? PURPOSE:
    This test will check your HDMI port.
STEPS:
    Skip this test if your system does not have a HDMI port.
    1. Connect a display (if not already connected) to the HDMI port on your system
VERIFICATION:
    Was the desktop displayed correctly on both screens? PURPOSE:
    This test will check your RCA port.
STEPS:
    Skip this test if your system does not have a RCA port.
    1. Connect a display (if not already connected) to the RCA port on your system
VERIFICATION:
    Was the desktop displayed correctly on both screens? PURPOSE:
    This test will check your S-VIDEO port.
STEPS:
    Skip this test if your system does not have a S-VIDEO port.
    1. Connect a display (if not already connected) to the S-VIDEO port on your system
VERIFICATION:
    Was the desktop displayed correctly on both screens? PURPOSE:
    This test will check your USB 3.0 connection.
STEPS:
    1. Plug a USB 3.0 HDD or thumbdrive into a USB 3.0 port in the computer.
    2. An icon should appear on the Launcher.
    3. Click "Test" to begin the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check your USB connection.
STEPS:
    1. Connect a USB storage device to an external USB slot on this computer.
    2. An icon should appear on the Launcher.
    3. Confirm that the icon appears.
    4. Eject the device.
    5. Repeat with each external USB slot.
VERIFICATION:
    Do all USB slots work with the device? PURPOSE:
    This test will check your USB connection.
STEPS:
    1. Plug a USB HDD or thumbdrive into the computer.
    2. An icon should appear on the Launcher.
    3. Click "Test" to begin the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check your VGA port.
STEPS:
    Skip this test if your system does not have a VGA port.
    1. Connect a display (if not already connected) to the VGA port on your system
VERIFICATION:
    Was the desktop displayed correctly on both screens? PURPOSE:
    This test will check your lid sensors
STEPS:
    1. Click "Test".
    2. Close and open the lid.
VERIFICATION:
    Did the screen turn off while the lid was closed? PURPOSE:
    This test will check your lid sensors.
STEPS:
    1. Click "Test".
    2. Close the lid.
    3. Wait 5 seconds with the lid closed.
    4. Open the lid.
VERIFICATION:
    Did the system resume when the lid was opened? PURPOSE:
    This test will check your lid sensors.
STEPS:
    1. Close your laptop lid.
VERIFICATION:
   Does closing your laptop lid cause your system to suspend? PURPOSE:
    This test will check your monitor power saving capabilities
STEPS:
    1. Click "Test" to try the power saving capabilities of your monitor
    2. Press any key or move the mouse to recover
VERIFICATION:
    Did the monitor go blank and turn on again? PURPOSE:
    This test will check your system shutdown/booting cycle
STEPS:
    1. Select 'Test' to initiate a system shutdown.
    2. Power the system back on.
    3. From the grub menu, boot into the Xen Hypervisor.
    4. When the system has restarted, log in and restart checkbox-certification-server.
    5. Select 'Re-Run' to return to this test.
    6. Select 'Yes' to indicate the test has passed if the machine shut down
    successfully otherwise, Select 'No' to indicate there was a problem.
VERIFICATION:
    Did the system shutdown and boot correctly? PURPOSE:
    This test will check your system shutdown/booting cycle.
STEPS:
    1. Shutdown your machine.
    2. Boot your machine.
    3. Repeat steps 1 and 2 at least 5 times.
VERIFICATION:
    Did the system shutdown and rebooted correctly? PURPOSE:
    This test will check your wired connection
STEPS:
    1. Click on the Network icon in the top panel
    2. Select a network below the "Wired network" section
    3. Click "Test" to verify that it's possible to establish a HTTP connection
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will check your wireless connection.
STEPS:
    1. Click on the Network icon in the panel.
    2. Select a network below the 'Wireless networks' section.
    3. Click "Test" to verify that it's possible to establish an HTTP connection.
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will cycle through the detected display modes
STEPS:
    1. Click "Test" and the display will cycle trough the display modes
VERIFICATION:
    Did your display look fine in the detected mode? PURPOSE:
    This test will ensure that the AC is plugged back in after the battery.
    tests
STEPS:
    1. Plug laptop into AC.
VERIFICATION:
    Was the laptop plugged into AC? PURPOSE:
    This test will ensure that the AC is unplugged for the battery drain tests to run.
STEPS:
    1. Unplug laptop from AC.
VERIFICATION:
    Was the laptop unplugged from AC? PURPOSE:
    This test will send the image 'JPEG_Color_Image_Ubuntu.jpg' to a specified device
STEPS:
    1. Click "Test" and you will be prompted to enter the Bluetooth device name of a device that can accept file transfers (It may take a few moments after entering the name for the file to begin sending)
    2. Accept any prompts that appear on both devices
VERIFICATION:
    Was the data correctly transferred? PURPOSE:
    This test will send the image 'JPEG_Color_Image_Ubuntu.jpg' to a specified device
STEPS:
    1. Make sure Bluetooth is enabled by checking the Bluetooth indicator applet
    2. Click "Test" and you will be prompted to enter the Bluetooth device name of a device that can accept file transfers (It may take a few moments after entering the name for the file to begin sending)
    3. Accept any prompts that appear on both devices
VERIFICATION:
    Was the data correctly transferred? PURPOSE:
    This test will test changes to screen brightness
STEPS:
    1. Click "Test" to try to dim the screen.
    2. Check if the screen was dimmed approximately to half of the maximum brightness.
    3. The screen will go back to the original brightness in 2 seconds.
VERIFICATION:
    Was your screen dimmed approximately to half of the maximum brightness? PURPOSE:
    This test will test display rotation
STEPS:
    1. Click "Test" to test display rotation. The display will be rotated every 4 seconds.
    2. Check if all rotations (normal right inverted left) took place without permanent screen corruption
VERIFICATION:
    Did the display rotation take place without without permanent screen corruption? PURPOSE:
    This test will test the battery information key
STEPS:
    Skip this test if you do not have a Battery Button.
    1. Click Test to begin
    2. Press the Battery Info button (or combo like Fn+F3)
    3: Close the Power Statistics tool if it opens
VERIFICATION:
    Did the Battery Info key work as expected? PURPOSE:
    This test will test the battery information key after resuming from suspend
STEPS:
    Skip this test if you do not have a Battery Button.
    1. Click Test to begin
    2. Press the Battery Info button (or combo like Fn+F3)
    3: Close the Power Statistics tool if it opens
VERIFICATION:
    Did the Battery Info key work as expected after resuming from suspend? PURPOSE:
    This test will test the brightness key
STEPS:
    1. Press the brightness buttons on the keyboard
VERIFICATION:
    Did the brightness change following to your key presses? PURPOSE:
    This test will test the brightness key after resuming from suspend
STEPS:
    1. Press the brightness buttons on the keyboard
VERIFICATION:
    Did the brightness change following to your key presses after resuming from suspend? PURPOSE:
    This test will test the default display
STEPS:
    1. Click "Test" to display a video test.
VERIFICATION:
    Do you see color bars and static? PURPOSE:
    This test will test the media keys of your keyboard
STEPS:
    Skip this test if your computer has no media keys.
    1. Click test to open a window on which to test the media keys.
    2. If all the keys work, the test will be marked as passed.
VERIFICATION:
    Do the keys work as expected? PURPOSE:
    This test will test the media keys of your keyboard after resuming from suspend
STEPS:
    Skip this test if your computer has no media keys.
    1. Click test to open a window on which to test the media keys.
    2. If all the keys work, the test will be marked as passed.
VERIFICATION:
    Do the keys work as expected after resuming from suspend? PURPOSE:
    This test will test the mute key of your keyboard
STEPS:
    1. Click test to open a window on which to test the mute key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Does the mute key work as expected? PURPOSE:
    This test will test the mute key of your keyboard after resuming from suspend
STEPS:
    1. Click test to open a window on which to test the mute key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Did the volume mute following your key presses? PURPOSE:
    This test will test the sleep key
STEPS:
    1. Press the sleep key on the keyboard
    2. Wake your system up by pressing the power button
VERIFICATION:
    Did the system go to sleep after pressing the sleep key? PURPOSE:
    This test will test the sleep key after resuming from suspend
STEPS:
    1. Press the sleep key on the keyboard
    2. Wake your system up by pressing the power button
VERIFICATION:
    Did the system go to sleep after pressing the sleep key after resuming from suspend? PURPOSE:
    This test will test the super key of your keyboard
STEPS:
    1. Click test to open a window on which to test the super key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Does the super key work as expected? PURPOSE:
    This test will test the super key of your keyboard after resuming from suspend
STEPS:
    1. Click test to open a window on which to test the super key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Does the super key work as expected after resuming from suspend? PURPOSE:
    This test will test the volume keys of your keyboard
STEPS:
    Skip this test if your computer has no volume keys.
    1. Click test to open a window on which to test the volume keys.
    2. If all the keys work, the test will be marked as passed.
VERIFICATION:
    Do the keys work as expected? PURPOSE:
    This test will test the volume keys of your keyboard after resuming from suspend
STEPS:
    Skip this test if your computer has no volume keys.
    1. Click test to open a window on which to test the volume keys.
    2. If all the keys work, the test will be marked as passed.
VERIFICATION:
    Did the volume change following to your key presses? PURPOSE:
    This test will test the wireless key
STEPS:
    1. Press the wireless key on the keyboard
    2. Check that the wifi LED turns off or changes color
    3. Check that wireless is disabled
    4. Press the same key again
    5. Check that the wifi LED turns on or changes color
    6. Check that wireless is enabled
VERIFICATION:
    Did the wireless turn off on the first press and on again on the second?
    (NOTE: the LED functionality will be reviewed in a following test. Please
    only consider the functionality of the wifi itself here.) PURPOSE:
    This test will test the wireless key after resuming from suspend
STEPS:
    1. Press the wireless key on the keyboard
    2. Press the same key again
VERIFICATION:
    Did the wireless go off on the first press and on again on the second after resuming from suspend? PURPOSE:
    This test will test your accelerometer to see if it is detected
    and operational as a joystick device.
STEPS:
    1. Click on Test
    2. Tilt your hardware in the directions onscreen until the axis threshold is met.
VERIFICATION:
    Is your accelerometer properly detected? Can you use the device? PURPOSE:
    This test will test your keyboard
STEPS:
    1. Click on Test
    2. On the open text area, use your keyboard to type something
VERIFICATION:
    Is your keyboard working properly? PURPOSE:
    This test will test your pointing device
STEPS:
    1. Move the cursor using the pointing device or touch the screen.
    2. Perform some single/double/right click operations.
VERIFICATION:
    Did the pointing device work as expected? PURPOSE:
    This test will verify that the GUI is usable after manually changing resolution
STEPS:
    1. Open the Displays application
    2. Select a new resolution from the dropdown list
    3. Click on Apply
    4. Select the original resolution from the dropdown list
    5. Click on Apply
VERIFICATION:
    Did the resolution change as expected? PURPOSE:
    This test will verify that your system can successfully reboot.
STEPS:
    1. Select 'Test' to initiate a system reboot.
    2. When the grub boot menu is displayed, boot into Ubuntu (Or allow the
    system to automatically boot on its own).
    3. Once the system has restarted, log in and restart checkbox-certification-server.
    4. Select 'Re-Run' to return to this test.
    5. Select 'Yes' to indicate the test has passed if the system rebooted
    successfully, otherwise, select 'No' to indicate there was a problem.
VERIFICATION:
    Did the system reboot correctly? PURPOSE:
    This test will verify the default display resolution
STEPS:
    1. This display is using the following resolution:
INFO:
    $output
VERIFICATION:
    Is this acceptable for your display? PURPOSE:
    This will verify that an ExpressCard slot can detect inserted devices.
STEPS:
    Skip this test if you do not have an ExpressCard slot.
    1. Plug an ExpressCard device into the ExpressCard slot
VERIFICATION:
    Was the device correctly detected? PURPOSE:
    To make sure that stressing the wifi hotkey does not cause applets to disappear from the panel or the system to lock up
STEPS:
    1. Log in to desktop
    2. Press wifi hotkey at a rate of 1 press per second and slowly increase the speed of the tap, until you are tapping as fast as possible
VERIFICATION:
    Verify the system is not frozen and the wifi and bluetooth applets are still visible and functional PURPOSE:
    Touchpad LED verification
STEPS:
    1. Click on the touchpad button or press key combination to enable/disable touchpad button
    2. Slide your finger on the touchpad
VERIFICATION:
    1. Touchpad LED status should toggle everytime the button is clicked or the key combination is pressed
    2. When the LED is on, the mouse pointer should move on touchpad usage
    3. When the LED is off, the mouse pointer should not move on touchpad usage PURPOSE:
    Touchpad LED verification after resuming from suspend
STEPS:
    1. Click on the touchpad button or press key combination to enable/disable touchpad button
    2. Slide your finger on the touchpad
VERIFICATION:
    1. Touchpad LED status should toggle everytime the button is clicked or the key combination is pressed
    2. When the LED is on, the mouse pointer should move on touchpad usage
    3. When the LED is off, the mouse pointer should not move on touchpad usage PURPOSE:
    Touchpad horizontal scroll verification
STEPS:
    1. Select "Test" when ready and place your cursor within the borders of the displayed test window.
    2. Verify that you can move the horizontal slider by moving your finger right and left in the lower part of the touchpad.
VERIFICATION:
    Could you scroll right and left? PURPOSE:
    Touchpad manual detection of multitouch.
STEPS:
    1. Look at the specifications for your system.
VERIFICATION:
    Is the touchpad supposed to be multitouch? PURPOSE:
    Touchpad user-verify
STEPS:
    1. Make sure that touchpad is enabled.
    2. Move cursor using the touchpad.
VERIFICATION:
    Did the cursor move? PURPOSE:
    Touchpad vertical scroll verification
STEPS:
    1. Select "Test" when ready and place your cursor within the borders of the displayed test window.
    2. Verify that you can move the vertical slider by moving your finger up and down in the right part of the touchpad.
VERIFICATION:
    Could you scroll up and down? PURPOSE:
    Touchscreen manual detection of multitouch.
STEPS:
    1. Look at the specifications for your system.
VERIFICATION:
    Is the screen supposed to be multitouch? PURPOSE:
    Validate Wireless (WLAN + Bluetooth) LED operated the same after resuming from suspend
STEPS:
    1. Make sure WLAN connection is established and Bluetooth is enabled.
    2. WLAN/Bluetooth LED should light
    3. Switch WLAN and Bluetooth off from a hardware switch (if present)
    4. Switch them back on
    5. Switch WLAN and Bluetooth off from the panel applet
    6. Switch them back on
VERIFICATION:
    Did the WLAN/Bluetooth LED light as expected after resuming from suspend? PURPOSE:
    Validate that WLAN LED shuts off when disabled
STEPS:
    1. During the keys/wireless test you should have observed the WLAN LED
    while performing that test after turning wireless off.
    2. WLAN LED should turn off or change color when wireless is turned off
VERIFICATION:
    Did the WLAN LED turn off or change color as expected? PURPOSE:
    Validate that WLAN LED shuts off when disabled after resuming from suspend
STEPS:
    1. Connect to AP
    2. Use Physical switch to disable WLAN
    3. Re-enable
    4. Use Network-Manager to disable WLAN
VERIFICATION:
    Did the LED turn off then WLAN is disabled after resuming from suspend? PURPOSE:
    Validate that the Bluetooth LED turns on and off when BT is enabled/disabled
STEPS:
    1. Switch bluetooth off from a hardware switch (if present)
    2. Switch bluetooth back on
    3. Switch bluetooth off from the panel applet
    4. Switch bluetooth back on
VERIFICATION:
    Did the bluetooth LED turn off and on twice? PURPOSE:
    Validate that the Bluetooth LED turns on and off when BT is enabled/disabled after resuming from suspend
STEPS:
    1. Switch bluetooth off from a hardware switch (if present)
    2. Switch bluetooth back on
    3. Switch bluetooth off from the panel applet
    4. Switch bluetooth back on
VERIFICATION:
    Did the bluetooth LED turn off and on twice after resuming from suspend? PURPOSE:
    Validate that the Caps Lock key operates the same before and after resuming from suspend
STEPS:
    1. Press "Block Cap Keys" to activate/deactivate cap keys blocking
    2. Cap Keys LED should be switched on/off every time the key is pressed
VERIFICATION:
    Did the Cap Keys LED light as expected after resuming from suspend? PURPOSE:
    Validate that the External Video hot key is working as expected
STEPS:
    1. Plug in an external monitor
    2. Press the display hot key to change the monitors configuration
VERIFICATION:
    Check that the video signal can be mirrored, extended, displayed on external or onboard only. PURPOSE:
    Validate that the External Video hot key is working as expected after resuming from suspend
STEPS:
    1. Plug in an external monitor
    2. Press the display hot key to change the monitors configuration
VERIFICATION:
    Check that the video signal can be mirrored, extended, displayed on external or onboard only, after resuming from suspend. PURPOSE:
    Validate that the HDD LED still operates as expected after resuming from suspend
STEPS:
    1. Select "Test" to write and read a temporary file for a few seconds
    2. HDD LED should blink when writing to/reading from HDD
VERIFICATION:
    Did the HDD LED still blink with HDD activity after resuming from suspend? PURPOSE:
    Validate that the battery LED indicated low power
STEPS:
    1. Let system run on battery for several hours
    2. Monitor battery LED carefully
VERIFICATION:
    Does the LED light orange when battery is low? PURPOSE:
    Validate that the battery LED indicated low power after resuming from suspend
STEPS:
    1. Let system run on battery for several hours
    2. Monitor battery LED carefully
VERIFICATION:
    Does the LED light orange when battery is low after resuming from suspend? PURPOSE:
    Validate that the battery LED properly displays charged status
STEPS:
    1. Let system run on battery for a short time
    2. Plug in AC
    3. Let system run on AC
VERIFICATION:
    Does the orange battery LED shut off when system is fully charged? PURPOSE:
    Validate that the battery LED properly displays charged status after resuming from suspend
STEPS:
    1. Let system run on battery for a short time
    2. Plug in AC
    3. Let system run on AC
VERIFICATION:
    Does the orange battery LED still shut off when system is fully charged after resuming from suspend? PURPOSE:
    Validate that the battery light shows charging status
STEPS:
    1. Let system run on battery for a while
    2. Plug in AC plug
VERIFICATION:
    Did the battery indicator LED turn orange? PURPOSE:
    Validate that the battery light shows charging status after resuming from suspend
STEPS:
    1. Let system run on battery for a while
    2. Plug in AC plug
VERIFICATION:
    Did the battery indicator LED still turn orange after resuming from suspend? PURPOSE:
    Validate that the camera LED still works as expected after resuming from suspend
STEPS:
    1. Select Test to activate camera
    2. Camera LED should light for a few seconds
VERIFICATION:
    Did the camera LED still turn on and off after resuming from suspend? PURPOSE:
    Validate that the numeric keypad LED operates the same before and after resuming from suspend
STEPS:
    1. Press "Block Num" key to toggle numeric keypad LED
    2. Click on the "Test" button to open a window to verify your typing
    3. Type using the numeric keypad both when the LED is on and off
VERIFICATION:
    1. Numeric keypad LED status should toggle everytime the "Block Num" key is pressed
    2. Numbers should only be entered in the keyboard verification window when the LED is on PURPOSE:
    Validate that the power LED operated the same after resuming from suspend
STEPS:
    1. Power LED should be on while device is switched on
VERIFICATION:
    Does the power LED remain on after resuming from suspend? PURPOSE:
    Verify touchpad hotkey toggles touchpad functionality on and off
STEPS:
    1. Verify the touchpad is functional
    2. Tap the touchpad toggle hotkey
    3. Tap the touchpad toggle hotkey again
VERIFICATION:
    Verify the touchpad has been disabled and re-enabled. PURPOSE:
    Verify touchpad hotkey toggles touchpad functionality on and off after resuming from suspend
STEPS:
    1. Verify the touchpad is functional
    2. Tap the touchpad toggle hotkey
    3. Tap the touchpad toggle hotkey again
VERIFICATION:
    Verify the touchpad has been disabled and re-enabled. PURPOSE:
    WLAN LED verification
STEPS:
    1. During the keys/wireless test you should have observed the
    wireless LED while turning wireless back on.
    2. WLAN LED should light or change color when wireless is turned on
VERIFICATION:
    Did the WLAN LED turn on or change color as expected? PURPOSE:
    WLAN LED verification after resuming from suspend
STEPS:
    1. Make sure WLAN connection is established
    2. WLAN LED should light
VERIFICATION:
    Did the WLAN LED light as expected after resuming from suspend? PURPOSE:
    Wake up by USB keyboard
STEPS:
    1. Enable "Wake by USB KB/Mouse" item in BIOS
    2. Press "Test" to enter suspend (S3) mode
    3. Press any key of USB keyboard to wake system up
VERIFICATION:
    Did the system wake up from suspend mode when you pressed a keyboard key? PURPOSE:
    Wake up by USB mouse
STEPS:
    1. Enable "Wake by USB KB/Mouse" item in BIOS
    2. Press "Test" to enter suspend (S3) mode
    3. Press any button of USB mouse to wake system up
VERIFICATION:
    Did the system wake up from suspend mode when you pressed the mouse button? PURPOSE:
    Wireless (WLAN + Bluetooth) LED verification
STEPS:
    1. Make sure WLAN connection is established and Bluetooth is enabled.
    2. WLAN/Bluetooth LED should light
    3. Switch WLAN and Bluetooth off from a hardware switch (if present)
    4. Switch them back on
    5. Switch WLAN and Bluetooth off from the panel applet
    6. Switch them back on
VERIFICATION:
    Did the WLAN/Bluetooth LED light as expected? PURPOSE:
   This is an automated stress test that will force the system to hibernate/resume for 250 cycles PURPOSE:
   This is an automated stress test that will force the system to hibernate/resume for 30 cycles PURPOSE:
   This is an automated stress test that will force the system to suspend/resume for 250 cycles. PURPOSE:
   This is an automated stress test that will force the system to suspend/resume for 30 cycles. PURPOSE:
   This test will verify that a USB DSL or Mobile Broadband modem works
STEPS:
   1. Connect the USB cable to the computer
   2. Right click on the Network icon in the panel
   3. Select 'Edit Connections'
   4. Select the 'DSL' (for ADSL modem) or 'Mobile Broadband' (for 3G modem) tab
   5. Click on 'Add' button
   6. Configure the connection parameters properly
   7. Notify OSD should confirm that the connection has been established
   8. Select Test to verify that it's possible to establish an HTTP connection
VERIFICATION:
   Was the connection correctly established? PURPOSE:
   This test will verify that a fingerprint reader can be used to unlock a locked system.
STEPS:
   1. Click on the Session indicator (Cog icon on the Left side of the panel) .
   2. Select 'Lock screen'.
   3. Press any key or move the mouse.
   4. A window should appear that provides the ability to unlock either typing your password or using fingerprint authentication.
   5. Use the fingerprint reader to unlock.
   6. Your screen should be unlocked.
VERIFICATION:
   Did the authentication procedure work correctly? PURPOSE:
   This test will verify that a fingerprint reader will work properly for logging into your system. This test case assumes that there's a testing account from which test cases are run and a personal account that the tester uses to verify the fingerprint reader
STEPS:
   1. Click on the User indicator on the left side of the panel (your user name).
   2. Select "Switch User Account"
   3. On the LightDM screen select your username.
   4. Use the fingerprint reader to login.
   5. Click on the user switcher applet.
   6. Select the testing account to continue running tests.
VERIFICATION:
   Did the authentication procedure work correctly? PURPOSE:
   This test will verify that a network printer is usable
STEPS:
   1. Make sure that a printer is available in your network
   2. Click on the Gear icon in the upper right corner and then click on Printers
   3. If the printer isn't already listed, click on Add
   4. The printer should be detected and proper configuration values  should be displayed
   5. Print a test page
VERIFICATION:
   Were you able to print a test page to the network printer? PURPOSE:
   This test will verify that the desktop clock displays the correct date and time
STEPS:
   1. Check the clock in the upper right corner of your desktop.
VERIFICATION:
   Is the clock displaying the correct date and time for your timezone? PURPOSE:
   This test will verify that the desktop clock synchronizes with the system clock.
STEPS:
   1. Click the "Test" button and verify the clock moves ahead by 1 hour.
   Note: It may take a minute or so for the clock to refresh
   2. Right click on the clock, then click on "Time & Date Settings..."
   3. Ensure that your clock application is set to manual.
   4. Change the time 1 hour back
   5. Close the window and reboot
VERIFICATION:
   Is your system clock displaying the correct date and time for your timezone? PURPOSE:
   This test will verify that you can reboot your system from the desktop menu
STEPS:
   1. Click the Gear icon in the upper right corner of the desktop and click on "Shut Down"
   2. Click the "Restart" button on the left side of the Shut Down dialog
   3. After logging back in, restart System Testing and it should resume here
VERIFICATION:
   Did your system restart and bring up the GUI login cleanly? PURPOSE:
   This test will verify your system's ability to play Ogg Vorbis audio files.
STEPS:
   1. Click Test to play an Ogg Vorbis file (.ogg)
   2. Please close the player to proceed.
VERIFICATION:
   Did the sample play correctly? PURPOSE:
   This test will verify your system's ability to play Wave Audio files.
STEPS:
   1. Select Test to play a Wave Audio format file (.wav)
   2. Please close the player to proceed.
VERIFICATION:
   Did the sample play correctly? PURPOSE:
 If Recovery is successful, you will see this test on restarting checkbox, not
 sniff4.
STEPS:
 1. Click Yes
VERIFICATION:
 None Necessary, this is a bogus test PURPOSE:
 Simulates a failure by rebooting the machine
STEPS:
 1. Click test to trigger a reboot
 2. Select "Continue" once logged back in and checkbox is restarted
VERIFICATION:
 You won't see the user-verify PURPOSE:
 Some systems do not share IPMI over all NICs but instead have a dedicated management port directly connected to the BMC.  This test verifies that you have used that port for remote IPMI connections and actions.
STEPS:
 1. Prior to running the test, you should have configured and used the Dedicated Management Port to remotely power off/on this sytem.
VERIFICATION:
 Skip this test if this system ONLY uses shared management/ethernet ports OR if this system does not have a BMC (Management Console)
 1. Select Yes if you successfully used IPMI to remotely power this system off and on using the dedicated management port.
 2. Select No if you attempted to use the dedicated management port to remotely power this system off/on and it failed for some reason. PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
  1. Cut
  2. Copy
  3. Paste
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
  1. Memory set
  2. Memory reset
  3. Memory last clear
  4. Memory clear
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
 1. Simple math functions (+,-,/,*)
 2. Nested math functions ((,))
 3. Fractional math
 4. Decimal math
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator.
VERIFICATION:
 Did it launch correctly? PURPOSE:
 This test checks that gedit works.
STEPS:
 1. Click the "Test" button to open gedit, and re-open the file you created previously.
 2. Edit then save the file, then close gedit.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test checks that gedit works.
STEPS:
 1. Click the "Test" button to open gedit.
 2. Enter some text and save the file (make a note of the file name you use), then close gedit.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the AOL Instant Messaging (AIM) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Facebook Chat service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Google Talk (gtalk) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Jabber service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Microsoft Network (MSN) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a IMAP account.
VERIFICATION:
 Were you able to receive and read e-mail correctly? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a POP3 account.
VERIFICATION:
 Were you able to receive and read e-mail correctly? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a SMTP account.
VERIFICATION:
 Were you able to send e-mail without errors? PURPOSE:
 This test will check that Firefox can play a Flash video. Note: this may
 require installing additional software to successfully complete.
STEPS:
 1. Select Test to launch Firefox and view a short flash video.
VERIFICATION:
 Did the video play correctly? PURPOSE:
 This test will check that Firefox can play a Quicktime (.mov) video file.
 Note: this may require installing additional software to successfully
 complete.
STEPS:
 1. Select Test to launch Firefox with a sample video.
VERIFICATION:
 Did the video play using a plugin? PURPOSE:
 This test will check that Firefox can render a basic web page.
STEPS:
 1. Select Test to launch Firefox and view the test web page.
VERIFICATION:
 Did the Ubuntu Test page load correctly? PURPOSE:
 This test will check that Firefox can run a java applet in a web page. Note:
 this may require installing additional software to complete successfully.
STEPS:
 1. Select Test to open Firefox with the Java test page, and follow the instructions there.
VERIFICATION:
 Did the applet display? PURPOSE:
 This test will check that Firefox can run flash applications. Note: this may
 require installing additional software to successfully complete.
STEPS:
 1. Select Test to launch Firefox and view a sample Flash test.
VERIFICATION:
 Did you see the text? PURPOSE:
 This test will check that Gnome Terminal works.
STEPS:
 1. Click the "Test" button to open Terminal.
 2. Type 'ls' and press enter. You should see a list of files and folder in your home directory.
 3. Close the terminal window.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test will check that the file browser can copy a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the file called Test File 1 and click Copy.
 3. Right click in the white space and click Paste.
 4. Right click on the file called Test File 1(copy) and click Rename.
 5. Enter the name Test File 2 in the name box and hit Enter.
 6. Close the File Browser.
VERIFICATION:
 Do you now have a file called Test File 2? PURPOSE:
 This test will check that the file browser can copy a folder
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the folder called Test Folder and click on Copy.
 3. Right Click on any white area in the window and click on Paste.
 4. Right click on the folder called Test Folder(copy) and click Rename.
 5. Enter the name Test Data in the name box and hit Enter.
 6. Close the File browser.
VERIFICATION:
 Do you now have a folder called Test Data? PURPOSE:
 This test will check that the file browser can create a new file.
STEPS:
 1. Click Select Test to open the File Browser.
 2. Right click in the white space and click Create Document -> Empty Document.
 3. Enter the name Test File 1 in the name box and hit Enter.
 4. Close the File browser.
VERIFICATION:
 Do you now have a file called Test File 1? PURPOSE:
 This test will check that the file browser can create a new folder.
STEPS:
 1. Click Test to open the File Browser.
 2. On the menu bar, click File -> Create Folder.
 3. In the name box for the new folder, enter the name Test Folder and hit Enter.
 4. Close the File browser.
VERIFICATION:
 Do you now have a new folder called Test Folder? PURPOSE:
 This test will check that the file browser can delete a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the file called Test File 1 and click on Move To Trash.
 3. Verify that Test File 1 has been removed.
 4. Close the File Browser.
VERIFICATION:
  Is Test File 1 now gone? PURPOSE:
 This test will check that the file browser can delete a folder.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the folder called Test Folder and click on Move To Trash.
 3. Verify that the folder was deleted.
 4. Close the file browser.
VERIFICATION:
 Has Test Folder been successfully deleted? PURPOSE:
 This test will check that the file browser can move a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Click and drag the file called Test File 2 onto the icon for the folder called Test Data.
 3. Release the button.
 4. Double click the icon for Test Data to open that folder up.
 5. Close the File Browser.
VERIFICATION:
 Was the file Test File 2 successfully moved into the Test Data folder? PURPOSE:
 This test will check that the update manager can find updates.
STEPS:
 1. Click Test to launch update-manager.
 2. Follow the prompts and if updates are found, install them.
 3. When Update Manager has finished, please close the app by clicking the Close button in the lower right corner.
VERIFICATION:
 Did Update manager find and install updates (Pass if no updates are found,
 but Fail if updates are found but not installed) PURPOSE:
 This test will check the system's ability to power-off and boot.
STEPS:
 1. Select "Test" to begin.
 2. The machine will shut down.
 3. Power the machine back on.
 4. After rebooting, wait for the test prompts to inform you that the test is complete.
 5. Once the test has completed, restart checkbox and select 'Re-run' when prompted.
VERIFICATION:
 If the machine successfully shuts down and boots, select 'Yes', otherwise,
 select 'No'. PURPOSE:
 This test will check the system's ability to reboot cleanly.
STEPS:
 1. Select "Test" to begin.
 2. The machine will reboot.
 3. After rebooting, wait for the test prompts to inform you that the test is complete.
 4. Once the test has completed, restart checkbox and select Re-Run when prompted.
VERIFICATION:
 If the machine successfully reboots, select Yes then select Next. PURPOSE:
 This test will verify that the file browser can move a folder.
STEPS:
 1. Click Test to open the File Browser.
 2. Click and drag the folder called Test Data onto the icon called Test Folder.
 3. Release the button.
 4. Double click the folder called Test Folder to open it up.
 5. Close the File Browser.
VERIFICATION:
 Was the folder called Test Data successfully moved into the folder called Test Folder? PURPOSE:
 To sniff things out
STEPS:
 1. Click Yes
VERIFICATION:
 None Necessary, this is a bogus test Panel Clock Verification tests Panel Reboot Verification tests Parses Xorg.0.Log and discovers the running X driver and version Peripheral tests Piglit tests Ping ubuntu.com and restart network interfaces 100 times Play back a sound on the default output and listen for it on the  default input. Please choose (%s):  Please press each key on your keyboard. Please type here and press Ctrl-D when finished:
 Pointing device tests. Power Management Test Power Management tests Press any key to continue... Previous Print version information and exit. Provides information about displays attached to the system Provides information about network devices Quit from keyboard Record mixer settings before suspending. Record the current network before suspending. Record the current resolution before suspending. Rendercheck tests Rerun Restart Returns the name, driver name and driver version of any touchpad discovered on the system. Run Cachebench Read / Modify / Write benchmark Run Cachebench Read benchmark Run Cachebench Write benchmark Run Compress 7ZIP benchmark Run Compress PBZIP2 benchmark Run Encode MP3 benchmark Run Firmware Test Suite (fwts) automated tests. Run GLmark2 benchmark Run GLmark2-ES2 benchmark Run GiMark, a geometry instancing test (OpenGL 3.3) Fullscreen 1920x1080 no antialiasing Run GiMark, a geometry instancing test (OpenGL 3.3) Windowed 1024x640 no antialiasing Run GnuPG benchmark Run Himeno benchmark Run Lightsmark benchmark Run N-Queens benchmark Run Network Loopback benchmark Run Qgears2 OpenGL gearsfancy benchmark Run Qgears2 OpenGL image scaling benchmark Run Qgears2 XRender Extension gearsfancy benchmark Run Qgears2 XRender Extension image scaling benchmark Run Render-Bench XRender/Imlib2 benchmark Run Stream Add benchmark Run Stream Copy benchmark Run Stream Scale benchmark Run Stream Triad benchmark Run Unigine Heaven benchmark Run Unigine Santuary benchmark Run Unigine Tropics benchmark Run a stress test based on FurMark (OpenGL 2.1 or 3.2) Fullscreen 1920x1080 no antialiasing Run a stress test based on FurMark (OpenGL 2.1 or 3.2) Windowed 1024x640 no antialiasing Run a tessellation test based on TessMark (OpenGL 4.0) Fullscreen 1920x1080 no antialiasing Run a tessellation test based on TessMark (OpenGL 4.0) Windowed 1024x640 no antialiasing Run globs benchmark Run gtkperf to make sure that GTK based test cases work Run the graphics stress test. This test can take a few minutes. Run x264 H.264/AVC encoder benchmark Running %s... Runs a test that transfers 100 10MB files 3 times to a SDHC card. Runs a test that transfers 100 10MB files 3 times to usb. Runs all of the rendercheck test suites. This test can take a few minutes. Runs piglit tests for checking OpenGL 2.1 support Runs piglit tests for checking support for GLSL fragment shader operations Runs piglit tests for checking support for GLSL vertex shader operations Runs piglit tests for checking support for framebuffer object operations, depth buffer and stencil buffer Runs piglit tests for checking support for texture from pixmap Runs piglit tests for checking support for vertex buffer object operations Runs piglit_tests for checking support for stencil buffer operations Runs the piglit results summarizing tool SATA/IDE device information. SMART test SYSTEM TESTING: Please enter your password. Some tests require root access to run properly. Your password will never be stored and will never be submitted with test results. Select All Select all Server Services checks Shorthand for --config=.*/jobs_info/blacklist. Shorthand for --config=.*/jobs_info/blacklist_file. Shorthand for --config=.*/jobs_info/whitelist. Shorthand for --config=.*/jobs_info/whitelist_file. Skip Smoke tests Sniff Sniffers Software Installation tests Some new hard drives include a feature that parks the drive heads after a short period of inactivity. This is a power-saving feature, but it can have a bad interaction with the operating system that results in the drive constantly parked then activated. This produces excess wear on the drive, potentially leading to early failures. Space when finished Start testing Status Stop process Stop typed at tty Stress poweroff system (100 cycles) Stress reboot system (100 cycles) Stress tests Submission details Submit results Submit to HEXR Successfully finished testing! Suspend Test Suspend tests System 
Testing System Daemon tests System Testing Tab 1 Tab 2 Termination signal Test Test ACPI Wakealarm (fwts wakealarm) Test Again Test and exercise memory. Test cancelled Test for clock jitter. Test if the atd daemon is running when the package is installed. Test if the cron daemon is running when the package is installed. Test if the cupsd daemon is running when the package is installed. Test if the getty daemon is running when the package is installed. Test if the init daemon is running when the package is installed. Test if the klogd daemon is running when the package is installed. Test if the nmbd daemon is running when the package is installed. Test if the smbd daemon is running when the package is installed. Test if the syslogd daemon is running when the package is installed. Test if the udevd daemon is running when the package is installed. Test if the winbindd daemon is running when the package is installed. Test interrupted Test offlining CPUs in a multicore system. Test that the /var/crash directory doesn't contain anything. Lists the files contained within if it does, or echoes the status of the directory (doesn't exist/is empty) Test that the X is not running in failsafe mode. Test that the X process is running. Test the CPU scaling capabilities using Firmware Test Suite (fwts cpufreq). Test the network after resuming. Test to check that a Xen domU image can boot and run on Xen on Ubuntu Test to check that a cloud image boots and works properly with KVM Test to check that virtualization is supported and the test system has at least a minimal amount of RAM to function as an OpenStack Compute Node Test to detect audio devices Test to detect the available network controllers Test to detect the optical drives Test to determine if this system is capable of running hardware accelerated KVM virtual machines Test to output the Xorg version Test to see if we can sync local clock to an NTP server Test to see that we have the same resolution after resuming as before. Test to verify that the Xen Hypervisor is running. Test your system and submit results to Launchpad Tested Tests oem-config using Xpresser, and then checks that the user has been created successfully. Cleans up the newly created user after the test has passed. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11b/g protocols after the system has been suspended. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11b/g protocols. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11n protocol after the system has been suspended. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11n protocol. Tests that the systems wireless hardware can connect to a router using no security and the 802.11b/g protocols after the system has been suspended. Tests that the systems wireless hardware can connect to a router using no security and the 802.11b/g protocols. Tests that the systems wireless hardware can connect to a router using no security and the 802.11n protocol after the system has been suspended. Tests that the systems wireless hardware can connect to a router using no security and the 802.11n protocol. Tests the performance of a systems wireless connection through the iperf tool, using UDP packets. Tests the performance of a systems wireless connection through the iperf tool. Tests to see that apt can access repositories and get updates (does not install updates). This is done to confirm that you could recover from an incomplete or broken update. Tests whether the system has a working Internet connection. TextLabel The file to write the log to. The following report has been generated for submission to the Launchpad hardware database:

  [[%s|View Report]]

You can submit this information about your system by providing the email address you use to sign in to Launchpad. If you do not have a Launchpad account, please register here:

  https://launchpad.net/+login The generated report seems to have validation errors,
so it might not be processed by Launchpad. There is another checkbox running. Please close it first. This Automated test attempts to detect a camera. This attaches screenshots from the suspend/cycle_resolutions_after_suspend_auto test to the results submission. This is a fully automated version of mediacard/sd-automated and assumes that the system under test has a memory card device plugged in prior to checkbox execution. It is intended for SRU automated testing. This is an automated Bluetooth file transfer test. It sends an image to the device specified by the BTDEVADDR environment variable. This is an automated Bluetooth test. It emulates browsing on a remote device specified by the BTDEVADDR environment variable. This is an automated Bluetooth test. It receives the given file from a remote host specified by the BTDEVADDR environment variable This is an automated test to gather some info on the current state of your network devices. If no devices are found, the test will exit with an error. This is an automated test which performs read/write operations on an attached FireWire HDD This is an automated test which performs read/write operations on an attached eSATA HDD This is an automated version of usb/storage-automated and assumes that the server has usb storage devices plugged in prior to checkbox execution. It is intended for servers and SRU automated testing. This is an automated version of usb3/storage-automated and assumes that the server has usb 3.0 storage devices plugged in prior to checkbox execution. It is intended for servers and SRU automated testing. This is the automated version of suspend/suspend_advanced. This test checks cpu topology for accuracy This test checks that CPU frequency governors are obeyed when set. This test checks that the wireless interface is working after suspending the system. It disconnects all interfaces and then connects to the wireless interface and checks that the connection is working as expected. This test checks the amount of memory which is reporting in meminfo against the size of the memory modules detected by DMI. This test disconnects all connections and then connects to the wireless interface. It then checks the connection to confirm it's working as expected. This test grabs the hardware address of the bluetooth adapter after suspend and compares it to the address grabbed before suspend. This test is automated and executes after the mediacard/cf-insert test is run. It tests reading and writing to the CF card. This test is automated and executes after the mediacard/cf-insert-after-suspend test is run. It tests reading and writing to the CF card after the system has been suspended. This test is automated and executes after the mediacard/mmc-insert test is run. It tests reading and writing to the MMC card. This test is automated and executes after the mediacard/mmc-insert-after-suspend test is run. It tests reading and writing to the MMC card after the system has been suspended. This test is automated and executes after the mediacard/ms-insert test is run. It tests reading and writing to the MS card. This test is automated and executes after the mediacard/ms-insert-after-suspend test is run. It tests reading and writing to the MS card after the system has been suspended. This test is automated and executes after the mediacard/msp-insert test is run. It tests reading and writing to the MSP card. This test is automated and executes after the mediacard/msp-insert-after-suspend test is run. It tests reading and writing to the MSP card after the system has been suspended. This test is automated and executes after the mediacard/sd-insert test is run. It tests reading and writing to the SD card. This test is automated and executes after the mediacard/sd-insert-after-suspend test is run. It tests reading and writing to the SD card after the system has been suspended. This test is automated and executes after the mediacard/sdhc-insert test is run. It tests reading and writing to the SDHC card. This test is automated and executes after the mediacard/sdhc-insert-after-suspend test is run. It tests reading and writing to the SDHC card after the system has been suspended. This test is automated and executes after the mediacard/sdxc-insert test is run. It tests reading and writing to the SDXC card. This test is automated and executes after the mediacard/sdxc-insert-after-suspend test is run. It tests reading and writing to the SDXC card after the system has been suspended. This test is automated and executes after the mediacard/xd-insert test is run. It tests reading and writing to the xD card. This test is automated and executes after the mediacard/xd-insert-after-suspend test is run. It tests reading and writing to the xD card after the system has been suspended. This test is automated and executes after the suspend/usb3_insert_after_suspend test is run. This test is automated and executes after the suspend/usb_insert_after_suspend test is run. This test is automated and executes after the usb/insert test is run. This test is automated and executes after the usb3/insert test is run. This test will check to make sure supported video modes work after a suspend and resume. This is done automatically by taking screenshots and uploading them as an attachment. This test will verify that the volume levels are at an acceptable level on your local system.  The test will validate that the volume is greater than or equal to minvol and less than or equal to maxvol for all sources (inputs) and sinks (outputs) recognized by PulseAudio.  It will also validate that the active source and sink are not muted.  You should not manually adjust the  volume or mute before running this test. This will attach any logs from the power-management/poweroff test to the results. This will attach any logs from the power-management/reboot test to the results. This will check to make sure that your audio device works properly after a suspend and resume.  This may work fine with speakers and onboard microphone, however, it works best if used with a cable connecting the audio-out jack to the audio-in jack. This will run some basic connectivity tests against a BMC, verifying that IPMI works. Timer signal from alarm(2) To fix this, close checkbox and add the missing dependencies to the whitelist. Touchpad tests Touchscreen tests Try to enable a remote printer on the network and print a test page. Type Text UNKNOWN USB Test USB tests Unknown signal Untested Usage: checkbox [OPTIONS] User Applications User-defined signal 1 User-defined signal 2 Validate that the Vector Floating Point Unit is running on ARM device Verifies that DNS server is running and working. Verifies that Print/CUPs server is running. Verifies that Samba server is running. Verifies that Tomcat server is running and working. Verifies that sshd is running. Verifies that the LAMP stack is running (Apache, MySQL and PHP). Verify USB3 external storage performs at or above baseline performance Verify system storage performs at or above baseline performance Verify that all CPUs are online after resuming. Verify that all memory is available after resuming from suspend. Verify that all the CPUs are online before suspending Verify that an installation of checkbox-server on the network can be reached over SSH. Verify that mixer settings after suspend are the same as before suspend. Verify that storage devices, such as Fibre Channel and RAID can be detected and perform under stress. View results Virtualization tests Welcome to System Testing!

Checkbox provides tests to confirm that your system is working properly. Once you are finished running the tests, you can view a summary report for your system. Whitelist not topologically ordered Wireless Test Wireless networking tests Wireless scanning test. It scans and reports on discovered APs. Working You can also close me by pressing ESC or Ctrl+C. _Deselect All _Exit _Finish _No _Previous _Select All _Skip this test _Test _Test Again _Yes and capacity as well. attaches the contents of various sysctl config files. eSATA disk tests empty and capacity as well. https://help.ubuntu.com/community/Installation/SystemRequirements installs the installer bootchart tarball if it exists. no skip test test again tty input for background process tty output for background process until empty and capacity as well.  Requires MOVIE_VAR to be set. yes Project-Id-Version: checkbox
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-01-27 17:46+0000
PO-Revision-Date: 2015-09-06 13:03+0000
Last-Translator: schuko24 <gerdsaenger@t-online.de>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:34+0000
X-Generator: Launchpad (build 18115)
 

Warnung: Einige Tests können Ihr System »einfrieren« lassen oder dazu führen, dass es nicht mehr reagiert. Bitte speichern Sie all Ihre Arbeit und schließen momentan laufende Anwendungen, bevor Sie mit den Tests beginnen.    Ermitteln, ob Tests speziell für tragbare Rechner ausgeführt werden müssen, welche nicht für Arbeitsplatzrechner gedacht sind.  Ergebnisse   Aktiv   Auswahl   Es werden mehrere Bilder mit den Auflösungen aufgenommen, die von der Kamera unterstützt werden.
 Dann wird die Bildgröße überprüft und ob die Bilder in einem gültigen Format erstellt wurden.  Dieser Test überprüft, ob sich die WLAN-Hardware des Systems mit einem Router durch das 802.11a-Protokoll verbinden kann. Dafür wird ein Router benötigt, der darauf vorkonfiguriert ist, nur auf Anfragen durch das 802.11a-Protokoll zu antworten.  Dieser Test überprüft, ob sich die WLAN-Hardware des Systems mit einem Router durch das 802.11b-Protokoll verbinden kann. Dafür wird ein Router benötigt, der darauf vorkonfiguriert ist, nur auf Anfragen durch das 802.11b-Protokoll zu antworten.  Dieser Test überprüft, ob sich die WLAN-Hardware des Systems mit einem Router durch das 802.11g-Protokoll verbinden kann. Dafür wird ein Router benötigt, der darauf vorkonfiguriert ist, nur auf Anfragen durch das 802.11g-Protokoll zu antworten. %(key_name)s-Taste wurde gedrückt &Nein &Zurück Diesen Test über&springen &Testen &Ja 10 von 30 Tests abgeschlossen (30%) <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'Ubuntu'; font-size:10pt; font-weight:400; font-style:normal;">
<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=;">Bitte geben Sie die mit Ihrem Launchpad-Konto verbundene Email-Adresse ein (sofern möglich)</span></p>
<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=;">und klicken Sie auf »Ergebnisse absenden«, um diese an Launchpad zu übermitteln.</span></p></body></html> Abbruch-Signal durch abort(3) Alle erforderlichen Tasten wurden getestet! Archiviert den Ordner mit der »piglit«-Zusammenfassung in der Datei »piglit-results.tar.gz«. Sind Sie sicher? Dem Bericht das Protokoll des »fwts wakealarm«-Tests als Anlage beilegen. Das Protokoll der »rendercheck«-Tests anhängen. Dies legt dem Bericht eine Kopie der Datei /var/log/dmesg als Anlage bei. Dies legt dem Bericht ein Abbild der »udev«-Datenbank mit System-Hardware-Informationen als Anlage bei. Dies legt dem Bericht eine Liste der momentan ausgeführten Kernel-Module als Anlage bei. Dies legt dem Bericht einen Bericht mit Prozessorinformationen als Anlage bei. Dies legt dem Bericht einen Bericht über die installierten Codecs für Intel HDA als Anlage bei. Dies legt dem Bericht einen Bericht über »sysfs«-Attribute als Anlage bei. Dies legt dem Bericht ein Tar-Archiv mit »gcov«-Daten als Anlage bei, wenn vorhanden. Dies legt dem Bericht die Ausgabe von »dmidecode« als Anlage bei. Dies legt dem Bericht Informationen zu DMI als Anlage bei. Informationen über Festplattenpartitionen anhängen lshw-Ausgabe hinzufügen Den Testergebnissen wird das FWTS-Ergebnisprotokoll hinzugefügt. Dadurch wird ein Protokoll der gesammelten Audio-Hardware-Daten an die Ergebnisse angehängt. Dies legt dem Bericht das Bootchart-Protokoll für Bootchart-Testläufe als Anlage bei. Dies legt dem Bericht die Datei »bootchart.png« für Bootchart-Testläufe als Anlage bei. Dies legt dem Bericht die Inhalte von /proc/acpi/sleep als Anlage bei, falls vorhanden. Dies legt dem Bericht die Inhalte der Datei /etc/modules als Anlage bei. Dies legt dem Bericht die Inhalte der verschiedenen »modprobe«-Konfigurationsdateien als Anlage bei. Die Firmware-Version hinzufügen Die Grafik‐Stresstest‐Ergebnisse zur Einreichung anhängen. Dies legt dem Bericht das Fehlerdiagnoseprotokoll des Installationsprogramms als Anlage bei, falls vorhanden. Dies legt dem Bericht das Protokoll des 250 mal ausgeführten Ruhezustand/Aufwecken-Tests als Anlage bei, falls es existiert. Dies legt dem Bericht das Protokoll des 250 mal ausgeführten Bereitschaft/Aufwecken-Tests als Anlage bei, falls es existiert. Dies legt dem Bericht das Protokoll des 30 mal ausgeführten Ruhezustand/Aufwecken-Tests als Anlage bei, falls es existiert. Dies legt dem Bericht das Protokoll des 30 mal ausgeführten Bereitschaft/Aufwecken-Tests als Anlage bei, falls es existiert. Dies legt dem Bericht das Protokoll des einzelnen Bereitschaft/Aufwecken-Tests als Anlage bei. Dadurch wird das von »cpu/scaling_test« erzeugte Protokoll an die Ergebnisse angehängt. Die Ausgabe von »udev_resource« für Fehlerdiagnosezwecke anhängen Dies legt dem Bericht das Bildschirmfoto als Anlage bei, das im Test »graphics/screenshot« aufgenommen wurde. Dies legt dem Bericht das Bildschirmfoto als Anlage bei, das im Test »graphics/screenshot_fullscreen_video« aufgenommen wurde. Sehr ausführliche »lspci«-Ausgabe (mit zentraler Datenbankabfrage) hinzufügen. Dies legt dem Bericht eine sehr lange Ausgabe von »lspci« als Anlage bei. Audio-Test Audio-Tests Automatischer CD‐Schreibtest Automatischer DVD‐Schreibtest. Automatisierte Prüfung des Protokolls der letzten 30 Ruhezustandsdurchläufe auf Fehler durch »fwts«. Automatische Überprüfung des Ruhezustandsprotokolls auf von »fwts« entdeckte Fehler. Automatisierte Prüfung des Bereitschaftsprotokolls auf Fehler, die von »fwts» gemeldet wurden. Automatisierter Auftrag, um einen PXE-Überprüfungstest für jedes NIC zu erstellen. Automatisierter Auftrag, um einen »Remote Shared IPMI«-Überprüfungstest für jedes NIC zu erstellen. Automatischer optischer Lesetest. Automatischer Test des Herunterladens von Dateien über HTTP Automatischer Test zur Überprüfung der Verfügbarkeit einiger Systeme im Netzwerk unter Verwendung von ICMP-ECHO-Paketen. Automatisierter Test zum Speichern von Bluetooth-Geräteinformationen im Checkbox-Bericht Automatisierter Test, um mehrere Netzwerkkarten der Reihe nach zu überprüfen. Leistungstest für jede Festplatte Leistungsüberprüfungen Bluetooth-Test Bluetooth-Tests Bootchart-Informationen. Defekte Pipe: Es wurde in eine Pipe geschrieben, die nicht ausgelesen wird. Bericht wird erstellt … CD‐Schreibtest. CPU-Test Prozessortests CPU-Nutzung im Leerlauf. Kameratest Kameratests Überprüft, ob der Shell-Test das Ergebnis »Gescheitert« liefert. Überprüft, ob der Auftrag ausgeführt wird, wenn die Abhängigkeiten erfüllt sind. Überprüft, ob der Auftrag ausgeführt wird, wenn die Erfordernisse gegeben sind. Überprüft, ob das Ergebnis des Auftrags »auf diesem System nicht erforderlich« ist, wenn die Erfordernisse nicht gegeben sind. Überprüft, ob das Ergebnis des Auftrags »nicht ausgeführt« ist, wenn Abhängigkeiten nicht erfüllt sind. Protokolle bezogen auf den Stresstest zum Ausschalten (100 mal) prüfen. Protokolle bezogen auf den Stresstest zum Neustarten (100 mal) prüfen. Statistikänderungen für jede Festplatte überprüfen Überprüft, ob der Shell-Test das Ergebnis »Erfolgreich« liefert. Überprüfen, dass keine VESA-Treiber verwendet werden Überprüfen, ob Ihre Hardware Unity-3D ausführen kann Überprüfen, ob Ihre Hardware Compiz ausführen kann Die Zeit feststellen, die benötigt wird, um die Verbindung zu einem W-LAN-Zugangspunkt wiederherzustellen Überprüfen, ob »CONFIG_NO_HZ« im Kernel gesetzt ist (Dies ist nur ein einfacher Regressionstest). Checkbox-Systemüberprüfung Die Systemüberprüfung konnte nicht vollständig
abgeschlossen werden. Möchten Sie den letzten
Test wiederholen, mit dem nächsten Test
fortfahren oder vom Anfang neu beginnen? Überprüft, ob die angegebene Paketquellenlistendatei die angeforderten Paketquellen enthält. Den Akkuverbrauch im Leerlauf überprüfen. Messen der Zeit, bis der Akku leer ist. Überprüft die Entladung der Batterie während der Bereitschaft.  Zeigt die Zeit bis Den Akkuverbrauch überprüfen, während Sie einen Film anschauen. Bestimmen der Laufzeit Überprüft, wie lange es dauert, sich nach einem Bereitschaft/Aufwach-Zyklus erneut mit einem vorhandenen Funknetzwerk zu verbinden. Überprüft, wie lange es dauert, sich nach einem Bereitschaft/Aufwach-Zyklus erneut mit einem vorhandenen Kabelnetzwerk zu verbinden. Überprüft die Bereitschaftszeiten, um sicherzustellen, dass eine Rechner innerhalb festgelegter Grenzen in Bereitschaft versetzt und fortgesetzt werden kann. Kind-Prozess gestoppt oder abgebrochen Wählen Sie die Tests, die auf Ihrem System ausgeführt werden: Codec-Tests Alles zuklappen Audio-bezogene Systeminformationen sammeln. Diese Daten können zur Simulation des Rechner-Audio-Subsystems verwendet werden und für weitere detaillierte Tests in einer kontrollierten Umgebung. Informationen zur Farbtiefe und zum Pixel-Format sammeln. Sammelt Informationen über die Wiederholrate. Sammelt Informationen über den Grafikspeicher. Informationen zum Grafikmodus (Bildschirmauflösung und Bildwiederholrate) sammeln Kombinieren Sie dies mit dem Zeichen darüber, um den Knoten zu erweitern. Der Befehl wurde nicht gefunden. Der Befehl empfing Signal %(signal_name)s: %(signal_description)s Bemerkungen Test für gebräuchliche Dokument-Typen Bestandteile Konfiguration überschreibt Parameter. Verbindung hergestellt, aber {}% o der Pakete verloren Fortfahren Fortsetzen, falls gestoppt Erstellt eine mobile Breitbandverbindung für ein CDMA-basiertes Modem und überprüft diese, um sicherzustellen, dass sie funktioniert. Creates a mobile broadband connection for a GSM based modem and checks the connection to ensure it's working.  DVD‐Schreibtest. Aufgrund fehlender Abhängigkeiten können einige Aufträge nicht ausgeführt werden. Auswahl aufheben Alle abwählen Detaillierte Informationen … Erkennt und zeigt an das System angeschlossene Laufwerke an. Erkennt und zeigt mit dem System verbundene USB-Geräte an. Feststellen, ob der Bildschirm automatisch als Multitouch-Gerät erkannt wurde. Feststellen, ob der Bildschirm automatisch als nicht berührungsgesteuertes Gerät erkannt wird. Bestimmen Sie, ob das Touchpad automatisch als Gerät zur Bedienung mit mehreren Fingern erkannt wird. Bestimmen Sie, ob das Touchpad automatisch als Gerät zur Bedienung mit einem Finger erkannt wird. Festplattentest Festplattentests Festplattennutzung im Leerlauf. Möchten Sie diesen Test wirklich überspringen? Nicht mehr nachfragen Fertig Kopiert Informationen aus dem Speicher in eine Datei, um diese nach dem Bereitschaftstest als Vergleich nutzen zu können. E-Mail Die E-Mail-Adresse muss das richtige Format haben. E-Mail: Stellen Sie sicher, dass die aktuelle Auflösung mindestens 800 x 600 Pixel beträgt. Hier finden Sie weitere Informationen: Text eingeben:
 Fehler Informationen werden mit dem Server ausgetauscht … %(test_name)s wird ausgeführt Alles aufklappen ExpressCard-Test ExpressCard‐Tests FEHLER: {}% Paketverlust ist höher als die {}% .Schwelle Die Verbindung zum Server ist gescheitert.
Bitte versuchen Sie es erneut oder laden Sie diese Datei:
%s

direkt in die Systemdatenbank hoch:
https://launchpad.net/+hwdb/+submit Datei »%s« konnte nicht geöffnet werden: %s Auswertung des Formulars gescheitert: %s Das Hochladen auf den Server ist gescheitert,
bitte versuchen Sie es später noch einmal. Fingerabdruckleser-Tests Firewire-Test Firewire-Festplattentests Fließkommafehler Diskettenlaufwerks-Tests Diskettenlaufwerkstest Formular Weitere Informationen: Informationen zu Ihrem System werden gesammelt … Grafiktest Grafiktests Die Blockierung eines Ablaufs auf der steuernden Befehlseingabe oder ein der Absturz eines Kontrollprozesses wurde festgestellt. Ruhezustandstest Kurztastentests Wird automatisch beendet, sobald alle Tasten gedrückt wurden. Wenn auf Ihrer Tastatur eine Taste nicht vorhanden ist, drücken Sie den »Überspringen«-Knopf unten, um diese aus dem Test zu entfernen. Wenn auf Ihrer Tastatur eine oder mehrere Tasten fehlen, überspringen Sie den Test dieser Taste indem Sie deren Nummer drücken. Ungültige Anweisung In Bearbeitung Info Info-Test Die Informationen wurden nicht auf Launchpad eingetragen. Informatorische Tests Eingabegerätetests Eingabetest Internetverbinndung erfolgreich hergestellt Unterbrechung durch die Tastatur Ungültige Speicherreferenz Die Aufträge werden neu angeordnet, um defekte Abhängigkeiten zu reparieren. Tastentest Tastentest Kill-Signal LED-Überprüfungen USB-Geräte auflisten Die Gerätetreiber und die Version für alle Audio-Geräte auflisten. Stellen Sie sicher, dass das RTC-Gerät (Echtzeituhr) existiert. Test des maximalen Festplattenspeichers, der während einer Standardinstallation belegt wird Speicherkartentest Speicherkartentests Speichertest Speichertests Verschiedene Tests Diverse Tests Konfigurationsdatei fehlt als Argument.
 Überprüfung mobiler Breitbandverbindungen Bildschirmtest Bildschirmtests Ein 3D-Fenster über den Bildschirm bewegen &Nächster _Weiter Netzwerk-Informationen Netzwerktest Netzwerktests Weiter Keine Internetverbindung Nicht gelöst Nicht gestartet Nicht unterstützt Nicht getestet Nicht erforderlich Einen von debug, info, warning, error oder critical. Vier 3D-Fenster mehrmals öffnen und schließen Ein 3D-Fenster mehrmals öffnen und schließen Ein 3D-Fenster mehrmals öffnen, minimieren, wiederherstellen und schließen Tests optischer Laufwerke Optischer Test Test des Lesens von optischen Laufwerken. BESTANDEN: {}% Paketverlust ist unterhalb des Schwellwerts von {}% PASSWORT:  ZWECK:
     Überprüfen, dass die externen Line-Out-Anschlüsse korrekt funktionieren
DURCHFÜHRUNG:
     1. Stecken Sie das Kabel in den Line-Out-Anschluss des Lautsprechers (mit eingebautem Verstärker).
     2. Öffnen Sie die Audio-Einstellungen. Im Reiter »Audio-Ausgabe«, wählen Sie »Line-out«. Klicken Sie dann auf die Schaltfläche »Test«.
     3. Wählen Sie »Internes Audio« aus der Geräteliste der Audio-Einstellungen und klicken Sie auf »Testgeräusch«, um den linken und rechten Kanal zu überprüfen.
ÜBERPRÜFUNG:
     1. Hören Sie einen Ton aus den Lautsprechern? Die internen Lautsprecher sollten *nicht* automatisch abgeschaltet sein.
     2. Hören Sie Geräusche auf dem entsprechenden Kanal? ZWECK:
    Prüfung der Stummschaltungs-LED.
SCHRITTE:
    Überspringen Sie diesen Test, falls Ihr System über keine spezielle Stummschaltungs-LED verfügt
    1. Drücken Sie die Stummschaltungstaste zweimal und beobachten Sie die Audio-LED,
    ob Sie ausgeschaltet ist oder die Farbe wechselt.
BESTÄTIGUNG:
    Blinkte die Stummschaltungs-LED oder wechselte sie die Farbe wie erwartet? ZWECK:
    Überprüfung der LED der Feststelltaste
DURCHFÜHRUNG:
    1. Drücken Sie die Feststelltaste, um die Feststelltastenfunktion zu aktivieren/deaktivieren.
    2. Die LED der Feststelltaste sollte jedes mal an-/ausgehen, wenn die Taste gedrückt wurde.
ÜBERPRÜFUNG:
    Leuchtete die LED der Feststelltaste wie erwartet? ZWECK:
    Überprüfung der Kamera-LED
DURCHFÜHRUNG:
    1. Wählen Sie »Testen«, um die Kamera einzuschalten.
    2. Die Kamera-LED sollte ein paar Sekunden lang leuchten.
ÜBERPRÜFUNG:
    Hat die Kamera-LED geleuchtet? ZWECK:
    Überprüfung zur korrekten Balance-Regelung des externen Kopfhörers
DURCHFÜHRUNG:
    1. Überprüfen Sie, ob sich der Balance-Regler problemlos von links nach rechts bewegen lässt.
    2. Klicken Sie auf »Testen«, um 10 Sekunden lang einen Audioton abzuspielen.
    3. Bewegen Sie den Balance-Regler von links nach rechts und zurück.
    4. Achten Sie darauf, ob sich der Ton auf dem externen Kopfhörer tatsächlich Ihren Einstellungen entsprechend verhält.
ÜBERPRÜFUNG:
    Lässt sich der Regler problemlos bewegen und verhält sich der ausgegebene Ton entsprechend Ihren Einstellungen? ZWECK:
    Überprüfung zur korrekten Balance-Regelung des internen Lautsprechers
DURCHFÜHRUNG:
    1. Überprüfen Sie, ob sich der Balance-Regler problemlos von links nach rechts bewegen lässt.
    2. Klicken Sie auf »Testen«, um 10 Sekunden lang einen Audioton abzuspielen.
    3. Bewegen Sie den Balance-Regler von links nach rechts und zurück.
    4. Achten Sie darauf, ob sich der Ton auf dem internen Lautsprecher tatsächlich Ihren Einstellungen entsprechend verhält.
ÜBERPRÜFUNG:
    Lässt sich der Regler problemlos bewegen und verhält sich der ausgegebene Ton entsprechend Ihren Einstellungen? ZWECK:
    Der Test überprüft, ob die externe Line-In-Verbindung richtig funktioniert.
DURCHFÜHRUNG:
    1. Verbinden Sie den Line-In-Anschluss mit einer externen Line-Out-Quelle.
    2. Öffnen Sie die Audio-Einstellungen, wechseln Sie zum Reiter »Eingang« und wählen Sie »Line-In« aus der Liste. Klicken Sie dann auf den Knopf »Testen«.
    3. Nach ein paar Sekunden wird Ihre Aufnahme wiedergegeben.
ÜBERPRÜFUNG:
    Konnten Sie Ihre Aufnahme hören? ZWECK:
    Dieser Test überprüft, ob die verschiedenen Audio-Kanäle korrekt funktionieren.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«.
ÜBERPRÜFUNG:
    Sie sollten deutlich eine Stimme über die verschiedenen Audio-Kanäle hören. ZWECK:
    Ziehen & Ablegen bei berührungsempfindlichen Bildschirmen überprüfen
DURCHFÜHRUNG:
    1. Tippen Sie zweimal, halten und ziehen Sie ein Objekt über die Arbeitsfläche.
    2. Lassen Sie das Objekt an einem anderen Ort los.
ÜBERPRÜFUNG:
    Konnten Sie das Objekt auswählen und ziehen & ablegen? ZWECK:
    Überprüfen der Touchscreen-Quetsch-Geste zum Verkleinern/Vergrößern
DURCHFÜHRUNG:
    1. Platzieren Sie zwei Finger auf dem Bildschirm und bewegen Sie diese aufeinander zu.
    2. Platzieren Sie zwei Finger auf dem Bildschirm und bewegen Sie diese voneinander weg.
ÜBERPRÜFUNG:
    Wird der Bildschirmausschnitt verkleinert/vergrößert? ZWECK:
    Überprüfen der Touchscreen-Tipp-Erkennung
DURCHFÜHRUNG:
    1. Tippen Sie mit dem Finger auf ein Objekt auf dem Bildschirm. Der Zeiger sollte zum angetippten Ort springen und das Objekt hervorheben.
ÜBERPRÜFUNG:
    Hat die Tipp-Erkennung funktioniert? ZWECK:
    Aufträge erstellen, die den Prozessor über 2 Stunden lang so stark wie möglich auslasten. Der Test wird als bestanden angesehen, wenn das System nicht einfriert. ZWECK:
    Überprüfung der DisplayPort-Audio-Schnittstelle
DURCHFÜHRUNG:
    1. Schließen Sie ein externes DisplayPort-Gerät mit Ton an (Benutzen Sie nur einen HDMI/DisplayPort-Anschluss zur gleichen Zeit)
    2. Klicken Sie auf den Knopf »Testen«
ÜBERPRÜFUNG:
    Konnten Sie den Ton über das DisplayPort-Gerät hören? ZWECK:
    Einige rechenaufwendige Vorgänge ausführen und dann prüfen, ob die GPU gesperrt wurde.
DURCHFÜHRUNG:
    1. Öffnen Sie 2 »glxgears«-Fenster und bewegen Sie diese schnell über den Bildschirm.
    2. Wechseln Sie die Arbeitsfläche mit »wmctrl«.
    3. Starten Sie die Wiedergabe eines HTML5-Videos in Firefox.
ÜBERPRÜFUNG:
    Nach einer Arbeitsbelastung über 60 Sekunden prüfen Sie »kern.log« auf GPU-Fehler. ZWECK:
    Überprüfung der Festplatten-LED
DURCHFÜHRUNG:
    1. Wählen Sie »Testen«, damit eine temporäre Datei für einige Sekunden gelesen und beschrieben wird.
    2. Die Festplatten-LED sollte leuchten, wenn von der Festplatte gelesen bzw. darauf geschrieben wird.
ÜBERPRÜFUNG:
    Leuchtete die Festplatten-LED? ZWECK:
    Überprüfung der HDMI-Audio-Schnittstelle
DURCHFÜHRUNG:
    1. Schließen Sie ein externes HDMI-Gerät mit Ton an (Benutzen Sie nur einen HDMI/DisplayPort-Anschluss zur gleichen Zeit)
    2. Klicken Sie auf den Knopf »Testen«
ÜBERPRÜFUNG:
    Konnten Sie den Ton über das HDMI-Gerät hören? ZWECK:
    Informationen, die sich auf die testende Person beziehen, in den Bericht aufnehmen.
DURCHFÜHRUNG:
    1. Informationen zur testenden Person.
    2. Bitte geben Sie folgende Informationen in das Bemerkungsfeld ein:
       a. Ihren Namen.
       b. Ihre E-Mail-Adresse.
       c. Den Grund, warum Sie diesen Test ausführen.
ÜBERPRÜFUNG:
    In diesem Test muss nichts überprüft werden. ZWECK:
    Manuelle Erkennung des Schwingungssensor.
DURCHFÜHRUNG:
    1. Sehen Sie sich die Beschreibung Ihres Systems an.
ÜBERPRÜFUNG:
    Verfügt Ihr System über einen Schwingungsensor? ZWECK:
    Überprüfung der Ziffernblock-LED
DURCHFÜHRUNG:
    1. Drücken Sie die »Block Num«-Taste, um die Ziffernblock-LED ein-/auszuschalten.
    2. Klicken Sie auf den »Testen«-Knopf, um ein Fenster zu öffnen, welches Ihre Eingabe überprüft.
    3. Tippen Sie mithilfe des Ziffernblocks, wenn die LED leuchtet bzw. nicht leuchtet.
ÜBERPRÜFUNG:
    1. Der Status der Ziffernblock-LED sollte sich ändern, sobald die »Block Num«-Taste gedrückt wurde.
    2. Die Eingabe des Ziffernblocks wird nur dann an das Überprüfungsfenster weitergeleitet, wenn die LED leuchtet. ZWECK:
    Überprüfung der Energie-LED
DURCHFÜHRUNG:
    1. Während das Gerät eingeschaltet ist sollte die Energie-LED leuchten.
ÜBERPRÜFUNG:
    Leuchtet die Energie-LED wie erwartet? ZWECK:
    Prüfung der Strom-LED
DURCHFÜHRUNG:
    1. Die Strom-LED sollte blinken oder die Farbe wechseln, während sich das System in Bereitschaft befindet.
ÜBERPRÜFUNG:
    Hat die Strom-LED geblinkt oder die Farbe gewechselt, während sich das System, für den vorherigen Test, in Bereitschaft befand? ZWECK:
    Prüfung der Ruhezustands-LED.
SCHRITTE:
    Überspringen Sie diesen Test, falls Ihr System über keine spezielle Ruhezustands-LED verfügt
    1. Die Ruhezustands-LED sollte blinken oder die Farbe wechseln,
    wenn sich das System im Ruhezustand befindet.
BESTÄTIGUNG:
    Blinkte die Ruhezustands-LED oder wechselte sie im Ruhezustand die Farbe? ZWECK:
    Ein Bildschirmfoto des aktuellen Bildschirms (bei Anmeldung unter Unity) aufnehmen.
DURCHFÜHRUNG:
    1. Nehmen Sie ein Foto mit einer USB-Webcam auf.
ÜBERPRÜFUNG:
    Sehen Sie sich den Anhang später manuell an. ZWECK:
    Ein Bildschirmfoto des aktuellen Bildschirms aufnehmen, nachdem das System in Bereitschaft gewesen ist (in der Unity-Arbeitsumgebung).
DURCHFÜHRUNG:
    1. Nehmen Sie ein Foto mit Ihrer USB-Webcam auf.
ÜBERPRÜFUNG:
    Überprüfen Sie die Anlage später manuell. ZWECK:
    Ein Bildschirmfoto des aktuellen Bildschirms während der Vollbild-Video-Wiedergabe aufnehmen.
DURCHFÜHRUNG:
    1. Starten Sie die Vollbildwiedergabe eines Videos.
    2. Nehmen Sie nach einigen Sekunden ein Foto mit einer USB-Webcam auf.
ÜBERPRÜFUNG:
    Sehen Sie sich den Anhang später manuell an. ZWECK:
    Testet, ob die Hardware des Systems sich mit einem Router verbinden kann, der das 802.11b/g-Protokoll ohne Verschlüsselung verwendet.
DURCHFÜHRUNG:
    1. Öffnen Sie das Konfigurationsmenü Ihres Routers.
    2. Ändern Sie die Einstellungen, sodass nur Verbindungen auf den kabellosen Bändern B und G akzeptiert werden.
    3. Stellen Sie sicher, dass die SSID »ROUTER_SSID« lautet.
    4. Ändern Sie die Sicherheitseinstellungen, sodass keine Verschlüsselung verwendet wird.
    5. Klicken Sie auf den »Testen«-Knopf, um eine Verbindung zu dem Router herzustellen und die Verbindung zu testen.
ÜBERPRÜFUNG:
    Die Überprüfung erfolgt automatisiert; ändern Sie die ausgewählten Ergebnisse nicht. ZWECK:
    Testet, ob die Hardware des Systems sich mit einem Router verbinden kann, der das 802.11b/g-Protokoll ohne Verschlüsselung verwendet.
DURCHFÜHRUNG:
    1. Öffnen Sie das Konfigurationsmenü Ihres Routers.
    2. Ändern Sie die Einstellungen, sodass nur Verbindungen auf dem kabellosen Band N akzeptiert werden.
    3. Stellen Sie sicher, dass die SSID »ROUTER_SSID« lautet.
    4. Ändern Sie die Sicherheitseinstellungen, sodass keine Verschlüsselung verwendet wird.
    5. Klicken Sie auf den »Testen«-Knopf, um eine Verbindung zu dem Router herzustellen und die Verbindung zu testen.
ÜBERPRÜFUNG:
    Die Überprüfung erfolgt automatisiert; ändern Sie die ausgewählten Ergebnisse nicht. ZWECK:
    Testet, ob die Hardware des Systems sich mithilfe einer WPA-Verschlüsselung und dem 802.11b/g-Protokoll mit einem Router verbinden kann.
DURCHFÜHRUNG:
    1. Öffnen Sie das Konfigurationsmenü Ihres Routers.
    2. Ändern Sie die Einstellungen, sodass nur Verbindungen auf den kabellosen Bändern B und G akzeptiert werden.
    3. Stellen Sie sicher, dass die SSID »ROUTER_SSID« lautet.
    4. Ändern Sie die Sicherheitseinstellungen, sodass WPA2 verwendet wird und der PSK-Schlüssel mit dem in »ROUTER_PSK« übereinstimmt.
    5. Klicken Sie auf den »Testen«-Knopf, um eine Verbindung zu dem Router herzustellen und die Verbindung zu testen.
ÜBERPRÜFUNG:
    Die Überprüfung erfolgt automatisiert; ändern Sie die ausgewählten Ergebnisse nicht. ZWECK:
    Testet, ob die Hardware des Systems sich mithilfe einer WPA-Verschlüsselung und dem 802.11n-Protokoll mit einem Router verbinden kann.
DURCHFÜHRUNG:
    1. Öffnen Sie das Konfigurationsmenü Ihres Routers.
    2. Ändern Sie die Einstellungen, sodass nur Verbindungen auf dem kabellosen Band N akzeptiert werden.
    3. Stellen Sie sicher, dass die SSID »ROUTER_SSID« lautet.
    4. Ändern Sie die Sicherheitseinstellungen, sodass WPA2 verwendet wird und der PSK-Schlüssel mit dem in »ROUTER_PSK« übereinstimmt.
    5. Klicken Sie auf den »Testen«-Knopf, um eine Verbindung zu dem Router herzustellen und die Verbindung zu testen.
ÜBERPRÜFUNG:
    Die Überprüfung erfolgt automatisiert; ändern Sie die ausgewählten Ergebnisse nicht. ZWECK:
    Dieser Test überprüft, ob der Erweiterung »Manuell« einwandfrei funktioniert.
DURCHFÜHRUNG:
    1. Fügen Sie eine Bemerkung hinzu.
    2. Markieren Sie das Ergebnis als bestanden.
ÜBERPRÜFUNG:
    Überprüfen Sie im Bericht, ob das Ergebnis des Tests »Bestanden« ist und die eingegebene Bemerkung angezeigt wird. ZWECK:
    Dieser Test schaltet durch die verschiedenen Video-Modi.
DURCHFÜHRUNG:
     1. Klicken Sie auf »Testen«, um mit dem Durchschalten der Video-Modi zu beginnen.
ÜBERPRÜFUNG:
    Schien der Bildschirm in jedem Modus zu funktionieren? ZWECK:
    Dieser Test überprüft die grundlegenden 3D-Fähigkeiten Ihrer Grafikkarte.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um eine OpenGL-Demonstration auszuführen.
    2. Überprüfen Sie, ob die Animation nicht ruckelt oder verzögert wird.
ÜBERPRÜFUNG:
    1. Wurde die 3D-Animation angezeigt?
    2. War die Animation frei von Verzögerungen/Ruckeln? ZWECK:
    Prüfung, ob der Mehr-Monitor-Betrieb auf Ihrem Desktoprechner funktioniert. Dies ist NICHT der gleiche Test wie für einen  externen Monitor für ein Notebook.  Sie benötigen zwei Monitore für diesen Test.
SCHRITTE:
    Überspringen Sie diesen Test, falls Ihr System keinen Mehr-Monitorbetrieb unterstützt.
    1. Wenn der zweite Monitor noch nicht angeschlossen ist, erledigen Sie das jetzt.
    2. Öffnen Sie »Anzeigegeräte« Menü (Öffnen Sie den Dash and suchen Sie nach »Anzeigegeräte«)
    3. Stellen Sie die Ausgabe ein auf »Eine Arbeitsfläche über beide Monitore«
    4. Öffnen Sie eine Anwendung und ziehen Sie sie von einem Monitor auf den anderen.
BESTÄTIGUNG:
    Wurde die erweiterte Arbeitsfläche richtig auf beiden Monitoren angezeigt? ZWECK:
    Dieser Test versetzt den Rechner in Bereitschaft und weckt ihn wieder auf.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um den Rechner für etwa 30-60 Sekunden in Bereitschaft zu versetzen.
    2. Beobachten Sie die Energie-LED, ob diese während der Bereitschaft blinkt oder die Farbe ändert.
    3. Falls ihr System nach 60 Sekunden nicht startet, müssen Sie den Ein-/Ausschalter drücken.
    4. Falls das System nicht aus der Bereitschaft aufwacht und neu gestartet werden muss, starten Sie bitte die Systemüberprüfung erneut und markieren diesen Test als gescheitert.
ÜBERPRÜFUNG:
    Wurde das System erfolgreich in Bereitschaft versetzt und wieder aufgeweckt?
    (Bitte berücksichtigen Sie nur, ob das System erfolgreich in Bereitschaft versetzt und wieder aufgeweckt wurde. Die Überprüfung der Energie-LED erfolgt nachdem dieser Test abgeschlossen ist.) ZWECK:
    Dieser Test überprüft, ob ein DSL-Modem konfiguriert und verbunden werden kann.
DURCHFÜHRUNG:
    1. Verbinden Sie die Telefonleitung mit dem Rechner.
    2. Klicken Sie auf das Netzwerksymbol in der oberen Menüleiste.
    3. Wählen Sie »Verbindungen bearbeiten«.
    4. Öffnen Sie den Reiter »DSL«.
    5. Klicken Sie auf die Schaltfläche »Hinzufügen«.
    6. Konfigurieren Sie die Verbindungsparameter.
    7. Klicken Sie auf »Testen«, um zu prüfen, ob eine HTTP-Verbindung hergestellt werden kann.
ÜBERPRÜFUNG:
    Wurde eine Benachrichtigung angezeigt und die Verbindung korrekt hergestellt? ZWECK:
    Dieser Test stellt sicher, dass ein USB-Audio-Gerät fehlerfrei funktioniert.
DURCHFÜHRUNG:
    1. Verbinden Sie das USB-Audio-Gerät mit Ihrem System.
    2. Klicken Sie auf »Testen« und sprechen Sie in das Mikrofon.
    3. Nach einigen Sekunden sollte Ihre Aufnahme wiedergegeben werden.
ÜBERPRÜFUNG:
    Haben Sie Ihre Aufnahme über die USB-Kopfhörer gehört? ZWECK:
    Dieser Test überprüft, ob die Bluetooth-Verbindung richtig funktioniert.
DURCHFÜHRUNG:
    1. Aktivieren Sie Bluetooth auf jedem mobilen Gerät (PDA, Smartphone, usw.).
    2. Klicken Sie auf das Bluetooth-Symbol in der Menüleiste.
    3. Wählen Sie »Neues Gerät einrichten«.
    4. Suchen Sie in der Liste nach dem Gerät und wählen Sie es aus.
    5. Geben Sie im Gerät den PIN-Code ein, der automatisch vom Assistenten vergeben wurde.
    6. Das Gerät sollte sich mit dem Rechner verbinden (Pairing).
    7. Klicken Sie mit der rechten Maustaste auf das Bluetooth-Symbol und wählen Sie »Dateien durchsuchen«.
    8. Wenn nötig, lassen Sie zu, dass der Rechner die Dateien auf dem Gerät durchsucht.
    9. Es sollte Ihnen nun möglich sein, die Dateien zu durchsuchen.
ÜBERPRÜFUNG:
    Konnten alle Schritte fehlerfrei ausgeführt werden? ZWECK:
    Dieser Test überprüft, ob Ihr Kopfhöreranschluss funktioniert.
DURCHFÜHRUNG:
    1. Verbinden Sie Ihre Kopfhörer mit Ihrem Audio-Gerät.
    2. Klicken Sie auf »Testen«, um einen Ton über Ihr Audio-Gerät wiederzugeben.
ÜBERPRÜFUNG:
    Haben Sie den Ton ohne Verzerrungen, Klicken oder andere seltsame Geräuschen über die Kopfhörer gehört? ZWECK:
    Dieser Test überprüft, ob die eingebauten Lautsprecher korrekt funktionieren.
DURCHFÜHRUNG:
    1. Stellen Sie sicher, dass keine externen Lautsprecher oder Kopfhörer angeschlossen sind.
       Falls es sich um einen fest installierten Rechner handelt, können externe Lautsprecher angeschlossen bleiben.
    2. Klicken Sie auf »Testen«, um einen kurzen Ton wiederzugeben.
ÜBERPRÜFUNG:
    Haben Sie einen Ton gehört? ZWECK:
    Dieser Test überprüft, ob die Tonaufnahme über ein externes Mikrofon funktioniert.
DURCHFÜHRUNG:
    1. Verbinden Sie ein Mikrofon mit Ihrem Mikrofonanschluss.
    2. Klicken Sie auf »Testen« und sprechen Sie in das externe Mikrofon.
    3. Nach einigen Sekunden sollte Ihre Aufnahme wiedergegeben werden.
ÜBERPRÜFUNG:
    Haben Sie Ihre Aufnahme gehört? ZWECK:
    Dieser Test überprüft, ob die Tonaufnahme über das eingebaute Mikrofon funktioniert.
DURCHFÜHRUNG:
    1. Entfernen Sie jegliche externen Mikrofone, die angeschlossen sind.
    2. Klicken Sie auf »Testen« und sprechen Sie in Ihr eingebautes Mikrofon.
    3. Nach einigen Sekunden sollte Ihre Aufnahme wiedergegeben werden.
ÜBERPRÜFUNG:
    Haben Sie Ihre Aufnahme gehört? ZWECK:
    Dieser Test überprüft, ob die eingebaute Kamera funktioniert.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um für 10 Sekunden ein Standbild der Kamera anzuzeigen.
ÜBERPRÜFUNG:
    Haben Sie das Bild gesehen? ZWECK:
    Dieser Test überprüft, ob die eingebaute Kamera fehlerfrei funktioniert.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um 10 Sekunden lang das Video der Kamera anzuzeigen.
ÜBERPRÜFUNG:
    Konnten Sie das Video sehen? ZWECK:
    Dieser Test überprüft, ob die Anzeige korrekt funktioniert, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Stellen Sie sicher, dass Ihre Anzeige keine Bildstörungen oder visuelle Artefakte aufweist, nachdem der Rechner aufgeweckt wurde.
ÜBERPRÜFUNG:
    Funktioniert Ihre Anzeige normal, nachdem der Rechner aus der Bereitschaft wieder aufgeweckt wurde? ZWECK:
    Dieser Test überprüft, ob das System zu einem virtuellen Terminal und zurück zur grafischen Oberfläche wechseln kann.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um zu einem anderen virtuellen Terminal und dann zurück zur grafischen Oberfläche zu wechseln.
ÜBERPRÜFUNG:
    Hat Ihr Bildschirm vorübergehend zur Textkonsole und wieder zurück zur derzeitigen Sitzung geschaltet? ZWECK:
    Dieser Test überprüft, ob das Entfernen einer CF-Karte aus dem Speicherkartenleser des Systems korrekt erkannt wird.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die CF-Karte aus dem Speicherkartenleser.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Speicherkartenleser des Systems das Entfernen einer MS-Speicherkarte korrekt erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die MS-Speicherkarte aus dem Kartenleser.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Speicherkartenleser des Systems das Entfernen einer MSP-Speicherkarte korrekt erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die MSP-Speicherkarte aus dem Kartenleser.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Speicherkartenleser des Systems das Entfernen einer SDXC-Speicherkarte korrekt erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die SDXC-Speicherkarte aus dem Kartenleser.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Speicherkartenleser des Systems das Entfernen einer xD-Speicherkarte korrekt erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die xD-Speicherkarte aus dem Kartenleser.
       (Anmerkung: Dieser Test wird nach 20 Sekunden abgebrochen.)
ÜBERPRÜFUNG:
    Dieser Test wird automatisch durchgeführt. Bitte ändern Sie das automatisch ausgewählte Ergebnis nicht. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Entfernen einer SD-Karte erkennt, nachdem sich das
    System in Bereitschaft befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die SD-Karte aus dem Lesegerät.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob das Entfernen einer SD-Karte aus dem Speicherkartenleser des Systems korrekt erkannt wird.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die SD-Karte aus dem Speicherkartenleser.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob das Entfernen einer SDHC-Karte aus dem Speicherkartenleser des Systems korrekt erkannt wird.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die SDHC-Karte aus dem Speicherkartenleser.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob das Entfernen eines »Multimedia Card«-Mediums (MMC) aus dem Speicherkartenleser des Systems korrekt erkannt wird.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die MMC-Karte aus dem Speicherkartenleser.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Test erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob Ihr System den Anschluss eines USB-3.0-Speichergeräts korrekt erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und schließen Sie ein USB-3.0-Gerät (USB-Stick/-Festplatte) am USB-3.0-Anschluss an. (Hinweis: Dieser Test wird nach 20 Sekunden automatisch abgebrochen.)
    2. Entfernen Sie das Gerät nach dem Test nicht.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests ist automatisiert. Bitte ändern Sie das automatisch ausgewählte Ergebnis nicht. ZWECK:
    Dieser Test dient zur Überprüfung, ob das System das Einbinden eines USB-3.0-Speichermediums nach der Rückkehr aus dem Bereitschaftszustand korrekt erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Test« und schließen Sie ein USB-3.0-Speichermedium (USB-Stick/Festplatte) an einen USB-3.0-Anschluss an.
       (Beachten Sie: Dieser Test bricht nach 20 Sekunden ab.)
    2. Entfernen Sie das Gerät nicht nach dem Test.
ÜBERPRÜFUNG:
    Das Ergebnis dieses Tests wird automatisch ermittelt. Verändern Sie nicht die automatische Auswahl. ZWECK:
    Dieser Test überprüft, ob das System den Anschluss eines USB-Speichermediums erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und schließen Sie ein USB-Speichermedium (USB-Stick oder USB-Festplatte) an. (Hinweis: Der Test führt zu einer Zeitüberschreitung, wenn kein Anschluss innerhalb von 20 Sekunden festgestellt wird.)
    2. Entfernen Sie nicht das USB-Speichermedium nach dem Test.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test dient zur Überprüfung, ob das System das Einbinden eines USB-Speichermediums nach der Rückkehr aus dem Bereitschaftszustand korrekt erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Test« und schließen Sie ein USB-Speichermedium (USB-Stick/Festplatte) an.
       (Beachten Sie: Dieser Test bricht nach 20 Sekunden ab.)
    2. Entfernen Sie das Gerät nicht nach dem Test.
ÜBERPRÜFUNG:
    Das Ergebnis dieses Tests wird automatisch ermittelt. Verändern Sie nicht die automatische Auswahl. ZWECK:
    Dieser Test überprüft, ob das Entfernen einer CF-Karte aus dem Speicherkartenleser des Systems korrekt erkannt wird, nachdem sich das System im Bereitschaftsmodus befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die CF-Karte aus dem Speicherkartenleser.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Speicherkartenleser des Systems das Entfernen einer MS-Speicherkarte erkennen kann, nachdem das System in Bereitschaft gegangen ist.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die MS-Speicherkarte aus dem Kartenleser.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Speicherkartenleser des Systems das Entfernen einer MSP-Speicherkarte erkennen kann, nachdem das System in Bereitschaft gegangen ist.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die MSP-Speicherkarte aus dem Kartenleser.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Entfernen einer SDXC-Karte erkennt, nachdem sich das System in Bereitschaft befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die SDXC-Karte aus dem Lesegerät.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Speicherkartenleser des Systems das Entfernen einer xD-Speicherkarte erkennen kann, nachdem das System in Bereitschaft gegangen ist.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die xD-Speicherkarte aus dem Kartenleser.
       (Anmerkung: Dieser Test wird nach 20 Sekunden abgebrochen.)
ÜBERPRÜFUNG:
    Dieser Test wird automatisch durchgeführt. Bitte ändern Sie das automatisch ausgewählte Ergebnis nicht. ZWECK:
    Dieser Test überprüft, ob das Entfernen eines »Multimedia Card«-Mediums (MMC) aus dem Speicherkartenleser des Systems korrekt erkannt wird, nachdem sich das System im Bereitschaftsmodus befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die MMC-Karte aus dem Speicherkartenleser.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob das Entfernen einer SDHC-Karte aus dem Speicherkartenleser des Systems korrekt erkannt wird, nachdem sich das System im Bereitschaftsmodus befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie die SDHC-Karte aus dem Speicherkartenleser.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob Ihr System das Entfernen eines USB-3.0-Speichergeräts korrekt erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie das USB-3.0-Gerät.
       (Hinweis: Dieser Test wird nach 20 Sekunden automatisch abgebrochen.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests ist automatisiert. Bitte ändern Sie das automatisch ausgewählte Ergebnis nicht. ZWECK:
    Dieser Test dient zur Überprüfung, ob das System das Entfernen eines USB-3.0-Speichermediums nach Aktivierung des Bereitschaftszustands erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Test« und entfernen Sie das USB-3.0-Speichermedium.
       (Beachten Sie: Dieser Test bricht nach 20 Sekunden ab.)
ÜBERPRÜFUNG:
    Das Ergebnis dieses Tests wird automatisch ermittelt. Verändern Sie nicht die automatische Auswahl. ZWECK:
    Dieser Test überprüft, ob das Entfernen eines USB-Speichermediums vom System korrekt erkannt wird.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und entfernen Sie das USB-Speichermedium.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test dient zur Überprüfung, ob das System das Entfernen eines USB-Speichermediums nach Aktivierung des Bereitschaftszustands erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Test« und entfernen Sie das USB-Speichermedium.
       (Beachten Sie: Dieser Test bricht nach 20 Sekunden ab.)
ÜBERPRÜFUNG:
    Das Ergebnis dieses Tests wird automatisch ermittelt. Verändern Sie nicht die automatische Auswahl. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Einstecken einer CF-Karte erkennt, nachdem sich das System in Bereitschaft befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und stecken Sie eine CF-Karte in das Lesegerät. Falls sich die Dateiverwaltung öffnet, können Sie diese gefahrlos schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie die Speicherkarte nach dem Test nicht.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Einstecken einer »Compact Flash (CF)«-Karte erkennt, nachdem sich das System in Bereitschaft befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und stecken Sie eine CF-Karte in das Lesegerät. Falls sich die Dateiverwaltung öffnet, können Sie diese gefahrlos schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie die Speicherkarte nach dem Test nicht.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Einstecken einer »Extreme Digital (xD)«-Karte erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und stecken Sie eine xD-Karte in das Lesegerät. Falls sich die Dateiverwaltung öffnet, können Sie diese gefahrlos schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie die Speicherkarte nach dem Test nicht.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Einstecken einer MS-Karte erkennt, nachdem sich das System in Bereitschaft befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und stecken Sie eine MS-Karte in das Lesegerät. Falls sich die Dateiverwaltung öffnet, können Sie diese gefahrlos schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie die Speicherkarte nach dem Test nicht.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Einstecken einer MSP-Karte erkennt, nachdem sich das System in Bereitschaft befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und stecken Sie eine MSP-Karte in das Lesegerät. Falls sich die Dateiverwaltung öffnet, können Sie diese gefahrlos schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie die Speicherkarte nach dem Test nicht.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Einstecken einer »Memory Stick (MS)«-Karte erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und stecken Sie eine MS-Karte in das Lesegerät. Falls sich die Dateiverwaltung öffnet, können Sie diese gefahrlos schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie die Speicherkarte nach dem Test nicht.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Einstecken einer »Memory Stick Pro (MSP)«-Karte erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und stecken Sie eine MSP-Karte in das Lesegerät. Falls sich die Dateiverwaltung öffnet, können Sie diese gefahrlos schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie die Speicherkarte nach dem Test nicht.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Speicherkartenleser des Systems das Einlegen eines »Multimedia Card«-Mediums (MMC) erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und legen Sie eine MMC-Karte in den Speicherkartenleser ein. Wenn die Dateiverwaltung gestartet wird, können Sie diese ruhig schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie nicht die Speicherkarte nach dem Test.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Test erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Einstecken einer SDXC-Karte erkennt, nachdem sich das System in Bereitschaft befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und stecken Sie eine SDXC-Karte in das Lesegerät. Falls sich die Dateiverwaltung öffnet, können Sie diese gefahrlos schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie die Speicherkarte nach dem Test nicht.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Einstecken einer »Secure Digital Extended Capacity (SDXC)«-Karte erkennt, nachdem sich das System in Bereitschaft befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und stecken Sie eine SDXC-Karte in das Lesegerät. Falls sich die Dateiverwaltung öffnet, können Sie diese gefahrlos schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie die Speicherkarte nach dem Test nicht.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Einstecken einer nicht gesperrten »Secure Digital High-Capacity (SDHC)«-Karte erkennt, nachdem sich das System in Bereitschaft befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und stecken Sie eine nicht gesperrte SDHC-Karte in das Lesegerät. Falls sich die Dateiverwaltung öffnet, können Sie diese gefahrlos schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie die Speicherkarte nach dem Test nicht.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Einstecken einer xD-Karte erkennt, nachdem sich das System in Bereitschaft befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und stecken Sie eine xD-Karte in das Lesegerät. Falls sich die Dateiverwaltung öffnet, können Sie diese gefahrlos schließen.
       (Hinweis: Dieser Test schlägt nach 20 Sekunden ohne Aktion fehl.)
    2. Entfernen Sie die Speicherkarte nach dem Test nicht.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests geschieht automatisch. Ändern Sie das automatisch ausgewählte Ergebnis nicht. ZWECK:
    Dieser Test überprüft, ob der Speicherkartenleser des Systems das Einlegen einer MMC-Karte erkennt, nachdem sich das System im Bereitschaftsmodus befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und legen Sie eine MMC-Karte in den Speicherkartenleser ein. Wenn die Dateiverwaltung gestartet wird, können Sie diese ruhig schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie nicht die Speicherkarte nach dem Test.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Test erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Einstecken einer nicht gesperrten SD-Karte erkennt, nachdem sich das System in Bereitschaft befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und stecken Sie eine nicht gesperrte SD-Karte in das Lesegerät. Falls sich die Dateiverwaltung öffnet, können Sie diese gefahrlos schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie die Speicherkarte nach dem Test nicht.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Medienkartenleser des Systems das Einstecken einer nicht gesperrten SDHC-Karte erkennt, nachdem sich das System in Bereitschaft befunden hat.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und stecken Sie eine nicht gesperrte SDHC-Karte in das Lesegerät. Falls sich die Dateiverwaltung öffnet, können Sie diese gefahrlos schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie die Speicherkarte nach dem Test nicht.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob der Speicherkartenleser des Systems das Einlegen eines nicht gesperrten »Secure Digital«-Mediums (SD) erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und legen Sie eine nicht gesperrte SD-Karte in den Speicherkartenleser ein. Wenn die Dateiverwaltung gestartet wird, können Sie diese ruhig schließen.
       (Hinweis: Der Test führt nach 20 Sekunden zu einer Zeitüberschreitung.)
    2. Entfernen Sie nicht die Speicherkarte nach dem Test.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob Sie Ton über ein Bluetooth-Audio-Gerät aufnehmen und hören können.
DURCHFÜHRUNG:
    1. Schalten Sie das Bluetooth-Headset ein.
    2. Klicken Sie auf das Bluetooth-Symbol in der Menüleiste.
    3. Wählen Sie »Neues Gerät einrichten«.
    4. Suchen Sie in der Liste nach dem Gerät und wählen Sie es aus.
    5. Geben Sie den PIN-Code in das Gerät ein, der automatisch vom Assistenten ausgewählt wurde.
    6. Das Gerät sollte eine Verbindung mit dem Rechner herstellen (Pairing).
    7. Klicken Sie auf »Testen«, um fünf Sekunden aufzunehmen und die Aufnahme über das Bluetooth-Gerät wiederzugeben.
ÜBERPRÜFUNG:
    Haben Sie die Aufnahme im Bluetooth-Gerät gehört? ZWECK:
    Dieser Test überprüft, ob Sie Informationen mit Hilfe einer Bluetooth-Verbindung übertragen können.
DURCHFÜHRUNG:
    1. Stellen Sie sicher, dass es Ihnen möglich ist, die Dateien Ihres mobilen Geräts zu durchsuchen.
    2. Kopieren Sie eine Datei vom Rechner auf das mobile Gerät.
    3. Kopieren Sie eine Datei vom mobilen Gerät auf den Rechner.
ÜBERPRÜFUNG:
    Wurden alle Dateien fehlerfrei kopiert? ZWECK:
    Dieser Test stellt sicher, dass Sie ein Bluetooth-HID-Gerät verwenden können.
DURCHFÜHRUNG:
    1. Schalten Sie eine Bluetooth-Maus oder -Tastatur ein.
    2. Klicken Sie auf das Bluetooth-Symbol in der Menüleiste.
    3. Wählen Sie »Neues Gerät einrichten«.
    4. Suchen Sie das Gerät in der Liste und wählen Sie es aus.
    5. Bei Mäusen sollten Sie den Mauszeiger bewegen, die Maustasten drücken und Doppelklicks durchführen.
    6. Bei Tastaturen klicken Sie auf den Test-Knopf, um ein kleines Fenster zu öffnen. Geben Sie etwas Text in das Fenster ein und schließen Sie es anschließend.
ÜBERPRÜFUNG:
    Funktioniert das Gerät wie gewünscht? ZWECK:
    Dieser Test überprüft, ob Sie ein USB-HID-Gerät verwenden können.
DURCHFÜHRUNG:
    1. Schalten Sie entweder eine USB-Maus oder -Tastatur ein.
    2. Falls Sie eine Maus verwenden, führen Sie einige Aktionen wie das Bewegen des Zeigers, Links-, Rechts- und Doppelklicks aus.
    3. Falls Sie eine Tastatur verwenden, klicken Sie auf den »Testen«-Knopf, um ein kleines Fenster zu öffnen. Geben Sie etwas Text ein und schließen Sie das Fenster.
ÜBERPRÜFUNG:
    Hat das Gerät wie erwartet funktioniert? ZWECK:
    Dieser Test überprüft, ob Ihr System USB-Speichergeräte erkennt.
DURCHFÜHRUNG:
    1. Schließen Sie einen oder mehrere USB-Sticks oder -Festplatten an.
    2. Klicken Sie auf »Testen«.
INFO:
    $output
ÜBERPRÜFUNG:
    Wurden die Datenträger erkannt? ZWECK:
    Dieser Test überprüft, ob das System den Anschluss einer FireWire-Festplatte erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«. Der Test führt zu einer Zeitüberschreitung, wenn kein Anschluss innerhalb von 20 Sekunden festgestellt wird.
    2. Schließen Sie eine FireWire-Festplatte an einen verfügbaren FireWire-Anschluss an.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Test erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob Ihr System eine angeschlossene eSATA-Festplatte erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um den Test zu starten. Dieser Test scheitert, wenn das angeschlossene Gerät nicht innerhalb von 20 Sekunden erkannt wird.
    2. Schließen Sie eine eSATA-Festplatte an einen freien eSATA-Anschluss an.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests verläuft automatisch. Ändern Sie nicht das automatisch ausgewählte Ergebnis. ZWECK:
    Dieser Test überprüft, ob das System das Entfernen einer FireWire-Festplatte erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«. Der Test führt zu einer Zeitüberschreitung, wenn kein Entfernen innerhalb von 20 Sekunden festgestellt wird.
    2. Trennen Sie die zuvor angeschlossene FireWire-Festplatte vom FireWire-Anschluss.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Test erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Dieser Test überprüft, ob Ihr System das Entfernen einer eSATA-Festplatte erkennt.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um den Test zu starten. Dieser Test scheitert, wenn das Entfernen des Gerätes nicht innerhalb von 20 Sekunden erkannt wird.
    2. Entfernen Sie die zuvor angeschlossene eSATA-Festplatte von dem eSATA-Anschluss.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Tests verläuft automatisch. Ändern Sie nicht das automatisch ausgewählte Ergebnis. ZWECK:
    Dieser Test überprüft, ob Ihr System vollständig in den Ruhezustand versetzt werden kann (falls unterstützt).
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«.
    2. Das System wird in den Ruhezustand versetzt und sollte innerhalb von 5 Minuten wieder aufwachen.
    3. Falls Ihr System nach 5 Minuten nicht aufgewacht ist, drücken Sie den Ein-/Ausschalter, um das System manuell aufzuwecken.
    4. Falls das System trotzdem nicht aus dem Ruhezustand aufwacht, starten Sie die Systemüberprüfung erneut und markieren Sie diesen Test als gescheitert.
ÜBERPRÜFUNG:
    Wurde das System erfolgreich in den Ruhezustand versetzt und hat es nach dem Aufwecken fehlerfrei funktioniert? ZWECK:
    Dieser Test überprüft die Audio-CD-Wiedergabefähigkeiten Ihres Laufwerks.
DURCHFÜHRUNG:
    1. Legen Sie eine Audio-CD in Ihr optisches Laufwerk ein.
    2. Wenn Sie dazu aufgefordert werden, starten Sie die Musikwiedergabe.
    3. Suchen Sie in der Musikwiedergabe nach der CD.
    4. Wählen Sie die CD in der Musikwiedergabe aus.
    5. Klicken Sie auf die Wiedergabe-Schaltfläche, um die Musik von der CD wiederzugeben.
    6. Beenden Sie die Wiedergabe nach einiger Zeit.
    7. Klicken Sie mit der rechten Maustaste auf das CD-Symbol und wählen Sie »CD auswerfen«.
    8. Die CD sollte ausgeworfen werden.
    9. Schließen Sie die Musikwiedergabe.
ÜBERPRÜFUNG:
    Haben alle Schritte funktioniert? ZWECK
    Dieser Test überprüft die DVD-Wiedergabefunktionen Ihres Systems.
DURCHFÜHRUNG:
    1. Legen Sie eine DVD mit einem Film in Ihr optisches Laufwerk.
    2. Klicken Sie auf »Testen«, um die DVD mit Totem wiederzugeben.
ÜBERPRÜFUNG:
    Wurde die Datei wiedergegeben? ZWECK:
    Prüfung des DVI-Ausgangs.
SCHRITTE:
    Überspringen Sie diese Test, wenn Ihr System keinen DVI-Anschluss besitzt.
    1. Verbinden Sie einen Monitor (falls nicht schon angeschlossen) mit der DVI-Buchse Ihres Systems
BESTÄTIGUNG:
    Wurde die Arbeitsfläche richtig auf beiden Anzeigegeräten dargestellt? ZWECK:
   Prüfung des DisplayPort-Anschlusses.
SCHRITTE:
    Überspringen Sie diesen Test, falls Ihr System keinen DisplayPort-Anschluss hat.
    1. Schließen Sie ein Anzeigegerät (falls nicht bereits angeschlossen) an den DisplayPort-Anschluss ihres Systems an
BESTÄTIGUNG:
    Wurde der Arbeitsplatz auf beiden Anzeigegeräten richtig dargestellt? ZWECK:
   Prüfung des HDMI-Anschlusses.
SCHRITTE:
    Überspringen Sie diesen Test, falls Ihr System keinen HDMI-Anschluss hat.
    1. Schließen Sie ein Anzeigegerät (falls nicht bereits angeschlossen) an den HDMI-Anschluss ihres Systems an
BESTÄTIGUNG:
    Wurde der Arbeitsplatz auf beiden Anzeigegeräten richtig dargestellt? ZWECK:
    Prüfung des RCA-Anschlusses.
SCHRITTE:
    Überspringen Sie diesen Test, falls Ihr System keinen RCA-Anschluss hat.
    1. Schließen Sie ein Anzeigegerät (falls nicht bereits angeschlossen) an den RCA-Anschluss ihres Systems an
BESTÄTIGUNG:
    Wurde der Arbeitsplatz auf beiden Anzeigegeräten richtig dargestellt? ZWECK:
    Prüfung des S-VIDEO-Anschlusses.
SCHRITTE:
    Überspringen Sie diesen Test, falls Ihr System keinen S-VIDEO-Anschluss hat.
    1. Schließen Sie ein Anzeigegerät (falls nicht bereits angeschlossen) an den S-VIDEO-Anschluss ihres Systems an
BESTÄTIGUNG:
    Wurde der Arbeitsplatz auf beiden Anzeigegeräten richtig dargestellt? ZWECK:
    Dieser Test überprüft Ihre USB-3.0-Verbindung.
DURCHFÜHRUNG:
    1. Verbinden Sie eine USB-3.0-Festplatte oder einen USB-Stick mit einem USB-3.0-Anschluss an Ihrem Rechner.
    2. Ein Symbol sollte auf Ihrem Starter erscheinen.
    3. Klicken Sie auf »Testen«, um den Test zu beginnen.
ÜBERPRÜFUNG:
    Dieser Test wird automatisch durchgeführt. Bitte ändern Sie nicht das automatisch ausgewählte Ergebnis. ZWECK:
    Dieser Test überprüft Ihre USB-Verbindung.
DURCHFÜHRUNG:
    1. Verbinden Sie ein USB-Speichergerät mit einem externen USB-Anschluss an diesem Rechner.
    2. Im Starter sollte ein Symbol erscheinen.
    3. Bestätigen Sie das Erscheinen des Symbols.
    4. Entfernen Sie das Gerät (»Auswerfen«).
    5. Wiederholen Sie den Vorgang an jedem externen USB-Anschluss.
ÜBERPRÜFUNG:
    Funktionieren alle USB-Anschlüsse mit dem Gerät? ZWECK:
    Dieser Test überprüft Ihre USB-Verbindung.
DURCHFÜHRUNG:
    1. Verbinden Sie eine USB-Festplatte oder einen USB-Stick mit Ihrem Rechner.
    2. Ein Symbol sollte im Starter erscheinen.
    3. Klicken Sie auf »Testen«, um mit der Überprüfung zu beginnen.
ÜBERPRÜFUNG:
    Die Überprüfung dieses Test erfolgt automatisch. Ändern Sie nicht das automatisch ermittelte Ergebnis. ZWECK:
    Prüfung des VGA-Ausgangs.
SCHRITTE:
    Überspringen Sie diese Test, wenn Ihr System keinen VGA-Anschluss besitzt.
    1. Verbinden Sie einen Monitor (falls nicht schon angeschlossen) mit der VGA-Buchse Ihres Systems
BESTÄTIGUNG:
    Wurde die Arbeitsfläche richtig auf beiden Anzeigegeräten dargestellt? ZWECK:
    Dieser Test überprüft die Sensoren am Deckel Ihres Laptops.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«.
    1. Schließen Sie den Deckel Ihres Laptops und öffnen Sie ihn wieder.
ÜBERPRÜFUNG:
    Geht der Bildschirm aus, wenn Sie den Deckel Ihres Laptops zuklappen? ZWECK:
    Dieser Test überprüft die Sensoren am Deckel Ihres Laptops.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«.
    2. Schließen Sie den Deckel.
    3. Lassen Sie Ihren Deckel 5 Sekunden lang geschlossen.
    4. Öffnen Sie den Deckel.
ÜBERPRÜFUNG:
    Wird das System aufgeweckt, sobald Sie den Deckel wieder öffnen? ZWECK:
    Dieser Test überprüft die Sensoren am Deckel Ihres Laptops.
DURCHFÜHRUNG:
    1. Schließen Sie den Deckel Ihres Laptops.
ÜBERPRÜFUNG:
    Wird Ihr System in Bereitschaft versetzt, wenn Sie den Deckel Ihres Laptops zuklappen? ZWECK:
    Dieser Test überprüft die Energiesparmöglichkeiten Ihres Bildschirms.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um die Energiesparmöglichkeiten Ihres Bildschirms zu testen.
    2. Drücken Sie eine Taste oder bewegen Sie die Maus, um den Bildschirm wiederherzustellen.
ÜBERPRÜFUNG:
     Dunkelte sich der Bildschirm ab und schaltete sich danach wieder ein? ZWECK:
    Dieser Test überprüft, ob Ihr System herunterfahren und starten kann.
DURCHFÜHRUNG:
    1. Wählen Sie »Testen«, um den Rechner herunterzufahren.
    2. Starten Sie den Rechner erneut.
    3. Wählen Sie im Grub-Boot-Menü den Eintrag »Xen Hypervisor«.
    4. Melden Sie sich nach dem Neustart an und starten Sie »checkbox-certification-server«.
    5. Wählen Sie »Wiederholen«, um diesen Test erneut zu starten.
    6. Wählen Sie »Ja«, wenn der Rechner erfolgreich heruntergefahren und neu gestartet wurde, ansonsten wählen Sie »Nein«.
ÜBERPRÜFUNG:
    Wurde das System erfolgreich heruntergefahren und neu gestartet? ZWECK:
    Dieser Test überprüft, ob Ihr System herunterfahren und starten kann.
DURCHFÜHRUNG:
    1. Schalten Sie Ihren Rechner aus.
    2. Starten Sie Ihren Rechner.
    3. Wiederholen Sie Schritt 1 und 2 mindestens 5 mal.
ÜBERPRÜFUNG:
    Wurde das System erfolgreich heruntergefahren und neu gestartet? ZWECK:
    Dieser Test überprüft Ihre Kabelnetzwerkverbindung.
DURCHFÜHRUNG:
    1. Klicken Sie auf das Netzwerksymbol in der oberen Menüleiste.
    2. Wählen Sie ein Netzwerk aus dem Bereich »Kabelnetzwerk«.
    3. Klicken Sie auf »Testen«, um zu prüfen, ob eine HTTP-Verbindung hergestellt werden kann.
ÜBERPRÜFUNG:
    Erschien ein Hinweisfenster und wurde die Verbindung korrekt hergestellt? ZWECK:
    Dieser Test überprüft Ihre Funknetzwerkverbindung.
DURCHFÜHRUNG:
    1. Klicken Sie auf das Netzwerksymbol in der oberen Menüleiste.
    2. Wählen Sie ein Netzwerk aus dem Abschnitt »Funknetzwerke«.
    3. Klicken Sie auf »Testen«, um zu überprüfen, ob eine HTTP-Verbindung hergestellt werden kann.
ÜBERPRÜFUNG:
    Wurde eine Benachrichtigung angezeigt und eine Verbindung erfolgreich hergestellt? ZWECK:
    Dieser Test wird durch die erkannten Bildschirmmodi schalten.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und der Bildschirm wird durch die Bildschirmmodi schalten.
ÜBERPRÜFUNG:
    Sah die Anzeige auf Ihrem Bildschirm in den erkannten Modi korrekt aus? ZWECK:
    Dieser Test stellt sicher, dass der Laptop nach Ausführung der Akkutests wieder mit dem Stromnetz verbunden wird.
DURCHFÜHRUNG:
    1. Schließen Sie den Laptop an das Stromnetz an.
ÜBERPRÜFUNG:
    Wurde der Laptop ans Stromnetz angeschlossen? ZWECK:
    Dieser Test stellt sicher, dass das Stromkabel nicht angeschlossen ist, um den Akkuleerlauftest ausführen zu können.
DURCHFÜHRUNG:
    1. Trennen Sie den Laptop von der Stromversorgung.
ÜBERPRÜFUNG:
    Wurde der Laptop von der Stromversorgung getrennt? ZWECK:
    Diese Test schickt das Bild »JPEG_Color_Image_Ubuntu.jpg« an ein festgelegtes Gerät.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und Sie werden aufgefordert, den Namen eines Bluetooth-Gerätes einzugeben, das Dateiübertragungen entgegen nehmen kann. (Nach der Eingabe des Namens kann es einen Moment dauern, bevor mit dem Senden der Datei begonnen wird.)
    2. Bestätigen Sie alle Nachfragen auf beiden Geräten.
ÜBERPRÜFUNG:
    Wurden die Daten fehlerfrei übertragen? ZWECK:
    Dieser Test sendet das Bild »JPEG_Color_Image_Ubuntu.jpg« an ein bestimmtes Gerät.
DURCHFÜHRUNG:
    1. Stellen Sie sicher, dass Bluetooth eingeschaltet ist, indem Sie das Bluetooth-Menü überprüfen.
    2. Klicken Sie auf »Testen«. Sie werden aufgefordert, den Namen eines Bluetooth-Gerätes einzugeben, welches den Empfang von Dateien akzeptiert (Es kann nach der Eingabe des Namens einige Sekunden dauern, bis die Datei gesendet wird).
    3. Akzeptieren Sie die erscheinenden Meldungen auf beiden Geräten.
ÜBERPRÜFUNG:
    Wurde die Datei korrekt verschickt? ZWECK:
    Dieser Test überprüft, ob die Bildschirmhelligkeit geändert werden kann.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um das Abdunkeln des Bildschirms zu versuchen.
    2. Überprüfen Sie, ob der Bildschirm auf etwa die Hälfte der maximalen Helligkeit abgedunkelt wurde.
    3. Der Bildschirm wird nach 2 Sekunden wieder auf die ursprüngliche Helligkeit gestellt.
ÜBERPRÜFUNG:
    Wurde Ihr Bildschirm ungefähr auf die Hälfte der maximalen Helligkeit abgedunkelt? ZWECK:
    Dieser Test überprüft das Drehen der Anzeige.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um das Drehen der Anzeige zu testen. Der Bildschirminhalt wird alle 4 Sekunden gedreht.
    2. Überprüfen Sie, ob alle gedrehten Anzeigen (normal, rechts, kopfüber, links) ohne dauerhafte Darstellungsfehler stattgefunden haben.
ÜBERPRÜFUNG:
    Wurde das Drehen der Anzeige ohne dauerhafte Darstellungsfehler ausgeführt? ZWECK:
    Prüfung der Akkuinformationstaste
SCHRITTE:
    Überspringen Sie diesen Test, falls Sie keine Akkuinformationstaste haben.
    1. Klicken Sie auf »Test« um zu beginnen
    2. Drücken Sie auf die Akkuinformationstaste (oder die jeweilige Kombination wie Fn+F3)
    3. Schließen Sie das Fenster mit der Leistungsstatistik
BESTÄTIGUNG:
    Funktioniert die Akkuinformationstaste wie erwartet? ZWECK:
    Prüfung der Batterieanzeige nach Beendigung des Ruhezustands 
SCHRITTE:
    Überspringen Sie diesen Test, wenn Ihr System über keine Batterieanzeigeknopf verfügt.
    1. Klicken Sie auf » Test«, um zu beginnen.
    2. Drücken Sie auf Batterieanzeigeknopf (oder die Tastenkombination wie z.B. Fn+F3)
    3: Schließen Sie Energieverbrauchsstatistik, falls diese angezeigt wurde
BESTÄTIGUNG:
    Funktionierte die Batterieanzeigetaste nach Beendigung des Ruhezustands wie erwartet? ZWECK:
    Dieser Test überprüft die Tasten zur Regelung der Bildschirm-Hintergrundbeleuchtung.
DURCHFÜHRUNG:
    1. Drücken Sie die Tasten zur Helligkeitsregelung auf der Tastatur.
ÜBERPRÜFUNG:
    Hat sich die Helligkeit der Bildschirm-Hintergrundbeleuchtung geändert, nachdem Sie die Tasten gedrückt haben? ZWECK:
    Dieser Test überprüft die Tasten zur Regelung der Bildschirm-Hintergrundbeleuchtung, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde
DURCHFÜHRUNG:
    1. Drücken Sie die Tasten zur Helligkeitsregelung auf der Tastatur.
ÜBERPRÜFUNG:
    Folgt die Helligkeit der Bildschirm-Hintergrundbeleuchtung Ihrem Tastendruck, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde? ZWECK:
    Dieser Test überprüft den Standardbildschirm.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um einen Video-Test anzuzeigen.
ÜBERPRÜFUNG:
    Sehen Sie farbige Balken und Rauschen? ZWECK:
    Prüfung der Multimediatasten auf Ihrer Tastatur.
SCHRITTE:
    Überspringen Sie diesen Test, falls Sie keine Multimediatasten auf der Tastatur haben.
    1. Klicken Sie auf »Test«, um zu beginnen.
    2. Falls alle Tasten richtig funktionieren, wird der Test als bestanden markiert.
BESTÄTIGUNG:
    Funktionieren die Tasten wie erwartet? ZWECK:
    Prüfung der Medientasten der Tastatur nach Beendigung des Ruhezustands
SCHRITTE:
    Überspringen Sie diesen Test, wenn Ihr System über keine Medientasten verfügt.
    1. Klicken Sie auf » Test«, um das Testfenster für die Medientasten zu öffnen.
    2. Wenn alle Tasten funktionieren, wird der Test als bestanden markiert.
BESTÄTIGUNG:
    Funktionieren die Medientasten nach Beendigung des Ruhezustands wie erwartet? ZWECK:
    Dieser Test überprüft die Funktion der Stumm-Taste Ihrer Tastatur.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um ein Fenster zu öffnen, welches die Stummschaltung überprüft.
    2. Wenn die Taste funktioniert, ist der Test bestanden und das Fenster wird geschlossen.
ÜBERPRÜFUNG:
    Funktioniert die Stumm-Taste wie erwartet? ZWECK:
    Dieser Test überprüft die Funktion der Stumm-Taste Ihrer Tastatur, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um ein Fenster zu öffnen, welches die Stummschaltung überprüft.
    2. Wenn die Taste funktioniert, ist der Test bestanden und das Fenster wird geschlossen.
ÜBERPRÜFUNG:
    Folgt die Stummschaltung Ihrem Tastendruck? ZWECK:
    Dieser Test überprüft die Bereitschaftstaste.
DURCHFÜHRUNG:
    1. Drücken Sie die Bereitschaftstaste auf der Tastatur.
    2. Wecken Sie das System durch Drücken des An-/Aus-Schalters auf.
ÜBERPRÜFUNG:
    Wurde das System nach dem Drücken der Bereitschaftstaste in Bereitschaft versetzt? ZWECK:
    Dieser Test überprüft die Bereitschaftstaste, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Drücken Sie die Bereitschaftstaste auf der Tastatur.
    2. Wecken Sie das System durch Drücken des An-/Aus-Schalters auf.
ÜBERPRÜFUNG:
    Wurde das System nach dem Drücken der Bereitschaftstaste in Bereitschaft versetzt, nachdem der Rechner zuvor in Bereitschaft versetzt und wieder aufgeweckt wurde? ZWECK:
    Dieser Test überprüft die Super-Taste Ihrer Tastatur.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um ein Fenster zu öffnen, in dem die Super-Taste überprüft werden kann.
    2. Wenn die Taste funktioniert, wird dieser Test als bestanden angesehen und das Fenster geschlossen.
ÜBERPRÜFUNG:
    Hat die Super-Taste so wie erwartet funktioniert? ZWECK:
    Dieser Test überprüft die Super-Taste Ihrer Tastatur, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um ein Fenster zu öffnen, in dem die Super-Taste überprüft werden kann.
    2. Wenn die Taste funktioniert, wird dieser Test als bestanden angesehen und das Fenster geschlossen.
ÜBERPRÜFUNG:
    Hat die Super-Taste wie erwartet funktioniert, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde? ZWECK:
    Prüfung der Lautstärketasten auf ihrer Tastatur
SCHRITTE:
    Überspringen Sie diesen Test, falls Sie keine Lautstärketasten auf der Tatstatur haben.
    1. Klicken Sie auf »Test« um das Testfenster zu öffnen.
    2. Falls alle Tasten funktionieren, wird der Test als bestanden markiert.
BESTÄTIGUNG:
    Funktioneren die Tasten wie erwartet? ZWECK:
    Prüfung der Lautstärketasten der Tastatur nach Rückkehr des Systems aus dem Ruhezustand.
STEPS:
    Überspringen Sie diesen Test, wenn Ihr System über keine Lautstärketasten verfügt.
    1. Klicken Sie auf »Test«, um ein Fenster für die Prüfung der Lautstärketasten zu öffnen.
    2. Wenn alle Tasten funktionieren, wird der Test als bestanden markiert.
BESTÄTIGUNG:
    Konnte die Lautstärke mit den Tasten geregelt werden? ZWECK:
    Dieser Test überprüft die WLAN-Taste.
DURCHFÜHRUNG:
    1. Drücken Sie die WLAN-Taste auf der Tastatur.
    2. Prüfen Sie, ob die WLAN-LED erlischt oder die Farbe ändert.
    3. Prüfen Sie, ob WLAN deaktiviert ist.
    4. Drücken Sie dieselbe Taste erneut.
    5. Prüfen Sie, ob die WLAN-LED leuchtet oder die Farbe ändert.
    6. Prüfen Sie, ob das WLAN aktiviert ist.
ÜBERPRÜFUNG:
    Wurde das WLAN beim ersten Tastendruck ausgeschaltet und beim zweiten wieder eingeschaltet?
    (HINWEIS: Die LED-Funktionalität wird im nachfolgenden Test überprüft. Bitte berücksichtigen Sie hier ausschließlich die Funktionalität des WLAN selbst.) ZWECK:
    Dieser Test überprüft die Funknetzwerktaste, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Drücken Sie die Funknetzwerktaste auf der Tastatur.
    2. Drücken Sie die gleiche Taste noch einmal.
ÜBERPRÜFUNG:
    Schaltete sich die Funknetzwerkverbindung beim ersten Betätigen ab und schaltete sie sich beim zweiten Mal wieder ein, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde? ZWECK:
    Dieser Test überprüft Ihren Beschleunigungsmesser, um festzustellen, ob er
    als Joystick erkannt wurde und als solcher eingesetzt werden kann.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«
    2. Bewegen Sie Ihre Hardware in die Richtungen auf dem Bildschirm bis die Grenze der Achse erreicht wurde.
ÜBERPRÜFUNG:
    Wurde Ihr Beschleunigungsmesser korrekt erkannt? Können Sie das Gerät verwenden? ZWECK:
    Dieser Test überprüft Ihre Tastatur.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«.
    2. Benutzen Sie Ihre Tastatur, um in den Textbereich irgendetwas einzugeben.
ÜBERPRÜFUNG:
    Arbeitet Ihre Tastatur einwandfrei? ZWECK:
    Dieser Test überprüft Ihr Zeigegerät.
DURCHFÜHRUNG:
    1. Bewegen Sie den Mauszeiger mithilfe des Zeigegeräts oder berühren Sie den Bildschirm.
    2. Führen Sie einige Einzel-/Doppel-/Rechtsklicks aus.
ÜBERPRÜFUNG:
    Arbeitete das Zeigegerät so, wie es vorgesehen ist? ZWECK:
    Dieser Test überprüft, ob die grafische Oberfläche nach einer manuellen Änderung der Bildschirmauflösung noch verwendet werden kann.
DURCHFÜHRUNG:
    1. Öffnen Sie die Anwendung »Systemeinstellungen → Monitore«.
    2. Wählen Sie eine neue Auflösung aus der Auswahlliste.
    3. Klicken Sie auf Anwenden.
    4. Wählen Sie die vorherige Auflösung aus der Auswahlliste.
    5. Klicken Sie auf Anwenden.
ÜBERPRÜFUNG:
    Hat sich die Auflösung wie erwartet geändert? ZWECK:
    Dieser Test überprüft, ob Ihr System erfolgreich neu starten kann.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um einen Neustart des Rechners zu veranlassen.
    2. Wenn das Grub-Boot-Menü erscheint, wählen Sie Ubuntu (oder lassen Sie das Standardsystem starten).
    3. Melden Sie sich nach dem Neustart an und starten Sie »checkbox-certification-server« neu.
    4. Wählen Sie »Wiederholen« um diesen Test erneut zu starten.
    5. Wählen Sie »Ja«, wenn der Rechner erfolgreich neu gestartet ist, ansonsten wählen Sie »Nein«.
ÜBERPRÜFUNG:
    Wurde der Rechner erfolgreich neu gestartet? ZWECK:
    Dieser Test überprüft die Standardauflösung des Bildschirms.
DURCHFÜHRUNG:
    1. Dieser Bildschirm verwendet die folgende Auflösung:
INFO:
    $output
ÜBERPRÜFUNG:
    Ist dies für Ihren Bildschirm angemessen? ZWECK:
    Prüfung, ob ein ExpressCard-Steckplatz eine eingesteckte Karte erkennen kann.
SCHRITTE:
    Überspringen Sie diesen Test, falls Sie keinen ExpressCard-Steckplatz haben.
    1. Stecken Sie ein ExpressCard-Gerät in den ExpressCard-Steckplatz.
BESTÄTIGUNG:
    Wurde das Gerät richtig erkannt? ZWECK:
    Stellt sicher, dass das Drücken des Funkverbindungsschalters das System nicht einfrieren oder Anwendungen aus der Menüleiste verschwinden lässt.
DURCHFÜHRUNG:
    1. Melden Sie sich an Ihrer Arbeitsumgebung an.
    2. Betätigen Sie den Funkverbindungsschalter einmal pro Sekunde und steigern Sie das Tempo anschließend.
ÜBERPRÜFUNG:
    Überprüfen Sie, dass das System nicht eingefroren ist und das Bluetooth- und Funkverbindungsmenü noch immer sichtbar und bedienbar sind. ZWECK:
    Überprüfung der Touchpad-LED
DURCHFÜHRUNG:
    1. Drücken Sie die Touchpad-Taste oder die Tastenkombination, um die Touchpad-Taste ein-/auszuschalten.
    2. Bewegen Sie Ihren Finger auf dem Touchpad.
ÜBERPRÜFUNG:
    1. Der Zustand der Touchpad-LED sollte sich jedes mal beim Drücken der Taste oder der Tastenkombination ändern.
    2. Wenn die LED leuchtet, sollte sich der Mauszeiger bei der Benutzung des Touchpads bewegen.
    3. Wenn die LED nicht leuchtet, sollte sich der Mauszeiger bei der Benutzung des Touchpads nicht bewegen. ZWECK:
    Dieser Test überprüft, ob die Touchpad-LED funktioniert, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Drücken Sie die Touchpad-Taste oder die Tastenkombination, um die Touchpad-Tasten ein-/auszuschalten.
    2. Bewegen Sie Ihren Finger auf dem Touchpad.
ÜBERPRÜFUNG:
    1. Der Zustand der Touchpad-LED sollte sich jedes mal beim Drücken der Taste oder der Tastenkombination ändern.
    2. Wenn die LED leuchtet, sollte sich der Mauszeiger bei der Benutzung des Touchpads bewegen.
    3. Wenn die LED nicht leuchtet, sollte sich der Mauszeiger bei der Benutzung des Touchpads nicht bewegen. ZWECK:
    Überprüfung des horizontalen Bildlaufs mit dem Touchpad
DURCHFÜHRUNG:
    1. Wählen Sie »Testen«, wenn Sie bereit sind, und platzieren Sie den Mauszeiger innerhalb der Grenzen des dargestellten Testfensters.
    2. Überprüfen Sie, ob Sie die horizontale Bildlaufleiste verschieben können, indem Sie Ihre Finger im unteren Teil des Touchpads nach links und rechts bewegen.
ÜBERPRÜFUNG:
    Konnten Sie die Bildlaufleiste nach rechts und links bewegen? ZWECK:
    Manuelle Multitouch-Erkennung über das Touchpad.
DURCHFÜHRUNG:
    1. Sehen Sie sich die Spezifikationen für Ihr System an.
ÜBERPRÜFUNG:
    Unterstützt das Touchpad Multitouch? ZWECK:
    Überprüfen, ob das Touchpad funktioniert.
DURCHFÜHRUNG:
    1. Stellen Sie sicher, dass das Touchpad aktiviert ist.
    2. Bewegen Sie den Mauszeiger mit Ihrem Touchpad.
ÜBERPRÜFUNG:
    Bewegt sich der Mauszeiger? ZWECK:
    Überprüfung des vertikalen Bildlaufs mit dem Touchpad
DURCHFÜHRUNG:
    1. Wählen Sie »Testen«, wenn Sie bereit sind, und platzieren Sie den Mauszeiger innerhalb der Grenzen des dargestellten Testfensters.
    2. Überprüfen Sie, ob Sie die vertikale Bildlaufleiste verschieben können, indem Sie Ihre Finger im unteren Teil des Touchpads nach oben und unten bewegen.
ÜBERPRÜFUNG:
    Konnten Sie die Bildlaufleiste nach oben und unten bewegen? ZWECK:
    Manuelle Multitouch-Erkennung über den Touchscreen.
DURCHFÜHRUNG:
    1. Sehen Sie sich die Spezifikationen für Ihr System an.
ÜBERPRÜFUNG:
    Unterstützt der Touchscreen Multitouch? ZWECK:
    Dieser Test überprüft die LED für Funkverbindungen (WLAN + Bluetooth), nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Stellen Sie sicher, dass die WLAN-Verbindung aufgebaut wurde und Bluetooth eingeschaltet ist.
    2. Die WLAN/Bluetooth-LED sollte leuchten.
    3. Schalten Sie WLAN und Bluetooth mit dem Schalter aus (falls vorhanden).
    4. Schalten Sie es wieder an.
    5. Schalten Sie WLAN und Bluetooth über die Menüleiste aus.
    6. Schalten Sie es wieder an.
ÜBERPRÜFUNG:
    Hat die WLAN/Bluetooth-LED wie erwartet geleuchtet, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde? ZWECK:
    Überprüfen, dass die WLAN-LED beim Deaktivieren abgeschaltet wird
DURCHFÜHRUNG:
    1. Während des Tasten/WLAN-Tests sollten Sie die WLAN-LED beobachten, nachdem das WLAN deaktiviert wurde.
    2. Die WLAN-LED sollte beim Deaktivieren des WLAN abgeschaltet werden oder die Farbe wechseln.
ÜBERPRÜFUNG:
    Hat sich die WLAN-LED wie erwartet abgeschaltet oder die Farbe gewechselt? ZWECK:
    Überprüft, ob die WLAN-LED bei deaktivierter WLAN-Funktion ausgeschaltet wird, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Verbinden Sie sich mit einem WLAN-Zugriffspunkt.
    2. Benutzen Sie den Schalter, um die WLAN-Funktion auszuschalten.
    3. Schalten Sie die Funktion wieder ein.
    4. Benutzen Sie die Netzwerkverwaltung, um die WLAN-Funktion zu deaktivieren.
ÜBERPRÜFUNG:
    Schaltet sich die LED aus, sobald die WLAN-Funktion deaktiviert ist, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde? ZWECK:
    Überprüft, ob die Bluetooth-LED leuchtet / nicht leuchtet, sobald Bluetooth aktiviert / deaktiviert ist.
DURCHFÜHRUNG:
    1. Deaktivieren Sie Bluetooth mithilfe eines Geräteschalters (sofern vorhanden).
    2. Aktivieren Sie Bluetooth wieder.
    3. Deaktivieren Sie Bluetooth mithilfe der Menüeinstellung.
    4. Aktivieren Sie Bluetooth wieder.
ÜBERPRÜFUNG:
    Hat sich die Bluetooth-LED zweimal aus- und eingeschaltet? ZWECK:
    Überprüfen, ob die Bluetooth-LED leuchtet/nicht leuchtet, sobald Bluetooth aktiviert/deaktiviert ist, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Deaktivieren Sie Bluetooth mithilfe eines Geräteschalters (sofern vorhanden).
    2. Aktivieren Sie Bluetooth wieder.
    3. Deaktivieren Sie Bluetooth mithilfe der Menüeinstellung.
    4. Aktivieren Sie Bluetooth wieder.
ÜBERPRÜFUNG:
    Hat sich die Bluetooth-LED zweimal aus- und eingeschaltet, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde? ZWECK: 
    Überprüfung der Funktion der Umschalttaste, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Drücken Sie die Umschalttaste, um die dauerhafte Großschreibung zu aktivieren/deaktivieren.
    2. Immer wenn die Umschalttaste betätigt wird, sollte die LED der Umschaltfunktion an/aus gehen.
ÜBERPRÜFUNG:
    Hat sich die LED der Umschalttaste entsprechend der Änderung der Funktion verhalten? ZWECK:
    Überprüfung der externen Video-Taste auf Funktionalität
DURCHFÜHRUNG:
    1. Schließen Sie einen externen Monitor an.
    2. Drücken Sie auf die Anzeige-Taste, um die Konfiguration des Monitors zu ändern.
ÜBERPRÜFUNG:
    Sehen Sie nach, ob das Videosignal auf dem externen Gerät oder nur auf dem eingebauten Gerät gespiegelt, erweitert und angezeigt werden kann. ZWECK:
    Überprüfung der externen Video-Taste auf Funktionalität, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Schließen Sie einen externen Monitor an.
    2. Drücken Sie auf die Anzeige-Taste, um die Konfiguration des Monitors zu ändern.
ÜBERPRÜFUNG:
    Sehen Sie nach, ob das Videosignal auf dem externen Gerät oder nur auf dem eingebauten Gerät gespiegelt, erweitert und angezeigt werden kann, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde. ZWECK:
    Überprüft, ob die Festplatten-LED auch dann korrekt arbeitet, wenn der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Wählen Sie »Testen«, um ein temporäre Datei einige Sekunden lang zu schreiben und zu lesen.
    2. Die Festplatten-LED sollte blinken, wenn von der Festplatte gelesen/auf die Festplatte geschrieben wird.
ÜBERPRÜFUNG:
    Hat die Festplatten-LED auch bei Aktivität geblinkt, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde? ZWECK:
    Dieser Test überprüft, ob die LED für den Akkustatus eine geringe Akkuleistung anzeigt.
DURCHFÜHRUNG:
    1. Benutzen Sie das System für mehrere Stunden im Akkubetrieb.
    2. Beobachten Sie die LED für den Akkustatus.
ÜBERPRÜFUNG:
    Leuchtet die LED orangefarbenen sobald die Akkuleistung niedrig ist? ZWECK:
    Überprüft, ob die Akkuanzeige-LED einen kritischen Ladezustand anzeigt, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Lassen Sie das System mehrere Stunden im Akkubetrieb laufen.
    2. Beobachten Sie die Akku-LED sorgfältig.
ÜBERPRÜFUNG:
    Leuchtete die Akkuanzeige-LED orange, auch nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde? ZWECK:
    Dieser Test überprüft, ob die LED für den Akkustatus mit aufgeladenem Akku funktioniert.
DURCHFÜHRUNG:
    1. Benutzen Sie das System kurz im Akkubetrieb.
    2. Schließen Sie das Stromkabel an.
    3. Benutzen Sie das System mit angeschlossenem Stromkabel.
ÜBERPRÜFUNG:
    Erlischt die orangefarbene LED für den Akkustatus sobald der Akku voll geladen ist? ZWECK:
    Überprüft, ob die Akkuanzeige-LED den geladenen Zustand anzeigt, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Lassen Sie das System einige Zeit im Akkubetrieb laufen.
    2. Schließen Sie ein Stromkabel an.
    3. Betreiben Sie das System mit Stromkabel.
ÜBERPRÜFUNG:
    Hat sich die orange Akkuanzeige-LED abgeschaltet als der Akku voll geladen war, auch nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde? ZWECK:
    Dieser Test überprüft, ob die LED für den Akkustatus leuchtet, wenn der Akku lädt.
DURCHFÜHRUNG:
    1. Benutzen Sie das System kurz im Akkubetrieb.
    2. Schließen Sie das Stromkabel an.
ÜBERPRÜFUNG:
    Leuchtet die LED für den Akkustatus orange? ZWECK:
    Sicherstellen, dass die Akkulampe den Ladezustand anzeigt, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Lassen Sie das System einige Zeit im Akkubetrieb laufen.
    2. Schließen Sie ein Stromkabel an.
ÜBERPRÜFUNG:
    Wurde die Akkuanzeige-LED orange, auch nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde? ZWECK:
    Überprüft, ob die Kamera-LED wie erwartet funktioniert, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde 
DURCHFÜHRUNG:
    1. Wählen Sie »Testen«, um die Kamera einzuschalten.
    2. Die Kamera-LED sollte ein paar Sekunden lang leuchten.
ÜBERPRÜFUNG:
    Hat sich die Kamera-LED ein- und ausgeschaltet, auch nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde? ZWECK:
    Überprüft, ob die LED des Ziffernblocks vor und nach dem Bereitschaftsmodus den gleichen Status anzeigt.
DURCHFÜHRUNG:
    1. Drücken Sie die »Num Lock«-Taste, um die Num-LED umzuschalten.
    2. Klicken Sie auf den Knopf »Testen«, um ein Fenster zu öffnen, in dem Sie Ihre Eingaben prüfen können.
    3. Geben Sie etwas über den Ziffernblock ein – einmal wenn die LED an ist, und einmal, wenn sie aus ist.
ÜBERPRÜFUNG:
    1. Die LED sollte bei jedem Druck auf »Num Lock« den Status ändern.
    2. Nummern sollten nur dann in das Eingabefeld geschrieben werden, wenn die LED an ist. ZWECK:
    Überprüfen Sie, ob die Energie-LED korrekt funktioniert, nachdem das System im Bereitschaftsmodus war.
DURCHFÜHRUNG:
    1. Die Energie-LED sollte leuchten, wenn das Gerät eingeschaltet ist.
ÜBERPRÜFUNG:
    Leuchtet die Energie-LED weiterhin, nachdem das System im Bereitschaftsmodus war? ZWECK:
    Überprüft das Ein- und Ausschalten des Touchpads mit der Schnelltaste.
DURCHFÜHRUNG:
    1. Überprüfen Sie, ob das Touchpad funktioniert.
    2. Drücken Sie die Schnelltaste für das Touchpad.
    3. Drücken Sie wieder die Schnelltaste für das Touchpad.
ÜBERPRÜFUNG:
    Überprüfen Sie, ob das Touchpad aus- und eingeschaltet wurde. ZWECK:
    Überprüft das Ein- und Ausschalten des Touchpads mit der Schnelltaste, nachdem der Rechner in Bereitschaft versetzt und wieder aufgeweckt wurde.
DURCHFÜHRUNG:
    1. Überprüfen Sie, ob das Touchpad funktioniert.
    2. Drücken Sie die Schnelltaste für das Touchpad.
    3. Drücken Sie wieder die Schnelltaste für das Touchpad.
ÜBERPRÜFUNG:
    Überprüfen Sie, ob das Touchpad aus- und eingeschaltet wurde. ZWECK:
    Überprüfung der WLAN-LED
DURCHFÜHRUNG:
    1. Während des Tasten/Funknetzwerktest, sollten Sie die WLAN-LED beobachten, während Sie die Funknetzwerkfunktion wieder eingeschaltet haben.
    2. Die WLAN-LED sollte leuchten oder die Farbe wechseln, wenn die Funknetzwerkfunktion eingeschaltet wird.
ÜBERPRÜFUNG:
    Hat die WLAN-LED wie erwartet geleuchtet oder die Farbe gewechselt? ZWECK:
    Überprüfung der WLAN-LED, nachdem dem Aufwachen aus dem Bereitschaftsmodus.
DURCHFÜHRUNG:
    1. Stellen Sie sicher, dass eine WLAN-Verbindung besteht.
    2. Die WLAN-LED sollte leuchten.
ÜBERPRÜFUNG:
    Leuchtete die WLAN-LED nach dem Aufwachen aus dem Bereitschaftsmodus wie erwartet? ZWECK:
    Aufwecken mittels USB-Tastatur
DURCHFÜHRUNG:
    1. Aktivieren Sie den Eintrag »Aufwecken mit USB-Tastatur/Maus« im BIOS.
    2. Drücken Sie auf »Testen«, um in den Bereitschaftsmodus (S3) zu wechseln.
    3. Drücken Sie eine beliebige Taste auf der USB-Tastatur, um das System aufzuwecken.
ÜBERPRÜFUNG:
    Wurde das System aus dem Bereitschaftsmodus aufgeweckt, als Sie eine Taste auf der Tastatur gedrückt haben? ZWECK:
    Aufwecken mittels USB-Maus
DURCHFÜHRUNG:
    1. Aktivieren Sie den Eintrag »Aufwecken mit USB-Tastatur/Maus« im BIOS.
    2. Drücken Sie auf »Testen«, um in den Bereitschaftsmodus (S3) zu wechseln.
    3. Drücken Sie eine beliebige Taste auf der USB-Maus, um das System aufzuwecken.
ÜBERPRÜFUNG:
    Wurde das System aus dem Bereitschaftsmodus aufgeweckt, als Sie eine Maustaste gedrückt haben? ZWECK:
    Überprüfung der LED für Funkverbindungen (WLAN + Bluetooth)
DURCHFÜHRUNG:
    1. Stellen Sie sicher, dass die WLAN-Verbindung aufgebaut wurde und Bluetooth eingeschaltet ist.
    2. Die WLAN/Bluetooth-LED sollte leuchten.
    3. Schalten Sie WLAN und Bluetooth mit dem Schalter aus (falls vorhanden).
    4. Schalten Sie es wieder an.
    5. Schalten Sie WLAN und Bluetooth über die Menüleiste aus.
    6. Schalten Sie es wieder an.
ÜBERPRÜFUNG:
    Hat die WLAN/Bluetooth-LED wie erwartet geleuchtet? ZWECK:
    Dies ist ein automatisierter Stresstest, der das System zwingt, 250 mal in den Ruhezustand versetzt und wieder aufgeweckt zu werden. ZWECK:
    Dies ist ein automatisierter Stresstest, der das System zwingt, 30 mal in den Ruhezustand versetzt und wieder aufgeweckt zu werden. ZWECK:
    Dies ist ein automatisierter Stresstest, der das System zwingt, 250 mal in Bereitschaft versetzt und wieder aufgeweckt zu werden. ZWECK:
    Dies ist ein automatisierter Stresstest, der das System zwingt, 30 mal in Bereitschaft versetzt und wieder aufgeweckt zu werden. ZWECK:
   Dieser Test überprüft, ob ein USB-DSL-Modem oder Mobiles-Breitband-Modem richtig funktioniert.
DURCHFÜHRUNG:
   1. Verbinden Sie das USB-Kabel mit dem Rechner.
   2. Rechtsklicken Sie auf das Netzwerk-Symbol in der Menüleiste.
   3. Wählen Sie »Verbindungen bearbeiten« aus.
   4. Wählen Sie den Reiter »DSL« (für ADSL-Modems) oder »Mobiles Breitband« (für 3G-Modems).
   5. Klicken Sie auf den »Hinzufügen«-Knopf.
   6. Richten Sie die Verbindungsparameter ordnungsgemäß ein.
   7. Eine Benachrichtigungsanzeige sollte die Herstellung der Verbindung bestätigen.
   8. Wählen Sie »Testen«, um zu überprüfen, ob eine HTTP-Verbindung aufgebaut werden kann.
ÜBERPRÜFUNG:
   Wurde die Verbindung ordnungsgemäß hergestellt? ZWECK:
   Dieser Test überprüft, ob der Fingerabdruckleser genutzt werden kann, um ein gesperrtes System zu entsperren.
DURCHFÜHRUNG:
   1. Öffnen Sie das Sitzungsmenü (Zahnrad auf der rechten Seite der Menüleiste)
   2. Wählen Sie »Bildschirm sperren«.
   3. Drücken Sie eine Taste oder bewegen Sie die Maus.
   4. Ein Fenster sollte sich öffnen, welches die Möglichkeit zum Entsperren anbietet, entweder durch Eingabe des Passworts oder durch die Fingerabdrucklegitimierung.
   5. Nutzen Sie zum Entsperren den Fingerabdruckleser.
   6. Ihr Bildschirm sollte jetzt entsperrt sein.
ÜBERPRÜFUNG:
   Hat der Legitimierungsprozess korrekt funktioniert? ZWECK:
   Dieser Test stellt sicher, dass ein Fingerabdruckleser zur Anmeldung an Ihrem System genutzt werden kann. Der Test setzt voraus, dass es ein Testkonto gibt, von dem aus Testfälle ausgeführt werden können, und ein persönliches Konto, welches der Tester zum Überprüfen des Fingerabdrucklesers verwenden kann.
DURCHFÜHRUNG:
   1. Klicken Sie auf die Benutzeranzeige auf der linken Seite der Menüleiste (Ihr Benutzername).
   2. Wählen Sie »Benutzerkonto wechseln«
   3. Wählen Sie auf dem LightDM-Bildschirm Ihren Benutzernamen.
   4. Melden Sie sich mit dem Fingerabdruckleser an.
   5. Klicken Sie auf die Benutzeranzeige.
   6. Wählen Sie das Testkonto, um mit der Ausführung der Tests fortzufahren.
ÜBERPRÜFUNG:
   Hat die Authentifizierung problemlos funktioniert? ZWECK:
    Dieser Test überprüft, ob ein Netzwerkdrucker genutzt werden kann.
DURCHFÜHRUNG:
    1. Stellen Sie sicher, dass ein Drucker in Ihrem Netzwerk vorhanden ist.
    2. Klicken Sie auf das Zahnradsymbol in der oberen rechten Ecke und wählen Sie »Drucker«.
    3. Wenn der Drucker noch nicht in der Liste aufgeführt ist, klicken Sie auf »Hinzufügen«.
    4. Der Drucker sollte erkannt und ordentliche Einstellungen angezeigt werden.
    5. Drucken Sie eine Testseite.
ÜBERPRÜFUNG:
    War es möglich, eine Testseite mit dem Netzwerkdrucker zu drucken? ZWECK:
    Dieser Test überprüft, ob Systemzeit und -datum stimmen, die in der Uhr der Arbeitsumgebung angezeigt werden.
DURCHFÜHRUNG:
    1. Lesen Sie die Uhrzeit und das Datum der Uhr in der oberen rechten Ecke Ihrer Arbeitsumgebung ab.
ÜBERPRÜFUNG:
    Zeigt die Uhr das korrekte Datum und die richtige Zeit für Ihre Zeitzone? ZWECK:
    Dieser Test überprüft, ob die Arbeitsflächenuhr mit der Systemuhr abgeglichen wird.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen« und stellen Sie sicher, dass sich die Uhr um eine Stunde vor bewegt.
       Hinweis: Es kann eine oder zwei Minuten dauern bis sich die Anzeige der Uhr aktualisiert.
    2. Führen Sie einen Rechtsklick auf die Uhr aus, dann öffnen Sie die »Zeit- & Datumseinstellungen …«.
    3. Vergewissern Sie sich, dass Ihre Uhr auf manuell gestellt ist.
    4. Stellen Sie die Zeit um 1 Stunde zurück.
    5. Schließen Sie das Fenster und führen Sie einen Neustart durch.
ÜBERPRÜFUNG:
    Zeigt Ihre Systemuhr das richtige Datum und die richtige Uhrzeit für Ihre Zeitzone an? ZWECK:
    Dieser Test überprüft, ob Sie Ihr System über das Menü der Arbeitsumgebung neu starten können.
DURCHFÜHRUNG:
    1. Klicken Sie auf das Zahnrad-Symbol in der rechten oberen Ecke der Arbeitsfläche und wählen Sie »Herunterfahren«.
    2. Klicken Sie auf die Schaltfläche »Neustarten« links im Fenster.
    3. Nachdem Sie sich wieder im System angemeldet haben, starten Sie die Systemüberprüfung neu und der Test sollte fortgesetzt werden.
ÜBERPRÜFUNG:
    Wurde Ihr System neu gestartet und der Anmeldebildschirm fehlerfrei angezeigt? ZWECK:
    Dieser Test wird die Möglichkeit Ihres Systems sicherstellen, Audio-Dateien im Ogg-Vorbis-Format wiederzugeben.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um eine Ogg-Vorbis-Datei (.ogg) wiederzugeben.
    2. Bitte schließen Sie die Medienwiedergabe, um fortzufahren.
ÜBERPRÜFUNG:
    Wurde die Audio-Datei korrekt wiedergegeben? ZWECK:
    Dieser Test wird die Möglichkeit Ihres Systems sicherstellen, Audio-Dateien im Wave-Format wiederzugeben.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um eine Wave-Datei (.wav) wiederzugeben.
    2. Bitte schließen Sie die Medienwiedergabe, um fortzufahren.
ÜBERPRÜFUNG:
    Wurde die Audio-Datei korrekt wiedergegeben? ZWECK:
 Ob die Wiederherstellung erfolgreich war, sehen Sie, nachdem »Systemüberprüfung« neu gestartet wurde, nicht »sniff4«.
DURCHFÜHRUNG:
 1. Klicken Sie auf »Ja«.
ÜBERPRÜFUNG:
 Nicht notwendig, da dies ein vorgetäuschter Test ist. ZWECK:
 Simulation eines Fehlers durch Neustarten des Rechners
DURCHFÜHRUNG:
 1. Klicken Sie auf »Testen«, um einen Neustart auszulösen.
 2. Wählen Sie nach dem Neustart und nachdem Sie »Systemüberprüfung« gestartet haben »Fortfahren«.
ÜBERPRÜFUNG:
 Sie sehen keine Benutzerüberprüfung. ZWECK:
 Einige System teilen IPMI nicht mit allen NICs, haben aber ein eigenes Port-Management für die direkte Verbindung zum BMC.  Dieser Test prüft, ob der Port für entfernte IPMI-Verbindungen und -Aktionen genutzt wird.
SCHRITTE:
 1. Vor dem Test sollten Sie den Management Port für das ferngesteuerte Ein/Ausschalten des System konfiguriert haben.
BESTÄTIGUNG:
 Überspringen diesen Test, wenn das System NUR gemeinsame Management/Ethernetports nutzt ODER wenn das System über kein BMC (Management Konsole) verfügt.
 1. Wählen Sie »JA«, wenn Sie zum Ausschalten des Systems IPMI oder den Managementport erfolgreich nutzen konnten.
 2. Wählen Sie »NEIN«, falls Sie versucht haben, das System mittels Port-Management auszuschalten und dies aus irgendeinem Grund scheiterte. ZWECK:
    Dieser Test überprüft, ob »gcalctool« (der Taschenrechner) funktioniert.
DURCHFÜHRUNG:
    Klicken Sie auf »Testen«, um den Taschenrechner zu öffnen und machen Sie Folgendes:
     1. Ausschneiden.
     2. Kopieren.
     3. Einfügen.
ÜBERPRÜFUNG:
    Wurden die Funktionen erwartungsgemäß ausgeführt? ZWECK:
    Dieser Test überprüft, ob »gcalctool« (der Taschenrechner) funktioniert.
DURCHFÜHRUNG:
    Klicken Sie auf »Testen«, um den Taschenrechner zu öffnen und machen Sie Folgendes:
     1. Etwas im Speicher ablegen.
     2. Speicher zurücksetzen.
     3. Letzten Speichereintrag löschen.
     4. Gesamten Speicher löschen.
ÜBERPRÜFUNG:
    Wurden die Funktionen erwartungsgemäß ausgeführt? ZWECK:
    Dieser Test überprüft, ob »gcalctool« (der Taschenrechner) funktioniert.
DURCHFÜHRUNG:
    Klicken Sie auf »Testen«, um den Taschenrechner zu öffnen und machen Sie Folgendes:
     1. Einfache Arithmetik (+, -, /, *)
     2. Berechnungen mit Klammern.
     3. Berechnungen mit Brüchen.
     4. Berechnungen mit Dezimalzahlen.
ÜBERPRÜFUNG:
    Wurden die Funktionen erwartungsgemäß ausgeführt? ZWECK:
    Dieser Test prüft, ob »gcalctool« (der Taschenrechner) funktioniert.
DURCHFÜHRUNG:
    Klicken Sie auf »Testen«, um den Taschenrechner zu öffnen.
ÜBERPRÜFUNG:
    Wurde er einwandfrei gestartet? ZWECK:
    Dieser Test überprüft, ob »gedit« (der Texteditor) funktioniert.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um den Texteditor zu öffnen, und öffnen Sie die zuvor erstellte Datei.
    2. Bearbeiten und speichern Sie die Datei, dann schließen Sie den Texteditor.
ÜBERPRÜFUNG:
    Konnte dieser Vorgang erfolgreich ausgeführt werden? ZWECK:
    Dieser Test überprüft, ob »gedit« (der Texteditor) funktioniert.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um den Texteditor zu öffnen.
    2. Geben Sie beliebigen Text ein und speichern Sie die Datei (merken Sie sich den Dateinamen), dann schließen Sie den Texteditor.
ÜBERPRÜFUNG:
    Konnte dieser Vorgang erfolgreich ausgeführt werden? ZWECK:
    Dieser Test überprüft, ob das Sofortnachrichtenprogramm Empathy funktioniert.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um Empathy zu öffnen.
    2. Konfigurieren Sie Empathy, sodass es eine Verbindung zu AOL Instant Messaging (AIM) herstellt.
    3. Schließen Sie Empathy, wenn Sie den Test abgeschlossen haben.
ÜBERPRÜFUNG:
     Wurde die Verbindung erfolgreich hergestellt und konnten Sie Nachrichten senden und empfangen? ZWECK:
    Dieser Test überprüft, ob das Sofortnachrichtenprogramm Empathy funktioniert.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um Empathy zu öffnen.
    2. Konfigurieren Sie Empathy, sodass es eine Verbindung zu Facebook-Chat herstellt.
    3. Schließen Sie Empathy, wenn Sie den Test abgeschlossen haben.
ÜBERPRÜFUNG:
     Wurde die Verbindung erfolgreich hergestellt und konnten Sie Nachrichten senden und empfangen? ZWECK:
    Dieser Test überprüft, ob das Sofortnachrichtenprogramm Empathy funktioniert.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um Empathy zu öffnen.
    2. Konfigurieren Sie Empathy, sodass es eine Verbindung zu Google Talk (gtalk) herstellt.
    3. Schließen Sie Empathy, wenn Sie den Test abgeschlossen haben.
ÜBERPRÜFUNG:
     Wurde die Verbindung erfolgreich hergestellt und konnten Sie Nachrichten senden und empfangen? ZWECK:
    Dieser Test überprüft, ob das Sofortnachrichtenprogramm Empathy funktioniert.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um Empathy zu öffnen.
    2. Konfigurieren Sie Empathy, sodass es eine Verbindung zu einem Jabber-Server herstellt.
    3. Schließen Sie Empathy, wenn Sie den Test abgeschlossen haben.
ÜBERPRÜFUNG:
    Wurde die Verbindung erfolgreich hergestellt und konnten Sie Nachrichten senden und empfangen? ZWECK:
    Dieser Test überprüft, ob das Sofortnachrichtenprogramm Empathy funktioniert.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um Empathy zu öffnen.
    2. Konfigurieren Sie Empathy, sodass es eine Verbindung zu Microsoft Network (MSN) herstellt.
    3. Schließen Sie Empathy, wenn Sie den Test abgeschlossen haben.
ÜBERPRÜFUNG:
     Wurde die Verbindung erfolgreich hergestellt und konnten Sie Nachrichten senden und empfangen? ZWECK:
    Dieser Test überprüft, ob Evolution funktioniert.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um Evolution zu öffnen.
    2. Konfigurieren Sie Evolution, sodass es eine Verbindung mit einem IMAP-Konto herstellt.
ÜBERPRÜFUNG:
    Konnten Sie E-Mails korrekt empfangen und lesen? ZWECK:
    Dieser Test überprüft, ob Evolution funktioniert.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um Evolution zu öffnen.
    2. Konfigurieren Sie Evolution, sodass es eine Verbindung zu einem POP3-Konto herstellt.
ÜBERPRÜFUNG:
    Konnten Sie E-Mails korrekt empfangen und lesen? ZWECK:
    Dieser Test überprüft, ob Evolution funktioniert.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um Evolution zu öffnen.
    2. Konfigurieren Sie Evolution, sodass es eine Verbindung zu einem SMTP-Konto herstellt.
ÜBERPRÜFUNG:
    Konnten Sie eine E-Mail versenden ohne Fehlermeldungen zu erhalten? ZWECK:
    Dieser Test überprüft, ob Firefox ein einfaches Flash-Video wiedergeben kann. Hinweis: Dies kann die Installation zusätzlicher Anwendungen erfordern, um erfolgreich abgeschlossen zu werden.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um Firefox zu starten und ein kurzes Flash-Video anzuzeigen.
ÜBERPRÜFUNG:
    Wurde das Video korrekt wiedergegeben? ZWECK:
    Dieser Test überprüft, ob Firefox QuickTime-Videos (.mov) wiedergeben kann. Hinweis: Dies kann die Installation zusätzlicher Anwendungen erfordern, um erfolgreich abgeschlossen zu werden.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um Firefox zu starten und ein Beispiel-Video anzuzeigen.
ÜBERPRÜFUNG:
    Wurde das Video mithilfe einer Erweiterung wiedergegeben? ZWECK:
    Dieser Test überprüft, ob Firefox eine einfache Webseite darstellen kann.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um Firefox zu starten und sich die Test-Webseite anzusehen.
ÜBERPRÜFUNG:
    Wurde die Ubuntu-Testseite korrekt geladen? ZWECK:
    Dieser Test überprüft, ob Firefox ein Java-Applet in einer Webseite ausführen kann.
    Hinweis: Dies kann die Installation zusätzlicher Anwendungen erfordern, um erfolgreich abgeschlossen zu werden.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um Firefox mit der Java-Testseite zu öffnen, folgen Sie den Anweisungen dort.
ÜBERPRÜFUNG:
    Wurde das Applet angezeigt? ZWECK:
    Dieser Test überprüft, ob Firefox Flash-Anwendungen ausführen kann. Hinweis: Dies kann die Installation zusätzlicher Anwendungen erfordern, um erfolgreich abgeschlossen zu werden.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um Firefox zu starten und einen einfachen Flash-Test anzuzeigen.
ÜBERPRÜFUNG:
    Haben Sie den Text gesehen? ZWECK:
    Dieser Test überprüft, ob GNOME Terminal funktioniert.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um ein Terminal-Fenster zu öffnen.
    2. Geben Sie »ls« ein und bestätigen Sie mit der Eingabetaste. Sie sollten eine Liste der Dateien und Ordner in Ihrem persönlichen Ordner sehen.
    3. Schließen Sie das Terminal-Fenster.
ÜBERPRÜFUNG:
    Konnte dieser Vorgang erfolgreich ausgeführt werden? ZWECK:
    Dieser Test überprüft, ob die Dateiverwaltung eine Datei kopieren kann
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um die Dateiverwaltung zu öffnen.
    2. Rechtsklicken Sie auf die Datei »Testdatei 1« und wählen Sie »Kopieren«.
    3. Rechtsklicken Sie in den freien, weißen Raum und wählen Sie »Einfügen«.
    4. Rechtsklicken Sie auf die Datei namens »Testdatei 1 (Kopie)« und wählen Sie »Umbenennen«.
    5. Geben Sie »Testdatei 2« als Dateinamen ein.
    6. Schließen Sie die Dateiverwaltung.
ÜBERPRÜFUNG:
    Haben Sie nun eine Datei namens »Testdatei 2«? ZWECK:
     Dieser Test überprüft, ob die Dateiverwaltung einen Ordner kopieren kann.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um die Dateiverwaltung zu öffnen.
    2. Rechtsklicken Sie auf den Ordner namens »Testordner« und wählen Sie »Kopieren«.
    3. Rechtsklicken Sie auf eine beliebige Stelle der weißen Fläche im Fenster und wählen Sie »Einfügen«.
    4. Rechtsklicken Sie auf den Order mit dem Namen »Testordner (Kopie)« und wählen Sie »Umbenennen«.
    5. Geben Sie den Namen »Testdaten« im Namensfeld für den neuen Ordner ein und drücken Sie die Eingabetaste.
    6. Schließen Sie die Dateiverwaltung.
ÜBERPRÜFUNG:
    Ist nun der Ordner mit dem Namen »Testdaten« vorhanden? ZWECK:
    Dieser Test überprüft, ob die Dateiverwaltung eine neue Datei erstellen kann.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um die Dateiverwaltung zu öffnen.
    2. Rechtsklicken Sie auf den freien, weißen Raum und wählen Sie »Neues Dokument anlegen → Leeres Dokument«
    3. Geben Sie »Testdatei 1« als Dateinamen ein und drücken Sie die Eingabetaste.
    4. Schließen Sie die Dateiverwaltung.
ÜBERPRÜFUNG:
    Wurde eine Datei namens »Testdatei 1« erstellt? ZWECK:
    Dieser Test überprüft, ob die Dateiverwaltung in der Lage ist, einen neuen Ordner zu erstellen.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um ein neues Fenster der Dateiverwaltung zu öffnen.
    2. Klicken Sie in der Menüleiste auf »Datei → Ordner anlegen«.
    3. Geben Sie in das Namensfeld des neuen Ordners den Namen »Testordner« ein und bestätigen Sie mit der Eingabetaste.
    4. Schließen Sie die Dateiverwaltung.
ÜBERPRÜFUNG:
    Ist nun der Ordner mit dem Namen »Testordner« vorhanden? ZWECK:
    Dieser Test überprüft, ob die Dateiverwaltung eine Datei löschen kann.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um die Dateiverwaltung zu öffnen.
    2. Rechtsklicken Sie auf die Datei namens »Testdatei 1« und wählen Sie »In den Müll verschieben«.
    3. Überprüfen Sie, ob »Testdatei 1« gelöscht wurde.
    4. Schließen Sie die Dateiverwaltung.
ÜBERPRÜFUNG:
     Wurde »Testdatei 1« erfolgreich gelöscht? ZWECK:
    Dieser Test überprüft, ob die Dateiverwaltung einen Ordner löschen kann.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um die Dateiverwaltung zu öffnen.
    2. Rechtsklicken Sie auf den Ordner namens »Testordner« und wählen Sie »In den Müll verschieben«.
    3. Überprüfen Sie, ob »Testordner« gelöscht wurde.
    4. Schließen Sie die Dateiverwaltung.
ÜBERPRÜFUNG:
     Wurde »Testordner« erfolgreich gelöscht? ZWECK:
    Dieser Test überprüft, ob die Dateiverwaltung eine Datei verschieben kann.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um die Dateiverwaltung zu öffnen.
    2. Klicken und ziehen Sie die Datei namens »Testdatei 2« auf das Symbol des Ordners »Testdaten«.
    3. Doppelklicken Sie auf das Symbol, um den Ordner zu öffnen.
    4. Schließen Sie die Dateiverwaltung.
ÜBERPRÜFUNG:
    Wurde die Datei namens »Testdatei 2« in den Ordner »Testdaten« verschoben? ZWECK:
    Dieser Test überprüft, ob die Aktualisierungsverwaltung Aktualisierungen finden kann.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um die Aktualisierungsverwaltung zu starten.
    2. Folgen Sie den Anweisungen und installieren Sie Aktualisierungen, falls vorhanden.
    3. Wenn der Vorgang abgeschlossen ist, schließen Sie die Anwendung mithilfe der Schaltfläche »Schließen« in der rechten unteren Ecke.
ÜBERPRÜFUNG:
    Hat die Aktualisierungsverwaltung Aktualisierungen gefunden und installiert? (Gilt als »bestanden«, falls keine Aktualisierungen gefunden wurden, gilt aber als »gescheitert«, falls Aktualisierungen gefunden, jedoch nicht installiert werden konnten.) ZWECK:
    Dieser Test überprüft die Fähigkeit Ihres Rechners, sich auszuschalten und das System zu starten.
DURCHFÜHRUNG:
    1. Wählen Sie »Testen«, um zu beginnen.
    2. Der Rechner wird heruntergefahren.
    3. Schalten Sie den Rechner wieder an.
    4. Warten Sie nach dem Neustart, bis die Testanzeige Sie informiert, dass der Test beendet wurde.
    5. Wenn der Test beendet wurde, starten Sie »Systemüberprüfung« neu und wählen Sie »Wiederholen« aus, wenn es angezeigt wird.
ÜBERPRÜFUNG:
    Wenn der Rechner erfolgreich heruntergefahren und wieder gestartet wurde, wählen Sie »Ja«, ansonsten wählen Sie »Nein«. ZWECK:
    Dieser Test überprüft die Fähigkeit des Systems fehlerfrei neuzustarten.
DURCHFÜHRUNG:
    1. Um zu beginnen, wählen Sie »Testen« aus.
    2. Der Rechner wird neu starten.
    3. Warten Sie nach dem Neustart bis die Anzeige des Tests Sie informiert, dass der Test beendet wurde.
    4. Sobald der Test beendet wurde, starten Sie »Systemüberprüfung« neu und wählen sie »Wiederholen« aus, wenn es angezeigt wird.
ÜBERPRÜFUNG:
    Wenn der Rechner erfolgreich neu gestartet wurde, wählen sie »Ja« und dann »Weiter« aus. ZWECK:
    Dieser Test überprüft, ob die Dateiverwaltung einen Ordner verschieben kann.
DURCHFÜHRUNG:
    1. Klicken Sie auf »Testen«, um die Dateiverwaltung zu öffnen.
    2. Klicken und ziehen Sie den Ordner namens »Testdaten« auf das Symbol »Testordner«.
    3. Doppelklicken Sie auf den »Testordner«, um ihn zu öffnen.
    4. Schließen Sie die Dateiverwaltung.
ÜBERPRÜFUNG:
    Wurde der Ordner »Testdaten« erfolgreich in den Ordner »Testordner« verschoben? ZWECK:
 Um Dinge herauszufinden
DURCHFÜHRUNG:
 1. Klicken Sie auf »Ja«.
ÜBERPRÜFUNG:
 Nicht notwendig, da dies ein vorgetäuschter Test ist. Verifizierungstest für die Menüleistenuhr Test zur Überprüfung des Neustarts Das Protokoll Xorg.0.log wird ausgewertet und der aktive X-Treiber sowie die Version festgestellt. Tests für Peripheriegeräte Piglit-Tests Ping-Befehl mit »ubuntu.com« ausführen und die Netzwerkschnittstelle 100 mal neu starten. Einen Ton auf der Standardausgabe wiedergeben und diesen über die Standardeingabe wieder aufnehmen. Bitte wählen Sie (%s):  Bitte drücken Sie jede Taste auf Ihrer Tastatur. Bitte schreiben Sie hier und drücken Sie Strg+D, wenn Sie fertig sind:
 Zeigegeräte-Tests. Energieverwaltungstest Energieverwaltungstests Beliebige Taste drücken, um fortzufahren … Zurück Versionsinformationen ausgeben und beenden. Informationen über an das System angeschlossene Bildschirme abrufen Informationen zu Netzwerkgeräten abrufen Abbruch durch die Tastatur Notieren Sie sich die Einstellungen der Lautstärkeregler vor dem in Bereitschaft Versetzen. Notieren Sie sich die aktuelle Netzwerkverbindung vor dem in Bereitschaft Versetzen. Notieren Sie sich die aktuelle Bildschirmauflösung vor dem in Bereitschaft Versetzen. Rendercheck-Tests Wiederholen Neustarten Gibt den Namen, den Treibernamen und die Treiberversion von jedem erkannten Touchpad auf diesem System wieder. »Cachebench Read / Modify / Write«-Leistungsprüfung ausführen »Cachebench Read«-Leistungsprüfung ausführen »Cachebench Write«-Leistungsprüfung ausführen »Compress 7ZIP«-Leistungsprüfung ausführen »Compress PBZIP2«-Leistungstest ausführen »Encode MP3«-Leistungstest ausführen Ausführen der automatisierten Tests der Firmware-Test-Suite (fwts). »GLmark2«-Leistungsprüfung ausführen »GLmark2-ES2«-Leistungsprüfung ausführen Führt »GiMark« aus, einen Geometrie-instanzierenden Test (OpenGL 3.3) Vollbild 1920x1080 ohne Anti-Aliasing. Führt »GiMark« aus, einen Geometrie-instanzierenden Test (OpenGL 3.3) im Fenstermodus 1024x640 ohne Anti-Aliasing. »GnuPG«-Leistungstest auführen »Himeno«-Leistungsprüfung ausführen »Lightsmark«-Leistungsprüfung ausführen »N-Queens«-Leistungsprüfung ausführen »Network Loopback«-Leistungsprüfung ausführen »Qgears2 OpenGL gearsfancy«-Leistungsprüfung ausführen »Qgears2 OpenGL image scaling«-Leistungsprüfung »Qgears2 XRender Extension gearsfancy«-Leistungsprüfung ausführen »Qgears2 XRender Extension image scaling«-Leistungsprüfung ausführen »Render-Bench XRender/Imlib2«-Leistungsprüfung ausführen »Stream Add«-Leistungsprüfung ausführen »Stream Copy«-Leistungsprüfung ausführen »Stream Scale«-Leistungsprüfung ausführen »Stream Triad«-Leistungsprüfung ausführen »Unigine Heaven«-Leistungsprüfung ausführen »Unigine Santuary«-Leistungsprüfung ausführen »Unigine Tropics«-Leistungsprüfung ausführen Führt einen Stresstest basierend auf FurMark (OpenGL 2.1 oder 3.2) Vollbild 1920x1080 ohne Anti-Aliasing aus. Führt einen Stresstest basierend auf FurMark (OpenGL 2.1 oder 3.2) im Fenstermodus 1024x640 ohne Anti-Aliasing aus. Führt einen Mosaik-Test basierend auf TessMark (OpenGL 4.0) Vollbild 1920x1080 ohne Anti-Aliasing aus. Führt einen Mosaik-Test basierend auf TessMark (OpenGL 4.0) im Fenstermodus 1080x640 ohne Anti-Aliasing aus. Führt Klecks-Leistungstest aus Führen Sie »gtkperf« aus, um sicherzustellen, dass GTK-basierte Testprogramme funktionieren. Einen Grafik‐Stresstest ausführen. Dieser Test kann ein paar Minuten dauern. »x264 H.264/AVC encoder«-Leistungstest ausführen %s wird ausgeführt … Einen Test durchführen, der drei mal 100 10-MB‐Dateien auf eine SDHC‐Karte überträgt. Einen Test durchführen, der drei mal 100 10-MB‐Dateien auf einen USB‐Stick überträgt. Führt alle »rendercheck«-Testfolgen aus. Dieser Test kann einige Minuten dauern. Prüft mit »piglit« die Unterstützung für OpenGL 2.1 Überprüft mit »piglit« die Unterstützung für GLSL-Fragment-Shader-Operationen. Überprüft mit »piglig« die Unterstützung für GLSL-Vertex-Shader-Operationen. Überprüft mit »piglit« die Unterstützung für Einzelbildpuffer-Objektoperationen, Z-Puffer sowie Stencil-Puffer. Prüft mit »piglit« die Unterstützung für Textur über »pixmap«. Prüft mit »piglit« die Unterstützung für Vertex-Puffer-Objekt(VBO)-Operationen. Überprüft mit »piglit« die Unterstützung für Stencil-Pufferoperationen. Fasst die Ergebnisse der »piglit«-Überprüfungen zusammen. SATA/IDE-Geräteinformationen. SMART-Test SYSTEMÜBERPRÜFUNG: Bitte geben Sie Ihr Passwort ein. Einige Überprüfungen benötigen Systemverwaltungsrechte um korrekt zu funktionieren. Ihr Passwort wird zu keinem Zeitpunkt gespeichert und nicht mit den Testergebnissen übertragen. Alles auswählen Alle auswählen Serverdiensteüberprüfung Kürzel für --config=.*/jobs_info/blacklist. Kürzel für --config=.*/jobs_info/blacklist_file. Kürzel für --config=.*/jobs_info/whitelist. Kürzel für --config=.*/jobs_info/whitelist_file. Überspringen Rauch-Überprüfungen Sniffer testen Anwendungsinstallations-Test Einige neue Festplatten parken die Lese-/Schreibköpfe nach einer kurzen Zeit der Inaktivität. Dies ist eine Stromsparmaßnahme, die sich jedoch mit dem Betriebssystem nicht gut vertragen könnte, weil die Festplatte ständig geparkt und aus der Parkposition wieder aktiviert wird. Dadurch erhöht sich der Verschleiß der Festplatte, was möglicherweise zu baldigen Fehlern führt. Freier Speicherplatz nach Abschluss Tests starten Status Prozess stoppen Stop in tty eingegeben Stresstest zum Ausschalten des Systems (100 mal) Stresstest zum Neustarten des Systems (100 mal) Stresstests Details zur Übermittlung Ergebnisse übermitteln An HEXR übertragen Test erfolgreich abgeschlossen Bereitschaftstest Bereitschaftstests System-
Überprüfung Systemhintergrunddienste-Test Systemüberprüfung Reiter 1 Reiter 2 Abbruch-Signal Testen ACPI-Aufwach-Signal testen (»fwts wakealarm«) Erneut testen Speicher überprüfen und beschreiben. Test abgebrochen Überprüfung des Takt-Jitters. Überprüfen, ob der »at«-Dienst ausgeführt wird, sofern das Paket installiert ist. Überprüfen, ob der »cron«-Dienst ausgeführt wird, sofern das Paket installiert ist. Überprüfen, ob der »cups«-Dienst ausgeführt wird, sofern das Paket installiert ist. Überprüfen, ob der »getty«-Dienst ausgeführt wird, sofern das Paket installiert ist. Überprüfen, ob der »init«-Dienst ausgeführt wird, sofern das Paket installiert ist. Überprüfen, ob der »klog«-Dienst ausgeführt wird, sofern das Paket installiert ist. Überprüfen, ob der »nmb«-Dienst ausgeführt wird, sofern das Paket installiert ist. Überprüfen, ob der »smb«-Dienst ausgeführt wird, sofern das Paket installiert ist. Überprüfen, ob der »syslog«-Dienst ausgeführt wird, sofern das Paket installiert ist. Überprüfen, ob der »udev«-Dienst ausgeführt wird, sofern das Paket installiert ist. Überprüfen, ob der »winbind«-Dienst ausgeführt wird, sofern das Paket installiert ist. Test unterbrochen Das Ausschalten von CPUs bei Mehrkernsystemen testen. Dieser Test überprüft, ob der Ordner /var/crash leer ist. Er listet alle darin enthalten Dateien auf, falls vorhanden, oder liefert den Status des Ordners zurück (»existiert nicht«/»ist leer«). Dieser Test überprüft, dass der X-Prozess nicht im abgesicherten Modus ausgeführt wird. Dieser Test überprüft, ob der X-Prozess ausgeführt wird. Die CPU-Skalierungsfunktionen mit der Firmware-Test-Suite (fwts cpufreq) überprüfen. Netzwerk nach Wiedereinschalten prüfen. Test zur Überprüfung, das ein Xen-domU-Abbild gestartet und auf Xen unter Ubuntu ausgeführt werden kann. Dieser Test überprüft, ob ein Cloud-Abbild gestartet werden kann und korrekt mit KVM funktioniert. Dieser Test überprüft, ob Virtualisierung unterstützt wird und ob das System die Mindestmenge an erforderlichem Arbeitsspeicher (RAM) aufweist, um als ein OpenStack-Rechnerknoten dienen zu können. Test zur Erkennung von Audio-Geräten Test zur Erkennung der verfügbaren Netzwerksteuergeräte Test zur Erkennung der optischen Laufwerke Dieser Test überprüft, ob dieses System Hardware-beschleunigte KVM-Virtuelle-Maschinen ausführen kann. Test zur Anzeige der Xorg-Version Test zur Überprüfung des Uhrzeitabgleichs mit einem NTP-Server Testen, ob nach Wiedereinschalten dieselbe Auflösung wie vorher eingestellt ist. Test zur Überprüfung, das Xen Hypervisor ausgeführt wird. Testen Sie Ihr System und übermitteln Sie Ihre Ergebnise an Launchpad Getestet Testet »oem-config« mit »Xpresser« und überprüft dann, ob der Benutzer erfolgreich erstellt wurde. Der eben angelegte Benutzer wird gelöscht, sobald der Test erfolgreich abgeschlossen wurde. Überprüft, ob sich die Funknetzwerk-Hardware des Systems nach dem Aufwecken aus der Bereitschaft mit einem Router, der WPA-Sicherheit und die Protokolle 802.11b/g verwendet, verbinden kann. Dieser Test überprüft, ob sich die Funknetzwerk-Hardware des Systems mit einem Router, der WPA-Sicherheit und die Protokolle 802.11b/g verwendet, verbinden kann. Überprüft, ob sich die Funknetzwerk-Hardware des Systems nach dem Aufwecken aus der Bereitschaft mit einem Router, der WPA-Sicherheit und das Protokoll 802.11n verwendet, verbinden kann. Dieser Test überprüft, ob sich die Funknetzwerk-Hardware des Systems mit einem Router verbinden kann, der WPA-Sicherheit und das Protokoll 802.11n verwendet Überprüft, ob sich die Funknetzwerk-Hardware Ihres Systems nach dem Aufwecken aus der Bereitschaft mit einem Router verbinden kann, der keine Verschlüsselung und die Protokolle 802.11b/g verwendet. Dieser Test überprüft, ob sich die Funknetzwerk-Hardware Ihres Systems mit einem Router verbinden kann, der keine Verschlüsselung und die Protokolle 802.11b/g verwendet. Überprüft, ob sich die Funknetzwerk-Hardware Ihres Systems nach dem Aufwecken aus der Bereitschaft mit einem Router verbinden kann, der keine Verschlüsselung und das Protokoll 802.11n verwendet. Dieser Test überprüft, ob sich die Funknetzwerk-Hardware Ihres Systems mit einem Router verbinden kann, der keine Verschlüsselung und das Protokoll 802.11n verwendet. Dieser Test überprüft die Performance der Funknetzwerkverbindung eines Systems mit dem Werkzeug »iperf« mithilfe von UDP-Paketen. Dieser Test überprüft die Performance der Funknetzwerkverbindung eines Systems mit dem Werkzeug »iperf«. Tests, um zu überprüfen, ob »apt« auf die Paketquellen zugreifen und Aktualisierungen herunterladen kann (installiert keine Aktualisierungen). Dies geschieht, um sicherzustellen, dass Sie Ihr System nach einer unvollständigen oder fehlgeschlagenen Aktualisierung wiederherstellen können. Überprüft, ob das System über eine funktionierende Internetverbindung verfügt. TextLabel Die Datei, in welche das Protokoll gespeichert wird. Der folgende Bericht wurde zum Senden an die Launchpad-Hardware-Datenbank erstellt:

  [[%s|Bericht anzeigen]]

Sie können diese Informationen zu Ihrem System senden, indem Sie die E-Mail-Adresse angeben, mit der Sie bei Launchpad angemeldet sind. Falls Sie noch kein Launchpad-Konto besitzen, registrieren Sie sich bitte hier:

  https://launchpad.net/+login Der erstellte Bericht scheint Fehler zu enthalten. Deshalb
könnte es sein, dass er nicht von Launchpad verarbeitet wird. Eine weitere Instanz der Systemüberprüfung läuft bereits. Bitte schließen Sie diese zunächst. Dieser automatische Test versucht eine Kamera zu erkennen. Dies legt dem Bericht die Bildschirmfotos vom Test »suspend/cycle_resolutions_after_suspend_auto« als Anlage bei. Dies ist eine vollständig automatisierte Version von »mediacard/sd-automated« und geht davon aus, dass an das System vor der Ausführung der Systemüberprüfung ein Speicherkartengerät angeschlossen ist. Sie wird für SRU-automatisierte Tests verwendet. Dies ist ein automatisierter Bluetooth-Datenübertragungstest. Dieser sendet ein Bild an das Gerät, welches durch die Umgebungsvariable »BTDEVADDR« festgelegt ist. Dies ist ein automatisierter Bluetooth-Test. Es wird das Durchsuchen des Dateisystems eines entfernten Gerätes simuliert, das in der Umgebungsvariable »BTDEVADDR« angegeben ist. Dies ist ein automatisierter Bluetooth-Test. Es wird eine Dateiübertragung von einem entfernten Gerät angenommen, das in der Umgebungsvariable »BTDEVADDR« angegeben ist. Dies ist ein automatisierter Test, um Informationen über den gegenwärtigen Zustand Ihrer Netzwerkgeräte zusammenzutragen. Wenn keine Geräte gefunden werden, wird der Test mit einem Fehler verlassen. Dies ist eine automatisierte Überprüfung, die Lese-/Schreibvorgänge auf einer angeschlossenen FireWire-Festplatte ausführt. Dies ist ein automatischer Test, der Lese‐/Schreiboperationen auf einer angeschlossenen eSATA-Festplatte durchführt. Dies ist eine automatisierte Variante des Tests »usb/storage-automated« und geht davon aus, dass an den Server USB-Speichermedien angeschlossen wurden, bevor die Systemüberprüfung ausgeführt wurde. Der Test ist für Server gedacht und automatisches SRU-Testen. Dies ist eine automatisierte Version des Tests »usb3/storage-automated« und geht davon aus, dass an den Server vor der Ausführung der Systemüberprüfung USB-3.0-Speichergeräte angeschlossen sind. Sie ist zur Nutzung mit Servern und zum SRU-automatisierten Testen konzipiert. Dies ist die automatisierte Version von »suspend/suspend_advanced«. Dieser Test prüft die CPU-Topologie auf Genauigkeit. Dieser Test überprüft, ob die eingestellten CPU-Leistungsprofile ordnungsgemäß angewandt werden. Dieser Test überprüft, ob die Funknetzwerkschnittstellen nach dem Aufwecken aus der Bereitschaft korrekt arbeiten. Er unterbricht die Verbindung aller Geräte und verbindet sich dann mit dem Funknetzwerkgerät, um zu prüfen ob die Verbindung wie erwartet funktioniert. Dieser Test vergleicht die von »meminfo« gefundene Menge an Arbeitsspeicher mit der Größe der über »DMI« erkannten Speichermodule. Dieser Test unterbricht alle Verbindungen und verbindet sich dann mit dem Funknetzwerk. Anschließend wird die Verbindung geprüft, um sicherzustellen, dass alles wie erwartet funktioniert. Dieser Test liest die Hardware-Adresse des Bluetooth-Adapters nach der Bereitschaft aus und vergleicht sie mit der Adresse, die vor der Bereitschaft ausgelesen wurde. Dieser Test ist automatisiert und wird nach dem Test »mediacard/cf-insert« ausgeführt. Er überprüft das Lesen und Schreiben auf einer CF-Karte. Dieser Test ist automatisiert und wird nach dem Test »mediacard/cf-insert-after-suspend« ausgeführt. Er überprüft das Lesen und Schreiben auf einer CF-Karte, nachdem das System in Bereitschaft versetzt worden ist. Dieser Test wurde automatisiert und wird nach dem Test »mediacard/mmc-insert« ausgeführt. Er überprüft das Lesen und Schreiben auf einer MMC-Karte. Dieser Test wurde automatisiert und wird nach dem Test »mediacard/mmc-insert-after-suspend« ausgeführt. Er überprüft das Lesen und Schreiben auf einer MMC-Karte, nachdem das System in Bereitschaft versetzt worden ist. Dieser Test ist automatisiert und wird ausgeführt, nachdem der Test des Einsetzens der MS-Speicherkarte beendet wurde. Es wird das Schreiben und Lesen auf der MS-Speicherkarte getestet. Dieser Test ist automatisiert und wird ausgeführt, nachdem der Test des Einsetzens der MS-Speicherkarte im Bereitschaftsmodus beendet wurde. Es wird das Schreiben und Lesen auf der MS-Speicherkarte getestet, nachdem das System in Bereitschaft versetzt wurde. Dieser Test ist automatisiert und wird ausgeführt, nachdem der Test des Einsetzens der MSP-Speicherkarte beendet wurde. Es wird das Schreiben und Lesen auf der MSP-Speicherkarte getestet. Dieser Test ist automatisiert und wird ausgeführt, nachdem der Test des Einsetzens der MSP-Speicherkarte im Bereitschaftsmodus beendet wurde. Es wird das Schreiben und Lesen auf der MSP-Speicherkarte getestet, nachdem das System in Bereitschaft versetzt wurde. Dieser Test wurde automatisiert und wird nach dem Test »mediacard/sd-insert« ausgeführt. Er überprüft das Lesen und Schreiben auf einer SD-Karte. Dieser Test ist automatisiert und wird nach dem Test »mediacard/sd-insert-after-suspend« ausgeführt. Er überprüft das Lesen und Schreiben auf einer SD-Karte, nachdem das System in Bereitschaft versetzt worden ist. Dieser Test ist automatisiert und wird nach dem Test »mediacard/sdhc-insert« ausgeführt. Er überprüft das Lesen und Schreiben auf einer SDHC-Karte. Dieser Test ist automatisiert und wird nach dem Test »mediacard/sdhc-insert-after-suspend« ausgeführt. Er überprüft das Lesen und Schreiben auf einer SDHC-Karte, nachdem das System in Bereitschaft versetzt worden ist. Dieser Test ist automatisiert und wird ausgeführt, nachdem der Test des Einsetzens der SDXC-Speicherkarte beendet wurde. Es wird das Schreiben und Lesen auf der SDXC-Speicherkarte getestet. Dieser Test ist automatisiert und wird ausgeführt, nachdem der Test des Einsetzens der SDXC-Speicherkarte im Bereitschaftsmodus beendet wurde. Es wird das Schreiben und Lesen auf der SDXC-Speicherkarte getestet, nachdem das System in Bereitschaft versetzt wurde. Dieser Test ist automatisiert und wird ausgeführt, nachdem der Test des Einsetzens der xD-Speicherkarte beendet wurde. Es wird das Schreiben und Lesen auf der xD-Speicherkarte getestet. Dieser Test ist automatisiert und wird ausgeführt, nachdem der Test des Einsetzens der xD-Speicherkarte im Bereitschaftsmodus beendet wurde. Es wird das Schreiben und Lesen auf der xD-Speicherkarte getestet, nachdem das System in Bereitschaft versetzt wurde. Dieser Test startet automatisch nachdem der Test »suspend/usb3_insert_after_suspend« abgeschlossen ist. Dieser Test startet automatisch nachdem der Test »suspend/usb_insert_after_suspend« abgeschlossen ist. Diese automatisierte Überprüfung wird nach dem Test »USB/insert« ausgeführt. Dies ist ein automatisierter Test, der nach dem Test »usb3/insert« ausgeführt wird. Dieser Test prüft, ob unterstützte Video-Modi nach dem Aufwecken aus der Bereitschaft funktionieren. Dies geschieht automatisch, indem Bildschirmfotos aufgenommen werden und diese als Anhang hochgeladen werden. Dieser Test überprüft, ob sich die Lautstärkeeinstellungen für Ihr lokales System in einem akzeptablen Bereich befinden. Der Test überprüft für alle Audio-Quellen (Eingaben) und Audio-Sinks (Ausgaben), die von PulseAudio erkannt wurden, ob die jeweilige Lautstärke größer oder gleich »minvol« und kleiner oder gleich »maxvol« ist. Außerdem wird geprüft, ob die aktive Audio-Quelle und das aktive Audio-Sink nicht stumm geschaltet sind. Bevor dieser Test ausgeführt wird, sollten Sie nicht die Lautstärke oder Stummschaltung manuell verändern. Dies legt dem Bericht alle Protokolle des Tests »power-management/poweroff« als Anlage bei. Dies legt dem Bericht alle Protokolle des Tests »power-management/reboot« als Anlage bei. Dies testet, ob Ihre Audio-Geräte nach dem Aufwecken aus der Bereitschaft korrekt funktionieren. Das funktioniert sowohl mit Lautsprechern als auch integrierten Mikrofonen, allerdings ist es am Besten, wenn Sie den Audio-Ausgang mit einem Kabel mit dem Audio-Eingang verbinden. Dies führt einige grundlegende Verbindungstests für BMC aus und stellt sicher, dass IPMI funktioniert. Timer-Signal durch alarm(2) Um dieses Problem zu beheben, schließen Sie die Systemüberprüfung und fügen Sie die fehlenden Abhängigkeiten zur Positivliste hinzu. Tastfeld-Tests Touchscreen-Überprüfungen Es wird versucht einen entfernt angeschlossenen Drucker im Netzwerk zu aktivieren und eine Testseite zu drucken. Text eintippen UNBEKANNT USB-Test USB-Tests Unbekanntes Signal Ungetestet Aufruf: checkbox [OPTIONEN] Benutzeranwendungen Benutzerdefiniertes Signal 1 Benutzerdefiniertes Signal 2 Es wird überprüft, ob die Vektor-Fließkommaeinheit auf dem ARM-Gerät läuft. Stellt sicher, dass der DNS-Server ausgeführt wird und arbeitet. Stellt sicher, dass der Drucker/CUPS-Server ausgeführt wird. Stellt sicher, dass der Samba-Server ausgeführt wird. Stellt sicher, dass der Tomcat-Server ausgeführt wird und arbeitet. Stellt sicher, dass »sshd« ausgeführt wird. Stellt sicher, dass der LAMP-Stack ausgeführt wird (Apache, MySQL und PHP). Überprüft, ob externe USB3-Speichermedien mit der minimal zu erwartenden Geschwindigkeit oder schneller funktionieren. Überprüfen, ob der Systemspeicher mit oder über der Grundleistung arbeitet. Stellen Sie sicher, dass alle Prozessoren nach dem Aufwecken aus dem Ruhezustand in Betrieb sind. Stellen Sie sicher, dass der gesamte Speicher nach dem Aufwecken aus der Bereitschaft zur Verfügung steht. Stellen Sie vor dem Versetzen in Bereitschaft sicher, dass alle Prozessoren in Betrieb sind. Überprüfung der Erreichbarkeit einer Installation von checkbox-server über das Netzwerk mittels SSH. Stellen Sie sicher, dass die Einstellungen der Lautstärkeregler nach dem Aufwecken aus der Bereitschaft dieselben sind, wie vorher. Überprüfen, ob Speichergeräte, wie »Fibre Channel« und RAID erkannt werden und unter Last arbeiten können. Ergebnisse anzeigen Virtualisierungstests Willkommen bei der Systemüberprüfung!

Die Systemüberprüfung stellt Tests bereit, mit denen geprüft werden kann, ob Ihr System ordnungsgemäß funktioniert. Sind die laufenden Tests abgeschlossen, können Sie sich einen Bericht zu Ihrem System ansehen. Die Positivliste ist nicht topologisch geordnet. Funknetzwerktest Funknetzwerktests Test der Funknetzwerksuche. Es wird nach Funknetzwerken gesucht und gefundene Zugriffspunkte werden angezeigt. Beschäftigt Schließen ist auch durch drücken der ESC-Taste oder Strg+C möglich. Auswahl au_fheben _Beenden _Fertigstellen _Nein _Zurück Alles au_swählen _Diesen Test überspringen _Testen Erneut _testen _Ja und auch der Kapazität. Dies legt dem Bericht die Inhalte der verschiedenen »sysctl«-Konfigurationsdateien als Anlage bei. eSATA-Festplatten-Überprüfungen zum vollständigen Entladen und auch die Kapazität. https://help.ubuntu.com/community/Installation/SystemRequirements Installiert das Installationsprogramm-Bootchart-Tar-Archiv, falls vorhanden. Nein Überspringen Testen Erneut testen tty-Eingabe des Hintergrundprozesses tty-Ausgabe des Hintergrundprozesses bis zum vollständigen Entladen und auch die Kapazität. »MOVIE_VAR« muss gesetzt sein. Ja 