��    0      �  C         (     )     9  
   K     V     \     p     x     �     �     �     �  !   �     �     �     �     �     �  "        )     /     8     E     T     `  1   �     �     �     �  
   �     �  	   �     �     �          %     5     E     T     g     �     �  ?   �     �     �     �  p   �     T  �  b     	     	     (	     4	     ;	     Z	  #   c	  	   �	     �	     �	     �	  -   �	     �	     �	     
     
  !   
  >   3
     r
  	   {
     �
     �
     �
  =   �
  [   �
     P     l     t     y     �     �     �  !   �     �  #   �  &         /  '   P  2   x     �     �  S   �                 w   $  "   �     /   +              -         )   %                                   *                        $                            0   ,      "      .   &         '             #             (          	                   !                 
    3d-ish (button) 3d-ish (gradient) Animations Arrow Cell Indicator Size Classic Colorize Scrollbar Contrast Disable focus drawing Dot Edge Thickness Enable Animations on Progressbars Flat Full Glossy Gummy Handlebox Marks If set, button corners are rounded Inset Inverted Inverted Dot Inverted Slash Mark Type 1 Mark Type for Scrollbar Buttons Mark Type for Scrollbar Handles, Handleboxes, etc Menubar Style None Nothing Paned Dots Radius Rectangle Relief Style Requires style Glossy or Gummy Rounded Buttons Scrollbar Color Scrollbar Marks Scrollbar Type Scrollbutton Marks Sets the Color of Scrollbars Shadow Shaped Size of check- and radiobuttons inside treeviews. (Bug #351764) Slash Some Style This option allows to disable the focus drawing. The primary purpose is to create screenshots for documentation. Toolbar Style Project-Id-Version: gtk-engines.HEAD
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 22:41+0000
PO-Revision-Date: 2009-01-09 11:26+0000
Last-Translator: Ricardo Pérez López <ricpelo@gmail.com>
Language-Team: Español <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:46+0000
X-Generator: Launchpad (build 18115)
 3er (botón) 3er (gradiente) Animaciones Flecha Indicador del tamaño de celda Clásico Colorear la barra de desplazamiento Contraste Desactivar el foco de dibujado Punto Grosor del borde Activar animaciones en las barras de progreso Plano Lleno Pulido Pegajoso Marcas de las cajas manipuladoras Si está activado, las esquinas de los botones son redondeadas Insertar Invertido Punto invertido Barra invertida Tipo de marcas 1 Tipo de marcas para los botones de la barra de desplazamiento Tipo de marcas para los manejadores de la barra de desplazamiento, cajas manipuladoras, etc Estilo de la barra de menú Ninguna Nada Puntos de panel Radio Rectángulo Estilo del relieve Requiere estilo Pulido o Pegajoso Botones redondeados Color de la barra de desplazamiento Marcas de las barras de desplazamiento Tipo de barras de desplazamiento Marcas de los botones de desplazamiento Establece el color de las barras de desplazamiento Sombra Contorno Tamaño de los botones de casilla y botones de radio dentro de las vistas de árbol Barra Algunas Estilo Esta opción permite desactivar el foco de dibujado. La propuesta es crear capturas de pantalla para la documentación. Estilo de la barra de herramientas 