��          �      �       H  (   I  '   r     �     �     �  K   �  +     %   3  %   Y  %     %   �  b   �     .  �  ;  -     0   <     m  
   u     �  I   �  8   �  %     &   @  #   g  %   �  u   �     '                          	                     
                  A cover is open on the printer “%s”. A door is open on the printer “%s”. Paused Printers Printing Problem The printer “%s” can’t be used, because required software is missing. The printer “%s” is currently off-line. The printer “%s” is low on paper. The printer “%s” is low on toner. The printer “%s” is out of paper. The printer “%s” is out of toner. You have %d job queued to print on this printer. You have %d jobs queued to print on this printer. _Settings… Project-Id-Version: indicator-printers
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-04 13:07+0000
PO-Revision-Date: 2013-04-06 06:53+0000
Last-Translator: Paco Molinero <paco@byasl.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 19:14+0000
X-Generator: Launchpad (build 18115)
 La tapa de la impresora «%s» está abierta. Una puerta está abierta en la impresora «%s». Pausado Impresoras Problema de impresión La impresora «%s» no se puede usar, porque falta el software necesario. La impresora «%s» está sin conexión en este momento. La impresora «%s» tiene poco papel. La impresora «%s» tiene poco tóner. La impresora «%s» no tiene papel. La impresora «%s»  no tiene tóner. Tiene %d tarea pendiente para imprimir en esta impresora. Tiene %d tareas pendientes para imprimir en esta impresora. _Configuración… 