��          �   %   �      P      Q     r     �  #   �  =   �  @   �     5     B     O     \     l  '   �     �  #   �  1   �  2        A  F   P  $   �  "   �  [   �  :   ;  X   v  S   �  '   #  �  K  ,   �     +     =  %   T  >   z  @   �     �     
	     	     &	  !   8	  2   Z	     �	  (   �	  3   �	  4   �	     (
  a   ;
  1   �
  )   �
  T   �
  <   N  ^   �  O   �  )   :                                                                                                    	               
           - Edit Web credentials settings Add account… All applications Another instance is already running Are you sure that you wish to remove this Ubuntu Web Account? Control whether this application integrates with Online Accounts Edit Options Grant access Legal notice Online Accounts Online Accounts preferences Online account credentials and settings Options Output version information and exit Please authorize Ubuntu to access your %s account Please authorize Ubuntu to access your %s account: Remove Account Run '%s --help' to see a full list of available command line options.
 Select to configure a new %s account Show accounts that integrate with: The Web Account which manages the integration of %s with your applications will be removed. The following applications integrate with your %s account: There are currently no account providers available which integrate with this application There are currently no applications installed which integrate with your %s account. Your online %s account is not affected. Project-Id-Version: gnome-control-center-signon
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-01 12:56+0000
PO-Revision-Date: 2013-04-06 06:56+0000
Last-Translator: Paco Molinero <paco@byasl.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:14+0000
X-Generator: Launchpad (build 18115)
  - Editar configuración de credenciales web Añadir cuenta… Todas las aplicaciones Ya se está ejecutando otra instancia ¿Está seguro de que quiere quitar esta cuenta web de Ubuntu? Controle si esta aplicación se integrará con Cuentas en línea Editar opciones Conceder acceso Aviso legal Cuentas en línea Preferencias de Cuentas en línea Credenciales y configuración de cuentas en línea Opciones Mostrar información de versión y salir Autorice a Ubuntu para que acceda a su cuenta de %s Autorice a Ubuntu para que acceda a su cuenta de %s: Eliminar la cuenta Ejecute «%s --help» para ver una lista completa de opciones de línea de órdenes disponibles.
 Seleccione para configurar una cuenta nueva de %s Mostrar todas las cuentas integradas con: Se eliminará la cuenta web que gestiona la integración de %s con sus aplicaciones. Las siguientes aplicaciones se integran con su cuenta de %s: Actualmente no hay ninguna cuenta proporcionada disponible que se integre con esta aplicación Actualmente no hay aplicaciones instaladas que se integren con su cuenta de %s. Su cuenta en línea %s no está afectada. 