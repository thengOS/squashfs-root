��          \      �       �   	   �   
   �      �      �   3   �   �   ,  !   �  �       �     �     �  	   �  >     �   A  '   ,                                       Calculate Calculator Open in calculator Result Sorry, there are no results that match your search. This is an Ubuntu search plugin that enables Calculator results to be displayed in the Dash underneath the Info header. If you do not wish to search this content source, you can disable this search plugin. calculator;result;calculate;calc; Project-Id-Version: unity-scope-calculator
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-28 09:22+0000
PO-Revision-Date: 2014-05-03 07:19+0000
Last-Translator: Mauricio J. Adonis C. <Unknown>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:07+0000
X-Generator: Launchpad (build 18115)
 Calcular Calculadora Abrir en la calculadora Resultado Lo sentimos, no hay resultados que coincidan con su búsqueda. Este es un complemento de búsqueda de Ubuntu que permite a la calculadora mostrar resultados en el panel debajo del encabezado de informaciones. Si no quiere buscar desde esta fuente de contenido, puede deshabilitar este complemento. calculadora;resultado;calcular;cálculo 