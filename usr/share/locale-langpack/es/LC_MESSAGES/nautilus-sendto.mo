��    	      d      �       �      �   )   �   2        F  ,   T  #   �  "   �  0   �  $  �  
     @   )  ;   j     �  I   �  -     5   3  9   i                                          	    Archive Could not parse command-line options: %s
 Expects URIs or filenames to be passed as options
 Files to send No mail client installed, not sending files
 Output version information and exit Run from build directory (ignored) Use XID as parent to the send dialogue (ignored) Project-Id-Version: nautilus-sendto.master.es
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=nautilus-sendto&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 23:48+0000
PO-Revision-Date: 2015-12-04 22:01+0000
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: Español <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:34+0000
X-Generator: Launchpad (build 18115)
Language: 
 Archivador No se pueden procesar las opciones de la línea de órdenes: %s
 Espera que se pasen como opciones URI o nombres de archivo
 Archivos para enviar No hay ningún cliente de correo-e instalado, no se envían los archivos
 Mostrar información de la versión y salir Ejecutar desde la carpeta de construcción (ignorada) Usar XID como el padre para enviar el diálogo (ignorada) 