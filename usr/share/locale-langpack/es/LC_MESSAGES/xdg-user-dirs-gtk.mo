��          |      �             !     5  -   E  '   s  3   �  ,   �     �  �        �     �     �  �       �     �  1   �  (     c   F  5   �  *   �  �        �          0                            
      	                         Current folder name New folder name Note that existing content will not be moved. There was an error updating the folders Update common folders names to match current locale Update standard folders to current language? User folders update You have logged in in a new language. You can automatically update the names of some standard folders in your home folder to match this language. The update would change the following folders: _Don't ask me this again _Keep Old Names _Update Names Project-Id-Version: xdg-user-dirs-gtk.HEAD.es
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-04 03:00+0000
PO-Revision-Date: 2012-04-04 10:20+0000
Last-Translator: Adolfo Jayme <fitoschido@gmail.com>
Language-Team: Spanish <traductores@es.gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:21+0000
X-Generator: Launchpad (build 18115)
 Nombre actual de la carpeta Nombre nuevo de la carpeta Nótese que no se moverá el contenido existente. Hubo un error al actualizar las carpetas Actualizar los nombres comunes de las carpetas para coincidir con la configuración regional actual ¿Actualizar las carpetas estándar al idioma actual? Actualización de las carpetas del usuario Ha iniciado sesión en un idioma nuevo. Puede actualizar los nombres de algunas carpetas estándar en su carpeta personal de forma automática para que coincidan con este idioma. La actualización cambiaría las siguientes carpetas: _No preguntarme esto de nuevo _Conservar nombres anteriores _Actualizar nombres 