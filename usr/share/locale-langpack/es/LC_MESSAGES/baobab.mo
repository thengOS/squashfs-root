��    9      �  O   �      �     �               $     7  '   H  ;   p  �   �     V  !   c     �  +   �     �     �     �     �     �  %        .  4   I     ~     �     �      �     �     �     
          &     2     A  "   J      m     �     �     �     �      �     �     �     	     	  (   	     A	     M	     Z	  	   c	     m	     t	     |	     �	     �	     �	     �	     �	     �	  I  �	      '     H     c     |     �  8   �  @   �  �   +     �  ,   �       E   &     l     s  	   z  %   �     �  0   �  %   �  :   !     \     w      �  (   �     �     �               1     B  
   W  +   b  -   �     �     �     �     �           "     C     G     Z  &   f     �     �     �     �  
   �  	   �     �     �                     "  �  C     .                3            ,                              6                7   9          	             -       %             +   !                      $         (       &           #   '          8   /   
   4          "       1      )            2       *   0       5            "%s" is not a valid folder %d day %d days %d item %d items %d month %d months %d year %d years A graphical tool to analyze disk usage. A list of URIs for partitions to be excluded from scanning. A simple application which can scan either specific folders (local or remote) or volumes and give a graphical representation including each directory size or percentage. Active Chart Apparent sizes are shown instead. Baobab Check folder sizes and available disk space Close Computer Contents Could not analyze disk usage. Could not analyze volume. Could not detect occupied disk sizes. Could not scan folder "%s" Could not scan some of the folders contained in "%s" Devices and locations Disk Usage Analyzer Excluded partitions URIs Failed to move file to the trash Failed to open file Failed to show help Folder Go to _parent folder Home folder Mo_ve to Trash Modified Print version information and exit Recursively analyze mount points Rings Chart Scan Folder… Select Folder Size The GdkWindowState of the window The initial size of the window Today Treemap Chart Unknown Which type of chart should be displayed. Window size Window state Zoom _in Zoom _out _About _Cancel _Copy Path to Clipboard _Help _Open _Open Folder _Quit storage;space;cleanup; translator-credits Project-Id-Version: gnome-utils.master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=baobab&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 14:31+0000
PO-Revision-Date: 2015-12-04 01:28+0000
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: Español; Castellano <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:41+0000
X-Generator: Launchpad (build 18115)
Language: 
 «%s» no es una carpeta válida hace %d día hace %d días %d elemento %d elementos hace %d mes hace %d meses hace %d año hace %d años Una herramienta gráfica para analizar el uso del disco. Una lista de URI para las particiones que excluir del análisis. Una sencilla aplicación que puede analizar carpetas específicas (locales o remotas) o volúmenes y mostrar una representación gráfica del tamaño o el porcentaje de cada carpeta. Gráfico activo Se muestran el tamaño aparente en su lugar. Baobab Compruebe el tamaño de las carpetas y el espacio disponible en disco Cerrar Equipo Contenido No se pudo analizar el uso del disco. No se pudo analizar el volumen. No se pudo detectar el espacio de disco ocupado. No se pudo analizar la carpeta «%s» No se pudieron analizar algunas de las carpetas de «%s». Dispositivos y ubicaciones Analizador de uso de disco URI de las particiones excluidas Falló al mover el archivo a la papelera Falló al abrir el archivo Falló al mostrar la ayuda Carpeta _Ir a la carpeta padre Carpeta personal Mo_ver a la papelera Modificado Mostrar información de la versión y salir Analizar recursivamente los puntos de montaje Gráfico de anillos Analizar carpeta… Seleccione la carpeta Tamaño El GdkWindowState de la ventana El tamaño inicial de la ventana Hoy Gráfico de árbol Desconocido Qué tipo de gráfico se debe mostrar. Tamaño de la ventana Estado de la ventana Ampliar Reducir A_cerca de _Cancelar _Copiar la ruta al portapapeles Ay_uda _Abrir _Abrir carpeta _Salir almacenamiento;espacio;limpieza; Daniel Mustieles <daniel.mustieles@gmail.com>, 2012
Jorge González <jorgegonz@svn.gnome.org>, 2007-2009
Francisco Javier F. Serrador <serrador@cvs.gnome.org>, 2003-2006
Pablo Gonzalo del Campo <pablodc@bigfoot.com>,2002-2003
QA - Germán Poo Caamaño <gpoo@ubiobio.cl>, 2002
Carlos Perelló Marín <carlos@gnome-db.org>, 2001
Manuel de Vega Barreiro <barreiro@arrakis.es> 2000-2001
Pablo Saratxaga <srtxg@chanae.alphanet.ch> 1999
Miguel de Icaza <miguel@metropolis.nuclecu.unam.mx> 1998

Launchpad Contributions:
  Adolfo Jayme https://launchpad.net/~fitojb
  Daniel Mustieles https://launchpad.net/~daniel-mustieles
  Paco Molinero https://launchpad.net/~franciscomol 