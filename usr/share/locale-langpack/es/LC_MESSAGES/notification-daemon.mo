��          |      �             !     9     R     h  (   ~     �     �     �     �       '     "  D      g     �     �  !   �  0   �  )        5  %   N  &   t     �  (   �                      	                          
           Clear all notifications Closes the notification. Display notifications Enable debugging code Exceeded maximum number of notifications Invalid notification identifier Notification Daemon Notification body text. Notification summary text. Notifications Replace a currently running application Project-Id-Version: notification-daemon.po.master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=notification-daemon&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-19 01:30+0200
PO-Revision-Date: 2015-12-09 12:31+0000
Last-Translator: Adolfo Jayme <fitoschido@gmail.com>
Language-Team: Español <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:46+0000
X-Generator: Launchpad (build 18115)
Language: 
 Limpiar todas las notificaciones Cierra la notificación. Mostrar notificaciones Activar el código de depuración Se excedió el número máximo de notificaciones Identificador de notificación no válido Demonio de notificación Texto del cuerpo de la notificación. Texto del resumen de la notificación. Notificaciones Reemplazar una aplicación en ejecución 