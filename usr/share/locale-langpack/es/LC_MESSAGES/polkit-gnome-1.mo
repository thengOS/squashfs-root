��          �      ,      �     �     �     �     �  �   �  �   �  ~        �  /   �     �     �     �  ?        C     Q  
   c    n     �     �      �     �  �   �  �   �  �   @  
   �  1   �     	     !	     5	  B   K	     �	     �	     �	        	                                                                  
    %s (%s) <small><b>Action:</b></small> <small><b>Vendor:</b></small> <small><b>_Details</b></small> An application is attempting to perform an action that requires privileges. Authentication as one of the users below is required to perform this action. An application is attempting to perform an action that requires privileges. Authentication as the super user is required to perform this action. An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Authenticate Authentication dialog was dismissed by the user Click to edit %s Click to open %s Select user... Your authentication attempt was unsuccessful. Please try again. _Authenticate _Password for %s: _Password: Project-Id-Version: policykit-gnome.HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=policykit-gnome&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-04 00:05+0000
PO-Revision-Date: 2015-12-04 21:28+0000
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: Español <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:41+0000
X-Generator: Launchpad (build 18115)
 %s (%s) <small><b>Acción:</b></small> <small><b>Proveedor:</b></small> <small><b>_Detalles</b></small> Una aplicación está intentando realizar una acción que necesita permisos especiales. Es necesario autenticarse como uno de los usuarios siguientes para realizar dicha acción. Una aplicación está intentando realizar una acción que necesita permisos especiales. Es necesario autenticarse como super usuario para realizar dicha acción. Una aplicación está intentando realizar una acción que necesita permisos especiales. Es necesario autenticarse para realizar dicha acción. Autenticar El usuario rechazó el diálogo de autenticación Pulse para editar %s Pulse para abrir %s Seleccione usuario… Su intento de autenticación fue infructuoso. Inténtelo de nuevo. _Autenticar Contrase_ña para %s: Contrase_ña: 