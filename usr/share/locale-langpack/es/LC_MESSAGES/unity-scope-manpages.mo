��    	      d      �       �      �      �      �      �   4        H  �   \     D  �  R     �               1  <   M     �  �   �     �        	                                      Manpages Open Search Manpages Search for Manpages Sorry, there are no Manpages that match your search. Technical Documents This is an Ubuntu search plugin that enables information from local manpages to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. manpages;man; Project-Id-Version: unity-scope-manpages
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-24 16:55+0000
PO-Revision-Date: 2014-05-03 07:28+0000
Last-Translator: Paco Molinero <paco@byasl.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Páginas de manual Abrir Buscar páginas de manual Buscar páginas del manual. Lo sentimos, no hay manuales que coincidan con su búsqueda. Documentos técnicos Este es un complemento de búsqueda de Ubuntu que permite buscar a través del tablero páginas de manual locales. Los resultados se muestran debajo del encabezado de Código. Si desea no buscar en esta fuente puede deshabilitar el complemento. Páginas de manual,man 