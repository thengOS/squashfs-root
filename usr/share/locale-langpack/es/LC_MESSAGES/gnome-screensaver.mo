��    7      �  I   �      �     �     �     �     �  )   
     4  (   @     i          �  "   �  E   �       %   +     Q     Z     b  )   �     �  *   �     �  /   �     #  	   6  8   @  "   y     �     �     �     �     �     �       D   9  `   ~     �     �  )   	     A	  *   S	  #   ~	  	   �	     �	  D   �	  D   
     R
  !   q
  ,   �
  B   �
  
          '         >  +   _  C  �     �     �  !     (   *  4   S     �  9   �     �     �  &     $   4  Q   Y     �  1   �     �       #     7   1      i  4   �     �  ,   �     �       :     %   U  .   {     �     �      �  #   �  &        9  Z   W  p   �     #  !   C  /   e     �  2   �  )   �            H   (  N   q  %   �  &   �  -     @   ;     |     �  ,   �     �  8   �         -              0                	      1              4      %       '      *      6                 !   &          #         7   
   "       (      5   /   +              )   2                           ,   $         .            3                         (current) Unix password: Authentication failed. Can't set PAM_TTY=%s Cannot get username Causes the screensaver to exit gracefully Checking… Command to invoke from the logout button Don't become a daemon Enable debugging code Enter new Unix password: Error while changing NIS password. If the screensaver is active then deactivate it (un-blank the screen) Incorrect password. Launch screensaver and locker program Log _Out MESSAGE Message to show in the dialog No longer permitted to access the system. No password supplied Not permitted to gain access at this time. Not used Password has been already used. Choose another. Password unchanged Password: Query the length of time the screensaver has been active Query the state of the screensaver Retype new Unix password: S_witch User… Screensaver Show debugging output Show the logout button Show the switch user button Sorry, passwords do not match Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Time has expired. Turn the screensaver on (blank the screen) Unable to establish service %s: %s
 Username: Version of this application You are required to change your password immediately (password aged) You are required to change your password immediately (root enforced) You have the Caps Lock key on. You must choose a longer password You must wait longer to change your password Your account has expired; please contact your system administrator _Password: _Unlock failed to register with the message bus not connected to the message bus screensaver already running in this session Project-Id-Version: gnome-screensaver.HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-01-04 14:27+0000
PO-Revision-Date: 2016-01-05 00:35+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Español <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
 Contraseña UNIX (actual): La autenticación ha fallado. No se puede establecer PAM_TTY=%s No se puede obtener el nombre de usuario Hace que el salvapantallas termine de forma correcta Comprobando… Comando para invocar desde el botón de cierre de sesión No convertirse en un demonio Activar código de depuración Introduzca una contraseña UNIX nueva: Error al cambiar la contraseña NIS. Si el salvapantallas está activo entonces lo desactiva (desenegrece la pantalla) Contraseña incorrecta. Lanzar el salvapantallas y el programa de bloqueo Cerrar _sesión MENSAJE Mensaje para mostrar en el diálogo Ya no se le permite acceder en adelante a este sistema. No introdujo ninguna contraseña No le está permitido ganar acceso en este instante. No usado La contraseña ya ha sido usada. Elija otra. Contraseña no cambiada Contraseña: Consultar cuanto tiempo ha estado activo el salvapantallas Consulta el estado del salvapantallas Vuelva a teclear la contraseña de UNIX nueva: _Cambiar usuario… Salvapantallas Mostrar la salida de depuración Mostrar el botón de cerrar sesión Mostrar el botón de cambio de usuario Las contraseñas no coinciden Le dice al proceso del salvapantallas en ejecución que bloquee la pantalla inmediatamente El salvapantallas ha estado activo durante %d segundo.
 El salvapantallas ha estado activo durante %d segundos.
 El salvapantallas está activo
 El salvapantallas está inactivo
 Actualmente el salvapantallas no está activo.
 Tiempo agotado. Activar el salvapantallas (ennegrecer la pantalla) Incapaz de establecer el servicio %s: %s
 Usuario: Versión de esta aplicación Tiene que cambiar la contraseña inmediatamente (la contraseña caducó) Tiene que cambiar la contraseña inmediatamente (forzado por el administrador) Tiene activada la tecla Bloq. Mayús. Debe elegir una contraseña más larga Debe esperar más para cambiar su contraseña Su cuenta ha caducado. Contacte con su administrador de sistemas C_ontraseña: _Desbloquear falló al registrarse con el bus del mensaje no conectado al bus de mensajes ya se está ejecutando el salvapantallas en esta sesión 