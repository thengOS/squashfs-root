��          �   %   �      0     1     4     :     @     I     V     f     y     �     �     �     �     �  $   �  D        H     b     n  %   �  F   �  �   �  *   �  i   �    X     t     w     }     �     �     �     �     �     �     �     �     �  )     %   0  L   V  !   �     �  "   �  1   �  K   *	  �   v	  *   Q
  w   |
                                                             
                        	                                          %R %R:%S %a %R %a %R:%S %a %b %e, %R %a %b %e, %R:%S %a %b %e, %l:%M %p %a %b %e, %l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop.HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-desktop&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:18+0000
PO-Revision-Date: 2015-12-04 14:53+0000
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: Español <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:58+0000
X-Generator: Launchpad (build 18115)
Language: 
 %R %R:%S %a %R %a %R:%S %a, %e de %b, %R %a %b %e, %R:%S %a %b %e, %l:%M %p %a %b %e, %l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %l:%M %p %l:%M:%S %p el CRTC %d no puede conducir la salida %s el CRTC %d no soporta la rotación=%d CRTC %d: intentando el modo %dx%d@%dHz con salida en %dx%d@%dHz (pasada %d)
 Intentando modos para el CRTC %d
 Sin especificar no se puede clonar la salida en %s no se pudieron asignar los CRTC a las salidas:
%s ninguno de los modos seleccionados es compatible con los modos posibles:
%s la salida %s no tiene los mismos parámetros que otra salida clonada:
modo existente = %d, modo nuevo = %d
coordenadas existentes = (%d, %d), coordenadas nuevas = (%d, %d)
rotación existente = %d, rotación nueva = %d la salida %s no permite el modo %dx%d@%dHz el tamaño virtual requerido no se ajusta al espacio disponible: requerido=(%d, %d), mínimo=(%d, %d), máximo=(%d, %d) 