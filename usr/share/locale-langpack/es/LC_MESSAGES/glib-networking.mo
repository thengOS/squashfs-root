��            )   �      �     �     �  #   �  #   �  #     #   1  #   U     y  "   �  &   �  $   �               +     A     X  $   p  &   �     �  -   �     	  ^   )     �  "   �  .   �     �  N         O  "  l  %   �     �  %   �  *   �  ,   !	  *   N	  ,   y	     �	  )   �	  &   �	  ,   
     ?
  -   G
     u
     �
  %   �
  -   �
  .        0  3   I  '   }  o   �       *   %  3   P  !   �  i   �                                                                                                             	             
                     Certificate has no private key Connection is closed Could not create TLS connection: %s Could not parse DER certificate: %s Could not parse DER private key: %s Could not parse PEM certificate: %s Could not parse PEM private key: %s Error performing TLS close: %s Error performing TLS handshake: %s Error reading data from TLS socket: %s Error writing data to TLS socket: %s Module No certificate data provided Operation would block PKCS#11 Module Pointer PKCS#11 Slot Identifier Peer failed to perform TLS handshake Peer requested illegal TLS rehandshake Proxy resolver internal error. Server did not return a valid TLS certificate Server required TLS certificate Several PIN attempts have been incorrect, and the token will be locked after further failures. Slot ID TLS connection closed unexpectedly TLS connection peer did not send a certificate The PIN entered is incorrect. This is the last chance to enter the PIN correctly before the token is locked. Unacceptable TLS certificate Project-Id-Version: glib-networking master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=glib&keywords=I18N+L10N&component=network
POT-Creation-Date: 2016-05-19 08:00+0000
PO-Revision-Date: 2013-04-16 20:36+0000
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: Español; Castellano <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:01+0000
X-Generator: Launchpad (build 18115)
Language: 
 El certificado no tiene clave privada La conexión está cerrada No se pudo crear la conexión TLS: %s No se pudo analizar el certificado DER: %s No se pudo analizar la clave privada DER: %s No se pudo analizar el certificado PEM: %s No se pudo analizar la clave privada PEM: %s Error al cerrar el TLS: %s Error al realizar la negociación TLS: %s Error al leer datos del socket TLS: %s Error al escribir datos en el socket TLS: %s Módulo No se han proporcionado datos del certificado La operación de bloqueará Puntero del módulo PKCS#11 Identificador de la ranura de PKCS#11 EL par falló al realizar la negociación TLS El par solicitó una renegociación TLS ilegal Error interno del proxy. El servidor no devolvió un certificado TLS válido El servidor requiere un certificado TLS Varios intentos de introducir el PIN han sido incorrectos y el «token» se bloqueará después de más fallos. ID de la ranura La conexión TLS se cerró inesperadamente El par de la conexión TLS no envió un certificado El PIN introducido es incorrecto. Esta es la última oportunidad para introducir el PIN correctamente antes de que se bloquee el «token». Certificado TLS inaceptable 