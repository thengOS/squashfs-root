<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup-default-printer" xml:lang="en-AU">

  <info>
    <link type="guide" xref="printing#setup"/>

    <desc>Pick the printer that you use most often.</desc>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email its:translate="no">jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email its:translate="no">stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email its:translate="no">jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Set the default printer</title>
  <p>If you have more than one printer available, you can select which will be your default printer. You may want to pick the printer you use most often.</p>

  <steps>
    <item>
      <p>Click the icon at the far right of the <gui>menu bar</gui> and select <gui>System Settings</gui>.</p>
    </item>
    <item>
      <p>Open <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Right click your desired default printer from the list of available printers, and click <gui>Set as Default</gui>.</p>
    </item>
  </steps>

  <note>
  <p>When choosing from the list of available printers, you can
  filter the printer search results by specifying a name or location of the
  printer (for example, <input>1st floor</input> or <input>entrance</input>).</p>
  <p>The search results filtering is available only in the dialog for addition of new printers.</p>
  </note>

  <p>When you print in an application, the default printer is automatically used, unless you choose a different printer for that specific print job.</p>
</page>
