<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="addremove-install" xml:lang="en-AU">

  <info>
    <credit type="author">
      <name>Ubuntu Documentation Team</name>
    </credit>
    <desc>
      Use <app>Ubuntu Software</app> to add programs and make Ubuntu more 
      useful.
    </desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="addremove" group="#first"/>
    <link type="seealso" xref="addremove-remove"/>
    <link type="seealso" xref="addremove-install-synaptic"/>
    <link type="seealso" xref="prefs-language-install"/>
    <revision version="16.04" date="2016-03-13" status="review"/>
  </info>

  <title>Install additional software</title>

  <p>The Ubuntu development team has chosen a default set of applications that we think makes Ubuntu very useful for most day-to-day tasks. However, you will certainly want to install more software to make Ubuntu more useful to you.</p>

  <p>To install additional software, complete the following steps:</p>

  <steps>
    <item>
      <p>Connect to the Internet using a <link xref="net-wireless-connect">wireless</link> or <link xref="net-wired-connect">wired connection</link>.</p>
    </item>
    <item>
      <p>
        Click the <app>Ubuntu Software</app> icon in the <gui>Launcher</gui>, or search
        for <input>Software</input> in the search bar of the <gui>Dash</gui>.
      </p>
    </item>
    <item>
      <p>
        When <app>Ubuntu Software</app> launches, search for an application, or select 
        a category and find an application from the list.
      </p>
    </item>
    <item>
      <p>Select the application that you are interested in and click <gui>Install</gui>.</p>
    </item>
    <item>
      <p>You will be asked to enter your password. Once you have done that the installation will begin.</p>
    </item>
    <item>
      <p>The installation usually finishes quickly, but could take a while if you have a slow Internet connection.</p>
    </item>
    <item>
      <p>A shortcut to your new app will be added to the Launcher. To disable this feature, uncheck <guiseq><gui>View</gui><gui>New Applications in Launcher</gui></guiseq>.</p>
    </item>
  </steps>
  
</page>
