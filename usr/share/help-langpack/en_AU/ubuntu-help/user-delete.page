<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="user-delete" xml:lang="en-AU">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <desc>Remove users that no longer use your computer.</desc>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>GNOME Documentation Project</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>
  <title>Delete a user account</title>

  <p>You can add multiple user accounts to your computer. See <link xref="user-add"/> to learn how. If somebody is no longer using your computer, you can delete that user's account.</p>

<steps>
  <item><p>Click the icon at the far right of the <gui>menu bar</gui> and select <gui>System Settings</gui>.</p></item>
  <item><p>Open <gui>User Accounts</gui>.</p></item>
  <item><p>Click <gui>Unlock</gui> in the top right corner and type your password to make changes. You must be an administrative user to delete user accounts.</p></item>
  <item><p>Select the user you want to delete and click the <gui>-</gui> button.</p></item>
  <item><p>Each user has their own home folder for their files and settings. You can choose to keep or delete the user's home folder. Click <gui>Delete Files</gui> if you're sure they won't be used anymore and you need to free up disk space. These files are permanently deleted. They can't be recovered. You may want to back up the files to an external drive or CD before deleting them.</p></item>
</steps>
</page>
