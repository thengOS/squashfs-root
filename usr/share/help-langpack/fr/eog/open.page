<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="open" xml:lang="fr">

  <info>
    <link type="guide" xref="index#viewing"/>
    <desc>Ouvrir une image dans une nouvelle fenêtre.</desc>

    <revision pkgversion="3.2" version="0.1" date="2011-09-05" status="final"/>
  <credit type="author">
    <name>Tiffany Antopolski</name>
    <email>tiffany.antopolski@gmail.com</email>
  </credit>
  <license>
   <p>Creative Commons Share Alike 3.0</p>
  </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2006-2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Vincent Untz</mal:name>
      <mal:email>vuntz@gnome.org</mal:email>
      <mal:years>2007</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sébastien Seguin</mal:name>
      <mal:email>seb24b@free.fr</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  </info>


<title>Ouverture d'une image</title>

<p>Les images devraient s'ouvrir automatiquement dans le visionneur d'images quand vous double-cliquez dessus dans le <app>navigateur de fichiers</app>. Si ce n'est pas le cas il est possible que vous deviez changer le visionneur d'images en <link xref="default">application par défaut</link> pour afficher les images.</p>

<p>Chaque image que vous double-cliquez, s'affiche dans une nouvelle fenêtre du visionneur d'images mais, si vous préférez, vous pouvez aussi <link xref="view">afficher plusieurs images</link> dans une seule fenêtre.</p>

<p>Vous pouvez aussi ouvrir des images à partir du visionneur d'images lui-même :</p>
<steps>
 <item><p>Cliquez sur <guiseq><gui>Image</gui><gui>Ouvrir</gui></guiseq> (ou pressez <keyseq><key>Ctrl</key><key>O</key></keyseq>). La boîte de dialogue <gui>Ouvrir une image</gui> apparaît.</p></item>
 <item><p>Sélectionnez l'image à ouvrir et cliquez sur <gui>Ouvrir</gui>.</p></item>
</steps>

</page>
