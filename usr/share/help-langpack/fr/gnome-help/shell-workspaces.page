<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="ui" version="1.0 if/1.0" id="shell-workspaces" xml:lang="fr">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.10.3" date="2014-01-26" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Les espaces de travail sont une façon de regrouper les fenêtres sur votre bureau.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Qu'est ce qu'un espace de travail et à quoi sert-il ?</title>

    <media type="image" src="figures/shell-workspaces.png" width="162" height="434" style="floatend floatright">
        <p>Sélecteur d'espace de travail</p>
    </media>

  <p if:test="!platform:gnome-classic">Les espaces de travail font référence à un groupement de fenêtres sur votre bureau. Vous pouvez créer plusieurs espaces de travail qui agissent comme des bureaux virtuels. L'utilité des espaces de travail est de réduire l'encombrement de votre bureau et d'y permettre une navigation plus aisée.</p>

  <p if:test="platform:gnome-classic">Les espaces de travail font référence à un groupement de fenêtres sur votre bureau. Vous pouvez créer plusieurs espaces de travail qui agissent comme des bureaux virtuels. L'utilité des espaces de travail est de réduire l'encombrement de votre bureau et d'y permettre une navigation plus aisée.</p>

  <p>Les espaces de travail peuvent être utilisés pour organiser votre travail. Par exemple, vous pouvez regrouper toutes vos fenêtres de communications, comme les courriels ou les programmes de discussions, dans un même espace de travail et le travail que vous êtes en train de faire sur un autre espace de travail. Votre gestionnaire de musique peut être sur un troisième espace de travail.</p>

<p>Utiliser les espaces de travail :</p>

<list>
  <item>
    <p if:test="!platform:gnome-classic">In the
    <gui xref="shell-introduction#activities">Activities</gui> overview, move your cursor
    to the right-most side of the screen.</p>
    <p if:test="platform:gnome-classic">Appuyez sur <key xref="keyboard-key-super">Logo</key> pour ouvrir la vue d'ensembles des <gui>Activités</gui> et ensuite déplacez le curseur sur l'extrême bord droit de l'écran.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">A vertical panel will appear showing
    workspaces in use, plus an empty workspace. This is the
    workspace selector.</p>
    <p if:test="platform:gnome-classic">A vertical panel will appear showing
    available workspaces. This is the workspace selector.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>To add a workspace, drag and drop a window from an existing workspace
    onto the empty workspace in the workspace selector. This workspace now
    contains the window you have dropped,
    and a new empty workspace will appear below it.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>Pour supprimer un espace de travail, fermez tout simplement toutes ses fenêtres ou déplacez-les vers d'autres espaces de travail.</p>
  </item>
</list>

<p if:test="!platform:gnome-classic">Il y a toujours au minimum un espace de travail.</p>

</page>
