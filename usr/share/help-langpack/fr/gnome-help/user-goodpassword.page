<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="fr">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Utiliser des mots de passe plus longs et plus complexes.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Choix d'un mot de passe sûr</title>

  <note style="important">
    <p>Choisissez des mots de passe dont vous pourrez vous rappeler facilement, mais que les autres (y compris les programmes informatiques) auront beaucoup de mal à deviner.</p>
  </note>

  <p>Un bon mot de passe vous aide à maintenir la sécurité de votre ordinateur. Si votre mot de passe est facile à deviner, d'autres risquent de le trouver et d'accéder à vos données personnelles.</p>

  <p>Il est même possible que des ordinateurs soient utilisés pour deviner votre mot de passe ; ce qu'un humain aurait beaucoup de mal à deviner peut s'avérer extrêmement simple pour un programme informatique. Voici quelques conseils pour choisir un bon mot de passe :</p>
  
  <list>
    <item>
      <p>Utilisez une combinaison de majuscules, minuscules, nombres, symboles et espaces dans le mot de passe. Cela rend le mot de passe plus difficile à deviner ; plus il y a de symboles, plus il y a de combinaisons possibles à vérifier si quelqu'un cherche à trouver votre mot de passe.</p>
      <note>
        <p>Une bonne méthode consiste à créer un mot de passe à partir des premières lettres de chaque mot d'une phrase dont vous pouvez vous rappeler facilement. Il peut s'agir du titre d'un film, d'un livre, d'une chanson ou d'un album. Par exemple, « Charles Baudelaire : Les Fleurs du mal » pourrait donner CB:LFdm ou CBLFdm ou CB :LFdm.</p>
      </note>
    </item>
    <item>
      <p>Choisissez un mot de passe aussi long que possible. Plus il contient de caractères, plus cela demande de temps à une personne ou à un ordinateur pour le deviner.</p>
    </item>
    <item>
      <p>Do not use any words that appear in a standard dictionary in any
      language. Password crackers will try these first. The most common
      password is "password" – people can guess passwords like this very
      quickly!</p>
    </item>
    <item>
      <p>N'utilisez pas d'informations personnelles comme une date, un numéro de plaque d'immatriculation ou le prénom d'un membre de votre famille.</p>
    </item>
    <item>
      <p>N'utilisez pas de noms.</p>
    </item>
    <item>
      <p>Choisissez un mot de passe qui peut être saisi rapidement afin d'éviter que quelqu'un puisse le deviner en vous regardant le saisir.</p>
      <note style="tip">
        <p>Ne notez jamais vos mots de passe. Ils peuvent être facilement retrouvés !</p>
      </note>
    </item>
    <item>
      <p>Utilisez des mots de passe différents pour des choses différentes.</p>
    </item>
    <item>
      <p>Utilisez des mots de passe différents pour des comptes différents.</p>
      <p>Si vous utilisez le même mot de passe pour tous vos comptes, il suffit qu'une personne le trouve pour pouvoir accéder immédiatement à tous vos comptes.</p>
      <p>Cependant, il peut être difficile de se souvenir de plusieurs mots de passe. Même si cela n'est pas aussi sécurisé qu'utiliser un mot de passe différent pour chaque chose, il est plus facile d'utiliser le même mot de passe pour les choses qui n'ont pas d'importance (comme les sites Web) et des mots de passe différents pour les choses importantes (comme votre compte bancaire en ligne ou votre courriel).</p>
   </item>
   <item>
     <p>Modifiez régulièrement vos mots de passe.</p>
   </item>
  </list>

</page>
