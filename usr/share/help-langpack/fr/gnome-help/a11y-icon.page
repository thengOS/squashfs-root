<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-icon" xml:lang="fr">

  <info>
    <link type="guide" xref="a11y"/>

    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>

    <desc>Le menu Accès universel est l'icône de la barre supérieure qui ressemble à un personnage.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Recherche du menu « Accès universel »</title>

  <p>Le <em>menu Accès universel</em> est le lieu où vous pouvez activer divers paramètres d'accessibilité. Vous trouverez ce menu en cliquant sur l'icône ressemblant à un personnage entouré d'un cercle dans la barre supérieure.</p>

  <figure>
    <desc>Le menu Accès universel se trouve dans la barre supérieure.</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/universal-access-menu.png"/>
  </figure>

  <p>Si vous ne voyez pas le menu d'accès universel dans la barre en haut de l'écran, vous pouvez l'activer via le panneau des paramètres de <gui>Accès Universel</gui>.</p>

  <steps>
    <item>
      <p>Ouvrez la vue d'ensemble des <link xref="shell-introduction#activities">Activités</link> et commencez à saisir <gui>Accès universel</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur l'icône <gui>Accès universel</gui> pour afficher le panneau.</p>
    </item>
    <item>
      <p>Basculez l'interrupteur <gui>Toujours afficher le menu Accès universel</gui> sur <gui>Activé</gui>.</p>
    </item>
  </steps>

  <p>Pour accéder à ce menu en utilisant le clavier au lieu de la souris, appuyez sur les touches <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> pour activer la barre supérieure. Une ligne blanche s'affiche sous le bouton <gui>Activités</gui>, elle vous indique l'élément sélectionné dans la barre supérieure. Utilisez les touches fléchées du clavier pour déplacer la ligne blanche sous l'icône du menu Accès universel et appuyez sur <gui>Entrée</gui> pour l'ouvrir. Servez-vous des touches flèches haut et bas pour sélectionner des éléments dans le menu. Appuyez sur <key>Entrée</key> pour activer ou désactiver l'élément sélectionné.</p>

</page>
