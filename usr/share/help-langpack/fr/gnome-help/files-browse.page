<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-browse" xml:lang="fr">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-copy"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Gérer et organiser les fichiers avec le gestionnaire de fichiers.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Consultation de vos fichiers et dossiers</title>

<p>Servez-vous du gestionnaire de fichiers <app>Fichiers</app> pour naviguer et organiser les fichiers de votre ordinateur. Vous pouvez également l'utiliser pour gérer les fichiers sur des supports de stockage (comme des disques durs externes), sur des <link xref="nautilus-connect">serveurs de fichiers</link> et sur des partages réseau.</p>

<p>To start the file manager, open <app>Files</app> in the
<gui xref="shell-introduction#activities">Activities</gui> overview. You can also search
for files and folders through the overview in the same way you would
<link xref="shell-apps-open">search for applications</link>.
</p>

<section id="files-view-folder-contents">
  <title>Parcours du contenu des dossiers</title>

<p>In the file manager, double-click any folder to view its contents, and
double-click or <link xref="mouse-middleclick">middle-click</link> any file to
open it with the default application for that file. Middle-click a folder to
open it in a new tab. You can also right-click a folder to open it in a new tab
or new window.</p>

<p>Quand vous consultez les fichiers d'un dossier, vous pouvez rapidement <link xref="files-preview">prévisualiser chaque fichier</link> en pressant sur la barre d'espace pour vous assurer que c'est le bon fichier avant de l'ouvrir, le copier ou le supprimer.</p>

<p>The <em>path bar</em> above the list of files and folders shows you which
folder you're viewing, including the parent folders of the current folder.
Click a parent folder in the path bar to go to that folder. Right-click any
folder in the path bar to open it in a new tab or window, or access its
properties.</p>

<p>Si vous voulez rapidement <link xref="files-search">rechercher un fichier</link> dans ou sous le dossier ouvert, commencez à saisir son nom. Un <em>champ de recherche</em> apparaît en haut de la fenêtre et seuls les fichiers correspondant à votre recherche s'affichent. Appuyez sur <key>Échap</key> pour annuler la recherche.</p>

<p>You can quickly access common places from the <em>sidebar</em>. If you do
not see the sidebar, click <gui>Files</gui> in the top bar and then select
<gui>Sidebar</gui>. You can add bookmarks to folders that you use often and
they will appear in the sidebar. Drag a folder to the sidebar, and drop it over
<gui>New bookmark</gui>, or click the window menu and then select
<gui style="menuitem">Bookmark this Location</gui>.</p>

</section>

</page>
