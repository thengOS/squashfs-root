<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="keyboard-nav" xml:lang="fr">
  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.7.5" version="0.2" date="2013-02-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
       <name>Ekaterina Gerasimova</name>
       <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Utiliser les applications et le bureau sans souris.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Navigation au clavier</title>

  <p>Cette page détaille la navigation au clavier pour les gens qui ne peuvent utiliser ni souris ni autres outils de pointage, ou qui souhaitent utiliser le clavier autant que possible. Pour des raccourcis claviers utiles à tous les utilisateurs, consultez plutôt <link xref="shell-keyboard-shortcuts"/>.</p>

  <note style="tip">
    <p>Si vous avez des problèmes à utiliser la souris ou d'autres périphériques de pointage, il est possible de contrôler le pointeur de la souris en utilisant le pavé numérique du clavier. Consultez <link xref="mouse-mousekeys"/> pour obtenir des informations.</p>
  </note>

<table frame="top bottom" rules="rows">
  <title>Navigation parmi les interfaces utilisateur</title>
  <tr>
    <td><p><key>Tab</key> et <keyseq><key>Ctrl</key><key>Tab</key></keyseq></p></td>
    <td>
      <p>Déplacer le focus du clavier entre les différents contrôles. <keyseq><key>Ctrl</key> <key>Tab</key></keyseq> change de groupe de contrôles, par exemple du panneau latéral vers le contenu principal. <keyseq><key>Ctrl</key><key>Tab</key></keyseq> peut aussi faire sortir d'un contrôle qui utilise <key>Tab</key> lui-même, comme par exemple une zone de texte.</p>
      <p>Maintenez appuyée la touche <key>Maj</key> pour déplacer le focus en sens inverse.</p>
    </td>
  </tr>
  <tr>
    <td><p>Flèches du clavier</p></td>
    <td>
      <p>Changer la sélection d'éléments au sein d'un contrôle, ou dans un ensemble de contrôles reliés. Utiliser les flèches du clavier pour mettre en évidence les boutons d'une barre d'outils, sélectionner les éléments dans une vue en liste ou en icônes ou sélectionner un bouton radio dans un groupe.</p>
      <p>Dans une vue arborescente, utiliser les flèches gauche et droite pour étendre et réduire les éléments avec enfants.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Touches fléchées</key></keyseq></p></td>
    <td><p>Dans une vue en liste ou en icônes, déplacer le focus du clavier vers un autre élément sans changer l'élément sélectionné.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Maj</key><key>Touches fléchées</key></keyseq></p></td>
    <td><p>Dans une vue en liste ou en icônes, sélectionner tous les éléments depuis l'élément sélectionné vers le nouvel élément mis en évidence.</p></td>
  </tr>
  <tr>
    <td><p><key>Barre d'espace</key></p></td>
    <td><p>Activer un élément mis en évidence, comme par exemple un bouton, une case à cocher ou un élément de liste.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Barre d'espace</key></keyseq></p></td>
    <td><p>Dans une vue en liste ou en icônes, sélectionner ou désélectionner l'élément mis en évidence sans désélectionner les autres éléments.</p></td>
  </tr>
  <tr>
    <td><p><key>Alt</key></p></td>
    <td><p>Maintenez enfoncée la touche <key>Alt</key> pour révéler les <em>accélérateurs</em> : les lettres soulignées sur les éléments du menu, les boutons et les autres contrôles. Appuyez sur <key>Alt</key> plus la lettre soulignée pour activer un contrôle, comme si vous aviez cliqué dessus.</p></td>
  </tr>
  <tr>
    <td><p><key>Échap</key></p></td>
    <td><p>Sortir d'un menu, d'une fenêtre surgissante ou d'une boîte de dialogue.</p></td>
  </tr>
  <tr>
    <td><p><key>F10</key></p></td>
    <td><p>Ouvrir le premier menu de la barre de menu d'une fenêtre. Utiliser les flèches du clavier pour naviguer dans les menus.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="windows-key">Logo</key><key>F10</key></keyseq></p></td>
    <td><p>Ouvrir le menu de l'application dans la barre supérieure.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Maj</key><key>F10</key></keyseq> ou <key xref="keyboard-key-menu">Menu</key></p></td>
    <td>
      <p>Afficher le menu contextuel pour la sélection en cours, comme si vous aviez effectué un clic-droit.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>F10</key></keyseq></p></td>
    <td><p>Dans le gestionnaire de fichiers, afficher le menu contextuel du dossier en cours, comme si vous aviez effectué un clic-droit sur l'arrière-plan et pas sur un élément.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Page haut</key></keyseq> et <keyseq><key>Ctrl</key><key>Page Bas</key></keyseq></p></td>
    <td><p>Dans une interface à onglets, basculer vers l'onglet de gauche ou de droite.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Déplacements dans le bureau</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-f1"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tick"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-updown"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
    <td><p>Pour parcourir les fenêtres d'une même application, maintenez appuyée la touche <key>Alt</key> et appuyez sur <key>F6</key> jusqu'à ce que la fenêtre que vous désirez soit en surbrillance, puis relâchez la touche <key>Alt</key>. Cela est identique à la fonction de <keyseq><key>Alt</key><key>`</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Échap</key></keyseq></p></td>
    <td><p>Parcourir toutes les fenêtres ouvertes d'un espace de travail.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Logo</key><key>M</key></keyseq></p></td>
    <td><p><link xref="shell-notifications#messagetray">Ouvrir le tiroir de messagerie.</link> Appuyez sur <key>Échap</key> pour fermer.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Gestion des fenêtres</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>Fermer la fenêtre actuelle.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F5</key></keyseq> ou <keyseq><key>Logo</key><key>↓</key></keyseq></p></td>
    <td><p>Pour restaurer à sa taille initiale ou maximiser une fenêtre, appuyez sur <keyseq><key>Alt</key> <key>F10</key></keyseq>. <keyseq><key>Alt</key><key>F10</key></keyseq> permet alternativement de maximiser et de restaurer.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
    <td><p>Déplacer la fenêtre actuelle. Appuyez sur <keyseq><key>Alt</key><key>F7</key></keyseq>, puis utilisez les touches fléchées pour déplacer la fenêtre. Appuyez sur <key>Entrée</key> pour terminer le déplacement, ou sur <key>Échap</key> pour la remettre à son point de départ.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
    <td><p>Pour redimensionner la fenêtre actuelle, appuyez sur <keyseq><key>Alt</key><key>F8</key></keyseq>, puis utilisez les touches fléchées pour obtenir sa nouvelle taille. Appuyez ensuite sur <key>Entrée</key> pour terminer le redimensionnement, ou sur <key>Échap</key> pour annuler et la remettre à sa taille d'origine.</p></td>
  </tr>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-shift-updown"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F10</key></keyseq> ou <keyseq><key>Logo</key><key>↑</key></keyseq></p>
    </td>
    <td><p><link xref="shell-windows-maximize">Maximiser</link> une fenêtre. Appuyez sur <keyseq><key>Alt</key><key>F10</key></keyseq> ou <keyseq><key>Logo</key><key>↓</key></keyseq> pour restaurer une fenêtre maximisée dans sa taille d'origine.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Logo</key><key>H</key></keyseq></p></td>
    <td><p>Minimiser une fenêtre.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Logo</key><key>←</key></keyseq></p></td>
    <td><p>Maximise une fenêtre verticalement le long du côté gauche de l'écran. Appuyez à nouveau pour la remettre à sa taille précédente. Appuyez sur <keyseq><key>Logo</key><key>→</key></keyseq> pour passer d'un côté à l'autre.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Logo</key><key>→</key></keyseq></p></td>
    <td><p>Maximise une fenêtre verticalement le long du côté droit de l'écran. Appuyez à nouveau pour la remettre à sa taille précédente. Appuyez sur <keyseq><key>Logo</key><key>←</key></keyseq> pour passer d'un côté à l'autre.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Barre d'espace</key></keyseq></p></td>
    <td><p>Afficher le menu de la fenêtre, comme avec un clic droit sur la barre de titre.</p></td>
  </tr>
</table>

</page>
