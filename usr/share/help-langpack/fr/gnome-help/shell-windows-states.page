<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-states" xml:lang="fr">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-03-24" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Arranger les fenêtres dans un espace de travail pour travailler plus efficacement.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Déplacement et redimensionnement de vos fenêtres.</title>

  <p>Vous pouvez déplacer et redimensionner les fenêtres pour travailler plus efficacement. En plus des fonctionnalités de glissement que vous pourriez attendre, GNOME fournit des raccourcis qui vous permettent d'arranger les fenêtres rapidement.</p>

  <list>
    <item>
      <p>Déplacez une fenêtre en faisant glisser sa barre de titre, ou maintenez <key>Logo</key> appuyée pour la faire glisser à partir de n'importe quel point de la fenêtre. Maintenez la touche <key>Maj</key> appuyée tout en déplaçant la fenêtre pour la positionner au bord de l'écran ou à côté d'une autre fenêtre.</p>
    </item>
    <item>
      <p>Redimensionnez une fenêtre en faisant glisser les bords ou les coins de celle-ci. Maintenez la touche <key>Maj</key> appuyée tout en redimensionnant la fenêtre pour la positionner au bord de l'écran ou à côté d'une autre fenêtre.</p>
      <p if:test="platform:gnome-classic">Vous pouvez aussi restaurer une fenêtre maximisée en cliquant sur le bouton adéquat dans la barre de titre.</p>
    </item>
    <item>
      <p>Déplacez ou redimensionnez une fenêtre en utilisant seulement le clavier. Appuyez sur <keyseq><key>Alt</key><key>F7</key></keyseq> pour déplacer une fenêtre ou <keyseq><key>Alt</key><key>F8</key></keyseq> pour la redimensionner. Utilisez les flèches du clavier pour déplacer ou redimensionner, puis appuyez sur <key>Entrée</key> pour terminer, ou appuyez sur <key>Échap</key> pour retourner à la position et la taille originales.</p>
    </item>
    <item>
      <p><link xref="shell-windows-maximize">Maximisez une fenêtre</link> en la faisant glisser jusqu'en haut de l'écran. Faites glisser une fenêtre jusqu'à un bord de l'écran pour la maximiser le long du bord, vous permettant ainsi d'<link xref="shell-windows-tiled">afficher des fenêtres côte-à-côte</link>.</p>
    </item>
  </list>

</page>
