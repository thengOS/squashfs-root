<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="status-icons" xml:lang="fr">

  <info>
    <link type="guide" xref="shell-overview#apps"/>

    <revision version="0.1" date="2013-02-23" status="review"/>

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email>monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Explication de la signification des icônes situées à droite de la barre supérieure.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Signification des icônes de la barre supérieure</title>
<p>Cette section explique la signification des icônes situées dans le coin en haut à droite de l'écran, et plus spécifiquement, la description des différentes variations possibles d'icônes fournies avec l'interface de GNOME.</p>

<if:choose>
<if:when test="!platform:gnome-classic">
<media type="image" src="figures/top-bar-icons.png" style="floatend">
  <p>La barre supérieure de GNOME Shell</p>
</media>
</if:when>
<if:when test="platform:gnome-classic">
<media type="image" src="figures/top-bar-icons-classic.png" width="395" height="70" style="floatend">
  <p>La barre supérieure de GNOME Shell</p>
</media>
</if:when>
</if:choose>

<links type="section"/>

<section id="universalicons">
<title>Icônes du menu accès universel</title>

 <table shade="rows">
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/preferences-desktop-accessibility-symbolic.svg"/></td>
      <td><p>Affiche un menu qui permet d'activer les différents paramètres d'accessibilité.</p></td>
    </tr>
    
  </table>
</section>


<section id="audioicons">
<title>Icônes de contrôle du volume</title>

 <table shade="rows">
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-high-symbolic.svg"/></td>
      <td><p>Le volume est fort.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-medium-symbolic.svg"/></td>
      <td><p>Le volume est moyen.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-low-symbolic.svg"/></td>
      <td><p>Le volume est faible.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-muted-symbolic.svg"/></td>
      <td><p>Le volume est en sourdine.</p></td>
    </tr>
  </table>
</section>


<section id="bluetoothicons">
<title>Icônes du gestionnaire Bluetooth</title>

 <table shade="rows">
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/></td>
      <td><p>Le Bluetooth est activé.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-disabled-symbolic.svg"/></td>
      <td><p>Le Bluetooth est désactivé.</p></td>
    </tr>
  </table>
</section>

<section id="networkicons">
<title>Icônes du gestionnaire de réseaux</title>
<p/>
<p><app>Connexion cellulaire</app></p>
 <table shade="rows">

    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-3g-symbolic.svg"/></td>
      <td><p>Connecté à un réseau 3G.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-4g-symbolic.svg"/></td>
      <td><p>Connecté à un réseau 4G.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-edge-symbolic.svg"/></td>
      <td><p>Connecté à un réseau EDGE.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-gprs-symbolic.svg"/></td>
      <td><p>Connecté à un réseau GPRS.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-umts-symbolic.svg"/></td>
      <td><p>Connecté à un réseau UMTS.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-connected-symbolic.svg"/></td>
      <td><p>Connecté à un réseau cellulaire.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-acquiring-symbolic.svg"/></td>
      <td><p>Acquisition d'une connexion de réseau cellulaire.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-excellent-symbolic.svg"/></td>
      <td><p>Signal très puissant.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-good-symbolic.svg"/></td>
      <td><p>Signal puissant.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-ok-symbolic.svg"/></td>
      <td><p>Signal d'intensité moyenne.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-weak-symbolic.svg"/></td>
      <td><p>Signal faible.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-none-symbolic.svg"/></td>
      <td><p>Signal extrêmement faible.</p></td>
    </tr></table>




<p><app>Connexions au réseau local (LAN)</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-error-symbolic.svg"/></td>
      <td><p>Une erreur est survenue lors de la recherche du réseau.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-idle-symbolic.svg"/></td>
      <td><p>Le réseau est inactif.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-no-route-symbolic.svg"/></td>
      <td><p>Aucun chemin vers le réseau n'a été trouvé.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-offline-symbolic.svg"/></td>
      <td><p>Le réseau est hors-ligne.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-receive-symbolic.svg"/></td>
      <td><p>Le réseau est en cours de réception de données.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-transmit-receive-symbolic.svg"/></td>
      <td><p>Le réseau est en cours d'émission et de réception de données.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-transmit-symbolic.svg"/></td>
      <td><p>Le réseau est en cours de transmission de données.</p></td>
    </tr>
</table>



<p><app>Connexion à un réseau privé virtuel (VPN)</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-vpn-acquiring-symbolic.svg"/></td>
      <td><p>Connexion à un réseau en cours.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-vpn-symbolic.svg"/></td>
      <td><p>Connecté à un réseau VPN.</p></td>
    </tr>
</table>


<p><app>Connexion filaire</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wired-acquiring-symbolic.svg"/></td>
      <td><p>Connexion à un réseau en cours.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wired-disconnected-symbolic.svg"/></td>
      <td><p>Déconnecté du réseau.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wired-symbolic.svg"/></td>
      <td><p>Connecté à un réseau filaire.</p></td>
    </tr>
</table>


<p><app>Connexion sans fil</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-acquiring-symbolic.svg"/></td>
      <td><p>Acquisition d'une connexion sans fil.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-encrypted-symbolic.svg"/></td>
      <td><p>Le réseaux sans fil est chiffré.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-connected-symbolic.svg"/></td>
      <td><p>Connecté à un réseau sans fil.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-excellent-symbolic.svg"/></td>
      <td><p>Signal très puissant.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-good-symbolic.svg"/></td>
      <td><p>Signal puissant.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-ok-symbolic.svg"/></td>
      <td><p>Signal d'intensité moyenne.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-weak-symbolic.svg"/></td>
      <td><p>Signal faible.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-none-symbolic.svg"/></td>
      <td><p>Signal très faible.</p></td>
    </tr>

  </table>
</section>

<section id="batteryicons">
<title>Icônes du gestionnaire d'énergie</title>

 <table shade="rows">
   <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-full-symbolic.svg"/></td>
      <td><p>La batterie est chargée.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-good-symbolic.svg"/></td>
      <td><p>La batterie est partiellement déchargée.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-low-symbolic.svg"/></td>
      <td><p>Le niveau de la batterie est faible.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-caution-symbolic.svg"/></td>
      <td><p>Attention : la batterie est très faible.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-empty-symbolic.svg"/></td>
      <td><p>Le niveau de la batterie est extrêmement faible.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-missing-symbolic.svg"/></td>
      <td><p>La batterie a été débranchée.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-full-charged-symbolic.svg"/></td>
      <td><p>La batterie est complètement chargée.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-full-charging-symbolic.svg"/></td>
      <td><p>La batterie est pleine et en cours de chargement.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-good-charging-symbolic.svg"/></td>
      <td><p>La batterie est partiellement pleine et en cours de chargement.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-low-charging-symbolic.svg"/></td>
      <td><p>La batterie est faible et en cours de chargement.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-caution-charging-symbolic.svg"/></td>
      <td><p>La batterie est très faible et en cours de chargement.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-empty-charging-symbolic.svg"/></td>
      <td><p>La batterie est vide et en cours de chargement.</p></td>
    </tr>
  </table>
</section>


</page>
