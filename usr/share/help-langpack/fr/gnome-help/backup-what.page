<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="fr">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sauvegarder tout ce qu'il n'est pas envisageable de perdre si quelque chose va mal.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Que faut-il sauvegarder ?</title>

  <p>Votre priorité doit être de sauvegarder vos <link xref="backup-thinkabout">fichiers les plus importants</link> ainsi que ceux qui sont difficiles à recréer. Par exemple, classés du plus au moins important :</p>

<terms>
 <item>
  <title>Vos fichiers personnels</title>
   <p>Cela comprend les documents, les feuilles de travail, les courriels, l'agenda des rendez-vous, les données financières, les photos de famille ou encore d'autres fichiers personnels que vous considérez comme irremplaçables.</p>
 </item>

 <item>
  <title>Vos réglages personnels</title>
   <p>Cela inclut les modifications que vous avez effectué aux paramètres couleurs, arrière-plans, résolution d'écran et souris de votre bureau. Cela inclut aussi les préférences des applications, tels que les paramètres pour <app>LibreOffice</app>, pour votre lecteur multimédia, votre logiciel de messagerie. Tout ceci peut être retrouvé mais cela peut prendre un certain temps.</p>
 </item>

 <item>
  <title>Paramètres système</title>
   <p>La plupart des gens ne modifie jamais les réglages système définis pendant l'installation. Si vous avez personnalisé les réglages de votre système pour une raison ou une autre ou si vous utilisez votre ordinateur en tant que serveur alors il vous faut sauvegarder ces réglages.</p>
 </item>

 <item>
  <title>Logiciels installés</title>
   <p>Vos logiciels préférés peuvent normalement être restaurés très rapidement après un problème sérieux de votre ordinateur en les réinstallant.</p>
 </item>
</terms>

  <p>En général, il faut sauvegarder les fichiers irremplaçables et ceux qui nécessitent beaucoup de temps pour être recréés. Par contre, rien ne sert de gaspiller de l'espace de stockage pour sauvegarder des fichiers facilement remplaçables.</p>

</page>
