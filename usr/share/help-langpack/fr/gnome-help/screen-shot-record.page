<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="screen-shot-record" xml:lang="fr">

  <info>
    <link type="guide" xref="tips"/>

    <revision pkgversion="3.6.1" date="2012-11-10" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.14.0" date="2015-01-14" status="review"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Prendre une capture ou une vidéo de ce qui se passe sur votre écran.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Captures d'écran et vidéo d'écran</title>

  <p>Vous pouvez prendre une <em>capture d'écran</em> ou enregistrer une vidéo de ce se passe à l'écran. C'est pratique, par exemple, si vous voulez montrer comment faire quelque chose à quelqu'un sur l'ordinateur. Les captures et les vidéos d'écran sont des fichiers images et vidéos normaux, vous pouvez donc les envoyer par courriel ou les partager sur le Web.</p>

<section id="screenshot">
  <title>Prendre une capture d'écran</title>

  <steps>
    <item>
      <p>Open <app>Screenshot</app> from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
    <item>
      <p>Dans la fenêtre <app>Capture d'écran</app>, choisissez si vous voulez copier tout l'écran, la fenêtre active ou une zone de l'écran. Définissez un délai si vous avez besoin de sélectionner une fenêtre ou de définir la taille de votre bureau pour la capture d'écran. Puis choisissez les effets que vous souhaitez.</p>
    </item>
    <item>
       <p>Cliquez sur <gui>Prendre une capture d'écran</gui>.</p>
       <p>Si vous avez sélectionné <gui>Sélectionner la zone à capturer</gui>, le pointeur devient une croix. Maintenez le bouton gauche de la souris appuyé et faites glisser votre souris afin d'obtenir la zone de capture de votre choix.</p>
    </item>
    <item>
      <p>Dans la fenêtre <gui>Enregistrer la capture d'écran</gui>, saisissez un nom de fichier, choisissez un dossier et cliquez sur <gui>Enregistrer</gui>.</p>
      <p>Vous pouvez aussi importer la capture d'écran directement dans un éditeur d'image sans avoir besoin de la sauvegarder préalablement. Cliquez sur <gui>Copier dans le presse-papiers</gui> puis collez l'image dans l'autre application, ou faites glisser la miniature de la capture jusqu'à l'application.</p>
    </item>
  </steps>

  <section id="keyboard-shortcuts">
    <title>Raccourcis clavier</title>

    <p>Vous pouvez prendre rapidement une capture d'écran du bureau, d'une fenêtre ou d'une zone de l'écran en utilisant ces raccourcis clavier :</p>

    <list style="compact">
      <item>
        <p><key>Impr. écran</key> pour prendre une capture d'écran du bureau.</p>
      </item>
      <item>
        <p><keyseq><key>Alt</key><key>Impr. écran</key></keyseq> pour prendre une capture d'écran d'une fenêtre.</p>
      </item>
      <item>
        <p><keyseq><key>Maj</key><key>Impr. écran</key></keyseq> pour prendre une capture d'écran d'une zone choisie.</p>
      </item>
    </list>

    <p>Quand vous utilisez un raccourci clavier, l'image est automatiquement sauvegardée dans votre dossier <file>Images</file> avec un nom de fichier commençant par <file>Capture</file> et incluant la date et l'heure à laquelle elle a été prise.</p>
    <note style="note">
      <p>Si vous ne possédez pas de dossier <file>Images</file>, elles sont enregistrées dans votre répertoire personnel.</p>
    </note>
    <p>Vous pouvez aussi maintenir appuyée la touche <key>Ctrl</key> avec n'importe-lequel des raccourcis ci-dessus afin de copier la capture d'écran vers le presse-papiers au lieu de la sauvegarder.</p>
  </section>

</section>

<section id="screencast">
  <title>Faire une vidéo d'écran</title>

  <p>Vous pouvez faire une vidéo d'écran qui enregistre ce qui ce passe à l'écran :</p>

  <steps>
    <item>
      <p>Appuyez sur les touches <keyseq><key>Ctrl</key><key>Alt</key><key>Maj</key><key>R</key></keyseq> pour commencer l'enregistrement de ce qui se passe sur votre écran.</p>
      <p>Un rond rouge est affiché dans le coin supérieur droit de l'écran quand l'enregistrement est en cours.</p>
    </item>
    <item>
      <p>Une fois que vous avez fini, appuyez de nouveau sur les touches <keyseq><key>Ctrl</key><key>Alt</key><key>Maj</key><key>R</key></keyseq> pour arrêter l'enregistrement.</p>
    </item>
    <item>
      <p>La vidéo est automatiquement sauvegardée dans votre dossier <file>Vidéo</file> avec un nom commençant par <file>Capture d'écran vidéo</file> et incluant la date et l'heure à laquelle elle a été prise.</p>
    </item>
  </steps>

  <note style="note">
    <p>Si vous ne possédez pas de dossier <file>Vidéo</file>, elles sont enregistrées dans votre répertoire personnel.</p>
  </note>

</section>

</page>
