<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="fs-info" xml:lang="fr">
  <info>
    <revision version="0.1" date="2014-01-27" status="review"/>
    <link type="guide" xref="index" group="filesystems"/>
    <link type="seealso" xref="fs-device"/>
    <link type="seealso" xref="units"/>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>Description des champs affichés dans l'onglet <gui>Système de fichiers</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>naybnet</mal:name>
      <mal:email>naybnet@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leonor Palazzo</mal:name>
      <mal:email>leonor.palazzo@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Signification des informations du système de fichiers</title>
  
  <p>L'onglet Système de fichiers affiche les disques qui sont branchés à l'ordinateur et indique le nom et la quantité d'espace disponible pour chacun d'eux.</p>
  
  <list>
    <item>
      <p><gui>Périphérique</gui> donne la liste des noms que le système d'exploitation a attribué aux disques. Chaque composant informatique relié à l'ordinateur possède une entrée dans le répertoire <file>/dev</file>, utilisée pour l'identifier. Pour les disques durs, cela ressemble à <file>/dev/sda</file>.</p>
    </item>
  
    <item>
      <p><gui>Répertoire</gui> indique l'endroit où le disque ou la partition est <em>monté</em>. Le montage est le terme technique qui désigne la connexion d'un disque ou d'une partition permettant de l'utiliser. Même si un disque est branché physiquement à l'ordinateur, l'accès aux fichiers qu'il contient n'est pas possible s'il n'est pas monté. Quand un disque est monté, il est lié à un dossier par lequel il est possible d'accéder aux fichiers du disque. Par exemple, si dans <gui>Répertoire</gui> figure <file>/media/disk</file>, vous pouvez accéder aux fichiers en allant dans le répertoire <file>/media/disk</file> de votre ordinateur.</p>
    </item>
    
    <item>
      <p><gui>Type</gui> indique le type de <em>système de fichiers</em> utilisé sur le disque ou la partition. Un système de fichiers spécifie la manière dont l'ordinateur doit stocker les fichiers sur le disque. Certains systèmes d'exploitation ne peuvent lire que certains types de système de fichiers, donc il peut être utile de vérifier quel système de fichiers est utilisé par exemple par un disque dur externe.</p>
    </item>
    
    <item>
      <p><gui>Total</gui> affiche la capacité totale du disque et <gui>Disponible</gui> montre quelle quantité d'espace est disponible pour utilisation par les fichiers et programmes. <gui>Utilisé</gui> montre la quantité d'espace disque déjà utilisée.</p>
    </item>
    
  </list>

  <p>La somme de Disponible et Utilisé n'est pas forcément égale à l'espace total. Ceci parce que de l'espace disque peut être réservé pour une utilisation par le système. La colonne importante à regarder est l'espace disponible, qui indique combien d'espace disque vous pouvez réellement utiliser.</p>
  <p>Il se peut aussi que l'espace total ne corresponde pas à la capacité annoncée de votre disque dur. Cela est normal et se produit pour plusieurs raisons. La première est que les constructeurs n'utilisent pas la même méthode de mesure de la capacité des disques que tout le monde. Une autre raison est que de l'espace disque peut être réservé pour une utilisation par le système d'exploitation. S'il y a une grande différence avec la capacité attendue, ce peut être parce que votre disque a été divisé en plusieurs partitions.</p>

</page>
