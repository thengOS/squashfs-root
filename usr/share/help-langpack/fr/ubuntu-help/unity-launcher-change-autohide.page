<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="unity-launcher-change-autohide" xml:lang="fr">
  <info>
    <link type="guide" xref="unity-launcher-intro#launcher-customizing" group="#last"/>

    <revision version="13.10" date="2013-10-23" status="outdated"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>

    <desc>Afficher le <gui>Lanceur</gui> uniquement lorsque vous en avez besoin.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Masquage automatique de la barre de lanceurs</title>

  <p>Vous pouvez masquer le <gui>Lanceur</gui> si vous voulez le voir seulement lorsque vous déplacez votre souris ou le pointeur du pavé tactile soit sur le côté gauche, soit dans le coin supérieur gauche de l'écran.</p>

  <steps>
    <item><p>Cliquez sur le <gui>menu système</gui> en haut à droite de la <gui>barre de menu</gui> et sélectionnez <gui>Paramètres système</gui>.</p></item>
    <item><p>Dans la section Personnel, cliquez sur <gui>Apparence</gui>.</p></item>
    <item><p>Basculez sur l'onglet <gui>Comportement</gui>.</p></item>
    <item><p>Activer <gui>Cacher automatiquement le lanceur</gui></p></item>
    <item><p>Sélectionnez le <gui>côté gauche</gui> ou le <gui>coin supérieur gauche</gui> pour révéler le <gui>Lanceur</gui>.</p></item>
  </steps>

  <p>Pour éviter que vous affichiez involontairement la barre de lanceurs, Ubuntu exige que vous poussiez un peu plus fort avec le pointeur de votre souris ou de votre pavé tactile pour faire apparaître la barre de lanceurs. Vous pouvez ajuster la force nécessaire en réglant la <gui>Sensibilité d'affichage</gui> plus basse ou plus haute.</p>

</page>
