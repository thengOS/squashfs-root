<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="wacom-stylus" xml:lang="fr">

  <info>
    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <link type="guide" xref="wacom"/>
    <revision pkgversion="3.5.5" version="0.1" date="2012-08-15" status="review"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
		<include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Définir les fonctions des boutons et la sensibilité de pression du stylet Wacom.</desc>

  </info>

  <title>Configurer le stylet</title>

<steps>
  <item><p>Cliquez sur l'icône la plus à droite de la <gui>barre de menu</gui> et sélectionnez <gui>Paramètres système…</gui>.</p></item>
  <item><p>Ouvrez <gui>Tablette graphique Wacom</gui>.</p>
     <note style="tip"><p>Si aucune tablette n'est détectée, il est écrit <gui>Veuillez connecter ou allumer votre tablette Wacom</gui>.</p></note>
  </item>
  <item><p>La partie inférieure du panneau contient des informations et des paramètres spécifiques à votre stylet, avec le nom du périphérique (la catégorie du stylet) et le diagramme sur la gauche. Ces paramètres peuvent être ajustés :</p>
    <list>
      <item><p><gui>Pression ressentie sur la gomme :</gui> utilisez le curseur pour ajuster la « sensibilité » (comment la pression physique est traduite en valeurs numériques) entre <gui>Doux</gui> et <gui>Ferme</gui>.</p></item>
      <item><p>Configuration des <gui>boutons et molette de défilement</gui> (elles changent pour tenir compte du stylet). Cliquez sur le menu à côté de chaque étiquette pour sélectionner l'une de ces fonctions : aucune action, clic sur le bouton gauche de la souris, clic sur le bouton du milieu de la souris, clic sur le bouton droit de la souris, défilement vers le haut, défilement vers le bas, défilement vers la gauche, défilement vers la droite, précédent, suivant.</p></item>
      <item><p><gui>Pression ressentie sur la pointe :</gui> utilisez le curseur pour ajuster la « sensibilité » entre <gui>Doux</gui> et <gui>Ferme</gui>.</p></item>
    </list>
  </item>
</steps>

<note style="tip"><p>Si vous avez plus d'un stylet, lorsque le stylet supplémentaire se rapproche de la tablette, un téléavertisseur (« pager ») sera affiché à côté du nom de périphérique du stylet. Utilisez le pager pour choisir quel stylet configurer.</p>
</note>

</page>
