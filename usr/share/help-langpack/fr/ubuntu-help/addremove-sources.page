<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="addremove-sources" xml:lang="fr">

  <info>
    <credit type="author">
      <name>L'équipe de documentation d'Ubuntu</name>
    </credit>
    <desc>Ajouter d'autres dépôts pour étendre les sources de logiciels qu'Ubuntu utilise pour les installations et mises à jour.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="addremove"/>
    <revision version="16.04" date="2016-03-13" status="review"/>
  </info>

  <title>Ajouter des sources de logiciels supplémentaires</title>

  <p>Des logiciels sont disponibles à partir de sources tierces, ainsi qu'à partir des sources de logiciels par défaut d'Ubuntu. Si vous souhaitez installer un logiciel à partir d'une source tierce, vous devez l'ajouter à la liste des dépôts d'Ubuntu.</p>

  <note style="warning">
    <p>Ajoutez seulement des dépôts de logiciels provenant de sources en lesquelles vous avez confiance !</p>
    <p>La sécurité et la fiabilité des dépôts de logiciels tiers ne sont pas vérifiées par les membres d'Ubuntu. Il est possible qu'un logiciel de ces dépôts puisse endommager votre ordinateur.</p>
  </note>

  <steps>
    <title>Installer d'autres dépôts</title>
    <item>
      <p>Cliquez sur l’icône de <app>Logiciels Ubuntu</app> dans le <gui>Lanceur</gui> ou recherchez <input>Logiciels</input> dans le <gui>Tableau de bord</gui>.</p>
    </item>
    <item>
      <p>Lorsque <app>Logiciels Ubuntu</app> se lance, cliquez sur <guiseq><gui>Modifier</gui> <gui>Logiciels &amp; mises à jour</gui></guiseq></p>
    </item>
    <item>
      <p>Vous allez être invité à entrer votre mot de passe. Ceci fait, basculez sur l'onglet <gui>Autres logiciels</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Ajouter…</gui> et entrez la ligne APT du dépôt. Celle-ci devrait être disponible à partir du site web du dépôt, et devrait être similaire à :</p>
      <p>
        <code its:translate="no">deb http://archive.ubuntu.com/ubuntu/ xenial main</code>
      </p>
    </item>
    <item>
      <p>
        Click <gui>Add Source</gui> then close the <app>Software &amp; Updates</app> window. 
        <app>Ubuntu Software</app> will then check your software sources for new 
        updates.
      </p>
    </item>
  </steps>

  <section id="canonical-partner">
    <title>Activer le dépôt des partenaires de Canonical</title>
    <p>
      The Canonical Partner repository offers some proprietary applications 
      that don't cost any money to use but are closed source. They include 
      software like <app>Skype</app>, <app>Adobe Reader</app> and <app>Adobe 
      Flash Plugin</app>. Software in this repository will appear in
      <app>Ubuntu Software</app> search results but won't be installable until this 
      repository is enabled.
    </p>
    <p>Pour activer ce dépôt, suivez les étapes ci-dessus pour ouvrir l'onglet <gui>Autres logiciels</gui> dans <app>Logiciels &amp; mises à jour</app>. Si vous voyez le dépôt <gui>Partenaires de Canonical</gui> dans la liste, assurez-vous qu'il soit coché puis fermez la fenêtre de <app>Logiciels &amp; mises à jour</app>. Si vous ne le voyez pas, cliquez sur <gui>Ajouter</gui> puis saisissez :</p>
    <p>
      <code its:translate="no">deb http://archive.canonical.com/ubuntu xenial partner</code>
    </p>
    <p>
      Click <gui>Add Source</gui> then close the <app>Software &amp; Updates</app> window. Wait a 
      moment for <app>Ubuntu Software</app> to download the repository information.
    </p>
  </section>

</page>
