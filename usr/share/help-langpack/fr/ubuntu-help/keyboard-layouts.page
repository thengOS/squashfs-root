<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="keyboard-layouts" xml:lang="fr">
  <info>
    <link type="guide" xref="prefs-language#textentry"/>
    <link type="guide" xref="keyboard" group="i18n"/>
    <link type="seealso" xref="keyboard-shortcuts-set"/>

    <revision pkgversion="3.8" version="0.3" date="2013-04-30" status="review"/>
    <revision version="16.04" date="2016-03-17" status="review"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>L'équipe de documentation d'Ubuntu</name>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ajouter des sources d'entrée et basculer entre elles.</desc>
  </info>

  <title>Utiliser des sources d'entrée alternatives</title>

  <p>Il existe des centaines d'agencements de claviers différents pour les différentes langues. Même pour une seule langue, il existe souvent de nombreux agencements de clavier, comme l'agencement Dvorak pour l'anglais. Vous pouvez modifier votre clavier afin qu'il se comporte comme un autre clavier, sans tenir compte des lettres et des symboles imprimés sur les touches. C'est utile si vous jonglez souvent entre plusieurs langues.</p>

  <p>Certaines langues, comme le chinois ou le coréen, ont besoin d'une méthode d'entrée plus complexe qu'une simple touche pour les tables de caractères. En conséquence, certaines des sources d'entrée que vous pouvez choisir permettent une telle méthode. En apprendre plus à ce sujet dans la section <link xref="#complex">Méthodes d'entrée complexes</link>.</p>

  <section id="add">
    <title>Ajouter des sources d'entrée</title>

    <note style="sidebar">
      <p>Vous pouvez prévisualiser une image de n'importe quel agencement en la sélectionnant dans la liste et en cliquant sur <gui><media type="image" src="figures/input-keyboard-symbolic.svg" width="16" height="16"> aperçu</media></gui>.</p>
    </note>

    <steps>
      <item>
       <p>Cliquez sur l'icône tout à droite de la barre de menu et sélectionnez <gui>Paramètres système…</gui>.</p>
      </item>
      <item>
       <p>Dans la section Personnel, cliquez sur <gui>Saisie de texte</gui></p>
      </item>
      <item>
       <p>Cliquez sur le bouton <gui>+</gui>, sélectionnez une source d'entrée et cliquez sur <gui>Ajouter</gui>.</p>
      </item>
    </steps>

    <p>La source par défaut est la source en tête de liste. Utilisez les boutons <gui>↑</gui> et <gui>↓</gui> pour faire monter et descendre les sources dans la liste.</p>

    <note style="tip">
      <p>Si vous sélectionnez une source avec une méthode d'entrée, vous pouvez cliquer sur <gui><media type="image" src="figures/input-preferences.png" width="18" height="18"> préférences</media></gui> pour accéder à la boîte de dialogue « Préférences » de cette méthode si disponible.</p>
    </note>
  </section>

  <section id="indicator">
    <title>Indicateur de source d'entrée</title>

    <p>Vous pouvez rapidement basculer entre les sources sélectionnées en utilisant l'indicateur de source d'entrée dans la barre de menu. Le menu affichera un bref identifiant pour la source actuelle, tel que <gui>Fr</gui> pour l'agencement standard en Français ou un symbole en cas de source qui utilise une méthode spéciale d'entrée, par exemple le Chinois (Chewing). Cliquez sur l'indicateur de source d'entrée et sélectionnez à partir du menu la source que vous souhaitez utiliser.</p>
  </section>

  <section id="shortcuts">
    <title>Raccourcis clavier</title>

    <p>Vous pouvez également utiliser les raccourcis clavier pour basculer rapidement entre vos sources d'entrées sélectionnées. Par défaut, le raccourcis pour basculer sur la source suivante est <keyseq><key xref="windows-key">Super</key><key>Espace</key></keyseq>, mais vous pouvez le modifier :</p>

    <steps>
      <item>
       <p>Cliquez sur l'icône tout à droite de la barre de menu et sélectionnez <gui>Paramètres système…</gui>.</p>
      </item>
      <item>
       <p>Dans la section Personnel, cliquez sur <gui>Saisie de texte</gui></p>
      </item>
      <item>
       <p>Cliquez sur la définition du raccourci actuelle sous le texte <gui>Basculer vers la source suivante en utilisant</gui>.</p>
      </item>
      <item>
       <p>Lorsque la définition du raccourci s'est transformée en <gui>Nouveau raccourci...</gui>, appuyez sur les touches que vous voulez utiliser en temps que nouveau raccourci.</p>
      </item>
    </steps>
  </section>

  <section id="windows">
    <title>Réglez la source d'entrée pour toutes les fenêtres ou individuellement pour chaque fenêtre</title>

    <p>Lorsque vous utilisez plusieurs sources, vous pouvez choisir que toutes les fenêtres utilisent la même source ou régler une source différente pour chaque fenêtre. Utiliser une source différente pour chaque fenêtre est utile, par exemple si vous êtes en train d'écrire un article dans une autre langue dans une fenêtre de traitement de texte. Votre sélection de source d'entrée sera mémorisée pour chaque fenêtre lorsque vous basculez entre elles.</p>

    <p>Par défaut, les nouvelles fenêtres utiliseront la source d'entrée par défaut. Vous pouvez choisir à la place qu'elles utilisent la source de la fenêtre que vous utilisiez en dernier.</p>
  </section>

  <section id="complex">
    <title>Méthodes d'entrée complexes</title>

    <p>Les structures logicielles recommandées pour les méthodes de saisie  sont <em>IBus</em> et <em>Fcitx</em>. Ce dernier est la structure logicielle par défaut dans Ubuntu pour le chinois, le japonais, le coréen et le vietnamien.</p>

    <p>Les options de sources d'entrée avec des méthodes d'entrée sont disponibles uniquement si le moteur de la méthode d'entrée respective (IM) est installé. Lorsque vous installez une langue, un moteur IM approprié est automatiquement installé, le cas échéant.</p>

    <p>Par exemple, pour se préparer à saisir en coréen (hangul) sur un système anglais, procédez comme suit :</p>

    <steps>
      <item>
        <p><link xref="prefs-language-install">Installez</link> le coréen. Un des paquets installés est <em>fcitx-hangul</em>, le moteur Hangul IM pour Fcitx.</p>
      </item>
      <item>
        <p>Fermez <gui>Prise en charge des langues</gui> et ouvrez-la de nouveau.</p>
      </item>
      <item>
        <p>Sélectionnez <em>fcitx</em> en tant que <gui>Méthode d'entrée clavier</gui>.</p>
      </item>
      <item>
        <p>Déconnectez-vous puis reconnectez-vous.</p>
      </item>
      <item>
       <p>Cliquez sur l'icône tout à droite de la barre de menu et sélectionnez <gui>Paramètres système…</gui>.</p>
      </item>
      <item>
       <p>Dans la section Personnel, cliquez sur <gui>Saisie de texte</gui></p>
      </item>
      <item>
       <p>Cliquez sur le bouton <gui>+</gui>, sélectionnez <em>Hangul (Fcitx)</em> et cliquez sur <gui>Ajouter</gui>.</p>
      </item>
    </steps>

    <p>Ceci rendra <em>Hangul</em> disponible dans l'indicateur de saisie Fcitx de la barre de menu (la conception diffère de l'équivalent IBus).</p>

    <p>Si vous préférez un autre moteur IM que celui qui est installé automatiquement lorsque vous installez une langue, vous pouvez <link xref="addremove-install">installer</link> le moteur IM IBus ou Fcitx de votre choix, séparément.</p>
  </section>

</page>
