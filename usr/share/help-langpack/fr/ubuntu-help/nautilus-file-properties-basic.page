<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-file-properties-basic" xml:lang="fr">

  <info>
    <link type="guide" xref="files" group="more"/>

   <desc>Afficher des informations basiques sur le fichier, définir les permissions et sélectionner les applications par défaut.</desc>

   <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
   <revision version="13.10" date="2013-09-15" status="review"/>

   <credit type="author">
     <name>Tiffany Antopolski</name>
     <email>tiffany@antopolski.com</email>
   </credit>
   <credit type="author">
     <name>Shaun McCance</name>
     <email>shaunm@gnome.org</email>
   </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>
  </info>
  <title>Propriétés de fichier</title>

  <p>Pour afficher les informations sur un fichier ou un dossier, faites un clic-droit dessus et choisissez <gui>Propriétés</gui>. Vous pouvez aussi sélectionner le fichier et utiliser le raccourci <keyseq><key>Alt</key><key>Entrée</key></keyseq>.</p>

  <p>La fenêtre propriétés donne des informations comme le type de fichier, sa taille et sa date de dernière modification. Si vous avez souvent besoin de ces données, vous pouvez les afficher comme <link xref="nautilus-list">colonnes de la vue en liste</link> ou comme <link xref="nautilus-display#icon-captions">libellés des icônes</link>.</p>

  <p>Les informations de l'onglet <gui>Général</gui> sont détaillées ci-dessous.</p>

<section id="basic">
 <title>Propriétés élémentaires</title>
 <terms>
  <item>
    <title><gui>Nom</gui></title>
    <p>Vous pouvez renommer le fichier en modifiant ce champ. Vous pouvez aussi le faire en dehors de cette fenêtre. Voir <link xref="files-rename"/>.</p>
  </item>
  <item>
    <title><gui>Type</gui></title>
    <p>Ce champ vous aide à identifier le type du document, comme par exemple un PDF, un texte ODT ou une image JPEG. Le type de fichier détermine, entre autres, les applications qui peuvent l'ouvrir (par exemple, un lecteur de musique ne peut pas lire un document image). Consultez <link xref="files-open"/> pour obtenir plus d'informations.</p>
  <p>Le <em>type MIME</em> du fichier est affiché entre parenthèses. C'est un standard utilisé par les ordinateurs pour identifier le type du fichier.</p>
  </item>

  <item>
    <title>Contenus</title>
    <p>Cette ligne ne s'affiche que pour les propriétés d'un dossier, pas d'un fichier. Elle donne le nombre d'éléments contenus dans le dossier. Si le dossier inclut d'autres dossiers, chacun d'eux est compté comme un seul élément même s'il contient lui-même d'autres éléments. Chaque fichier est aussi compté comme un élément. Si le dossier est vide, le contenu affiche <gui>aucun</gui>.</p>
  </item>

  <item>
    <title>Taille</title>
    <p>Cette ligne ne s'affiche que pour les propriétés d'un fichier, pas d'un dossier. La taille d'un fichier indique la place qu'il occupe sur le disque. Cela donne aussi une indication sur le temps qu'il faut pour le télécharger ou l'envoyer par courriel par exemple (de gros fichiers mettent plus de temps pour être envoyés ou être reçus).</p>
    <p>Les tailles peuvent être indiquées en octets, Ko, Mo ou Go ; ces trois dernières sont aussi indiquées en octets entre parenthèses. Techniquement, 1 Ko égale 1024 octets, 1 Mo égale 1024 Ko et ainsi de suite.</p>
  </item>

  <item>
    <title>Emplacement</title>
    <p>L'emplacement de chaque fichier sur votre ordinateur est indiqué par son <em>chemin absolu</em>. C'est l'« adresse » unique de ce fichier sur votre ordinateur, composée de tous les dossiers que vous devez traverser pour l'atteindre. Par exemple, si Pierre a un fichier nommé <file>CV.pdf</file> dans son dossier personnel, son emplacement est <file>/home/Pierre/CV.pdf</file>.</p>
  </item>

  <item>
    <title>Volume</title>
    <p>C'est le système de fichiers ou le périphérique où est stocké ce fichier. Cela vous indique l'endroit où il se trouve physiquement, par exemple sur un disque dur, un CD, ou un <link xref="nautilus-connect">partage réseau ou un serveur de fichier</link>. Si les disques durs sont divisés en plusieurs <link xref="disk-partitions">partitions</link>, les partitions vont aussi apparaître dans cette ligne.</p>
  </item>

  <item>
    <title>Espace libre</title>
    <p>Cette information n'est affichée que pour les dossiers. Elle indique le l'espace disponible sur le disque où se trouve le dossier. C'est utile pour savoir si le disque dur est plein.</p>
  </item>


  <item>
    <title>Dernier accès</title>
    <p>La date et l'heure du dernier accès au fichier.</p>
  </item>

  <item>
    <title>Dernière modification</title>
    <p>La date et l'heure de la dernière modification et enregistrement du fichier.</p>
  </item>
 </terms>
</section>

</page>
