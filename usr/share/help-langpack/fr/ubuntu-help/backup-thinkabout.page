<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-thinkabout" xml:lang="fr">

  <info>
    <link type="guide" xref="files#backup"/>
    <desc>Une liste des dossiers contenant des documents, des fichiers et des paramètres que vous aimeriez sans doute sauvegarder.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Où trouver les fichiers à sauvegarder ?</title>

  <p>Voici la liste des emplacements les plus courants de fichiers et paramètres importants que vous voudrez peut-être sauvegarder.</p>

<list>
 <item>
  <p>Fichiers personnels (documents, musique, photos et vidéos)</p>
  <p xmlns:its="http://www.w3.org/2005/11/its" its:locNote="translators: xdg    dirs are localised by package xdg-user-dirs and need to be translated.  You    can find the correct translations for your language here:    http://translationproject.org/domain/xdg-user-dirs.html">Ceux-ci sont habituellement stockés dans votre dossier personnel (<file>/home/votre_nom</file>). Ils peuvent être dans des sous-dossiers tels que Bureau, Documents, Images, Musique et Vidéos.</p>
  <p>Si votre média de sauvegarde possède suffisamment d'espace disponible (par exemple un disque dur externe), vous devez envisager de sauvegarder votre dossier personnel dans son intégralité. Pour savoir combien d'espace requiert votre dossier personnel, utilisez l'application <app>Analyseur d'utilisation des disques</app>.</p>
 </item>

 <item>
  <p>Fichiers cachés</p>
  <p>Tout fichier ou dossier dont le nom commence par un point (.) est masqué par défaut. Pour afficher les fichiers cachés, cliquez sur <guiseq><gui>Affichage</gui><gui>Afficher les fichiers cachés</gui></guiseq> ou pressez le raccourci clavier <keyseq><key>Ctrl</key><key>H</key></keyseq>. Vous pouvez copier ces fichiers dans une sauvegarde comme n'importe quel autre fichier.</p>
 </item>

 <item>
  <p>Réglages personnels (préférences du bureau, thèmes et paramètres des logiciels)</p>
  <p>La plupart des applications stockent leurs paramètres dans des dossiers cachés à l'intérieur de votre dossier personnel (voyez ci-dessus les explications sur les fichiers cachés).</p>
  <p>La plupart des paramètres de vos applications sont enregistrés dans les fichiers cachés <file>.config</file>, <file>.gconf</file>, <file>.gnome2</file> et <file>.local</file> dans votre dossier personnel.</p>
 </item>

 <item>
  <p>Préférences système</p>
  <p>Les paramètres pour les parties importantes du système ne sont pas enregistrés dans votre dossier personnel. Ils peuvent être stockés à plusieurs endroits mais la plupart le sont dans le dossier <file>/etc</file>. En général, vous n'avez pas besoin de sauvegarder ces fichiers pour un ordinateur personnel. Par contre, si vous utilisez un serveur, vous devriez sauvegarder les fichiers correspondant aux services en cours d'utilisation.</p>
 </item>
</list>

</page>
