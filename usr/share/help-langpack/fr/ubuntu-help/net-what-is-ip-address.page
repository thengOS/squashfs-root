<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="net-what-is-ip-address" xml:lang="fr">
  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <desc>Une adresse IP ressemble à un numéro de téléphone pour votre ordinateur.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Adresse IP</title>

  <p>« IP address » signifie <em>Internet Protocol address (adresse de Protocole Internet)</em>. Chaque périphérique connecté à un réseau (comme internet) en possède une.</p>

  <p>Une adresse IP peut être comparée à votre numéro de téléphone. Ce dernier est une suite de chiffres unique qui identifie votre combiné pour que d'autres personnes puissent vous appeler. De la même façon, une adresse IP est une suite de chiffres unique qui identifie votre ordinateur pour qu'il puisse envoyer et recevoir des données d'autres ordinateurs.</p>

  <p>Actuellement, la plupart des adresses IP se composent de quatre groupes de chiffres, chaque groupe étant séparé des autres par un point. Voici un exemple d'adresse IP : <code>192.168.1.42</code>.</p>

  <note style="tip"><p>Une adresse IP peut être <em>dynamique</em> ou <em>statique</em>. Chaque fois que votre ordinateur se connecte à un réseau, il reçoit temporairement une adresse IP dynamique. Les adresses IP statiques elles, sont fixes et ne changent pas. Les adresses IP dynamiques sont plus usuelles que les statiques - les adresses statiques ne sont utilisées que dans des cas spécifiques, comme pour l'administration d'un serveur par exemple.</p></note>
</page>
