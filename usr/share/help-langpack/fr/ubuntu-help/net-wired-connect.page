<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-wired-connect" xml:lang="fr">
  <info>
    <link type="guide" xref="net-wired"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Pour configurer la plupart des connexions câblées, il suffit de brancher un câble réseau.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Connexion à un réseau filaire (Ethernet)</title>

<p>Pour configurer la plupart des connexions réseaux, tout ce dont vous avez besoin est de brancher un câble réseau. L'icône réseau dans la barre de menu devrait clignoter pendant quelques secondes  et changera alors pour une icône de « prise réseau » quand vous serez connecté.</p>

<p>Si cela ne se passe pas de cette façon, commencez par vérifier que votre câble réseau est correctement branché. L'une de ses extrémités doit être branchée dans le port Ethernet rectangulaire de votre ordinateur, l'autre extrémité va dans un commutateur, la prise d'un routeur ou d'un connecteur réseau mural, ou quelque chose d'équivalent (en fonction de votre configuration réseau). Parfois, une lumière à côté du port Ethernet indique qu'il est connecté et actif.</p>

<note>
 <p>Vous ne pouvez pas brancher directement un ordinateur à un autre avec un câble réseau (du moins pas sans quelques paramétrages supplémentaires). La bonne manière de procéder est de brancher les deux ordinateurs sur un hub réseau, un routeur ou un commutateur.</p>
</note>

<p>Si vous n'êtes pas encore connecté, votre réseau ne supporte peut-être pas la configuration automatique (DHCP). Dans ce cas,  vous devrez <link xref="net-manual">le configurer manuellement</link>.</p>

</page>
