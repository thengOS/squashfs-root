<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-vpn-connect" xml:lang="fr">
  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-wired"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="16.04" date="2016-03-14" status="review"/>

    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Les VPN permettent une connexion à un réseau local en passant par internet. Apprendre à configurer une connexion VPN.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Connexion à un VPN</title>

<p>Un VPN (<em>Virtual Private Network</em> ou <em>Réseau Privé Virtuel</em>) est une façon de se connecter à un réseau local en passant par internet. Voici un exemple : admettons que vous vouliez vous connecter au réseau local de votre entreprise alors que vous êtes en déplacement professionnel. Recherchez d'abord une connexion internet disponible (comme à votre hôtel), et connectez-vous ensuite au VPN de votre entreprise. La connexion est identique à celle que vous avez à votre réseau d'entreprise quand vous êtes sur place, sauf qu'elle passe par le réseau internet de votre hôtel. Les connexions VPN sont normalement chiffrées pour empêcher les personnes ne possédant pas les identifiants de votre réseau local d'y accéder.</p>

<p>There are a number of different types of VPN. You may have to install some extra software depending on what type of VPN
you're connecting to. Find out the connection details from whoever is in charge of the VPN and see which <em>VPN client</em>
you need to use. Then, open <app>Ubuntu Software</app> and search for the <app>network-manager</app> package which works
with your VPN (if there is one) and install it.</p>

<note>
 <p>S'il n'existe pas de paquet « NetworkManager » pour votre type de VPN, vous devrez sans doute télécharger et installer un logiciel client chez le fournisseur qui a fourni le logiciel du VPN. Il y a certainement aussi une procédure à suivre pour le rendre opérationnel.</p>
</note>

<p>Une fois ceci fait, vous pouvez configurer votre connexion VPN :</p>

<steps>
 <item>
  <p>Cliquez sur le  <gui>menu réseau</gui> dans la barre de menu et sous  <gui>Connexions VPN</gui>, sélectionnez <gui>Configurer VPN</gui>.</p>
 </item>

 <item>
  <p>Cliquez sur <gui>Ajouter</gui> et choisissez votre type de connexion VPN.</p>
 </item>

 <item>
  <p>Cliquez sur <gui>Créer</gui> et suivez les instructions à l'écran en saisissant les informations qui vous sont demandées, comme votre nom d'utilisateur et votre mot de passe au fur et à mesure de votre avancement.</p>
 </item>

 <item>
  <p>Quand vous aurez terminé de configurer le VPN, cliquez sur <gui>menu réseau</gui> dans la barre de menu, allez dans <gui>Connexions</gui> et cliquez sur la connexion que vous venez de créer. Ceci tentera d'établir une connexion au VPN - l'icône du réseau changera lorsqu'il essaiera de se connecter.</p>
 </item>

 <item>
  <p>Si tout se passe bien, vous serez connecté avec succès au VPN. Sinon, vous devez vérifier les paramètres VPN que vous avez saisis. Pour cela, cliquez sur l'icône réseau, ouvrez <gui>Paramètres du réseau</gui> et allez à l'onglet <gui>VPN</gui>.</p>
 </item>

 <item>
  <p>Pour vous déconnecter du VPN, cliquez sur le menu réseau et sélectionnez <gui>Déconnecter</gui> sous le nom de la connexion à ce VPN.</p>
 </item>
</steps>

</page>
