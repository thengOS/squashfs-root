<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="clock-set" xml:lang="fr">

  <info>
    <link type="guide" xref="clock" group="#first"/>
    <link type="seealso" xref="clock-timezone"/>
    <desc>Mettre à jour l'heure/la date affichée en haut de l'écran.</desc>
    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Projet de documentation d'Ubuntu</name>
      <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Modifier la date et l'heure</title>

<if:choose>
  <if:when test="platform:unity">
  <p>Si la date et l'heure affichées dans la barre de menu sont incorrectes ou apparaissent dans un format inadéquat, vous pouvez les modifier :</p>
  </if:when>
  <p>Si la date et l'heure affichées dans la barre de menu sont incorrectes ou apparaissent dans un format inadéquat, vous pouvez les modifier :</p>
</if:choose>

<if:choose>
<if:when test="platform:unity">
<steps>
 <item>
  <p>Pour ajuster la date et l'heure, cliquez sur l'horloge située dans la <gui>barre de menu</gui> et sélectionnez <gui>Réglages de la date et de l'heure</gui>.</p>
 </item>
 <item>
  <p>Changez le fuseau horaire en cliquant sur la carte ou en saisissant le nom de votre ville dans la zone de texte <gui>Lieu</gui>.</p>
 </item>
 <item>
  <p>Par défaut, Ubuntu synchronise l'horloge avec une horloge très précise sur Internet, afin que vous n'ayez pas besoin de régler votre horloge manuellement.</p>
 </item>
</steps>
</if:when>
<steps>
 <item>
  <p>Cliquez sur l'horloge sur le côté droit de la barre du haut et sélectionnez <gui>Réglages de la date et de l'heure</gui>.</p>
 </item>
 <item>
  <p>Il vous sera peut-être demandé de <gui>Déverrouiller</gui> et de saisir votre <link xref="user-admin-explain">mot de passe administrateur</link>.</p>
 </item>
 <item>
  <p>Réglez la date et l'heure en cliquant sur les flèches pour choisir l'heure et les minutes. Vous pouvez choisir l'année, le mois et le jour à partir des listes déroulantes.</p>
 </item>
 <item>
  <p>Si vous souhaitez que l'horloge se règle automatiquement toute seule, activez la case <gui>Automatiquement depuis Internet</gui>.</p>
  <p>Lorsque la case <gui>Automatiquement depuis Internet</gui> est activée, l'ordinateur synchronise périodiquement son horloge avec une horloge Internet très précise et vous n'avez donc pas à le faire manuellement. Mais cette fonction ne peut être utilisée que si vous êtes connecté à internet.</p>
 </item>
 <item>
  <p>Vous pouvez également modifier la façon dont l'heure est affichée en sélectionnant le format <gui>Horloge sur 12 heures</gui> ou <gui>Horloge sur 24 heures</gui>.</p>
 </item>
</steps>
</if:choose>

</page>
