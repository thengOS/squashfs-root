<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-connect" xml:lang="fr">

  <info>
    <link type="guide" xref="files" group="more"/>
    <link type="guide" xref="sharing"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-06" status="review"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <desc>Afficher et modifier des fichiers sur un autre ordinateur par FTP, SSH, partage Windows ou WebDAV.</desc>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Consultation de fichiers sur un serveur ou un réseau partagé</title>

<p>Vous pouvez vous connecter à un serveur ou à un réseau pour y chercher et afficher des fichiers comme s'ils étaient sur votre propre ordinateur. C'est une bonne façon de télécharger ou téléverser des fichiers sur internet, ou de les partager avec les autres membres de votre réseau local.</p>

<p>Pour parcourir les fichiers sur le réseau, ouvrez l'application <app>Files</app> à partir du <gui>Tableau de bord</gui>, et cliquez sur <gui>Explorer le réseau</gui> dans le panneau latéral. Le gestionnaire de fichiers trouvera tous les ordinateurs sur votre réseau local qui affichent leur capacité à délivrer des fichiers. Si vous souhaitez vous connecter à un serveur sur Internet, ou si vous ne voyez pas l'ordinateur que vous recherchez, vous pouvez manuellement vous connecter à un serveur en saisissant son adresse Internet/réseau.</p>

<steps>
  <title>Connexion à un serveur de fichier</title>
  <item><p>Dans le gestionnaire de fichiers, cliquer sur <gui>Fichiers</gui> dans la barre de menu et choisissez <gui>Se connecter au serveur</gui> à partir du menu de l'application.</p></item>
  <item><p>Entrez l'adresse du serveur, sous la forme d'un <link xref="#urls">URL</link>. Les détails des URL pris en charge sont <link xref="#types">listés ci-dessous</link>.</p>
  <note>
    <p>Si vous vous êtes connecté au serveur précédemment, vous pouvez cliquer dessus dans la liste des <gui>Connexions récentes</gui>.</p>
  </note>
  </item>
  <item><p>Cliquez sur <gui>Se connecter</gui>. Une nouvelle fenêtre apparaîtra en vous montrant les fichiers sur le serveur. Vous pouvez parcourir les fichiers de la même manière que vous pourriez parcourir les fichiers de votre propre ordinateur. Le serveur sera également ajouté au panneau latéral de manière à ce que vous y accédiez rapidement dans le futur.</p>
  </item>
</steps>

<section id="urls">
 <title>Écrire des URL</title>

<p>Une <em>URL</em> ou <em>Uniform Resource Locator</em> est une forme d'adresse qui renvoie à un emplacement ou un fichier sur un réseau. L'adresse est formatée comme ceci :</p>
  <example>
    <p><sys>schéma://nomduserveur.exemple.com/dossier</sys></p>
  </example>
<p>Le <em>schéma</em> spécifie le protocole ou le type de serveur. La partie <em>exemple.com</em> de l'adresse est appelée <em>nom de domaine</em>. Si un nom d'utilisateur est requis, il est inséré avant le nom du serveur :</p>
  <example>
    <p><sys>schéma://nomdutilisateur@nomduserveur.exemple.com/dossier</sys></p>
  </example>
<p>Certains schémas exigent un numéro de port. Insérer-le après le nom de domaine :</p>
  <example>
    <p><sys>schéma://nomduserveur.exemple.com:port/dossier</sys></p>
  </example>
<p>Voici des exemples spécifiques pour les différents types de serveurs pris en charge.</p>
</section>

<section id="types">
 <title>Types de serveurs</title>

<p>Vous pouvez vous connecter à différents types de serveurs. Certains serveurs sont publics et permettent à quiconque de se connecter. D'autres serveurs ont besoin d'un nom d'utilisateur et d'un mot de passe pour vous connecter.</p>
<p>Il se peut que vous n'ayez pas toutes les autorisations pour intervenir sur les fichiers d'un serveur. Par exemple, sur des sites FTP publics, vous n'aurez probablement pas l'autorisation de supprimer des fichiers.</p>
<p>L'URL que vous entrez dépend du protocole utilisé par le serveur pour exporter ses partages de fichiers.</p>
<terms>
<item>
  <title>SSH</title>
  <p>Si vous avez un compte avec une <em>interface sécurisée</em> (SSH) sur un serveur, vous pouvez vous connecter en utilisant cette méthode. De nombreux hébergeurs offrent des comptes SSH à leurs membres afin qu'ils puissent télécharger des fichiers en toute sécurité. Les serveurs SSH exigent toujours de vous enregistrer.</p>
  <p>Une URL SSH typique ressemble à ceci :</p>
  <example>
    <p><sys>ssh://nomdutilisateur@nomduserveur.exemple.com/dossier</sys></p>
  </example>

  <p>Avec SSH, toutes vos données envoyées (y compris votre mot de passe) sont chiffrées et donc parfaitement invisibles pour les autres utilisateurs du réseau.</p>
</item>
<item>
  <title>FTP (avec identification)</title>
  <p>FTP est un protocole très répandu d'échange de fichiers par internet. Mais comme les échanges ne sont pas chiffrés avec FTP, de nombreux serveurs proposent maintenant l'accès via SSH. Cependant certains serveurs permettent ou exigent toujours le téléversement ou le téléchargement de fichiers via FTP. Les sites FTP avec identification vous autorisent habituellement à supprimer et téléverser des fichiers.</p>
  <p>Une URL FTP typique ressemble à ceci :</p>
  <example>
    <p><sys>ftp://nomdutilisateur@ftp.exemple.com/chemin/</sys></p>
  </example>
</item>
<item>
  <title>FTP public</title>
  <p>Les sites qui vous permettent de télécharger des fichiers fournissent parfois un accès FTP public ou anonyme. Ces serveurs ne nécessitent pas de nom d'utilisateur ou de mot de passe et ne vous permettront généralement pas de supprimer ou de télécharger des fichiers.</p>
  <p>Une URL FTP anonyme typique ressemble à ceci :</p>
  <example>
    <p><sys>ftp://ftp.exemple.com/chemin/</sys></p>
  </example>
  <p>Certains sites FTP anonymes nécessitent que vous vous connectiez avec un identifiant et mot de passe public, ou avec un nom d'utilisateur public en utilisant votre adresse électronique comme mot de passe. Pour ces serveurs, utilisez la méthode <gui>FTP (avec identification)</gui>, et utilisez les informations d'authentification spécifiées par le site FTP.</p>
</item>
<item>
  <title>Partage Windows</title>
  <p>Les ordinateurs Windows utilisent un protocole propriétaire pour le partage de fichiers sur un réseau local. Ils sont parfois groupés en <em>domaines</em> pour l'organisation et un meilleur contrôle de l'accès. Si vous disposez des autorisations sur l'ordinateur distant, vous pouvez vous connecter à un partage Windows à partir du gestionnaire de fichiers.</p>
  <p>Une URL de partage Windows typique ressemble à ceci :</p>
  <example>
    <p><sys>smb://nomduserveur/Partage</sys></p>
  </example>
</item>
<item>
  <title>WebDAV et WebDAV sécurisé</title>
  <p>Basé sur le protocole HTTP en vigueur sur le Web, WebDAV sert parfois à partager des données sur un réseau local et à enregistrer des fichiers sur internet. Si le serveur distant l'accepte, il est préférable d'établir une connexion sécurisée. Le WebDAV sécurisé utilise un chiffrement SSL très puissant, de sorte que personne ne peut voir votre mot de passe.</p>
  <p>Une URL WebDAV typique ressemble à ceci :</p>
  <example>
    <p><sys>http://exemple.nomdhote.com/chemin</sys></p>
  </example>
</item>
</terms>
</section>

</page>
