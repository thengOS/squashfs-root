<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="question" version="1.0 if/1.0" id="color-virtualdevice" xml:lang="fr">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-why-calibrate"/>
    <desc>Un périphérique virtuel est un périphérique dont la couleur est gérée mais qui n'est pas connecté à votre ordinateur.</desc>

    <revision version="13.10" date="2013-09-07" status="review"/>
    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Gestion des couleurs d'un périphérique virtuel</title>

  <p>Un périphérique virtuel est un périphérique dont la couleur est gérée mais qui n'est pas connecté à votre ordinateur. Voici des exemples possibles :</p>

  <list>
    <item><p>un magasin d'impression en ligne où vos photos sont déposées, imprimées et vous sont renvoyées,</p></item>
    <item><p>les photos d'un appareil photo numérique stockées sur une carte mémoire.</p></item>
  </list>

<if:choose>
<if:when test="platform:unity">
  <p>Pour créer un profil virtuel pour un appareil photo numérique, faites glisser et déposez un des fichiers image dans la boîte de dialogue <guiseq><gui>Paramètres système...</gui> <gui>Couleur</gui></guiseq>. Vous pouvez ensuite lui <link xref="color-assignprofiles">attribuer des profils</link> comme à n'importe quel autre périphérique ou même le <link xref="color-calibrate-camera">calibrer</link>.</p>
</if:when>
  <p>Pour créer un profil virtuel pour un appareil photo numérique, faites glisser et déposez un des fichiers image dans la boîte de dialogue <guiseq><gui>Paramètres système...</gui> <gui>Couleur</gui></guiseq>. Vous pouvez ensuite lui <link xref="color-assignprofiles">attribuer des profils</link> comme à n'importe quel autre périphérique ou même le <link xref="color-calibrate-camera">calibrer</link>.</p>
</if:choose>

</page>
