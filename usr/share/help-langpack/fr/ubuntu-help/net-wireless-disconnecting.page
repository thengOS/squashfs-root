<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="fr">
  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Avoir un signal faible, ou ne pas pouvoir se connecter correctement au réseau.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Interruptions intempestives du réseau sans fil</title>

<p>Il se pourrait que vous soyez déconnecté d'un réseau sans fil même si vous vouliez rester connecté. Votre ordinateur devrait normalement essayer de se reconnecter au réseau dès que cela est possible (l'icône réseau dans la barre de menu clignotera s'il est en train d'essayer de se reconnecter), mais cela peut être ennuyeux, spécialement si vous étiez en train d'utiliser Internet en même temps.</p>

<section id="signal">
 <title>Signal sans fil faible</title>

 <p>Un signal réseau sans fil faible est la principale cause de déconnexions intempestives. Les réseaux sans fil ont un rayon de capture limité, et si vous vous trouvez un peu trop éloigné de la station émettrice, le signal devient trop faible pour maintenir une connexion fiable. Des murs ou d'autres obstacles entre vous et l'émetteur peuvent aussi affaiblir le signal.</p>
 
 <p>L'icône réseau dans la barre de menu affiche la puissance du signal. Si le signal semble faible, essayez de vous positionner plus près du point d'accès sans fil.</p>
 
</section>

<section id="network">
 <title>Mauvaise connexion au réseau</title>

 <p>Il arrive parfois que lors de votre connexion à un réseau sans fil, il semble que vous soyez connecté correctement au début, mais vous êtes déconnecté aussitôt après. Ceci est dû à une connexion incomplète de votre ordinateur au réseau - il a fait son possible pour établir une connexion, mais pour une raison inconnue, il n'a pas pu faire aboutir la connexion et celle-ci a été interrompue.</p>

 <p>De probables raisons à ceci sont la saisie d'une phrase de passe erronée ou l'interdiction de votre ordinateur sur le réseau (un nom d'utilisateur manquant pendant l'identification par exemple).</p>

</section>

<section id="hardware">
 <title>Pilote ou périphérique réseau sans fil instables</title>

 <p>Certains équipements pour réseau sans fil peuvent être quelque peu instables. Les réseaux sans fil sont complexes et les cartes réseau comme les émetteurs peuvent connaître parfois des problèmes mineurs et perdre la connexion. C'est ennuyeux, mais cela arrive fréquemment avec beaucoup de périphériques. S'il vous arrive d'être déconnecté de temps en temps du réseau sans fil, ce peut être l'unique raison. Si cela vous arrive très fréquemment, il serait bon d'envisager de changer de matériel.</p>

</section>

<section id="busy">
 <title>Réseaux sans fil encombrés</title>

 <p>Dans certains endroits très fréquentés (comme dans les universités et les cybercafés par exemple), il y a souvent de très nombreux ordinateurs qui essaient de se connecter tous en même temps. Les réseaux deviennent alors surchargés et n'arrivent plus à traiter toutes ces connexions, il y a donc des ordinateurs qui se retrouvent déconnectés.</p>

</section>

</page>
