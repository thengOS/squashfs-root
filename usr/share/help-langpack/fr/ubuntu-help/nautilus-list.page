<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-list" xml:lang="fr">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-list"/>

    <desc>Contrôler les informations affichées dans les colonnes dans la vue liste.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>
  </info>

<title>Préférences des colonnes des listes</title>

<p>Il existe neuf colonnes d'informations affichables dans la vue en liste du gestionnaire de fichiers. Cliquez sur <gui>Fichiers</gui> dans la barre de menu, choisissez <gui>Préférences</gui> puis l'onglet <gui>Colonnes des listes</gui> pour sélectionner quelles colonnes seront visibles.</p>

<note style="tip">
<p>Utilisez les boutons <gui>Déplacer vers le haut</gui> et <gui>Déplacer vers le bas</gui> pour modifier l'ordre d'affichage des colonnes.</p>
</note>
<terms>
 <item>
  <title><gui>Nom</gui></title>
  <p>Le nom des dossiers et des fichiers dans le dossier affiché.</p>
 </item>
 <item>
  <title><gui>Taille</gui></title>
  <p>La taille d'un dossier indique le nombre d'éléments contenus dans ce dossier. La taille d'un fichier est exprimée en octets, Ko ou Mo.</p>
 </item>
 <item>
  <title><gui>Type</gui></title>
  <p>Affiché sous la dénomination « dossier » ou par type de fichier comme « document PDF », « image JPEG », « audio MP3 » ou d'autres.</p>
 </item>
 <item>
  <title><gui>Dernière modification</gui></title>
  <p>Shows when the file was last modified.</p>
 </item>
 <item>
  <title><gui>Propriétaire</gui></title>
  <p>Le nom du propriétaire du dossier ou du fichier.</p>
 </item>
 <item>
  <title><gui>Groupe</gui></title>
  <p>Le groupe auquel appartient le fichier. Sur un ordinateur personnel, chaque utilisateur est dans son propre groupe. Les groupes sont parfois utilisés dans un environnement professionnel où les utilisateurs peuvent être réunis en fonction de leur service d'appartenance ou d'un projet commun.</p>
 </item>
  <item>
  <title><gui>Permissions</gui></title>
  <p>Affiche les permissions d'accès au fichier, par exemple <gui>drwxrw-r--</gui></p>
  <list>
   <item>
    <p>Le premier caractère est le type de fichier. <gui>-</gui> signifie fichier standard et <gui>d</gui> veut dire répertoire (dossier).</p>
   </item>
   <item>
     <p>Les trois caractères suivants <gui>rwx</gui> indiquent les permissions spécifique au propriétaire du fichier.</p>
   </item>
   <item>
     <p>Les trois suivants <gui>rw-</gui> indiquent les permissions accordées à tous les membres du groupe propriétaire du fichier.</p>
   </item>
   <item>
    <p>Les trois derniers <gui>r--</gui> de la colonne indiquent les permissions accordées à tous les autres utilisateurs du système.</p>
   </item>
  </list>
  <p>Voici la signification de chaque caractère :</p>
  <list>
    <item><p>r : permission en lecture.</p></item>
    <item><p>w : permission en écriture.</p></item>
    <item><p>x : permission d'exécution.</p></item>
    <item><p>- : aucune permission.</p></item>
    </list>
 </item>
 <item>
  <title><gui>Type MIME</gui></title>
  <p>Affiche le type MIME de l'élément.</p>
 </item>
 <item>
  <title><gui>Emplacement</gui></title>
  <p>Le chemin vers l'emplacement du fichier.</p>
 </item>
</terms>

</page>
