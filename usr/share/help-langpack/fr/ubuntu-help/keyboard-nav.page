<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="tip" id="keyboard-nav" xml:lang="fr">
  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>L'équipe de documentation d'Ubuntu</name>
       <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>

    <revision version="14.04" date="2014-03-07" status="review"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Utiliser les applications et le bureau sans souris.</desc>
  </info>

  <title>Navigation au clavier</title>

  <p>Cette page détaille la navigation au clavier pour les gens qui ne peuvent utiliser ni souris ni d'autres outils de pointage, ou qui souhaitent utiliser le clavier autant que possible. Pour des raccourcis claviers utiles à tous les utilisateurs, consultez plutôt <link xref="shell-keyboard-shortcuts"/>.</p>

  <note style="tip">
    <p>Si vous avez des problèmes à utiliser la souris ou d'autres périphériques de pointage, il est possible de contrôler le pointeur de la souris en utilisant le pavé numérique du clavier. Consultez <link xref="mouse-mousekeys"/> pour obtenir des informations.</p>
  </note>

<table frame="top bottom" rules="rows">
  <title>Naviguer parmi les interfaces utilisateur</title>
  <tr>
    <td><p><key>Tab</key> et <keyseq><key>Ctrl</key><key>Tab</key></keyseq></p></td>
    <td>
      <p>Déplacer le focus du clavier entre les différents contrôles. <keyseq><key>Ctrl</key> <key>Tab</key></keyseq> change de groupe de contrôles, par exemple du panneau latéral vers le contenu principal. <keyseq><key>Ctrl</key><key>Tab</key></keyseq> peut aussi faire sortir d'un contrôle qui utilise <key>Tab</key> lui-même, comme par exemple une zone de texte.</p>
      <p>Maintenez appuyée la touche <key>Maj</key> pour déplacer le focus en sens inverse.</p>
    </td>
  </tr>
  <tr>
    <td><p>Flèches du clavier</p></td>
    <td>
      <p>Changez la sélection d'éléments au sein d'un contrôle, ou dans un ensemble de contrôles reliés. Utilisez les flèches du clavier pour mettre en évidence les boutons d'une barre d'outils, sélectionner les éléments dans une vue en liste ou en icônes ou sélectionner un bouton radio dans un groupe.</p>
      <p>Dans une vue arborescente, utilisez les flèches gauche et droite pour étendre et réduire les éléments avec enfants.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Touches fléchées</key></keyseq></p></td>
    <td><p>Dans une vue en liste ou en icônes, déplacez le focus du clavier vers un autre élément sans changer l'élément sélectionné.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Maj</key><key>Touches fléchées</key></keyseq></p></td>
    <td><p>Dans une vue en liste ou en icônes, sélectionnez tous les éléments depuis l'élément sélectionné vers le nouvel élément mis en évidence.</p></td>
  </tr>
  <tr>
    <td><p><key>Barre d'espace</key></p></td>
    <td><p>Activez un élément mis en évidence, comme par exemple un bouton, une case à cocher ou un élément de liste.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Barre d'espace</key></keyseq></p></td>
    <td><p>Dans une vue en liste ou en icônes, sélectionnez ou désélectionnez l'élément mis en évidence sans désélectionner les autres éléments.</p></td>
  </tr>
  <tr>
    <td><p><key>Alt</key></p></td>
    <td><p>Maintenez enfoncée la touche <key>Alt</key> pour révéler les <em>accélérateurs</em> : les lettres soulignées sur les éléments du menu, les boutons et les autres contrôles. Appuyez sur <key>Alt</key> plus la lettre soulignée pour activer un contrôle, comme si vous aviez cliqué dessus.</p></td>
  </tr>
  <tr>
    <td><p><key>Échap</key></p></td>
    <td><p>Sortir d'un menu, d'une fenêtre surgissante ou d'une boîte de dialogue.</p></td>
  </tr>
  <tr>
    <td><p><key>F10</key></p></td>
    <td><p>Ouvrez le premier menu dans la barre de menu d'une fenêtre. Utilisez les touches fléchées pour naviguer dans les menus.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Maj</key><key>F10</key></keyseq> ou la touche Menu</p></td>
    <td>
      <p>Affiche le menu contextuel pour la sélection en cours, comme si vous aviez effectué un clic-droit.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>F10</key></keyseq></p></td>
    <td><p>Dans le gestionnaire de fichiers, affiche le menu contextuel du dossier en cours, comme si vous aviez effectué un clic-droit sur l'arrière-plan et pas sur un élément.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Page haut</key></keyseq> et <keyseq><key>Ctrl</key><key>Page Bas</key></keyseq></p></td>
    <td><p>Dans une interface à onglets, bascule vers l'onglet de gauche ou de droite.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Déplacement dans le bureau</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-tick"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-updown"/>
</table>

<table frame="top bottom" rules="rows">
  <title>Gestion des fenêtres</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>Fermer la fenêtre actuelle.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key xref="windows-key">Super</key><key>↓</key></keyseq></p></td>
    <td><p>Restaure une fenêtre maximisée à sa taille originale.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
    <td><p>Déplacer la fenêtre actuelle. Appuyez sur <keyseq><key>Alt</key><key>F7</key></keyseq>, puis utilisez les touches fléchées pour déplacer la fenêtre. Appuyez sur <key>Entrée</key> pour terminer le déplacement, ou sur <key>Échap</key> pour la remettre à son point de départ.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
    <td><p>Redimensionner la fenêtre actuelle. Appuyez sur <keyseq><key>Alt</key><key>F8</key></keyseq>, puis utilisez les touches fléchées pour redimensionner la fenêtre. Appuyez sur <key>Entrée</key> pour terminer le redimensionnement, ou sur <key>Échap</key> pour la remettre à sa taille d'origine.</p></td>
  </tr>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-shift-updown"/>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key xref="windows-key">Super</key><key>↑</key></keyseq></p></td>
    <td><p><link xref="shell-windows-maximize">Maximiser</link> une fenêtre.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key xref="windows-key">Super</key><key>←</key></keyseq></p></td>
    <td><p>Maximiser une fenêtre verticalement le long de la partie gauche de l'écran.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key xref="windows-key">Super</key><key>→</key></keyseq></p></td>
    <td><p>Maximiser une fenêtre verticalement le long de la partie droite de l'écran.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Barre d'espace</key></keyseq></p></td>
    <td><p>Affiche le menu de la fenêtre, comme avec un clic droit sur la barre de titre.</p></td>
  </tr>
</table>

</page>
