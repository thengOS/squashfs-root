<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="video-dvd" xml:lang="fr">

  <info>
    <link type="guide" xref="media#videos"/>

    <desc>Les bons codecs ne sont peut-être pas installés, ou le DVD n'est peut-être pas de la bonne zone.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-10-23" status="review"/>    
    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Pourquoi les DVD ne se lisent-ils pas ?</title>

  <p>Si vous insérez un DVD dans votre ordinateur et qu'il ne fonctionne pas, il se peut que vous n'ayez pas les codecs DVD nécessaires installés, ou que votre DVD n'appartienne pas à la bonne <em>zone</em>.</p>

<section id="codecs">
 <title>Installation des codecs nécessaires pour la lecture de DVD</title>
 <p>Pour lire des DVD, les <em>codecs</em> nécessaires doivent être installés. Un codec est un élément logiciel qui permet aux applications de lire les formats audio ou vidéo. Si vous essayez de lire un DVD mais n'avez pas les codecs appropriés, le lecteur vidéo devrait vous en avertir et vous proposer leur installation.</p>

  <p>Les DVD sont également protégés contre la copie par un système nommé CSS. Celui-ci empêche la copie du DVD, mais peut également bloquer leur lecture, à moins de disposer d'un <link xref="video-dvd-restricted">logiciel complémentaire</link> qui gère cette protection contre la copie.</p>
</section>

<section id="region">
 <title>Vérification de la zone de votre DVD</title>
  <p>Les DVD possèdent un code de <em>zone</em> qui définit dans quels pays du monde ils sont autorisés à être lus. Si la zone du lecteur vidéo de votre ordinateur ne correspond pas à la zone du DVD que vous essayez de lire, vous ne pourrez pas lire le DVD. Par exemple, si vous possédez un lecteur vidéo de zone 1, vous ne pourrez lire que les DVD d'Amérique du Nord.</p>

  <p>Il est souvent possible de changer le code régional de votre lecteur DVD, mais au bout d'un petit nombre de changements il se bloquera à une région de manière permanente. Pour changer le code régional du lecteur DVD de votre ordinateur, utilisez <link href="apt:regionset">regionset</link>.</p>
</section>
	
</page>
