<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="fr">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision version="14.10" date="2014-09-14" status="review"/>

    <desc>Ajouter, supprimer et renommer les signets du gestionnaire de fichiers.</desc>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Modification des signets d'un dossier</title>
<p>Vos signets sont répertoriés dans la barre latérale du gestionnaire de fichiers.</p>

    <steps>
    <title>Ajout d'un signet :</title>
    <item><p>Ouvrez le dossier (ou l'emplacement) que vous voulez mettre en signet.</p></item>
    <item><p>Cliquez sur <gui>Signets</gui> dans la barre de menu et sélectionnez <gui>Ajouter un signer pour cet emplacement</gui>.</p></item>
    </steps>

  <steps>
  <title>Suppression d'un signet :</title>
   <item><p>Cliquez sur <gui>Signets</gui> dans la barre de menu et sélectionnez <gui>Signets...</gui> à partir du menu de l'application.</p></item>
   <item><p>Dans la fenêtre <gui>Signets</gui>, sélectionnez le signet que vous souhaitez supprimer et cliquez sur le bouton <key>-</key>.</p></item>
  </steps>

    <steps>
    <title>Renommage un signet :</title>
    <item><p>Cliquez sur <gui>Fichiers</gui> dans la barre supérieure et choisissez <gui>Signets</gui> à partir du menu de l'application.</p></item>
    <item><p>Dans la fenêtre <gui>Signets</gui>, sélectionnez le signet que vous souhaitez renommer.</p></item>
    <item><p>Saisissez le nouveau nom dans le champ <gui>Nom</gui>.</p>
    <note>
     <p>Renommer un signet ne modifie pas le nom du dossier. Si vous avez mis en signets deux dossiers différents dans deux emplacements distincts portant le même nom, les signets auront le même nom, vous ne pouvez pas les différencier. Dans ce cas, il est utile de ne pas donner le même nom au dossier et au signet qui le désigne.</p>
    </note>
</item>
    </steps>

</page>
