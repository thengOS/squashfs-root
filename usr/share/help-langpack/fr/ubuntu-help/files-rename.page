<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename" xml:lang="fr">

  <info>
    <link type="guide" xref="files"/>
    <desc>Modifier le nom d'un fichier ou d'un dossier.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision version="14.10" date="2014-09-14" status="review"/>

    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Modification d'un nom de fichier ou de dossier</title>

  <p>Vous pouvez utiliser le gestionnaire de fichiers pour changer le nom d'un fichier ou d'un dossier.</p>
  <steps>
    <title>Pour renommer un fichier ou un dossier :</title>
    <item><p>Faites un clic droit sur l'élément et choisissez <gui>Renommer</gui>, ou sélectionnez-le et appuyez sur <key>F2</key>.</p></item>
    <item><p>Saisissez le nouveau nom et appuyez sur <key>Entrée</key>.</p></item>
  </steps>

  <p>Vous pouvez aussi modifier le nom d'un fichier dans la fenêtre <link xref="nautilus-file-properties-basic">propriétés</link>.</p>

  <p>Quand vous renommez un fichier, seule la première partie de son nom est sélectionnée, pas l'extension (la partie derrière le « . »). L'extension décrit normalement le type de fichier (par exemple <file>fichier.pdf</file> est un document PDF) et normalement, vous ne voulez pas la modifier. Si malgré tout, vous souhaitez modifier aussi l'extension, sélectionnez le nom complet et modifiez-le.</p>

  <note style="tip">
    <p>Si vous renommez le mauvais fichier ou si vous vous trompez dans le nommage, vous pouvez annuler le renommage. Pour annuler l'action et restaurer le nom précédent, cliquez immédiatement sur <gui>Édition</gui> dans la barre de menu et sélectionnez <gui>Annuler le renommage</gui>.</p>
  </note>

  <section id="valid-chars">
    <title>Caractères valides pour les noms de fichier</title>
    <p>Vous pouvez utiliser n'importe quel caractère sauf le <key>/</key> (barre oblique ou slash) dans les noms de fichiers. Certains périphériques utilisent cependant <em>un système de fichiers</em> plus restrictif sur la composition du nom. Par conséquent, il est préférable d'éviter les caractères suivants dans la composition des noms de fichiers : <key>|</key>, <key>\</key>, <key>?</key>, <key>*</key>, <key>&lt;</key>, <key>"</key>, <key>:</key>, <key>&gt;</key>, <key>/</key>.</p>

    <note style="warning">
    <p>Si vous mettez un <key>.</key> comme premier caractère d'un nom de fichier, le fichier est alors <link xref="files-hidden">caché</link> lorsque vous tentez de l'afficher dans le gestionnaire de fichiers.</p>
    </note>
  </section>

  <section id="common-probs">
    <title>Problèmes connus</title>
    <terms>
      <item>
        <title>Le nom du fichier est déjà utilisé</title>
        <p>Il ne peut pas y avoir deux fichiers portant le même nom dans le même dossier. Si vous renommez un fichier en lui donnant un nom de fichier qui existe déjà dans le dossier, le gestionnaire de fichiers ne l'accepte pas.</p>
        <p>Les noms de fichier et de dossier sont sensibles à la casse donc le nom de fichier <file>Fichier.txt</file> n'est pas le même que <file>fichier.txt</file>. L'utilisation de noms différents de ce type est autorisé, même si ce n'est recommandé.</p>
      </item>
      <item>
        <title>Le nom de fichier est trop long</title>
        <p>Sur certains systèmes, les noms de fichiers ne peuvent pas avoir plus de 255 caractères. Cette limite inclut à la fois le nom du fichier et l'arborescence vers le fichier (par exemple, <file>/home/sylvie/Documents/travail/propositions-de-travail/… </file>), donc évitez si possible d'utiliser de longs noms de fichiers et de dossiers.</p>
      </item>
      <item>
        <title>L'option pour renommer est grisée</title>
        <p>Si l'option <gui>Renommer</gui> est grisée, vous n'avez pas la permission de modifier le nom du fichier. En règle générale, il est préférable de s'abstenir de modifier le nom de fichiers protégés car cela pourrait rendre votre système instable. Consultez <link xref="nautilus-file-properties-permissions"/> pour plus d'informations.</p>
      </item>
    </terms>
  </section>
</page>
