<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="fr">
  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>
    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Contributeurs du wiki de la documentation Ubuntu</name>
    </credit>

    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Munissez vous de toutes informations utiles, comme le numéro de modèle de votre adaptateur pour les étapes suivantes de dépannage.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Dépannage du réseau sans fil</title>
<subtitle>Collecte d'informations sur votre matériel réseau</subtitle>

<p>Dans cette étape, vous allez collecter les informations concernant votre périphérique de réseau sans fil. La manière de corriger beaucoup de problèmes du réseau sans fil dépend de la marque et du numéro de modèle de votre adaptateur, prenez donc par écrit tous ces détails. Il peut aussi être utile d'avoir sous la main des éléments qui sont généralement livrés avec l'ordinateur, comme le CD d'installation des pilotes par exemple. Recherchez les éléments suivants, si vous les avez encore :</p>

<list>
 <item>
  <p>les emballages et les instructions de vos périphériques sans fil (notamment le manuel utilisateur de votre routeur),</p>
 </item>
 <item>
  <p>le CD d'installation des pilotes de votre adaptateur (même s'il ne contient que des pilotes pour Windows),</p>
 </item>
 <item>
  <p>les marques et modèles de votre ordinateur, adaptateur sans fil et routeur ; ces informations sont généralement disponibles derrière ou dessous le périphérique,</p>
 </item>
 <item>
  <p>tout numéro de version/révision éventuellement étiqueté sur votre matériel ou sur son emballage ; ceci peut être particulièrement utile, donc regardez attentivement,</p>
 </item>
 <item>
  <p>sur le CD des pilotes, tout ce qui peut identifier soit le périphérique lui-même, soit sa version de « micrologiciel » ou les composants (circuits) qu'il utilise.</p>
 </item>
</list>

<p>Si possible, essayez d'avoir accès à une connexion internet active d'une autre façon pour pouvoir télécharger des logiciels ou des pilotes si besoin est (brancher votre ordinateur directement au routeur par un câble Ethernet est une façon de faire, mais ne le faites que quand vous en avez besoin).</p>

<p>Une fois collecté le plus possible de ces éléments, cliquez sur <gui>Suivant</gui>.</p>

</page>
