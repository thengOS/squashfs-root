<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="files-tilde" xml:lang="fr">

  <info>
    <link type="guide" xref="files#faq"/>
    <link type="seealso" xref="files-hidden"/>
    <desc>Ce sont des fichiers de sauvegarde. Ils sont masqués par défaut.</desc>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision version="13.10" date="2013-09-12" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Quel est ce fichier avec un « ~ » à la fin de son nom ?</title>

  <p>Les fichiers avec un « ~ » à la fin de leur nom (par exemple, <file>exemple.txt~</file>) sont des copies de sauvegarde automatiquement créées pour les documents ouverts dans l'éditeur de texte <app>gedit</app> ou par d'autres applications. C'est sans danger de les effacer, cependant il n'y a pas de danger non plus à les laisser sur votre ordinateur.</p>

  <p>Ces fichiers sont cachés par défaut. Si vous les voyez, c'est parce que vous avez soit sélectionné <gui>Afficher les fichiers cachés</gui> (dans le menu <media type="image" src="figures/go-down.png">bas</media> de la barre d'outils <app>Fichers</app>) soit appuyé sur les touches <keyseq><key>Ctrl</key><key>H</key></keyseq>. Vous pouvez les cacher de nouveau en répétant l'une ou l'autre de ces étapes.</p>

  <p>Ces fichiers sont gérés exactement de la même manière que ceux qui sont visibles. Consultez <link xref="files-hidden"/> pour savoir comment manipuler les fichiers cachés.</p>

</page>
