<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="video-sending" xml:lang="fr">

  <info>
    <link type="guide" xref="media#videos"/>
    <desc>Vérifiez qu'ils ont installé les codecs vidéos nécessaires.</desc>
    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>D'autres personnes ne parviennent pas à lire les vidéos que j'ai réalisées</title>

  <p>Si vous avez créé une vidéo sur votre ordinateur Linux et que vous l'envoyez à quelqu'un utilisant Windows ou Mac OS, il se peut qu'il ait des problèmes pour lire la vidéo.</p>

  <p>Pour lire votre vidéo, la personne à qui vous l'envoyez doit disposer des bons <em>codecs</em>. Un codec est un petit morceau de logiciel qui sait comment lire et afficher une vidéo à l'écran. Il existe un grand nombre de formats vidéo différents et chacun nécessite un codec spécifique pour être lu. Vous pouvez vérifier le format de votre vidéo de la manière suivante :</p>
<list>
    <item><p>Ouvrir le <link xref="files-browse">Gestionnaire de fichiers</link>.</p></item>
    <item><p>Faites un clic-droit sur le fichier vidéo et sélectionnez <gui>Propriétés</gui>.</p></item>
    <item><p>Allez dans l'onglet <gui>Audio/Vidéo</gui> et regardez quel <gui>codec</gui> est affiché sous <gui>Vidéo</gui>.</p></item>
</list>

  <p>Demandez aux personnes qui ont des problèmes pour lire votre vidéo si elles ont les codecs nécessaires installés sur leur ordinateur. Il peut être utile de rechercher sur le Web le nom du codec suivi du nom du lecteur vidéo. Par exemple, si votre vidéo utilise le format <em>Theora</em> et que vous avez un ami qui essaie de la lire avec Windows Media Player, recherchez « theora windows media player ». Vous pourrez probablement télécharger le codec correct gratuitement s'il n'est pas déjà installé.</p>

  <p>If you can't find the right codec, try the
 <link href="http://www.videolan.org/vlc/">VLC media player</link>. It works on
 Windows and Mac OS as well as Linux, and supports a lot of different video
 formats. Otherwise, try converting your video into a different format. Most
 video editors are able to do this, and specific video converter applications are
 available. Check <app>Ubuntu Software</app> to see what's available.</p>

<note>
  <p>Il y a quelques autres problèmes qui peuvent empêcher une personne de lire votre vidéo. La vidéo a pu être corrompue lors de la transmission (parfois les gros fichiers ne sont pas copiés parfaitement), la personne peut rencontrer des problèmes avec son application de lecture vidéo, ou encore la vidéo n'a pas été créée proprement (il y a peut-être eu des erreurs lors de l'enregistrement).</p>
</note>

</page>
