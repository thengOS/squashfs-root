<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="fr">

	<info>
		<credit type="author">
			<name>Phil Bull</name>
			<email>philbull@gmail.com</email>
		</credit>

		<credit type="editor">
    	<name>Greg Beam</name>
    	<email>ki7mt@yahoo.com</email>
    </credit>

		<link type="guide" xref="accounts"/>
		<link type="seealso" xref="accounts-remove"/>
    <revision pkgversion="3.10.2" version="0.1" date="2014-01-08" status="review"/>
		<revision version="14.04" date="2014-01-08" status="review"/>
		<include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Certains comptes en ligne vous permettent d'utiliser plusieurs services (comme le calendrier et le courrier électronique). Vous pouvez contrôler lesquels de ces services peuvent être utilisés par les applications locales.</desc>
	</info>

  <title>Désactiver les services du compte</title>

  <p>Certains types de comptes en ligne permettent l'accès à plusieurs services à partir d'un seul compte utilisateur. Par exemple, les comptes Google permettent l'accès, entre autres, au courrier électronique, calendrier, code, contacts. Vous pouvez choisir Google pour le courrier électronique et Yahoo! pour la messagerie instantanée, ou n'importe quelle combinaison que permet le fournisseur du service.</p>

	<p>Pour désactiver les services :</p>

  <steps>
		<item>
			<p>Cliquez sur l'icône tout à droite de la <gui>barre de menu</gui> et sélectionnez <gui>Paramètres système...</gui>.</p>
		</item>
		<item>
			<p>Sélectionner  <gui>Comptes en ligne</gui></p>
		</item>

		<item>
      <p>Sélectionnez le compte que vous voulez changer depuis la fenêtre de gauche.</p>
    </item>
    <item>
      <p>Les services disponibles pour ce compte sont listés dans le panneau de droite.</p>
    </item>
    <item>
      <p>Fermer les services que vous ne souhaitez pas utiliser.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Une fois qu'un service a été désactivé, les applications locales n'y ont plus accès. Pour y accéder de nouveau, retournez à <gui>Comptes en ligne</gui> et activez le.</p>
  </note>

</page>
