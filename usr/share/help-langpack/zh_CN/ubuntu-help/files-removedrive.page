<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="files-removedrive" xml:lang="zh-CN">

  <info>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <desc>弹出或卸载 USB 闪存驱动器、CD、DVD 或其他设备。</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="files#removable"/>
    <revision pkgversion="3.6.0" version="0.2" date="2012-10-08" status="review"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
  </info>

  <title>安全删除外部设备</title>

  <p>当您使用外部存储设备(如 USB 闪存驱动器)时，您应在将其拔下之前安全删除它们。如果您只是拔下设备，就会遇到拔下时应用程序仍在使用设备的危险，这会造成您的部分文件丢失或受损。当您使用 CD 或 DVD 等光盘时，您可以使用相同步骤将光盘从计算机中弹出。</p>

  <steps>
    <title>格式化可移动磁盘</title>
    <item><p><link xref="files-browse">打开文件管理器</link>。</p>
    </item>
    <item><p>在侧边栏找到设备。它的名称旁边应有一个小的弹出图标。单击弹出图标以安全删除或弹出设备。</p>
    <p>此外，您可以用右键单击侧边栏上的设备名并选择 <gui>弹出</gui>。</p></item>
  </steps>

  <section id="remove-busy-device">
    <title>安全删除外部设备</title>

  <p>如果您存储在设备上的任何文件已在任何应用程序中打开，您将无法安全删除该设备。您将会看到提示窗口，提示您 “卷被占用”，并列出设备上所有打开的文件。当您关闭设备上的所有文件后，设备将自动安全删除(以便您拔下或弹出)。</p>

  <p>如果您无法关闭其中一个文件，例如使用该文件的应用程序已被锁定，您可以在<gui>卷被占用</gui>窗口中右击该文件，然后选择<gui>结束进程</gui>。这将强制整个被锁定的应用程序关闭，从而可能会关闭使用该应用程序打开的其他文件。</p>

  <note style="warning"><p>您还可以选择 <gui>强制弹出</gui> 来移除设备，无需关闭打开的文件，但这可能会导致打开的这些程序文件出错。</p></note>
  </section>
</page>
