<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="disk-partitions" xml:lang="zh-CN">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <desc>了解什么是卷和分区，使用磁盘实用程序管理卷和分区。</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

 <title>管理卷和分区</title>

  <p><em>卷</em>是一个描述硬盘等存储设备的术语。当您把存储设备分成若干个区之后，它也可以指设备上的<em>部分</em>存储空间。您可以将卷<em>挂载</em>到计算机上。您可以挂载硬盘、USB 驱动器、DVD-RW、SD 卡以及其他介质。如果某个卷现在处于已挂载状态，您就可以对其进行读取和写入操作。</p>

  <p>已被挂载的卷一般被称为<em>分区</em>，尽管这两个术语严格来说并不相同。“分区”是指一个磁盘驱动器上的<em>物理</em>存储区域。分区安装后，也可以被称为卷，因为您可以对其进行读写操作。</p>

<section id="manage">
 <title>使用磁盘实用工具查看和管理卷和分区</title>

  <p>您可以使用磁盘实用工具检查和修改计算机的存储卷。</p>

<steps>
 <item>
  <p>打开<gui> Dash </gui> 启动<app>磁盘</app>应用程序。</p>
 </item>
 <item>
  <p>在标为<gui>存储设备</gui>的窗格中，您可以找到硬盘、CD/DVD 驱动器及其他连接到计算机的存储设备。单击您想要查看的设备。</p>
 </item>
 <item>
  <p>在右窗格中，标记为<gui>卷</gui>的区域提供所选设备上存在的卷和分区的直观分类图示。您也可以在这一区域找到用来管理这些卷的各种工具。</p>
  <p>请注意：使用这些实用程序可能会彻底清除您磁盘上的数据。</p>
 </item>
</steps>

  <p>一般来说，您的计算机有至少一个<em>主</em>分区和一个单一的<em>交换</em>分区。交换分区由操作系统用来进行内存管理，很少被挂载。主分区包含您的操作系统、应用程序、设置以及个人文件。为安全或方便起见，这些文件还可能分布在多个分区。</p>

  <p>一个主分区必须包含您的计算机进行启动(或称<em>引导</em>)所需要的信息。因此，它有时被称为引导分区，或引导卷。要确定卷是否可引导，可在磁盘实用程序中查看其<gui>分区标志</gui>。外部介质，如 USB 驱动器和 CD，也可能包含可引导卷。</p>

</section>

</page>
