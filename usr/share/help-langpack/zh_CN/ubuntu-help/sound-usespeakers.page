<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-usespeakers" xml:lang="zh-CN">
  <info>
    <link type="guide" xref="media#sound"/>
    <link type="seealso" xref="sound-usemic"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <desc>连接扬声器或耳机，并选择默认的音频输出设备。</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>使用其他扬声器或耳机</title>

  <p>您可以在计算机上使用外部扬声器或耳机。通常使用圆形 TRS(<em>尖端，环形，套筒</em>)插头或 USB 连接扬声器。</p>

  <p>如果您的扬声器或耳机带有 TRS 插头，则将其插入到您计算机上相应的插槽。多数计算机有两个插槽：一个插麦克风，另一个插扬声器。耳机插槽旁边可以看到一个耳机图标。默认情况下，通常将使用插入到 TRS 插槽的扬声器或耳机。如果没有，则请参阅以下说明选择默认设备。</p>

  <p>一些电脑支持多声道环绕声输出。这通常会使用编有不同颜色的多 TRS 插孔。如果不确定不同插头所对应的插槽，则您可在声音设置里测试声音输出。单击<gui>菜单栏</gui>中的<gui>声音菜单</gui>，然后单击<gui>声音设置</gui>。在<gui>硬件</gui>标签页的设备列表中选择您的扬声器，然后单击<gui>测试扬声器</gui>。单击弹出窗口中每个扬声器的按钮。每个按钮将仅向与其扬声器对应的声道报告自己的位置。</p>

  <p>如果您有 USB 扬声器或耳机，或插入到USB 声卡中的模拟耳机，则将其插入到任意的 USB 端口。USB 麦克风作为单独的音频设备，您必须指定默认情况下使用的扬声器。</p>

  <steps>
    <title>选择默认的音频输入设备</title>
    <item><p>单击<gui>顶部工具栏</gui>上的<gui>声音菜单</gui>并选择<gui>声音设置</gui>。</p></item>
    <item><p>在<gui>输出</gui>标签页中，选择设备列表中的设备。</p></item>
  </steps>

  <p>如果在<gui>输出</gui>标签页中看不到您的设备，则选中<gui>硬件</gui>标签页。选择您的设备，并尝试不同的配置。</p>
</page>
