<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-order" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="printing#paper"/>

    <desc>逐份打印和调转打印顺序。</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>以不同的顺序打印纸张</title>

<section id="reverse">
 <title>调转打印顺序</title>
 <p>通常，打印机先打印第一页，最后打印最后一页，因此打印出来的页面顺序是逆向的。如有需要，可反向打印顺序。</p>
 <p>要进行逆打印：</p>
 <steps>
  <item><p>单击<guiseq><gui>文件</gui><gui>打印</gui></guiseq>。</p></item>
  <item><p>在打印窗口的 <gui>常规</gui> 标签页下找到 <em>份数</em>，选中 <gui>逆续</gui>。这样会先打印最后一页，然后逆续进行。</p></item>
 </steps>
</section>

<section id="collate">
 <title>逐份打印</title>
 <p>如果要对文档进行多份复印，则默认情况下将按页码对输出品进行分组(即先打印出每份的第一页，然后是每份的第二页，依此类推)。然而，<em>逐份打印</em>可使每个副本随其分组的页面按正确顺序一起打印出。</p>

 <p>要逐份打印：</p>
 <steps>
  <item><p>单击<guiseq><gui>文件</gui><gui>打印</gui></guiseq>。</p></item>
  <item><p>在打印窗口的 <gui>常规</gui> 标签页下找到 <em>份数</em>，选中 <gui>校正</gui>。</p></item>
 </steps>
 <!-- Need to add this image <media type="image" src="figures/reverse-collate.png"/> -->
</section>

</page>
