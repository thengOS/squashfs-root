<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-file-properties-basic" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files" group="more"/>

   <desc>查看基本文件信息，设置权限，以及选择默认应用程序。</desc>

   <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
   <revision version="13.10" date="2013-09-15" status="review"/>

   <credit type="author">
     <name>Tiffany Antopolski</name>
     <email>tiffany@antopolski.com</email>
   </credit>
   <credit type="author">
     <name>Shaun McCance</name>
     <email>shaunm@gnome.org</email>
   </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>
  </info>
  <title>文件属性</title>

  <p>要查看有关文件或文件夹的信息，请右键单击文件或文件夹，然后选择<gui>属性</gui>。也可先选择文件再按 <guiseq><gui>Alt</gui><gui>回车</gui></guiseq>。</p>

  <p>文件属性窗口用于显示文件类型、文件大小和上次文件修改时间之类的信息。如果常需要了解这些信息，则可将其显示在<link xref="nautilus-list">列表视图列</link>或<link xref="nautilus-display#icon-captions">图标标题</link>中。也可用此窗口<link xref="nautilus-file-properties-permissions">设置文件权限</link>和<link xref="files-open">选择用于打开文件的应用程序</link>。</p>

  <p>显示在<gui>基本</gui>标签页中的信息的说明如下。其中也有<gui><link xref="nautilus-file-properties-permissions">权限</link></gui>和<gui><link xref="files-open#default">打开方式</link></gui>标签页。对于某些类型的文件(如图像和视频)，会多显示一个标签页，其中有尺寸、持续时间和编码之类的信息。</p>

<section id="basic">
 <title>基本属性</title>
 <terms>
  <item>
    <title><gui>名称</gui></title>
    <p>可通过更改此字段重命名文件。也可在属性窗口外重命名文件。请参阅<link xref="files-rename"/>。</p>
  </item>
  <item>
    <title><gui>类型</gui></title>
    <p>此字段有助于识别文件的类型，如 PDF 文档、OpenDocument 文本文件或 JPEG 图像。文件类型等属性决定着哪种应用程序能打开相应文件。例如，不能用音乐播放器打开图片。对于更多信息，请参见<link xref="files-open"/>。</p>
  <p>文件的 <em>MIME 类型</em>显示在括号中；使用 MIME 类型是计算机用于指代相应文件类型的标准方式。</p>
  </item>

  <item>
    <title>目录</title>
    <p>如果正在查看文件夹而不是文件的属性，则此字段已显示出来。此字段有助于查看文件夹中的项目数量。如果所查看的文件夹中有其他文件夹，则会将其中的每个文件夹视为一个项目，即使其中含有其他项目。每个文件也都视为一个项目。如果文件夹是空的，则会在内容中显示<gui>无</gui>。</p>
  </item>

  <item>
    <title>大小</title>
    <p>如果正在查看文件(不是文件夹)，则此字段已显示出来。从文件大小中可以看出文件占有多少磁盘空间。从中还可看出下载文件或通过电子邮件发送文件会耗费多长时间(收发大文件所耗的时间较长)。</p>
    <p>可能会以字节、KB、MB 或 GB 为单位显示大小；如果用后三种方式显示大小，也会在括号中以字节为单位显示大小。从技术角度来说，1 KB 是 1024 字节，1 MB 是 1024 KB，以此类推。</p>
  </item>

  <item>
    <title>位置</title>
    <p>每个文件在计算机中的位置均由其<em>绝对路径</em>显示。这是文件在计算机中的唯一 "地址"，由列出的文件夹构成，必须转到这些文件夹中才能找到相应文件。例如，如果 Jim 的主文件夹中有个文件名为 <file>Resume.pdf</file>，则其位置会是 <file>/home/jim/Resume.pdf</file>。</p>
  </item>

  <item>
    <title>卷</title>
    <p>存有文件的文件系统或设备。此字段用于显示文件实体的存储位置，例如，文件是在硬盘还是 CD 中，在<link xref="nautilus-connect">网络共享还是服务器</link>中。可将硬盘分为多个<link xref="disk-partitions">磁盘分区</link>；磁盘分区也会显示在<gui>卷</gui>下。</p>
  </item>

  <item>
    <title>可用空间</title>
    <p>仅会对文件夹显示此字段。此字段用于显示其中有相应文件夹的磁盘上的可用磁盘空间量。可用此字段核实硬盘是否已满。</p>
  </item>


  <item>
    <title>访问时间</title>
    <p>上次打开文件的日期和时间。</p>
  </item>

  <item>
    <title>修改时间</title>
    <p>上次修改文件的日期和时间。</p>
  </item>
 </terms>
</section>

</page>
