<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="addremove-ppa" xml:lang="zh-CN">

  <info>
    <credit type="author">
      <name>Ubuntu 文档团队</name>
    </credit>
    <desc>添加 PPA 帮助测试预发行或专业软件。</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="addremove"/>
    <link type="seealso" xref="addremove-sources"/>
    <revision version="16.04" date="2016-03-13" status="review"/>
  </info>

  <title>添加个人软件包仓库(PPA)</title>

  <p><em>个人软件包仓库(PPA)</em> 是专为 Ubuntu 用户设计的软件仓库，比其他第三方软件库更易于使用。</p>

  <note style="warning">
    <p>请务必确保您添加的软件源的安全性。</p>
    <p>第三方软件库未经过 Ubuntu 开发者检查安全性和可靠性，可能包含对您的计算机有害的软件。</p>
  </note>

  <steps>
    <title>安装 PPA</title>
    <item>
      <p>在 PPA 概述页面，寻找名为<gui>将此 PPA 添加到系统</gui>的标题。记下 PPA 的位置，它看起来类似于：<code>ppa:mozillateam/firefox-stable</code>。</p>
    </item>
    <item>
      <p>
        Click the <app>Ubuntu Software</app> icon in the <gui>Launcher</gui>, or search
        for <input>Software</input> in the search bar of the <gui>Dash</gui>.
      </p>
    </item>
    <item>
      <p>
        When <app>Ubuntu Software</app> launches, click <gui>Software &amp; Updates</gui>
      </p>
    </item>
    <item>
      <p>切换到<gui>其他软件</gui>标签页。</p>
    </item>
    <item>
      <p>单击<gui>添加</gui>，然后输入 <code>ppa:</code> 地址。</p>
    </item>
    <item>
      <p>单击<gui>添加软件源</gui>并在认证窗口输入密码。</p>
    </item>
    <item>
      <p>
        Close the <app>Software &amp; Updates</app> window. <app>Ubuntu Software</app> will then 
        check your software sources for new software.
      </p>
    </item>
  </steps>

</page>
