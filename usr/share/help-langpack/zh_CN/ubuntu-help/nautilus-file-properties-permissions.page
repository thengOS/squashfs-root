<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-file-properties-permissions" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files#faq"/>
    <link type="seealso" xref="nautilus-file-properties-basic"/>

    <desc>控制谁能查看和编辑用户的文件和文件夹。</desc>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>
  </info>
  <title>设置文件权限</title>

  <p>可用文件权限控制谁能查看和编辑用户拥有的文件。要查看和设置文件权限，请右键单击文件，选择<gui>属性</gui>，然后选择<gui>权限</gui>标签页。</p>

  <p>要详细了解可以设置的权限的类型，请参见下面的<link xref="#files"/>和<link xref="#folders"/>。</p>

  <section id="files">
    <title>文件</title>

    <p>可设置文件所有者、组所有者和所有其他系统用户的权限。对于用户的文件，用户是所有者，用户也能赋予自己只读或读写权限。如果不想意外更改文件，请将文件设置为只读文件。</p>

    <p>计算机中的每位用户都属于某个组。在家庭计算机中，用户通常各有各的组，但往往不会设置组权限。在团体环境中，有时会将组用于部门或项目。除了有所有者，每个文件都还属于某个组。可设置文件所属的组并控制该组中所有用户的权限。用户仅可将文件所属的组设置为自己所属的组。</p>

    <p>也可对除所有者以外的其他用户，以及文件所属的组中的用户设置权限。</p>

    <p>如果文件是一个程序(如脚本)，则必须选择<gui>允许以程序的形式执行文件</gui>才能运行文件。即使已选择此选项，文件管理器还是有可能在应用程序中打开文件或询问要执行什么操作。对于更多信息，请参见<link xref="nautilus-behavior#executable"/>。</p>
  </section>

  <section id="folders">
    <title>文件夹</title>
    <p>对于文件夹所有者、组和其他用户，均可对文件夹设置权限。对于所有者、组和其他用户的说明，请参见上面的文件权限详细信息。</p>
    <p>可对文件夹设置的权限，都不同于可对文件设置的权限。</p>
    <terms>
      <item>
        <title><gui>无</gui></title>
        <p>用户甚至不能查看文件夹中有什么文件。</p>
      </item>
      <item>
        <title><gui>只能列出文件</gui></title>
        <p>用户将能够查看文件夹中有什么文件，但不能打开、创建或删除文件。</p>
      </item>
      <item>
        <title><gui>能访问文件</gui></title>
        <p>用户将能够打开文件夹中的文件(前提是他们有权对特定文件执行这种操作)，但不能创建新文件或删除文件。</p>
      </item>
      <item>
        <title><gui>创建和删除文件</gui></title>
        <p>用户将对相应目录拥有最高访问权，包括打开、创建和删除文件的权限。</p>
      </item>
    </terms>

    <p>您还可以在文件夹中通过点击 <gui>改变封闭文件权限</gui> 为所有文件快速设置文件权限，使用下拉列表调整包含文件或者文件夹的权限，然后点击 <gui>更改</gui> ，该权限也将应用于任何深度的子文件夹中的文件和文件夹。</p>
  </section>

</page>
