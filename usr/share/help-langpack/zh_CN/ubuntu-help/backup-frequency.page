<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files#backup"/>
    <desc>应该多久备份一次文件以确保数据安全？</desc>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>备份频率</title>

  <p>多久备份一次取决于要备份的数据的类型。例如，如果您运行的网络环境含有存储在服务器上的关键数据，则每晚备份可能都不够。</p>

  <p>另一方面，如果您要备份家用计算机上的数据，每小时备份一次就不必要了。规范备份时，可能需要考虑如下几个因素：</p>

<list style="compact">
<item><p>您花在计算机上的时间量。</p></item>
<item><p>计算机上的数据变化的频率以及幅度。</p></item>
</list>

  <p>如果仅仅是一些音乐、电子邮件和家庭照片，每周甚或每月备份一次可能足矣。但是，如果您碰巧处在税务审计期间，可能需要更频繁的备份。</p>

  <p>通常，备份间隔时间不应超过您能够忍受的重做丢失作业所需的时间。例如，如果用一周的时间来重写丢失的文档对您来说太久了，您就应该至少每周备份一次。</p>

</page>
