<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="documents-select" xml:lang="zh-CN">

  <info>
    <credit type="author copyright">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <desc>使用选择模式选择多个文档或集合。</desc>
    <link type="guide" xref="documents#print"/>
    <revision pkgversion="3.5.90" date="2012-09-05" status="review"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>选择文档</title>

  <p>使用<app>文档</app>的选择模式，您可以打开、打印、查看或将您的文档分组。使用选择模式：</p>

  <steps>
    <item><p>点击<gui>✓</gui> 按钮。</p></item>
    <item><p>选择一个或多个文档或者集合。按钮栏将根据您的选择显示。</p></item>
  </steps>

  <section id="selection-mode">

  <title>选择模式操作</title>

  <p>选择一个或多个文档，您可以在：</p>

  <list>
    <item><p>打开文档查看器(文件夹图标)。</p></item>
    <item><p>打印(打印机图标)：打印文件(仅当选择单个文档)。</p></item>
    <item><p>组织(加号图标)：创建文档的集合。</p></item>
    <item><p>属性(扳手图标)：显示文档的属性(仅当选择单个文件)。</p></item>
    <item><p>删除(回收站图标)：删除一个或多个集合。</p></item>
  </list>
  </section>
</page>
