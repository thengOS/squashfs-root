<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="shell-workspaces" xml:lang="zh-CN">

  <info>

    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <desc>工作区是将您桌面上的窗口分组的一种方法。</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="14.10" date="2014-09-14" status="review"/>
    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

  </info>

<title>工作区是什么，它对我有何帮助？</title>

<media type="image" src="figures/unity-workspace-intro.png" style="floatend floatright">
  <p>“工作区切换器”按钮</p>
</media>

  <p>工作区是指您的桌面窗口的分组。这些虚拟桌面增大了您工作区域。工作区是为了减少混乱，使桌面更容易导航。</p>

  <p>您可以使用工作区来组织您的工作。例如，您可使所有的通信窗口(如电子邮件和您的聊天程序)位于一个工作区，并且使您正在进行的工作位于另一个工作区，您的音乐管理器可位于第三个工作区。</p>

  <steps>
    <title>启用工作区</title>
    <item>
      <p>单击菜单栏最右侧的图标，选择<gui>系统设置</gui>。</p>
    </item>
    <item>
      <p>在“个人”区域，单击<gui>外观</gui> 并选择<gui>行为</gui>标签页。</p>
    </item>
    <item>
      <p>勾选<gui>开启工作区</gui> 复选框。</p>
    </item>
  </steps>

 <p>打开 <link xref="unity-launcher-intro">启动器</link>并单击靠近底部的<gui>工作区切换器</gui>图标。默认情况下，Ubuntu 会显示 4 个工作区，成 2 排 2 列排列。您可以更改工作区数量：</p>

<steps>
  <title>更改工作区编号</title>
  <item><p>前往 <link xref="unity-dash-intro">Dash</link> 并打开 <app>终端</app>。</p></item>
  <item><p>要更改行的数量，请键入以下命令，将最终数量更改成您希望的数字。按<key>回车</key>。</p>

    <code its:translate="no">gsettings set org.compiz.core:/org/compiz/profiles/unity/plugins/core/ vsize 2</code></item>
  <item><p>要更改列编号，请键入以下命令，将最终数量更改成您希望的数字。按<key>回车</key>。</p>
    <code its:translate="no">gsettings set org.compiz.core:/org/compiz/profiles/unity/plugins/core/ hsize 3</code></item>
</steps>

</page>
