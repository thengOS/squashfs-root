<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="tips-specialchars" xml:lang="zh-CN">
  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="14.10" date="2014-09-14" status="review"/>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ubuntu 文档团队</name>
    </credit>

    <desc>键入在您的键盘上找不到的字符，包括外国字母、数学符号以及杂锦符号。</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>输入特殊字符</title>

  <p>您可输入和查看世界上大多数写入系统的数千种字符，虽然在您的键盘上无法找到它们。此页面列出了一些您可输入特殊字符的不同方式。</p>

  <links type="section">
    <title>输入字符的方法</title>
  </links>

  <section id="charmap">
    <title>字符映射表</title>
    <p>GNOME 带有的字符映射表应用程序允许您浏览 Unicode 中的所有字符。使用字映射表查找您想要的字符，然后将其复制并粘贴到您需要的位置。</p>
    <p>可以在 <gui>Dash</gui> 中查找 <app>字符映射表</app>。关于字符映射表的更多信息，请查看 <link href="help:gucharmap">字符映射表手册</link>。</p>
  </section>

  <section id="ctrlshiftu">
    <title>码位</title>
    <p>您可仅通过字符的码位用键盘输入任何 Unicode 字符。每个字符可由四位的字符码位表示。要找到一个字符的码位，请在字符映射表应用程序中找到该字符，并查看状态栏或<gui>字符详情</gui>标签页。码位为 <gui>U+</gui> 后的四个字符。</p>

    <p>要通过其码位键入字符，请按住<keyseq><key>Ctrl</key><key>Shift</key><key>U</key></keyseq>，输入四位码位，最后按住 <key>Enter</key>。如果您经常使用容易地通过其他方法获得的字符，您会发现记住这些字符的码位非常有用，这样您可快速的输入这些字符。</p>
  </section>

  <section id="sources">
    <title>输入源</title>
    <p>您可以使您的键盘的行为类似其他语言的键盘，无论键上打印的字母。您甚至可以通过菜单栏上的一个图标在不同的输入源间切换。要了解如何做，请查看 <link xref="keyboard-layouts"/>。</p>
  </section>

</page>
