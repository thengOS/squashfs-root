<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-windows-states" xml:lang="zh-CN">

  <info>

    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="unity-menubar-intro#window-management"/>

    <desc>恢复、调整、排列和隐藏。</desc>

    <revision pkgversion="3.4.0" date="2012-09-20" status="candidate"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>窗口操作</title>

<p>窗口可以调整大小或隐藏以适合工作流程。</p>


<section id="min-rest-close">
<title>最小化，恢复和关闭</title>

    <p>要最小化窗口或隐藏窗口，您可以：</p>
    <list>
      <item>
       <p>点击应用程序的<gui>菜单栏</gui>中看起来像“减号”的按钮。如果应用程序已最大化(填满您的整个屏幕)，则菜单栏将出现在屏幕最上方。否则，最小化按钮将出现在应用程序窗口的顶端。</p>
      </item>
      <item>
       <p>或者按 <keyseq><key>Alt</key><key>Space</key></keyseq> 显示窗口菜单。然后按 <key>n</key>。窗口“消失”到了启动器中。</p>
      </item>
    </list>

    <p>要恢复该窗口：</p>
    <list>
      <item>
       <p>在 <link xref="unity-launcher-intro">启动器</link> 上点击或者通过按 <keyseq><key>Alt</key><key>Tab</key></keyseq> 在窗口切换器中获取。</p>
      </item>
    </list>

    <p>要关闭该窗口：</p>
    <list>
      <item>
       <p>单击位于窗口左上角的<gui>x</gui>，或者</p>
      </item>
      <item>
       <p>按 <keyseq><key>Alt</key><key>F4</key></keyseq>，或者</p>
      </item>
      <item>
       <p>按 <keyseq><key>Alt</key><key>Space</key></keyseq> 以呼出窗口菜单。然后按 <key>c</key>。</p>
      </item>
    </list>

</section>

<section id="resize">
<title>调整大小</title>

<note style="important">
 <p>窗口<em>最大化</em>时无法调整大小。</p>
</note>
<p>要调整窗口的水平或竖直大小：</p>
<list>
 <item>
 <p>将鼠标指针移至窗口的任意一个角，直到显示“角落光标”。然后，按住鼠标并拖动，以在任意方向调整窗口尺寸。</p>
 </item>
</list>
<p>要只在水平方向调整</p>
<list>
 <item>
 <p>将鼠标指针移至窗口的左右任意一条边，直到显示“边线光标”。然后，按住鼠标并拖动，以在水平方向调整窗口尺寸。</p>
 </item>
</list>
<p>要只在竖直方向调整大小：</p>
<list>
 <item>
 <p>将鼠标指针移至窗口顶部或底部，直到它分别更改为“向上指针”或“向下指针”。然后，按住鼠标并拖动，可以在垂直方向调整窗口尺寸。</p>
 </item>
</list>

</section>

<section id="arrange">

<title>在工作区中排列窗口</title>

<p>要将窗口肩并肩排列：</p>
  
<list>
 <item><p>点击窗口的<gui>标题栏</gui>并向屏幕的左边缘拖动。当<gui>鼠标指针</gui>接触到屏幕边缘时，左半侧屏幕将突出显示。放开鼠标按钮后此窗口将填满左侧屏幕。</p></item>
 <item><p>将其他窗口拖动到右侧：当右半边屏幕高亮时放开。每个屏幕将分别占据屏幕的半边。</p></item>
</list>

    <note style="tip">
      <p>在窗口的任意位置单击时按 <key>Alt</key>，将允许您移动该窗口。有些人可能发现这比在应用程序的<gui>标题栏</gui>单击更加容易。</p>
    </note>
    
</section>

</page>
