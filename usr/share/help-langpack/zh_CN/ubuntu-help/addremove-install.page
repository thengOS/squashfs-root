<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="addremove-install" xml:lang="zh-CN">

  <info>
    <credit type="author">
      <name>Ubuntu 文档团队</name>
    </credit>
    <desc>
      Use <app>Ubuntu Software</app> to add programs and make Ubuntu more 
      useful.
    </desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="addremove" group="#first"/>
    <link type="seealso" xref="addremove-remove"/>
    <link type="seealso" xref="addremove-install-synaptic"/>
    <link type="seealso" xref="prefs-language-install"/>
    <revision version="16.04" date="2016-03-13" status="review"/>
  </info>

  <title>安装其他软件</title>

  <p>Ubuntu 开发团队选择了一组我们认为会使 Ubuntu 对大多数日常任务都非常有用的默认应用程序。不过，您肯定想要安装更多软件以使 Ubuntu 对您更有用。</p>

  <p>要安装其他软件，请按以下步骤操作：</p>

  <steps>
    <item>
      <p>使用 <link xref="net-wireless-connect">无线</link> 或 <link xref="net-wired-connect">有线</link> 连接到互联网。</p>
    </item>
    <item>
      <p>
        Click the <app>Ubuntu Software</app> icon in the <gui>Launcher</gui>, or search
        for <input>Software</input> in the search bar of the <gui>Dash</gui>.
      </p>
    </item>
    <item>
      <p>
        When <app>Ubuntu Software</app> launches, search for an application, or select 
        a category and find an application from the list.
      </p>
    </item>
    <item>
      <p>选择您感兴趣的应用程序，然后单击<gui>安装</gui>。</p>
    </item>
    <item>
      <p>您会被要求输入密码。输完密码后安装即会开始。</p>
    </item>
    <item>
      <p>安装通常很快就能完成，但如果您的网络连接速度较慢，就会花上一段时间。</p>
    </item>
    <item>
      <p>新应用的快捷方式会添加到启动器。如果要禁用该特性，请取消选中<guiseq><gui>视图</gui><gui>新应用程序置于启动器中</gui></guiseq>。</p>
    </item>
  </steps>
  
</page>
