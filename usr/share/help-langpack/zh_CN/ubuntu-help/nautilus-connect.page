<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-connect" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files" group="more"/>
    <link type="guide" xref="sharing"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-06" status="review"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <desc>通过 FTP、SSH、Windows 共享或 WebDAV 查看和编辑另一计算机中的文件。</desc>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>浏览服务器或网络共享中的文件</title>

<p>可连接到服务器或网络共享，以浏览和查看该服务器中的文件，就如同那些文件在自己的计算机中。这是一种在因特网上下载或上载文件，或与本地网络中的其他人共享文件的简便方法。</p>

<p>通过网络浏览文件，从 <gui>Dash</gui> 中打开 <app>文件</app> 管理器，在侧边栏点击 <gui>浏览网络</gui> 。文件管理器将会发现所有在您局域网中宣传它们有能力服务文件的计算机。如果您想要通过互联网连接到服务器，或者如果您未看到您寻找的计算机，您可以通过输入它的互联网/网络地址手动连接到服务器。</p>

<steps>
  <title>连接到文件服务器</title>
  <item><p>在文件管理器的菜单栏选择 <gui>文件</gui> ，然后从程序菜单中选择 <gui>连接到服务器</gui> 。</p></item>
  <item><p>在 <link xref="#urls">URL</link> 地址栏中输入服务器地址。可用的 URL 详细 <link xref="#types">列表如下</link> 。</p>
  <note>
    <p>如果您之前连接过该服务器，您还可以在 <gui>最近使用的服务器</gui> 列表中点击它。</p>
  </note>
  </item>
  <item><p>点击 <gui>连接</gui> 按钮，将会打开一个在服务器上显示文件的新窗口。您可以像浏览自己电脑上的文件一样浏览它们，服务器还将被添加到侧边栏，方便您以后快速访问它。</p>
  </item>
</steps>

<section id="urls">
 <title>输入 URL 网址</title>

<p><em>URL</em> 或者 <em>统一资源定位符</em> ，是一个指向网络位置或者文件的地址，地址格式如下：</p>
  <example>
    <p><sys>scheme://servername.example.com/folder</sys></p>
  </example>
<p><em>scheme</em> 指定了服务器的协议或者类型，地址中的 <em>example.com</em> 部分被称作 <em>域名</em> ，如果需要用户名，请插入到服务器名称之前：</p>
  <example>
    <p><sys>scheme://username@servername.example.com/folder</sys></p>
  </example>
<p>一些方案需要指定端口号，请插入到域名之后：</p>
  <example>
    <p><sys>scheme://servername.example.com:port/folder</sys></p>
  </example>
<p>下面是支持的各种服务类型的具体实例。</p>
</section>

<section id="types">
 <title>服务器的类型</title>

<p>您可以连接到不同类型的服务器，一些服务器是公开的，允许任何人连接；另一些服务器需要您使用用户名和密码登录。</p>
<p>可能没有对服务器中的文件执行某些操作的权限。例如，在公用 FTP 站点中，用户多半不能删除文件。</p>
<p>您输入的 URL 取决于服务器使用的文件共享协议。</p>
<terms>
<item>
  <title>SSH</title>
  <p>如果您有服务器的 <em>SSH</em> 账户，您可以使用它来连接，许多网络主机为成员提供 SSH 账户，以便安全的上传文件，SSH 服务器总是需要用户登录。</p>
  <p>典型的 SSH URL 如下：</p>
  <example>
    <p><sys>ssh://username@servername.example.com/folder</sys></p>
  </example>

  <p>使用 SSH 时，发送的所有数据(包括密码)都已加密，因此网络中的其他用户都不能看到这些数据。</p>
</item>
<item>
  <title>FTP(要求登录)</title>
  <p>使用 FTP 是一种在因特网上传输文件的流行方法。因为通过 FTP 传输的数据未经加密，所以许多服务器现在都可以通过 SSH 访问。但是，对某些服务器仍可以或必须用 FTP 上载或下载文件。通过要求登录的 FTP 站点，通常可以删除和上载文件。</p>
  <p>典型的 FTP URL 如下：</p>
  <example>
    <p><sys>ftp://username@ftp.example.com/path/</sys></p>
  </example>
</item>
<item>
  <title>公用 FTP</title>
  <p>网站为了允许您下载文件，有时将会提供公共或者匿名 FTP 访问，这些服务无需用户名和密码，但通常不允许您删除或者上传文件。</p>
  <p>典型的匿名 FTP URL 如下：</p>
  <example>
    <p><sys>ftp://ftp.example.com/path/</sys></p>
  </example>
  <p>有些匿名 FTP 站点需要您使用一个公共用户名和密码登录，或者一个使用您电子邮件地址作为密码的公共用户名。对于这些服务，使用 <gui>FTP (登录)</gui> 方式，并使用指定的 FTP 站点证书。</p>
</item>
<item>
  <title>Windows 共享</title>
  <p>Windows 计算机用专属协议通过局域网共享文件。Windows 网络上的计算机有时会归并到<em>域</em>中，以便进行整理并加强访问控制。如果在远程计算机中有相应权限，则可通过文件管理器连接到 Windows 共享。</p>
  <p>典型的 Windows 共享 URL 如下：</p>
  <example>
    <p><sys>smb://servername/Share</sys></p>
  </example>
</item>
<item>
  <title>WebDAV 和安全 WebDAV</title>
  <p>WebDAV 基于用在网络上的 HTTP 协议，有时用于在本地网络上共享文件，以及在因特网上存储文件。如果要连接的服务器支持安全连接，则应该选择此选项。安全 WebDAV 采用强 SSL 加密，这样其他用户就看不到您的密码。</p>
  <p>典型的 WebDAV URL 如下：</p>
  <example>
    <p><sys>http://example.hostname.com/path</sys></p>
  </example>
</item>
</terms>
</section>

</page>
