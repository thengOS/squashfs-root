<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-lost" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files" group="more"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <desc>如果您找不到您创建或下载的文件，请按照以下方式进行操作。</desc>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>寻找丢失的文件</title>

<p>如果您创建或下载了一个文件，但现在却找不到它，请按照以下方式进行操作。</p>

<list>
  <item><p>如果您忘记文件保存的位置，但您还记得其名称，您可以按名称搜索该文件。请参阅<link xref="files-search"/>了解如何搜索。</p></item>

  <item><p>如果您刚刚下载了该文件，您的浏览器可能已自动将其保存在某个文件夹中。检查您的主文件夹中的<file>桌面</file>和<file>下载</file>文件夹。</p></item>

  <item><p>您可能意外删除了文件。您删除文件时，文件将被移入回收站，在您手动清空回收站之前，它将一直保存在此处。请参阅<link xref="files-recover"/>了解如何还原已删除的文件。</p></item>

  <item><p>以重命名文件的方式隐藏文件。在文件管理器中以<file> . </file>开头或以<file> ~ </file>结尾的文件是隐藏文件。点击文件管理器工具栏上的<media type="image" src="figures/go-down.png"> 向下</media>按钮，选择<gui>显示隐藏文件</gui>来显示他们。请参阅<link xref="files-hidden"/>了解更多。</p></item>
</list>

</page>
