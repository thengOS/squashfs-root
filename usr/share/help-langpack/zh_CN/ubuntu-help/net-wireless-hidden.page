<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-wireless-hidden" xml:lang="zh-CN">
  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="seealso" xref="net-wireless-edit-connection#wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <credit type="author">
      <name>Ubuntu 文档团队</name>
    </credit>

    <desc>单击顶部面板中的<gui>网络菜单</gui>并选择<gui>连接到隐藏的无线网络</gui>。</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>设置隐藏无线网络</title>

<p>您可将无线网络设置为“隐藏”。隐藏网络不会在单击顶部面板中的网络菜单时显示的网络列表中出现，也不会在其他任何计算机上的无线网络列表中出现。</p>

<steps>
 <item>
  <p>单击顶部面板中的<gui>网络菜单</gui>并选择<gui>连接到隐藏的无线网络</gui>。</p>
 </item>
 <item>
  <p>要连接到隐藏的无线网络，请单击顶部面板中的<gui>网络菜单</gui>并选择<gui>连接到隐藏的无线网络</gui>。在出现的窗口中，键入网络名称、选择无线安全类型，然后单击<gui>连接</gui>。</p>
 </item>
</steps>

<p>您可能要检查无线基站/路由器的设置才能确定网络名称。有时称为 <em>BSSID</em>(基本服务集标识符)，看起来类似于：<gui>02:00:01:02:03:04</gui>。</p>

<p>您还要检查无线基站的安全设置；查找说明中的 WEP 和 WPA 等术语。</p>

<note>
 <p>您可能认为隐藏无线网络可防止不知道网络的人连接到该网络，从而提高安全性。实际情况并非如此，虽然查找隐藏网络略有难度，但它仍可被检测到。</p>
</note>

</page>
