<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="zh-CN">

  <info>
    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <desc>使用更长、更复杂的密码。</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="user-accounts#passwords"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="14.04" date="2013-10-24" status="candidate"/>
  </info>

  <title>添加密码</title>

  <note style="important">
    <p>使您的密码对您来说很容易记住，但是对别人(包括计算机程序)来说很难猜测。</p>
  </note>

  <p>选择一个好的密码将有助于保证您计算机的安全。如果您的密码很容易猜到，则有人可能猜到并获取您个人信息的访问权限。</p>
  <p>人们甚至可以使用计算机来系统地猜测您的密码，因此人类很难猜到的密码，计算机程序可能很容易破译。以下是选择一个好密码的一些技巧：</p>

  <list>
    <item>
      <p>最好在密码中混合使用大写字母、小写字母、数字、符号和空格，这样会让密码更难被外人猜到。您可以选择混用多种特殊符号，这样如果其它人尝试猜测您的密码，猜对的可能性会变得非常低。</p>
      <note>
        <p>选择密码有很多种方法，例如您可以选择一个熟悉的短语，然后取短语中每个单词的首字母。短语可以是一部电影、一本书、一首歌或者一张专辑的名字。例如 "Flatland: A Romance of Many Dimensions" 按照这种方式就可以变成 F:ARoMD 或者 faromd 或者 f: aromd，都是比较理想的密码。</p>
      </note>
    </item>
    <item>
      <p>您的密码越长越好。密码包含的字符越多，人或计算机猜测它所花费的时间越长。</p>
    </item>
    <item>
      <p>不要直接使用任何常用的单词或短语。因为自动密码破解工具一般都会优先尝试这些组合可能。研究结果表明最常见的密码就是“password”，这样的密码很容易就会被破解掉。</p>
    </item>
    <item>
      <p>不要使用任何公开的个人信息作为密码，比如重要纪念日、驾照号码或者家人的名字。</p>
    </item>
    <item>
      <p>不要使用任何名词。</p>
    </item>
    <item>
      <p>选择可以在瞬间快速输入的密码，以免在输入密码时被人轻易偷窥和记住。</p>
      <note style="tip">
        <p>永远不要把密码写在任何地方，很容易被别有用心的人找到！</p>
      </note>
    </item>
    <item>
      <p>不同的地方使用不同的密码。</p>
    </item>
    <item>
      <p>不同的帐户使用不同的密码。</p>
      <p>如果您所有的帐户都使用相同的密码，则任何猜到的人都将能够立即访问您所有的帐户。</p>
      <p>记住多个密码会比较困难。为每个服务使用不同的密码固然是最安全的，但是如果没有精力这样做，您也可以将各种服务分成数类，对不重要的服务使用同一个密码（例如随手注册的网站），而为特别重要的服务使用各不相同的密码（例如网上银行或者个人邮箱）。</p>
    </item>
    <item>
      <p>等其更改密码。</p>
    </item>
  </list>

</page>
