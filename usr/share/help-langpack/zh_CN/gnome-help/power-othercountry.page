<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-othercountry" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="power#problems"/>
    <desc>您的计算机将会工作，但可能需要一个不同的电源线或旅行电源适配器。</desc>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>我的计算机可以使用其他国家/地区的电源工作吗？</title>

<p>不同国家/地区所使用的电源的电压(通常 110V 或 220-240V)和交流频率(通常 50 Hz 或 60 Hz)有所不同。只要您具有相应的电源适配器，计算机即可使用不同国家/地区的电源工作。您可能还需要准备插头转换器。</p>

<p>如果您使用的是笔记本电脑，则您所需做的就是为电源线准备一个合适的转换插头。有些笔记本电脑的包装带有多个转换插头，因此您可能无需另外准备。如果未随附，则将现有的插头插入标准旅行适配器即可。</p>

<p>如果您使用的是台式计算机，则可以购买一根带不同插头的电缆，或使用旅行适配器。但是，此种情况下，您可能需要调整计算机电源上的电压转换开关。很多计算机没有此类开关，它们可以在任何电压下正常工作。查看计算机的背面并查找电源线插入的插座。在附近的某个位置，可能有一个标记为“110V”或“220V”的开关。如果需要，请将其打开。</p>

<note style="warning">
  <p>更改电压时一定要仔细。首先关闭所有的部件。</p>
</note>

</page>
