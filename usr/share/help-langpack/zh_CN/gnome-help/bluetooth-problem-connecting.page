<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="guide" style="problem" version="1.0 if/1.0" id="bluetooth-problem-connecting" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="bluetooth#problems"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>适配器没有开启或者驱动不正确，或者蓝牙被禁用或阻止了。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>I cannot connect my Bluetooth device</title>

  <p>您无法连接蓝牙设备(如手机或耳机)可能的原因有很多。</p>

  <terms>
    <item>
      <title>连接被阻止或不被信任</title>
      <p>有些蓝牙设备会默认拦截连接，或要求您更改设置才能允许建立连接。确保将您的设备设置为允许连接。</p>
    </item>
    <item>
      <title>蓝牙硬件无法识别</title>
      <p>Your Bluetooth adapter or dongle may not have been recognized by the
      computer. This could be because
      <link xref="hardware-driver">drivers</link> for the adapter are not
      installed. Some Bluetooth adapters are not supported on Linux, so you may
      not be able to get the right drivers for them. In this case, you will
      probably have to get a different Bluetooth adapter.</p>
    </item>
    <item>
      <title>Adapter is not switched on</title>
      <if:choose>
        <if:when test="platform:unity">
          <p>确保您的蓝牙适配器已开启。在<gui>菜单栏</gui>中单击蓝牙图标，检查确定它未禁用。</p>
        </if:when>
        <p>Make sure that your Bluetooth adapter is switched on. Open the
        Bluetooth panel and check that it is not
        <link xref="bluetooth-turn-on-off">disabled</link>.</p>
      </if:choose>
    </item>
    <item>
      <title>要连接的蓝牙设备已关闭</title>
      <p>Check that Bluetooth is turned on on the device you are trying to
      connect to, and that it is <link xref="bluetooth-visibility">discoverable
      or visible</link>. For example, if you are trying to connect to a phone,
      make sure that it is not in airplane mode.</p>
    </item>
    <item>
      <title>计算机上没有蓝牙适配器</title>
      <p>很多计算机都不具备蓝牙适配器。如果您想要使用蓝牙，可以购买适配器。</p>
    </item>
  </terms>

</page>
