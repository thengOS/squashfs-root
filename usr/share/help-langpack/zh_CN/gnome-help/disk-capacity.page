<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-capacity" xml:lang="zh-CN">
  <info>
    <link type="guide" xref="disk"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <revision pkgversion="3.4.3" date="2012-06-15" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>使用<gui>磁盘使用分析器</gui>或者<gui>系统监视器</gui>来检查磁盘容量和空闲情况。</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>检查磁盘剩余空间</title>

  <p>您可以使用<app>磁盘使用分析器</app>或<app>系统监视器</app>来检查剩余的磁盘空间。</p>

<section id="disk-usage-analyzer">
<title>使用磁盘使用分析器来检查</title>

  <p>要使用<app>磁盘使用分析器</app>检查空余磁盘空间和磁盘容量：</p>

  <list>
    <item>
      <p>Open <app>Disk Usage Analyzer</app> from the <gui>Activities</gui>
      overview. The window will display a list of file locations together with
      the usage and capacity of each.</p>
    </item>
    <item>
      <p>Click one of the items in the list to view a detailed summary of the
      usage for that item. Click the menu button, and then <gui>Scan
      Folder…</gui> or <gui>Scan Remote Folder…</gui> to scan a different
      location.</p>
    </item>
  </list>
  <p>The information is displayed according to <gui>Folder</gui>,
  <gui>Size</gui>, <gui>Contents</gui> and when the data was last
  <gui>Modified</gui>. See more details in <link href="help:baobab"><app>Disk
  Usage Analyzer</app></link>.</p>

</section>

<section id="system-monitor">

<title>使用系统监视器进行检查</title>

  <p>要使用<app>系统监视器</app>来检查空余磁盘空间和磁盘容量：</p>

<steps>
 <item>
  <p>在<gui>活动</gui>概览中，打开<app>系统监视器</app>程序。</p>
 </item>
 <item>
  <p>选择<gui>文件系统</gui>标签页查看系统的分区和磁盘空间使用情况。信息根据<gui>总计</gui>、<gui>空余</gui>、<gui>可用</gui>和<gui>已用</gui>分别显示。</p>
 </item>
</steps>
</section>

<section id="disk-full">

<title>如果磁盘太满怎么办？</title>

  <p>如果磁盘太满您应该：</p>

 <list>
  <item>
   <p>删除不重要的或者您不再使用的文件。</p>
  </item>
  <item>
   <p>制作您暂时不用重要文件的<link xref="backup-why">备份</link>，然后从磁盘上删除它们。</p>
  </item>
 </list>
</section>

</page>
