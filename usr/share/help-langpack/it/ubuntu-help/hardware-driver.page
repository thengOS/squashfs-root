<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="hardware-driver" xml:lang="it">

  <info>
    <link type="guide" xref="hardware" group="more"/>

    <desc>Un driver hardware permette al computer di usare i dispositivi ad esso collegati.</desc>

    <revision version="13.10" date="2013-09-12" status="review"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Cos'e un driver?</title>

<p>I dispositivi sono la "parte" fisica del computer. Possono essere <em>esterni</em>, per esempio stampanti e monitor, oppure <em>interni</em>, per esempio schede grafiche e audio.</p>

<p>Il computer, per essere in grado di usare questi dispositivi, necessita di sapere come poter comunicare con essi. Queste informazioni vengono fornite da software chiamati <em>driver del dispositivo</em>.</p>

<p>Quando si collega un dispositivo al computer, è necessario che sia installato il driver corretto affinché funzioni. Per esempio, se viene collegata una stampante ma il driver corretto non è disponibile, non sarà possibile usare la stampante. Solitamente, ogni modello usa uno specifico driver diverso da quello per altri modelli.</p>

<p>Su Linux, sono già presenti i driver per la maggior parte dei dispositivi; per cui, una volta collegato, tutto dovrebbe funzionare correttamente. Potrebbe comunque essere necessario installare manualmente il driver oppure potrebbe non essere disponibile.</p>

<p>Inoltre, alcuni driver esistenti sono incompleti o funzionano solo parzialmente. Per esempio, potrebbe verificarsi che non sia possibile stampare fronte e retro con la propria stampante, sebbene tutto il resto funzioni correttamente.</p>

</page>
