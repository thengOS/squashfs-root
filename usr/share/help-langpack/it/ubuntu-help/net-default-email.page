<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-default-email" xml:lang="it">
  <info>
    <link type="guide" xref="net-email"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Cambiare l'applicazione predefinita per la posta elettronica usando <gui>Dettagli</gui> in <gui>Impostazioni di sistema</gui>.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Cambiare l'applicazione predefinita per scrivere messaggi di posta elettronica</title>

<p>Quando si fa clic su un pulsante o su un collegamento per inviare un nuovo messaggio di posta (per esempio nel proprio elaboratore di testi), l'applicazione predefinita per la posta elettronica aprirà un nuovo messaggio vuoto, pronto per essere scritto. Se sono installate più applicazioni per la posta elettronica, potrebbe avviarsi quella sbagliata. Per evitare questo inconveniente, scegliere l'applicazione predefinita per la posta:</p>

<steps>
 <item>
  <p>Fare clic sull'icona a destra nella <gui>barra dei menù</gui> e selezionare <gui>Impostazioni di sistema</gui>.</p>
 </item>

 <item>
  <p>Aprire <gui>Dettagli</gui> e scegliere <gui>Applicazioni predefinite</gui> dall'elenco presente sulla parte sinistra della finestra.</p>
 </item>

 <item>
  <p>Scegliere l'applicazione per la posta da usare come predefinita modificando l'opzione <gui>Email</gui>.</p>
 </item>
</steps>

</page>
