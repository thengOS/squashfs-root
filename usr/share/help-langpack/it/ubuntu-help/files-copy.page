<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="files-copy" xml:lang="it">

  <info>
    <link type="guide" xref="files"/>
    <desc>Copiare o spostare elementi in una nuova cartella.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-15" status="review"/>
    <revision version="14.10" date="2014-09-14" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Copiare o spostare file e cartelle</title>

 <p>È possibile copiare o spostare file o cartelle in una nuova posizione trascinandoli con il mouse, usando i comandi copia e incolla oppure utilizzando le scorciatoie da tastiera.</p>

 <p>Per esempio, è possibile copiare una presentazione su un dispositivo rimovibile da portare al lavoro. È anche possibile effettuare una copia di backup di un documento prima di modificarlo (e quindi usare la copia se le modifiche non risultano soddisfacenti).</p>

 <p>Queste istruzioni si applicano sia ai file che alle cartelle: le stesse azioni possono essere usate per entrambi.</p>

<steps ui:expanded="false">
<title>Copiare e incollare file</title>
<item><p>Selezionare il file da copiare facendoci clic una sola volta.</p></item>
<item><p>Fare clic col pulsante destro e selezionare <gui>Copia</gui>, o premere <keyseq><key>Ctrl</key><key>C</key></keyseq>.</p></item>
<item><p>Spostarsi in un'altra cartella dove mettere la copia.</p></item>
<item><p>Fare clic col pulsante destro e scegliere <gui>Incolla</gui> per terminare la copia del file oppure premere <keyseq><key>Ctrl</key><key>V</key></keyseq>. Ci sarà ora una copia del file sia nella cartella originale che nell'altra cartella.</p></item>
</steps>

<steps ui:expanded="false">
<title>Tagliare e incollare file per muoverli</title>
<item><p>Selezionare il file da spostare facendoci clic una sola volta.</p></item>
<item><p>Fare clic col pulsante destro e selezionare <gui>Taglia</gui> o premere <keyseq><key>Ctrl</key><key>X</key></keyseq>.</p></item>
<item><p>Spostarsi in un'altra cartella, dove spostare il file.</p></item>
<item><p>Fare clic col pulsante destro e scegliere <gui>Incolla</gui> per terminare lo spostamento del file oppure premere <keyseq><key>Ctrl</key><key>V</key></keyseq>. Il file verrà eliminato dalla cartella originale e trasferito nell'altra cartella.</p></item>
</steps>

<steps ui:expanded="false">
<title>Trascinare file per copiarli o spostarli</title>
<item><p>Aprire il gestore di file e spostarsi nella cartella che contiene il file da copiare.</p></item>
<item><p>Fare clic su <gui>File</gui> nella barra superiore, selezionare <gui>Nuova finestra</gui> (o premere <keyseq><key>Ctrl</key><key>N</key></keyseq>) per aprire una seconda finestra. Nella nuova finestra, posizionarsi nella cartella dove spostare o copiare il file.</p></item>
<item>
 <p>Fare clic e trascinare il file da una finestra all'altra. Questa operazione <em>sposterà</em> il file se la destinazione sarà sullo <em>stesso</em> dispositivo, o lo <em>copierà</em> se la destinazione sarà su un <em>diverso</em> dispositivo.</p>
 <p>Per esempio, se si trascina un file da un supporto USB nella cartella Home, questo verrà copiato in quanto viene trascinato da un dispositivo a un altro.</p>
 <p>Si può forzare la copia del file trascinandolo e tenendo premuto il tasto <key>Ctrl</key>, o forzarne lo spostamento trascinandolo e tenendo premuto il tasto <key>Maiusc</key>.</p>
 </item>
</steps>

<note>
 <p>Non è possibile copiare o spostare file in una cartella impostata in <em>sola lettura</em>. Alcune cartelle sono impostate in sola lettura per evitare che i loro contenuti possano essere modificati. L'impostazione di sola lettura può essere modificata <link xref="nautilus-file-properties-permissions">cambiando i permessi sul file</link>.</p>
</note>

</page>
