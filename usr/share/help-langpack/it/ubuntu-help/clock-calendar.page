<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="clock-calendar" xml:lang="it">

  <info>
    <link type="guide" xref="clock"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <desc>Mostrare gli appuntamenti nel calendario in alto nello schermo.</desc>

    <revision pkgversion="3.8.0" date="2013-03-09" status="outdated"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

 <title>Appuntamenti in calendario</title>

<p>Un'applicazione integrata per la gestione del calendario, chiamata <app>Calendario</app>, consente di organizzare i propri appuntamenti ed eventi. È possibile consultare fino a un massimo di cinque eventi, nell'arco temporale di quattro settimane, facendo clic sull'orologio nella <gui>barra dei menù</gui>.</p>

<note><p>L'integrazione degli appuntamenti è supportata anche da un'applicazione di posta e calendario chiamata <app>Evolution</app> che può essere installata separatamente.</p></note>

<section id="launch-calendar-application">
  <title>Avviare l'applicazione Calendario</title>
<p>Appuntamenti ed eventi possono essere gestiti tramite l'applicazione integrata <app>Calendario</app>.</p>

<steps>
  <item><p>Fare clic sull'icona della <gui>Dash</gui> nel <link xref="unity-launcher-intro">Launcher</link>.</p></item>
  <item><p>Digitare <input>calendario</input> in <gui>Cerca sul computer</gui> per trovare l'applicazione <app>Calendario</app>.</p></item>
  <item><p>Fare clic sull'icona dell'applicazione <gui>Calendario</gui>.</p></item>
</steps>

<p>In alternativa, è possibile digitare <input>gnome-calendar</input> nel <app>Terminale</app>.</p>
</section>


</page>
