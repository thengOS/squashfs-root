<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="windows-key" xml:lang="it">

  <info>
    <link type="guide" xref="keyboard"/>
    <desc>Il tasto Super fornisce accesso alla Dash e al Launcher.</desc>
    <revision version="14.04" date="2014-03-07" status="review"/>
    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
       <name>Ubuntu Documentation Team</name>
       <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>
 
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Cos'è il tasto "Super"?</title>

  <p>Questo tasto solitamente può essere trovato nella parte in basso a sinistra della tastiera, vicino al tasto <key>Alt</key>, e solitamente ha sopra un'icona con finestre quadrate. A volte viene chiamato tasto Windows, tasto logo o tasto di sistema.</p>

<note>
  <p>Se si utilizza una tastiera Apple, non esiste alcun tasto Windows su di essa. Al suo posto è possibile utilizzare il tasto <key>⌘</key> (Command).</p>
</note>

  <p>Il tasto Super offre una funzione speciale in <em>Unity</em>. Quando viene premuto viene visualizzata la Dash. Se il tasto Super viene <em>tenuto premuto</em>, verrà visualizzata una schermata con molte delle scorciatoie da tastiera di Unity fino a quando il tasto non verrà rilasciato.</p>

<p>Il tasto Super può tuttavia fare molto di più. Per imparare gli ulteriori utilizzi del tasto <em>Super</em>, consultare la pagina <link xref="shell-keyboard-shortcuts">delle scorciatoie da tastiera</link>.</p>

<!--
<p>To change which key is used to display the activities overview:</p>

  <steps>
    <item>
      <p>Click your name on the menu bar and select <gui>System Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Keyboard</gui>.</p>
    </item>
    <item>
      <p>Click the <gui>Shortcuts</gui> tab.</p>
    </item>
    <item>
      <p>Select <gui>System</gui> on the left side of the window, and
      <gui>Show the activities overview</gui> on the right.</p>
    </item>
    <item>
      <p>Click the current shortcut definition on the far right.</p>
    </item>
    <item>
      <p>Hold down the desired key combination.</p>
    </item>
  </steps>
-->


</page>
