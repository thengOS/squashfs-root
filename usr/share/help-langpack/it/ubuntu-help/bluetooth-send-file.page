<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="guide" style="task" version="1.0 if/1.0" id="bluetooth-send-file" xml:lang="it">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="files-share"/>

    <revision pkgversion="3.8.2" version="0.2" date="2013-05-16" status="review"/>
    <revision version="13.10" date="2013-09-20" status="review"/>

    <desc>Condividere file con dispositivi Bluetooth come il proprio telefono.</desc>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Inviare un file a un dispositivo Bluetooth</title>

<if:choose>
  <if:when test="platform:unity">
    <p>È possibile inviare file ai dispositivi Bluetooth connessi, per esempio alcuni telefoni cellulari oppure altri computer. Alcuni generi di dispositivi non consentono il trasferimento di file o ne consentono solo alcune tipologie. È possibile eseguire tale operazione in tre modi: utilizzando l'icona Bluetooth nella barra dei menù, dalla finestra di impostazioni Bluetooth o direttamente dal gestore di file.</p>
  </if:when>
  <p>È possibile inviare file ai dispositivi Bluetooth connessi, come telefoni cellulari e altri computer. Alcuni tipi di dispositivi non permettono il trasferimento di file o di specifici tipi di file. È possibile effettuare questa operazione usando l'icona Bluetooth nella barra superiore o dalla finestra di impostazione del Bluetooth.</p>
</if:choose>

<if:choose>
  <if:when test="platform:unity">
  <p>Per inviare file direttamente dal gestore di file, consultare <link xref="files-share"/>.</p>
  </if:when>
</if:choose>

  <note style="important">
    <p>Prima di cominciare, assicurarsi che il Bluetooth sia abilitato sul computer. Consultare <link xref="bluetooth-turn-on-off"/>.</p>
  </note>

  <steps>
    <title>Inviare file utilizzando l'icona Bluetooth</title>
    <item>
      <if:choose>
        <if:when test="platform:unity">
      <p>Fare clic sull'icona Bluetooth nella barra dei menù e selezionare <gui>Invia file al dispositivo</gui>.</p>
        </if:when>
      <p>Fare clic sull'icona Bluetooth nella barra superiore e selezionare <gui>Invia file al dispositivo</gui>.</p>
      </if:choose>
    </item>
    <item>
      <p>Scegliere il file che si vuole inviare e fare clic su <gui>Seleziona</gui>.</p>
      <p>Per inviare più file in una cartella, tenere premuto <key>Ctrl</key> per selezionare ciascun file.</p>
    </item>
    <item>
      <p>Selezionare dall'elenco il dispositivo a cui inviare i file e fare clic su <gui>Invia</gui>.</p>
      <p>L'elenco dei dispositivi mostra sia i <link xref="bluetooth-connect-device">dispositivi a cui si è già connessi</link> sia i <link xref="bluetooth-visibility">dispositivi visibili</link> nel raggio d'azione. Nel caso in cui non si sia ancora connessi al dispositivo selezionato, si verrà invitati ad associare il dispositivo dopo aver fatto clic su <gui>Invia</gui>. Questa operazione richiederà probabilmente conferma sull'altro dispositivo.</p>
      <p>Nel caso vi siano molti dispositivi, è possibile limitare l'elenco solo a specifici tipi di dispositivo utilizzando il menù a discesa <gui>Tipo di dispositivo</gui>.</p>
    </item>
    <item>
      <p>Il proprietario del dispositivo deve solitamente premere un pulsante per accettare il file. Una volta accettato o rifiutato, il risultato del trasferimento del file sarà mostrato sullo schermo.</p>
    </item>
  </steps>

  <steps>
    <title>Inviare file dalle impostazioni Bluetooth</title>
    <item>
      <if:choose>
        <if:when test="platform:unity">
          <p>Fare clic sull'icona Bluetooth nella barra dei menù e selezionare <gui>Impostazioni Bluetooth</gui>.</p>
        </if:when>
        <p>Fare clic sull'icona Bluetooth nella barra superiore e selezionare <gui>Impostazioni Bluetooth</gui>.</p>
      </if:choose>
    </item>
    <item><p>Selezionare dall'elenco di sinistra il dispositivo a cui inviare i file. Esso mostrerà solo i dispositivi ai quali si è già connessi. Consultare <link xref="bluetooth-connect-device"/>.</p></item>
    <item><p>Nelle informazioni del dispositivo sulla destra, fare clic su <gui>Invia file</gui>.</p></item>
    <item>
      <p>Scegliere il file che si vuole inviare e fare clic su <gui>Seleziona</gui>.</p>
      <p>Per inviare più file in una cartella, tenere premuto <key>Ctrl</key> per selezionare ciascun file.</p>
    </item>
    <item>
      <p>Il proprietario del dispositivo deve solitamente premere un pulsante per accettare il file. Una volta accettato o rifiutato, il risultato del trasferimento del file sarà mostrato sullo schermo.</p>
    </item>
  </steps>
</page>
