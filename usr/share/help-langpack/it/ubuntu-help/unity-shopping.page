<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="unity-shopping" xml:lang="it">

  <info>
    <link type="guide" xref="unity-dash-intro"/>
    <link type="seealso" xref="unity-dash-intro#dash-home"/>

    <desc>I risultati online rendono la Dash più utile e aiutano a sovvenzionare lo sviluppo di Ubuntu.</desc>

    <revision version="14.04" date="2014-04-03" status="review"/>
    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="editor">
       <name>Ubuntu Documentation Team</name>
       <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Perché nella Dash sono presenti collegamenti a siti di commercio online?</title>

<p>Oltre a essere d'aiuto per trovare file e applicazioni sul proprio computer, la <gui>Dash</gui> è in grado di mostrare risultati da ricerche online. Tra le varie risorse online interrogate vie è anche Amazon.com.</p>

<p>Quando vengono effettuati acquisti di musica o di prodotti da queste fonti, Canonical riceve una piccola parte dei profitti in cambio della propria attività di promozione di questi negozi. Canonical, la società che ha creato e continua a sostenere il progetto Ubuntu, usa questo denaro per migliorare Ubuntu.</p>

<p>Come impostazione predefinita, non vengono effettuate ricerche online.</p>

<section id="enable-shopping">
  <title>Attivare i risultati delle ricerche online</title>

  <p>Per ricevere suggerimenti dalle ricerche online, è possibile abilitare questa funzionalità attraverso la sezione <gui>Sicurezza e privacy</gui> delle impostazioni di sistema.</p>

<steps>

  <item><p>Fare clic sul <gui>menù di sistema</gui> sulla destra della <gui>barra dei menù</gui> e selezionare <gui>Impostazioni di sistema</gui>.</p></item>

  <item><p>Aprire <gui>Sicurezza &amp; Privacy</gui> e selezionare la scheda <gui>Ricerca</gui>.</p></item>

  <item><p>Attivare <gui>Includere i risultati da ricerche online</gui>.</p></item>


</steps>
</section>

</page>
