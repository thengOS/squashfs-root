<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="user-add" xml:lang="it">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>
    <link type="seealso" xref="shell-guest-session"/>
    <desc>Aggiungere nuovi utenti al sistema.</desc>
    <!-- Setting 3.4.0 version to final for release, but we need to do something
         about the various comments in here, so incomplete for 3.4.1.
    -->
    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Aggiungere un nuovo account utente</title>

  <p>È possibile aggiungere molteplici account al proprio computer: uno per ogni familiare o, all'interno di un'azienda, uno per ogni dipendente. Ogni utente avrà a disposizione una propria cartella personale per documenti, foto e per le proprie impostazioni.</p>

<steps>
  <item><p>Fare clic sull'icona a destra nella <gui>barra dei menù</gui> e selezionare <gui>Impostazioni di sistema</gui>.</p></item>
  <item><p>Aprire <gui>Account utente</gui>.</p></item>

  <item><p>È necessario avere <link xref="user-admin-explain">privilegi di amministrazione</link> pr aggiungere account utente. Fare clic su <gui>Sblocca</gui> nell'angolo in alto a destra e digitare la propria password.</p></item>

  <item><p>Nell'elenco degli account sulla sinistra, fare clic sul pulsante <gui>+</gui> per aggiungere un nuovo account.</p></item>

  <item><p>Se è necessario che il nuovo utente abbia <link xref="user-admin-explain">accesso come amministratore</link> al computer, selezionare <gui>Amministratore</gui> come tipo di account. Gli amministratori possono eseguire operazioni come l'aggiunta e l'eliminazione di utenti, l'installazione di software e driver e la modifica di data e orario.</p></item>

  <item><p>Inserire il nome completo del nuovo utente. Il nome dell'utente verrà completato automaticamente in base al nome completo fornito. Il valore predefinito solitamente è una buona scelta, ma è comunque possibile modificarlo.</p></item>

  <item><p>Fare clic su <gui>Crea</gui>.</p></item>

  <item><p>L'account è inizialmente disabilitato finché non si sceglie come gestire la password dell'utente. Nella sezione <gui>Opzioni di accesso</gui> fare clic su <gui>Account disabilitato</gui> accanto a <gui>Password</gui>. Dall'elenco a discesa <gui>Azione</gui>, selezionare <gui>Impostare una password</gui> e far digitare all'utente la password nei campi <gui>Nuova password</gui> e <gui>Conferma password</gui> (per informazioni sulle password, consultare <link xref="user-goodpassword"/>).</p>
  <p>È possibile anche utilizzare il pulsante accanto al campo <gui>Nuova password</gui> per selezionare una password casuale. Solitamente queste risultano difficili per altre persone da indovinare, ma allo stesso tempo lo sono anche da ricordare.</p></item>
  <item><p>Fare clic su <gui>Cambia</gui>.</p></item>
</steps>

<note><p>Nella finestra <gui>Account utente</gui> è possibile fare clic sull'immagine vicino al nome utente sulla destra per impostare un'immagine per l'account; immagine che verrà visualizzata nella finestra d'accesso. Sono disponibili una serie di immagini pronte per l'uso e in alternativa è possibile scattare una foto con la propria telecamera per PC.</p>
</note>

</page>
