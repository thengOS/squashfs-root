<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="it">

  <info>
    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <desc>Usare password più lunghe e complicate.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="user-accounts#passwords"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="14.04" date="2013-10-24" status="candidate"/>
  </info>

  <title>Scegliere una password sicura</title>

  <note style="important">
    <p>Cercare una password facile da ricordare, ma molto difficile per altri (compresi programmi per computer) da indovinare.</p>
  </note>

  <p>Scegliere una buona password aiuta a rendere il proprio computer più sicuro. Se la password è facile da indovinare, qualcuno potrebbe trovarla e accedere alle proprie informazioni personali.</p>
  <p>Alcune persone possono usare i computer per indovinare più facilmente le password di altri utenti: una password difficile da indovinare per una persona, potrebbe non esserlo per un computer. Di seguito vengono indicati alcuni consigli da seguire per scegliere una buona password:</p>

  <list>
    <item>
      <p>Usare un insieme di lettere maiuscole e minuscole, numeri, simboli e spazi. In questo modo la propria password sarà più difficile da indovinare: essendoci più simboli da cui scegliere, ci sarà un maggior numero di password da dover provare prima di trovare quella corretta.</p>
      <note>
        <p>Un buon metodo per scegliere una password consiste nel prendere la prima lettera di ogni parola in una frase facile da ricordare. Tale frase può essere il titolo di un film, un libro, una canzone o un album musicale. Per esempio, "Flatlandia: Racconto fantastico a più dimensioni" diventerebbe "F:Rfapd" o "frfapd" o "f: rfapd".</p>
      </note>
    </item>
    <item>
      <p>Cercare di creare password più lunghe possibili. Più caratteri contengono, più tempo ci vuole a un'altra persona o a un computer per indovinarla.</p>
    </item>
    <item>
      <p>Non usare parole presenti in un normale dizionario di una qualsiasi lingua. I software per trovare le password utilizzano i dizionari come primo metodo. La password utilizzata più comunemente è "password": anche le persone possono indovinare tali password in poco tempo.</p>
    </item>
    <item>
      <p>Non usare informazioni personali come date di nascita, targhe dei propri veicoli o nomi dei propri familiari.</p>
    </item>
    <item>
      <p>Non usare alcun nome.</p>
    </item>
    <item>
      <p>Scegliere una password che può essere digitata velocemente in modo da diminuire le possibilità per altre persone di vedere cosa viene digitato.</p>
      <note style="tip">
        <p>Mai scrivere la propria password da qualche parte: potrebbe essere trovata!</p>
      </note>
    </item>
    <item>
      <p>Usare password diverse in differenti occasioni.</p>
    </item>
    <item>
      <p>Usare password diverse per differenti account.</p>
      <p>Se viene usata la stessa password per tutti gli account, chiunque sia in grado di trovarla potrà accedere a tutti gli account senza problemi.</p>
      <p>Ricordarsi molte password può comunque essere difficile. Benché non sicuro come l'utilizzo di diverse password in differenti occasioni, potrebbe risultare più facile usare la stessa password in occasioni non molto importanti (come alcune categorie di siti web) e password diversi per le occasioni più importanti (come la propria email, l'account dell'home banking).</p>
    </item>
    <item>
      <p>Cambiare la password regolarmente.</p>
    </item>
  </list>

</page>
