<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-othercountry" xml:lang="it">

  <info>
    <link type="guide" xref="power#problems"/>
    <desc>Il computer funziona, ma può essere necessario un diverso cavo di alimentazione o un adattatore da viaggio.</desc>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Il computer può funzionare con la presa elettrica di un altro Paese?</title>

<p>Le prese elettriche e il sistema elettrico possono variare da Paese a Paese, presentando generalmente differenti tensioni (solitamente 110V o 220-240V) e frequenze (50 o 60 Hz). Il computer dovrebbe essere in grado di funzionare con una diversa alimentazione elettrica utilizzando un alimentatore o un adattatore appropriato (potrebbe anche essere necessario attivare o disattivare un interruttore su tali dispositivi).</p>

<p>Nel caso di un computer portatile, è necessario unicamente utilizzare una spina compatibile con l'adattatore di corrente. Alcuni computer portatili hanno in dotazione più spine per l'adattatore e pertanto quella compatibile potrebbe essere già disponibile. In caso contrario, potrebbe essere sufficiente inserire la spina esistente in un adattatore da viaggio standard.</p>

<p>Nel caso di un computer desktop, è possibile utilizzare un cavo di alimentazione con una spina diversa o usare un adattatore da viaggio: in questo caso, tuttavia, può essere necessario modificare la tensione dell'alimentatore del computer per mezzo dell'apposito interruttore, se presente; molti computer non sono dotati di questo accessorio e funzionano con qualsiasi tensione. Guardare la parte posteriore del computer nella zona in cui c'è la presa in cui è collegato il cavo di alimentazione: nelle vicinanze ci potrebbe essere un piccolo interruttore contrassegnato "110V" o "230V" (per esempio): azionarlo se necessario.</p>

<note style="warning">
  <p>Fare attenzione quando si cambiano i cavi di alimentazione o si usano adattatori da viaggio: se possibile, spegnere tutto prima di effettuare queste operazioni.</p>
</note>

</page>
