<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-lock" xml:lang="it">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>
    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-07" status="review"/>
    <desc>Impedire ad altre persone di usare il computer in caso di assenza.</desc>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Bloccare automaticamente lo schermo</title>

  <p>Quando ci si allontana dal computer, bisognerebbe <link xref="shell-exit#lock-screen">bloccare lo schermo</link> per impedire ad altre persone l'uso del computer e l'accesso ai dati contenuti. La sessione attiva verrà mantenuta e tutte le applicazioni continueranno a funzionare, ma occorrerà immettere la password per usare nuovamente il computer. Lo schermo può essere bloccato manualmente o automaticamente.</p>

  <steps>
    <item><p>Fare clic sull'icona a destra nella <gui>barra dei menù</gui> e selezionare <gui>Impostazioni di sistema</gui>.</p></item>
    <item><p>Selezionare <gui>Luminosità e blocco</gui>.</p></item>
    <item><p>Assicurarsi che il controllo <gui>Blocco</gui> sia attivato, quindi selezionare un intervallo di tempo dall'elenco a discesa sottostante: lo schermo verrà bloccato trascorso questo lasso di tempo. È possibile anche selezionare <gui>Arresto dello schermo</gui> per bloccare lo schermo dopo lo spegnimento automatico, attivato dall'elenco a discesa soprastante <gui>Spegnere lo schermo dopo</gui>.</p></item>
  </steps>

</page>
