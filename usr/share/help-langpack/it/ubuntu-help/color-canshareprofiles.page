<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-canshareprofiles" xml:lang="it">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <desc>Condividere i profili del colore non è mai una buona idea in quanto l'hardware cambia nel tempo.</desc>
    <revision version="13.10" date="2013-09-07" status="review"/>
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>È possibile condividere il profilo del colore?</title>

  <p>I profili del colore creati personalmente sono specifici all'hardware e alle condizioni d'illuminazione per cui sono stati calibrati. Un monitor attivo per poche centinaia di ore avrà un profilo del colore molto diverso da quello di un monitor simile con il numero seriale successivo che è stato acceso per mille ore.</p>
  <p>Questo significa che nel caso in cui si condivida il proprio profilo del colore con un altro utente, si potrebbe essere <em>più vicini</em> alla calibrazione, ma nel migliore dei casi è fuorviante dire che il loro monitor sia calibrato.</p>
  <p>Similmente, a meno che tutti abbiano un'illuminazione controllata raccomandata (niente luce solare dalle finestre, muri neri, lampade a luce diurna ecc.) in una stanza dove hanno luogo la visualizzazione e la modifica di immagini, risulta inutile condividere un profilo creato nelle proprie condizioni d'illuminazione specifiche.</p>

  <note style="warning">
    <p>Prestare molta attenzione alla condizioni d'uso dei profili scaricati dai siti web o creati appositamente per voi.</p>
  </note>

</page>
