<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="shell-exit" xml:lang="it">

  <info>

    <link type="guide" xref="index" group="shell-exit"/>
    <link type="guide" xref="shell-overview#desktop" group="#last"/>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-hibernate"/>

    <desc>Imparare come terminare la sessione, cambiare utente e altro.</desc>
    <!-- Should this be a guide which links to other topics? -->

    <revision pkgversion="3.6.0" date="2012-09-15" status="review"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="author">
      <name>Alexandre Franke</name>
      <email>afranke@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Terminare la sessione, spegnere il computer, cambiare utente</title>

  <p>Una volta terminato di usare il computer, è possible spegnerlo, sospenderlo (per risparmiare energia) oppure lasciarlo acceso e terminare la sessione.</p>

<section id="logout">
<title>Terminare la sessione o passare a un altro utente</title>
  <p>Per permettere l'uso del computer a un altro utente, è possibile sia terminare la propria sessione sia lasciare in esecuzione la sessione e semplicemente cambiare utente. Se si cambia utente, tutte le proprie applicazioni resteranno in esecuzione, disponibili come le si era lasciate quando si riaccederà.</p>
  <p>Per terminare la sessione o cambiare utente, fare clic sul <link xref="unity-menubar-intro">menù di sistema</link> alla destra della barra dei menù e scegliere l'azione appropriata.</p>
</section>

<section id="lock-screen">
<info>
  <link type="seealso" xref="session-screenlocks"/>
</info>
<title>Bloccare lo schermo</title>
  <p>Se ci si allontana dal computer per breve tempo, è opportuno bloccare lo schermo in modo da impedire ad altre persone di accedere ai propri file o alle applicazioni in esecuzione. Al proprio ritorno sarà sufficiente inserire la propria password per accedere nuovamente. Se non si blocca lo schermo, questo verrà bloccato automaticamente dopo un certo tempo.</p>

  <p>Per bloccare lo schermo, fare clic sul <gui>menù di sistema</gui> nella barra dei menù e selezionare <gui>Blocca schermo</gui>.</p>

  <p>Quando lo schermo è bloccato, altri utenti possono entrare nel loro account facendo clic su <gui>Cambia utente</gui> nella schermata della password. Quando avranno finito, sarà possibile ritornare al proprio ambiente di lavoro.</p>
</section>

<section id="suspend">
<info>
  <link type="seealso" xref="power-suspend"/>
</info>
<title>Sospendere</title>

  <p>Per salvare energia, è utile sospendere il computer quando non viene utilizzato. Se si utilizza un computer portatile, il sistema viene messo in sospensione automaticamente alla chiusura del coperchio del computer, salvando lo stato della sessione nelle memoria del computer e disattivando la maggior parte delle funzioni del computer. In questa modalità di operazione, il computer continua a utilizzare una minima parte di energia.</p>

  <p>Per sospendere il computer manualmente, fare clic sul <gui>menù di sistema</gui> nella barra dei menù e selezionare <gui>Sospendi</gui>.</p>
</section>

<section id="shutdown">
<title>Spegnere o riavviare</title>

  <p>Per spegnere il computer o riavviarlo, fare clic sul <gui>menù di sistema</gui> e selezionare <gui>Arresta</gui>.</p>

  <p>Se altri utenti sono collegati non è consentito arrestare o riavviare il computer poiché questo terminerebbe le loro sessioni. In tali casi, potrebbe venir richiesta la password di amministratore per arrestare il computer.</p>

</section>
</page>
