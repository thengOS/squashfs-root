<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="files-delete" xml:lang="it">

  <info>
    <link type="guide" xref="files"/>
    <link type="seealso" xref="files-recover"/>
    <desc>Rimuovere file e cartelle non più necessarie.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
  </info>

<title>Eliminare file e cartelle</title>

  <p>Si possono eliminare file e cartelle non più necessari. Quando si elimina un elemento, questo viene spostato nella cartella <gui>Cestino</gui>, dove è conservato fino a quando il cestino non viene svuotato. Si possono <link xref="files-recover">ripristinare elementi</link> dalla cartella <gui>Cestino</gui> nella posizione originale, se nuovamente necessari, o se sono stati cancellati accidentalmente.</p>

  <steps>
    <title>Per spostare un file nel cestino:</title>
    <item><p>Selezionare il file da mettere nel cestino facendoci clic una sola volta.</p></item>
    <item><p>Premere <key>Canc</key> sulla tastiera o trascinare l'elemento nel <gui>Cestino</gui> nel riquadro laterale.</p></item>
  </steps>

  <p>Per eliminare definitivamente i file, e liberare spazio sul computer, è necessario svuotare il cestino. Per svuotare il cestino, fare clic con il pulsante destro del mouse sul <gui>Cestino</gui> nel riquadro laterale e selezionare <gui>Svuota cestino</gui>.</p>

  <section id="permanent">
    <title>Eliminare definitivamente un file</title>
    <p>È possibile eliminare immediatamente un file in maniera definitiva, senza doverlo spostare prima nel cestino.</p>

  <steps>
    <title>Per eliminare definitivamente un file:</title>
    <item><p>Selezionare l'elemento da eliminare.</p></item>
    <item><p>Premere e mantenere premuto il tasto <key>Maiusc</key>, quindi premere <key>Canc</key> sulla tastiera.</p></item>
    <item><p>Dato che l'azione non può essere annullata, verrà richiesta una conferma prima dell'eliminazione del file o della cartella.</p></item>
  </steps>

  <note style="tip"><p>Se è necessario eliminare frequentemente file senza usare il cestino (per esempio, se si lavora spesso con dati sensibili) è possibile aggiungere una voce <gui>Elimina</gui> al menù per file e cartelle che compare al clic destro del mouse. Fare clic su <gui>File</gui> nella barra dei menù, selezionare <gui>Preferenze</gui>, la scheda <gui>Comportamento</gui> e quindi <gui>Includere un comando «Elimina» che scavalchi il cestino</gui>.</p></note>

  <note><p>I file eliminati da un <link xref="files#removable">dispositivo rimovibile</link> potrebbero non essere visibili utilizzando altri sistemi operativi, come Windows o Mac OS. I file sono ancora presenti e saranno disponibili al successivo utilizzo del dispositivo in ambiente Linux.</p></note>

  </section>
</page>
