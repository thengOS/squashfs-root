<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="tip" id="keyboard-nav" xml:lang="it">
  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Ubuntu Documentation Team</name>
       <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>

    <revision version="14.04" date="2014-03-07" status="review"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Usare le applicazioni e il desktop senza un mouse.</desc>
  </info>

  <title>Navigazione da tastiera</title>

  <p>Questa pagina descrive la navigazione da tastiera per persone che non possono usare un mouse o altri dispositivi di puntamento, o che vogliono usare la tastiera il più possibile. Per scorciatoie da tastiera utili per tutti gli utenti, consultare <link xref="shell-keyboard-shortcuts"/>.</p>

  <note style="tip">
    <p>Se non è possibile usare un dispositivo di puntamento come un mouse, è possibile controllare il puntatore del mouse usando il tastierino numerico sulla tastiera. Per maggiori informazioni, consultare <link xref="mouse-mousekeys"/>.</p>
  </note>

<table frame="top bottom" rules="rows">
  <title>Spostarsi all'interno dell'interfaccia grafica</title>
  <tr>
    <td><p><key>Tab</key> e <keyseq><key>Ctrl</key><key>Tab</key></keyseq></p></td>
    <td>
      <p>Sposta il focus tra i diversi controlli. <keyseq><key>Ctrl</key><key>Tab</key></keyseq> consente lo spostamento tra gruppi di controlli, per esempio da un riquadro laterale al contenuto principale, e può anche uscire da un controllo che utilizza lo stesso <key>Tab</key>, come un'area di testo.</p>
      <p>Mantenere premuto <key>Maiusc</key> per spostare il focus in ordine inverso.</p>
    </td>
  </tr>
  <tr>
    <td><p>Tasti freccia</p></td>
    <td>
      <p>Sposta la selezione tra elementi in un singolo controllo o tra un insieme di controlli correlati. Usare i tasti freccia per dare il focus ai pulsanti pulsanti in una barra degli strumenti, per selezionare elementi nella vista a elenco o a icone o per selezionare un pulsante radio da un gruppo.</p>
      <p>In una vista ad albero, usare i tasti freccia sinistra e destra per ridurre ed espandere elementi con sottocartelle.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key>Tasti freccia</keyseq></p></td>
    <td><p>Nella vista a elenco o a icone, sposta il focus della tastiera su un altro elemento senza cambiare l'elemento selezionato.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Maiusc</key>Tasti freccia</keyseq></p></td>
    <td><p>Nella vista a elenco o a icone, seleziona tutti gli elementi a partire da quello selezionato fino quello attuale col focus.</p></td>
  </tr>
  <tr>
    <td><p><key>Spazio</key></p></td>
    <td><p>Attiva un elemento col focu come un pulsante, un'opzione o un elemento di un elenco.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Spazio</key></keyseq></p></td>
    <td><p>Nella vista a elenco o a icone, seleziona o deseleziona l'elemento col focus senza deselezionare gli altri elementi.</p></td>
  </tr>
  <tr>
    <td><p><key>Alt</key></p></td>
    <td><p>Tenere premuto il tasto <key>Alt</key> per mostrare gli <em>acceleratori</em>: lettere sottolineate di elementi del menù, pulsanti e altri controlli. Premere <key>Alt</key> assieme alla lettera sottolineata per attivare il controllo, proprio come se si fosse fatto clic su di esso.</p></td>
  </tr>
  <tr>
    <td><p><key>Esc</key></p></td>
    <td><p>Chiude un menù, un pop up, un selettore o una finestra.</p></td>
  </tr>
  <tr>
    <td><p><key>F10</key></p></td>
    <td><p>Aprire il primo menù della barra dei menù di una finestra; usare i tasti freccia per spostarsi nei menù.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Maiusc</key><key>F10</key></keyseq> o il tasto menù</p></td>
    <td>
      <p>Fa apparire il menù contestuale per la selezione corrente, come con un clic col pulsante destro del mouse.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>F10</key></keyseq></p></td>
    <td><p>Nel gestore di file, fa apparire il menù contestuale per la cartella corrente, come con un clic col pulsante destro del mouse sullo sfondo e non su un elemento.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>PagSu</key></keyseq> e <keyseq><key>Ctrl</key><key>PagGiù</key></keyseq></p></td>
    <td><p>In una interfaccia a schede, passa alla scheda a sinistra o a destra.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Spostarsi nell'ambiente grafico</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-tick"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-updown"/>
</table>

<table frame="top bottom" rules="rows">
  <title>Spostarsi tra le finestre</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>Chiude la finestra attuale.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key xref="windows-key">Super</key><key>↓</key></keyseq></p></td>
    <td><p>Ripristinare le dimensioni originarie di una finestra massimizzata.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
    <td><p>Sposta la finestra attiva. Premere <keyseq><key>Alt</key><key>F7</key></keyseq> e usare i tasti freccia per spostare la finestra. Premere <key>Invio</key> per terminare lo spostamento della finestra o <key>Esc</key> per riportarla alla sua posizione originale.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
    <td><p>Ridimensiona la finestra attiva. Premere <keyseq><key>Alt</key><key>F8</key></keyseq> e i tasti freccia per ridimensionare la finestra. Premere <key>Invio</key> per terminare il ridimensionamento o <key>Esc</key> per riportarla alla dimensione originale.</p></td>
  </tr>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-shift-updown"/>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key xref="windows-key">Super</key><key>↑</key></keyseq></p></td>
    <td><p><link xref="shell-windows-maximize">Massimizza</link> una finestra.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key xref="windows-key">Super</key><key>←</key></keyseq></p></td>
    <td><p>Massimizza verticalmente una finestra sul lato sinistro dello schermo.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key xref="windows-key">Super</key><key>→</key></keyseq></p></td>
    <td><p>Massimizza verticalmente una finestra sul lato destro dello schermo.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Spazio</key></keyseq></p></td>
    <td><p>Fa apparire il menù della finestra, come con un clic col pulsante destro del mouse sulla barra del titolo.</p></td>
  </tr>
</table>

</page>
