<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="files-recover" xml:lang="it">

  <info>
    <link type="guide" xref="files" group="more"/>
    <link type="seealso" xref="files-lost"/>
    <desc>I file eliminati sono normalmente spostati nel Cestino, ma possono essere ripristinati.</desc>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision version="13.10" date="2013-10-23" status="review"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Ripristinare i file dal cestino</title>
    <p>Se si elimina un file con il gestore di file, il file viene di norma spostato nel <gui>Cestino</gui>, e dovrebbe essere possibile ripristinarlo.</p>
    <steps>
      <title>Per ripristinare un file dal Cestino:</title>
        <item><p>Aprire il <gui>Launcher</gui> e fare clic sull'icona del <app>Cestino</app> posta in fondo al Launcher.</p></item>
        <item><p>Se il file eliminato è presente, fare clic su di esso con il l pulsante destro del mouse e selezionare <gui>Ripristina</gui>. Il file sarà ripristinato nella cartella dalla quale era stato eliminato.</p></item>
    </steps>

  <p>Se il file è stato eliminato premendo <keyseq><key>Maiusc</key><key>Canc</key></keyseq> o usando la riga di comando, il file viene definitivamente eliminato. I file definitivamente eliminati non possono essere ripristinati dal <gui>Cestino</gui>.</p>

  <p>Esistono diversi strumenti che talvolta consentono di ripristinare file definitivamente eliminati. Di solito questi strumenti non sono comunque di semplice utilizzo. Se accidentalmente si elimina definitivamente un file, la cosa migliore da fare è probabilmente chiedere un consiglio su un forum di supporto su come ripristinarlo.</p>

</page>
