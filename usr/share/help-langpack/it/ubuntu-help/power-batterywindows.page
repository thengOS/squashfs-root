<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-batterywindows" xml:lang="it">

  <info>
    <link type="guide" xref="power#battery"/>
    <link type="seealso" xref="power-batteryestimate"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-10-23" status="review"/>

    <desc>È possibile che delle modifiche da parte del produttore e stime diverse della vita della batteria causino tale problema.</desc>
    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Perché la batteria dura meno rispetto a Windows o Mac OS?</title>

<p>Alcuni computer sembra abbiano una minore durata della batteria quando eseguono Linux anziché Windows o Mac OS. Una possibile spiegazione per questo è dovuta al fatto che i produttori installano software speciale per Windows/Mac OS che ottimizza varie impostazioni hardware e software per un preciso modello. Queste modifiche sono spesso altamente specifiche e possono non essere documentate, per cui includerle in Linux è difficile.</p>

<p>Sfortunatamente, non c'è un modo semplice per applicare da sé queste modifiche senza una precisa conoscenza di quali siano. Potrebbe essere utile, invece, applicare alcuni <link xref="power-batterylife">metodi di risparmio energetico</link>. Se il computer ha un <link xref="power-batteryslow">processore a velocità variabile</link>, può essere utile modificarne le impostazioni.</p>

<p>Un'altra possibile spiegazione per la discrepanza è il diverso metodo di stima della batteria usato da Windows/Mac OS e Linux. La durata reale potrebbe essere la stessa, ma i metodi possono fornire stime diverse.</p>
	
</page>
