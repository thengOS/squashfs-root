<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task a11y" version="1.0 if/1.0" id="a11y-right-click" xml:lang="it">
  <info>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <desc>Premere e mantenere premuto il pulsante sinistro del mouse per simulare il clic col pulsante destro.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>
    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision version="13.10" date="2013-10-25" status="review"/>
  </info>

  <title>Simulare il clic col pulsante destro</title>

  <p>È possibile simulare il clic col pulsante destro mantenendo premuto il pulsante sinistro del mouse. Questa operazione risulta utile nel caso in cui si trovino difficoltà a muovere le dita di una mano individualmente o se il mouse abbia solo un singolo pulsante.</p>

  <steps>
    <item>
      <if:choose>
        <if:when test="platform:unity">
          <p>Fare clic sull'icona a destra nella <gui>barra dei menù</gui> e selezionare <gui>Impostazioni di sistema</gui>.</p>
        </if:when>
        <p>Fare clic sul proprio nome nella barra dei menù e selezionare <gui>Impostazioni di Sistema</gui>.</p>
      </if:choose>
    </item>
    <item>
      <p>Aprire <gui>Accesso universale</gui> e selezionare la scheda <gui>Puntamento</gui>.</p>
    </item>
    <item>
      <p>Abilitare <gui>Clic secondario simulato</gui>.</p>
    </item>
  </steps>

  <p>È possibile modificare la durata della pressione del pulsante sinistro del mouse prima che venga registrato come un clic destro. Nella scheda <gui>Puntamento</gui>, cambiare il valore del <gui>Ritardo di accettazione</gui> nella sezione <gui>Clic secondario simulato</gui>.</p>

  <p>Per fare clic-destro con il clic secondario simulato, tenere premuto il pulsante sinistro del mouse laddove normalmente si farebbe clic-destro, quindi rilasciare. Il puntatore si colora di blu mentre si tiene premuto il pulsante sinistro del mouse. Una volta che è interamente blu, rilasciare il pulsante del mouse per fare clic-destro.</p>

  <p>Alcuni puntatori speciali, come i puntatori di ridimensionamento, non cambiano colore. È comunque possibile usare normalmente il clic secondario simulato, anche se non si ha alcun riscontro visivo dal puntatore.</p>

  <p>Se si usano i <link xref="mouse-mousekeys">mouse da tastiera</link>, è anche possibile fare clic-destro tenendo premuto il tasto <key>5</key> sul tastierino numerico.</p>

  <if:choose>
    <if:when test="platform:unity">
    </if:when>
    <note>
      <p>Nella panoramica <gui>Attività</gui> è sempre possibile esercitare una pressione prolungata per fare clic-destro, anche quando questa funzione è disattivata. La pressione prolungata funziona in modo leggeremente diverso nella panoramica: non è necessario rilasciare il pulsante per fare clic-destro.</p>
    </note>
  </if:choose>

</page>
