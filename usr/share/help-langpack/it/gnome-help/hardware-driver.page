<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="hardware-driver" xml:lang="it">

  <info>
    <link type="guide" xref="hardware" group="more"/>

    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Un driver hardware permette al computer di usare i dispositivi ad esso collegati.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Cos'e un driver?</title>

<p>I dispositivi sono la "parte" fisica del computer. Possono essere <em>esterni</em>, per esempio stampanti e monitor, oppure <em>interni</em>, per esempio schede grafiche e audio.</p>

<p>Il computer, per essere in grado di usare questi dispositivi, necessita di sapere come poter comunicare con essi. Queste informazioni vengono fornite da software chiamati <em>driver del dispositivo</em>.</p>

<p>When you attach a device to your computer, you must have the correct driver
installed for that device to work. For example, if you plug in a printer but
the correct driver is not available, you will not be able to use the printer.
Normally, each model of device uses a driver that is not compatible with any
other model.</p>

<p>Su Linux, sono già presenti i driver per la maggior parte dei dispositivi; per cui, una volta collegato, tutto dovrebbe funzionare correttamente. Potrebbe comunque essere necessario installare manualmente il driver oppure potrebbe non essere disponibile.</p>

<p>In addition, some existing drivers are incomplete or partially
non-functional. For example, you might find that your printer cannot do
double-sided printing, but is otherwise completely functional.</p>

</page>
