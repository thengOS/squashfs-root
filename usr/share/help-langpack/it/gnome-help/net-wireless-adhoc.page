<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-adhoc" xml:lang="it">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>

    <desc>Usare una rete ad-hoc per consentire ad altri dispositivi di connettersi al proprio computer e alle sue connessioni di rete.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Creare un hotspot senza fili</title>

  <p>È possibile usare il proprio computer come hotspot senza fili. Questo consente ad altri dispositivi di connettersi al proprio senza che ci sia bisogno di una rete a sé stante e di condividere una connessione Internet creata con un'altra interfaccia, come una rete via cavo o tramite telefono cellulare.</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#yourname">system menu</gui> from the right
    side of the top bar.</p>
  </item>
  <item>
    <p>Select
    <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-excellent-symbolic.svg" width="16" height="16"/>
      Wi-Fi Not Connected</gui> or the name of the wireless network to which
    you are already connected. The Wi-Fi section of the menu will expand.</p>
  </item>
  <item>
    <p>Click <gui>Wi-Fi Settings</gui>.</p></item>
  <item><p>Click the <gui>Use as Hotspot...</gui> button.</p></item>
  <item><p>If you are already connected to a wireless network, you will be
  asked if you want to disconnect from that network. A single wireless adapter
  can connect to or create only one network at a time. Click <gui>Turn On</gui>
  to confirm.</p></item>
</steps>

<p>Vengono generati automaticamente un nome di rete (SSID) e una chiave di protezione. Il nome della rete è stabilito in base al nome del computer, altri dispositivi avranno bisogno di questa informazione per connettersi all'hotspot appena creato.</p>

</page>
