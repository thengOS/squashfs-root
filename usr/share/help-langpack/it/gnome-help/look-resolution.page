<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-resolution" xml:lang="it">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email its:translate="no">nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email its:translate="no">tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Modificare la risoluzione dello schermo e l'orientamento (rotazione).</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Change the resolution or rotation of the screen</title>

  <p>Per definire quanto grandi (o quanto dettagliati) gli oggetti appaiono a schermo, modificare la <em>risoluzione dello schermo</em>. Per definire il modo in cui appaiono (per esempio, con un monitor ruotabile), modificare la <em>rotazione</em>.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Displays</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Displays</gui> to open the panel.</p>
    </item>
    <item>
      <p>If you have multiple displays and they are not mirrored, you can have
      different settings on each display. Select a display in the preview
      area.</p>
    </item>
    <item>
      <p>Select the resolution and rotation.</p>
    </item>
    <item>
      <p>Click <gui>Apply</gui>. The new settings will be applied for 20
      seconds before reverting back. That way, if you cannot see anything with
      the new settings, your old settings will be automatically restored. If
      you are happy with the new settings, click <gui>Keep Changes</gui>.</p>
    </item>
  </steps>

<section id="resolution">
  <title>Risoluzione</title>

  <p>The resolution is the number of pixels (dots on the screen) in each
  direction that can be displayed. Each resolution has an <em>aspect
  ratio</em>, the ratio of the width to the height. Wide-screen displays use a
  16:9 aspect ratio, while traditional displays use 4:3. If you choose a
  resolution that does not match the aspect ratio of your display, the screen
  will be letterboxed to avoid distortion, by adding black bars to the top and
  bottom or both sides of the screen.</p>

  <p>È possibile modificare la risoluzione dall'elenco a discesa <gui>Risoluzione</gui>. Se questa non è adatta per il proprio schermo, gli oggetti potrebbero <link xref="look-display-fuzzy">sembrare strani oppure pixelati</link>.</p>

</section>

<section id="rotation">
  <title>Rotazione</title>

  <p>On some laptops, you can physically rotate the screen in many directions.
  It is useful to be able to change the display rotation. You can rotate what
  you see on your screen by pressing the buttons with the arrows.</p>

</section>

</page>
