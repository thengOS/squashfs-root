<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-slowkeys" xml:lang="it">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aggiungere un ritardo tra la pressione del tasto e la sua visualizzazione a schermo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Attivare i tasti lenti</title>

  <p>Attivare i <em>Tasti lenti</em> per avere un ritardo tra la pressione del tasto e la sua visualizzazione sullo schermo. In questo modo, per digitare una lettera o un numero è necessario tenere premuto un po' il tasto corrispondente prima di poterlo visualizzare sullo schermo. I tasti lenti possono essere utilizzati in casi in cui si prema involontariamente lo stesso più volte quando si usa la tastiera oppure nei casi in cui sia difficile premere il tasto giusto.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Universal Access</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Universal Access</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui>Typing Assist (AccessX)</gui> in the <gui>Typing</gui>
      section.</p>
    </item>
    <item>
      <p>Switch <gui>Slow Keys</gui> to <gui>ON</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Attivare e disattivare rapidamente i tasti lenti</title>
    <p>Under <gui>Enable by Keyboard</gui>, select
    <gui>Turn on accessibility features from the keyboard</gui> to
    turn slow keys on and off from the keyboard. When this option is selected,
    you can press and hold <key>Shift</key> for eight seconds to enable or
    disable slow keys.</p>
    <p>È possibile attivare e disattivare i tasti lenti facendo clic sulla <link xref="a11y-icon">icona accesso universale</link> nella barra superiore e selezionando <gui>Tasti lenti</gui>. L'icona di accesso universale è visibile quando una o più impostazione sono state attivate dal pannello <gui>Accesso universale</gui>.</p>
  </note>

  <p>Utilizzare il controllo scorrevole <gui>Ritardo di accettazione</gui> per controllare la durata della pressione del tasto affinché venga registrata.</p>

  <p>È anche possibile fare in modo che il computer riproduca un avviso sonoro quando un tasto viene premuto, accettato o rifiutato perché non premuto per il tempo necessario.</p>

</page>
