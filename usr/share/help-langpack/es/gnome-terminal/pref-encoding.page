<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="pref-encoding" xml:lang="es">
  <info>
    <link type="guide" xref="index#preferences"/>
    <link type="guide" xref="pref"/>
    <revision version="0.1" date="2013-03-02" status="candidate"/>

    <credit type="author copyright">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cambiar a otra codificación de caracteres soportada.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2012 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2007-2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Javier F. Serrador</mal:name>
      <mal:email>serrador@gnome.org</mal:email>
      <mal:years>2001-2006</mal:years>
    </mal:credit>
  </info>

  <title>Codificación de caracteres</title>

  <p>Generalmente, la <em>codificación de caracteres</em> predeterminada es UTF-8. Puede querer cambiar la codificación de caracteres de la <app>Terminal</app> si:</p>

  <list>
    <item>
      <p>está trabajando con nombres de archivos o carpetas que usan caracteres no disponibles en su codificación predeterminada.</p>
    </item>
    <item>
      <p>usa un disco duro externo que usa una codificación de caracteres diferente a la de su sistema.</p>
    </item>
    <item>
      <p>se conecta a un equipo remoto que usa una codificación de caracteres diferente.</p>
    </item>
  </list>

  <section id="change-default-encoding">
    <title>Cambiar la codificación de caracteres predeterminada</title>

    <steps>
      <item>
        <p>Seleccione <guiseq><gui style="menu">Terminal</gui> <gui style="menuitem">Establecer codificación de caracteres</gui></guiseq>.</p>
      </item>
      <item>
        <p>Seleccione la codificación de caracteres que quiere.</p>
      </item>
    </steps>

  </section>

  <section id="add-encoding-menu-choices">
    <title>añadir más opciones al menú de codificación de caracteres</title>

    <steps>
      <item>
        <p>Seleccione <guiseq><gui style="menu">Terminal</gui> <gui style="menuitem">Establecer codificación de caracteres</gui> <gui style="menuitem">Añadir o quitar…</gui></guiseq>.</p>
      </item>
      <item>
        <p>Explorar las codificaciones de caracteres disponibles.</p>
      </item>
      <item>
        <p>Seleccione las codificaciones que quiere añadir al menú.</p>
      </item>
      <item>
	<p>Pulse <gui style="button">Cerrar</gui> para salir del diálogo y volver a la <app>Terminal</app>.</p>
      </item>
    </steps>

    <note>
      <p>La mayoría de los sistemas operativos modernos soportan la codificación de caracteres UTF-8 de manera predeterminada.</p>
    </note>

  </section>

</page>
