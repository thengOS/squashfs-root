<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="clock-set" xml:lang="es">

  <info>
    <link type="guide" xref="clock" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use la <gui>Configuración de fecha y hora</gui> para cambiar la fecha y/o la hora.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Cambiar la fecha y la hora</title>

  <p>Si la fecha y hora mostradas en la barra superior son incorrectas, o tienen el formato incorrecto, puede cambiarlas:</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Fecha y hora</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Fecha y hora</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Si tiene <gui>Activada</gui> la <gui>Zona horaria automática</gui>, su zona horaria se debería actualizar automáticamente si tiene una conexión a Internet. Para actualizar su zona horaria manualmente, <gui>Desactívela</gui>.</p>
    </item> 
    <item>
      <p>Pulse en <gui>Fecha y hora</gui> y ajuste la fecha y la hora.</p>
    </item>
    <item>
      <p>También puede cambiar el formato de hora, seleccionando el formato <gui>24 horas</gui> o <gui>AM/PM</gui> en <gui>Formato de hora</gui>.</p>
    </item>
  </steps>

  <p>También puede querer <link xref="clock-timezone">configurar la zona horaria manualmente</link>.</p>

</page>
