<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="screen-shot-record" xml:lang="es">

  <info>
    <link type="guide" xref="tips"/>

    <revision pkgversion="3.6.1" date="2012-11-10" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.14.0" date="2015-01-14" status="review"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Obtener una imagen o grabar un vídeo de lo que ocurre en su pantalla.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Capturas de pantalla y grabaciones de vídeo</title>

  <p>Puede tomar una imagen de su pantalla (una <em>captura de pantalla</em>) o grabar un vídeo de lo que ocurre en la pantalla (una <em>grabación de la pantalla</em>). Esto es útil si quiere mostrar a alguien como hacer algo en el equipo, por ejemplo. Las capturas de pantalla/grabaciones de vídeo son solamente archivos de imagen/vídeo, de manera que los puede enviar por correo y compartirlos en la web.</p>

<section id="screenshot">
  <title>Capturar la pantalla</title>

  <steps>
    <item>
      <p>Open <app>Screenshot</app> from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
    <item>
      <p>En la ventana <app>Capturar pantalla</app>, seleccione si se debe capturar el escritorio completo, la ventana actual o un área de la pantalla. Establezca un retardo si necesita seleccionar una ventana o si necesita configurar su escritorio para la captura. Luego, elija los efectos que quiera</p>
    </item>
    <item>
       <p>Pulse <gui>Capturar pantalla</gui>.</p>
       <p>Si selecciona <gui>Seleccionar área que capturar</gui>, el cursor se convertirá en una cruz. Pulse y arrastre el área que quiere para la captura de pantalla.</p>
    </item>
    <item>
      <p>En la ventana <gui>Guardar la captura de pantalla</gui>, introduzca el nombre del archivo, elija una carpeta y pulse <gui>Guardar</gui>.</p>
      <p>Alternativamente, importe la captura de pantalla directamente en su aplicación de edición de imágenes sin guardar previamente. Pulse <gui>Copiar al portapapeles</gui>, pegue la imagen en la otra aplicación o la miniatura de la captura de la pantalla a aplicación.</p>
    </item>
  </steps>

  <section id="keyboard-shortcuts">
    <title>Atajos de teclado</title>

    <p>Tome una captura rápida del escritorio, una ventana o un área en cualquier momento usando estos atajos de teclado globales:</p>

    <list style="compact">
      <item>
        <p><key>Imp Pant</key> para hacer una captura de pantalla del escritorio.</p>
      </item>
      <item>
        <p><keyseq><key>Alt</key><key>Impt Pant</key></keyseq> para hacer una captura de pantalla de la ventana.</p>
      </item>
      <item>
        <p><keyseq><key>Mayús</key><key>Imp Pant</key></keyseq> to take a screenshot of an area you select.</p>
      </item>
    </list>

    <p>Cuando usa un atajo de teclado, la imagen se guarda automáticamente en su carpeta <file>Imágenes</file> con un nombre de archivo que empieza por <file>Captura de pantalla</file> y que incluye la fecha y la hora en que se creó.</p>
    <note style="note">
      <p>Si no tiene una carpeta <file>Imágenes</file>, las imágenes se guardarán en su carpeta personal.</p>
    </note>
    <p>También puede mantener pulsada la tecla <key>Ctrl</key> con cualquiera de los atajos anteriores para copiar la imagen de la captura de pantalla al portapapeles en lugar de guardarla.</p>
  </section>

</section>

<section id="screencast">
  <title>Grabación de vídeo</title>

  <p>Puede grabar un vídeo de lo que ocurre en su pantalla:</p>

  <steps>
    <item>
      <p>Pulse <keyseq><key>Ctrl</key><key>Alt</key><key>Mayús</key><key>R</key></keyseq> para comenzar a grabar lo que sucede en su pantalla.</p>
      <p>Se muestra un círculo rojo en la esquina superior derecha de la pantalla cuando la grabación está en proceso.</p>
    </item>
    <item>
      <p>Una vez que haya acabado, pulse <keyseq><key>Ctrl</key><key>Alt</key><key>Mayús</key><key>R</key></keyseq> de nuevo para detener la grabación.</p>
    </item>
    <item>
      <p>El vídeo se guarda automáticamente en su carpeta <file>Vídeos</file> con un nombre de archivo que empieza por <file>Captura de vídeo</file> e incluye la fecha y la hora en que se creó.</p>
    </item>
  </steps>

  <note style="note">
    <p>Si no tiene una carpeta <file>Vídeos</file>, los vídeos se guardarán en su carpeta personal.</p>
  </note>

</section>

</page>
