<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="about-this-guide" xml:lang="es">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Unos cuantos consejos sobre el uso de la guía de ayuda del escritorio.</desc>
    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Acerca de esta guía</title>
<p>Está guía está diseñada para describir las características de su escritorio, responder a sus preguntas relacionadas con la informática y ofrecerle consejos para usar su equipo de forma más efectiva. Hemos intentado hacer esta guía los más sencilla de usar posible:</p>

<list>
  <item><p>La guía está ordenada por temas pequeños, orientados a las tareas, no por capítulos. Esto significa que no necesita hojear a través de un manual completo para encontrar las respuestas a sus preguntas.</p></item>
  <item><p>Los temas relacionados están vinculados entre sí. Los vínculos «Consulte también» al final de algunas páginas le llevarán a temas relacionados. Esto le facilita la búsqueda de temas similares que podrían ayudarle a realizar una determinada tarea.</p></item>
  <item><p>Incluye una búsqueda interna. La barra por encima del navegador de ayuda es una <em>barra de búsqueda</em> y los resultados relevantes aparecerán tan pronto como empiece a teclear.</p></item>
  <item><p>Esta guía está mejorándose constantemente. Aunque intentamos ofrecerle un conjunto completo de información útil, sabemos que no podemos responder aquí a todas sus preguntas. No obstante, seguiremos añadiendo más información para hacerle las cosas más útiles.</p></item>
</list>

<p>Gracias por tomarse el tiempo de leer la ayuda del escritorio. Sinceramente esperamos que nunca tenga que usarla.</p>

<p>-- El equipo de documentación GNOME</p>
</page>
