<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-states" xml:lang="es">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-03-24" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Colocar ventanas en un área de trabajo para ayudarle a trabajar más eficientemente.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Mover y redimensionar ventanas</title>

  <p>Puede mover y redimensionar ventanas para ayudarle a trabajar más eficientemente. Además del comportamiento de arrastre, GNOME tiene atajos de teclado y modificadores para ayudarle a colocar ventanas rápidamente.</p>

  <list>
    <item>
      <p>Mueva una ventana arrastrándola desde la barra de título o manteniendo pulsada la tecla <key xref="keyboard-key-super">Súper</key> y arrastrando en cualquier parte de la ventana. Mantenga pulsada la tecla <key>Mayús</key> al mover para ajustar la ventana a los bordes de la pantalla y a otras ventanas.</p>
    </item>
    <item>
      <p>Redimensione una ventana arrastrándo los bordes o una esquina de la ventana. Mantenga pulsada la tecla <key>Mayús</key> al redimensionar para ajustar la ventana a los bordes de la pantalla y a otras ventanas.</p>
      <p if:test="platform:gnome-classic">También puede redimensionar una ventana maximizada pulsando el botón de maximizar en la barra de título.</p>
    </item>
    <item>
      <p>Mueva o redimensione una ventana usando sólo el teclado. Pulse <keyseq><key>Alt</key><key>F7</key></keyseq> para mover una ventana o <keyseq><key>Alt</key><key>F8</key></keyseq> para redimensionarla. Use las teclas de flechas para mover o redimensionar, después pulse <key>Intro</key> para finalizar o pulse <key>Esc</key> para volver a la posición y tamaño original.</p>
    </item>
    <item>
      <p><link xref="shell-windows-maximize">Maximice una ventana</link> arrastrándola a la parte superior de la pantalla. Arrastre una ventana a un lateral de la pantalla para maximizarla a lo largo de ese lado, lo que le permite <link xref="shell-windows-tiled">colocar las ventanas lado a lado</link>.</p>
    </item>
  </list>

</page>
