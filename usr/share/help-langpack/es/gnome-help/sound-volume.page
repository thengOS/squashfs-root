<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-volume" xml:lang="es">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ajustar el volumen del sonido para el equipo y controlar el volumen de cada aplicación.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Cambiar el volumen del sonido</title>

  <p>Para cambiar el volumen del sonido, abra el <gui xref="shell-introduction#yourname">menú del sistema</gui> en el lado derecho de la barra superior y mueva el control deslizante de volumen a la derecha o a la izquierda. Puede desactivar completamente el sonido llevando el deslizador al extremo izquierdo.</p>

  <p>Algunos teclados tienen teclas que le permiten controlar el volumen. Normalmente representan altavoces estilizados emitiendo «ondas» y frecuentemente están cerca de las teclas «F» en la parte superior. En los portátiles, normalmente están en las teclas «F». Para usarles mantenga pulsada la tecla <key>Fn</key> en us teclado.</p>

  <p>If you have external speakers, you can also change the volume
  using the speakers' volume control. Some headphones have a
  volume control too.</p>

<section id="apps">
 <title>Cambiar el volumen del sonido para aplicaciones individuales</title>

  <p>You can change the volume for one application and leave the volume for
  others unchanged. This is useful if you are listening to music and browsing
  the web, for example. You might want to turn off the audio in the web browser
  so sounds from websites do not interrupt the music.</p>

  <p>Algunas aplicaciones tienen controles de volumen en su ventana principal. Si su aplicación lo tiene, úselo para cambiar el volumen. Si no lo tiene:</p>

    <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Sonido</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Sonido</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Go to the <gui>Applications</gui> tab and change the volume of the
      application listed there.</p>

  <note style="tip">
    <p>Only applications that are playing sounds are listed. If an
    application is playing sounds but is not listed, it might not support the
    feature that lets you control its volume in this way. In such case, you
    cannot change its volume.</p>
  </note>
    </item>

  </steps>

</section>

</page>
