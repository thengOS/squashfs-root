<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="es">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email its:translate="no">ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cambiar a un idioma diferente la interfaz de usuario y el texto de la ayuda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Cambiar el idioma que usa</title>

  <p>Puede usar su escritorio y aplicaciones en decenas de idiomas, siempre y cuando tenga instalados en su equipo los paquetes de idiomas apropiados.</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Región e idioma</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Región e idioma</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Pulse <gui>Idioma</gui>.</p>
    </item>
    <item>
      <p>Seleccione su región y su idioma. Si su región e idioma no aparecen en la lista, pulse <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">...</span></media></gui> en la parte inferior de la lista para selecciona de entre todas las regiones e idiomas disponibles.</p>
    </item>
    <item>
      <p>Pulse <gui style="button">Hecho</gui> para guardar.</p>
    </item>
    <item>
      <p>Responda a la solicitud, <gui>Debe reiniciar su sesión para que los cambios surtan efecto</gui> pulsando en <gui style="button">Reiniciar ahora</gui>, o pulse <gui style="button">X</gui> para reiniciar más tarde.</p>
    </item>
  </steps>

  <p>Algunas traducciones pueden estar incompletas y ciertas aplicaciones pueden no soportar su idioma por completo. Cualquier texto no traducido aparecerá en el idioma en el que se desarrolló el software de manera original, generalmente inglés americano.</p>

  <p>Hay algunas carpetas especiales en la carpeta de inicio donde las aplicaciones pueden almacenar cosas como música, imágenes y documentos. Estas carpetas usan nombres estándar de acuerdo a su idioma. Cuando vuelva a entrar, se le preguntará si quiere cambiar el nombre de estas carpetas con los nombres estándar para el idioma seleccionado. Si va a utilizar el nuevo idioma todo el tiempo, debe actualizar los nombres de las carpetas.</p>

</page>
