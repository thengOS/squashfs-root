<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="help-irc" xml:lang="es">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Obtener soporte en vivo en el IRC.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <credit type="author">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>IRC</title>
   <p>IRC significa «Internet Relay Chat». Es un sistema de mensajería multi-usuario en tiempo real. Puede obtener ayuda y consejos en el servidor IRC de GNOME o de otros usuarios y desarrolladores de GNOME.</p>
   <p>Para conectarse al servidor IRC de GNOME use <app>empathy</app> o <app>xchat</app>, o use una interfaz web como puede ser <link href="http://chat.mibbit.com/">mibbit</link>.</p>
   <p>Para crear una cuenta IRC en Empathy, consulte la <link href="help:empathy#irc-manage">documentación de Empathy</link>.</p>
   <p>El servidor IRC de GNOME es <sys>irc.gnome.org</sys>. También puede verlo mencionado como «GIMP network». Si su equipo está correctamente configurado, puede pulsar en el enlace <link href="irc://irc.gnome.org/gnome"/>para acceder al canal de <sys>gnome</sys>.</p>
   <p>Aunque IRC es un sistema de conversaciones en tiempo real, la gente no suele responder inmediatamente; sea paciente.</p>

  <note>
    <p>Tenga en cuenta el <link href="https://wiki.gnome.org/Foundation/CodeOfConduct">código de conducta de GNOME</link> cuando chatee en IRC.</p>
  </note>

</page>
