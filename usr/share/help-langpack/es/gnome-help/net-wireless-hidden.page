<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-hidden" xml:lang="es">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <desc>Conectarse a una red inalámbrica que no aparece en la lista de redes.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Conectar a una red inalámbrica oculta</title>

<p>Es posible configurar una red inalámbrica para que esté «oculta». Las redes ocultas no se muestran en la lista de redes que se muestran al pulsar en la configuración de la <gui>Red</gui> (o la lista de redes inalámbricas en cualquier otro equipo). Para conectarse a una red inalámbrica oculta:</p>

<steps>
  <item>
    <p>Abra el <gui xref="shell-introduction#yourname">menú del sistema</gui> en la parte derecha de la barra superior.</p>
  </item>
  <item>
    <p>Select
    <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-excellent-symbolic.svg" width="16" height="16"/>
      Wi-Fi Not Connected</gui>. The Wi-Fi section of the menu will expand.</p>
  </item>
  <item>
    <p>Pulse en <gui>Configuración inalámbrica</gui>.</p>
  </item>
  <item><p>Pulse el botón <gui>Conectar a una red oculta...</gui>.</p></item>
 <item>
  <p>En la ventana que aparece, seleccione una red oculta a la que se hubiese conectado anteriormente usando la lista desplegable <gui>Conexión</gui>, o <gui>Nueva</gui> para crear una nueva.</p>
 </item>
 <item>
  <p>Para una conexión nueva, escriba el nombre de la red, elija el tipo de seguridad inalámbrica en la lista desplegable <gui>Seguridad inalámbrica</gui>.</p>
 </item>
 <item>
  <p>Introduzca la contraseña u otros detalles de seguridad.</p>
 </item>
 <item>
  <p>Pulse <gui>Conectar</gui>.</p>
 </item>
</steps>

  <p>Puede tener que comprobar la configuración del punto de acceso inalámbrico/enrutador para ver el nombre de la red. Si no tiene el nombre de la red (SSID) puede usar el <em>BSSID</em> (Identificador básico de conjunto de servicio «Basic Service Set Identifier», la dirección MAC del punto de acceso), y buscar algo parecido a esto: <gui>02:00:01:02:03:04</gui>. Suele estar debajo del punto de acceso.</p>

  <p>También deberá comprobar la configuración de seguridad del punto de acceso inalámbrico; busque términos del tipo WEP o WPA.</p>

<note>
 <p>Puede pensar que esconder su red inalámbrica mejorará la seguridad mediante la prevención de las personas que no conocen la conexión. En la práctica, este no es el caso, la red es un poco más difícil de encontrar, pero todavía es perceptible.</p>
</note>

</page>
