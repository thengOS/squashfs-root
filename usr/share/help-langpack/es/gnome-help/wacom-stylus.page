<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-stylus" xml:lang="es">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Definir las funciones de los botones y el tacto de la presión de la Wacom stylus.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Configurar la tableta</title>

<steps>
  <item>
    <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Tableta Wacom</gui>.</p>
  </item>
  <item>
    <p>Pulse en <gui>Tableta Wacom</gui> para abrir el panel.</p>
    <!-- TODO: document how to connet the tablet using Bluetooth/add link -->
    <note style="tip"><p>Si no se detecta ninguna tableta, se le pedirá que <gui>Conecte o encienda su tableta Wacom</gui>. Pulse el enlace <gui>Configuración de Bluetooth</gui> para conectar una tableta inalámbrica</p></note>
  </item>
  <item><p>La parte inferior del panel contiene detalles y configuraciones específicas para su tableta, con el nombre del dispositivo (la clase de «stylus») y un diagrama a la izquierda. Esta configuración se puede ajustar:</p>
    <list>
      <item><p><gui>Sensibilidad de presión del borrador:</gui> use el deslizador para ajustar la sensibilidad (cómo se traduce la presión a valores digitales) entre <gui>Suave</gui> y <gui>Firme</gui>.</p></item>
      <item><p><gui>Button/Scroll Wheel</gui> configuration (these change to
       reflect the stylus). Click the menu next to each label to select one of
       these functions: No Action, Left Mouse Button Click, Middle Mouse Button
       Click, Right Mouse Button Click, Scroll Up, Scroll Down, Scroll Left,
       Scroll Right, Back, or Forward.</p></item>
      <item><p><gui>Sensibilidad de presión del lápiz:</gui> use el deslizador para ajustar la sensibilidad entre <gui>Suave</gui> y <gui>Firme</gui>.</p></item>
    </list>
  </item>
</steps>

<note style="info"><p>Si tiene más de una «stylus», cuando la «stylus» adicional esté cerca de la tableta, aparecerá un paginador junto al nombre del dispositivo. Use el paginador para elegir qué «stylus» configurar.</p>
</note>

</page>
