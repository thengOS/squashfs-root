<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="rules" xml:lang="es">

  <info>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="flags"/>
    <revision version="0.1" date="2012-08-12" status="review"/>

    <credit type="author copyright">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
      <years>2011</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2012</years>
    </credit>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2007-2008</mal:years>
    </mal:credit>
  </info>


  <title>Reglas del juego</title>

  <p>El objetivo de <app>Minas</app> es descubrir todas las minas ocultas bajo las casillas sin hacer que exploten.</p>

  <steps>
    <item>
      <p>Empiece eligiendo uno de estos tres tamaños de tablero, o seleccionando un tablero personalizado.</p>
    </item>
    <item>
      <p>El juego empieza con el tablero lleno de casillas. Pulse sobre una de ellas para descubrirla. Revelará:</p>
      <list>
        <item>
	  <p>Un número de color, que representa el número de minas en las casillas adyacentes. Esto le ayudará a deducir dónde están las minas, por lo que puede marcarlas con <link xref="flags">banderas</link>.</p>
        </item>
        <item>
	  <p>Una casilla vacía, lo que significa que no hay minas en las casillas adyacentes.</p>
        </item>
        <item>
	  <p>Una mina, que explotará y hará que termine la partida.</p>
	</item>
      </list>
    </item>
    <item>
      <p>Repita los pasos anteriores hasta que descubra todas las casillas que no contienen una mina.</p>
    </item>
  </steps>

  <p>Si ha sido lo bastante rápido para completar el juego, se le añadirá a la lista de <link xref="high-scores">puntuaciones más altas</link>.</p>

</page>
