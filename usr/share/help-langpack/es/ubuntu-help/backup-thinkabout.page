<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-thinkabout" xml:lang="es">

  <info>
    <link type="guide" xref="files#backup"/>
    <desc>Una lista de carpetas donde puede encontrar documentos, archivos y configuraciones que podría querer respaldar.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>¿Dónde puedo encontrar los archivos de los que quiero hacer copia de seguridad?</title>

  <p>Abajo se listan las localizaciones más comunes de sus archivos importantes y configuraciones que quizá quiera respaldar.</p>

<list>
 <item>
  <p>Archivos personales (documentos, música, fotos y vídeos)</p>
  <p xmlns:its="http://www.w3.org/2005/11/its" its:locNote="translators: xdg    dirs are localised by package xdg-user-dirs and need to be translated.  You    can find the correct translations for your language here:    http://translationproject.org/domain/xdg-user-dirs.html">Estos están situados normalmente en su directorio personal (<file>/home/nombre_de_usuario</file>). Puede ser que estén almacenados en subdirectorios tales como: Escritorio, Documentos, Imágenes, Música y Vídeos.</p>
  <p>Si su ubicación de respaldo tiene el espacio suficiente (por ejemplo si se trata de un disco duro externo) considere respaldar su carpeta personal entera. Puede saber cuánto espacio ocupa su carpeta personal con el <app>Analizador de uso de disco</app>.</p>
 </item>

 <item>
  <p>Archivos ocultos</p>
  <p>Cualquier archivo o carpeta cuyo nombre empieza con un punto (.) está oculto de manera predeterminada. Para ver archivos ocultos, pulse <guiseq><gui>Ver</gui><gui>Mostrar archivos ocultos</gui></guiseq> o pulse <keyseq><key>Ctrl</key><key>H</key></keyseq>. Estos archivos se pueden incluir en una copia de seguridad como cualquier otro archivo.</p>
 </item>

 <item>
  <p>Configuraciones personales (preferencias del escritorio, temas y opciones del software)</p>
  <p>La mayoría de aplicaciones almacenan su configuración en subcarpetas ocultas en su carpeta personal (véase más arriba para información sobre archivos ocultos).</p>
  <p>La mayoría de la configuración de sus aplicaciones se almacena en las carpetas ocultas <file>.config</file>, <file>.gconf</file>, <file>.gnome2</file> y <file>.local</file> en su carpeta personal.</p>
 </item>

 <item>
  <p>Configuraciones que afectan a todo el sistema</p>
  <p>La configuración de importantes partes del sistema no se almacenan en la carpeta de usuario. Hay un número de lugares donde podrían guardarse, pero la mayoría se encuentra en el directorio <file>/etc</file>. Por lo general no necesitará hacer copia de seguridad de estos datos. Sin embargo si esta ejecutando un servidor, debería hacer por lo menos una copia de seguridad de la configuración de estos servicios.</p>
 </item>
</list>

</page>
