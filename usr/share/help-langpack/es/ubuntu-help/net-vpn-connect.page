<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-vpn-connect" xml:lang="es">
  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-wired"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="16.04" date="2016-03-14" status="review"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Los VPN le permiten conectarse a una red local a través de Internet. Aprenda cómo configurar una conexión VPN.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Conectarse a un VPN</title>

<p>Una VPN (o <em>Virtual Private Network</em>, «Red Privada Virtual») es una forma de conectarse a una red local a través de Internet. Por ejemplo, suponga que quiere conectarse a la red local de su trabajo mientras está en viaje de negocios. Tendrá que buscar una conexión a Internet en alguna parte (como en un hotel) y luego conectarse a la VPN de su trabajo. Sería como si se estuviera conectando directamente a la red de su trabajo, pero la conexión de red real sería a través de la conexión a Internet del hotel. Las conexiones VPN normalmente van <em>cifradas</em> para evitar que la gente pueda acceder a la red local a la que está conectándose sin autenticarse.</p>

<p>There are a number of different types of VPN. You may have to install some extra software depending on what type of VPN
you're connecting to. Find out the connection details from whoever is in charge of the VPN and see which <em>VPN client</em>
you need to use. Then, open <app>Ubuntu Software</app> and search for the <app>network-manager</app> package which works
with your VPN (if there is one) and install it.</p>

<note>
 <p>Si no hay ningún paquete de NetworkManager para su tipo de VPN, probablemente tendrá que descargar e instalar algún programa cliente de la empresa que proporcione el software VPN. Probablemente tendrá que seguir algunas instrucciones distintas para hacer que funcione.</p>
</note>

<p>Una vez que esté hecho, puede configurar la conexión VPN:</p>

<steps>
 <item>
  <p>Pulse el <gui>menú de red</gui> en la barra de menús y, bajo <gui>Conexiones VPN</gui>, seleccione <gui>Configurar VPN</gui>.</p>
 </item>

 <item>
  <p>Pulse <gui>Añadir</gui> y seleccione qué tipo de conexión VPN tiene.</p>
 </item>

 <item>
  <p>Pulse <gui>Crear</gui> y siga las instrucciones en la pantalla, introduciendo los detalles como su nombre de usuario y contraseñas.</p>
 </item>

 <item>
  <p>Cuándo ha terminado de configurar VPN, pulse <gui>menú de red</gui> en la barra de menú, vaya a <gui>Conexiones VPN</gui> y pulse en la conexión recién creada. Intentará establecer una conexión VPN - el icono de red cambiará e intentará conectar.</p>
 </item>

 <item>
  <p>Si todo va bien, se conectará correctamente a la VPN. Si no, puede que necesite volver a comprobar las opciones VPN que introdujo. Puede hacer esto pulsando en el menú de red, seleccionando <gui>Editar las conexiones</gui> y yendo a la pestaña <gui>VPN</gui>.</p>
 </item>

 <item>
  <p>Para desconectarse de la VPN, abra el menú de red y seleccione <gui>Desconectar</gui> bajo el nombre de su conexión VPN.</p>
 </item>
</steps>

</page>
