<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-dimscreen" xml:lang="es">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="display-lock"/>
    <revision pkgversion="3.7.1" version="0.3" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-07" status="review"/>
    <desc>Atenuar la pantalla para ahorrar energía o incrementar el brillo para hacerla más legible en condiciones de mucha luz ambiental.</desc>
    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Establecer el brillo de la pantalla</title>

  <p>Puede cambiar el brillo de la pantalla para ahorrar energía o para hacer la pantalla más fácil de leer con luz brillante. También puede hacer que la pantalla se apague automáticamente cuando use la energía de la batería y que la apague automáticamente cuando no esté en uso.</p>

  <steps>
    <title>Establecer el brillo</title>
  <item><p>Pulse en el icono situado en el extremo derecho de la <gui>barra de menús</gui> y seleccione <gui>Configuración del sistema</gui>.</p></item>
    <item><p>Seleccione <gui>Brillo y bloqueo</gui>.</p></item>
    <item><p>Mueva el deslizador de <gui>Brillo</gui> para obtener un valor confortable.</p></item>
  </steps>

  <note style="tip">
    <p>Muchos teclados de portátiles incluyen teclas especiales para ajustar el brillo. Esas teclas tienen un símbolo que representa a un sol y se ubican por lo general en las teclas de función. Mantenga presionada la tecla <key>Fn</key> para usar estas teclas especiales.</p>
  </note>

  <p>Seleccione <gui>Reducir el brillo de fondo</gui> para hacer que el brillo se reduzca automáticamente cuando esté usando la batería. La luz de fondo de su pantalla puede consumir mucha energía y reducir significativamente el tiempo que durará la batería hasta que tenga que volver a cargarla.</p>

  <p>La pantalla se apagará automáticamente después de que no la utilice durante unos minutos. Este apagado únicamente afecta a la pantalla y no implica que se apague el equipo. Puede ajustar el tiempo que debe permanecer inactivo el equipo utilizando la opción <gui>Apagar pantalla si está inactiva durante</gui>.</p>
</page>
