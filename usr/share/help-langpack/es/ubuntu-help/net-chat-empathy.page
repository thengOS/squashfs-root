<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-chat-empathy" xml:lang="es">
  <info>
    <link type="guide" xref="net-chat"/>
    <link type="seealso" xref="net-chat-video"/>
    
    <revision version="16.04" date="2016-05-07" status="review"/>

    <credit type="author">
      <name>Equipo de documentación de Ubuntu</name>
      <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>

    <desc>Con <app>Empathy</app> puede conversar, hacer audio y videollamadas con amigos y compañeros de varias redes</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Mensajería instantánea con Ubuntu</title>

  <p>Con la aplicación <app>Empathy</app> puede conversar con personas en línea y con amigos y compañeros que usen Google Talk, AIM, Windows Live y muchos otros programas de chat. Con un micrófono o una webcam también puede hacer audio o videollamadas.</p>

  <p>
    Empathy is not installed by default in Ubuntu, but you can <link href="apt:empathy">install</link> it from the Ubuntu package archive. Start
    <app>Empathy Instant Messaging</app> from the <link xref="unity-dash-intro">Dash</link>, the <link xref="unity-launcher-intro">Launcher</link> or choose <gui>Empathy</gui>
    from the <link xref="unity-menubar-intro">Messaging menu</link>.
  </p>

  <note style="tip">
    <p>Puede cambiar el estado de la mensajería instantánea (disponible, ausente, ocupado, etc.) desde el <link xref="unity-menubar-intro">Menú de mensajería</link>.</p>
  </note>

  <p>Para ayuda con el uso de Empathy, lea el <link href="help:empathy">manual de Empathy</link>.</p>

</page>
