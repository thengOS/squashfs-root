<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="clock-more-info" xml:lang="es">

  <info>
    <link type="guide" xref="clock"/>
    <desc>Seleccione mostrar información adicional como la fecha o el día de la semana.</desc>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <credit type="author">
      <name>Proyecto de documentación de Ubuntu</name>
      <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Cambiar cuánta información se muestra en el reloj</title>

<p>De manera predeterminada, Ubuntu solo muestra la hora en el reloj. Puede hacer que el reloj muestre información adicional si así lo desea.</p>

<p>Haga clic sobre el reloj y seleccione <gui>Configuración de fecha y hora</gui>. Cambie a la pestaña <gui>Reloj</gui>. Seleccione las opciones de fecha y hora que desee que se muestren.</p>

<note><p>También puede desactivar el reloj completamente, desmarcando la opción <gui>Mostrar un reloj en la barra de menús</gui>.</p>
<p>Si después cambia de opinión, puede volver a activar el reloj pulsando en el icono situado en el extremo derecho de la barra de menús y seleccionando <gui>Configuración del sistema</gui>. En la sección Sistema, pulse en <gui>Fecha y hora</gui>.</p></note>

<section id="change-date-format">
  <title>Cambiar el formato de fecha</title>
<p>También puede cambiar el formato del reloj para que coincida con el estándar preferido de su ubicación.</p>

<steps>
  <item><p>Pulse en el icono situado en el extremo derecho de la barra de menús y seleccione <gui>Configuración del sistema</gui>.</p></item>
  <item><p>En la sección Personal pulse <gui>Soporte de idiomas</gui>.</p></item>
  <item><p>Cambie a la pestaña <gui>Formatos regionales</gui>.</p></item>
  <item><p>Seleccione su ubicación preferida en la lista desplegable.</p></item>
  <item><p>Necesitará <link xref="shell-exit">cerrar sesión</link> y volver a abrirla para que este cambio tenga efecto.</p></item>
</steps>

</section>

</page>
