<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="net-what-is-ip-address" xml:lang="es">
  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <desc>Una dirección IP es como un número de teléfono para su equipo.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>¿Qué es una dirección IP?</title>

  <p>«Dirección IP» significa <em>dirección del Protocolo de Internet</em>, y cada dispositivo que está conectado a una red (como Internet) tiene una.</p>

  <p>Una dirección IP se parece a su número de teléfono. Su número de teléfono es un conjunto único de números que identifican a su teléfono de forma otra persona pueda llamarle. Igualmente, una dirección IP es un conjunto único de números que identifican a su equipo de forma que pueda enviar y recibir datos hacia y desde otros equipos, respectivamente.</p>

  <p>Actualmente, la mayoría de las direcciones IP constan de cuatro conjuntos de números separados por un punto. <code>192.168.1.42</code> es un ejemplo de dirección IP.</p>

  <note style="tip"><p>Una dirección IP puede ser <em>dinámica</em> o <em>estática</em>. Las direcciones IP dinámicas son asignadas temporalmente cada vez que su equipo se conecta a la red. Las direcciones IP son fijas, y no cambian. Las direcciones IP dinámicas son más frecuentes que las estáticas, las estáticas solo se usan habitualmente cuando hay un motivo especial par ello, como administrar un servidor.</p></note>
</page>
