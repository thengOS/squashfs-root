<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="addremove-install-synaptic" xml:lang="es">

  <info>
    <credit type="author">
      <name>Equipo de documentación de Ubuntu</name>
    </credit>

		<credit type="editor">
      <name>Greg Beam</name>
      <email>ki7mt@yahoo.com</email>
    </credit>

    <link type="guide" xref="addremove"/>
    <revision pkgversion="0.80.4" version="0" date="2014-01-11" status="review"/>
		<revision version="16.04" date="2016-03-13" status="review"/>
                <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Synaptic es una herramienta potente pero compleja de gestión de software que puede emplearse como alternativa a <app>Ubuntu Software</app>.</desc>
  </info>

  <title>Use Synaptic para una gestión de software más avanzada</title>

  <p>
    <app>Synaptic Package Manager</app> is more powerful and can do some 
    software management tasks which <app>Ubuntu Software</app> can't. 
    Synaptic's interface is more complicated and doesn't support newer
    <app>Ubuntu Software</app> features like ratings and reviews and therefore
    isn't recommended for use by those new to Ubuntu.
  </p>

  <p>
    Synaptic isn't installed by default, but you can <link href="apt:synaptic">install</link> it from the Ubuntu package archive.
  </p>

  <section id="install-software-with-synaptic">
    <title>Instalar software con Synaptic</title>
    <steps>
      <item>
        <p>Abra Synaptic desde el <gui>tablero</gui> o el <gui>lanzador</gui>. Necesitará escribir su contraseña en la ventana <gui>Autenticar</gui>.</p>
      </item>
      <item>
        <p>Pulse en <gui>Búsqueda</gui> para buscar una aplicación, o en <gui>Secciones</gui> y vea las categorías para encontrar una.</p>
      </item>
      <item>
        <p>Pulse con el botón derecho en la aplicación que quiere instalar y seleccione <gui>Marcar para instalar</gui>.</p>
      </item>
      <item>
        <p>Si se le pide que marque cambios adicionales, haga clic en <gui>Marcar</gui>.</p>
      </item>
      <item>
        <p>Seleccione cualquier otra aplicación que desee instalar.</p>
      </item>
      <item>
        <p>Pulse <gui>Aplicar</gui>, y luego otra vez en <gui>Aplicar</gui> en la ventana que aparecerá. Se descargarán e instalarán las aplicaciones que seleccionó.</p>
      </item>
    </steps>

    <p>Para más información acerca del uso de <app>Synaptic</app>, consulte el <link href="https://help.ubuntu.com/community/SynapticHowto">tutorial de Synaptic</link>.</p>

  </section>

</page>
