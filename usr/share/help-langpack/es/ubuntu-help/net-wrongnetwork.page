<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="net-wrongnetwork" xml:lang="es">
  <info>
    <link type="guide" xref="net-problem"/>

    <revision version="13.10" date="2013-10-22" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Edite la configuración de su conexión y eliminar la opción de conexión no deseada.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Mi equipo se conecta a una red incorrecta</title>

<p>Al encender el equipo o moverlo a una ubicación diferente, intentará automáticamente conectarse a redes inalámbricas que se ha conectado en el pasado. Si cada vez intenta conectarse a la red equivocada (es decir, no a la que quiere que se conecte), haga lo siguiente:</p>

<steps>
 <item>
  <p>Pulse en el <gui>menú de red</gui> en la barra de menús y seleccione <gui>Editar conexiones</gui>.</p>
 </item>
 
 <item>
  <p>Vaya a la pestaña <gui>Inalámbrica</gui> y busque la red a la que <em>no</em> quiere permanecer conectado.</p>
 </item>
 
 <item>
  <p>Pulse sobre esa red una vez para seleccionarla y luego pulse en <gui>Eliminar</gui>. Su equipo no volverá a intentar conectarse a esa red.</p>
 </item>
 
</steps>

<p>Sí más tarde desea conectar a la red que acaba de eliminar, simplemente selecciónela del listado de redes inalámbricas que aparece al pulsaren el menú de red en la barra de menú - igual que al conectar a otra red inalámbrica.</p>

</page>
