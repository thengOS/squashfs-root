<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-alert" xml:lang="es">
  <info>
    <link type="guide" xref="media#sound"/>

   <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <desc>Elija el sonido para reproducir los mensajes, establezca el volumen de alerta, o desactive los sonidos de alerta.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Elija o desactive la alerta de sonido</title>

  <p>Su equipo reproducirá un sonido de alerta sencillo para ciertos tipos de mensajes y eventos. Puede elegir entre varias muestras de sonido diferentes para las alertas, asignar el volumen de las alertas independientemente del volumen de su sistema, o desactivar los sonidos de alerta completamente.</p>

  <steps>
    <item><p>Pulse en el <gui>menú de sonido</gui> de la <gui>barra de menús</gui> y pulse en <gui>Preferencias de sonido</gui>.</p></item>
    <item><p>En la pestaña <gui>Efectos de sonido</gui>, seleccione un sonido de alerta. Cada sonido se reproducirá al hacer clic en él, de manera que pueda escuchar cómo suena.</p></item>
  </steps>

  <p>Utilice el control deslizante de volumen de la pestaña <gui>Efectos de sonido</gui> para definir el volumen del sonido de alerta. Esta opción no afectará al volumen de la música, las películas ni otros archivos de sonido.</p>

  <p>Para deshabilitar por completo las alertas de sonidos, seleccione <gui>Silenciar</gui> al lado de <gui>Volumen de alerta</gui>.</p>
</page>
