<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="color-calibrate-screen" xml:lang="es">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-camera"/>
    <desc>Es importante calibrar la pantalla para que esta muestre los colores con precisión.</desc>

    <revision version="13.10" date="2013-10-23" status="review"/>
    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>¿Cómo puedo calibrar mi pantalla?</title>

  <p>La calibración de la pantalla debería ser un requisito si está involucrado en algo relacionado con diseño o ilustraciones.</p>
<if:choose>
<if:when test="platform:unity">
  <p>Usando un dispositivo llamado colorímetro puede medir de forma precisa los diferentes colores que es capaz de mostrar la pantalla. Ejecutando <guiseq><gui>Configuración del sistema</gui><gui>Color</gui></guiseq> puede crear un perfil y el asistente le mostrará cómo conectar el colorimetro y qué parámetros ajustar.</p>
</if:when>
  <p>Usando un dispositivo llamado colorímetro puede medir de forma precisa los diferentes colores que es capaz de mostrar la pantalla. Ejecutando <guiseq><gui>Configuración del sistema</gui><gui>Color</gui></guiseq> puede crear un perfil y el asistente le mostrará cómo conectar el colorimetro y qué parámetros ajustar.</p>
</if:choose>

  <note style="tip">
    <p>El color de las pantallas cambia todo el tiempo: la luz de fondo en una pantalla TFT tiene aproximadamente cada 18 meses una reducción de brillo, y a medida que envejece irá amarilleando. Esto significa que debe volver a calibrar la pantalla cuando  aparece el icono [!] en el panel de control del color.</p>
    <p>Las pantallas LED también cambian con el tiempo, pero un ritmo mucho más lento que el TFT.</p>
  </note>

</page>
