<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="music-player-notrecognized" xml:lang="es">
  <info>
    <link type="guide" xref="media#music"/>
    <link type="seealso" xref="music-player-newipod"/>

    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Añadir un archivo <input>.is_audio_player</input> para decirle a su equipo que es un reproductor de sonido.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>¿Por qué no se reconoce mi reproductor de audio cuando lo conecto?</title>

<p>Si conecta un reproductor de sonido (reproductor MP3, etc.) al equipo pero no puede verlo en su aplicación de organización musical, puede que no se haya reconocido correctamente como reproductor de sonido.</p>

<p>Intente desconectar el reproductor y conectarlo de nuevo. Si eso no le ayuda, <link xref="files-browse">abra el gestor de archivos</link>. Debería ver el nombre del reproductor bajo <gui>Dispositivos</gui> en la barra lateral, pulse para abrir la carpeta del reproductor de audio. Ahora, pulse <guiseq><gui>Archivo</gui><gui>Documento nuevo</gui><gui>Documento vacío</gui></guiseq>, teclee <input>.is_audio_player</input> y pulse <key>Intro</key> (el punto y los guiones bajos son importantes, y debería estar todo en minúsculas). Este archivo le dice al equipo que reconozca el dispositivo como un reproductor de audio.</p>

<p>Ahora, busque el reproductor de audio en el gestor de archivos en la barra lateral y expúlselo (pulsación derecha y seleccionar <gui>Expulsar</gui>). Desconéctelo y vuelva a conectarlo. Esta vez deberá ser reconocido como un reproductor de audio por su organizador de música. Si no es así, intente cerrar el organizador de música y volver a abrirlo.</p>

<note>
 <p>Estas instrucciones no funcionan con iPods y algunos otros reproductores de sonido. En cambio, deberían funcionar si su reproductor es un dispositivo <em>de almacenamiento masivo USB</em>; en el manual del dispositivo debería decir si lo es.</p>
</note>

<note>
 <p>Cuando mire en la carpeta del reproductor de audio de nuevo, no verá el archivo <input>.is_audio_player</input>. Esto es debido al punto en el nombre de archivo, que le dice al gestor de archivos que le oculte. Puede comprobar que sigue ahí pulsando <guiseq><gui>Ver</gui><gui>Mostrar archivos ocultos</gui></guiseq>.</p>
</note>

</page>
