<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="hardware-cardreader" xml:lang="es">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="hardware#problems"/>

    <desc>Resolver problemas de lectores de tarjetas multimedia</desc>

    <revision version="13.10" date="2013-09-12" status="review"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Problemas con la lectura de tarjetas multimedia</title>

<p>Muchos equipos incluyen lectores para SD (Secure Digital), MMC (MultiMediaCard), SmartMedia, Memory Stick, CompactFlash y otras tarjetas de memoria de almacenaminto. Ellas deberían detectarse y <link xref="disk-partitions">montarse</link> automáticamente. Aquí hay algunos pasos para solucionar problemas, por si no fueron detectadas:</p>

<steps>
<item>
<p>Asegúrese de que la tarjeta está introducida correctamente. Muchas tarjetas parece que están vueltas del revés cuando están correctamente insertadas. Asegúrese también de que la tarjeta está firmemente asentada en la ranura; algunas tarjetas, especialmente las CompactFlash, necesitan un cierta cantidad de fuerza para insertarlas correctamente (¡tenga cuidado de no apretar demasiado! Si topa con algo firme, no lo fuerce).</p>
</item>

<item>
<p>Abra <app>Archivos</app> usando el <gui>Tablero</gui>. ¿Aparece la tarjeta insertada en la lista de <gui>Dispositivos</gui> en la barra lateral izquierda? En ocasiones la tarjeta aparece en la lista, pero no está montada; haga clic sobre ella para montarla (si la barra lateral no está visible, pulse <key>F9</key> o haga clic en <guiseq><gui>Ver</gui><gui>Panel lateral</gui><gui>Mostrar panel lateral</gui></guiseq>).</p>
</item>

<item>
<p>Si su tarjeta no se muestra en la barra lateral, pulse <guiseq><gui>Ir</gui><gui>Equipo</gui></guiseq>. Si su lector de tarjetas está configurado correctamente, el lector debería verse como un soporte cuando no está presente la tarjeta, y como la tarjeta misma cuando esté montada la tarjeta (vea la imagen a continuación).</p>
</item>

<item>
<p>Si usted ve el lector de tarjetas, pero no la tarjeta, el problema puede estar en la propia tarjeta. Intente con otra tarjeta o compruebe la tarjeta en un lector diferente si es posible.</p>
</item>
</steps>

<p>Si no hay ninguna tarjeta o soporte en la carpeta <gui>Equipo</gui>, es posible que su lector de tarjetas no funcione con Linux por problemas de controladores. Si su lector de tarjetas es interno (que está situado dentro de su equipo) esto es más probable. La mejor solución es conectar su dispositivo (cámara, teléfono, etc.) directamente mediante un puerto USB. También hay lectores de tarjetas externos que se conectan por USB, y son mucho más compatibles con Linux.</p>

</page>
