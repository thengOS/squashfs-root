<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="addremove-install" xml:lang="es">

  <info>
    <credit type="author">
      <name>Equipo de documentación de Ubuntu</name>
    </credit>
    <desc>Use <app>Ubuntu Software</app> para añadir programas y hacer Ubuntu mas utilizable</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="addremove" group="#first"/>
    <link type="seealso" xref="addremove-remove"/>
    <link type="seealso" xref="addremove-install-synaptic"/>
    <link type="seealso" xref="prefs-language-install"/>
    <revision version="16.04" date="2016-03-13" status="review"/>
  </info>

  <title>Instalar software adicional</title>

  <p>El equipo de desarrollo de Ubuntu ha seleccionado un conjunto predeterminado de aplicaciones que considera que hacen a Ubuntu muy útil para la mayoría de las tareas cotidianas. Sin embargo, seguramente querrá instalar más programas para hacer que Ubuntu le resulte más útil.</p>

  <p>Para instalar software adicional, complete los pasos siguientes:</p>

  <steps>
    <item>
      <p>Conéctese a Internet usando una conexión <link xref="net-wireless-connect">inalámbrica</link> o <link xref="net-wired-connect">cableada</link>.</p>
    </item>
    <item>
      <p>
        Click the <app>Ubuntu Software</app> icon in the <gui>Launcher</gui>, or search
        for <input>Software</input> in the search bar of the <gui>Dash</gui>.
      </p>
    </item>
    <item>
      <p>
        When <app>Ubuntu Software</app> launches, search for an application, or select 
        a category and find an application from the list.
      </p>
    </item>
    <item>
      <p>Seleccione la aplicación que le interese y pulse <gui>Instalar</gui>.</p>
    </item>
    <item>
      <p>Se le pedirá que introduzca su contraseña. Cuando lo haya hecho, comenzará la instalación.</p>
    </item>
    <item>
      <p>La instalación normalmente termina rápido, pero puede llevar tiempo si tiene una conexión a Internet lenta.</p>
    </item>
    <item>
      <p>Se añadirá un acceso directo a su nueva aplicación en el Lanzador. Para desactivar esta función, desmarque <guiseq><gui>Ver</gui><gui> Nuevas aplicaciones en el Lanzador</gui></guiseq>.</p>
    </item>
  </steps>
  
</page>
