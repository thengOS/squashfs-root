<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-add" xml:lang="es">

  <info>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

		<credit type="editor">
      <name>Greg Beam</name>
      <email>ki7mt@yahoo.com</email>
    </credit>

		<link type="guide" xref="accounts"/>
    <revision pkgversion="3.10.2" version="0.1" date="2014-01-08" status="review"/>
		<revision version="14.04" date="2014-01-08" status="review"/>
		<include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

		<desc>Conectar con cuentas en línea</desc>

  </info>

	<title>Añadir una cuenta</title>

  <p>Añadiendo una cuenta ayudará a enlazar sus cuentas en línea con el escritorio de Ubuntu. Por lo tanto, su correo electrónico, chat y otras aplicaciones relacionadas se prepararán para usted.</p>

  <steps>
		<item>
			<p>Pulse en el icono situado en el extremo derecho de la <gui>barra de menús</gui> y seleccione <gui>Configuración del sistema</gui>.</p>
		</item>
		<item>
			<p>Seleccione <gui>Cuentas en línea</gui>.</p>
		</item>
    <item>
	    <p>Seleccione un <gui>Tipo de cuenta</gui> en el panel de la derecha.</p>
			<note style="tip">
		    <p>Si desea configurar más cuentas, puede repetir el proceso de nuevo.</p>
		  </note>
    </item>
    <item>
      <p>Se abrirá una pequeña interfaz web de usuario en la que puede introducir las credenciales de su cuenta en línea. Por ejemplo, si está configurando una cuenta de Google, escriba su nombre de usuario de Google, contraseña e inicie sesión.</p>
    </item>
    <item>
      <p>Si ha introducido sus credenciales correctamente, se le pedirá que acepte los términos de uso. Seleccione <gui>Aceptar</gui> para continuar. Una vez aceptados, Ubuntu necesita permiso para acceder a su cuenta. Para permitir el acceso, pulse el botón <gui>Conceder acceso</gui>. Cuando se le pregunte, escriba la contraseña del usuario actual.</p>
    </item>
		<item>
      <p>Ahora puede seleccionar las aplicaciones que quiere enlazar con su cuenta en línea. Por ejemplo, si quiere usar una cuenta en línea para chatear, pero no desea el calendario, desactive la opción <gui>calendario</gui>.</p>
    </item>
  </steps>
  <p>Después de agregar las cuentas, cada aplicación seleccionada utilizará estas credenciales cuando inicie sesión.</p>
  <note style="tip">
    <p>Por razones de seguridad, Ubuntu no almacenará la contraseña en su equipo. En su lugar, almacena un testigo proporcionado por el servicio en línea. Si lo desea puede revocar totalmente la relación entre el escritorio y el servicio en línea, usando <link xref="accounts-remove">eliminar</link>.</p>
  </note>
</page>
