<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="guide" style="task" version="1.0 if/1.0" id="bluetooth-send-file" xml:lang="es">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="files-share"/>

    <revision pkgversion="3.8.2" version="0.2" date="2013-05-16" status="review"/>
    <revision version="13.10" date="2013-09-20" status="review"/>

    <desc>Compartir archivos con dispositivos Bluetooth como su teléfono.</desc>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Enviar un archivo a un dispositivo Bluetooth</title>

<if:choose>
  <if:when test="platform:unity">
    <p>Puede enviar archivos a dispositivos Bluetooth conectados, como teléfonos celulares u otras computadoras. Algunos dispositivos no permiten la transferencia de archivos, o archivos de un tipo específico. Puede enviar archivos de tres formas diferentes: a través del icono de Bluetooth en la barra de menús, desde la ventana de configuración de Bluetooth, o desde el gestor de archivos.</p>
  </if:when>
  <p>Puede enviar archivos a dispositivos conectados mediante Bluetooh, tales como teléfonos móviles u otros equipos. Algunos tipos de dispositivos no permiten la transmisión de archivos o tipos específicos de archivos. Puede enviar archivos usando el icono de Bluetooth en la barra superior o desde la ventana de configuración del Bluetooth.</p>
</if:choose>

<if:choose>
  <if:when test="platform:unity">
  <p>Para enviar archivos directamente desde el gestor de archivos, vea <link xref="files-share"/>.</p>
  </if:when>
</if:choose>

  <note style="important">
    <p>Antes de comenzar, asegúrese de que Bluetooth está activado en su equipo. Vea <link xref="bluetooth-turn-on-off"/>.</p>
  </note>

  <steps>
    <title>Enviar archivos usando el icono Bluetooth</title>
    <item>
      <if:choose>
        <if:when test="platform:unity">
      <p>Pulse en el icono de Bluetooth en la barra de menús y seleccione <gui>Enviar archivos al dispositivo</gui>.</p>
        </if:when>
      <p>Haga clic en el icono de Bluetooth en la barra superior y seleccione <gui>Enviar archivos al dispositivo</gui>.</p>
      </if:choose>
    </item>
    <item>
      <p>Seleccione el archivo que quiera enviar y pulse <gui>Seleccionar</gui>.</p>
      <p>Para enviar más de un archivo en una carpeta, mantenga presionada la tecla <key>Ctrl</key> mientras selecciona cada archivo.</p>
    </item>
    <item>
      <p>Seleccione el dispositivo al que quiere enviar los archivos y pulse <gui>Enviar</gui>.</p>
      <p>La lista de los dispositivos que se mostrarán <link xref="bluetooth-connect-device"> son dispositivos que tiene conectados a </link> así como <link xref="bluetooth-visibility"> dispositivos visibles </link> dentro de un rango. Si todavía no se ha conectado al dispositivo seleccionado, se le preguntará para emparejar con el dispositivo después de pulsar <gui>Enviar</gui>. Esto requerirá probablemente confirmación por parte del otro dispositivo.</p>
      <p>Si hay muchos dispositivos, puede limitar la lista a únicamente determinados tipos de dispositivos utilizando la lista desplegable <gui>Tipo de dispositivo</gui>.</p>
    </item>
    <item>
      <p>El propietario del dispositivo receptor normalmente tiene que pulsar un botón para aceptar el archivo. Cuando este acepte o rechace, el resultado de la transferencia de archivos se mostrará en pantalla.</p>
    </item>
  </steps>

  <steps>
    <title>Enviar archivos desde la configuración de Bluetooth</title>
    <item>
      <if:choose>
        <if:when test="platform:unity">
          <p>Pulse en el icono de Bluetooth en la barra de menús y seleccione <gui>Configuración de Bluetooth</gui>.</p>
        </if:when>
        <p>Haga clic en el icono de Bluetooth en la barra superior y seleccione <gui>Configuración de Bluetooth</gui>.</p>
      </if:choose>
    </item>
    <item><p>Seleccione el dispositivo al que desea enviar los archivos en la lista izquierda. Esta lista únicamente muestra los dispositivos a los que ya se ha conectado. Consulte <link xref="bluetooth-connect-device"/>.</p></item>
    <item><p>En la información del dispositivo, en la derecha, pulse <gui>Enviar archivos</gui>.</p></item>
    <item>
      <p>Seleccione el archivo que quiera enviar y pulse <gui>Seleccionar</gui>.</p>
      <p>Para enviar más de un archivo en una carpeta, mantenga presionada la tecla <key>Ctrl</key> mientras selecciona cada archivo.</p>
    </item>
    <item>
      <p>El propietario del dispositivo receptor normalmente tiene que pulsar un botón para aceptar el archivo. Cuando este acepte o rechace, el resultado de la transferencia de archivos se mostrará en pantalla.</p>
    </item>
  </steps>
</page>
