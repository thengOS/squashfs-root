<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="power-closelid" xml:lang="es">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.10" date="2013-07-18" status="outdated"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Los portátiles se suspenden cuando se cierra la tapa para ahorrar energía.</desc>
  </info>

<title>¿Por qué se apaga mi equipo cuando cierro la tapa?</title>

<p>Cuando cierra la cubierta de su portátil, su equipo se <link xref="power-suspend"><em>suspenderá</em></link> para ahorrar energía. Esto significa que el equipo no está apagado en realidad, sino en un estado «dormido». Puede reactivar el equipo abriendo la cubierta. Si no se reactiva, presione cualquier tecla o pulse con el ratón. Si esto todavía no funciona, presione el botón de encendido.</p>

<p>Algunos equipos no son capaces de suspender adecuadamente, debido a que el hardware no está soportado completamente por el sistema operativo (por ejemplo, los controladores Linux no están adaptados). En este caso, puede encontrar que no es capaz de reanudar el equipo después de haber cerrado la tapa. Puede intentar <link xref="power-suspendfail">arreglar el  problema con suspender</link>, o impedir que el equipo suspenda cuando cierre la tapa.</p>

<section id="nosuspend">
 <title>No permitir que el equipo se suspenda al cerrar la tapa</title>
 <p>Sí no quiere que el equipo suspenda cuando cierre la tapa, puede cambiar la configuración de ese comportamiento:</p>
 <note style="warning">
   <p>Tenga mucho cuidado si cambia esta opción. Algunos portátiles pueden sobrecalentarse si se dejan funcionando con la tapa cerrada, especialmente si están en un lugar confinado, como una mochila.</p>
 </note>

<steps>
  <item><p>Pulse en el icono situado en el extremo derecho de la <gui>barra de menús</gui> y seleccione <gui>Configuración del sistema</gui>.</p></item>
  <item><p>En la sección <gui>Hardware</gui>, pulse <gui>Energía</gui>.</p></item>
  <item><p>Establezca los menús desplegables a <gui>Al cerrar la tapa</gui> to <gui>No hacer nada</gui>.</p></item>
</steps>
</section>

</page>
