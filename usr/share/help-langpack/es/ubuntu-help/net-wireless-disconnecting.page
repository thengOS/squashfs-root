<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="es">
  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Puede que haya poca señal, o que la red no le permita conectarse correctamente.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>¿Por qué mi red inalámbrica se sigue desconectando?</title>

<p>Puede toparse con que ha sido desconectado/a de una red inalámbrica aún cuando ha decidido permanecer conectado/a. Su equipo normalmente intentará reconectarse a la red en cuanto suceda esto (el icono de red en la barra de menús se animará si el equipo intenta reconectarse), pero esto puede resultar molesto, en especial si estaba usando Internet en ese momento.</p>

<section id="signal">
 <title>Señal inalámbrica débil</title>

 <p>Una razón frecuente por la que puede desconectarse de una red inalámbrica es que recibe poca señal. Las redes inalámbricas tienen un alcance limitado, por lo que si está demasiado lejos de la estación base inalámbrica no recibirá una señal lo bastante fuerte como para mantener la conexión. Los muros y otros objetos situados entre usted y la estación base también debilitan la señal.</p>
 
 <p>El icono de red en la barra de menús muestra qué tan fuerte es su señal inalámbrica. Si la señal es baja, intente moverse más cerca de la estación base inalámbrica.</p>
 
</section>

<section id="network">
 <title>La conexión de red no se establece correctamente</title>

 <p>A veces, cuando se conecta a una red inalámbrica, puede parecer que se ha conectado correctamente al principio, pero luego se desconectará. Esto sucede normalmente porque el equipo obtuvo solo un éxito parcial en la conexión a la red. Gestionó cómo establecer una conexión, pero no pudo finalizar la conexión por alguna razón y la desconectó.</p>

 <p>Una posible razón para esto es que se equivocó al introducir la contraseña inalámbrica, o que su equipo no tiene permiso para conectarse a la red (porque la red necesita un nombre de usuario, por ejemplo).</p>

</section>

<section id="hardware">
 <title>Controladores o hardware inalámbrico poco fiables</title>

 <p>Algunos equipos de red inalámbrica pueden ser poco fiables. Las redes inalámbricas son complicadas, por lo que las tarjetas inalámbricas y estaciones base de vez en cuando tienen problemas de menor importancia y pueden perder la conexión. Esto es molesto, pero ocurre con bastante regularidad con muchos dispositivos. Si se desconecta de conexiones inalámbricas de vez en cuando, esta puede ser la única razón. Si esto ocurre con mucha frecuencia, es posible que desee considerar el uso de hardware diferente.</p>

</section>

<section id="busy">
 <title>Redes inalámbricas ocupadas</title>

 <p>Las redes inalámbricas en lugares ocupados (en universidades y cibercafés, por ejemplo) a menudo tienen muchos equipos que intentan conectarse todo a la vez. A veces esas redes se saturan y pueden no ser capaces de manejar todos los equipos que están intentando conectarse, por lo que algunos de ellos quedan desconectados.</p>

</section>

</page>
