<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="documents-view" xml:lang="es">

  <info>
    <credit type="author copyright">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <desc>Ver documentos a pantalla completa.</desc>
    <link type="guide" xref="documents#view" group="#first"/>
    <revision pkgversion="3.5.90" date="2012-09-02" status="review"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Mostrar documentos almacenados localmente o en línea.</title>

  <p>Cuando abra <app>Documentos</app>, todos sus documentos, tanto los almacenados en local como los almacenados en línea, se muestran como miniaturas.</p>

  <note style="tip"><p>Para que sus documentos de <em>Google Docs</em> o <em>SkyDrive</em> aparezcan es necesario que configure Google o Windows Live, respectivamente, como una <link xref="accounts-add">cuenta en línea</link>.</p>
  </note>

  <p>Para ver el contenido de un documento:</p>

  <steps>
    <item><p>Haga clic sobre la miniatura. El documento se muestra a anchura completa en la ventana <app>Documentos</app> (o a pantalla completa si está maximizada).</p></item>
  </steps>

  <p>Para salir del documento, pulse el botón de la flecha hacia atrás.</p>

</page>
