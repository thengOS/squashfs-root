<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="guide" style="task" version="1.0 if/1.0" id="bluetooth-turn-on-off" xml:lang="es">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="15.04" date="2015-03-19" status="review"/>

    <desc>Activar o desactivar el dispositivo Bluetooth de su equipo.</desc>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Equipo de documentación de Ubuntu</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Encender o apagar el Bluetooth</title>

<media type="image" mime="image/svg" src="figures/bluetooth-active.svg" style="floatend">
  <p>El icono de Bluetooth en la barra de menús</p>
</media>

<p>Puede activar el Bluetooth para conectar con dispositivos Bluetooth y enviar y recibir archivos, pero desactívelo para ahorrar energía. Para activar el Bluetooth, pulse en el icono de Bluetooth en la barra superior, y seleccione <gui>Encender Bluetooth</gui>.</p>

<if:choose>
  <if:when test="platform:unity">
    <p>Muchos portátiles tienen un interruptor o una combinación de teclas para encender o apagar el Bluetooth. Si el hardware del Bluetooth está apagado, no verá un icono en la barra del menú. Busque un interruptor  en su equipo o una tecla. La tecla frecuentemente se activa con la ayuda de la tecla <key>Fn</key>.</p>
 </if:when>
 <p>Muchos portátiles tienen un interruptor físico o una combinación de teclas para encender y apagar el Bluetooth. Si el interruptor físico está apagado, no podrá ver el icono de Bluetooth en la barra superior. Busque un interruptor en su equipo o una tecla en su teclado. La tecla suele ir asociada con la tecla <key>Fn</key>.</p>
</if:choose>

<p>Para desactivar el Bluetooth, haga clic en el icono de Bluetooth y seleccione <gui>Apagar Bluetooth</gui>.</p>

<note><p>Solo debe cambiar <gui> Visible</gui>  si conecta a este equipo desde otro dispositivo. Ver <link xref="bluetooth-visibilidad"/> para obtener más información.</p></note>

</page>
