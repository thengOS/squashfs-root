<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_gecko" xml:lang="es">
  <info>
    <link type="guide" xref="preferences#application"/>
    <link type="next" xref="preferences_table_navigation"/>
    <title type="sort">1. Navegación Gecko</title>
    <title type="link">Navegación Gecko</title>
    <desc>Configurar el soporte de <app>Orca</app> para <app>Firefox</app> y <app>Thunderbird</app></desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Compartir Igual 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2012 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>
  <title>Preferencias de la navegación Gecko</title>
  <section id="page_navigation">
    <title>Navegación por páginas</title>
    <p>El grupo de controles <gui>Navegación por páginas</gui> le permite personalizar cómo se presenta <app>Orca</app>, y le permite interactuar con el texto y otros contenidos.</p>
    <section>
      <title>Controlar la navegación de cursor</title>
      <p>Esta casilla activa o desactiva la navegación de cursor de <app>Orca</app>. Cuando está activada, <app>Orca</app> toma el control del cursor a mientras se mueve por una página; cuando está desactivada, la navegación del cursor nativa de Gecko está activa.</p>
      <p>Valor predeterminado: marcado</p>
      <note style="tip">
        <title>Esta configuración se puede cambiar al vuelo</title>
        <p>Para cambiar esta configuración al vuelo sin guardarla, use <keyseq><key>Modificador de Orca</key><key>F12</key></keyseq>.</p>
       </note>
    </section>
    <section>
      <title>Automatic focus mode during caret navigation</title>
      <p>
        If this checkbox is checked, <app>Orca</app> will automatically turn on
        focus mode when you use caret navigation commands to navigate to a form
        field. For example, pressing <key>Down</key> would allow you to move
        into an entry but once you had done so, Orca would switch to focus mode
        and subsequent presses of <key>Down</key> would be controlled by the web
        browser and not by Orca. If this checkbox is not checked, <app>Orca</app>
        would continue to control what happens when you press <key>Down</key>,
        thus making it possible to arrow out of the entry and continue reading.
      </p>
      <p>Valor predeterminado: no marcado</p>
      <note style="tip">
        <title>Manually switching between browse mode and focus mode</title>
        <p>
          In order to start or stop interacting with the focused form field,
          use <keyseq><key>Orca Modifier</key><key>A</key></keyseq> to switch
          between browse mode and focus mode.
        </p>
       </note>
    </section>
    <section>
      <title>Activar la navegación estructural</title>
      <p>Esta opción activa o desactiva la <link xref="howto_structural_navigation">navegación estructura</link> de <app>Orca</app>. La navegación estructural le permite navegar por elementos como cabeceras, enlaces y campos de formularios.</p>
      <p>Valor predeterminado: marcado</p>
      <note style="tip">
        <title>Esta configuración se puede cambiar al vuelo</title>
        <p>Para cambiar esta configuración al vuelo sin guardarla, use <keyseq><key>Modificador de Orca</key><key>Z</key></keyseq>.</p>
       </note>
    </section>
    <section>
      <title>Automatic focus mode during structural navigation</title>
      <p>
        If this checkbox is checked, <app>Orca</app> will automatically turn on
        focus mode when you use structural navigation commands to navigate to a
        form field. For example, pressing <key>E</key> to move to the next entry
        would move focus there and also turn focus mode on so that your next press
        of <key>E</key> would type an "e" into that entry. If this checkbox is not
        checked, then <app>Orca</app> will leave you in browse mode and your next
        press of <key>E</key> would move you to the next entry on the page.
      </p>
      <p>Valor predeterminado: no marcado</p>
      <note style="tip">
        <title>Manually switching between browse mode and focus mode</title>
        <p>
          In order to start or stop interacting with the focused form field,
          use <keyseq><key>Orca Modifier</key><key>A</key></keyseq> to switch
          between browse mode and focus mode.
        </p>
       </note>
    </section>
    <section>
      <title>Empezar a leer una página automáticamente cuando se carga por primera vez</title>
      <p>Si esta casilla está marcada, <app>Orca</app> leerá la página web o el correo-e completos.</p>
      <p>Valor predeterminado: activado para Firefox, desactivado para Thunderbird</p>
    </section>
    <section>
      <title>Enable layout mode for content</title>
      <p>
        If this checkbox is checked, <app>Orca</app>'s caret navigation will respect
        the on-screen layout of the content and present the full line, including any
        links or form fields on that line. If this checkbox is not checked, <app>Orca</app>
        will treat objects such as links and form fields as if they were on separate
        lines, both for presentation and navigation.
      </p>
      <p>Valor predeterminado: marcado</p>
    </section>
  </section>
  <section id="table_options">
    <title>Opciones de tabla</title>
    <note>
      <p>Para aprender más sobre las opciones de <app>Orca</app> para navegar por tablas, consulta las <link xref="preferences_table_navigation">Preferencias de navegación por tablas</link>.</p>
    </note>
  </section>
  <section id="find_options">
    <title>Opciones de búsqueda</title>
    <p>El grupo de controles <gui>Opciones de búsqueda</gui> hace posible que personalice cómo <app>Orca</app> presenta los resultados de una búsqueda llevada a cabo usando la funcionalidad de búsqueda de la aplicación.</p>
    <section>
      <title>Hablar resultados durante la búsqueda</title>
      <p>Si esta casilla está marcada, <app>Orca</app> leerá la línea que coincida con su búsqueda actual.</p>
      <p>Valor predeterminado: marcado</p>
    </section>
    <section>
      <title>Hablar solamente las líneas modificadas durante la búsqueda</title>
      <p>Si esta casilla de verificación está marcada, <app>Orca</app> no presentará la línea coincidente si es la misma línea que para la coincidencia anterior. Esta opción está diseñada para evitar «charlatanería» en una línea con múltiples coincidencias de la cadena que está buscando.</p>
      <p>Valor predeterminado: no marcado</p>
    </section>
    <section>
      <title>Longitud mínima del texto coincidente</title>
      <p>Este botón contador editable es donde puede especificar el número de caracteres que deben coincidir al menos antes que <app>Orca</app> anuncie la línea coincidente. Esta opción también está diseñada para prevenir «ruido» ya que al principio cuando comienza a teclear la cadena de búsqueda hay muchas coincidencias.</p>
      <p>Valor predeterminado: 4</p>
    </section>
  </section>
</page>
