<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-lost" xml:lang="en-GB">

  <info>
    <link type="guide" xref="files" group="more"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <desc>Follow these tips if you can't find a file you created or downloaded.</desc>

    <credit type="author">
      <name>GNOME Documentation Project</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Find a lost file</title>

<p>If you created or downloaded a file, but now you can't find it, follow these tips.</p>

<list>
  <item><p>If you don't remember where you saved the file, but you have some idea of how you named it, you can search for the file by name. See <link xref="files-search"/> to learn how.</p></item>

  <item><p>If you just downloaded the file, your web browser might have automatically saved it to a common folder. Check the Desktop and Downloads folders in your home folder.</p></item>

  <item><p>You might have accidentally deleted the file. When you delete a file, it gets moved to the Rubbish Bin, where it stays until you manually empty the Rubbish Bin. See <link xref="files-recover"/> to learn how to recover a deleted file.</p></item>

  <item><p>You might have renamed the file in a way that made the file hidden. Files that start with a <file>.</file> or end with a <file>~</file> are hidden in the file manager. Click the <media type="image" src="figures/go-down.png">down</media> button in the file manager toolbar and pick <gui>Show Hidden Files</gui> to display them. See <link xref="files-hidden"/> to learn more.</p></item>
</list>

</page>
