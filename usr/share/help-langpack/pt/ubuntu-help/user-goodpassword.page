<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="pt">

  <info>
    <credit type="author">
      <name>Projeto de Documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <desc>Escolha passwords longas e complicadas.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="user-accounts#passwords"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="14.04" date="2013-10-24" status="candidate"/>
  </info>

  <title>Escolha uma palavra-chave segura</title>

  <note style="important">
    <p>Faça as sua senhas faceis o suficiente para que se lembre, mas dificil para os outros (incluindo programas de computador) adivinhar.</p>
  </note>

  <p>Escolher uma boa senha ajuda-lo-à a manter o seu computador seguro. Se a sua senha é facil de adivinhar, alguém poderá perceber e conseguir acesso a sua informação pessoal.</p>
  <p>Algumas pessoas até poderão usar computadores para de uma forma sistemática tentar adivinhar a sua senha, por isso poderá ser humanamente dificil de adivinhar mas poderá ser extremamente fácil de decifrar por um programa de computador. Aqui estão algumas dicas para escolher uma boa senha:</p>

  <list>
    <item>
      <p>Utilize uma mistura entre letras maiusculas e minusculas, números, simbolos e espaços na senha. Isto torna-a mais dificil de adivinhar. Há mais símbolos para escolher, de modo que mais senhas possíveis teriam de ser verificadas por alguém ao tentar adivinhar a sua.</p>
      <note>
        <p>
          A good method for choosing a password is to take the first letter of 
          each word in a phrase that you can remember. The phrase could be the 
          name of a movie, a book, a song, or an album. For example, "Flatland: 
          A Romance of Many Dimensions" would become F:ARoMD or faromd or f: 
          aromd.
        </p>
      </note>
    </item>
    <item>
      <p>
        Make your password as long as possible. The more characters it contains, 
        the longer it should take for a person or computer to guess it.
      </p>
    </item>
    <item>
      <p>Não use quaisquer palavras que apareçam em dicionários predefinidos em qualquer idioma. Os piratas informáticos vão experimentar essa  primeiro. A senha mais comum é "password" -- qualquer um adivinha senhas como esta muito rapidamente!</p>
    </item>
    <item>
      <p>Não use nenhum informação pessoal, tal como datas, matricula ou nome de um membro da família.</p>
    </item>
    <item>
      <p>Não utilize quaisquer substantivos.</p>
    </item>
    <item>
      <p>Escolha uma senha que possa ser escrita rapidamente, para reduzir a hipótese de que alguém seja capaz de perceber o que escreveu se estiver em posição de ver o que está fazendo.</p>
      <note style="tip">
        <p>Nunca escreva as suas senhas por baixo de alguma coisa. Podem ser encontradas!</p>
      </note>
    </item>
    <item>
      <p>Use diferentes passwords para diferentes coisas.</p>
    </item>
    <item>
      <p>Use diferentes passwords para diferentes contas.</p>
      <p>Se você usa a mesma senha para todas as suas contas, qualquer um que adivinhe fica com a possibilidade de aceder a todas as suas contas de imediato.</p>
      <p>Muitas senhas são difíceis de lembrar. Apesar de não ser tão seguro como o uso de senhas diferentes para tudo, pode ser mais fácil de usar a mesma para as coisas que não importam (como paginas web), e outras diferentes para coisas importantes (como a sua conta bancária on-line e e-mail).</p>
    </item>
    <item>
      <p>Altere a sua password regularmente.</p>
    </item>
  </list>

</page>
