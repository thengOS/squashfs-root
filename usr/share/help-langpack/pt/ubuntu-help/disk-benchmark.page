<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="disk-benchmark" xml:lang="pt">

  <info>
    <link type="guide" xref="disk"/>

    <revision pkgversion="3.6.2" version="0.2" date="2012-11-16" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <credit type="author">
      <name>Projeto de Documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
   <credit type="editor">
     <name>Michael Hill</name>
     <email>mdhillca@gmail.com</email>
   </credit>

    <desc>Execute testes de referência ao seu disco rígido para vericar o quanto rápido é.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Teste o desempenho do seu disco rígido</title>

  <p>Para testar a velocidade do seu disco rígido</p>

<steps>

 <item>
<if:choose>
<if:when test="platform:unity">
  <p>Abra a aplicação de <app>Discos</app> a partir do <link xref="unity-dash-intro">Painel</link>.</p>
</if:when>
  <p>Abra a app de <app>Discos</app> a partir da vista geral de atividades.</p>
</if:choose>
 </item>
 <item>
  <p>Escolha o disco rígido a partir da lista de <gui>Unidades de Disco</gui>.</p>
 </item>
 <item>
  <p>Clique no botão de rodagem e selecione <gui>Teste de Referência da Unidade</gui></p>
 </item>
 <item>
  <p>Clique em <gui> Começar o teste de referência</gui> e ajuste de <gui>Taxa de Transferência </gui> e <gui>Tempo de Acesso</gui> e parâmetros se desejar.</p>
 </item>
 <item>
  <p>Clique em<gui>Começar o teste de referência</gui> para testar a velocidade de leitura de dados do disco rigido. <link xref="user-admin-explain">privilégios Administrativos</link> talvez sejam pedidos. Insira a sua senha, ou a senha para o administrador de contas pedido.</p>
 <note>
  <p>Se <gui>Also perform write-benchmark</gui> é verificado, o benchmark vai testar a velocidade de dados que podem ser lidos e escritos no disco. Isto vai demorar até ficar completo.</p>
 </note>
 </item>

</steps>

  <p>Quando o teste estiver finalizado, os resultados vão aparecer no gráfico. Os pontos e as linhas verdes indicam os exemplos escolhidos; este corresponde ao eixo direito, mostrando o tempo de acesso, em função do eixo de baixo, representando o tempo decorrido durante o benchmark. A linha azul representa as taxas de leitura, enquanto que a linha vermelha representa as taxas de escrita; estas são mostradas como as taxas de dados de acesso no eixo da esquerda, representada graficamente em função da percentagem do disco percorrida, a partir do exterior para o eixo, ao longo do eixo inferior.</p>

  <p>Abaixo do gráfico, os valores são exibidos para mínimo, máximo e médio de leitura e gravação de câmbio, o tempo médio de acesso e do tempo decorrido desde o último teste de benchmark.</p>

</page>
