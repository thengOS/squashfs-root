<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="documents-collections" xml:lang="en-CA">

  <info>
    <credit type="author copyright">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <desc>
      Group related documents in a collection.
    </desc>
    <link type="guide" xref="documents#print"/>
    <revision pkgversion="3.5.90" date="2012-09-05" status="review"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Make collections of documents</title>

  <p><app>Documents</app> lets you put together documents of different types in
  one place called a <em>collection</em>. If you have documents that are
  related, you can group them to make them easier to find. For example, if you
  had a business trip where you made a presentation, your slides, your flight
  itinerary (a PDF file), your budget spreadsheet, and other hybrid PDF/ODF
  documents, can be grouped in one collection.</p>

  <p>To create or add to a collection:</p>
  <list>
   <item><p>Click the <gui>✓</gui> button.</p></item>
   <item><p>In selection mode, check the documents to be collected.</p></item>
   <item><p>Click the <gui>+</gui> button in the button bar.</p></item>
   <item><p>In the collection list, click <gui>Add</gui> and type a new
    collection name, or select an existing collection. The selected documents
    will be added to the collection.</p></item>
  </list>

  <note>
   <p>Collections do not behave like folders and their hierarchy: <em>you
   cannot put collections inside collections.</em></p>
  </note>

  <p>To delete a collection:</p>
  <list>
   <item><p>Click the <gui>✓</gui> button.</p></item>
   <item><p>In selection mode, check the collection to be deleted.</p></item>
   <item><p>Click the Trash button in the button bar. The collection will be
    deleted, leaving the original documents.</p></item>
  </list>

</page>
