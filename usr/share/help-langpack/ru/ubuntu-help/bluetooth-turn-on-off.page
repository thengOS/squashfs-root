<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="guide" style="task" version="1.0 if/1.0" id="bluetooth-turn-on-off" xml:lang="ru">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="15.04" date="2015-03-19" status="review"/>

    <desc>Включите или отключите Bluetooth-устройство на вашем компьютере.</desc>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Команда документации Ubuntu</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Включение или выключение Bluetooth</title>

<media type="image" mime="image/svg" src="figures/bluetooth-active.svg" style="floatend">
  <p>Значок Bluetooth на панели меню</p>
</media>

<p>Можно как включить Bluetooth, чтобы использовать Bluetooth-устройства для передачи и приёма файлов, так и выключить его для экономии электроэнергии. Чтобы включить Bluetooth, щёлкните значок Bluetooth на верхней панели и включите переключатель <gui>Bluetooth</gui>.</p>

<if:choose>
  <if:when test="platform:unity">
    <p>Многие портативные компьютеры для включения или отключения Bluetooth имеют аппаратный переключатель или используют для этого комбинацию клавиш. Если Bluetooth отключён аппаратно, то значка Bluetooth на панели меню увидеть нельзя. Поищите переключатель на компьютере или специальную клавишу на клавиатуре. Часто задействовать такую клавишу можно с помощью клавиши <key>Fn</key>.</p>
 </if:when>
 <p>Многие портативные компьютеры для включения или отключения Bluetooth имеют аппаратный переключатель или используют для этого комбинацию клавиш. Если Bluetooth отключён аппаратно, вы не увидите значка Bluetooth на верхней панели. Поищите переключатель на компьютере или специальную клавишу на клавиатуре. Часто задействовать эту клавишу можно с помощью клавиши <key>Fn</key>.</p>
</if:choose>

<p>Чтобы выключить Bluetooth, щёлкните значок Bluetooth и выключите переключатель <gui>Bluetooth</gui>.</p>

<note><p>Включение переключателя <gui>Видимость</gui> понадобится только в том случае, если вы подключаетесь к этому компьютеру с другого устройства. Подробнее смотрите <link xref="bluetooth-visibility"/>.</p></note>

</page>
