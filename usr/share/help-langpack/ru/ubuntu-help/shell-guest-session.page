<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="shell-guest-session" xml:lang="ru">

  <info>
    <link type="guide" xref="shell-overview#desktop" group="#last"/>
    <link type="guide" xref="user-accounts#manage"/>

    <desc>Позвольте друзьям или коллегам воспользоваться вашим компьютером, не подвергая риску безопасность системы.</desc>
    <revision version="16.04" date="2016-05-08" status="review"/>
    <credit type="author">
      <name>Gunnar Hjalmarsson</name>
      <email>gunnarhj@ubuntu.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Запуск ограниченного гостевого сеанса</title>

<section id="restricted">
<title>Временный сеанс с ограниченными привилегиями</title>

<p>Иногда другу, члену семьи или коллеге может понадобиться воспользоваться вашим компьютером. <app>Гостевой сеанс</app> предоставляет удобную возможность поработать на компьютере другому человеку, не подвергая риску безопасность системы. Гостевой сеанс можно запустить или из экрана входа в систему, или из обычного сеанса. Если вы уже вошли в систему, щёлкните самый правый значок <gui>панели меню</gui> и выберите <gui>Гостевой сеанс</gui>. Так вы заблокируете экран вашего сеанса и запустите гостевой сеанс.</p>

<p>Гость не сможет просматривать домашние папки других пользователей, и по умолчанию любые сохранённые данные или изменённые настройки будут удалены или отменены при выходе из сеанса. Это означает, что каждый гостевой сеанс запускается в «чистом» рабочем окружении, независящем от того, что делали предыдущие гостевые пользователи.</p>

</section>

<section id="customize">
<title>Настройка</title>

<p>В онлайн-руководстве <link href="https://help.ubuntu.com/community/CustomizeGuestSession"> Customize Guest Session</link> объясняется, как настроить внешний вид и поведение гостевого сеанса.</p>

</section>

<section id="disable">
<title>Отключение гостевого сеанса</title>

<p>Если вы предпочитаете не разрешать гостевой доступ к компьютеру, то можете отключить <app>Гостевой сеанс</app>. Для этого нажмите <keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq>, чтобы открыть окно терминала, и выполните следующую команду:</p>

<p><cmd its:translate="no">sudo sh -c 'printf "[Seat:*]\nallow-guest=false\n" &gt;/etc/lightdm/lightdm.conf.d/50-no-guest.conf'</cmd></p>

<p>Эта команда создаёт небольшой конфигурационный файл. Чтобы снова разрешить <app>гостевой сеанс</app>, просто удалите этот файл:</p>

<p><cmd its:translate="no">sudo rm /etc/lightdm/lightdm.conf.d/50-no-guest.conf</cmd></p>

</section>

</page>
