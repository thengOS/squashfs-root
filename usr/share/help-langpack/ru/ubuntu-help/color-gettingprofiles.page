<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-gettingprofiles" xml:lang="ru">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-why-calibrate"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-missingvcgt"/>
    <desc>Цветовые профили предоставляются поставщиками и могут быть созданы вами.</desc>

    <revision version="13.10" date="2013-09-07" status="review"/>
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Где взять цветовые профили?</title>

  <p>Лучший способ получения профилей — создать их самому, хотя это и требует некоторых первоначальных затрат.</p>
  <p>Многие производители стараются предоставлять цветовые профили для устройств, однако в ряде случаев они свёрнуты в <em>пакет драйверов</em>, который, возможно, надо будет скачать, распаковать, а затем найти там цветовые профили.</p>
  <p>Некоторые производители не предоставляют точных профилей для оборудования, таких профилей лучше избегать. Откроем секрет: загрузите профиль, и если он создан больше года назад от даты приобретения устройства, — это, скорее всего, фиктивно созданные данные, непригодные к использованию.</p>

  <p>Для информации о том, почему использование поставляемых производителем профилей часто не только бесполезно, но и вредно, см. <link xref="color-why-calibrate"/>.</p>

</page>
