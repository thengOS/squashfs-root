<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="prefs-language-install" xml:lang="ru">

  <info>
    <link type="guide" xref="prefs-language#langsupport" group="#first"/>
    <link type="guide" xref="addremove" group="#last"/>

    <desc>Установите дополнительные переводы и соответствующие языковые пакеты.</desc>
    <revision version="14.04" date="2014-01-12" status="review"/>
    <credit type="author">
      <name>Gunnar Hjalmarsson</name>
      <email>gunnarhj@ubuntu.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Установка языков</title>

  <p>Выбранный при установке Ubuntu язык устанавливается совместно с английским, но в дальнейшем можно добавить и другие языки.</p>

  <steps>
    <item>
      <p>Щёлкните самый правый значок на панели меню и выберите <gui>Параметры системы</gui>.</p>
    </item>
    <item>
      <p>В разделе «Персональные» щёлкните <gui>Язык системы</gui>.</p>
    </item>
    <item>
      <p>Щёлкните <gui>Установка и удаление языков...</gui>. Окно <gui>Установленные языки</gui> покажет список всех имеющихся языков, уже установленные языки отмечены в списке.</p>
    </item>
    <item>
      <p>Отметьте языки, которые требуется установить, и снимите метки с тех языков, которые нужно удалить.</p>
    </item>
    <item>
      <p>Щёлкните <gui>Применить изменения</gui>.</p>
    </item>
    <item>
      <p>Потребуются <link xref="user-admin-explain">административные привилегии</link>. Введите ваш пароль или пароль запрашиваемой учётной записи администратора.</p>
    </item>
  </steps>

  <p>Вместе с новым языком, как дополнение к переводам меню и сообщений, могут быть установлены различные компоненты поддержки языка, такие как словари для проверки правописания, шрифты и методы ввода.</p>

  <note>
    <p>Некоторые переводы могут оказаться неполными, а отдельные приложения могут совсем не поддерживать ваш язык.</p>
  </note>

</page>
