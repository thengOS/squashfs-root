<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="session-screenlocks" xml:lang="ru">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <revision pkgversion="3.8" version="0.3" date="2013-03-09" status="candidate"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <desc>Измените время ожидания перед блокированием экрана в настройках <gui>Яркость и блокировка</gui>.</desc>
    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Экран блокируется слишком быстро</title>

  <p>Если отойти от компьютера на несколько минут, экран автоматически блокируется, поэтому потребуется ввести пароль, чтобы начать им пользоваться снова. Это сделано по соображениям безопасности (чтобы никто не смог вмешаться в вашу работу, если оставить компьютер без присмотра), но это может и досаждать, если экран блокируется слишком быстро.</p>

  <p>Для увеличения времени ожидания перед автоматическим блокированием экрана:</p>

  <steps>
    <item><p>Щёлкните самый правый значок на <gui>панели меню</gui> и выберите <gui>Параметры системы</gui>.</p></item>
    <item><p>Щёлкните <gui>Яркость и блокировка</gui>.</p></item>
    <item><p>Измените значение из выпадающего списка <gui>Блокировать экран через</gui>.</p></item>
  </steps>

<note style="tip">
 <p>Если автоматическое блокирование экрана больше не потребуется, выключите переключатель <gui>Заблокировать</gui>.</p>
</note>

</page>
