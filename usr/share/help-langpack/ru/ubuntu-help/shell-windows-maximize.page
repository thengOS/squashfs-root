<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-windows-maximize" xml:lang="ru">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <desc>Дважды щёлкните или перетащите заголовок для развёртывания или восстановления размера окна.</desc>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Развёртывание во весь экран и восстановление размера окна</title>

  <p>Окно можно развернуть, чтобы занять всё пространство на рабочем столе, а после восстановить его до нормального размера. Чтобы одновременно просматривать два окна, можно развернуть окна вертикально вдоль левой и правой стороны экрана. Для дополнительной информации см. <link xref="shell-windows-tiled"/>.</p>

  <p>Чтобы развернуть окно во весь экран, захватите заголовок и перетащите его к верху экрана, или дважды щёлкните заголовок. Чтобы развернуть окно с помощью клавиатуры, удерживайте клавиши <key>Ctrl</key> и <link xref="windows-key">Super</link> и нажмите <key>↑</key>.</p>

  <p>Для восстановления первоначального размера окна, оттащите его от краёв экрана. Если окно развёрнуто во весь экран, можно дважды щёлкнуть заголовок для восстановления его размера. Кроме того, можно воспользоваться комбинацией клавиш <keyseq><key>Ctrl</key> <key><link xref="windows-key">Super</link></key> <key>↓</key></keyseq>.</p>

  <note style="tip">
    <p>Для перемещения окна удерживайте нажатой клавишу <key>Alt</key> и перетаскивайте его за любое место в окне.</p>
  </note>
</page>
