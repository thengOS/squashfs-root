<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="session-language" xml:lang="ru">

  <info>
    <link type="guide" xref="prefs-language#langsupport"/>
    <desc>Переключите пользовательский интерфейс и справку на другой язык.</desc>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision version="14.04" date="2014-01-12" status="review"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Команда документации Ubuntu</name>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Изменение используемого языка</title>

  <p>Можно пользоваться любым из множества языков для вашего рабочего стола и приложений, если на вашем компьютере <link xref="prefs-language-install">установлены соответствующие языковые пакеты</link>.</p>

  <steps>
    <item><p>Щёлкните самый правый значок на панели меню и выберите <gui>Параметры системы</gui>.</p></item>
    <item><p>Щёлкните <gui>Язык системы</gui>.</p></item>
    <item><p>Выберите предпочитаемый язык на вкладке <gui>Языки</gui>. Переместите выбранный язык в самый верх списка.</p></item>
    <item><p>Чтобы изменения вступили в силу, нужно выйти из системы и вновь в неё войти. Щёлкните самый правый значок на панели меню и выберите <gui>Завершение сеанса</gui> для выхода из системы.</p></item>
  </steps>

  <note>
    <p>Некоторые переводы могут оказаться неполными, а отдельные приложения могут совсем не поддерживать ваш язык.</p>
  </note>

  <p>В вашей домашней папке есть несколько специальных папок, в которых приложения могут сохранять музыку, изображения, документы и прочее. Эти папки используют стандартные имена, зависящие от вашего языка. При следующем входе в систему будет предложено переименовать эти папки на стандартные названия выбранного вами языка. Если вы планируете использовать новый язык постоянно, необходимо обновить названия папок.</p>

  <section id="system">
    <title>Изменение языка системы</title>

    <p>При изменении языка происходит его изменение только для вашей учётной записи после вашего входа в систему. Можно также изменить и <em>язык системы</em> — язык, используемый в таких местах, как экран входа.</p>

    <steps>
      <item><p>Измените язык, как описано выше.</p></item>
      <item><p>Щёлкните <gui>Применить для всей системы</gui>.</p></item>
      <item><p>Потребуются <link xref="user-admin-explain">административные привилегии</link>. Введите ваш пароль или пароль запрашиваемой учётной записи администратора.</p></item>
    </steps>

  <!-- The following line must not be broken into two lines. Post processing of the compiled HTML needs to remove it -->
  <note><p>Более подробное руководство по языкам и региональным форматам можно найти в <link href="help:language-selector">Справке по языковой поддержке</link>.</p></note>

  </section>

</page>
