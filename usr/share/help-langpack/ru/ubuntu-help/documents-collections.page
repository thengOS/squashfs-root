<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="documents-collections" xml:lang="ru">

  <info>
    <credit type="author copyright">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <desc>Соберите взаимосвязанные документы в коллекцию.</desc>
    <link type="guide" xref="documents#print"/>
    <revision pkgversion="3.5.90" date="2012-09-05" status="review"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Создание коллекции документов</title>

  <p><app>Документы</app> позволяют собирать документы разных типов в одном месте, называемом <em>коллекцией</em>. Если у вас есть связанные между собой документы, то для лёгкого поиска их можно сгруппировать. Например, в командировке вы делали презентацию — ваши слайды, ваша полётная маршрут-квитанция (PDF-файл), ваш авансовый отчёт, а также другие комбинированные PDF/ODF документы могут быть сгруппированы в одной коллекции.</p>

  <p>Чтобы создать коллекцию или добавить в неё:</p>
  <list>
   <item><p>Щёлкните кнопку <gui>✓</gui>.</p></item>
   <item><p>В режиме выбора отметьте документы, которые нужно собрать.</p></item>
   <item><p>Щёлкните кнопку <gui>+</gui> на панели кнопок.</p></item>
   <item><p>В списке коллекций щёлкните <gui>Добавить</gui> и наберите имя новой коллекции или выберите существующую коллекцию. Отмеченные документы будут добавлены в коллекцию.</p></item>
  </list>

  <note>
   <p>Коллекции не подчиняются правилам для папок и их иерархии — <em>нельзя разместить коллекции внутри других коллекций.</em></p>
  </note>

  <p>Чтобы удалить коллекцию:</p>
  <list>
   <item><p>Щёлкните кнопку <gui>✓</gui>.</p></item>
   <item><p>В режиме выбора отметьте коллекцию, которую нужно удалить.</p></item>
   <item><p>Щёлкните кнопку Корзины на панели кнопок. Коллекция будет удалена, исходные документы останутся.</p></item>
  </list>

</page>
