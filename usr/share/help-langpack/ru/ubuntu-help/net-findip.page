<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-findip" xml:lang="ru">
  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Знание своего IP-адреса может помочь вам в решении проблем с сетью.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Определение своего IP-адреса</title>

  <p>Знание своего IP-адреса может помочь в решении проблем с подключением к Интернету. Возможно, вы удивитесь, узнав, что у вас <em>два</em> IP-адреса: IP-адрес вашего компьютера в локальной сети и IP-адрес вашего компьютера в Интернете.</p>

  <steps>
    <title>Определение внутреннего (сетевого) IP-адреса</title>
    <item><p>Щёлкните самый правый значок на <gui>панели меню</gui> и выберите <gui>Параметры системы</gui>.</p></item>
    <item><p>Откройте <gui>Сеть</gui> и выберите из списка слева <gui>Проводное</gui> или <gui>Беспроводное</gui>, в зависимости от того, IP-адрес какого сетевого соединения нужно узнать.</p></item>
    <item><p>Ваш внутренний IP-адрес будет отображён в списке информации.</p></item>
  </steps>

  <steps>
  	<title>Определение внешнего IP-адреса (адреса в Интернете)</title>
    <item><p>Посетите сайт <link href="http://whatismyipaddress.com/">whatismyipaddress.com</link>.</p></item>
    <item><p>Этот сайт покажет ваш внешний IP-адрес.</p></item>
  </steps>

<p>Эти адреса могут совпадать. Всё зависит от того, как ваш компьютер подключён к Интернету.</p>

</page>
