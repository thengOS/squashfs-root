<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="unity-launcher-menu" xml:lang="ru">
  <info>
    <link type="guide" xref="unity-launcher-intro#launcher-using"/>

    <revision version="14.10" date="2014-09-14" status="review"/>

    <credit type="author">
      <name>Stephen M. Webb</name>
      <email>stephen@ubuntu.com</email>
    </credit>

    <desc>Щелчок правой кнопкой на значке панели запуска раскроет меню действий.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Меню значков панели запуска</title>

  <p>Щёлчок правой кнопкой на значке в панели запуска покажет меню действий. Доступные действия зависят от того, что это за значок, закреплён ли он на панели и запущено ли в данный момент это приложение. Меню может содержать следующие действия.</p>
  <list>
    <item><p>запуск приложений или открытие документа, папки или устройства</p></item>
    <item><p>удаление значка из панели запуска, если до этого он был прикреплён (см. <link xref="shell-apps-favorites"/>)</p></item>
    <item><p>прикрепление значка к панели запуска, если до этого он не был прикреплён (см. <link xref="shell-apps-favorites"/>)</p></item>
    <item><p>закрытие приложения, если оно уже запущено</p></item>
    <item><p>переключение между копиями или окнами приложения, если открыто более одного окна или копии</p></item>
    <item><p>действия, характерные для приложения, например открытие нового документа или окна</p></item>
  </list>

</page>
