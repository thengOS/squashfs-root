<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="app-colors" xml:lang="ru">

  <info>
    <revision version="0.1" date="2013-03-02" status="candidate"/>
    <link type="guide" xref="index#appearance"/>
    <link type="guide" xref="profile"/>

    <credit type="author copyright">
      <name>С. Синдху (Sindhu S)</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Изменить цвет и фон.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Сергей В. Миронов</mal:name>
      <mal:email>sergo@bk.ru</mal:email>
      <mal:years>2007.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Цветовые схемы</title>

  <p>Если не нравится исходная цветовая схема <app>Терминала</app>, есть возможность изменить цвета, используемые для текста и фона. Можно использовать цвета из собственной темы, выбрать из предустановленных цветов или использовать пользовательскую схему.</p>

  <section id="system-theme">
    <title>Цвета системной темы</title>

    <p>Чтобы использовать цвета системной темы:</p>

    <steps>
      <item>
        <p>Выберите <guiseq><gui style="menu">Правка</gui> <gui style="menuitem">Параметры профиля</gui> <gui style="tab">Цвета</gui></guiseq>.</p>
      </item>
      <item>
        <p>Отметьте пункт <gui style="checkbox">Использовать цвета из системной темы</gui>. Изменения будут автоматически применены.</p>
      </item>
    </steps>

  </section>

  <section id="built-in-scheme">
    <title>Встроенные схемы</title>

    <p>Можно выбрать одну из встроенных цветовых схем: <gui>чёрный на светло-жёлтом</gui>, <gui>чёрный на белом</gui>, <gui>серый на чёрном</gui>, <gui>зелёный на чёрном</gui> или <gui>белый на чёрном</gui>, <gui>светлая соляризация</gui>, <gui>тёмная соляризация</gui>. Чтобы установить одну из встроенных цветовых схем:</p>

    <steps>
      <item>
        <p>Выберите <guiseq><gui style="menu">Правка</gui> <gui style="menuitem">Параметры профиля</gui> <gui style="tab">Цвета</gui></guiseq>.</p>
      </item>
      <item>
        <p>Убедитесь, что пункт <gui style="checkbox">Использовать цвета из системной темы</gui> не отмечен. Выберите желаемую <gui>Встроенную схему</gui>. Настройки цветовой схемы будут автоматически применены.</p>
        <note>
	  <p>Приложения могут предпочесть использовать цветовую схему из палитры, а не указанный цвет для полужирного текста.</p>
        </note>
      </item>
    </steps>

  </section>

  <section id="custom-scheme">
    <title>Пользовательская цветовая схема</title>

    <p>В <app>Терминале</app> можно использовать свои цвета для текста и фона:</p>

    <steps>
      <item>
        <p>Выберите <guiseq><gui style="menu">Правка</gui> <gui style="menuitem">Параметры профиля</gui> <gui style="tab">Цвета</gui></guiseq>.</p>
      </item>
      <item>
        <p>Убедитесь, что пункт <gui style="checkbox">Использовать цвета из системной темы</gui> не отмечен. Из выпадающего списка <gui>Встроенные схемы</gui> выберите пункт <gui>другая</gui>.</p>
      </item>
      <item>
        <p>Нажмите на образец цвета рядом с компонентом, цвет которого нужно изменить.</p>
      </item>
      <item>
        <p>Укажите желаемый цвет образца и нажмите <gui style="button">Выбрать</gui>.</p>
        <p>Для выбора цвета из палитры нажмите на <gui style="button">+</gui>. Выбрать нужный цвет можно несколькими способами:</p>
        <list>
          <item>
            <p>Ввести шестнадцатеричный код нужного цвета в поле ввода.</p>
          </item>
          <item>
            <p>Перетащить бегунок слева, чтобы настроить цвета, и затем нажать на цвет в области выбора цвета.</p>
          </item>
        </list>
	<p>Изменения будут сохранены автоматически.</p>
      </item>
    </steps>

    <note style="tip">
      <p>При выборе встроенной схемы изменить цвет также можно, нажав на прямоугольник с образцом цвета. Как только выбор сделан, пункт меню встроенной схемы изменится на <gui>другая</gui>.</p>
    </note>

  </section>

</page>
