<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="pref-keyboard-access" xml:lang="ru">

  <info>
    <revision version="0.1" date="2013-02-25" status="candidate"/>
    <link type="guide" xref="index#preferences"/>
    <link type="guide" xref="pref"/>
    <link type="seealso" xref="pref-menubar"/>

    <credit type="author copyright">
      <name>С. Синдху (Sindhu S)</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Навигация по меню <app>Терминала</app> с помощью клавиш.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Сергей В. Миронов</mal:name>
      <mal:email>sergo@bk.ru</mal:email>
      <mal:years>2007.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Специальные возможности клавиатуры</title>

  <note style="important">
    <p>Эти клавиши будут работать только в окнах с включённой панелью меню.</p>
  </note>

  <p>По меню <app>Терминала</app> можно передвигаться с помощью комбинации клавиш, которые называются <em>мнемоническими</em>, или такими особыми клавишами, переносящими напрямую в главное меню в панели меню, которые называются <em>быстрыми клавишами доступа к меню</em>.</p>

  <section id="mnemonics">
    <title>Мнемонические клавиши</title>

    <p>В меню <app>Терминала</app> можно попасть с помощью комбинации клавиши <key>Alt</key> и буквы пункта меню. Эта буква меню будет подчёркнута, если нажать клавишу <key>Alt</key>.</p>

    <p>Просмотрим, например, меню <gui style="menu">Правка</gui> с помощью клавиш <keyseq><key>Alt</key><key>П</key></keyseq>. Точно также можно получить доступ к меню <gui style="menu">Файл</gui> с помощью <keyseq><key>Alt</key><key>Ф</key></keyseq>.</p>

    <p>Чтобы активировать мнемонические клавиши:</p>

    <steps>
      <item>
        <p>Выберите <guiseq><gui style="menu">Правка</gui> <gui style="menuitem">Параметры</gui> <gui style="tab">Общие</gui></guiseq>.</p>
      </item>
      <item>
        <p>Отметьте пункт <gui style="checkbox">Включить клавишные ускорители</gui>.</p>
      </item>
    </steps>

  </section>

  <section id="menu-accelerator">
    <title>Быстрая клавиша доступа к меню</title>

    <p>Эта настройка активирует запуск меню <gui style="menu">Файл</gui> по нажатию быстрой клавиши доступа к меню. По умолчанию это обычно клавиша <key>F10</key>.</p>

    <steps>
      <item>
        <p>Выберите <guiseq><gui style="menu">Правка</gui> <gui style="menuitem">Параметры</gui> <gui style="tab">Комбинации клавиш</gui></guiseq>.</p>
      </item>
      <item>
        <p>Отметьте пункт <gui style="checkbox">Включить быструю клавишу доступа к меню</gui>.</p>
      </item>
    </steps>

  </section>

</page>
