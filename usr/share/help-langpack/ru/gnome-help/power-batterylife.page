<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="power-batterylife" xml:lang="ru">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="shell-exit#suspend"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <link type="seealso" xref="display-brightness"/>
    <link type="seealso" xref="power-whydim"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Советы по уменьшению энергопотребления компьютера</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Уменьшение потребляемой мощности и увеличение времени работы от аккумулятора</title>

  <p>Компьютеры могут потреблять значительное количество электроэнергии. С помощью некоторых простых энергосберегающих стратегий можно уменьшить счёт за электроэнергию и помочь в сохранении окружающей среды.</p>

<section id="general">
  <title>Общие советы</title>

<list>
  <item>
    <p><link xref="shell-exit#suspend">Переводите компьютер в ждущий режим</link>, когда не пользуетесь им. Это значительно уменьшает энергопотребление и позволяет быстро вернуть его в рабочий режим.</p>
  </item>
  <item>
    <p><link xref="shell-exit#shutdown">Выключайте</link> компьютер, если не собираетесь им пользоваться в ближайшее время. Некоторые люди беспокоятся, что при регулярном выключении компьютера его аппаратные компоненты быстрее изнашиваются, но в данном случае это не так.</p>
  </item>
  <item>
    <p>Use the <gui>Power</gui> panel in <app>Settings</app> to change your
    power settings. There are a number of options that will help to save power:
    you can <link xref="power-whydim">automatically dim the screen</link> after
    a certain time, reduce the <link xref="display-brightness">screen
    brightness</link>, and have the computer
    <link xref="power-suspend">automatically suspend</link> if you have not used
    it for a certain period of time.</p>
  </item>
  <item>
    <p>Отключайте все внешние устройства (типа принтеров и сканеров), когда не пользуетесь ими.</p>
  </item>
</list>

</section>

<section id="laptop">
  <title>Ноутбуки, нетбуки и другие устройства с аккумуляторами</title>

 <list>
   <item>
     <p>Reduce the <link xref="display-brightness">screen
     brightness</link>. Powering the screen accounts for a significant fraction
     of a laptop power consumption.</p>
     <p>На клавиатурах большинства ноутбуков есть кнопки (или комбинации клавиш) для уменьшения яркости.</p>
   </item>
   <item>
     <p>If you do not need an Internet connection for a little while, turn off
     the wireless or Bluetooth cards. These devices work by broadcasting radio
     waves, which takes quite a bit of power.</p>
     <p>На некоторых компьютерах для их отключения есть специальный переключатель, на других можно использовать для этого комбинацию клавиш. Когда в устройстве возникнет необходимость, его можно будет снова включить.</p>
   </item>
 </list>

</section>

<section id="advanced">
  <title>Дополнительные советы</title>

 <list>
   <item>
     <p>Уменьшите число выполняемых в фоновом режиме задач. Компьютеры потребляют больше энергии, когда больше загружены работой.</p>
     <p>Most of your running applications do very little when you are not
     actively using them. However, applications that frequently grab data from
     the internet or play music or movies can impact your power consumption.</p>
   </item>
 </list>

</section>

</page>
