<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="power-nowireless" xml:lang="ru">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>У некоторых беспроводных устройств возникают проблемы с обработкой состояния компьютера, если компьютер входил в спящий режим и потом вышел из спящего режима некорректным образом.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>При «пробуждении» компьютера отсутствует беспроводное соединение</title>

  <p>If you have suspended your computer, you may find that your wireless
  internet connection does not work when you resume it again. This happens when
  the <link xref="hardware-driver">driver</link> for the wireless device does
  not fully support certain power saving features.
  Typically, the wireless connection fails to turn on properly when the
  computer is resumed.</p>

  <p>Если это случится, попробуйте выключить и снова включить беспроводное соединение:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Сеть</gui> чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Выберите <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Switch the wireless <gui>OFF</gui> and then <gui>ON</gui> again.</p>
    </item>
    <item>
      <p>If the wireless still does not work, switch <gui>ON</gui> the
      <gui>Airplane Mode</gui> and then switch it <gui>OFF</gui> again.</p>
    </item>
  </steps>

  <p>Если ничего не помогло, беспроводная сеть должна снова заработать после перезагрузки компьютера. Если и после перезагрузки проблема не исчезла, подключитесь к интернету с помощью проводного соединения и обновите систему.</p>

</page>
