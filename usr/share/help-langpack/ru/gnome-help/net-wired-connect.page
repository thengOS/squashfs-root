<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wired-connect" xml:lang="ru">

  <info>
    <link type="guide" xref="net-wired" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Всё, что нужно для настройки большинства проводных сетевых соединений — это подключить сетевой кабель.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Подключение к проводной (Ethernet) сети</title>

  <!-- TODO: create icon manually because it is one overlayed on top of another
       in real life. -->
  <p>To set up most wired network connections, all you need to do is plug in a
  network cable. The wired network icon
  (<media its:translate="no" type="image" src="figures/network-wired-symbolic.svg"><span its:translate="yes">settings</span></media>) is displayed
  on the top bar with three dots while the connection is being established. The
  dots disappear when you are connected.</p>

  <p>Если этого не произошло, в первую очередь проверьте, подключён ли сетевой кабель. Один его конец должен быть включен в прямоугольный порт Ethernet на компьютере, а второй должен быть подключён к сетевому коммутатору, маршрутизатору, настенной сетевой розетке и т.п. (в зависимости от устройства вашей сети). Иногда световой индикатор рядом с портом Ethernet указывает, что он подключён и активен.</p>

  <note>
    <p>Нельзя просто подключить один компьютер к другому сетевым кабелем (по крайней мере, без некоторых дополнительных настроек). Чтобы соединить два компьютера, нужно подключить каждый из них к сетевому концентратору, маршрутизатору или коммутатору.</p>
  </note>

  <p>Если соединение всё ещё не установлено, возможно, ваша сеть не поддерживает автоматическую настройку (DHCP). В таком случае, придётся <link xref="net-manual">настроить её вручную</link>.</p>

</page>
