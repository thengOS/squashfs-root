<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="guide" style="task" version="1.0 if/1.0" id="bluetooth-send-file" xml:lang="ru">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="files-share"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Джим Кэмпбелл (Jim Campbell)</name>
      <email its:translate="no">jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email its:translate="no">stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Дэвид Кинг (David King)</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Обмен файлами с устройствами Bluetooth, такими как телефон.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Отправка файлов на устройство Bluetooth</title>

<if:choose>
  <if:when test="platform:unity">
    <p>Можно отправлять файлы на устройства Bluetooth, такие, как мобильные телефоны или другие компьютеры. Некоторые типы устройств не позволяют передавать файлы или определённые типы файлов. Отправлять файлы можно одним из трёх способов: с помощью значка Bluetooth в верхней панели, из окна параметров Bluetooth или непосредственно из менеджера файлов.</p>
  </if:when>
  <p>You can send files to connected Bluetooth devices, such as some mobile
  phones or other computers. Some types of devices do not allow the transfer
  of files, or specific types of files. You can send files using the Bluetooth
  settings window.</p>
</if:choose>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Bluetooth</gui> to open the panel.</p>
    </item>
    <item>
      <p>Make sure Bluetooth is enabled: the switch in the titlebar should be
      set to <gui>ON</gui>.</p>
    </item>
    <item>
      <p>In the <gui>Devices</gui> list, select the device to which to send the
      files. If the desired device is not shown as <gui>Connected</gui> in the
      list, you need to <link xref="bluetooth-connect-device">connect</link>
      to it.</p>
      <p>A panel specific to the external device appears.</p>
    </item>
    <item>
      <p>Click <gui>Send Files…</gui> and the file chooser will appear.</p>
    </item>
    <item>
      <p>Выберите файл для отправки и нажмите <gui>Выбрать</gui>.</p>
      <p>Чтобы отправить несколько файлов из папки, удерживайте нажатой <key>Ctrl</key>, выбирая каждый файл.</p>
    </item>
    <item>
      <p>The owner of the receiving device usually has to press a button to
      accept the file. The <gui>Bluetooth File Transfer</gui> dialog will show
      the progress bar. Click <gui>Close</gui> when the transfer is
      complete.</p>
    </item>
  </steps>

</page>
