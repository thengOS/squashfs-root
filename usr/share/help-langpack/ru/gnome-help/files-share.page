<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-share" xml:lang="ru">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="nautilus-connect"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-11" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Простая отправка файлов контактам в почту из менеджера файлов.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Обмен файлами по почте</title>

<p>Файлами можно легко обмениваться, посылая их по почте непосредственно из файлового менеджера.</p>

  <note style="important">
    <p>Before you begin, make sure <app>Evolution</app> or <app>Geary</app> is
    installed on your computer, and your email account is configured.</p>
  </note>

<steps>
  <title>Чтобы поделиться файлом по почте:</title>
    <item>
      <p>Open the <app>Files</app> application from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
  <item><p>Найдите файл, который нужно отправить.</p></item>
    <item>
      <p>Right-click the file and select <gui>Send to…</gui>. An email compose
      window will appear with the file attached.</p>
    </item>
  <item><p>Нажмите <gui>Кому</gui> чтобы выбрать адресата, или введите почтовый адрес, на который нужно послать файл. Обязательно заполните поле <gui>Тема</gui> и поле сообщения, затем нажмите <gui>Отправить</gui>.</p></item>
</steps>

<note style="tip">
  <p>You can send multiple files at once. Select multiple files by holding
  down <key>Ctrl</key> while clicking the files, then right-click any selected
  file.</p>
</note>

</page>
