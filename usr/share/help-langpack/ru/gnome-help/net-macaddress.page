<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="net-macaddress" xml:lang="ru">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Джим Кэмпбелл (Jim Campbell)</name>
      <email its:translate="no">jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Уникальный код, присвоенный сетевому устройству.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Что такое MAC-адрес?</title>

  <p><em>MAC-адрес</em> — это уникальный код, присвоенный производителем сетевому устройству (например, беспроводному сетевому адаптеру или ethernet-адаптеру). MAC — это сокращение от <em>Media Access Control</em>. Предполагается, что каждый код является уникальным для определённого устройства.</p>

  <p>MAC-адрес состоит из шести групп по два символа, разделённых двоеточиями, например, <code>00:1B:44:11:3A:B7</code>.</p>

  <p>Чтобы определить MAC-адрес сетевого устройства:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Сеть</gui> чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Choose which device, <gui>Wi-Fi</gui> or <gui>Wired</gui>, from
      the left pane.</p>
      <p>The MAC address for the wired device will be displayed as the
      <gui>Hardware Address</gui> on the right.</p>
      
      <p>Click the
      <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">settings</span></media>
      button to see the MAC address for the wireless device displayed as the
      <gui>Hardware Address</gui> in the <gui>Details</gui> panel.</p>
    </item>
  </steps>

  <p>In practice, you may need to modify or "spoof" a MAC address. For example,
  some internet service providers may require that a specific MAC address be
  used to access their service. If the network card stops working, and you need
  to swap a new card in, the service won't work anymore. In such cases, you
  would need to spoof the MAC address.</p>

</page>
