<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="files-search" xml:lang="ru">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Дэвид Кинг (David King)</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Locate files based on file name and type.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Поиск файлов</title>

  <p>You can search for files based on their name or file type directly
  within the file manager.</p>

  <links type="topic" style="linklist">
    <title>Другие приложения для поиска</title>
    <!-- This is an extension point where search apps can add
    their own topics. It's empty by default. -->
  </links>

  <steps>
    <title>Поиск</title>
    <item>
      <p>Open the <app>Files</app> application from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
    <item>
      <p>Если известно, что файл находится в определённой папке, перейдите в эту папку.</p>
    </item>
    <item>
      <p>Type a word or words that you know appear in the file name, and they
      will be shown in the search bar. For example, if you name all your
      invoices with the word "Invoice", type <input>invoice</input>. Words are
      matched regardless of case.</p>
      <note>
        <p>Instead of typing words directly to bring up the search bar, you can
	click the magnifying glass in the toolbar, or press
        <keyseq><key>Ctrl</key><key>F</key></keyseq>.</p>
      </note>
    </item>
    <item>
      <p>Можно сузить результаты поиска, указав местоположение и тип файла.</p>
      <list>
        <item>
          <p>Нажмите <gui>Домашняя папка</gui> чтобы ограничить результаты поиска только вашим <file>Домашним</file> каталогом, или же нажмите <gui>Все файлы</gui> для поиска по всей файловой системе.</p>
        </item>
        <item>
          <p>Click the <gui>+</gui> button and pick a <gui>File Type</gui> from
	  the drop-down list to narrow the search results based on file type.
	  Click the <gui>x</gui> button to remove this option and widen the
	  search results.</p>
        </item>
      </list>
    </item>
    <item>
      <p>Файлы из результатов поиска можно открывать, копировать, удалять и выполнять с ними другие действия, как если бы это были файлы из любой другой папки в менеджере файлов.</p>
    </item>
    <item>
      <p>Снова нажмите на значок с лупой на панели инструментов, чтобы выйти из режима поиска и вернуться к содержимому папки.</p>
    </item>
  </steps>

</page>
