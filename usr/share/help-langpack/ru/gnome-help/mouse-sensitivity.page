<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-sensitivity" xml:lang="ru">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>
    <credit>
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Шон МакКенс (Shaun McCance)</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Изменение скорости перемещения указателя для мыши или сенсорной панели.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Настройка скорости мыши и сенсорной панели</title>

  <p>Если при перемещении мыши или использовании сенсорной панели указатель перемещается слишком быстро или медленно, можно настроить скорость указателя для этих устройств.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Mouse &amp; Touchpad</gui>.</p>
    </item>
    <item>
      <p>Нажмите на <gui>Мышь и сенсорная панель</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>С помощью ползунка <gui>Скорость указателя</gui> настройте комфортную для вас скорость перемещения курсора. Для каждого устройства указателя, такого, как мышь или сенсорная панель, имеется ползунок.</p>
    </item>
  </steps>

  <p>You can set different pointer speed for each type of device. For example,
  you can have one scroll speed for a laptop touchpad and another for a mouse.
  Sometimes the most comfortable settings for one type of device are not the
  best for another.</p>

  <note>
    <p>Раздел <gui>Сенсорная панель</gui> появится только если в вашей системе есть сенсорная панель. Раздел <gui>Мышь</gui> виден, только если ваша мышь подключена.</p>
  </note>

</page>
