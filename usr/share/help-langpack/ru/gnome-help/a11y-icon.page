<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-icon" xml:lang="ru">

  <info>
    <link type="guide" xref="a11y"/>

    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>

    <desc>Меню универсального доступа — это значок в верхней панели в виде человечка.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Найдите меню универсального доступа</title>

  <p><em>Меню универсального доступа</em> позволяет включать и отключать различные параметры специальных возможностей. Чтобы открыть это меню, нажмите на значок в виде круга с силуэтом человечка внутри, находящийся в верхней панели.</p>

  <figure>
    <desc>Меню специальных возможностей находится в верхней панели.</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/universal-access-menu.png"/>
  </figure>

  <p>Если вы не видите меню универсального доступа, то его можно включить на панели настроек <gui>Универсальный доступ</gui>:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Universal Access</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Универсальный доступ</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Измените переключатель <gui>Всегда показывать меню универсального доступа</gui> в положение <gui>Включено</gui>.</p>
    </item>
  </steps>

  <p>Чтобы получить доступ к этому меню с помощью клавиатуры, нажмите <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> для перемещения фокуса клавиатуры на верхнюю панель. Под кнопкой <gui>Обзор</gui> появится белая полоса, показывающая, какой элемент верхней панели выбран. С помощью клавиш со стрелками переместите белую полосу под меню специальных возможностей и нажмите <key>Enter</key>, чтобы открыть меню. Для выбора пунктов меню используйте клавиши со стрелками вверх и вниз. Чтобы переключить выбранный пункт меню, нажмите <key>Enter</key>.</p>

</page>
