<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-movewindow" xml:lang="ru">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.8" version="0.3" date="2013-05-10" status="review"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Зайдите в режим <gui>Обзора</gui> и перетащите окно на другое рабочее место.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Перемещение окна на другое рабочее место</title>

  <steps>
    <title>С помощью мыши:</title>
    <item>
      <p if:test="!platform:gnome-classic">Open the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
      <p if:test="platform:gnome-classic">Open the
      <gui xref="shell-introduction#activities">Activities Overview</gui> from the
      <gui xref="shell-introduction#activities">Applications</gui> menu
      at the top left of the screen.</p>
    </item>
    <item>
      <p>Нажмите на окно и тащите его к правой стороне экрана.</p>
    </item>
    <item>
      <p>The <em xref="shell-workspaces">workspace selector</em> will
      appear.</p>
    </item>
    <item>
      <p if:test="!platform:gnome-classic">Перетащите окно на пустое рабочее место. Теперь на этом рабочем месте будет размещаться окно, которое вы перетащили, а внизу <em>переключателя рабочих мест</em> появится новое пустое рабочее место.</p>
      <p if:test="platform:gnome-classic">Перетащите окно на пустое рабочее место. Теперь на этом рабочем месте будет размещаться окно, которое вы перетащили.</p>
    </item>
  </steps>

  <steps>
    <title>С помощью клавиатуры:</title>
    <item>
      <p>Select the window that you want to move (for example, using the
      <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq>
      <em xref="shell-windows-switching">window switcher</em>).</p>
    </item>
    <item>
      <p>Нажмите <keyseq><key>Super</key><key>Shift</key><key>Page Up</key></keyseq> для перехода на рабочее место выше текущего в <em>переключателе рабочих мест</em>.</p>
      <p>Нажмите <keyseq><key>Super</key><key>Shift</key><key>Page Down</key></keyseq> для перехода на рабочее место ниже текущего в <em>переключателе рабочих мест</em>.</p>
    </item>
  </steps>

</page>
