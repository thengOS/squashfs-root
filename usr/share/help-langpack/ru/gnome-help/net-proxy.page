<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-proxy" xml:lang="ru">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Батист Милле-Матиас (Baptiste Mille-Mathias)</name>
      <email its:translate="no">baptistem@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Прокси — это посредник для веб-трафика, с его помощью можно анонимно использовать веб-службы, с целью контроля или для обеспечения безопасности.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Настройка прокси-сервера</title>

<section id="what">
  <title>Что такое прокси?</title>

  <p>A <em>web proxy</em> filters websites that you look at, it receives
  requests from your web browser to fetch web pages and their elements, and
  following a policy will decide to pass them you back. They are commonly used
  in businesses and at public wireless hotspots to control what websites you
  can look at, prevent you from accessing the internet without logging in, or
  to do security checks on websites.</p>

</section>

<section id="change">
  <title>Изменение режима прокси-сервера</title>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Сеть</gui> чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Выберите <gui>Прокси-сервер</gui> в списке слева.</p>
    </item>
    <item>
      <p>Выберите нужный режим прокси-сервера из указанных:</p>
      <terms>
        <item>
          <title>Нет</title>
          <p>Для получения web-содержимого, приложения будут использовать прямое подключение.</p>
        </item>
        <item>
          <title>Вручную</title>
          <p>Для проксирования каждого протокола необходимо определить адрес прокси и порт для протоколов. Это протоколы: <gui>HTTP</gui>, <gui>HTTPS</gui>, <gui>FTP</gui> и <gui>SOCKS</gui>.</p>
        </item>
        <item>
          <title>Автоматически</title>
          <p>Адрес URL указывает на ресурс, содержащий подходящую для вашей системы конфигурацию.</p>
        </item>
      </terms>
    </item>
  </steps>

  <p>Приложения, использующие сетевое соединение, будут использовать указанные параметры прокси.</p>

</section>

</page>
