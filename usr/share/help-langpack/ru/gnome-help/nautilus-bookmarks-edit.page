<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="ru">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Дэвид Кинг (David King)</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Добавление, удаление и переименование закладок в менеджере файлов.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Редактирование закладок</title>

  <p>Сделанные вами закладки отображаются в боковой панели менеджера файлов.</p>

  <steps>
    <title>Добавление закладки:</title>
    <item>
      <p>Откройте папку (или место), для которой нужно добавить закладку.</p>
    </item>
    <item>
      <p>Click the window menu in the toolbar and pick <gui>Bookmark this
      Location</gui>.</p>
    </item>
  </steps>

  <steps>
    <title>Удаление закладки:</title>
    <item>
      <p>Right-click on the bookmark in the sidebar and select
      <gui>Remove</gui> from the menu.</p>
    </item>
  </steps>

  <steps>
    <title>Переименование закладки:</title>
    <item>
      <p>Right-click on the bookmark in the sidebar and select
      <gui>Rename…</gui>.</p>
    </item>
    <item>
      <p>Наберите новое названия закладки в текстовом поле <gui>Названия</gui>.</p>
      <note>
        <p>При переименовании закладки папка не переименовывается. Если создать две закладки на две разные папки с одинаковыми именами, названия закладок тоже будут одинаковыми и их нельзя будет отличить. В таком случае полезно дать одной из закладок название, отличающееся от имени папки, на которую она указывает.</p>
      </note>
    </item>
  </steps>

</page>
