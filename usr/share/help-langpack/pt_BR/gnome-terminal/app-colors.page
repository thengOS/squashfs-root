<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="app-colors" xml:lang="pt-BR">

  <info>
    <revision version="0.1" date="2013-03-02" status="candidate"/>
    <link type="guide" xref="index#appearance"/>
    <link type="guide" xref="profile"/>

    <credit type="author copyright">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Altere cores e plano de fundos.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luciana Bastos de Freitas Menezes</mal:name>
      <mal:email>lubfm@gnome.org</mal:email>
      <mal:years>2007.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Pedro Vaz de Mello de Medeiros</mal:name>
      <mal:email>pedrovmm@gmail.com</mal:email>
      <mal:years>2007.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leonardo Ferreira Fontenelle</mal:name>
      <mal:email>leonardof@gnome.org</mal:email>
      <mal:years>2008.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andrius da Costa Ribas</mal:name>
      <mal:email>andriusmao@gmail.com</mal:email>
      <mal:years>2008.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2009, 2013.</mal:years>
    </mal:credit>
  </info>

  <title>Esquemas de cores</title>

  <p>Se você não gostar do tema padrão do <app>Terminal</app>, você pode alterar as cores que são usadas para o texto e plano de fundo. Você pode usar cores do seu tema, selecione uma das predefinições ou uso um esquema personalizado.</p>

  <section id="system-theme">
    <title>Usando cores do tema do seu sistema</title>

    <p>Para usar cores do tema do sistema:</p>

    <steps>
      <item>
        <p>Selecione <guiseq><gui style="menu">Editar</gui> <gui style="menuitem">Preferências do perfil</gui> <gui style="tab">Cores</gui></guiseq>.</p>
      </item>
      <item>
        <p>Marque <gui style="checkbox">Usar cores do tema do sistema</gui>. As alterações vão ser aplicadas automaticamente.</p>
      </item>
    </steps>

  </section>

  <section id="built-in-scheme">
    <title>Esquemas embutidos</title>

    <p>You can choose one of the built-in color schemes: <gui>Black on light
    yellow</gui>, <gui>Black on white</gui>, <gui>Gray on black</gui>,
    <gui>Green on black</gui>, <gui>White on black</gui>, <gui>Solarized
    light</gui>, <gui>Solarized dark</gui>. To set any of the built-in
    schemes:</p>

    <steps>
      <item>
        <p>Selecione <guiseq><gui style="menu">Editar</gui> <gui style="menuitem">Preferências do perfil</gui> <gui style="tab">Cores</gui></guiseq>.</p>
      </item>
      <item>
        <p>Certifique-se de que <gui style="checkbox">Usar cores do tema do sistema</gui> está desmarcada. Escolha o esquema de cores de <gui>Esquemas embutidos</gui>. As escolhas de esquema de cor são aplicadas automaticamente.</p>
        <note>
	  <p>Aplicativos podem escolher usar uma cor da paleta em vez da cor especificada.</p>
        </note>
      </item>
    </steps>

  </section>

  <section id="custom-scheme">
    <title>Esquema de cor personalizada</title>

    <p>Você pode usar cores de personalizadas para texto e plano de fundo no <app>Terminal</app>:</p>

    <steps>
      <item>
        <p>Selecione <guiseq><gui style="menu">Editar</gui> <gui style="menuitem">Preferências do perfil</gui> <gui style="tab">Cores</gui></guiseq>.</p>
      </item>
      <item>
        <p>Certifique-se de que <gui style="checkbox">Usar cores do tema do sistema</gui> está desmarcado. Selecione <gui>Personalizado</gui> da lista suspensa <gui>Esquemas embutidos</gui>.</p>
      </item>
      <item>
        <p>Clique na amostra de cor próxima ao componente para o qual você gostaria de alterar.</p>
      </item>
      <item>
        <p>Escolha a cor desejada da amostra de cores e clique em <gui style="button">Selecionar</gui>.</p>
        <p>Se você deseja selecionar uma cor no seletor de cores, clique no <gui style="button">+</gui>. Você pode escolher a cor desejada nas seguintes formas:</p>
        <list>
          <item>
            <p>Digite o código hexadecimal da cor na caixa de entrada.</p>
          </item>
          <item>
            <p>Arraste o deslizador à esquerda para ajustar as cores e clique na cor desejada na área de seleção de cores.</p>
          </item>
        </list>
	<p>Suas alterações serão salvas automaticamente.</p>
      </item>
    </steps>

    <note style="tip">
      <p>Você também pode alterar uma cor quando você escolher um esquema embutido clicando no bloco de amostra de cores. Assim que tiver alterado, sua seleção no menu de esquemas embutidos vai ser alterado para <gui>Personalizado</gui>.</p>
    </note>

  </section>

</page>
