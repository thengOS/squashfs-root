<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-select" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="files#faq"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <desc>Pressione <keyseq><key>Ctrl</key><key>S</key></keyseq> para selecionar múltiplos arquivos que tenham nomes parecidos.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Selecione arquivos por um padrão</title>

  <p>Você pode selecionar arquivos em uma pasta usando um padrão no nome do arquivo. Pressione <keyseq><key>Ctrl</key><key>S</key></keyseq> para chamar a janela <gui>Selecionar ocorrências de padrão</gui>. Digite um padrão usando partes comuns dos nomes do arquivo e caracteres curinga. Existem dois tipos de caracteres curinga disponíveis:</p>

  <list style="compact">
    <item><p><file>*</file> corresponde a qualquer quantidade de quaisquer caracteres, até mesmo nenhum.</p></item>
    <item><p><file>?</file> corresponde a exatamente um caractere qualquer.</p></item>
  </list>

  <p>Por exemplo:</p>

  <list>
    <item><p>Se você tem um arquivo de texto OpenDocument, um arquivo PDF e uma imagem que tenham, todos eles, o mesmo nome base <file>Fatura</file>, selecione todos os três com o padrão</p>
    <example><p><file>Fatura.*</file></p></example></item>

    <item><p>Se você tem fotos que tenham nomes como <file>Férias-001.jpg</file>, <file>Férias-002.jpg</file>, <file>Férias-003.jpg</file>; selecione todas elas com o padrão</p>
    <example><p><file>Férias-???.jpg</file></p></example></item>

    <item><p>Se você tem fotos como no caso anterior, mas editou algumas delas e acrescentou <file>-editada</file> no final do nome de arquivo delas, selecione as fotos editadas com</p>
    <example><p><file>Férias-???-editada.jpg</file></p></example></item>
  </list>
</page>
