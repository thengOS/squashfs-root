<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="unity-dash-intro" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="index" group="unity-dash-intro"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <desc>O Painel é o botão no topo do Lançador.</desc>

    <revision version="13.10" date="2013-10-23" status="outdated"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Find applications, files, music, and more with the Dash</title>

  <media type="image" src="figures/unity-dash-sample.png" style="floatend floatright">
    <p>Pesquisa Unity</p>
  </media>

  <p>O <gui>Painel</gui> permite a você procurar por aplicativos, arquivos, músicas e vídeos, e mostra a você os itens usados recentemente. Se você já trabalhou em uma planilha ou editou uma imagem e esqueceu onde a salvou, você certamente achará esta característica do Painel útil.</p>

  <p>Para começar a usar o <gui>Painel</gui>, clique no ícone no topo do <link xref="unity-launcher-intro">Lançador</link>. Este ícone tem o logotipo do Ubuntu nele. Para rápido acesso, você pode apenas pressionar a tecla <key xref="windows-key">Super</key>.</p>

  <p>Para esconder o <gui>Painel</gui>, clique no ícone do topo de novo ou pressione <key>Super</key> ou <key>Esc</key>.</p>

<section id="dash-home">
  <title>Pesquise tudo no lente de Início do Painel</title>

  <p>The first thing you'll see when opening the Dash is the Dash Home. Without typing
    or clicking anything, the Dash Home will show you applications and files you've used recently.</p>
  <p>Somente uma linha de resultados será mostrada para cada tipo. Se houver mais resultados, você pode clicar em <gui>Ver mais resultados</gui> para vê-los.</p>

  <p>Para procurar, apenas inicie a digitação e os resultados da busca aparecerão automaticamente nas diferentes lentes instaladas.</p>

  <p>Clique em um resultado para abri-lo ou você pode pressionar <key>Enter</key> para abrir o primeiro item na lista.</p>

</section>

<section id="dash-lenses">
  <title>Lentes</title>

  <p>As lentes permitem que você focalize os resultados do Painel e excluem resultados de outras lentes.</p>

  <p>Você pode ver as lentes disponíveis na <gui>barra de lentes</gui>, a faixa mais escura na parte inferior do Painel.</p>

  <p>Para alternar para uma lente diferente, apenas clique no ícone apropriado ou pressione <keyseq><key>Ctrl</key><key>Tab</key></keyseq>.</p>

</section>

<section id="dash-filters">
  <title>Filtros</title>

  <p>Filtros permitem à você refinar ainda mais suas pesquisas.</p>

  <p>Clique em <gui>Filtrar resultados</gui> para escolher filtros. Você pode ter que clicar em um cabeçalho de filtro como <gui>Fontes</gui> para ver as escolhas disponíveis.</p>

</section>

<section id="dash-previews">
  <title>Pré-visualizações</title>

  <p>Se você clicar com o botão direito em um resultado de pesquisa, uma <gui>pré-visualização</gui> será aberta com mais informações sobre os resultados.</p>

  <p>Para fechar a pré-visualização, clique em um espaço vazio ou pressione <key>Esc</key>.</p>

</section>

</page>
