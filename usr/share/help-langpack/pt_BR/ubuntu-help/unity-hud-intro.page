<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="question" id="unity-hud-intro" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="shell-overview#desktop"/>

    <desc>Use o HUD para pesquisar os menus dos aplicativos que você usa.</desc>
    <revision version="13.10" date="2013-10-23" status="outdated"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>O que é HUD?</title>

<p>The <gui>HUD</gui> or <gui>Heads Up Display</gui> is a search-based 
alternative to traditional menus and was introduced in Ubuntu 12.04 
LTS.</p>

<p>Alguns aplicativos como <link href="apt:gimp">Gimp</link> ou <link href="apt:inkscape">Inkscape</link> possuem centenas de itens nos menus. Se você estiver usando aplicativos como estes, você pode lembrar do nome de uma opção de menu, mas pode não se lembrar de como encontrá-la nos menus.</p>

<p>Using a search box can be quite a bit faster than navigating extended 
menu hierarchies. The HUD also can be more accessible than normal menus as 
some people are unable to precisely control a mouse pointer.</p>

<section id="use-the-hud">
<title>Usar o HUD</title>

  <p>To try the HUD:</p>

  <steps>
    <item>
      <p>Tecle <key>Alt</key> para abrir o HUD.</p>
    </item>
    <item>
      <p>Comece a digitar.</p>
    </item>
    <item>
      <p>When you see a result that you want to run, use the up and down 
      keys to select the result, then press <key>Enter</key>, or click 
      your desired search result.</p>
    </item>
    <item>
      <p>If you change your mind and want to exit the HUD, tap the 
      <key>Alt</key> again or the <key>Esc</key>. You can also click 
      anywhere outside the HUD to close the HUD.</p>
    </item>
  </steps>

<p>O HUD controla o seu histórico de buscas e ajusta os resultados da pesquisa para ser ainda mais útil quanto mais você usá-lo.</p>

</section>

</page>
