<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files" group="more"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <desc>Abrir arquivos usando um aplicativo que não é o padrão para esse tipo de arquivo. Você pode alterar o padrão também.</desc>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Abra arquivos com outros aplicativos</title>

<p>Quando você dá um duplo-clique em um arquivo no gerenciador de arquivos, ele será aberto com o aplicativo padrão para aquele tipo de arquivo. Você pode abri-lo em um programa diferente, buscar aplicativos na Internet ou configurar o aplicativo padrão para todos arquivos do mesmo tipo.</p>

<p>Para abrir um arquivo com um aplicativo diferente do padrão, clique com o botão direito do mouse no arquivo e selecione o aplicativo que você deseja a partir do topo do menu. Se você não vê o aplicativo que você quer, clique em <gui>Abrir com outro aplicativo</gui>. Por padrão, o gerenciador de arquivos mostra somente os aplicativos que ele sabe que podem manipular o arquivo. Para ver todos os aplicativos em seu computador, clique <gui>Exibir outros aplicativos</gui>.</p>

<p>Se você ainda não achar o aplicativo que você quer, você pode pesquisar por mais aplicativos clicando <gui>Pesquisar aplicativos na Internet</gui>. O gerenciador de arquivos irá pesquisar na Internet por pacotes contendo aplicativos que sabidamente manipulam arquivos daquele tipo.</p>

<section id="default">
  <title>Altere o aplicativo padrão</title>
  <p>Você pode alterar o aplicativo padrão que é usado para abrir arquivos de um dado tipo. Isso permitirá que você abra seus aplicativos preferidos quando der um clique duplo para abrir um arquivo. Por exemplo, você pode querer que seu reprodutor de música favorito abra quando você dá um clique duplo em um arquivo MP3.</p>

  <steps>
    <item><p>Selecione um arquivo do tipo para o qual você deseja alterar o aplicativo padrão. Por exemplo, para alterar qual aplicativo é usado para abrir arquivos MP3, selecione um arquivo <file>.mp3</file>.</p></item>
    <item><p>Clique com o botão direito no arquivo e selecione <gui>Propriedades</gui>.</p></item>
    <item><p>Selecione a aba <gui>Abrir com</gui>.</p></item>
    <item><p>Selecione o aplicativo que você quer e clique em <gui>Configurar como padrão</gui>. Por padrão, o gerenciador de arquivos mostra somente os aplicativos que ele sabe que podem manipular o arquivo. Para ver todos os aplicativos em seu computador, clique em <gui>Exibir outros aplicativos</gui>.</p>
    <p>Se <gui>Outros aplicativos</gui> contém um aplicativo que você quer usar algumas vezes, mas não quer que ele seja padrão, selecione esse aplicativo e clique em <gui>Adicionar</gui>. Isso irá adicioná-lo em <gui>Aplicativos recomendados</gui>. Você será, então, capaz de usar esse aplicativo clicando com o botão direito no arquivo e selecionando-o da lista.</p></item>
  </steps>

  <p>Isso altera o aplicativo padrão não apenas para o arquivo selecionado, mas para todos aqueles que sejam do mesmo tipo.</p>

</section>

</page>
