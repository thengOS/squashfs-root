<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-findip" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Saber seu endereço IP pode ajudar a solucionar problemas de rede.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Encontre o seu endereço IP</title>

  <p>Conhecer o seu endereço de IP pode ajudá-lo a solucionar problemas com sua conexão de Internet. Você pode se surpreender ao saber que você tem <em>dois</em> endereços IP: um endereço IP para o seu computador na rede interna e um endereço IP para o seu computador na Internet.</p>

  <steps>
    <title>Descubra o seu endereço IP interno (da rede)</title>
    <item><p>Clique no ícone mais à direita da <gui>barra de menu</gui> e selecione <gui>Configurações do sistema</gui>.</p></item>
    <item><p>Open <gui>Network</gui> and select <gui>Wired</gui> or <gui>Wireless</gui> from the list on the left, depending on which network connection you want to find the IP address for.</p></item>
    <item><p>Your internal IP address will be displayed in the list of information.</p></item>
  </steps>

  <steps>
  	<title>Encontre o seu endereço IP externo (Internet)</title>
    <item><p>Visite <link href="http://whatismyipaddress.com/">whatismyipaddress.com</link>.</p></item>
    <item><p>A página irá exibir a você o seu endereço IP externo.</p></item>
  </steps>

<p>Dependendo da forma com que seu computador se conecta à Internet, estes endereços podem ser o mesmo.</p>

</page>
