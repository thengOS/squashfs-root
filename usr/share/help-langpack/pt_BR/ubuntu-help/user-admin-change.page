<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="user-admin-change" xml:lang="pt-BR">

  <info>

    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>

    <desc>Você pode alterar quais usuários estão autorizados a fazer alterações no sistema, dando-lhes privilégios administrativos.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Alterar quem tem privilégios adminitrativos</title>

  <p>Privilégios administrativos são uma forma de decidir quem pode fazer mudanças em partes importantes do sistema. Você pode alterar quais usuários possuem privilégios de administrador e quais não possuem. É um bom jeito de deixar seu sistema seguro e prevenir mudanças não-autorizadas potencialmente prejudiciais.</p>

<steps>
 <item>
    <p>Clique no ícone no canto direito da <gui>barra de menu</gui> e selecione <gui>Configuração do Sistema</gui>.</p>
 </item>
 <item>
    <p>Abrir <gui>Contas de Usuário</gui>.</p>
 </item>

 <item>
  <p>Clique em <gui>Desbloquear</gui> e digite sua senha para desbloquear as configurações da conta. (Para dar uma privilégios de administrador ao usuário, você deve ter privilégios de administrador.)</p>
 </item>

 <item>
  <p>Selecione o usuário cujos privilégios você deseja alterar.</p>
  </item>

 <item><p>Clique no rótulo <gui>Padrão</gui> próximo ao <gui>Tipo de conta</gui> e selecione <gui>Administrador</gui>.</p>
 </item>


 <item>
  <p>Feche a janela Contas de Usuário. Os privilégios do usuário serão alterados na próxima vez que eles entrarem.</p>
  </item>
</steps>

<note>
 <p>A primeira conta de usuário no sistema tem privilégios administrativos. Essa é a conta de usuário que foi criada quando você instalou o sistema pela primeira vez.</p>
 <p>Não é recomendado ter muitos usuários com privilégios <gui>Administrator</gui> em um sistema.</p>
</note>
	
</page>
