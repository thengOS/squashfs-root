<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="hardware-driver-proprietary" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="hardware" group="more"/>
    
    <desc>Drivers de dispositivo proprietários não estão disponíveis livremente e não são de código aberto.</desc>
    
    <revision version="13.10" date="2013-09-12" status="review"/>

    <credit type="author">
      <name>Time de documentação do Ubuntu</name>
      <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>O que são drivers proprietários?</title>

  <p>A maioria dos dispositivos (equipamentos) conectados aos seu computador deve funcionar corretamente com o Ubuntu. Esses dispositivos devem ter drivers de código aberto, o que significa que os drivers podem ser modificados pelos desenvolvedores do Ubuntu e que problemas com eles podem ser corrigidos.</p>

  <p>Alguns equipamentos não têm drivers de código aberto, geralmente porque o fabricante não liberou detalhes sobre o equipamento, o que torna impossível criar tal driver. Esses dispositivos podem ter funcionalidade limitada ou podem não funcionar.</p>

  <p>Se um driver proprietário está disponível para um certo dispositivo, você pode instalá-lo para permitir ao seu dispositivo funcionar corretamente ou adicionar novos recursos. Por exemplo, instalar um driver proprietário para algumas placas de vídeo pode lhe permitir usar efeitos visuais mais avançados.</p>

  <p>Muitos computadores não necessitam de drivers proprietários porque os drivers de código aberto suportam completamente o equipamento.</p>

  <note style="warning">
    <p>A maioria dos problemas com os drivers proprietários não podem ser corrigidos pelos desenvolvedores do Ubuntu.</p>
  </note>

</page>
