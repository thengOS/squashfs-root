<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-hidden" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files#faq"/>
    <desc>Torne um arquivo invisível, para que você não o veja no gerenciador de arquivos.</desc>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-29" status="review"/>
    <revision version="13.10" date="2013-09-20" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Oculte um arquivo</title>

  <p>O gerenciador de arquivos do GNOME permite que você oculte e exiba novamente arquivos à sua discrição. Quando um arquivo está oculto, ele não é exibido no gerenciador de arquivos, mas ainda está lá em sua pasta.</p>

  <p>Para ocultar um arquivo, <link xref="files-rename">renomeie-o</link> com um <key>.</key> no início do seu nome. Por exemplo, para ocultar um arquivo chamado <file>exemplo.txt</file>, você deve renomeá-lo  para <file>.exemplo.txt</file>.</p>

<note>
  <p>Você pode ocultar pastas da mesma maneira que oculta arquivos. Oculte uma pasta colocando um <key>.</key> no início do nome da pasta.</p>
</note>

<section id="show-hidden">
 <title>Exiba todos os arquivos ocultos</title>
  <p>If you want to see all hidden files in a folder, go to that folder and
  either click the <media type="image" src="figures/go-down.png">down</media>
  button in the toolbar and pick <gui>Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq>. You will see all hidden files,
  along with regular files that are not hidden.</p>

  <p>To hide these files again, either click the
  <media type="image" src="figures/go-down.png">down</media> button in the
  toolbar and pick <gui>Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq> again.</p>

</section>

<section id="unhide">
 <title>Desocultar um arquivo</title>
  <p>To unhide a file, go to the folder containing the hidden file and click
  the <media type="image" src="figures/go-down.png">down</media> button in the
  toolbar and pick <gui>Show Hidden Files</gui>. Then, find the hidden file and
  rename it so that it doesn't have a <key>.</key> in front of its name. For
  example, to unhide a file called <file>.example.txt</file>, you should rename
  it to <file>example.txt</file>.</p>

  <p>Once you have renamed the file, you can either click the
  <media type="image" src="figures/go-down.png">down</media> button
  in the toolbar and pick <gui>Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq> to hide any other hidden files
  again.</p>

  <note><p>Por padrão, você só verá arquivos ocultos no gerenciador de arquivos até que você o feche. Para mudar esta configuração, de forma que o gerenciador de arquivos sempre exiba os arquivos ocultos, veja <link xref="nautilus-views"/>.</p></note>

  <note><p>A maioria dos arquivos ocultos terão um <key>.</key> no início do seu nome, mas outros, em vez disto, podem tem um <key>~</key> no final do nome. Estes arquivos são cópias de segurança. Veja <link xref="files-tilde"/> para mais informações.</p></note>
</section>
</page>
