<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="shell-guest-session" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="shell-overview#desktop" group="#last"/>
    <link type="guide" xref="user-accounts#manage"/>

    <desc>Empreste o seu computador a um amigo ou colega de maneira segura.</desc>
    <revision version="16.04" date="2016-05-08" status="review"/>
    <credit type="author">
      <name>Gunnar Hjalmarsson</name>
      <email>gunnarhj@ubuntu.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Lance uma sessão restrita para convidado</title>

<section id="restricted">
<title>Sessão temporária com privilégios restritos</title>

<p>Eventualmente um amigo, familiar ou colega pode pedir seus computador emprestado. O recurso <app>Sessão convidado</app> do Ubuntu proporciona uma maneira conveniente e altamente segura de emprestar seu computador a outra pessoa. A sessão convidado pode ser iniciada tanto na tela de boas-vindas quanto de uma sessão regular já aberta. Se você já está conectado, clique no ícone na extrema direita da <gui>barra de menu</gui> e selecione <gui>Sessão convidado</gui>. Isso irá bloquear a tela da sua sessão e iniciará uma sessão convidado.</p>

<p>Um convidado não pode ver as pastas pessoais de outros usuários e, por padrão, todos os dados salvos e configurações alteradas serão removidas/reiniciadas ao sair. Isso significa que cada sessão inicia com um ambiente limpo sem ser afetada pelo que o convidado anterior fez.</p>

</section>

<section id="customize">
<title>Personalização</title>

<p>O tutorial on-line <link href="https://help.ubuntu.com/community/CustomizeGuestSession">Personalizar sessão convidado</link> explica como personalizar a aparência e o comportamento.</p>

</section>

<section id="disable">
<title>Disabling the feature</title>

<p>If you prefer to not allow guest access to your computer, you can disable the <app>Guest
Session</app> feature. To do so, press <keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq>
to open a terminal window, and then run this command:</p>

<p><cmd its:translate="no">sudo sh -c 'printf "[Seat:*]\nallow-guest=false\n" &gt;/etc/lightdm/lightdm.conf.d/50-no-guest.conf'</cmd></p>

<p>The command creates a small configuration file. To re-enable <app>Guest Session</app>, simply
remove that file:</p>

<p><cmd its:translate="no">sudo rm /etc/lightdm/lightdm.conf.d/50-no-guest.conf</cmd></p>

</section>

</page>
