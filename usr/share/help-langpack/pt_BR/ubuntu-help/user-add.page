<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="user-add" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>
    <link type="seealso" xref="shell-guest-session"/>
    <desc>Adicionar novos usuários para que outras pessoas possam se logar no computador.</desc>
    <!-- Setting 3.4.0 version to final for release, but we need to do something
         about the various comments in here, so incomplete for 3.4.1.
    -->
    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Adicione um nova conta de usuário</title>

  <p>Você pode adicionar várias contas de usuário no computador. Dê uma conta para cada pessoa de sua casa ou empresa. Cada usuário terá a sua própria pasta pessoal, documentos e configurações.</p>

<steps>
  <item><p>Clique no ícone no canto direito da <gui>barra de menu</gui> e selecione <gui>Configuração do Sistema</gui>.</p></item>
  <item><p>Abrir <gui>Contas de Usuário</gui>.</p></item>

  <item><p>Voce precisa de <link xref="user-admin-explain">privilégios administrativos</link> para adicionar contas de usuário. Clique em <gui>Destravar</gui> no canto superior direito e digite sua senha.</p></item>

  <item><p>Na lista de contas à esquerda, clique no botão <key>+</key> para adicionar uma nova conta de usuário.</p></item>

  <item><p>Se você deseja que o novo usuário passe a ter <link xref="user-admin-explain">acesso de administrador</link> para esse computador, selecione <gui> Administrador</gui> para alterar o tipo de conta. Os administradores podem fazer coisas como adicionar e excluir usuários, instalar programas e drivers , e alterar a data e a hora.</p></item>

  <item><p>Digite o nome completo do novo usuário. O nome de usuário será automaticamente preenchido com base no nome completo. O padrão é provavelmente OK, mas você poderá mudar se quiser.</p></item>

  <item><p>Clique em <gui>Criar</gui>.</p></item>

  <item><p>The account is initially disabled until you choose what to do about
  the user's password. Under <gui>Login Options</gui> click <gui>Account
  disabled</gui> next to <gui>Password</gui>. Select <gui>Set a password now</gui>
  from the <gui>Action</gui> drop-down list, and have the user type their
  password in the <gui>New password</gui> and <gui>Confirm password</gui> fields.
  See <link xref="user-goodpassword"/>.</p>
  <p>Você também pode clicar no botão próximo ao campo <gui>Nova senha</gui> para selecionar uma senha segura gerada aleatoriamente. Essas senhas são difíceis de se adivinhar, mas podem ser difíceis de memorizar, então tenha cuidado.</p></item>
  <item><p>Clique <gui>Alterar</gui>.</p></item>
</steps>

<note><p>Na janela <gui>Contas de usuários</gui>, você pode clicar na imagem próxima ao nome do usuário, à direita, para definir uma imagem para a conta. Essa imagem será mostrada na janela de login. O GNOME fornece algumas imagens que você pode utilizar ou você pode selecionar sua própria imagem ou tirar uma foto com sua webcam.</p>
</note>

</page>
