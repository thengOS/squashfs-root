<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-windows-switching" xml:lang="pt-BR">

  <info>

    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="guide" xref="shell-overview#apps"/>

    <desc>Pressione <keyseq><key>Alt</key><key>Tab</key></keyseq>.</desc>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-10-23" status="outdated"/>
    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Projeto de documentação do Ubuntu</name>
      <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Alterne entre janelas</title>

<section id="launcher">
  <title>A partir do lançador</title>
  <steps>
    <item><p>Mostrar o <gui>Lançador</gui> movendo o mouse para o canto superior esquerdo da tela.</p></item>
    <item><p>Os aplicativos que estão em execução tem um pequeno triângulo branco no seu lado esquerdo. Clique no aplicativo que está em execução para alternar para ele.</p></item>
    <item><p>Se um aplicativo em execução tiver múltiplas janelas abertas, haverá múltiplas flechas brancas à esquerda. Clique no ícone do aplicativo uma segunda vez para mostrar todas as janelas abertas reduzidas. Clique na janela para a qual você quer alternar.</p></item>
  </steps>
</section>

<section id="keyboard">
  <title>Do teclado</title>
<list>
  <item><p>Pressione <keyseq><key>Alt</key><key>Tab</key></keyseq> para exibir o <gui>alternador de janelas</gui></p>
<list>
  <item><p>Libere a tecla <key>Alt</key> para selecionar a próxima janela (realçada) no alternador de janelas.</p></item>
  <item><p>Caso contrário, mantenha pressionada a tecla <key>Alt</key> e pressione a tecla <key>Tab</key> para movimentar-se à frente pela lista de janelas abertas ou <keyseq><key>Shift</key><key>Tab</key></keyseq> para movimentar-se para trás.</p>

<note style="tip">
  <p>As janelas no alternador de janelas estão agrupadas por aplicativo. Pré-visualizações de aplicativos com múltiplas janelas surgem quando você clica.</p>
</note></item>

  <item><p>Você também pode mover entre os ícones dos aplicativos no alternador de janelas com as teclas <key>→</key> e <key>←</key>, ou selecionar uma um deles clicando com o mouse.</p></item>
  <item><p>Pré-visualizações de aplicativos com uma só janela podem ser exibidas com a tecla <key>↓</key>.</p>

<note>
  <p>Somente janelas do <link xref="shell-workspaces">espaço de trabalho</link> atual serão mostradas. Para mostrar janelas de todos os espaços de trabalho, mantenha pressionadas as teclas <key>Ctrl</key> e <key>Alt</key> e pressione <key>Tab</key> ou <keyseq><key>Shift</key><key>Tab</key></keyseq>.</p>
</note></item>
</list></item>
</list>

<list>
<item>
  <p>Pressione <keyseq><key><link xref="windows-key">Super</link></key><key>W</key></keyseq> para exibir todas as janelas abertas em tamanho reduzido.</p>
    <list>
      <item><p>Clique na janela para a qual você quer alternar.</p></item>
    </list>
</item>

</list>


</section>

</page>
