<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="tip" version="1.0 ui/1.0" id="shell-keyboard-shortcuts" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="keyboard"/>
    <link type="guide" xref="shell-overview"/>
    <link type="seealso" xref="windows-key"/>
    <desc>Percorra a área de trabalho usando o teclado.</desc>
    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision version="13.10" date="2013-10-23" status="outdated"/>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Atalhos de teclado úteis</title>

<p>Esta página fornece uma visão geral de atalhos de teclado que podem ajudá-lo a utilizar seu computador e aplicativos de forma mais eficiente. Se você não pode usar um mouse ou dispositivo apontador em tudo, ver <link xref="keyboard-nav"/> para mais informações na navegação de interfaces de usuário apenas com o teclado.</p>

<table rules="rows" frame="top bottom" ui:expanded="true">
<title>Conhecendo o ambiente de trabalho</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>Fechar a janela atual.</p></td>
  </tr>
  <tr xml:id="alt-f2">
    <td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td> <td><p>Janela de comando (para executar comandos rapidamente)</p></td>
  </tr>
  <tr xml:id="alt-tab">
    <td><p><keyseq><key>Alt</key><key>Tab</key></keyseq></p></td>
    <td><p><link xref="shell-windows-switching">Alterne rapidamente as janelas.</link> Mantenha pressionado <key>Shift</key> para ir na ordem reversa.</p></td>
  </tr>
  <tr xml:id="alt-tick">
    <td><p><keyseq><key>Alt</key><key>`</key></keyseq></p></td>
    <td><p>Alterna entre janelas do mesmo aplicativo ou do aplicativo selecionado depois por <keyseq><key>Alt</key><key>Tab</key></keyseq>.</p>
    <p>Este atalho usa <key>`</key> em teclados americanos, sendo que a tecla <key>`</key> está acima do <key>Tab</key>. Em todos os outros teclados, o atalho é <key>Alt</key> mais a tecla que estiver acima do <key>Tab</key>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="windows-key">Super</key><key>S</key></keyseq></p></td>
    <td><p>Ativa o comutador de janelas. Reduz o tamanho de todos os <link xref="shell-workspaces">espaços de trabalho</link>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="windows-key">Super</key><key>W</key></keyseq></p></td>
    <td><p>Ativa o modo "Expo". Mostra todas as janelas deste espaço de trabalho.</p></td>
  </tr>
  <tr xml:id="ctrl-alt-updown">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Teclas de seta</key></keyseq></p></td>
    <td><p><link xref="shell-workspaces-switch">Alternar entre os espaços de trabalho.</link></p></td>
  </tr>
  <tr xml:id="ctrl-alt-shift-updown">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Shift</key><key>Teclas de seta</key></keyseq></p></td>
    <td><p><link xref="shell-workspaces-movewindow">Mova a janela atual para outro espaço de trabalho</link></p></td>
  </tr>
  <tr xml:id="ctrl-alt-Del">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq></p></td>
    <td><p><link xref="shell-exit">Encerrar sessão.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key xref="windows-key">Super</key><key>D</key></keyseq></p></td>
    <td><p>Esconde todas as janelas e mostra a área de trabalho. Pressione novamente para restaurar suas janelas.</p></td>
  </tr>
  <tr xml:id="ctrl-alt-l">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>L</key></keyseq></p></td>
    <td><p><link xref="shell-exit#lock-screen">Bloquear tela.</link></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Atalhos de edição comuns</title>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>A</key></keyseq></p></td>
    <td><p>Selecionar todo o texto ou itens em uma lista.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>X</key></keyseq></p></td>
    <td><p>Cortar (remover) texto ou itens selecionados e colocá-los na área de transferência.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>C</key></keyseq></p></td>
    <td><p>Copiar texto selecionado ou itens para a área de transferência.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>V</key></keyseq></p></td>
    <td><p>Cola o conteúdo da área de transferência.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Z</key></keyseq></p></td>
    <td><p>Desfaz a última ação.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Capturando da tela</title>
  <tr>
    <td><p><key>Print Screen</key></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Capturar uma imagem da tela.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Print Screen</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Capturar uma imagem de uma janela.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>Print Screen</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Capturar uma imagem de uma área da tela.</link> O cursor muda para uma cruz. Clique e arraste para selecionar uma área.</p></td>
  </tr>
</table>

</page>
