<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-crackle" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="sound-broken"/>

    <desc>Verifique os cabos de áudio e drivers da placa de som.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-10-23" status="review"/>   
    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Ouço estalidos e zumbidos quando sons são reproduzidos</title>

  <p>Se você ouvir estalidos ou zumbidos quando sons são reproduzidos em seu computador, talvez você tenha um problema com os cabos ou conectores de áudio, ou com os drivers de sua placa de som.</p>

<list>
 <item>
  <p>Verifique se os alto-falantes estão conectados corretamente.</p>
  <p>Se os alto-falantes não estão completamente conectados ou conectados no conector errado, você poderá ouvir um zumbido.</p>
 </item>

 <item>
  <p>Certifique-se de que o cabo do alto-falante/fone de ouvido não está danificado.</p>
  <p>Cabos e conectores de áudio podem se desgastar com o uso. Experimente conectar o cabo ou fones de ouvido a outro dispositivo de áudio (como um reprodutor de MP3 ou de CD) para verificar se ainda há estalidos. Se houver, talvez seja necessário substituir o cabo ou fones.</p>
 </item>

 <item>
  <p>Verifique se os drivers de som funcionam bem.</p>
  <p>Algumas placas de som não funcionam muito bem no Linux porque elas não possuem drivers muito bons. Este problema é mais difícil de ser identificado. Experimente procurar pela marca e modelo de sua placa na Internet, juntamente ao termo "Ubuntu", para ver se outras pessoas estão tendo o mesmo problema.</p>
  <p>Você pode executar <cmd>sudo lspci -v</cmd> no <app>Terminal</app> para conseguir mais informações sobre a sua placa de som.</p>
 </item>
</list>

</page>
