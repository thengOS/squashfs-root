<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="power-closelid" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.10" date="2013-07-18" status="outdated"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Computadores portáteis entram em suspensão quando você fecha a tampa, a fim de economizar energia.</desc>
  </info>

<title>Por que meu computador desliga quando eu fecho a tampa?</title>

<p>Quando você fecha a tampa do seu computador portátil, ele <link xref="power-suspend"><em>entra em suspensão</em></link> para economizar energia. Isto significa que o computador não está realmente desligado, ele está apenas dormindo. Você pode fazê-lo voltar abrindo a tampa. Se ele não voltar, tente clicar com o mouse ou pressionar uma tecla. Se isso não funcionar, pressione o botão de energia.</p>

<p>Alguns computadores são incapazes de suspender adequadamente, normalmente, porque o seu hardware não é completamente suportado pelo sistema operacional (por exemplo, os drivers para Linux são incompletos). Neste caso, é possível que você não consiga reiniciar o computador depois que fechou a tampa. Você pode tentar <link xref="power-suspendfail">corrigir o problema com suspensão</link>, ou você pode impedir que o computador tente entrar em suspensão quando você fechar a tampa.</p>

<section id="nosuspend">
 <title>Desative o recurso de suspensão quando a tampa é fechada</title>
 <p>Se você não quiser que o computador suspenda quando você fecha a tampa, você pode alterar as configurações para esse comportamento:</p>
 <note style="warning">
   <p>Tenha muito cuidado se você alterar essa configuração. Alguns computadores portáteis podem superaquecer se forem deixados em execução com a tampa fechada, especialmente se eles estiverem em um lugar confinado, como uma mochila.</p>
 </note>

<steps>
  <item><p>Clique no ícone mais à direita da <gui>barra de menu</gui> e selecione <gui>Configurações do sistema</gui>.</p></item>
  <item><p>Na sessão <gui>Hardware</gui>, clique em <gui>Energia</gui>.</p></item>
  <item><p>Defina as opções ao lado de <gui>Quando a tampa estiver fechada</gui> para <gui>Não fazer nada</gui>.</p></item>
</steps>
</section>

</page>
