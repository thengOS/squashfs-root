<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-display" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-display"/>

    <desc>Control icon captions used in the file manager.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision version="14.04" date="2014-03-18" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Time de documentação do Ubuntu</name>
      <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>
  </info>

<title>Preferências de exibição do gerenciador de arquivos</title>

<p>You can control how the file manager displays captions under icons.  Click
<gui>Edit</gui> in the menu bar, pick <gui>Preferences</gui> and select the
<gui>Display</gui> tab.</p>

<section id="icon-captions">
  <title>Legendas dos ícones</title>
  <media type="image" src="figures/nautilus-icons.png" width="250" height="110" style="floatend floatright">
    <p>Ícones do gerenciador de arquivos com legendas</p>
  </media>
  <p>Quando você usa visão de ícone, você pode escolher ter informação extra sobre arquivos e pastas exibida em uma legenda embaixo de cada ícone. Isso é útil, por exemplo, se você precisa ver com frequência quem é proprietário de um arquivo ou quando ele foi modificado pela última vez.</p>
  <p>You can zoom in a folder by clicking the <guiseq><gui>View</gui>
  <gui>Zoom In</gui></guiseq> or press <keyseq><key>Ctrl</key><key>+</key></keyseq>.
  As you zoom in, the file manager will display more and more information in captions.
  You can choose up to three things to show in captions. The first will be displayed
  at most zoom levels. The last will only be shown at very large sizes.</p>
  <note><p>Se você tem uma janela do gerenciador de arquivos aberta, você pode ter que recarregar para alterar a legenda do ícone para ter efeito. Clique em <guiseq><gui>Ver</gui><gui>Recarregar</gui></guiseq> ou pressione <keyseq><key>Ctrl</key><key>R</key></keyseq>.</p></note>
</section>

</page>
