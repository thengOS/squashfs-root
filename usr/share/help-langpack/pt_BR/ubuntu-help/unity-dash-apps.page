<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="unity-dash-apps" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="unity-dash-intro#dash-lenses"/>
    <link type="seealso" xref="unity-dash-intro#dash-lenses"/>

    <desc>Execute, instale, ou desinstale aplicativos.</desc>

    <revision version="16.04" date="2013-03-14" status="outdated"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Lente de aplicativos</title>

  <p>The applications lens is the first lens after the Dash home in the
    <gui>lens bar</gui>. The applications lens gives you access to your 
    applications or applications available for install.</p>

  <p>Você pode usar <keyseq><key>super</key><key>a</key></keyseq> para abrir o painel diretamente na lente de aplicativos.</p>

<section id="dash-apps-previews">
  <title>Pré-visualizações</title>

  <p>Right click on a search result to open a <gui>preview</gui>. The 
  preview shows a short description of the application, a screenshot, its 
  <app>Ubuntu Software</app> rating, and what version is available.</p>

  <p>For installed applications, you can see when the application was installed and
     either launch the application or uninstall it. Certain essential applications cannot
     be uninstalled from the preview.</p>

  <p>For applications that haven't been installed, you can install them right from 
  the preview.</p>

</section>

<section id="dash-apps-filters">
  <title>Filtros</title>

  <p>Click <gui>Filter results</gui> if you'd like to only see results for 
  a certain type of application. You can also click 
  <guiseq><gui>Sources</gui><gui>Local Applications</gui></guiseq> to only view 
  installed applications or <guiseq><gui>Sources</gui><gui>Software 
  Center</gui></guiseq> to only show applications available for install.</p>

</section>

</page>
