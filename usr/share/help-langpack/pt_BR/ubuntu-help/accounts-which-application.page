<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-which-application" xml:lang="pt-BR">

  <info>
    <credit type="author copyright">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
			<years>2012-2013</years>
    </credit>

    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

		<credit type="editor">
	<name>Greg Beam</name>
	<email>ki7mt@yahoo.com</email>
    </credit>

    <link type="guide" xref="accounts"/>
    <revision pkgversion="3.10.2" version="0.1" date="2014-01-08" status="review"/>
    <revision version="14.04" date="2014-01-08" status="review"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <desc>
      Applications can use the accounts created in <app>Online Accounts</app> 
      and the services they exploit.
    </desc>
  </info>

  <title>Quais aplicativos tiram proveito das contas on-line?</title>

  <p>
    <app>Online Accounts</app> can be used by external applications to 
    automatically configure themselves.
  </p>

  <section id="accounts-google-services">
    <title>Com uma conta Google</title>

    <list>
      <item>
        <p>
          <app>Evolution</app>, the email application. Your email account will 
          be added to <app>Evolution</app> automatically, so it will retrieve 
          your mail, give you access to your contacts, and display your calendar 
          items in your Google agenda.
        </p>
      </item>
      <item>
        <p><app>Empathy</app>, o aplicativo de mensagens instantâneas. A sua conta on-line será adicionada e você poderá se comunicar com os seus amigos.</p>
      </item>
      <item>
        <p>
          <app>Contacts</app>, which will allow to see and edit your contacts.
        </p>
      </item>
      <item>
        <p>
          <app>Documents</app> can access your online documents and display 
          them.
        </p>
      </item>
    </list>
  </section>

  <section id="accounts-windows-services">
    <title>
      With Windows Live, Facebook or Twitter accounts
    </title>

    <p>
      <app>Empathy</app> can use these accounts to connect you online and chat 
      with your contacts, friends, and followers.
    </p>
  </section>

  <section id="account-windows-skydrive">
    <title>Com uma conta do SkyDrive</title>

    <p><app>Documentos</app> poderá acessar e exibir seus documentos on-line na Microsoft SkyDrive.</p>
  </section>

  <section id="account-exchange">
    <title>Com uma conta Exchange</title>

    <p>
      Once you have created an Exchange account, <app>Evolution</app> will start 
      retrieving mails from this account.
    </p>
  </section>

  <section id="accounts-ownCloud">
    <title>Com uma conta ownCloud</title>

    <p>
      When an ownCloud account is set up, <app>Evolution</app> is able to access 
      and edit contacts and calendar appointments.
    </p>

    <p>
      <app>Files</app> and other applications will be able to list and access 
      your online files stored in the ownCloud installation.
    </p>
  </section>

</page>
