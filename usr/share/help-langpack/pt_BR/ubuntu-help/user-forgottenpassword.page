<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="user-forgottenpassword" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <desc>Técnicas avançadas para reinicializar sua senha</desc>
    <revision version="13.10" date="2013-10-23" status="outdated"/>
    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Esqueci minha senha!</title>
  <p>É importante escolher não somente <link xref="user-goodpassword">uma senha boa e segura</link>, mas também uma que você possa se lembrar. Se você tiver esquecido a senha para iniciar a sessão na sua conta de computador, siga os seguintes passos para reiniciá-la.</p>
  <note style="important">
  <p>Se voce tiver seu diretório pessoal encriptado, você não será capaz de resetar uma senha esquecida.</p>
  </note>
  <p>Se voce quer simplesmente mudar sua senha, veja <link xref="user-changepassword"/>.</p>

  <links type="section"/>

  <section id="reset-password-grub2"><title>Reinicialize a senha utilizando o Grub</title>
     <steps>
         <item>
           <p>Reinicie seu computador e mantenha pressionada a tecla <key>Shift</key> durante a inicialização para entrar no menu do Grub.</p>
           <note style="tip">
           <p>Se você tem um computador com dois sistemas operacionais e você escolhe durante a inicialização qual deles você quer iniciar, o menu do Grub deve aparecer sem que você precise manter pressionada a tecla <key>Shift</key>.</p>
           </note>
           <note>
           <p>Se voce não conseguir entrar no menu de inicialização do Grub, e por conta disso não puder escolher o modo de recuperação, você pode <link xref="user-forgottenpassword#live-cd">utilizar um live CD para resetar a senha</link>.</p>
           </note>
          </item>
          <item>
           <p>Pressione a tecla seta para baixo no seu teclado para selecionar a linha que termina com as palavras 'modo de recuperação' e pressione <key>Enter</key>.</p>
          </item>
          <item>
           <p>Seu computador vai agora iniciar o processo de boot. Depois de alguns instantes o <gui>Menu de Recuperação</gui> aparecerá. Utilize a tecla seta para baixo para selecionar <gui>root</gui> e pressione <key>Enter</key>.</p>
          </item>
          <item>
           <p>No símbolo <cmd>#</cmd>, digite:</p>
           <p><cmd>passwd <var>username</var></cmd>, onde <var>username</var> é o nome do usuário da conta que você quer mudar a senha.</p>
          </item>
          <item>
           <p>Será solicitado para voce digitar uma nova senha UNIX e confirmar novamente a senha.</p>
          </item>
          <item>
           <p>Então, digite:</p>
           <p># <cmd>reboot</cmd></p>
          </item>
     </steps>
     <p>Depois que você entrar, não terás acesso ao seu chaveiro (desde que você não se lembre da senha antiga). Isso significa que todas as sua senhas salvas para redes sem fio, contas Jabber, etc. não estarão mais acessíveis. Você precisará <link xref="#delete-keyring">apagar o chaveiro antigo</link> e iniciar um novo.</p>
  </section>
    <section id="live-cd"><title>Redefinir senha usando um Live Cd ou USB</title>
     <steps>
         <item>
           <p>Inicializar o Live CD ou USB</p>
          </item>
          <item>
           <p>Montar seu drive</p>
          </item>
          <item>
           <p>Pressione <keyseq><key>Alt</key><key>F2</key></keyseq> para abrir a janela <gui>Executar aplicativo</gui>.</p>
          </item>
          <item>
           <p>Tecle <cmd>gksu nautilus</cmd> para executar o gerenciador de arquivos com todos os privilégios do sistema.</p>
           <note style="tip">
           <!-- Translators: do not translate the word "home". -->
           <p>Dentro da unidade que você acabou de montar, você pode verificar se é o driver correto clicando em <gui> home </gui> e, em seguida no seu nome de usuário.</p>
           </note>
          </item>
          <item>
           <p>Vá para o diretório um nível superior da unidade montada. Em seguida, vá para o diretório <gui> etc </gui>.</p>
           <p>Localize o arquivo 'shadow' e faça uma cópia de segurança:</p>
           <steps>

           <item><p>Clique com o botão direito no arquivo shadow e depois selecione <gui>copiar</gui>.</p></item>
           <item><p>Então dê um clique com o botão direito no espaço em branco e selecione <gui>colar</gui>.</p></item>

           <item><p><link xref="files-rename">Renomear</link> a cópia de segurança "shadow.bak".</p></item>
           </steps>
          </item>


          <item>
           <p>Edite o arquivo original "shadow" com um editor de texto.</p>
          </item>

          <item>
           <p>Encontre o nome de usuário que você tenha esquecido a senha. Deve ser algo parecido com isto (os caracteres após os dois pontos serão diferentes):</p>
           <p>username:$1$2abCd0E or</p>
            <p>username:$1$2abCd0E:13721a:0:99999:7:::</p>
          </item>
          <item>
           <p>Exclua os caracteres depois da primeira vírgula e antes da segunda vírgula. Isso irá remover a senha para a conta.</p>
           <p>Salve o arquivo, feche tudo e reinicie o seu computador sem o Live CD ou USB.</p>
          </item>
          <item>
           <p>Quando você inicializar novamente sua instalação, clique no seu nome na barra de menu. Abra <gui>Minha Conta</gui> e altere sua senha.</p>
          </item>
          <item>
          <p>Para <gui>Senha Atual</gui> não digite nada, a sua senha atual está em branco. Clique em <gui>Autenticar</gui> e digite a nova senha.</p>
          </item>

     </steps>
     <p>Depois que você entrar, não terás acesso ao seu chaveiro (desde que você não se lembre da senha antiga). Isso significa que todas as sua senhas salvas para redes sem fio, contas Jabber, etc. não estarão mais acessíveis. Você precisará <link xref="#delete-keyring">apagar o chaveiro antigo</link> e iniciar um novo.</p>
  </section>


  <section id="delete-keyring">
    <title>Livrar-se do conjunto de chaves</title>

    <note style="warning"><p>Isso irá excluir todas as suas senhas de rede sem fio salvas, contas de mensagens instantâneas, etc. Só faça isso se você não consegue se lembrar da senha que você usou para seu chaveiro.</p></note>

  <steps>
   <item><p>Vá para a sua pasta pessoal digitando 'pessoal' no <gui>Painel</gui>.</p></item>
   <item><p>Pressione <keyseq><key>Ctrl</key><key>h</key></keyseq> (ou clique <guiseq><gui>Ver</gui><gui>Mostrar arquivos ocultos</gui></guiseq>.)</p></item>
   <item><p>Clique duas vezes sobre a pasta <file>~/.local/share</file>.</p></item>
   <item><p>Clique duas vezes sobre a pasta chamada keyrings.</p></item>
   <item><p>Apague quaisquer arquivos que encontrar na pasta de chaveiros</p></item>
   <item><p>Reinicie o computador.</p></item>
 </steps>
<p>Depois que reiniciar e iniciar a sessão, você será solicitado que entre com a senha da sua rede sem fio.</p>
  </section>

</page>
