<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="user-changepassword" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>

    <desc>Mantenha sua conta segura modificando várias vezes a senha nas configurações de sua conta.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Alterar sua senha</title>

  <p>É uma boa ideia mudar sua senha de tempos em tempos, especialmente se você acha que alguém mais a conhece.</p>
   <steps>
    <item><p>Clique no ícone no canto direito da <gui>barra de menu</gui> e selecione <gui>Configuração do Sistema</gui>.</p></item>
    <item><p>Abrir <gui>Contas de Usuário</gui>.</p></item>
    <item><p>Clique no rótulo próximo à <gui>Senha</gui>.</p><note>
    <p>O rótulo deve parecer com uma série de pontos ou caixas se você já possui uma senha definida.</p></note></item>
    <item><p>Digite a sua senha atual, então uma nova senha. Digite sua nova senha novamente no campo <gui>Confirme senha</gui>.</p>
    <p>Você também pode clicar no botão próximo ao campo <gui>Nova senha</gui> para selecionar uma senha segura gerada aleatoriamente. Essas senhas são difíceis de se adivinhar, mas podem ser difíceis de memorizar, então tenha cuidado.</p></item>
    <item><p>Clique <gui>Alterar</gui>.</p></item>
   </steps>

  <p>Certifique-se de  <link xref="user-goodpassword">escolher uma boa senha</link>. Isto irá ajudar a manter segura a sua conta de usuário.</p>

  <section id="changepass">
    <title>Alterar a senha do chaveiro</title>

  <p>Se você alterar sua senha de login, pode ficar sem sincronia com o <em>chaveiro de sessão</em>. O chaveiro evita que você precise lembrar de muitas senhas diferentes exigindo apenas uma senha <em>mestre</em> para acessar tudo. Se você alterar sua senha de usuário (ver acima), sua senha do chaveiro continuará igual a antiga. Para alterar a senha do chaveiro (para combinar com sua senha de login):</p>

  <steps>
    <item><p>Abra o aplicativo <app>Senhas e chaves</app> a partir do <gui>Painel</gui>.</p></item>
    <item><p>No menu <gui>Exibir</gui>, garanta que a opção <gui>Por chaveiro</gui> esteja marcada.</p></item>
    <item><p>Na barra lateral, em <gui>Senhas</gui>, clique com o botão direito em <gui>Chave de login</gui> e selecione <gui>Alterar senha</gui>.</p></item>
    <item><p>Entre com a sua <gui>Antiga senha</gui>, seguida da nova <gui>Senha</gui> e <gui>Confirmar</gui> sua nova senha digitando-a novamente.</p></item>
    <item><p>Clique <gui>OK</gui>.</p></item>
  </steps>
  </section>
</page>
