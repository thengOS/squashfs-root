<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="guide" style="task" version="1.0 if/1.0" id="bluetooth-send-file" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="files-share"/>

    <revision pkgversion="3.8.2" version="0.2" date="2013-05-16" status="review"/>
    <revision version="13.10" date="2013-09-20" status="review"/>

    <desc>Compartilhe arquivos com dispositivos Bluetooth tal como o seu celular.</desc>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Enviar um arquivo para um dispositivo Bluetooth</title>

<if:choose>
  <if:when test="platform:unity">
    <p>Você pode enviar arquivos para dispositivos Bluetooth conectados, tais como telefones celulares ou outros computadores. Alguns tipos de dispositivos não permitem a transferência de arquivos, ou de tipos específicos de arquivos. Você pode enviar arquivos por uma dessas três maneiras: utilizando o ícone Bluetooth na barra de menus, a partir da janela de configuração de Bluetooth ou diretamente a partir do gerenciador de arquivos.</p>
  </if:when>
  <p>You can send files to connected Bluetooth devices, such as some mobile
  phones or other computers. Some types of devices don't allow the transfer
  of files, or specific types of files. You can send files using the Bluetooth
  icon on the top bar, or from the Bluetooth settings window.</p>
</if:choose>

<if:choose>
  <if:when test="platform:unity">
  <p>Para enviar arquivos diretamente a partir do gerenciador de arquivos, veja <link xref="files-share"/>.</p>
  </if:when>
</if:choose>

  <note style="important">
    <p>Antes de começar, certifique-se que o recurso Bluetooth está ativado no seu computador. Veja <link xref="bluetooth-turn-on-off"/>.</p>
  </note>

  <steps>
    <title>Enviar arquivos usando um ícone Bluetooth</title>
    <item>
      <if:choose>
        <if:when test="platform:unity">
      <p>Clique no ícone do Bluetooth na barra de menu e selecione <gui>Enviar arquivos a dispositivo</gui>.</p>
        </if:when>
      <p>Clique no ícone Bluetooth na barra superior e selecione <gui>Enviar arquivos para o dispositivo</gui>.</p>
      </if:choose>
    </item>
    <item>
      <p>Selecione o arquivo que deseja enviar e clique em <gui>Selecionar</gui>.</p>
      <p>Para enviar mais de um arquivo de uma pasta, mantenha pressionada a tecla <key>Ctrl</key> enquanto seleciona cada arquivo.</p>
    </item>
    <item>
      <p>Selecione o dispositivo para o qual deseja enviar os arquivos e clique em <gui>Enviar</gui>.</p>
      <p>A lista de dispositivos mostrará tanto <link xref="bluetooth-connect-device">os dispositivos a que você já está conectado</link> bem como <link xref="bluetooth-visibility">os dispositivos visíveis</link> dentro do alcance. Se você ainda não se conectou ao dispositivo selecionado, lhe será solicitado o emparelhamento com o dispositivo depois de clicar em <gui>Enviar</gui>. Provavelmente, será exigida a confirmação no outro dispositivo.</p>
      <p>Se houver muitos dispositivos, você pode limitar a lista para apenas tipos de dispositivos específicos utilizando a lista <gui>Tipo de dispositivo</gui>.</p>
    </item>
    <item>
      <p>O proprietário do dispositivo receptor normalmente tem que pressionar um botão para aceitar o arquivo. Uma vez que o proprietário aceita ou recusa, o resultado da transferência do arquivo será mostrada na sua tela.</p>
    </item>
  </steps>

  <steps>
    <title>Envie arquivos a partir das configurações de Bluetooth</title>
    <item>
      <if:choose>
        <if:when test="platform:unity">
          <p>Clique no ícone do Bluetooth na barra de menu e selecione <gui>Configurações de Bluetooth</gui></p>
        </if:when>
        <p>Clique no ícone Bluetooth na barra superior e selecione <gui>Configurações de Bluetooth</gui>.</p>
      </if:choose>
    </item>
    <item><p>Selecione o dispositivo ao qual enviar arquivos a partir da lista à esquerda. A lista exibe somente dispositivos a que você já esteja conectado. Veja <link xref="bluetooth-connect-device"/>.</p></item>
    <item><p>Nas informações sobre o dispositivo à direita, clique em <gui>Enviar arquivos</gui>.</p></item>
    <item>
      <p>Selecione o arquivo que deseja enviar e clique em <gui>Selecionar</gui>.</p>
      <p>Para enviar mais de um arquivo de uma pasta, mantenha pressionada a tecla <key>Ctrl</key> enquanto seleciona cada arquivo.</p>
    </item>
    <item>
      <p>O proprietário do dispositivo receptor normalmente tem que pressionar um botão para aceitar o arquivo. Uma vez que o proprietário aceita ou recusa, o resultado da transferência do arquivo será mostrada na sua tela.</p>
    </item>
  </steps>
</page>
