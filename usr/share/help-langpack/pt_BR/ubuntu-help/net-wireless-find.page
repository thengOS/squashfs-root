<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="net-wireless-find" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-wireless-hidden"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>A rede sem fio pode estar desligada ou interrompida, pode haver muitas redes sem fio nas proximidades, ou você pode estar fora de alcance.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Eu não vejo a minha rede sem fio na lista</title>

<p>Há diversos motivos pelos quais você pode não ser capaz de ver sua rede sem fio na lista de redes que aparece quando você clica no ícone de rede na barra de menu.</p>

<list>
 <item>
  <p>Se nenhuma rede é mostrada na lista, sua placa de rede sem fio pode estar desligada, ou ela <link xref="net-wireless-troubleshooting">pode não estar funcionando corretamente</link>. Certifique-se de que ela está ligada.</p>
 </item>

 <item>
  <p>Se houver muitas redes sem fio por perto, a rede que você está procurando pode não estar na primeira página da lista. Se este for o caso, procure, na parte de baixo da lista, por uma seta apontando para a direita e passe o ponteiro do mouse sobre ela para exibir o resto das redes sem fio.</p>
 </item>

 <item>
  <p>Você pode estar fora do alcance da rede. Tente se mover para mais perto da base da estação/roteador sem fio e veja se a rede aparece na lista depois de um tempo.</p>
 </item>

 <item>
  <p>A lista de redes sem fio leva um tempo para se atualizar. Se você acaba de ligar o seu computador ou de se mudar para um local diferente, aguarde mais ou menos um minuto e depois verifique se a rede apareceu na lista.</p>
 </item>

 <item>
  <p>A rede pode estar oculta. Você precisa <link xref="net-wireless-hidden">conectar-se de uma maneira diferente</link> se ela for uma rede oculta.</p>
 </item>
</list>

</page>
