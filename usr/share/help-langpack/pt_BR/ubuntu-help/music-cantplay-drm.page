<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="music-cantplay-drm" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="media#music"/>

    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>O suporte a esse formato de arquivo pode não estar instalado ou as músicas podem estar protegidas contra cópias.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Eu não consigo reproduzir as músicas que comprei em uma loja virtual</title>

<p>Se você baixou algumas músicas de uma loja online, é possível que elas não toquem em seu computador, especialmente se você as comprou em um computador Windows ou Mac OS X e, em seguida, as copiou.</p>

<p>Isso acontece porque a música está em um formato que seu computador não reconhece. Para ser capaz de reproduzir uma música, você precisa instalar o suporte aos formatos de áudio corretos. Por exemplo, se você quiser reproduzir arquivos MP3, você precisa instalar o suporte a MP3. Se você não tiver suporte a um dado formato de áudio, você deverá receber uma mensagem dizendo isso quando for tentar tocar uma música. A mensagem deverá, ainda, prover instruções sobre como instalar o suporte para aquele formato.</p>

<p>Se você tiver instalado o suporte para o formato de áudio de uma música mas ainda assim não conseguir reproduzi-la, é possível que a música esteja <em>protegida contra gravação</em> (também conhecido como <em>restrição por DRM</em>). DRM é uma forma de restringir quem pode reproduzir uma música e em quais aparelhos. A empresa que vendeu a música a você tem o controle sobre isso, e não você. Se um arquivo de música possui restrições por DRM, você provavelmente não será capaz de reproduzi-la, porque você precisa de aplicativos especiais do fabricante para reproduzir arquivos com restrição por DRM, mas esses aplicativos geralmente não são suportados pelo Linux.</p>

<p>Você poderá aprender mais sobre DRM  em <link href="http://www.eff.org/issues/drm">Electronic Frontier Foundation</link>.</p>

</page>
