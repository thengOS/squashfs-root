<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-volume" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="media#sound"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <desc>Ajuste o volume do som para o computador e controle o volume de cada aplicativo.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Altere o volume de som</title>

<p>Para alterar o volume do som, clique no <gui>menu de som</gui> na <gui>barra de menus</gui> e mova o controle de volume para a esquerda ou direita. Você pode desligar completamente o som clicando em <gui>Mudo</gui>.</p>

  <p>Alguns teclados têm teclas que lhe permitem controlar o volume. Normalmente elas se parecem com alto-falantes estilizados com ondas saindo deles. Elas costumam ficar próximas às teclas "F" (teclas de função) na parte superior. Em teclados de laptops, ficam frequentemente nas teclas "F". Mantenha pressionada a tecla <key>Fn</key> do teclado para usá-las.</p>

  <p>Claro, se você tem alto-falantes externos, você também pode alterar o volume usando o controle de volume dos próprios alto-falantes. Alguns fones de ouvido têm um controle de volume também.</p>

<section id="apps">
 <title>Altere o volume de som para aplicativos individuais</title>
 <p>Você pode alterar o volume para um aplicativo, mas manter o volume para os outros inalterado. Isto pode ser útil se você estiver ouvindo música e navegando na Internet, por exemplo. Você pode querer desligar o volume no navegador, para que nenhum som dos sites interrompa a música.</p>
 <p>Alguns aplicativos tem controles de volume em suas janelas principais. Se seu aplicativo tem um desses, use-o para alterar o volume. Caso contrário, clique no <gui>menu de som</gui> na <gui>barra de menus</gui> e clique em <gui>Configurações de som</gui>. Vá para a guia <gui>Aplicativos</gui> e altere o volume do aplicativo por lá.</p>
 <p>Somente os aplicativos que estão reproduzindo sons serão listados. Se um aplicativo está reproduzindo sons, mas não é listado, ele não deve suportar essa funcionalidade que lhe permite controlar o volume desta forma. Nesse caso, você não poderá mudar o seu volume.</p>
</section>

</page>
