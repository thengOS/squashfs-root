<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-install-flash" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="net-browser"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision version="16.04" date="2016-03-14" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Você pode precisar instalar o Flash para ser capaz de ver sites como o YouTube, que apresenta vídeos e páginas web interativas.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Instalar o plug-in Flash</title>

<p>O <app>Flash</app> é um <em>plug-in</em> para o seu navegador de Internet que permite que você assista vídeos e use páginas da Web interativas em alguns sites. Alguns sites não funcionarão sem Flash.</p>

<p>Se você não tiver o Flash instalado, provavelmente será exibida uma mensagem dizendo isto quando você visita uma página que precisa dele. O Flash está disponível para download gratuito (mas não de código-aberto) para a maioria dos navegadores.</p>

<steps>
 <title>Como instalar o Flash</title>
 <item>
  <p>Click <link href="apt:flashplugin-installer">this link</link> to launch an install option window.</p>
 </item>
 <item>
  <p>If you choose to install Flash, click <gui>Install</gui>.</p>
 </item>
 <item>
  <p>Se você tiver com alguma janela do navegador aberta, feche-a e volte a abri-la. O navegador irá detectar que o Flash foi instalado quando você abri-lo novamente, e então você poderá ver sites com Flash.</p>
 </item>
</steps>

<section id="alternatives">
 <title>Alternativas em código aberto para o Flash</title>
 <p>Algumas alternativas livres e de código-aberto para Flash estão disponíveis. Elas tendem a funcionar melhor que o plug-in do Flash em alguns casos (por exemplo, por manipularem melhor a reprodução de som), mas pior em outros (por exemplo, por não serem capazes de exibir algumas páginas em Flash mais complicadas).</p>
 <p>Talvez você queira tentar alguma dessas alternativas se você está insatisfeito com o reprodutor de Flash proprietário ou se você deseja usar tantos programas de código aberto quanto possível no seu computador. Aqui estão algumas das opções:</p>
 <list>
  <item><p><link href="apt:browser-plugin-gnash">Gnash</link></p></item>
  <item><p><link href="apt:browser-plugin-lightspark">LightSpark</link></p></item>
 </list>
</section>

</page>
