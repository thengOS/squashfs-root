<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batterylife" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="power-hibernate"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <desc>Dicas para reduzir o consumo de energia do seu computador.</desc>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Consuma menos energia e melhore a duração da bateria</title>

<p>Computadores podem utilizar bastante energia. Usando algumas simples estratégias de economia de energia, você pode reduzir a sua conta de luz e ajudar o meio ambiente. Se você tem um laptop, isto vai ajudar também a aumentar a quantidade de tempo que ele pode funcionar com a energia da bateria.</p>

<section id="general">

<title>Dicas gerais</title>
<list>
  <item>
    <p><link xref="shell-exit#suspend">Suspenda o seu computador</link> quando você não estiver usando. Isto reduz significativamente a quantidade de energia que o computador utiliza, e ele pode ser acordado rapidamente.</p>
  </item>
  <item>
    <p><link xref="shell-exit#shutdown">Turn off</link> the computer when you
    will not be using it for longer periods. Some people worry that turning off
    a computer regularly may cause it to wear out faster, but this is not the
    case.</p>
  </item>
  <item>
    <p>Use as preferências de <gui>Energia</gui> nas <app>Configurações do sistema</app> para alterar suas configurações de energia. Há uma série de opções que irão ajudar a economizar energia: você pode <link xref="display-dimscreen">escurecer automaticamente</link> a tela depois de um certo tempo; <link xref="display-dimscreen">reduzir o brilho da tela</link> (para computadores portáteis); e fazer o computador <link xref="power-suspend">suspender automaticamente</link> se não estiver em uso por um período de tempo.</p>
  </item>
  <item>
    <p>Turn off any external devices (like printers and scanners) when you are
    not using them.</p>
  </item>
 </list>
</section>

<section id="laptop">
 <title>Laptops, netbooks e outros dispositivos com baterias</title>

 <list>
   <item>
     <p><link xref="display-dimscreen">Reduza o brilho da tela</link>: a energia fornecida para a tela compõe uma fração significativa do consumo de energia de um computador portátil.</p>
     <p>A maioria dos laptops têm botões no teclado (ou atalhos de teclado) que você pode usar para reduzir o brilho.</p>
   </item>
   <item>
     <p>If you do not need an Internet connection for a little while, turn off
     the wireless/Bluetooth card. These devices work by broadcasting radio
     waves, which takes quite a bit of power.</p>
     <p>Alguns computadores têm um interruptor físico que pode ser usado para desligar esses dispositivos, ao passo que outros têm um atalho no teclado que pode ser usado em seu lugar. Você pode ligar de novo quando precisar.</p>
   </item>
 </list>
</section>

<section id="advanced">
 <title>Mais dicas avançadas</title>

 <list>
   <item>
     <p>Reduza o número de tarefas que estão rodando em segundo plano. Computadores usam mais energia quando têm mais trabalhos para executar.</p>
     <p>A maioria dos aplicativos em execução fazem muito pouco quando você não os estiver utilizando ativamente. Entretanto, aplicativos que buscam dados na Internet com frequência, ou que reproduzem música ou vídeos, podem impactar o consumo de energia.</p>
   </item>
 </list>
</section>

</page>
