<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="unity-introduction" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="index" group="unity-introduction"/>
    <link type="guide" xref="shell-overview" group="#first"/>
    
    <desc>Uma introdução visual ao desktop Unity.</desc>
    
    <revision version="13.10" date="2013-10-23" status="outdated"/>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit> 

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>
  
  <title>Bem-vindo ao Ubuntu</title>

<p>O Ubuntu possui o <em>Unity</em>, uma maneira reinventada de utilizar seu computador. O Unity é projetado para minimizar distrações, dar a você mais espaço para trabalhar e ajudá-lo a fazer suas coisas.</p>

<p>This guide is designed to answer your questions about using Unity and your Ubuntu desktop. 
First we will take a moment to look at some of Unity's key features, and how you can use them.</p>

<section id="unity-overview">
  <title>Primeiros passos com Unity</title>

<media type="image" src="figures/unity-overview.png">
  <p>A área de trabalho do Unity</p>
</media>

<section id="launcher-home-button">
<title>O Lançador</title>

<media type="image" src="figures/unity-launcher.png" style="floatstart floatleft">
  <p>O Lançador</p>
</media>

<p>O <gui>lançador</gui> aparece automaticamente quando você inicia sua área de trabalho, e lhe dá acesso rápido aos aplicativos que você usa com mais frequência.</p>

<list style="compact">
  <item><p><link xref="unity-launcher-intro">saiba mais sobre o lançador.</link></p></item>
</list>

</section>

<section id="the-dash">
  <title>O Painel</title>

<p>O <gui>Botão do Ubuntu</gui> fica próximo ao canto superior esquerdo da tela e sempre é o primeiro item do Lançador. Se você clicar no <gui>Botão do Ubuntu</gui>, o Unity lhe mostrará um recurso adicional da área de trabalho, o <gui>Painel</gui>.</p>

<media type="image" src="figures/unity-dash.png">
  <p>O Painel do Unity</p>
</media>

<p>The <em>Dash</em> is designed to make it easier to find, open, and use applications, 
files, music, and more. For example, if you type the word "document" into the <em>Search Bar</em>, 
the Dash will show you applications that help you write and edit documents. It will also show you 
relevant folders and documents that you have been working on recently.</p>


<list style="compact">
  <item><p><link xref="unity-dash-intro">Saiba mais sobre o painel.</link></p></item>
</list>
</section>

</section>
</page>
