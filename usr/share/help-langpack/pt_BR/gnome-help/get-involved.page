<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="get-involved" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Como e onde relatar problemas com esses tópicos de ajuda.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>
  <title>Participando para melhorar este guia</title>

  <section id="bug-report">
   <title>Relatar um erro ou um melhoramento</title>
   <p>Essa documentação de ajuda é criada por uma comunidade voluntária. Você é bem-vindo para participar. Se você perceber um problema com essas páginas de ajuda (como erros de escrita, instruções incorretas ou tópicos que deveriam ser abordados mas não estão), você pode fazer um <em>relatório de erro</em>. Para informar um erro, vá para <link href="https://bugzilla.gnome.org/">bugzilla.gnome.org</link>.</p>
   <p>Você precisa registrar para que possa informar um erro e receber atualizações por e-mail sobre o status dele. Se você não tem uma conta ainda, clique no link de <gui>New Account</gui> para criar uma.</p>
   <p>Assim que você tiver uma conta, conecte-se, clique em <guiseq><gui>File a Bug</gui><gui>Core</gui><gui>gnome-user-docs</gui></guiseq>. Antes de relatar um erro, por favor leia as <link href="https://bugzilla.gnome.org/page.cgi?id=bug-writing.html">diretrizes de escrita de relatórios de erro</link> e por favor <link href="https://bugzilla.gnome.org/browse.cgi?product=gnome-user-docs">pesquise</link> pelo erro para ver se alguma coisa similar já existe.</p>
   <p>Para relatar um erro, escolha o componente no menu <gui>Component</gui>. Se você está relatando um erro da documentação, você deveria escolher o componente <gui>gnome-help</gui>. Se você não tem certeza qual componente o erro está relacionado, escolha <gui>general</gui>.</p>
   <p>Se você está pedindo ajuda sobre um tópico que você sente que não está sendo abordado, escolha <gui>enhancement</gui> no menu <gui>Severity</gui>. Preencha nas seções de Summary e Description e clique em <gui>Commit</gui>.</p>
   <p>Ao seu relatório será dado um número de ID e seu status será atualizado como sendo tratado. Obrigado por ajudar a fazer da Ajuda do GNOME melhor!</p>
   </section>

   <section id="contact-us">
   <title>Nos contate</title>
   <p>Você pode enviar um <link href="mailto:gnome-doc-list@gnome.org">e-mail</link> para a lista de discussão do GNOME docs para aprender mais sobre como se envolver com a equipe de documentação.</p>
   </section>
</page>
