<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-slowkeys" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tenha um atraso entre uma tecla ser pressionada e a sua letra aparecer na tela.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Ativando as teclas lentas</title>

  <p>Ative as <em>teclas lentas</em> se você gostaria que existisse um atraso entre o pressionar da tecla e a letra ser exibida na tela. Isso significa que você teria que manter pressionada por um tempo cada tecla que quisesse digitar antes de ela aparecer de fato. Use as teclas lentas se você pressiona acidentalmente muitas teclas de uma vez enquanto digita ou se você tem dificuldades em pressionar a tecla certa de primeira no teclado.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Universal Access</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Acessibilidade</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Pressione <gui>Assistência de digitação (AccessX)</gui> na seção <gui>Digitação</gui>.</p>
    </item>
    <item>
      <p>Switch <gui>Slow Keys</gui> to <gui>ON</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Ativar ou desativar rapidamente as teclas lentas</title>
    <p>Em <gui>Habilitar por teclado</gui>, selecione <gui>Ativar os recursos de acessibilidade do teclado</gui> para ativar ou desativar as teclas lentas no teclado. Quando esta opção está selecionada, você pode pressionar e manter assim a tecla <key>Shift</key> por oito segundos para habilitar ou desabilitar as teclas lentas.</p>
    <p>Você pode ativar e desativar as teclas lentar clicando no <link xref="a11y-icon">ícone de acessibilidade</link> na barra superior e selecionando <gui>Teclas lentas</gui>. O ícone de acessibilidade fica visível quando uma ou mais configurações de <gui>Acessibilidade</gui> foram habilitadas.</p>
  </note>

  <p>Use a barra deslizante <gui>Atraso de aceitação</gui> para controlar por quanto tempo você tem que manter pressionada uma tecla para ela ser registrada.</p>

  <p>Seu computador pode emitir um som quando você pressiona uma tecla, quando uma tecla é aceita ou quando ela é rejeitada porque você não a manteve pressionada pelo tempo suficiente.</p>

</page>
