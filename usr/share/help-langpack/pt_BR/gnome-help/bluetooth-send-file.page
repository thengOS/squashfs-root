<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="guide" style="task" version="1.0 if/1.0" id="bluetooth-send-file" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="files-share"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email its:translate="no">jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email its:translate="no">stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Compartilhe arquivos a dispositivos Bluetooth tais como seu celular.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Enviando arquivos a um dispositivo Bluetooth</title>

<if:choose>
  <if:when test="platform:unity">
    <p>Você pode enviar arquivos para dispositivos Bluetooth conectados, tais como alguns telefones celulares ou outros computadores. Alguns tipos de dispositivos não permitem a transferência de arquivos ou de tipos específicos de arquivos. Você pode fazer esses envios de três formas: usando o ícone Bluetooth na barra superior, a partir da janela de configurações de Bluetooth ou diretamente do gerenciador de arquivos.</p>
  </if:when>
  <p>You can send files to connected Bluetooth devices, such as some mobile
  phones or other computers. Some types of devices do not allow the transfer
  of files, or specific types of files. You can send files using the Bluetooth
  settings window.</p>
</if:choose>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Bluetooth</gui> to open the panel.</p>
    </item>
    <item>
      <p>Certifique-se de que o Bluetooth está habilitado: o alternador na barra de título deveria estar definido em <gui>ON</gui>.</p>
    </item>
    <item>
      <p>Na lista <gui>Dispositivos</gui>, selecione o dispositivo ao qual os arquivos serão enviados. Se o dispositivo desejado não é mostrado como <gui>Conectado</gui> na lista, você precisa se <link xref="bluetooth-connect-device">conectar</link> a ele.</p>
      <p>Um painel específico para o dispositivo externo aparecerá.</p>
    </item>
    <item>
      <p>Click <gui>Send Files…</gui> and the file chooser will appear.</p>
    </item>
    <item>
      <p>Escolha o arquivo que deseja enviar e clique em <gui>Selecionar</gui>.</p>
      <p>Para enviar mais de um arquivo de uma mesma pasta, mantenha <key>Ctrl</key> pressionado enquanto seleciona cada arquivo.</p>
    </item>
    <item>
      <p>The owner of the receiving device usually has to press a button to
      accept the file. The <gui>Bluetooth File Transfer</gui> dialog will show
      the progress bar. Click <gui>Close</gui> when the transfer is
      complete.</p>
    </item>
  </steps>

</page>
