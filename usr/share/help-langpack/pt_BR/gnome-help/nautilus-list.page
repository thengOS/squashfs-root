<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-list" xml:lang="pt-BR">

  <info>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="nautilus-prefs" group="nautilus-list"/>

    <revision pkgversion="3.5.92" date="2012-09-19" status="review"/>
    <revision pkgversion="3.14.0" date="2014-09-23" status="review"/>
    <revision pkgversion="3.18" date="2014-09-30" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Controle que informação é exibida nas colunas na visão de lista.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Files list columns preferences</title>

  <p>There are eleven columns of information that you can display in the
  <gui>Files</gui> list view. Click <gui>Files</gui> in the top bar, pick
  <gui>Preferences</gui> and choose the <gui>List Columns</gui> tab to select
  which columns will be visible.</p>

  <note style="tip">
    <p>Use the <gui>Move Up</gui> and <gui>Move Down</gui> buttons to choose
    the order in which the selected columns will appear. Click <gui>Reset to
    Default</gui> to undo any changes and return to the default columns.</p>
  </note>

  <terms>
    <item>
      <title><gui>Nome</gui></title>
      <p>The name of folders and files.</p>
      <note style="tip">
        <p>The <gui>Name</gui> column cannot be hidden.</p>
      </note>
    </item>
    <item>
      <title><gui>Tamanho</gui></title>
      <p>O tamanho de uma pasta é dada como o número de itens contidos na pasta. O tamanho de um arquivo é dado em bytes KB ou MB.</p>
    </item>
    <item>
      <title><gui>Tipo</gui></title>
      <p>Exibido como pasta ou um tipo de arquivo, como um documento PDF, imagem JPEG, áudio MP3 e mais.</p>
    </item>
    <item>
      <title><gui>Modificado</gui></title>
      <p>Gives the date of the last time the file was modified.</p>
    </item>
    <item>
      <title><gui>Dono</gui></title>
      <p>O nome do usuário dono da pasta ou arquivo.</p>
    </item>
    <item>
      <title><gui>Grupo</gui></title>
      <p>The group the file is owned by. Each user is normally in their own
      group, but it is possible to have many users in one group. For example, a
      department may have their own group in a work environment.</p>
    </item>
    <item>
      <title><gui>Permissões</gui></title>
      <p>Displays the file access permissions. For example,
      <gui>drwxrw-r--</gui></p>
      <list>
        <item>
          <p>The first character is the file type. <gui>-</gui> means regular
          file and <gui>d</gui> means directory (folder). In rare cases, other
          characters can also be shown.</p>
        </item>
        <item>
          <p>Os próximos três caracteres <gui>rwx</gui> especificam permissões para o usuário que é dono do arquivo.</p>
        </item>
        <item>
          <p>Os próximos três <gui>rw-</gui> especificam permissões para todos os membros do grupo de que é dono do arquivo.</p>
        </item>
        <item>
          <p>Os últimos três caracteres da coluna <gui>r--</gui> especificam permissões para todos os usuários no sistema.</p>
        </item>
      </list>
      <p>Each permission has the following meanings:</p>
      <list>
        <item>
          <p><gui>r</gui>: readable, meaning that you can open the file or
          folder</p>
        </item>
        <item>
          <p><gui>w</gui>: writable, meaning that you can save changes to it</p>
        </item>
        <item>
          <p><gui>x</gui>: executable, meaning that you can run it if it is a
          program or script file, or you can access subfolders and files if it
          is a folder</p>
        </item>
        <item>
          <p><gui>-</gui>: permission not set</p>
        </item>
      </list>
    </item>
    <item>
      <title><gui>Tipo MIME</gui></title>
      <p>Exibe o tipo MIME do item.</p>
    </item>
    <item>
      <title><gui>Local</gui></title>
      <p>O caminho do local do arquivo.</p>
    </item>
    <item>
      <title><gui>Modified – Time</gui></title>
      <p>Informa a data e hora da última vez em que o arquivo foi modificado.</p>
    </item>
    <item>
      <title><gui>Acessado</gui></title>
      <p>Gives the date or time of the last time the file was modified.</p>
    </item>
  </terms>

</page>
