<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="clock-world" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="clock" group="#last"/>
    <link type="seealso" href="help:gnome-clocks/index">
      <title>Clocks Help</title>
    </link>

    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhill@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Display times in other cities under the calendar.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Add a world clock</title>

  <p>Use <app>Clocks</app> to add times in other cities.</p>

  <note>
    <p>This requires the <app>Clocks</app> application to be installed.</p>
    <p>Most distributions come with <app>Clocks</app> installed by default.
    If yours does not, you may need to install it using your distribution
    package manager.</p>
 </note>

  <p>To add a world clock:</p>

  <steps>
    <item>
      <p>Clique no relógio na barra superior.</p>
    </item>
    <item>
      <p>Click the <gui>Add world clocks...</gui> link under the calendar to
      launch <app>Clocks</app>.</p>

    <note>
       <p>If you already have one or more world clocks, click on one and
       <app>Clocks</app> will launch.</p>
    </note>

    </item>
    <item>
      <p>In the <app>Clocks</app> window, click
      <gui style="button">New</gui> button or
      <keyseq><key>Ctrl</key><key>N</key></keyseq> to add a new city.</p>
    </item>
    <item>
      <p>Start typing the name of the city into the search.</p>
    </item>
    <item>
      <p>Select the correct city or the closest location to you from the
      list.</p>
    </item>
    <item>
      <p>Press <gui style="button">Add</gui> to finish adding the city.</p>
    </item>
  </steps>

  <p>Refer to the <link href="help:gnome-clocks">Clocks Help</link> for more
  of the capabilities of <app>Clocks</app>.</p>

</page>
