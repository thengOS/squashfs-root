<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-howtoimport" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-assignprofiles"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email its:translate="no">richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Perfis de cores podem ser importados abrindo-os.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Como importo perfis de cores?</title>

  <p>Você pode importar um perfil de cores clicando duas vezes em um arquivo <file>.ICC</file> ou <file>.ICM</file> no navegador de arquivos.</p>

  <p>Alternativamente, você pode gerenciar seus perfis de cores por meio do painel <gui>Cor</gui>.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Color</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Cor</gui> para abrir o painel e selecione seu dispositivo.</p>
    </item>
    <item>
      <p>Clique em <gui>Adicionar perfil</gui> para selecionar um perfil existente ou importar um novo perfil.</p>
    </item>
    <item>
      <p>Pressione <gui>Adicionar</gui> para confirmar sua seleção.</p>
    </item>
  </steps>

  <p>O fabricante do seu monitor pode fornecer um perfil que você possa usar. Esses perfis são normalmente feitos para um monitor padrão, de forma que pode não ser perfeito para o seu especificamente. Para a melhor calibração, você deveria <link xref="color-calibrate-screen">criar seu próprio perfil</link> usando um colorímetro ou um espectrofotômetro.</p>

</page>
