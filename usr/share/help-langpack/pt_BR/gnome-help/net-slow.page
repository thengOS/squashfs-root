<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-slow" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Outras coisas podem estar baixando, você pode ter uma conexão ruim ou pode ser um horário concorrido do dia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>A Internet parece estar lenta</title>

  <p>Se você está usando a internet e ela parece estar lenta, pode haver várias coisas causando essa lentidão.</p>

  <p>Tente fechar seu navegador web e, então, reabri-lo, e se desconectar da internet e, então, conectar novamente. (ao fazer isso, está se reiniciando várias coisas que poderia estar tornando a internet ficar lenta.)</p>

  <list>
    <item>
      <p><em style="strong">Hora ocupada do dia</em></p>
      <p>Provedores de Serviço de Internet (ISP) normalmente configuram conexões internet de forma que elas são compartilhadas entre diversas residências. Apesar de conectarem separadamente, cada um por sua própria linha telefônica ou conexão por cabo, a conexão para o resto da Internet na troca telefônica pode ser, na verdade, compartilhada. Se esse for o caso e se muitos vizinhos estão usando a internet ao mesmo tempo que você está, você pode sentir uma lentidão. Você muito provavelmente perceberá isso nos momentos em que seus vizinhos estiverem na internet (no final da tarde, por exemplo).</p>
    </item>
    <item>
      <p><em style="strong">Baixando muitas coisas de uma só vez</em></p>
      <p>Se você ou outra pessoa usando sua conexão internet está baixando vários arquivos de uma só vez ou vendo vídeos, a conexão internet pode não ser rápida o suficiente para aguentar a demanda. Neste caso, ela parecerá estar mais lenta.</p>
    </item>
    <item>
      <p><em style="strong">Conexão instável</em></p>
      <p>Algumas conexões internet são simplesmente não confiáveis, especialmente aquelas temporárias ou aquelas em áreas de alta demanda. Se você está em um web café cheio ou um centro de conferência, a conexão internet pode estar muito ocupada ou simplesmente não confiável.</p>
    </item>
    <item>
      <p><em style="strong">Sinal fraco de conexão sem fio</em></p>
      <p>Se você está conectado à Internet por meio de sem fio (Wi-Fi), verifique se o ícone da rede na barra superior para ver se você tem um bom sinal de rede sem fio. Se não, a internet pode ficar lenta porque você não tem um sinal muito forte.</p>
    </item>
    <item>
      <p><em style="strong">Usando uma conexão de internet móvel lenta</em></p>
      <p>Se você tem uma conexão internet móvel e reparou que ela está lenta, você pode ter entrar em uma área na qual a recepção de sinal é fraca. Quando isso acontecer, a conexão internet vai trocar automaticamente de uma conexão de "banda larga móvel" rápida tipo 3G para uma mais confiável, mas lenta, como GPRS.</p>
    </item>
    <item>
      <p><em style="strong">Navegador web tem um problema</em></p>
      <p>Os navegadores web algumas vezes encontram um problema que os deixam lentos. Isso pode ser por quaisquer dos motivos - você pode ter visitado um site no qual o navegador se esforçou para carregar ou você pode ter o navegador aberto por muito tempo, por exemplo. Tente fechar todas as janelas do navegador e, então, abrir o navegador novamente para ver se isso faz diferença.</p>
    </item>
  </list>

</page>
