<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-connect" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="guide" xref="sharing"/>

    <revision pkgversion="3.6.0" date="2012-10-06" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Veja e edite arquivos em outro computador via FTP, SSH, compartilhamentos Windows ou WebDAV.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>Navegando nos arquivos em um servidor ou compartilhamento de rede</title>

<p>Você pode conectar a um servidor ou compartilhamento de rede para navegar e ver arquivos naquele servidor, exatamente como se eles estivessem no seu próprio computador. Esta é uma forma conveniente de fazer baixar e enviar de arquivos na internet ou para compartilhar arquivos com outras pessoas em sua rede local.</p>

<p>To browse files over the network, open the <app>Files</app>
application from the <gui>Activities</gui> overview, and click
<gui>Other Locations</gui> in the sidebar. The file manager
will find any computers on your local area network that advertize
their ability to serve files. If you want to connect to a server
on the internet, or if you do not see the computer you're looking
for, you can manually connect to a server by typing in its
internet/network address.</p>

<steps>
  <title>Conectar a um servidor de arquivos</title>
  <item><p>In the file manager, click <gui>Other Locations</gui> in the
   sidebar.</p>
  </item>
  <item><p>In <gui>Connect to Server</gui>, enter the address of the server, in
  the form of a
   <link xref="#urls">URL</link>. Details on supported URLs are
   <link xref="#types">listed below</link>.</p>
  <note>
    <p>Se você conectou ao servidor antes, você vai clicar nele na lista de <gui>Servidores recentes</gui>.</p>
  </note>
  </item>
  <item>
    <p>Click <gui>Connect</gui>. The files on the server will be shown. You
    can browse the files just as you would for those on your own computer. The
    server will also be added to the sidebar so you can access it quickly in
    the future.</p>
  </item>
</steps>

<section id="urls">
 <title>Escrevendo URLs</title>

<p>Uma <em>URL</em>, ou <em>uniform resource locator</em>, é uma forma de endereço que se refere a uma localização ou arquivo em uma rede. O endereço é formatado assim:</p>
  <example>
    <p><sys>esquema://servidor.exemplo.com/pasta</sys></p>
  </example>
<p>O <em>esquema</em> especifica o protocolo ou tipo de servidor. A porção <em>exemplo.com</em> do endereço é chamado o <em>nome de domínio</em>. Se um nome de usuário for exigido, ele deve ser inserido antes do nome do servidor:</p>
  <example>
    <p><sys>esquema://usuario@servidor.exemplo.com/pasta</sys></p>
  </example>
<p>Alguns esquemas precisam que o número da porta seja especificado. Insira-o o nome do domínio:</p>
  <example>
    <p><sys>esquema://servidor.exemplo.com:porta/pasta</sys></p>
  </example>
<p>Seguem abaixo exemplos específicos para os diversos tipos de servidor que têm suporte.</p>
</section>

<section id="types">
 <title>Tipos de servidores</title>

<p>Você pode conectar a tipos diferentes de servidores. Alguns servidores são públicos e permitem que qualquer um se conecte. Outros servidores exigem que você se conecte com um nome de usuário e senha.</p>
<p>Você pode não ter permissões para realizar certas ações em arquivos em um servidor. Por exemplo, em sites de FTP público, você provavelmente não será capaz de excluir arquivos.</p>
<p>A URL que você digita depende do protocolo que o servidor usa para exportar seus compartilhamentos de arquivos.</p>
<terms>
<item>
  <title>SSH</title>
  <p>Se você tiver uma conta de <em>shell segura</em> em um servidor, você pode se conectar usando este método. Muitas máquinas na web fornecem contas SSH para membros de forma que eles podem enviar arquivos com segurança. Os servidores SSH sempre exigem que você se conectem.</p>
  <p>Uma URL de SSH comum se parece com isso:</p>
  <example>
    <p><sys>ssh://usuario@servidor.exemplo.com/pasta</sys></p>
  </example>

  <p>Quando estiver usando SSH, todos os dados que você enviar (incluindo a sua senha) são criptografados de forma que outras pessoas na sua rede não possam vê-los.</p>
</item>
<item>
  <title>FTP (com login)</title>
  <p>FTP é uma forma popular de trocar arquivos na Internet. Por os dados não estarem criptografados sobre FTP, muitos servidores agora fornecem acesso por meio de SSH. Alguns servidores, porém, ainda permitem ou exigem que você use FTP para enviar ou baixar arquivos. Sites com FTP com autenticação normalmente vão permitir que você exclua e envie arquivos.</p>
  <p>Uma URL de FTP comum se parece com isso:</p>
  <example>
    <p><sys>ftp://usuario@ftp.exemplo.com/caminho/</sys></p>
  </example>
</item>
<item>
  <title>FTP público</title>
  <p>Sites que permitem que você baixe arquivos podem, em alguns casos, fornecer acesso FTP público ou anônimo. Esses servidores não exigem um nome de usuário e senha, e normalmente não vão permitir que você exclua ou envie arquivos.</p>
  <p>Um URL comum de FTP anônimo se parece com isso:</p>
  <example>
    <p><sys>ftp://ftp.exemplo.com/caminho/</sys></p>
  </example>
  <p>Alguns sites de FTP anônimo exigem que você se conecte com um nome de usuário e senha públicos ou com um nome de usuário público usando seu endereço de e-mail como a senha. Para esses servidores, use o método de <gui>FTP (com login)</gui> e use as credenciais especificadas por um site FTP.</p>
</item>
<item>
  <title>Compartilhamento Windows</title>
  <p>Computadores com Windows usam um protocolo proprietário para compartilhar arquivos em uma rede local. Computadores em uma rede Windows em algumas situações são agrupadas em <em>domínios</em> para organização e para melhorar o controle de acesso. Se você tem as permissões de acesso à máquina remota, você pode se conectar a um compartilhamento Windows a partir do gerenciador de arquivos.</p>
  <p>Uma URL de compartilhamento Windows comum se parece com isso:</p>
  <example>
    <p><sys>smb://servidor/Compartilhamento</sys></p>
  </example>
</item>
<item>
  <title>WebDAV e WebDAV seguro</title>
  <p>Baseado no protocolo HTTP usado na web, WebDAV algumas vezes é usado para compartilhar arquivos em uma rede local e também para armazenar arquivos na internet. Se o servidor ao qual você está se conectando tem suporte a conexões seguras, você deveria escolher esta opção. WebDAV seguro usa criptografia SSL forte, de forma que outros usuários não conseguem ver sua senha.</p>
  <p>A WebDAV URL looks like this:</p>
  <example>
    <p><sys>dav://example.hostname.com/path</sys></p>
  </example>
</item>
<item>
  <title>NFS share</title>
  <p>UNIX computers traditionally use the Network File System protocol to
  share files over a local network. With NFS, security is based on the UID of
  the user accessing the share, so no authentication credentials are
  needed when connecting.</p>
  <p>A typical NFS share URL looks like this:</p>
  <example>
    <p><sys>nfs://servername/path</sys></p>
  </example>
</item>
</terms>
</section>

</page>
