<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" version="1.0 if/1.0" id="bluetooth-turn-on-off" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-21" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email its:translate="no">jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Habilite ou desabilite o dispositivo Bluetooth no seu computador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>Ligando ou desligando o Bluetooth</title>

<if:choose>
  <if:when test="platform:unity">
    <p>You can turn Bluetooth on to connect to other Bluetooth devices, or turn
    it off to conserve power. To turn Bluetooth on, click the Bluetooth icon in
    the menu bar and switch <gui>Bluetooth</gui> to <gui>ON</gui>.</p>
  </if:when>

  <p>You can turn Bluetooth on to connect to other Bluetooth devices, or turn
  it off to conserve power. To turn Bluetooth on:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Bluetooth</gui> to open the panel.</p>
    </item>
    <item>
      <p>Set the switch at the top to <gui>ON</gui>.</p>
    </item>
  </steps>
</if:choose>

<if:choose>
  <if:when test="platform:unity">
    <p>Muitos computadores portáteis têm um interruptor ou uma combinação de teclas para ligar e desligar o Bluetooth. Se estiver desligado, você não verá o ícone Bluetooth na barra de menu. Procure por um interruptor no seu computador ou por uma tecla em seu teclado. Frequentemente, a tecla é acessada com a ajuda da tecla <key>Fn</key>.</p>
  </if:when>
  <p>Muitos computadores portáteis têm um interruptor ou uma combinação de teclas para ligar e desligar o Bluetooth. Procure por um interruptor no seu computador ou por uma tecla em seu teclado. Frequentemente, a tecla é acessada com a ajuda da tecla <key>Fn</key>.</p>
</if:choose>

<if:choose>
  <if:when test="platform:unity">
    <p>To turn Bluetooth off, click the Bluetooth icon and switch
    <gui>Bluetooth</gui> to <gui>OFF</gui>.</p>
  </if:when>
  <p>Para ligar ou desligar o Bluetooth:</p>
  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#yourname">system menu</gui> from the
      right side of the top bar.</p>
    </item>
    <item>
      <p>Select
      <gui><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/>
      Not In Use</gui>. The Bluetooth section of the menu will expand.</p>
    </item>
    <item>
      <p>Selecione <gui>Desligar</gui>.</p>
    </item>
  </steps>
</if:choose>

<if:choose>
  <if:when test="platform:unity">
    <note><p>Your computer is <link xref="bluetooth-visibility">visible</link>
    if <gui>Visible</gui> is switched to <gui>ON</gui> in the Bluetooth
    menu.</p></note>
  </if:when>
  <note><p>Seu computador fica <link xref="bluetooth-visibility">visível</link> enquanto o painel <gui>Bluetooth</gui> está aberto.</p></note>
</if:choose>

</page>
