<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-assignprofiles" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="color"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email its:translate="no">richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Procure em <guiseq><gui>Configurações</gui><gui>Cor</gui></guiseq> para adicionar um perfil de cor para sua tela.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Como atribuo perfis a dispositivos?</title>

  <p>Você pode querer atribuir um perfil de cores para sua tela ou impressora de forma que as cores que ela mostra são mais precisas.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Color</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Cor</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione o dispositivo para o qual você deseja adicionar um perfil.</p>
    </item>
    <item>
      <p>Clique em <gui>Adicionar perfil</gui> para selecionar um perfil existente ou importar um novo perfil.</p>
    </item>
    <item>
      <p>Pressione <gui>Adicionar</gui> para confirmar sua seleção.</p>
    </item>
  </steps>

  <p>Cada dispositivo pode ter múltiplos perfis a ele associados, mas somente um perfil pode ser o <em>padrão</em>. O perfil padrão é usado quando não existem informações extras que permitam a escolha do perfil de forma automática. Um exemplo dessa seleção automática seria se um perfil fosse criado para papel fotográfico e outro para papel comum.</p>

  <!--
  <figure>
    <desc>You can make a profile default by changing it with the radio button.</desc>
    <media type="image" mime="image/png" src="figures/color-profile-default.png"/>
  </figure>
  -->

  <p>Se o dispositivo de calibração estiver conectado, o botão <gui>Calibrar…</gui> criará um novo perfil.</p>

</page>
