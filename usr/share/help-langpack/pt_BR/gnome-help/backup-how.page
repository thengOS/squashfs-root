<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-how" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use Déjà Dup (ou algum outro aplicativo para cópia de segurança) para fazer cópias dos seus valiosos arquivos e configuração para se proteger contra perdas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>Como fazer cópia de segurança</title>

  <p>A forma mais fácil de fazer uma cópia de segurança dos seus arquivos e das suas configurações é deixar um aplicativo especializado gerenciar esse processo para você. Há uma boa diversidade de aplicativos de cópias de segurança disponíveis; por exemplo, o <app>Déjà Dup</app>.</p>

  <p>A ajuda do seu aplicativo escolhido irá guiá-lo sobre como configurar suas preferências para as cópias, e também como restaurar seus dados.</p>

  <p>Uma opção alternativa é <link xref="files-copy">copiar seus arquivos</link> para um local seguro, como um disco rígido externo, outro computador da rede ou um dispositivo USB. Seus <link xref="backup-thinkabout">arquivos pessoais</link> e suas configurações normalmente ficam na pasta pessoal, então você pode copiá-los de lá.</p>

  <p>A quantidade de dados que você pode fazer cópia de segurança é limitada ao tamanho do dispositivo de armazenamento. Se você tiver espaço no seu dispositivo de segurança, é melhor fazer a cópia de toda a pasta pessoal, com as seguintes exceções:</p>

<list>
 <item><p>Arquivos que já possuem cópias de segurança em outro lugar, como em um CD, DVD ou outra mídia removível.</p></item>
 <item><p>Files that you can recreate easily. For example, if you are a
 programmer, you do not have to back up the files that get produced when you
 compile your programs. Instead, just make sure that you back up the original
 source files.</p></item>
 <item><p>Quaisquer arquivos na pasta Lixeira. Sua pasta Lixeira pode ser localizada em <file>~/.local/share/Trash</file>.</p></item>
</list>

</page>
