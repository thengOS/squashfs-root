<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-preview" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-preview"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Controla quando as miniatures são usadas para arquivos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>Preferências de visualização do gerenciador de arquivos</title>

<p>O gerenciador de arquivos cria miniaturas de visualização de arquivos de imagem, vídeo e texto. A visualização da miniatura pode ser lenta para arquivos grandes ou por redes, de forma que você pode controlar quando as visualizações são feitas. Clique em <gui>Arquivos</gui> na barra superior, vá em <gui>Preferências</gui> e selecione a aba <gui>Visualização</gui>.</p>

<terms>
  <item>
    <title><gui>Files</gui></title>
    <p>Por padrão, todas as visualização são feitas para <gui>Apenas arquivos locais</gui>, aqueles em seu computador ou conectados a unidades externas. Você pode definir este recurso para <gui>Sempre</gui> ou <gui>Nunca</gui>. O gerenciador de arquivos pode <link xref="nautilus-connect">navegar nos arquivos em outros computadores</link> em uma área de rede local ou na Internet. Se você com frequência navega em arquivos em um área de rede local e a rede possui uma velocidade comunicação alta, então você pode querer definir a opção de visualização para <gui>Sempre</gui>.</p>
    <p>Além disso, você pode usar a configuração de <gui>Apenas para arquivos menores que:</gui> para limitar o tamanho dos arquivos visualizados.</p>
  </item>
  <item>
    <title><gui>Folders</gui></title>
    <p>Se você mostra tamanhos de arquivos em <link xref="nautilus-list">Colunas de visualização de lista</link> ou <link xref="nautilus-display#icon-captions">legendas de ícones</link>, pastas serão mostradas com uma contagem de quantos arquivos e pastas elas contém. A contagem de itens em uma pasta pode ser lenta, especialmente para pastas muito grandes ou em uma rede. Você pode desabilitar ou habilitar este recurso ou habilitá-la somente para arquivos em seu computador e em unidades externas locais.</p>
  </item>
</terms>
</page>
