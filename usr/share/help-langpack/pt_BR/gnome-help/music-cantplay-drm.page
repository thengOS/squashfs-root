<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="music-cantplay-drm" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="media#music"/>


    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Suporte àquele formato de arquivo pode não estar instalado ou as músicas podem estar "protegidas contra cópia".</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>Não consigo reproduzir aquelas músicas que eu comprei de uma loja de músicas on-line</title>

<p>Se você baixou alguma música de uma loja on-line, você pode descobrir que ela é reproduzida em seu computador, especialmente se você comprou-a em um computador com Windows ou Mac OS e, então, copiou-o para Linux.</p>

<p>Isso pode estar acontecendo, pois a música está em um formato que não é reconhecido pelo seu computador. Para ser capaz de reproduzir uma música, você precisa ter suporte aos formatos de áudio certos instalados - por exemplo, se você deseja reproduzir arquivos MP3, você precisa ter suporte a MP3 instalado. Se você não tem suporte a um determinado formato de áudio, você deve ver uma mensagem dizendo isso a você quando você tentar reproduzir uma música. A mensagem deve fornecer instruções de como instalar suporte para aquele formato, de forma que você possa reproduzi-la.</p>

<p>Se você tiver instalado suporte ao formato do áudio, mas ainda não conseguir reproduzi-lo, a música pode estar <em>protegida contra cópia</em> (também conhecida como estando <em>restrito por DRM</em>). DRM, também conhecida em português como GDD, é uma forma de restringir quem pode reproduzir uma música e em quais dispositivos ela pode ser reproduzida. A empresa que vendeu aquela música a você está no controle dela, e não você. Se um arquivo de música possuir restrições de DRM, você provavelmente não será capaz de reproduzi-la - você geralmente precisará de um software especial do fornecedor para reproduzir arquivos restringidos por DRM, mas este software geralmente não tem suporte no Linux.</p>

<p>Você pode aprender mais sobre o DRM (ou GDD) da <link href="http://www.eff.org/issues/drm">Electronic Frontier Foundation</link>.</p>

</page>
