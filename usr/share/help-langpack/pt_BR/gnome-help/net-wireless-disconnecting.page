<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email its:translate="no">jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>

    <desc>Você pode ter um sinal fraco ou a rede pode não estar permitindo que você se conecte apropriadamente.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>Por que minha conexão sem fio fica desconectando?</title>

<p>Você pode descobrir que você foi desconectado de uma rede sem fio ainda que você quisesse se manter conectado. Seu computador normalmente vai tentar se reconectar à rede assim que isso acontecer (o ícone de rede na barra superior vai exibir três pontos se ele estiver tentando se reconectar), mas isso pode ser irritante, especialmente se você estava tentando usar a internet no momento.</p>

<section id="signal">
 <title>Sinal fraco da rede sem fio</title>

 <p>Um motivo comum para ser desconectado de uma rede sem fio é que você tem um sinal fraco. Redes sem fio têm um alcance limitado, então se você está muito longe da estação base de rede sem fio você pode não conseguir manter um sinal forte o suficiente para manter uma conexão. Paredes e outros objetos entre você e a estação base também pode enfraquecer o sinal.</p>

 <p>O ícone de rede na barra superior exibe o quão forte seu sinal sem fio está. Se o sinal parece estar fraco, tente mover para mais perto da estação base de rede sem fio.</p>

</section>

<section id="network">
 <title>Conexão de rede não estabeleceu adequadamente</title>

 <p>Em certos casos, quando você se conecta a uma rede sem fio, pode parecer que você se conectou com sucesso a primeira vista, mas, então, você será desconectado em seguida. Isso normalmente acontece porque seu computador teve sucesso parcial na conexão com a rede - ele conseguiu estabelecer uma conexão, mas não conseguiu finalizar a conexão por algum motivo e, então, foi desconectado.</p>

 <p>Um possível motivo para isso é que você inseriu a palavra-chave errada para a conexão sem fio ou que seu computador não tem permissão na rede (porque a rede requer um nome de usuário para se autenticar, por exemplo).</p>

</section>

<section id="hardware">
 <title>Hardware/drivers de rede sem fio instáveis</title>

 <p>Alguns hardware de rede sem fio podem ser um pouco não confiáveis. Redes sem fio são complicadas e, portanto, placas de rede sem fio e estações bases ocasionalmente acabam tendo pequenos problemas e podem descartar a conexão. Isso é irritante, mas acontece com certa frequência com muitos dispositivos. Se você é desconectado de redes sem fio de tempo em tempo, isso pode ser o único motivo. se isso acontece com muita frequência, você poderia considerar obter um hardware diferente.</p>

</section>

<section id="busy">
 <title>Redes sem fio saturadas</title>

 <p>Redes sem fio em locais cheios (em universidades e webcafés, por exemplo) normalmente têm muitos computadores tentando se conectar a elas de uma só vez. Algumas vezes, essas redes ficam muito congestionadas e podem não conseguir lidar com todos os computadores que estão tentando se conectar e aí alguns deles são desconectados.</p>

</section>

</page>
