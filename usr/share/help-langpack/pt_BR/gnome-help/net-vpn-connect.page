<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-vpn-connect" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-wired"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <desc>Configure uma conexão VPN para uma rede local pela Internet.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>Conectando a um VPN</title>

<p>Uma VPN (ou <em>Virtual Private Network</em>, ou rede virtual privada) é uma forma de conectar a uma rede local pela internet. Por exemplo, digamos que você deseja conectar à uma rede local em seu local de trabalho enquanto você está em uma viagem de negócios. Você poderia encontrar uma conexão internet em algum lugar (como um hotel) e, então, se conectar à VPN do seu local de trabalho. Seria como se você estivesse conectado diretamente à rede do trabalho, mas a conexão de rede real seria por meio da conexão internet do hotel. Conexões VPN são normalmente <em>criptografadas</em> para prevenir pessoas de acessar rede local que você está conectando sem se autenticar.</p>

<p>Há um número de diferentes tipos de VPN. Você pode ter que instalar alguns softwares extras dependendo de qual tipo de VPN que você está conectando. Descubra os detalhes da conexão de quem está em cargo da VPN e veja qual <em>cliente VPN</em> você precisa usar. Então, vá ao aplicativo instalador de software e pesquise pelo pacote <app>NetworkManager</app>, que funciona com sua VPN (se houver um) e instale-o.</p>

<note>
 <p>Se não houver um pacote NetworkManager do seu tipo de VPN, você provavelmente vai ter que baixar e instalar algum software cliente da empresa que fornece o software VPN. Você provavelmente vai ter que seguir algumas instruções diferentes para que funcione.</p>
</note>

<p>To set up the VPN connection:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Rede</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>At the bottom of the list on the left, click the <gui>+</gui> button
      to add a new connection.</p>
    </item>
    <item>
      <p>Escolha <gui>VPN</gui> na lista de interfaces.</p>
    </item>
    <item>
      <p>Escolha qual tipo de conexão VPN que você tenha.</p>
    </item>
    <item>
      <p>Preencha os detalhes da conexão VPN e, então, pressione <gui>Adicionar</gui> assim que você tiver finalizado.</p>
    </item>
    <item>
      <p>When you have finished setting-up the VPN, open the
      <gui xref="shell-introduction#yourname">system menu</gui> from the right side of
      the top bar, click <gui>VPN off</gui> and select <gui>Connect</gui>. You
      may need to enter a password for the connection before it is established.
      Once the connection is made, you will see a lock shaped icon in the top
      bar.</p>
    </item>
    <item>
      <p>Hopefully you will successfully connect to the VPN. If not, you may
      need to double-check the VPN settings you entered. You can do this from
      the <gui>Network</gui> panel that you used to create the connection.
      Select the VPN connection from the list, then press the
<media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">settings</span></media> button to review the settings.</p>
    </item>
    <item>
      <p>To disconnect from the VPN, click the system menu on the top bar and
      click <gui>Turn Off</gui> under the name of your VPN connection.</p>
    </item>
  </steps>

</page>
