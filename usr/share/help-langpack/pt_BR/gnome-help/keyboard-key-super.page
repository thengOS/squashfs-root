<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="keyboard-key-super" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.7.91" version="0.2" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A tecla <key>Super</key> abre o panorama de <gui>Atividades</gui>. Ela normalmente fica próxima à tecla <key>Alt</key> do seu teclado.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>O que é a tecla <key>Super</key>?</title>

  <p>Quando você pressiona a tecla <key>Super</key>, o panorama de <gui>Atividades</gui> é exibido. Essa tecla é normalmente encontrada no canto inferior esquerdo do seu teclado, próximo à tecla <key>Alt</key>, e muitas vezes há um ícone do Windows. Algumas vezes, é chamada de <em>tecla "Windows"</em> ou de tecla de sistema.</p>

  <note>
    <p>Se você tiver um teclado da Apple, você terá uma tecla <key>⌘</key> (Command) em vez da tecla Windows, enquanto os Chromebooks possui uma lente de aumento</p>
  </note>

  <p>Para alterar qual tecla é usada para exibir o panorama de <gui>Atividades</gui>:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview, then
      open the <app>Settings</app>.</p>
    </item>
    <item>
      <p>Clique em <gui>Teclado</gui>.</p>
    </item>
    <item>
      <p>Clique na aba <gui style="tab">Atalhos</gui>.</p>
    </item>
    <item>
      <p>Selecione <gui>Sistema</gui> ao lado esquerdo da janela e <gui>Mostrar o panorama de atividades</gui> à direita.</p>
    </item>
    <item>
      <p>Pressione a combinação de teclas desejada.</p>
    </item>
  </steps>

</page>
