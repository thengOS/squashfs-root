<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="prob-reset" xml:lang="de">

  <info>
    <revision version="0.1" date="2013-03-03" status="draft"/>
    <link type="guide" xref="index#troubleshooting"/>
    <link type="seealso" xref="pref-keyboard-access"/>

    <credit type="author copyright">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Wie kann ich meinen <app>Terminal</app>-Bildschirm retten, wenn er eingefroren ist oder viele seltsame Symbole darauf erscheinen?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jan Arne Petersen</mal:name>
      <mal:email>jap@gnome.org</mal:email>
      <mal:years>2007</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2008-2010, 2012-2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bernd Homuth</mal:name>
      <mal:email>dev@hmt.im</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Setzen Sie Ihre <app>Terminal</app>-Ausgabe zurück</title>

  <p>Nach dem versehentlichen Betrachten einer Nicht-Text-Datei oder dem Ausführen eines Befehls kann es passieren, dass der Bildschirm von <app>Terminal</app> nicht mehr reagiert oder beim Tippen seltsame Zeichen ausgegeben werden. Sie können <app>Terminal</app> auf eine der folgenden Weisen wieder retten:</p>

  <section id="reset">
    <title>Zurücksetzen</title>

    <p><app>Terminal</app>-Bildschirm zurücksetzen.</p>

    <steps>
      <item>
        <p>Wählen Sie <guiseq><gui style="menu">Terminal</gui> <gui style="menuitem">Zurücksetzen</gui></guiseq>.</p>
      </item>
      <item>
        <p>Um die Eingabeaufforderung zu erhalten, drücken Sie <key>Eingabe</key>.</p>
      </item>
    </steps>

  </section>

  <section id="reset-clear">
    <title>Zurücksetzen und löschen</title>

    <p>Zusätzlich zum Zurücksetzen Ihres <app>Terminals</app>, können Sie mit <gui style="menuitem">Zurücksetzen und löschen</gui> den sichtbaren <app>Terminal</app>-Bildschirmplatz von sämtlicher Ausgabe löschen. Um <gui style="menuitem">Zurücksetzen und löschen</gui> in Ihrem <app>Terminal</app> auszuführen:</p>

    <steps>
      <item>
        <p>Wählen Sie <guiseq><gui style="menu">Terminal</gui> <gui style="menuitem">Zurücksetzen und löschen</gui></guiseq>.</p>
      </item>
      <item>
        <p>Um die Eingabeaufforderung zu erhalten, drücken Sie <key>Eingabe</key>.</p>
      </item>
    </steps>

    <note style="tip"><p><gui>Zurücksetzen und löschen</gui> bietet die Funktion ähnlich zum Befehl <cmd>reset</cmd> (»zurücksetzen«).</p></note>

  </section>
</page>
