<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="mail-read-receipts" xml:lang="de">

  <info>
    <desc>Aktivieren von Lesebestätigungen.</desc>
    
    <link type="guide" xref="index#mail-advanced"/>
    <link type="guide" xref="mail-account-manage-imap-plus#defaults"/>
    <link type="guide" xref="mail-account-manage-local-delivery#defaults"/>
    <link type="guide" xref="mail-account-manage-maildir-format-directories#defaults"/>
    <link type="guide" xref="mail-account-manage-mh-format-directories#defaults"/>
    <link type="guide" xref="mail-account-manage-pop#defaults"/>
    <link type="guide" xref="mail-account-manage-unix-mbox-spool-directory#defaults"/>
    <link type="guide" xref="mail-account-manage-unix-mbox-spool-file#defaults"/>
    <link type="guide" xref="mail-account-manage-usenet-news#defaults"/>

    <revision pkgversion="3.13.90" version="0.3" date="2015-03-09" status="final"/>
    <credit type="author">
      <name its:translate="no">April Gonzales</name>
      <email its:translate="no">loonycookie@gmail.com</email>
    </credit>
    <credit type="author">
      <name its:translate="no">Andre Klapper</name>
      <email its:translate="no">ak-47@gmx.net</email>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andre Klapper</mal:name>
      <mal:email>ak-47@gmx.net</mal:email>
      <mal:years>2007, 2008, 2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009-2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@gmail.com</mal:email>
      <mal:years>2009, 2010, 2011, 2012, 2013, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bernd Homuth</mal:name>
      <mal:email>dev@hmt.im</mal:email>
      <mal:years>2015.</mal:years>
    </mal:credit>
  </info>

<title>Lesebestätigungen für E-Mails</title>

<p>Lesebestätigungen sind eine Möglichkeit, dass Empfänger den Erhalt Ihrer Nachricht bestätigen. Der Empfänger hat normalerweise die Wahl, den Empfang entweder zu bestätigen oder auch nicht. Demnach ist es kein zuverlässiger Weg herauszufinden, ob Ihre E-Mails empfangen worden sind.</p>

<p>Fordern Sie Lesebestätigungen an, indem Sie im Menü des E-Mail-Editors <guiseq><gui>Optionen</gui><gui>Lesebestätigung anfordern</gui></guiseq> auswählen, oder indem Sie auf den entsprechenden Knopf in der Werkzeugleiste klicken.</p>

<p>Wie Evolution mit Lesebestätigungsanforderungen umgeht, die Sie erhalten, legen Sie fest unter <gui>Lesebestätigungen</gui> in den <link xref="mail-default-folder-locations"><gui>Vorgaben</gui></link> des jeweiligen E-Mail-Kontos.</p>

</page>
