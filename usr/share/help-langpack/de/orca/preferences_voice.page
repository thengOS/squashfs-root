<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_voice" xml:lang="de">
  <info>
    <link type="guide" xref="preferences#orca"/>
    <link type="next" xref="preferences_speech"/>
    <title type="sort">2.0 Voice</title>
    <title type="link">Voice</title>
    <desc>
      Configuring the voice used by <app>Orca</app>
    </desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>
  <title>Voice Preferences</title>
  <section id="voice_type_settings">
    <title>Voice Type Settings</title>
    <section>
      <title>Voice type</title>
      <p>
        This combo box makes it possible for you to use different voices so that
        you can better distinguish uppercase and linked text from other text,
        and on-screen text from text added by <app>Orca</app>.
      </p>
      <note style="tip">
        <title>Mehrere Stimmen konfigurieren</title>
        <p>
          For each voice you wish to configure, first select the
          voice in the <gui>Voice type</gui> combo box. Then
          configure the person, rate, pitch, and volume to be used
          for that voice.
        </p>
      </note>
    </section>
    <section>
      <title>Sprachsystem</title>
      <p>
        This combo box allows you to select your preferred speech system from
        those you have installed, such as Speech Dispatcher.
      </p>
    </section>
    <section>
      <title>Sprachsynthesizer</title>
      <p>
        This combo box allows you to select the speech synthesizer to be used
        with your chosen Speech system.
      </p>
    </section>
    <section>
      <title>Person</title>
      <p>
        This combo box allows you to choose which "person" or "speaker" should
        be used with the selected voice. For instance, you might wish to have
        David speak by default, but have hyperlinks spoken by Alice. Note that
        what you find in the <gui>Person</gui> combo box will depend on which
        speech synthesizers you have installed.
      </p>
    </section>
    <section>
      <title>Geschwindigkeit, Tonhöhe und Lautstärke</title>
      <p>
        These three left-right sliders allow you to further customize the sound
        of the person you have just selected.
      </p>
    </section>
  </section>
  <section id="global_voice_settings">
  <title>Global Voice Settings</title>
    <section id="pauses">
      <title>Sprache zwischen Pausen in einzelne Blöcke aufteilen</title>
      <p>Abhängig von den eingeschalteten Spracheigenschaften kann <application>Orca</application> einiges zu einem bestimmten Objekt sagen, wie etwa seinen Namen, seine Rolle, seinen Zustand, seine Tastaturkürzel, seine informative Botschaft und so weiter. Das Ankreuzfeld <guilabel>Sprechpausen zum Zerteilen nutzen</guilabel> veranlasst <application>Orca</application>, kurze Pausen zwischen diese Informationshäppchen einzufügen.</p>
      <p>Standardwert: aktiviert</p>
    </section>
    <section id="multicase_strings">
      <title>Zusammengesetzte Wörter einzeln vorlesen</title>
      <p>
        In some text, and especially when working with code, one often comes
        across a "word" consisting of several words with alternating case, such
        as "MultiCaseString." Speech synthesizers do not always pronounce such
        multicase strings correctly. Checking the <gui>Speak multicase strings
        as words</gui> checkbox will cause <app>Orca</app> to break a word like
        "MultiCaseString" into separate words ("Multi," "Case," and "String")
        prior to passing it along to the speech synthesizer.
      </p>
      <p>Standardwert: nicht aktiviert</p>
    </section>
    <section id="say_all_by">
      <title>Say All By</title>
      <p>
        The <gui>Say All By</gui> combo box allows you to specify whether
        <app>Orca</app> speaks a sentence at a time or a line at a time when
        doing a "Say All" of a document.
      </p>
      <p>
        Default value: <gui>Sentence</gui>
      </p>
    </section>
  </section>
</page>
