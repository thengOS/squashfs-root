<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_chat" xml:lang="de">
  <info>
    <link type="guide" xref="preferences#application"/>
    <link type="next" xref="preferences_spellcheck"/>
    <title type="sort">3. Chat</title>
    <title type="link">Chat</title>
    <desc>Einstellungen der <app>Orca</app>-Unterstützung für Sofortnachrichten und IRC</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>
  <title>Chat-Einstellungen</title>
  <p>
    The following options allow you to customize how <app>Orca</app>
    behaves when providing access to instant messaging and internet
    relay chat clients.
  </p>
  <section>
    <title>Name des Chatraums vorlesen</title>
    <p>
      If this checkbox is checked, <app>Orca</app> will prefix incoming messages
      with the name of the room or buddy they came from, unless they came from
      the currently-focused conversation.
    </p>
    <p>Standardwert: nicht aktiviert</p>
  </section>
  <section>
    <title>Anzeigen, wenn ihre Kontakte etwas eintippen</title>
    <p>
      If this checkbox is checked, and if <app>Orca</app> has sufficient
      information identifying that your buddy is typing, <app>Orca</app> will
     announce changes in typing status.
    </p>
    <p>Standardwert: nicht aktiviert</p>
  </section>
  <section>
    <title>Nachrichtenverläufe je Chatraum bereitstellen</title>
    <p>
      If this checkbox is checked, <app>Orca</app>'s commands for reviewing
      recent messages will only apply to the currently-focused conversation.
      Otherwise, the history will contain the most recent messages regardless
      of which conversation they came from.
    </p>
    <p>Standardwert: nicht aktiviert</p>
  </section>
  <section>
    <title>Nachrichten vorlesen von</title>
    <p>
      This group of radio buttons allows you to control under what circumstances
      <app>Orca</app> will present an incoming message to you. Your choices are:
    </p>
    <list>
      <item>
        <p><gui>Alle Kanäle</gui></p>
      </item>
      <item>
        <p><gui>Nur einen Kanal, wenn dessen Fenster aktiv ist</gui></p>
      </item>
      <item>
        <p><gui>Alle Kanäle, wenn irgendein Chat-Fenster aktiv ist</gui></p>
      </item>
    </list>
    <p>Standardwert: alle Kanäle</p>
  </section>
</page>
