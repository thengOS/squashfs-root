<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_speech" xml:lang="de">
  <info>
    <link type="guide" xref="preferences#orca"/>
    <link type="next" xref="preferences_braille"/>
    <title type="sort">2.1 Speech</title>
    <title type="link">Sprache</title>
    <desc>
      Configuring what gets spoken
    </desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>
  <title>Einstellungen für Sprache</title>
  <section id="enable_speech">
    <title>Sprache aktivieren</title>
    <p>
      The <gui>Enable speech</gui> check box controls whether or not
      <app>Orca</app> will make use of a speech synthesizer.
      Braille-only users will likely want to uncheck this checkbox.
    </p>
    <p>Standardwert: aktiviert</p>
  </section>
  <section id="verbosity">
    <title>Ausführlichkeit</title>
    <p>
      The <gui>Verbosity</gui> setting determines the amount of information
      that will be spoken in various situations. For example, if it is set
      to verbose, and you arrow into a word that is misspelled, <app>Orca</app>
      will announce that the word is misspelled. When the level is set to brief,
      this announcement will not be made.
    </p>
    <p>Vorgabewert: <gui>Ausführlich</gui></p>
  </section>
  <section id="table_rows">
    <title>Tabellenzeilen</title>
    <p>
      The <gui>Table Rows</gui> radio button group determines what gets
      spoken when navigating amongst rows in a table. The available
      options are <gui>speak row</gui> and <gui>speak cell</gui>.
    </p>
    <p>
      Consider the process of examining the list of messages in your Inbox.
      In order to have <app>Orca</app> announce the sender, subject, date,
      and presence of attachments you would need <gui>speak row</gui>. On
      the other hand, when navigating amongst rows in a spreadsheet, hearing
      the full row may not be desired. In that case, <gui>speak cell</gui>
      should instead be chosen.
    </p>
    <p>Vorgabewert: <gui>Zeile sprechen</gui></p>
  </section>
  <section id="punctuation_level">
    <title>Interpunktionsgrad</title>
    <p>
      The <gui>Punctuation Level</gui> radio button group is used to
      adjust the amount of punctuation spoken by the synthesizer. The
      available levels are <gui>None</gui>, <gui>Some</gui>, <gui>Most</gui>,
      and <gui>All</gui>.
    </p>
    <p>Vorgabewert: <gui>Das meiste</gui></p>
    <section>
      <title>Keiner</title>
      <p>
        Choosing a punctuation level of <gui>None</gui> would, as you expect,
        cause no punctuation to be spoken. Note, however, that special symbols
        such as subscripted and superscripted numbers, unicode fractions, and
        bullets are still spoken at this level, even though some might consider
        these types of symbols punctuation.
      </p>
    </section>
    <section>
      <title>Einige</title>
      <p>
        Choosing a punctuation level of <gui>Some</gui> causes all of the
        previously-mentioned symbols to be spoken. In addition, <app>Orca</app>
        will speak known mathematical symbols, currency symbols, and "^", "@",
        "/", "&amp;", "#".
      </p>
    </section>
    <section>
      <title>Das meiste</title>
      <p>
        Choosing a punctuation level of <gui>Most</gui> causes all of the
        previous-mentioned symbols to be spoken. In addition, <app>Orca</app>
        will speak all other known punctuation symbols <em>other than</em> "!",
        "'", ",", ".", "?".
      </p>
    </section>
    <section>
      <title>Alle</title>
      <p>
        Choosing a punctuation level of <gui>All</gui>, as expected, causes
        <app>Orca</app> to speak all known punctuation symbols.
      </p>
    </section>
  </section>
  <section id="spoken_context">
    <title>Spoken Context</title>
    <p>
      The following items control the presentation of a variety of
      supplemental, "system" information about the item with focus.
      Because the associated text does not appear on screen, this
      information is presented in <app>Orca</app>'s System voice.
    </p>
    <section id="only_speak_displayed_text">
      <title>Nur angezeigten Text vorlesen</title>
      <p>
        Checking this checkbox causes <app>Orca</app> to only speak actual
        text displayed on screen. This option is intended primarily for low
        vision users and users with a visual learning disability.
      </p>
      <p>Standardwert: nicht aktiviert</p>
      <note style="note">
        <p>Die folgenden Einstellungsmöglichkeiten sind nicht verfügbar, wenn <gui>Nur angezeigten Text vorlesen</gui> aktiviert ist.</p>
      </note>
    </section>
    <section id="speak_blank_lines">
      <title>Leerzeilen vorlesen</title>
      <p>
        If the <gui>Speak blank lines</gui> checkbox is checked,
        <app>Orca</app> will say "blank" each time you arrow to a blank
        line. If it is unchecked, <app>Orca</app> will say nothing when
        you move to a blank line.
      </p>
      <p>Standardwert: aktiviert</p>
    </section>
    <section id="indentation_and_justification">
      <title>Einrückungen und Ausrichtung vorlesen</title>
      <p>
        When working with code or editing other documents it is often
        desirable to be aware of justification and indentation. Checking
        the <gui>Speak indentation and justification</gui> checkbox will
        cause <app>Orca</app> to announce this information.
      </p>
      <p>Standardwert: nicht aktiviert</p>
    </section>
    <section id="mnemonics">
     <title>Tastenkombination eines Objektes vorlesen</title>
      <p>
        If the <gui>Speak object mnemonics</gui> checkbox is checked,
        <app>Orca</app> will announce the mnemonic associated with the
        object with focus (such as <keyseq><key>Alt</key><key>O</key>
        </keyseq> for an <gui>OK</gui> button).
      </p>
      <p>Standardwert: nicht aktiviert</p>
    </section>
    <section id="child_position">
      <title>Position des Unterelements vorlesen</title>
      <p>
        Checking the <gui>Speak child position</gui> checkbox will cause
        <app>Orca</app> to announce the position of the focused item in
        menus, lists, and trees (e.g. "9 of 16").
      </p>
      <p>Standardwert: nicht aktiviert</p>
    </section>
    <section id="speak_tutorial_messages">
      <title>Benutzungshinweise vorlesen</title>
      <p>
        If the <gui>Speak tutorial messages</gui> checkbox is checked, as
        you move amongst objects in an interface, <app>Orca</app> will
        provide additional information, such as how to interact with the
        currently-focused object.
      </p>
      <p>Standardwert: nicht aktiviert</p>
    </section>
  </section>
  <section id="progress_bar_updates">
    <title>Aktualisierungen von Fortschrittsanzeigen</title>
    <section>
      <title>Aktiviert</title>
      <p>
        If the <gui>Enabled</gui> checkbox is checked <app>Orca</app> will
        periodically present the status of progress bars.
      </p>
      <p>Standardwert: aktiviert</p>
    </section>
    <section>
      <title>Frequenz (Sekunden)</title>
      <p>
        This spin button determines how often the announcement is made.
      </p>
      <p>Standardwert: 10</p>
    </section>
    <section>
      <title>Beschränken auf</title>
      <p>
         This combo box allows you to control which progress bars should be
         presented, assuming the presentation of progress bar updates has been
         enabled. The choices are <gui>All</gui>, <gui>Application</gui>, and
         <gui>Window</gui>.
      </p>
      <p>
        Choosing <gui>All</gui> will result in <app>Orca</app> presenting
        updates for all progress bars, regardless of where the progress bars
        are located.
      </p>
      <p>
        Choosing <gui>Application</gui> will result in <app>Orca</app>
        presenting updates from progress bars in the active application, even
        if they are not in the active window.
      </p>
      <p>
         Choosing <gui>Window</gui> will result in <app>Orca</app> only
         presenting updates for progress bars in the active window.
      </p>
      <p>Vorgabewert: <gui>Anwendung</gui></p>
    </section>
  </section>
</page>
