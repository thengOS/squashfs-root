<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepassword" xml:lang="de">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Halten Sie Ihr Konto sicher, indem Sie Ihr Passwort häufig in den Benutzereinstellungen ändern.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Ändern Ihres Passworts</title>

  <p>It is a good idea to change your password from time to time, especially if
  you think someone else knows your password.</p>

  <p>Sie benötigen <link xref="user-admin-explain">Systemverwalterrechte</link>, um fremde Benutzerkonten zu bearbeiten.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Users</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Click the label <gui>·····</gui> next to <gui>Password</gui>. If you
      are changing the password for a different user, you will first need to
      <gui>Unlock</gui> the panel.</p>
    </item>
    <item>
      <p>Enter your current password, then a new password. Enter your new
      password again in the <gui>Verify New Password</gui> field.</p>
      <p>You can press the <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generate password</span></media></gui> icon to
      automatically generate a random password.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Ändern</gui>.</p>
    </item>
  </steps>

  <p>Stellen Sie sicher, dass Sie <link xref="user-goodpassword">ein gutes Passwort gewählt haben</link>. Das hilft Ihnen, die Sicherheit Ihres Benutzerkontos zu gewährleisten.</p>

  <note>
    <p>When you update your login password, your login keyring password will
    automatically be updated to be the same as your new login password.</p>
  </note>

  <p>Wenn Sie Ihr Passwort vergessen, kann jeder Benutzer mit Systemverwalterrechten es für Sie ändern.</p>

</page>
