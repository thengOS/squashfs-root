<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="power-batteryestimate" xml:lang="de">

  <info>

    <link type="guide" xref="power#battery"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Die beim Anklicken des <gui>Akku-Symbols</gui> angezeigte Nutzungsdauer des Akkus ist eine Schätzung.</desc>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

<title>Die geschätzte Akkulaufzeit ist falsch</title>

<p>Wenn Sie die verbleibende Akkulaufzeit überprüfen, kann es sein, dass Sie feststellen, dass die angezeigte verbleibende Zeit nicht mit der tatsächlichen Akkulaufzeit übereinstimmt. Dies kommt vor, da es sich bei der verbleibenden Akkulaufzeit um eine Schätzung handelt. Die Schätzungen sollten allerdings mit der Zeit besser werden.</p>

<p>In order to estimate the remaining battery life, a number of factors must be
taken into account. One is the amount of power currently being used by the
computer: power consumption varies depending on how many programs you have
open, which devices are plugged in, and whether you are running any intensive
tasks (like watching high-definition video or converting music files, for
example). This changes from moment to moment, and is difficult to predict.</p>

<p>Ein weiterer Faktor ist die Art, wie sich der Akku entlädt. Einige Akkus entladen sich schneller, umso leerer sie werden. Ohne genaue Informationen zum Entladevorgang eines Akkus ist nur eine grobe Schätzung der verbleibenden Akkulaufzeit möglich.</p>

<p>Während sich der Akku entlädt, beobachtet die Energieverwaltung den Entladevorgang und lernt, daraus bessere Schätzungen der Akkulaufzeit zu erstellen. Allerdings werden diese nie vollkommen korrekt sein.</p>

<note>
  <p>Wenn die Schätzung der Akkulaufzeit total daneben liegt (sagen wir Hunderte von Tagen), fehlen der Energieverwaltung vermutlich einige Daten, die sie zur genauen Schätzung benötigt.</p>
  <p>Wenn Sie die Stromversorgung abstellen, den Laptop einige Zeit über den Akku betreiben, das Stromkabel dann wieder anschließen und der Akku sich auflädt, sollte die Energieverwaltung die benötigten Daten erhalten.</p>
</note>

</page>
