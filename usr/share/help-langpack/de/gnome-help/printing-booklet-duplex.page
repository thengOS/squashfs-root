<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-duplex" xml:lang="de">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Print folded booklets (like a book or pamphlet) from a PDF using normal
    A4/Letter-size paper.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Print a booklet on a double-sided printer</title>

  <p>You can make a folded booklet (like a small book or pamphlet) by printing
  pages of a document in a special order and changing a couple of printing
  options.</p>

  <p>Diese Anweisungen beziehen sich auf das Drucken einer Broschüre aus einem PDF-Dokument.</p>

  <p>Falls Sie eine Broschüre aus einem <app>LibreOffice</app>-Dokument erstellen wollen, müssen Sie dieses zunächst in ein PDF umwandeln. Wählen Sie im Menü <guiseq><gui>Datei</gui><gui>Als PDF exportieren …</gui></guiseq>. Die Seitenzahl Ihres Dokuments muss ein Vielfaches von 4 sein (4, 8, 12, 16, …), wobei Sie bis zu drei leere Seiten hinzufügen müssen.</p>

  <p>So drucken Sie eine Broschüre:</p>

  <steps>
    <item>
      <p>Open the print dialog. This can normally be done through
      <gui style="menuitem">Print</gui> in the menu or using the
      <keyseq><key>Ctrl</key><key>P</key></keyseq> keyboard shortcut.</p>
    </item>
    <item>
      <p>Click the <gui>Properties...</gui> button </p>
      <p>In the <gui>Orientation</gui> drop-down list, make sure that
      <gui>Landscape</gui> is selected.</p>
      <p>In the <gui>Duplex</gui> drop-down list, select <gui>Short Edge</gui>.
      </p>
      <p>Click <gui>OK</gui> to go back to the print dialog.</p>
    </item>
    <item>
      <p>Under <gui>Range and Copies</gui>, choose <gui>Pages</gui>.</p>
    </item>
    <item>
      <p>Geben Sie die Seitennummern in dieser Reihenfolge ein, wobei n die Gesamtzahl der Seiten ist und a ein Vielfaches von 4:</p>
      <p>n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11 …</p>
      <p>Beispielsweise:</p>
      <list>
        <item><p>4-seitige Broschüre: Geben Sie <input>4,1,2,3</input> ein</p></item>
        <item><p>8-seitige Broschüre: Geben Sie <input>8,1,2,7,6,3,4,5</input> ein</p></item>
        <item><p>20-seitige Broschüre: Geben Sie <input>20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11</input> ein</p></item>
      </list>
    </item>
    <item>
      <p>Choose the <gui>Page Layout</gui> tab.</p>
      <p>Under <gui>Layout</gui>, select <gui>Brochure</gui>.</p>
      <p>Under <gui>Page Sides</gui>, in the <gui>Include</gui> drop-down list,
      select <gui>All pages</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Drucken</gui>.</p>
    </item>
  </steps>

</page>
