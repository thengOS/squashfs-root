<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="de">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <desc>Schnell wiederholtes Drücken derselben Tasten ignorieren.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Springende Tasten aktivieren</title>

  <p>Schalten Sie <em>Springende Tasten</em> ein, um das Drücken von Tasten zu ignorieren, wenn es schnell wiederholt wird. Wenn zum Beispiel Ihre Hände zittern und Sie deswegen eine Taste mehrmals drücken, obwohl Sie sie nur einmal drücken wollen, dann sollten Sie springende Tasten einschalten.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Universal Access</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Barrierefreiheit</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Öffnen Sie <gui>Tippassistent (AccessX)</gui> und wählen Sie den Reiter <gui>Texteingabe</gui>.</p>
    </item>
    <item>
      <p>Schalten Sie <gui>Springende Tasten</gui> auf <gui>AN</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Springende Tasten schnell ein- und ausschalten</title>
    <p>Sie können Springende Tasten ein- oder ausschalten, indem Sie auf das <link xref="a11y-icon">Barrierefreiheitssymbol</link> im oberen Panel klicken und <gui>Springende Tasten</gui> wählen. Das Barrierefreiheitssymbol ist sichtbar, wenn eine oder mehrere Einstellungen in den <gui>Barrierefreiheitseinstellungen</gui> aktiviert wurden.</p>
  </note>

  <p>Verwenden Sie den Schieberegler <gui>Akzeptanzverzögerung</gui>, um einzustellen, wie lange springende Tasten wartet, bis es eine Taste akzeptiert, nachdem Sie die Taste das erste Mal gedrückt hatten. Wählen Sie <gui>Signalton ausgeben, wenn eine Taste abgewiesen wird</gui>, wenn Sie den Rechner jedes Mal einen Ton ausgeben lassen wollen, wenn er einen Tastendruck ignoriert, weil er zu früh nach dem vorigen Tastendruck getätigt wurde.</p>

</page>
