<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-mode" xml:lang="de">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Wechseln Sie vom Tablett- zum Mausmodus.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Mausverfolgungsmodus des Wacom-Tabletts einstellen</title>

<p>Der <gui>Mausverfolgungsmodus</gui> legt fest, wie der Zeiger auf dem Bildschirm abgebildet wird.</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
    start typing <gui>Wacom Tablet</gui>.</p>
  </item>
  <item>
    <p>Klicken Sie auf <gui>Wacom-Grafiktablett</gui>, um das Panel zu öffnen.</p>
    <!-- TODO: document how to connet the tablet using Bluetooth/add link -->
    <note style="tip"><p>Falls kein Tablett erkannt wurde, dann werden Sie gebeten <gui>Bitte schließen Sie Ihr Wacom-Tablett an oder schalten Sie es ein</gui>. Klicken Sie auf <gui>Bluetooth-Einstellungen</gui>, um ein Funk-Tablett zu verbinden.</p></note>
  </item>
  <item><p>Wählen Sie neben <gui>Mausverfolgungsmodus</gui> entweder <gui>Tablett (absolute)</gui> oder <gui>Touchpad (relativ)</gui>.</p></item>
</steps>

<note style="info"><p>Im <em>absoluten</em> Modus entspricht jeder Punkt auf dem Tablett einem Punkt auf dem Bildschirm. Beispielsweise ist die linke obere Ecke des Bildschirms auch stets die linke obere Ecke des Tabletts.</p>
 <p>In <em>relative</em> mode, if you lift the pointer off the tablet and put it
 down in a different position, the cursor on the screen doesn't move. This is
    the way a mouse operates.</p>
  </note>

</page>
