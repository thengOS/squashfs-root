<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-batterywindows" xml:lang="de">

  <info>
    <link type="guide" xref="power#battery"/>
    <link type="seealso" xref="power-batteryestimate"/>
    <link type="seealso" xref="power-batterylife"/>
    <link type="seealso" xref="power-batteryslow"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Optimierungen des Herstellers und abweichende Schätzungen der Akkulaufzeit können dieses Problem verursachen.</desc>
    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

<title>Warum hält mein Akku nicht so lange wie unter Windows/Mac OS?</title>

<p>Einige Rechner scheinen kürzere Akkulaufzeiten aufzuweisen, wenn darauf Linux statt Windows oder Mac OS ausgeführt wird. Ein Grund dafür ist spezielle Software für Windows/Mac OS, die von Rechnerherstellern installiert wird, um verschiedene Hardware-/Software-Einstellung des vorliegenden Rechnermodells zu optimieren. Diese Anpassungen sind oft sehr speziell und eventuell nicht schriftlich festgehalten und können daher nur schwer in Linux integriert werden.</p>

<p>Leider gibt es keine einfache Möglichkeit, diese Optimierungen selbst anzuwenden, ohne sie genau zu kennen. Allerdings könnten einige <link xref="power-batterylife">einfache Energiesparfunktionen</link> nützlich sein. Verfügt Ihr Rechner über einen <link xref="power-batteryslow">geschwindigkeitsvariablen Prozessor</link>, könnte eine Anpassung der Einstellungen dieses Prozessors ebenfalls helfen.</p>

<p>Ein weiterer möglicher Grund der Abweichung ist, dass sich die Methode zur Bestimmung der Akkulaufzeit in Linux von der in Windows/Mac OS unterscheidet. Die eigentliche Akkulaufzeit kann die gleiche sein, aber die verschiedenen Methoden liefern unterschiedliche Schätzungen.</p>
	
</page>
