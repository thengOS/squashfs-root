<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="solaris-mode" xml:lang="de">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index" group="other"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Solaris-Modus verwenden, um die Anzahl der CPUs wiederzuspiegeln</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Was ist der Solaris-Modus?</title>

  <p>In einem System mit mehreren CPUs oder <link xref="cpu-multicore">Kernen</link> können Prozesse mehr als eine/einen gleichzeitig verwenden. In der Spalte <gui>% CPU</gui> können Werte auftauchen, die in Summe mehr als 100% ergeben (z.B. 400% in einem System mit 4 CPUs). <gui>Solaris-Modus</gui> teilt den Wert <gui>% CPU</gui> für jeden Prozess durch die Anzahl der CPUs im System, so dass die Summe höchstens 100% ergibt.</p>

  <p>So wird die Spalte <gui>% CPU</gui> im <gui>Solaris-Modus</gui> angezeigt:</p>

  <steps>
    <item><p>Klicken Sie auf <gui>Einstellungen</gui> im Anwendungsmenü.</p></item>
    <item><p>Klicken Sie auf den Reiter <gui>Prozesse</gui>.</p></item>
    <item><p>Wählen Sie <gui>CPU-Auslastung durch CPU-Anzahl dividieren</gui>.</p></item>
  </steps>

    <note><p>Der Begriff <gui>Solaris-Modus</gui> stammt vom UNIX von Sun, im Vergleich zum voreingestellten IRIX-Modus in Linux, benannt nach dem UNIX von SGI.</p></note>

</page>
