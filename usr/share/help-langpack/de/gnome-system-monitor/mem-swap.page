<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mem-swap" xml:lang="de">

  <info>
    <revision pkgversion="3.11" date="2014-01-28" status="final"/>
    <link type="guide" xref="index" group="memory"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Auslagerungsspeicher ermöglicht es Ihrem Rechner mehr Anwendungen gleichzeitig auszuführen, als in den Systemspeicher (RAM) passen würden. </desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Was ist »Auslagerungsspeicher«?</title>

  <p>Auslagerungsspeicher ist der Teil des virtuellen Speichersystems auf der Festplatte. Der Speicher wird bei der ersten Installation von Linux als <em>Auslagerungspartition</em> oder <em>Auslagerungsdatei</em> eingerichtet, kann aber auch später hinzugefügt werden.</p>

  <note>
    <p>Festplattenzugriff ist <em>sehr</em> langsam im Vergleich zu Speicherzugriff. Das System läuft deutlich langsamer, wenn häufiger Zugriff auf den Auslagerungsspeicher vorgenommen werden muss, weil das System nicht fähig ist genug freien Systemspeicher zu finden. In dieser Situation ist die einzige Lösung mehr RAM in Ihren Rechner einzubauen.</p>
  </note>

  <p>So stellen Sie fest, ob Auslagern zu einem Leistungsproblem auf Ihrem Rechner wird:</p>

  <steps>
    <item>
      <p>Klicken Sie auf den Reiter <gui>Ressourcen</gui>.</p>
    </item>
    <item>
      <p>Das Diagramm zur <gui>Speicher- und Auslagerungschronik</gui> zeigt Speicher- und Auslagerungsverbrauch als Prozentzahlen.</p>
    </item>
  </steps>

</page>
