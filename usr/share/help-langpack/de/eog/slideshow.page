<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="slideshow" xml:lang="de">

  <info>
   <link type="guide" xref="index#viewing"/>
   <link type="seealso" xref="preferences#slideshow"/>
   <link type="seealso" xref="plugin-slideshow-shuffle"/>
   <desc>Klicken Sie auf <guiseq><gui>Ansicht</gui><gui>Diaschau</gui></guiseq>, um alle Bilder in einem Ordner in einer Diaschau anzuzeigen.</desc>

   <revision pkgversion="3.2" version="0.1" date="2011-09-06" status="final"/>
   <credit type="author">
     <name>Tiffany Antopolski</name>
     <email>tiffany.antopolski@gmail.com</email>
   </credit>
   <license>
     <p>Creative Commons Share Alike 3.0</p>
   </license>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jan Arne Petersen</mal:name>
      <mal:email>jpetersen@jpetersen.org</mal:email>
      <mal:years>2006</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2008, 2010, 2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Felix Riemann</mal:name>
      <mal:email>friemann@svn.gnome.org</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2011, 2012, 2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Diaschau</title>

<p>So betrachten Sie die Bilder in einem Ordner als Diaschau:</p>

<steps>
 <item>
  <p><link xref="open">Öffnen</link> Sie eines der Bilder im Ordner.</p>
 </item>
 <item>
  <p>Klicken Sie auf <guiseq><gui>Ansicht</gui><gui>Diaschau</gui></guiseq> oder drücken Sie <key>F5</key>.</p>
  <p>Eine Diaschau im Vollbildmodus beginnt. In Abständen einiger Sekunden wird ein neues Bild angezeigt. Sie können mit <key>Pfeiltaste links</key> und <key>Pfeiltaste rechts</key> Bilder überspringen, wenn die Diaschau schneller ablaufen soll oder Sie zu einem bereits betrachteten Bild zurückkehren wollen.</p>
 </item>
 <item>
  <p>TUm die Diaschau zu beenden, drücken Sie <key>Esc</key> oder <key>F5</key>.</p>
 </item>
</steps>

<p>Sie können die Einstellungen der Diaschau anpassen, beispielsweise die Zeit für die Anzeige eines Bildes. Weitere Informationen finden Sie in den <link xref="preferences#slideshow">Diaschau-Einstellungen</link>.</p>

</page>
