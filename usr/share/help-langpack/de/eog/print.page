<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="print" xml:lang="de">

  <info>
    <link type="guide" xref="index#printing"/>
    <desc>Wählen Sie <guiseq><gui>Bild</gui><gui>Drucken</gui></guiseq>. Anschließend können Sie in den einzelnen Reitern die Druckeinstellungen anpassen.</desc>

    <revision pkgversion="3.2" version="0.1" date="2011-09-06" status="final"/>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jan Arne Petersen</mal:name>
      <mal:email>jpetersen@jpetersen.org</mal:email>
      <mal:years>2006</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2008, 2010, 2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Felix Riemann</mal:name>
      <mal:email>friemann@svn.gnome.org</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2011, 2012, 2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Drucken eines Bildes</title>

<p>So drucken Sie das gegenwärtig angezeigte Bild:</p>

<steps>
 <item>
  <p>Wählen Sie <guiseq><gui>Bild</gui><gui>Drucken</gui></guiseq>.</p>
 </item>
 <item>
  <p>Wählen Sie im Reiter <gui>Allgemein</gui> den zu verwendenden Drucker sowie die Anzahl der zu druckenden Kopien aus.</p>
 </item>
 <item>
  <p>Wählen Sie den Reiter <gui>Bildeinstellungen</gui>. Hier können Sie die <gui>Position</gui> und <gui>Größe</gui> Ihres Bildes anpassen.</p>
  <p>Die Position des Bildes in der Seite kann auch angepasst werden, in dem Sie es mit der Maus innerhalb der Ansicht verschieben.</p>
 </item>
 <item>
  <p>Wenn Sie qualitativ hochwertiges Fotopapier verwenden, öffnen Sie den Reiter <gui>Seite einrichten</gui> und wählen Sie den korrekten <gui>Papiertyp</gui>. Sie sollten außerdem im Reiter <gui>Bildqualität</gui> eine hohe Qualität wählen, so dass Sie die bestmögliche Fotoqualität erzielen.</p>
 </item>
 <item>
  <p>Klicken Sie auf <gui>Drucken</gui>.</p>
 </item>
</steps>

<p>Beachten Sie, dass einige der oben erwähnten Reiter nicht für alle Druckermodelle angezeigt werden. Das ergibt sich dadurch, dass die <em>Treiber</em> für jene Drucker die Änderung bestimmter Einstellungen nicht zulassen. Wenn beispielsweise der Reiter <gui>Bildqualität</gui> nicht angezeigt wird, ist das wahrscheinlich darauf zurückzuführen, dass diese Einstellungen nicht vom Druckertreiber unterstützt werden.</p>

</page>
