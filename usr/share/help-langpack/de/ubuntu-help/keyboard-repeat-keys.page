<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="keyboard-repeat-keys" xml:lang="de">
  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision version="13.10" date="2013-10-22" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
       <name>Natalia Ruz Leiva</name>
       <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <desc>Buchstaben von der Tastatur nicht wiederholt auslösen lassen, wenn Sie eine Taste gedrückt halten, oder die Verzögerung und Geschwindigkeit der Wiederholung ändern.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Wiederholte Tastendrücke abschalten</title>

  <p>Wenn Sie eine Taste auf Ihrer Tastatur gedrückt halten, wird der Buchstabe oder das Zeichen standardmäßig so lange wiederholt, bis Sie die Taste loslassen. Wenn es Ihnen schwerfällt, Ihre Finger schnell genug wieder hochzuheben, können Sie dieses Verhalten ausschalten oder Sie können ändern, wie lange es dauert, bis sich die Taste wiederholt auszulösen beginnt.</p>

  <steps>
    <item>
    <p>Klicken Sie auf das Symbol ganz rechts in der Menüleiste und wählen Sie <gui>Systemeinstellungen</gui> aus.</p>
    </item>
    <item>
     <p>Klicken Sie unter <gui>Hardware</gui> auf <gui>Tastatur</gui>.</p>
    </item>
    <item>
     <p>Schalten Sie die Einstellung <gui>Tasten wiederholt auslösen, wenn sie gedrückt gehalten werden</gui> aus, um wiederholt ausgelöste Tasten komplett zu deaktivieren.</p>
     <p>Alternativ können Sie den Schieberegler <gui>Verzögerung</gui> anpassen, um einzustellen, wie lange Sie eine Taste gedrückt halten müssen, bevor sie wiederholt ausgelöst wird; stellen Sie den Schieberegler <gui>Geschwindigkeit</gui> ein, um festzulegen, wie schnell die Tasten sich wiederholen.</p>
    </item>
  </steps>
</page>
