<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="disk-format" xml:lang="de">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <desc>Entferne alle Dateien und Ordner auf der externen Festplatte oder des USB-Sticks durch Formatieren.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Alles von einem Wechseldatenträger löschen</title>

  <p>Wenn Sie einen Wechseldatenträger wie einen USB-Stick oder eine externe Festplatte besitzen, möchten Sie möglicherweise von Zeit zu Zeit alle Dateien und Ordner von diesem löschen. Das können Sie tun, indem Sie den Datenträger <em>formatieren</em> – diese Aktion löscht alle Dateien und hinterlässt einen komplett leeren Datenträger.</p>

<steps>
 <title>Formatieren eines Wechseldatenträgers</title>
  <item>
<if:choose>
<if:when test="platform:unity">
   <p>Öffnen Sie die Anwendung <app>Festplatten</app> aus dem <gui>Dash</gui>.</p>
</if:when>
   <p>Öffnen Sie in der <gui>Aktivitäten</gui>-Übersicht die Anwendung <app>Festplatten</app>.</p>
</if:choose>
  </item>
  <item>
   <p>Wählen Sie die Festplatte, die Sie formatieren möchten, aus der Liste der <gui>Speichergeräte</gui> aus.</p>

  <note style="warning">
   <p>Stellen Sie unbedingt sicher, dass Sie das richtige Medium gewählt haben! Falls Sie das falsche gewählt haben, werden alle Dateien auf diesem Medium gelöscht!</p>
  </note>
  </item>
  <item>
   <p>Im Abschnitt Datenträger wählen Sie <gui>Datenträger aushängen</gui>. Dann klicken Sie auf <gui>Datenträger formatieren</gui>.</p>
  </item>
  <item>
   <p>Wählen Sie im daraufhin erscheinenden Fenster einen Dateisystem-<gui>Typ</gui> für die Festplatte.</p>
   <p>Falls Sie die Festplatte zusätzlich zu Linux-Rechnern auch auf Windows- und MacOS-Rechnern verwenden, wählen Sie <gui>FAT</gui>. Falls Sie die Festplatte nur unter Windows benutzen, ist <gui>NTFS</gui> vermutlich die bessere Einstellung. Eine kurze Beschreibung des <gui>Dateisystemtyps</gui> wird als Hinweis angezeigt.</p>
  </item>
  <item>
   <p>Geben Sie der Festplatte einen Namen und wählen Sie <gui>Formatieren</gui>, um mit dem Vorgang zu beginnen.</p>
  </item>
  <item>
   <p>Nachdem die Formatierung abgeschlossen ist, müssen Sie die Festplatte nur noch <gui>sicher entfernen</gui>. Sie sollte jetzt vollständig leer und bereit zur neuerlichen Benutzung sein.</p>
  </item>
</steps>

<note style="warning">
 <title>Das Formatieren einer Festplatte entfernt die Dateien nicht sicher.</title>
  <p>Das Formatieren einer Festplatte ist keine vollkommen sichere Art, alle Daten davon zu löschen. Formatierte Festplatten enthalten zwar scheinbar keine Dateien, aber mit einer speziellen Anwendung ist es möglich, die Dateien wiederherzustellen. Falls Sie die Dateien sicher für immer löschen möchten, müssen Sie ein Befehlszeilenwerkzeug wie <app>shred</app> verwenden.</p>
</note>

</page>
