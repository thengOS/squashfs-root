<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="printing-streaks" xml:lang="de">

  <info>
    <link type="guide" xref="printing#problems"/>

    <desc>Falls Ihre Ausdrucke Streifen aufweisen oder Farben verblassen bzw. ganz fehlen, so überprüfen Sie die Tintenfüllstände oder reinigen Sie den Druckkopf.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Warum sind auf meinen Ausdrucken Streifen, Linien oder falsche Farben zu sehen?</title>

  <p>Falls Ihre Ausdrucke Streifen aufweisen, dort Linien sind, wo keine sein sollten, oder die Qualität anderweitig gemindert wird, liegt entweder ein Problem mit dem Drucker vor oder die Tinten-/Tonerfüllstände sind zu niedrig.</p>

<list>
 <item>
  <p>Verblasster Text oder verblasste Bilder</p>
  <p>Ihr Tinten-/Tonervorrat könnte zur Neige gehen. Überprüfen Sie die Füllstände und kaufen Sie nötigenfalls eine neue Kartusche.</p>
 </item>

 <item>
  <p>Streifen und Linien</p>
  <p>Wenn Sie einen Tintendrucker besitzen, kann der Druckkopf verschmutzt oder teilweise blockiert sein. Versuchen Sie den Druckkopf zu reinigen (Anweisungen zur Reinigung finden Sie im Handbuch des Druckers).</p>
 </item>

 <item>
  <p>Falsche Farben</p>
  <p>Der Tinten-/Tonervorrat einer Farbe könnte zur Neige gehen. Überprüfen Sie die Füllstände und kaufen Sie nötigenfalls eine neue Kartusche.</p>
 </item>

 <item>
  <p>Gezackte oder ungerade Linien</p>
  <p>Wenn Linien auf Ihrem Ausdruck gezackt sind, obwohl sie gerade sein sollten, müssen Sie eventuell den Druckkopf neu ausrichten. Werfen Sie einen Blick in das Handbuch Ihres Druckers, um herauszufinden, wie Sie dafür vorgehen müssen.</p>
 </item>
</list>

</page>
