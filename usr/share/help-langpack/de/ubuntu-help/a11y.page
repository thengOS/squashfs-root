<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="guide" style="a11y task" version="1.0 if/1.0" id="a11y" xml:lang="de">
  <info>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <desc><link xref="a11y#vision">Sehen</link>, <link xref="a11y#sound">Hören</link>, <link xref="a11y#mobility">Bewegung</link>, <link xref="a11y-braille">Blindenschrift</link> …</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="index" group="a11y"/>
    <revision version="13.10" date="2013-10-25" status="review"/>
  </info>

  <title>Zugangshilfen</title>

  <if:choose>
    <if:when test="platform:unity">
      <p>Die Unity-Arbeitsumgebung stellt Hilfstechnologien bereit, um die Nutzer mit verschiedenen Beeinträchtigungen und besonderen Bedürfnissen zu unterstützen und mit gebräuchlichen Hilfsgeräten zusammenarbeiten zu können. Viele Barrierefreiheitsfunktionen sind über den Abschnitt <gui>Zugangshilfen</gui> der <gui>Systemeinstellungen</gui> zu erreichen.</p>
    </if:when>
    <p>Die GNOME-Arbeitsumgebung beinhaltet Hilfstechnologien zur Unterstützung von Benutzern mit diversen Beeinträchtigungen und besonderen Bedürfnissen und zur Interaktion mit gebräuchlichen Hilfsgeräten. Viele Barrierefreiheitsfunktionen können über das Barrierefreiheitsmenü im oberen Panel erreicht werden.</p>
  </if:choose>

  <section id="vision">
    <title>Visuelle Beeinträchtigungen</title>

    <links type="topic" groups="blind" style="linklist">
      <title>Blindheit</title>
    </links>
    <links type="topic" groups="lowvision" style="linklist">
      <title>Sehschwäche</title>
    </links>
    <links type="topic" groups="colorblind" style="linklist">
      <title>Farbenblindheit</title>
    </links>
    <links type="topic" style="linklist">
      <title>Weitere Themen</title>
    </links>
  </section>

  <section id="sound">
    <title>Auditive Beeinträchtigungen</title>
    <links type="topic" style="linklist"/>
  </section>

  <section id="mobility">
    <title>Mobilitätseinschränkungen</title>
    <links type="topic" groups="pointing" style="linklist">
      <title>Mausbewegung</title>
    </links>
    <links type="topic" groups="clicking" style="linklist">
      <title>Klicken und ziehen</title>
    </links>
    <links type="topic" groups="keyboard" style="linklist">
      <title>Tastaturbedienung</title>
    </links>
    <links type="topic" style="linklist">
      <title>Weitere Themen</title>
    </links>
  </section>

</page>
