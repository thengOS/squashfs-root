<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="de">
  <info>
    <link type="guide" xref="files#faq"/>

    <desc>Schnell neue Dokumente aus benutzerdefinierten Vorlagen erzeugen.</desc>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision version="13.10" date="2013-09-12" status="review"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Vorlagen für häufig verwendete Dokumentenarten</title>

  <p>Wenn Sie oft Dokumente erstellen, die auf dem gleichen Inhalt basieren, könnte die Verwendung von Dateivorlagen für Sie hilfreich sein. Eine Dateivorlage kann ein Dokument eines beliebigen Typs sein, das die Formatierungen oder Inhalte enthält, die Sie wiederverwenden möchten. So könnten Sie beispielsweise ein Vorlagendokument mit Ihrem Briefkopf erstellen.</p>

  <steps>
   <title>Erstellen einer neuen Vorlage</title>
   <item><p>Erstellen Sie ein Dokument, das Sie als Vorlage verwenden wollen. Fügen Sie beispielsweise einem leeren Dokument Ihren Briefkopf hinzu oder ändern Sie die Schriftart.</p></item>
   <item><p>Speichern Sie die Datei mit dem Inhalt der Vorlage im Ordner <file>Vorlagen</file> in Ihrem <file>Persönlichen Ordner</file>. Falls der Ordner <file>Vorlagen</file> nicht existiert, müssen Sie ihn zuerst anlegen.</p></item>
  </steps>

  <steps>
    <title>Eine Vorlage verwenden, um ein Dokument zu erstellen</title>
    <item><p>Öffnen Sie den Ordner, in dem Sie das neue Dokument erstellen wollen.</p></item>
    <item><p>Klicken der rechten Maustaste an einer freien Stelle in der Ordneransicht und Auswahl von <gui style="menuitem">Neues Dokument</gui> zeigt für ein neues Dokument die möglichen Typen in einem Untermenü an.</p></item>
    <item><p>Wählen Sie die gewünschte Vorlage in der Liste aus.</p></item>
    <item><p>Geben Sie einen Dateinamen für das neu erstellte Dokument ein.</p></item>
    <item><p>Doppelklicken Sie auf das Dokument und bearbeiten Sie seinen Inhalt.</p></item>
  </steps>

</page>
