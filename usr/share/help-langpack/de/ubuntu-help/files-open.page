<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="de">

  <info>
    <link type="guide" xref="files" group="more"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <desc>Dateien mit einer Anwendung öffnen, die nicht die Standardanwendung für diesen Dateityp ist. Sie können die Standardanwendung auch ändern.</desc>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Dateien mit anderen Anwendungen öffnen</title>

<p>Wenn Sie in der Dateiverwaltung auf eine Datei doppelt klicken, wird sie mit der Standardanwendung für diesen Dateityp geöffnet. Sie können sie aber auch mit einer anderen Anwendung öffnen, online nach Anwendungen suchen oder die Standardanwendung für alle Dateien des gleichen Typs ändern.</p>

<p>Um eine Datei mit einer anderen als der Standardanwendung zu öffnen, führen Sie einen Rechtsklick auf die Datei aus und wählen die gewünschte Anwendung im oberen Teil des Menüs aus. Falls die Anwendung, mit der Sie die Datei öffnen möchten, dort nicht zu sehen ist, klicken Sie auf <gui>Öffnen mit anderer Anwendung</gui>. Der Vorgabe entsprechend, zeigt die Dateiverwaltung nur Anwendungen, die mit dem Dateityp erfahrungsgemäß umgehen können. Um alle Anwendungen auf Ihrem Rechner zu durchsuchen, klicken Sie auf <gui>Andere Anwendungen anzeigen</gui>.</p>

<p>Wenn Sie auch dort die gewünschte Anwendung nicht finden, können Sie nach weiteren Anwendungen suchen, indem Sie auf <gui>Anwendungen online suchen</gui> klicken. Die Dateiverwaltung wird im Internet nach Paketen mit Anwendungen suchen, die Dateien dieses Typs öfnen können.</p>

<section id="default">
  <title>Ändern der Standardanwendung</title>
  <p>Sie können die Standardanwendung für einen bestimmten Dateityp ändern. Das ermöglicht es Ihnen, Ihre bevorzugte Anwendung zu öffnen, wenn Sie auf die Datei doppelklicken. Beispielsweise möchten Sie, dass Ihre Lieblingsmusikwiedergabe geöffnet wird, wenn Sie auf eine MP3-Datei doppelklicken.</p>

  <steps>
    <item><p>Wählen Sie eine Datei des Typs, dessen Standardanwendung Sie wechseln möchten. Um zum Beispiel die Anwendung zu ändern, die MP3-Dateien öffnet, wählen Sie eine <file>.mp3</file>-Datei.</p></item>
    <item><p>Klicken Sie mit der rechten Maustaste auf die Datei und wählen Sie <gui>Eigenschaften</gui>.</p></item>
    <item><p>Wählen Sie den Reiter <gui>Öffnen mit</gui>.</p></item>
    <item><p>Wählen Sie die gewünschte Anwendung aus und klicken Sie auf <gui>Als Vorgabe festlegen</gui>. Der Vorgabe entsprechend, werden in dieser Liste nur Anwendungen angezeigt, von denen bekannt ist, dass sie mit diesem Dateityp umgehen können. Um alle Anwendungen auf Ihrem Rechner anzuzeigen, klicken Sie <gui>Andere Anwendungen anzeigen</gui>.</p>
    <p>Wenn <gui>Weitere Anwendungen</gui> eine Anwendung enthält, die Sie manchmal verwenden, aber nicht als Vorgabe festlegen wollen, wählen Sie diese Anwendung und klicken Sie auf <gui>Hinzufügen</gui>. Dadurch wird sie zur Rubrik <gui>Empfohlene Anwendungen</gui> hinzugefügt. Anschließend können Sie diese Anwendung verwenden, indem Sie mit der rechten Maustaste auf eine Datei klicken und sie aus der Liste auswählen.</p></item>
  </steps>

  <p>Das ändert die Standardanwendung nicht nur für die gewählte Datei, sondern für alle Dateien dieses Typs.</p>

</section>

</page>
