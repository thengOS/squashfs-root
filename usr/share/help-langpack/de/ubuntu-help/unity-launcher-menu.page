<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="unity-launcher-menu" xml:lang="de">
  <info>
    <link type="guide" xref="unity-launcher-intro#launcher-using"/>

    <revision version="14.10" date="2014-09-14" status="review"/>

    <credit type="author">
      <name>Stephen M. Webb</name>
      <email>stephen@ubuntu.com</email>
    </credit>

    <desc>Rechts-Klick auf ein Starter-Symbol öffnet ein Aktionsmenü</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Die Starter-Symbol-Menüs</title>

  <p>Das Rechtsklicken auf ein Launcher-Symbol ruft ein Aktionsmenü auf. Die verfügbaren Aktionen hängen davon ab, ob das Symbol im Launcher angeheftet ist oder nicht, ob das Programm gerade läuft oder nicht, und vom Symbol selbst. Die verfügbaren Aktionen enthalten folgendes.</p>
  <list>
    <item><p>Starten der Anwendung oder Öffnen eines Dokumentes, Ordners oder Gerätes</p></item>
    <item><p>Symbol aus der Starterleiste entfernen (siehe <link xref="shell-apps-favorites"/>)</p></item>
    <item><p>Symbol an die Starterleiste binden, wenn es vorher noch nicht dort gebunden war (siehe <link xref="shell-apps-favorites"/>)</p></item>
    <item><p>Schließen der laufenden Anwendung</p></item>
    <item><p>Wechseln zwischen einzelnen Instanzen oder Fenstern einer Anwendung</p></item>
    <item><p>anwendungs-spezifische Tastenkombinationen zum Beispiel zum Öffnen eines neuen Fensters oder Dokumentes</p></item>
  </list>

</page>
