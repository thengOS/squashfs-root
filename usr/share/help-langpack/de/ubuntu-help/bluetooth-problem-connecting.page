<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="guide" style="problem" version="1.0 if/1.0" id="bluetooth-problem-connecting" xml:lang="de">

  <info>
    <link type="guide" xref="bluetooth#problems"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <desc>Der Adapter ist ausgeschaltet oder hat keinen Treiber, oder Bluetooth ist deaktiviert oder gesperrt.</desc>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Ich kann mein Bluetooth-Gerät nicht verbinden</title>

<p>Es gibt eine Reihe von Gründen, warum Sie nicht in der Lage sein könnten, eine Verbindung zu einem Bluetooth-Gerät, wie einem Mobiltelefon oder einem Headset, herzustellen.</p>

<terms>
  <item>
    <title>Verbindung wird blockiert oder ist nicht vertrauenswürdig</title>
    <p>Einige Bluetooth-Geräte blocken Verbindungen standardmäßig oder verlangen von Ihnen, dass Sie die Einstellungen ändern, um Verbindungen zu erlauben. Stellen Sie sicher, dass Ihr Gerät so eingerichtet ist, dass es Verbindungen zulässt.</p>
  </item>
  <item>
    <title>Bluetooth-Hardware wurde nicht erkannt</title>
    <p>Ihr Bluetooth-Adapter/-Dongle wird eventuell vom Rechner nicht erkannt. Das kann an nicht installierten <link xref="hardware-driver">Treibern</link> für den Adapter liegen. Einige Bluetooth-Adapter werden von Linux nicht unterstützt, sodass Sie die benötigten Treiber nicht aktivieren können. In diesem Fall müssen Sie sich wahrscheinlich einen neuen Bluetooth-Adapter zulegen.</p>
  </item>
  <item>
    <title>Adapter ist nicht eingeschaltet</title>
      <if:choose>
        <if:when test="platform:unity">
          <p>Stellen Sie sicher, dass Ihr Bluetooth-Adapter eingeschaltet ist. Öffnen Sie das <gui>Bluetooth-Menü</gui> in der <gui>Menüleiste</gui> und stellen Sie sicher, dass es nicht ausgeschaltet ist.</p>
        </if:when>
        <p>Stellen Sie sicher, das Ihr Bluetooth-Adapter eingeschaltet ist. Klicken Sie auf das Bluetooth-Symbol in der Menüleiste und stellen Sie sicher, dass es nicht <link xref="bluetooth-turn-on-off">deaktiviert</link> ist.</p>
      </if:choose>
  </item>
  <item>
    <title>Gerät für die Verbindung ist ausgeschaltet</title>
  <p>Kontrollieren Sie, dass Bluetooth im zu verbindenden Gerät aktiviert ist. Wenn Sie zum Beispiel ein Mobiltelefon verbinden möchten, stellen Sie sicher, dass es sich nicht im Flugzeugmodus befindet.</p>
 </item>

 <item>
  <title>Kein Bluetooth-Adapter in Ihrem Rechner</title>
  <p>Viele Rechner haben keine Bluetooth-Schnittstelle. Sie können einen Adapter kaufen, falls Sie Bluetooth verwenden wollen.</p>
 </item>
</terms>

</page>
