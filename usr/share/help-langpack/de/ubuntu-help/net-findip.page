<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-findip" xml:lang="de">
  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Die Kenntnis Ihrer IP-Adresse kann Ihnen bei der Lösung von Netzwerkproblemen helfen.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Ihre IP-Adresse ermitteln</title>

  <p>Die Kenntnis Ihrer IP-Adresse kann Ihnen bei der Lösung von Netzwerkproblemen helfen. Sie werden vielleicht überrascht sein, dass Sie <em>zwei</em> IP-Adressen besitzen: Eine IP-Adresse für Ihren Rechner im internen Netzwerk und eine IP-Adresse für Ihren Rechner im Internet.</p>

  <steps>
    <title>Finden Sie Ihre interne (Netzwerk-)IP-Adresse</title>
    <item><p>Klicken Sie auf das Symbol ganz rechts in der <gui>Menüleiste</gui> und wählen Sie <gui>Systemeinstellungen</gui>.</p></item>
    <item><p>Öffnen Sie <gui>Netzwerk</gui> und wählen Sie <gui>Kabelgebunden</gui> oder <gui>Kabellos</gui> aus der Liste auf der linken Seite, je nach dem, für welche Netzwerkverbindung Sie die IP-Adresse ermitteln wollen.</p></item>
    <item><p>Ihre interne IP-Adresse wird in der Informationsliste angezeigt.</p></item>
  </steps>

  <steps>
  	<title>Finden Sie Ihre externe (Internet-)IP-Adresse</title>
    <item><p>Besuchen Sie <link href="http://whatismyipaddress.com/">whatismyipaddress.com</link>.</p></item>
    <item><p>Diese Seite zeigt Ihnen Ihre externe IP-Adresse an.</p></item>
  </steps>

<p>Abhängig von der Art, wie sich Ihr Rechner mit dem Internet verbindet, können diese Adressen gleich sein.</p>

</page>
