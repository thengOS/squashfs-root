<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-wireless-connect" xml:lang="de">
  <info>
    <link type="guide" xref="net-wireless" group="#first"/>
    <link type="seealso" xref="net-wireless-troubleshooting"/>
    <link type="seealso" xref="net-wireless-noconnection"/>
    <link type="seealso" xref="net-wireless-disconnecting"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <credit type="author">
      <name>Das Ubuntu-Dokumentations-Team</name>
    </credit>

    <desc>Gehen Sie ins Internet - drahtlos.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Mit einem Funknetzwerk verbinden</title>

<p>Falls mit Ihrem Rechner Funknetzwerkverbindungen möglich sind, können Sie sich mit einem verfügbaren Funknetzwerk verbinden, um auf das Internet zuzugreifen, im Netzwerk freigegebene Dateien zu betrachten und vieles mehr.</p>

<steps>
  <item>
   <p>Wenn Sie einen Funknetzwerkschalter an Ihrem Rechner haben, stellen Sie sicher, dass er angeschaltet wurde.</p>
  </item>
  <item>
    <p>Wählen Sie im <gui>Netzwerkmenü</gui> der <gui>Menüleiste</gui> den Namen des Netzwerks aus, mit dem Sie sich verbinden möchten.</p>
   <p>Sollte der Name der gewünschten Netzwerkverbindung nicht in der Liste erscheinen, so wählen Sie <gui>Weitere …</gui>, um zu sehen, ob das Netzwerk weiter unten in der Liste steht. Sollten Sie den Namen dann immer noch nicht sehen ist ein möglicher Grund, dass Sie außerhalb der Reichweite sind oder das Netzwerk <link xref="net-wireless-hidden">könnte unsichtbar sein</link>.</p>
  </item>
  <item><p>Wenn das Netzwerk passwortgeschützt ist (<link xref="net-wireless-wepwpa">Verschlüsselung</link>), geben Sie das Passwort ein, wenn es verlangt wird, und klicken Sie auf <gui>Verbinden</gui>.</p>
  <p>Wenn Sie den Schlüssel nicht kennen, könnte er auf der Unterseite des WLAN-Routers oder der Basisstation, oder in der Anleitung stehen, oder Sie müssen denjenigen fragen, der das Funknetzwerk verwaltet.</p></item>
  <item><p>Das Netzwerksymbol ändert sein Aussehen, sobald der Rechner versucht, sich mit dem Netzwerk zu verbinden.</p></item>
  <item>
   <p>Wenn die Verbindung erfolgreich hergestellt wurde, ändert sich das Symbol zu einem Punkt mit mehreren Balken darüber. Mehr Balken stehen für eine stärkere Verbindung zum Netzwerk. Wenn nur wenige Balken angezeigt werden, ist die Verbindung schwach und unter Umständen nicht besonders zuverlässig.</p>
  </item>
</steps>

<p>Wenn die Verbindung nicht erfolgreich war, werden Sie unter Umständen <link xref="net-wireless-noconnection">noch einmal nach Ihrem Kennwort gefragt</link> oder einfach nur darüber informiert, dass die Verbindung unterbrochen wurde. Es gibt eine Reihe von Dingen, die das verursacht haben könnten. Sie könnten zum Beispiel ein falsches Kennwort eingegeben haben, das Funksignal könnte zu schwach sein oder die Funknetzwerkkarte Ihres Rechners könnte ein Problem haben. Siehe <link xref="net-wireless-troubleshooting"/> für weitere Hilfe.</p>

<p>Eine stärkere Verbindung zu einem Funknetzwerk bedeutet nicht unbedingt, dass die Internetverbindung schneller ist oder dass Sie mit höheren Raten herunterladen können. Die Funkverbindung verbindet Ihren Rechner <em>mit einem Gerät, das die Internetverbindung bereitstellt</em> (wie einem Router oder einem Modem), aber die beiden Verbindungen sind unterschiedlich und haben deswegen unterschiedliche Übertragungsgeschwindigkeiten.</p>

</page>
