<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" type="topic" style="task" id="keyboard-shortcuts-set" xml:lang="de">
  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Legen Sie Tastenkombinationen fest oder ändern Sie diese in den <gui>Tastatur</gui>-Einstellungen</desc>
  </info>

  <title>Tastenkombinationen einrichten</title>

<p>So ändern Sie für eine Tastenkombination die zu drückenden Tasten:</p>

  <steps>
    <item>
    <p>Klicken Sie auf das Symbol ganz rechts in der Menüleiste und wählen Sie <gui>Systemeinstellungen</gui> aus.</p></item>
    <item><p>Öffnen Sie <gui>Tastatur</gui> und wählen Sie den Reiter <gui>Tastaturkürzel</gui>.</p>
    </item>
    <item>
      <p>Wählen Sie in der linken Seitenleiste eine Kategorie und die Zeile der gewünschten Aktion auf der rechten Seite. Die aktuelle Tastenkombination ändert sich in <gui>Neue Tastenkombination …</gui></p>
    </item>
    <item>
      <p>Halten Sie die gewünschte Tastenkombination gedrückt oder drücken Sie die <key>Rückstelltaste</key>, um sie zurückzusetzen.</p>
    </item>
  </steps>

<section id="custom">
<title>Eigene Tastenkürzel</title>

<p>So erstellen Sie Ihre eigenen Tastenkürzel:</p>

  <steps>
    <item>
      <p>Wählen Sie <gui>Eigene Tastenkürzel</gui> in der linken Seitenleiste und klicken Sie auf den <key>+</key>-Knopf (oder klicken Sie auf den <key>+</key>-Knopf in einer beliebigen Kategorie). Das Fenster <gui>Eigene Tastenkürzel</gui> erscheint.</p>
    </item>
    <item>
      <p>Geben Sie dem Tastenkürzel einen <gui>Name</gui>, den Sie wiedererkennen, und einen <gui>Befehl</gui>, um eine Anwendung auszuführen, dann klicken Sie afu <gui>Anwenden</gui>. Wenn Sie zum Beispiel ein Tastenkürzel zum Öffnen von Rhythmbox erstellen möchten, nennen Sie es <input>Musik</input> und benutzen Sie den Befehl <input>rhythmbox</input>.</p>
    </item>
    <item>
      <p>Klicken Sie in der Zeile, die gerade hinzugefügt wurde, auf <gui>Deaktiviert</gui>. Sobald es sich in <gui>Neue Tastenkombination …</gui> ändert, halten Sie die gewünschten Tasten gedrückt.</p>
    </item>
  </steps>
 <p>Der von Ihnen eingegebene Befehl sollte ein gültiger Befehl des Systems sein. Sie können überprüfen, ob der Befehl funktioniert, indem Sie ihn in einem Terminal eingeben. Der zum Öffnen einer Anwendung verwendete Befehl muss nicht zwangsläufig dem Anwendungsnamen entsprechen.</p>

 <p>Wenn Sie den Befehl, der mit einer individuellen Tastenkombination verknüpft ist, ändern möchten, machen Sie einen Doppelklick auf den <em>Namen</em> der Tastenkombination. Das Fenster für eine <gui>Individuelle Tastenkombination</gui> wird geöffnet und Sie können den Befehl ändern.</p>
</section>

</page>
