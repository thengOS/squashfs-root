<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-suspend" xml:lang="de">

  <info>
    <link type="guide" xref="power"/>

    <desc>Bereitschaft versetzt Ihren Rechner in einen Energiesparmodus, sodass er weniger Strom verbraucht.</desc>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Was passiert, wenn ich den Rechner in Bereitschaft versetze?</title>

<p>Wenn Sie den Rechner in <em>Bereitschaft</em> versetzen, schicken Sie ihn in einen Schlafmodus. Alle Ihre Anwendungen und Dokumente bleiben geöffnet, aber der Bildschirm und andere Teile des Rechners werden ausgeschaltet, um Strom zu sparen. Der Rechner ist aber immer noch eingeschaltet und verbraucht ein klein wenig Strom. Sie können ihn durch einen Tastendruck oder Mausklick aufwecken. Falls das nicht funktioniert, versuchen Sie die Ein-/Austaste zu betätigen.</p>

<p>Manche Rechner haben mangelhafte Hardware-Unterstützung, was dazu führt, dass sie unter Umständen <link xref="power-suspendfail">nicht fehlerfrei in Bereitschaft oder in den Ruhezustand versetzt</link> werden können. Es ist eine gute Idee, die Bereitschaft auf Ihrem Rechner auf ihre Funktionsfähigkeit hin zu überprüfen.</p>

<note style="important">
  <title>Speichern Sie immer Ihre Arbeit, bevor Sie den Rechner in Bereitschaft versetzen</title>
  <p>Sie sollten Ihre Arbeit speichern, bevor Sie den Rechner in Bereitschaft versetzen, für den Fall, dass etwas schiefgeht und Ihre offenen Anwendungen und Dokumente nicht wiederhergestellt werden können, wenn Sie den Rechner wieder einschalten.</p>
</note>

</page>
