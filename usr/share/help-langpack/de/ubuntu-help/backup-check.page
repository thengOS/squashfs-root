<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-check" xml:lang="de">

  <info>
    <link type="guide" xref="files#backup"/>
    <desc>Überprüfen Sie, ob Ihre Sicherung erfolgreich war.</desc>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Ihre Sicherung überprüfen</title>

  <p>Nachdem Sie Ihre Dateien gesichert haben, sollten Sie sich vergewissern, dass die Datensicherung erfolgreich war. Sollte dies nicht der Fall sein, können Sie unter Umständen wichtige Daten verlieren, da manche Dateien möglicherweise nicht gesichert wurden.</p>

   <p>Wenn Sie die Dateiverwaltung nutzen, um Dateien zu kopieren oder zu verschieben, überprüft der Rechner, ob auch alle Daten korrekt übertragen wurden. Falls Sie jedoch Daten übertragen, die für Sie sehr wichtig sind, dann sollten Sie noch einmal zusätzlich prüfen, ob Ihre Daten auch ordentlich übertragen wurden.</p>

  <p>Sie können nochmal extra kontrollieren, indem Sie die erfolgreich kopierten Dateien und Ordner im Zielmedium durchsuchen. Die Kontrolle, dass die transferierten Dateien und Ordner auch wirklich in der Sicherung sind, verschafft Ihnen zusätzliche Gewissheit, dass alles korrekt funktioniert hat.</p>

  <note style="tip"><p>Wenn Sie feststellen, dass Sie regelmässig grosse Mengen von Daten sichern müssen, könnte es für Sie einfacher sein, ein ordentliches Programm zur Datensicherung wie <app>Déjà Dup</app> zu benutzen. Ein solches Programm ist leistungsstärker und verlässlicher als das ledigliche Kopieren und Wiedereinfügen von Dateien.</p></note>

</page>
