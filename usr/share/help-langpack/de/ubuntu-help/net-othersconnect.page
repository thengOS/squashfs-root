<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="net-othersconnect" xml:lang="de">
  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-othersedit"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Sie können die Einstellungen (beispielsweise das Kennwort) für eine Netzwerkverbindung speichern, sodass jeder Benutzer Ihres Rechners eine Verbindung herstellen kann.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Andere Benutzer können keine Verbindung zum Internet herstellen</title>

<p>Wenn Sie eine Netzwerkverbindung eingerichtet haben, aber andere Benutzer Ihres Rechners nicht in der Lage sind, eine Verbindung herzustellen, dann geben sie vermutlich beim Verbindungsaufbau nicht die richtigen Einstellungen an. Wenn es sich beispielsweise um eine Funknetzwerkverbindung handelt, könnten sie das falsche Passwort angegeben haben.</p>

<p>Sie können festlegen, dass jeder Benutzer Ihres Rechners die Einstellungen einer Netzwerkverbindung nutzen kann, sobald Sie diese eingerichtet haben. Das bedeutet, dass Sie die Einrichtung nur einmal vornehmen müssen, und jeder Benutzer Ihres Rechners ist in der Lage, eine Verbindung aufzubauen, ohne zusätzliche Fragen zu beantworten. Gehen Sie hierzu folgendermaßen vor:</p>

<steps>
 <item>
  <p>Öffnen Sie das <gui>Netzwerkmenü</gui> in der Menüleiste und wählen Sie <gui>Verbindungen bearbeiten</gui>.</p>
 </item>

 <item>
  <p>Suchen Sie nach der Verbindung, die jeder benutzen dürfen soll. Sie müssen dazu womöglich in den Reiter <gui>Funknetzwerk</gui> wechseln. Wählen Sie den Netzwerknamen und klicken Sie dann auf <gui>Bearbeiten</gui>.</p>
 </item>

 <item>
  <p>Aktivieren Sie <gui>Für alle Benutzer verfügbar</gui> und klicken Sie auf <gui>Speichern</gui>. Sie müssen Ihr Systemverwaltungspasswort eingeben, um die Einstellungen zu speichern. Nur Systemverwalter können dies tun.</p>
 </item>

 <item>
  <p>Andere Benutzer des Rechners sind nun in der Lage, diese Verbindung zu nutzen, ohne weitere Details eingeben zu müssen.</p>
 </item>
</steps>

</page>
