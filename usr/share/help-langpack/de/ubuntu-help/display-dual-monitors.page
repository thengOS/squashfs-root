<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="display-dual-monitors" xml:lang="de">

  <info>
    <desc>Konfigurieren Sie die Nutzung zweier Bildschirme mit Ihrem Laptop.</desc>
    <revision pkgversion="3.8.2" date="2014-01-19" status="review"/>
    <revision version="14.04" date="2014-01-19" status="review"/>
    <credit type="author">
      <name>Ubuntu-Dokumentationsteam</name>
      <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="prefs-display"/>
  </info>

<title>Einen externen Bildschirm mit Ihrem Laptop verbinden</title>

	<section id="unity-steps">
		<title>Einen externen Bildschirm einrichten</title>
		<p>Um einen externen Bildschirm für Ihren Laptop einzurichten, verbinden Sie den Bildschirm mit Ihrem Laptop. So gehen Sie vor, falls Ihr System den Bildschirm nicht sofort erkennt oder Sie die Einstellungen anpassen möchten:</p>
		<steps>
			<item>
				<p>Klicken Sie auf das Symbol ganz rechts in der <gui>Menüleiste</gui> und wählen Sie <gui>Systemeinstellungen</gui>.</p>
			</item>
			<item>
				<p>Öffnen Sie <gui>Anzeigegeräte</gui>.</p>
			</item>
			<item>
				<p>Klicken Sie auf das Bild des Bildschirms, den Sie ein- oder ausschalten möchten und schalten Sie ihn <gui>EIN/AUS</gui>.</p>
			</item>
			<item>
				<p>Der Starter wird standardmäßig nur auf dem ersten Bildschirm angezeigt. Um die Festlegung des »primären« Bildschirms zu ändern, wechseln Sie den Bildschirm in der Auswahlliste <gui>Starter-Position</gui>. Alternativ können Sie den Starter auch in der Vorschau auf den Bildschirm ziehen, der als »primärer« Bildschirm dienen soll.</p>
				<p>Wenn der Starter auf allen Bildschirmen angezeigt werden soll, wählen Sie für <gui>Starter-Position</gui> <gui>Alle Bildschirme</gui>.</p>
			</item>
			<item>
				<p>Um die »Position« eines Bildschirms zu ändern, klicken Sie darauf und ziehen Sie ihn an die gewünschte Position.</p>
				<note>
					<p>Falls Sie möchten, dass beide Bildschirme den gleichen Inhalt anzeigen, setzen Sie einen Haken in den Kasten <gui>Bildschirme spiegeln</gui>.</p>
				</note>
			</item>
			<item>
				<p>Wenn Sie mit Ihren Einstellungen zufrieden sind, klicken Sie auf <gui>Anwenden</gui> und klicken Sie dann auf <gui>Diese Einstellungen beibehalten</gui>.</p>
			</item>
			<item>
				<p>Um die Einstellungen für <gui>Anzeigegeräte</gui> zu schließen, klicken Sie auf das <gui>x</gui> in der oberen Ecke.</p>
			</item>
		</steps>
	</section>

	<section id="sticky-edges">
		<title>Klebrige Ränder</title>
		<p>Ein häufig auftretendes Problem bei der Verwendung von zwei Bildschirmen ist es, dass der Mauszeiger auf den anderen Bildschirm »rutscht«, ohne dass Sie dies beabsichtigt haben. Unitys Funktion <gui>Klebrige Ränder</gui> schafft Abhilfe für dieses Problem, indem sie es notwendig macht, dass Sie den Mauszeiger ein bisschen stärker zum Rand bewegen müssen, damit er zum anderen Bildschirm wechselt.</p>
		<p>Sie können <gui>Klebrige Ränder</gui> ausschalten, falls Ihnen diese Funktion nicht gefällt.</p>
	</section>

</page>
