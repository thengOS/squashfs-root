<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="question" id="unity-hud-intro" xml:lang="de">

  <info>
    <link type="guide" xref="shell-overview#desktop"/>

    <desc>Das HUD benutzen, um in Menüs Ihrer Anwendungen zu suchen.</desc>
    <revision version="13.10" date="2013-10-23" status="outdated"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Was ist das HUD?</title>

<p>Das <gui>HUD</gui> oder auch <gui>Heads Up Display</gui> ist eine suchbasierte Alternative zu traditionellen Menüs und wurde mit Ubuntu 12.04 LTS eingeführt.</p>

<p>Manche Anwendungen wie <link href="apt:gimp">Gimp</link> oder <link href="apt:inkscape">Inkscape</link> haben hunderte Menüeinträge. Wenn Sie solche Anwendungen benutzen, merken Sie sich vielleicht, wie der Menüpunkt heißt, aber nicht wo sich dieser befindet.</p>

<p>Eine Sucheingabe zu verwenden kann deutlich schneller zum Ziel führen, als durch verschachtelte Menühierarchien zu navigieren. Im Vergleich zu normalen Menüs hat das HUD auch Vorteile für die Barrierefreiheit, da eine exakte Navigation mit der Maus nicht mehr notwendig ist.</p>

<section id="use-the-hud">
<title>Das HUD benutzen</title>

  <p>Um das HUD auszuprobieren:</p>

  <steps>
    <item>
      <p>Tippen Sie die <key>Alt</key>-Taste an, um das HUD zu öffnen.</p>
    </item>
    <item>
      <p>Beginnen Sie mit Ihrer Texteingabe.</p>
    </item>
    <item>
      <p>Wenn Sie ein Ergänzungsergebnis ausführen wollen, verwenden Sie die Aufwärts- und Abwärtstasten, um das Ergebnis auszuwählen und drücken Sie <key>Eingabe</key> oder klicken Sie auf das gewünschte Ergebnis.</p>
    </item>
    <item>
      <p>Wenn Sie sich anders entscheiden und das HUD schließen wollen, drücken Sie nochmals <key>Alt</key> oder <key>Esc</key>. Sie können auch irgendwo außerhalb des HUD klicken, um das HUD zu schließen.</p>
    </item>
  </steps>

<p>Um noch hilfreicher zu sein, zeichnet das HUD Ihren Suchverlauf auf und passt die Suchergebnisse daraufhin an.</p>

</section>

</page>
