<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="look-display-fuzzy" xml:lang="de">

  <info>
   <link type="guide" xref="hardware-problems-graphics"/>
   <desc>Die Bildschirmauflösung könnte falsch eingestellt sein.</desc>

    <revision version="13.10" date="2013-10-22" status="review"/>
    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Warum ist das Bild auf meinem Bildschirm unscharf/verpixelt?</title>

<p>Das kann passieren, wenn die Bildschirmauflösung, die Sie eingestellt haben, für Ihren Bildschirm ungeeignet ist.</p>

<p>Zur Lösung des Problems klicken Sie auf das Symbol ganz rechts in der Menüleiste und gehen Sie zu <gui>Systemeinstellungen</gui>. Wählen Sie im Abschnitt Hardware <gui>Anzeigegeräte</gui>. Probieren Sie einige der Werte in <gui>Auflösung</gui> durch und wählen Sie denjenigen, bei dem das Bild wieder besser aussieht.</p>

<section id="multihead">
 <title>Bei mehreren angeschlossenen Bildschirmen</title>

 <p>Wenn Sie zwei Bildschirme am Rechner angeschlossen haben (zum Beispiel einen normalen Monitor und einen Projektor), können die Bildschirme unterschiedliche Auflösungen haben. Die Grafikkarte des Rechners kann aber jeweils nur eine Auflösung zur Verfügung stellen, wodurch zumindest einer der Bildschirme verschwommen erscheinen kann.</p>

 <p>Sie können für die zwei Bildschirme unterschiedliche Auflösungen einstellen, werden dann aber nicht auf beiden Bildschirmen gleichzeitig dasselbe darstellen können. Sie haben damit also zwei voneinander unabhängige Bildschirme gleichzeitig angeschlossen. Sie können Fenster von einem Bildschirm zum anderen verschieben, aber Sie können nicht dasselbe Fenster gleichzeitig auf beiden Bildschirmen darstellen.</p>

 <p>Einrichten der Bildschirme, so dass jeder eine eigene Auflösung verwendet:</p>

 <steps>
  <item>
   <p>Klicken Sie auf das Symbol ganz rechts in der Menüleiste und dann auf <gui>Systemeinstellungen</gui>. Öffnen Sie <gui>Anzeigegeräte</gui>.</p>
  </item>

  <item>
   <p>Deaktivieren Sie <gui>Bildschirme spiegeln</gui>.</p>
  </item>

  <item>
   <p>Wählen Sie der Reihe nach jeden Bildschirm aus dem grauen Feld im oberen Bereich des Fensters <gui>Anzeigegeräte</gui>. Ändern Sie die <gui>Auflösung</gui>, bis der Bildschirm richtig erscheint.</p>
  </item>
 </steps>

</section>

</page>
