<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-lock" xml:lang="de">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>
    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-07" status="review"/>
    <desc>Hindern Sie andere Personen daran, Ihre Arbeitsumgebung zu verwenden, wenn Sie sich von Ihrem Rechner entfernen.</desc>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Ihren Bildschirm automatisch sperren</title>

  <p>Wenn Sie Ihren Rechner verlassen, sollten Sie <link xref="shell-exit#lock-screen">den Bildschirm sperren</link>, um andere Personen daran zu hindern, Ihre Arbeitsumgebung zu benutzen und auf Ihre Dateien zuzugreifen. Sie sind dann immer noch angemeldet und alle Ihre Anwendungen laufen weiter, aber Sie müssen Ihr Passwort eingeben, um Ihren Rechner wieder benutzen zu können. Sie können den Bildschirm manuell sperren, Sie können ihn aber auch automatisch sperren lassen.</p>

  <steps>
    <item><p>Klicken Sie auf das Symbol ganz rechts in der <gui>Menüleiste</gui> und wählen Sie <gui>Systemeinstellungen</gui>.</p></item>
    <item><p>Wählen Sie <gui>Helligkeit &amp; Sperren</gui>.</p></item>
    <item><p>Stellen Sie sicher, dass <gui>Sperren</gui> aktiviert ist, und wählen Sie ein Zeitlimit aus der Auswahlliste darunter. Der Bildschirm wird automatisch gesperrt, wenn Sie die angegebene Zeit lang untätig waren. Sie können auch <gui>Bildschirm schaltet ab</gui> auswählen, der Bildschirm wird dann mit dem Abschalten auch gesperrt; das Zeitlimit zum Abschalten stellen Sie dabei mithilfe der Auswahlliste <gui>Bildschirm abschalten, wenn inaktiv für</gui> darüber ein.</p></item>
  </steps>

</page>
