<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-chat-social" xml:lang="de">
  <info>
    <link type="guide" xref="net-chat"/>

    <revision version="13.10" date="2013-09-18" status="review"/>

    <credit type="author">
      <name>Ubuntu-Dokumentationsteam</name>
    </credit>

    <desc>Beiträge auf <em>Twitter</em>, <em>Facebook</em> und weiteren sozialen Netzwerken direkt von der Arbeitsfläche aus verschicken</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>»Soziales Netzwerken« direkt vom Schreibtisch</title>

  <p>Mit Ubuntu können Sie in Ihren liebsten sozialen Netzwerkseiten direkt vom Schreibtisch aus schreiben. Ubuntu verwendet den <app>Freundesbereich</app>, um Ihnen eine zentrale Organisation Ihrer sozialen Netzwerkseiten zu bieten und um Aktualisierungen vom <gui>Me-Menü</gui> aus zu geben, ohne das Öffnen einer Internetseite zu benötigen.</p>

  <p>So richten Sie die Konten Ihrer sozialen Netzwerke ein:</p>
  <steps>

    <item><p>Bitte das <link xref="unity-menubar-intro">Systemmenü</link> rechts in der Menüleiste Öffnen und »Systemeinstellungen …« auswählen.</p></item>

    <item><p><gui>Online-Konten</gui> auswählen</p></item>

    <item><p>Das soziale Netzwerk, welches Sie einrichten wollen, auswählen und auf <gui>Konto hinzufügen …</gui> klicken.</p></item>

    <item><p>Auf <gui>Legitimieren</gui> klicken, Ihre Kontodaten für dieses Netzwerk eingeben und den Anweisungen folgen.</p></item>

  </steps>

  <p>Sie können jetzt Ihre Nachrichten aus sozialen Netzwerken vom <link xref="unity-menubar-intro"> Nachrichtenmenü</link>, rechts auf der Menüleiste im <gui>Übertragungsabschnitt</gui> ansehen. Klicken Sie auf eines der Elemente in diesem Abschnitt, um den <gui>Freundesbereich</gui> zu öffnen und Nachrichten in Ihrem sozialen Netzwerk zu lesen und zu schreiben.</p>

</page>
