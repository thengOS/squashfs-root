<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-envelopes" xml:lang="de">

  <info>
    <link type="guide" xref="printing#paper"/>

    <desc>Stellen Sie sicher, dass der Briefumschlag/das Etikett richtig herum liegt und Sie die richtige Papiergröße eingestellt haben.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Umschläge und Etiketten bedrucken</title>

<p>Mit den meisten Druckern ist es möglich, einen Umschlag oder Etiketten direkt zu bedrucken. Dies ist insbesondere dann nützlich, wenn Sie beispielsweise viele Briefe verschicken möchten.</p>

<section id="envelope">
 <title>Umschläge bedrucken</title>

 <p>Um einen Briefumschlag zu bedrucken, müssen Sie zwei Dinge überprüfen. Als erstes, stellen Sie sicher, dass Ihr Drucker die Größe des Umschlags kennt. Nachdem Sie auf <gui>Drucken</gui> geklickt haben und das Druck-Fenster erschienen ist, gehen Sie zum Reiter <gui>Seite einrichten</gui> und wählen Sie »Umschlag« als <gui>Papiertyp</gui>, falls möglich. Falls dies nicht möglich ist, sehen Sie nach, ob Sie die <gui>Papiergröße</gui> in Umschlaggröße (z.B. »C5«) ändern können. Auf der Packung der Umschläge ist die Größe vermerkt. Die meisten Umschläge werden in Standardformaten vertrieben.</p>

 <p>Als zweites müssen Sie sicherstellen, dass die Umschläge richtig herum im Einzug des Druckers liegen. Sehen Sie im Druckerhandbuch nach oder bedrucken Sie zunächst einen einzelnen Umschlag und überprüfen Sie auf welche Seite gedruckt wird, um herauszufinden, wie herum Sie die Umschläge einlegen müssen.</p>

<note style="warning">
 <p>Einige Drucker, besonders Laserdrucker, können keine Briefumschläge bedrucken. Sehen Sie im Handbuch Ihres Druckers nach, um herauszufinden, ob dieser Briefumschläge annimmt; ansonsten können Sie Ihren Drucker beschädigen, wenn Sie versuchen einen Briefumschlag hineinzustecken.</p>
</note>

</section>

<!--
INCOMPLETE: Please write this section!

<section id="labels">
 <title>Printing labels</title>

</section>
-->

</page>
