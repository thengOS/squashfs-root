<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="unity-introduction" xml:lang="de">

  <info>
    <link type="guide" xref="index" group="unity-introduction"/>
    <link type="guide" xref="shell-overview" group="#first"/>
    
    <desc>Eine visuelle Einführung in die Unity-Benutzeroberfläche.</desc>
    
    <revision version="13.10" date="2013-10-23" status="outdated"/>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit> 

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>
  
  <title>Willkommen bei Ubuntu</title>

<p>Ein Leistungsmerkmal von Ubuntu ist <em>Unity</em>, eine überdachte Art und Weise, Ihren Rechner zu bedienen. Unity ist so konzipiert, dass es weniger Ablenkungen gibt, Ihnen mehr Freiraum beim Arbeiten bietet und Ihnen dabei hilft, Dinge zu erledigen.</p>

<p>Dieser Leitfaden soll Fragen zu Unity und der Ubuntu-Arbeitsumgebung beantworten. Zuerst einige Hauptmerkmale von Unity und deren Verwendung:</p>

<section id="unity-overview">
  <title>Einstieg in Unity</title>

<media type="image" src="figures/unity-overview.png">
  <p>Die Unity-Benutzeroberfläche</p>
</media>

<section id="launcher-home-button">
<title>Der Starter</title>

<media type="image" src="figures/unity-launcher.png" style="floatstart floatleft">
  <p>Der Starter</p>
</media>

<p>Der <gui>Starter</gui> erscheint automatisch, wenn Sie sich anmelden, und bietet schnellen Zugriff auf die meist genutzten Anwendungen.</p>

<list style="compact">
  <item><p><link xref="unity-launcher-intro">Erfahren Sie mehr über den Starter.</link></p></item>
</list>

</section>

<section id="the-dash">
  <title>Das Dash</title>

<p>Der <gui>Ubuntu-Knopf</gui> befindet sich in der Nähe der linken oberen Ecke des Bildschirms und ist immer das oberste Element im Starter. Wenn Sie auf den <gui>Ubuntu-Knopf</gui> klicken, zeigt Ihnen Unity eine zusätzliche Funktion der Arbeitsumgebung an, das <gui>Dash</gui>.</p>

<media type="image" src="figures/unity-dash.png">
  <p>Das Unity-Dash</p>
</media>

<p>Das <em>Unity-Dash</em> soll die Suche nach  Dateien, Musik und mehr sowie den Start von Anwendungen vereinfachen. Wenn Sie z.B. das Wort »Dokument« in die <em>Suchzeile</em> eingeben, werden Ihnen Anwendungen zum Schreiben und Bearbeiten von Dokumenten angezeigt. Es werden Ihnen auch entsprechende Ordner und Dokumente angezeigt, die Sie kürzlich bearbeitet haben.</p>


<list style="compact">
  <item><p><link xref="unity-dash-intro">Erfahren Sie mehr über das Dash.</link></p></item>
</list>
</section>

</section>
</page>
