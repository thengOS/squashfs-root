<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="unity-launcher-shapes" xml:lang="de">
  <info>
    <link type="guide" xref="unity-launcher-intro#launcher-using"/>

    <revision version="13.10" date="2013-10-23" status="outdated"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>

    <desc>Die Dreiecke zeigen Ihnen Ihre gestarteten Anwendungen.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Was bedeuten unterschiedliche Formen und Farben bei Starter-Symbolen?</title>

  <p>Wenn Sie eine Anwendung starten, beginnt das Startersymbol zu pulsieren und zeigt damit an, dass Ubuntu die gewünschte Anwendung startet.</p>

  <p>Sobald die Anwendung gestartet ist, erscheinen <em>weiße Dreiecke</em> an der linken und rechten Seite des Anwendungssymbols. Weitere Dreiecke erscheinen auf der linken Seite, wenn weitere Fenster der selben Anwendung geöffnet werden (d.h. zwei Dreiecke stehen für zwei Fenster der selben Anwendung; drei Dreiecke für drei Fenster). Es erscheinen nicht mehr als drei Dreiecke, auch wenn Sie mehr als drei Fenster der selben Anwendung geöffnet haben.</p>

    <note style="tip"><p>Anwendungen, die gerade nicht laufen, haben durchscheinende Symbolecken. Sobald die Anwendung läuft, wird das Anwendungsymbol farbig gefüllt.</p></note>

<section id="launcher-notifications">
  <title>Benachrichtigungen</title>

  <p>Wenn eine Anwendung Ihre Aufmerksamkeit erfordert und Sie über etwas informieren will (beispielsweise über einen abgeschlossenen Download), beginnt das Programmsymbol zu wackeln und zu leuchten, das weiße Dreieck färbt sich <em>blau</em>. Ein Klick auf das Programmsymbol löscht die Benachrichtigung.</p>

  <p>Anwendungen können auch eine <em>Nummer</em> auf ihrem Programmsymbol zeigen. Kommunikationsprogramme nutzen dies, um die Anzahl noch ungelesener Nachrichten anzuzeigen. Die <gui>Systemaktualisierung</gui> zeigt üblicherweise die Anzahl möglicher Programmaktualisierungen.</p>

  <p>Außerdem können Anwendungen einen <em>Fortschrittsbalken</em> nutzen, um Ihnen die Restdauer eines Vorgangs anzuzeigen, ohne das Sie dabei das Anwendungsfenster geöffnet im Blick behalten müssen.</p>

</section>

</page>
