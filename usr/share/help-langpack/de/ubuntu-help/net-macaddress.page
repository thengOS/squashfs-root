<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="net-macaddress" xml:lang="de">
  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Die eindeutige Kennung, die Ihrer Netzwerk-Hardware zugeordnet ist.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Was ist eine MAC-Adresse?</title>

  <p>Eine <em>MAC-Adresse</em> ist die durch den Hersteller vergebene eindeutige Identifizierung eines Netzwerk-Gerätes (wie einer WLAN- oder Ethernet-Karte). MAC steht für <em>Media Access Control</em>.</p>

  <p>Eine MAC-Adresse besteht aus sechsmal zwei Zeichen, jedes Zeichenpaar ist dabei durch einen Doppelpunkt getrennt. <code>00:1B:44:11:3A:B7</code> ist ein Beispiel für eine MAC-Adresse.</p>

  <p>So finden Sie die MAC-Adresse Ihres Netzwerkgerätes heraus:</p>
  <steps>
    <item><p>Öffnen Sie das <gui>Netzwerkmenü</gui> in der Menüleiste.</p></item>
    <item><p>Wählen Sie <gui>Verbindungsinformationen</gui>.</p></item>
    <item><p>Ihre MAC-Adresse wird als <gui>Hardware-Adresse</gui> angezeigt.</p></item>
  </steps>

  <p>In der Praxis kann es vorkommen, dass Sie die <link xref="net-wireless-edit-connection">MAC-Adresse ändern</link> müssen (sogenanntes MAC-Spoofing). Manche Internet-Dienstanbieter verlangen zum Beispiel, dass ihr Dienst nur von einer bestimmten MAC-Adresse aus genutzt wird. Wenn diese Netzwerkkarte kaputt geht und Sie Ihre Netzwerkkarte austauschen müssen, wird der Dienst nicht mehr funktionieren. In solchen Fällen müssten Sie eine andere MAC-Adresse vortäuschen.</p>

</page>
