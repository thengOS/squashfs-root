<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="shell-workspaces" xml:lang="de">

  <info>

    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <desc>Arbeitsflächen sind eine Möglichkeit zum Gruppieren von Fenstern in Ihrer Arbeitsumgebung.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="14.10" date="2014-09-14" status="review"/>
    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

  </info>

<title>Was ist eine Arbeitsfläche, und was habe ich davon?</title>

<media type="image" src="figures/unity-workspace-intro.png" style="floatend floatright">
  <p>Arbeitsflächenumschalter</p>
</media>

  <p>Arbeitsflächen sollen das Verwalten von Fenstern vereinfachen. Durch den größeren Arbeitsbereich können Sie Ihre Fenster leichter sortieren und so Chaos vorbeugen.</p>

  <p>Sie können Arbeitsflächen dazu verwenden, um Ihre Arbeit besser zu organisieren. Beispielsweise könnten alle Fenster von Kommunikationsanwendungen, wie E-Mail- und Sofortnachrichten, auf einer Arbeitsfläche liegen und alle sonstigen Anwendungen auf einer anderen Arbeitsfläche. Ihre Musikverwaltung könnte dann eine weitere Arbeitsfläche belegen.</p>

  <steps>
    <title>Arbeitsflächen aktivieren</title>
    <item>
      <p>Klicken Sie auf das Symbol ganz rechts in der Menüleiste und wählen Sie <gui>Systemeinstellungen</gui> aus.</p>
    </item>
    <item>
      <p>Im Persönlich-Abschnitt, klicken Sie auf <gui>Darstellung</gui> und wählen Sie den <gui>Verhalten</gui>-Reiter.</p>
    </item>
    <item>
      <p>Das Auswahlfeld <gui>Arbeitsflächen aktivieren</gui> wählen.</p>
    </item>
  </steps>

 <p>Jetzt können Sie den <link xref="unity-launcher-intro">Starter</link> öffnen und auf das Symbol <gui>Arbeitsflächenumschalter</gui> am unteren Bildschirmrand klicken. Standardmäßig zeigt Ubuntu 4 Arbeitsflächen, angeordnet in 2 Reihen und 2 Spalten. Sie können die Anzahl der Arbeitsflächen ändern:</p>

<steps>
  <title>Die Arbeitsflächenzahl ändern</title>
  <item><p>Gehen Sie ins <link xref="unity-dash-intro">Dash</link> und öffnen Sie das <app>Terminal</app>.</p></item>
  <item><p>Um die Anzahl der Zeilen zu ändern, geben Sie den folgenden Befehl ein, die Zahl ersetzen Sie dabei durch die gewünschte Anzahl. Drücken Sie die <key>Eingabetaste</key>.</p>

    <code its:translate="no">gsettings set org.compiz.core:/org/compiz/profiles/unity/plugins/core/ vsize 2</code></item>
  <item><p>Um die Anzahl der Spalten zu ändern, geben Sie den folgenden Befehl ein, die Zahl ersetzen Sie dabei durch die gewünschte Anzahl. Drücken Sie die <key>Eingabetaste</key>.</p>
    <code its:translate="no">gsettings set org.compiz.core:/org/compiz/profiles/unity/plugins/core/ hsize 3</code></item>
</steps>

</page>
