<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-vpn-connect" xml:lang="de">
  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-wired"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="16.04" date="2016-03-14" status="review"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>VPNs erlauben einem lokalen Netzwerk über das Internet beizutreten. Erfahren Sie, wie man eine VPN-Verbindung erstellt.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Mit einem VPN verbinden</title>

<p>Ein VPN (oder <em>Virtuelles Privates Netzwerk</em>) ist eine Möglichkeit, sich mit einem lokalen Netzwerk über das Internet zu verbinden. Angenommen, Sie möchten sich mit dem lokalen Netzwerk an Ihrem Arbeitsplatz verbinden, während Sie auf einer Geschäftsreise sind. Dazu suchen Sie sich irgendwo eine Internetverbindung (etwa in einem Hotel) und verbinden sich mit dem VPN Ihres Arbeitsplatzes. Das ist so, als wären Sie mit dem Netzwerk in Ihrem Büro verbunden, tatsächlich geht der Netzwerkzugang aber über die Internetverbindung des Hotels. VPN-Verbindungen sind normalerweise <em>verschlüsselt</em>, um zu verhindern, dass nicht angemeldete Personen Zugriff auf das lokale Netzwerk erlangen, mit dem Sie gerade verbunden sind.</p>

<p>There are a number of different types of VPN. You may have to install some extra software depending on what type of VPN
you're connecting to. Find out the connection details from whoever is in charge of the VPN and see which <em>VPN client</em>
you need to use. Then, open <app>Ubuntu Software</app> and search for the <app>network-manager</app> package which works
with your VPN (if there is one) and install it.</p>

<note>
 <p>Wenn es kein Netzwerkverwaltungs-Paket für die Art Ihres VPNs gibt, müssen Sie wahrscheinlich von der Firma, die die VPN-Software zur Verfügung stellt, eine Client-Software herunterladen und installieren. Folgen Sie den entsprechenden Anweisungen, um sie zum Laufen zu bringen.</p>
</note>

<p>Sobald dies erledigt ist, können Sie die VPN-Verbindung einrichten:</p>

<steps>
 <item>
  <p>Öffnen Sie das <gui>Netzwerkmenü</gui> in der Menüleiste und wählen Sie <gui>VPN konfigurieren</gui> unter <gui>VPN-Verbindungen</gui>.</p>
 </item>

 <item>
  <p>Klicken Sie auf <gui>Hinzufügen</gui> und wählen Sie, welche Art von VPN-Verbindung Sie haben.</p>
 </item>

 <item>
  <p>Klicken Sie auf <gui>Erzeugen</gui>, folgen Sie den Anweisungen am Bildschirm und geben Sie die verlangten Informationen, wie Ihren Benutzernamen und Ihr Passwort, ein.</p>
 </item>

 <item>
  <p>Wenn Sie die Einrichtung des VPN beendet haben, öffnen Sie das <gui>Netzwerkmenü</gui> aus der Menüleiste und wählen Sie <gui>VPN-Verbindungen</gui>. Klicken Sie dort auf die gerade erstellte Verbindung. Während des Verbindungsversuchs verändert sich das Netzwerksymbol.</p>
 </item>

 <item>
  <p>Sie haben sich nun hoffentlich erfolgreich mit dem VPN verbunden. Wenn nicht, sollten Sie die VPN-Einstellungen, die Sie eingegeben haben, noch einmal überprüfen. Sie können das tun, indem Sie das Netzwerkmenü öffnen, auf <gui>Verbindungen bearbeiten</gui> klicken und zum Reiter <gui>VPN</gui> wechseln.</p>
 </item>

 <item>
  <p>Um die Verbindung zum VPN zu trennen, öffnen Sie das Netzwerkmenü und klicken dann auf <gui>VPN trennen</gui> unter dem Namen Ihrer VPN-Verbindung.</p>
 </item>
</steps>

</page>
