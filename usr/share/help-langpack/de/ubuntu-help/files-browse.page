<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="files-browse" xml:lang="de">


  <info>
    <link type="guide" xref="files" group="#first"/>
    <link type="seealso" xref="files-copy"/>
    <desc>Verwalten und Organisieren von Dateien mithilfe der Dateiverwaltung.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision version="14.04" date="2014-02-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ubuntu-Dokumentationsteam</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Dateien und Ordner durchsuchen</title>

<p>Verwenden Sie die Anwendung <app>Dateien</app>, um die Dateien Ihres Rechners zu verwalten und auf sie zuzugreifen. Der Zugriff auf weitere Speichergeräte wie externe Festplatten, auf <link xref="nautilus-connect">Datei-Server</link> und Netzlaufwerke.</p>

<p>Um die Dateiverwaltung zu starten, öffnen Sie <app>Dateien</app> aus dem <gui>Starter</gui>. Sie können auch mit dem <gui>Dash</gui> nach Dateien und Ordnern suchen, analog zur <link xref="unity-dash-intro#dash-home">Suche nach Anwendungen</link>. Sie werden unter <gui>Dateien &amp; Ordner</gui> angezeigt.</p>

<section id="files-view-folder-contents">
  <title>Den Inhalt von Ordnern erkunden</title>

<p>In der Dateiverwaltung können Sie durch Doppelklick auf einen Ordner den Inhalt darstellen, durch einen Doppelklick auf eine Datei öffnen Sie diese mit der Standardanwendung für den betreffenden Dateityp. Mit einem Rechts-Klick auf einen Ordner können Sie einen Ordner auch in einem neuen Reiter oder in einem neuen Fenster öffnen.</p>

<p>Die <em>Pfadleiste</em> über der Liste von Dateien und Ordnern zeigt Ihnen, in welchem Ordner Sie sich gerade befinden, inklusive der übergeordneten Ordnern. Klicken Sie auf einen übergeordneten Ordner in der Pfadleiste, um in diesen Ordner zu wechseln. Führen Sie einen Rechtsklick auf einen beliebigen Ordner in der Pfadleiste aus, um ihn in einem neuen Reiter oder neuen Fenster zu öffnen, ihn zu kopieren oder zu verschieben oder um auf seine Eigenschaften zuzugreifen.</p>

<p>Wenn Sie innerhalb eines Ordners schnell zu einer bestimmten Datei springen möchten, geben Sie den Anfang des Dateinamens ein. Ein Suchfeld erscheint und die erste gefundene Datei wird hervorgehoben. Drücken Sie den »Herunter«-Pfeil oder nutzen Sie den Bildlauf der Maus, um zur nächsten gefundenen Datei zu springen.</p>

<p>Häufig genutzte Ordner können Sie über die <em>Seitenleiste</em> erreichen. Sollte die Seitenleiste nicht angezeigt werden, klicken Sie den <media type="image" src="figures/go-down.png">herunter</media>-Knopf in der Werkzeugleiste und wählen Sie <gui>Seitenleiste anzeigen</gui>. Sie können Lesezeichen für weitere Ordner einfügen. Verwenden Sie dazu das <gui>Lesezeichen</gui>-Menü oder ziehen Sie den betreffenden Ordner einfach auf die Seitenleiste.</p>

</section>
</page>
