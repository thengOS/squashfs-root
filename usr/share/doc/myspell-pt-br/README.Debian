myspell-pt-br for Debian
------------------------

myspell-pt-br
-------------

myspell-pt-br used to be generated from the br.ispell source package. It is
now obtained from the sources distributed by the BrOffice.org team.
According to the BrOffice.org people, their dictionary is more complete and
up-to-date as regards the one distributed in br.ispell (2.8M of words for
the former instead of 250k for the later).

The ustream authors provide three different tarballs:

  * Complete version: pt_BR-*-*-*C.zip
  * Light version: pt_BR-*-*-*L.zip
  * Ideal versião: pt_BR-*-*-*V.zip

The last tarball is not recommended for OpenOffice.org.  The Debian package
corresponds to the "complete version".

Debian myspell-pt-br is already integrated into OpenOffice.org and should
work out-of-the-box with it, no need of special actions. README_pt_BR-OOO2x.TXT
is included only for completion.

aspell-pt-br
------------

aspell-pt-br used to be a stand-alone package, but currently is also
generated from this package with some minor tweak. As a matter of fact,
that is what upstream does (with similar tweaks) when releasing official
aspell6-pt-br dictionaries. br-abnt2.kbd and pt_BR.dat were borrowed from
aspell-pt-br and put in debian/aspell-files.

From upstream README.aspell.txt

 The Brazilian Portuguese dictionary for Aspell is a version of the
 OpenOffice.org one. The idea is to avoid forking or duplicate efforts.
 Previously (previously to Aspell 6) the Brazilian Portuguese dictionary for
 Aspell was derived from the Ispell counterpart, but that project is long
 unmaintained. Please visit the spell checking project page at the BrOffice.org
 (Brazilian Portuguese OpenOffice.org) site if you want to report a missing
 word, a misspelled word, or if you are looking for further information on
 credits and new versions:

 http://www.broffice.org/verortografico

 -- Agustin Martin Domingo <agmartin@debian.org>, Mon, 26 Oct 2009 16:22:37 +0100
